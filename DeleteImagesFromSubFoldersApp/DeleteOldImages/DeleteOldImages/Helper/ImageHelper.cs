﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SearchFeedApp.Helper;

namespace DeleteOldImages
{
    public class ImageHelper
    {
        #region Private Global Member

        private static string appPath = AppDomain.CurrentDomain.BaseDirectory;
        List<string> SourceImagesNotFound = new List<string>();

        #endregion

        /// <summary>
        /// Delete Old Image in Folder
        /// </summary>
        /// <param name="imageName"></param>
        public static int DeleteOldImage(string imageName)
        {
            StringBuilder logErrorMessage = new StringBuilder();
            int totalLocation = 0, imageExistLocation = 0;
            try
            {
                string directoryPath = ConfigurationSettings.AppSettings["MainDirectory"] != null ? ConfigurationSettings.AppSettings["MainDirectory"].ToString() : string.Empty;
                if (!string.IsNullOrWhiteSpace(directoryPath) && !string.IsNullOrWhiteSpace(imageName))
                {
                    string[] subFolders = System.IO.Directory.GetDirectories(directoryPath, "*", System.IO.SearchOption.TopDirectoryOnly);
                    totalLocation = subFolders.Length - 1;//Excluding Origional Folder
                    for (int index = 0; index < subFolders.Length; index++)
                    {
                        //Dont Delete Image From Origional Folder
                        if (!subFolders[index].ToString().Substring(subFolders[index].ToString().LastIndexOf("\\")).Equals("\\original", StringComparison.InvariantCultureIgnoreCase))
                        {
                            bool isFound = false;
                            StringBuilder logMessage = new StringBuilder();
                            string fileStatus = string.Empty;
                            string currentDir = subFolders[index].ToString();
                            string filePath = currentDir + "\\" + imageName;
                            if (File.Exists(filePath))
                            {
                                isFound = true;
                                File.Delete(filePath);
                                imageExistLocation++;
                            }
                            //log Image Found In Location details
                            fileStatus = isFound ? "Found and Deleted!!" : "Not Found";
                            logMessage.AppendLine(imageName);
                            logMessage.AppendLine(fileStatus);
                            logMessage.AppendLine(currentDir);
                            LogWritter.Write(appPath, logMessage);
                        }
                    }
                    //log Image Found In Location
                    logErrorMessage.AppendLine("Found In " + imageExistLocation + "Location out Of " + totalLocation);
                    LogWritter.Write(appPath, logErrorMessage);
                }
            }
            catch (Exception ex)
            {
                logErrorMessage.Append(ex.StackTrace);
                LogWritter.Write(appPath, logErrorMessage);
            }
            return imageExistLocation;
        }

        /// <summary>
        /// Get All Images From Folder
        /// </summary>
        /// <param name="searchFolder">string</param>
        /// <param name="filters">String[]</param>
        /// <param name="isRecursive">bool</param>
        /// <returns></returns>
        public static String[] GetFilesFrom(string searchFolder, String[] filters, bool isRecursive)
        {
            List<String> filesFound = new List<String>();
            if (!string.IsNullOrEmpty(searchFolder))
            {
                var searchOption = isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
                foreach (var filter in filters)
                {
                    filesFound.AddRange(Directory.GetFiles(searchFolder, String.Format("*.{0}", filter), searchOption));
                }
            }
            return filesFound.ToArray();
        }

        /// <summary>
        /// Delete Images Fom Folder
        /// </summary>
        public static void DeleteImages()
        {
            StringBuilder logMessage = new StringBuilder();
            try
            {
                string searchFolder = ConfigurationSettings.AppSettings["SourceDirectory"] != null ? ConfigurationSettings.AppSettings["SourceDirectory"].ToString() : string.Empty;
                var filters = new String[] { "jpg", "jpeg", "png", "gif", "tiff", "bmp" };
                var files = ImageHelper.GetFilesFrom(searchFolder, filters, false);
                if (files != null)
                {
                    //Move all Files To BackUp 
                    MoveToBackUpFolder(searchFolder);
                    foreach (var file in files)
                    {
                        string oldImage = file.ToString().Substring(file.ToString().LastIndexOf("\\")).Replace("\\", string.Empty);
                        int deleteFromLocation = ImageHelper.DeleteOldImage(oldImage);
                        if (deleteFromLocation <= 0)
                        {
                            logMessage.Append(oldImage + "********* : : Not Found In Any Sub Directory*************************");
                            LogWritter.Write(appPath, logMessage);

                        }
                        //Delete Image From Source Folder 
                        File.Delete(file.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                logMessage.Append(ex.StackTrace);
                LogWritter.Write(appPath, logMessage);
            }

        }

        /// <summary>
        /// Move all Files to BackUp Directory
        /// </summary>
        /// <param name="sourcePath">string</param>
        private static void MoveToBackUpFolder(string sourcePath)
        {
            StringBuilder logMessage = new StringBuilder();
            try
            {
                var filters = new String[] { "jpg", "jpeg", "png", "gif", "tiff", "bmp" };
                string backUpFolder = ConfigurationSettings.AppSettings["BackUpDirectory"] != null ? ConfigurationSettings.AppSettings["BackUpDirectory"].ToString() : string.Empty;
                if (!string.IsNullOrEmpty(sourcePath) && !string.IsNullOrEmpty(backUpFolder))
                {
                    string backupDirName = backUpFolder + string.Format("\\BackUp_{0}-{1}-{2}", System.DateTime.Today.Year, System.DateTime.Today.Month, System.DateTime.Today.Day);
                    Directory.CreateDirectory(backupDirName);
                    var files = ImageHelper.GetFilesFrom(sourcePath, filters, false);
                    if (files != null)
                    {
                        //Copy all the files & Replaces any files with the same name
                        foreach (string newPath in files)
                        {
                            File.Copy(newPath, newPath.Replace(sourcePath, backupDirName), true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logMessage.Append(ex.StackTrace);
                LogWritter.Write(appPath, logMessage);
            }

        }
    }
}

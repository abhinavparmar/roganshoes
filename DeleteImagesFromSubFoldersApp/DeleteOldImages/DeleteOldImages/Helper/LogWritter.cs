﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SearchFeedApp.Helper
{
    public class LogWritter
    {
        public static void Write(string filePath, StringBuilder message)
        {
           string fileName = filePath +"DeleteOldImageLog_"+ System.DateTime.Today.Year + "-" + System.DateTime.Today.Month + "-" + System.DateTime.Today.Day + ".txt";
            TextWriter textWriter = new StreamWriter(fileName,true);
            textWriter.WriteLine("************************");
            textWriter.WriteLine(message);
            textWriter.WriteLine (DateTime.Now.ToString());
            textWriter.WriteLine("************************");
            textWriter.WriteLine("");
            textWriter.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SearchFeedApp.Helper;

namespace DeleteOldImages
{
    class Program
    {
        static void Main(string[] args)
        {
            string appPath = AppDomain.CurrentDomain.BaseDirectory;
            StringBuilder logMessage = new StringBuilder();
            logMessage.Append(DateTime.Now.ToString() + ":Deleting  Images...Started" + Environment.NewLine + " " + System.DateTime.Now);
            LogWritter.Write(appPath, logMessage);
            ImageHelper.DeleteImages();
            logMessage.Append(DateTime.Now.ToString() + ":Deleting  Images...Done" + Environment.NewLine + " " + System.DateTime.Now);
            LogWritter.Write(appPath, logMessage);
           
        }
    }
}

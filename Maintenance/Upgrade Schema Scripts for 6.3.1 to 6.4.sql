/*
Run this script on:

        BUILD-PC\SQLEXPRESS.Znode_Multifront631    -  This database will be modified

to synchronize it with:

        BUILD-PC\SQLEXPRESS.znode_multifront

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.50.10 from Red Gate Software Ltd at 4/5/2011 3:37:04 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[ZNodeReferralCommission]'
GO
ALTER TABLE [dbo].[ZNodeReferralCommission] DROP
CONSTRAINT [FK_ZnodeReferralCommission_ZnodeReferralCommissionType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[ZNodeIPCommerce]'
GO
ALTER TABLE [dbo].[ZNodeIPCommerce] DROP
CONSTRAINT [FK_ZNodeIPCommerce_ZNodePortal]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[ZNodeSavedPaymentMethod]'
GO
ALTER TABLE [dbo].[ZNodeSavedPaymentMethod] DROP
CONSTRAINT [FK_ZNodeSavedPaymentMethod_ZNodeAccount],
CONSTRAINT [FK_ZNodeSavedPaymentMethod_ZNodePaymentSetting]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[ZNodeVendorUser]'
GO
ALTER TABLE [dbo].[ZNodeVendorUser] DROP
CONSTRAINT [FK_ZNodeVendorUser_ZNodeSupplier]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeIPCommerce]'
GO
ALTER TABLE [dbo].[ZNodeIPCommerce] DROP CONSTRAINT [PK_ZNodeIPCommerce]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeSavedPaymentMethod]'
GO
ALTER TABLE [dbo].[ZNodeSavedPaymentMethod] DROP CONSTRAINT [PK_ZNodeAccountPaymentMethod]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeVendorUser]'
GO
ALTER TABLE [dbo].[ZNodeVendorUser] DROP CONSTRAINT [PK_ZNodeVendorUser]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeAccount]'
GO
ALTER TABLE [dbo].[ZNodeAccount] DROP CONSTRAINT [DF__account__Billing__3F672F2C]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeAccount]'
GO
ALTER TABLE [dbo].[ZNodeAccount] DROP CONSTRAINT [DF__account__Billing__405B5365]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeAccount]'
GO
ALTER TABLE [dbo].[ZNodeAccount] DROP CONSTRAINT [DF__account__Billing__414F779E]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeAccount]'
GO
ALTER TABLE [dbo].[ZNodeAccount] DROP CONSTRAINT [DF__account__Billing__42439BD7]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeAccount]'
GO
ALTER TABLE [dbo].[ZNodeAccount] DROP CONSTRAINT [DF__account__Billing__4337C010]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeAccount]'
GO
ALTER TABLE [dbo].[ZNodeAccount] DROP CONSTRAINT [DF__account__ShipFir__442BE449]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeAccount]'
GO
ALTER TABLE [dbo].[ZNodeAccount] DROP CONSTRAINT [DF__account__ShipLas__45200882]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeAccount]'
GO
ALTER TABLE [dbo].[ZNodeAccount] DROP CONSTRAINT [DF__account__ShipCom__46142CBB]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeAccount]'
GO
ALTER TABLE [dbo].[ZNodeAccount] DROP CONSTRAINT [DF__account__ShipStr__470850F4]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeAccount]'
GO
ALTER TABLE [dbo].[ZNodeAccount] DROP CONSTRAINT [DF__account__ShipPho__47FC752D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_ZNodeMessageConfig] from [dbo].[ZNodeMessageConfig]'
GO
DROP INDEX [IX_ZNodeMessageConfig] ON [dbo].[ZNodeMessageConfig]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_ZNodeIPCommerce] from [dbo].[ZNodeIPCommerce]'
GO
DROP INDEX [IX_ZNodeIPCommerce] ON [dbo].[ZNodeIPCommerce]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_GetProductsByKeyword_Helper]'
GO
DROP PROCEDURE [dbo].[ZNode_GetProductsByKeyword_Helper]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_DeletePortalCatalog]'
GO
DROP PROCEDURE [dbo].[ZNode_DeletePortalCatalog]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_GetCategoryNodeDetail]'
GO
DROP PROCEDURE [dbo].[ZNode_GetCategoryNodeDetail]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_GetAccountsBySupplierId]'
GO
DROP PROCEDURE [dbo].[ZNode_GetAccountsBySupplierId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_GetAttribuetypesByProductType]'
GO
DROP PROCEDURE [dbo].[ZNode_GetAttribuetypesByProductType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_GetProductsByCategory_Helper]'
GO
DROP PROCEDURE [dbo].[ZNode_GetProductsByCategory_Helper]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_GetCategoryHierarchy_Helper]'
GO
DROP PROCEDURE [dbo].[ZNode_GetCategoryHierarchy_Helper]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_GetAttribuetypesByProductTypeID]'
GO
DROP PROCEDURE [dbo].[ZNode_GetAttribuetypesByProductTypeID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_GetSkuAttributes_Attributes]'
GO
DROP PROCEDURE [dbo].[ZNode_GetSkuAttributes_Attributes]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNODE_GetCategoriesTree]'
GO
DROP PROCEDURE [dbo].[ZNODE_GetCategoriesTree]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_GetAttribuesByProductType]'
GO
DROP PROCEDURE [dbo].[ZNode_GetAttribuesByProductType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_TruncateZipcodeData]'
GO
DROP PROCEDURE [dbo].[ZNode_TruncateZipcodeData]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNodeVendorUser]'
GO
DROP TABLE [dbo].[ZNodeVendorUser]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNodeSavedPaymentMethod]'
GO
DROP TABLE [dbo].[ZNodeSavedPaymentMethod]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNodeIPCommerce]'
GO
DROP TABLE [dbo].[ZNodeIPCommerce]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeParentChildProduct]'
GO
CREATE TABLE [dbo].[ZNodeParentChildProduct]
(
[ParentChildProductID] [int] NOT NULL IDENTITY(1, 1),
[ParentProductID] [int] NULL,
[ChildProductID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeParentChildProduct] on [dbo].[ZNodeParentChildProduct]'
GO
ALTER TABLE [dbo].[ZNodeParentChildProduct] ADD CONSTRAINT [PK_ZNodeParentChildProduct] PRIMARY KEY CLUSTERED  ([ParentChildProductID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeParentChildProduct] on [dbo].[ZNodeParentChildProduct]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ZNodeParentChildProduct] ON [dbo].[ZNodeParentChildProduct] ([ParentProductID], [ChildProductID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeParentChildProduct_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeParentChildProduct table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeParentChildProduct_Get_List

AS


				
				SELECT
					[ParentChildProductID],
					[ParentProductID],
					[ChildProductID]
				FROM
					[dbo].[ZNodeParentChildProduct]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeParentChildProduct_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeParentChildProduct table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeParentChildProduct_Insert
(

	@ParentChildProductID int    OUTPUT,

	@ParentProductID int   ,

	@ChildProductID int   
)
AS


				
				INSERT INTO [dbo].[ZNodeParentChildProduct]
					(
					[ParentProductID]
					,[ChildProductID]
					)
				VALUES
					(
					@ParentProductID
					,@ChildProductID
					)
				
				-- Get the identity value
				SET @ParentChildProductID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeParentChildProduct_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeParentChildProduct table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeParentChildProduct_Update
(

	@ParentChildProductID int   ,

	@ParentProductID int   ,

	@ChildProductID int   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeParentChildProduct]
				SET
					[ParentProductID] = @ParentProductID
					,[ChildProductID] = @ChildProductID
				WHERE
[ParentChildProductID] = @ParentChildProductID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeParentChildProduct_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeParentChildProduct table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeParentChildProduct_Delete
(

	@ParentChildProductID int   
)
AS


				DELETE FROM [dbo].[ZNodeParentChildProduct] WITH (ROWLOCK) 
				WHERE
					[ParentChildProductID] = @ParentChildProductID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeParentChildProduct_GetByParentProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeParentChildProduct table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeParentChildProduct_GetByParentProductID
(

	@ParentProductID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ParentChildProductID],
					[ParentProductID],
					[ChildProductID]
				FROM
					[dbo].[ZNodeParentChildProduct]
				WHERE
					[ParentProductID] = @ParentProductID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeParentChildProduct_GetByChildProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeParentChildProduct table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeParentChildProduct_GetByChildProductID
(

	@ChildProductID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ParentChildProductID],
					[ParentProductID],
					[ChildProductID]
				FROM
					[dbo].[ZNodeParentChildProduct]
				WHERE
					[ChildProductID] = @ChildProductID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeParentChildProduct_GetByParentProductIDChildProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeParentChildProduct table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeParentChildProduct_GetByParentProductIDChildProductID
(

	@ParentProductID int   ,

	@ChildProductID int   
)
AS


				SELECT
					[ParentChildProductID],
					[ParentProductID],
					[ChildProductID]
				FROM
					[dbo].[ZNodeParentChildProduct]
				WHERE
					[ParentProductID] = @ParentProductID
					AND [ChildProductID] = @ChildProductID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeParentChildProduct_GetByParentChildProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeParentChildProduct table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeParentChildProduct_GetByParentChildProductID
(

	@ParentChildProductID int   
)
AS


				SELECT
					[ParentChildProductID],
					[ParentProductID],
					[ChildProductID]
				FROM
					[dbo].[ZNodeParentChildProduct]
				WHERE
					[ParentChildProductID] = @ParentChildProductID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeParentChildProduct_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeParentChildProduct table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeParentChildProduct_Find
(

	@SearchUsingOR bit   = null ,

	@ParentChildProductID int   = null ,

	@ParentProductID int   = null ,

	@ChildProductID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ParentChildProductID]
	, [ParentProductID]
	, [ChildProductID]
    FROM
	[dbo].[ZNodeParentChildProduct]
    WHERE 
	 ([ParentChildProductID] = @ParentChildProductID OR @ParentChildProductID IS NULL)
	AND ([ParentProductID] = @ParentProductID OR @ParentProductID IS NULL)
	AND ([ChildProductID] = @ChildProductID OR @ChildProductID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ParentChildProductID]
	, [ParentProductID]
	, [ChildProductID]
    FROM
	[dbo].[ZNodeParentChildProduct]
    WHERE 
	 ([ParentChildProductID] = @ParentChildProductID AND @ParentChildProductID is not null)
	OR ([ParentProductID] = @ParentProductID AND @ParentProductID is not null)
	OR ([ChildProductID] = @ChildProductID AND @ChildProductID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodePaymentToken]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodePaymentToken]
(
[PaymentTokenID] [int] NOT NULL IDENTITY(1, 1),
[AccountID] [int] NOT NULL,
[PaymentSettingID] [int] NOT NULL,
[CreditCardDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardAuthCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardTransactionID] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardExp] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodePaymentToken] on [dbo].[ZNodePaymentToken]'
GO
ALTER TABLE [dbo].[ZNodePaymentToken] ADD CONSTRAINT [PK_ZNodePaymentToken] PRIMARY KEY CLUSTERED  ([PaymentTokenID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentToken_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodePaymentToken table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentToken_Get_List

AS


				
				SELECT
					[PaymentTokenID],
					[AccountID],
					[PaymentSettingID],
					[CreditCardDescription],
					[CardAuthCode],
					[CardTransactionID],
					[CardExp]
				FROM
					[dbo].[ZNodePaymentToken]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_DeleteParentChildProduct]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ZNode_DeleteParentChildProduct](@ParentProductID int,@ChildProductID int)
AS      
BEGIN      
	DELETE FROM ZNodeParentChildProduct WHERE ParentProductID = @ParentProductID AND ChildProductID = @ChildProductID;      
END; 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentToken_Insert]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodePaymentToken table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentToken_Insert
(

	@PaymentTokenID int    OUTPUT,

	@AccountID int   ,

	@PaymentSettingID int   ,

	@CreditCardDescription nvarchar (MAX)  ,

	@CardAuthCode nvarchar (MAX)  ,

	@CardTransactionID nvarchar (MAX)  ,

	@CardExp datetime   
)
AS


				
				INSERT INTO [dbo].[ZNodePaymentToken]
					(
					[AccountID]
					,[PaymentSettingID]
					,[CreditCardDescription]
					,[CardAuthCode]
					,[CardTransactionID]
					,[CardExp]
					)
				VALUES
					(
					@AccountID
					,@PaymentSettingID
					,@CreditCardDescription
					,@CardAuthCode
					,@CardTransactionID
					,@CardExp
					)
				
				-- Get the identity value
				SET @PaymentTokenID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentToken_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodePaymentToken table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentToken_Update
(

	@PaymentTokenID int   ,

	@AccountID int   ,

	@PaymentSettingID int   ,

	@CreditCardDescription nvarchar (MAX)  ,

	@CardAuthCode nvarchar (MAX)  ,

	@CardTransactionID nvarchar (MAX)  ,

	@CardExp datetime   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodePaymentToken]
				SET
					[AccountID] = @AccountID
					,[PaymentSettingID] = @PaymentSettingID
					,[CreditCardDescription] = @CreditCardDescription
					,[CardAuthCode] = @CardAuthCode
					,[CardTransactionID] = @CardTransactionID
					,[CardExp] = @CardExp
				WHERE
[PaymentTokenID] = @PaymentTokenID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentToken_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodePaymentToken table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentToken_Delete
(

	@PaymentTokenID int   
)
AS


				DELETE FROM [dbo].[ZNodePaymentToken] WITH (ROWLOCK) 
				WHERE
					[PaymentTokenID] = @PaymentTokenID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentToken_GetByAccountID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePaymentToken table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentToken_GetByAccountID
(

	@AccountID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PaymentTokenID],
					[AccountID],
					[PaymentSettingID],
					[CreditCardDescription],
					[CardAuthCode],
					[CardTransactionID],
					[CardExp]
				FROM
					[dbo].[ZNodePaymentToken]
				WHERE
					[AccountID] = @AccountID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentToken_GetByPaymentSettingID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePaymentToken table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentToken_GetByPaymentSettingID
(

	@PaymentSettingID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PaymentTokenID],
					[AccountID],
					[PaymentSettingID],
					[CreditCardDescription],
					[CardAuthCode],
					[CardTransactionID],
					[CardExp]
				FROM
					[dbo].[ZNodePaymentToken]
				WHERE
					[PaymentSettingID] = @PaymentSettingID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentToken_GetByPaymentTokenID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePaymentToken table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentToken_GetByPaymentTokenID
(

	@PaymentTokenID int   
)
AS


				SELECT
					[PaymentTokenID],
					[AccountID],
					[PaymentSettingID],
					[CreditCardDescription],
					[CardAuthCode],
					[CardTransactionID],
					[CardExp]
				FROM
					[dbo].[ZNodePaymentToken]
				WHERE
					[PaymentTokenID] = @PaymentTokenID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentToken_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodePaymentToken table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentToken_Find
(

	@SearchUsingOR bit   = null ,

	@PaymentTokenID int   = null ,

	@AccountID int   = null ,

	@PaymentSettingID int   = null ,

	@CreditCardDescription nvarchar (MAX)  = null ,

	@CardAuthCode nvarchar (MAX)  = null ,

	@CardTransactionID nvarchar (MAX)  = null ,

	@CardExp datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PaymentTokenID]
	, [AccountID]
	, [PaymentSettingID]
	, [CreditCardDescription]
	, [CardAuthCode]
	, [CardTransactionID]
	, [CardExp]
    FROM
	[dbo].[ZNodePaymentToken]
    WHERE 
	 ([PaymentTokenID] = @PaymentTokenID OR @PaymentTokenID IS NULL)
	AND ([AccountID] = @AccountID OR @AccountID IS NULL)
	AND ([PaymentSettingID] = @PaymentSettingID OR @PaymentSettingID IS NULL)
	AND ([CreditCardDescription] = @CreditCardDescription OR @CreditCardDescription IS NULL)
	AND ([CardAuthCode] = @CardAuthCode OR @CardAuthCode IS NULL)
	AND ([CardTransactionID] = @CardTransactionID OR @CardTransactionID IS NULL)
	AND ([CardExp] = @CardExp OR @CardExp IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PaymentTokenID]
	, [AccountID]
	, [PaymentSettingID]
	, [CreditCardDescription]
	, [CardAuthCode]
	, [CardTransactionID]
	, [CardExp]
    FROM
	[dbo].[ZNodePaymentToken]
    WHERE 
	 ([PaymentTokenID] = @PaymentTokenID AND @PaymentTokenID is not null)
	OR ([AccountID] = @AccountID AND @AccountID is not null)
	OR ([PaymentSettingID] = @PaymentSettingID AND @PaymentSettingID is not null)
	OR ([CreditCardDescription] = @CreditCardDescription AND @CreditCardDescription is not null)
	OR ([CardAuthCode] = @CardAuthCode AND @CardAuthCode is not null)
	OR ([CardTransactionID] = @CardTransactionID AND @CardTransactionID is not null)
	OR ([CardExp] = @CardExp AND @CardExp is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeOrderLineItemRelationshipType]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodeOrderLineItemRelationshipType]
(
[OrderLineItemRelationshipTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
SET IDENTITY_INSERT [dbo].[ZNodeOrderLineItemRelationshipType] ON
GO
INSERT INTO [dbo].[ZNodeOrderLineItemRelationshipType] ([OrderLineItemRelationshipTypeID], [Name]) VALUES (1, N'Product Bundles')
INSERT INTO [dbo].[ZNodeOrderLineItemRelationshipType] ([OrderLineItemRelationshipTypeID], [Name]) VALUES (2, N'Add-Ons')
GO
SET IDENTITY_INSERT [dbo].[ZNodeOrderLineItemRelationshipType] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeOrderLineItemRelationshipType] on [dbo].[ZNodeOrderLineItemRelationshipType]'
GO
ALTER TABLE [dbo].[ZNodeOrderLineItemRelationshipType] ADD CONSTRAINT [PK_ZNodeOrderLineItemRelationshipType] PRIMARY KEY CLUSTERED  ([OrderLineItemRelationshipTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeOrderLineItemRelationshipType_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeOrderLineItemRelationshipType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItemRelationshipType_Get_List

AS


				
				SELECT
					[OrderLineItemRelationshipTypeID],
					[Name]
				FROM
					[dbo].[ZNodeOrderLineItemRelationshipType]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeOrderLineItemRelationshipType_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeOrderLineItemRelationshipType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItemRelationshipType_Insert
(

	@OrderLineItemRelationshipTypeID int    OUTPUT,

	@Name nvarchar (100)  
)
AS


				
				INSERT INTO [dbo].[ZNodeOrderLineItemRelationshipType]
					(
					[Name]
					)
				VALUES
					(
					@Name
					)
				
				-- Get the identity value
				SET @OrderLineItemRelationshipTypeID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeOrderLineItemRelationshipType_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeOrderLineItemRelationshipType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItemRelationshipType_Update
(

	@OrderLineItemRelationshipTypeID int   ,

	@Name nvarchar (100)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeOrderLineItemRelationshipType]
				SET
					[Name] = @Name
				WHERE
[OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeOrderLineItemRelationshipType_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeOrderLineItemRelationshipType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItemRelationshipType_Delete
(

	@OrderLineItemRelationshipTypeID int   
)
AS


				DELETE FROM [dbo].[ZNodeOrderLineItemRelationshipType] WITH (ROWLOCK) 
				WHERE
					[OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeOrderLineItemRelationshipType_GetByOrderLineItemRelationshipTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItemRelationshipType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItemRelationshipType_GetByOrderLineItemRelationshipTypeID
(

	@OrderLineItemRelationshipTypeID int   
)
AS


				SELECT
					[OrderLineItemRelationshipTypeID],
					[Name]
				FROM
					[dbo].[ZNodeOrderLineItemRelationshipType]
				WHERE
					[OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeOrderLineItemRelationshipType_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeOrderLineItemRelationshipType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItemRelationshipType_Find
(

	@SearchUsingOR bit   = null ,

	@OrderLineItemRelationshipTypeID int   = null ,

	@Name nvarchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [OrderLineItemRelationshipTypeID]
	, [Name]
    FROM
	[dbo].[ZNodeOrderLineItemRelationshipType]
    WHERE 
	 ([OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID OR @OrderLineItemRelationshipTypeID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [OrderLineItemRelationshipTypeID]
	, [Name]
    FROM
	[dbo].[ZNodeOrderLineItemRelationshipType]
    WHERE 
	 ([OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID AND @OrderLineItemRelationshipTypeID is not null)
	OR ([Name] = @Name AND @Name is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeSavedCart]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodeSavedCart]
(
[SavedCartID] [int] NOT NULL IDENTITY(1, 1),
[CookieMappingID] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[LastUpdatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeSavedCart] on [dbo].[ZNodeSavedCart]'
GO
ALTER TABLE [dbo].[ZNodeSavedCart] ADD CONSTRAINT [PK_ZNodeSavedCart] PRIMARY KEY CLUSTERED  ([SavedCartID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeCookieMapping]'
GO
CREATE TABLE [dbo].[ZNodeCookieMapping]
(
[CookieMappingID] [int] NOT NULL IDENTITY(1, 1),
[AccountID] [int] NULL,
[PortalID] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[LastUpdatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeCookieMapping] on [dbo].[ZNodeCookieMapping]'
GO
ALTER TABLE [dbo].[ZNodeCookieMapping] ADD CONSTRAINT [PK_ZNodeCookieMapping] PRIMARY KEY CLUSTERED  ([CookieMappingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeCookieMapping] on [dbo].[ZNodeCookieMapping]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeCookieMapping] ON [dbo].[ZNodeCookieMapping] ([AccountID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodePaymentTokenAuthorize]'
GO
CREATE TABLE [dbo].[ZNodePaymentTokenAuthorize]
(
[PaymentTokenAuthorizeID] [int] NOT NULL IDENTITY(1, 1),
[PaymentTokenID] [int] NOT NULL,
[PaymentProfileID] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerProfileID] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShippingProfileID] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodePaymentTokenAuthorize] on [dbo].[ZNodePaymentTokenAuthorize]'
GO
ALTER TABLE [dbo].[ZNodePaymentTokenAuthorize] ADD CONSTRAINT [PK_ZNodePaymentTokenAuthorize] PRIMARY KEY CLUSTERED  ([PaymentTokenAuthorizeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentTokenAuthorize_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodePaymentTokenAuthorize table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentTokenAuthorize_Get_List

AS


				
				SELECT
					[PaymentTokenAuthorizeID],
					[PaymentTokenID],
					[PaymentProfileID],
					[CustomerProfileID],
					[ShippingProfileID]
				FROM
					[dbo].[ZNodePaymentTokenAuthorize]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentTokenAuthorize_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodePaymentTokenAuthorize table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentTokenAuthorize_Insert
(

	@PaymentTokenAuthorizeID int    OUTPUT,

	@PaymentTokenID int   ,

	@PaymentProfileID nvarchar (MAX)  ,

	@CustomerProfileID nvarchar (MAX)  ,

	@ShippingProfileID nvarchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[ZNodePaymentTokenAuthorize]
					(
					[PaymentTokenID]
					,[PaymentProfileID]
					,[CustomerProfileID]
					,[ShippingProfileID]
					)
				VALUES
					(
					@PaymentTokenID
					,@PaymentProfileID
					,@CustomerProfileID
					,@ShippingProfileID
					)
				
				-- Get the identity value
				SET @PaymentTokenAuthorizeID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentTokenAuthorize_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodePaymentTokenAuthorize table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentTokenAuthorize_Update
(

	@PaymentTokenAuthorizeID int   ,

	@PaymentTokenID int   ,

	@PaymentProfileID nvarchar (MAX)  ,

	@CustomerProfileID nvarchar (MAX)  ,

	@ShippingProfileID nvarchar (MAX)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodePaymentTokenAuthorize]
				SET
					[PaymentTokenID] = @PaymentTokenID
					,[PaymentProfileID] = @PaymentProfileID
					,[CustomerProfileID] = @CustomerProfileID
					,[ShippingProfileID] = @ShippingProfileID
				WHERE
[PaymentTokenAuthorizeID] = @PaymentTokenAuthorizeID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentTokenAuthorize_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodePaymentTokenAuthorize table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentTokenAuthorize_Delete
(

	@PaymentTokenAuthorizeID int   
)
AS


				DELETE FROM [dbo].[ZNodePaymentTokenAuthorize] WITH (ROWLOCK) 
				WHERE
					[PaymentTokenAuthorizeID] = @PaymentTokenAuthorizeID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentTokenAuthorize_GetByPaymentTokenID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePaymentTokenAuthorize table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentTokenAuthorize_GetByPaymentTokenID
(

	@PaymentTokenID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PaymentTokenAuthorizeID],
					[PaymentTokenID],
					[PaymentProfileID],
					[CustomerProfileID],
					[ShippingProfileID]
				FROM
					[dbo].[ZNodePaymentTokenAuthorize]
				WHERE
					[PaymentTokenID] = @PaymentTokenID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeTrackingOutbound]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodeTrackingOutbound]
(
[TrackingOutboundID] [int] NOT NULL IDENTITY(1, 1),
[ProductID] [int] NULL,
[Date] [date] NULL,
[FromUrl] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToUrl] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EventName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [money] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeTrackingOutbound] on [dbo].[ZNodeTrackingOutbound]'
GO
ALTER TABLE [dbo].[ZNodeTrackingOutbound] ADD CONSTRAINT [PK_ZNodeTrackingOutbound] PRIMARY KEY CLUSTERED  ([TrackingOutboundID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeTrackingOutbound_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeTrackingOutbound table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeTrackingOutbound_Get_List

AS


				
				SELECT
					[TrackingOutboundID],
					[ProductID],
					[Date],
					[FromUrl],
					[ToUrl],
					[EventName],
					[Price]
				FROM
					[dbo].[ZNodeTrackingOutbound]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentTokenAuthorize_GetByPaymentTokenAuthorizeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePaymentTokenAuthorize table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentTokenAuthorize_GetByPaymentTokenAuthorizeID
(

	@PaymentTokenAuthorizeID int   
)
AS


				SELECT
					[PaymentTokenAuthorizeID],
					[PaymentTokenID],
					[PaymentProfileID],
					[CustomerProfileID],
					[ShippingProfileID]
				FROM
					[dbo].[ZNodePaymentTokenAuthorize]
				WHERE
					[PaymentTokenAuthorizeID] = @PaymentTokenAuthorizeID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentTokenAuthorize_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodePaymentTokenAuthorize table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentTokenAuthorize_Find
(

	@SearchUsingOR bit   = null ,

	@PaymentTokenAuthorizeID int   = null ,

	@PaymentTokenID int   = null ,

	@PaymentProfileID nvarchar (MAX)  = null ,

	@CustomerProfileID nvarchar (MAX)  = null ,

	@ShippingProfileID nvarchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PaymentTokenAuthorizeID]
	, [PaymentTokenID]
	, [PaymentProfileID]
	, [CustomerProfileID]
	, [ShippingProfileID]
    FROM
	[dbo].[ZNodePaymentTokenAuthorize]
    WHERE 
	 ([PaymentTokenAuthorizeID] = @PaymentTokenAuthorizeID OR @PaymentTokenAuthorizeID IS NULL)
	AND ([PaymentTokenID] = @PaymentTokenID OR @PaymentTokenID IS NULL)
	AND ([PaymentProfileID] = @PaymentProfileID OR @PaymentProfileID IS NULL)
	AND ([CustomerProfileID] = @CustomerProfileID OR @CustomerProfileID IS NULL)
	AND ([ShippingProfileID] = @ShippingProfileID OR @ShippingProfileID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PaymentTokenAuthorizeID]
	, [PaymentTokenID]
	, [PaymentProfileID]
	, [CustomerProfileID]
	, [ShippingProfileID]
    FROM
	[dbo].[ZNodePaymentTokenAuthorize]
    WHERE 
	 ([PaymentTokenAuthorizeID] = @PaymentTokenAuthorizeID AND @PaymentTokenAuthorizeID is not null)
	OR ([PaymentTokenID] = @PaymentTokenID AND @PaymentTokenID is not null)
	OR ([PaymentProfileID] = @PaymentProfileID AND @PaymentProfileID is not null)
	OR ([CustomerProfileID] = @CustomerProfileID AND @CustomerProfileID is not null)
	OR ([ShippingProfileID] = @ShippingProfileID AND @ShippingProfileID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeTrackingOutbound_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeTrackingOutbound table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeTrackingOutbound_Insert
(

	@TrackingOutboundID int    OUTPUT,

	@ProductID int   ,

	@Date date   ,

	@FromUrl nvarchar (MAX)  ,

	@ToUrl nvarchar (MAX)  ,

	@EventName nvarchar (MAX)  ,

	@Price money   
)
AS


				
				INSERT INTO [dbo].[ZNodeTrackingOutbound]
					(
					[ProductID]
					,[Date]
					,[FromUrl]
					,[ToUrl]
					,[EventName]
					,[Price]
					)
				VALUES
					(
					@ProductID
					,@Date
					,@FromUrl
					,@ToUrl
					,@EventName
					,@Price
					)
				
				-- Get the identity value
				SET @TrackingOutboundID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeTrackingOutbound_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeTrackingOutbound table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeTrackingOutbound_Update
(

	@TrackingOutboundID int   ,

	@ProductID int   ,

	@Date date   ,

	@FromUrl nvarchar (MAX)  ,

	@ToUrl nvarchar (MAX)  ,

	@EventName nvarchar (MAX)  ,

	@Price money   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeTrackingOutbound]
				SET
					[ProductID] = @ProductID
					,[Date] = @Date
					,[FromUrl] = @FromUrl
					,[ToUrl] = @ToUrl
					,[EventName] = @EventName
					,[Price] = @Price
				WHERE
[TrackingOutboundID] = @TrackingOutboundID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeTrackingOutbound_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeTrackingOutbound table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeTrackingOutbound_Delete
(

	@TrackingOutboundID int   
)
AS


				DELETE FROM [dbo].[ZNodeTrackingOutbound] WITH (ROWLOCK) 
				WHERE
					[TrackingOutboundID] = @TrackingOutboundID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeTrackingOutbound_GetByProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeTrackingOutbound table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeTrackingOutbound_GetByProductID
(

	@ProductID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[TrackingOutboundID],
					[ProductID],
					[Date],
					[FromUrl],
					[ToUrl],
					[EventName],
					[Price]
				FROM
					[dbo].[ZNodeTrackingOutbound]
				WHERE
					[ProductID] = @ProductID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeTrackingOutbound_GetByTrackingOutboundID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeTrackingOutbound table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeTrackingOutbound_GetByTrackingOutboundID
(

	@TrackingOutboundID int   
)
AS


				SELECT
					[TrackingOutboundID],
					[ProductID],
					[Date],
					[FromUrl],
					[ToUrl],
					[EventName],
					[Price]
				FROM
					[dbo].[ZNodeTrackingOutbound]
				WHERE
					[TrackingOutboundID] = @TrackingOutboundID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeTrackingOutbound_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeTrackingOutbound table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeTrackingOutbound_Find
(

	@SearchUsingOR bit   = null ,

	@TrackingOutboundID int   = null ,

	@ProductID int   = null ,

	@Date date   = null ,

	@FromUrl nvarchar (MAX)  = null ,

	@ToUrl nvarchar (MAX)  = null ,

	@EventName nvarchar (MAX)  = null ,

	@Price money   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [TrackingOutboundID]
	, [ProductID]
	, [Date]
	, [FromUrl]
	, [ToUrl]
	, [EventName]
	, [Price]
    FROM
	[dbo].[ZNodeTrackingOutbound]
    WHERE 
	 ([TrackingOutboundID] = @TrackingOutboundID OR @TrackingOutboundID IS NULL)
	AND ([ProductID] = @ProductID OR @ProductID IS NULL)
	AND ([Date] = @Date OR @Date IS NULL)
	AND ([FromUrl] = @FromUrl OR @FromUrl IS NULL)
	AND ([ToUrl] = @ToUrl OR @ToUrl IS NULL)
	AND ([EventName] = @EventName OR @EventName IS NULL)
	AND ([Price] = @Price OR @Price IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [TrackingOutboundID]
	, [ProductID]
	, [Date]
	, [FromUrl]
	, [ToUrl]
	, [EventName]
	, [Price]
    FROM
	[dbo].[ZNodeTrackingOutbound]
    WHERE 
	 ([TrackingOutboundID] = @TrackingOutboundID AND @TrackingOutboundID is not null)
	OR ([ProductID] = @ProductID AND @ProductID is not null)
	OR ([Date] = @Date AND @Date is not null)
	OR ([FromUrl] = @FromUrl AND @FromUrl is not null)
	OR ([ToUrl] = @ToUrl AND @ToUrl is not null)
	OR ([EventName] = @EventName AND @EventName is not null)
	OR ([Price] = @Price AND @Price is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCart_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeSavedCart table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCart_Get_List

AS


				
				SELECT
					[SavedCartID],
					[CookieMappingID],
					[CreatedDate],
					[LastUpdatedDate]
				FROM
					[dbo].[ZNodeSavedCart]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCart_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeSavedCart table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCart_Insert
(

	@SavedCartID int    OUTPUT,

	@CookieMappingID int   ,

	@CreatedDate datetime   ,

	@LastUpdatedDate datetime   
)
AS


				
				INSERT INTO [dbo].[ZNodeSavedCart]
					(
					[CookieMappingID]
					,[CreatedDate]
					,[LastUpdatedDate]
					)
				VALUES
					(
					@CookieMappingID
					,@CreatedDate
					,@LastUpdatedDate
					)
				
				-- Get the identity value
				SET @SavedCartID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCart_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeSavedCart table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCart_Update
(

	@SavedCartID int   ,

	@CookieMappingID int   ,

	@CreatedDate datetime   ,

	@LastUpdatedDate datetime   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeSavedCart]
				SET
					[CookieMappingID] = @CookieMappingID
					,[CreatedDate] = @CreatedDate
					,[LastUpdatedDate] = @LastUpdatedDate
				WHERE
[SavedCartID] = @SavedCartID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCart_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeSavedCart table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCart_Delete
(

	@SavedCartID int   
)
AS


				DELETE FROM [dbo].[ZNodeSavedCart] WITH (ROWLOCK) 
				WHERE
					[SavedCartID] = @SavedCartID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCart_GetByCookieMappingID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeSavedCart table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCart_GetByCookieMappingID
(

	@CookieMappingID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[SavedCartID],
					[CookieMappingID],
					[CreatedDate],
					[LastUpdatedDate]
				FROM
					[dbo].[ZNodeSavedCart]
				WHERE
					[CookieMappingID] = @CookieMappingID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCart_GetBySavedCartID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeSavedCart table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCart_GetBySavedCartID
(

	@SavedCartID int   
)
AS


				SELECT
					[SavedCartID],
					[CookieMappingID],
					[CreatedDate],
					[LastUpdatedDate]
				FROM
					[dbo].[ZNodeSavedCart]
				WHERE
					[SavedCartID] = @SavedCartID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCart_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeSavedCart table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCart_Find
(

	@SearchUsingOR bit   = null ,

	@SavedCartID int   = null ,

	@CookieMappingID int   = null ,

	@CreatedDate datetime   = null ,

	@LastUpdatedDate datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SavedCartID]
	, [CookieMappingID]
	, [CreatedDate]
	, [LastUpdatedDate]
    FROM
	[dbo].[ZNodeSavedCart]
    WHERE 
	 ([SavedCartID] = @SavedCartID OR @SavedCartID IS NULL)
	AND ([CookieMappingID] = @CookieMappingID OR @CookieMappingID IS NULL)
	AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
	AND ([LastUpdatedDate] = @LastUpdatedDate OR @LastUpdatedDate IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SavedCartID]
	, [CookieMappingID]
	, [CreatedDate]
	, [LastUpdatedDate]
    FROM
	[dbo].[ZNodeSavedCart]
    WHERE 
	 ([SavedCartID] = @SavedCartID AND @SavedCartID is not null)
	OR ([CookieMappingID] = @CookieMappingID AND @CookieMappingID is not null)
	OR ([CreatedDate] = @CreatedDate AND @CreatedDate is not null)
	OR ([LastUpdatedDate] = @LastUpdatedDate AND @LastUpdatedDate is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[TagProductView]'
GO
EXEC sp_refreshview N'[dbo].[TagProductView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeMultifront]'
GO
ALTER TABLE [dbo].[ZNodeMultifront] DROP
COLUMN [IPC]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMultifront_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeMultifront table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMultifront_Get_List

AS


				
				SELECT
					[ID],
					[MajorVersion],
					[MinorVersion],
					[Build],
					[LVK]
				FROM
					[dbo].[ZNodeMultifront]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMultifront_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeMultifront table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMultifront_Insert
(

	@ID int   ,

	@MajorVersion int   ,

	@MinorVersion int   ,

	@Build int   ,

	@LVK ntext   
)
AS


				
				INSERT INTO [dbo].[ZNodeMultifront]
					(
					[ID]
					,[MajorVersion]
					,[MinorVersion]
					,[Build]
					,[LVK]
					)
				VALUES
					(
					@ID
					,@MajorVersion
					,@MinorVersion
					,@Build
					,@LVK
					)
				
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMultifront_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeMultifront table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMultifront_Update
(

	@ID int   ,

	@OriginalID int   ,

	@MajorVersion int   ,

	@MinorVersion int   ,

	@Build int   ,

	@LVK ntext   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeMultifront]
				SET
					[ID] = @ID
					,[MajorVersion] = @MajorVersion
					,[MinorVersion] = @MinorVersion
					,[Build] = @Build
					,[LVK] = @LVK
				WHERE
[ID] = @OriginalID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMultifront_GetByID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeMultifront table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMultifront_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[MajorVersion],
					[MinorVersion],
					[Build],
					[LVK]
				FROM
					[dbo].[ZNodeMultifront]
				WHERE
					[ID] = @ID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeCookieMapping_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeCookieMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeCookieMapping_Get_List

AS


				
				SELECT
					[CookieMappingID],
					[AccountID],
					[PortalID],
					[CreatedDate],
					[LastUpdatedDate]
				FROM
					[dbo].[ZNodeCookieMapping]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMultifront_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeMultifront table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMultifront_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@MajorVersion int   = null ,

	@MinorVersion int   = null ,

	@Build int   = null ,

	@LVK ntext   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [MajorVersion]
	, [MinorVersion]
	, [Build]
	, [LVK]
    FROM
	[dbo].[ZNodeMultifront]
    WHERE 
	 ([ID] = @ID OR @ID IS NULL)
	AND ([MajorVersion] = @MajorVersion OR @MajorVersion IS NULL)
	AND ([MinorVersion] = @MinorVersion OR @MinorVersion IS NULL)
	AND ([Build] = @Build OR @Build IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [MajorVersion]
	, [MinorVersion]
	, [Build]
	, [LVK]
    FROM
	[dbo].[ZNodeMultifront]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([MajorVersion] = @MajorVersion AND @MajorVersion is not null)
	OR ([MinorVersion] = @MinorVersion AND @MinorVersion is not null)
	OR ([Build] = @Build AND @Build is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_InsertParentChildProduct]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ZNode_InsertParentChildProduct](@ParentProductID INT,@ChildProductID INT)        
AS        
BEGIN        
IF NOT EXISTS (SELECT ParentProductID FROM ZNodeParentChildProduct WHERE  ParentProductID=@ParentProductID AND ChildProductID=@ChildProductID)        
	BEGIN        
		INSERT INTO ZNodeParentChildProduct(ParentProductID,ChildProductID) VALUES (@ParentProductID,@ChildProductID)        
	END;        
END; 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeCookieMapping_Insert]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeCookieMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeCookieMapping_Insert
(

	@CookieMappingID int    OUTPUT,

	@AccountID int   ,

	@PortalID int   ,

	@CreatedDate datetime   ,

	@LastUpdatedDate datetime   
)
AS


				
				INSERT INTO [dbo].[ZNodeCookieMapping]
					(
					[AccountID]
					,[PortalID]
					,[CreatedDate]
					,[LastUpdatedDate]
					)
				VALUES
					(
					@AccountID
					,@PortalID
					,@CreatedDate
					,@LastUpdatedDate
					)
				
				-- Get the identity value
				SET @CookieMappingID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeCookieMapping_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeCookieMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeCookieMapping_Update
(

	@CookieMappingID int   ,

	@AccountID int   ,

	@PortalID int   ,

	@CreatedDate datetime   ,

	@LastUpdatedDate datetime   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeCookieMapping]
				SET
					[AccountID] = @AccountID
					,[PortalID] = @PortalID
					,[CreatedDate] = @CreatedDate
					,[LastUpdatedDate] = @LastUpdatedDate
				WHERE
[CookieMappingID] = @CookieMappingID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeCookieMapping_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeCookieMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeCookieMapping_Delete
(

	@CookieMappingID int   
)
AS


				DELETE FROM [dbo].[ZNodeCookieMapping] WITH (ROWLOCK) 
				WHERE
					[CookieMappingID] = @CookieMappingID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeCookieMapping_GetByPortalID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeCookieMapping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeCookieMapping_GetByPortalID
(

	@PortalID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[CookieMappingID],
					[AccountID],
					[PortalID],
					[CreatedDate],
					[LastUpdatedDate]
				FROM
					[dbo].[ZNodeCookieMapping]
				WHERE
					[PortalID] = @PortalID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeCookieMapping_GetByAccountID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeCookieMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeCookieMapping_GetByAccountID
(

	@AccountID int   
)
AS


				SELECT
					[CookieMappingID],
					[AccountID],
					[PortalID],
					[CreatedDate],
					[LastUpdatedDate]
				FROM
					[dbo].[ZNodeCookieMapping]
				WHERE
					[AccountID] = @AccountID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeCookieMapping_GetByCookieMappingID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeCookieMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeCookieMapping_GetByCookieMappingID
(

	@CookieMappingID int   
)
AS


				SELECT
					[CookieMappingID],
					[AccountID],
					[PortalID],
					[CreatedDate],
					[LastUpdatedDate]
				FROM
					[dbo].[ZNodeCookieMapping]
				WHERE
					[CookieMappingID] = @CookieMappingID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeCookieMapping_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeCookieMapping table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeCookieMapping_Find
(

	@SearchUsingOR bit   = null ,

	@CookieMappingID int   = null ,

	@AccountID int   = null ,

	@PortalID int   = null ,

	@CreatedDate datetime   = null ,

	@LastUpdatedDate datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CookieMappingID]
	, [AccountID]
	, [PortalID]
	, [CreatedDate]
	, [LastUpdatedDate]
    FROM
	[dbo].[ZNodeCookieMapping]
    WHERE 
	 ([CookieMappingID] = @CookieMappingID OR @CookieMappingID IS NULL)
	AND ([AccountID] = @AccountID OR @AccountID IS NULL)
	AND ([PortalID] = @PortalID OR @PortalID IS NULL)
	AND ([CreatedDate] = @CreatedDate OR @CreatedDate IS NULL)
	AND ([LastUpdatedDate] = @LastUpdatedDate OR @LastUpdatedDate IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CookieMappingID]
	, [AccountID]
	, [PortalID]
	, [CreatedDate]
	, [LastUpdatedDate]
    FROM
	[dbo].[ZNodeCookieMapping]
    WHERE 
	 ([CookieMappingID] = @CookieMappingID AND @CookieMappingID is not null)
	OR ([AccountID] = @AccountID AND @AccountID is not null)
	OR ([PortalID] = @PortalID AND @PortalID is not null)
	OR ([CreatedDate] = @CreatedDate AND @CreatedDate is not null)
	OR ([LastUpdatedDate] = @LastUpdatedDate AND @LastUpdatedDate is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeMessageConfig]'
GO
ALTER TABLE [dbo].[ZNodeMessageConfig] ADD
[MessageTypeID] [int] NOT NULL CONSTRAINT [DF_ZNodeMessageConfig_MessageTypeID] DEFAULT ((1))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeMessageConfig] on [dbo].[ZNodeMessageConfig]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeMessageConfig] ON [dbo].[ZNodeMessageConfig] ([Key], [PortalID], [LocaleID], [MessageTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeMessageConfig table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_Insert
(

	@MessageID int    OUTPUT,

	@Key nvarchar (100)  ,

	@PortalID int   ,

	@LocaleID int   ,

	@Description nvarchar (MAX)  ,

	@Value nvarchar (MAX)  ,

	@MessageTypeID int   
)
AS


				
				INSERT INTO [dbo].[ZNodeMessageConfig]
					(
					[Key]
					,[PortalID]
					,[LocaleID]
					,[Description]
					,[Value]
					,[MessageTypeID]
					)
				VALUES
					(
					@Key
					,@PortalID
					,@LocaleID
					,@Description
					,@Value
					,@MessageTypeID
					)
				
				-- Get the identity value
				SET @MessageID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeMessageConfig table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_Update
(

	@MessageID int   ,

	@Key nvarchar (100)  ,

	@PortalID int   ,

	@LocaleID int   ,

	@Description nvarchar (MAX)  ,

	@Value nvarchar (MAX)  ,

	@MessageTypeID int   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeMessageConfig]
				SET
					[Key] = @Key
					,[PortalID] = @PortalID
					,[LocaleID] = @LocaleID
					,[Description] = @Description
					,[Value] = @Value
					,[MessageTypeID] = @MessageTypeID
				WHERE
[MessageID] = @MessageID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeMessageConfig_GetByMessageTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeMessageConfig table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_GetByMessageTypeID
(

	@MessageTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[MessageID],
					[Key],
					[PortalID],
					[LocaleID],
					[Description],
					[Value],
					[MessageTypeID]
				FROM
					[dbo].[ZNodeMessageConfig]
				WHERE
					[MessageTypeID] = @MessageTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeMessageConfig_GetByKeyPortalIDLocaleIDMessageTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeMessageConfig table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_GetByKeyPortalIDLocaleIDMessageTypeID
(

	@Key nvarchar (100)  ,

	@PortalID int   ,

	@LocaleID int   ,

	@MessageTypeID int   
)
AS


				SELECT
					[MessageID],
					[Key],
					[PortalID],
					[LocaleID],
					[Description],
					[Value],
					[MessageTypeID]
				FROM
					[dbo].[ZNodeMessageConfig]
				WHERE
					[Key] = @Key
					AND [PortalID] = @PortalID
					AND [LocaleID] = @LocaleID
					AND [MessageTypeID] = @MessageTypeID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodePortal]'
GO
ALTER TABLE [dbo].[ZNodePortal] ADD
[PersistentCartEnabled] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_GetByMessageID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeMessageConfig table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_GetByMessageID
(

	@MessageID int   
)
AS


				SELECT
					[MessageID],
					[Key],
					[PortalID],
					[LocaleID],
					[Description],
					[Value],
					[MessageTypeID]
				FROM
					[dbo].[ZNodeMessageConfig]
				WHERE
					[MessageID] = @MessageID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeMessageConfig table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_Find
(

	@SearchUsingOR bit   = null ,

	@MessageID int   = null ,

	@Key nvarchar (100)  = null ,

	@PortalID int   = null ,

	@LocaleID int   = null ,

	@Description nvarchar (MAX)  = null ,

	@Value nvarchar (MAX)  = null ,

	@MessageTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [MessageID]
	, [Key]
	, [PortalID]
	, [LocaleID]
	, [Description]
	, [Value]
	, [MessageTypeID]
    FROM
	[dbo].[ZNodeMessageConfig]
    WHERE 
	 ([MessageID] = @MessageID OR @MessageID IS NULL)
	AND ([Key] = @Key OR @Key IS NULL)
	AND ([PortalID] = @PortalID OR @PortalID IS NULL)
	AND ([LocaleID] = @LocaleID OR @LocaleID IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([Value] = @Value OR @Value IS NULL)
	AND ([MessageTypeID] = @MessageTypeID OR @MessageTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [MessageID]
	, [Key]
	, [PortalID]
	, [LocaleID]
	, [Description]
	, [Value]
	, [MessageTypeID]
    FROM
	[dbo].[ZNodeMessageConfig]
    WHERE 
	 ([MessageID] = @MessageID AND @MessageID is not null)
	OR ([Key] = @Key AND @Key is not null)
	OR ([PortalID] = @PortalID AND @PortalID is not null)
	OR ([LocaleID] = @LocaleID AND @LocaleID is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([Value] = @Value AND @Value is not null)
	OR ([MessageTypeID] = @MessageTypeID AND @MessageTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ProductsView]'
GO
EXEC sp_refreshview N'[dbo].[ProductsView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeAccount]'
GO
ALTER TABLE [dbo].[ZNodeAccount] ADD
[Email] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeAddress]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodeAddress]
(
[AddressID] [int] NOT NULL IDENTITY(1, 1),
[FirstName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ZNodeAddress_FirstName] DEFAULT (''),
[MiddleName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ZNodeAddress_MiddleName] DEFAULT (''),
[LastName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ZNodeAddress_LastName] DEFAULT (''),
[CompanyName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ZNodeAddress_CompanyName] DEFAULT (''),
[Street] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street1] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ZNodeAddress_Street1] DEFAULT (''),
[City] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CountryCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDefaultBilling] [bit] NOT NULL,
[AccountID] [int] NOT NULL,
[IsDefaultShipping] [bit] NOT NULL CONSTRAINT [DF_ZNodeAddress_IsDefaultShipping] DEFAULT ((0)),
[Name] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [ZNodeAddress]
           ([FirstName]
           ,[MiddleName]
           ,[LastName]
           ,[CompanyName]
           ,[Street]
           ,[Street1]
           ,[City]
           ,[StateCode]
           ,[PostalCode]
           ,[CountryCode]
           ,[PhoneNumber]
           ,[IsDefaultBilling]
           ,[AccountID]
           ,[IsDefaultShipping]
           ,[Name])
    
     (select BillingFirstName,
'',
BillingLastName,
BillingCompanyName,
BillingStreet,
BillingStreet1,
BillingCity,
BillingStateCode,
BillingPostalCode,
BillingCountryCode,
BillingPhoneNumber,
1,
AccountID,
0,
'Home Billing'
 from znodeaccount)
GO
      

INSERT INTO [ZNodeAddress]
           ([FirstName]
           ,[MiddleName]
           ,[LastName]
           ,[CompanyName]
           ,[Street]
           ,[Street1]
           ,[City]
           ,[StateCode]
           ,[PostalCode]
           ,[CountryCode]
           ,[PhoneNumber]
           ,[IsDefaultBilling]
           ,[AccountID]
           ,[IsDefaultShipping]
           ,[Name])
    
     (select ShipFirstName,
'',
ShipLastName,
ShipCompanyName,
ShipStreet,
ShipStreet1,
ShipCity,
ShipStateCode,
ShipPostalCode,
ShipCountryCode,
ShipPhoneNumber,
0,
AccountID,
1,
'Home Shipping'
 from znodeaccount)
GO

ALTER TABLE [dbo].[ZNodeAccount] DROP
COLUMN [BillingFirstName],
COLUMN [BillingLastName],
COLUMN [BillingCompanyName],
COLUMN [BillingStreet],
COLUMN [BillingStreet1],
COLUMN [BillingCity],
COLUMN [BillingStateCode],
COLUMN [BillingPostalCode],
COLUMN [BillingCountryCode],
COLUMN [BillingPhoneNumber],
COLUMN [BillingEmailID],
COLUMN [ShipFirstName],
COLUMN [ShipLastName],
COLUMN [ShipCompanyName],
COLUMN [ShipStreet],
COLUMN [ShipStreet1],
COLUMN [ShipCity],
COLUMN [ShipStateCode],
COLUMN [ShipPostalCode],
COLUMN [ShipCountryCode],
COLUMN [ShipEmailID],
COLUMN [ShipPhoneNumber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeAccount table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_Get_List

AS


				
				SELECT
					[AccountID],
					[ParentAccountID],
					[UserID],
					[ExternalAccountNo],
					[CompanyName],
					[AccountTypeID],
					[ProfileID],
					[AccountProfileCode],
					[SubAccountLimit],
					[Description],
					[CreateUser],
					[CreateDte],
					[UpdateUser],
					[UpdateDte],
					[ActiveInd],
					[Website],
					[Source],
					[ReferralAccountID],
					[ReferralStatus],
					[Custom1],
					[Custom2],
					[Custom3],
					[EmailOptIn],
					[WebServiceDownloadDte],
					[ReferralCommission],
					[ReferralCommissionTypeID],
					[TaxID],
					[Email]
				FROM
					[dbo].[ZNodeAccount]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeOrderLineItem]'
GO
ALTER TABLE [dbo].[ZNodeOrderLineItem] ADD
[OrderLineItemRelationshipTypeID] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeAccount table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_Insert
(

	@AccountID int    OUTPUT,

	@ParentAccountID int   ,

	@UserID uniqueidentifier   ,

	@ExternalAccountNo nvarchar (MAX)  ,

	@CompanyName nvarchar (255)  ,

	@AccountTypeID int   ,

	@ProfileID int   ,

	@AccountProfileCode nvarchar (MAX)  ,

	@SubAccountLimit int   ,

	@Description ntext   ,

	@CreateUser nvarchar (MAX)  ,

	@CreateDte datetime   ,

	@UpdateUser nvarchar (MAX)  ,

	@UpdateDte datetime   ,

	@ActiveInd bit   ,

	@Website nvarchar (MAX)  ,

	@Source nvarchar (MAX)  ,

	@ReferralAccountID int   ,

	@ReferralStatus nvarchar (1)  ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@EmailOptIn bit   ,

	@WebServiceDownloadDte datetime   ,

	@ReferralCommission money   ,

	@ReferralCommissionTypeID int   ,

	@TaxID nvarchar (MAX)  ,

	@Email nvarchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[ZNodeAccount]
					(
					[ParentAccountID]
					,[UserID]
					,[ExternalAccountNo]
					,[CompanyName]
					,[AccountTypeID]
					,[ProfileID]
					,[AccountProfileCode]
					,[SubAccountLimit]
					,[Description]
					,[CreateUser]
					,[CreateDte]
					,[UpdateUser]
					,[UpdateDte]
					,[ActiveInd]
					,[Website]
					,[Source]
					,[ReferralAccountID]
					,[ReferralStatus]
					,[Custom1]
					,[Custom2]
					,[Custom3]
					,[EmailOptIn]
					,[WebServiceDownloadDte]
					,[ReferralCommission]
					,[ReferralCommissionTypeID]
					,[TaxID]
					,[Email]
					)
				VALUES
					(
					@ParentAccountID
					,@UserID
					,@ExternalAccountNo
					,@CompanyName
					,@AccountTypeID
					,@ProfileID
					,@AccountProfileCode
					,@SubAccountLimit
					,@Description
					,@CreateUser
					,@CreateDte
					,@UpdateUser
					,@UpdateDte
					,@ActiveInd
					,@Website
					,@Source
					,@ReferralAccountID
					,@ReferralStatus
					,@Custom1
					,@Custom2
					,@Custom3
					,@EmailOptIn
					,@WebServiceDownloadDte
					,@ReferralCommission
					,@ReferralCommissionTypeID
					,@TaxID
					,@Email
					)
				
				-- Get the identity value
				SET @AccountID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeAccount table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_Update
(

	@AccountID int   ,

	@ParentAccountID int   ,

	@UserID uniqueidentifier   ,

	@ExternalAccountNo nvarchar (MAX)  ,

	@CompanyName nvarchar (255)  ,

	@AccountTypeID int   ,

	@ProfileID int   ,

	@AccountProfileCode nvarchar (MAX)  ,

	@SubAccountLimit int   ,

	@Description ntext   ,

	@CreateUser nvarchar (MAX)  ,

	@CreateDte datetime   ,

	@UpdateUser nvarchar (MAX)  ,

	@UpdateDte datetime   ,

	@ActiveInd bit   ,

	@Website nvarchar (MAX)  ,

	@Source nvarchar (MAX)  ,

	@ReferralAccountID int   ,

	@ReferralStatus nvarchar (1)  ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@EmailOptIn bit   ,

	@WebServiceDownloadDte datetime   ,

	@ReferralCommission money   ,

	@ReferralCommissionTypeID int   ,

	@TaxID nvarchar (MAX)  ,

	@Email nvarchar (MAX)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeAccount]
				SET
					[ParentAccountID] = @ParentAccountID
					,[UserID] = @UserID
					,[ExternalAccountNo] = @ExternalAccountNo
					,[CompanyName] = @CompanyName
					,[AccountTypeID] = @AccountTypeID
					,[ProfileID] = @ProfileID
					,[AccountProfileCode] = @AccountProfileCode
					,[SubAccountLimit] = @SubAccountLimit
					,[Description] = @Description
					,[CreateUser] = @CreateUser
					,[CreateDte] = @CreateDte
					,[UpdateUser] = @UpdateUser
					,[UpdateDte] = @UpdateDte
					,[ActiveInd] = @ActiveInd
					,[Website] = @Website
					,[Source] = @Source
					,[ReferralAccountID] = @ReferralAccountID
					,[ReferralStatus] = @ReferralStatus
					,[Custom1] = @Custom1
					,[Custom2] = @Custom2
					,[Custom3] = @Custom3
					,[EmailOptIn] = @EmailOptIn
					,[WebServiceDownloadDte] = @WebServiceDownloadDte
					,[ReferralCommission] = @ReferralCommission
					,[ReferralCommissionTypeID] = @ReferralCommissionTypeID
					,[TaxID] = @TaxID
					,[Email] = @Email
				WHERE
[AccountID] = @AccountID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_GetByParentAccountID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAccount table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_GetByParentAccountID
(

	@ParentAccountID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AccountID],
					[ParentAccountID],
					[UserID],
					[ExternalAccountNo],
					[CompanyName],
					[AccountTypeID],
					[ProfileID],
					[AccountProfileCode],
					[SubAccountLimit],
					[Description],
					[CreateUser],
					[CreateDte],
					[UpdateUser],
					[UpdateDte],
					[ActiveInd],
					[Website],
					[Source],
					[ReferralAccountID],
					[ReferralStatus],
					[Custom1],
					[Custom2],
					[Custom3],
					[EmailOptIn],
					[WebServiceDownloadDte],
					[ReferralCommission],
					[ReferralCommissionTypeID],
					[TaxID],
					[Email]
				FROM
					[dbo].[ZNodeAccount]
				WHERE
					[ParentAccountID] = @ParentAccountID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_GetByAccountTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAccount table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_GetByAccountTypeID
(

	@AccountTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AccountID],
					[ParentAccountID],
					[UserID],
					[ExternalAccountNo],
					[CompanyName],
					[AccountTypeID],
					[ProfileID],
					[AccountProfileCode],
					[SubAccountLimit],
					[Description],
					[CreateUser],
					[CreateDte],
					[UpdateUser],
					[UpdateDte],
					[ActiveInd],
					[Website],
					[Source],
					[ReferralAccountID],
					[ReferralStatus],
					[Custom1],
					[Custom2],
					[Custom3],
					[EmailOptIn],
					[WebServiceDownloadDte],
					[ReferralCommission],
					[ReferralCommissionTypeID],
					[TaxID],
					[Email]
				FROM
					[dbo].[ZNodeAccount]
				WHERE
					[AccountTypeID] = @AccountTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_WS_GetProductReviews]'
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[ZNode_WS_GetProductReviews]
@PortalID INT,
@ProductID INT
AS
BEGIN
    SET NOCOUNT ON;
    IF(@ProductID=0)
		BEGIN
			SELECT P.ProductID
				,P.Name
				,ReviewRating=(SELECT ISNULL(AVG(Rating),0) FROM ZNodeReview Rv WHERE Rv.ProductID=P.ProductID  AND Rv.[Status]='A')     
				,R.[Subject]
				,R.Comments
				,R.CreateUser
				,R.UserLocation
			  FROM ZNodeReview R 
			  INNER JOIN ZNodeProduct P
			  ON R.ProductID=P.ProductID 
			  INNER JOIN ZNodePortal Portal
			  ON  Portal.PortalID=@PortalID
			  AND  R.[Status]='A'
		END
	ELSE
		BEGIN
		SELECT P.ProductID
				,P.Name
				,ReviewRating=(SELECT ISNULL(AVG(Rating),0) FROM ZNodeReview Rv WHERE Rv.ProductID=P.ProductID  AND Rv.[Status]='A')     
				,R.[Subject]
				,R.Comments
				,R.CreateUser
				,R.UserLocation
			  FROM ZnodeReview R 
			  INNER JOIN ZNodeProduct P
			  ON R.ProductID=P.ProductID 
			  INNER JOIN ZNodePortal Portal
			  ON  Portal.PortalID=@PortalID
			  AND P.ProductID=@ProductID
			  AND  R.[Status]='A'
		END
	
END  
 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_GetByReferralAccountID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAccount table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_GetByReferralAccountID
(

	@ReferralAccountID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AccountID],
					[ParentAccountID],
					[UserID],
					[ExternalAccountNo],
					[CompanyName],
					[AccountTypeID],
					[ProfileID],
					[AccountProfileCode],
					[SubAccountLimit],
					[Description],
					[CreateUser],
					[CreateDte],
					[UpdateUser],
					[UpdateDte],
					[ActiveInd],
					[Website],
					[Source],
					[ReferralAccountID],
					[ReferralStatus],
					[Custom1],
					[Custom2],
					[Custom3],
					[EmailOptIn],
					[WebServiceDownloadDte],
					[ReferralCommission],
					[ReferralCommissionTypeID],
					[TaxID],
					[Email]
				FROM
					[dbo].[ZNodeAccount]
				WHERE
					[ReferralAccountID] = @ReferralAccountID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_GetByReferralCommissionTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAccount table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_GetByReferralCommissionTypeID
(

	@ReferralCommissionTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AccountID],
					[ParentAccountID],
					[UserID],
					[ExternalAccountNo],
					[CompanyName],
					[AccountTypeID],
					[ProfileID],
					[AccountProfileCode],
					[SubAccountLimit],
					[Description],
					[CreateUser],
					[CreateDte],
					[UpdateUser],
					[UpdateDte],
					[ActiveInd],
					[Website],
					[Source],
					[ReferralAccountID],
					[ReferralStatus],
					[Custom1],
					[Custom2],
					[Custom3],
					[EmailOptIn],
					[WebServiceDownloadDte],
					[ReferralCommission],
					[ReferralCommissionTypeID],
					[TaxID],
					[Email]
				FROM
					[dbo].[ZNodeAccount]
				WHERE
					[ReferralCommissionTypeID] = @ReferralCommissionTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_GetByUserID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAccount table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_GetByUserID
(

	@UserID uniqueidentifier   
)
AS


				SELECT
					[AccountID],
					[ParentAccountID],
					[UserID],
					[ExternalAccountNo],
					[CompanyName],
					[AccountTypeID],
					[ProfileID],
					[AccountProfileCode],
					[SubAccountLimit],
					[Description],
					[CreateUser],
					[CreateDte],
					[UpdateUser],
					[UpdateDte],
					[ActiveInd],
					[Website],
					[Source],
					[ReferralAccountID],
					[ReferralStatus],
					[Custom1],
					[Custom2],
					[Custom3],
					[EmailOptIn],
					[WebServiceDownloadDte],
					[ReferralCommission],
					[ReferralCommissionTypeID],
					[TaxID],
					[Email]
				FROM
					[dbo].[ZNodeAccount]
				WHERE
					[UserID] = @UserID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_GetByCompanyName]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAccount table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_GetByCompanyName
(

	@CompanyName nvarchar (255)  
)
AS


				SELECT
					[AccountID],
					[ParentAccountID],
					[UserID],
					[ExternalAccountNo],
					[CompanyName],
					[AccountTypeID],
					[ProfileID],
					[AccountProfileCode],
					[SubAccountLimit],
					[Description],
					[CreateUser],
					[CreateDte],
					[UpdateUser],
					[UpdateDte],
					[ActiveInd],
					[Website],
					[Source],
					[ReferralAccountID],
					[ReferralStatus],
					[Custom1],
					[Custom2],
					[Custom3],
					[EmailOptIn],
					[WebServiceDownloadDte],
					[ReferralCommission],
					[ReferralCommissionTypeID],
					[TaxID],
					[Email]
				FROM
					[dbo].[ZNodeAccount]
				WHERE
					[CompanyName] = @CompanyName
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_GetByAccountID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAccount table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_GetByAccountID
(

	@AccountID int   
)
AS


				SELECT
					[AccountID],
					[ParentAccountID],
					[UserID],
					[ExternalAccountNo],
					[CompanyName],
					[AccountTypeID],
					[ProfileID],
					[AccountProfileCode],
					[SubAccountLimit],
					[Description],
					[CreateUser],
					[CreateDte],
					[UpdateUser],
					[UpdateDte],
					[ActiveInd],
					[Website],
					[Source],
					[ReferralAccountID],
					[ReferralStatus],
					[Custom1],
					[Custom2],
					[Custom3],
					[EmailOptIn],
					[WebServiceDownloadDte],
					[ReferralCommission],
					[ReferralCommissionTypeID],
					[TaxID],
					[Email]
				FROM
					[dbo].[ZNodeAccount]
				WHERE
					[AccountID] = @AccountID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeAccount table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_Find
(

	@SearchUsingOR bit   = null ,

	@AccountID int   = null ,

	@ParentAccountID int   = null ,

	@UserID uniqueidentifier   = null ,

	@ExternalAccountNo nvarchar (MAX)  = null ,

	@CompanyName nvarchar (255)  = null ,

	@AccountTypeID int   = null ,

	@ProfileID int   = null ,

	@AccountProfileCode nvarchar (MAX)  = null ,

	@SubAccountLimit int   = null ,

	@Description ntext   = null ,

	@CreateUser nvarchar (MAX)  = null ,

	@CreateDte datetime   = null ,

	@UpdateUser nvarchar (MAX)  = null ,

	@UpdateDte datetime   = null ,

	@ActiveInd bit   = null ,

	@Website nvarchar (MAX)  = null ,

	@Source nvarchar (MAX)  = null ,

	@ReferralAccountID int   = null ,

	@ReferralStatus nvarchar (1)  = null ,

	@Custom1 nvarchar (MAX)  = null ,

	@Custom2 nvarchar (MAX)  = null ,

	@Custom3 nvarchar (MAX)  = null ,

	@EmailOptIn bit   = null ,

	@WebServiceDownloadDte datetime   = null ,

	@ReferralCommission money   = null ,

	@ReferralCommissionTypeID int   = null ,

	@TaxID nvarchar (MAX)  = null ,

	@Email nvarchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AccountID]
	, [ParentAccountID]
	, [UserID]
	, [ExternalAccountNo]
	, [CompanyName]
	, [AccountTypeID]
	, [ProfileID]
	, [AccountProfileCode]
	, [SubAccountLimit]
	, [Description]
	, [CreateUser]
	, [CreateDte]
	, [UpdateUser]
	, [UpdateDte]
	, [ActiveInd]
	, [Website]
	, [Source]
	, [ReferralAccountID]
	, [ReferralStatus]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [EmailOptIn]
	, [WebServiceDownloadDte]
	, [ReferralCommission]
	, [ReferralCommissionTypeID]
	, [TaxID]
	, [Email]
    FROM
	[dbo].[ZNodeAccount]
    WHERE 
	 ([AccountID] = @AccountID OR @AccountID IS NULL)
	AND ([ParentAccountID] = @ParentAccountID OR @ParentAccountID IS NULL)
	AND ([UserID] = @UserID OR @UserID IS NULL)
	AND ([ExternalAccountNo] = @ExternalAccountNo OR @ExternalAccountNo IS NULL)
	AND ([CompanyName] = @CompanyName OR @CompanyName IS NULL)
	AND ([AccountTypeID] = @AccountTypeID OR @AccountTypeID IS NULL)
	AND ([ProfileID] = @ProfileID OR @ProfileID IS NULL)
	AND ([AccountProfileCode] = @AccountProfileCode OR @AccountProfileCode IS NULL)
	AND ([SubAccountLimit] = @SubAccountLimit OR @SubAccountLimit IS NULL)
	AND ([CreateUser] = @CreateUser OR @CreateUser IS NULL)
	AND ([CreateDte] = @CreateDte OR @CreateDte IS NULL)
	AND ([UpdateUser] = @UpdateUser OR @UpdateUser IS NULL)
	AND ([UpdateDte] = @UpdateDte OR @UpdateDte IS NULL)
	AND ([ActiveInd] = @ActiveInd OR @ActiveInd IS NULL)
	AND ([Website] = @Website OR @Website IS NULL)
	AND ([Source] = @Source OR @Source IS NULL)
	AND ([ReferralAccountID] = @ReferralAccountID OR @ReferralAccountID IS NULL)
	AND ([ReferralStatus] = @ReferralStatus OR @ReferralStatus IS NULL)
	AND ([Custom1] = @Custom1 OR @Custom1 IS NULL)
	AND ([Custom2] = @Custom2 OR @Custom2 IS NULL)
	AND ([Custom3] = @Custom3 OR @Custom3 IS NULL)
	AND ([EmailOptIn] = @EmailOptIn OR @EmailOptIn IS NULL)
	AND ([WebServiceDownloadDte] = @WebServiceDownloadDte OR @WebServiceDownloadDte IS NULL)
	AND ([ReferralCommission] = @ReferralCommission OR @ReferralCommission IS NULL)
	AND ([ReferralCommissionTypeID] = @ReferralCommissionTypeID OR @ReferralCommissionTypeID IS NULL)
	AND ([TaxID] = @TaxID OR @TaxID IS NULL)
	AND ([Email] = @Email OR @Email IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AccountID]
	, [ParentAccountID]
	, [UserID]
	, [ExternalAccountNo]
	, [CompanyName]
	, [AccountTypeID]
	, [ProfileID]
	, [AccountProfileCode]
	, [SubAccountLimit]
	, [Description]
	, [CreateUser]
	, [CreateDte]
	, [UpdateUser]
	, [UpdateDte]
	, [ActiveInd]
	, [Website]
	, [Source]
	, [ReferralAccountID]
	, [ReferralStatus]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [EmailOptIn]
	, [WebServiceDownloadDte]
	, [ReferralCommission]
	, [ReferralCommissionTypeID]
	, [TaxID]
	, [Email]
    FROM
	[dbo].[ZNodeAccount]
    WHERE 
	 ([AccountID] = @AccountID AND @AccountID is not null)
	OR ([ParentAccountID] = @ParentAccountID AND @ParentAccountID is not null)
	OR ([UserID] = @UserID AND @UserID is not null)
	OR ([ExternalAccountNo] = @ExternalAccountNo AND @ExternalAccountNo is not null)
	OR ([CompanyName] = @CompanyName AND @CompanyName is not null)
	OR ([AccountTypeID] = @AccountTypeID AND @AccountTypeID is not null)
	OR ([ProfileID] = @ProfileID AND @ProfileID is not null)
	OR ([AccountProfileCode] = @AccountProfileCode AND @AccountProfileCode is not null)
	OR ([SubAccountLimit] = @SubAccountLimit AND @SubAccountLimit is not null)
	OR ([CreateUser] = @CreateUser AND @CreateUser is not null)
	OR ([CreateDte] = @CreateDte AND @CreateDte is not null)
	OR ([UpdateUser] = @UpdateUser AND @UpdateUser is not null)
	OR ([UpdateDte] = @UpdateDte AND @UpdateDte is not null)
	OR ([ActiveInd] = @ActiveInd AND @ActiveInd is not null)
	OR ([Website] = @Website AND @Website is not null)
	OR ([Source] = @Source AND @Source is not null)
	OR ([ReferralAccountID] = @ReferralAccountID AND @ReferralAccountID is not null)
	OR ([ReferralStatus] = @ReferralStatus AND @ReferralStatus is not null)
	OR ([Custom1] = @Custom1 AND @Custom1 is not null)
	OR ([Custom2] = @Custom2 AND @Custom2 is not null)
	OR ([Custom3] = @Custom3 AND @Custom3 is not null)
	OR ([EmailOptIn] = @EmailOptIn AND @EmailOptIn is not null)
	OR ([WebServiceDownloadDte] = @WebServiceDownloadDte AND @WebServiceDownloadDte is not null)
	OR ([ReferralCommission] = @ReferralCommission AND @ReferralCommission is not null)
	OR ([ReferralCommissionTypeID] = @ReferralCommissionTypeID AND @ReferralCommissionTypeID is not null)
	OR ([TaxID] = @TaxID AND @TaxID is not null)
	OR ([Email] = @Email AND @Email is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodePortal table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Get_List

AS


				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled]
				FROM
					[dbo].[ZNodePortal]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodePortal table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Insert
(

	@PortalID int    OUTPUT,

	@CompanyName nvarchar (MAX)  ,

	@StoreName nvarchar (MAX)  ,

	@LogoPath nvarchar (MAX)  ,

	@UseSSL bit   ,

	@AdminEmail nvarchar (MAX)  ,

	@SalesEmail nvarchar (MAX)  ,

	@CustomerServiceEmail nvarchar (MAX)  ,

	@SalesPhoneNumber nvarchar (MAX)  ,

	@CustomerServicePhoneNumber nvarchar (MAX)  ,

	@ImageNotAvailablePath nvarchar (MAX)  ,

	@MaxCatalogDisplayColumns tinyint   ,

	@MaxCatalogDisplayItems int   ,

	@MaxCatalogCategoryDisplayThumbnails int   ,

	@MaxCatalogItemSmallThumbnailWidth int   ,

	@MaxCatalogItemSmallWidth int   ,

	@MaxCatalogItemMediumWidth int   ,

	@MaxCatalogItemThumbnailWidth int   ,

	@MaxCatalogItemLargeWidth int   ,

	@MaxCatalogItemCrossSellWidth int   ,

	@ShowSwatchInCategory bit   ,

	@ShowAlternateImageInCategory bit   ,

	@ActiveInd bit   ,

	@SMTPServer nvarchar (MAX)  ,

	@SMTPUserName nvarchar (MAX)  ,

	@SMTPPassword nvarchar (MAX)  ,

	@SMTPPort int   ,

	@SiteWideBottomJavascript ntext   ,

	@SiteWideTopJavascript ntext   ,

	@OrderReceiptAffiliateJavascript ntext   ,

	@SiteWideAnalyticsJavascript ntext   ,

	@GoogleAnalyticsCode ntext   ,

	@UPSUserName nvarchar (MAX)  ,

	@UPSPassword nvarchar (MAX)  ,

	@UPSKey nvarchar (MAX)  ,

	@ShippingOriginZipCode nvarchar (50)  ,

	@MasterPage nvarchar (MAX)  ,

	@ShopByPriceMin int   ,

	@ShopByPriceMax int   ,

	@ShopByPriceIncrement int   ,

	@FedExAccountNumber nvarchar (MAX)  ,

	@FedExMeterNumber nvarchar (MAX)  ,

	@FedExProductionKey nvarchar (MAX)  ,

	@FedExSecurityCode nvarchar (MAX)  ,

	@FedExCSPKey nvarchar (MAX)  ,

	@FedExCSPPassword nvarchar (MAX)  ,

	@FedExClientProductId nvarchar (MAX)  ,

	@FedExClientProductVersion nvarchar (MAX)  ,

	@FedExDropoffType nvarchar (MAX)  ,

	@FedExPackagingType nvarchar (MAX)  ,

	@FedExUseDiscountRate bit   ,

	@FedExAddInsurance bit   ,

	@ShippingOriginAddress1 nvarchar (MAX)  ,

	@ShippingOriginAddress2 nvarchar (MAX)  ,

	@ShippingOriginCity nvarchar (MAX)  ,

	@ShippingOriginStateCode nvarchar (MAX)  ,

	@ShippingOriginCountryCode nvarchar (MAX)  ,

	@ShippingOriginPhone nvarchar (MAX)  ,

	@CurrencyTypeID int   ,

	@WeightUnit nvarchar (MAX)  ,

	@DimensionUnit nvarchar (MAX)  ,

	@EmailListLogin nvarchar (100)  ,

	@EmailListPassword nvarchar (100)  ,

	@EmailListDefaultList nvarchar (MAX)  ,

	@ShippingTaxable bit   ,

	@DefaultOrderStateID int   ,

	@DefaultReviewStatus nvarchar (1)  ,

	@DefaultAnonymousProfileID int   ,

	@DefaultRegisteredProfileID int   ,

	@InclusiveTax bit   ,

	@SeoDefaultProductTitle nvarchar (MAX)  ,

	@SeoDefaultProductDescription nvarchar (MAX)  ,

	@SeoDefaultProductKeyword nvarchar (MAX)  ,

	@SeoDefaultCategoryTitle nvarchar (MAX)  ,

	@SeoDefaultCategoryDescription nvarchar (MAX)  ,

	@SeoDefaultCategoryKeyword nvarchar (MAX)  ,

	@SeoDefaultContentTitle nvarchar (MAX)  ,

	@SeoDefaultContentDescription nvarchar (MAX)  ,

	@SeoDefaultContentKeyword nvarchar (MAX)  ,

	@TimeZoneOffset nvarchar (50)  ,

	@LocaleID int   ,

	@SplashCategoryID int   ,

	@SplashImageFile nvarchar (MAX)  ,

	@MobileTheme nvarchar (MAX)  ,

	@PersistentCartEnabled bit   
)
AS


				
				INSERT INTO [dbo].[ZNodePortal]
					(
					[CompanyName]
					,[StoreName]
					,[LogoPath]
					,[UseSSL]
					,[AdminEmail]
					,[SalesEmail]
					,[CustomerServiceEmail]
					,[SalesPhoneNumber]
					,[CustomerServicePhoneNumber]
					,[ImageNotAvailablePath]
					,[MaxCatalogDisplayColumns]
					,[MaxCatalogDisplayItems]
					,[MaxCatalogCategoryDisplayThumbnails]
					,[MaxCatalogItemSmallThumbnailWidth]
					,[MaxCatalogItemSmallWidth]
					,[MaxCatalogItemMediumWidth]
					,[MaxCatalogItemThumbnailWidth]
					,[MaxCatalogItemLargeWidth]
					,[MaxCatalogItemCrossSellWidth]
					,[ShowSwatchInCategory]
					,[ShowAlternateImageInCategory]
					,[ActiveInd]
					,[SMTPServer]
					,[SMTPUserName]
					,[SMTPPassword]
					,[SMTPPort]
					,[SiteWideBottomJavascript]
					,[SiteWideTopJavascript]
					,[OrderReceiptAffiliateJavascript]
					,[SiteWideAnalyticsJavascript]
					,[GoogleAnalyticsCode]
					,[UPSUserName]
					,[UPSPassword]
					,[UPSKey]
					,[ShippingOriginZipCode]
					,[MasterPage]
					,[ShopByPriceMin]
					,[ShopByPriceMax]
					,[ShopByPriceIncrement]
					,[FedExAccountNumber]
					,[FedExMeterNumber]
					,[FedExProductionKey]
					,[FedExSecurityCode]
					,[FedExCSPKey]
					,[FedExCSPPassword]
					,[FedExClientProductId]
					,[FedExClientProductVersion]
					,[FedExDropoffType]
					,[FedExPackagingType]
					,[FedExUseDiscountRate]
					,[FedExAddInsurance]
					,[ShippingOriginAddress1]
					,[ShippingOriginAddress2]
					,[ShippingOriginCity]
					,[ShippingOriginStateCode]
					,[ShippingOriginCountryCode]
					,[ShippingOriginPhone]
					,[CurrencyTypeID]
					,[WeightUnit]
					,[DimensionUnit]
					,[EmailListLogin]
					,[EmailListPassword]
					,[EmailListDefaultList]
					,[ShippingTaxable]
					,[DefaultOrderStateID]
					,[DefaultReviewStatus]
					,[DefaultAnonymousProfileID]
					,[DefaultRegisteredProfileID]
					,[InclusiveTax]
					,[SeoDefaultProductTitle]
					,[SeoDefaultProductDescription]
					,[SeoDefaultProductKeyword]
					,[SeoDefaultCategoryTitle]
					,[SeoDefaultCategoryDescription]
					,[SeoDefaultCategoryKeyword]
					,[SeoDefaultContentTitle]
					,[SeoDefaultContentDescription]
					,[SeoDefaultContentKeyword]
					,[TimeZoneOffset]
					,[LocaleID]
					,[SplashCategoryID]
					,[SplashImageFile]
					,[MobileTheme]
					,[PersistentCartEnabled]
					)
				VALUES
					(
					@CompanyName
					,@StoreName
					,@LogoPath
					,@UseSSL
					,@AdminEmail
					,@SalesEmail
					,@CustomerServiceEmail
					,@SalesPhoneNumber
					,@CustomerServicePhoneNumber
					,@ImageNotAvailablePath
					,@MaxCatalogDisplayColumns
					,@MaxCatalogDisplayItems
					,@MaxCatalogCategoryDisplayThumbnails
					,@MaxCatalogItemSmallThumbnailWidth
					,@MaxCatalogItemSmallWidth
					,@MaxCatalogItemMediumWidth
					,@MaxCatalogItemThumbnailWidth
					,@MaxCatalogItemLargeWidth
					,@MaxCatalogItemCrossSellWidth
					,@ShowSwatchInCategory
					,@ShowAlternateImageInCategory
					,@ActiveInd
					,@SMTPServer
					,@SMTPUserName
					,@SMTPPassword
					,@SMTPPort
					,@SiteWideBottomJavascript
					,@SiteWideTopJavascript
					,@OrderReceiptAffiliateJavascript
					,@SiteWideAnalyticsJavascript
					,@GoogleAnalyticsCode
					,@UPSUserName
					,@UPSPassword
					,@UPSKey
					,@ShippingOriginZipCode
					,@MasterPage
					,@ShopByPriceMin
					,@ShopByPriceMax
					,@ShopByPriceIncrement
					,@FedExAccountNumber
					,@FedExMeterNumber
					,@FedExProductionKey
					,@FedExSecurityCode
					,@FedExCSPKey
					,@FedExCSPPassword
					,@FedExClientProductId
					,@FedExClientProductVersion
					,@FedExDropoffType
					,@FedExPackagingType
					,@FedExUseDiscountRate
					,@FedExAddInsurance
					,@ShippingOriginAddress1
					,@ShippingOriginAddress2
					,@ShippingOriginCity
					,@ShippingOriginStateCode
					,@ShippingOriginCountryCode
					,@ShippingOriginPhone
					,@CurrencyTypeID
					,@WeightUnit
					,@DimensionUnit
					,@EmailListLogin
					,@EmailListPassword
					,@EmailListDefaultList
					,@ShippingTaxable
					,@DefaultOrderStateID
					,@DefaultReviewStatus
					,@DefaultAnonymousProfileID
					,@DefaultRegisteredProfileID
					,@InclusiveTax
					,@SeoDefaultProductTitle
					,@SeoDefaultProductDescription
					,@SeoDefaultProductKeyword
					,@SeoDefaultCategoryTitle
					,@SeoDefaultCategoryDescription
					,@SeoDefaultCategoryKeyword
					,@SeoDefaultContentTitle
					,@SeoDefaultContentDescription
					,@SeoDefaultContentKeyword
					,@TimeZoneOffset
					,@LocaleID
					,@SplashCategoryID
					,@SplashImageFile
					,@MobileTheme
					,@PersistentCartEnabled
					)
				
				-- Get the identity value
				SET @PortalID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodePortal table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Update
(

	@PortalID int   ,

	@CompanyName nvarchar (MAX)  ,

	@StoreName nvarchar (MAX)  ,

	@LogoPath nvarchar (MAX)  ,

	@UseSSL bit   ,

	@AdminEmail nvarchar (MAX)  ,

	@SalesEmail nvarchar (MAX)  ,

	@CustomerServiceEmail nvarchar (MAX)  ,

	@SalesPhoneNumber nvarchar (MAX)  ,

	@CustomerServicePhoneNumber nvarchar (MAX)  ,

	@ImageNotAvailablePath nvarchar (MAX)  ,

	@MaxCatalogDisplayColumns tinyint   ,

	@MaxCatalogDisplayItems int   ,

	@MaxCatalogCategoryDisplayThumbnails int   ,

	@MaxCatalogItemSmallThumbnailWidth int   ,

	@MaxCatalogItemSmallWidth int   ,

	@MaxCatalogItemMediumWidth int   ,

	@MaxCatalogItemThumbnailWidth int   ,

	@MaxCatalogItemLargeWidth int   ,

	@MaxCatalogItemCrossSellWidth int   ,

	@ShowSwatchInCategory bit   ,

	@ShowAlternateImageInCategory bit   ,

	@ActiveInd bit   ,

	@SMTPServer nvarchar (MAX)  ,

	@SMTPUserName nvarchar (MAX)  ,

	@SMTPPassword nvarchar (MAX)  ,

	@SMTPPort int   ,

	@SiteWideBottomJavascript ntext   ,

	@SiteWideTopJavascript ntext   ,

	@OrderReceiptAffiliateJavascript ntext   ,

	@SiteWideAnalyticsJavascript ntext   ,

	@GoogleAnalyticsCode ntext   ,

	@UPSUserName nvarchar (MAX)  ,

	@UPSPassword nvarchar (MAX)  ,

	@UPSKey nvarchar (MAX)  ,

	@ShippingOriginZipCode nvarchar (50)  ,

	@MasterPage nvarchar (MAX)  ,

	@ShopByPriceMin int   ,

	@ShopByPriceMax int   ,

	@ShopByPriceIncrement int   ,

	@FedExAccountNumber nvarchar (MAX)  ,

	@FedExMeterNumber nvarchar (MAX)  ,

	@FedExProductionKey nvarchar (MAX)  ,

	@FedExSecurityCode nvarchar (MAX)  ,

	@FedExCSPKey nvarchar (MAX)  ,

	@FedExCSPPassword nvarchar (MAX)  ,

	@FedExClientProductId nvarchar (MAX)  ,

	@FedExClientProductVersion nvarchar (MAX)  ,

	@FedExDropoffType nvarchar (MAX)  ,

	@FedExPackagingType nvarchar (MAX)  ,

	@FedExUseDiscountRate bit   ,

	@FedExAddInsurance bit   ,

	@ShippingOriginAddress1 nvarchar (MAX)  ,

	@ShippingOriginAddress2 nvarchar (MAX)  ,

	@ShippingOriginCity nvarchar (MAX)  ,

	@ShippingOriginStateCode nvarchar (MAX)  ,

	@ShippingOriginCountryCode nvarchar (MAX)  ,

	@ShippingOriginPhone nvarchar (MAX)  ,

	@CurrencyTypeID int   ,

	@WeightUnit nvarchar (MAX)  ,

	@DimensionUnit nvarchar (MAX)  ,

	@EmailListLogin nvarchar (100)  ,

	@EmailListPassword nvarchar (100)  ,

	@EmailListDefaultList nvarchar (MAX)  ,

	@ShippingTaxable bit   ,

	@DefaultOrderStateID int   ,

	@DefaultReviewStatus nvarchar (1)  ,

	@DefaultAnonymousProfileID int   ,

	@DefaultRegisteredProfileID int   ,

	@InclusiveTax bit   ,

	@SeoDefaultProductTitle nvarchar (MAX)  ,

	@SeoDefaultProductDescription nvarchar (MAX)  ,

	@SeoDefaultProductKeyword nvarchar (MAX)  ,

	@SeoDefaultCategoryTitle nvarchar (MAX)  ,

	@SeoDefaultCategoryDescription nvarchar (MAX)  ,

	@SeoDefaultCategoryKeyword nvarchar (MAX)  ,

	@SeoDefaultContentTitle nvarchar (MAX)  ,

	@SeoDefaultContentDescription nvarchar (MAX)  ,

	@SeoDefaultContentKeyword nvarchar (MAX)  ,

	@TimeZoneOffset nvarchar (50)  ,

	@LocaleID int   ,

	@SplashCategoryID int   ,

	@SplashImageFile nvarchar (MAX)  ,

	@MobileTheme nvarchar (MAX)  ,

	@PersistentCartEnabled bit   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodePortal]
				SET
					[CompanyName] = @CompanyName
					,[StoreName] = @StoreName
					,[LogoPath] = @LogoPath
					,[UseSSL] = @UseSSL
					,[AdminEmail] = @AdminEmail
					,[SalesEmail] = @SalesEmail
					,[CustomerServiceEmail] = @CustomerServiceEmail
					,[SalesPhoneNumber] = @SalesPhoneNumber
					,[CustomerServicePhoneNumber] = @CustomerServicePhoneNumber
					,[ImageNotAvailablePath] = @ImageNotAvailablePath
					,[MaxCatalogDisplayColumns] = @MaxCatalogDisplayColumns
					,[MaxCatalogDisplayItems] = @MaxCatalogDisplayItems
					,[MaxCatalogCategoryDisplayThumbnails] = @MaxCatalogCategoryDisplayThumbnails
					,[MaxCatalogItemSmallThumbnailWidth] = @MaxCatalogItemSmallThumbnailWidth
					,[MaxCatalogItemSmallWidth] = @MaxCatalogItemSmallWidth
					,[MaxCatalogItemMediumWidth] = @MaxCatalogItemMediumWidth
					,[MaxCatalogItemThumbnailWidth] = @MaxCatalogItemThumbnailWidth
					,[MaxCatalogItemLargeWidth] = @MaxCatalogItemLargeWidth
					,[MaxCatalogItemCrossSellWidth] = @MaxCatalogItemCrossSellWidth
					,[ShowSwatchInCategory] = @ShowSwatchInCategory
					,[ShowAlternateImageInCategory] = @ShowAlternateImageInCategory
					,[ActiveInd] = @ActiveInd
					,[SMTPServer] = @SMTPServer
					,[SMTPUserName] = @SMTPUserName
					,[SMTPPassword] = @SMTPPassword
					,[SMTPPort] = @SMTPPort
					,[SiteWideBottomJavascript] = @SiteWideBottomJavascript
					,[SiteWideTopJavascript] = @SiteWideTopJavascript
					,[OrderReceiptAffiliateJavascript] = @OrderReceiptAffiliateJavascript
					,[SiteWideAnalyticsJavascript] = @SiteWideAnalyticsJavascript
					,[GoogleAnalyticsCode] = @GoogleAnalyticsCode
					,[UPSUserName] = @UPSUserName
					,[UPSPassword] = @UPSPassword
					,[UPSKey] = @UPSKey
					,[ShippingOriginZipCode] = @ShippingOriginZipCode
					,[MasterPage] = @MasterPage
					,[ShopByPriceMin] = @ShopByPriceMin
					,[ShopByPriceMax] = @ShopByPriceMax
					,[ShopByPriceIncrement] = @ShopByPriceIncrement
					,[FedExAccountNumber] = @FedExAccountNumber
					,[FedExMeterNumber] = @FedExMeterNumber
					,[FedExProductionKey] = @FedExProductionKey
					,[FedExSecurityCode] = @FedExSecurityCode
					,[FedExCSPKey] = @FedExCSPKey
					,[FedExCSPPassword] = @FedExCSPPassword
					,[FedExClientProductId] = @FedExClientProductId
					,[FedExClientProductVersion] = @FedExClientProductVersion
					,[FedExDropoffType] = @FedExDropoffType
					,[FedExPackagingType] = @FedExPackagingType
					,[FedExUseDiscountRate] = @FedExUseDiscountRate
					,[FedExAddInsurance] = @FedExAddInsurance
					,[ShippingOriginAddress1] = @ShippingOriginAddress1
					,[ShippingOriginAddress2] = @ShippingOriginAddress2
					,[ShippingOriginCity] = @ShippingOriginCity
					,[ShippingOriginStateCode] = @ShippingOriginStateCode
					,[ShippingOriginCountryCode] = @ShippingOriginCountryCode
					,[ShippingOriginPhone] = @ShippingOriginPhone
					,[CurrencyTypeID] = @CurrencyTypeID
					,[WeightUnit] = @WeightUnit
					,[DimensionUnit] = @DimensionUnit
					,[EmailListLogin] = @EmailListLogin
					,[EmailListPassword] = @EmailListPassword
					,[EmailListDefaultList] = @EmailListDefaultList
					,[ShippingTaxable] = @ShippingTaxable
					,[DefaultOrderStateID] = @DefaultOrderStateID
					,[DefaultReviewStatus] = @DefaultReviewStatus
					,[DefaultAnonymousProfileID] = @DefaultAnonymousProfileID
					,[DefaultRegisteredProfileID] = @DefaultRegisteredProfileID
					,[InclusiveTax] = @InclusiveTax
					,[SeoDefaultProductTitle] = @SeoDefaultProductTitle
					,[SeoDefaultProductDescription] = @SeoDefaultProductDescription
					,[SeoDefaultProductKeyword] = @SeoDefaultProductKeyword
					,[SeoDefaultCategoryTitle] = @SeoDefaultCategoryTitle
					,[SeoDefaultCategoryDescription] = @SeoDefaultCategoryDescription
					,[SeoDefaultCategoryKeyword] = @SeoDefaultCategoryKeyword
					,[SeoDefaultContentTitle] = @SeoDefaultContentTitle
					,[SeoDefaultContentDescription] = @SeoDefaultContentDescription
					,[SeoDefaultContentKeyword] = @SeoDefaultContentKeyword
					,[TimeZoneOffset] = @TimeZoneOffset
					,[LocaleID] = @LocaleID
					,[SplashCategoryID] = @SplashCategoryID
					,[SplashImageFile] = @SplashImageFile
					,[MobileTheme] = @MobileTheme
					,[PersistentCartEnabled] = @PersistentCartEnabled
				WHERE
[PortalID] = @PortalID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByCurrencyTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByCurrencyTypeID
(

	@CurrencyTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[CurrencyTypeID] = @CurrencyTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeSavedCartLineItem]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodeSavedCartLineItem]
(
[SavedCartLineItemID] [int] NOT NULL IDENTITY(1, 1),
[SavedCartID] [int] NOT NULL,
[SKUID] [int] NOT NULL,
[Quantity] [int] NOT NULL CONSTRAINT [DF_ZNodeSavedCartLineItem_Quantity] DEFAULT ((0)),
[ParentSavedCartLineItemID] [int] NULL,
[OrderLineItemRelationshipTypeID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeSavedCartLineItem] on [dbo].[ZNodeSavedCartLineItem]'
GO
ALTER TABLE [dbo].[ZNodeSavedCartLineItem] ADD CONSTRAINT [PK_ZNodeSavedCartLineItem] PRIMARY KEY CLUSTERED  ([SavedCartLineItemID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByLocaleID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByLocaleID
(

	@LocaleID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[LocaleID] = @LocaleID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByDefaultOrderStateID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByDefaultOrderStateID
(

	@DefaultOrderStateID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[DefaultOrderStateID] = @DefaultOrderStateID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByActiveInd]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByActiveInd
(

	@ActiveInd bit   
)
AS


				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[ActiveInd] = @ActiveInd
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_DeleteCatalog]'
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[ZNode_DeleteCatalog]        
(@CatalogID INT)    
AS     
BEGIN          
       
    
CREATE TABLE #CategoryList (CategoryID INT)    
    
INSERT INTO #CategoryList    
SELECT CategoryID     
FROM     
ZNodeCategoryNode ZCN    
WHERE    
ZCN.CatalogID = @CatalogID    
GROUP BY CategoryID;    
    
CREATE TABLE #ProductList (ProductID INT)    
    
INSERT INTO #ProductList    
SELECT ProductID     
FROM     
#CategoryList ZCN INNER JOIN    
ZNodeProductCategory ZPC ON ZPC.CategoryID = ZCN.CategoryID    
GROUP BY ProductID;    
    
    
 DECLARE  @ProductTypeTable Table([ID] INT);    
       
 INSERT INTO @ProductTypeTable (ID)    
 SELECT ProductTypeID FROM ZnodeProduct    
  WHERE    
 ProductID IN (SELECT ProductID FROM #ProductList)    
 GROUP BY ProductTypeID;    
    
DECLARE @SkuExists BIT;    
DECLARE @AddOnExists BIT;    
DECLARE @TagExists BIT;    
DECLARE @ProductTypeExists BIT;    
DECLARE @ManufacturerExists BIT;    
    
SET @AddOnExists = 0;    
SET @SkuExists = 0;    
SET @TagExists = 0;    
SET @ProductTypeExists = 0;    
    
-- AddOn     
SET @AddOnExists    
= (SELECT COUNT(1) FROM ZNodeProductAddOn ZPA    
INNER JOIN ZNodeAddOn ZA ON ZPA.AddOnID = ZA.AddOnID    
INNER JOIN #ProductList PL ON ZPA.ProductID != PL.ProductID)    
    
-- SKU    
SET @SkuExists    
= (SELECT COUNT(1) FROM ZNodeProductAttribute ZPA    
INNER JOIN ZNodeSKUAttribute ZSA ON ZSA.AttributeId = ZPA.AttributeId    
INNER JOIN ZNodeSKU ZS ON ZSA.SKUID = ZS.SKUID    
INNER JOIN #ProductList PL ON ZS.ProductID != PL.ProductID)    
    
    
SET @TagExists    
= (SELECT Count(1)    
FROM ZNodeTagProductSKU TPS    
INNER JOIN #ProductList PL ON TPS.ProductID != PL.ProductID)    
    
SET @ProductTypeExists    
= (SELECT Count(1) FROM ZNodeProduct p    
WHERE     
p.ProductID NOT IN (SELECT ProductID FROM #ProductList)    
AND    
p.ProductTypeID IN (SELECT ID FROM @ProductTypeTable))    

-- Delete the data for persistent cart
DELETE FROM ZNODESAVEDCARTLINEITEM WHERE 
SKUID IN 
(SELECT AddOnValueId FROM ZNodeAddOnValue WHERE AddOnID IN 
(SELECT ZPA.AddOnID FROM ZNodeProductAddOn ZPA    
  INNER JOIN ZNodeAddOn ZA ON ZPA.AddOnID = ZA.AddOnID    
  INNER JOIN #ProductList PL ON ZPA.ProductID = PL.ProductID))
AND
OrderLineItemRelationshipTypeID = 2

DELETE FROM ZNODESAVEDCARTLINEITEM WHERE 
SKUID IN (    
SELECT SKUID FROM ZNodeSKU WHERE ProductID    
IN (SELECT ProductID FROM #ProductList))    
AND
OrderLineItemRelationshipTypeID = 1

DELETE FROM ZNODESAVEDCARTLINEITEM WHERE 
SKUID IN (    
SELECT SKUID FROM ZNodeSKU WHERE ProductID    
IN (SELECT ProductID FROM #ProductList))    
AND
OrderLineItemRelationshipTypeID IS NULL

DELETE FROM ZNODESAVEDCART WHERE SavedCartID NOT IN (Select SavedCartID FROM ZNodeSavedCartLineItem)
 
DELETE FROM ZNODECOOKIEMAPPING WHERE CookieMappingID NOT IN (Select CookieMappingID FROM ZNODESAVEDCART)
    
--    
IF @AddOnExists = 0    
BEGIN    
 DELETE FROM ZNodeAddOnValue WHERE AddOnID IN (     
 (SELECT ZPA.AddOnID FROM ZNodeProductAddOn ZPA    
  INNER JOIN ZNodeAddOn ZA ON ZPA.AddOnID = ZA.AddOnID    
  INNER JOIN #ProductList PL ON ZPA.ProductID = PL.ProductID))    
      
  DELETE FROM ZNodeAddOn WHERE AddOnID IN (     
 (SELECT ZPA.AddOnID FROM     
  ZNodeProductAddOn ZPA    
  INNER JOIN #ProductList PL ON ZPA.ProductID = PL.ProductID))    
END    
    
DELETE FROM ZNodeProductAddOn WHERE ProductID    
IN (SELECT ProductID FROM #ProductList);    
    
IF @SkuExists = 0    
BEGIN    
 DELETE FROM ZNodeAddOnValue WHERE AddOnID IN (     
 SELECT COUNT(1) FROM ZNodeProductAttribute ZPA    
 INNER JOIN ZNodeSKUAttribute ZSA ON ZSA.AttributeId = ZPA.AttributeId    
 INNER JOIN ZNodeSKU ZS ON ZSA.SKUID = ZS.SKUID    
 INNER JOIN #ProductList PL ON ZS.ProductID != PL.ProductID)    
    
END    
    
DELETE FROM ZNodeSKUAttribute WHERE    
SKUID IN (    
SELECT SKUID FROM ZNodeSKU WHERE ProductID    
IN (SELECT ProductID FROM #ProductList))    
    
DELETE FROM ZNodeSKU WHERE ProductID    
IN (SELECT ProductID FROM #ProductList)    
    
    
IF @TagExists = 0    
BEGIN    
    
 CREATE TABLE #TagList (TagID INT)    
     
 INSERT INTO #TagList     
 SELECT TagID FROM     
 ZNodeTagProductSKU TPS    
 WHERE TPS.ProductID IN (SELECT ProductID FROM #ProductList);    
     
 DELETE FROM #TagList;    
 DELETE FROM ZNodeTag    
 WHERE TagID IN (SELECT TagID FROM #TagList);    
    
END    
ELSE    
BEGIN    
 DELETE FROM ZNodeTagProductSKU    
 WHERE ProductID IN (SELECT ProductID FROM #ProductList);    
END    
    
    
-- DELETE product reviews    
DELETE FROM ZNodeReview    
WHERE ProductID IN (SELECT ProductID FROM #ProductList);    
    
-- Delete Product Highlights    
DELETE FROM ZNodeProductHighlight    
WHERE ProductID IN (SELECT ProductID FROM #ProductList);    
    
-- Delete Product alternate and swatch images    
DELETE FROM ZNodeProductImage    
WHERE ProductID IN (SELECT ProductID FROM #ProductList);    
    
-- Delete Product Digital Assets    
DELETE FROM ZNodeDigitalAsset    
WHERE ProductID IN (SELECT ProductID FROM #ProductList);    
    
-- Delete Product category assocation    
DELETE FROM ZNodeProductCategory WHERE ProductID    
IN (SELECT ProductID FROM #ProductList)    
    
-- Delete Product Review History
DELETE FROM ZNODEPRODUCTREVIEWHISTORY WHERE ProductID    
IN (SELECT ProductID FROM #ProductList)    
    
-- Delete Product records    
DELETE FROM ZNodeProduct WHERE ProductID    
IN (SELECT ProductID FROM #ProductList);    
    
IF @ProductTypeExists = 0    
BEGIN    
    
 SELECT AttributeTypeID INTO #AttributeTypeData FROM ZNodeProductTypeAttribute     
 WHERE ProductTypeId IN (SELECT ID FROM @ProductTypeTable);    
      
 DELETE FROM ZNodeProductTypeAttribute     
 WHERE ProductTypeId IN (SELECT ID FROM @ProductTypeTable);    
     
 DELETE FROM ZNodeProductTypeAttribute     
 WHERE AttributeTypeId IN (    
 SELECT AttributeTypeId FROM #AttributeTypeData);    
     
 DELETE FROM ZNodeProductType WHERE ProductTypeId    
 IN (SELECT ID FROM @ProductTypeTable)    
     
END    
    
-- Delete Catalog category records and Category  
  
SELECT CategoryID INTO #CategoryData FROM ZNodeCategoryNode WHERE CatalogID = @CatalogID;  
DELETE FROM ZNodeCategory WHERE CategoryID IN (SELECT CategoryID FROM #CategoryData)  
DELETE FROM ZNodeCategoryNode WHERE CatalogID = @CatalogID;    
-- Delete Portal Catalog assocaition    
DELETE FROM ZNodePortalCatalog WHERE CatalogID = @CatalogID;    
    
-- Delete Catalog    
DELETE FROM ZNodeCatalog WHERE CatalogID = @CatalogID;    
    
END    
    
    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByPortalID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByPortalID
(

	@PortalID int   
)
AS


				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[PortalID] = @PortalID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeAddress] on [dbo].[ZNodeAddress]'
GO
ALTER TABLE [dbo].[ZNodeAddress] ADD CONSTRAINT [PK_ZNodeAddress] PRIMARY KEY CLUSTERED  ([AddressID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Find]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodePortal table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Find
(

	@SearchUsingOR bit   = null ,

	@PortalID int   = null ,

	@CompanyName nvarchar (MAX)  = null ,

	@StoreName nvarchar (MAX)  = null ,

	@LogoPath nvarchar (MAX)  = null ,

	@UseSSL bit   = null ,

	@AdminEmail nvarchar (MAX)  = null ,

	@SalesEmail nvarchar (MAX)  = null ,

	@CustomerServiceEmail nvarchar (MAX)  = null ,

	@SalesPhoneNumber nvarchar (MAX)  = null ,

	@CustomerServicePhoneNumber nvarchar (MAX)  = null ,

	@ImageNotAvailablePath nvarchar (MAX)  = null ,

	@MaxCatalogDisplayColumns tinyint   = null ,

	@MaxCatalogDisplayItems int   = null ,

	@MaxCatalogCategoryDisplayThumbnails int   = null ,

	@MaxCatalogItemSmallThumbnailWidth int   = null ,

	@MaxCatalogItemSmallWidth int   = null ,

	@MaxCatalogItemMediumWidth int   = null ,

	@MaxCatalogItemThumbnailWidth int   = null ,

	@MaxCatalogItemLargeWidth int   = null ,

	@MaxCatalogItemCrossSellWidth int   = null ,

	@ShowSwatchInCategory bit   = null ,

	@ShowAlternateImageInCategory bit   = null ,

	@ActiveInd bit   = null ,

	@SMTPServer nvarchar (MAX)  = null ,

	@SMTPUserName nvarchar (MAX)  = null ,

	@SMTPPassword nvarchar (MAX)  = null ,

	@SMTPPort int   = null ,

	@SiteWideBottomJavascript ntext   = null ,

	@SiteWideTopJavascript ntext   = null ,

	@OrderReceiptAffiliateJavascript ntext   = null ,

	@SiteWideAnalyticsJavascript ntext   = null ,

	@GoogleAnalyticsCode ntext   = null ,

	@UPSUserName nvarchar (MAX)  = null ,

	@UPSPassword nvarchar (MAX)  = null ,

	@UPSKey nvarchar (MAX)  = null ,

	@ShippingOriginZipCode nvarchar (50)  = null ,

	@MasterPage nvarchar (MAX)  = null ,

	@ShopByPriceMin int   = null ,

	@ShopByPriceMax int   = null ,

	@ShopByPriceIncrement int   = null ,

	@FedExAccountNumber nvarchar (MAX)  = null ,

	@FedExMeterNumber nvarchar (MAX)  = null ,

	@FedExProductionKey nvarchar (MAX)  = null ,

	@FedExSecurityCode nvarchar (MAX)  = null ,

	@FedExCSPKey nvarchar (MAX)  = null ,

	@FedExCSPPassword nvarchar (MAX)  = null ,

	@FedExClientProductId nvarchar (MAX)  = null ,

	@FedExClientProductVersion nvarchar (MAX)  = null ,

	@FedExDropoffType nvarchar (MAX)  = null ,

	@FedExPackagingType nvarchar (MAX)  = null ,

	@FedExUseDiscountRate bit   = null ,

	@FedExAddInsurance bit   = null ,

	@ShippingOriginAddress1 nvarchar (MAX)  = null ,

	@ShippingOriginAddress2 nvarchar (MAX)  = null ,

	@ShippingOriginCity nvarchar (MAX)  = null ,

	@ShippingOriginStateCode nvarchar (MAX)  = null ,

	@ShippingOriginCountryCode nvarchar (MAX)  = null ,

	@ShippingOriginPhone nvarchar (MAX)  = null ,

	@CurrencyTypeID int   = null ,

	@WeightUnit nvarchar (MAX)  = null ,

	@DimensionUnit nvarchar (MAX)  = null ,

	@EmailListLogin nvarchar (100)  = null ,

	@EmailListPassword nvarchar (100)  = null ,

	@EmailListDefaultList nvarchar (MAX)  = null ,

	@ShippingTaxable bit   = null ,

	@DefaultOrderStateID int   = null ,

	@DefaultReviewStatus nvarchar (1)  = null ,

	@DefaultAnonymousProfileID int   = null ,

	@DefaultRegisteredProfileID int   = null ,

	@InclusiveTax bit   = null ,

	@SeoDefaultProductTitle nvarchar (MAX)  = null ,

	@SeoDefaultProductDescription nvarchar (MAX)  = null ,

	@SeoDefaultProductKeyword nvarchar (MAX)  = null ,

	@SeoDefaultCategoryTitle nvarchar (MAX)  = null ,

	@SeoDefaultCategoryDescription nvarchar (MAX)  = null ,

	@SeoDefaultCategoryKeyword nvarchar (MAX)  = null ,

	@SeoDefaultContentTitle nvarchar (MAX)  = null ,

	@SeoDefaultContentDescription nvarchar (MAX)  = null ,

	@SeoDefaultContentKeyword nvarchar (MAX)  = null ,

	@TimeZoneOffset nvarchar (50)  = null ,

	@LocaleID int   = null ,

	@SplashCategoryID int   = null ,

	@SplashImageFile nvarchar (MAX)  = null ,

	@MobileTheme nvarchar (MAX)  = null ,

	@PersistentCartEnabled bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalID]
	, [CompanyName]
	, [StoreName]
	, [LogoPath]
	, [UseSSL]
	, [AdminEmail]
	, [SalesEmail]
	, [CustomerServiceEmail]
	, [SalesPhoneNumber]
	, [CustomerServicePhoneNumber]
	, [ImageNotAvailablePath]
	, [MaxCatalogDisplayColumns]
	, [MaxCatalogDisplayItems]
	, [MaxCatalogCategoryDisplayThumbnails]
	, [MaxCatalogItemSmallThumbnailWidth]
	, [MaxCatalogItemSmallWidth]
	, [MaxCatalogItemMediumWidth]
	, [MaxCatalogItemThumbnailWidth]
	, [MaxCatalogItemLargeWidth]
	, [MaxCatalogItemCrossSellWidth]
	, [ShowSwatchInCategory]
	, [ShowAlternateImageInCategory]
	, [ActiveInd]
	, [SMTPServer]
	, [SMTPUserName]
	, [SMTPPassword]
	, [SMTPPort]
	, [SiteWideBottomJavascript]
	, [SiteWideTopJavascript]
	, [OrderReceiptAffiliateJavascript]
	, [SiteWideAnalyticsJavascript]
	, [GoogleAnalyticsCode]
	, [UPSUserName]
	, [UPSPassword]
	, [UPSKey]
	, [ShippingOriginZipCode]
	, [MasterPage]
	, [ShopByPriceMin]
	, [ShopByPriceMax]
	, [ShopByPriceIncrement]
	, [FedExAccountNumber]
	, [FedExMeterNumber]
	, [FedExProductionKey]
	, [FedExSecurityCode]
	, [FedExCSPKey]
	, [FedExCSPPassword]
	, [FedExClientProductId]
	, [FedExClientProductVersion]
	, [FedExDropoffType]
	, [FedExPackagingType]
	, [FedExUseDiscountRate]
	, [FedExAddInsurance]
	, [ShippingOriginAddress1]
	, [ShippingOriginAddress2]
	, [ShippingOriginCity]
	, [ShippingOriginStateCode]
	, [ShippingOriginCountryCode]
	, [ShippingOriginPhone]
	, [CurrencyTypeID]
	, [WeightUnit]
	, [DimensionUnit]
	, [EmailListLogin]
	, [EmailListPassword]
	, [EmailListDefaultList]
	, [ShippingTaxable]
	, [DefaultOrderStateID]
	, [DefaultReviewStatus]
	, [DefaultAnonymousProfileID]
	, [DefaultRegisteredProfileID]
	, [InclusiveTax]
	, [SeoDefaultProductTitle]
	, [SeoDefaultProductDescription]
	, [SeoDefaultProductKeyword]
	, [SeoDefaultCategoryTitle]
	, [SeoDefaultCategoryDescription]
	, [SeoDefaultCategoryKeyword]
	, [SeoDefaultContentTitle]
	, [SeoDefaultContentDescription]
	, [SeoDefaultContentKeyword]
	, [TimeZoneOffset]
	, [LocaleID]
	, [SplashCategoryID]
	, [SplashImageFile]
	, [MobileTheme]
	, [PersistentCartEnabled]
    FROM
	[dbo].[ZNodePortal]
    WHERE 
	 ([PortalID] = @PortalID OR @PortalID IS NULL)
	AND ([CompanyName] = @CompanyName OR @CompanyName IS NULL)
	AND ([StoreName] = @StoreName OR @StoreName IS NULL)
	AND ([LogoPath] = @LogoPath OR @LogoPath IS NULL)
	AND ([UseSSL] = @UseSSL OR @UseSSL IS NULL)
	AND ([AdminEmail] = @AdminEmail OR @AdminEmail IS NULL)
	AND ([SalesEmail] = @SalesEmail OR @SalesEmail IS NULL)
	AND ([CustomerServiceEmail] = @CustomerServiceEmail OR @CustomerServiceEmail IS NULL)
	AND ([SalesPhoneNumber] = @SalesPhoneNumber OR @SalesPhoneNumber IS NULL)
	AND ([CustomerServicePhoneNumber] = @CustomerServicePhoneNumber OR @CustomerServicePhoneNumber IS NULL)
	AND ([ImageNotAvailablePath] = @ImageNotAvailablePath OR @ImageNotAvailablePath IS NULL)
	AND ([MaxCatalogDisplayColumns] = @MaxCatalogDisplayColumns OR @MaxCatalogDisplayColumns IS NULL)
	AND ([MaxCatalogDisplayItems] = @MaxCatalogDisplayItems OR @MaxCatalogDisplayItems IS NULL)
	AND ([MaxCatalogCategoryDisplayThumbnails] = @MaxCatalogCategoryDisplayThumbnails OR @MaxCatalogCategoryDisplayThumbnails IS NULL)
	AND ([MaxCatalogItemSmallThumbnailWidth] = @MaxCatalogItemSmallThumbnailWidth OR @MaxCatalogItemSmallThumbnailWidth IS NULL)
	AND ([MaxCatalogItemSmallWidth] = @MaxCatalogItemSmallWidth OR @MaxCatalogItemSmallWidth IS NULL)
	AND ([MaxCatalogItemMediumWidth] = @MaxCatalogItemMediumWidth OR @MaxCatalogItemMediumWidth IS NULL)
	AND ([MaxCatalogItemThumbnailWidth] = @MaxCatalogItemThumbnailWidth OR @MaxCatalogItemThumbnailWidth IS NULL)
	AND ([MaxCatalogItemLargeWidth] = @MaxCatalogItemLargeWidth OR @MaxCatalogItemLargeWidth IS NULL)
	AND ([MaxCatalogItemCrossSellWidth] = @MaxCatalogItemCrossSellWidth OR @MaxCatalogItemCrossSellWidth IS NULL)
	AND ([ShowSwatchInCategory] = @ShowSwatchInCategory OR @ShowSwatchInCategory IS NULL)
	AND ([ShowAlternateImageInCategory] = @ShowAlternateImageInCategory OR @ShowAlternateImageInCategory IS NULL)
	AND ([ActiveInd] = @ActiveInd OR @ActiveInd IS NULL)
	AND ([SMTPServer] = @SMTPServer OR @SMTPServer IS NULL)
	AND ([SMTPUserName] = @SMTPUserName OR @SMTPUserName IS NULL)
	AND ([SMTPPassword] = @SMTPPassword OR @SMTPPassword IS NULL)
	AND ([SMTPPort] = @SMTPPort OR @SMTPPort IS NULL)
	AND ([UPSUserName] = @UPSUserName OR @UPSUserName IS NULL)
	AND ([UPSPassword] = @UPSPassword OR @UPSPassword IS NULL)
	AND ([UPSKey] = @UPSKey OR @UPSKey IS NULL)
	AND ([ShippingOriginZipCode] = @ShippingOriginZipCode OR @ShippingOriginZipCode IS NULL)
	AND ([MasterPage] = @MasterPage OR @MasterPage IS NULL)
	AND ([ShopByPriceMin] = @ShopByPriceMin OR @ShopByPriceMin IS NULL)
	AND ([ShopByPriceMax] = @ShopByPriceMax OR @ShopByPriceMax IS NULL)
	AND ([ShopByPriceIncrement] = @ShopByPriceIncrement OR @ShopByPriceIncrement IS NULL)
	AND ([FedExAccountNumber] = @FedExAccountNumber OR @FedExAccountNumber IS NULL)
	AND ([FedExMeterNumber] = @FedExMeterNumber OR @FedExMeterNumber IS NULL)
	AND ([FedExProductionKey] = @FedExProductionKey OR @FedExProductionKey IS NULL)
	AND ([FedExSecurityCode] = @FedExSecurityCode OR @FedExSecurityCode IS NULL)
	AND ([FedExCSPKey] = @FedExCSPKey OR @FedExCSPKey IS NULL)
	AND ([FedExCSPPassword] = @FedExCSPPassword OR @FedExCSPPassword IS NULL)
	AND ([FedExClientProductId] = @FedExClientProductId OR @FedExClientProductId IS NULL)
	AND ([FedExClientProductVersion] = @FedExClientProductVersion OR @FedExClientProductVersion IS NULL)
	AND ([FedExDropoffType] = @FedExDropoffType OR @FedExDropoffType IS NULL)
	AND ([FedExPackagingType] = @FedExPackagingType OR @FedExPackagingType IS NULL)
	AND ([FedExUseDiscountRate] = @FedExUseDiscountRate OR @FedExUseDiscountRate IS NULL)
	AND ([FedExAddInsurance] = @FedExAddInsurance OR @FedExAddInsurance IS NULL)
	AND ([ShippingOriginAddress1] = @ShippingOriginAddress1 OR @ShippingOriginAddress1 IS NULL)
	AND ([ShippingOriginAddress2] = @ShippingOriginAddress2 OR @ShippingOriginAddress2 IS NULL)
	AND ([ShippingOriginCity] = @ShippingOriginCity OR @ShippingOriginCity IS NULL)
	AND ([ShippingOriginStateCode] = @ShippingOriginStateCode OR @ShippingOriginStateCode IS NULL)
	AND ([ShippingOriginCountryCode] = @ShippingOriginCountryCode OR @ShippingOriginCountryCode IS NULL)
	AND ([ShippingOriginPhone] = @ShippingOriginPhone OR @ShippingOriginPhone IS NULL)
	AND ([CurrencyTypeID] = @CurrencyTypeID OR @CurrencyTypeID IS NULL)
	AND ([WeightUnit] = @WeightUnit OR @WeightUnit IS NULL)
	AND ([DimensionUnit] = @DimensionUnit OR @DimensionUnit IS NULL)
	AND ([EmailListLogin] = @EmailListLogin OR @EmailListLogin IS NULL)
	AND ([EmailListPassword] = @EmailListPassword OR @EmailListPassword IS NULL)
	AND ([EmailListDefaultList] = @EmailListDefaultList OR @EmailListDefaultList IS NULL)
	AND ([ShippingTaxable] = @ShippingTaxable OR @ShippingTaxable IS NULL)
	AND ([DefaultOrderStateID] = @DefaultOrderStateID OR @DefaultOrderStateID IS NULL)
	AND ([DefaultReviewStatus] = @DefaultReviewStatus OR @DefaultReviewStatus IS NULL)
	AND ([DefaultAnonymousProfileID] = @DefaultAnonymousProfileID OR @DefaultAnonymousProfileID IS NULL)
	AND ([DefaultRegisteredProfileID] = @DefaultRegisteredProfileID OR @DefaultRegisteredProfileID IS NULL)
	AND ([InclusiveTax] = @InclusiveTax OR @InclusiveTax IS NULL)
	AND ([SeoDefaultProductTitle] = @SeoDefaultProductTitle OR @SeoDefaultProductTitle IS NULL)
	AND ([SeoDefaultProductDescription] = @SeoDefaultProductDescription OR @SeoDefaultProductDescription IS NULL)
	AND ([SeoDefaultProductKeyword] = @SeoDefaultProductKeyword OR @SeoDefaultProductKeyword IS NULL)
	AND ([SeoDefaultCategoryTitle] = @SeoDefaultCategoryTitle OR @SeoDefaultCategoryTitle IS NULL)
	AND ([SeoDefaultCategoryDescription] = @SeoDefaultCategoryDescription OR @SeoDefaultCategoryDescription IS NULL)
	AND ([SeoDefaultCategoryKeyword] = @SeoDefaultCategoryKeyword OR @SeoDefaultCategoryKeyword IS NULL)
	AND ([SeoDefaultContentTitle] = @SeoDefaultContentTitle OR @SeoDefaultContentTitle IS NULL)
	AND ([SeoDefaultContentDescription] = @SeoDefaultContentDescription OR @SeoDefaultContentDescription IS NULL)
	AND ([SeoDefaultContentKeyword] = @SeoDefaultContentKeyword OR @SeoDefaultContentKeyword IS NULL)
	AND ([TimeZoneOffset] = @TimeZoneOffset OR @TimeZoneOffset IS NULL)
	AND ([LocaleID] = @LocaleID OR @LocaleID IS NULL)
	AND ([SplashCategoryID] = @SplashCategoryID OR @SplashCategoryID IS NULL)
	AND ([SplashImageFile] = @SplashImageFile OR @SplashImageFile IS NULL)
	AND ([MobileTheme] = @MobileTheme OR @MobileTheme IS NULL)
	AND ([PersistentCartEnabled] = @PersistentCartEnabled OR @PersistentCartEnabled IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalID]
	, [CompanyName]
	, [StoreName]
	, [LogoPath]
	, [UseSSL]
	, [AdminEmail]
	, [SalesEmail]
	, [CustomerServiceEmail]
	, [SalesPhoneNumber]
	, [CustomerServicePhoneNumber]
	, [ImageNotAvailablePath]
	, [MaxCatalogDisplayColumns]
	, [MaxCatalogDisplayItems]
	, [MaxCatalogCategoryDisplayThumbnails]
	, [MaxCatalogItemSmallThumbnailWidth]
	, [MaxCatalogItemSmallWidth]
	, [MaxCatalogItemMediumWidth]
	, [MaxCatalogItemThumbnailWidth]
	, [MaxCatalogItemLargeWidth]
	, [MaxCatalogItemCrossSellWidth]
	, [ShowSwatchInCategory]
	, [ShowAlternateImageInCategory]
	, [ActiveInd]
	, [SMTPServer]
	, [SMTPUserName]
	, [SMTPPassword]
	, [SMTPPort]
	, [SiteWideBottomJavascript]
	, [SiteWideTopJavascript]
	, [OrderReceiptAffiliateJavascript]
	, [SiteWideAnalyticsJavascript]
	, [GoogleAnalyticsCode]
	, [UPSUserName]
	, [UPSPassword]
	, [UPSKey]
	, [ShippingOriginZipCode]
	, [MasterPage]
	, [ShopByPriceMin]
	, [ShopByPriceMax]
	, [ShopByPriceIncrement]
	, [FedExAccountNumber]
	, [FedExMeterNumber]
	, [FedExProductionKey]
	, [FedExSecurityCode]
	, [FedExCSPKey]
	, [FedExCSPPassword]
	, [FedExClientProductId]
	, [FedExClientProductVersion]
	, [FedExDropoffType]
	, [FedExPackagingType]
	, [FedExUseDiscountRate]
	, [FedExAddInsurance]
	, [ShippingOriginAddress1]
	, [ShippingOriginAddress2]
	, [ShippingOriginCity]
	, [ShippingOriginStateCode]
	, [ShippingOriginCountryCode]
	, [ShippingOriginPhone]
	, [CurrencyTypeID]
	, [WeightUnit]
	, [DimensionUnit]
	, [EmailListLogin]
	, [EmailListPassword]
	, [EmailListDefaultList]
	, [ShippingTaxable]
	, [DefaultOrderStateID]
	, [DefaultReviewStatus]
	, [DefaultAnonymousProfileID]
	, [DefaultRegisteredProfileID]
	, [InclusiveTax]
	, [SeoDefaultProductTitle]
	, [SeoDefaultProductDescription]
	, [SeoDefaultProductKeyword]
	, [SeoDefaultCategoryTitle]
	, [SeoDefaultCategoryDescription]
	, [SeoDefaultCategoryKeyword]
	, [SeoDefaultContentTitle]
	, [SeoDefaultContentDescription]
	, [SeoDefaultContentKeyword]
	, [TimeZoneOffset]
	, [LocaleID]
	, [SplashCategoryID]
	, [SplashImageFile]
	, [MobileTheme]
	, [PersistentCartEnabled]
    FROM
	[dbo].[ZNodePortal]
    WHERE 
	 ([PortalID] = @PortalID AND @PortalID is not null)
	OR ([CompanyName] = @CompanyName AND @CompanyName is not null)
	OR ([StoreName] = @StoreName AND @StoreName is not null)
	OR ([LogoPath] = @LogoPath AND @LogoPath is not null)
	OR ([UseSSL] = @UseSSL AND @UseSSL is not null)
	OR ([AdminEmail] = @AdminEmail AND @AdminEmail is not null)
	OR ([SalesEmail] = @SalesEmail AND @SalesEmail is not null)
	OR ([CustomerServiceEmail] = @CustomerServiceEmail AND @CustomerServiceEmail is not null)
	OR ([SalesPhoneNumber] = @SalesPhoneNumber AND @SalesPhoneNumber is not null)
	OR ([CustomerServicePhoneNumber] = @CustomerServicePhoneNumber AND @CustomerServicePhoneNumber is not null)
	OR ([ImageNotAvailablePath] = @ImageNotAvailablePath AND @ImageNotAvailablePath is not null)
	OR ([MaxCatalogDisplayColumns] = @MaxCatalogDisplayColumns AND @MaxCatalogDisplayColumns is not null)
	OR ([MaxCatalogDisplayItems] = @MaxCatalogDisplayItems AND @MaxCatalogDisplayItems is not null)
	OR ([MaxCatalogCategoryDisplayThumbnails] = @MaxCatalogCategoryDisplayThumbnails AND @MaxCatalogCategoryDisplayThumbnails is not null)
	OR ([MaxCatalogItemSmallThumbnailWidth] = @MaxCatalogItemSmallThumbnailWidth AND @MaxCatalogItemSmallThumbnailWidth is not null)
	OR ([MaxCatalogItemSmallWidth] = @MaxCatalogItemSmallWidth AND @MaxCatalogItemSmallWidth is not null)
	OR ([MaxCatalogItemMediumWidth] = @MaxCatalogItemMediumWidth AND @MaxCatalogItemMediumWidth is not null)
	OR ([MaxCatalogItemThumbnailWidth] = @MaxCatalogItemThumbnailWidth AND @MaxCatalogItemThumbnailWidth is not null)
	OR ([MaxCatalogItemLargeWidth] = @MaxCatalogItemLargeWidth AND @MaxCatalogItemLargeWidth is not null)
	OR ([MaxCatalogItemCrossSellWidth] = @MaxCatalogItemCrossSellWidth AND @MaxCatalogItemCrossSellWidth is not null)
	OR ([ShowSwatchInCategory] = @ShowSwatchInCategory AND @ShowSwatchInCategory is not null)
	OR ([ShowAlternateImageInCategory] = @ShowAlternateImageInCategory AND @ShowAlternateImageInCategory is not null)
	OR ([ActiveInd] = @ActiveInd AND @ActiveInd is not null)
	OR ([SMTPServer] = @SMTPServer AND @SMTPServer is not null)
	OR ([SMTPUserName] = @SMTPUserName AND @SMTPUserName is not null)
	OR ([SMTPPassword] = @SMTPPassword AND @SMTPPassword is not null)
	OR ([SMTPPort] = @SMTPPort AND @SMTPPort is not null)
	OR ([UPSUserName] = @UPSUserName AND @UPSUserName is not null)
	OR ([UPSPassword] = @UPSPassword AND @UPSPassword is not null)
	OR ([UPSKey] = @UPSKey AND @UPSKey is not null)
	OR ([ShippingOriginZipCode] = @ShippingOriginZipCode AND @ShippingOriginZipCode is not null)
	OR ([MasterPage] = @MasterPage AND @MasterPage is not null)
	OR ([ShopByPriceMin] = @ShopByPriceMin AND @ShopByPriceMin is not null)
	OR ([ShopByPriceMax] = @ShopByPriceMax AND @ShopByPriceMax is not null)
	OR ([ShopByPriceIncrement] = @ShopByPriceIncrement AND @ShopByPriceIncrement is not null)
	OR ([FedExAccountNumber] = @FedExAccountNumber AND @FedExAccountNumber is not null)
	OR ([FedExMeterNumber] = @FedExMeterNumber AND @FedExMeterNumber is not null)
	OR ([FedExProductionKey] = @FedExProductionKey AND @FedExProductionKey is not null)
	OR ([FedExSecurityCode] = @FedExSecurityCode AND @FedExSecurityCode is not null)
	OR ([FedExCSPKey] = @FedExCSPKey AND @FedExCSPKey is not null)
	OR ([FedExCSPPassword] = @FedExCSPPassword AND @FedExCSPPassword is not null)
	OR ([FedExClientProductId] = @FedExClientProductId AND @FedExClientProductId is not null)
	OR ([FedExClientProductVersion] = @FedExClientProductVersion AND @FedExClientProductVersion is not null)
	OR ([FedExDropoffType] = @FedExDropoffType AND @FedExDropoffType is not null)
	OR ([FedExPackagingType] = @FedExPackagingType AND @FedExPackagingType is not null)
	OR ([FedExUseDiscountRate] = @FedExUseDiscountRate AND @FedExUseDiscountRate is not null)
	OR ([FedExAddInsurance] = @FedExAddInsurance AND @FedExAddInsurance is not null)
	OR ([ShippingOriginAddress1] = @ShippingOriginAddress1 AND @ShippingOriginAddress1 is not null)
	OR ([ShippingOriginAddress2] = @ShippingOriginAddress2 AND @ShippingOriginAddress2 is not null)
	OR ([ShippingOriginCity] = @ShippingOriginCity AND @ShippingOriginCity is not null)
	OR ([ShippingOriginStateCode] = @ShippingOriginStateCode AND @ShippingOriginStateCode is not null)
	OR ([ShippingOriginCountryCode] = @ShippingOriginCountryCode AND @ShippingOriginCountryCode is not null)
	OR ([ShippingOriginPhone] = @ShippingOriginPhone AND @ShippingOriginPhone is not null)
	OR ([CurrencyTypeID] = @CurrencyTypeID AND @CurrencyTypeID is not null)
	OR ([WeightUnit] = @WeightUnit AND @WeightUnit is not null)
	OR ([DimensionUnit] = @DimensionUnit AND @DimensionUnit is not null)
	OR ([EmailListLogin] = @EmailListLogin AND @EmailListLogin is not null)
	OR ([EmailListPassword] = @EmailListPassword AND @EmailListPassword is not null)
	OR ([EmailListDefaultList] = @EmailListDefaultList AND @EmailListDefaultList is not null)
	OR ([ShippingTaxable] = @ShippingTaxable AND @ShippingTaxable is not null)
	OR ([DefaultOrderStateID] = @DefaultOrderStateID AND @DefaultOrderStateID is not null)
	OR ([DefaultReviewStatus] = @DefaultReviewStatus AND @DefaultReviewStatus is not null)
	OR ([DefaultAnonymousProfileID] = @DefaultAnonymousProfileID AND @DefaultAnonymousProfileID is not null)
	OR ([DefaultRegisteredProfileID] = @DefaultRegisteredProfileID AND @DefaultRegisteredProfileID is not null)
	OR ([InclusiveTax] = @InclusiveTax AND @InclusiveTax is not null)
	OR ([SeoDefaultProductTitle] = @SeoDefaultProductTitle AND @SeoDefaultProductTitle is not null)
	OR ([SeoDefaultProductDescription] = @SeoDefaultProductDescription AND @SeoDefaultProductDescription is not null)
	OR ([SeoDefaultProductKeyword] = @SeoDefaultProductKeyword AND @SeoDefaultProductKeyword is not null)
	OR ([SeoDefaultCategoryTitle] = @SeoDefaultCategoryTitle AND @SeoDefaultCategoryTitle is not null)
	OR ([SeoDefaultCategoryDescription] = @SeoDefaultCategoryDescription AND @SeoDefaultCategoryDescription is not null)
	OR ([SeoDefaultCategoryKeyword] = @SeoDefaultCategoryKeyword AND @SeoDefaultCategoryKeyword is not null)
	OR ([SeoDefaultContentTitle] = @SeoDefaultContentTitle AND @SeoDefaultContentTitle is not null)
	OR ([SeoDefaultContentDescription] = @SeoDefaultContentDescription AND @SeoDefaultContentDescription is not null)
	OR ([SeoDefaultContentKeyword] = @SeoDefaultContentKeyword AND @SeoDefaultContentKeyword is not null)
	OR ([TimeZoneOffset] = @TimeZoneOffset AND @TimeZoneOffset is not null)
	OR ([LocaleID] = @LocaleID AND @LocaleID is not null)
	OR ([SplashCategoryID] = @SplashCategoryID AND @SplashCategoryID is not null)
	OR ([SplashImageFile] = @SplashImageFile AND @SplashImageFile is not null)
	OR ([MobileTheme] = @MobileTheme AND @MobileTheme is not null)
	OR ([PersistentCartEnabled] = @PersistentCartEnabled AND @PersistentCartEnabled is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetAttributesByProductType]'
GO
SET ANSI_NULLS ON
GO

-- =============================================  
-- Create date: 18 Jan 2007  
-- Description: Returns Attibutes for this product type  
-- =============================================  
CREATE PROCEDURE [dbo].[ZNode_GetAttributesByProductType]  
 @PRODUCTTYPEID  int  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- get attributes  
 SELECT [AttributeId]  
      ,[AttributeTypeId]  
      ,[Name]  
      ,[ExternalId]  
      ,[DisplayOrder]  
      ,[IsActive]  
      ,[OldAttributeId]  
  FROM [dbo].[ZNodeProductAttribute] where attributetypeid in  
(  
SELECT [AttributeTypeId]      
  FROM [dbo].[ZNodeAttributeType] where attributetypeid in  
(  
SELECT [AttributeTypeId]  
  FROM [dbo].[ZNodeProductTypeAttribute] where producttypeid=@PRODUCTTYPEID  
))  
END  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetAllProductVendor]'
GO

  
ALTER  Procedure [dbo].[ZNode_GetAllProductVendor]    
as    
Select (select username from aspnet_Users where UserId=ZNodeAccount.UserID) ContactName,    
ISNULL(Email,'') ContactEmail,    
ISNULL(CompanyName,'') CompanyName,  
AccountID    
    
from ZNodeAccount    
Where AccountID in (Select AccountID from ZNodeProduct) and
 UserID in (select UserId from aspnet_UsersInRoles where   
RoleId in (select RoleId from aspnet_Roles where RoleName='VENDOR'))  
    
  
  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetAttributeTypesByProductType]'
GO

-- =============================================    
-- Create date: 19 Jan 2007    
-- Description: Returns Attibute Types for this product type    
-- =============================================    
CREATE PROCEDURE [dbo].[ZNode_GetAttributeTypesByProductType]    
@PRODUCTTYPEID  int    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- get attributes    
SELECT [AttributeTypeId],Name,Isprivate        
  FROM [ZNodeAttributeType] where attributetypeid in  
(    
SELECT [AttributeTypeId]    
  FROM [ZNodeProductTypeAttribute] where producttypeid= @PRODUCTTYPEID  
)    
END   

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNODE_WS_GetSkuQuantyListByFilter_XML]'
GO

ALTER PROCEDURE [dbo].[ZNODE_WS_GetSkuQuantyListByFilter_XML](@Filter int = NULL)     
AS        
 BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
	CREATE TABLE #temp (Name nvarchar(max), ProductNum nvarchar(max),Sku nvarchar(max),QuantityOnHand int,ReOrderLevel int, ActiveInd bit)
  IF (@Filter = 0)        
   BEGIN  
		INSERT INTO #temp(Name,ProductNum,ActiveInd)        
			(SELECT Name,ZNodeProduct.ProductNum,ActiveInd FROM ZNodeProduct   
 WHERE ProductId NOT IN (SELECT ProductID FROM ZNodeSKU))

		INSERT INTO #temp(Sku,QuantityOnHand,ReOrderLevel,ActiveInd)  
			(SELECT SKU.Sku,ZNodeSKUInventory.QuantityOnHand,ReOrderLevel,ActiveInd FROM ZNodeSKU SKU INNER JOIN ZNodeSKUInventory ON ZNodeSKUInventory.SKU = SKU.SKU)  
	  
		INSERT INTO #temp(Name,Sku,ActiveInd)  
			(SELECT Name,ZNodeAddOnvalue.Sku,1 FROM ZNodeAddOnvalue) 
   END        
  ELSE IF (@Filter = 1)        
    BEGIN
		INSERT INTO #temp(Name,ProductNum,ActiveInd)        
			(SELECT Name, ZNodeProduct.ProductNum,ActiveInd FROM ZNodeProduct WHERE ProductId NOT IN (SELECT ProductID FROM ZNodeSKU) AND [WebServiceDownloadDte] IS NULL)

		INSERT INTO #temp(Sku,QuantityOnHand,ReOrderLevel,ActiveInd)  
			(SELECT SKU.Sku,ZNodeSKUInventory.QuantityOnHand,ReOrderLevel,ActiveInd FROM ZNodeSKU SKU  INNER JOIN ZNodeSKUInventory ON ZNodeSKUInventory.SKU = SKU.SKU WHERE [WebServiceDownloadDte] IS NULL)  
	  
		INSERT INTO #temp(Name,Sku,ActiveInd)  
			(SELECT Name,ZNodeAddOnvalue.Sku,1 FROM ZNodeAddOnvalue WHERE [WebServiceDownloadDte] IS NULL) 
    END
   ELSE IF (@Filter = 2)        
     BEGIN 
		INSERT INTO #temp(Name,ProductNum,ActiveInd)
			(SELECT Name, ZNodeProduct.ProductNum, ActiveInd FROM ZNodeProduct WHERE ProductId NOT IN (SELECT ProductID FROM ZNodeSKU) AND [WebServiceDownloadDte] < [UpdateDte])

		INSERT INTO #temp(Sku,QuantityOnHand,ReOrderLevel,ActiveInd)  
			(SELECT SKU.Sku,ZNodeSKUInventory.QuantityOnHand,ReOrderLevel,ActiveInd FROM ZNodeSKU SKU  INNER JOIN ZNodeSKUInventory ON ZNodeSKUInventory.SKU = SKU.SKU WHERE [WebServiceDownloadDte] < [UpdateDte])  
	  
		INSERT INTO #temp(Name,Sku,ActiveInd)  
			(SELECT Name,ZNodeAddOnvalue.Sku,1 FROM ZNodeAddOnvalue WHERE [WebServiceDownloadDte] < [UpdateDte])     
     END

-- Select result sets from temporary table --  
 SELECT * FROM #temp Inventory FOR XML AUTO , TYPE , ELEMENTS
 -- Drop temporary table  
 DROP TABLE #temp            
 END   

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_WS_GetCustomers]'
GO


ALTER PROCEDURE [dbo].[ZNode_WS_GetCustomers]  
AS  
BEGIN  
-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM          
 -- INTERFERING WITH SELECT STATEMENTS.          
 SET NOCOUNT ON;  
  
SELECT ZA.[ACCOUNTID]      
      ,ZA.[PARENTACCOUNTID]                   
      ,(SELECT USERNAME FROM [ASPNET_USERS] WHERE [USERID] = ZA.[USERID]) AS 'USERNAME'           
      ,ZA.[EXTERNALACCOUNTNO]      
      ,ZA.[COMPANYNAME]      
      ,(SELECT ACCOUNTTYPENME FROM [ZNODEACCOUNTTYPE] WHERE ACCOUNTTYPEID = ZA.[ACCOUNTTYPEID]) AS 'ACCOUNTTYPE NAME'  
      ,(SELECT NAME FROM [ZNODEPROFILE] WHERE [PROFILEID] = ZA.[PROFILEID]) AS 'PROFILE NAME'  
      ,ZA.[ACCOUNTPROFILECODE]      
      ,ZA.[SUBACCOUNTLIMIT]      
      ,A.FirstName as [BILLINGFIRSTNAME]      
      ,A.LastName as [BILLINGLASTNAME]      
      ,A.CompanyName as[BILLINGCOMPANYNAME]      
      ,A.Street as [BILLINGSTREET]      
      ,A.Street1 as [BILLINGSTREET1]      
      ,A.City as [BILLINGCITY]      
      ,A.StateCode as [BILLINGSTATECODE]      
      ,A.PostalCode as [BILLINGPOSTALCODE]      
      ,A.CountryCode as [BILLINGCOUNTRYCODE]      
      ,A.Phonenumber as [BILLINGPHONENUMBER]      
      ,ZA.Email as [BILLINGEMAILID]      
      ,AD.FirstName as [SHIPFIRSTNAME]      
      ,AD.LastName as [SHIPLASTNAME]      
      ,AD.CompanyName as [SHIPCOMPANYNAME]      
      ,AD.Street as [SHIPSTREET]      
      ,AD.Street1 as [SHIPSTREET1]      
      ,AD.City as [SHIPCITY]      
      ,AD.StateCode as [SHIPSTATECODE]      
      ,AD.PostalCode as [SHIPPOSTALCODE]      
      ,AD.CountryCode as [SHIPCOUNTRYCODE]      
      ,ZA.Email as [SHIPEMAILID]      
      ,AD.Phonenumber as [SHIPPHONENUMBER]      
      ,ZA.[DESCRIPTION]      
      ,ZA.[CREATEUSER]      
      ,ZA.[CREATEDTE]      
      ,ZA.[UPDATEUSER]      
      ,ZA.[UPDATEDTE]      
      ,ZA.[ACTIVEIND]  
      ,ZA.[WEBSITE]  
      ,ZA.[SOURCE]        
      ,ZA.[CUSTOM1]  
      ,ZA.[CUSTOM2]  
      ,ZA.[CUSTOM3]        
    FROM ZnodeAccount ZA
      Left join ZNodeAddress A ON ZA.AccountID = A.AccountID AND IsDefaultBilling = 1 
      Left join ZNodeAddress AD ON ZA.AccountID = AD.AccountID AND AD.IsDefaultShipping = 1 
      FOR XML AUTO,TYPE,ELEMENTS  
END  


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNODE_WS_GetAccountsByFilter_XML]'
GO


ALTER PROCEDURE [dbo].[ZNODE_WS_GetAccountsByFilter_XML]   
(    
 @Filter int = NULL,    
 @BeginDate DateTime = NULL,    
 @EndDate DateTime = NULL,    
 @FirstName Varchar(1000) = NULL,    
 @LastName Varchar(1000) = NULL,    
 @AccountNumber Varchar(1000) = NULL,    
 @CompanyName Varchar(1000) = NULL,    
 @ZipCode Varchar(1000) = NULL,    
 @Phone Varchar(1000) = NULL,    
 @PortalID Varchar(1000) = NULL        
)     
AS            
 BEGIN            
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
 With Account AS          
 (          
   SELECT *,          
  Filter = (CASE WHEN WebServiceDownloadDte IS NULL AND @Filter = 1 THEN 1          
    WHEN [WebServiceDownloadDte] < [UpdateDte] AND @Filter = 2 THEN 2          
    WHEN @Filter = 0 THEN 0          
    WHEN @Filter IS NULL THEN NULL                 
     End) FROM ZNodeAccount     
          
 )         
        
    SELECT ZA.[AccountID]          
      ,ZA.[ParentAccountId]                 
      ,ZA.[UserID]        
      ,ZA.[ExternalAccountNo]          
      ,ZA.[CompanyName]          
      ,ZA.[AccountTypeId]          
      ,ZA.[ProfileId]        
      ,ZA.[AccountProfileCode]          
      ,ZA.[SubAccountLimit]          
      ,A.FirstName as [BillingFirstName]          
      ,A.LastName as [BillingLastName]          
      ,A.CompanyName as [BILLINGCOMPANYNAME] 
      ,A.Street as [BILLINGSTREET] 
      ,A.Street1 as[BILLINGSTREET1]        
      ,A.City as [BillingCity]          
      ,A.StateCode as [BillingStateCode]          
      ,A.PostalCode as [BillingPostalCode]          
      ,A.CountryCode as [BillingCountryCode]          
      ,A.Phonenumber as [BillingPhoneNumber]          
      ,ZA.Email as [BillingEmailId]          
      ,AD.FirstName as [ShipFirstName]          
      ,AD.LastName as [ShipLastName]          
      ,AD.CompanyName as [ShipCompanyName]          
      ,AD.Street as [ShipStreet]          
      ,AD.Street1 as [ShipStreet1]          
      ,AD.City as [ShipCity]          
      ,AD.StateCode as [ShipStateCode]          
      ,AD.PostalCode as [ShipPostalCode]          
      ,AD.CountryCode as [ShipCountryCode]          
      ,ZA.Email as [ShipEmailId]          
      ,AD.Phonenumber as [ShipPhoneNumber]          
      ,ZA.[Description]          
      ,ZA.[CreateUser]          
      ,ZA.[CreateDte]          
      ,ZA.[UpdateUser]          
      ,ZA.[UpdateDte]          
      ,ZA.[ActiveInd]          
      ,ZA.[Website]      
      ,ZA.[Source]                
      ,ZA.[Custom1]          
      ,ZA.[Custom2]          
      ,ZA.[Custom3]             
      ,ZA.[EmailOptIn]          
      ,[WebServiceDownloadDte]         
  FROM Account ZA
      Left join ZNodeAddress A ON ZA.AccountID = A.AccountID AND IsDefaultBilling = 1 
      Left join ZNodeAddress AD ON ZA.AccountID = AD.AccountID AND AD.IsDefaultShipping = 1
   WHERE Filter = @Filter        
   AND ((CreateDte Between @BeginDate AND @EndDate) OR (@BeginDate IS NULL OR @EndDate IS NULL))        
   AND (IsNull(A.FirstName,'') LIKE @FirstName + '%' OR IsNull(AD.FirstName,'') LIKE @FirstName + '%' OR @FirstName IS NULL)        
   AND (IsNull(A.LastName ,'') LIKE @LastName + '%' OR IsNull(AD.LastName,'') LIKE @LastName + '%' OR @LastName IS NULL)        
   AND (IsNull(ZA.CompanyName,'')  LIKE @CompanyName + '%' OR IsNull(A.CompanyName,'') LIKE @CompanyName + '%' OR IsNull(AD.CompanyName,'') LIKE @CompanyName + '%' OR @CompanyName IS NULL)        
   AND (IsNull(ExternalAccountNo,'') LIKE @AccountNumber + '%' OR @AccountNumber IS NULL)        
   AND (IsNull(A.PostalCode,'') LIKE @ZipCode + '%' OR IsNull(AD.PostalCode,'') LIKE @ZipCode + '%' OR @ZipCode IS NULL)        
   AND (IsNull(A.Phonenumber,'') LIKE @Phone + '%' OR IsNull(AD.Phonenumber,'') LIKE @Phone + '%' OR @Phone IS NULL)         
   AND  ZA.ACCOUNTID IN (SELECT ACCOUNTID FROM ZNODEACCOUNTPROFILE, ZNODEPORTALPROFILE WHERE ZNODEPORTALPROFILE.PROFILEID = ZNODEACCOUNTPROFILE.ProfileID 
   AND ZNODEPORTALPROFILE.PORTALID = @PortalID OR @PortalID IS NULL)  
FOR XML AUTO , TYPE , ELEMENTS        
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_WS_GetProductAddOnsByID_XML]'
GO


ALTER PROCEDURE [dbo].[ZNode_WS_GetProductAddOnsByID_XML](@ProductID INT = NULL)
AS            
	BEGIN

		SELECT [AddOnId]
			  ,[ProductId]
			  ,[Title]
			  ,[Name]
			  ,[Description]
			  ,[DisplayOrder]
			  ,[DisplayType]
			  ,[OptionalInd]
			  ,[AllowBackOrder]
			  ,[InStockMsg]
			  ,[OutOfStockMsg]
			  ,[BackOrderMsg]
			  ,[PromptMsg]
			  ,[TrackInventoryInd]
			  ,ZL.LocaleCode,
			  (SELECT [AddOnValueId]
					  ,[AddOnId]
					  ,[Name]
					  ,[Description]
					  ,AddOnValue.[Sku]
					  ,[DefaultInd]
					  ,[DisplayOrder]
					  ,[ImageFile]
					  ,[RetailPrice]
					  ,[SalePrice]
					  ,[WholesalePrice]
					  ,[Weight]
					  ,[Length]
					  ,[Height]
					  ,[Width]
					  ,[ShippingRuleTypeID]
					  ,[FreeShippingInd]
					  ,[WebServiceDownloadDte]
					  ,[UpdateDte]
					 FROM ZNodeAddOnValue AddOnValue
					 WHERE AddOnValue.AddOnId = AddOn.AddOnId FOR XML AUTO , TYPE , ELEMENTS)
			FROM ZNodeAddOn AddOn 
			LEFT JOIN ZNodeLocale ZL ON ZL.LocaleId = AddOn.LocaleId
			WHERE AddOn.AddOnID IN 
				(SELECT AddOnId FROM ZNodeProductAddOn WHERE ProductID = @ProductID) FOR XML PATH ('AddOn'), TYPE , ELEMENTS
	END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetSkuInventoryListByFilterByPortalIds]'
GO

  
    
ALTER PROCEDURE [dbo].[ZNode_GetSkuInventoryListByFilterByPortalIds]
(
@ProductType Int = Null,
@PortalID VARCHAR(100) = ''
)               
AS                  
 BEGIN                  
 -- SET NOCOUNT ON added to prevent extra result sets from              
 -- interfering with SELECT statements.              
  
IF (@ProductType = 1)      
BEGIN  
  SELECT     
   DISTINCT   
   B.Name,
   B.ProductNum    
  FROM ZNodeProduct B 
  INNER JOIN ZNodeProductCategory C ON C.ProductID = B.ProductID 
  INNER JOIN ZNodeCategoryNode D ON D.CategoryID = C.CategoryID
  INNER JOIN ZNodePortalCatalog E ON E.CatalogID = D.CatalogID 
  WHERE E.PortalID IN (SELECT Value FROM SC_Splitter(@PortalID, ',')) OR LEN(@PortalID) = 0
END  
ELSE IF (@ProductType = 2)   
BEGIN  
         
  SELECT     
   DISTINCT
   A.SKUInventoryID,    
   A.SKU,  
   A.QuantityOnHand,     
   A.ReOrderLevel     
  FROM ZNodeSKUInventory A 
  INNER JOIN ZNodeSKU B ON A.SKU = B.SKU
  INNER JOIN ZNodeProductCategory C ON C.ProductID = B.ProductID 
  INNER JOIN ZNodeCategoryNode D ON D.CategoryID = C.CategoryID
  INNER JOIN ZNodePortalCatalog E ON E.CatalogID = D.CatalogID 
  WHERE PortalID IN (SELECT Value FROM SC_Splitter(@PortalID, ',')) OR LEN(@PortalID) = 0
END  
ELSE IF (@ProductType = 3)   
BEGIN  
          
 SELECT     
    DISTINCT
    Y.Name    
   FROM ZNodeAddOnValue Y 
   INNER JOIN ZNodeAddOn A ON A.AddOnID = Y.AddOnID
   INNER JOIN ZNodeProductAddOn B ON A.AddOnID = B.AddOnID
   INNER JOIN ZNodeProductCategory C ON C.ProductID = B.ProductID 
   INNER JOIN ZNodeCategoryNode D ON D.CategoryID = C.CategoryID
   INNER JOIN ZNodePortalCatalog E ON E.CatalogID = D.CatalogID 
   WHERE E.PortalID IN (SELECT Value FROM SC_Splitter(@PortalID, ',')) OR LEN(@PortalID) = 0
END    
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_CustomerSearch]'
GO
-- =================================================  
-- Create date: Jun 1, 2009    
-- Description: Returns list of Customers  
-- ================================================  
ALTER PROCEDURE [dbo].[ZNode_CustomerSearch]
@FIRSTNAME VARCHAR (MAX)='', @LASTNAME VARCHAR (MAX)='', @COMPANYNAME VARCHAR (MAX)='', @POSTALCODE VARCHAR (MAX)='', @LOGINNAME VARCHAR (MAX)='', @ORDERID INT=0, @PORTALID INT=0, @PortalIDs VARCHAR (MAX)='0'
AS
BEGIN
    -- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM
    -- INTERFERING WITH SELECT STATEMENTS.        
    SET NOCOUNT ON;
    SELECT   DISTINCT (ZA.AccountID),
                      A.LastName AS BillingLastName,
                      A.FirstName AS BillingFirstName,
                      A.CompanyName AS BillingCompanyName,
                      A.Street AS BillingStreet,
                      A.Street1 AS BillingStreet1,
                      A.City AS BillingCity,
                      A.StateCode AS BillingStateCode,
                      A.PostalCode AS BillingPostalCode,
                      A.PhoneNumber AS BillingPhoneNumber,
                      ZA.UserID,
                      ISNULL((SELECT UserName
                              FROM   aspnet_users
                              WHERE  Userid = ZA.UserID), '') AS Username
    FROM     ZNodeAccount AS ZA
             LEFT OUTER JOIN
             ZNodeAddress AS A
             ON ZA.AccountID = A.AccountID
                AND A.IsDefaultBilling = 1
    WHERE    EXISTS (SELECT USERID
                     FROM   Aspnet_Profile
                     WHERE  CAST (PropertyValuesString AS NVARCHAR (1000)) IN (SELECT Value
                                                                               FROM   SC_Splitter (@PortalIDs + ',AllStores', ',')))
             AND (ZA.AccountID IN (SELECT AccountID
                                   FROM   ZNodeAccountProfile
                                   WHERE  ProfileID IN (SELECT ProfileID
                                                        FROM   ZNodePortalProfile
                                                        WHERE  PortalID = @PortalID)))
             AND (((A.FirstName LIKE '%' + @FirstName + '%')
                   OR LEN(@FirstName) = 0)
                  AND ((A.LastName LIKE '%' + @LastName + '%')
                       OR LEN(@LastName) = 0)
                  AND ((ZA.CompanyName LIKE '%' + @CompanyName + '%')
                       OR (A.CompanyName LIKE '%' + @CompanyName + '%')
                       OR (A.CompanyName LIKE '%' + @CompanyName + '%')
                       OR LEN(@CompanyName) = 0)
                  AND ((A.PostalCode LIKE '%' + @POSTALCODE + '%')
                       OR LEN(@POSTALCODE) = 0)
                  AND ((ZA.UserID IN (SELECT USERID
                                      FROM   ASPNET_USERS
                                      WHERE  USERNAME LIKE '%' + @LOGINNAME + '%')
                        OR LEN(@LOGINNAME) = 0))
                  AND ((ZA.AccountID IN (SELECT AccountID
                                         FROM   ZNodeOrder
                                         WHERE  ORDERID = @ORDERID))
                       OR @OrderID = 0))
    ORDER BY ZA.ACCOUNTID DESC;
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductList]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetProductList]   
AS        
 BEGIN              
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
  
SELECT ProductId ,Name ,ShortDescription ,ZNodeProduct.ProductNum ,ProductTypeId ,RetailPrice ,SalePrice ,WholeSalePrice ,
    ImageFile ,Weight ,ActiveInd ,DisplayOrder ,CallForPricing ,HomePageSpecial ,CategorySpecial ,ManufacturerId ,SEOTitle ,  
    SEOKeywords,SEODescription ,Keywords ,AllowBackOrder ,BackOrderMsg ,DropShipInd ,InStockMsg ,OutofStockMsg ,SEOURL FROM ZNodeProduct  
    
END  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetAttributeTypesByProductTypeID]'
GO

-- =============================================      
-- Description: Returns Attibute Types for this product type      
-- =============================================      
CREATE PROCEDURE [dbo].[ZNode_GetAttributeTypesByProductTypeID]      
 @PRODUCTTYPEID  int      
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    -- get attributes      

SELECT * FROM [ZNodeProductTypeAttribute],[ZNodeAttributeType]
where [ZNodeAttributeType].AttributeTypeID = [ZNodeProductTypeAttribute].AttributeTypeID AND
producttypeid= @PRODUCTTYPEID    
   
END     

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductsByCatalogID]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetProductsByCatalogID] 
(@CatalogId int)    
As     
Begin    
SELECT [ProductID]  
      ,[Name]
      ,[ShortDescription]  
      ,[Description]  
      ,[FeaturesDesc]  
      ,[ProductNum]  
      ,[ProductTypeID]  
      ,[RetailPrice]  
      ,[SalePrice]  
      ,[WholesalePrice]  
      ,[ImageFile]  
      ,[ImageAltTag]  
      ,[Weight]  
      ,[Length]  
      ,[Width]  
      ,[Height]  
      ,[BeginActiveDate]  
      ,[EndActiveDate]  
      ,[DisplayOrder]  
      ,[ActiveInd]  
      ,[CallForPricing]  
      ,[HomepageSpecial]  
      ,[CategorySpecial]  
      ,[InventoryDisplay]  
      ,[Keywords]  
      ,[ManufacturerID]  
      ,[AdditionalInfoLink]  
      ,[AdditionalInfoLinkLabel]  
      ,[ShippingRuleTypeID]  
      ,[SEOTitle]  
      ,[SEOKeywords]  
      ,[SEODescription]  
      ,[Custom1]  
      ,[Custom2]  
      ,[Custom3]  
      ,[ShipEachItemSeparately]      
      ,[AllowBackOrder]  
      ,[BackOrderMsg]  
      ,[DropShipInd]  
      ,[DropShipEmailID]  
      ,[Specifications]  
      ,[AdditionalInformation]  
      ,[InStockMsg]  
      ,[OutOfStockMsg]  
      ,[TrackInventoryInd]  
      ,[DownloadLink]  
      ,[FreeShippingInd]  
      ,[NewProductInd]  
      ,[SEOURL]  
      ,[MaxQty]  
      ,[ShipSeparately]  
      ,[FeaturedInd]  
      ,[WebServiceDownloadDte]  
      ,[UpdateDte]  
      ,[SupplierID]  
      ,[RecurringBillingInd]  
      ,[RecurringBillingInstallmentInd]  
      ,[RecurringBillingPeriod]  
      ,[RecurringBillingFrequency]  
      ,[RecurringBillingTotalCycles]  
      ,[RecurringBillingInitialAmount]  
      ,[TaxClassID] 
      ,[PortalID],
      [ReviewStateID],
       [Franchisable]
  FROM [ZNodeProduct]  
 WHERE ProductID in    
  (SELECT ProductID   
     FROM ZNodeProductCategory   
    WHERE CategoryID in    
    (SELECT CategoryID   
       FROM ZNodeCategoryNode   
      WHERE CatalogID = @CatalogId)) 
End       
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportVendorProductRevenue]'
GO


  

ALTER PROCEDURE [dbo].[ZNode_ReportVendorProductRevenue]  
    (  
      @FromDate DATETIME = NULL,  
      @ToDate DATETIME = NULL,  
      @PortalId VARCHAR(100) = '0'  
    )  
AS   
    BEGIN                        
      
  -- SET TODAY IF NO DATE CONSTRAINT PASSED  
  IF (@FromDate IS NULL) SET @FromDate = GETDATE()  
  IF (@ToDate IS NULL) SET @ToDate = DATEADD(D,1,GETDATE())  
    
  DECLARE @TempProduct TABLE (ProductID INT, SKU NVARCHAR(MAX), PortalID INT)  
    
  -- SELECT PRODUCTS WITH PORTALID ASSOCIATED        
  INSERT INTO @TempProduct  
        SELECT ZS.ProductID, ZS.SKU, ZC.PortalID   
  FROM ZNodeSKU ZS  
  INNER JOIN ZNodeProductCategory ZPC ON ZPC.ProductID = ZS.ProductID   
  INNER JOIN ZNodeCategory ZC ON ZC.CategoryID = ZPC.CategoryID  
  WHERE SKU IN (SELECT SKU FROM ZNodeOrderLineItem)  
  AND ZS.ProductID NOT IN (SELECT ParentProductId FROM ZNodeParentChildProduct)
  AND ZC.PortalID IN (SELECT Value FROM SC_Splitter(@PortalId, ',')) OR @PortalId = '0'    
      
  -- SELECT ORDERS COUNT, TOTAL SALES GROUPED BY ORDER PORTAL AND PRODUCTS PORTAL  
    
  DECLARE @TempOrders TABLE (TotalOrders INT, Sales MONEY, PortalID INT, ProductID INT, ProductName NVARCHAR(MAX))  
    
  INSERT INTO @TempOrders       
  SELECT   
   TotalOrders = COUNT(ZO.OrderID),  
   Sales = SUM(QUANTITY * PRICE),  
   ZPD.PortalID,  
   TP.ProductID,  
   ZPD.Name  
  FROM      
   ZNodeOrder ZO  
  INNER JOIN   
   ZNodeOrderLineItem ZOL ON ZOL.OrderID = ZO.OrderID  
  INNER JOIN   
   @TempProduct TP ON TP.SKU = ZOL.SKU AND ZO.PortalID = TP.PortalID  
  INNER JOIN   
   ProductsView ZPD ON TP.ProductID = ZPD.ProductID  
  WHERE (ZO.PortalID IN (SELECT Value FROM SC_Splitter(@PortalId, ',')) OR @PortalId = '0')  
   AND ZO.OrderDate >= CAST(CONVERT(VARCHAR(10),@FromDate, 101) AS DATETIME)  
   AND ZO.OrderDate <= CAST(CONVERT(VARCHAR(10),@ToDate, 101) AS DATETIME)   
   AND ZOL.SKU NOT IN (Select SKU From ZNodeSKU WHERE ProductID IN (SELECT ParentProductId FROM ZNodeParentChildProduct))  
  GROUP BY   
   ZPD.PortalID,  
   ZO.PortalId,  
   TP.ProductID,  
   ZPD.Name
    SELECT * FROM @TempOrders
  -- SELECT ORDERS COUNT, TOTAL SALES GROUPED BY PORTAL   
  SELECT   
   ZP.PortalID,    
   ZP.CompanyName,  
   SUM(TotalOrders) TotalOrders,  
   SUM(Sales) Sales,  
   ZO.ProductID,  
   ZO.ProductName        
  FROM      
   @TempOrders ZO  
  INNER JOIN  
   ZNodePortal ZP ON ZP.PortalID = ZO.PortalId  
      
  GROUP BY   
   ZP.PortalID,  
   ZP.CompanyName,    
   ZO.ProductID,  
   ZO.ProductName  
  
  UNION ALL     
     
  -- SELECT ORDERS COUNT, TOTAL SALES GROUPED BY PORTAL   
  SELECT   
   0 PortalID,    
   'Global Sales' CompanyName,  
   SUM(TotalOrders) TotalOrders,  
   SUM(Sales) Sales,  
   ZO.ProductID,  
   ZO.ProductName        
  FROM      
   @TempOrders ZO  
  WHERE ZO.PortalID IS NULL      
  GROUP BY      
   ZO.ProductID,  
   ZO.ProductName     
                                  
    END  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportsPopularFiltered]'
GO

  
    
ALTER PROCEDURE [dbo].[ZNode_ReportsPopularFiltered]    
    (    
      @FromDate DateTime = NULL,    
      @ToDate DateTime = NULL,    
      @PortalId VARCHAR(100)    
    )    
AS     
    BEGIN                                          
        SET NOCOUNT ON ;      
        SELECT  p.StoreName AS 'StoreName',    
                Item.SKU,    
                Item.Name,    
                SUM(Item.Quantity) AS 'Quantity',    
                Item.Price,    
                Item.ProductNum    
        FROM    ZNodeOrderlineitem Item    
                INNER JOIN ZnodeOrder O ON O.OrderID = Item.OrderId    
                INNER JOIN ZNodePortal P ON p.PortalID = O.PortalId    
        WHERE   O.OrderDate >= @FromDate    
                AND O.OrderDate <= DateAdd(mi,1,@ToDate)  
                AND (P.PortalID = @PortalId OR @PortalId = '0')    
                AND Item.SKU NOT IN (Select SKU From ZNodeSKU WHERE ProductID IN (SELECT ParentProductId FROM ZNodeParentChildProduct))
        GROUP BY P.StoreName,    
                Item.SKU,    
                Item.Price,    
                Item.Name,    
                Item.ProductNum    
        HAVING  SUM(Item.Quantity) > 0     
            
    END    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_Search_Addons]'
GO

ALTER PROCEDURE [dbo].[ZNode_Search_Addons]  
@NAME VARCHAR (MAX), @TITLE VARCHAR (MAX), @SKUPRODUCT VARCHAR (MAX), @LocaleId INT=NULL, @AccountId INT=NULL, @PortalID int=NULL  
AS  
BEGIN  
    SET NOCOUNT ON;  
    DECLARE @ADDONTITLE AS VARCHAR (MAX);  
    DECLARE @ADDONNAME AS VARCHAR (MAX);  
    IF (LEN(@NAME) = 0)  
        BEGIN  
            SET @ADDONNAME = '%';  
        END  
    ELSE  
        BEGIN  
            SET @ADDONNAME = '%' + @NAME + '%';  
        END  
    IF (LEN(@TITLE) = 0)  
        BEGIN  
            SET @ADDONTITLE = '%';  
        END  
    ELSE  
        BEGIN  
            SET @ADDONTITLE = '%' + @TITLE + '%';  
        END  
    IF (LEN(@LocaleId) = 0  
        OR @LocaleId = 0)  
        SET @LocaleId = NULL;  
    IF (LEN(@AccountId) = 0)  
        SET @AccountId = NULL;  
    CREATE TABLE #TEMP_ZNODEADDONS  
    (  
        ADDONID INT  
    );  
    INSERT INTO #TEMP_ZNODEADDONS (ADDONID)  
    (SELECT ADDONID  
     FROM   [ZNodeAddOnValue]  
     WHERE  SKU LIKE '%' + @SKUPRODUCT + '%');  
    INSERT INTO #TEMP_ZNODEADDONS (ADDONID)  
    ((SELECT ADDONID  
      FROM   ZNODEPRODUCTADDON  
      WHERE  PRODUCTID IN (SELECT PRODUCTID  
                           FROM   ZNODEPRODUCT  
                           WHERE  PRODUCTNUM LIKE '%' + @SKUPRODUCT + '%')));  
    IF LEN(@SKUPRODUCT) <> 0  
        BEGIN  
            SELECT ADDONID,  
                   NAME,  
                   TITLE,  
                   DESCRIPTION,  
                   DISPLAYORDER,  
                   OPTIONALIND,  
                   PortalID  
            FROM   ZNODEADDON  
            WHERE  NAME LIKE @ADDONNAME  
                   AND (AccountID IN (SELECT value  
                                      FROM   sc_splitter (@AccountId, ',')) OR AccountID IS NULL AND (PortalID = @PortalID OR (PortalID IS NULL AND (@PortalID IS NULL OR @PortalID = '0'))))
                   AND TITLE LIKE @ADDONTITLE  
                   AND ADDONID IN (SELECT ADDONID  
                                   FROM   #TEMP_ZNODEADDONS)  
                   AND LocaleId = COALESCE (@LocaleId, LocaleId);  
        END  
    ELSE  
        BEGIN  
            SELECT ADDONID,  
                   NAME,  
                   TITLE,  
                   DESCRIPTION,  
                   DISPLAYORDER,  
                   OPTIONALIND,  
                   PortalID  
            FROM   ZNODEADDON  
            WHERE  NAME LIKE @ADDONNAME  
                   AND (AccountID IN (SELECT value  
                                      FROM   sc_splitter (@AccountId, ',')) OR AccountID IS NULL AND (PortalID = @PortalID OR (PortalID IS NULL AND (@PortalID IS NULL OR @PortalID = '0'))))  
                   AND TITLE LIKE @ADDONTITLE  
                   AND LocaleId = COALESCE (@LocaleId, LocaleId);  
        END  
END  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchAffiliate]'
GO

-- =============================================                  
-- Create date: Oct 16,2008      
-- Create date: April 20,2010
-- Description: Returns the Affialiate info        
-- =============================================            
ALTER PROCEDURE  [dbo].[ZNode_SearchAffiliate]                   
(                  
 -- ADD THE PARAMETERS FOR THE STORED PROCEDURE HERE             
 @FirstName VARCHAR(MAX) = NULL,              
 @LastName VARCHAR(MAX) = NULL,              
 @CompanyName VARCHAR(MAX) = NULL,              
 @AccountId VARCHAR(100) = NULL,    
 @Status VARCHAR(MAX) = NULL      
)
AS                  
BEGIN                  
	-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM                  
	-- INTERFERING WITH SELECT STATEMENTS.                  
	SET NOCOUNT ON;
	
	SELECT   Account.AccountId,
			 A.CompanyName,
			 A.FirstName AS BillingFirstName,
			 A.LastName AS BillingLastName
	FROM     ZNodeAccount AS Account
			 LEFT OUTER JOIN
			 ZNodeAddress AS A
			 ON Account.AccountID = A.AccountID
				AND A.IsDefaultBilling = 1
	WHERE    (ReferralStatus = 'N'
			  OR ReferralStatus = 'A')
			 AND ((FirstName LIKE '%' + @FirstName + '%')
				  OR LEN(@FirstName) = 0)
			 AND ((LastName LIKE '%' + @LastName + '%')
				  OR LEN(@LastName) = 0)
			 AND ((Account.CompanyName LIKE '%' + @CompanyName + '%')
				  OR (A.CompanyName LIKE '%' + @CompanyName + '%')
				  OR LEN(@CompanyName) = 0)
			 AND (ReferralStatus LIKE '%' + @Status + '%'
				  OR LEN(@Status) = 0)
			 AND ((A.AccountID LIKE '%' + @AccountId + '%')
				  OR LEN(@AccountId) = 0)
	ORDER BY [AccountID] DESC;
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportsSupplierList]'
GO



ALTER PROCEDURE [dbo].[ZNode_ReportsSupplierList]
    (
      @FromDate DateTime = NULL,
      @ToDate DateTime = NULL,
      @PortalId VARCHAR(10),
      @SupplierId int	
    )
AS 
    BEGIN                                      
        SET NOCOUNT ON ;                           

		CREATE TABLE #SupplierTable (SKU NVARCHAR(MAX), SupplierID INT)
		-- Fetch only SupplierId assigned items (Product, Sku, AddOnValue)
		
		
			INSERT INTO #SupplierTable
			SELECT SKU, SupplierID			 
				 FROM   ZNodeSku
				 WHERE  SupplierId = @SupplierId OR @SupplierId = 0
			UNION
			SELECT SKU, SupplierId
				 FROM   ZNodeAddOnValue
				 Where  SupplierID = @SupplierId OR @SupplierId = 0
		
		
		CREATE TABLE #OrderLineItem (ProductNum NVARCHAR(MAX), SKU NVARCHAR(MAX), OrderID INT, OrderLineItemID INT)
										 
		INSERT INTO #OrderLineItem										 
		SELECT ProductNum, SKU, OrderID, OrderLineItemID 		
		FROM ZNodeOrderLineItem OL

		CREATE TABLE #OrderLineItemTable (ProductNum NVARCHAR(MAX), SKU NVARCHAR(MAX), OrderID INT, OrderLineItemID INT)
		
		INSERT INTO #OrderLineItemTable
		SELECT ProductNum, Sku, OrderID, OrderLineItemID 		
		FROM #OrderLineItem OL
		WHERE OL.Sku IN (SELECT Sku
								FROM #SupplierTable)

		SELECT  DISTINCT O.[OrderID],
                O.OrderDate,
                O.[ShipFirstName],
                O.[ShipLastName],
                O.[ShipCompanyName],
                O.[ShipStreet],
                O.[ShipStreet1],
                O.[ShipCity],
                O.[ShipStateCode],
                O.[ShipPostalCode],
                O.[ShipCountry],
                O.[ShipPhoneNumber],
                P.[StoreName] AS 'StoreName',
                ( SELECT TOP 1
                            SU.[Name]
                  FROM      ZNodeSupplier SU
                  WHERE     SU.SupplierId = S.SupplierID
                ) AS 'Custom1',
                ISNULL(S.SupplierID,-1) AS SupplierID
        FROM ZNodeOrder O
        INNER JOIN #OrderLineItemTable OL ON O.OrderID = OL.OrderID         
        INNER JOIN ZNodePortal P ON P.PortalId = O.PortalId
        INNER JOIN #SupplierTable S ON S.Sku = OL.Sku         
        WHERE   (P.PortalID = @PortalId OR @PortalId = '0')
                AND O.OrderDate BETWEEN @FromDate AND @ToDate                
                ORDER BY O.OrderID DESC

                
		-- Select Order Line Item                
        SELECT  Ol.OrderId,
                Ol.SKU,
                Ol.Quantity,
                Ol.Name,
                Ol.Price,
                ISNULL(S.SupplierID,-1) AS SupplierID
        FROM    ZNodeOrderLineItem Ol            
        INNER JOIN #SupplierTable S ON S.Sku = OL.Sku         
        WHERE   Ol.OrderLineItemID IN (
                SELECT  O.[OrderLineItemID]
                FROM    #OrderLineItemTable O)
        ORDER BY Ol.OrderID DESC
    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCartLineItem_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeSavedCartLineItem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCartLineItem_Get_List

AS


				
				SELECT
					[SavedCartLineItemID],
					[SavedCartID],
					[SKUID],
					[Quantity],
					[ParentSavedCartLineItemID],
					[OrderLineItemRelationshipTypeID]
				FROM
					[dbo].[ZNodeSavedCartLineItem]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportsReOrder]'
GO
SET ANSI_NULLS ON
GO


ALTER PROCEDURE [dbo].[ZNode_ReportsReOrder]
    @PortalId VARCHAR(10) = ''
AS 
    BEGIN                                      
        SET NOCOUNT ON ;

        SELECT DISTINCT                                
                S.SKU AS 'SKU',
                Si.QuantityOnHand AS 'Quantity',
                Si.reorderlevel AS 'ReOrderLevel',
                Pr.ProductNum AS 'ProductNUM',
                Pr.Name AS 'Description',
                'SKU' AS 'Re-Order levelFrom'
        FROM    ZNodeProduct Pr
                INNER JOIN ZNodeSKU S ON S.ProductID = Pr.ProductID
                INNER JOIN ZNodeSKUInventory Si ON Si.SKU = S.SKU
                INNER JOIN ZNodeProductCategory Pc ON Pc.ProductID = Pr.ProductID
                INNER JOIN ZNodeCategoryNode Cn ON Cn.CategoryID = Pc.CategoryID
                INNER JOIN ZNodePortalCatalog Pca ON Pca.CatalogID = Cn.CatalogID
                INNER JOIN ZNodePortal P ON P.PortalID = Pca.PortalId
        WHERE   (P.PortalID = @PortalId OR @PortalId = '0')
                AND COALESCE(Si.QuantityOnHand, 0) <= COALESCE(Si.ReOrderLevel,0)
                AND S.ProductID NOT IN (SELECT ParentProductId FROM ZNodeParentChildProduct) 
        UNION
        SELECT                 
                AV.Sku AS 'SKU',
                Avi.QuantityOnHand AS 'Quantity',
                Avi.ReOrderLevel as 'ReOrderLevel',
                '' AS 'ProductNUM',
                AV.[name] AS 'Description',
                'Addon' AS 'Re-OrderlevelFrom'
        FROM    ZNodeAddOnValue AV
                INNER JOIN ZNodeSKUInventory Avi ON Avi.SKU = AV.SKU
        WHERE   COALESCE(Avi.QuantityOnHand, 0) <= COALESCE(Avi.ReOrderLevel,0)
    END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportsAccounts]'
GO


ALTER PROCEDURE [dbo].[ZNode_ReportsAccounts]
@FromDate DATETIME=NULL, 
@ToDate DATETIME=NULL, 
@PortalId VARCHAR (10)='0'
AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        WITH     AccountList (AccountId, FirstName, LastName, ExternalAccountNo, CompanyName, Email, ProfileName)
        AS       (SELECT A.AccountID,
                         ISNULL(ZA.FirstName, '') AS FirstName,
                         ISNULL(ZA.LastName, '') AS LastName,
                         A.ExternalAccountNo,
                         ZA.CompanyName,
                         A.Email,
                         (SELECT   P.Name + ','
                          FROM     ZNodeProfile AS P
                                   INNER JOIN
                                   ZNodeAccountProfile AS AP
                                   ON AP.AccountID = A.AccountID
                                      AND P.ProfileID = AP.ProfileID
                                      AND AP.ProfileID = P.ProfileID
                          ORDER BY P.Name
                          FOR      XML PATH ('')) AS ProfileName 
                  FROM   ZNodeAccount AS A
                         LEFT OUTER JOIN
                         ZNodeAddress AS ZA
                         ON A.AccountID = ZA.AccountID
                            AND ZA.IsDefaultBilling = 1
                  WHERE  A.CreateDte BETWEEN @FromDate AND @ToDate
                         AND A.AccountID IN (SELECT DISTINCT AP.AccountID
                                             FROM   ZNodeProfile AS P
                                                    INNER JOIN
                                                    ZNodeAccountProfile AS AP
                                                    ON P.ProfileID = AP.ProfileID
                                                    INNER JOIN
                                                    ZNodePortalProfile AS PP
                                                    ON AP.ProfileID = PP.ProfileID
                                                       AND (PP.PortalID = @PortalId
                                                       OR @PortalId = '0')))
        SELECT   AccountID,
                 FirstName AS BillingFirstName,
                 LastName AS BillingLastName,
                 ExternalAccountNo,
                 CompanyName AS BillingCompanyName,
                 Email AS BillingEmailID,
                 CASE 
WHEN (ProfileName IS NOT NULL
                            AND LEN(ProfileName) > 1) THEN LEFT(ProfileName, LEN(ProfileName) - 1) ELSE ProfileName 
END AS ProfileName
        FROM     AccountList AS A
        ORDER BY A.AccountID DESC;
    END
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportsOptIn]'
GO



ALTER PROCEDURE [dbo].[ZNode_ReportsOptIn]
    @PortalId VARCHAR(10) = ''
AS 
    BEGIN                                      
        SET NOCOUNT ON ;  
        
        SELECT  A.[AccountID],
                Adrs.FirstName AS [BillingFirstName],
                Adrs.LastName AS [BillingLastName],
                Adrs.CompanyName AS [BillingCompanyName],
                A.Email AS [BillingEmailID],
                P.StoreName
        FROM    ZNodeAccount A
                INNER JOIN ZNodeAccountProfile Zap ON A.AccountId = Zap.AccountId
                INNER JOIN ZNodePortalProfile Zpp ON Zpp.ProfileID = A.ProfileID
                INNER JOIN ZNodePortal P ON P.PortalId = Zpp.PortalId
                LEFT OUTER JOIN ZNodeAddress Adrs ON (A.AccountId = Adrs.AccountID AND Adrs.IsDefaultBilling=1)                
        WHERE   A.EmailOptIn = 1
                AND (P.PortalID = @PortalId OR @PortalId = '0');
		
	                                
    END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeOrderLineItem table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_Get_List

AS


				
				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3]
				FROM
					[dbo].[ZNodeOrderLineItem]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductByProductID]'
GO
SET ANSI_NULLS ON
GO

  
--[ZNode_GetProductByProductID] 302  
ALTER PROCEDURE [dbo].[ZNode_GetProductByProductID]                        
 -- Add the parameters for the stored procedure here                            
 @ProductId int = 0  
AS                            
BEGIN                            
 -- SET NOCOUNT ON added to prevent extra result sets from  
  -- interfering with SELECT statements.  
        SET NOCOUNT ON ;  
        
        -- Get SiteAdmin PortalID, if not franchise product
        DECLARE @OwnPortalID INT
        SELECT @OwnPortalID = ISNULL(A.PortalID,0) FROM ZNodePortal A
			WHERE A.PortalID NOT IN (SELECT ISNULL(PortalID, 0) FROM ZNodeCatalog)
			AND PortalID IN 
			(SELECT PortalID FROM ZNodePortalCatalog WHERE CatalogID IN
			(SELECT ZC.CatalogID FROM ZNodeCatalog ZC
			INNER JOIN ZNodeCategoryNode ZCN ON ZC.CatalogID = ZCN.CatalogID
			INNER JOIN ZNodeProductCategory ZPC ON ZPC.CategoryID = ZCN.CategoryID
			WHERE ZPC.ProductID = @ProductID))

    
        CREATE TABLE #PRODUCTCATEGORYLIST
            (
              ProductId int,
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )
                WITH ( IGNORE_DUP_KEY = ON ) 
            )
    
		
		SELECT  ZNodeProductCrossSell.RelatedProductID AS ProductID
				INTO #CrossSellItem
		FROM	ZNodeProductCrossSell ZNodeProductCrossSell
		INNER JOIN ZnodeProduct ZP ON ZNodeProductCrossSell.RelatedProductID = ZP.ProductID
		WHERE	ZNodeProductCrossSell.ProductID = @ProductId
			

		IF ((SELECT COUNT(1) FROM #CrossSellItem) > 0)
		BEGIN
			INSERT  INTO #PRODUCTCATEGORYLIST ( ProductId )
                SELECT  ZPC.ProductID
                FROM 
                        ZNodeProductCategory ZPC
                        INNER JOIN ZNodeCategoryNode ZCN ON ZPC.CategoryID = ZCN.CategoryID
                        INNER JOIN ZNodePortalCatalog PC ON ZCN.CatalogID = PC.CatalogID
                WHERE
						ZPC.ProductID IN  (SELECT ProductID FROM #CrossSellItem)
						AND	ZCN.ActiveInd = 1
                        AND ZPC.ActiveInd = 1
                      
		END;
		
        SELECT  [ProductID],  
                [Name],  
                [ShortDescription],  
                [Description],  
                [FeaturesDesc],  
                [ProductNum],  
                [ProductTypeID],  
                [RetailPrice],  
                [SalePrice],  
                [WholesalePrice],  
                [ImageFile],  
                [ImageAltTag],  
                [Weight],  
                [Length],  
                [Width],  
                [Height],  
                [BeginActiveDate],  
                [EndActiveDate],  
                [DisplayOrder],  
                [ActiveInd],  
                [CallForPricing],  
                [HomepageSpecial],  
                [CategorySpecial],  
                [InventoryDisplay],  
                [Keywords],  
                [ManufacturerID],  
                [AdditionalInfoLink],  
                [AdditionalInfoLinkLabel],  
                [ShippingRuleTypeID],  
                [ShippingRate],  
                [SEOTitle],  
                [SEOKeywords],  
                [SEODescription],  
                [Custom1],  
                [Custom2],  
                [Custom3],  
                [ShipEachItemSeparately],  
                [AllowBackOrder],  
                [BackOrderMsg],  
                [DropShipInd],  
                [DropShipEmailID],  
                [Specifications],  
                [AdditionalInformation],  
                [InStockMsg],  
                [OutOfStockMsg],  
                [TrackInventoryInd],  
                [DownloadLink],  
                [FreeShippingInd],  
                [NewProductInd],  
                [SEOURL],  
                [MinQty],  
                [MaxQty],  
                [ShipSeparately],  
                [FeaturedInd],  
                [WebServiceDownloadDte],  
                [UpdateDte],  
                [SupplierID],  
                [RecurringBillingInd],  
                [RecurringBillingInstallmentInd],  
                [RecurringBillingPeriod],  
                [RecurringBillingFrequency],  
                [RecurringBillingTotalCycles],  
                [RecurringBillingInitialAmount],  
                [TaxClassID],  
                ISNULL([PortalID], @OwnPortalID),  
                [AffiliateUrl],  
                (SELECT TOP 1 sku.SKU, QuantityOnHand, ExternalProductID  
          FROM ZNodeSKU sku  
           INNER JOIN ZNodeSKUInventory SkuInventory   
          ON sku.SKU = SkuInventory.SKU  
          WHERE   
           EXISTS  
           (SELECT 1 FROM ZNodeSKUAttribute  
            WHERE SKUID = sku.SKUID having COUNT(1) = 0)  
            AND sku.ProductID = @ProductID  
              
          FOR XML PATH(''),TYPE  
        ),               
                ( Select    ZNodeAttributeType.[AttributeTypeId],  
                            ZNodeAttributeType.[Name],  
                            ZNodeAttributeType.[Description],  
                            ZNodeAttributeType.[DisplayOrder],  
                            [IsPrivate],  
                            [LocaleId],  
                            ( SELECT  
        Distinct ( ZNodeAttribute.AttributeId ),  
                                        ZNodeAttribute.Name,  
                                        ZNodeAttribute.DisplayOrder  
                              FROM      ZNodeskuattribute skua  
                                        INNER JOIN ZNodeProductattribute ZNodeAttribute ON skua.attributeid = ZNodeAttribute.attributeid  
                              WHERE     ZNodeAttribute.attributetypeid = ZNodeAttributeType.attributetypeid  
                 and ZNodeAttribute.IsActive = 1  
                                        and SKUID in (  
                                        SELECT  SKUID  
                                        FROM    ZNodeSKU  
                                        WHERE   ProductID = @ProductId  
                                                and ActiveInd = 1 )  
                              Group By  ZNodeAttribute.AttributeId,  
                                        ZNodeAttribute.Name,  
                                        ZNodeAttribute.DisplayOrder  
                              Order By  ZNodeAttribute.DisplayOrder  
                            FOR  
                              XML AUTO,  
                                  TYPE,  
                                  ELEMENTS  
                            )  
                  FROM      ZNodeAttributeType ZNodeAttributeType  
                            INNER JOIN ZNodeProductTypeAttribute ProductTypeAttribute   
                            ON ZNodeAttributeType.attributetypeid = ProductTypeAttribute.attributetypeid  
                            AND ProductTypeAttribute.ProductTypeId = ZNodeProduct.ProductTypeID  
                  Order By  ZNodeAttributeType.DisplayOrder  
                  FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( select    ZNodeCrossSellItem.ProductId,
                            ZNodeCrossSellItem.Name,
                            ZNodeCrossSellItem.ShortDescription,
                            ZNodeCrossSellItem.ImageFile,
                            ZNodeCrossSellItem.SeoURL,
                            ZNodeCrossSellItem.ImageAltTag,
                            ZNodeCrossSellItem.RetailPrice,
                            ZNodeCrossSellItem.SalePrice,
                            ZNodeCrossSellItem.WholesalePrice,
                            ZNodeCrossSellItem.CallForPricing,
                            ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                                        TotalReviews = IsNull(COUNT(ProductId), 0)
                              FROM      ZNodeReview ZNodeReview
                              WHERE     ZNodeReview.ProductId = ZNodeCrossSellItem.ProductId
                                        AND ZNodeReview.[Status] = 'A'
                            FOR
                              XML PATH(''),
                                  TYPE
                            )
                  FROM     ProductsView ZNodeCrossSellItem
                  WHERE 
							ZNodeCrossSellItem.ProductId IN (
                            SELECT  ProductId
                            from    #PRODUCTCATEGORYLIST )
                            AND ZNodeCrossSellItem.ActiveInd = 1
                                   
                  ORDER BY  ZNodeCrossSellItem.DisplayOrder asc
                  
                FOR  
                  XML AUTO,  
                      TYPE,  
                      ELEMENTS  
                ),  
                ( SELECT    ZNodeAddOn.[AddOnID],  
                            ZNodeAddOn.[ProductID],  
                            [Title],  
                            [Name],  
                            [Description],  
                            [DisplayOrder],  
                            [DisplayType],  
                            [OptionalInd],  
                            [AllowBackOrder],  
                            [InStockMsg],  
                            [OutOfStockMsg],  
                            [BackOrderMsg],  
                            [PromptMsg],  
                            [TrackInventoryInd],  
                            [LocaleId],  
                            ( SELECT Distinct  
                                        ( ZNodeAddonValue.AddOnValueID ),  
                                        ZNodeAddonValue.Name,  
                                        ZNodeAddonValue.DisplayOrder,  
                                        ZNodeAddonValue.RetailPrice as RetailPrice,  
                                        ZNodeAddonValue.SalePrice as SalePrice,  
                                        ZNodeAddonValue.WholeSalePrice as WholesalePrice,  
                                        ZNodeAddonValue.DefaultInd as IsDefault,  
                                        QuantityOnHand = ( SELECT   QuantityOnHand  
                                                           FROM     ZNodeSKUInventory  
                                                           WHERE    SKU = ZNodeAddOnValue.SKU  
                                                         ),  
                                        ZNodeAddonValue.weight as WeightAdditional,  
                                        ZNodeAddonValue.ShippingRuleTypeID,  
                                        ZNodeAddonValue.FreeShippingInd,  
                                        ZNodeAddonValue.SupplierID,  
                                        ZNodeAddonValue.TaxClassID,  
                                        ZNodeAddOnValue.ExternalProductID  
                              FROM      ZNodeAddOnValue ZNodeAddOnValue  
                              WHERE     ZNodeAddOnValue.AddOnId = ZNodeAddOn.AddOnId  
                              Order By  ZNodeAddonValue.DisplayOrder  
                            FOR  
                              XML AUTO,  
                                  TYPE,  
                                  ELEMENTS  
                            )  
                  FROM      ZNodeAddOn ZNodeAddOn  
                            INNER JOIN ZNodeProductAddOn ZNodeProductAddOn ON ZNodeAddOn.AddOnId = ZNodeProductAddOn.AddOnId  
                  WHERE     ZNodeProductAddOn.ProductId = @ProductId  
                  Order By  ZNodeAddon.DisplayOrder  
                FOR  
                  XML AUTO,  
                      TYPE,  
                      ELEMENTS  
                ),  
                ( SELECT    [ProductTierID],  
                            [ProductID],  
                            [ProfileID],  
                            [TierStart],  
                            [TierEnd],  
                            [Price]  
                  FROM      ZNodeProductTier ZNodeProductTier  
                  WHERE     ZNodeProductTier.ProductId = ZNodeProduct.ProductId  
                FOR  
                  XML AUTO,  
                      TYPE,  
                      ELEMENTS  
                ),  
                ( SELECT TOP 1 *  
                  FROM      ZNodeSKU   
                  WHERE     ZNodeSKU.ProductId = ZNodeProduct.ProductId  
                FOR  
                  XML AUTO,  
                      TYPE,  
                      ELEMENTS  
                ),
                (SELECT  [ProductID],
                [Name],
                [ShortDescription],
                [Description],
                [FeaturesDesc],
                [ProductNum],
                [ProductTypeID],
                [RetailPrice],
                [SalePrice],
                [WholesalePrice],
                [ImageFile],
                [ImageAltTag],
                [Weight],
                [Length],
                [Width],
                [Height],
                [BeginActiveDate],
                [EndActiveDate],
                [DisplayOrder],
                [ActiveInd],
                [CallForPricing],
                [HomepageSpecial],
                [CategorySpecial],
                [InventoryDisplay],
                [Keywords],
                [ManufacturerID],
                [AdditionalInfoLink],
                [AdditionalInfoLinkLabel],
                [ShippingRuleTypeID],
                [ShippingRate],
                [SEOTitle],
                [SEOKeywords],
                [SEODescription],
                [Custom1],
                [Custom2],
                [Custom3],
                [ShipEachItemSeparately],
                [AllowBackOrder],
                [BackOrderMsg],
                [DropShipInd],
                [DropShipEmailID],
                [Specifications],
                [AdditionalInformation],
                [InStockMsg],
                [OutOfStockMsg],
                [TrackInventoryInd],
                [DownloadLink],
                [FreeShippingInd],
                [NewProductInd],
                [SEOURL],
                [MinQty],
                [MaxQty],
                [ShipSeparately],
                [FeaturedInd],
                [WebServiceDownloadDte],
                [UpdateDte],
                [SupplierID],
                [RecurringBillingInd],
                [RecurringBillingInstallmentInd],
                [RecurringBillingPeriod],
                [RecurringBillingFrequency],
                [RecurringBillingTotalCycles],
                [RecurringBillingInitialAmount],
                [TaxClassID],
                [PortalID],
                [AffiliateUrl],
                (SELECT TOP 1 sku.SKU, QuantityOnHand,ExternalProductID
										FROM ZNodeSKU sku
											INNER JOIN ZNodeSKUInventory SkuInventory 
										ON sku.SKU = SkuInventory.SKU
										WHERE 
											EXISTS
											(SELECT 1 FROM ZNodeSKUAttribute
												WHERE SKUID = sku.SKUID having COUNT(1) = 0)
												AND	sku.ProductID = ZNodeBundleProduct.ProductID
												
										FOR	XML PATH(''),TYPE
								),
                ManufacturerName = ( Select Name
                                     from   ZNodeManufacturer
                                     where  ManufacturerId = ZNodeBundleProduct.ManufacturerId AND ActiveInd = 1
                                   ),
                                   ( SELECT TOP 1 *  
                  FROM      ZNodeSKU
                  INNER JOIN ZnodeSKUInventory ON ZNodeSKU.SKU = ZNodeSKUInventory.SKU                     
                  WHERE     ZNodeSKU.ProductId = ZNodeBundleProduct.ProductID  
                FOR  
                  XML PATH('ZNodeSKU'),  
                      TYPE,  
                      ELEMENTS  
                )       
                FROM ZNodeProduct ZNodeBundleProduct INNER JOIN ZNodeParentChildProduct B
                ON ZNodeBundleProduct.ProductID = B.ChildProductID WHERE   B.ParentProductID = @ProductId AND ZNodeBundleProduct.ActiveInd = 1 FOR     XML AUTO,
                    TYPE,
                    ELEMENTS )  
        FROM    ZNodeProduct ZNodeProduct         
        WHERE   ZNodeProduct.ProductId = @ProductId  
        FOR     XML AUTO,  
                    TYPE,  
                    ELEMENTS  
    END    
  
  
  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCartLineItem_Insert]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeSavedCartLineItem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCartLineItem_Insert
(

	@SavedCartLineItemID int    OUTPUT,

	@SavedCartID int   ,

	@SKUID int   ,

	@Quantity int   ,

	@ParentSavedCartLineItemID int   ,

	@OrderLineItemRelationshipTypeID int   
)
AS


				
				INSERT INTO [dbo].[ZNodeSavedCartLineItem]
					(
					[SavedCartID]
					,[SKUID]
					,[Quantity]
					,[ParentSavedCartLineItemID]
					,[OrderLineItemRelationshipTypeID]
					)
				VALUES
					(
					@SavedCartID
					,@SKUID
					,@Quantity
					,@ParentSavedCartLineItemID
					,@OrderLineItemRelationshipTypeID
					)
				
				-- Get the identity value
				SET @SavedCartLineItemID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsProductSoldOnVendorSites]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ZNode_ReportsProductSoldOnVendorSites]
    (
      @FromDate DATETIME = NULL,
      @ToDate DATETIME = NULL,
      @PortalId VARCHAR(100) = '0',      
      @AccountId VARCHAR(100) = '0'      
    )
AS    
    BEGIN                              
    
    IF (@FromDate IS NULL) SET @FromDate = GETDATE()
	IF (@ToDate IS NULL) SET @ToDate = DATEADD(D,1,GETDATE())
				
    IF(@AccountId<>'0')
		BEGIN
			-- Mall Admin Product Sold On Vendor Site Reprot
			-- SET TODAY IF NO DATE CONSTRAINT PASSED
				
						
				SELECT 
				A.CompanyName,
				P.Name,
				OT.FromUrl,
				OT.ToUrl,
				OT.Price
			FROM   
				ZNodeAccount AS A 
				INNER JOIN ZNodeProduct AS P ON P.AccountID = A.AccountId 
				INNER JOIN ZNodeTrackingOutbound AS OT ON P.ProductID = OT.ProductID 
			WHERE  
				OT.[Date]>= @FromDate 
				AND OT.[Date]<=@ToDate
				AND (@AccountId='0'  OR  P.AccountID=@AccountId)								
		END
	ELSE
		BEGIN		
			-- Franchise Admin Product Sold On Vendor Site Reprot
			-- SET TODAY IF NO DATE CONSTRAINT PASSED
				SELECT 
				A.CompanyName AS CompanyName,
				P.Name,
				OT.FromUrl,
				OT.ToUrl,
				OT.Price
			FROM  ZNodeProduct AS P
				INNER JOIN ZNodeTrackingOutbound AS OT ON P.ProductID = OT.ProductID 
				INNER JOIN ZNodeProductCategory AS PC ON PC.ProductID = P.ProductID
				INNER JOIN ZNodeCategoryNode AS ZC ON ZC.CategoryID = PC.CategoryID
				INNER JOIN ZNodePortalCatalog AS ZPC ON ZPC.CatalogID = ZC.CatalogID
				LEFT JOIN ZNodeAccount AS A ON P.AccountID = A.AccountId				
			WHERE  
				OT.[Date]>= @FromDate 
				AND OT.[Date]<=@ToDate				
				AND (@PortalId = '0' OR ZPC.PortalID=@PortalId)		
			END
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCartLineItem_Update]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeSavedCartLineItem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCartLineItem_Update
(

	@SavedCartLineItemID int   ,

	@SavedCartID int   ,

	@SKUID int   ,

	@Quantity int   ,

	@ParentSavedCartLineItemID int   ,

	@OrderLineItemRelationshipTypeID int   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeSavedCartLineItem]
				SET
					[SavedCartID] = @SavedCartID
					,[SKUID] = @SKUID
					,[Quantity] = @Quantity
					,[ParentSavedCartLineItemID] = @ParentSavedCartLineItemID
					,[OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID
				WHERE
[SavedCartLineItemID] = @SavedCartLineItemID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeOrderLineItem table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_Insert
(

	@OrderLineItemID int    OUTPUT,

	@OrderID int   ,

	@ShipmentID int   ,

	@ProductNum nvarchar (MAX)  ,

	@Name nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@Quantity int   ,

	@Price money   ,

	@Weight decimal (18, 2)  ,

	@SKU nvarchar (MAX)  ,

	@ParentOrderLineItemID int   ,

	@OrderLineItemRelationshipTypeID int   ,

	@DownloadLink nvarchar (MAX)  ,

	@DiscountAmount money   ,

	@ShipSeparately bit   ,

	@ShipDate datetime   ,

	@ReturnDate datetime   ,

	@ShippingCost money   ,

	@PromoDescription nvarchar (MAX)  ,

	@SalesTax money   ,

	@VAT money   ,

	@GST money   ,

	@PST money   ,

	@HST money   ,

	@TransactionNumber nvarchar (MAX)  ,

	@PaymentStatusID int   ,

	@TrackingNumber nvarchar (MAX)  ,

	@AutoGeneratedTracking bit   ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[ZNodeOrderLineItem]
					(
					[OrderID]
					,[ShipmentID]
					,[ProductNum]
					,[Name]
					,[Description]
					,[Quantity]
					,[Price]
					,[Weight]
					,[SKU]
					,[ParentOrderLineItemID]
					,[OrderLineItemRelationshipTypeID]
					,[DownloadLink]
					,[DiscountAmount]
					,[ShipSeparately]
					,[ShipDate]
					,[ReturnDate]
					,[ShippingCost]
					,[PromoDescription]
					,[SalesTax]
					,[VAT]
					,[GST]
					,[PST]
					,[HST]
					,[TransactionNumber]
					,[PaymentStatusID]
					,[TrackingNumber]
					,[AutoGeneratedTracking]
					,[Custom1]
					,[Custom2]
					,[Custom3]
					)
				VALUES
					(
					@OrderID
					,@ShipmentID
					,@ProductNum
					,@Name
					,@Description
					,@Quantity
					,@Price
					,@Weight
					,@SKU
					,@ParentOrderLineItemID
					,@OrderLineItemRelationshipTypeID
					,@DownloadLink
					,@DiscountAmount
					,@ShipSeparately
					,@ShipDate
					,@ReturnDate
					,@ShippingCost
					,@PromoDescription
					,@SalesTax
					,@VAT
					,@GST
					,@PST
					,@HST
					,@TransactionNumber
					,@PaymentStatusID
					,@TrackingNumber
					,@AutoGeneratedTracking
					,@Custom1
					,@Custom2
					,@Custom3
					)
				
				-- Get the identity value
				SET @OrderLineItemID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportsAffiliateFiltered]'
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[ZNode_ReportsAffiliateFiltered]
    (
      @FromDate DATETIME,
      @ToDate DATETIME,
      @PortalId VARCHAR(100) = ''
    )
AS
    BEGIN
        SET NOCOUNT ON ;
            SELECT ( Adrs.FirstName + ' ' + Adrs.LastName ) As 'Name',
                A.TaxID AS 'TaxID',
                P.StoreName AS 'StoreName',
                COUNT(O.AccountId) AS 'NumberOfOrders',
                SUM(O.Total) AS 'OrderValue',
                R.[Name] AS 'CommissionType',
                RC.ReferralCommission AS 'Commission',
                CASE WHEN RC.ReferralCommissionTypeID = 1
                    THEN SUM(O.Total) * RC.ReferralCommission / 100
                    WHEN RC.ReferralCommissionTypeID = 2
                    THEN RC.ReferralCommission
                END AS 'CommissionOwed',
                RC.ReferralCommissionTypeID AS ReferralCommissionTypeID
            FROM
                  ZNodeAccount A
                  INNER JOIN ZNodeOrder B ON B.ReferralAccountID = A.AccountID
				  INNER JOIN ZNodeReferralCommission RC ON RC.ReferralAccountID=A.AccountID  
                  INNER JOIN ZNodeReferralCommissionType R ON R.ReferralCommissionTypeID = RC.ReferralCommissionTypeID
                  INNER JOIN ZNodeOrder O ON O.AccountID = B.AccountID
                  INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
                  LEFT OUTER JOIN ZNodeAddress Adrs ON A.AccountID=Adrs.AccountID AND Adrs.IsDefaultBilling=1 

            WHERE  A.ReferralStatus = 'A'
              AND o.OrderID=RC.OrderID 
              AND o.OrderDate >= @FromDate
              AND o.OrderDate <= @ToDate
              AND (P.PortalID = @PortalId OR @PortalId = '0')
                  GROUP BY p.StoreName,
                Adrs.FirstName,
                Adrs.LastName,
                A.TaxID,
                r.Name,
                RC.ReferralCommission,
                RC.ReferralCommissionTypeID
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCartLineItem_Delete]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeSavedCartLineItem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCartLineItem_Delete
(

	@SavedCartLineItemID int   
)
AS


				DELETE FROM [dbo].[ZNodeSavedCartLineItem] WITH (ROWLOCK) 
				WHERE
					[SavedCartLineItemID] = @SavedCartLineItemID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeOrderLineItem table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_Update
(

	@OrderLineItemID int   ,

	@OrderID int   ,

	@ShipmentID int   ,

	@ProductNum nvarchar (MAX)  ,

	@Name nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@Quantity int   ,

	@Price money   ,

	@Weight decimal (18, 2)  ,

	@SKU nvarchar (MAX)  ,

	@ParentOrderLineItemID int   ,

	@OrderLineItemRelationshipTypeID int   ,

	@DownloadLink nvarchar (MAX)  ,

	@DiscountAmount money   ,

	@ShipSeparately bit   ,

	@ShipDate datetime   ,

	@ReturnDate datetime   ,

	@ShippingCost money   ,

	@PromoDescription nvarchar (MAX)  ,

	@SalesTax money   ,

	@VAT money   ,

	@GST money   ,

	@PST money   ,

	@HST money   ,

	@TransactionNumber nvarchar (MAX)  ,

	@PaymentStatusID int   ,

	@TrackingNumber nvarchar (MAX)  ,

	@AutoGeneratedTracking bit   ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeOrderLineItem]
				SET
					[OrderID] = @OrderID
					,[ShipmentID] = @ShipmentID
					,[ProductNum] = @ProductNum
					,[Name] = @Name
					,[Description] = @Description
					,[Quantity] = @Quantity
					,[Price] = @Price
					,[Weight] = @Weight
					,[SKU] = @SKU
					,[ParentOrderLineItemID] = @ParentOrderLineItemID
					,[OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID
					,[DownloadLink] = @DownloadLink
					,[DiscountAmount] = @DiscountAmount
					,[ShipSeparately] = @ShipSeparately
					,[ShipDate] = @ShipDate
					,[ReturnDate] = @ReturnDate
					,[ShippingCost] = @ShippingCost
					,[PromoDescription] = @PromoDescription
					,[SalesTax] = @SalesTax
					,[VAT] = @VAT
					,[GST] = @GST
					,[PST] = @PST
					,[HST] = @HST
					,[TransactionNumber] = @TransactionNumber
					,[PaymentStatusID] = @PaymentStatusID
					,[TrackingNumber] = @TrackingNumber
					,[AutoGeneratedTracking] = @AutoGeneratedTracking
					,[Custom1] = @Custom1
					,[Custom2] = @Custom2
					,[Custom3] = @Custom3
				WHERE
[OrderLineItemID] = @OrderLineItemID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCartLineItem_GetByOrderLineItemRelationshipTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeSavedCartLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCartLineItem_GetByOrderLineItemRelationshipTypeID
(

	@OrderLineItemRelationshipTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[SavedCartLineItemID],
					[SavedCartID],
					[SKUID],
					[Quantity],
					[ParentSavedCartLineItemID],
					[OrderLineItemRelationshipTypeID]
				FROM
					[dbo].[ZNodeSavedCartLineItem]
				WHERE
					[OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetAllCustomers]'
GO
SET ANSI_NULLS ON
GO


  
ALTER PROCEDURE [dbo].[ZNode_GetAllCustomers]                
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;                
 -- Insert statements for procedure here         
 SELECT           
   DISTINCT(ZA.[AccountID])    
   ,[ParentAccountID]   
   ,(CAST(ZA.[AccountID] AS VARCHAR(MAX)) + ' - ' + ISNULL(A.FirstName + ' ','') + ISNULL(A.LastName,'')) 'CustomerName'
   ,A.FirstName AS  [BillingFirstName]      
   ,A.LastName AS  [BillingLastName]      
   ,A.PhoneNumber AS  [BillingPhoneNumber]      
   ,ZA.Email AS [BillingEmailID]      
   ,[UserId]  
   ,ZA.ProfileID  
	,[ActiveInd] 
	FROM ZNODEACCOUNT ZA LEFT OUTER JOIN 
	ZNODEACCOUNTPROFILE ZAP ON ZA.ACCOUNTID=ZAP.AccountID
	LEFT OUTER JOIN ZNodeAddress A ON ZA.AccountID=A.AccountID AND A.IsDefaultBilling=1	
    Order By ZA.[AccountID]      
END         
  
  
  


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductByProductID_XML]'
GO
-- ==========================================================   
-- Create date: July 02, 2006
-- Update date: May 26,2010        
-- Description: Retrieve product and return a serialized xml
-- ==========================================================
ALTER PROCEDURE [dbo].[ZNode_GetProductByProductID_XML]
    @ProductId int = 0,
    @LocaleId int = 0,
    @PortalId int = 0
AS 
    BEGIN                                
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;

		-- Get SiteAdmin PortalID, if not franchise product        
        DECLARE @OwnPortalID INT
        SELECT ISNULL(A.PortalID,0) PortalID
			INTO #TmpPortal
			FROM ZNodePortal A
			WHERE A.PortalID NOT IN (SELECT ISNULL(PortalID, 0) FROM ZNodeCatalog)
			AND PortalID IN 
			(SELECT PortalID FROM ZNodePortalCatalog WHERE CatalogID IN
			(SELECT ZC.CatalogID FROM ZNodeCatalog ZC
			INNER JOIN ZNodeCategoryNode ZCN ON ZC.CatalogID = ZCN.CatalogID
			INNER JOIN ZNodeProductCategory ZPC ON ZPC.CategoryID = ZCN.CategoryID
			WHERE ZPC.ProductID = @ProductId))
			
		IF (EXISTS(SELECT PortalID FROM #TmpPortal WHERE PortalID = @PortalID))
		BEGIN
			SET @OwnPortalID = @PortalID
		END
		ELSE
		BEGIN
			SELECT TOP 1 @OwnPortalID = PortalID FROM #TmpPortal
		END

        CREATE TABLE #PRODUCTCATEGORYLIST
            (
              ProductId int,
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )
                WITH ( IGNORE_DUP_KEY = ON ) 
            )
		
		CREATE TABLE #CrossSellItem
            (
              ProductId int,
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )
                WITH ( IGNORE_DUP_KEY = ON ) 
            )
		INSERT  INTO #CrossSellItem ( ProductId )
		SELECT  ZNodeProductCrossSell.RelatedProductID AS ProductID				
		FROM	ZNodeProductCrossSell ZNodeProductCrossSell
		INNER JOIN ZnodeProduct ZP ON ZNodeProductCrossSell.RelatedProductID = ZP.ProductID
		WHERE	ZNodeProductCrossSell.ProductID = @ProductId
				AND ZP.ReviewStateID = 20

		IF ((SELECT COUNT(1) FROM #CrossSellItem) > 0)
		BEGIN
			INSERT  INTO #PRODUCTCATEGORYLIST ( ProductId )
                SELECT  ZPC.ProductID
                FROM 
                        ZNodeProductCategory ZPC
                        INNER JOIN ZNodeCategoryNode ZCN ON ZPC.CategoryID = ZCN.CategoryID
                        INNER JOIN ZNodePortalCatalog PC ON ZCN.CatalogID = PC.CatalogID
                WHERE
						ZPC.ProductID IN  (SELECT ProductID FROM #CrossSellItem)
						AND	ZCN.ActiveInd = 1
                        AND ZPC.ActiveInd = 1
                        AND PC.LocaleID = @LocaleId
                        AND PC.PortalID = @PortalId
		END;
		
										
        SELECT  [ProductID],
                [Name],
                [ShortDescription],
                [Description],
                [FeaturesDesc],
                [ProductNum],
                [ProductTypeID],
                [RetailPrice],
                [SalePrice],
                [WholesalePrice],
                [ImageFile],
                [ImageAltTag],
                [Weight],
                [Length],
                [Width],
                [Height],
                [BeginActiveDate],
                [EndActiveDate],
                [DisplayOrder],
                [ActiveInd],
                [CallForPricing],
                [HomepageSpecial],
                [CategorySpecial],
                [InventoryDisplay],
                [Keywords],
                [ManufacturerID],
                [AdditionalInfoLink],
                [AdditionalInfoLinkLabel],
                [ShippingRuleTypeID],
                [ShippingRate],
                [SEOTitle],
                [SEOKeywords],
                [SEODescription],
                [Custom1],
                [Custom2],
                [Custom3],
                [ShipEachItemSeparately],
                [AllowBackOrder],
                [BackOrderMsg],
                [DropShipInd],
                [DropShipEmailID],
                [Specifications],
                [AdditionalInformation],
                [InStockMsg],
                [OutOfStockMsg],
                [TrackInventoryInd],
                [DownloadLink],
                [FreeShippingInd],
                [NewProductInd],
                [SEOURL],
                [MinQty],
                [MaxQty],
                [ShipSeparately],
                [FeaturedInd],
                [WebServiceDownloadDte],
                [UpdateDte],
                [SupplierID],
                [RecurringBillingInd],
                [RecurringBillingInstallmentInd],
                [RecurringBillingPeriod],
                [RecurringBillingFrequency],
                [RecurringBillingTotalCycles],
                [RecurringBillingInitialAmount],
                [TaxClassID],
                ISNULL([PortalID], @OwnPortalID) PortalID,
                [AffiliateUrl],
                (SELECT TOP 1 sku.SKU, QuantityOnHand,ExternalProductID
										FROM ZNodeSKU sku
											INNER JOIN ZNodeSKUInventory SkuInventory 
										ON sku.SKU = SkuInventory.SKU
										WHERE 
											EXISTS
											(SELECT 1 FROM ZNodeSKUAttribute
												WHERE SKUID = sku.SKUID having COUNT(1) = 0)
												AND	sku.ProductID = @ProductID
												
										FOR	XML PATH(''),TYPE
								),
                ManufacturerName = ( Select Name
                                     from   ZNodeManufacturer
                                     where  ManufacturerId = ZNodeProduct.ManufacturerId AND ActiveInd = 1
                                   ),
                ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                            TotalReviews = IsNull(COUNT(ProductId), 0)
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.Status = 'A'
                FOR
                  XML PATH(''),
                      TYPE
                ),
                ( SELECT    [ProductCategoryID],
                            ZNodeProductCategory.[ProductID],
                            [Theme],
                            [MasterPage],
                            [CSS],
                            [CategoryID],
                            [BeginDate],
                            [EndDate],
                            [DisplayOrder],
                            [ActiveInd]
                  FROM      ZNodeProductCategory ZNodeProductCategory
                  WHERE     ZNodeProductCategory.ProductId = ZNodeProduct.productid
                  ORDER BY  ZNodeProductCategory.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    ZNodeHighlight.[HighlightID],
                            [ImageFile],
                            [ImageAltTag],
                            [Name],
                            [Description],
                            [DisplayPopup],
                            [Hyperlink],
                            [HyperlinkNewWinInd],
                            [HighlightTypeID],
                            [ActiveInd],
                            ZNodeHighlight.[DisplayOrder],
                            [ShortDescription],
                            [LocaleId]
                  FROM      ZNodehighlight ZNodeHighlight
                            INNER JOIN ZNodeProductHighlight ZNodeProductHighlight ON ZNodeHighlight.[HighlightID] = ZNodeProductHighlight.[HighlightID]
                  WHERE     ZNodeProductHighlight.productid = ZNodeProduct.productid
                  ORDER BY  ZNodeProductHighlight.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    [ReviewID],
                            ZNodeReview.[ProductID],
                            [AccountID],
                            [Subject],
                            [Pros],
                            [Cons],
                            [Comments],
                            [CreateUser],
                            [UserLocation],
                            [Rating],
                            [Status],
                            [CreateDate],
                            [Custom1],
                            [Custom2],
                            [Custom3]
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.[Status] = 'A'
                  ORDER BY  ZNodeReview.ReviewId desc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( select    ZNodeCrossSellItem.ProductId,
                            ZNodeCrossSellItem.Name,
                            ZNodeCrossSellItem.ShortDescription,
                            ZNodeCrossSellItem.ImageFile,
                            ZNodeCrossSellItem.SeoURL,
                            ZNodeCrossSellItem.ImageAltTag,
                            ZNodeCrossSellItem.RetailPrice,
                            ZNodeCrossSellItem.SalePrice,
                            ZNodeCrossSellItem.WholesalePrice,
                            ZNodeCrossSellItem.CallForPricing,
                            ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                                        TotalReviews = IsNull(COUNT(ProductId), 0)
                              FROM      ZNodeReview ZNodeReview
                              WHERE     ZNodeReview.ProductId = ZNodeCrossSellItem.ProductId
                                        AND ZNodeReview.[Status] = 'A'
                            FOR
                              XML PATH(''),
                                  TYPE
                            )
                  FROM     ProductsView ZNodeCrossSellItem
                  WHERE 
							ZNodeCrossSellItem.ProductId IN (
                            SELECT  ProductId
                            from    #PRODUCTCATEGORYLIST )
                            AND ZNodeCrossSellItem.ActiveInd = 1
                   
                  ORDER BY  ZNodeCrossSellItem.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( Select    ZNodeAttributeType.[AttributeTypeId],
                            ZNodeAttributeType.[Name],
                            ZNodeAttributeType.[Description],
                            ZNodeAttributeType.[DisplayOrder],
                            [IsPrivate],
                            [LocaleId],
                            ( SELECT
      Distinct                          ( ZNodeAttribute.AttributeId ),
                                        ZNodeAttribute.Name,
                                        ZNodeAttribute.DisplayOrder
                              FROM      ZNodeskuattribute skua
                                        INNER JOIN ZNodeProductattribute ZNodeAttribute ON skua.attributeid = ZNodeAttribute.attributeid
                              WHERE     ZNodeAttribute.attributetypeid = ZNodeAttributeType.attributetypeid
                                        and ZNodeAttribute.IsActive = 1
                                        and SKUID in (
                                        SELECT  SKUID
                                        FROM    ZNodeSKU
                                        WHERE   ProductID = @ProductId
                                                and ActiveInd = 1 )
                              Group By  ZNodeAttribute.AttributeId,
                                        ZNodeAttribute.Name,
                                        ZNodeAttribute.DisplayOrder
                              Order By  ZNodeAttribute.DisplayOrder
                            FOR
                              XML AUTO,
                                  TYPE,
                                  ELEMENTS
                            )
                  FROM      ZNodeAttributeType ZNodeAttributeType
                            INNER JOIN ZNodeProductTypeAttribute ProductTypeAttribute 
                            ON ZNodeAttributeType.attributetypeid = ProductTypeAttribute.attributetypeid
                            AND ProductTypeAttribute.ProductTypeId = ZNodeProduct.ProductTypeID
                  Order By  ZNodeAttributeType.DisplayOrder
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    ZNodeAddOn.[AddOnID],
                            ZNodeAddOn.[ProductID],
                            [Title],
                            [Name],
                            [Description],
                            [DisplayOrder],
                            [DisplayType],
                            [OptionalInd],
                            [AllowBackOrder],
                            [InStockMsg],
                            [OutOfStockMsg],
                            [BackOrderMsg],
                            [PromptMsg],
                            [TrackInventoryInd],
                            [LocaleId],
                            ( SELECT Distinct
                                        ( ZNodeAddonValue.AddOnValueID ),
                                        ZNodeAddonValue.Name,
                                        ZNodeAddonValue.[Description],
                                        ZNodeAddonValue.DisplayOrder,
                                        ZNodeAddonValue.RetailPrice as RetailPrice,
                                        ZNodeAddonValue.SalePrice as SalePrice,
                                        ZNodeAddonValue.WholeSalePrice as WholesalePrice,
                                        ZNodeAddonValue.DefaultInd as IsDefault,
                                        QuantityOnHand = ( SELECT   QuantityOnHand
                                                           FROM     ZNodeSKUInventory
                                                           WHERE    SKU = ZNodeAddOnValue.SKU
                                                         ),
                                        ZNodeAddonValue.weight as WeightAdditional,
                                        ZNodeAddonValue.ShippingRuleTypeID,
                                        ZNodeAddonValue.FreeShippingInd,
                                        ZNodeAddonValue.SupplierID,
                                        ZNodeAddonValue.TaxClassID,
                                        ZNodeAddonValue.ExternalProductID
                              FROM      ZNodeAddOnValue ZNodeAddOnValue
                              WHERE     ZNodeAddOnValue.AddOnId = ZNodeAddOn.AddOnId
                              Order By  ZNodeAddonValue.DisplayOrder
                            FOR
                              XML AUTO,
                                  TYPE,
                                  ELEMENTS
                            )
                  FROM      ZNodeAddOn ZNodeAddOn
                            INNER JOIN ZNodeProductAddOn ZNodeProductAddOn ON ZNodeAddOn.AddOnId = ZNodeProductAddOn.AddOnId
                  WHERE     ZNodeProductAddOn.ProductId = @ProductId
                  Order By  ZNodeAddon.DisplayOrder
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    [ProductTierID],
                            [ProductID],
                            [ProfileID],
                            [TierStart],
                            [TierEnd],
                            [Price]
                  FROM      ZNodeProductTier ZNodeProductTier
                  WHERE     ZNodeProductTier.ProductId = ZNodeProduct.ProductId
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT TOP 1 *  
                  FROM      ZNodeSKU
                  INNER JOIN ZnodeSKUInventory ON ZNodeSKU.SKU = ZNodeSKUInventory.SKU                     
                  WHERE     ZNodeSKU.ProductId = ZNodeProduct.ProductId  
                FOR  
                  XML PATH('ZNodeSKU'),  
                      TYPE,  
                      ELEMENTS  
                ),
                (SELECT  [ProductID],
                [Name],
                [ShortDescription],
                [Description],
                [FeaturesDesc],
                [ProductNum],
                [ProductTypeID],
                [RetailPrice],
                [SalePrice],
                [WholesalePrice],
                [ImageFile],
                [ImageAltTag],
                [Weight],
                [Length],
                [Width],
                [Height],
                [BeginActiveDate],
                [EndActiveDate],
                [DisplayOrder],
                [ActiveInd],
                [CallForPricing],
                [HomepageSpecial],
                [CategorySpecial],
                [InventoryDisplay],
                [Keywords],
                [ManufacturerID],
                [AdditionalInfoLink],
                [AdditionalInfoLinkLabel],
                [ShippingRuleTypeID],
                [ShippingRate],
                [SEOTitle],
                [SEOKeywords],
                [SEODescription],
                [Custom1],
                [Custom2],
                [Custom3],
                [ShipEachItemSeparately],
                [AllowBackOrder],
                [BackOrderMsg],
                [DropShipInd],
                [DropShipEmailID],
                [Specifications],
                [AdditionalInformation],
                [InStockMsg],
                [OutOfStockMsg],
                [TrackInventoryInd],
                [DownloadLink],
                [FreeShippingInd],
                [NewProductInd],
                [SEOURL],
                [MinQty],
                [MaxQty],
                [ShipSeparately],
                [FeaturedInd],
                [WebServiceDownloadDte],
                [UpdateDte],
                [SupplierID],
                [RecurringBillingInd],
                [RecurringBillingInstallmentInd],
                [RecurringBillingPeriod],
                [RecurringBillingFrequency],
                [RecurringBillingTotalCycles],
                [RecurringBillingInitialAmount],
                [TaxClassID],
                [PortalID],
                [AffiliateUrl],
                (SELECT TOP 1 sku.SKU, QuantityOnHand,ExternalProductID
										FROM ZNodeSKU sku
											INNER JOIN ZNodeSKUInventory SkuInventory 
										ON sku.SKU = SkuInventory.SKU
										WHERE 
											EXISTS
											(SELECT 1 FROM ZNodeSKUAttribute
												WHERE SKUID = sku.SKUID having COUNT(1) = 0)
												AND	sku.ProductID = ZNodeBundleProduct.ProductID
												
										FOR	XML PATH(''),TYPE
								),
                ManufacturerName = ( Select Name
                                     from   ZNodeManufacturer
                                     where  ManufacturerId = ZNodeBundleProduct.ManufacturerId AND ActiveInd = 1
                                   ),
                                   ( SELECT TOP 1 *  
                  FROM      ZNodeSKU
                  INNER JOIN ZnodeSKUInventory ON ZNodeSKU.SKU = ZNodeSKUInventory.SKU                     
                  WHERE     ZNodeSKU.ProductId = ZNodeBundleProduct.ProductID  
                FOR  
                  XML PATH('ZNodeSKU'),  
                      TYPE,  
                      ELEMENTS  
                )       
                FROM ZNodeProduct ZNodeBundleProduct INNER JOIN ZNodeParentChildProduct B
                ON ZNodeBundleProduct.ProductID = B.ChildProductID WHERE   B.ParentProductID = @ProductId AND ZNodeBundleProduct.ActiveInd = 1 FOR XML AUTO,
                    TYPE,
                    ELEMENTS )
                  
        FROM    ZNodeProduct ZNodeProduct			    
        WHERE   ZNodeProduct.ProductId = @ProductId
        FOR     XML AUTO,
                    TYPE,
                    ELEMENTS                  
    END  




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCartLineItem_GetBySavedCartID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeSavedCartLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCartLineItem_GetBySavedCartID
(

	@SavedCartID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[SavedCartLineItemID],
					[SavedCartID],
					[SKUID],
					[Quantity],
					[ParentSavedCartLineItemID],
					[OrderLineItemRelationshipTypeID]
				FROM
					[dbo].[ZNodeSavedCartLineItem]
				WHERE
					[SavedCartID] = @SavedCartID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetAffiliate]'
GO
SET ANSI_NULLS ON
GO

-- =============================================                  
-- Create date: Oct 16,2008
-- Update date: April,12,2010
-- Description: Returns the Affialiate info        
-- =============================================            
ALTER PROCEDURE  [dbo].[ZNode_GetAffiliate]                                           
AS                  
BEGIN                  
	-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM                  
	-- INTERFERING WITH SELECT STATEMENTS.                  
	SET NOCOUNT ON;
	
	SELECT Account.AccountId,
		Account.CompanyName,
		A.FirstName AS BillingFirstName,
		A.LastName AS BillingLastName
	FROM   ZNodeAccount Account LEFT OUTER JOIN ZNodeAddress A
		ON Account.AccountID=A.AccountID AND   A.IsDefaultBilling=1 
	WHERE  ReferralStatus = 'N'
       OR ReferralStatus = 'A'; 
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_GetByOrderID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetByOrderID
(

	@OrderID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3]
				FROM
					[dbo].[ZNodeOrderLineItem]
				WHERE
					[OrderID] = @OrderID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCartLineItem_GetByParentSavedCartLineItemID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeSavedCartLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCartLineItem_GetByParentSavedCartLineItemID
(

	@ParentSavedCartLineItemID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[SavedCartLineItemID],
					[SavedCartID],
					[SKUID],
					[Quantity],
					[ParentSavedCartLineItemID],
					[OrderLineItemRelationshipTypeID]
				FROM
					[dbo].[ZNodeSavedCartLineItem]
				WHERE
					[ParentSavedCartLineItemID] = @ParentSavedCartLineItemID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetAccountByPortal]'
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[ZNode_GetAccountByPortal](@Portals varchar(max))
as 
Begin
SELECT *,
       (CAST (Account.AccountID AS VARCHAR (MAX)) + ' - ' + ISNULL(FirstName + ' ', '') + ISNULL(LastName, '')) AS 'CustomerName'
FROM   ZNodeAccount AS Account LEFT OUTER JOIN ZNodeAddress A ON Account.AccountID=A.AccountID AND A.IsDefaultBilling=1
WHERE  Account.AccountID IN (SELECT AccountID
                     FROM   ZNodeAccountProfile
                     WHERE  ProfileID IN (SELECT ProfileID
                                          FROM   ZNodePortalProfile
                                          WHERE  PortalID IN ((SELECT [Value]
                                                               FROM   Sc_Splitter (@Portals, ',')))));  End

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_GetByParentOrderLineItemID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetByParentOrderLineItemID
(

	@ParentOrderLineItemID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3]
				FROM
					[dbo].[ZNodeOrderLineItem]
				WHERE
					[ParentOrderLineItemID] = @ParentOrderLineItemID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_GetByKeyPortalIDLocaleID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeMessageConfig table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_GetByKeyPortalIDLocaleID
(

	@Key nvarchar (100)  ,

	@PortalID int   ,

	@LocaleID int   
)
AS


				SELECT
					[MessageID],
					[Key],
					[PortalID],
					[LocaleID],
					[Description],
					[Value],
					[MessageTypeID]
				FROM
					[dbo].[ZNodeMessageConfig]
				WHERE
					[Key] = @Key
					AND [PortalID] = @PortalID
					AND [LocaleID] = @LocaleID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCartLineItem_GetBySavedCartLineItemID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeSavedCartLineItem table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCartLineItem_GetBySavedCartLineItemID
(

	@SavedCartLineItemID int   
)
AS


				SELECT
					[SavedCartLineItemID],
					[SavedCartID],
					[SKUID],
					[Quantity],
					[ParentSavedCartLineItemID],
					[OrderLineItemRelationshipTypeID]
				FROM
					[dbo].[ZNodeSavedCartLineItem]
				WHERE
					[SavedCartLineItemID] = @SavedCartLineItemID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeOrderLineItem_GetByOrderLineItemRelationshipTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetByOrderLineItemRelationshipTypeID
(

	@OrderLineItemRelationshipTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3]
				FROM
					[dbo].[ZNodeOrderLineItem]
				WHERE
					[OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCartLineItem_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeSavedCartLineItem table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCartLineItem_Find
(

	@SearchUsingOR bit   = null ,

	@SavedCartLineItemID int   = null ,

	@SavedCartID int   = null ,

	@SKUID int   = null ,

	@Quantity int   = null ,

	@ParentSavedCartLineItemID int   = null ,

	@OrderLineItemRelationshipTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SavedCartLineItemID]
	, [SavedCartID]
	, [SKUID]
	, [Quantity]
	, [ParentSavedCartLineItemID]
	, [OrderLineItemRelationshipTypeID]
    FROM
	[dbo].[ZNodeSavedCartLineItem]
    WHERE 
	 ([SavedCartLineItemID] = @SavedCartLineItemID OR @SavedCartLineItemID IS NULL)
	AND ([SavedCartID] = @SavedCartID OR @SavedCartID IS NULL)
	AND ([SKUID] = @SKUID OR @SKUID IS NULL)
	AND ([Quantity] = @Quantity OR @Quantity IS NULL)
	AND ([ParentSavedCartLineItemID] = @ParentSavedCartLineItemID OR @ParentSavedCartLineItemID IS NULL)
	AND ([OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID OR @OrderLineItemRelationshipTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SavedCartLineItemID]
	, [SavedCartID]
	, [SKUID]
	, [Quantity]
	, [ParentSavedCartLineItemID]
	, [OrderLineItemRelationshipTypeID]
    FROM
	[dbo].[ZNodeSavedCartLineItem]
    WHERE 
	 ([SavedCartLineItemID] = @SavedCartLineItemID AND @SavedCartLineItemID is not null)
	OR ([SavedCartID] = @SavedCartID AND @SavedCartID is not null)
	OR ([SKUID] = @SKUID AND @SKUID is not null)
	OR ([Quantity] = @Quantity AND @Quantity is not null)
	OR ([ParentSavedCartLineItemID] = @ParentSavedCartLineItemID AND @ParentSavedCartLineItemID is not null)
	OR ([OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID AND @OrderLineItemRelationshipTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_GetByPaymentStatusID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetByPaymentStatusID
(

	@PaymentStatusID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3]
				FROM
					[dbo].[ZNodeOrderLineItem]
				WHERE
					[PaymentStatusID] = @PaymentStatusID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_GetByOrderLineItemID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItem table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetByOrderLineItemID
(

	@OrderLineItemID int   
)
AS


				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3]
				FROM
					[dbo].[ZNodeOrderLineItem]
				WHERE
					[OrderLineItemID] = @OrderLineItemID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeOrderLineItem table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_Find
(

	@SearchUsingOR bit   = null ,

	@OrderLineItemID int   = null ,

	@OrderID int   = null ,

	@ShipmentID int   = null ,

	@ProductNum nvarchar (MAX)  = null ,

	@Name nvarchar (MAX)  = null ,

	@Description nvarchar (MAX)  = null ,

	@Quantity int   = null ,

	@Price money   = null ,

	@Weight decimal (18, 2)  = null ,

	@SKU nvarchar (MAX)  = null ,

	@ParentOrderLineItemID int   = null ,

	@OrderLineItemRelationshipTypeID int   = null ,

	@DownloadLink nvarchar (MAX)  = null ,

	@DiscountAmount money   = null ,

	@ShipSeparately bit   = null ,

	@ShipDate datetime   = null ,

	@ReturnDate datetime   = null ,

	@ShippingCost money   = null ,

	@PromoDescription nvarchar (MAX)  = null ,

	@SalesTax money   = null ,

	@VAT money   = null ,

	@GST money   = null ,

	@PST money   = null ,

	@HST money   = null ,

	@TransactionNumber nvarchar (MAX)  = null ,

	@PaymentStatusID int   = null ,

	@TrackingNumber nvarchar (MAX)  = null ,

	@AutoGeneratedTracking bit   = null ,

	@Custom1 nvarchar (MAX)  = null ,

	@Custom2 nvarchar (MAX)  = null ,

	@Custom3 nvarchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [OrderLineItemID]
	, [OrderID]
	, [ShipmentID]
	, [ProductNum]
	, [Name]
	, [Description]
	, [Quantity]
	, [Price]
	, [Weight]
	, [SKU]
	, [ParentOrderLineItemID]
	, [OrderLineItemRelationshipTypeID]
	, [DownloadLink]
	, [DiscountAmount]
	, [ShipSeparately]
	, [ShipDate]
	, [ReturnDate]
	, [ShippingCost]
	, [PromoDescription]
	, [SalesTax]
	, [VAT]
	, [GST]
	, [PST]
	, [HST]
	, [TransactionNumber]
	, [PaymentStatusID]
	, [TrackingNumber]
	, [AutoGeneratedTracking]
	, [Custom1]
	, [Custom2]
	, [Custom3]
    FROM
	[dbo].[ZNodeOrderLineItem]
    WHERE 
	 ([OrderLineItemID] = @OrderLineItemID OR @OrderLineItemID IS NULL)
	AND ([OrderID] = @OrderID OR @OrderID IS NULL)
	AND ([ShipmentID] = @ShipmentID OR @ShipmentID IS NULL)
	AND ([ProductNum] = @ProductNum OR @ProductNum IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([Quantity] = @Quantity OR @Quantity IS NULL)
	AND ([Price] = @Price OR @Price IS NULL)
	AND ([Weight] = @Weight OR @Weight IS NULL)
	AND ([SKU] = @SKU OR @SKU IS NULL)
	AND ([ParentOrderLineItemID] = @ParentOrderLineItemID OR @ParentOrderLineItemID IS NULL)
	AND ([OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID OR @OrderLineItemRelationshipTypeID IS NULL)
	AND ([DownloadLink] = @DownloadLink OR @DownloadLink IS NULL)
	AND ([DiscountAmount] = @DiscountAmount OR @DiscountAmount IS NULL)
	AND ([ShipSeparately] = @ShipSeparately OR @ShipSeparately IS NULL)
	AND ([ShipDate] = @ShipDate OR @ShipDate IS NULL)
	AND ([ReturnDate] = @ReturnDate OR @ReturnDate IS NULL)
	AND ([ShippingCost] = @ShippingCost OR @ShippingCost IS NULL)
	AND ([PromoDescription] = @PromoDescription OR @PromoDescription IS NULL)
	AND ([SalesTax] = @SalesTax OR @SalesTax IS NULL)
	AND ([VAT] = @VAT OR @VAT IS NULL)
	AND ([GST] = @GST OR @GST IS NULL)
	AND ([PST] = @PST OR @PST IS NULL)
	AND ([HST] = @HST OR @HST IS NULL)
	AND ([TransactionNumber] = @TransactionNumber OR @TransactionNumber IS NULL)
	AND ([PaymentStatusID] = @PaymentStatusID OR @PaymentStatusID IS NULL)
	AND ([TrackingNumber] = @TrackingNumber OR @TrackingNumber IS NULL)
	AND ([AutoGeneratedTracking] = @AutoGeneratedTracking OR @AutoGeneratedTracking IS NULL)
	AND ([Custom1] = @Custom1 OR @Custom1 IS NULL)
	AND ([Custom2] = @Custom2 OR @Custom2 IS NULL)
	AND ([Custom3] = @Custom3 OR @Custom3 IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [OrderLineItemID]
	, [OrderID]
	, [ShipmentID]
	, [ProductNum]
	, [Name]
	, [Description]
	, [Quantity]
	, [Price]
	, [Weight]
	, [SKU]
	, [ParentOrderLineItemID]
	, [OrderLineItemRelationshipTypeID]
	, [DownloadLink]
	, [DiscountAmount]
	, [ShipSeparately]
	, [ShipDate]
	, [ReturnDate]
	, [ShippingCost]
	, [PromoDescription]
	, [SalesTax]
	, [VAT]
	, [GST]
	, [PST]
	, [HST]
	, [TransactionNumber]
	, [PaymentStatusID]
	, [TrackingNumber]
	, [AutoGeneratedTracking]
	, [Custom1]
	, [Custom2]
	, [Custom3]
    FROM
	[dbo].[ZNodeOrderLineItem]
    WHERE 
	 ([OrderLineItemID] = @OrderLineItemID AND @OrderLineItemID is not null)
	OR ([OrderID] = @OrderID AND @OrderID is not null)
	OR ([ShipmentID] = @ShipmentID AND @ShipmentID is not null)
	OR ([ProductNum] = @ProductNum AND @ProductNum is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([Quantity] = @Quantity AND @Quantity is not null)
	OR ([Price] = @Price AND @Price is not null)
	OR ([Weight] = @Weight AND @Weight is not null)
	OR ([SKU] = @SKU AND @SKU is not null)
	OR ([ParentOrderLineItemID] = @ParentOrderLineItemID AND @ParentOrderLineItemID is not null)
	OR ([OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID AND @OrderLineItemRelationshipTypeID is not null)
	OR ([DownloadLink] = @DownloadLink AND @DownloadLink is not null)
	OR ([DiscountAmount] = @DiscountAmount AND @DiscountAmount is not null)
	OR ([ShipSeparately] = @ShipSeparately AND @ShipSeparately is not null)
	OR ([ShipDate] = @ShipDate AND @ShipDate is not null)
	OR ([ReturnDate] = @ReturnDate AND @ReturnDate is not null)
	OR ([ShippingCost] = @ShippingCost AND @ShippingCost is not null)
	OR ([PromoDescription] = @PromoDescription AND @PromoDescription is not null)
	OR ([SalesTax] = @SalesTax AND @SalesTax is not null)
	OR ([VAT] = @VAT AND @VAT is not null)
	OR ([GST] = @GST AND @GST is not null)
	OR ([PST] = @PST AND @PST is not null)
	OR ([HST] = @HST AND @HST is not null)
	OR ([TransactionNumber] = @TransactionNumber AND @TransactionNumber is not null)
	OR ([PaymentStatusID] = @PaymentStatusID AND @PaymentStatusID is not null)
	OR ([TrackingNumber] = @TrackingNumber AND @TrackingNumber is not null)
	OR ([AutoGeneratedTracking] = @AutoGeneratedTracking AND @AutoGeneratedTracking is not null)
	OR ([Custom1] = @Custom1 AND @Custom1 is not null)
	OR ([Custom2] = @Custom2 AND @Custom2 is not null)
	OR ([Custom3] = @Custom3 AND @Custom3 is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeAddress_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeAddress table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeAddress_Get_List

AS


				
				SELECT
					[AddressID],
					[FirstName],
					[MiddleName],
					[LastName],
					[CompanyName],
					[Street],
					[Street1],
					[City],
					[StateCode],
					[PostalCode],
					[CountryCode],
					[PhoneNumber],
					[IsDefaultBilling],
					[AccountID],
					[IsDefaultShipping],
					[Name]
				FROM
					[dbo].[ZNodeAddress]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeAddress_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeAddress table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeAddress_Insert
(

	@AddressID int    OUTPUT,

	@FirstName nvarchar (MAX)  ,

	@MiddleName nvarchar (MAX)  ,

	@LastName nvarchar (MAX)  ,

	@CompanyName nvarchar (MAX)  ,

	@Street nvarchar (MAX)  ,

	@Street1 nvarchar (MAX)  ,

	@City nvarchar (MAX)  ,

	@StateCode nvarchar (MAX)  ,

	@PostalCode nvarchar (MAX)  ,

	@CountryCode nvarchar (MAX)  ,

	@PhoneNumber nvarchar (MAX)  ,

	@IsDefaultBilling bit   ,

	@AccountID int   ,

	@IsDefaultShipping bit   ,

	@Name nvarchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[ZNodeAddress]
					(
					[FirstName]
					,[MiddleName]
					,[LastName]
					,[CompanyName]
					,[Street]
					,[Street1]
					,[City]
					,[StateCode]
					,[PostalCode]
					,[CountryCode]
					,[PhoneNumber]
					,[IsDefaultBilling]
					,[AccountID]
					,[IsDefaultShipping]
					,[Name]
					)
				VALUES
					(
					@FirstName
					,@MiddleName
					,@LastName
					,@CompanyName
					,@Street
					,@Street1
					,@City
					,@StateCode
					,@PostalCode
					,@CountryCode
					,@PhoneNumber
					,@IsDefaultBilling
					,@AccountID
					,@IsDefaultShipping
					,@Name
					)
				
				-- Get the identity value
				SET @AddressID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeAddress_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeAddress table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeAddress_Update
(

	@AddressID int   ,

	@FirstName nvarchar (MAX)  ,

	@MiddleName nvarchar (MAX)  ,

	@LastName nvarchar (MAX)  ,

	@CompanyName nvarchar (MAX)  ,

	@Street nvarchar (MAX)  ,

	@Street1 nvarchar (MAX)  ,

	@City nvarchar (MAX)  ,

	@StateCode nvarchar (MAX)  ,

	@PostalCode nvarchar (MAX)  ,

	@CountryCode nvarchar (MAX)  ,

	@PhoneNumber nvarchar (MAX)  ,

	@IsDefaultBilling bit   ,

	@AccountID int   ,

	@IsDefaultShipping bit   ,

	@Name nvarchar (MAX)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeAddress]
				SET
					[FirstName] = @FirstName
					,[MiddleName] = @MiddleName
					,[LastName] = @LastName
					,[CompanyName] = @CompanyName
					,[Street] = @Street
					,[Street1] = @Street1
					,[City] = @City
					,[StateCode] = @StateCode
					,[PostalCode] = @PostalCode
					,[CountryCode] = @CountryCode
					,[PhoneNumber] = @PhoneNumber
					,[IsDefaultBilling] = @IsDefaultBilling
					,[AccountID] = @AccountID
					,[IsDefaultShipping] = @IsDefaultShipping
					,[Name] = @Name
				WHERE
[AddressID] = @AddressID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeAddress_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeAddress table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeAddress_Delete
(

	@AddressID int   
)
AS


				DELETE FROM [dbo].[ZNodeAddress] WITH (ROWLOCK) 
				WHERE
					[AddressID] = @AddressID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeAddress_GetByAccountID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAddress table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeAddress_GetByAccountID
(

	@AccountID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AddressID],
					[FirstName],
					[MiddleName],
					[LastName],
					[CompanyName],
					[Street],
					[Street1],
					[City],
					[StateCode],
					[PostalCode],
					[CountryCode],
					[PhoneNumber],
					[IsDefaultBilling],
					[AccountID],
					[IsDefaultShipping],
					[Name]
				FROM
					[dbo].[ZNodeAddress]
				WHERE
					[AccountID] = @AccountID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeAddress_GetByAddressID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAddress table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeAddress_GetByAddressID
(

	@AddressID int   
)
AS


				SELECT
					[AddressID],
					[FirstName],
					[MiddleName],
					[LastName],
					[CompanyName],
					[Street],
					[Street1],
					[City],
					[StateCode],
					[PostalCode],
					[CountryCode],
					[PhoneNumber],
					[IsDefaultBilling],
					[AccountID],
					[IsDefaultShipping],
					[Name]
				FROM
					[dbo].[ZNodeAddress]
				WHERE
					[AddressID] = @AddressID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeAddress_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeAddress table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeAddress_Find
(

	@SearchUsingOR bit   = null ,

	@AddressID int   = null ,

	@FirstName nvarchar (MAX)  = null ,

	@MiddleName nvarchar (MAX)  = null ,

	@LastName nvarchar (MAX)  = null ,

	@CompanyName nvarchar (MAX)  = null ,

	@Street nvarchar (MAX)  = null ,

	@Street1 nvarchar (MAX)  = null ,

	@City nvarchar (MAX)  = null ,

	@StateCode nvarchar (MAX)  = null ,

	@PostalCode nvarchar (MAX)  = null ,

	@CountryCode nvarchar (MAX)  = null ,

	@PhoneNumber nvarchar (MAX)  = null ,

	@IsDefaultBilling bit   = null ,

	@AccountID int   = null ,

	@IsDefaultShipping bit   = null ,

	@Name nvarchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AddressID]
	, [FirstName]
	, [MiddleName]
	, [LastName]
	, [CompanyName]
	, [Street]
	, [Street1]
	, [City]
	, [StateCode]
	, [PostalCode]
	, [CountryCode]
	, [PhoneNumber]
	, [IsDefaultBilling]
	, [AccountID]
	, [IsDefaultShipping]
	, [Name]
    FROM
	[dbo].[ZNodeAddress]
    WHERE 
	 ([AddressID] = @AddressID OR @AddressID IS NULL)
	AND ([FirstName] = @FirstName OR @FirstName IS NULL)
	AND ([MiddleName] = @MiddleName OR @MiddleName IS NULL)
	AND ([LastName] = @LastName OR @LastName IS NULL)
	AND ([CompanyName] = @CompanyName OR @CompanyName IS NULL)
	AND ([Street] = @Street OR @Street IS NULL)
	AND ([Street1] = @Street1 OR @Street1 IS NULL)
	AND ([City] = @City OR @City IS NULL)
	AND ([StateCode] = @StateCode OR @StateCode IS NULL)
	AND ([PostalCode] = @PostalCode OR @PostalCode IS NULL)
	AND ([CountryCode] = @CountryCode OR @CountryCode IS NULL)
	AND ([PhoneNumber] = @PhoneNumber OR @PhoneNumber IS NULL)
	AND ([IsDefaultBilling] = @IsDefaultBilling OR @IsDefaultBilling IS NULL)
	AND ([AccountID] = @AccountID OR @AccountID IS NULL)
	AND ([IsDefaultShipping] = @IsDefaultShipping OR @IsDefaultShipping IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AddressID]
	, [FirstName]
	, [MiddleName]
	, [LastName]
	, [CompanyName]
	, [Street]
	, [Street1]
	, [City]
	, [StateCode]
	, [PostalCode]
	, [CountryCode]
	, [PhoneNumber]
	, [IsDefaultBilling]
	, [AccountID]
	, [IsDefaultShipping]
	, [Name]
    FROM
	[dbo].[ZNodeAddress]
    WHERE 
	 ([AddressID] = @AddressID AND @AddressID is not null)
	OR ([FirstName] = @FirstName AND @FirstName is not null)
	OR ([MiddleName] = @MiddleName AND @MiddleName is not null)
	OR ([LastName] = @LastName AND @LastName is not null)
	OR ([CompanyName] = @CompanyName AND @CompanyName is not null)
	OR ([Street] = @Street AND @Street is not null)
	OR ([Street1] = @Street1 AND @Street1 is not null)
	OR ([City] = @City AND @City is not null)
	OR ([StateCode] = @StateCode AND @StateCode is not null)
	OR ([PostalCode] = @PostalCode AND @PostalCode is not null)
	OR ([CountryCode] = @CountryCode AND @CountryCode is not null)
	OR ([PhoneNumber] = @PhoneNumber AND @PhoneNumber is not null)
	OR ([IsDefaultBilling] = @IsDefaultBilling AND @IsDefaultBilling is not null)
	OR ([AccountID] = @AccountID AND @AccountID is not null)
	OR ([IsDefaultShipping] = @IsDefaultShipping AND @IsDefaultShipping is not null)
	OR ([Name] = @Name AND @Name is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeMessageType]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodeMessageType]
(
[MessageTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayOrder] [int] NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_ZNodeMessageType_IsActive] DEFAULT ((1))
)
GO
-- Add 2 rows to [dbo].[ZNodeMessageType]
SET IDENTITY_INSERT [dbo].[ZNodeMessageType] ON
GO
INSERT INTO [dbo].[ZNodeMessageType] ([MessageTypeID], [Name], [DisplayOrder], [IsActive]) VALUES (1, N'Message Block', 1, 1)
INSERT INTO [dbo].[ZNodeMessageType] ([MessageTypeID], [Name], [DisplayOrder], [IsActive]) VALUES (2, N'Banner', 2, 1)
GO
SET IDENTITY_INSERT [dbo].[ZNodeMessageType] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeMessageType] on [dbo].[ZNodeMessageType]'
GO
ALTER TABLE [dbo].[ZNodeMessageType] ADD CONSTRAINT [PK_ZNodeMessageType] PRIMARY KEY CLUSTERED  ([MessageTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeMessageType_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeMessageType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeMessageType_Get_List

AS


				
				SELECT
					[MessageTypeID],
					[Name],
					[DisplayOrder],
					[IsActive]
				FROM
					[dbo].[ZNodeMessageType]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeMessageType_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeMessageType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeMessageType_Insert
(

	@MessageTypeID int    OUTPUT,

	@Name nvarchar (MAX)  ,

	@DisplayOrder int   ,

	@IsActive bit   
)
AS


				
				INSERT INTO [dbo].[ZNodeMessageType]
					(
					[Name]
					,[DisplayOrder]
					,[IsActive]
					)
				VALUES
					(
					@Name
					,@DisplayOrder
					,@IsActive
					)
				
				-- Get the identity value
				SET @MessageTypeID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeMessageType_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeMessageType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeMessageType_Update
(

	@MessageTypeID int   ,

	@Name nvarchar (MAX)  ,

	@DisplayOrder int   ,

	@IsActive bit   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeMessageType]
				SET
					[Name] = @Name
					,[DisplayOrder] = @DisplayOrder
					,[IsActive] = @IsActive
				WHERE
[MessageTypeID] = @MessageTypeID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeMessageType_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeMessageType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeMessageType_Delete
(

	@MessageTypeID int   
)
AS


				DELETE FROM [dbo].[ZNodeMessageType] WITH (ROWLOCK) 
				WHERE
					[MessageTypeID] = @MessageTypeID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeMessageType_GetByMessageTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeMessageType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeMessageType_GetByMessageTypeID
(

	@MessageTypeID int   
)
AS


				SELECT
					[MessageTypeID],
					[Name],
					[DisplayOrder],
					[IsActive]
				FROM
					[dbo].[ZNodeMessageType]
				WHERE
					[MessageTypeID] = @MessageTypeID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeMessageType_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeMessageType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeMessageType_Find
(

	@SearchUsingOR bit   = null ,

	@MessageTypeID int   = null ,

	@Name nvarchar (MAX)  = null ,

	@DisplayOrder int   = null ,

	@IsActive bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [MessageTypeID]
	, [Name]
	, [DisplayOrder]
	, [IsActive]
    FROM
	[dbo].[ZNodeMessageType]
    WHERE 
	 ([MessageTypeID] = @MessageTypeID OR @MessageTypeID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([IsActive] = @IsActive OR @IsActive IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [MessageTypeID]
	, [Name]
	, [DisplayOrder]
	, [IsActive]
    FROM
	[dbo].[ZNodeMessageType]
    WHERE 
	 ([MessageTypeID] = @MessageTypeID AND @MessageTypeID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([IsActive] = @IsActive AND @IsActive is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeMessageConfig table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_Get_List

AS


				
				SELECT
					[MessageID],
					[Key],
					[PortalID],
					[LocaleID],
					[Description],
					[Value],
					[MessageTypeID]
				FROM
					[dbo].[ZNodeMessageConfig]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_DeletePortalByPortalID]'
GO
SET ANSI_NULLS ON
GO


-- Delete Portal Related Information  
ALTER PROCEDURE [dbo].[ZNode_DeletePortalByPortalID](@PortalID INT)          
AS    
BEGIN  
  
	-- Delete Notes only for this portal ID  
	DELETE ZNodeNote FROM ZNodeNote WITH (ROWLOCK) inner join 
	ZNodeCaseRequest on ZNodeNote.CaseID = ZNodeCaseRequest.CaseID 
	WHERE ZNodeCaseRequest.PortalID = @PortalID  
  
    -- Delete Case Request for this portal ID   
    DELETE FROM ZNodeCaseRequest WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete Content Page Revision for this portal ID  
    DELETE ZNodeContentPageRevision  
    FROM ZNodeContentPageRevision WITH (ROWLOCK)   
       inner join ZNodeContentPage on ZNodeContentPageRevision.ContentPageID = ZNodeContentPage.ContentPageID  
    WHERE ZNodeContentPage.PortalID = @PortalID  
      
    -- Delete Content Page for this portal ID  
    DELETE FROM ZNodeContentPage WITH (ROWLOCK) WHERE PortalID = @PortalID       
      
    -- Delete Account Payments for this portal ID  
    DELETE ZNodeAccountPayment  
    FROM ZNodeAccountPayment WITH (ROWLOCK)   
       inner join ZNodeOrder on ZNodeAccountPayment.OrderID = ZNodeOrder.OrderID  
    WHERE ZNodeOrder.PortalID = @PortalID  
      
    -- Delete Digital Assets for this portal ID  
    DELETE ZNodeDigitalAsset  
    FROM ZNodeDigitalAsset WITH (ROWLOCK)   
       inner join ZNodeOrderLineItem on ZNodeDigitalAsset.OrderLineItemID = ZNodeOrderLineItem.OrderLineItemID  
       inner join ZNodeOrder on ZNodeOrderLineItem.OrderID = ZNodeOrder.OrderID  
    WHERE ZNodeOrder.PortalID = @PortalID  
      
    -- Delete Order Line Items for this portal ID  
    DELETE ZNodeOrderLineItem  
    FROM ZNodeOrderLineItem WITH (ROWLOCK)   
       inner join ZNodeOrder on ZNodeOrderLineItem.OrderID = ZNodeOrder.OrderID  
    WHERE ZNodeOrder.PortalID = @PortalID  
      
    -- Delete Orders for this portal ID  
    DELETE FROM ZNodeOrder WITH (ROWLOCK) WHERE PortalID = @PortalID  
   
   
    -- Delete Product related tables based on ProductID
	DELETE FROM ZnodeWishList
	WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID)
  
    DELETE FROM ZnodeReview
	WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID)

	DELETE FROM ZNodeProductAddOn
	WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID)

	DELETE FROM ZNodeProductCategory
	WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID)

    DELETE FROM ZNodeProductImage
    WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID)

    DELETE FROM ZNodeProductCrossSell
    WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID)
			  OR RelatedProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID)

    DELETE FROM ZNodeProductHighlight
    WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID)

    DELETE FROM ZNodeSKU
    WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID)
	  
    DELETE FROM ZNodeProductTier 
    WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID)
	  
    DELETE FROM ZNodeDigitalAsset
    WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID) 
	        
    DELETE FROM ZNodeProductProfile
    WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID) 
	  
    DELETE FROM ZNodeTagProductSKU
    WHERE  ProductId IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID) 
	  
    DELETE FROM ZNodeProductReviewHistory
    WHERE  ProductID IN (SELECT ProductID FROM ZNodeProduct WHERE PortalID = @PortalID)
      
    
    -- Delete Addons for this portalID
    DELETE FROM ZNodeAddOn WITH (ROWLOCK) WHERE PortalID = @PortalID
    
    -- Delete Tax Rule Type for this portalID
    DELETE FROM ZNodeTaxRuleType WITH (ROWLOCK) WHERE PortalID = @PortalID
    
    -- Delete Message Config for this portalID
    DELETE FROM ZNodeMessageConfig WITH (ROWLOCK) WHERE PortalID = @PortalID
    
    -- Delete Attribute Type for this portalID
    DELETE FROM ZNodeAttributeType WITH (ROWLOCK) WHERE PortalID = @PortalID
    
    -- Delete Highlight for this portalID
    DELETE FROM ZNodeHighlight WITH (ROWLOCK) WHERE PortalID = @PortalID
    
    -- Delete Supplier for this portalID
    DELETE FROM ZNodeSupplier WITH (ROWLOCK) WHERE PortalID = @PortalID
    
    -- Delete Taxclass for this portalID
    DELETE FROM ZNodeTaxClass WITH (ROWLOCK) WHERE PortalID = @PortalID
    
    -- Delete Manufacturer for this portalID
    DELETE FROM ZNodeManufacturer WITH (ROWLOCK) WHERE PortalID = @PortalID
    
    -- Delete Category for this portalID
    DELETE FROM ZNodeCategory WITH (ROWLOCK) WHERE PortalID = @PortalID
    
    -- Delete Product Type for this portalID
    DELETE FROM ZNodeProductType WITH (ROWLOCK) WHERE PortalID = @PortalID
    
    -- Delete ProductsView for this portalID
    DELETE FROM ProductsView WITH (ROWLOCK) WHERE PortalID = @PortalID
    
       
      
      
    -- Delete Products for this portal ID
    DELETE FROM ZNodeProduct WITH (ROWLOCK) WHERE PortalID = @PortalID
       
    
    -- Delete Portal Catalog for this portal ID  
    DELETE FROM ZNodePortalCatalog WITH (ROWLOCK) WHERE PortalID = @PortalID  
    
    -- Delete Catalog for this portalID
    DELETE FROM ZNodeCatalog WITH (ROWLOCK) WHERE PortalID = @PortalID
         
    -- Delete Portal Country for this portal ID  
    DELETE FROM ZNodePortalCountry WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete Portal Profile for this portal ID  
    DELETE FROM ZNodePortalProfile WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete Portal Tax Rules for this portal ID  
    DELETE FROM ZNodeTaxRule WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete Tracking Events for this portal ID  
    DELETE ZNodeTrackingEvent  
    FROM ZNodeTrackingEvent WITH (ROWLOCK)   
       inner join ZNodeTracking on ZNodeTrackingEvent.TrackingID = ZNodeTracking.TrackingID  
    WHERE ZNodeTracking.PortalID = @PortalID  
      
    -- Delete Tracking for this portal ID  
    DELETE FROM ZNodeTracking WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete the portal for this portal ID  
    DELETE FROM ZNodePortal WITH (ROWLOCK) WHERE PortalID = @PortalID  
  
END  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_EmptyCatalog]'
GO



ALTER PROCEDURE [dbo].[ZNode_EmptyCatalog]    
(@UserName Varchar(1000))    
AS    
BEGIN            
-- DOES NOT DELETE CONTENT PAGES    
 
  
DELETE FROM ZNODETAGGROUPCATEGORY  
DELETE FROM ZNODETAGGROUP  
DELETE FROM ZNODETAG  
DELETE FROM ZNODECATEGORYNODE  
DELETE FROM ZNODEWISHLIST      
DELETE FROM ZNODEREVIEW      
DELETE FROM ZNODESKUATTRIBUTE              
DELETE FROM ZNODEPRODUCTTYPEATTRIBUTE              
DELETE FROM ZNODEPRODUCTATTRIBUTE              
DELETE FROM ZNODEATTRIBUTETYPE              
DELETE FROM ZNODEADDONVALUE  
DELETE FROM ZNODEPRODUCTADDON              
DELETE FROM ZNODEADDON      
DELETE FROM ZNODEPRODUCTCATEGORY              
DELETE FROM ZNODEPRODUCTIMAGE              
DELETE FROM ZNODECATEGORY              
DELETE FROM ZNODEPRODUCTCROSSSELL              
DELETE FROM ZNODEPRODUCTHIGHLIGHT        
DELETE FROM ZNODEDIGITALASSET        
DELETE FROM ZNODEPRODUCTTIER        
DELETE FROM ZNODEORDERLINEITEM               
DELETE FROM ZNODEORDER              
DELETE FROM ZNODESAVEDCARTLINEITEM
DELETE FROM ZNODESAVEDCART
DELETE FROM ZNODECOOKIEMAPPING
DELETE FROM ZNODESKU  
DELETE FROM ZNODESKUINVENTORY           
DELETE FROM ZNODEPROMOTION         
DELETE FROM ZNODEPARENTCHILDPRODUCT  
DELETE FROM ZNODEPRODUCTREVIEWHISTORY
DELETE FROM ZNODEPRODUCT  
DELETE FROM ZNODEMANUFACTURER            
DELETE FROM ZNODEPRODUCTTYPE              
DELETE FROM ZNODEHIGHLIGHT      
DELETE FROM ZNODESHIPPINGRULE              
DELETE FROM ZNODESHIPPING              
DELETE FROM ZNODETAXRULE              
DELETE FROM ZNODENOTE              
DELETE FROM ZNODECASEREQUEST              
DELETE FROM ZNODEPAYMENTTOKEN
DELETE FROM ZNODEPAYMENTSETTING
DELETE FROM ZNODESTORE      
DELETE FROM ZNODEPASSWORDLOG WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName)      
DELETE FROM ZNODEACCOUNTPROFILE WHERE AccountID NOT IN(SELECT AccountID FROM ZnodeAccount WHERE USERID IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName))
DELETE FROM ZNODEACCOUNT WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName) OR USERID IS NULL      
DELETE FROM ASPNET_MEMBERSHIP WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName) OR USERID IS NULL            
DELETE FROM ASPNET_USERSINROLES WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName) OR USERID IS NULL        
DELETE FROM ASPNET_PROFILE WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName) OR USERID IS NULL            
DELETE FROM ASPNET_USERS WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName) OR USERID IS NULL            
            
INSERT INTO ZNODEPRODUCTTYPE (NAME, DESCRIPTION, DISPLAYORDER) VALUES ('DEFAULT','DEFAULT PRODUCT', 1)                
END  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchCustomer]'
GO


ALTER PROCEDURE [dbo].[ZNode_SearchCustomer]    
(            
-- ADD THE PARAMETERS FOR THE STORED PROCEDURE HERE            
@FirstName VARCHAR(MAX) = '',            
@LastName VARCHAR(MAX) = '',            
@CompanyName VARCHAR(MAX) = '',            
@ProfileID int = 0,            
@StartDate VARCHAR(MAX),            
@EndDate VARCHAR(MAX),            
@LoginName VARCHAR(MAX) = '',            
@AccountID VARCHAR(MAX) = '',            
@ExternalAccountNum VARCHAR(MAX) = '',            
@PhoneNum VARCHAR(MAX) = '',            
@EmailID VARCHAR(MAX) = '',            
@ReferralStatus VARCHAR(MAX) = '',            
@PortalID INT=0           
)            
AS            
BEGIN            
 -- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM            
 -- INTERFERING WITH SELECT STATEMENTS.            
	SET NOCOUNT ON;      
	
	DECLARE	@VARStateDate DATETIME;
	DECLARE @VAREndDate DATETIME;
		
	IF(LEN(@StartDate) = 0 OR @StartDate IS NULL)        
		BEGIN		   
			SET @VARStateDate =(SELECT Min([CreateDte])FROM ZNODEACCOUNT)        
		END
	ELSE
		BEGIN        
			SET @VARStateDate = CAST(@StartDate AS DATETIME);
		END	
		
	IF(LEN(@EndDate) = 0 OR @EndDate IS NULL)        
		BEGIN        
			SET @VAREndDate = (SELECT Max([CreateDte])FROM ZNODEACCOUNT)        
		END
	ELSE
		BEGIN        
			SET @VAREndDate =CAST(@EndDate  AS DATETIME);
		END		
		
	SET @VAREndDate = (SELECT DateAdd(dd, 1, @VAREndDate));  
	
	
	CREATE TABLE #TempAccount (AccountID INT);

	IF (@PROFILEID > 0 AND @PORTALID = 0) 
	BEGIN
			INSERT INTO #TempAccount (AccountID)
			(SELECT ACCOUNTID 			
			FROM ZNODEACCOUNTPROFILE 
			WHERE ProfileID = @PROFILEID)
	END
	ELSE IF (@PORTALID > 0 AND @PROFILEID = 0) 
	BEGIN
			INSERT INTO #TempAccount (AccountID)
			(SELECT ACCOUNTID 
			FROM ZNODEACCOUNTPROFILE ZAP
			INNER JOIN ZNODEPORTALPROFILE ZPP ON ZAP.ProfileID = ZPP .ProfileID
			WHERE ZPP.PORTALID = @PORTALID)
	END
    ELSE IF (@PORTALID > 0 AND @PROFILEID > 0) 
    BEGIN
			INSERT INTO #TempAccount (AccountID)
			(SELECT ACCOUNTID
			FROM ZNODEACCOUNTPROFILE ZAP
			INNER JOIN ZNODEPORTALPROFILE ZPP ON ZAP.ProfileID = ZPP .ProfileID
			WHERE  ZPP.PORTALID = @PORTALID AND ZPP.PROFILEID = @PROFILEID)
	END
	ELSE
	BEGIN
			INSERT INTO #TempAccount (AccountID)
			(SELECT ACCOUNTID
				FROM ZNODEACCOUNT)
	END
      
       
	SELECT ZA.[ACCOUNTID]
			,[USERID]                  
			,A.FirstName AS [BILLINGFIRSTNAME]          
			,A.LastName AS [BILLINGLASTNAME]                  
			,A.PhoneNumber AS [BILLINGPHONENUMBER]          
			,ZA.Email AS [BILLINGEMAILID]          			
			FROM ZNodeAccount ZA LEFT OUTER JOIN ZNodeAddress A 
			ON ZA.AccountID=A.AccountID AND A.IsDefaultBilling=1
	WHERE  EXISTS(SELECT AccountID FROM #TempAccount TA WHERE TA.AccountID = ZA.AccountID)		
		AND ((A.FirstName LIKE '%' + @FirstName + '%') OR LEN(@FirstName) = 0) 
		AND	((A.LastName LIKE '%' + @LastName + '%') OR LEN(@LastName) = 0) 
		AND ((ZA.CompanyName LIKE '%' + @CompanyName + '%') OR (A.CompanyName LIKE '%' + @CompanyName + '%') 
			OR (A.CompanyName LIKE '%' + @CompanyName + '%') OR LEN(@CompanyName) = 0) 
		AND (ZA.CreateDte BETWEEN @VARStateDate AND @VAREndDate)
		AND (ZA.UserID IN (SELECT UserID FROM Aspnet_Users WHERE UserName LIKE @LOGINNAME + '%') OR LEN(@LOGINNAME)= 0)
		AND	(ZA.AccountID LIKE @AccountID + '%' OR LEN(@ACCOUNTID) = 0)
		AND	(ZA.ExternalAccountNo LIKE @ExternalAccountNum + '%' OR LEN(@ExternalAccountNum) = 0)
		AND (A.PhoneNumber LIKE @PHONENUM + '%' OR LEN(@PHONENUM) = 0
				OR A.PhoneNumber LIKE @PHONENUM + '%')
		AND	(ZA.Email LIKE @EmailID + '%' OR LEN(@EmailID) = 0	OR ZA.Email  LIKE @EmailID + '%')
		AND	(ZA.ReferralStatus LIKE @ReferralStatus + '%' OR LEN(@ReferralStatus) = 0)
	
	ORDER BY [ACCOUNTID] DESC; 
     
END  
  



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNODE_WS_GetProductDetailBySku_XML]'
GO


ALTER PROCEDURE [dbo].[ZNODE_WS_GetProductDetailBySku_XML](@Sku VARCHAR(1000) = NULL)   
AS      
 BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.
 SET NOCOUNT ON;  
   
   WITH ProductList(ProductID,  StoreName, LocaleCode)
   AS
   (
		SELECT
			DISTINCT(product.[ProductId]),
			[dbo].[FN_ConcatStoreName](Product.ProductID, null, null),
			[dbo].[FN_ConcatLocaleCode](Product.ProductID, null, null)
		FROM ZNodeProduct Product  
		LEFT JOIN ZNodeProductCategory ZPC ON ZPC.ProductID = product.ProductID 
		INNER JOIN ZNodeCategoryNode ZCN ON ZCN.CategoryID = ZPC.CategoryID
		INNER JOIN ZNodePortalCatalog ZPOC ON ZPOC.CatalogID = ZCN.CatalogID
		WHERE Product.ProductId IN 
		(SELECT DISTINCT ProductId FROM ZNodeSku WHERE SKU = @Sku)
   )   
   SELECT
   product.[ProductId],  
   [Name]
   ,CAST([ShortDescription] AS NVarchar(MAX)) "ShortDescription"  
   ,CAST([Description] AS NVarchar(MAX)) "Description"  
   ,CAST([FeaturesDesc] AS NVarchar(MAX)) "FeaturesDesc",    
   Product.[ProductNum],  
   [ProductTypeId],  
   [RetailPrice] AS  'ProductPricing/RetailPrice',
   [SalePrice] AS 'ProductPricing/SalePrice',
   [WholesalePrice] AS 'ProductPricing/WholesalePrice',
   [ImageFile],  
   [Weight],  
   [Length],  
   [Width],  
   [Height],  
   product.[ActiveInd],  
   product.[DisplayOrder],  
   [CallForPricing],  
   [HomepageSpecial],  
   [CategorySpecial],  
   [InventoryDisplay],  
   [Keywords],  
   [ManufacturerId],  
   [AdditionalInfoLink],  
   [AdditionalInfoLinkLabel],  
   [ShippingRuleTypeId],  
   [SEOTitle],  
   [SEOKeywords],  
   [SEODescription],  
   [Custom1],  
   [Custom2],  
   [Custom3],  
   [ShipEachItemSeparately],  
   [AllowBackOrder],  
   [BackOrderMsg],  
   [DropShipInd],  
   [DropShipEmailId]
   ,CAST([Specifications] AS NVarchar(MAX)) "Specifications"  
   ,CAST([AdditionalInformation] AS NVarchar(MAX)) "AdditionalInformation"  
   ,CAST([InStockMsg] AS NVarchar(MAX)) "InStockMsg"  
   ,CAST([OutOfStockMsg] AS NVARCHAR(MAX)) "OutOfStockMsg",   
   [TrackInventoryInd],  
   [DownloadLink],  
   [FreeShippingInd],  
   [NewProductInd],  
   [SEOURL],  
   [MaxQty],  
   [ShipSeparately],  
   [FeaturedInd],  
   zpi.[StoreName],
   zpi.[LocaleCode],
   (SELECT [ReviewId]  
       ,[ProductId]  
       ,[AccountId]  
       ,[Subject]  
       ,[Pros]  
       ,[Cons]  
       ,[Comments]  
       ,[CreateUser]  
       ,[UserLocation]  
       ,[Rating]  
       ,[Status]  
       ,[CreateDate]  
       ,[Custom1]  
       ,[Custom2]  
       ,[Custom3] FROM [ZNodeReview] Review WHERE Review.ProductId = Product.ProductId FOR XML AUTO, TYPE , ELEMENTS)  
     -- Product Cross sell items --  
       ,(SELECT [ProductCrossSellTypeId]         
       ,[ProductId]  
       ,[RelatedProductId]  
       ,(SELECT  [ProductId],  		     
		   [Name],  
		   [ShortDescription],  
		   [Description],  
		   [FeaturesDesc],  
		   RelatedProduct.[ProductNum],  
		   [ProductTypeId],  
		   [RetailPrice] AS  'ProductPricing/RetailPrice',
		   [SalePrice] AS 'ProductPricing/SalePrice',
		   [WholesalePrice] AS 'ProductPricing/WholesalePrice',
		   [ImageFile],  
		   [Weight],  
		   [Length],  
		   [Width],  
		   [Height],  
		   [ActiveInd],  
		   [DisplayOrder],  
		   [CallForPricing],  
		   [HomepageSpecial],  
		   [CategorySpecial],  
		   [InventoryDisplay],  
		   [Keywords],  
		   [ManufacturerId],  
		   [AdditionalInfoLink],  
		   [AdditionalInfoLinkLabel],  
		   [ShippingRuleTypeId],  
		   [SEOTitle],  
		   [SEOKeywords],  
		   [SEODescription],  
		   [Custom1],  
		   [Custom2],  
		   [Custom3],  
		   [ShipEachItemSeparately],  
		   [AllowBackOrder],  
		   [BackOrderMsg],  
		   [DropShipInd],  
		   [DropShipEmailId],  
		   [Specifications],  
		   [AdditionalInformation],  
		   [InStockMsg],  
		   [OutOfStockMsg],  
		   [TrackInventoryInd],  
		   [DownloadLink],  
		   [FreeShippingInd],  		   
		   [NewProductInd],  
		   [SEOURL],  
		   [MaxQty],  
		   [ShipSeparately],  
		   [FeaturedInd] FROM ZNodeProduct RelatedProduct    
 WHERE RelatedProduct.ProductId = CrossSellItem.RelatedProductId FOR XML PATH('RelatedProduct'), TYPE , ELEMENTS)   
       ,[DisplayOrder] FROM [ZNodeProductCrossSell] CrossSellItem WHERE CrossSellItem.ProductId = Product.ProductId FOR XML AUTO, TYPE , ELEMENTS)  
      
     ,(SELECT [HighlightId]         
       ,[ImageFile]  
       ,[Name]  
       ,[Description]  
       ,[DisplayPopup]  
       ,[Hyperlink]  
       ,[HyperlinkNewWinInd]  
       ,[HighlightTypeId]  
       ,[ActiveInd]  
       ,[DisplayOrder]  
       ,[ShortDescription]       
        FROM [ZNodeHighlight] Highlight WHERE Highlight.HighlightId IN (SELECT ProductId FROM ZNodeProductHighlight  WHERE ProductId = Product.ProductId)  
         FOR XML AUTO, TYPE , ELEMENTS)     
	  ,(SELECT [SkuId]
		  ,[ProductId]  
		  ,Attribute.[Sku]
		  ,SupplierId
		  ,ImageAltTag
		  ,DisplayOrder  	  
		  ,[Note]  
		  ,[QuantityOnHand]   
		  ,[ReorderLevel] 
		  ,[WeightAdditional]
		  ,[SKUPicturePath] 'SkuPicturePath'  
		  ,[DisplayOrder]
		  ,[RetailPriceOverride] 
		  ,[SalePriceOverride] 
		  ,[WholesalePriceOverride] 
		  ,RecurringBillingInd
		  ,RecurringBillingInstallmentInd
		  ,RecurringBillingFrequency
		  ,RecurringBillingPeriod
		  ,RecurringBillingTotalCycles
		  ,RecurringBillingInitialAmount
		  ,[ActiveInd] FROM [ZNodeSKU] Attribute INNER JOIN ZNodeSKUInventory ON ZNodeSKUInventory.SKU = Attribute.SKU WHERE Attribute.ProductId = Product.ProductId FOR XML Path('Attribute') ,TYPE)
   
   FROM ZNodeProduct product INNER JOIN ProductList zpi
   ON product.ProductID = zpi.ProductID
   FOR XML PATH('ProductDetail') , Type, Elements
 END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_UpsertProduct]'
GO

  
  
ALTER PROCEDURE [dbo].[ZNode_UpsertProduct]  
(
 @ProductID int = 0,  
 @Name nvarchar(MAX) = null,  
 @ShortDescription nvarchar(MAX) = null,  
 @Description nvarchar(MAX) = null,  
 @FeaturesDesc nvarchar(MAX) = null,  
 @ProductNum nvarchar(100) = null,  
 @ProductTypeID int = null,  
 @RetailPrice money = null,  
 @SalePrice money = null,  
 @WholesalePrice money = null,  
 @ImageFile nvarchar(MAX) = null,  
 @ImageAltTag nvarchar(MAX) = null,  
 @Weight decimal(18, 2) = null,  
 @Length decimal(18, 2) = null,  
 @Width decimal(18, 2) = null,  
 @Height decimal(18, 2) = null,  
 @BeginActiveDate datetime = null,  
 @EndActiveDate datetime = null,  
 @DisplayOrder int = null,  
 @ActiveInd bit = null,  
 @CallForPricing bit = null,  
 @HomepageSpecial bit = 0,  
 @CategorySpecial bit = 0,  
 @InventoryDisplay tinyint = null,  
 @Keywords nvarchar(MAX) = null,  
 @ManufacturerID int = null,  
 @AdditionalInfoLink nvarchar(MAX) = null,  
 @AdditionalInfoLinkLabel nvarchar(MAX) = null,  
 @ShippingRuleTypeID int = null,  
 @ShippingRate money = null,  
 @SEOTitle nvarchar(MAX) = null,  
 @SEOKeywords nvarchar(MAX) = null,  
 @SEODescription nvarchar(MAX) = null,  
 @Custom1 nvarchar(MAX) = null,  
 @Custom2 nvarchar(MAX) = null,  
 @Custom3 nvarchar(MAX) = null,  
 @ShipEachItemSeparately bit = null,  
 @AllowBackOrder bit = null,  
 @BackOrderMsg nvarchar(MAX) = null,  
 @DropShipInd bit = null,  
 @DropShipEmailID nvarchar(MAX) = null,  
 @Specifications nvarchar(MAX) = null ,  
 @AdditionalInformation nvarchar(MAX) = null,  
 @InStockMsg nvarchar(MAX) =null ,  
 @OutOfStockMsg nvarchar(MAX) =null ,  
 @TrackInventoryInd bit = null ,  
 @DownloadLink nvarchar(MAX) = null,  
 @FreeShippingInd bit = null,  
 @NewProductInd bit = null,  
 @SEOURL nvarchar(100) = null,  
 @MaxQty int = null,  
 @ShipSeparately bit = null,  
 @FeaturedInd bit = null,  
 @WebServiceDownloadDte datetime = null,  
 @UpdateDte datetime = null,  
 @SupplierID int = null,  
 @RecurringBillingInd bit = null,  
 @RecurringBillingInstallmentInd bit = null,  
 @RecurringBillingPeriod nvarchar (50)  =null,  
 @RecurringBillingFrequency nvarchar(MAX) = null ,  
 @RecurringBillingTotalCycles int = null,  
 @RecurringBillingInitialAmount money = null,  
 @TaxClassID int =null,  
 @MinQty int  = null,  
 @ReviewStateID int = null,  
 @AffiliateUrl varchar (512) = null,  
 @IsShippable bit = null ,  
 @AccountID int = null,  
 @PortalID int = null ,
 @Franchisable bit =0,
 @SKU nvarchar(100) = null,
 @QuantityOnHand int = null
)  
AS   
BEGIN  
DECLARE @ExistingReviewStateID INT  
								
 IF(Exists(SELECT ProductID From ZNodeProduct WHERE ProductId = @ProductID))  
  BEGIN  
   SELECT @ExistingReviewStateID = ReviewStateID FROM ZNODEPRODUCT WHERE ProductId=@ProductID;
IF(@ReviewStateID  IS NULL)
BEGIN
SET @ExistingReviewStateID = @ExistingReviewStateID
END
ELSE
BEGIN
SET @ExistingReviewStateID = @ReviewStateID
END
   Exec ZNode_NT_ZNodeProduct_Update    
   @ProductID ,  
   @Name ,  
   @ShortDescription,  
   @Description,  
   @FeaturesDesc ,  
   @ProductNum,  
   @ProductTypeID,  
   @RetailPrice,  
   @SalePrice,  
   @WholesalePrice,  
   @ImageFile ,  
   @ImageAltTag,  
   @Weight,  
   @Length,  
   @Width,  
   @Height ,  
   @BeginActiveDate,  
   @EndActiveDate ,  
   @DisplayOrder,  
   @ActiveInd ,  
   @CallForPricing,  
   @HomepageSpecial,  
   @CategorySpecial,  
   @InventoryDisplay,  
   @Keywords,  
   @ManufacturerID,  
   @AdditionalInfoLink,  
   @AdditionalInfoLinkLabel,  
   @ShippingRuleTypeID,  
   @ShippingRate,  
   @SEOTitle ,  
   @SEOKeywords ,  
   @SEODescription ,  
   @Custom1,   
   @Custom2 ,  
   @Custom3,  
   @ShipEachItemSeparately,  
   @AllowBackOrder,  
   @BackOrderMsg,  
   @DropShipInd ,  
   @DropShipEmailID,  
   @Specifications,  
   @AdditionalInformation,  
   @InStockMsg ,  
   @OutOfStockMsg,  
   @TrackInventoryInd ,  
   @DownloadLink ,  
   @FreeShippingInd,  
   @NewProductInd,  
   @SEOURL ,  
   @MaxQty ,  
   @ShipSeparately ,  
   @FeaturedInd ,  
   @WebServiceDownloadDte,  
   @UpdateDte,  
   @SupplierID ,  
   @RecurringBillingInd ,  
   @RecurringBillingInstallmentInd ,  
   @RecurringBillingPeriod ,  
   @RecurringBillingFrequency,  
   @RecurringBillingTotalCycles ,  
   @RecurringBillingInitialAmount,  
   @TaxClassID,   
   @MinQty, 
   @ExistingReviewStateID,  					
   @AffiliateUrl ,  
   @IsShippable,  
   @AccountID,  
   @PortalID,  
   @Franchisable  
   
        
  END   
 ELSE  
  BEGIN   
   EXEC ZNode_NT_ZNodeSKUInventory_Insert null,@SKU,@QuantityOnHand,null

   Exec ZNode_NT_ZNodeProduct_Insert    
   @ProductID out,  
   @Name ,  
   @ShortDescription,  
   @Description,  
   @FeaturesDesc ,  
   @ProductNum,  
   @ProductTypeID,  
   @RetailPrice,  
   @SalePrice,  
   @WholesalePrice,  
   @ImageFile ,  
   @ImageAltTag,  
   @Weight,  
   @Length,  
   @Width,  
   @Height ,  
   @BeginActiveDate,  
   @EndActiveDate ,  
   @DisplayOrder,  
   @ActiveInd ,  
   @CallForPricing,  
   @HomepageSpecial,  
   @CategorySpecial,  
   @InventoryDisplay,  
   @Keywords,  
   @ManufacturerID,  
   @AdditionalInfoLink,  
   @AdditionalInfoLinkLabel,  
   @ShippingRuleTypeID,  
   @ShippingRate,  
   @SEOTitle ,  
   @SEOKeywords ,  
   @SEODescription ,  
   @Custom1,  
   @Custom2 ,  
   @Custom3,  
   @ShipEachItemSeparately,  
   @AllowBackOrder,  
   @BackOrderMsg,  
   @DropShipInd ,  
   @DropShipEmailID,  
   @Specifications,  
   @AdditionalInformation,  
   @InStockMsg ,  
   @OutOfStockMsg,  
   @TrackInventoryInd ,  
   @DownloadLink ,  
   @FreeShippingInd,  
   @NewProductInd,  
   @SEOURL ,  
   @MaxQty ,  
   @ShipSeparately ,  
   @FeaturedInd ,  
   @WebServiceDownloadDte,  
   @UpdateDte,  
   @SupplierID ,  
   @RecurringBillingInd ,  
   @RecurringBillingInstallmentInd ,  
   @RecurringBillingPeriod ,  
   @RecurringBillingFrequency,  
   @RecurringBillingTotalCycles ,  
   @RecurringBillingInitialAmount,  
   @TaxClassID,   
   @MinQty,  
   @ReviewStateID,  
   @AffiliateUrl ,  
   @IsShippable,  
   @AccountID,  
   @PortalID,  
   @Franchisable  
   
      EXEC	[dbo].[ZNode_NT_ZNodeSKU_Insert]
		@SKUID = null,
		@ProductID = @ProductID,
		@SKU = @SKU,
		@SupplierID = NULL,
		@Note = NULL,
		@WeightAdditional = NULL,
		@SKUPicturePath = NULL,
		@ImageAltTag = NULL,
		@DisplayOrder = NULL,
		@RetailPriceOverride = NULL,
		@SalePriceOverride = NULL,
		@WholesalePriceOverride = NULL,
		@RecurringBillingPeriod = NULL,
		@RecurringBillingFrequency = NULL,
		@RecurringBillingTotalCycles = NULL,
		@RecurringBillingInitialAmount = NULL,
		@ActiveInd = 1,
		@Custom1 = NULL,
		@Custom2 = NULL,
		@Custom3 = NULL,
		@WebServiceDownloadDte = NULL,
		@UpdateDte = null,
		@ExternalProductID = NULL,
		@ExternalProductAPIID = NULL

     
  END  
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_Reports]'
GO

ALTER PROCEDURE [dbo].[ZNode_Reports]  
    (  
      @Filter VARCHAR(MAX) = NULL,  
      @FromDate VARCHAR(MAX) = NULL,  
      @ToDate VARCHAR(MAX) = NULL,  
      @SupplierID VARCHAR(MAX) = NULL,  
      @Custom1 VARCHAR(MAX) = '',  
      @Custom2 VARCHAR(MAX) = ''  
    )  
AS   
    BEGIN                                        
        SET NOCOUNT ON ;                                        
        -- Report Name  
          
        DECLARE @Orders VARCHAR(100)= 'Orders'  
          
        DECLARE @Picklist VARCHAR(100)= 'Picklist'  
          
        DECLARE @EmailOptInCustomer VARCHAR(100)= 'EmailOptInCustomer'  
          
        DECLARE @Accounts VARCHAR(100)= 'Accounts'  
          
        DECLARE @ActivityLog VARCHAR(100)= 'ActivityLog'  
          
        DECLARE @ReOrder VARCHAR(100)= 'ReOrder'  
          
        DECLARE @ServiceRequest VARCHAR(100)= 'ServiceRequest'  
          
        DECLARE @SupplierList VARCHAR(100)= 'SupplierList'  
          
        -- Coupon Usage  
        DECLARE @CouponUsage VARCHAR(100)= 'CouponUsage'  
        DECLARE @CouponUsageByDay VARCHAR(100)= 'CouponUsageByDay'  
        DECLARE @CouponUsageByWeek VARCHAR(100)= 'CouponUsageByWeek'  
        DECLARE @CouponUsageByMonth VARCHAR(100)= 'CouponUsageByMonth'  
        DECLARE @CouponUsageByQuarter VARCHAR(100)= 'CouponUsageByQuarter'  
        DECLARE @CouponUsageByYear VARCHAR(100)= 'CouponUsageByYear'  
          
        -- Sales Tax  
        DECLARE @SalesTax VARCHAR(100)= 'SalesTax'  
        DECLARE @SalesTaxByMonth VARCHAR(100)= 'SalesTaxByMonth'  
        DECLARE @SalesTaxByQuarter VARCHAR(100)= 'SalesTaxByQuarter'  
          
        -- Frequent Customer  
        DECLARE @FrequentCustomer VARCHAR(100)= 'FrequentCustomer'  
        DECLARE @FrequentCustomerByDay VARCHAR(100)= 'FrequentCustomerByDay'  
        DECLARE @FrequentCustomerByWeek VARCHAR(100)= 'FrequentCustomerByWeek'  
        DECLARE @FrequentCustomerByMonth VARCHAR(100)= 'FrequentCustomerByMonth'  
        DECLARE @FrequentCustomerByQuarter VARCHAR(100)= 'FrequentCustomerByQuarter'  
        DECLARE @FrequentCustomerByYear VARCHAR(100)= 'FrequentCustomerByYear'  
          
        DECLARE @TopSpendingCustomer VARCHAR(100)= 'TopSpendingCustomer'  
        DECLARE @TopSpendingCustomerByDay VARCHAR(100)= 'TopSpendingCustomerByDay'  
        DECLARE @TopSpendingCustomerByWeek VARCHAR(100)= 'TopSpendingCustomerByWeek'  
        DECLARE @TopSpendingCustomerByMonth VARCHAR(100)= 'TopSpendingCustomerByMonth'  
        DECLARE @TopSpendingCustomerByQuarter VARCHAR(100)= 'TopSpendingCustomerByQuarter'  
        DECLARE @TopSpendingCustomerByYear VARCHAR(100)= 'TopSpendingCustomerByYear'  
          
        DECLARE @TopEarningProduct VARCHAR(100)= 'TopEarningProduct'  
        DECLARE @TopEarningProductByDay VARCHAR(100)= 'TopEarningProductByDay'  
        DECLARE @TopEarningProductByWeek VARCHAR(100)= 'TopEarningProductByWeek'  
        DECLARE @TopEarningProductByMonth VARCHAR(100)= 'TopEarningProductByMonth'  
        DECLARE @TopEarningProductByQuarter VARCHAR(100)= 'TopEarningProductByQuarter'  
        DECLARE @TopEarningProductByYear VARCHAR(100)= 'TopEarningProductByYear'  
          
        -- Best Seller   
        DECLARE @BestSeller VARCHAR(100)= 'BestSeller'  
        DECLARE @BestSellerByDay VARCHAR(100)= 'BestSellerByDay'  
        DECLARE @BestSellerByWeek VARCHAR(100)= 'BestSellerByWeek'  
        DECLARE @BestSellerByMonth VARCHAR(100)= 'BestSellerByMonth'  
        DECLARE @BestSellerByQuarter VARCHAR(100)= 'BestSellerByQuarter'  
        DECLARE @BestSellerByYear VARCHAR(100)= 'BestSellerByYear'  
          
        -- Affiliate Order   
        DECLARE @AffiliateOrder VARCHAR(100)= 'AffiliateOrder'  
        DECLARE @AffiliateOrderByDay VARCHAR(100)= 'AffiliateOrderByDay'  
        DECLARE @AffiliateOrderByWeek VARCHAR(100)= 'AffiliateOrderByWeek'  
        DECLARE @AffiliateOrderByMonth VARCHAR(100)= 'AffiliateOrderByMonth'  
        DECLARE @AffiliateOrderByQuarter VARCHAR(100)= 'AffiliateOrderByQuarter'  
        DECLARE @AffiliateOrderByYear VARCHAR(100)= 'AffiliateOrderByYear' 
        DECLARE @VendorRevenue VARCHAR(100)= 'VendorRevenue' 
        DECLARE @VendorProductRevenue VARCHAR(100)= 'VendorProductRevenue'
          
        -- Popular Search    
        DECLARE @PopularSearch VARCHAR(100)= 'PopularSearch'  
        DECLARE @PopularSearchByDay VARCHAR(100)= 'PopularSearchByDay'  
        DECLARE @PopularSearchByWeek VARCHAR(100)= 'PopularSearchByWeek'  
        DECLARE @PopularSearchByMonth VARCHAR(100)= 'PopularSearchByMonth'  
        DECLARE @PopularSearchByQuarter VARCHAR(100)= 'PopularSearchByQuarter'  
        DECLARE @PopularSearchByYear VARCHAR(100)= 'PopularSearchByYear'  
        
        -- Products sold on vendor sites.
        DECLARE @ProductSoldOnVendorSites VARCHAR(100)= 'ProductSoldOnVendorSites'  
        
                  
        DECLARE @Diff int ;                                        
        DECLARE @Diff2 int ;                                        
        DECLARE @StartDate DATETIME ;                                        
        DECLARE @Year NVARCHAR(MAX) ;  
        DECLARE @FinalDate NVARCHAR(MAX) ;       
        DECLARE @CompareDate DATETIME ;    
        DECLARE @CompareMonthDate DATETIME ;                                      
        DECLARE @CompareFinalDate NVARCHAR(MAX) ;  
        DECLARE @TodayDate DATETIME ;  
  
        SELECT  @TodayDate = GetDate() ;                                        
              
        SET @Diff = ( SELECT    DATEPART(dayofyear, @TodayDate)  
                    ) - ( SELECT    DATEPART(day, @TodayDate)  
                        ) ;                                          
        SET @Diff2 = @Diff + ( SELECT   DATEPART(day, @TodayDate)  
                             ) - 1 ;                                          
        SET @StartDate = ( SELECT   DateAdd(dd, -@Diff2, @TodayDate)  
                         ) ;                                          
        SET @FinalDate = ( SELECT   CONVERT(Varchar(10), @StartDate, 101)  
                         ) ;                                          
        SET @CompareDate = ( SELECT DateAdd(dd, 1, @TodayDate)  
                           ) ;                                          
        SET @CompareFinalDate = ( SELECT    CONVERT(Varchar(10), @CompareDate, 101)  
                                ) ;        
        SET @Year = YEAR(@TodayDate) ;                      
  
  
  IF @ToDate IS NOT NULL  
  BEGIN  
   SET @ToDate = (SELECT DateAdd(dd, 1, @ToDate));  
  END;     
    
        IF @Filter = '0'   
            BEGIN                            
                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate  
            END  
   
        IF @Filter = '1'   
            BEGIN                                                          
                SELECT  Prod.ProductId,  
                        Prod.[Name],  
                        Prod.ProductNum,  
                        Prod.RetailPrice,  
                        Prod.SalePrice,  
                        SUM(ord.Quantity)  
                FROM    ZNodeProduct Prod  
                        INNER JOIN ZNodeOrderLineItem ord ON Prod.ProductNum = ord.ProductNum  
                WHERE   LEN(ParentOrderLineItemID) = 0  
                GROUP BY Prod.ProductID,  
                        Prod.[Name],  
                        Prod.ProductNum,  
                        Prod.RetailPrice,  
                        Prod.SalePrice,  
                        Ord.ProductNum  
                HAVING  SUM(Quantity) > 0  
                ORDER BY SUM(ord.Quantity) DESC ;                            
    
                SET @Diff = ( SELECT    DATEPART(day, @TodayDate)  
                            ) - 1 ;                                        
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                          
  
                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate  
    
            END  
   
        IF @Filter = '2'   
            BEGIN  
                SET @StartDate = ( SELECT   DateAdd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareDate = ( SELECT DateAdd(dd, 1, @TodayDate)  
                                   ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
                                          
                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate  
            END  
  
        IF @Filter = '3'   
            BEGIN  
                SET @StartDate = @TodayDate ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                                                               
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                          
    
                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate  
            END  
   
        IF @Filter = @Orders   
            BEGIN  
  -- Custom 1: Portal Id  
  -- Custom 2: Order State  
                EXEC ZNode_ReportsCustom @FromDate, @ToDate, @Custom1,  
                    @Custom2  
            END  
   
        IF @Filter = @Picklist   
            BEGIN  
                EXEC ZNode_ReportsPicklist @Custom1  
            END  
   
        IF @Filter = @EmailOptInCustomer   
            BEGIN  
                EXEC dbo.ZNode_ReportsOptIn @Custom1  
            END  
   
        IF @Filter = @FrequentCustomer   
            BEGIN  
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder  
    SET @CompareDate = ( SELECT DateAdd(dd, 1, @CompareDate)) ;  
                SET @CompareFinalDate = ( SELECT Convert(varchar(10), @CompareDate, 101)) ;   
                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END  
  
  
        IF @Filter = @FrequentCustomerByDay   
            BEGIN  
                SET @StartDate = @TodayDate ;                 
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;       
   
                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
    
            END  
   
        IF @Filter = @FrequentCustomerByWeek   
            BEGIN                       
                SET @StartDate = ( SELECT   DateAdd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                             
    EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END  
              
        IF @Filter = @FrequentCustomerByMonth   
            BEGIN                                   
                SET @Diff = ( SELECT    DatePart(day, @TodayDate)  
                            ) - 1 ;                                          
                SET @StartDate = ( SELECT   DateAdd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                    
  
                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END  
   
        IF @Filter = @FrequentCustomerByQuarter   
            BEGIN                                   
               
				SET @Diff = (Select DatePart(QQ, GetDate())) ;  
				Set @FinalDate = Convert(datetime, 
						CASE WHEN @Diff = 1 THEN '01/01/' + @Year
							 WHEN @Diff = 2 THEN '04/01/' + @Year
							 WHEN @Diff = 3 THEN '07/01/' + @Year
							 WHEN @Diff = 4 THEN '10/01/' + @Year
						END
						)
				SET @CompareFinalDate = Convert(datetime,                                   
						 CASE WHEN @Diff = 1 THEN '03/31/' + @Year
						 WHEN @Diff = 2 THEN '06/30/' + @Year
						 WHEN @Diff = 3 THEN '09/30/' + @Year
						 WHEN @Diff = 4 THEN '12/31/' + @Year
						END
						)                            
	  
					EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
						@CompareFinalDate, @Custom1  
           
            END                       
  
        IF @Filter = @FrequentCustomerByYear   
            BEGIN                                       
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)  
                                     ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                          
  
                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
                    @CompareFinalDate,@Custom1  
            END  
       
        IF @Filter = @TopSpendingCustomer   
            BEGIN                             
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder  
    SET @CompareDate = ( SELECT DateAdd(dd, 1, @CompareDate)) ;  
                SET @CompareFinalDate = ( SELECT Convert(varchar(10), @CompareDate, 101)) ;   
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
                                    
-- For Most Volume Customers and day --                                          
        IF @Filter = @TopSpendingCustomerByDay   
            BEGIN                  
                SET @StartDate = @TodayDate ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                            ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                     
    
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
                                     
-- For Most Volume Customers and week --                                          
        IF @Filter = @TopSpendingCustomerByWeek   
            BEGIN                                  
                SET @StartDate = ( SELECT   Dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                         
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
          EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
    
                                    
-- For Most Volume Customers and month --                                          
        IF @Filter = @TopSpendingCustomerByMonth   
            BEGIN                                      
                SET @Diff = ( SELECT    Datepart(day, @TodayDate)  
                            ) - 1 ;                            
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
   
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END                                
             
-- For Most Volume Customers and quater--                                          
        IF @Filter = @TopSpendingCustomerByQuarter  
            BEGIN    
                                              
                SET @Diff = (Select DatePart(QQ, GetDate())) ;  
				Set @FinalDate = Convert(datetime, 
						CASE WHEN @Diff = 1 THEN '01/01/' + @Year
							 WHEN @Diff = 2 THEN '04/01/' + @Year
							 WHEN @Diff = 3 THEN '07/01/' + @Year
							 WHEN @Diff = 4 THEN '10/01/' + @Year
						END
						)
				SET @CompareFinalDate = Convert(datetime,                                   
						CASE WHEN @Diff = 1 THEN '03/31/' + @Year
						 WHEN @Diff = 2 THEN '06/30/' + @Year
						 WHEN @Diff = 3 THEN '09/30/' + @Year
						 WHEN @Diff = 4 THEN '12/31/' + @Year
						END
						)                            
	                         
  
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END                                    
                                    
-- For Most Volume Customers and year--                                          
        IF @Filter = @TopSpendingCustomerByYear   
            BEGIN                                     
                SET @Diff = ( SELECT    Datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    Datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   Datepart(day, @TodayDate)  
                               ) - 1 ;                                          
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                           
  
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
                                       
                                    
-- For Most Popular Product --                                          
        IF @Filter = @TopEarningProduct   
            BEGIN                                
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
   
            END                                    
                                    
-- For Most Popular Product and day --                                          
        IF @Filter = @TopEarningProductByDay   
            BEGIN  
    SET @StartDate = @TodayDate;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101))  
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;       
     
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                                    
                                    
-- For Most Popular Product and week --                                          
        IF @Filter = @TopEarningProductByWeek   
            BEGIN                            
                SET @StartDate = ( SELECT   Dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                                   
                                    
-- For Most Popular Product and month --                                          
        IF @Filter = @TopEarningProductByMonth   
            BEGIN                                       
                SET @Diff = ( SELECT    Datepart(day, @TodayDate)  
                            ) - 1 ;                 
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;  
    
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END  
                                    
                                    
-- For Most Popular Product and Quater--                      
        IF @Filter = @TopEarningProductByQuarter   
            BEGIN                            
                
                SET @Diff = (Select DatePart(QQ, GetDate())) ;  
				Set @FinalDate = Convert(datetime, 
						CASE WHEN @Diff = 1 THEN '01/01/' + @Year
							 WHEN @Diff = 2 THEN '04/01/' + @Year
							 WHEN @Diff = 3 THEN '07/01/' + @Year
							 WHEN @Diff = 4 THEN '10/01/' + @Year
						END
						)
				SET @CompareFinalDate = Convert(datetime,                                   
						 CASE WHEN @Diff = 1 THEN '03/31/' + @Year
						 WHEN @Diff = 2 THEN '06/30/' + @Year
						 WHEN @Diff = 3 THEN '09/30/' + @Year
						 WHEN @Diff = 4 THEN '12/31/' + @Year
						END
						)                            
	                                      
  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END  
   
-- For Most Popular Product and Year--                                          
        IF @Filter = @TopEarningProductByYear   
            BEGIN                                       
                SET @Diff = ( SELECT    Datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    Datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   Datepart(day, @TodayDate)  
                                     ) - 1 ;                                          
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                       
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                           
      
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                              
                            
-- For Best Sellers                              
        IF @Filter = @BestSeller   
            BEGIN                             
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                              
                            
-- For BestSellers and day --                                          
        IF @Filter = @BestSellerByDay   
            BEGIN                                       
                SET @StartDate = @TodayDate ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                     
   
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                                    
                                    
-- For BestSellers and week --                                          
        IF @Filter = @BestSellerByWeek   
            BEGIN                                       
                SET @StartDate = ( SELECT   Dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                    
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
     
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                                    
                                    
-- For BestSellers and month --                                          
        If @Filter = @BestSellerByMonth   
            BEGIN                                       
                SET @Diff = ( SELECT    datepart(day, @TodayDate)  
                            ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;              
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                                        
                                    
-- For BestSellers and Quater--                                          
        IF @Filter = @BestSellerByQuarter   
            BEGIN                                       
                SET @Diff = ( SELECT    Datepart(month, @TodayDate)  
                            ) - 4 ;                                          
                SET @StartDate = ( SELECT   Dateadd(month, -@Diff,  
                                                    @TodayDate + 1)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
  
            END                       
                                    
-- For BestSellers and Year--                      
        IF @Filter = @BestSellerByYear   
            BEGIN                                       
                SET @Diff = ( SELECT    Datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)  
                                     ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;    
  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
                  
            END                       
                            
-- For Most Popular Searches --                                          
        IF @Filter = @PopularSearch   
            BEGIN  
                SELECT  @FinalDate = MIN(CreateDte),  
                        @CompareFinalDate = MAX(CReateDte)  
                FROM    ZNodeActivityLog  
        EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate, @Custom1                                          
            END  
                                     
-- For Most Popular Searches and day --                                          
        IF @Filter = @PopularSearchByDay   
            BEGIN                                      
                SET @StartDate = @TodayDate ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                               
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                     
    
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate,@Custom1  
            END                                    
                                    
-- For Most Popular Searches and week --                                          
        IF @Filter = @PopularSearchByWeek   
            BEGIN                                      
                SET @StartDate = ( SELECT   dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
    
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate , @Custom1 
            END                                    
                                    
-- For Most Popular Searches and month --                                          
        If @Filter = @PopularSearchByMonth   
            BEGIN                                      
                SET @Diff = ( SELECT    datepart(day, @TodayDate)  
                            ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate, @Custom1  
            END  
  
                                    
-- For Most Popular Searches and quater--                                          
        IF ( @Filter = @PopularSearchByQuarter )   
            BEGIN                          
                SET @StartDate = CAST(YEAR(getdate()) AS VARCHAR(4))  
                    + CASE WHEN MONTH(getdate()) IN ( 1, 2, 3 ) THEN '/01/01'  
                           WHEN MONTH(getdate()) IN ( 4, 5, 6 ) THEN '/04/01'  
                           WHEN MONTH(getdate()) IN ( 7, 8, 9 ) THEN '/07/01'  
                           WHEN MONTH(getdate()) IN ( 10, 11, 12 )  
                           THEN '/10/01'  
                      END    
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareMonthDate = ( SELECT    dateadd(Month, 3,  
                                                            @StartDate)  
                                        ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareMonthDate, 101)  
                                        ) ;                                   
    
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate,@Custom1  
            END                                   
                                    
-- For Most Popular Searches and year--                                          
        IF @Filter = @PopularSearchByYear   
            BEGIN                                     
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)  
                                     ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                           
     
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate,@Custom1  
            END  
                       
-- For ActivityLog                             
        IF @Filter = @ActivityLog   
            BEGIN  
  -- Custom1 : ZnodeActivity log category  
  -- Custom2 : Znode Store name  
                EXEC ZNode_ReportsActivityLog @FromDate, @Custom1, @Custom2   
            END                            
                            
-- For Inventory Re-Orderlevel                            
        IF @Filter = @ReOrder   
            BEGIN                                  
                EXEC ZNode_ReportsReOrder @Custom1  
            END  
                                 
-- For Customer Feedback                            
        IF @Filter = @ServiceRequest   
            BEGIN                   
                EXEC ZNode_ReportsFeedback @Custom1  
            END                               
                                    
-- For Accounts                            
        IF @Filter = @Accounts   
            BEGIN                            
  -- Custom 1: Store name  
                EXEC ZNode_ReportsAccounts @FromDate, @ToDate, @Custom1 ;                                 
            END                            
                          
-- For Coupon Usage                            
        IF @Filter = @CouponUsage   
            BEGIN  
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder   
    SET @CompareDate = ( SELECT DateAdd(dd, 1, @CompareDate)) ;  
                SET @CompareFinalDate = ( SELECT Convert(varchar(10), @CompareDate, 101)) ;   
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END                            
                          
-- For Coupon Usage and day --                             
        IF @Filter = @CouponUsageByDay   
            BEGIN                                      
                SET @StartDate = @TodayDate ;        
                                                   
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                           
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                      
  
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END  
                          
-- For Coupon Usage and week --         
        IF @Filter = @CouponUsageByWeek   
            BEGIN                         
                SET @StartDate = ( SELECT   dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END                              
                                    
-- For Coupon Usage and month --                                          
        IF @Filter = @CouponUsageByMonth   
            BEGIN                                      
                SET @Diff = ( SELECT    datepart(day, @TodayDate)  
                            ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END                                                               
                                    
-- For Coupon Usage and quater--                                          
        IF @Filter = @CouponUsageByQuarter   
            BEGIN                                      
                SET @Diff = ( SELECT    datepart(month, @TodayDate)  
                            ) - 4 ;                                         
                SET @StartDate = ( SELECT   dateadd(month, -@Diff,  
                                                    @TodayDate + 1)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                   
  
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END                                
                                    
-- For Coupon Usage and year--                                          
        IF @Filter = @CouponUsageByYear   
            BEGIN                                     
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    datepart(day, @TodayDate)  
                                ) ;  
                set @Diff2 = @Diff  
                    + ( SELECT  datepart(day, ( select  getdate()  
                                              ))  
                      ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                           
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END                                
          
-- For Affiliate Customers                  
        IF @Filter = @AffiliateOrder   
            BEGIN              
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder  
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
                  
-- For Affiliate Customers by day --                                          
        IF @Filter = @AffiliateOrderByDay   
            BEGIN  
                SET @StartDate = @TodayDate ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                     
    
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END                                    
                                    
-- For Affiliate Customers by week --                                          
        IF @Filter = @AffiliateOrderByWeek   
            BEGIN  
                SET @StartDate = ( SELECT   Dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                             
  
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
                                    
-- For Affiliate Customers by month --                                        
        IF @Filter = @AffiliateOrderByMonth   
            BEGIN                           
                SET @Diff = ( SELECT    datepart(day, @TodayDate)  
                            ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                    
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END                                    
  
-- For Affiliate Customers by quater--                                          
        IF @Filter = @AffiliateOrderByQuarter   
            BEGIN                           
                
                SET @Diff = (Select DatePart(QQ, GetDate())) ;  
				Set @FinalDate = Convert(datetime, 
						CASE WHEN @Diff = 1 THEN '01/01/' + @Year
							 WHEN @Diff = 2 THEN '04/01/' + @Year
							 WHEN @Diff = 3 THEN '07/01/' + @Year
							 WHEN @Diff = 4 THEN '10/01/' + @Year
						END
						)
				SET @CompareFinalDate = Convert(datetime,                                   
						 CASE WHEN @Diff = 1 THEN '03/31/' + @Year
						 WHEN @Diff = 2 THEN '06/30/' + @Year
						 WHEN @Diff = 3 THEN '09/30/' + @Year
						 WHEN @Diff = 4 THEN '12/31/' + @Year
						END
						)    
  
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END                                                
                                    
-- For Affiliate Customers by year --                                          
        IF @Filter = @AffiliateOrderByYear   
            BEGIN                                      
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)  
                                     ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                          
    
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
                  
            END  
            
-- For Vendor Revenue                                       
        IF @Filter = @VendorRevenue   
            BEGIN                           
                EXEC ZNode_ReportVendorRevenue @FromDate, @ToDate,  @Custom1                   
            END       

-- For Vendor Revenue                                       
        IF @Filter = @VendorProductRevenue   
            BEGIN                           
                EXEC [ZNode_ReportVendorProductRevenue] @FromDate, @ToDate, @Custom1                  
            END             
                   
                        
                        
 --For Tax Detail and Year--                      
        IF @Filter = @SalesTax 
            BEGIN           
                EXEC ZNode_ReportsTaxFiltered @FinalDate, @CompareFinalDate,
                    'month',@Custom1
            END                 
                      
--For Tax Detail and Month--                     
        IF @Filter = @SalesTaxByMonth 
            BEGIN       
                SELECT  @FinalDate = convert(varchar(10), '01/01/' + @Year, 101)
                SELECT  @CompareFinalDate = convert(varchar(10), '12/31/'
                        + @Year, 101)         
		
                EXEC ZNode_ReportsTaxFiltered @FinalDate, @CompareFinalDate,
                    'month',@Custom1
            END                   
                         
--For Tax Detail and Quarter--                      
        IF @Filter = @SalesTaxByQuarter 
            BEGIN                    
                SELECT  @FinalDate = convert(varchar(10), '01/01/' + @Year, 101)
                SELECT  @CompareFinalDate = convert(varchar(10), '12/31/'
                        + @Year, 101)         
		
                EXEC ZNode_ReportsTaxFiltered @FinalDate, @CompareFinalDate,
                    'quarter',@Custom1
            END                   
                
-- For Supplier --                                            
        IF @Filter = @SupplierList 
            BEGIN                       
							                      
				-- Custom1 : Store name.
                EXEC ZNode_ReportsSupplierList @FromDate, @ToDate, @Custom1,
                    @SupplierID
            END               
            
        -- For product sold on vendor sites. --                                            
        IF @Filter = @ProductSoldOnVendorSites
            BEGIN                       
							                      
				-- Custom1 : Store name.
                EXEC ZNode_ReportsProductSoldOnVendorSites @FromDate, @ToDate, @Custom1
            END               
           
    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeSavedPaymentMethod_GetPaged]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeSavedPaymentMethod table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeSavedPaymentMethod_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[SavedPaymentMethodID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [SavedPaymentMethodID]'
				SET @SQL = @SQL + ', [AccountID]'
				SET @SQL = @SQL + ', [PaymentSettingID]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ', [PaymentProfileID]'
				SET @SQL = @SQL + ', [CustomerProfileID]'
				SET @SQL = @SQL + ', [ShippingProfileID]'
				SET @SQL = @SQL + ', [AuthCode]'
				SET @SQL = @SQL + ', [ReferenceID]'
				SET @SQL = @SQL + ', [ExpirationDate]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeSavedPaymentMethod]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [SavedPaymentMethodID],'
				SET @SQL = @SQL + ' [AccountID],'
				SET @SQL = @SQL + ' [PaymentSettingID],'
				SET @SQL = @SQL + ' [Name],'
				SET @SQL = @SQL + ' [PaymentProfileID],'
				SET @SQL = @SQL + ' [CustomerProfileID],'
				SET @SQL = @SQL + ' [ShippingProfileID],'
				SET @SQL = @SQL + ' [AuthCode],'
				SET @SQL = @SQL + ' [ReferenceID],'
				SET @SQL = @SQL + ' [ExpirationDate]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeSavedPaymentMethod]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeSavedPaymentMethod_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeSavedPaymentMethod table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeSavedPaymentMethod_Insert
(

	@SavedPaymentMethodID int    OUTPUT,

	@AccountID int   ,

	@PaymentSettingID int   ,

	@Name nvarchar (MAX)  ,

	@PaymentProfileID nvarchar (MAX)  ,

	@CustomerProfileID nvarchar (MAX)  ,

	@ShippingProfileID nvarchar (MAX)  ,

	@AuthCode nvarchar (MAX)  ,

	@ReferenceID nvarchar (MAX)  ,

	@ExpirationDate datetime   
)
AS


				
				INSERT INTO [dbo].[ZNodeSavedPaymentMethod]
					(
					[AccountID]
					,[PaymentSettingID]
					,[Name]
					,[PaymentProfileID]
					,[CustomerProfileID]
					,[ShippingProfileID]
					,[AuthCode]
					,[ReferenceID]
					,[ExpirationDate]
					)
				VALUES
					(
					@AccountID
					,@PaymentSettingID
					,@Name
					,@PaymentProfileID
					,@CustomerProfileID
					,@ShippingProfileID
					,@AuthCode
					,@ReferenceID
					,@ExpirationDate
					)
				
				-- Get the identity value
				SET @SavedPaymentMethodID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeSavedPaymentMethod_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeSavedPaymentMethod table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeSavedPaymentMethod_Update
(

	@SavedPaymentMethodID int   ,

	@AccountID int   ,

	@PaymentSettingID int   ,

	@Name nvarchar (MAX)  ,

	@PaymentProfileID nvarchar (MAX)  ,

	@CustomerProfileID nvarchar (MAX)  ,

	@ShippingProfileID nvarchar (MAX)  ,

	@AuthCode nvarchar (MAX)  ,

	@ReferenceID nvarchar (MAX)  ,

	@ExpirationDate datetime   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeSavedPaymentMethod]
				SET
					[AccountID] = @AccountID
					,[PaymentSettingID] = @PaymentSettingID
					,[Name] = @Name
					,[PaymentProfileID] = @PaymentProfileID
					,[CustomerProfileID] = @CustomerProfileID
					,[ShippingProfileID] = @ShippingProfileID
					,[AuthCode] = @AuthCode
					,[ReferenceID] = @ReferenceID
					,[ExpirationDate] = @ExpirationDate
				WHERE
[SavedPaymentMethodID] = @SavedPaymentMethodID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeSavedPaymentMethod_GetByAccountID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeSavedPaymentMethod table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeSavedPaymentMethod_GetByAccountID
(

	@AccountID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[SavedPaymentMethodID],
					[AccountID],
					[PaymentSettingID],
					[Name],
					[PaymentProfileID],
					[CustomerProfileID],
					[ShippingProfileID],
					[AuthCode],
					[ReferenceID],
					[ExpirationDate]
				FROM
					[dbo].[ZNodeSavedPaymentMethod]
				WHERE
					[AccountID] = @AccountID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeSavedPaymentMethod_GetByPaymentSettingID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeSavedPaymentMethod table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeSavedPaymentMethod_GetByPaymentSettingID
(

	@PaymentSettingID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[SavedPaymentMethodID],
					[AccountID],
					[PaymentSettingID],
					[Name],
					[PaymentProfileID],
					[CustomerProfileID],
					[ShippingProfileID],
					[AuthCode],
					[ReferenceID],
					[ExpirationDate]
				FROM
					[dbo].[ZNodeSavedPaymentMethod]
				WHERE
					[PaymentSettingID] = @PaymentSettingID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeSavedPaymentMethod_GetBySavedPaymentMethodID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeSavedPaymentMethod table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeSavedPaymentMethod_GetBySavedPaymentMethodID
(

	@SavedPaymentMethodID int   
)
AS


				SELECT
					[SavedPaymentMethodID],
					[AccountID],
					[PaymentSettingID],
					[Name],
					[PaymentProfileID],
					[CustomerProfileID],
					[ShippingProfileID],
					[AuthCode],
					[ReferenceID],
					[ExpirationDate]
				FROM
					[dbo].[ZNodeSavedPaymentMethod]
				WHERE
					[SavedPaymentMethodID] = @SavedPaymentMethodID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeSavedPaymentMethod_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeSavedPaymentMethod table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeSavedPaymentMethod_Find
(

	@SearchUsingOR bit   = null ,

	@SavedPaymentMethodID int   = null ,

	@AccountID int   = null ,

	@PaymentSettingID int   = null ,

	@Name nvarchar (MAX)  = null ,

	@PaymentProfileID nvarchar (MAX)  = null ,

	@CustomerProfileID nvarchar (MAX)  = null ,

	@ShippingProfileID nvarchar (MAX)  = null ,

	@AuthCode nvarchar (MAX)  = null ,

	@ReferenceID nvarchar (MAX)  = null ,

	@ExpirationDate datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SavedPaymentMethodID]
	, [AccountID]
	, [PaymentSettingID]
	, [Name]
	, [PaymentProfileID]
	, [CustomerProfileID]
	, [ShippingProfileID]
	, [AuthCode]
	, [ReferenceID]
	, [ExpirationDate]
    FROM
	[dbo].[ZNodeSavedPaymentMethod]
    WHERE 
	 ([SavedPaymentMethodID] = @SavedPaymentMethodID OR @SavedPaymentMethodID IS NULL)
	AND ([AccountID] = @AccountID OR @AccountID IS NULL)
	AND ([PaymentSettingID] = @PaymentSettingID OR @PaymentSettingID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([PaymentProfileID] = @PaymentProfileID OR @PaymentProfileID IS NULL)
	AND ([CustomerProfileID] = @CustomerProfileID OR @CustomerProfileID IS NULL)
	AND ([ShippingProfileID] = @ShippingProfileID OR @ShippingProfileID IS NULL)
	AND ([AuthCode] = @AuthCode OR @AuthCode IS NULL)
	AND ([ReferenceID] = @ReferenceID OR @ReferenceID IS NULL)
	AND ([ExpirationDate] = @ExpirationDate OR @ExpirationDate IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SavedPaymentMethodID]
	, [AccountID]
	, [PaymentSettingID]
	, [Name]
	, [PaymentProfileID]
	, [CustomerProfileID]
	, [ShippingProfileID]
	, [AuthCode]
	, [ReferenceID]
	, [ExpirationDate]
    FROM
	[dbo].[ZNodeSavedPaymentMethod]
    WHERE 
	 ([SavedPaymentMethodID] = @SavedPaymentMethodID AND @SavedPaymentMethodID is not null)
	OR ([AccountID] = @AccountID AND @AccountID is not null)
	OR ([PaymentSettingID] = @PaymentSettingID AND @PaymentSettingID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([PaymentProfileID] = @PaymentProfileID AND @PaymentProfileID is not null)
	OR ([CustomerProfileID] = @CustomerProfileID AND @CustomerProfileID is not null)
	OR ([ShippingProfileID] = @ShippingProfileID AND @ShippingProfileID is not null)
	OR ([AuthCode] = @AuthCode AND @AuthCode is not null)
	OR ([ReferenceID] = @ReferenceID AND @ReferenceID is not null)
	OR ([ExpirationDate] = @ExpirationDate AND @ExpirationDate is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAccount_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeAccount table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAccount_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[AccountID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [AccountID]'
				SET @SQL = @SQL + ', [ParentAccountID]'
				SET @SQL = @SQL + ', [UserID]'
				SET @SQL = @SQL + ', [ExternalAccountNo]'
				SET @SQL = @SQL + ', [CompanyName]'
				SET @SQL = @SQL + ', [AccountTypeID]'
				SET @SQL = @SQL + ', [ProfileID]'
				SET @SQL = @SQL + ', [AccountProfileCode]'
				SET @SQL = @SQL + ', [SubAccountLimit]'
				SET @SQL = @SQL + ', [Description]'
				SET @SQL = @SQL + ', [CreateUser]'
				SET @SQL = @SQL + ', [CreateDte]'
				SET @SQL = @SQL + ', [UpdateUser]'
				SET @SQL = @SQL + ', [UpdateDte]'
				SET @SQL = @SQL + ', [ActiveInd]'
				SET @SQL = @SQL + ', [Website]'
				SET @SQL = @SQL + ', [Source]'
				SET @SQL = @SQL + ', [ReferralAccountID]'
				SET @SQL = @SQL + ', [ReferralStatus]'
				SET @SQL = @SQL + ', [Custom1]'
				SET @SQL = @SQL + ', [Custom2]'
				SET @SQL = @SQL + ', [Custom3]'
				SET @SQL = @SQL + ', [EmailOptIn]'
				SET @SQL = @SQL + ', [WebServiceDownloadDte]'
				SET @SQL = @SQL + ', [ReferralCommission]'
				SET @SQL = @SQL + ', [ReferralCommissionTypeID]'
				SET @SQL = @SQL + ', [TaxID]'
				SET @SQL = @SQL + ', [Email]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeAccount]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [AccountID],'
				SET @SQL = @SQL + ' [ParentAccountID],'
				SET @SQL = @SQL + ' [UserID],'
				SET @SQL = @SQL + ' [ExternalAccountNo],'
				SET @SQL = @SQL + ' [CompanyName],'
				SET @SQL = @SQL + ' [AccountTypeID],'
				SET @SQL = @SQL + ' [ProfileID],'
				SET @SQL = @SQL + ' [AccountProfileCode],'
				SET @SQL = @SQL + ' [SubAccountLimit],'
				SET @SQL = @SQL + ' [Description],'
				SET @SQL = @SQL + ' [CreateUser],'
				SET @SQL = @SQL + ' [CreateDte],'
				SET @SQL = @SQL + ' [UpdateUser],'
				SET @SQL = @SQL + ' [UpdateDte],'
				SET @SQL = @SQL + ' [ActiveInd],'
				SET @SQL = @SQL + ' [Website],'
				SET @SQL = @SQL + ' [Source],'
				SET @SQL = @SQL + ' [ReferralAccountID],'
				SET @SQL = @SQL + ' [ReferralStatus],'
				SET @SQL = @SQL + ' [Custom1],'
				SET @SQL = @SQL + ' [Custom2],'
				SET @SQL = @SQL + ' [Custom3],'
				SET @SQL = @SQL + ' [EmailOptIn],'
				SET @SQL = @SQL + ' [WebServiceDownloadDte],'
				SET @SQL = @SQL + ' [ReferralCommission],'
				SET @SQL = @SQL + ' [ReferralCommissionTypeID],'
				SET @SQL = @SQL + ' [TaxID],'
				SET @SQL = @SQL + ' [Email]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeAccount]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodePortal table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[PortalID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [PortalID]'
				SET @SQL = @SQL + ', [CompanyName]'
				SET @SQL = @SQL + ', [StoreName]'
				SET @SQL = @SQL + ', [LogoPath]'
				SET @SQL = @SQL + ', [UseSSL]'
				SET @SQL = @SQL + ', [AdminEmail]'
				SET @SQL = @SQL + ', [SalesEmail]'
				SET @SQL = @SQL + ', [CustomerServiceEmail]'
				SET @SQL = @SQL + ', [SalesPhoneNumber]'
				SET @SQL = @SQL + ', [CustomerServicePhoneNumber]'
				SET @SQL = @SQL + ', [ImageNotAvailablePath]'
				SET @SQL = @SQL + ', [MaxCatalogDisplayColumns]'
				SET @SQL = @SQL + ', [MaxCatalogDisplayItems]'
				SET @SQL = @SQL + ', [MaxCatalogCategoryDisplayThumbnails]'
				SET @SQL = @SQL + ', [MaxCatalogItemSmallThumbnailWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemSmallWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemMediumWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemThumbnailWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemLargeWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemCrossSellWidth]'
				SET @SQL = @SQL + ', [ShowSwatchInCategory]'
				SET @SQL = @SQL + ', [ShowAlternateImageInCategory]'
				SET @SQL = @SQL + ', [ActiveInd]'
				SET @SQL = @SQL + ', [SMTPServer]'
				SET @SQL = @SQL + ', [SMTPUserName]'
				SET @SQL = @SQL + ', [SMTPPassword]'
				SET @SQL = @SQL + ', [SMTPPort]'
				SET @SQL = @SQL + ', [SiteWideBottomJavascript]'
				SET @SQL = @SQL + ', [SiteWideTopJavascript]'
				SET @SQL = @SQL + ', [OrderReceiptAffiliateJavascript]'
				SET @SQL = @SQL + ', [SiteWideAnalyticsJavascript]'
				SET @SQL = @SQL + ', [GoogleAnalyticsCode]'
				SET @SQL = @SQL + ', [UPSUserName]'
				SET @SQL = @SQL + ', [UPSPassword]'
				SET @SQL = @SQL + ', [UPSKey]'
				SET @SQL = @SQL + ', [ShippingOriginZipCode]'
				SET @SQL = @SQL + ', [MasterPage]'
				SET @SQL = @SQL + ', [ShopByPriceMin]'
				SET @SQL = @SQL + ', [ShopByPriceMax]'
				SET @SQL = @SQL + ', [ShopByPriceIncrement]'
				SET @SQL = @SQL + ', [FedExAccountNumber]'
				SET @SQL = @SQL + ', [FedExMeterNumber]'
				SET @SQL = @SQL + ', [FedExProductionKey]'
				SET @SQL = @SQL + ', [FedExSecurityCode]'
				SET @SQL = @SQL + ', [FedExCSPKey]'
				SET @SQL = @SQL + ', [FedExCSPPassword]'
				SET @SQL = @SQL + ', [FedExClientProductId]'
				SET @SQL = @SQL + ', [FedExClientProductVersion]'
				SET @SQL = @SQL + ', [FedExDropoffType]'
				SET @SQL = @SQL + ', [FedExPackagingType]'
				SET @SQL = @SQL + ', [FedExUseDiscountRate]'
				SET @SQL = @SQL + ', [FedExAddInsurance]'
				SET @SQL = @SQL + ', [ShippingOriginAddress1]'
				SET @SQL = @SQL + ', [ShippingOriginAddress2]'
				SET @SQL = @SQL + ', [ShippingOriginCity]'
				SET @SQL = @SQL + ', [ShippingOriginStateCode]'
				SET @SQL = @SQL + ', [ShippingOriginCountryCode]'
				SET @SQL = @SQL + ', [ShippingOriginPhone]'
				SET @SQL = @SQL + ', [CurrencyTypeID]'
				SET @SQL = @SQL + ', [WeightUnit]'
				SET @SQL = @SQL + ', [DimensionUnit]'
				SET @SQL = @SQL + ', [EmailListLogin]'
				SET @SQL = @SQL + ', [EmailListPassword]'
				SET @SQL = @SQL + ', [EmailListDefaultList]'
				SET @SQL = @SQL + ', [ShippingTaxable]'
				SET @SQL = @SQL + ', [DefaultOrderStateID]'
				SET @SQL = @SQL + ', [DefaultReviewStatus]'
				SET @SQL = @SQL + ', [DefaultAnonymousProfileID]'
				SET @SQL = @SQL + ', [DefaultRegisteredProfileID]'
				SET @SQL = @SQL + ', [InclusiveTax]'
				SET @SQL = @SQL + ', [SeoDefaultProductTitle]'
				SET @SQL = @SQL + ', [SeoDefaultProductDescription]'
				SET @SQL = @SQL + ', [SeoDefaultProductKeyword]'
				SET @SQL = @SQL + ', [SeoDefaultCategoryTitle]'
				SET @SQL = @SQL + ', [SeoDefaultCategoryDescription]'
				SET @SQL = @SQL + ', [SeoDefaultCategoryKeyword]'
				SET @SQL = @SQL + ', [SeoDefaultContentTitle]'
				SET @SQL = @SQL + ', [SeoDefaultContentDescription]'
				SET @SQL = @SQL + ', [SeoDefaultContentKeyword]'
				SET @SQL = @SQL + ', [TimeZoneOffset]'
				SET @SQL = @SQL + ', [LocaleID]'
				SET @SQL = @SQL + ', [SplashCategoryID]'
				SET @SQL = @SQL + ', [SplashImageFile]'
				SET @SQL = @SQL + ', [MobileTheme]'
				SET @SQL = @SQL + ', [PersistentCartEnabled]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePortal]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [PortalID],'
				SET @SQL = @SQL + ' [CompanyName],'
				SET @SQL = @SQL + ' [StoreName],'
				SET @SQL = @SQL + ' [LogoPath],'
				SET @SQL = @SQL + ' [UseSSL],'
				SET @SQL = @SQL + ' [AdminEmail],'
				SET @SQL = @SQL + ' [SalesEmail],'
				SET @SQL = @SQL + ' [CustomerServiceEmail],'
				SET @SQL = @SQL + ' [SalesPhoneNumber],'
				SET @SQL = @SQL + ' [CustomerServicePhoneNumber],'
				SET @SQL = @SQL + ' [ImageNotAvailablePath],'
				SET @SQL = @SQL + ' [MaxCatalogDisplayColumns],'
				SET @SQL = @SQL + ' [MaxCatalogDisplayItems],'
				SET @SQL = @SQL + ' [MaxCatalogCategoryDisplayThumbnails],'
				SET @SQL = @SQL + ' [MaxCatalogItemSmallThumbnailWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemSmallWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemMediumWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemThumbnailWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemLargeWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemCrossSellWidth],'
				SET @SQL = @SQL + ' [ShowSwatchInCategory],'
				SET @SQL = @SQL + ' [ShowAlternateImageInCategory],'
				SET @SQL = @SQL + ' [ActiveInd],'
				SET @SQL = @SQL + ' [SMTPServer],'
				SET @SQL = @SQL + ' [SMTPUserName],'
				SET @SQL = @SQL + ' [SMTPPassword],'
				SET @SQL = @SQL + ' [SMTPPort],'
				SET @SQL = @SQL + ' [SiteWideBottomJavascript],'
				SET @SQL = @SQL + ' [SiteWideTopJavascript],'
				SET @SQL = @SQL + ' [OrderReceiptAffiliateJavascript],'
				SET @SQL = @SQL + ' [SiteWideAnalyticsJavascript],'
				SET @SQL = @SQL + ' [GoogleAnalyticsCode],'
				SET @SQL = @SQL + ' [UPSUserName],'
				SET @SQL = @SQL + ' [UPSPassword],'
				SET @SQL = @SQL + ' [UPSKey],'
				SET @SQL = @SQL + ' [ShippingOriginZipCode],'
				SET @SQL = @SQL + ' [MasterPage],'
				SET @SQL = @SQL + ' [ShopByPriceMin],'
				SET @SQL = @SQL + ' [ShopByPriceMax],'
				SET @SQL = @SQL + ' [ShopByPriceIncrement],'
				SET @SQL = @SQL + ' [FedExAccountNumber],'
				SET @SQL = @SQL + ' [FedExMeterNumber],'
				SET @SQL = @SQL + ' [FedExProductionKey],'
				SET @SQL = @SQL + ' [FedExSecurityCode],'
				SET @SQL = @SQL + ' [FedExCSPKey],'
				SET @SQL = @SQL + ' [FedExCSPPassword],'
				SET @SQL = @SQL + ' [FedExClientProductId],'
				SET @SQL = @SQL + ' [FedExClientProductVersion],'
				SET @SQL = @SQL + ' [FedExDropoffType],'
				SET @SQL = @SQL + ' [FedExPackagingType],'
				SET @SQL = @SQL + ' [FedExUseDiscountRate],'
				SET @SQL = @SQL + ' [FedExAddInsurance],'
				SET @SQL = @SQL + ' [ShippingOriginAddress1],'
				SET @SQL = @SQL + ' [ShippingOriginAddress2],'
				SET @SQL = @SQL + ' [ShippingOriginCity],'
				SET @SQL = @SQL + ' [ShippingOriginStateCode],'
				SET @SQL = @SQL + ' [ShippingOriginCountryCode],'
				SET @SQL = @SQL + ' [ShippingOriginPhone],'
				SET @SQL = @SQL + ' [CurrencyTypeID],'
				SET @SQL = @SQL + ' [WeightUnit],'
				SET @SQL = @SQL + ' [DimensionUnit],'
				SET @SQL = @SQL + ' [EmailListLogin],'
				SET @SQL = @SQL + ' [EmailListPassword],'
				SET @SQL = @SQL + ' [EmailListDefaultList],'
				SET @SQL = @SQL + ' [ShippingTaxable],'
				SET @SQL = @SQL + ' [DefaultOrderStateID],'
				SET @SQL = @SQL + ' [DefaultReviewStatus],'
				SET @SQL = @SQL + ' [DefaultAnonymousProfileID],'
				SET @SQL = @SQL + ' [DefaultRegisteredProfileID],'
				SET @SQL = @SQL + ' [InclusiveTax],'
				SET @SQL = @SQL + ' [SeoDefaultProductTitle],'
				SET @SQL = @SQL + ' [SeoDefaultProductDescription],'
				SET @SQL = @SQL + ' [SeoDefaultProductKeyword],'
				SET @SQL = @SQL + ' [SeoDefaultCategoryTitle],'
				SET @SQL = @SQL + ' [SeoDefaultCategoryDescription],'
				SET @SQL = @SQL + ' [SeoDefaultCategoryKeyword],'
				SET @SQL = @SQL + ' [SeoDefaultContentTitle],'
				SET @SQL = @SQL + ' [SeoDefaultContentDescription],'
				SET @SQL = @SQL + ' [SeoDefaultContentKeyword],'
				SET @SQL = @SQL + ' [TimeZoneOffset],'
				SET @SQL = @SQL + ' [LocaleID],'
				SET @SQL = @SQL + ' [SplashCategoryID],'
				SET @SQL = @SQL + ' [SplashImageFile],'
				SET @SQL = @SQL + ' [MobileTheme],'
				SET @SQL = @SQL + ' [PersistentCartEnabled]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePortal]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeOrderLineItem table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[OrderLineItemID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [OrderLineItemID]'
				SET @SQL = @SQL + ', [OrderID]'
				SET @SQL = @SQL + ', [ShipmentID]'
				SET @SQL = @SQL + ', [ProductNum]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ', [Description]'
				SET @SQL = @SQL + ', [Quantity]'
				SET @SQL = @SQL + ', [Price]'
				SET @SQL = @SQL + ', [Weight]'
				SET @SQL = @SQL + ', [SKU]'
				SET @SQL = @SQL + ', [ParentOrderLineItemID]'
				SET @SQL = @SQL + ', [OrderLineItemRelationshipTypeID]'
				SET @SQL = @SQL + ', [DownloadLink]'
				SET @SQL = @SQL + ', [DiscountAmount]'
				SET @SQL = @SQL + ', [ShipSeparately]'
				SET @SQL = @SQL + ', [ShipDate]'
				SET @SQL = @SQL + ', [ReturnDate]'
				SET @SQL = @SQL + ', [ShippingCost]'
				SET @SQL = @SQL + ', [PromoDescription]'
				SET @SQL = @SQL + ', [SalesTax]'
				SET @SQL = @SQL + ', [VAT]'
				SET @SQL = @SQL + ', [GST]'
				SET @SQL = @SQL + ', [PST]'
				SET @SQL = @SQL + ', [HST]'
				SET @SQL = @SQL + ', [TransactionNumber]'
				SET @SQL = @SQL + ', [PaymentStatusID]'
				SET @SQL = @SQL + ', [TrackingNumber]'
				SET @SQL = @SQL + ', [AutoGeneratedTracking]'
				SET @SQL = @SQL + ', [Custom1]'
				SET @SQL = @SQL + ', [Custom2]'
				SET @SQL = @SQL + ', [Custom3]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeOrderLineItem]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [OrderLineItemID],'
				SET @SQL = @SQL + ' [OrderID],'
				SET @SQL = @SQL + ' [ShipmentID],'
				SET @SQL = @SQL + ' [ProductNum],'
				SET @SQL = @SQL + ' [Name],'
				SET @SQL = @SQL + ' [Description],'
				SET @SQL = @SQL + ' [Quantity],'
				SET @SQL = @SQL + ' [Price],'
				SET @SQL = @SQL + ' [Weight],'
				SET @SQL = @SQL + ' [SKU],'
				SET @SQL = @SQL + ' [ParentOrderLineItemID],'
				SET @SQL = @SQL + ' [OrderLineItemRelationshipTypeID],'
				SET @SQL = @SQL + ' [DownloadLink],'
				SET @SQL = @SQL + ' [DiscountAmount],'
				SET @SQL = @SQL + ' [ShipSeparately],'
				SET @SQL = @SQL + ' [ShipDate],'
				SET @SQL = @SQL + ' [ReturnDate],'
				SET @SQL = @SQL + ' [ShippingCost],'
				SET @SQL = @SQL + ' [PromoDescription],'
				SET @SQL = @SQL + ' [SalesTax],'
				SET @SQL = @SQL + ' [VAT],'
				SET @SQL = @SQL + ' [GST],'
				SET @SQL = @SQL + ' [PST],'
				SET @SQL = @SQL + ' [HST],'
				SET @SQL = @SQL + ' [TransactionNumber],'
				SET @SQL = @SQL + ' [PaymentStatusID],'
				SET @SQL = @SQL + ' [TrackingNumber],'
				SET @SQL = @SQL + ' [AutoGeneratedTracking],'
				SET @SQL = @SQL + ' [Custom1],'
				SET @SQL = @SQL + ' [Custom2],'
				SET @SQL = @SQL + ' [Custom3]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeOrderLineItem]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetImageType]'
GO
SET ANSI_NULLS ON
GO


ALTER PROCEDURE [dbo].[ZNode_GetImageType]
@ProductId VARCHAR (MAX)='', 
@ProductNum VARCHAR (MAX)='', 
@ProductName VARCHAR (MAX)='', 
@Vendor VARCHAR (MAX)='', 
@VendorId VARCHAR (MAX)='', 
@ProductStatus VARCHAR (MAX)='', 
@COId VARCHAR (MAX)=''
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @Query NVARCHAR(3000)
	DECLARE @WhereClause NVARCHAR(1000)
	
    SET @Query=' SELECT   
		I.ProductImageID,
		I.ProductID,		
		I.Name,
		I.ImageFile,
		I.ImageAltTag,
		I.AlternateThumbnailImageFile,
		I.ActiveInd,
		I.ShowOnCategoryPage,
		I.ProductImageTypeID,
		I.DisplayOrder,
		I.ReviewStateID,
		T.Name AS ''ImageTypeName'',
		ISNULL(P.AccountID, 0) AccountID
    FROM     
		ZNodeProductImage AS I
	LEFT OUTER JOIN 
		ZNodeProductImageType T		
	ON I.ProductImageTypeID=T.ProductImageTypeID    
		LEFT OUTER JOIN ZNodeProduct P
	ON P.ProductID=I.ProductID 
		LEFT OUTER JOIN ZNodeProductReviewState R
	ON I.ReviewStateID=R.ReviewStateID '
	
	SET @WhereClause='';
    IF(LEN(@ProductName)>0)
		BEGIN
			SET @ProductName = '%' + @ProductName + '%'
			SET @WhereClause=@WhereClause+' AND P.Name LIKE @ProductName'
		END
	IF(LEN(@ProductId)>0 AND @ProductId<>'0')
		BEGIN	
			SET @WhereClause=@WhereClause+' AND I.ProductID = @ProductId';
		END		
	IF(LEN(@ProductNum)>0)
		BEGIN	
			SET @WhereClause=@WhereClause+' AND P.ProductNum = @ProductNum';
		END
    
    IF(LEN(@ProductStatus)>0)
		BEGIN	
			SET @WhereClause=@WhereClause+' AND I.ReviewStateId = @ProductStatus AND I.ReviewStateId IS NOT NULL';
		END
	IF(LEN(@VendorId)>0)
		BEGIN	
			SET @WhereClause=@WhereClause+' AND P.AccountId = @VendorId';
		END		
		
	IF(LEN(@Vendor)>0)
		BEGIN	
			SET @WhereClause=@WhereClause+' AND P.AccountId IN 
					(SELECT 
						AccountID 
					FROM 
						ZNodeAddress 
					WHERE FirstName LIKE @Vendor
						OR LastName LIKE @Vendor)';
		END		
    IF(LEN(@COId)>0)
		BEGIN	
			SET @WhereClause=@WhereClause+' AND I.ProductID IN (SELECT ProductID FROM ZNodeSKU WHERE SKU=@COId)'
		END
	
     
     
     IF(RTRIM(LTRIM(@WhereClause))<>'')
		BEGIN
		-- Replace the left most AND with WHERE clause.
			SET @WhereClause=' WHERE '+ SUBSTRING(LTRIM(@WhereClause), 4, LEN(@WhereClause));
		END
     
     SET @Query =@Query + @WhereClause +' ORDER BY DisplayOrder ASC';     
     
	 -- PRINT @Query;
	 EXEC SP_EXECUTESQL @Query, N'@ProductName nvarchar(max),@ProductId varchar(max),@ProductNum varchar(max),@Vendor nvarchar(max),@VendorId varchar(max),@ProductStatus varchar(max),@COId varchar(max)',
    @ProductName = @ProductName,@ProductId = @ProductId,@ProductNum = @ProductNum,@Vendor = @Vendor,@VendorId = @VendorId,@ProductStatus = @ProductStatus,@COId = @COId;	
    
    
    
END  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchVendorProduct]'
GO


ALTER PROCEDURE [dbo].[ZNode_SearchVendorProduct]
@ProductName VARCHAR (MAX)='', 
@ProductId VARCHAR (MAX)='', 
@Vendor VARCHAR (MAX)='', 
@VendorId VARCHAR (MAX)='', 
@ProductStatus VARCHAR (MAX)='', 
@COId VARCHAR (MAX)=''
AS
BEGIN
	DECLARE @Query NVARCHAR(3000)
	DECLARE @WhereClause NVARCHAR(1000)
	
    SET @Query='SELECT P.ProductID,
           ImageFile,
           P.Name,
           P.ProductNum,
           RetailPrice,
           SalePrice,
           P.DisplayOrder,
           P.ActiveInd,
           (FirstName + '' ''+ LastName + '' '' + CompanyName) AS Vendor,
           (SELECT TOP 1 QuantityOnHand
            FROM   ZNodeSKUInventory AS SkuInventory
            WHERE  SkuInventory.SKU IN (SELECT SKU
                                        FROM   ZNodeSKU
                                        WHERE  ProductID = P.ProductID)) AS ''QuantityOnHand'',
           P.ReviewStateId,
           R.ReviewStateName AS ProductStatus
           FROM   
			ZNodeProduct AS P LEFT OUTER JOIN ZNodeProductReviewState R ON P.ReviewStateID=R.ReviewStateID 
			LEFT OUTER JOIN ZNodeAddress A ON P.AccountID=A.AccountID AND A.IsDefaultBilling=1   '
    
    SET @WhereClause='';
    IF(LEN(@ProductName)>0)
		BEGIN
	 	    SET @ProductName = '%' + @ProductName + '%'
			SET @WhereClause=@WhereClause+' AND P.Name LIKE @ProductName'

		END
	IF(LEN(@ProductId)>0)
		BEGIN	
			SET @WhereClause=@WhereClause+' AND P.ProductNum = @ProductId';
		END
    
    IF(LEN(@ProductStatus)>0)
		BEGIN	
			SET @WhereClause=@WhereClause+' AND P.ReviewStateId = @ProductStatus';
		END
	IF(LEN(@VendorId)>0)
		BEGIN	
			SET @WhereClause=@WhereClause+' AND P.AccountId = @VendorId';
		END		
		
	IF(LEN(@Vendor)>0)
		BEGIN	
			SET @Vendor='%'+ @Vendor + '%'
			SET @WhereClause=@WhereClause+' AND P.AccountId IN 
					(SELECT 
						AccountID 
					FROM 
						ZNodeAddress 
					WHERE FirstName LIKE @Vendor OR LastName LIKE @Vendor)';
		END		
    IF(LEN(@COId)>0)
		BEGIN	
			SET @WhereClause = @WhereClause+' AND P.ProductID IN (SELECT ProductID FROM ZNodeSKU WHERE SKU = @COId)'
		END
     
     
     IF(RTRIM(LTRIM(@WhereClause))<>'')
		BEGIN
		-- Replace the left most AND with WHERE clause.
			SET @WhereClause=' WHERE '+ SUBSTRING(LTRIM(@WhereClause), 4, LEN(@WhereClause));
		END
     
     SET @Query =@Query + @WhereClause;     
     
     EXEC SP_EXECUTESQL @Query, N'@ProductName nvarchar(max),@ProductId varchar(max),@Vendor nvarchar(max),@VendorId varchar(max),@ProductStatus INT,@COId varchar(max)',
    @ProductName = @ProductName,@ProductId = @ProductId,@Vendor = @Vendor,@VendorId = @VendorId,@ProductStatus = @ProductStatus,@COId = @COId;	
           
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMultifront_GetPaged]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeMultifront table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMultifront_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[ID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [ID]'
				SET @SQL = @SQL + ', [MajorVersion]'
				SET @SQL = @SQL + ', [MinorVersion]'
				SET @SQL = @SQL + ', [Build]'
				SET @SQL = @SQL + ', [LVK]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeMultifront]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [ID],'
				SET @SQL = @SQL + ' [MajorVersion],'
				SET @SQL = @SQL + ' [MinorVersion],'
				SET @SQL = @SQL + ' [Build],'
				SET @SQL = @SQL + ' [LVK]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeMultifront]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeSavedPaymentMethod_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeSavedPaymentMethod table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeSavedPaymentMethod_Get_List

AS


				
				SELECT
					[SavedPaymentMethodID],
					[AccountID],
					[PaymentSettingID],
					[Name],
					[PaymentProfileID],
					[CustomerProfileID],
					[ShippingProfileID],
					[AuthCode],
					[ReferenceID],
					[ExpirationDate]
				FROM
					[dbo].[ZNodeSavedPaymentMethod]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeMessageConfig table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[MessageID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [MessageID]'
				SET @SQL = @SQL + ', [Key]'
				SET @SQL = @SQL + ', [PortalID]'
				SET @SQL = @SQL + ', [LocaleID]'
				SET @SQL = @SQL + ', [Description]'
				SET @SQL = @SQL + ', [Value]'
				SET @SQL = @SQL + ', [MessageTypeID]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeMessageConfig]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [MessageID],'
				SET @SQL = @SQL + ' [Key],'
				SET @SQL = @SQL + ' [PortalID],'
				SET @SQL = @SQL + ' [LocaleID],'
				SET @SQL = @SQL + ' [Description],'
				SET @SQL = @SQL + ' [Value],'
				SET @SQL = @SQL + ' [MessageTypeID]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeMessageConfig]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeParentChildProduct_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeParentChildProduct table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeParentChildProduct_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[ParentChildProductID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [ParentChildProductID]'
				SET @SQL = @SQL + ', [ParentProductID]'
				SET @SQL = @SQL + ', [ChildProductID]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeParentChildProduct]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [ParentChildProductID],'
				SET @SQL = @SQL + ' [ParentProductID],'
				SET @SQL = @SQL + ' [ChildProductID]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeParentChildProduct]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentToken_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodePaymentToken table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentToken_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[PaymentTokenID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [PaymentTokenID]'
				SET @SQL = @SQL + ', [AccountID]'
				SET @SQL = @SQL + ', [PaymentSettingID]'
				SET @SQL = @SQL + ', [CreditCardDescription]'
				SET @SQL = @SQL + ', [CardAuthCode]'
				SET @SQL = @SQL + ', [CardTransactionID]'
				SET @SQL = @SQL + ', [CardExp]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePaymentToken]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [PaymentTokenID],'
				SET @SQL = @SQL + ' [AccountID],'
				SET @SQL = @SQL + ' [PaymentSettingID],'
				SET @SQL = @SQL + ' [CreditCardDescription],'
				SET @SQL = @SQL + ' [CardAuthCode],'
				SET @SQL = @SQL + ' [CardTransactionID],'
				SET @SQL = @SQL + ' [CardExp]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePaymentToken]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeOrderLineItemRelationshipType_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeOrderLineItemRelationshipType table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItemRelationshipType_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[OrderLineItemRelationshipTypeID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [OrderLineItemRelationshipTypeID]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeOrderLineItemRelationshipType]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [OrderLineItemRelationshipTypeID],'
				SET @SQL = @SQL + ' [Name]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeOrderLineItemRelationshipType]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePaymentTokenAuthorize_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodePaymentTokenAuthorize table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePaymentTokenAuthorize_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[PaymentTokenAuthorizeID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [PaymentTokenAuthorizeID]'
				SET @SQL = @SQL + ', [PaymentTokenID]'
				SET @SQL = @SQL + ', [PaymentProfileID]'
				SET @SQL = @SQL + ', [CustomerProfileID]'
				SET @SQL = @SQL + ', [ShippingProfileID]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePaymentTokenAuthorize]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [PaymentTokenAuthorizeID],'
				SET @SQL = @SQL + ' [PaymentTokenID],'
				SET @SQL = @SQL + ' [PaymentProfileID],'
				SET @SQL = @SQL + ' [CustomerProfileID],'
				SET @SQL = @SQL + ' [ShippingProfileID]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePaymentTokenAuthorize]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeTrackingOutbound_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeTrackingOutbound table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeTrackingOutbound_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[TrackingOutboundID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [TrackingOutboundID]'
				SET @SQL = @SQL + ', [ProductID]'
				SET @SQL = @SQL + ', [Date]'
				SET @SQL = @SQL + ', [FromUrl]'
				SET @SQL = @SQL + ', [ToUrl]'
				SET @SQL = @SQL + ', [EventName]'
				SET @SQL = @SQL + ', [Price]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeTrackingOutbound]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [TrackingOutboundID],'
				SET @SQL = @SQL + ' [ProductID],'
				SET @SQL = @SQL + ' [Date],'
				SET @SQL = @SQL + ' [FromUrl],'
				SET @SQL = @SQL + ' [ToUrl],'
				SET @SQL = @SQL + ' [EventName],'
				SET @SQL = @SQL + ' [Price]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeTrackingOutbound]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCart_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeSavedCart table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCart_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[SavedCartID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [SavedCartID]'
				SET @SQL = @SQL + ', [CookieMappingID]'
				SET @SQL = @SQL + ', [CreatedDate]'
				SET @SQL = @SQL + ', [LastUpdatedDate]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeSavedCart]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [SavedCartID],'
				SET @SQL = @SQL + ' [CookieMappingID],'
				SET @SQL = @SQL + ' [CreatedDate],'
				SET @SQL = @SQL + ' [LastUpdatedDate]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeSavedCart]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSavedCartLineItem_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeSavedCartLineItem table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeSavedCartLineItem_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[SavedCartLineItemID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [SavedCartLineItemID]'
				SET @SQL = @SQL + ', [SavedCartID]'
				SET @SQL = @SQL + ', [SKUID]'
				SET @SQL = @SQL + ', [Quantity]'
				SET @SQL = @SQL + ', [ParentSavedCartLineItemID]'
				SET @SQL = @SQL + ', [OrderLineItemRelationshipTypeID]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeSavedCartLineItem]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [SavedCartLineItemID],'
				SET @SQL = @SQL + ' [SavedCartID],'
				SET @SQL = @SQL + ' [SKUID],'
				SET @SQL = @SQL + ' [Quantity],'
				SET @SQL = @SQL + ' [ParentSavedCartLineItemID],'
				SET @SQL = @SQL + ' [OrderLineItemRelationshipTypeID]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeSavedCartLineItem]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeAddress_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeAddress table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeAddress_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[AddressID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [AddressID]'
				SET @SQL = @SQL + ', [FirstName]'
				SET @SQL = @SQL + ', [MiddleName]'
				SET @SQL = @SQL + ', [LastName]'
				SET @SQL = @SQL + ', [CompanyName]'
				SET @SQL = @SQL + ', [Street]'
				SET @SQL = @SQL + ', [Street1]'
				SET @SQL = @SQL + ', [City]'
				SET @SQL = @SQL + ', [StateCode]'
				SET @SQL = @SQL + ', [PostalCode]'
				SET @SQL = @SQL + ', [CountryCode]'
				SET @SQL = @SQL + ', [PhoneNumber]'
				SET @SQL = @SQL + ', [IsDefaultBilling]'
				SET @SQL = @SQL + ', [AccountID]'
				SET @SQL = @SQL + ', [IsDefaultShipping]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeAddress]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [AddressID],'
				SET @SQL = @SQL + ' [FirstName],'
				SET @SQL = @SQL + ' [MiddleName],'
				SET @SQL = @SQL + ' [LastName],'
				SET @SQL = @SQL + ' [CompanyName],'
				SET @SQL = @SQL + ' [Street],'
				SET @SQL = @SQL + ' [Street1],'
				SET @SQL = @SQL + ' [City],'
				SET @SQL = @SQL + ' [StateCode],'
				SET @SQL = @SQL + ' [PostalCode],'
				SET @SQL = @SQL + ' [CountryCode],'
				SET @SQL = @SQL + ' [PhoneNumber],'
				SET @SQL = @SQL + ' [IsDefaultBilling],'
				SET @SQL = @SQL + ' [AccountID],'
				SET @SQL = @SQL + ' [IsDefaultShipping],'
				SET @SQL = @SQL + ' [Name]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeAddress]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeCookieMapping_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeCookieMapping table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeCookieMapping_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[CookieMappingID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [CookieMappingID]'
				SET @SQL = @SQL + ', [AccountID]'
				SET @SQL = @SQL + ', [PortalID]'
				SET @SQL = @SQL + ', [CreatedDate]'
				SET @SQL = @SQL + ', [LastUpdatedDate]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeCookieMapping]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [CookieMappingID],'
				SET @SQL = @SQL + ' [AccountID],'
				SET @SQL = @SQL + ' [PortalID],'
				SET @SQL = @SQL + ' [CreatedDate],'
				SET @SQL = @SQL + ' [LastUpdatedDate]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeCookieMapping]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeMessageType_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeMessageType table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeMessageType_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[MessageTypeID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [MessageTypeID]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ', [DisplayOrder]'
				SET @SQL = @SQL + ', [IsActive]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeMessageType]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [MessageTypeID],'
				SET @SQL = @SQL + ' [Name],'
				SET @SQL = @SQL + ' [DisplayOrder],'
				SET @SQL = @SQL + ' [IsActive]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeMessageType]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeAddress]'
GO
ALTER TABLE [dbo].[ZNodeAddress] ADD
CONSTRAINT [FK_ZNodeAddress_ZNodeAccount] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[ZNodeAccount] ([AccountID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeSavedCart]'
GO
ALTER TABLE [dbo].[ZNodeSavedCart] ADD
CONSTRAINT [FK_ZNodeSavedCart_ZNodeCookieMapping] FOREIGN KEY ([CookieMappingID]) REFERENCES [dbo].[ZNodeCookieMapping] ([CookieMappingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeCookieMapping]'
GO
ALTER TABLE [dbo].[ZNodeCookieMapping] ADD
CONSTRAINT [FK_ZNodeCookieMapping_ZNodeAccount] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[ZNodeAccount] ([AccountID]),
CONSTRAINT [FK_ZNodeCookieMapping_ZNodePortal] FOREIGN KEY ([PortalID]) REFERENCES [dbo].[ZNodePortal] ([PortalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeMessageConfig]'
GO
ALTER TABLE [dbo].[ZNodeMessageConfig] ADD
CONSTRAINT [FK_ZNodeMessageConfig_ZNodeMessageType] FOREIGN KEY ([MessageTypeID]) REFERENCES [dbo].[ZNodeMessageType] ([MessageTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeOrderLineItem]'
GO
ALTER TABLE [dbo].[ZNodeOrderLineItem] ADD
CONSTRAINT [FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType] FOREIGN KEY ([OrderLineItemRelationshipTypeID]) REFERENCES [dbo].[ZNodeOrderLineItemRelationshipType] ([OrderLineItemRelationshipTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeSavedCartLineItem]'
GO
ALTER TABLE [dbo].[ZNodeSavedCartLineItem] ADD
CONSTRAINT [FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType] FOREIGN KEY ([OrderLineItemRelationshipTypeID]) REFERENCES [dbo].[ZNodeOrderLineItemRelationshipType] ([OrderLineItemRelationshipTypeID]),
CONSTRAINT [FK_ZNodeSavedCartLineItem_ZNodeSavedCart] FOREIGN KEY ([SavedCartID]) REFERENCES [dbo].[ZNodeSavedCart] ([SavedCartID]),
CONSTRAINT [FK_ZNodeSavedCartLineItem_ZNodeSavedCartLineItem] FOREIGN KEY ([ParentSavedCartLineItemID]) REFERENCES [dbo].[ZNodeSavedCartLineItem] ([SavedCartLineItemID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeParentChildProduct]'
GO
ALTER TABLE [dbo].[ZNodeParentChildProduct] ADD
CONSTRAINT [FK_ZNodeParentChildProduct_ZNodeProduct] FOREIGN KEY ([ParentProductID]) REFERENCES [dbo].[ZNodeProduct] ([ProductID]),
CONSTRAINT [FK_ZNodeParentChildProduct_ZNodeProduct1] FOREIGN KEY ([ChildProductID]) REFERENCES [dbo].[ZNodeProduct] ([ProductID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodePaymentTokenAuthorize]'
GO
ALTER TABLE [dbo].[ZNodePaymentTokenAuthorize] ADD
CONSTRAINT [FK_ZNodePaymentTokenAuthorize_ZNodePaymentToken] FOREIGN KEY ([PaymentTokenID]) REFERENCES [dbo].[ZNodePaymentToken] ([PaymentTokenID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodePaymentToken]'
GO
ALTER TABLE [dbo].[ZNodePaymentToken] ADD
CONSTRAINT [FK_ZNodePaymentToken_ZNodeAccount] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[ZNodeAccount] ([AccountID]),
CONSTRAINT [FK_ZNodePaymentToken_ZNodePaymentSetting] FOREIGN KEY ([PaymentSettingID]) REFERENCES [dbo].[ZNodePaymentSetting] ([PaymentSettingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeReferralCommission]'
GO
ALTER TABLE [dbo].[ZNodeReferralCommission] ADD
CONSTRAINT [FK_ZnodeReferralCommission_ZnodeReferralCommissionType] FOREIGN KEY ([ReferralCommissionTypeID]) REFERENCES [dbo].[ZNodeReferralCommissionType] ([ReferralCommissionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeTrackingOutbound]'
GO
ALTER TABLE [dbo].[ZNodeTrackingOutbound] ADD
CONSTRAINT [FK_ZNodeTrackingOutbound_ZNodeProduct] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[ZNodeProduct] ([ProductID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
update znodeportal set PersistentCartEnabled=1
GO
ALTER PROCEDURE [dbo].[ZNode_GetLocaleListByAddOnValueId]
(  
@AddOnValueId INT,  
@PortalId INT  
)  
AS  
BEGIN  
  
SELECT  
 DISTINCT  
 ZAV.AddOnValueId,  
 ZL.*,  
 ZC.Name,  
 ZPOC.PortalID  
FROM ZNodeAddOnValue ZAV  
INNER JOIN ZNodeAddOn ZA ON ZAV.AddOnID = ZA.AddOnID  
INNER JOIN ZNodeLocale ZL ON ZL.LocaleID = ZA.LocaleID  
LEFT JOIN ZNodeProductAddOn ZPA ON ZPA.AddOnID = ZA.AddOnID  
LEFT JOIN ZNodeProductCategory ZPC ON ZPC.ProductID = ZPA.ProductID  
LEFT JOIN ZNodeCategoryNode ZCN ON ZPC.CategoryID = ZCN.CategoryID  
LEFT JOIN ZNodePortalCatalog ZPOC ON ZPOC.CatalogID = ZCN.CatalogID  
LEFT JOIN ZNodeCatalog ZC ON ZPOC.CatalogID = ZC.CatalogID  

WHERE  
 ZAV.SKU IN (SELECT ZAOI.SKU from ZNodeSKUInventory ZAOI  
      INNER JOIN ZNodeAddOnValue ZAV1 ON ZAV1.SKU = ZAOI.SKU  
      WHERE ZAV1.AddOnValueID = @AddOnValueId)  
 AND (ZPOC.PortalID = @PortalId OR ZPOC.PortalID IS NULL)
  
ORDER BY ZL.LocaleDescription    
END  
  
  
  
GO
ALTER PROCEDURE [dbo].[ZNode_ReportsPicklist]
    @PortalId VARCHAR(10) = ''
AS 
    BEGIN                                      
        SET NOCOUNT ON ;          

		-- Select PickList report data
        SELECT  O.[OrderID],
                O.[BillingFirstName],
                O.[BillingLastName],
                O.[OrderDate],
                P.[StoreName] AS 'StoreName'
        FROM    ZNodeOrder O
                INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
        WHERE   O.OrderStateID = ( SELECT   OrderStateId
                                   FROM     ZnodeOrderState
                                   WHERE    OrderStateName = 'Submitted'
                                 )
                AND (P.PortalID = @PortalId OR @PortalId = '0');	
                
         -- Select picklist Sub-Report items
        SELECT  O.OrderID,
                O.SKU,
                O.Quantity,
                O.Name,
                O.[Description],
                O.Price
        FROM    ZNodeOrderLineItem O
        WHERE   OrderID IN (
                SELECT  OrderId
                FROM    ZNodeOrder O
                        INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
                WHERE   O.OrderStateID = ( SELECT   OrderStateID
                                           FROM     ZnodeOrderState
                                           WHERE    OrderStateName = 'Submitted'
                                         )
                        AND (P.PortalID = @PortalId OR @PortalId = '0'))	
                                
    END


GO
update znodemultifront set MajorVersion='6',MinorVersion='4',Build='0'
GO




/*
Run this script on:

        ZNODE-BUILD\SQLEXPRESS2008.znode_multifront_v6.1.2    -  This database will be modified

to synchronize it with:

        ZNODE-BUILD\SQLEXPRESS2008.znode_multifront

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 6/28/2010 1:52:53 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[ZNodeTagGroupCategory]'
GO
ALTER TABLE [dbo].[ZNodeTagGroupCategory] DROP
CONSTRAINT [FK__ZNodeTagGroupCategory__ZNodeCategory],
CONSTRAINT [FK__ZNodeTagGroupCategory__ZNodeTagGroup]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[ZNodeTagProductSKU]'
GO
ALTER TABLE [dbo].[ZNodeTagProductSKU] DROP
CONSTRAINT [FK__ZNodeTagP__Produ__6FDBB715],
CONSTRAINT [FK__ZNodeTagP__Produ__70CFDB4E],
CONSTRAINT [FK__ZNodeTagP__SKUID__71C3FF87],
CONSTRAINT [FK__ZNodeTagP__TagID__72B823C0],
CONSTRAINT [FK__ZNodeTagP__TagID__73AC47F9]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[ZNode_GetProductsByCatlogID]'
GO
DROP PROCEDURE [dbo].[ZNode_GetProductsByCatlogID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeTagGroup]'
GO
ALTER TABLE [dbo].[ZNodeTagGroup] ADD
[DisplayOrder] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTagGroup_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeTagGroup table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTagGroup_Get_List

AS


				
				SELECT
					[TagGroupID],
					[TagGroupLabel],
					[ControlTypeID],
					[CatalogID],
					[DisplayOrder]
				FROM
					[dbo].[ZNodeTagGroup]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeSKU_GetBySKUProductID]'
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeSKU table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodeSKU_GetBySKUProductID]
(

	@SKU nvarchar (100)  ,

	@ProductID int   
)
AS


				SELECT
					[SKUID],
					[ProductID],
					[SKU],
					[SupplierID],
					[Note],
					[WeightAdditional],
					[SKUPicturePath],
					[ImageAltTag],
					[DisplayOrder],
					[RetailPriceOverride],
					[SalePriceOverride],
					[WholesalePriceOverride],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[ActiveInd],
					[Custom1],
					[Custom2],
					[Custom3],
					[WebServiceDownloadDte],
					[UpdateDte]
				FROM
					[dbo].[ZNodeSKU]
				WHERE
					[SKU] = @SKU
					AND [ProductID] = @ProductID
				SELECT @@ROWCOUNT
					
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeTagGroup_GetByCatalogID]'
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeTagGroup table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodeTagGroup_GetByCatalogID]
(

	@CatalogID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[TagGroupID],
					[TagGroupLabel],
					[ControlTypeID],
					[CatalogID],
					[DisplayOrder]
				FROM
					[dbo].[ZNodeTagGroup]
				WHERE
					[CatalogID] = @CatalogID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeTagProductSKU_GetByTagIDProductID]'
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeTagProductSKU table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodeTagProductSKU_GetByTagIDProductID]
(

	@TagID int   ,

	@ProductID int   
)
AS


				SELECT
					[TagProductSKUID],
					[TagID],
					[ProductID],
					[SKUID]
				FROM
					[dbo].[ZNodeTagProductSKU]
				WHERE
					[TagID] = @TagID
					AND [ProductID] = @ProductID
				SELECT @@ROWCOUNT
					
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTagGroup_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeTagGroup table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTagGroup_Insert
(

	@TagGroupID int    OUTPUT,

	@TagGroupLabel nvarchar (MAX)  ,

	@ControlTypeID int   ,

	@CatalogID int   ,

	@DisplayOrder int   
)
AS


				
				INSERT INTO [dbo].[ZNodeTagGroup]
					(
					[TagGroupLabel]
					,[ControlTypeID]
					,[CatalogID]
					,[DisplayOrder]
					)
				VALUES
					(
					@TagGroupLabel
					,@ControlTypeID
					,@CatalogID
					,@DisplayOrder
					)
				
				-- Get the identity value
				SET @TagGroupID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SC_Splitter]'
GO
SET ANSI_NULLS ON
GO

ALTER FUNCTION [dbo].[SC_Splitter](@Text varchar(8000), @Delimiter varchar(20) = ' ')
RETURNS @Strings TABLE
( 
  Position int IDENTITY PRIMARY KEY,
  Value varchar(8000),
  RowNumber INT
)
AS
BEGIN

DECLARE @RowIndex INT
DECLARE @Index INT 

SET @RowIndex=1;
SET @Index = -1;

WHILE (LEN(@Text) > 0) 
  BEGIN  
		SET @Index = CHARINDEX(@Delimiter , @Text)  
		IF (@Index = 0) AND (LEN(@Text) > 0)  
		  BEGIN   
			INSERT INTO @Strings VALUES (@Text, @RowIndex)
			  BREAK  
		  END  
		IF (@Index > 1)  
		  BEGIN   
			INSERT INTO @Strings VALUES (LEFT(@Text, @Index - 1),@RowIndex)   
			SET @Text = RIGHT(@Text, (LEN(@Text) - @Index))  
		  END  
		ELSE 
		  SET @Text = RIGHT(@Text, (LEN(@Text) - @Index)) 
		
		-- Increse the row index 
		SET @RowIndex=@RowIndex+1;
    END
  RETURN
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTagGroup_Update]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeTagGroup table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTagGroup_Update
(

	@TagGroupID int   ,

	@TagGroupLabel nvarchar (MAX)  ,

	@ControlTypeID int   ,

	@CatalogID int   ,

	@DisplayOrder int   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeTagGroup]
				SET
					[TagGroupLabel] = @TagGroupLabel
					,[ControlTypeID] = @ControlTypeID
					,[CatalogID] = @CatalogID
					,[DisplayOrder] = @DisplayOrder
				WHERE
[TagGroupID] = @TagGroupID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetTagsByCategories]'
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[ZNode_GetTagsByCategories]
(@CategoryID int,
 @mode int,
 @CatalogID int = 0)
AS
-- MODE is zero, then Tags list out those associated with Categories
if(@mode = 0)
BEGIN
	SELECT
	ZNodeTagGroup.TagGroupID,
	ZNodeTagGroup.TagGroupLabel,
	ZNodeTag.TagID,
	ZNodeTag.TagGroupID,
	ZNodeTag.TagName,
	ZNodeTag.TagDisplayOrder,
	ZNodeTagProductSKU.TagProductSKUID,
	ZNodeTagProductSKU.TagID,
	ZNodeTagProductSKU.ProductID,
	ZNodeTagProductSKU.SKUID,
	ZNodeTagGroupCategory.TagGroupID,
	ZNodeTagGroupCategory.CategoryID,
	ZNodeTagGroupCategory.CategoryDisplayOrder
	FROM
	ZNodeTagGroup 
	INNER JOIN ZNodeTag ON  ZNodeTagGroup.TagGroupID = ZNodeTag.TagGroupID 
	INNER JOIN ZNodeTagProductSKU ON ZNodeTag.TagID = ZNodeTagProductSKU.TagID 
	INNER JOIN ZNodeTagGroupCategory ON ZNodeTagGroup.TagGroupID = ZNodeTagGroupCategory.TagGroupID
	WHERE (ZNodeTagGroupCategory.CategoryID = @CategoryID OR @CategoryID = 0) AND (ZNodeTagGroup.CatalogID = @CatalogID OR @CatalogID = 0)
END
ELSE
-- MODE is not zero, then Tags list out those not associated with Categories
BEGIN
	SELECT
	ZNodeTagGroup.TagGroupID,
	ZNodeTagGroup.TagGroupLabel,
	ZNodeTag.TagID,
	ZNodeTag.TagGroupID,
	ZNodeTag.TagName,
	ZNodeTag.TagDisplayOrder,
	ZNodeTagProductSKU.TagProductSKUID,
	ZNodeTagProductSKU.TagID,
	ZNodeTagProductSKU.ProductID,
	ZNodeTagProductSKU.SKUID
	FROM
	ZNodeTagGroup 
	INNER JOIN ZNodeTag ON ZNodeTagGroup.TagGroupID = ZNodeTag.TagGroupID
	INNER JOIN ZNodeTagProductSKU ON ZNodeTag.TagID = ZNodeTagProductSKU.TagID
	WHERE (ZNodeTagGroup.CatalogID = @CatalogID OR @CatalogID = 0)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[TagProductView]'
GO

CREATE VIEW [dbo].[TagProductView]
AS
SELECT TagID, ProductId
FROM dbo.ZNodeTagProductSKU
WHERE ProductId IS NOT NULL
UNION
SELECT TPS.TagId, S.ProductId
FROM dbo.ZNodeTagProductSku TPS
INNER JOIN dbo.ZNodeSku S
ON S.Skuid = TPS.Skuid
WHERE TPS.Skuid IS NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTag_GetByTagGroupID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeTag table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTag_GetByTagGroupID
(

	@TagGroupID int   
)
AS


				SELECT
					[TagID],
					[TagGroupID],
					[TagName],
					[TagDisplayOrder],
					[IconPath]
				FROM
					[dbo].[ZNodeTag]
				WHERE
					[TagGroupID] = @TagGroupID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_WS_GetProductAttributesByID_XML]'
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[ZNode_WS_GetProductAttributesByID_XML](@ProductID INT = NULL)  
AS              
 BEGIN              
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON       
      
   SELECT Attribute.SkuId  
      ,Attribute.ProductId    
      ,Attribute.Sku          
      ,Attribute.Note  
      ,ZNodeSKUInventory.QuantityOnHand   
      ,ZNodeSKUInventory.ReorderLevel  
      ,Attribute.WeightAdditional  
      ,Attribute.SKUPicturePath "SkuPicturePath" 
      ,Attribute.DisplayOrder  
      ,Attribute.RetailPriceOverride  
      ,Attribute.SalePriceOverride  
      ,Attribute.WholesalePriceOverride  
      ,Attribute.ActiveInd FROM ZNodeSKU Attribute INNER JOIN ZNodeSKUInventory ON ZNodeSKUInventory.SKU = Attribute.SKU WHERE Attribute.ProductID = @ProductID FOR XML PATH('Attribute')    
   
 END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchTagGroup]'
GO

ALTER PROCEDURE [dbo].[ZNode_SearchTagGroup] 
(
	@NAME VARCHAR(MAX),
	@CatalogID int = null
)  
AS             
BEGIN     
   
 IF(@CatalogID > 0)
	SELECT 
		TagGroupId
		,TagGroupLabel
		,CatalogId
		,ControlTypeID
		,DisplayOrder 
	FROM 
		ZNodeTagGroup      
	WHERE 
		[TagGroupLabel] LIKE '%'+ @Name +'%' AND CatalogId = @CatalogID
ELSE
	SELECT 
		TagGroupId
		,TagGroupLabel
		,CatalogId
		,ControlTypeID
		,DisplayOrder 
	FROM 
		ZNodeTagGroup      
	WHERE 
		[TagGroupLabel] LIKE '%'+ @Name +'%'	
END  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTagGroup_GetByControlTypeID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeTagGroup table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTagGroup_GetByControlTypeID
(

	@ControlTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[TagGroupID],
					[TagGroupLabel],
					[ControlTypeID],
					[CatalogID],
					[DisplayOrder]
				FROM
					[dbo].[ZNodeTagGroup]
				WHERE
					[ControlTypeID] = @ControlTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTagGroup_GetByTagGroupID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeTagGroup table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTagGroup_GetByTagGroupID
(

	@TagGroupID int   
)
AS


				SELECT
					[TagGroupID],
					[TagGroupLabel],
					[ControlTypeID],
					[CatalogID],
					[DisplayOrder]
				FROM
					[dbo].[ZNodeTagGroup]
				WHERE
					[TagGroupID] = @TagGroupID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrder_GetByPortalId]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrder table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrder_GetByPortalId
(

	@PortalId int   
)
AS


				SELECT
					[OrderID],
					[PortalId],
					[AccountID],
					[OrderStateID],
					[ShippingID],
					[PaymentTypeId],
					[ShipFirstName],
					[ShipLastName],
					[ShipCompanyName],
					[ShipStreet],
					[ShipStreet1],
					[ShipCity],
					[ShipStateCode],
					[ShipPostalCode],
					[ShipCountry],
					[ShipPhoneNumber],
					[ShipEmailID],
					[BillingFirstName],
					[BillingLastName],
					[BillingCompanyName],
					[BillingStreet],
					[BillingStreet1],
					[BillingCity],
					[BillingStateCode],
					[BillingPostalCode],
					[BillingCountry],
					[BillingPhoneNumber],
					[BillingEmailId],
					[CardTransactionID],
					[CardAuthCode],
					[CardTypeId],
					[CardExp],
					[TaxCost],
					[ShippingCost],
					[SubTotal],
					[DiscountAmount],
					[Total],
					[OrderDate],
					[Custom1],
					[Custom2],
					[AdditionalInstructions],
					[Custom3],
					[TrackingNumber],
					[CouponCode],
					[PromoDescription],
					[ReferralAccountID],
					[PurchaseOrderNumber],
					[PaymentStatusID],
					[WebServiceDownloadDate],
					[PaymentSettingID],
					[ShipDate],
					[ReturnDate],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST]
				FROM
					[dbo].[ZNodeOrder]
				WHERE
					[PortalId] = @PortalId
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTagGroup_GetByCategoryIDFromTagGroupCategory]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records through a junction table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTagGroup_GetByCategoryIDFromTagGroupCategory
(

	@CategoryID int   
)
AS


SELECT dbo.[ZNodeTagGroup].[TagGroupID]
       ,dbo.[ZNodeTagGroup].[TagGroupLabel]
       ,dbo.[ZNodeTagGroup].[ControlTypeID]
       ,dbo.[ZNodeTagGroup].[CatalogID]
       ,dbo.[ZNodeTagGroup].[DisplayOrder]
  FROM dbo.[ZNodeTagGroup]
 WHERE EXISTS (SELECT 1
                 FROM dbo.[ZNodeTagGroupCategory] 
                WHERE dbo.[ZNodeTagGroupCategory].[CategoryID] = @CategoryID
                  AND dbo.[ZNodeTagGroupCategory].[TagGroupID] = dbo.[ZNodeTagGroup].[TagGroupID]
                  )
				SELECT @@ROWCOUNT			
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTagProductSKU_GetByProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeTagProductSKU table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTagProductSKU_GetByProductID
(

	@ProductID int   
)
AS


				SELECT
					[TagProductSKUID],
					[TagID],
					[ProductID],
					[SKUID]
				FROM
					[dbo].[ZNodeTagProductSKU]
				WHERE
					[ProductID] = @ProductID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTagGroup_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeTagGroup table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTagGroup_Find
(

	@SearchUsingOR bit   = null ,

	@TagGroupID int   = null ,

	@TagGroupLabel nvarchar (MAX)  = null ,

	@ControlTypeID int   = null ,

	@CatalogID int   = null ,

	@DisplayOrder int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [TagGroupID]
	, [TagGroupLabel]
	, [ControlTypeID]
	, [CatalogID]
	, [DisplayOrder]
    FROM
	[dbo].[ZNodeTagGroup]
    WHERE 
	 ([TagGroupID] = @TagGroupID OR @TagGroupID IS NULL)
	AND ([TagGroupLabel] = @TagGroupLabel OR @TagGroupLabel IS NULL)
	AND ([ControlTypeID] = @ControlTypeID OR @ControlTypeID IS NULL)
	AND ([CatalogID] = @CatalogID OR @CatalogID IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [TagGroupID]
	, [TagGroupLabel]
	, [ControlTypeID]
	, [CatalogID]
	, [DisplayOrder]
    FROM
	[dbo].[ZNodeTagGroup]
    WHERE 
	 ([TagGroupID] = @TagGroupID AND @TagGroupID is not null)
	OR ([TagGroupLabel] = @TagGroupLabel AND @TagGroupLabel is not null)
	OR ([ControlTypeID] = @ControlTypeID AND @ControlTypeID is not null)
	OR ([CatalogID] = @CatalogID AND @CatalogID is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTagGroupCategory_GetByCategoryID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeTagGroupCategory table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTagGroupCategory_GetByCategoryID
(

	@CategoryID int   
)
AS


				SELECT
					[TagGroupID],
					[CategoryID],
					[CategoryDisplayOrder]
				FROM
					[dbo].[ZNodeTagGroupCategory]
				WHERE
					[CategoryID] = @CategoryID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTagProductSKU_GetBySKUID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeTagProductSKU table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTagProductSKU_GetBySKUID
(

	@SKUID int   
)
AS


				SELECT
					[TagProductSKUID],
					[TagID],
					[ProductID],
					[SKUID]
				FROM
					[dbo].[ZNodeTagProductSKU]
				WHERE
					[SKUID] = @SKUID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetSKUAttributeCombo_ByProductID]'
GO
SET ANSI_NULLS ON
GO
--======================================      
-- Created Date : 7-Sep-2007    
-- Modified Date: 19-April-2010
-- To Retrive the SKU Combination      
--======================================          
ALTER PROCEDURE [ZNode_GetSKUAttributeCombo_ByProductID]
(      
@PRODUCTID INT = 0,      
@ATTRIBUTES VARCHAR(MAX)      
)      
AS      
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from          
	-- interfering with SELECT statements.          
	SET NOCOUNT ON;
	
	SELECT 
		SKUID,
		sku.ProductID,		
		sku.SKU,
		QuantityOnHand,
		ReorderLevel,
		SKUPicturePath,
		sku.DisplayOrder, 
		RetailPriceOverride,
		SalePriceOverride, 
		WholesalePriceOverride,
		sku.ActiveInd
	FROM ZNODESKU sku
	INNER JOIN ZNodeSkuInventory ON sku.SKU = ZNodeSkuInventory.SKU   
	WHERE SKUID IN 
   (SELECT skuid from ZNodeSKUattribute WHERE AttributeId IN (SELECT [VALUE] FROM SC_Splitter(@Attributes,',')) 
	GROUP BY SKUID Having Count(1) = (SELECT COUNT(1) FROM SC_Splitter(@ATTRIBUTES,',')))
	AND sku.ProductID = @PRODUCTID;
	
END     
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeCategory]'
GO
ALTER TABLE [dbo].[ZNodeCategory] ALTER COLUMN [ShortDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[ZNodeCategory] ALTER COLUMN [Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[ZNodeCategory] ALTER COLUMN [AlternateDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProductCategory_GetByCategoryID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProductCategory table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProductCategory_GetByCategoryID
(

	@CategoryID int   
)
AS


				SELECT
					[ProductCategoryID],
					[ProductID],
					[Theme],
					[MasterPage],
					[CSS],
					[CategoryID],
					[BeginDate],
					[EndDate],
					[DisplayOrder],
					[ActiveInd]
				FROM
					[dbo].[ZNodeProductCategory]
				WHERE
					[CategoryID] = @CategoryID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeProduct]'
GO
ALTER TABLE [dbo].[ZNodeProduct] ALTER COLUMN [ShortDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[ZNodeProduct] ALTER COLUMN [Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
ALTER TABLE [dbo].[ZNodeProduct] ALTER COLUMN [FeaturesDesc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[ZNodeProduct] ALTER COLUMN [Specifications] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[ZNodeProduct] ALTER COLUMN [AdditionalInformation] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[ZNodeProduct] ALTER COLUMN [InStockMsg] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[ZNodeProduct] ALTER COLUMN [OutOfStockMsg] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeProduct_ActiveInd] on [dbo].[ZNodeProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeProduct_ActiveInd] ON [dbo].[ZNodeProduct] ([ActiveInd]) INCLUDE ([DisplayOrder], [ProductID], [ProductNum], [RetailPrice])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeCategoryNode_GetByActiveInd]'
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeCategoryNode table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodeCategoryNode_GetByActiveInd]
(

	@ActiveInd bit   
)
AS


				SELECT
					[CategoryNodeID],
					[CatalogID],
					[CategoryID],
					[ParentCategoryNodeID],
					[BeginDate],
					[EndDate],
					[Theme],
					[MasterPage],
					[CSS],
					[DisplayOrder],
					[ActiveInd]
				FROM
					[dbo].[ZNodeCategoryNode]
				WHERE
					[ActiveInd] = @ActiveInd
				SELECT @@ROWCOUNT
					
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeCategoryNode_GetByCatalogIDParentCategoryNodeIDActiveInd]'
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeCategoryNode table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodeCategoryNode_GetByCatalogIDParentCategoryNodeIDActiveInd]
(

	@CatalogID int   ,

	@ParentCategoryNodeID int   ,

	@ActiveInd bit   
)
AS


				SELECT
					[CategoryNodeID],
					[CatalogID],
					[CategoryID],
					[ParentCategoryNodeID],
					[BeginDate],
					[EndDate],
					[Theme],
					[MasterPage],
					[CSS],
					[DisplayOrder],
					[ActiveInd]
				FROM
					[dbo].[ZNodeCategoryNode]
				WHERE
					[CatalogID] = @CatalogID
					AND [ParentCategoryNodeID] = @ParentCategoryNodeID
					AND [ActiveInd] = @ActiveInd
				SELECT @@ROWCOUNT
					
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeHighlight_GetByLocaleId]'
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeHighlight table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodeHighlight_GetByLocaleId]
(

	@LocaleId int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[HighlightID],
					[ImageFile],
					[ImageAltTag],
					[Name],
					[Description],
					[DisplayPopup],
					[Hyperlink],
					[HyperlinkNewWinInd],
					[HighlightTypeID],
					[ActiveInd],
					[DisplayOrder],
					[ShortDescription],
					[LocaleId]
				FROM
					[dbo].[ZNodeHighlight]
				WHERE
					[LocaleId] = @LocaleId
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePortalCatalog_GetByPortalIDCatalogID]'
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortalCatalog table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodePortalCatalog_GetByPortalIDCatalogID]
(

	@PortalID int   ,

	@CatalogID int   
)
AS


				SELECT
					[PortalCatalogID],
					[PortalID],
					[CatalogID],
					[Theme],
					[CSS],
					[LocaleID]
				FROM
					[dbo].[ZNodePortalCatalog]
				WHERE
					[PortalID] = @PortalID
					AND [CatalogID] = @CatalogID
				SELECT @@ROWCOUNT
					
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeProduct_GetByActiveInd]'
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodeProduct_GetByActiveInd]
(

	@ActiveInd bit   
)
AS


				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[SKU],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[ActiveInd] = @ActiveInd
				SELECT @@ROWCOUNT
					
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProductCategory_GetByProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProductCategory table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProductCategory_GetByProductID
(

	@ProductID int   
)
AS


				SELECT
					[ProductCategoryID],
					[ProductID],
					[Theme],
					[MasterPage],
					[CSS],
					[CategoryID],
					[BeginDate],
					[EndDate],
					[DisplayOrder],
					[ActiveInd]
				FROM
					[dbo].[ZNodeProductCategory]
				WHERE
					[ProductID] = @ProductID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeProductCategory_GetByActiveInd]'
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProductCategory table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodeProductCategory_GetByActiveInd]
(

	@ActiveInd bit   
)
AS


				SELECT
					[ProductCategoryID],
					[ProductID],
					[Theme],
					[MasterPage],
					[CSS],
					[CategoryID],
					[BeginDate],
					[EndDate],
					[DisplayOrder],
					[ActiveInd]
				FROM
					[dbo].[ZNodeProductCategory]
				WHERE
					[ActiveInd] = @ActiveInd
				SELECT @@ROWCOUNT
					
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeReview_GetByProductIDStatus]'
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeReview table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodeReview_GetByProductIDStatus]
(

	@ProductID int   ,

	@Status nvarchar (1)  
)
AS


				SELECT
					[ReviewID],
					[ProductID],
					[AccountID],
					[Subject],
					[Pros],
					[Cons],
					[Comments],
					[CreateUser],
					[UserLocation],
					[Rating],
					[Status],
					[CreateDate],
					[Custom1],
					[Custom2],
					[Custom3]
				FROM
					[dbo].[ZNodeReview]
				WHERE
					[ProductID] = @ProductID
					AND [Status] = @Status
				SELECT @@ROWCOUNT
					
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsActivityLog]'
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[ZNode_ReportsActivityLog]
    @FromDate DateTime = NULL,
    @Category VARCHAR(100) = '',
    @PortalId VARCHAR(10) = ''
AS 
    BEGIN                                      
        SET NOCOUNT ON ;      
          
        IF ( @FromDate ) IS NULL 
            BEGIN
			-- If date is NULL then display current date activity log report.
                SET @FromDate = GETDATE() ;
            END

        SELECT  AL.[ActivityLogID],
                Alt.[ActivityLogTypeID],
                Alt.[Name],
                Alt.[TypeCategory] AS 'Category',
                AL.[CreateDte],
                P.[StoreName] AS 'StoreName',
                Al.[EndDte],
                AL.[Data1],
                AL.[Data2],
                AL.[Data3],
                AL.[Status],
                AL.[LongData]
        FROM    ZNodeActivityLog AL
                INNER JOIN ZNodeActivityLogType Alt ON Alt.ActivityLogTypeID = AL.ActivityLogTypeID
                INNER JOIN ZNodePortal P ON P.PortalID = AL.PortalID
        WHERE   AL.CreateDte >= @FromDate
                AND Alt.TypeCategory LIKE '%' + @Category + '%'
                AND (P.PortalID = @PortalId OR @PortalId = '0')
        ORDER BY AL.ActivityLogID DESC
                           
    END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsCouponFiltered]'
GO


CREATE PROCEDURE [dbo].[ZNode_ReportsCouponFiltered]
    (
      @FromDate DATETIME,
      @ToDate DATETIME,
      @PortalId VARCHAR(10) = ''
    )
AS 
    BEGIN
        SET NOCOUNT ON ;

        SELECT  O.CouponCode AS 'CouponCode',
                Dt.[Name] As 'DiscountType',
                Po.[StoreName] AS 'StoreName',
                COUNT(O.couponcode) AS 'TotalCount',
                SUM(O.Total) AS 'Total',
                SUM(O.DiscountAmount) AS 'DiscountAmount',
                P.[description] AS 'Description',
                P.StartDate,
                P.EndDate
        FROM    ZNodeOrder O
                INNER JOIN ZNodePromotion P ON P.CouponCode = O.CouponCode
                INNER JOIN ZNodeDiscountType Dt ON Dt.DiscountTypeID = P.DiscountTypeID
                INNER JOIN ZNodePortal Po ON Po.PortalID = O.PortalId
        WHERE   LEN(O.couponcode) > 0
                AND O.OrderDate >= @FromDate
                AND O.OrderDate <= @ToDate
                AND (PO.PortalID = @PortalId OR @PortalId = '0')
        GROUP BY Po.StoreName,
                O.CouponCode,
                P.[Description],
                P.StartDate,
                P.EndDate,
                Dt.Name
    END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsFeedback]'
GO


CREATE PROCEDURE [dbo].[ZNode_ReportsFeedback] @PortalId VARCHAR(10)
AS 
    BEGIN                                      
        SET NOCOUNT ON ;  
        
		-- Service Request Main Report
        SELECT  CR.[CaseID],
                CR.[Title]
        FROM    ZNodeCaseStatus CS
                INNER JOIN ZNodeCaseRequest CR ON CS.CaseStatusID = CR.CaseStatusID
                INNER JOIN ZNodePortal P ON P.PortalId = CR.PortalID
        WHERE   (P.PortalID = @PortalId OR @PortalId = '0')
        
        -- Service Request Sub-Report
        SELECT  CR.[CaseID],
                CR.[Title],
                CR.[Description],
                CR.[FirstName],
                CR.[LastName],
                CR.[CompanyName],
                CR.[EmailID],
                CR.[PhoneNumber],
                CR.[CreateDte],
                CS.CaseStatusNme AS 'StatusName',
                P.[StoreName] AS 'StoreName'
        FROM    ZNodeCaseStatus CS
                INNER JOIN ZNodeCaseRequest CR ON CS.CaseStatusID = CR.CaseStatusID
                INNER JOIN ZNodePortal P ON P.PortalId = CR.PortalID
        WHERE   (P.PortalID = @PortalId OR @PortalId = '0')

    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsFrequentFiltered]'
GO


CREATE PROCEDURE [dbo].[ZNode_ReportsFrequentFiltered]
    (
      @FromDate DATETIME,
      @ToDate DATETIME,
      @PortalId VARCHAR(10) = ''
    )
AS 
    BEGIN                                      
        SET NOCOUNT ON ;  
			
        SELECT DISTINCT
                ( O.AccountID ),
                O.BillingFirstName,
                O.BillingLastName,
                O.BillingCompanyName,
                P.StoreName AS 'StoreName',
                COUNT(O.AccountId) AS 'OrderCount',
                SUM(O.Total) AS 'Total',
                SUM(ol.Quantity) AS 'Quantity'
        FROM    ZNodeOrder O
                INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
                INNER JOIN ZNodeOrderLineItem ol ON ol.OrderID = O.OrderID
        WHERE   O.OrderDate >= @FromDate
                AND O.OrderDate <= @ToDate
                AND (P.PortalID = @PortalId OR @PortalId = '0')
        GROUP BY O.PortalId,
                O.AccountId,
                O.BillingFirstName,
                O.BillingLastName,
                O.BillingCompanyName,
                P.StoreName
        ORDER BY OrderCount desc
                                
    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsOptIn]'
GO

CREATE PROCEDURE [dbo].[ZNode_ReportsOptIn]
    @PortalId VARCHAR(10) = ''
AS 
    BEGIN                                      
        SET NOCOUNT ON ;  
        
        SELECT  A.[AccountID],
                A.[BillingFirstName],
                A.[BillingLastName],
                A.[BillingCompanyName],
                A.[BillingEmailID],
                P.StoreName
        FROM    ZNodeAccount A
                INNER JOIN ZNodeAccountProfile Zap ON A.AccountId = Zap.AccountId
                INNER JOIN ZNodePortalProfile Zpp ON Zpp.ProfileID = A.ProfileID
                INNER JOIN ZNodePortal P ON P.PortalId = Zpp.PortalId
        WHERE   A.EmailOptIn = 1
                AND (P.PortalID = @PortalId OR @PortalId = '0');
		
	                                
    END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsPicklist]'
GO


CREATE PROCEDURE [dbo].[ZNode_ReportsPicklist]
    @PortalId VARCHAR(10) = ''
AS 
    BEGIN                                      
        SET NOCOUNT ON ;          

		-- Select PickList report data
        SELECT  O.[OrderID],
                O.[BillingFirstName],
                O.[BillingLastName],
                O.[OrderDate],
                P.[StoreName] AS 'StoreName'
        FROM    ZNodeOrder O
                INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
        WHERE   O.OrderStateID = ( SELECT   OrderStateId
                                   FROM     ZnodeOrderState
                                   WHERE    OrderStateName = 'Submitted'
                                 )
                AND (P.PortalID = @PortalId OR @PortalId = '0');	
                
         -- Select picklist Sub-Report items
        SELECT  O.OrderID,
                O.SKU,
                O.Quantity,
                O.Name,
                O.Price
        FROM    ZNodeOrderLineItem O
        WHERE   OrderID IN (
                SELECT  OrderId
                FROM    ZNodeOrder O
                        INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
                WHERE   O.OrderStateID = ( SELECT   OrderStateID
                                           FROM     ZnodeOrderState
                                           WHERE    OrderStateName = 'Submitted'
                                         )
                        AND (P.PortalID = @PortalId OR @PortalId = '0'))	
                                
    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsPopularFiltered]'
GO


CREATE PROCEDURE [dbo].[ZNode_ReportsPopularFiltered]
    (
      @FromDate DateTime = NULL,
      @ToDate DateTime = NULL,
      @PortalId VARCHAR(100)
    )
AS 
    BEGIN                                      
        SET NOCOUNT ON ;  

        SELECT  p.StoreName AS 'StoreName',
                Item.SKU,
                Item.Name,
                SUM(Item.Quantity) AS 'Quantity',
                Item.Price,
                Item.ProductNum
        FROM    ZNodeOrderlineitem Item
                INNER JOIN ZnodeOrder O ON O.OrderID = Item.OrderId
                INNER JOIN ZNodePortal P ON p.PortalID = O.PortalId
        WHERE   O.OrderDate >= @FromDate
                AND O.OrderDate <= @ToDate
                AND (P.PortalID = @PortalId OR @PortalId = '0')
        GROUP BY P.StoreName,
                Item.SKU,
                Item.Price,
                Item.Name,
                Item.ProductNum
        HAVING  SUM(Item.Quantity) > 0 
        
    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsReOrder]'
GO

CREATE PROCEDURE [dbo].[ZNode_ReportsReOrder]
    @PortalId VARCHAR(10) = ''
AS 
    BEGIN                                      
        SET NOCOUNT ON ;  
       
        SELECT DISTINCT
                ( Pr.ProductID ),
                P.StoreName,
                Pr.sku AS 'SKU',
                Pin.quantityonhand AS 'Quantity',
                Pin.reorderlevel AS 'ReOrderLevel',
                Pr.ProductNum AS 'ProductNUM',
                Pr.[name] AS 'Description',
                'Product' AS 'Re-OrderlevelFrom'
        FROM    ZNodeProduct Pr
                INNER JOIN ZNodeProductCategory Pc ON Pc.ProductID = Pr.ProductId
                INNER JOIN ZNodeCategoryNode Cn ON Cn.CategoryID = Pc.CategoryId
                INNER JOIN ZNodePortalCatalog Pca ON Pca.CatalogID = Cn.CatalogID
                INNER JOIN ZNodeProductInventory Pin ON Pin.ProductNum = Pr.ProductNum
                INNER JOIN ZNodePortal P ON P.PortalID = Pca.PortalID
        WHERE   (P.PortalID = @PortalId OR @PortalId = '0')
                AND Pr.ProductId NOT IN ( SELECT    ProductId
                                          FROM      ZNodeSKU )
                AND Pin.QuantityOnHand <= Pin.ReOrderLevel
        UNION
        SELECT DISTINCT
                ( S.ProductID ),
                P.StoreName,
                S.SKU AS 'SKU',
                Si.QuantityOnHand AS 'Quantity',
                Si.reorderlevel AS 'ReOrderLevel',
                Pr.ProductNum AS 'ProductNUM',
                '' AS 'Description',
                'SKU' AS 'Re-Order levelFrom'
        FROM    ZNodeProduct Pr
                INNER JOIN ZNodeSKU S ON S.ProductID = Pr.ProductID
                INNER JOIN ZNodeProductCategory Pc ON Pc.ProductID = Pr.ProductID
                INNER JOIN ZNodeCategoryNode Cn ON Cn.CategoryID = Pc.CategoryID
                INNER JOIN ZNodePortalCatalog Pca ON Pca.CatalogID = Cn.CatalogID
                INNER JOIN ZNodePortal P ON P.PortalID = Pca.PortalId
                INNER JOIN ZNodeSKUInventory Si ON Si.SKU = S.SKU
        WHERE   (P.PortalID = @PortalId OR @PortalId = '0')
                AND Si.QuantityOnHand <= Si.ReOrderLevel
        UNION
        SELECT  '' AS 'ProductID',
                '' AS 'StoreName',
                AV.Sku AS 'SKU',
                Avi.QuantityOnHand AS 'Quantity',
                Avi.ReOrderLevel as 'ReOrderLevel',
                '' AS 'ProductNUM',
                AV.[name] AS 'Description',
                'Addon' AS 'Re-OrderlevelFrom'
        FROM    ZNodeAddOnValue AV
                INNER JOIN ZNodeAddOnValueInventory Avi ON Avi.SKU = AV.SKU
        WHERE   Avi.QuantityOnHand <= Avi.reorderlevel
    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsSEOFiltered]'
GO


CREATE PROCEDURE [dbo].[ZNode_ReportsSEOFiltered]
    (
      @FromDate DATETIME,
      @ToDate DATETIME            
    )
AS 
    BEGIN                                      
        SET NOCOUNT ON ;  

        SELECT  Data1 As 'Search_Phrase',
                Count(Data1) As 'Times_Searched'
        FROM    ZNodeActivityLog
        WHERE   ActivityLogTypeID IN ( 9500, 9501, 9502 )
                AND CreateDte >= @FromDate
                AND CreateDte <= @ToDate
        GROUP BY DATA1
        ORDER BY 'Times_Searched' DESC
    END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsVolumeFiltered]'
GO


CREATE PROCEDURE [dbo].[ZNode_ReportsVolumeFiltered]
    (
      @FromDate DATETIME,
      @ToDate DATETIME,
      @PortalId VARCHAR(100)
    )
AS 
    BEGIN                                      
        SET NOCOUNT ON ;        

        SELECT DISTINCT
                ( O.AccountID ),
                O.BillingFirstName,
                O.BillingLastName,
                O.BillingCompanyName,
                p.StoreName AS 'StoreName',
                COUNT(O.AccountId) AS 'OrderCount',
                SUM(O.Total) AS 'Total'
        FROM    ZNodeOrder O
                INNER JOIN ( SELECT OrderId,
                                    SUM(Quantity) AS Quantity
                             FROM   ZNodeOrderLineItem
                             GROUP BY OrderID
                           ) ol ON ol.OrderID = O.OrderID
                INNER JOIN ZNodePortal p ON p.PortalID = O.PortalId
        WHERE   O.OrderDate >= @FromDate
                AND O.OrderDate <= @ToDate
                AND (P.PortalID = @PortalId OR @PortalId = '0')
        GROUP BY O.PortalId,
                O.AccountID,
                O.BillingFirstName,
                O.BillingLastName,
                O.BillingCompanyName,
                p.StoreName
        ORDER BY Total desc
                                
    END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsAffiliateFiltered]'
GO


CREATE PROCEDURE [dbo].[ZNode_ReportsAffiliateFiltered]
    (
      @FromDate DATETIME,
      @ToDate DATETIME,
      @PortalId VARCHAR(100) = ''
    )
AS 
    BEGIN                                      
        SET NOCOUNT ON ;      

        SELECT  ( A.BillingFirstName + ' ' + A.BillingLastName ) As 'Name',
                A.TaxID AS 'TaxID',
                p.StoreName AS 'StoreName',
                COUNT(o.AccountId) AS 'NumberOfOrders',
                SUM(o.Total) AS 'OrderValue',
                r.[Name] AS 'CommissionType',
                A.ReferralCommission AS 'CommissionAmount',
                CASE WHEN A.ReferralCommissionTypeID = 1
                     THEN SUM(o.Total) * A.ReferralCommission / 100
                     WHEN A.ReferralCommissionTypeID = 2
                     THEN A.ReferralCommission
                END AS 'CommissionOwed',
                A.ReferralCommissionTypeID AS ReferralCommissionTypeID
        FROM    ZNodeAccount A
                INNER JOIN ZNodeReferralCommissionType r ON r.ReferralCommissionTypeID = A.ReferralCommissionTypeID
                INNER JOIN ZNodeOrder o ON o.AccountID = A.AccountId
                INNER JOIN ZNodePortal p ON p.PortalID = o.PortalId
        WHERE   A.ReferralStatus = 'A'
                AND o.OrderDate >= @FromDate
                AND o.OrderDate <= @ToDate
                AND (P.PortalID = @PortalId OR @PortalId = '0')
        GROUP BY p.StoreName,
                A.BillingFirstName,
                A.BillingLastName,
                A.TaxID,
                r.Name,
                A.ReferralCommission,
                A.ReferralCommissionTypeID                        
          
    END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsTaxFiltered]'
GO


CREATE PROCEDURE [dbo].[ZNode_ReportsTaxFiltered]
    (
      @FromDate DATETIME,
      @ToDate DATETIME,
      @Interval Varchar(50),
      @PortalId VARCHAR(10) = '0'
    )
AS 
    BEGIN                                      
        SET NOCOUNT ON ; 
       
        IF ( LEN(@Interval) > 0 ) 
            BEGIN
                SELECT  P.[StoreName] AS 'StoreName',
                        O.OrderDate,
                        O.ShipStateCode,
                        SUM(O.subtotal) AS 'SubTotal',
                        SUM(O.salestax) AS 'TaxCost',
                        SPACE(10) AS 'Title',
                        CASE @Interval
                          WHEN 'month' THEN DATENAME(month, O.OrderDate)
                          WHEN 'quarter' THEN DATENAME(quarter, O.OrderDate)
                        END AS 'Custom1'
                FROM    ZNodeOrder O
                        INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
                WHERE   O.OrderDate >= @FromDate
                        AND O.OrderDate <= @ToDate
                        AND (P.PortalID = @PortalId OR @PortalId = '0')
                GROUP BY P.StoreName,
                        O.OrderDate,
                        O.ShipStateCode
                ORDER BY O.OrderDate
            END
        ELSE 
            BEGIN
                SELECT  P.[StoreName] AS 'StoreName',
                        O.OrderDate,
                        O.ShipStateCode,
                        SUM(O.subtotal) AS 'SubTotal',
                        SUM(O.salestax) AS 'TaxCost',
                        SPACE(30) As 'Custom1'
                FROM    ZNodeOrder O
                        INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
                GROUP BY P.StoreName,
                        O.OrderDate,
                        O.ShipStateCode
                HAVING  SUM(O.SalesTax) > 0 
            END
                                
    END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchOrder]'
GO

ALTER PROCEDURE [dbo].[ZNode_SearchOrder](              
@ORDERID int = null,              
@BILLINGFIRSTNAME nvarchar(MAX) = null,              
@BILLINGLASTNAME nvarchar(MAX) = null,              
@BILLINGCOMPANYNAME nvarchar(MAX) = null,              
@ACCOUNTID nvarchar(MAX) = null,              
@STARTDATE DateTime = null,              
@ENDDATE DateTime = null,              
@ORDERSTATEID int = null,              
@PORTALID int = null,      
@portalIds Varchar(Max)= '0')              
AS              
BEGIN      

IF (@ENDDATE IS NOT NULL AND @STARTDATE IS NULL)
BEGIN
	SET @STARTDATE = '1/1/1901'
END
IF (@ENDDATE IS NULL AND @STARTDATE IS NOT NULL)
BEGIN
	SET @ENDDATE = GETDATE()
END

SET @ENDDATE = (SELECT DateAdd(dd, 1, @ENDDATE))
   
SELECT [OrderID]            
      ,[PortalId]            
      ,[AccountID]            
      ,[OrderStateID]            
      ,[ShippingID]              
      ,[PaymentTypeId]    
      ,[PaymentStatusID]          
      ,(SELECT OrderStateName FROM ZNodeOrderState WHERE OrderStateID = ZNodeOrder.[OrderStateID]) AS 'OrderStatus'                  
      ,(SELECT Description FROM ZNodeShipping WHERE ShippingID = ZNodeOrder.[ShippingID]) AS 'ShippingTypeName'                    
      ,(SELECT Name FROM ZNodePaymentType WHERE PaymentTypeID = ZNodeOrder.[PaymentTypeId]) AS 'PaymentTypeName'                  
      ,(SELECT PaymentStatusName FROM ZNodePaymentStatus WHERE PaymentStatusID = ZNodeOrder.[PaymentStatusID]) AS 'PaymentStatusName'                   
      ,[BillingFirstName]            
      ,[BillingLastName]                       
      ,[Total]            
      ,[OrderDate]  
      ,[ShipDate]
      ,[TrackingNumber]                            
FROM              
ZNODEORDER  ZNodeOrder            
WHERE              
(PortalID in (SELECT [value] from sc_splitter(@portalIds,','))OR  @portalIds = '0')     
 AND ([BillingFirstName] LIKE @BILLINGFIRSTNAME + '%' OR [ShipFirstName] LIKE @BILLINGFIRSTNAME + '%' OR @BILLINGFIRSTNAME IS NULL)    
 AND ([BillingLastName] LIKE @BILLINGLASTNAME + '%' OR [ShipLastName] LIKE @BILLINGLASTNAME + '%' OR @BILLINGLASTNAME IS NULL)    
 AND ([BillingCompanyName] LIKE @BILLINGCOMPANYNAME + '%' OR [ShipCompanyName] LIKE @BILLINGCOMPANYNAME + '%' OR @BILLINGCOMPANYNAME IS NULL)    
 AND ([ORDERID] = @ORDERID OR @ORDERID IS NULL)    
 AND ([AccountID] = @AccountID OR @AccountID IS NULL)    
 AND ([PortalID] = @PortalID OR @PortalID IS NULL)    
 AND ([OrderStateID] = @ORDERSTATEID OR @ORDERSTATEID IS NULL)     
 AND ((OrderDate BETWEEN @STARTDATE AND @ENDDATE) OR (@STARTDATE IS NULL OR @ENDDATE IS NULL))    
ORDER BY OrderID DESC     
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCategory_Find]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeCategory table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCategory_Find
(

	@SearchUsingOR bit   = null ,

	@CategoryID int   = null ,

	@Name nvarchar (MAX)  = null ,

	@Title nvarchar (MAX)  = null ,

	@ShortDescription nvarchar (MAX)  = null ,

	@Description nvarchar (MAX)  = null ,

	@ImageFile nvarchar (MAX)  = null ,

	@ImageAltTag nvarchar (MAX)  = null ,

	@VisibleInd bit   = null ,

	@SubCategoryGridVisibleInd bit   = null ,

	@SEOTitle nvarchar (MAX)  = null ,

	@SEOKeywords nvarchar (MAX)  = null ,

	@SEODescription nvarchar (MAX)  = null ,

	@AlternateDescription nvarchar (MAX)  = null ,

	@DisplayOrder int   = null ,

	@Custom1 nvarchar (MAX)  = null ,

	@Custom2 nvarchar (MAX)  = null ,

	@Custom3 nvarchar (MAX)  = null ,

	@SEOURL nvarchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CategoryID]
	, [Name]
	, [Title]
	, [ShortDescription]
	, [Description]
	, [ImageFile]
	, [ImageAltTag]
	, [VisibleInd]
	, [SubCategoryGridVisibleInd]
	, [SEOTitle]
	, [SEOKeywords]
	, [SEODescription]
	, [AlternateDescription]
	, [DisplayOrder]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [SEOURL]
    FROM
	[dbo].[ZNodeCategory]
    WHERE 
	 ([CategoryID] = @CategoryID OR @CategoryID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Title] = @Title OR @Title IS NULL)
	AND ([ShortDescription] = @ShortDescription OR @ShortDescription IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([ImageFile] = @ImageFile OR @ImageFile IS NULL)
	AND ([ImageAltTag] = @ImageAltTag OR @ImageAltTag IS NULL)
	AND ([VisibleInd] = @VisibleInd OR @VisibleInd IS NULL)
	AND ([SubCategoryGridVisibleInd] = @SubCategoryGridVisibleInd OR @SubCategoryGridVisibleInd IS NULL)
	AND ([SEOTitle] = @SEOTitle OR @SEOTitle IS NULL)
	AND ([SEOKeywords] = @SEOKeywords OR @SEOKeywords IS NULL)
	AND ([SEODescription] = @SEODescription OR @SEODescription IS NULL)
	AND ([AlternateDescription] = @AlternateDescription OR @AlternateDescription IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([Custom1] = @Custom1 OR @Custom1 IS NULL)
	AND ([Custom2] = @Custom2 OR @Custom2 IS NULL)
	AND ([Custom3] = @Custom3 OR @Custom3 IS NULL)
	AND ([SEOURL] = @SEOURL OR @SEOURL IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CategoryID]
	, [Name]
	, [Title]
	, [ShortDescription]
	, [Description]
	, [ImageFile]
	, [ImageAltTag]
	, [VisibleInd]
	, [SubCategoryGridVisibleInd]
	, [SEOTitle]
	, [SEOKeywords]
	, [SEODescription]
	, [AlternateDescription]
	, [DisplayOrder]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [SEOURL]
    FROM
	[dbo].[ZNodeCategory]
    WHERE 
	 ([CategoryID] = @CategoryID AND @CategoryID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Title] = @Title AND @Title is not null)
	OR ([ShortDescription] = @ShortDescription AND @ShortDescription is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([ImageFile] = @ImageFile AND @ImageFile is not null)
	OR ([ImageAltTag] = @ImageAltTag AND @ImageAltTag is not null)
	OR ([VisibleInd] = @VisibleInd AND @VisibleInd is not null)
	OR ([SubCategoryGridVisibleInd] = @SubCategoryGridVisibleInd AND @SubCategoryGridVisibleInd is not null)
	OR ([SEOTitle] = @SEOTitle AND @SEOTitle is not null)
	OR ([SEOKeywords] = @SEOKeywords AND @SEOKeywords is not null)
	OR ([SEODescription] = @SEODescription AND @SEODescription is not null)
	OR ([AlternateDescription] = @AlternateDescription AND @AlternateDescription is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([Custom1] = @Custom1 AND @Custom1 is not null)
	OR ([Custom2] = @Custom2 AND @Custom2 is not null)
	OR ([Custom3] = @Custom3 AND @Custom3 is not null)
	OR ([SEOURL] = @SEOURL AND @SEOURL is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductByProductID]'
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[ZNode_GetProductByProductID]                           
 -- Add the parameters for the stored procedure here                          
 @ProductId int = 0
AS                          
BEGIN                          
 -- SET NOCOUNT ON added to prevent extra result sets from                          
 -- interfering with SELECT statements.                          
 SET NOCOUNT ON;                          
                          
    -- Insert statements for procedure here                           
                           
 SELECT                          
  ZNodeProduct.*,        
  ManufacturerName = (Select Name from  ZNodeManufacturer where ManufacturerId = ZNodeProduct.ManufacturerId And ActiveInd = 1), 
  QuantityOnHand = (Select QuantityOnHand from ZnodeProductInventory where ZnodeProductInventory.ProductNum = ZNodeProduct.ProductNum),       
 (SELECT ReviewRating = IsNull(AVG(Rating),0),          
  TotalReviews = IsNull(COUNT(ProductId),0) FROM ZNodeReview ZNodeReview           
  WHERE ZNodeReview.ProductId = ZNodeProduct.ProductId AND ZNodeReview.Status = 'A' FOR XML PATH(''),TYPE),                         
                          
  (                          
   select ZNodeHighlight.*,ZNodeProduct.ProductId                         
   from                           
    ZNodeproducthighlight ZNodeProductHighlight,                           
    ZNodehighlight ZNodeHighlight                           
   where                           
    ZNodeProductHighlight.highlightid = ZNodeHighlight.highlightid                          
    and ZNodeProductHighlight.productid = ZNodeProduct.productid                          
   ORDER BY ZNodeProductHighlight.DisplayOrder asc                          
   FOR XML AUTO, TYPE, ELEMENTS                          
  ) ,                          
                
  (select ZNodeReview.* FROM ZNodeReview ZNodeReview  where                 
    ZNodeReview.ProductId = ZNodeProduct.ProductId AND ZNodeReview.Status = 'A'  ORDER BY ZNodeReview.ReviewId desc FOR XML AUTO, TYPE, ELEMENTS                        
  ) ,            
              
  (                          
   select                           
    ZNodeCrossSellItem.ProductId, ZNodeCrossSellItem.Name,ZNodeCrossSellItem.ShortDescription,ZNodeCrossSellItem.ImageFile,          
 ZNodeCrossSellItem.SeoURL,ZNodeCrossSellItem.ImageAltTag          
    FROM          
 ZNodeproductcrosssell ZNodeProductCrossSell, ZNodeproduct ZNodeCrossSellItem                           
   where                           
    ZNodeProductCrossSell.relatedproductid = ZNodeCrossSellItem.productid                          
    and ZNodeProductCrossSell.productid = ZNodeProduct.productid                          
   ORDER BY ZNodeCrossSellItem.DisplayOrder asc                          
   FOR XML AUTO, TYPE, ELEMENTS                          
  ) ,                          
                          
  (                          
   Select                          
    ZNodeAttributeType.*,                          
     (                          
      select                          
      Distinct(ZNodeAttribute.AttributeId), ZNodeAttribute.Name, ZNodeAttribute.DisplayOrder                          
      from                           
      ZNodeskuattribute skua,                          
      ZNodeProductattribute ZNodeAttribute,                          
      ZNodeattributetype aty                          
      where                          
      skua.attributeid = ZNodeAttribute.attributeid                          
      and ZNodeAttribute.attributetypeid = aty.attributetypeid                          
      and aty.attributetypeid = ZNodeAttributeType.attributetypeid                 
      and ZNodeAttribute.IsActive = 1                          
      and skuid in (select skuid from znodesku where productid=@ProductId and ActiveInd = 1)                               
      Group By                          
      ZNodeAttribute.AttributeId, ZNodeAttribute.Name, ZNodeAttribute.DisplayOrder                          
      Order By                          
      ZNodeAttribute.DisplayOrder                          
      FOR XML AUTO, TYPE, ELEMENTS                          
     )                          
   from                          
    ZNodeattributetype ZNodeAttributeType,                          
    ZNodeproducttypeattribute ProductTypeAttribute,                        
    ZNodeProductType,                          
   ZNodeProduct                          
   where                          
    ZNodeAttributeType.attributetypeid = ProductTypeAttribute.attributetypeid                          
    AND ZNodeProductType.producttypeid = ProductTypeAttribute.producttypeid                          
    AND ZNodeProduct.producttypeid = ZNodeProductType.producttypeid                    
    AND ZNodeProduct.ProductID = @ProductId              
 Order By              
 ZNodeAttributeType.DisplayOrder                
   FOR XML AUTO, TYPE, ELEMENTS                          
  ),                          
                          
  (select                          
                   
    ZNodeAddOn.*,                          
     (                          
      select Distinct(ZNodeAddonValue.AddOnValueID), ZNodeAddonValue.Name, ZNodeAddonValue.DisplayOrder,                          
      ZNodeAddonValue.RetailPrice as RetailPrice,ZNodeAddonValue.SalePrice as SalePrice,ZNodeAddonValue.WholeSalePrice as WholesalePrice,ZNodeAddonValue.DefaultInd as IsDefault,    
      QuantityOnHand = (SELECT QuantityOnHand FROM ZNodeAddOnValueInventory WHERE ZNodeAddOnValueInventory.SKU = ZNodeAddOnValue.SKU),ZNodeAddonValue.weight as WeightAdditional,ZNodeAddonValue.ShippingRuleTypeID,ZNodeAddonValue.FreeShippingInd,ZNodeAddOnValue.TaxClassID    
      from                          
      ZNodeAddOnValue ZNodeAddOnValue,                              
      ZNodeProductaddOn ZNodeproductAddOn,                          
 ZNodeAddOn Options 
      where                                      
      ZNodeAddOnValue.AddOnId = ZNodeAddOn.AddOnId                                
      and ZNodeproductAddOn.productId = @ProductId                          
      Group By                          
      ZNodeAddonValue.AddOnValueId, ZNodeAddonValue.Name, ZNodeAddonValue.DisplayOrder,ZNodeAddonValue.RetailPrice,ZNodeAddonValue.SalePrice,ZNodeAddonValue.WholeSalePrice,                        
   ZNodeAddonValue.DefaultInd,ZNodeAddonValue.weight,ZNodeAddonValue.ShippingRuleTypeID,ZNodeAddonValue.FreeShippingInd,ZNodeAddOnValue.TaxClassID,ZNodeAddOnValue.SKU       
      Order By                          
      ZNodeAddonValue.DisplayOrder                          
      FOR XML AUTO, TYPE, ELEMENTS                          
     )                          
                          
   from                          
    ZNodeAddOn ZNodeAddOn,                          
    ZNodeProductAddOn ZNodeProductAddOn,                              
    ZNodeProduct                          
   where                          
    ZNodeAddOn.AddOnId = ZNodeProductAddOn.AddOnId                          
    AND ZNodeProductAddOn.ProductId = @ProductId                              
    AND ZNodeProduct.ProductID = @ProductId                          
    Order By ZNodeAddon.DisplayOrder FOR XML AUTO, TYPE, ELEMENTS                          
  ),          
          
  (select ZNodeProductTier.* from ZNodeProductTier  ZNodeProductTier                          
   where ZNodeProductTier.ProductId = ZNodeProduct.ProductId FOR XML AUTO, TYPE, ELEMENTS)                              
                          
 FROM                          
  ZNodePRODUCT ZNodeProduct                          
 WHERE                             
  ZNodeProduct.ProductId = @ProductId                                    
 FOR XML AUTO, TYPE, ELEMENTS          
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ProductsView]'
GO

CREATE VIEW [dbo].[ProductsView] 
AS
	SELECT
		ProductID                        
		,Name         
		,SKU                     
		,RetailPrice                              
		,SalePrice                           
		,WholesalePrice                              
		,ImageFile                              
		,DisplayOrder                              
		,CallForPricing                            
		,ShortDescription                        
		,ProductNum                          
		,InStockMsg                        
		,OutOfStockMsg
		,AllowBackOrder                          
		,BackOrderMsg                      
		,SEOURL                      
		,NewProductInd              
		,ImageAltTag
		,TaxClassID
		,ManufacturerID
		,ProductTypeID
		,SEOTitle
		,SEODescription
		,SEOKeywords
		,ActiveInd
		,HomepageSpecial
		,FeaturedInd
		FROM [DBO].ZNodeProduct WHERE ActiveInd = 1

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetProductsByCategory_Helper]'
GO

CREATE PROCEDURE [dbo].[ZNode_GetProductsByCategory_Helper]
-- Add the parameters for the stored procedure here        
@PortalId  INT = 0,
@CatalogId INT = NULL,
@Keywords  VARCHAR(4000) = ''
AS
  BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from        
      -- interfering with SELECT statements.        
      SET NOCOUNT ON;

	  WITH CategoryList ( CategoryId, CategoryNodeID )
          AS ( SELECT
                   ZCN.CategoryID ,
                   ZCN.CategoryNodeID
                FROM  ZNodeCategory ZC
                      INNER JOIN ZNodeCategoryNode ZCN
					  ON ZC.CategoryID = ZCN.CategoryID
					  INNER JOIN ZNodePortalCatalog ZPC
					  ON  ZCN.CatalogID = ZPC.CatalogID
               WHERE  EXISTS (SELECT Value
                              FROM   SC_Splitter(@Keywords, ' ')
                              WHERE  PATINDEX('%' + Value + '%', ZC.Name) > 0)
					  AND ZPC.CatalogID = @CatalogId
					  AND ZPC.PortalID = @PortalId
                      AND ZCN.ActiveInd = 1),
                                            
			  CATLIST( CategoryId, CategoryNodeID )
				AS (SELECT CategoryId,
                      CategoryNodeID
               FROM   CategoryList
               UNION ALL
               SELECT ZCN.CategoryID,
                      ZCN.CategoryNodeID
               FROM   ZNodeCategoryNode ZCN
                      INNER JOIN CATLIST d
                        ON ZCN.ParentCategoryNodeID = d.CategoryNodeID
               WHERE  ZCN.CatalogID = @CatalogId
                      AND ZCN.ActiveInd = 1)
                      
		SELECT ProductId
               FROM   ZNodeProductCategory
               WHERE  CategoryID IN (SELECT CategoryId
                                     FROM   CATLIST);
  END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCategoryNode_GetByCategoryID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeCategoryNode table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCategoryNode_GetByCategoryID
(

	@CategoryID int   
)
AS


				SELECT
					[CategoryNodeID],
					[CatalogID],
					[CategoryID],
					[ParentCategoryNodeID],
					[BeginDate],
					[EndDate],
					[Theme],
					[MasterPage],
					[CSS],
					[DisplayOrder],
					[ActiveInd]
				FROM
					[dbo].[ZNodeCategoryNode]
				WHERE
					[CategoryID] = @CategoryID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetProductsByIds]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [ZNode_GetProductsByIds]
(@ProductIds NVARCHAR(MAX)) 
AS
BEGIN

      CREATE TABLE #ProductSPLITER
      (
        ID INT ,RowIndex INT )

      INSERT INTO
          #ProductSPLITER
          (
            ID ,
            RowIndex )
          SELECT
              [VALUE] ,
              RowNumber
          FROM
              SC_Splitter(@ProductIds , ',')

      SELECT
          [ProductID] ,
          [Name] ,
          [RetailPrice] ,
          [SalePrice] ,
          [WholesalePrice] ,
          [ImageFile] ,
          [DisplayOrder] ,
          [CallForPricing] ,
          [ShortDescription] ,
          [ProductNum] ,
          [InStockMsg] ,
          [OutOfStockMsg] ,
          [AllowBackOrder] ,
          [BackOrderMsg] ,
          [SEOURL] ,
          [NewProductInd] ,
          [ImageAltTag] ,
          [TaxClassID] ,
          [ManufacturerID] ,
          [ProductTypeID] ,
          [ActiveInd] ,
          [HomepageSpecial] ,
          [FeaturedInd] ,
          PS.RowIndex ,
          ( SELECT
                COUNT(1)
            FROM
                ZNodeProductImage
            WHERE
                ProductID = ZNodeProduct.ProductId
                AND ShowOnCategoryPage = 1
                AND LEN(ImageFile) > 0
                AND ImageFile IS NOT NULL ) AS AlternateProductImageCount ,
          QuantityOnHand = ( SELECT
                                 QuantityOnHand
                             FROM
                                 ZNodeProductInventory
                             WHERE
                                 ZNodeProductInventory.ProductNum = ZNodeProduct.ProductNum ) ,
          ( SELECT
                ReviewRating = IsNull(AVG(Rating) , 0) ,
                TotalReviews = IsNull(COUNT(ProductId) , 0)
            FROM
                ZNodeReview review
            WHERE
                review.ProductID = ZNodeProduct.ProductId
                AND review.Status = 'A'
            FOR
                XML PATH('') ,
                    TYPE )
      FROM
          ProductsView ZNodeProduct
      INNER JOIN #ProductSPLITER PS
      ON  ZNodeProduct.ProductID = PS.ID
      FOR
          XML PATH('ZNodeProduct') ;
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductsByTagIDs]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetProductsByTagIDs](@TagIds varchar(Max),@CategoryId int)  
AS   
BEGIN  

	DECLARE	@ZNodeTagSplit TABLE(TagId INT); 

	INSERT INTO 
		@ZNodeTagSplit (TagId)
	(SELECT
			[Value]	
	FROM 
			SC_SPLITTER(@TagIds,','))
 
  -- Select Products that matches tags.
  SELECT ProductID INTO #TEMP 
	FROM ZNodeTagProductSKU 
	WHERE TagId IN (SELECT TagID FROM @ZNodeTagSplit) AND ProductID IS NOT NULL   
  UNION ALL
  SELECT ProductID 
	FROM ZNODESKU 
	WHERE SKUID IN(SELECT SkuID FROM ZNodeTagProductSKU 
					WHERE TagId IN (SELECT TagID FROM @ZNodeTagSplit) AND SkuID IS NOT NULL )

 -- Select Products that matches all tags.
  SELECT ProductId,
             ( Row_number() OVER (ORDER BY Displayorder, ProductID ASC) ) 'Displayorder',
             ( Row_number() OVER (ORDER BY RetailPrice ASC) )  'RetailPrice',
             ( Row_number() OVER (ORDER BY RetailPrice DESC) )  'RetailPriceDESC'
      FROM   ProductsView ZNodeProduct
  WHERE ProductID IN     
	 (SELECT ProductID from ZNodeProductCategory WHERE ProductID IN   
			(SELECT ProductID FROM #TEMP GROUP BY ProductID HAVING Count(ProductID) = (SELECT Count(TagID) FROM @ZNodeTagSplit))
	 AND ActiveInd = 1 AND CategoryID = @CategoryId)
 AND ZNodeProduct.ActiveInd = 1 ORDER BY ZNodeProduct.DisplayOrder ASC
 
 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsSupplierList]'
GO


CREATE PROCEDURE [dbo].[ZNode_ReportsSupplierList]
    (
      @FromDate DateTime = NULL,
      @ToDate DateTime = NULL,
      @PortalId VARCHAR(10),
      @SupplierId int	
    )
AS 
    BEGIN                                      
        SET NOCOUNT ON ;                           

		-- Fetch only SupplierId assigned items (Product, Sku, AddOnValue)
        SELECT ProductNum, SKU, SupplierID 
			INTO #SupplierTable 
			FROM ZNodeProduct 
			WHERE (SupplierId IS NOT NULL) AND (SupplierId = @SupplierId OR @SupplierId = 0 )  
		UNION
		SELECT NULL, SKU, SupplierId
			 FROM   ZNodeSku
			 WHERE  (SupplierId IS NOT NULL) AND (SupplierId = @SupplierId OR @SupplierId = 0)
		UNION
		SELECT NULL, SKU, SupplierId
			 FROM   ZNodeAddOnValue
			 Where  (SupplierId IS NOT NULL) AND (SupplierId = @SupplierId OR @SupplierId = 0)
								 
		SELECT ProductNum, SKU, OrderID, OrderLineItemID 
		INTO #OrderLineItem 
		FROM ZNodeOrderLineItem OL

		SELECT ProductNum, Sku, OrderID, OrderLineItemID 
		INTO #OrderLineItemTable 
		FROM #OrderLineItem OL
		WHERE OL.ProductNum IN (SELECT ProductNum
								FROM #SupplierTable)		                                      		
			  OR OL.Sku IN (SELECT Sku
								FROM #SupplierTable)		                                      										

		SELECT  DISTINCT O.[OrderID],
                O.OrderDate,
                O.[ShipFirstName],
                O.[ShipLastName],
                O.[ShipCompanyName],
                O.[ShipStreet],
                O.[ShipStreet1],
                O.[ShipCity],
                O.[ShipStateCode],
                O.[ShipPostalCode],
                O.[ShipCountry],
                O.[ShipPhoneNumber],
                P.[StoreName] AS 'StoreName',
                ( SELECT TOP 1
                            SU.[Name]
                  FROM      ZNodeSupplier SU
                  WHERE     SU.SupplierId = S.SupplierID
                ) AS 'Custom1',
                S.SupplierID
        FROM ZNodeOrder O
        INNER JOIN #OrderLineItemTable OL ON O.OrderID = OL.OrderID         
        INNER JOIN ZNodePortal P ON P.PortalId = O.PortalId
        INNER JOIN #SupplierTable S ON S.ProductNum = OL.ProductNum OR S.Sku = OL.Sku         
        WHERE   (P.PortalID = @PortalId OR @PortalId = '0')
                AND O.OrderDate BETWEEN @FromDate AND @ToDate                   
                AND S.SupplierId IS NOT NULL

                
                -- Select Order Line Item
                
        SELECT  Ol.OrderId,
                Ol.SKU,
                Ol.Quantity,
                Ol.Name,
                Ol.Price,
                S.SupplierID
        FROM    ZNodeOrderLineItem Ol            
        INNER JOIN #SupplierTable S ON S.ProductNum = OL.ProductNum OR S.Sku = OL.Sku         
        WHERE   Ol.OrderLineItemID IN (
                SELECT  O.[OrderLineItemID]
                FROM    #OrderLineItemTable O)
        ORDER BY Ol.OrderID DESC
    END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetSkuInventoryListByFilter]'
GO

  
ALTER PROCEDURE [dbo].[ZNode_GetSkuInventoryListByFilter](@ProductType Int = Null)             
AS                
 BEGIN                
 -- SET NOCOUNT ON added to prevent extra result sets from            
 -- interfering with SELECT statements.            

IF (@ProductType = 1)    
BEGIN
  SELECT   
   ProductInventoryID,  
   Name = (SELECT TOP 1 Name FROM ZNodeProduct WHERE ZNodeProductInventory.ProductNum = ProductNum),  
   ProductNum,  
   Sku  = (SELECT TOP 1 SKU FROM ZNodeProduct WHERE ZNodeProductInventory.ProductNum = ProductNum),  
   QuantityOnHand,  
   ReOrderLevel   
  FROM ZNodeProductInventory        
END
ELSE IF (@ProductType = 2) 
BEGIN
       
  SELECT   
	  SKU.SKUInventoryID,  
	  SKU.SKU,
	  SKU.QuantityOnHand,   
	  SKU.ReOrderLevel   
	 FROM ZNodeSKUInventory SKU  
END
ELSE IF (@ProductType = 3) 
BEGIN
        
	SELECT   
	   ZNodeAddOnValueInventory.AddOnValueInventoryID,   
	   Name = (SELECT TOP 1 Name FROM ZNodeAddOnValue WHERE ZNodeAddOnValueInventory.SKU = SKU),  
	   ZNodeAddOnValueInventory.Sku,  
	   ZNodeAddOnValueInventory.QuantityOnHand,   
	   ZNodeAddOnValueInventory.ReOrderLevel   
	  FROM ZNodeAddOnValueInventory   
END  
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchBestSellerList]'
GO


-- ==========================================================   
-- Create date: July 02, 2006
-- Update date: May 26,2010        
-- Description: Retrieve product and return a serialized xml                          
-- ==========================================================
ALTER PROCEDURE [dbo].[ZNode_SearchBestSellerList]
    (
      @CatalogID INT,
      @DisplayItem INT = 0,
      @CategoryId INT,
      @PortalId INT
    )
AS 
    BEGIN         
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.                    
        SET NOCOUNT ON ;                     

    
        WITH PRODUCTCATEGORYLIST AS
        (
			SELECT  
					ZNodeProduct.ProductID
            FROM    
					ProductsView ZNodeProduct
            JOIN 
					ZNodeProductCategory ZPC 
			ON 
					ZNodeProduct.ProductID = ZPC.ProductID
            JOIN 
					ZNodeCategoryNode ZCN 
			ON 
					ZPC.CategoryID = ZCN.CategoryID
            JOIN 
					ZNodePortalCatalog PC ON ZCN.CatalogID = PC.CatalogID
            WHERE   
					ZPC.ActiveInd = 1
			AND
					ZCN.ActiveInd = 1
            AND 
					PC.PortalID = @PortalId
			AND 
					PC.CatalogID = @CatalogID
            AND 
					ZCN.CategoryID = 
						CASE WHEN 
							@CategoryId > 0
                        THEN 
							@CategoryId
                        ELSE 
							ZCN.CategoryID
                        END
            AND 
					ZNodeProduct.ProductNum IN (
                        SELECT TOP (@DisplayItem)
							ProductNum
						FROM    
							ZNodeOrderLineItem x
                        JOIN 
								ZNodeOrder y 
						ON 
								x.OrderId = y.OrderId
                        WHERE   
								y.PortalId = @PortalId
                        AND 
								x.ParentOrderLineItemID IS NULL
						AND
								x.ProductNum IS NOT NULL
                        GROUP BY 
								ProductNum
                        HAVING  SUM
								(Quantity) > 0
                        ORDER BY 
								SUM(x.Quantity) DESC )

		) 
        SELECT  TOP 
				(@DisplayItem) ZNodeProduct.ProductID,
                ZNodeProduct.[Name],
                ZNodeProduct.ProductNum,
                ZNodeProduct.RetailPrice,
                ZNodeProduct.SalePrice,
                ZNodeProduct.WholesalePrice,
                ZNodeProduct.ShortDescription,
                ZNodeProduct.CallForPricing,
                ZNodeProduct.ImageFile,
                ZNodeProduct.SEOURL,
                ZNodeProduct.NewProductInd,
                ZNodeProduct.FeaturedInd,
                ZNodeProduct.DisplayOrder,
                ZNodeProduct.ImageAltTag,
                ZNodeProduct.TaxClassID,
                ( SELECT    ReviewRating = ISNULL(AVG(Rating), 0),
                            TotalReviews = ISNULL(COUNT(ProductId), 0)
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.Status = 'A'
                FOR
                  XML PATH(''),
                      TYPE
                )
        FROM    
				ProductsView ZNodeProduct
        JOIN
				PRODUCTCATEGORYLIST PCL
		ON
				PCL.ProductID = ZNodeProduct.ProductID
        FOR     XML AUTO,
                    TYPE,
       
            ELEMENTS
    END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchCategories]'
GO
SET ANSI_NULLS OFF
GO

ALTER PROCEDURE [dbo].[ZNode_SearchCategories]
(
@Name VARCHAR(MAX),
@CatalogID int
)
AS
BEGIN

	SELECT CategoryID ,
		Name ,
		Title ,
		ShortDescription ,
		[Description] ,
		ImageFile ,
		ImageAltTag ,
		VisibleInd ,
		SubCategoryGridVisibleInd ,
		SEOTitle ,
		SEOKeywords ,
		SEODescription ,
		AlternateDescription ,
		DisplayOrder ,
		Custom1 ,
		Custom2 ,
		Custom3 ,
		SEOURL ,
		CatalogName 
		FROM 
		(SELECT
			ZNodeCategory.CategoryID ,
			ZNodeCategory.Name ,
			ZNodeCategory.Title ,
			ZNodeCategory.ShortDescription ,
			ZNodeCategory.[Description] ,
			ZNodeCategory.ImageFile ,
			ZNodeCategory.ImageAltTag ,
			ZNodeCategory.VisibleInd ,
			ZNodeCategory.SubCategoryGridVisibleInd ,
			ZNodeCategory.SEOTitle ,
			ZNodeCategory.SEOKeywords ,
			ZNodeCategory.SEODescription ,
			ZNodeCategory.AlternateDescription ,
			ZNodeCategory.DisplayOrder ,
			ZNodeCategory.Custom1 ,
			ZNodeCategory.Custom2 ,
			ZNodeCategory.Custom3 ,
			ZNodeCategory.SEOURL ,
			ZNodeCatalog.Name CatalogName,
			ZNodeCatalog.CatalogID
		FROM
			ZNodeCategory
			LEFT OUTER JOIN ZNodeCategoryNode
					ON  ZNodeCategory.CategoryID = ZNodeCategoryNode.CategoryID
					AND ZNodeCategoryNode.CatalogID = ( CASE
															 WHEN @CatalogID > 0 THEN @CatalogID
															 ELSE ZNodeCategoryNode.CatalogID
														END )
			LEFT OUTER JOIN ZNodeCatalog
					ON  ZNodeCatalog.CatalogID = ZNodeCategoryNode.CatalogID
		WHERE
			ZNodeCategory.Name LIKE '%' + @Name + '%') CategoryList    
    WHERE 
		CatalogID = @CatalogID OR (@CatalogID = 0 OR @CatalogID IS NULL);
		
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetProductsByCategoryID]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ZNode_GetProductsByCategoryID]
    (
      @CategoryId INT = 0,
      @PortalId   INT = 0,
      @LocaleId   INT = 0
    )
As 
    BEGIN

        WITH    CATLIST ( CategoryNodeId, CatalogId, CategoryId )
                  AS ( ( SELECT c.CategoryNodeID,
                                c.CatalogID,
                                c.CategoryID
                         FROM   ZNodeCategoryNode c
                                INNER JOIN ZNodeCatalog clog ON c.CatalogID = clog.CatalogID
                                INNER JOIN ZNodePortalCatalog pc ON clog.CatalogID = pc.CatalogID
                         WHERE  pc.PortalId = @PortalId
								AND c.ActiveInd = 1
                                AND pc.LocaleID = @LocaleId
                                AND c.CategoryID = ( case when @CategoryId > 0
                                                          then @CategoryId
                                                          else categoryid
                                                     end )
                       )
                       UNION ALL
                       SELECT   c.categoryNodeId,
                                c.catalogId,
                                c.categoryid
                       FROM     ZNodecategoryNode c
                                INNER JOIN CATLIST d ON c.parentcategoryNodeid = d.CategoryNodeId
                                AND c.CatalogID = d.CatalogID
                       WHERE    c.CategoryID != @CategoryId
								AND c.ActiveInd = 1
                     )
					, PRODUCTCATEGORYLIST ( ProductId )
					AS
					(
                    SELECT  ProductID
                    FROM    ZNodeProductCategory
                    WHERE   CategoryID IN ( SELECT  CategoryId
                                            FROM    CATLIST ) AND ActiveInd = 1)
                                            
        SELECT  pv.ProductId,
                ( ROW_NUMBER() OVER ( ORDER BY pv.Displayorder, pv.ProductID ASC ) ) "DisplayOrder",
                ( ROW_NUMBER() OVER ( ORDER BY pv.RetailPrice ASC ) ) "RetailPrice",
                ( ROW_NUMBER() OVER ( ORDER BY pv.RetailPrice DESC ) ) "RetailPriceDESC"
        FROM    [ProductsView] pv 
        WHERE ProductID IN 
				(SELECT DISTINCT ProductID FROM PRODUCTCATEGORYLIST)
    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCategoryNode_GetByParentCategoryNodeID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeCategoryNode table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCategoryNode_GetByParentCategoryNodeID
(

	@ParentCategoryNodeID int   
)
AS


				SELECT
					[CategoryNodeID],
					[CatalogID],
					[CategoryID],
					[ParentCategoryNodeID],
					[BeginDate],
					[EndDate],
					[Theme],
					[MasterPage],
					[CSS],
					[DisplayOrder],
					[ActiveInd]
				FROM
					[dbo].[ZNodeCategoryNode]
				WHERE
					[ParentCategoryNodeID] = @ParentCategoryNodeID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetCategoryHierarchy]'
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[ZNode_GetCategoryHierarchy]
(
  @PortalId INT = 0 ,
  @CategoryId INT = 0 ,
  @CatalogId INT = NULL
)
RETURNS TABLE
AS
RETURN (

	WITH CATLIST( CategoryId, CategoryNodeID )
          AS ( SELECT
                   ZCN.CategoryID ,
                   ZCN.CategoryNodeID
               FROM
                   ZNodeCategoryNode ZCN
               INNER JOIN ZNodeCatalog ZCG
               ON  ZCN.CatalogID = ZCG.CatalogID
               INNER JOIN ZNodePortalCatalog ZPC
               ON  ZCG.CatalogID = ZPC.CatalogID
               WHERE
                   ZPC.PortalID = @PortalId
                   AND ZCG.CatalogID = @CatalogId
                   AND ZCN.CategoryID = @CategoryId 
			   UNION ALL 
			   
			   SELECT
					ZCN.CategoryID ,
					ZCN.CategoryNodeID
				  FROM
					  ZNodeCategoryNode ZCN
				  INNER JOIN ZNodeCatalog ZCG
				  ON  ZCN.CatalogID = ZCG.CatalogID
				  INNER JOIN ZNodePortalCatalog ZPC
				  ON  ZCG.CatalogID = ZPC.CatalogID
				  INNER JOIN CATLIST d
				  ON  ZCN.ParentCategoryNodeID = d.CategoryNodeID
				  WHERE
					  ZPC.PortalID = @PortalId
					  AND ZCG.CatalogID = @CatalogId )
		
          -- get ids
          SELECT
              CategoryId
          FROM
              CATLIST )
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetProductsBySearch1_XML]'
GO

CREATE PROCEDURE [dbo].[ZNode_GetProductsBySearch1_XML]
@PortalId INT=0, @Keyword VARCHAR (4000)=NULL, @Delimiter VARCHAR (10)=' ', @CategoryId INT=0, @SKU VARCHAR (50)=NULL, @ProductNum VARCHAR (50)=NULL, @CatalogId INT=NULL
AS
BEGIN
    SET NOCOUNT ON;
    CREATE TABLE #ProductList
    (
        ProductId INT
    );
    DECLARE @Count AS INT;
    SET @Count = 0;
    IF @CategoryId > 0
        BEGIN
            INSERT #ProductList (ProductId)
            (SELECT DISTINCT ProductID
             FROM   ZNodeProductCategory
             WHERE  CategoryID IN (SELECT CategoryID
                                   FROM   [dbo].[ZNode_GetCategoryHierarchy] (@PortalID, @CategoryId, @CatalogId)) AND ActiveInd = 1 );
        END
    ELSE
        BEGIN
            INSERT #ProductList (ProductId)
            (SELECT DISTINCT ProductID
             FROM   ZNodeProductCategory
             WHERE  CategoryID IN (SELECT CategoryID
                                   FROM   ZNodeCategoryNode
                                   WHERE  CatalogID = @CatalogId) AND ActiveInd = 1 );
        END
        
    IF LEN(@SKU) <> 0
        BEGIN
            INSERT #ProductList (ProductId)
            SELECT ProductId
            FROM   ZNodeSKU
            WHERE  Sku LIKE @SKU + '%';
            SET @Count += 1;
        END
        
    IF (LEN(@Keyword) <> 0)
        BEGIN
            SET @Count += 1;
            INSERT #ProductList (ProductId)
            SELECT pr.ProductID
            FROM   ProductsView AS pr
                   INNER JOIN
                   #ProductList AS PC
                   ON pr.ProductID = PC.ProductID
                   LEFT OUTER JOIN
                   ZNodeManufacturer AS m
                   ON m.ManufacturerID = pr.ManufacturerID
                   LEFT OUTER JOIN
                   ZNodeProductType AS t
                   ON t.ProductTypeId = pr.ProductTypeID
            WHERE  EXISTS (SELECT 1
                           FROM   SC_Splitter (@Keyword, ' ')
                           WHERE  PATINDEX('%' + Value + '%', Pr.Name) > 0
                                  OR PATINDEX('%' + Value + '%', pr.SEOTitle) > 0
                                  OR PATINDEX('%' + Value + '%', pr.SEODescription) > 0
                                  OR PATINDEX('%' + Value + '%', pr.ProductNum) > 0
                                  OR PATINDEX('%' + Value + '%', m.Name) > 0
                                  OR PATINDEX('%' + Value + '%', t.Name) > 0);
        END
    SELECT P.ProductID,
           Row_number() OVER ( ORDER BY P.Displayorder, P.ProductID ASC) AS 'DisplayOrder',
           Row_number() OVER ( ORDER BY P.RetailPrice ASC) AS 'RetailPrice',
           Row_number() OVER ( ORDER BY P.RetailPrice DESC) AS 'RetailPriceDESC'
    FROM   ProductsView AS P
    WHERE  (LEN(@ProductNum) = 0
            OR P.ProductNum LIKE @ProductNum + '%')
           AND EXISTS (SELECT   1
                       FROM     #ProductList
                       WHERE    P.ProductID = ProductID
                       GROUP BY ProductId
                       HAVING   COUNT(ProductID) > @COUNT)
           AND (LEN(@SKU) = 0
                OR P.SKU LIKE @SKU + '%');
END  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetProductsBySearch2_XML]'
GO
CREATE PROCEDURE [dbo].[ZNode_GetProductsBySearch2_XML]
@PortalId INT=0, @Keywords VARCHAR (4000)=NULL, @Delimiter VARCHAR (10)=' ', @CategoryId INT=0, @SKU VARCHAR (50)=NULL, @ProductNum VARCHAR (50)=NULL, @CatalogId INT=NULL
AS
BEGIN
    SET NOCOUNT ON;
    CREATE TABLE #ProductList
    (
        ProductId INT
    );
    
    DECLARE @Count AS INT;
    SET @Count = 0;
    
    IF @CategoryId > 0
        BEGIN
            INSERT #ProductList (ProductId)
            (SELECT DISTINCT ProductID
             FROM   ZNodeProductCategory
             WHERE  CategoryID IN (SELECT CategoryID
                                   FROM   [dbo].[ZNode_GetCategoryHierarchy] (@PortalID, @CategoryId, @CatalogId)) AND ActiveInd = 1 );
        END
    ELSE
        BEGIN
            INSERT #ProductList (ProductId)
            (SELECT DISTINCT ProductID
             FROM   ZNodeProductCategory
             WHERE  CategoryID IN (SELECT CategoryID
                                   FROM   ZNodeCategoryNode
                                   WHERE  CatalogID = @CatalogId) AND ActiveInd = 1 );
        END
    IF LEN(@SKU) <> 0
        BEGIN
            INSERT #ProductList (ProductId)
            SELECT ProductId
            FROM   ZNodeSKU
            WHERE  Sku LIKE @SKU + '%';
            SET @Count += 1;
        END
    IF (LEN(@Keywords) <> 0)
        BEGIN
            SET @Count += 1;
            DECLARE @TempANDkeyword AS VARCHAR (4000);
            DECLARE @MaxRows AS INT;
            DECLARE @RowIndex AS INT;
            SET @MaxRows = 0;
            SET @RowIndex = 1;
            SELECT VALUE,
                   RowNumber
            INTO   #TempANDKeyWords
            FROM   SC_Splitter (@Keywords, @Delimiter);
            SELECT @MaxRows = COUNT(1)
            FROM   #TempANDKeyWords;
            CREATE TABLE #TEMP_PRODUCTS_KEYWORD_MATCH
            (
                ProductId INT
            );
            WHILE @RowIndex <= @MaxRows
                BEGIN
                    SELECT @TempANDkeyword = VALUE
                    FROM   #TempANDKeyWords
                    WHERE  RowNumber = @RowIndex;
                    INSERT #TEMP_PRODUCTS_KEYWORD_MATCH (ProductId)
                    SELECT pr.ProductID
                    FROM   ProductsView AS pr
                           INNER JOIN
                           #ProductList AS PC
                           ON pr.ProductID = PC.ProductID
                           LEFT OUTER JOIN
                           ZNodeManufacturer AS m
                           ON m.ManufacturerID = pr.ManufacturerID
                           LEFT OUTER JOIN
                           ZNodeProductType AS t
                           ON t.ProductTypeId = pr.ProductTypeID
                    WHERE  PATINDEX('%' + @TempANDkeyword + '%', Pr.Name) > 0
                           OR PATINDEX('%' + @TempANDkeyword + '%', pr.SEOTitle) > 0
                           OR PATINDEX('%' + @TempANDkeyword + '%', pr.SEODescription) > 0
                           OR PATINDEX('%' + @TempANDkeyword + '%', pr.ProductNum) > 0
                           OR PATINDEX('%' + @TempANDkeyword + '%', m.Name) > 0
                           OR PATINDEX('%' + @TempANDkeyword + '%', t.Name) > 0;
                    SET @RowIndex = @RowIndex + 1;
                END
            INSERT #ProductList (ProductId)
            SELECT   ProductId
            FROM     #TEMP_PRODUCTS_KEYWORD_MATCH
            GROUP BY ProductId
            HAVING   COUNT(1) >= @MaxRows;
        END
        
    SELECT P.ProductID,
           Row_number() OVER ( ORDER BY P.Displayorder, P.ProductID ASC) AS 'DisplayOrder',
           Row_number() OVER ( ORDER BY P.RetailPrice ASC) AS 'RetailPrice',
           Row_number() OVER ( ORDER BY P.RetailPrice DESC) AS 'RetailPriceDESC'
    FROM   ProductsView AS P
    WHERE  (LEN(@ProductNum) = 0
            OR P.ProductNum LIKE @ProductNum + '%')
           AND EXISTS (SELECT   1
                       FROM     #ProductList
                       WHERE    P.ProductID = ProductID
                       GROUP BY ProductId
                       HAVING   COUNT(ProductID) > @COUNT)
           AND (LEN(@SKU) = 0
                OR P.SKU LIKE @SKU + '%');
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsAccounts]'
GO

CREATE PROCEDURE [dbo].[ZNode_ReportsAccounts]
    @FromDate DateTime = NULL,
    @ToDate DateTime = NULL,
    @PortalId VARCHAR(10) = '0'
AS 
    BEGIN                                      
        SET NOCOUNT ON ;
		
		CREATE TABLE #ProfileList
		(
			ProfileID INT            ,
			AccountID INT            ,
			Name      NVARCHAR (1000)
		);
		IF @PortalId <> '0'
			BEGIN
				INSERT INTO #ProfileList (ProfileID, AccountID, Name)
				SELECT P.ProfileID,
					   AP.AccountID,
					   P.Name
				FROM   ZNodeProfile AS P
					   INNER JOIN
					   ZNodeAccountProfile AS AP
					   ON P.ProfileID = AP.ProfileID
					   INNER JOIN
					   ZNodePortalProfile AS PP
					   ON AP.ProfileID = PP.ProfileID
						  AND PP.PortalID = @PortalId;
			END
		ELSE
			BEGIN
				INSERT INTO #ProfileList (ProfileID, AccountID, Name)
				SELECT P.ProfileID,
					   AP.AccountID,
					   P.Name
				FROM   ZNodeProfile AS P
					   INNER JOIN
					   ZNodeAccountProfile AS AP
					   ON P.ProfileID = AP.ProfileID
				WHERE  AP.ProfileID IN (SELECT ProfileID
										FROM   ZNodePortalProfile);
			END;
	        
		WITH     AccountList (AccountID, BillingFirstName, BillingLastName, ExternalAccountNo, BillingCompanyName, BillingEmailID, CreateDte, ProfileIdValue, ProfileName)
		AS       (SELECT A.AccountID,
						 A.BillingFirstName,
						 A.BillingLastName,
						 A.ExternalAccountNo,
						 A.BillingCompanyName,
						 A.BillingEmailID,
						 A.CreateDte,
						 A.[ProfileId] AS 'ProfileIdValue',
						 (SELECT   P.Name + ','
						  FROM     #ProfileList AS P
						  WHERE    (P.AccountID = A.AccountId)
						  ORDER BY P.Name
						  FOR      XML PATH ('')) AS ProfileName
				  FROM   ZNodeAccount AS A
				  WHERE  (A.CreateDte BETWEEN @FromDate AND @ToDate))
		SELECT   A.AccountID,
				 A.BillingFirstName,
				 A.BillingLastName,
				 A.ExternalAccountNo,
				 A.BillingCompanyName,
				 A.BillingEmailID,
				 A.CreateDte,
				 A.ProfileIdValue,
				 CASE WHEN (ProfileName IS NOT NULL
							AND LEN(ProfileName) > 1) THEN LEFT(ProfileName, LEN(ProfileName) - 1) ELSE ProfileName END AS ProfileName
		FROM     AccountList AS A
		ORDER BY A.AccountID DESC;
    END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FN_GetProductCountByCategory]'
GO

CREATE FUNCTION [dbo].[FN_GetProductCountByCategory] ( @CatalogId INT, @CategoryID    INT )
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ProductCount INT;
	
	SET @ProductCount = 0;

	 WITH CATLIST(CategoryId, CategoryNodeID) AS
		 (
			SELECT           
				ZCN.CategoryID
				,ZCN.CategoryNodeID 
		  FROM
			ZNodeCategoryNode ZCN
			INNER JOIN ZNodeCatalog ZCG ON  ZCN.CatalogID = ZCG.CatalogID
		 WHERE
			ZCG.CatalogID = @CatalogId
			AND ZCN.CategoryID = @CategoryId
			AND ZCN.ActiveInd = 1
		  
		  UNION ALL
		            
		  SELECT           
			ZCN.CategoryID			
			,ZCN.CategoryNodeID
		  FROM
			ZNodeCategoryNode ZCN
			INNER JOIN ZNodeCatalog ZCG ON  ZCN.CatalogID = ZCG.CatalogID			
			INNER JOIN CATLIST d  ON ZCN.ParentCategoryNodeID = d.CategoryNodeID 
		 WHERE
			ZCG.CatalogID = @CatalogId
			AND ZCN.ActiveInd = 1
		 ),
		 ProductCategoryList(ProductId) -- get list associated products        
			AS
			(
			 SELECT DISTINCT PC.ProductId FROM ZNodeProductCategory PC
			 INNER JOIN ProductsView P ON PC.ProductID = P.ProductID
			 WHERE CategoryID IN    
			 (SELECT Distinct(CategoryId) FROM CATLIST) AND PC.ActiveInd = 1			 
			)

		SELECT @ProductCount = Count(1) FROM ProductCategoryList

		-- Return the result of the function
		RETURN @ProductCount

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetShoppingCartNavigationItems]'
GO

-- =============================================    
-- Create date: July 17, 2006      
-- Updated date: Mar 17, 2010     
-- Description: 
-- =============================================      
ALTER PROCEDURE [dbo].[ZNode_GetShoppingCartNavigationItems]
(      
	 -- Add the parameters for the stored procedure here             
	@PortalId int,
	@LocaleId int
)
AS       
BEGIN      
	 -- SET NOCOUNT   ON added to prevent extra result sets from      
	 -- interfering with SELECT statements.      
	 SET NOCOUNT ON;      

		SELECT ZCN.CategoryNodeId, 
			ZCN.CategoryId, 
			ZC.[Name], 
			ZCN.ParentCategoryNodeId,  
			ZC.SEOURL, 
			ZCN.ActiveInd "VisibleInd", 
			ZCN.DisplayOrder,
			(SELECT [dbo].[FN_GetProductCountByCategory](ZCG.CatalogID, ZC.CategoryId)) AS ProductCount FROM 
			ZNodeCategory ZC
			INNER JOIN ZNodeCategoryNode ZCN ON ZC.CategoryID = ZCN.CategoryID
			INNER JOIN ZNodeCatalog ZCG ON ZCN.CatalogID = ZCG.CatalogID
			INNER JOIN ZNodePortalCatalog ZPC ON ZCG.CatalogID = ZPC.CatalogID
		WHERE             
			ZPC.PORTALID = @PortalId  AND
			ZPC.LocaleID = @LocaleId AND
			ZCN.ActiveInd = 1 AND
			ZC.VisibleInd = 1
		ORDER By DisplayOrder;
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_Reports]'
GO



ALTER PROCEDURE [dbo].[ZNode_Reports]
    (
      @Filter VARCHAR(MAX) = NULL,
      @FromDate VARCHAR(MAX) = NULL,
      @ToDate VARCHAR(MAX) = NULL,
      @SupplierID VARCHAR(MAX) = NULL,
      @Custom1 VARCHAR(MAX) = '',
      @Custom2 VARCHAR(MAX) = ''
    )
AS 
    BEGIN                                      
        SET NOCOUNT ON ;                                      
        -- Report Name
        
        DECLARE @Orders VARCHAR(100)= 'Orders'
        
        DECLARE @Picklist VARCHAR(100)= 'Picklist'
        
        DECLARE @EmailOptInCustomer VARCHAR(100)= 'EmailOptInCustomer'
        
        DECLARE @Accounts VARCHAR(100)= 'Accounts'
        
        DECLARE @ActivityLog VARCHAR(100)= 'ActivityLog'
        
        DECLARE @ReOrder VARCHAR(100)= 'ReOrder'
        
        DECLARE @ServiceRequest VARCHAR(100)= 'ServiceRequest'
        
        DECLARE @SupplierList VARCHAR(100)= 'SupplierList'
        
        -- Coupon Usage
        DECLARE @CouponUsage VARCHAR(100)= 'CouponUsage'
        DECLARE @CouponUsageByDay VARCHAR(100)= 'CouponUsageByDay'
        DECLARE @CouponUsageByWeek VARCHAR(100)= 'CouponUsageByWeek'
        DECLARE @CouponUsageByMonth VARCHAR(100)= 'CouponUsageByMonth'
        DECLARE @CouponUsageByQuarter VARCHAR(100)= 'CouponUsageByQuarter'
        DECLARE @CouponUsageByYear VARCHAR(100)= 'CouponUsageByYear'
        
        -- Sales Tax
        DECLARE @SalesTax VARCHAR(100)= 'SalesTax'
        DECLARE @SalesTaxByMonth VARCHAR(100)= 'SalesTaxByMonth'
        DECLARE @SalesTaxByQuarter VARCHAR(100)= 'SalesTaxByQuarter'
        
        -- Frequent Customer
        DECLARE @FrequentCustomer VARCHAR(100)= 'FrequentCustomer'
        DECLARE @FrequentCustomerByDay VARCHAR(100)= 'FrequentCustomerByDay'
        DECLARE @FrequentCustomerByWeek VARCHAR(100)= 'FrequentCustomerByWeek'
        DECLARE @FrequentCustomerByMonth VARCHAR(100)= 'FrequentCustomerByMonth'
        DECLARE @FrequentCustomerByQuarter VARCHAR(100)= 'FrequentCustomerByQuarter'
        DECLARE @FrequentCustomerByYear VARCHAR(100)= 'FrequentCustomerByYear'
        
        DECLARE @TopSpendingCustomer VARCHAR(100)= 'TopSpendingCustomer'
        DECLARE @TopSpendingCustomerByDay VARCHAR(100)= 'TopSpendingCustomerByDay'
        DECLARE @TopSpendingCustomerByWeek VARCHAR(100)= 'TopSpendingCustomerByWeek'
        DECLARE @TopSpendingCustomerByMonth VARCHAR(100)= 'TopSpendingCustomerByMonth'
        DECLARE @TopSpendingCustomerByQuarter VARCHAR(100)= 'TopSpendingCustomerByQuarter'
        DECLARE @TopSpendingCustomerByYear VARCHAR(100)= 'TopSpendingCustomerByYear'
        
        DECLARE @TopEarningProduct VARCHAR(100)= 'TopEarningProduct'
        DECLARE @TopEarningProductByDay VARCHAR(100)= 'TopEarningProductByDay'
        DECLARE @TopEarningProductByWeek VARCHAR(100)= 'TopEarningProductByWeek'
        DECLARE @TopEarningProductByMonth VARCHAR(100)= 'TopEarningProductByMonth'
        DECLARE @TopEarningProductByQuarter VARCHAR(100)= 'TopEarningProductByQuarter'
        DECLARE @TopEarningProductByYear VARCHAR(100)= 'TopEarningProductByYear'
        
        -- Best Seller 
        DECLARE @BestSeller VARCHAR(100)= 'BestSeller'
        DECLARE @BestSellerByDay VARCHAR(100)= 'BestSellerByDay'
        DECLARE @BestSellerByWeek VARCHAR(100)= 'BestSellerByWeek'
        DECLARE @BestSellerByMonth VARCHAR(100)= 'BestSellerByMonth'
        DECLARE @BestSellerByQuarter VARCHAR(100)= 'BestSellerByQuarter'
        DECLARE @BestSellerByYear VARCHAR(100)= 'BestSellerByYear'
        
        -- Affiliate Order 
        DECLARE @AffiliateOrder VARCHAR(100)= 'AffiliateOrder'
        DECLARE @AffiliateOrderByDay VARCHAR(100)= 'AffiliateOrderByDay'
        DECLARE @AffiliateOrderByWeek VARCHAR(100)= 'AffiliateOrderByWeek'
        DECLARE @AffiliateOrderByMonth VARCHAR(100)= 'AffiliateOrderByMonth'
        DECLARE @AffiliateOrderByQuarter VARCHAR(100)= 'AffiliateOrderByQuarter'
        DECLARE @AffiliateOrderByYear VARCHAR(100)= 'AffiliateOrderByYear'
        
        -- Popular Search  
        DECLARE @PopularSearch VARCHAR(100)= 'PopularSearch'
        DECLARE @PopularSearchByDay VARCHAR(100)= 'PopularSearchByDay'
        DECLARE @PopularSearchByWeek VARCHAR(100)= 'PopularSearchByWeek'
        DECLARE @PopularSearchByMonth VARCHAR(100)= 'PopularSearchByMonth'
        DECLARE @PopularSearchByQuarter VARCHAR(100)= 'PopularSearchByQuarter'
        DECLARE @PopularSearchByYear VARCHAR(100)= 'PopularSearchByYear'
        
                
        DECLARE @Diff int ;                                      
        DECLARE @Diff2 int ;                                      
        DECLARE @StartDate DATETIME ;                                      
        DECLARE @Year NVARCHAR(MAX) ;
        DECLARE @FinalDate NVARCHAR(MAX) ;     
        DECLARE @CompareDate DATETIME ;  
        DECLARE @CompareMonthDate DATETIME ;                                    
        DECLARE @CompareFinalDate NVARCHAR(MAX) ;
        DECLARE @TodayDate DATETIME ;

        SELECT  @TodayDate = GetDate() ;                                      
            
        SET @Diff = ( SELECT    DATEPART(dayofyear, @TodayDate)
                    ) - ( SELECT    DATEPART(day, @TodayDate)
                        ) ;                                        
        SET @Diff2 = @Diff + ( SELECT   DATEPART(day, @TodayDate)
                             ) - 1 ;                                        
        SET @StartDate = ( SELECT   DateAdd(dd, -@Diff2, @TodayDate)
                         ) ;                                        
        SET @FinalDate = ( SELECT   CONVERT(Varchar(10), @StartDate, 101)
                         ) ;                                        
        SET @CompareDate = ( SELECT DateAdd(dd, 1, @TodayDate)
                           ) ;                                        
        SET @CompareFinalDate = ( SELECT    CONVERT(Varchar(10), @CompareDate, 101)
                                ) ;      
        SET @Year = YEAR(@TodayDate) ;                    


		IF	@ToDate IS NOT NULL
		BEGIN
			SET @ToDate = (SELECT DateAdd(dd, 1, @ToDate));
		END;
		
        IF @Filter = '0' 
            BEGIN                          
                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate
            END
	
        IF @Filter = '1' 
            BEGIN                                                        
                SELECT  Prod.ProductId,
                        Prod.[Name],
                        Prod.ProductNum,
                        Prod.RetailPrice,
                        Prod.SalePrice,
                        SUM(ord.Quantity)
                FROM    ZNodeProduct Prod
                        INNER JOIN ZNodeOrderLineItem ord ON Prod.ProductNum = ord.ProductNum
                WHERE   LEN(ParentOrderLineItemID) = 0
                GROUP BY Prod.ProductID,
                        Prod.[Name],
                        Prod.ProductNum,
                        Prod.RetailPrice,
                        Prod.SalePrice,
                        Ord.ProductNum
                HAVING  SUM(Quantity) > 0
                ORDER BY SUM(ord.Quantity) DESC ;                          
		
                SET @Diff = ( SELECT    DATEPART(day, @TodayDate)
                            ) - 1 ;                                      
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                        

                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate
		
            END
	
        IF @Filter = '2' 
            BEGIN
                SET @StartDate = ( SELECT   DateAdd(week,
                                                    datediff(week, 0,
                                                             @TodayDate) - 1,
                                                    6)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareDate = ( SELECT DateAdd(dd, 1, @TodayDate)
                                   ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      
                                        
                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate
            END

        IF @Filter = '3' 
            BEGIN
                SET @StartDate = @TodayDate ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                                                             
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                        
		
                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate
            END
	
        IF @Filter = @Orders 
            BEGIN
		-- Custom 1: Portal Id
		-- Custom 2: Order State
                EXEC ZNode_ReportsCustom @FromDate, @ToDate, @Custom1,
                    @Custom2
            END
	
        IF @Filter = @Picklist 
            BEGIN
                EXEC ZNode_ReportsPicklist @Custom1
            END
	
        IF @Filter = @EmailOptInCustomer 
            BEGIN
                EXEC dbo.ZNode_ReportsOptIn @Custom1
            END
	
        IF @Filter = @FrequentCustomer 
            BEGIN
                SELECT  @FinalDate = MIN(OrderDate),
                        @CompareFinalDate = MAX(OrderDate)
                FROM    ZNodeOrder
				SET @CompareDate = ( SELECT DateAdd(dd, 1, @CompareDate)) ;
                SET @CompareFinalDate = ( SELECT Convert(varchar(10), @CompareDate, 101)) ; 
                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END


        IF @Filter = @FrequentCustomerByDay 
            BEGIN
                SET @StartDate = @TodayDate ;               
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;     
	
                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
		
            END
	
        IF @Filter = @FrequentCustomerByWeek 
            BEGIN                     
                SET @StartDate = ( SELECT   DateAdd(week,
                                                    datediff(week, 0,
                                                             @TodayDate) - 1,
                                                    6)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                           
				EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END
            
        IF @Filter = @FrequentCustomerByMonth 
            BEGIN                                 
                SET @Diff = ( SELECT    DatePart(day, @TodayDate)
                            ) - 1 ;                                        
                SET @StartDate = ( SELECT   DateAdd(dd, -@Diff, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                  

                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END
	
        IF @Filter = @FrequentCustomerByQuarter 
            BEGIN                                 
                SET @Diff = ( SELECT    datepart(month, @TodayDate)
                            ) - 4 ;                                        
                SET @StartDate = ( SELECT   dateadd(month, -@Diff,
                                                    @TodayDate + 1)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                  

                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
         
            END                     

        IF @Filter = @FrequentCustomerByYear 
            BEGIN                                     
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)
                            ) - ( SELECT    datepart(day, @TodayDate)
                                ) ;                                        
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)
                                     ) - 1 ;                                        
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                        

                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,
                    @CompareFinalDate,@Custom1
            END
     
        IF @Filter = @TopSpendingCustomer 
            BEGIN                           
                SELECT  @FinalDate = MIN(OrderDate),
                        @CompareFinalDate = MAX(OrderDate)
                FROM    ZNodeOrder
				SET @CompareDate = ( SELECT DateAdd(dd, 1, @CompareDate)) ;
                SET @CompareFinalDate = ( SELECT Convert(varchar(10), @CompareDate, 101)) ; 
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
            END
                                  
-- For Most Volume Customers and day --                                        
        IF @Filter = @TopSpendingCustomerByDay 
            BEGIN                
                SET @StartDate = @TodayDate ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                   
		
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
            END
	                                  
-- For Most Volume Customers and week --                                        
        IF @Filter = @TopSpendingCustomerByWeek 
            BEGIN                                
                SET @StartDate = ( SELECT   Dateadd(week,
                                                    datediff(week, 0,
                                                             @TodayDate) - 1,
                                                    6)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                       
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      

		        EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
            END
  
                                  
-- For Most Volume Customers and month --                                        
        IF @Filter = @TopSpendingCustomerByMonth 
            BEGIN                                    
                SET @Diff = ( SELECT    Datepart(day, @TodayDate)
                            ) - 1 ;                          
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      
	
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
            END                              
           
-- For Most Volume Customers and quater--                                        
        IF @Filter = @TopSpendingCustomerByQuarter
            BEGIN                                    
                SET @Diff = ( SELECT    Datepart(month, @TodayDate)
                            ) - 4 ;                                        
                SET @StartDate = ( SELECT   Dateadd(month, -@Diff,
                                                    @TodayDate + 1)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                 

                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
            END                                  
                                  
-- For Most Volume Customers and year--                                        
        IF @Filter = @TopSpendingCustomerByYear 
            BEGIN                                   
                SET @Diff = ( SELECT    Datepart(dayofyear, @TodayDate)
                            ) - ( SELECT    Datepart(day, @TodayDate)
                                ) ;                                        
                SET @Diff2 = @Diff + ( SELECT   Datepart(day, @TodayDate)
                                     ) - 1 ;                                        
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff2, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                         

                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
            END
                                     
                                  
-- For Most Popular Product --                                        
        IF @Filter = @TopEarningProduct 
            BEGIN                              
                SELECT  @FinalDate = MIN(OrderDate),
                        @CompareFinalDate = MAX(OrderDate)
                FROM    ZNodeOrder
                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
	
            END                                  
                                  
-- For Most Popular Product and day --                                        
        IF @Filter = @TopEarningProductByDay 
            BEGIN
				SET @StartDate = @TodayDate;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101))
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;     
			
                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END                                  
                                  
-- For Most Popular Product and week --                                        
        IF @Filter = @TopEarningProductByWeek 
            BEGIN                          
                SET @StartDate = ( SELECT   Dateadd(week,
                                                    datediff(week, 0,
                                                             @TodayDate) - 1,
                                                    6)
                                 ) ;        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      

                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END                                 
                                  
-- For Most Popular Product and month --                                        
        IF @Filter = @TopEarningProductByMonth 
            BEGIN                                     
                SET @Diff = ( SELECT    Datepart(day, @TodayDate)
                            ) - 1 ;               
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;
		
                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END
	                                 
                                  
-- For Most Popular Product and Quater--                                        
        IF @Filter = @TopEarningProductByQuarter 
            BEGIN                          
                SET @Diff = ( SELECT    Datepart(month, @TodayDate)
                            ) - 4 ;                                        
                SET @StartDate = ( SELECT   Dateadd(month, -@Diff,
                                                    @TodayDate + 1)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                 
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      

                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END
	
-- For Most Popular Product and Year--                                        
        IF @Filter = @TopEarningProductByYear 
            BEGIN                                     
                SET @Diff = ( SELECT    Datepart(dayofyear, @TodayDate)
                            ) - ( SELECT    Datepart(day, @TodayDate)
                                ) ;                                        
                SET @Diff2 = @Diff + ( SELECT   Datepart(day, @TodayDate)
                                     ) - 1 ;                                        
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff2, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                     
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                         
				
                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END                            
                          
-- For Best Sellers                            
        IF @Filter = @BestSeller 
            BEGIN                           
                SELECT  @FinalDate = MIN(OrderDate),
                        @CompareFinalDate = MAX(OrderDate)
                FROM    ZNodeOrder
                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END                            
                          
-- For BestSellers and day --                                        
        IF @Filter = @BestSellerByDay 
            BEGIN                                     
                SET @StartDate = @TodayDate ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                   
	
                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END                                  
                                  
-- For BestSellers and week --                                        
        IF @Filter = @BestSellerByWeek 
            BEGIN                                     
                SET @StartDate = ( SELECT   Dateadd(week,
                                                    datediff(week, 0,
                                                             @TodayDate) - 1,
                                                    6)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                  
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      
			
                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END                                  
                                  
-- For BestSellers and month --                                        
        If @Filter = @BestSellerByMonth 
            BEGIN                                     
                SET @Diff = ( SELECT    datepart(day, @TodayDate)
                            ) - 1 ;                                        
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;            
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      

                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
            END                                      
                                  
-- For BestSellers and Quater--                                        
        IF @Filter = @BestSellerByQuarter 
            BEGIN                                     
                SET @Diff = ( SELECT    Datepart(month, @TodayDate)
                            ) - 4 ;                                        
                SET @StartDate = ( SELECT   Dateadd(month, -@Diff,
                                                    @TodayDate + 1)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      
                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1

            END                     
                                  
-- For BestSellers and Year--                    
        IF @Filter = @BestSellerByYear 
            BEGIN                                     
                SET @Diff = ( SELECT    Datepart(dayofyear, @TodayDate)
                            ) - ( SELECT    datepart(day, @TodayDate)
                                ) ;                                        
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)
                                     ) - 1 ;                                        
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;  

                EXEC ZNode_ReportsPopularFiltered @FinalDate,
                    @CompareFinalDate, @Custom1
                
            END                     
                          
-- For Most Popular Searches --                                        
        IF @Filter = @PopularSearch 
            BEGIN
                SELECT  @FinalDate = MIN(CreateDte),
                        @CompareFinalDate = MAX(CReateDte)
                FROM    ZNodeActivityLog
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate                                        
            END
	                                  
-- For Most Popular Searches and day --                                        
        IF @Filter = @PopularSearchByDay 
            BEGIN                                    
                SET @StartDate = @TodayDate ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                             
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                   
		
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate
            END                                  
                                  
-- For Most Popular Searches and week --                                        
        IF @Filter = @PopularSearchByWeek 
            BEGIN                                    
                SET @StartDate = ( SELECT   dateadd(week,
                                                    datediff(week, 0,
                                                             @TodayDate) - 1,
                                                    6)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      
		
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate
            END                                  
                                  
-- For Most Popular Searches and month --                                        
        If @Filter = @PopularSearchByMonth 
            BEGIN                                    
                SET @Diff = ( SELECT    datepart(day, @TodayDate)
                            ) - 1 ;                                        
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      

                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate
            END

                                  
-- For Most Popular Searches and quater--                                        
        IF ( @Filter = @PopularSearchByQuarter ) 
            BEGIN                        
                SET @StartDate = CAST(YEAR(getdate()) AS VARCHAR(4))
                    + CASE WHEN MONTH(getdate()) IN ( 1, 2, 3 ) THEN '/01/01'
                           WHEN MONTH(getdate()) IN ( 4, 5, 6 ) THEN '/04/01'
                           WHEN MONTH(getdate()) IN ( 7, 8, 9 ) THEN '/07/01'
                           WHEN MONTH(getdate()) IN ( 10, 11, 12 )
                           THEN '/10/01'
                      END  
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareMonthDate = ( SELECT    dateadd(Month, 3,
                                                            @StartDate)
                                        ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareMonthDate, 101)
                                        ) ;                                 
		
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate
            END                                 
                                  
-- For Most Popular Searches and year--                                        
        IF @Filter = @PopularSearchByYear 
            BEGIN                                   
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)
                            ) - ( SELECT    datepart(day, @TodayDate)
                                ) ;                                        
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)
                                     ) - 1 ;                                        
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                         
			
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate
            END
                     
-- For ActivityLog                           
        IF @Filter = @ActivityLog 
            BEGIN
		-- Custom1 : ZnodeActivity log category
		-- Custom2 : Znode Store name
                EXEC ZNode_ReportsActivityLog @FromDate, @Custom1, @Custom2 
            END                          
                          
-- For Inventory Re-Orderlevel                          
        IF @Filter = @ReOrder 
            BEGIN                                
                EXEC ZNode_ReportsReOrder @Custom1
            END
                               
-- For Customer Feedback                          
        IF @Filter = @ServiceRequest 
            BEGIN                 
                EXEC ZNode_ReportsFeedback @Custom1
            END                             
                                  
-- For Accounts                          
        IF @Filter = @Accounts 
            BEGIN                          
		-- Custom 1: Store name
                EXEC ZNode_ReportsAccounts @FromDate, @ToDate, @Custom1 ;                               
            END                          
                        
-- For Coupon Usage                          
        IF @Filter = @CouponUsage 
            BEGIN
                SELECT  @FinalDate = MIN(OrderDate),
                        @CompareFinalDate = MAX(OrderDate)
                FROM    ZNodeOrder	
				SET @CompareDate = ( SELECT DateAdd(dd, 1, @CompareDate)) ;
                SET @CompareFinalDate = ( SELECT Convert(varchar(10), @CompareDate, 101)) ; 
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,
                    @Custom1
            END                          
                        
-- For Coupon Usage and day --                           
        IF @Filter = @CouponUsageByDay 
            BEGIN                                    
                SET @StartDate = @TodayDate ;      
                                                 
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                         
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                    

                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,
                    @Custom1
            END
                        
-- For Coupon Usage and week --                                        
        IF @Filter = @CouponUsageByWeek 
            BEGIN                       
                SET @StartDate = ( SELECT   dateadd(week,
                                                    datediff(week, 0,
                                                             @TodayDate) - 1,
                                                    6)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      

                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,
                    @Custom1
            END                            
                                  
-- For Coupon Usage and month --                                        
        IF @Filter = @CouponUsageByMonth 
            BEGIN                                    
                SET @Diff = ( SELECT    datepart(day, @TodayDate)
                            ) - 1 ;                                        
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                      

                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,
                    @Custom1
            END                                                             
                                  
-- For Coupon Usage and quater--                                        
        IF @Filter = @CouponUsageByQuarter 
            BEGIN                                    
                SET @Diff = ( SELECT    datepart(month, @TodayDate)
                            ) - 4 ;                                       
                SET @StartDate = ( SELECT   dateadd(month, -@Diff,
                                                    @TodayDate + 1)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                 

                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,
                    @Custom1
            END                              
                                  
-- For Coupon Usage and year--                                        
        IF @Filter = @CouponUsageByYear 
            BEGIN                                   
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)
                            ) - ( SELECT    datepart(day, @TodayDate)
                                ) ;
                set @Diff2 = @Diff
                    + ( SELECT  datepart(day, ( select  getdate()
                                              ))
                      ) - 1 ;                                        
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                         
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,
                    @Custom1
            END                              
        
-- For Affiliate Customers                
        IF @Filter = @AffiliateOrder 
            BEGIN            
                SELECT  @FinalDate = MIN(OrderDate),
                        @CompareFinalDate = MAX(OrderDate)
                FROM    ZNodeOrder
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
            END
                
-- For Affiliate Customers by day --                                        
        IF @Filter = @AffiliateOrderByDay 
            BEGIN
                SET @StartDate = @TodayDate ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)
                                        ) ;                                   
		
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
            END                                  
                                  
-- For Affiliate Customers by week --                                        
        IF @Filter = @AffiliateOrderByWeek 
            BEGIN
                SET @StartDate = ( SELECT   Dateadd(week,
                                                    datediff(week, 0,
                                                             @TodayDate) - 1,
                                                    6)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                           

                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
            END
                                  
-- For Affiliate Customers by month --                                      
        IF @Filter = @AffiliateOrderByMonth 
            BEGIN                         
                SET @Diff = ( SELECT    datepart(day, @TodayDate)
                            ) - 1 ;                                        
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                  
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
            END                                  

-- For Affiliate Customers by quater--                                        
        IF @Filter = @AffiliateOrderByQuarter 
            BEGIN                         
                SET @Diff = ( SELECT    datepart(month, @TodayDate)
                            ) - 4 ;                                        
                SET @StartDate = ( SELECT   dateadd(month, -@Diff,
                                                    @TodayDate + 1)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                  

                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
            END                                              
                                  
-- For Affiliate Customers by year --                                        
        IF @Filter = @AffiliateOrderByYear 
            BEGIN                                    
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)
                            ) - ( SELECT    datepart(day, @TodayDate)
                                ) ;                                        
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)
                                     ) - 1 ;                                        
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)
                                 ) ;                                        
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)
                                 ) ;                                        
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)
                                        ) ;                                        
		
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,
                    @CompareFinalDate, @Custom1 
                
            END
                      
                      
	--For Tax Detail and Year--                      
        IF @Filter = @SalesTax 
            BEGIN           
                EXEC ZNode_ReportsTaxFiltered @FinalDate, @CompareFinalDate,
                    ''
            END                 
                      
--For Tax Detail and Month--                     
        IF @Filter = @SalesTaxByMonth 
            BEGIN       
                SELECT  @FinalDate = convert(varchar(10), '01/01/' + @Year, 101)
                SELECT  @CompareFinalDate = convert(varchar(10), '12/31/'
                        + @Year, 101)         
		
                EXEC ZNode_ReportsTaxFiltered @FinalDate, @CompareFinalDate,
                    'month'
            END                   
                         
--For Tax Detail and Quarter--                      
        IF @Filter = @SalesTaxByQuarter 
            BEGIN                    
                SELECT  @FinalDate = convert(varchar(10), '01/01/' + @Year, 101)
                SELECT  @CompareFinalDate = convert(varchar(10), '12/31/'
                        + @Year, 101)         
		
                EXEC ZNode_ReportsTaxFiltered @FinalDate, @CompareFinalDate,
                    'quarter'
            END                   
                
-- For Supplier --                                            
        IF @Filter = @SupplierList 
            BEGIN                       
							                      
				-- Custom1 : Store name.
                EXEC ZNode_ReportsSupplierList @FromDate, @ToDate, @Custom1,
                    @SupplierID
            END               
           
    END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchProductByNames]'
GO
ALTER PROCEDURE [dbo].[ZNode_SearchProductByNames]
(          
@NAME VARCHAR(MAX) = NULL,          
@PRODUCTNUM VARCHAR(MAX) = NULL,          
@SKU VARCHAR(MAX) = NULL,          
@MANUFACTURERNAME VARCHAR(MAX) = NULL,          
@PRODUCTTYPEID VARCHAR(MAX) = NULL,          
@CATEGORYNAME VARCHAR(MAX) = NULL,          
@portalIds VARCHAR(MAX) = '0'          
)AS          
BEGIN   
	SET NOCOUNT ON;
	
SELECT [ProductID]
      ,[Name]
      ,[ShortDescription]      
      ,[ProductNum]
      ,[ProductTypeID]
      ,[RetailPrice]
      ,[SalePrice]
      ,[WholesalePrice]
      ,[ImageFile]
      ,[ImageAltTag]
      ,[CallForPricing]
      ,[HomepageSpecial]
      ,[CategorySpecial]            
      ,[SKU]      
      ,[TaxClassID]
      ,[ActiveInd]
	FROM [ZNodeProduct]
	WHERE	
	(NAME LIKE '%' + @NAME + '%' OR LEN(@NAME) = 0) AND
	(PRODUCTNUM LIKE '%' + @PRODUCTNUM + '%' OR LEN(@PRODUCTNUM) = 0) AND
	(SKU LIKE '%' + @SKU + '%' OR LEN(@SKU) = 0) AND
	(ManufacturerID IN (SELECT MANUFACTURERID FROM ZNODEMANUFACTURER WHERE NAME LIKE @MANUFACTURERNAME + '%') OR (LEN(@MANUFACTURERNAME) = 0)) AND
	(ProductTypeID = @PRODUCTTYPEID OR LEN(@PRODUCTTYPEID) = 0 OR @PRODUCTTYPEID = '0') AND
	((ProductID IN (SELECT PRODUCTID FROM ZNODEPRODUCTCATEGORY WHERE CATEGORYID IN         
	(SELECT CATEGORYID FROM ZNODECATEGORY WHERE NAME LIKE '%' + @CATEGORYNAME +'%'))) OR LEN(@CATEGORYNAME) = 0) 
	ORDER BY Displayorder, Name
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_CopyCatalog]'
GO
  
ALTER PROCEDURE [dbo].[ZNode_CopyCatalog]   
 @CatalogID int,    
 @CatalogName nvarchar(max),    
 @LocaleID int = NULL     
AS    
BEGIN     
SET NOCOUNT ON;    
    
/* Insert a new Catalog Record */    
DECLARE @NewCatalogId int, @ErrCode int, @ErrMessage nvarchar(4000);    
  
INSERT INTO [ZNodeCatalog]      
           ([Name]    
           ,[IsActive])    
SELECT     
   @CatalogName,     
   IsActive    
FROM    
   ZNodeCatalog     
WHERE     
   CatalogId = @CatalogID;    
      
   SELECT @NewCatalogID = @@IDENTITY;     
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    


/* Get the LocaleCode */
DECLARE @LocaleCode VARCHAR(10) = NULL
SET @LocaleCode = (SELECT LocaleCode FROM ZNodeLocale WHERE LocaleID = @LocaleID)

       
/* Create copy of the PortalCatalog record, of existing catalog for new catalog */          
    
-- This block is commented because right now we are not using locale    
-- Comment Start    
/*     
INSERT INTO     
   [ZNodePortalCatalog]    
           ([PortalID]    
           ,[CatalogID]    
           ,[LocaleID]    
           ,[Theme]    
           ,[CSS])               
SELECT     
   PortalID,     
   @NewCatalogId,     
   @LocaleID,     
   Theme,     
   CSS    
FROM    
   ZNodePortalCatalog ZP     
WHERE    
   ZP.CatalogID = @CatalogID;        
       
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
   */    
--Comment End    
-- PRINT @NewCatalogId    
/* Table variable to record the existing productid and link to the newly copied product */        
DECLARE  @ProductIDTable TABLE(    
   [KeyField] [int] IDENTITY(1,1),    
   [OldProductID] [int],    
   [NewProductID] [int]);    
       
INSERT INTO     
   @ProductIDTable ([OldProductID])    
SELECT    
   ProductID    
FROM    
   ZNodeProduct     
WHERE    
   ProductID IN    
   (SELECT    
    ProductID     
    FROM    
    ZNodeProductCategory     
    WHERE    
    CategoryID IN    
    (SELECT    
     CategoryID     
     FROM     
     ZNodeCategoryNode     
    WHERE     
     CatalogID = @CatalogID));    
    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
    
/******************************************************************************    
 Iterate through the table variable created above, insert new copy product,     
 update table variable with new product id     
 *****************************************************************************/    
DECLARE  @i [int], @maxI [int], @newID [int]    
SET @i = 1;    
    
SELECT     
   @maxI = Max([KeyField])     
FROM     
   @ProductIDTable;    
    
WHILE  @i <= @maxI    
BEGIN    
   INSERT INTO [ZNodeProduct]    
           ([Name]    
           ,[ShortDescription]    
           ,[Description]    
           ,[FeaturesDesc]    
           ,[ProductNum]    
           ,[ProductTypeID]    
           ,[RetailPrice]    
           ,[SalePrice]    
           ,[WholesalePrice]    
           ,[ImageFile]    
           ,[ImageAltTag]    
           ,[Weight]    
           ,[Length]    
           ,[Width]    
           ,[Height]    
           ,[BeginActiveDate]    
           ,[EndActiveDate]    
           ,[DisplayOrder]    
           ,[ActiveInd]    
           ,[CallForPricing]    
           ,[HomepageSpecial]    
           ,[CategorySpecial]    
           ,[InventoryDisplay]    
           ,[Keywords]    
           ,[ManufacturerID]    
           ,[AdditionalInfoLink]    
           ,[AdditionalInfoLinkLabel]    
           ,[ShippingRuleTypeID]    
           ,[SEOTitle]    
           ,[SEOKeywords]    
           ,[SEODescription]    
           ,[Custom1]    
           ,[Custom2]    
           ,[Custom3]    
           ,[ShipEachItemSeparately]    
           ,[SKU]    
           ,[AllowBackOrder]    
           ,[BackOrderMsg]    
           ,[DropShipInd]    
           ,[DropShipEmailID]    
           ,[Specifications]    
           ,[AdditionalInformation]    
           ,[InStockMsg]    
           ,[OutOfStockMsg]    
           ,[TrackInventoryInd]    
           ,[DownloadLink]    
           ,[FreeShippingInd]    
           ,[NewProductInd]    
           ,[SEOURL]    
           ,[MaxQty]    
           ,[ShipSeparately]    
           ,[FeaturedInd]    
           ,[WebServiceDownloadDte]    
           ,[UpdateDte]    
           ,[SupplierID]    
           ,[RecurringBillingInd]    
           ,[RecurringBillingInstallmentInd]    
     ,[RecurringBillingPeriod]    
           ,[RecurringBillingFrequency]    
           ,[RecurringBillingTotalCycles]    
           ,[RecurringBillingInitialAmount]    
           ,[TaxClassID])    
SELECT     
   [Name],     
   [ShortDescription],    
   [Description],    
   [FeaturesDesc],    
   [ProductNum],    
   [ProductTypeID],    
   [RetailPrice],    
   [SalePrice],    
   [WholesalePrice],    
   [ImageFile],    
   [ImageAltTag],    
   [Weight],    
   [Length],    
   [Width],    
   [Height],    
   [BeginActiveDate],    
   [EndActiveDate],    
   [DisplayOrder],    
   [ActiveInd],    
   [CallForPricing],    
   [HomepageSpecial],     
   [CategorySpecial],    
   [InventoryDisplay],    
   [Keywords],    
   [ManufacturerID],     
   [AdditionalInfoLink],     
   [AdditionalInfoLinkLabel],    
   [ShippingRuleTypeID],     
   [SEOTitle],    
   [SEOKeywords],    
   [SEODescription],    
   [Custom1],    
   [Custom2],     
   [Custom3],     
   [ShipEachItemSeparately],    
   [SKU],    
   [AllowBackOrder],    
   [BackOrderMsg],     
   [DropShipInd],    
   [DropShipEmailID],     
   [Specifications],    
   [AdditionalInformation],    
   [InStockMsg],    
   [OutOfStockMsg],    
   [TrackInventoryInd],    
   [DownloadLink],     
   [FreeShippingInd],     
   [NewProductInd],     
   [SEOURL],     
   [MaxQty],    
   [ShipSeparately],     
   [FeaturedInd],     
   [WebServiceDownloadDte],     
   [UpdateDte],     
   [SupplierID],     
   [RecurringBillingInd],     
   [RecurringBillingInstallmentInd],     
   [RecurringBillingPeriod],    
   [RecurringBillingFrequency],    
   [RecurringBillingTotalCycles],     
   [RecurringBillingInitialAmount],     
   [TaxClassID]    
FROM    
   ZNodeProduct P    
INNER JOIN    
   @ProductIDTable PT    
ON    
   P.ProductID = PT.OldProductID    
WHERE    
   PT.KeyField = @i;    
    
   SELECT @newID = @@IDENTITY    
      
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
       
   UPDATE     
     @ProductIDTable    
   SET    
     NewProductID = @newID    
   WHERE    
     KeyField = @i;    
         
   SELECT @i = @i + 1;    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
END    
          
       
/* Create table variable to record existing categoryid, linked to new categoryid*/       
DECLARE  @CategoryIDTable TABLE (    
   [KeyField] [int] IDENTITY(1,1),    
   [OldCategoryID] [int],    
   [NewCategoryID] [int]);    
       
INSERT INTO @CategoryIDTable    
           ([OldCategoryID])    
SELECT    
   [CategoryId]    
FROM    
   ZNodeCategory    
WHERE    
   CategoryID IN    
   (SELECT    
    CategoryID     
   FROM    
    ZNodeCategoryNode     
   WHERE    
    CatalogID = @CatalogID);      
      
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
    
/******************************************************************************    
 Iterate through the table variable created above, insert new copy category,     
 update table variable with new category id     
 *****************************************************************************/     
DECLARE  @intCounter int, @int int, @new int;    
SET @int = 1;    
    
SELECT     
   @intCounter = Max([KeyField])     
FROM     
   @CategoryIDTable;    
    
WHILE  @int <= @intCounter     
BEGIN    
  INSERT INTO [ZNodeCategory]    
           ([Name]    
           ,[Title]    
           ,[ShortDescription]    
           ,[Description]    
           ,[ImageFile]    
           ,[ImageAltTag]    
           ,[VisibleInd]    
           ,[SubCategoryGridVisibleInd]    
   ,[SEOTitle]    
           ,[SEOKeywords]    
           ,[SEODescription]    
           ,[AlternateDescription]    
           ,[DisplayOrder]    
           ,[Custom1]    
           ,[Custom2]    
           ,[Custom3]    
           ,[SEOURL])    
SELECT    
   [Name],    
   [Title],    
   [ShortDescription],    
   [Description],    
   [ImageFile],    
   [ImageAltTag],    
   [VisibleInd],    
   [SubCategoryGridVisibleInd],     
   [SEOTitle],    
   [SEOKeywords],    
   [SEODescription],    
   [AlternateDescription],    
   [DisplayOrder],    
   [Custom1],     
   [Custom2],     
   [Custom3],     
   [SEOURL]    
FROM    
   ZNodeCategory C    
INNER JOIN    
   @CategoryIDTable CT    
ON    
   CT.OldCategoryID = C.CategoryID    
WHERE    
   KeyField = @int;    
       
   SELECT @new = @@IDENTITY;    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
   UPDATE    
     @CategoryIDTable    
   SET    
     NewCategoryID = @new    
   WHERE     
     KeyField = @int;    
     
   SELECT @int = @int + 1;    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
END    
    
/*Insert newly created product and categories */       
INSERT INTO [ZNodeProductCategory]    
   ([ProductID]     
   ,[Theme]     
   ,[MasterPage]     
   ,[CSS]     
   ,[CategoryID]     
   ,[BeginDate]    
   ,[EndDate]    
   ,[DisplayOrder]    
   ,[ActiveInd])    
SELECT     
   PT.NewProductID,     
   ZP.Theme,     
   ZP.MasterPage,     
   ZP.CSS,     
   CT.NewCategoryID,     
   ZP.BeginDate,     
   ZP.EndDate,     
   ZP.DisplayOrder,     
   ZP.ActiveInd    
FROM    
   ZNodeProductCategory ZP    
INNER JOIN    
   @CategoryIDTable CT     
ON     
   CT.OldCategoryID = ZP.CategoryID    
INNER JOIN    
   @ProductIDTable PT     
ON    
   PT.OldProductID = ZP.ProductID;    
    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
    
/* Create table variable to record existing categoryid, linked to new categoryid*/       
DECLARE  @CategoryNodeIDTable TABLE (    
   [KeyField] [int] IDENTITY(1,1),    
   [OldID] [int],    
   [ParentID] [int],    
   [NewID] [int]);    
    
INSERT INTO @CategoryNodeIDTable    
           ([OldID],    
            [ParentID])    
SELECT    
   [CategoryNodeId],    
   [ParentCategoryNodeID]    
FROM    
   [ZNodeCategoryNode]    
WHERE        
   CatalogID = @CatalogID;     
       
          
DECLARE  @CNCount int, @CNIndex int, @CNNew int;    
SET @CNIndex = 1;    
    
SELECT     
   @CNCount = Max([KeyField])     
FROM     
   @CategoryNodeIDTable;    
    
WHILE  @CNIndex <= @CNCount    
BEGIN    
 INSERT INTO [ZNodeCategoryNode]    
    ([CatalogID]    
    ,[CategoryID]    
    ,[ParentCategoryNodeID]        
    ,[BeginDate]    
    ,[EndDate]    
    ,[Theme]    
    ,[MasterPage]    
    ,[CSS]    
    ,[DisplayOrder]    
    ,[ActiveInd])    
 SELECT     
    @NewCatalogID,     
    NewCategoryID,    
    ParentCategoryNodeID,    
    BeginDate,     
    EndDate,     
    Theme,     
    MasterPage,     
    CSS,     
    CN.DisplayOrder,     
    ActiveInd    
 FROM    
    ZnodeCategoryNode CN    
 INNER JOIN    
    @CategoryIDTable CT    
 ON    
    CT.OldCategoryID = CN.CategoryID    
 WHERE        
    CategoryNodeID = (SELECT OldId FROM @CategoryNodeIDTable WHERE KeyField = @CNIndex); --Added this line to avoid the duplicate    
    
 IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END     
        
 SELECT @CNNew = @@IDENTITY;    
      
 UPDATE @CategoryNodeIDTable     
 SET  [NewID] = @CNNew     
 WHERE KeyField = @CNIndex;    
     
     
 SELECT @CNIndex = @CNIndex + 1;    
           
END             
            
-- Update ParentCategoryNodeId with New CategoryNodeId    
UPDATE dbo.ZNodeCategoryNode    
SET  ParentCategoryNodeID = D.[NewID]    
FROM @CategoryNodeIDTable D     
WHERE ZNodeCategoryNode.ParentCategoryNodeID = D.OldID    
  AND ZNodeCategoryNode.[CatalogID] = @NewCatalogID;    
       
IF @@ERROR != 0     
BEGIN    
 SET @ErrMessage = ERROR_MESSAGE()    
 RETURN @ErrMessage    
END    
        
/* Table variable to record existing taggroupid's linked to new taggroupids */    
DECLARE  @TagGroupTable Table(    
   KeyField int IDENTITY(1,1),     
   OldTagGroupID int,     
   NewTagGroupID int);    
    
INSERT INTO    
   @TagGroupTable     
   (OldTagGroupID)    
SELECT     
   TG.TagGroupID    
FROM     
   ZNodeTagGroup TG    
WHERE    
   TG.TagGroupID IN    
   (SELECT     
     TagGroupID     
   FROM    
     ZNodeTagGroup TG    
   WHERE    
     TG.CatalogID = @CatalogID);    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
/******************************************************************************    
 Iterate through the table variable created above, insert new copy tag groups,     
 update table variable with new taggroup id     
 *****************************************************************************/    
DECLARE  @intMaxID int, @NewIdentity int, @intCount int;    
SET @intCount = 1;    
    
SELECT     
   @intMaxID = MAX(KeyField)     
FROM    
   @TagGroupTable;    
    
WHILE  @intCount <= @intMaxID    
BEGIN    
    
 INSERT INTO ZNodeTagGroup     
    (TagGroupLabel,     
    CatalogID,    
    ControlTypeID)    
 SELECT     
    TagGroupLabel,     
    @NewCatalogID,    
    ControlTypeID    
 FROM     
    ZNodeTagGroup T    
 INNER JOIN    
    @TagGroupTable TT    
 ON    
    TT.OldTagGroupID = T.TagGroupID    
 WHERE    
    KeyField = @intCount;    
        
 SELECT  @newIdentity = @@IDENTITY;    
     
 IF @@ERROR != 0     
 BEGIN    
   SET @ErrMessage = ERROR_MESSAGE()    
   RETURN @ErrMessage    
 END    
       
 UPDATE    
    @TagGroupTable     
 SET     
    NewTagGroupID = @newIdentity    
 WHERE     
    KeyField = @intCount;    
        
 IF @@ERROR != 0     
  BEGIN    
   SET @ErrMessage = ERROR_MESSAGE()    
   RETURN @ErrMessage    
  END    
        
     
         
 SELECT @intCount = @intCount + 1;    
     
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
END    
    
    
-- Update Association    
INSERT INTO [ZNodeTagGroupCategory]    
     ([TagGroupID]    
     ,[CategoryID]    
     ,[CategoryDisplayOrder])    
 SELECT      
    T.NewTagGroupID,     
    C.NewCategoryID,     
    TG.CategoryDisplayOrder    
 FROM     
    ZNodeTagGroupCategory TG    
 INNER JOIN    
    @TagGroupTable T     
 ON    
    T.OldTagGroupID = TG.TagGroupID    
 INNER JOIN    
    @CategoryIDTable C    
 ON    
    C.OldCategoryID = TG.CategoryID    
    
    
/* Table variable to record existing tag ids linked to new tag id's */    
DECLARE  @TagTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewTagID] int,     
   [OldTagID] int);    
    
    
INSERT INTO @TagTable     
   (OldTagID)    
SELECT      
   T.TagID    
FROM    
   ZnodeTag T    
WHERE    
   T.TagGroupID IN    
   (SELECT     
     TagGroupID    
   FROM    
     @TagGroupTable TG    
   INNER JOIN    
     ZNodeTag T    
   ON    
     TG.OldTagGroupID = T.TagGroupID);    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
/******************************************************************************    
 Iterate through the table variable created above, insert new copy tags,     
 update table variable with new tag id     
 *****************************************************************************/    
DECLARE @intMID int, @nId int, @iCount int;    
SET @iCount = 1;    
    
SELECT @intMID = MAX(KeyField) From @TagTable;    
    
WHILE @iCount <= @intMID    
BEGIN    
  INSERT INTO ZNodeTag     
     (TagGroupId    
     ,TagName    
     ,TagDisplayOrder    
     ,IconPath)    
  SELECT     
     TG.NewTagGroupID,     
     T.TagName,     
     T.TagDisplayOrder,     
     IconPath    
  FROM     
     ZNodeTag T    
  INNER JOIN    
     @TagTable TT    
  ON    
   TT.OldTagID = T.TagId    
  INNER JOIN    
     @TagGroupTable TG    
  ON    
     T.TagGroupID = TG.OldTagGroupID    
  WHERE    
     TT.KeyField = @iCount;    
    
  SELECT  @nId = @@IDENTITY;    
      
  IF @@ERROR != 0     
  BEGIN    
   SET @ErrMessage = ERROR_MESSAGE()    
   RETURN @ErrMessage    
  END    
     
  UPDATE      
     @TagTable     
  SET       
     NewTagID = @nId    
  WHERE     
     KeyField = @iCount;    
    
  SELECT  @iCount = @iCount + 1;    
      
  IF @@ERROR != 0     
  BEGIN    
   SET @ErrMessage = ERROR_MESSAGE()    
   RETURN @ErrMessage    
  END    
        
END    
    
/* Insert new tag and product records in the znodetagproductsku table */       
INSERT INTO [ZNodeTagProductSKU]    
   ([TagID]    
   ,[ProductID])    
SELECT     
   T.NewTagID,     
   P.NewProductID    
FROM    
   @TagTable T    
INNER JOIN    
   ZNodeTagProductSKU TPS    
ON    
   TPS.TagID = T.OldTagID    
INNER JOIN    
   @ProductIDTable P    
ON    
   P.OldProductID = TPS.ProductID;    
    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
     
         
    
/* Variables for Iterations */    
DECLARE @recCount INT    
DECLARE @recIndex INT    
DECLARE @recID INT    
    
------------------------------    
-- Copy ZNodeHighlight records    
------------------------------    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @HighlightTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
INSERT INTO @HighlightTable     
   (OldID)    
SELECT      
   HighLightID    
FROM    
   ZNodeHighlight     
WHERE    
   HighLightID IN    
   (SELECT     
     PH.HighLightId    
   FROM    
     @ProductIDTable PT    
   INNER JOIN    
     ZNodeProductHighlight PH    
   ON    
     PT.OldProductID = PH.ProductID);          
    
-- Loop through each record and insert new highlight record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @HighlightTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    
     
 INSERT INTO ZNodeHighlight     
	  ([ImageFile]    
      ,[ImageAltTag]    
      ,[Name]    
      ,[Description]    
      ,[DisplayPopup]    
      ,[Hyperlink]    
      ,[HyperlinkNewWinInd]    
      ,[HighlightTypeID]    
      ,[ActiveInd]    
      ,[DisplayOrder]    
      ,[ShortDescription]    
      ,[LocaleId])    
 SELECT     
       [ImageFile]    
      ,[ImageAltTag]    
      ,[Name]    
      ,[Description]    
      ,[DisplayPopup]    
      ,[Hyperlink]    
      ,[HyperlinkNewWinInd]    
      ,[HighlightTypeID]    
      ,[ActiveInd]    
      ,[DisplayOrder]    
      ,[ShortDescription]    
      ,COALESCE(@LocaleID, [LocaleId])         
 FROM ZNodeHighlight    
 WHERE HighlightID = (SELECT OldID FROM @HighlightTable WHERE KeyField=@recIndex);    
     
 UPDATE @HighlightTable    
 SET NewID = @@IDENTITY    
 WHERE KeyField = @recIndex;     
     
 SET @recIndex = @recIndex + 1;    
     
END    
    
-- Insert new highlight association for new products.    
INSERT INTO ZNodeProductHighlight     
    ([ProductID]    
    ,[HighlightID]    
    ,[DisplayOrder])    
      SELECT    
     A.NewProductID    
    ,C.NewID    
    ,B.DisplayOrder     
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductHighlight B ON A.OldProductID = B.ProductID    
   INNER JOIN @HighlightTable C ON C.OldID = B.HighlightID;    
    
    
---------------------------    
-- Copy ZNodeAddOns records    
---------------------------    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @AddOnTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
INSERT INTO @AddOnTable     
   (OldID)    
SELECT      
   AddOnId    
FROM    
   ZNodeAddOn     
WHERE    
   AddOnId IN    
   (SELECT     
     B.AddOnId    
   FROM    
     @ProductIDTable A    
   INNER JOIN    
     ZNodeProductAddOn B    
   ON    
     A.OldProductID = B.ProductID);    
         
-- Loop through each record and insert new highlight record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @AddOnTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    
     
 -- Duplicate the ZNodeAddOn records.    
 INSERT INTO ZNodeAddOn     
   ([ProductID]    
      ,[Title]    
      ,[Name]    
      ,[Description]    
      ,[DisplayOrder]    
      ,[DisplayType]    
      ,[OptionalInd]    
      ,[AllowBackOrder]    
      ,[InStockMsg]    
      ,[OutOfStockMsg]    
      ,[BackOrderMsg]    
      ,[PromptMsg]    
      ,[TrackInventoryInd]    
      ,[LocaleId])    
 SELECT     
       [ProductID]    
      ,[Title]    
      ,[Name]    
      ,[Description]    
      ,[DisplayOrder]    
      ,[DisplayType]    
      ,[OptionalInd]    
      ,[AllowBackOrder]    
      ,[InStockMsg]    
      ,[OutOfStockMsg]    
      ,[BackOrderMsg]    
      ,[PromptMsg]    
      ,[TrackInventoryInd]    
      ,COALESCE(@LocaleID, [LocaleId])    
 FROM ZNodeAddOn    
 WHERE AddOnID = (SELECT OldID FROM @AddOnTable WHERE KeyField=@recIndex);    
     
 SET @recID = @@IDENTITY;    
        
 UPDATE @AddOnTable    
 SET NewID = @recID    
 WHERE KeyField = @recIndex;      
     
 -- Duplicate the ZNodeAddOnValue records based on new AddOn record.    
 INSERT INTO ZNodeAddOnValue    
    ([AddOnID]    
    ,[Name]    
    ,[Description]    
    ,[SKU]    
    ,[DefaultInd]    
    ,[DisplayOrder]    
    ,[ImageFile]    
    ,[ImageAltTag]    
    ,[RetailPrice]    
    ,[SalePrice]    
    ,[WholesalePrice]    
    ,[RecurringBillingInd]    
    ,[RecurringBillingInstallmentInd]    
    ,[RecurringBillingPeriod]    
    ,[RecurringBillingFrequency]    
    ,[RecurringBillingTotalCycles]    
    ,[RecurringBillingInitialAmount]    
    ,[Weight]    
    ,[Length]    
    ,[Height]    
    ,[Width]    
    ,[ShippingRuleTypeID]    
    ,[FreeShippingInd]    
    ,[WebServiceDownloadDte]    
    ,[UpdateDte]    
    ,[SupplierID]    
    ,[TaxClassID])    
 SELECT      
     @recID    
    ,[Name]    
    ,[Description]    
    ,[SKU]    
    ,[DefaultInd]    
    ,[DisplayOrder]    
    ,[ImageFile]    
    ,[ImageAltTag]    
    ,[RetailPrice]    
    ,[SalePrice]    
    ,[WholesalePrice]    
    ,[RecurringBillingInd]    
    ,[RecurringBillingInstallmentInd]    
    ,[RecurringBillingPeriod]    
    ,[RecurringBillingFrequency]    
    ,[RecurringBillingTotalCycles]    
    ,[RecurringBillingInitialAmount]    
    ,[Weight]    
    ,[Length]    
    ,[Height]    
    ,[Width]    
    ,[ShippingRuleTypeID]    
    ,[FreeShippingInd]    
    ,[WebServiceDownloadDte]    
    ,[UpdateDte]    
    ,[SupplierID]    
    ,[TaxClassID]    
 FROM ZNodeAddOnValue     
 WHERE AddOnID = (SELECT OldID FROM @AddOnTable WHERE KeyField=@recIndex);    
     
 SET @recIndex = @recIndex + 1;    
       
END    
    
-- Insert new AddOn association for new products.    
INSERT INTO ZNodeProductAddOn    
    ([ProductID]    
    ,AddOnID)    
      SELECT    
     A.NewProductID    
    ,C.NewID    
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductAddOn B ON A.OldProductID = B.ProductID    
   INNER JOIN @AddOnTable C ON C.OldID = B.AddOnID;       
            
              
-----------------------------------    
-- Copy ZNodeProductProfile records    
-----------------------------------    
-- Insert new Profile association for new products.    
INSERT INTO ZNodeProductProfile    
    ([ProductID]    
    ,ProfileID    
    ,IncludeInd)    
      SELECT    
    DISTINCT    
     A.NewProductID    
    ,B.ProfileID    
    ,B.IncludeInd    
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductProfile B ON A.OldProductID = B.ProductID    
         
         
--------------------------------    
-- Copy ZNodeProductTier records    
--------------------------------    
-- Insert new Tier association for new products.    
INSERT INTO ZNodeProductTier    
    ([ProductID]    
    ,[ProfileID]    
    ,[TierStart]    
    ,[TierEnd]    
    ,[Price])    
      SELECT    
      DISTINCT    
     A.NewProductID    
    ,B.ProfileID    
    ,B.TierStart    
    ,B.TierEnd    
    ,B.Price    
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductTier B ON A.OldProductID = B.ProductID    
       
       
-------------------------------------    
-- Copy ZNodeProductCrossSell records    
-------------------------------------    
-- Insert new CrossSell association for new products.    
INSERT INTO ZNodeProductCrossSell    
    ([ProductID]    
    ,[RelatedProductId]    
    ,[DisplayOrder])    
      SELECT    
      DISTINCT    
     A.NewProductID    
    ,C.NewProductID    
    ,B.DisplayOrder    
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductCrossSell B ON A.OldProductID = B.ProductID    
   INNER JOIN @ProductIDTable C ON B.RelatedProductId = C.OldProductID  
   WHERE A.NewProductID IS NOT NULL  
   AND C.NewProductID IS NOT NULL;  
       
       
---------------------------------    
-- Copy ZNodeProductImage records    
---------------------------------    
-- Insert new Image association for new products.    
INSERT INTO ZNodeProductImage    
    ([ProductID]    
    ,[Name]    
    ,[ImageFile]    
    ,[ImageAltTag]    
    ,[AlternateThumbnailImageFile]    
    ,[ActiveInd]    
    ,[ShowOnCategoryPage]    
    ,[ProductImageTypeID]    
    ,[DisplayOrder])    
      SELECT    
      DISTINCT    
     A.NewProductID        
    ,B.[Name]    
    ,B.[ImageFile]    
    ,B.[ImageAltTag]    
    ,B.[AlternateThumbnailImageFile]    
    ,B.[ActiveInd]    
    ,B.[ShowOnCategoryPage]    
    ,B.[ProductImageTypeID]    
    ,B.[DisplayOrder]    
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductImage B ON A.OldProductID = B.ProductID    
   WHERE A.NewProductID IS NOT NULL;     
               
--------------------------------------------------------------------    
-- Copy ZNodeProductAttribute / ZNodeSKU / ZNodeSKUAttribute records    
--------------------------------------------------------------------    
    
-- Duplicate ZNodeAttributeType    
    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @AttributeTypeTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
    
INSERT INTO @AttributeTypeTable    
  (OldID)    
 SELECT    
  DISTINCT A.AttributeTypeId    
 FROM ZNodeAttributeType A    
 INNER JOIN ZNodeProductTypeAttribute B ON A.AttributeTypeId = B.AttributeTypeId    
 INNER JOIN ZNodeProductType C ON C.ProductTypeId = B.ProductTypeId    
 INNER JOIN ZNodeProduct D ON D.ProductTypeID = C.ProductTypeId    
 INNER JOIN @ProductIDTable E ON D.ProductID = E.OldProductID;    
    
-- Loop through each record and insert new record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @AttributeTypeTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    
    
 INSERT INTO ZNodeAttributeType     
   ([Name]    
   ,[Description]    
   ,[DisplayOrder]    
   ,[IsPrivate]    
   ,[LocaleId])    
 SELECT      
    [Name]    
      ,[Description]    
      ,[DisplayOrder]    
      ,[IsPrivate]    
      ,COALESCE(@LocaleID, [LocaleId])     
    FROM ZNodeAttributeType A    
    INNER JOIN @AttributeTypeTable B ON A.AttributeTypeId = B.OldID    
    WHERE B.KeyField = @recIndex;    
    
 SET @recID = @@IDENTITY;    
    
 UPDATE @AttributeTypeTable     
 SET NewID = @recID    
 WHERE KeyField = @recIndex;    
     
 SET @recIndex = @recIndex + 1;    
     
END    
    
-- Duplicate ZNodeProductAttribute    
    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @ProductAttributeTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
    
INSERT INTO @ProductAttributeTable    
  (OldID)    
 SELECT    
  DISTINCT A.AttributeId    
 FROM ZNodeProductAttribute A    
 INNER JOIN ZNodeSKUAttribute B ON A.AttributeId = B.AttributeId    
 INNER JOIN ZNodeSKU C ON C.SKUID = B.SKUID    
 INNER JOIN @ProductIDTable E ON C.ProductID = E.OldProductID;    
    
-- Loop through each record and insert new record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @ProductAttributeTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    
    
 INSERT INTO ZNodeProductAttribute     
	  ([AttributeTypeId]    
      ,[Name]    
      ,[ExternalId]    
      ,[DisplayOrder]    
      ,[IsActive]    
      ,[OldAttributeId])    
 SELECT      
       C.NewID    
      ,[Name]    
      ,[ExternalId]    
      ,[DisplayOrder]    
      ,[IsActive]    
      ,[OldAttributeId]    
    FROM ZNodeProductAttribute A        
    INNER JOIN @ProductAttributeTable B ON A.AttributeId = B.OldID    
    INNER JOIN @AttributeTypeTable C ON A.AttributeTypeId = C.OldID    
    WHERE B.KeyField = @recIndex AND C.NewID IS NOT NULL;  
    
    
 SET @recID = @@IDENTITY;    
    
 UPDATE @ProductAttributeTable     
 SET NewID = @recID    
 WHERE KeyField = @recIndex;    
     
 SET @recIndex = @recIndex + 1;    
     
END    
    
        
-- Duplicate ZNodeSKU    
    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @SkuTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
    
INSERT INTO @SkuTable    
  (OldID)    
 SELECT    
  DISTINCT A.SKUID    
 FROM ZNodeSKU A     
 INNER JOIN @ProductIDTable E ON A.ProductID = E.OldProductID;    
    
-- Loop through each record and insert new record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @SkuTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    
    
 INSERT INTO ZNodeSKU     
   ([ProductID]    
      ,[SKU]    
      ,[SupplierID]    
      ,[Note]    
      ,[WeightAdditional]    
      ,[SKUPicturePath]    
      ,[ImageAltTag]    
      ,[DisplayOrder]    
      ,[RetailPriceOverride]    
      ,[SalePriceOverride]    
      ,[WholesalePriceOverride]    
      ,[RecurringBillingPeriod]    
      ,[RecurringBillingFrequency]    
      ,[RecurringBillingTotalCycles]    
      ,[RecurringBillingInitialAmount]    
      ,[ActiveInd]    
      ,[Custom1]    
      ,[Custom2]    
      ,[Custom3]    
      ,[WebServiceDownloadDte]    
      ,[UpdateDte])    
 SELECT      
    C.NewProductID    
      ,[SKU]    
      ,[SupplierID]    
      ,[Note]    
      ,[WeightAdditional]    
      ,[SKUPicturePath]    
      ,[ImageAltTag]    
      ,[DisplayOrder]    
      ,[RetailPriceOverride]    
      ,[SalePriceOverride]    
      ,[WholesalePriceOverride]    
      ,[RecurringBillingPeriod]    
      ,[RecurringBillingFrequency]    
      ,[RecurringBillingTotalCycles]    
      ,[RecurringBillingInitialAmount]    
      ,[ActiveInd]    
      ,[Custom1]    
      ,[Custom2]    
      ,[Custom3]    
      ,[WebServiceDownloadDte]    
      ,[UpdateDte]    
    FROM ZNodeSKU A        
    INNER JOIN @SkuTable B ON A.SKUID = B.OldID    
    INNER JOIN @ProductIDTable C ON A.ProductID = C.OldProductID    
    WHERE B.KeyField = @recIndex          
    AND C.NewProductID IS NOT NULL;  
    
 SET @recID = @@IDENTITY;    
    
 UPDATE @SkuTable     
 SET NewID = @recID    
 WHERE KeyField = @recIndex;    
     
 SET @recIndex = @recIndex + 1;    
     
END            
    
-- Duplicate ZNodeSKUAttribute    
    
INSERT INTO ZNodeSKUAttribute     
 ([SKUID]    
 ,[AttributeId])    
SELECT    
 DISTINCT    
  B.NewID    
 ,C.NewID    
FROM ZNodeSKUAttribute A    
INNER JOIN @SkuTable B ON A.SKUID = B.OldID    
INNER JOIN @ProductAttributeTable C ON A.AttributeId = C.OldID               
WHERE C.NEWID IS NOT NULL AND B.NewID IS NOT NULL;    
    
-- Duplicate ZNodeProductType    
    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @ProductTypeTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
    
INSERT INTO @ProductTypeTable    
  (OldID)    
 SELECT    
  DISTINCT A.ProductTypeId    
 FROM ZNodeProductType A    
 INNER JOIN ZNodeProduct C ON C.ProductTypeID = A.ProductTypeID    
 INNER JOIN @ProductIDTable E ON C.ProductID = E.OldProductID;    
    
-- Loop through each record and insert new record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @ProductTypeTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    
    
 INSERT INTO ZNodeProductType    
      ([Name]     
      ,[Description]    
      ,[DisplayOrder])    
 SELECT      
       [Name] + ISNULL(' - ' + @LocaleCode, '')
      ,[Description]    
      ,[DisplayOrder]    
    FROM ZNodeProductType A        
    INNER JOIN @ProductTypeTable B ON A.ProductTypeId = B.OldID    
    WHERE B.KeyField = @recIndex;    
    
 SET @recID = @@IDENTITY;    
    
 UPDATE @ProductTypeTable     
 SET NewID = @recID    
 WHERE KeyField = @recIndex;    
     
 SET @recIndex = @recIndex + 1;    
     
END    
    
-- Duplicate ZNodeProductTypeAttribute    
INSERT INTO ZNodeProductTypeAttribute     
 (ProductTypeId    
 ,[AttributeTypeId])    
SELECT    
  B.NewID    
 ,C.NewID    
FROM ZNodeProductTypeAttribute A    
INNER JOIN @ProductTypeTable B ON A.ProductTypeId = B.OldID    
INNER JOIN @AttributeTypeTable C ON A.AttributeTypeId = C.OldID              
WHERE C.NEWID IS NOT NULL AND B.NewID IS NOT NULL;    
     
-- Update ZNodeProduct table with new ProductTypeId for new Products    
    
UPDATE ZNodeProduct    
SET ProductTypeID = B.NewID    
FROM @ProductTypeTable B    
INNER JOIN ZNodeProduct C ON C.ProductTypeID = B.OldID    
INNER JOIN @ProductIDTable D ON C.ProductID = D.NewProductID    
WHERE ProductID = C.ProductID AND B.NewID IS NOT NULL;    
    
END  -- end tag for BEGIN (root)    
               
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCategoryNode_GetByCatalogID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeCategoryNode table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCategoryNode_GetByCatalogID
(

	@CatalogID int   
)
AS


				SELECT
					[CategoryNodeID],
					[CatalogID],
					[CategoryID],
					[ParentCategoryNodeID],
					[BeginDate],
					[EndDate],
					[Theme],
					[MasterPage],
					[CSS],
					[DisplayOrder],
					[ActiveInd]
				FROM
					[dbo].[ZNodeCategoryNode]
				WHERE
					[CatalogID] = @CatalogID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeProduct table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_Insert
(

	@ProductID int    OUTPUT,

	@Name nvarchar (MAX)  ,

	@ShortDescription nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@FeaturesDesc nvarchar (MAX)  ,

	@ProductNum nvarchar (100)  ,

	@ProductTypeID int   ,

	@RetailPrice money   ,

	@SalePrice money   ,

	@WholesalePrice money   ,

	@ImageFile nvarchar (MAX)  ,

	@ImageAltTag nvarchar (MAX)  ,

	@Weight decimal (18, 2)  ,

	@Length decimal (18, 2)  ,

	@Width decimal (18, 2)  ,

	@Height decimal (18, 2)  ,

	@BeginActiveDate datetime   ,

	@EndActiveDate datetime   ,

	@DisplayOrder int   ,

	@ActiveInd bit   ,

	@CallForPricing bit   ,

	@HomepageSpecial bit   ,

	@CategorySpecial bit   ,

	@InventoryDisplay tinyint   ,

	@Keywords nvarchar (MAX)  ,

	@ManufacturerID int   ,

	@AdditionalInfoLink nvarchar (MAX)  ,

	@AdditionalInfoLinkLabel nvarchar (MAX)  ,

	@ShippingRuleTypeID int   ,

	@SEOTitle nvarchar (MAX)  ,

	@SEOKeywords nvarchar (MAX)  ,

	@SEODescription nvarchar (MAX)  ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@ShipEachItemSeparately bit   ,

	@SKU nvarchar (100)  ,

	@AllowBackOrder bit   ,

	@BackOrderMsg nvarchar (MAX)  ,

	@DropShipInd bit   ,

	@DropShipEmailID nvarchar (MAX)  ,

	@Specifications nvarchar (MAX)  ,

	@AdditionalInformation nvarchar (MAX)  ,

	@InStockMsg nvarchar (MAX)  ,

	@OutOfStockMsg nvarchar (MAX)  ,

	@TrackInventoryInd bit   ,

	@DownloadLink nvarchar (MAX)  ,

	@FreeShippingInd bit   ,

	@NewProductInd bit   ,

	@SEOURL nvarchar (100)  ,

	@MaxQty int   ,

	@ShipSeparately bit   ,

	@FeaturedInd bit   ,

	@WebServiceDownloadDte datetime   ,

	@UpdateDte datetime   ,

	@SupplierID int   ,

	@RecurringBillingInd bit   ,

	@RecurringBillingInstallmentInd bit   ,

	@RecurringBillingPeriod nvarchar (50)  ,

	@RecurringBillingFrequency nvarchar (MAX)  ,

	@RecurringBillingTotalCycles int   ,

	@RecurringBillingInitialAmount money   ,

	@TaxClassID int   
)
AS


				
				INSERT INTO [dbo].[ZNodeProduct]
					(
					[Name]
					,[ShortDescription]
					,[Description]
					,[FeaturesDesc]
					,[ProductNum]
					,[ProductTypeID]
					,[RetailPrice]
					,[SalePrice]
					,[WholesalePrice]
					,[ImageFile]
					,[ImageAltTag]
					,[Weight]
					,[Length]
					,[Width]
					,[Height]
					,[BeginActiveDate]
					,[EndActiveDate]
					,[DisplayOrder]
					,[ActiveInd]
					,[CallForPricing]
					,[HomepageSpecial]
					,[CategorySpecial]
					,[InventoryDisplay]
					,[Keywords]
					,[ManufacturerID]
					,[AdditionalInfoLink]
					,[AdditionalInfoLinkLabel]
					,[ShippingRuleTypeID]
					,[SEOTitle]
					,[SEOKeywords]
					,[SEODescription]
					,[Custom1]
					,[Custom2]
					,[Custom3]
					,[ShipEachItemSeparately]
					,[SKU]
					,[AllowBackOrder]
					,[BackOrderMsg]
					,[DropShipInd]
					,[DropShipEmailID]
					,[Specifications]
					,[AdditionalInformation]
					,[InStockMsg]
					,[OutOfStockMsg]
					,[TrackInventoryInd]
					,[DownloadLink]
					,[FreeShippingInd]
					,[NewProductInd]
					,[SEOURL]
					,[MaxQty]
					,[ShipSeparately]
					,[FeaturedInd]
					,[WebServiceDownloadDte]
					,[UpdateDte]
					,[SupplierID]
					,[RecurringBillingInd]
					,[RecurringBillingInstallmentInd]
					,[RecurringBillingPeriod]
					,[RecurringBillingFrequency]
					,[RecurringBillingTotalCycles]
					,[RecurringBillingInitialAmount]
					,[TaxClassID]
					)
				VALUES
					(
					@Name
					,@ShortDescription
					,@Description
					,@FeaturesDesc
					,@ProductNum
					,@ProductTypeID
					,@RetailPrice
					,@SalePrice
					,@WholesalePrice
					,@ImageFile
					,@ImageAltTag
					,@Weight
					,@Length
					,@Width
					,@Height
					,@BeginActiveDate
					,@EndActiveDate
					,@DisplayOrder
					,@ActiveInd
					,@CallForPricing
					,@HomepageSpecial
					,@CategorySpecial
					,@InventoryDisplay
					,@Keywords
					,@ManufacturerID
					,@AdditionalInfoLink
					,@AdditionalInfoLinkLabel
					,@ShippingRuleTypeID
					,@SEOTitle
					,@SEOKeywords
					,@SEODescription
					,@Custom1
					,@Custom2
					,@Custom3
					,@ShipEachItemSeparately
					,@SKU
					,@AllowBackOrder
					,@BackOrderMsg
					,@DropShipInd
					,@DropShipEmailID
					,@Specifications
					,@AdditionalInformation
					,@InStockMsg
					,@OutOfStockMsg
					,@TrackInventoryInd
					,@DownloadLink
					,@FreeShippingInd
					,@NewProductInd
					,@SEOURL
					,@MaxQty
					,@ShipSeparately
					,@FeaturedInd
					,@WebServiceDownloadDte
					,@UpdateDte
					,@SupplierID
					,@RecurringBillingInd
					,@RecurringBillingInstallmentInd
					,@RecurringBillingPeriod
					,@RecurringBillingFrequency
					,@RecurringBillingTotalCycles
					,@RecurringBillingInitialAmount
					,@TaxClassID
					)
				
				-- Get the identity value
				SET @ProductID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetTagsByCategory]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ZNode_GetTagsByCategory]  
(  
 @CategoryID INT 
)    

AS
BEGIN    

	SET NOCOUNT ON;
	     
	SELECT  TG.TagGroupID,  
                        TagGroupLabel,  
                        ControlTypeID,  
                        DisplayOrder  
                INTO    #ZNodeTagGroup  
                FROM    ZNodeTagGroup TG  
                        INNER JOIN ZNodeTagGroupCategory TGC ON TG.TagGroupID = TGC.TagGroupID  
                WHERE   TGC.CategoryID = @CategoryID  
                ORDER BY DisplayOrder ;  
  
                ( SELECT  
					DISTINCT A.ProductID  
                  INTO      #CategoryProduct  
                  FROM      ZNodeProductCategory A  
                  INNER JOIN ProductsView B ON A.ProductID = B.ProductID  
                  WHERE     A.ActiveInd = 1  
                            AND A.CategoryId = @CategoryID
                ) ;  
  
                SELECT  TagGroupID,  
                        TagGroupLabel,  
                        ControlTypeID,  
                        DisplayOrder  
                FROM    #ZNodeTagGroup  
  
                SELECT  Tag.TagID,  
                        Tag.TagName,  
                        Tag.TagGroupID,  
                        Tag.IconPath,  
                        Tag.TagDisplayOrder,  
                        COUNT(TP.ProductID) AS TagProductCount  
                FROM    ZNodeTag Tag  
                        INNER JOIN #ZNodeTagGroup TG ON Tag.TagGroupID = TG.TagGroupID  
                        INNER JOIN TagProductView TP ON Tag.TagID = TP.TagID
				WHERE	TP.ProductId IN (SELECT ProductID FROM #CategoryProduct) 
                GROUP BY	Tag.TagID,  
							Tag.TagName,  
							Tag.TagGroupID,  
							Tag.IconPath,  
							Tag.TagDisplayOrder
				ORDER By Tag.TagDisplayOrder
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetTagsByCategoryIdTagIds]'
GO

CREATE PROCEDURE [dbo].[ZNode_GetTagsByCategoryIdTagIds]  
(  
 @CategoryID INT,  
 @TagIds varchar(max)  
)    

AS    
  
BEGIN    


CREATE TABLE #ZnodeProducts (ProductID INT)

INSERT INTO #ZnodeProducts
SELECT 
		    A.ProductID AS 'ProductId'	
FROM 
			ZNodeProductCategory A
INNER JOIN
			ProductsView B
ON
			B.ProductID = A.ProductID
WHERE 
			A.CategoryId = @CategoryId
AND
			A.ActiveInd = 1
AND
			B.ActiveInd = 1

CREATE TABLE #ZnodeTagProduct (TagId INT, ProductId INT)
					
INSERT INTO 
		#ZnodeTagProduct
SELECT 
		TPS.TagId, 
		TPS.ProductId
FROM 
		TagProductView TPS
INNER JOIN
		#ZNodeProducts P
ON
		P.ProductId = TPS.ProductId

CREATE TABLE #ZNodeTagSplit (TagId INT)

INSERT INTO 
		#ZNodeTagSplit
SELECT
		[Value]	AS 'TagId'
FROM 
		SC_SPLITTER(@TagIds,',') 
 
CREATE TABLE #ZNodeTag (TagID INT
						,TagGroupId INT 
						,TagName  NVARCHAR(MAX)
						,IconPath VARCHAR(512) 
						,TagCount INT
						,DisplayOrder INT)
 
INSERT INTO #ZNodeTag (TagID, TagGroupId, TagName, IconPath, TagCount, DisplayOrder)
	SELECT 
		TagID
		,TagGroupId
		,TagName
		,IconPath		
		,ISNULL((SELECT COUNT(TP.TagId)
					FROM
						#ZnodeTagProduct TP
					WHERE
						TP.TagId = ZNodeTag.TagID
					AND
						ProductId IN 
									(SELECT 
										ProductID
				 					FROM 
										#ZnodeTagProduct TP 
									WHERE
										TP.TagId IN 
											(SELECT
												TagId 
											FROM 
												#ZNodeTagSplit)
									GROUP BY 
										ProductId
									HAVING COUNT(ProductId) = (SELECT
																COUNT(TagID) 
															   FROM 
																#ZNodeTagSplit))
					AND
						TP.TagId
					NOT IN
						(SELECT TagId FROM #ZNodeTagSplit)
					GROUP BY
						TP.TagID), 0)
				,TagDisplayOrder
    FROM 
		ZNodeTag		
	WHERE
		ZNodeTag.TagID 
					IN 
					(SELECT 
							TagID
					FROM
						#ZnodeTagProduct	
					WHERE
						ProductId 
					IN
						(SELECT 
							ProductId
						FROM
							#ZnodeTagProduct 
						WHERE 
							TagId
						IN
							(SELECT 
								TagId
							FROM
								#ZNodeTagSplit)))
	AND
		ZNodeTag.TagGroupID
	NOT IN
		(SELECT TagGroupID FROM ZNodeTag INNER JOIN #ZnodeTagSplit TS ON TS.TagId = ZNodeTag.TagID)

	
	SELECT
		ZnodeTagGroup.TagGroupId
		,TagGroupLabel
		,ControlTypeId
		,0 AS 'TagGroupProductCount'
		,ZnodeTagGroup.DisplayOrder
		INTO #ZNodeTagGroup
	FROM
		ZnodeTagGroup
		INNER JOIN ZNodeTagGroupCategory TGC ON ZNodeTagGroup.TagGroupID = TGC.TagGroupID AND TGC.CategoryID = @CategoryID
	WHERE
		ZnodeTagGroup.TagGroupID IN 
			(SELECT 
				TagGroupID 
			FROM
				ZNodeTag
			WHERE 
				TagID 
			IN
				(SELECT 
					TagID 
				FROM 
					#ZnodeTag)
			AND
				TagGroupID 
			NOT IN
				(SELECT 
					TagGroupID 
				FROM
					ZNodeTag
				INNER JOIN	
					#ZNodeTagSplit TS
				ON
					TS.TagId = ZNodeTag.TagID))
	
	
		SELECT
			TagGroupId
			,TagGroupLabel
			,ControlTypeId
			,TagGroupProductCount
		FROM
			#ZNodeTagGroup ORDER BY DisplayOrder;
					
		SELECT
				TagId,
				TagGroupID,
				TagName,
				IconPath,
				TagCount AS 'TagProductCount'
		FROM
				#ZNodeTag
		WHERE
				EXISTS
				(SELECT 1 FROM #ZNodeTagGroup WHERE TagGroupId = #ZNodeTag.TagGroupId)	
				AND TagCount > 0
		ORDER BY DisplayOrder;
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_WS_GetProductAddOnsByID_XML]'
GO

ALTER PROCEDURE [dbo].[ZNode_WS_GetProductAddOnsByID_XML](@ProductID INT = NULL)
AS            
	BEGIN

		SELECT [AddOnId]
			  ,[ProductId]
			  ,[Title]
			  ,[Name]
			  ,[Description]
			  ,[DisplayOrder]
			  ,[DisplayType]
			  ,[OptionalInd]
			  ,[AllowBackOrder]
			  ,[InStockMsg]
			  ,[OutOfStockMsg]
			  ,[BackOrderMsg]
			  ,[PromptMsg]
			  ,[TrackInventoryInd]
			  ,ZL.LocaleCode,
			  (SELECT [AddOnValueId]
					  ,[AddOnId]
					  ,[Name]
					  ,[Description]
					  ,AddOnValue.[Sku]
					  ,ZNodeAddOnValueInventory.[QuantityOnHand]
					  ,[DefaultInd]
					  ,[DisplayOrder]
					  ,[ImageFile]
					  ,[RetailPrice]
					  ,[SalePrice]
					  ,[WholesalePrice]
					  ,[Weight]
					  ,[Length]
					  ,[Height]
					  ,[Width]
					  ,[ShippingRuleTypeID]
					  ,[FreeShippingInd]
					  ,[WebServiceDownloadDte]
					  ,[UpdateDte]
					 ,ZNodeAddOnValueInventory.[ReorderLevel] FROM ZNodeAddOnValue AddOnValue
					 INNER JOIN ZNodeAddOnValueInventory ON ZNodeAddOnValueInventory.SKU = AddOnValue.SKU
					 WHERE AddOnValue.AddOnId = AddOn.AddOnId FOR XML AUTO , TYPE , ELEMENTS)
			FROM ZNodeAddOn AddOn 
			LEFT JOIN ZNodeLocale ZL ON ZL.LocaleId = AddOn.LocaleId
			WHERE AddOn.AddOnID IN 
				(SELECT AddOnId FROM ZNodeProductAddOn WHERE ProductID = @ProductID) FOR XML PATH ('AddOn'), TYPE , ELEMENTS
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_Find]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeProduct table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_Find
(

	@SearchUsingOR bit   = null ,

	@ProductID int   = null ,

	@Name nvarchar (MAX)  = null ,

	@ShortDescription nvarchar (MAX)  = null ,

	@Description nvarchar (MAX)  = null ,

	@FeaturesDesc nvarchar (MAX)  = null ,

	@ProductNum nvarchar (100)  = null ,

	@ProductTypeID int   = null ,

	@RetailPrice money   = null ,

	@SalePrice money   = null ,

	@WholesalePrice money   = null ,

	@ImageFile nvarchar (MAX)  = null ,

	@ImageAltTag nvarchar (MAX)  = null ,

	@Weight decimal (18, 2)  = null ,

	@Length decimal (18, 2)  = null ,

	@Width decimal (18, 2)  = null ,

	@Height decimal (18, 2)  = null ,

	@BeginActiveDate datetime   = null ,

	@EndActiveDate datetime   = null ,

	@DisplayOrder int   = null ,

	@ActiveInd bit   = null ,

	@CallForPricing bit   = null ,

	@HomepageSpecial bit   = null ,

	@CategorySpecial bit   = null ,

	@InventoryDisplay tinyint   = null ,

	@Keywords nvarchar (MAX)  = null ,

	@ManufacturerID int   = null ,

	@AdditionalInfoLink nvarchar (MAX)  = null ,

	@AdditionalInfoLinkLabel nvarchar (MAX)  = null ,

	@ShippingRuleTypeID int   = null ,

	@SEOTitle nvarchar (MAX)  = null ,

	@SEOKeywords nvarchar (MAX)  = null ,

	@SEODescription nvarchar (MAX)  = null ,

	@Custom1 nvarchar (MAX)  = null ,

	@Custom2 nvarchar (MAX)  = null ,

	@Custom3 nvarchar (MAX)  = null ,

	@ShipEachItemSeparately bit   = null ,

	@SKU nvarchar (100)  = null ,

	@AllowBackOrder bit   = null ,

	@BackOrderMsg nvarchar (MAX)  = null ,

	@DropShipInd bit   = null ,

	@DropShipEmailID nvarchar (MAX)  = null ,

	@Specifications nvarchar (MAX)  = null ,

	@AdditionalInformation nvarchar (MAX)  = null ,

	@InStockMsg nvarchar (MAX)  = null ,

	@OutOfStockMsg nvarchar (MAX)  = null ,

	@TrackInventoryInd bit   = null ,

	@DownloadLink nvarchar (MAX)  = null ,

	@FreeShippingInd bit   = null ,

	@NewProductInd bit   = null ,

	@SEOURL nvarchar (100)  = null ,

	@MaxQty int   = null ,

	@ShipSeparately bit   = null ,

	@FeaturedInd bit   = null ,

	@WebServiceDownloadDte datetime   = null ,

	@UpdateDte datetime   = null ,

	@SupplierID int   = null ,

	@RecurringBillingInd bit   = null ,

	@RecurringBillingInstallmentInd bit   = null ,

	@RecurringBillingPeriod nvarchar (50)  = null ,

	@RecurringBillingFrequency nvarchar (MAX)  = null ,

	@RecurringBillingTotalCycles int   = null ,

	@RecurringBillingInitialAmount money   = null ,

	@TaxClassID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ProductID]
	, [Name]
	, [ShortDescription]
	, [Description]
	, [FeaturesDesc]
	, [ProductNum]
	, [ProductTypeID]
	, [RetailPrice]
	, [SalePrice]
	, [WholesalePrice]
	, [ImageFile]
	, [ImageAltTag]
	, [Weight]
	, [Length]
	, [Width]
	, [Height]
	, [BeginActiveDate]
	, [EndActiveDate]
	, [DisplayOrder]
	, [ActiveInd]
	, [CallForPricing]
	, [HomepageSpecial]
	, [CategorySpecial]
	, [InventoryDisplay]
	, [Keywords]
	, [ManufacturerID]
	, [AdditionalInfoLink]
	, [AdditionalInfoLinkLabel]
	, [ShippingRuleTypeID]
	, [SEOTitle]
	, [SEOKeywords]
	, [SEODescription]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [ShipEachItemSeparately]
	, [SKU]
	, [AllowBackOrder]
	, [BackOrderMsg]
	, [DropShipInd]
	, [DropShipEmailID]
	, [Specifications]
	, [AdditionalInformation]
	, [InStockMsg]
	, [OutOfStockMsg]
	, [TrackInventoryInd]
	, [DownloadLink]
	, [FreeShippingInd]
	, [NewProductInd]
	, [SEOURL]
	, [MaxQty]
	, [ShipSeparately]
	, [FeaturedInd]
	, [WebServiceDownloadDte]
	, [UpdateDte]
	, [SupplierID]
	, [RecurringBillingInd]
	, [RecurringBillingInstallmentInd]
	, [RecurringBillingPeriod]
	, [RecurringBillingFrequency]
	, [RecurringBillingTotalCycles]
	, [RecurringBillingInitialAmount]
	, [TaxClassID]
    FROM
	[dbo].[ZNodeProduct]
    WHERE 
	 ([ProductID] = @ProductID OR @ProductID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([ShortDescription] = @ShortDescription OR @ShortDescription IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([FeaturesDesc] = @FeaturesDesc OR @FeaturesDesc IS NULL)
	AND ([ProductNum] = @ProductNum OR @ProductNum IS NULL)
	AND ([ProductTypeID] = @ProductTypeID OR @ProductTypeID IS NULL)
	AND ([RetailPrice] = @RetailPrice OR @RetailPrice IS NULL)
	AND ([SalePrice] = @SalePrice OR @SalePrice IS NULL)
	AND ([WholesalePrice] = @WholesalePrice OR @WholesalePrice IS NULL)
	AND ([ImageFile] = @ImageFile OR @ImageFile IS NULL)
	AND ([ImageAltTag] = @ImageAltTag OR @ImageAltTag IS NULL)
	AND ([Weight] = @Weight OR @Weight IS NULL)
	AND ([Length] = @Length OR @Length IS NULL)
	AND ([Width] = @Width OR @Width IS NULL)
	AND ([Height] = @Height OR @Height IS NULL)
	AND ([BeginActiveDate] = @BeginActiveDate OR @BeginActiveDate IS NULL)
	AND ([EndActiveDate] = @EndActiveDate OR @EndActiveDate IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([ActiveInd] = @ActiveInd OR @ActiveInd IS NULL)
	AND ([CallForPricing] = @CallForPricing OR @CallForPricing IS NULL)
	AND ([HomepageSpecial] = @HomepageSpecial OR @HomepageSpecial IS NULL)
	AND ([CategorySpecial] = @CategorySpecial OR @CategorySpecial IS NULL)
	AND ([InventoryDisplay] = @InventoryDisplay OR @InventoryDisplay IS NULL)
	AND ([Keywords] = @Keywords OR @Keywords IS NULL)
	AND ([ManufacturerID] = @ManufacturerID OR @ManufacturerID IS NULL)
	AND ([AdditionalInfoLink] = @AdditionalInfoLink OR @AdditionalInfoLink IS NULL)
	AND ([AdditionalInfoLinkLabel] = @AdditionalInfoLinkLabel OR @AdditionalInfoLinkLabel IS NULL)
	AND ([ShippingRuleTypeID] = @ShippingRuleTypeID OR @ShippingRuleTypeID IS NULL)
	AND ([SEOTitle] = @SEOTitle OR @SEOTitle IS NULL)
	AND ([SEOKeywords] = @SEOKeywords OR @SEOKeywords IS NULL)
	AND ([SEODescription] = @SEODescription OR @SEODescription IS NULL)
	AND ([Custom1] = @Custom1 OR @Custom1 IS NULL)
	AND ([Custom2] = @Custom2 OR @Custom2 IS NULL)
	AND ([Custom3] = @Custom3 OR @Custom3 IS NULL)
	AND ([ShipEachItemSeparately] = @ShipEachItemSeparately OR @ShipEachItemSeparately IS NULL)
	AND ([SKU] = @SKU OR @SKU IS NULL)
	AND ([AllowBackOrder] = @AllowBackOrder OR @AllowBackOrder IS NULL)
	AND ([BackOrderMsg] = @BackOrderMsg OR @BackOrderMsg IS NULL)
	AND ([DropShipInd] = @DropShipInd OR @DropShipInd IS NULL)
	AND ([DropShipEmailID] = @DropShipEmailID OR @DropShipEmailID IS NULL)
	AND ([Specifications] = @Specifications OR @Specifications IS NULL)
	AND ([AdditionalInformation] = @AdditionalInformation OR @AdditionalInformation IS NULL)
	AND ([InStockMsg] = @InStockMsg OR @InStockMsg IS NULL)
	AND ([OutOfStockMsg] = @OutOfStockMsg OR @OutOfStockMsg IS NULL)
	AND ([TrackInventoryInd] = @TrackInventoryInd OR @TrackInventoryInd IS NULL)
	AND ([DownloadLink] = @DownloadLink OR @DownloadLink IS NULL)
	AND ([FreeShippingInd] = @FreeShippingInd OR @FreeShippingInd IS NULL)
	AND ([NewProductInd] = @NewProductInd OR @NewProductInd IS NULL)
	AND ([SEOURL] = @SEOURL OR @SEOURL IS NULL)
	AND ([MaxQty] = @MaxQty OR @MaxQty IS NULL)
	AND ([ShipSeparately] = @ShipSeparately OR @ShipSeparately IS NULL)
	AND ([FeaturedInd] = @FeaturedInd OR @FeaturedInd IS NULL)
	AND ([WebServiceDownloadDte] = @WebServiceDownloadDte OR @WebServiceDownloadDte IS NULL)
	AND ([UpdateDte] = @UpdateDte OR @UpdateDte IS NULL)
	AND ([SupplierID] = @SupplierID OR @SupplierID IS NULL)
	AND ([RecurringBillingInd] = @RecurringBillingInd OR @RecurringBillingInd IS NULL)
	AND ([RecurringBillingInstallmentInd] = @RecurringBillingInstallmentInd OR @RecurringBillingInstallmentInd IS NULL)
	AND ([RecurringBillingPeriod] = @RecurringBillingPeriod OR @RecurringBillingPeriod IS NULL)
	AND ([RecurringBillingFrequency] = @RecurringBillingFrequency OR @RecurringBillingFrequency IS NULL)
	AND ([RecurringBillingTotalCycles] = @RecurringBillingTotalCycles OR @RecurringBillingTotalCycles IS NULL)
	AND ([RecurringBillingInitialAmount] = @RecurringBillingInitialAmount OR @RecurringBillingInitialAmount IS NULL)
	AND ([TaxClassID] = @TaxClassID OR @TaxClassID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ProductID]
	, [Name]
	, [ShortDescription]
	, [Description]
	, [FeaturesDesc]
	, [ProductNum]
	, [ProductTypeID]
	, [RetailPrice]
	, [SalePrice]
	, [WholesalePrice]
	, [ImageFile]
	, [ImageAltTag]
	, [Weight]
	, [Length]
	, [Width]
	, [Height]
	, [BeginActiveDate]
	, [EndActiveDate]
	, [DisplayOrder]
	, [ActiveInd]
	, [CallForPricing]
	, [HomepageSpecial]
	, [CategorySpecial]
	, [InventoryDisplay]
	, [Keywords]
	, [ManufacturerID]
	, [AdditionalInfoLink]
	, [AdditionalInfoLinkLabel]
	, [ShippingRuleTypeID]
	, [SEOTitle]
	, [SEOKeywords]
	, [SEODescription]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [ShipEachItemSeparately]
	, [SKU]
	, [AllowBackOrder]
	, [BackOrderMsg]
	, [DropShipInd]
	, [DropShipEmailID]
	, [Specifications]
	, [AdditionalInformation]
	, [InStockMsg]
	, [OutOfStockMsg]
	, [TrackInventoryInd]
	, [DownloadLink]
	, [FreeShippingInd]
	, [NewProductInd]
	, [SEOURL]
	, [MaxQty]
	, [ShipSeparately]
	, [FeaturedInd]
	, [WebServiceDownloadDte]
	, [UpdateDte]
	, [SupplierID]
	, [RecurringBillingInd]
	, [RecurringBillingInstallmentInd]
	, [RecurringBillingPeriod]
	, [RecurringBillingFrequency]
	, [RecurringBillingTotalCycles]
	, [RecurringBillingInitialAmount]
	, [TaxClassID]
    FROM
	[dbo].[ZNodeProduct]
    WHERE 
	 ([ProductID] = @ProductID AND @ProductID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([ShortDescription] = @ShortDescription AND @ShortDescription is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([FeaturesDesc] = @FeaturesDesc AND @FeaturesDesc is not null)
	OR ([ProductNum] = @ProductNum AND @ProductNum is not null)
	OR ([ProductTypeID] = @ProductTypeID AND @ProductTypeID is not null)
	OR ([RetailPrice] = @RetailPrice AND @RetailPrice is not null)
	OR ([SalePrice] = @SalePrice AND @SalePrice is not null)
	OR ([WholesalePrice] = @WholesalePrice AND @WholesalePrice is not null)
	OR ([ImageFile] = @ImageFile AND @ImageFile is not null)
	OR ([ImageAltTag] = @ImageAltTag AND @ImageAltTag is not null)
	OR ([Weight] = @Weight AND @Weight is not null)
	OR ([Length] = @Length AND @Length is not null)
	OR ([Width] = @Width AND @Width is not null)
	OR ([Height] = @Height AND @Height is not null)
	OR ([BeginActiveDate] = @BeginActiveDate AND @BeginActiveDate is not null)
	OR ([EndActiveDate] = @EndActiveDate AND @EndActiveDate is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([ActiveInd] = @ActiveInd AND @ActiveInd is not null)
	OR ([CallForPricing] = @CallForPricing AND @CallForPricing is not null)
	OR ([HomepageSpecial] = @HomepageSpecial AND @HomepageSpecial is not null)
	OR ([CategorySpecial] = @CategorySpecial AND @CategorySpecial is not null)
	OR ([InventoryDisplay] = @InventoryDisplay AND @InventoryDisplay is not null)
	OR ([Keywords] = @Keywords AND @Keywords is not null)
	OR ([ManufacturerID] = @ManufacturerID AND @ManufacturerID is not null)
	OR ([AdditionalInfoLink] = @AdditionalInfoLink AND @AdditionalInfoLink is not null)
	OR ([AdditionalInfoLinkLabel] = @AdditionalInfoLinkLabel AND @AdditionalInfoLinkLabel is not null)
	OR ([ShippingRuleTypeID] = @ShippingRuleTypeID AND @ShippingRuleTypeID is not null)
	OR ([SEOTitle] = @SEOTitle AND @SEOTitle is not null)
	OR ([SEOKeywords] = @SEOKeywords AND @SEOKeywords is not null)
	OR ([SEODescription] = @SEODescription AND @SEODescription is not null)
	OR ([Custom1] = @Custom1 AND @Custom1 is not null)
	OR ([Custom2] = @Custom2 AND @Custom2 is not null)
	OR ([Custom3] = @Custom3 AND @Custom3 is not null)
	OR ([ShipEachItemSeparately] = @ShipEachItemSeparately AND @ShipEachItemSeparately is not null)
	OR ([SKU] = @SKU AND @SKU is not null)
	OR ([AllowBackOrder] = @AllowBackOrder AND @AllowBackOrder is not null)
	OR ([BackOrderMsg] = @BackOrderMsg AND @BackOrderMsg is not null)
	OR ([DropShipInd] = @DropShipInd AND @DropShipInd is not null)
	OR ([DropShipEmailID] = @DropShipEmailID AND @DropShipEmailID is not null)
	OR ([Specifications] = @Specifications AND @Specifications is not null)
	OR ([AdditionalInformation] = @AdditionalInformation AND @AdditionalInformation is not null)
	OR ([InStockMsg] = @InStockMsg AND @InStockMsg is not null)
	OR ([OutOfStockMsg] = @OutOfStockMsg AND @OutOfStockMsg is not null)
	OR ([TrackInventoryInd] = @TrackInventoryInd AND @TrackInventoryInd is not null)
	OR ([DownloadLink] = @DownloadLink AND @DownloadLink is not null)
	OR ([FreeShippingInd] = @FreeShippingInd AND @FreeShippingInd is not null)
	OR ([NewProductInd] = @NewProductInd AND @NewProductInd is not null)
	OR ([SEOURL] = @SEOURL AND @SEOURL is not null)
	OR ([MaxQty] = @MaxQty AND @MaxQty is not null)
	OR ([ShipSeparately] = @ShipSeparately AND @ShipSeparately is not null)
	OR ([FeaturedInd] = @FeaturedInd AND @FeaturedInd is not null)
	OR ([WebServiceDownloadDte] = @WebServiceDownloadDte AND @WebServiceDownloadDte is not null)
	OR ([UpdateDte] = @UpdateDte AND @UpdateDte is not null)
	OR ([SupplierID] = @SupplierID AND @SupplierID is not null)
	OR ([RecurringBillingInd] = @RecurringBillingInd AND @RecurringBillingInd is not null)
	OR ([RecurringBillingInstallmentInd] = @RecurringBillingInstallmentInd AND @RecurringBillingInstallmentInd is not null)
	OR ([RecurringBillingPeriod] = @RecurringBillingPeriod AND @RecurringBillingPeriod is not null)
	OR ([RecurringBillingFrequency] = @RecurringBillingFrequency AND @RecurringBillingFrequency is not null)
	OR ([RecurringBillingTotalCycles] = @RecurringBillingTotalCycles AND @RecurringBillingTotalCycles is not null)
	OR ([RecurringBillingInitialAmount] = @RecurringBillingInitialAmount AND @RecurringBillingInitialAmount is not null)
	OR ([TaxClassID] = @TaxClassID AND @TaxClassID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_SearchReview]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ZNode_SearchReview]
    (
      @ReviewTitle VARCHAR(100)='',
      @Nickname VARCHAR(100)='',
      @ProductName VARCHAR(100)='',
      @Status VARCHAR(10)=''
    )
AS 
    BEGIN    
	 -- SET NOCOUNT ON added to prevent extra result sets from    
	 -- interfering with SELECT statements.    
        SET NOCOUNT ON ;        
     
     
        SELECT  ReviewID,
                AccountID,
                P.Name,
                [Subject],
                Pros,
                Cons,
                Comments,
                CreateUser,
                UserLocation,
                Rating,
                CreateDate,
                CASE [Status]
                  WHEN 'A' THEN 'Active'
                  WHEN 'N' THEN 'New'
                  ELSE 'Inactive'
                END AS [Status]
        FROM    ZnodeReview R,
                ProductsView P
        WHERE   
			R.ProductID = P.ProductID AND
			R.[Subject] LIKE '%'+ @ReviewTitle + '%' AND
			R.CreateUser LIKE '%'+ @Nickname + '%' AND
			P.Name LIKE '%'+ @ProductName + '%' AND
			R.[Status] LIKE '%'+ @Status + '%'			
		ORDER BY ReviewId DESC
    END    

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchStore]'
GO
ALTER PROCEDURE [dbo].[ZNode_SearchStore]    
(    
@NAME VARCHAR(MAX) = NULL,    
@ZIP VARCHAR(MAX) = NULL,    
@CITY VARCHAR(MAX) = NULL,    
@STATE VARCHAR(MAX) = NULL    
)    
AS    
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT 
		STOREID,
		NAME,
		CITY,
		[STATE],
		ZIP,
		ACTIVEIND 
	FROM ZNODESTORE WHERE 
	(Name LIKE '%' + @NAME + '%' OR LEN(@NAME) = 0) AND
	(ZIP LIKE '%' + @ZIP + '%' OR LEN(@ZIP) = 0) AND
	(CITY LIKE '%' + @CITY + '%' OR LEN(@CITY) = 0) AND
	([State] LIKE '%' + @STATE + '%' OR LEN(@STATE) = 0)
	Order By Name
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_UpdateSkuPrice]'
GO

-- =============================================                  
-- Create date: August 30, 2008              
-- Description: Update product/attribute / addon  prices using Sku    
-- =============================================           
ALTER PROCEDURE [dbo].[ZNode_UpdateSkuPrice]    
(    
 @Sku varchar(max),    
 @ProductId int = 0,    
 @SkuId int = 0,    
 @AddOnValueId int = 0,    
 @RetailPrice Money = NULL,    
 @SalePrice Money = NULL,    
 @WholeSalePrice Money = NULL    
)    
AS            
 BEGIN            
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;    

 IF (@ProductId > 0)    
  BEGIN    
   UPDATE ZNodeProduct SET RetailPrice = @RetailPrice,SalePrice = @SalePrice,WholeSalePrice = @WholeSalePrice WHERE ProductId = @ProductId    
  END    
 ELSE IF (@SkuId > 0)    
  BEGIN    
   UPDATE ZNodeSKU SET RetailPriceOverride = @RetailPrice,SalePriceOverride = @SalePrice,WholeSalePriceOverride = @WholeSalePrice WHERE SKUID = @SkuId    
  END    
 ELSE IF (@AddOnValueId > 0)    
  BEGIN    
   UPDATE ZNodeAddOnValue SET RetailPrice = @RetailPrice,SalePrice = @SalePrice,WholeSalePrice = @WholeSalePrice WHERE AddOnValueId = @AddOnValueId    
  END      
 ELSE IF EXISTS(SELECT * FROM ZNodeProduct WHERE SKU = @Sku)    
  BEGIN    
   UPDATE ZNodeSKU SET RetailPriceOverride = @RetailPrice,SalePriceOverride = @SalePrice,WholeSalePriceOverride = @WholeSalePrice WHERE SKU = @Sku    
  END    
 ELSE IF EXISTS(SELECT * FROM ZNodeSKU WHERE SKU = @Sku) 
  BEGIN    
   UPDATE ZNodeProduct SET RetailPrice = @RetailPrice,SalePrice = @SalePrice,WholeSalePrice = @WholeSalePrice WHERE SKU = @Sku    
  END    
 ELSE IF EXISTS(SELECT * FROM ZNodeAddOnValue WHERE SKU = @Sku)    
  BEGIN    
   UPDATE ZNodeAddOnValue SET RetailPrice = @RetailPrice,SalePrice = @SalePrice,WholeSalePrice = @WholeSalePrice WHERE SKU = @Sku    
  END    
  
     
 SELECT @@RowCount    
END    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetCategoryHierarchy_Helper]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetCategoryHierarchy_Helper]       
 -- Add the parameters for the stored procedure here      
 @PortalId int = 0,     
 @CategoryId int = 0,  
 @CatalogId int = null  
AS      
BEGIN      
	-- SET NOCOUNT ON added to prevent extra result sets from      
	-- interfering with SELECT statements.      
	SET NOCOUNT ON;      
    
    WITH CATLIST(CategoryId, CategoryNodeID) AS
		 (
			SELECT           
				ZCN.CategoryID
				,ZCN.CategoryNodeID 
		  FROM
			ZNodeCategoryNode ZCN
			INNER JOIN ZNodeCatalog ZCG ON  ZCN.CatalogID = ZCG.CatalogID
			INNER JOIN ZNodePortalCatalog ZPC ON ZCG.CatalogID = ZPC.CatalogID
		 WHERE
			ZPC.PortalID = @PortalId AND ZCG.CatalogID = @CatalogId
			AND ZCN.CategoryID = @CategoryId
		  
		  UNION ALL
		            
		  SELECT           
			ZCN.CategoryID			
			,ZCN.CategoryNodeID
		  FROM
			ZNodeCategoryNode ZCN
			INNER JOIN ZNodeCatalog ZCG ON  ZCN.CatalogID = ZCG.CatalogID
			INNER JOIN ZNodePortalCatalog ZPC ON ZCG.CatalogID = ZPC.CatalogID
			INNER JOIN CATLIST d  ON ZCN.ParentCategoryNodeID = d.CategoryNodeID 
		 WHERE
			ZPC.PortalID = @PortalId AND ZCG.CatalogID = @CatalogId
		 )
		
		-- get ids
		SELECT CategoryId FROM CATLIST
END   
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_Update]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeProduct table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_Update
(

	@ProductID int   ,

	@Name nvarchar (MAX)  ,

	@ShortDescription nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@FeaturesDesc nvarchar (MAX)  ,

	@ProductNum nvarchar (100)  ,

	@ProductTypeID int   ,

	@RetailPrice money   ,

	@SalePrice money   ,

	@WholesalePrice money   ,

	@ImageFile nvarchar (MAX)  ,

	@ImageAltTag nvarchar (MAX)  ,

	@Weight decimal (18, 2)  ,

	@Length decimal (18, 2)  ,

	@Width decimal (18, 2)  ,

	@Height decimal (18, 2)  ,

	@BeginActiveDate datetime   ,

	@EndActiveDate datetime   ,

	@DisplayOrder int   ,

	@ActiveInd bit   ,

	@CallForPricing bit   ,

	@HomepageSpecial bit   ,

	@CategorySpecial bit   ,

	@InventoryDisplay tinyint   ,

	@Keywords nvarchar (MAX)  ,

	@ManufacturerID int   ,

	@AdditionalInfoLink nvarchar (MAX)  ,

	@AdditionalInfoLinkLabel nvarchar (MAX)  ,

	@ShippingRuleTypeID int   ,

	@SEOTitle nvarchar (MAX)  ,

	@SEOKeywords nvarchar (MAX)  ,

	@SEODescription nvarchar (MAX)  ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@ShipEachItemSeparately bit   ,

	@SKU nvarchar (100)  ,

	@AllowBackOrder bit   ,

	@BackOrderMsg nvarchar (MAX)  ,

	@DropShipInd bit   ,

	@DropShipEmailID nvarchar (MAX)  ,

	@Specifications nvarchar (MAX)  ,

	@AdditionalInformation nvarchar (MAX)  ,

	@InStockMsg nvarchar (MAX)  ,

	@OutOfStockMsg nvarchar (MAX)  ,

	@TrackInventoryInd bit   ,

	@DownloadLink nvarchar (MAX)  ,

	@FreeShippingInd bit   ,

	@NewProductInd bit   ,

	@SEOURL nvarchar (100)  ,

	@MaxQty int   ,

	@ShipSeparately bit   ,

	@FeaturedInd bit   ,

	@WebServiceDownloadDte datetime   ,

	@UpdateDte datetime   ,

	@SupplierID int   ,

	@RecurringBillingInd bit   ,

	@RecurringBillingInstallmentInd bit   ,

	@RecurringBillingPeriod nvarchar (50)  ,

	@RecurringBillingFrequency nvarchar (MAX)  ,

	@RecurringBillingTotalCycles int   ,

	@RecurringBillingInitialAmount money   ,

	@TaxClassID int   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeProduct]
				SET
					[Name] = @Name
					,[ShortDescription] = @ShortDescription
					,[Description] = @Description
					,[FeaturesDesc] = @FeaturesDesc
					,[ProductNum] = @ProductNum
					,[ProductTypeID] = @ProductTypeID
					,[RetailPrice] = @RetailPrice
					,[SalePrice] = @SalePrice
					,[WholesalePrice] = @WholesalePrice
					,[ImageFile] = @ImageFile
					,[ImageAltTag] = @ImageAltTag
					,[Weight] = @Weight
					,[Length] = @Length
					,[Width] = @Width
					,[Height] = @Height
					,[BeginActiveDate] = @BeginActiveDate
					,[EndActiveDate] = @EndActiveDate
					,[DisplayOrder] = @DisplayOrder
					,[ActiveInd] = @ActiveInd
					,[CallForPricing] = @CallForPricing
					,[HomepageSpecial] = @HomepageSpecial
					,[CategorySpecial] = @CategorySpecial
					,[InventoryDisplay] = @InventoryDisplay
					,[Keywords] = @Keywords
					,[ManufacturerID] = @ManufacturerID
					,[AdditionalInfoLink] = @AdditionalInfoLink
					,[AdditionalInfoLinkLabel] = @AdditionalInfoLinkLabel
					,[ShippingRuleTypeID] = @ShippingRuleTypeID
					,[SEOTitle] = @SEOTitle
					,[SEOKeywords] = @SEOKeywords
					,[SEODescription] = @SEODescription
					,[Custom1] = @Custom1
					,[Custom2] = @Custom2
					,[Custom3] = @Custom3
					,[ShipEachItemSeparately] = @ShipEachItemSeparately
					,[SKU] = @SKU
					,[AllowBackOrder] = @AllowBackOrder
					,[BackOrderMsg] = @BackOrderMsg
					,[DropShipInd] = @DropShipInd
					,[DropShipEmailID] = @DropShipEmailID
					,[Specifications] = @Specifications
					,[AdditionalInformation] = @AdditionalInformation
					,[InStockMsg] = @InStockMsg
					,[OutOfStockMsg] = @OutOfStockMsg
					,[TrackInventoryInd] = @TrackInventoryInd
					,[DownloadLink] = @DownloadLink
					,[FreeShippingInd] = @FreeShippingInd
					,[NewProductInd] = @NewProductInd
					,[SEOURL] = @SEOURL
					,[MaxQty] = @MaxQty
					,[ShipSeparately] = @ShipSeparately
					,[FeaturedInd] = @FeaturedInd
					,[WebServiceDownloadDte] = @WebServiceDownloadDte
					,[UpdateDte] = @UpdateDte
					,[SupplierID] = @SupplierID
					,[RecurringBillingInd] = @RecurringBillingInd
					,[RecurringBillingInstallmentInd] = @RecurringBillingInstallmentInd
					,[RecurringBillingPeriod] = @RecurringBillingPeriod
					,[RecurringBillingFrequency] = @RecurringBillingFrequency
					,[RecurringBillingTotalCycles] = @RecurringBillingTotalCycles
					,[RecurringBillingInitialAmount] = @RecurringBillingInitialAmount
					,[TaxClassID] = @TaxClassID
				WHERE
[ProductID] = @ProductID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchSupplier]'
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Create date: 25 Nov 2008        
-- Update date: 13 April 2010
-- Description: Search supplier for a given input          
-- =============================================          
ALTER procedure[dbo].[ZNode_SearchSupplier]          
 (            
  @Name Varchar(300),            
  @Status VARCHAR(MAX) = NULL       
 )          
As          
Begin         
	 -- SET NOCOUNT ON added to prevent extra result sets from          
	 -- interfering with SELECT statements.          
	Set nocount on;       


	SELECT [SupplierID]
      ,[SupplierTypeID]
      ,[ExternalSupplierNo]
      ,[Name]
      ,[Description]
      ,[ContactFirstName]
      ,[ContactLastName]
      ,[ContactPhone]
      ,[ContactEmail]
      ,[NotificationEmailID]
      ,[EmailNotificationTemplate]
      ,[EnableEmailNotification]
      ,[DisplayOrder]
      ,[ActiveInd]
      ,[Custom1]
      ,[Custom2]
      ,[Custom3]
      ,[Custom4]
      ,[Custom5]
	FROM [ZNodeSupplier]
  
	WHERE (Name  Like '%' + @Name + '%' OR Len(@Name) = 0)
	AND (ActiveInd  Like '%' + @Status + '%' OR Len(@Status) = 0)
	Order by DisplayOrder desc       
End 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_CustomerSearch]'
GO

-- =================================================  
-- Create date: Jun 1, 2009    
-- Description: Returns list of Customers  
-- ================================================  
ALTER PROCEDURE [dbo].[ZNode_CustomerSearch]   
(        
	@FIRSTNAME VARCHAR(MAX) ='',
	@LASTNAME VARCHAR(MAX) = '',
	@COMPANYNAME VARCHAR(MAX) = '',
	@POSTALCODE VARCHAR(MAX) = '',
	@LOGINNAME VARCHAR(MAX) = '',
	@ORDERID INT = 0,
	@PORTALID INT = 0,
	@PortalIDs VARCHAR(MAX) = '0'
)        
AS        
BEGIN
	-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM
	-- INTERFERING WITH SELECT STATEMENTS.        
	SET NOCOUNT ON;        

	SELECT 
		DISTINCT(ZA.AccountID),
		ZA.BillingLastName,
		ZA.BillingFirstName,
		ZA.BillingCompanyName,
		ZA.BillingStreet,    
		ZA.BillingStreet1,
		ZA.BillingCity,
		ZA.BillingStateCode,
		ZA.BillingPostalCode,
		ZA.BillingPhoneNumber,
		ZA.UserID,
		ISNULL((Select UserName from aspnet_users where Userid= ZA.UserID),'') as Username		 
	FROM ZNodeAccount ZA
	WHERE
		EXISTS (SELECT USERID FROM Aspnet_Profile WHERE CAST(PropertyValuesString AS NVARCHAR(1000)) IN (SELECT Value FROM SC_Splitter(@PortalIDs + ',AllStores', ',')))
		
		AND
		(ZA.AccountID IN (SELECT AccountID FROM ZNodeAccountProfile WHERE 
		ProfileID IN (SELECT ProfileID FROM ZNodePortalProfile 
		WHERE PortalID = @PortalID)))
		
		AND
		(
			((BillingFirstName LIKE '%' + @FirstName + '%') OR LEN(@FirstName) = 0) AND
			((BillingLastName LIKE '%' + @LastName + '%') OR LEN(@LastName) = 0) AND 
			((CompanyName LIKE '%' + @CompanyName + '%') OR (BillingCompanyName LIKE '%' + @CompanyName + '%') 
			OR (ShipCompanyName LIKE '%' + @CompanyName + '%') OR LEN(@CompanyName) = 0) AND
			((BillingPostalCode LIKE '%' + @POSTALCODE + '%') OR LEN(@POSTALCODE) = 0) AND
			((ZA.UserID IN (SELECT USERID FROM ASPNET_USERS WHERE USERNAME like '%' + @LOGINNAME + '%') OR LEN(@LOGINNAME) = 0)) AND
			((ZA.AccountID IN (SELECT AccountID FROM ZNodeOrder WHERE ORDERID = @ORDERID)) OR @OrderID = 0)
		) ORDER BY ZA.ACCOUNTID DESC 
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetProductsByCatalogID]'
GO

CREATE PROCEDURE [dbo].[ZNode_GetProductsByCatalogID]
(@CatalogId int)  
As   
Begin  
SELECT [ProductID]
      ,[Name]
      ,[ShortDescription]
      ,[Description]
      ,[FeaturesDesc]
      ,[ProductNum]
      ,[ProductTypeID]
      ,[RetailPrice]
      ,[SalePrice]
      ,[WholesalePrice]
      ,[ImageFile]
      ,[ImageAltTag]
      ,[Weight]
      ,[Length]
      ,[Width]
      ,[Height]
      ,[BeginActiveDate]
      ,[EndActiveDate]
      ,[DisplayOrder]
      ,[ActiveInd]
      ,[CallForPricing]
      ,[HomepageSpecial]
      ,[CategorySpecial]
      ,[InventoryDisplay]
      ,[Keywords]
      ,[ManufacturerID]
      ,[AdditionalInfoLink]
      ,[AdditionalInfoLinkLabel]
      ,[ShippingRuleTypeID]
      ,[SEOTitle]
      ,[SEOKeywords]
      ,[SEODescription]
      ,[Custom1]
      ,[Custom2]
      ,[Custom3]
      ,[ShipEachItemSeparately]
      ,[SKU]
      ,[AllowBackOrder]
      ,[BackOrderMsg]
      ,[DropShipInd]
      ,[DropShipEmailID]
      ,[Specifications]
      ,[AdditionalInformation]
      ,[InStockMsg]
      ,[OutOfStockMsg]
      ,[TrackInventoryInd]
      ,[DownloadLink]
      ,[FreeShippingInd]
      ,[NewProductInd]
      ,[SEOURL]
      ,[MaxQty]
      ,[ShipSeparately]
      ,[FeaturedInd]
      ,[WebServiceDownloadDte]
      ,[UpdateDte]
      ,[SupplierID]
      ,[RecurringBillingInd]
      ,[RecurringBillingInstallmentInd]
      ,[RecurringBillingPeriod]
      ,[RecurringBillingFrequency]
      ,[RecurringBillingTotalCycles]
      ,[RecurringBillingInitialAmount]
      ,[TaxClassID]
  FROM [ZNodeProduct]
 WHERE ProductID in  
		(SELECT ProductID 
		   FROM ZNodeProductCategory 
		  WHERE CategoryID in  
				(SELECT CategoryID 
				   FROM ZNodeCategoryNode 
				  WHERE CatalogID = @CatalogId))  
End  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeAddOn_GetByLocaleId]'
GO
SET ANSI_NULLS OFF
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAddOn table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodeAddOn_GetByLocaleId]
(

	@LocaleId int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AddOnID],
					[ProductID],
					[Title],
					[Name],
					[Description],
					[DisplayOrder],
					[DisplayType],
					[OptionalInd],
					[AllowBackOrder],
					[InStockMsg],
					[OutOfStockMsg],
					[BackOrderMsg],
					[PromptMsg],
					[TrackInventoryInd],
					[LocaleId]
				FROM
					[dbo].[ZNodeAddOn]
				WHERE
					[LocaleId] = @LocaleId
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeAttributeType_GetByLocaleId]'
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAttributeType table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ZNode_NT_ZNodeAttributeType_GetByLocaleId]
(

	@LocaleId int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AttributeTypeId],
					[Name],
					[Description],
					[DisplayOrder],
					[IsPrivate],
					[LocaleId]
				FROM
					[dbo].[ZNodeAttributeType]
				WHERE
					[LocaleId] = @LocaleId
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchProduct]'
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[ZNode_SearchProduct]                  
(                  
@NAME VARCHAR(MAX) = NULL,                  
@PRODUCTNUM VARCHAR(MAX) = NULL,                  
@SKU VARCHAR(MAX) = NULL,                  
@MANUFACTURERID VARCHAR(MAX) = NULL,                  
@PRODUCTTYPEID VARCHAR(MAX) = NULL,                  
@CATEGORYID VARCHAR(MAX) = NULL,  
@CATALOGID INT = NULL  
)                  
AS                  
BEGIN                  
	SET NOCOUNT ON;                  
	
    SELECT
		DISTINCT(p.ProductID),
		ImageFile,
		p.Name,
		p.ProductNum,
		p.Sku,
		RetailPrice,
		SalePrice,  
		WholeSalePrice,
		p.DisplayOrder,
		p.ActiveInd,
		QuantityOnHand FROM ZNodeProduct p
		INNER JOIN ZNodeProductInventory inv ON inv.ProductNum = p.ProductNum		
	WHERE
	
	(p.Name  Like '%' + @Name + '%' OR Len(@Name) = 0) AND
	
	(p.ProductNum  Like '%' + @PRODUCTNUM + '%' OR Len(@PRODUCTNUM) = 0) AND	

	(p.ProductTypeID = @PRODUCTTYPEID OR Len(@PRODUCTTYPEID) = 0 OR @PRODUCTTYPEID = 0 OR @PRODUCTTYPEID = '0') AND	
		
	(p.ManufacturerID = @MANUFACTURERID Or Len(@MANUFACTURERID) = 0 OR @MANUFACTURERID = '0')	AND
	
	((p.ProductID IN (SELECT ProductID FROM ZNodeSKU WHERE SKU Like '%' + @SKU + '%') OR Len(@SKU) = 0) OR (p.SKU Like '%' + @SKU + '%' OR Len(@SKU) = 0)) 
	
	AND
	
	(p.ProductID IN
		(SELECT ProductId FROM ZNODEPRODUCTCATEGORY WHERE CATEGORYID IN (SELECT DISTINCT  
		CategoryID FROM ZNodeCategoryNode WHERE CatalogID = @CATALOGID)) OR @CATALOGID = 0) AND
		
	(p.ProductID IN (SELECT ProductId FROM ZNODEPRODUCTCATEGORY WHERE CATEGORYID = @CATEGORYID) OR LEN(@CATEGORYID) = 0 OR @CATEGORYID = '0') 
		
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_GetBySKU]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAddOnValue table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_GetBySKU
(

	@SKU nvarchar (100)  
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AddOnValueID],
					[AddOnID],
					[Name],
					[Description],
					[SKU],
					[DefaultInd],
					[DisplayOrder],
					[ImageFile],
					[ImageAltTag],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[Weight],
					[Length],
					[Height],
					[Width],
					[ShippingRuleTypeID],
					[FreeShippingInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[TaxClassID]
				FROM
					[dbo].[ZNodeAddOnValue]
				WHERE
					[SKU] = @SKU
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_DeleteCategoryNode]'
GO
SET ANSI_NULLS ON
GO

---Delete Related Details for particular Product---        
CREATE PROCEDURE [dbo].[ZNode_DeleteCategoryNode]
(
	@CategoryNodeID INT
)
AS
  BEGIN
  
	-- Delete the child nodes first
    DELETE FROM 
		ZNodeCategoryNode
    WHERE  ParentCategoryNodeID = @CategoryNodeID;
      
    -- Delete the parent node
    DELETE FROM 
		ZNodeCategoryNode
	WHERE  CategoryNodeID = @CategoryNodeID;
      
  END; 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetAddOnsByAddOnValues_XML]'
GO
-- =============================================                
-- Create date: August 02, 2007
-- Last Updated date: April 24,2008
-- Update date: Apeil 19,2010
-- Description: Returns the add-ons and their value for this product           
-- =============================================              
ALTER PROCEDURE [dbo].[ZNode_GetAddOnsByAddOnValues_XML]  
 -- Add the parameters for the stored procedure here                    
 @ProductId int = 0,        
 @AddOnValues varchar(8000)                
AS                
BEGIN                

	-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM                  
	-- INTERFERING WITH SELECT STATEMENTS.                  
	SET NOCOUNT ON;
	
SELECT
      ZNodeAddOn.[AddOnID]  
      ,ZNodeAddOn.[ProductID]  
      ,ZNodeAddOn.[Title]  
      ,ZNodeAddOn.[Name]  
      ,ZNodeAddOn.[Description]  
      ,ZNodeAddOn.[DisplayOrder]  
      ,ZNodeAddOn.[DisplayType]  
      ,ZNodeAddOn.[OptionalInd]  
      ,ZNodeAddOn.[AllowBackOrder]  
      ,ZNodeAddOn.[InStockMsg]  
      ,ZNodeAddOn.[OutOfStockMsg]  
      ,ZNodeAddOn.[BackOrderMsg]  
      ,ZNodeAddOn.[PromptMsg]  
      ,ZNodeAddOn.[TrackInventoryInd]  
      ,ZNodeAddOn.[LocaleId],                     
     (                
      SELECT DISTINCT ZNodeAddonValue.AddOnValueID,   
      ZNodeAddonValue.Name,   
      ZNodeAddonValue.DisplayOrder,                
      ZNodeAddonValue.RetailPrice as RetailPrice,  
      ZNodeAddonValue.SalePrice as SalePrice,  
      ZNodeAddonValue.WholeSalePrice as WholesalePrice,  
      ZNodeAddonValue.DefaultInd as IsDefault,                
      QuantityOnHand = (SELECT QuantityOnHand FROM ZNodeAddOnValueInventory WHERE ZNodeAddOnValueInventory.SKU = ZNodeAddOnValue.SKU),  
      ZNodeAddonValue.[Weight] as [Weight],  
      ZNodeAddOnValue.SKU,          
      ZNodeAddOnValue.Height,
      ZNodeAddOnValue.Width,  
      ZNodeAddOnValue.[Length],  
      ZNodeAddOnValue.ShippingRuleTypeID,   
      ZNodeAddOnValue.FreeShippingInd,   
      ZNodeAddOnValue.SupplierID,    
      ZNodeAddOnValue.RecurringBillingInd,    
      ZNodeAddOnValue.RecurringBillingInstallmentInd,    
      ZNodeAddOnValue.RecurringBillingPeriod,    
      ZNodeAddOnValue.RecurringBillingFrequency,    
      ZNodeAddOnValue.RecurringBillingTotalCycles,    
      ZNodeAddOnValue.RecurringBillingInitialAmount,
      ZNodeAddOnValue.TaxClassID   
      FROM                
      ZNodeAddOnValue ZNodeAddOnValue
      WHERE                 
      ZNodeAddOnValue.AddOnvalueid in (select [value] from SC_Splitter(@AddOnValues,','))                               
      AND  ZNodeAddOnValue.AddOnId = ZNodeAddOn.AddOnId
      Order By                
      ZNodeAddonValue.DisplayOrder                
      FOR XML AUTO, TYPE, ELEMENTS                
     )                              
   FROM               
    ZNodeAddOn ZNodeAddOn              
    INNER JOIN ZNodeProductAddOn ZNodeProductAddOn ON ZNodeAddOn.AddOnId = ZNodeProductAddOn.AddOnId    
   WHERE              
    ZNodeProductAddOn.ProductId = @ProductId                    
    Order By ZNodeAddon.DisplayOrder asc FOR XML AUTO, TYPE, ELEMENTS                
END         
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetAffiliate]'
GO
-- =============================================                  
-- Create date: Oct 16,2008
-- Update date: April,12,2010
-- Description: Returns the Affialiate info        
-- =============================================            
ALTER PROCEDURE  [dbo].[ZNode_GetAffiliate]                                           
AS                  
BEGIN                  
	-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM                  
	-- INTERFERING WITH SELECT STATEMENTS.                  
	SET NOCOUNT ON;
	
	SELECT AccountId, CompanyName, BillingFirstName, BillingLastName from ZNodeAccount where ReferralStatus = 'N' or ReferralStatus = 'A';  
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetCategoryByID_XML]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetCategoryByID_XML]
-- Add the parameters for the stored procedure here
@CategoryId int = 0,
@PortalId   int = 0,
@LocaleId   int = 0
AS
  BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;

      SELECT ZNodeCategory.[CategoryID],
             ZNodeCategory.[Name],
             [Title],
             [ShortDescription],
             [Description],
             [ImageFile],
             [ImageAltTag],
             [VisibleInd],
             [SubCategoryGridVisibleInd],
             [SEOTitle],
             [SEOKeywords],
             [SEODescription],
             [AlternateDescription],
             ZNodeCategory.[DisplayOrder],
             [Custom1],
             [Custom2],
             [Custom3],
             [SEOURL],
             [dbo].[FN_GetCategoryPath](ZCN.CategoryNodeId) as 'CategoryPath',
             (SELECT ZNodeSubCategory.CategoryID,
                     ZNodeSubCategory.Name,
                     ZnodeSubCategoryNode.MasterPage,
                     ZnodeSubCategoryNode.Theme,
                     ZnodeSubCategoryNode.Css,
                     ZNodeSubCategory.Title,
                     ZNodeSubCategory.ImageFile,
                     ZNodeSubCategory.ShortDescription,
                     ZNodeSubCategory.SEOURL,
                     ZNodeSubCategory.ImageAltTag
              FROM
				     ZNodeCategory ZNodeSubCategory
                     INNER JOIN
                     ZNodeCategoryNode ZnodeSubCategoryNode
                     ON 
					 ZNodeSubCategory.CategoryId = ZnodeSubCategoryNode.CategoryId
			  WHERE ZNodeSubCategoryNode.ParentCategoryNodeID = ZCN.CategoryNodeId
                     AND ZNodeSubCategoryNode.ActiveInd = 1
              ORDER  BY ZNodeSubCategory.DisplayOrder asc
              FOR XML AUTO, TYPE, ELEMENTS),
             ZCN.MasterPage,
             ZCN.Theme,
             ZCN.Css
      FROM   ZNodeCategory ZNodeCategory
             INNER JOIN ZNodeCategoryNode ZCN ON ZNodeCategory.CategoryID = ZCN.CategoryID
			 INNER JOIN ZNodeCatalog ZCG ON ZCN.CatalogID = ZCG.CatalogID
			 INNER JOIN ZNodePortalCatalog ZPC ON ZCG.CatalogID = ZPC.CatalogID
      WHERE  ZNodeCategory.CategoryId = @CategoryId
             AND ZPC.PortalId = @PortalId
             AND ZPC.LocaleID = @LocaleId
      FOR XML Path('ZNodeCategory')
  END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetCategoryRootItems]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetCategoryRootItems]              
 -- Add the parameters for the stored procedure here              
 @PortalId int        
 ,@CatalogID int = 0
AS              
BEGIN              
	-- SET NOCOUNT ON added to prevent extra result sets from              
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
              
	SELECT               
		ZCN.CategoryNodeId,ZCN.CategoryId,ZC.[Name],VisibleInd, ImageFile, ShortDescription              
	FROM
		ZNodeCategory ZC INNER JOIN ZNodeCategoryNode ZCN ON ZC.CategoryID = ZCN.CategoryID
		INNER JOIN ZNodePortalCatalog ZPC ON ZCN.CatalogID = ZPC.CatalogID  
	WHERE
		ZCN.ParentCategoryNodeId IS NULL                 
		AND ZCN.ActiveInd = 1
		AND ZPC.PortalId= @PortalId 
		AND ZPC.CatalogID = @CatalogID
	Order by ZC.DisplayOrder
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetHomePageSpecials_XML]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetHomePageSpecials_XML]
(
 @PortalId INT = 0,
 @LocaleId INT = 0,
 @DisplayItem INT = 0
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT
		 ZNodeProduct.ProductID,
		 ZNodeProduct.[Name],
		 ZNodeProduct.ProductNum,
		 ZNodeProduct.RetailPrice,
		 ZNodeProduct.SalePrice,
		 ZNodeProduct.WholesalePrice,
		 ZNodeProduct.ShortDescription,
		 ZNodeProduct.CallForPricing,
		 ZNodeProduct.ImageFile,
		 ZNodeProduct.SEOURL,
		 ZNodeProduct.NewProductInd,
		 ZNodeProduct.FeaturedInd,
		 ZNodeProduct.DisplayOrder,
		 ZNodeProduct.ImageAltTag,
		 ZNodeProduct.TaxClassID,
		( SELECT    ReviewRating = ISNULL(AVG(Rating), 0),
                            TotalReviews = ISNULL(COUNT(ProductId), 0)
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.Status = 'A'
                FOR
                  XML PATH(''),
                      TYPE
                )
	FROM ProductsView ZNodeProduct
	
	WHERE ProductID IN (SELECT
							TOP (@DisplayItem) ZNodeProduct.ProductID
						FROM
							ProductsView ZNodeProduct 
							INNER JOIN ZNodeProductCategory ZPC ON ZNodeProduct.ProductID = ZPC.ProductID
							INNER JOIN ZNodeCategoryNode ZCN  ON ZPC.CategoryID = ZCN.CategoryID
							INNER JOIN ZNodePortalCatalog PC ON ZCN.CatalogID = PC.CatalogID
						WHERE
							ZNodeProduct.HomePageSpecial = 1
							AND ZCN.ActiveInd = 1
							AND ZPC.ActiveInd = 1
							AND PC.LocaleID = @LocaleId
							AND PC.PortalID = @PortalId
						GROUP BY 
							ZNodeProduct.ProductID, ZNodeProduct.DisplayOrder
						ORDER BY 
							ZNodeProduct.DisplayOrder)
	
	ORDER BY ZNodeProduct.DisplayOrder ASC
	
	FOR XML AUTO, TYPE, ELEMENTS
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetLocaleListByProductId]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetLocaleListByProductId]
(
@ProductNum NVarchar(1000),
@PortalID INT
)
AS

BEGIN

SELECT
	DISTINCT
	ZP.ProductID,
	ZL.LocaleID,
	ZL.LocaleDescription,
	ZPOC.PortalID
FROM 
ZNodeLocale ZL 
INNER JOIN ZNodePortalCatalog ZPOC ON ZL.LocaleID = ZPOC.LocaleID
INNER JOIN ZNodeCategoryNode ZCN ON ZPOC.CatalogID = ZCN.CatalogID
INNER JOIN ZNodeProductCategory ZPC ON ZCN.CategoryID = ZPC.CategoryID
INNER JOIN ProductsView ZP ON ZPC.ProductID = ZP.ProductID

WHERE

ZP.ProductNum = @ProductNum

AND ZPOC.PortalID = @PortalId

ORDER BY ZL.LocaleDescription

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetPagedProductListByPortalID]'
GO

  
ALTER PROCEDURE [dbo].[ZNode_GetPagedProductListByPortalID]                                    
 -- Add the parameters for the stored procedure here                                                   
 @PortalId int = 0,                                
 @Name varchar(1000) = NULL,                                
 @ProductNum varchar(1000) = NULL,                                
 @Sku varchar(1000) = NULL,                   
 @Brand varchar(1000) = Null,                  
 @Category varchar(1000) = Null,                       
 @PageIndex int = 0,                        
 @PageSize int = 0,         
 @CatalogId int = 0,               
 @TotalPages int OUTPUT                      
AS                                   
/*          
[ZNode_GetPagedProductListByPortalID] @PortalId = 1, @PageSize=5, @TotalPages=10,@Name='b',@PageIndex=1            
*/          
                    
BEGIN                                                  
   -- SET NOCOUNT ON added to prevent extra result sets from                                                  
   -- interfering with SELECT statements.                                                  
   SET NOCOUNT ON;                    
                    
   -- SQL Server 2005 Paging                       
   DECLARE @PageLowerBound INT                        
   DECLARE @PageUpperBound INT                     
   DECLARE @ProductCount INT                       
                       
   SET @ProductCount = 0                    
   SET @TotalPages = 0                        
                        
   -- Set the page bounds                        
   SET @PageLowerBound = @PageSize * @PageIndex;                        
   SET @PageUpperBound = @PageLowerBound + @PageSize;
                              
   IF @PageIndex > 0                        
   BEGIN                                         
   -- Select Distinct Products  
   SELECT        
		ZP.ProductID        
   INTO #TempProductList        
   FROM ProductsView ZP 
		INNER JOIN ZNodeProductCategory ZPC         
     ON ZPC.ProductID = ZP.ProductID AND ZPC.ActiveInd = 1           
		INNER JOIN ZNodeCategory ZC         
     ON ZC.CategoryId = ZPC.CategoryId AND (ZC.Name LIKE '%' + @Category + '%' OR @Category IS NULL) AND ZC.VisibleInd = 1             
		INNER JOIN ZNodeCategoryNode ZCN        
     ON ZC.CategoryID = ZCN.CategoryID AND ZCN.ActiveInd = 1 AND ZCN.CatalogID=@CatalogId
	WHERE 
		ZP.CallForPricing = 0 AND ZP.ActiveInd = 1 AND 
		(ZP.Name LIKE '%' + @NAME + '%' OR  @NAME IS NULL) AND 
		(ZP.ProductNum LIKE '%' + @ProductNum + '%' OR  @ProductNum IS NULL) AND 
		(ZP.SKU LIKE  '%' + @Sku + '%' OR  @Sku IS NULL) AND 
		(ZP.ManufacturerID IN (SELECT ManufacturerID from ZNodeManufacturer WHERE Name LIKE '%' + @Brand + '%') 
		OR @Brand IS NULL OR LEN(@Brand)= 0)
		         
       
   -- Add Description and Item Index.  
   SELECT ZP.Name        
    ,ZP.ProductID        
    ,@PageSize + ROW_NUMBER() OVER (ORDER BY ZP.Name, ZP.ProductNum) as RowIndex        
    ,ZP.ProductNum        
    ,ZP.Sku  
    ,ZP.ShortDescription        
    ,ZP.RetailPrice      
   INTO #ProductList  
   FROM ProductsView ZP
   WHERE ProductID IN (SELECT ProductID FROM #TempProductList)
                                                          
   SELECT TOP (@PageUpperBound) * FROM #ProductList WHERE RowIndex > @PageLowerBound AND RowIndex <= @PageUpperBound ORDER BY Name, ProductNum                           
                         
   IF (@PageSize > 0)               
   BEGIN                        
   SET @ProductCount = (SELECT  COUNT(*) FROM #ProductList)                    
                     
   SET @TotalPages =(                    
    CASE  WHEN @ProductCount = 0 THEN 0                    
    WHEN @ProductCount % @PageSize != 0 THEN (@ProductCount / @PageSize) + 1                    
    WHEN @ProductCount / @PageSize = 0 THEN 0                    
    ELSE @ProductCount / @PageSize                    
 END)                    
   END                    
                     
   -- Total pages  
   SELECT @TotalPages 'Total Pages'
   END                    
   ELSE                    
 BEGIN                    
   SELECT TOP 0 Name,ProductID , ProductNum, Sku, ShortDescription, RetailPrice FROM ZNodeProduct                    
 END                 
END   
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductsByKeyword_Helper]'
GO


ALTER PROCEDURE [dbo].[ZNode_GetProductsByKeyword_Helper]         
 -- Add the parameters for the stored procedure here        
    @PortalId int = 0,
    @CatalogId int = null,
    @Keyword varchar(4000) = ''
AS 
    BEGIN        
	-- SET NOCOUNT ON added to prevent extra result sets from        
	-- EXEC ZNode_GetProductsByKeyword_Helper 1,81, 'apple'
	-- interfering with SELECT statements.        
        SET NOCOUNT ON ;
   
	-- Insert statements for procedure here        
        SELECT  pr.ProductID
        FROM    ProductsView pr
                INNER JOIN ZNodeProductCategory PC ON pr.ProductID = PC.ProductID
                INNER JOIN ZNodeCategoryNode ZCN ON PC.CategoryID = ZCN.CategoryID
                LEFT OUTER JOIN ZNodeManufacturer m ON m.ManufacturerID = pr.ManufacturerID
                LEFT OUTER JOIN ZNodeProductType t ON t.ProductTypeId = pr.ProductTypeID
        WHERE   EXISTS ( SELECT Value
                         FROM   SC_Splitter(@Keyword, ' ')
                         WHERE  PATINDEX('%' + Value + '%', Pr.Name) > 0
                                OR PATINDEX('%' + Value + '%', pr.SEOTitle) > 0
                                OR PATINDEX('%' + Value + '%',
                                            pr.SEODescription) > 0
                                OR PATINDEX('%' + Value + '%', pr.ProductNum) > 0
                                OR PATINDEX('%' + Value + '%', m.Name) > 0
                                OR PATINDEX('%' + Value + '%', t.Name) > 0 )
                AND ZCN.CatalogID = @CatalogId 
	
    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetSEOUrlList]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetSEOUrlList]               
 @PortalID int = 0,  
 @LocaleId int = 0           
AS              
BEGIN
	         
	-- SET NOCOUNT ON added to prevent extra result sets from              
	-- interfering with SELECT statements.              
	SET NOCOUNT ON;              
    
    DECLARE @CATEGORYLIST TABLE
    (
		CategoryID INT,
		SeoURL NVARCHAR(MAX),
        PRIMARY KEY CLUSTERED ( [CategoryID] ASC ) WITH (IGNORE_DUP_KEY = ON) ON [PRIMARY]
	) ;

	INSERT INTO @CATEGORYLIST (CategoryID, SeoURL)
	SELECT
		ZC.CategoryID,
		ZC.SeoURL
	FROM
		ZNodeCategory ZC
		INNER JOIN ZNodeCategoryNode ZCN  ON ZC.CategoryID = ZCN.CategoryID
		INNER JOIN ZNodePortalCatalog PC ON ZCN.CatalogID = PC.CatalogID
	WHERE
		ZCN.ActiveInd = 1		
		AND PC.LocaleID = @LocaleId
		AND PC.PortalID = @PortalId
		AND ZC.SEOURL IS NOT NULL
		AND LEN(ZC.SEOURL) > 0

	-- PRODUCT
	SELECT DISTINCT(PV.ProductId),SeoURL 
	FROM 
			ProductsView PV
			INNER JOIN ZNodeProductCategory ZPC ON PV.ProductID = ZPC.ProductID
	WHERE	
			CategoryID IN (SELECT CategoryID from @CATEGORYLIST)
			AND PV.SeoURL IS NOT NULL
			AND LEN(PV.SEOURL) > 0
			AND PV.ActiveInd = 1
			AND ZPC.ActiveInd = 1

	-- CATEGORY    
	SELECT  CategoryID,SeoURL FROM @CATEGORYLIST

	-- CONTENT    
	SELECT DISTINCT(ContentPageID),Name,SeoURL FROM ZNodeContentPage 
	WHERE 
	SeoURL IS NOT NULL
	AND LEN(SEOURL) > 0
	AND ActiveInd = 1 
	AND PortalID = @PortalID 
	AND LocaleID = @LocaleId   

	-- ZNodeUrlRedirect   
	SELECT UrlRedirectId, OldUrl, NewUrl FROM ZNodeUrlRedirect WHERE IsActive = 1      
END    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetSkuAttributes_Attributes]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetSkuAttributes_Attributes]
(@PRODUCTID INT,
@ATTRIBUTES VARCHAR(MAX))
AS        
	BEGIN         
	-- SET NOCOUNT ON added to prevent extra result sets from          
	-- interfering with SELECT statements.          
	SET NOCOUNT ON;
	
	SELECT 
		SKUID,
		prod.ProductID,
		prod.Name AS 'ProductName',
		sku.SKU,
		QuantityOnHand,
		ReorderLevel,
		SKUPicturePath,
		sku.DisplayOrder, 
		RetailPriceOverride,
		SalePriceOverride, 
		WholesalePriceOverride,
		sku.ActiveInd 
   FROM 
   ZNodeProduct prod 
   INNER JOIN ZNODESKU sku ON sku.ProductID = prod.ProductID    
   INNER JOIN ZNodeSkuInventory ON sku.SKU = ZNodeSkuInventory.SKU   
   WHERE SKUID IN 
   (SELECT skuid from ZNodeSKUattribute WHERE AttributeId IN (SELECT [VALUE] FROM SC_Splitter(@Attributes,',')) 
	GROUP BY SKUID Having Count(1) = (SELECT COUNT(1) FROM SC_Splitter(@ATTRIBUTES,',')))
	AND sku.ProductID = @PRODUCTID AND prod.ProductID = @PRODUCTID;
END
    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetSKUByAttributes_XML]'
GO
-- =============================================              
-- Create date: July 12, 2006        
-- Update date: April 19, 2010       
-- Description:           
-- =============================================          
ALTER PROCEDURE [dbo].[ZNode_GetSKUByAttributes_XML]           
 -- Add the parameters for the stored procedure here          
 @ProductId int = 0,           
 @Attributes varchar(8000)
AS       
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE  @ValueList NVARCHAR(MAX);
   
	SET @ValueList = '';
   
	SELECT @ValueList = @ValueList + attributeType.Name +  ' - ' + attrib.Name + '<br />'
	FROM 
	ZNodeAttributeType attributeType INNER JOIN 
	ZNodeProductAttribute attrib ON attributeType.AttributeTypeId = attrib.AttributeTypeId
	INNER JOIN ZNodeSkuAttribute skuAttrib on attrib.AttributeId = skuAttrib.AttributeId	
	WHERE attrib.attributeid IN (SELECT [VALUE] FROM SC_Splitter(@Attributes,','))
      
	SELECT 		
		[SKUID]
		,[ProductID]
		,[SKU]
		,[SupplierID]
		,[Note]
		,[WeightAdditional]
		,[SKUPicturePath]
		,[ImageAltTag]
		,[DisplayOrder]
		,[RetailPriceOverride]
		,[SalePriceOverride]
		,[WholesalePriceOverride]
		,[RecurringBillingPeriod]
		,[RecurringBillingFrequency]
		,[RecurringBillingTotalCycles]
		,[RecurringBillingInitialAmount]
		,[ActiveInd]
		,QuantityOnHand = (SELECT QuantityOnHand from ZNodeSKUInventory Inven WHERE Inven.SKU = ZNodeSKU.SKU)   
		,ReOrderLevel = (SELECT ReOrderLevel from ZNodeSKUInventory Inven WHERE Inven.SKU = ZNodeSKU.SKU)
		,@ValueList AttributesDescription
	FROM ZNodeSKU WHERE productid = @ProductID
	AND SKUID IN
	(SELECT skuid from ZNodeSKUattribute WHERE AttributeId IN (SELECT [VALUE] FROM SC_Splitter(@Attributes,',')) 
	GROUP BY SKUID Having Count(1) = (SELECT COUNT(1) FROM SC_Splitter(@Attributes,','))
	) FOR XML AUTO, ELEMENTS
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetSkuPricingListByFilter]'
GO

-- =============================================               
-- Create date: August 30, 2008  
-- Description: Returns list of products, attributes,addons with retailprice,sale price,wholesalprice  
-- =============================================          
ALTER PROCEDURE [dbo].[ZNode_GetSkuPricingListByFilter](@ProductType Int = Null)       
AS          
 BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;      
   
	SELECT  ProductID,
			NULL SKUId,
			NULL AddOnvalueId,
			Name,
			ProductNum,
			Sku,
			RetailPrice,
			SalePrice,
			WholeSalePrice
	FROM    ZNodeProduct Product
	WHERE   ProductId NOT IN (SELECT   ProductID
							   FROM     ZNodeSKU )
			AND ( @ProductType = 0
				  OR @ProductType = 1)        

	UNION
	
	SELECT  NULL ProductID,
			SkuID,
			NULL AddOnvalueId,
			NULL Name,
			NULL ProductNum,
			Sku,
			RetailPriceOverride,
			SalePriceOverride,
			WholeSalePriceOverride
	FROM    ZNodeSKU
	WHERE   (@ProductType = 0
			  OR @ProductType = 2)        

	UNION
	
	SELECT  NULL ProductID,
			NULL SKUId,
			AddOnvalueId,
			Name,
			NULL ProductNum,
			Sku,
			RetailPrice,
			SalePrice,
			WholeSalePrice
	FROM    ZNodeAddOnvalue
	WHERE   ( @ProductType = 0
			  OR @ProductType = 3)        
      
 END    

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCategory_Insert]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeCategory table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCategory_Insert
(

	@CategoryID int    OUTPUT,

	@Name nvarchar (MAX)  ,

	@Title nvarchar (MAX)  ,

	@ShortDescription nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@ImageFile nvarchar (MAX)  ,

	@ImageAltTag nvarchar (MAX)  ,

	@VisibleInd bit   ,

	@SubCategoryGridVisibleInd bit   ,

	@SEOTitle nvarchar (MAX)  ,

	@SEOKeywords nvarchar (MAX)  ,

	@SEODescription nvarchar (MAX)  ,

	@AlternateDescription nvarchar (MAX)  ,

	@DisplayOrder int   ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@SEOURL nvarchar (100)  
)
AS


				
				INSERT INTO [dbo].[ZNodeCategory]
					(
					[Name]
					,[Title]
					,[ShortDescription]
					,[Description]
					,[ImageFile]
					,[ImageAltTag]
					,[VisibleInd]
					,[SubCategoryGridVisibleInd]
					,[SEOTitle]
					,[SEOKeywords]
					,[SEODescription]
					,[AlternateDescription]
					,[DisplayOrder]
					,[Custom1]
					,[Custom2]
					,[Custom3]
					,[SEOURL]
					)
				VALUES
					(
					@Name
					,@Title
					,@ShortDescription
					,@Description
					,@ImageFile
					,@ImageAltTag
					,@VisibleInd
					,@SubCategoryGridVisibleInd
					,@SEOTitle
					,@SEOKeywords
					,@SEODescription
					,@AlternateDescription
					,@DisplayOrder
					,@Custom1
					,@Custom2
					,@Custom3
					,@SEOURL
					)
				
				-- Get the identity value
				SET @CategoryID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsMain]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ZNode_ReportsMain]
(                                      
       @FinalDate DATETIME,
	   @CompareFinalDate DATETIME            
)                                      
AS                                      
BEGIN                                      
SET NOCOUNT ON;  

SELECT DISTINCT
	  O.[OrderID]
      ,O.[PortalId]
      ,O.[AccountID]
      ,O.[OrderStateID]
      ,O.[ShippingID]
      ,O.[PaymentTypeId]
      ,O.[ShipFirstName]
      ,O.[ShipLastName]
      ,O.[ShipCompanyName]
      ,O.[ShipStreet]
      ,O.[ShipStreet1]
      ,O.[ShipCity]
      ,O.[ShipStateCode]
      ,O.[ShipPostalCode]
      ,O.[ShipCountry]
      ,O.[ShipPhoneNumber]
      ,O.[ShipEmailID]
      ,O.[BillingFirstName]
      ,O.[BillingLastName]
      ,O.[BillingCompanyName]
      ,O.[BillingStreet]
      ,O.[BillingStreet1]
      ,O.[BillingCity]
      ,O.[BillingStateCode]
      ,O.[BillingPostalCode]
      ,O.[BillingCountry]
      ,O.[BillingPhoneNumber]
      ,O.[BillingEmailId]
      ,O.[CardTransactionID]
      ,O.[CardAuthCode]
      ,O.[CardTypeId]
      ,O.[CardExp]
      ,O.[TaxCost]
      ,O.[ShippingCost]
      ,O.[SubTotal]
      ,O.[DiscountAmount]
      ,O.[Total]
      ,O.[OrderDate]
      ,O.[Custom1]
      ,O.[Custom2]
      ,O.[AdditionalInstructions]
      ,O.[Custom3]
      ,O.[TrackingNumber]
      ,O.[CouponCode]
      ,O.[PromoDescription]
      ,O.[ReferralAccountID]
      ,O.[PurchaseOrderNumber]
      ,O.[PaymentStatusID]
      ,O.[WebServiceDownloadDate]
      ,O.[PaymentSettingID]
      ,O.[ShipDate]
      ,O.[ReturnDate]
      ,O.[SalesTax]
      ,O.[VAT]
      ,O.[GST]
      ,O.[PST]
      ,O.[HST]
	  ,P.[StoreName] AS 'StoreName'
	  ,OS.[OrderStateName] AS 'OrderStatus'
	  ,PT.[Name] AS 'PaymentTypeName'
	  ,ST.[Name] AS 'ShippingTypeName'
FROM 
	ZNodeOrder O
INNER JOIN
	ZNodePortal P
ON
	P.PortalID = O.PortalId
INNER JOIN
	ZNodeOrderState OS
ON
	OS.OrderStateID = O.OrderStateID 
INNER JOIN
	ZNodeShipping S
ON
	S.ShippingID = O.ShippingID
INNER JOIN
	ZNodeShippingType ST
ON
	ST.ShippingTypeID = S.ShippingTypeID 
INNER JOIN
	ZNodePaymentType PT
ON
	PT.PaymentTypeID = O.PaymentTypeId 
INNER JOIN
	ZNodeOrderLineItem OL
ON
	OL.OrderID = O.OrderID
WHERE
	O.OrderDate >= @FinalDate 
AND
	O.OrderDate <= @CompareFinalDate          
                                
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortalCatalog_GetByLocaleID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortalCatalog table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortalCatalog_GetByLocaleID
(

	@LocaleID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalCatalogID],
					[PortalID],
					[CatalogID],
					[Theme],
					[CSS],
					[LocaleID]
				FROM
					[dbo].[ZNodePortalCatalog]
				WHERE
					[LocaleID] = @LocaleID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_Search_StoreLocator]'
GO
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[ZNode_Search_StoreLocator]
(@ZIPCODE NVARCHAR(30)= NULL,        
 @INRADIUS FLOAT,        
 @CITY NVARCHAR(MAX) = NULL,        
 @STATE NVARCHAR(MAX) = NULL,        
 @AREACODE NVARCHAR(MAX) = NULL,  
 @PORTALID INT)              
AS            
BEGIN            
 --GLOBAL DECLARATION FOR RADIUS OUTPUT VALUES            
   DECLARE @BOTTOMLINE FLOAT;            
   DECLARE @TOPLINE FLOAT;            
   DECLARE @RIGHTLINE FLOAT;            
   DECLARE @LEFTLINE FLOAT;          
         
 --RADIUS CALCULATION.        
 IF(@INRADIUS > 0 and LEN(@ZIPCODE) != 0)        
 BEGIN         
   -- DECLARE LONGITUDE AND LATITUDE VARIABLES.            
   DECLARE @LONGITUDE FLOAT;            
   DECLARE @LATITUDE FLOAT;            
               
   -- RETRIEVE THE LONGITUDE AND LATITUDE FOR ZIPCODE.            
   SET @LONGITUDE = (SELECT DISTINCT(LONGITUDE) FROM ZNODEZIPCODE WHERE ZIP = @ZIPCODE);            
   SET @LATITUDE = (SELECT DISTINCT(LATITUDE) FROM ZNODEZIPCODE WHERE ZIP = @ZIPCODE);             
               
   -- DEFAULT VALUES            
   DECLARE @EARTHRADIUSMILES FLOAT;            
              
   -- CONST EARTHRADIUS MILES VALUE            
   SET @EARTHRADIUSMILES = 3963.1676;             
               
   -- CALCULATION VARIABLES.            
   DECLARE @LATITUDEINRADIUS FLOAT;            
   DECLARE @LONGITUDEINRADIUS FLOAT;            
   DECLARE @DISTANCEINRADIUS FLOAT;            
   DECLARE @LAT FLOAT;            
   DECLARE @DLLON FLOAT;            
   DECLARE @DRLON FLOAT;            
              
               
   -- MODULOS CALCULATION VARIABLES            
   DECLARE @RVAR1 FLOAT;            
   DECLARE @RVAR2 FLOAT;            
   DECLARE @RVAR3 INT;             
   DECLARE @RLINE FLOAT;            
               
   DECLARE @LVAR1 FLOAT;            
   DECLARE @LVAR2 FLOAT;            
   DECLARE @LVAR3 INT;             
   DECLARE @LLINE FLOAT;         
        
   -- CALCULATION HERE            
   SET @LATITUDEINRADIUS = @LATITUDE * (PI()/180.0);            
   SET @LONGITUDEINRADIUS = @LONGITUDE * (PI()/180.0);            
              
   SET @DISTANCEINRADIUS = @INRADIUS/@EARTHRADIUSMILES;            
              
   SET @TOPLINE = (@LATITUDEINRADIUS + @DISTANCEINRADIUS) * (180.0/PI());            
   SET @BOTTOMLINE = (@LATITUDEINRADIUS - @DISTANCEINRADIUS) * (180.0/PI());            
              
   SET @LAT = ASIN (SIN (@LATITUDEINRADIUS) * COS (@DISTANCEINRADIUS));            
   SET @DRLON = ATN2 (SIN(PI() / 2.0) * SIN (@DISTANCEINRADIUS) * COS (@LATITUDEINRADIUS), COS (@DISTANCEINRADIUS) - SIN (@LATITUDEINRADIUS) * SIN (@LAT));            
              
   -- RIGHT LINE CALCULATION            
   SET @RVAR1 = (@LONGITUDEINRADIUS + @DRLON + PI());             
   SET @RVAR2 = (2.0 * PI());            
   SET @RVAR3 = (@RVAR1 / @RVAR2);            
   SET @RLINE = (@RVAR1 - (@RVAR3) * @RVAR2) - PI();            
   SET @RIGHTLINE = @RLINE * (180.0 / PI());            
              
   -- LEFT LINE CALCULATION            
   SET @DLLON = ATN2 (SIN (3.0 * PI() / 2.0) * SIN (@DISTANCEINRADIUS) * COS (@LATITUDEINRADIUS), COS (@DISTANCEINRADIUS) - SIN (@LATITUDEINRADIUS)* SIN (@LAT));            
   SET @LVAR1 = (@LONGITUDEINRADIUS + @DLLON + PI());             
   SET @LVAR2 = (2.0 * PI());            
   SET @LVAR3 = (@LVAR1 / @LVAR2);            
   SET @LLINE = (@LVAR1 - (@LVAR3) * @LVAR2) - PI();            
   SET @LEFTLINE = @LLINE * (180.0 / PI());           
 END        
 -- RADIUS CALCULATION ENDS.
        
	-- BUILD A DYNAMIC QUERY.         
	SET NOCOUNT ON; 
 
	SELECT             
		  DISTINCT (ZNodeStore.StoreID),            
		  ZNodeStore.Name,           
		  ZNodeStore.ContactName,          
		  ZNodeStore.Address1,           
		  ZNodeStore.Address2,          
		  ZNodeStore.Address3,          
		  ZNodeStore.City,            
		  ZNodeStore.State,            
		  ZNodeStore.Zip,          
		  ZNodeStore.Phone,          
		  ZNodeStore.Fax,            
		  ZNodeStore.AccountID,               
		  CASE WHEN ZNodeStore.DisplayOrder IS NULL THEN 99999 ELSE ZNodeStore.DisplayOrder END AS 'DisplayOrder2',        
		  ZNodeStore.ActiveInd,          
		  ZNodeStore.ImageFile,        
		  ZNODEZIPCODE.AREACODE,          
		  ZNODEZIPCODE.LATITUDE AS 'Latitude',
		  ZNODEZIPCODE.LONGITUDE AS 'Longitude',
			CASE 
				WHEN LEN(@ZIPCODE) != 0 THEN (SELECT dbo.FN_DistanceCalculatorByZipcode(@ZIPCODE ,ZNodeStore.Zip))
				ELSE 0 
			END AS 'Distance'
			FROM ZNodeStore INNER JOIN ZNodeZipCode ON ZNodeStore.Zip = ZNODEZIPCODE.ZIP
			WHERE ZNodeStore.ActiveInd = 1 AND ZNodeStore.PortalId = @PORTALID	
			AND ((ZNODEZIPCODE.LATITUDE >= @BOTTOMLINE) OR (@INRADIUS = 0 and LEN(@ZIPCODE) > 0))
			AND ((ZNODEZIPCODE.LATITUDE <= @TOPLINE) OR (@INRADIUS = 0 and LEN(@ZIPCODE) > 0))
			AND ((ZNODEZIPCODE.LONGITUDE >= @LEFTLINE) OR (@INRADIUS = 0 and LEN(@ZIPCODE) > 0))
			AND ((ZNODEZIPCODE.LONGITUDE <= @RIGHTLINE) OR (@INRADIUS = 0 and LEN(@ZIPCODE) > 0))
			AND ((ZNODEZIPCODE.ZIP = @ZIPCODE) OR LEN(@ZIPCODE) = 0 OR @INRADIUS > 0)
			AND ((ZNODEZIPCODE.STATEABBR LIKE @STATE + '%') OR LEN(@STATE) = 0)	
			AND ((ZNODEZIPCODE.CITYNAME LIKE @CITY + '%') OR LEN(@CITY) = 0)
			AND ((ZNODEZIPCODE.AREACODE LIKE @AREACODE + '%') OR LEN(@AREACODE) = 0) 
			ORDER BY DisplayOrder2,Distance FOR XML AUTO, TYPE, ELEMENTS
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchAffiliate]'
GO
-- =============================================                  
-- Create date: Oct 16,2008      
-- Create date: April 20,2010
-- Description: Returns the Affialiate info        
-- =============================================            
ALTER PROCEDURE  [dbo].[ZNode_SearchAffiliate]                   
(                  
 -- ADD THE PARAMETERS FOR THE STORED PROCEDURE HERE             
 @FirstName VARCHAR(MAX) = NULL,              
 @LastName VARCHAR(MAX) = NULL,              
 @CompanyName VARCHAR(MAX) = NULL,              
 @AccountId VARCHAR(100) = NULL,    
 @Status VARCHAR(MAX) = NULL      
)
AS                  
BEGIN                  
	-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM                  
	-- INTERFERING WITH SELECT STATEMENTS.                  
	SET NOCOUNT ON;
	
	SELECT AccountId, CompanyName, BillingFirstName, BillingLastName from ZNodeAccount WHERE	
		(ReferralStatus = 'N' or ReferralStatus = 'A') AND 
		((BillingFirstName LIKE '%' + @FirstName + '%') OR LEN(@FirstName) = 0) AND
		((BillingLastName LIKE '%' + @LastName + '%') OR LEN(@LastName) = 0) AND 
		((CompanyName LIKE '%' + @CompanyName + '%') OR (BillingCompanyName LIKE '%' + 
			@CompanyName + '%') OR (ShipCompanyName LIKE '%' + @CompanyName + '%') OR LEN(@CompanyName) = 0) AND
		(ReferralStatus LIKE '%' + @Status + '%' OR LEN(@Status) = 0) AND
		((AccountId LIKE '%' + @AccountId + '%') OR LEN(@AccountId) = 0) 
	ORDER BY [AccountID] DESC
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchCase]'
GO

ALTER PROCEDURE [dbo].[ZNode_SearchCase]
    (
      @CASEID VARCHAR(100),
      @FIRSTNAME VARCHAR(100),
      @LASTNAME VARCHAR(100),
      @COMPANYNAME VARCHAR(100),
      @TITLE VARCHAR(100),
      @CASESTATUS INT = 0,
      @PORTALID int = 0,
      @portalIds VARCHAR(MAX) = '0'
    )
AS 
    BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
        SET NOCOUNT ON ;        
       
        DECLARE @CASESTATUSID VARCHAR(100) ;   
        DECLARE @PORTAL VARCHAR(100) ;   
        IF ( @PORTALID = 0 ) 
            BEGIN    
                SET @PORTAL = '%' ;       
            END    
        ELSE 
            BEGIN    
                SET @PORTAL = @PORTALID ;    
            END     
  
        IF ( @CASESTATUS = -1 ) 
            BEGIN    
                SET @CASESTATUSID = '%' ;       
            END    
        ELSE 
            BEGIN    
                SET @CASESTATUSID = @CASESTATUS ;    
            END    
    
     
     
    -- Insert statements for procedure here    
        SELECT  R.[CaseID],
                P.StoreName,
                R.[AccountID],
                R.[OwnerAccountID],
                S.CaseStatusNme,
                Py.CasePriorityNme,
                R.[CaseTypeID],
                R.[CaseOrigin],
                R.[Title],
                R.[Description],
                R.[FirstName],
                R.[LastName],
                R.[CompanyName],
                R.[EmailID],
                R.[PhoneNumber],
                R.[CreateDte],
                R.[CreateUser]
        FROM    
			ZNodeCaseRequest  R,
			ZNodePortal P,
			ZNodeCasePriority Py,
			ZNodeCaseStatus S
        WHERE   
			 R.PortalID=P.PortalID AND
			R.CasePriorityID=Py.CasePriorityID AND
			R.CaseStatusID=S.CaseStatusID AND
			( R.PortalID in ( SELECT  [value] from    sc_splitter(@portalIds, ',') ) OR @portalIds = '0' )
                AND TITLE LIKE '%' + @TITLE + '%'
                AND FIRSTNAME LIKE '%' + @FIRSTNAME + '%'
                AND LASTNAME LIKE '%' + @LASTNAME + '%'
                AND CASEID LIKE '%' + @CASEID + '%'
                AND R.CompanyName LIKE '%' + @COMPANYNAME + '%'
                AND ( R.CaseStatusID like @CASESTATUSID )
                AND ( R.PortalID like @PORTAL )       
    END    

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchCustomer]'
GO

ALTER PROCEDURE [dbo].[ZNode_SearchCustomer]  
(          
-- ADD THE PARAMETERS FOR THE STORED PROCEDURE HERE          
@FIRSTNAME VARCHAR(MAX) = '',          
@LASTNAME VARCHAR(MAX) = '',          
@COMPANYNAME VARCHAR(MAX) = '',          
@PROFILEID int = 0,          
@STARTDATE DATETIME = NULL,          
@ENDDATE DATETIME = NULL,          
@LOGINNAME VARCHAR(MAX) = '',          
@ACCOUNTID VARCHAR(MAX) = '',          
@EXTERNALACCOUNTNUM VARCHAR(MAX) = '',          
@PHONENUM VARCHAR(MAX) = '',          
@EMAILID VARCHAR(MAX) = '',          
@REFERRALSTATUS VARCHAR(MAX) = '',          
@PORTALID INT=0         
)          
AS          
BEGIN          
	-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM          
	-- INTERFERING WITH SELECT STATEMENTS.          
	SET NOCOUNT ON;    
	IF(LEN(@STARTDATE) = 0 OR @STARTDATE IS NULL)        
	BEGIN        
		SET @STARTDATE = (SELECT Min([CREATEDTE]) FROM ZNODEACCOUNT)        
	END
	IF(LEN(@ENDDATE) = 0 OR @ENDDATE IS NULL OR @ENDDATE = '1/1/1900')        
	BEGIN        
		SET @ENDDATE = (SELECT Max([CREATEDTE]) FROM ZNODEACCOUNT)        
	END
	
	CREATE TABLE #tempAccount (AccountID INT);

	IF (@PROFILEID > 0 AND @PORTALID = 0) 
	BEGIN
			INSERT INTO #tempAccount (AccountID)
			(SELECT ACCOUNTID 			
			FROM ZNODEACCOUNTPROFILE 
			WHERE ProfileID = @PROFILEID)
	END
	ELSE IF (@PORTALID > 0 AND @PROFILEID = 0) 
	BEGIN
			INSERT INTO #tempAccount (AccountID)
			(SELECT ACCOUNTID 
			FROM ZNODEACCOUNTPROFILE ZAP
			INNER JOIN ZNODEPORTALPROFILE ZPP ON ZAP.ProfileID = ZPP .ProfileID
			WHERE ZPP.PORTALID = @PORTALID)
	END
    ELSE IF (@PORTALID > 0 AND @PROFILEID > 0) 
    BEGIN
			INSERT INTO #tempAccount (AccountID)
			(SELECT ACCOUNTID
			FROM ZNODEACCOUNTPROFILE ZAP
			INNER JOIN ZNODEPORTALPROFILE ZPP ON ZAP.ProfileID = ZPP .ProfileID
			WHERE  ZPP.PORTALID = @PORTALID AND ZPP.PROFILEID = @PROFILEID)
	END
	ELSE
	BEGIN
			INSERT INTO #tempAccount (AccountID)
			(SELECT ACCOUNTID
				FROM ZNODEACCOUNT)
	END
      
       
	SELECT ZA.[ACCOUNTID]
			,[USERID]                  
			,[BILLINGFIRSTNAME]          
			,[BILLINGLASTNAME]                  
			,[BILLINGPHONENUMBER]          
			,[BILLINGEMAILID]          			
			FROM ZNODEACCOUNT ZA 
	WHERE  
		EXISTS(SELECT AccountID FROM #tempAccount TA WHERE TA.AccountID = ZA.AccountID)		
		AND ((BillingFirstName LIKE '%' + @FirstName + '%') OR LEN(@FirstName) = 0) 
		AND	((BillingLastName LIKE '%' + @LastName + '%') OR LEN(@LastName) = 0) 
		AND ((CompanyName LIKE '%' + @CompanyName + '%') OR (BillingCompanyName LIKE '%' + @CompanyName + '%') 
			OR (ShipCompanyName LIKE '%' + @CompanyName + '%') OR LEN(@CompanyName) = 0) 
		AND (ZA.CreateDte BETWEEN @STARTDATE AND @ENDDATE)
		AND (ZA.UserID IN (SELECT UserID FROM Aspnet_Users WHERE UserName LIKE @LOGINNAME + '%') OR LEN(@LOGINNAME)= 0)
		AND	(ZA.AccountID LIKE @AccountID + '%' OR LEN(@ACCOUNTID) = 0)
		AND	(ZA.ExternalAccountNo LIKE @EXTERNALACCOUNTNUM + '%' OR LEN(@EXTERNALACCOUNTNUM) = 0)
		AND (ZA.BillingPhoneNumber LIKE @PHONENUM + '%' OR LEN(@PHONENUM) = 0
				OR ZA.ShipPhoneNumber LIKE @PHONENUM + '%')
		AND	(ZA.BillingEmailID LIKE @EMAILID + '%' OR LEN(@EMAILID) = 0	OR ZA.ShipEmailID LIKE @EMAILID + '%')
		AND	(ZA.ReferralStatus LIKE @REFERRALSTATUS + '%' OR LEN(@REFERRALSTATUS) = 0)
	
	ORDER BY [ACCOUNTID] DESC; 
   
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCategory_Update]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeCategory table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCategory_Update
(

	@CategoryID int   ,

	@Name nvarchar (MAX)  ,

	@Title nvarchar (MAX)  ,

	@ShortDescription nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@ImageFile nvarchar (MAX)  ,

	@ImageAltTag nvarchar (MAX)  ,

	@VisibleInd bit   ,

	@SubCategoryGridVisibleInd bit   ,

	@SEOTitle nvarchar (MAX)  ,

	@SEOKeywords nvarchar (MAX)  ,

	@SEODescription nvarchar (MAX)  ,

	@AlternateDescription nvarchar (MAX)  ,

	@DisplayOrder int   ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@SEOURL nvarchar (100)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeCategory]
				SET
					[Name] = @Name
					,[Title] = @Title
					,[ShortDescription] = @ShortDescription
					,[Description] = @Description
					,[ImageFile] = @ImageFile
					,[ImageAltTag] = @ImageAltTag
					,[VisibleInd] = @VisibleInd
					,[SubCategoryGridVisibleInd] = @SubCategoryGridVisibleInd
					,[SEOTitle] = @SEOTitle
					,[SEOKeywords] = @SEOKeywords
					,[SEODescription] = @SEODescription
					,[AlternateDescription] = @AlternateDescription
					,[DisplayOrder] = @DisplayOrder
					,[Custom1] = @Custom1
					,[Custom2] = @Custom2
					,[Custom3] = @Custom3
					,[SEOURL] = @SEOURL
				WHERE
[CategoryID] = @CategoryID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_ReportsCustom]'
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[ZNode_ReportsCustom]
    @FromDate DateTime = NULL,
    @ToDate DateTime = NULL,
    @PortalId VARCHAR(10) = '',
    @OrderStateId VARCHAR(10) = ''
AS 
    BEGIN                                      
        SET NOCOUNT ON ;  
        
        SELECT  O.[OrderID],
                O.[PortalId],
                O.[AccountID],
                O.[OrderStateID],
                O.[ShippingID],
                O.[PaymentTypeId],
                O.[ShipFirstName],
                O.[ShipLastName],
                O.[ShipCompanyName],
                O.[ShipStreet],
                O.[ShipStreet1],
                O.[ShipCity],
                O.[ShipStateCode],
                O.[ShipPostalCode],
                O.[ShipCountry],
                O.[ShipPhoneNumber],
                O.[ShipEmailID],
                O.[BillingFirstName],
                O.[BillingLastName],
                O.[BillingCompanyName],
                O.[BillingStreet],
                O.[BillingStreet1],
                O.[BillingCity],
                O.[BillingStateCode],
                O.[BillingPostalCode],
                O.[BillingCountry],
                O.[BillingPhoneNumber],
                O.[BillingEmailId],
                O.[CardTransactionID],
                O.[CardAuthCode],
                O.[CardTypeId],
                O.[CardExp],
                O.[TaxCost],
                O.[ShippingCost],
                O.[SubTotal],
                O.[DiscountAmount],
                O.[Total],
                O.[OrderDate],
                O.[Custom1],
                O.[Custom2],
                O.[AdditionalInstructions],
                O.[Custom3],
                O.[TrackingNumber],
                O.[CouponCode],
                O.[PromoDescription],
                O.[ReferralAccountID],
                O.[PurchaseOrderNumber],
                O.[PaymentStatusID],
                O.[WebServiceDownloadDate],
                O.[PaymentSettingID],
                O.[ShipDate],
                O.[ReturnDate],
                O.[SalesTax],
                O.[VAT],
                O.[GST],
                O.[PST],
                O.[HST],
                P.[StoreName] AS 'StoreName',
                OS.[OrderStateName] AS 'OrderStatus',
                PT.[Name] AS 'PaymentTypeName',
                ST.[Name] AS 'ShippingTypeName'
        FROM    ZNodeOrder O
                INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
                INNER JOIN ZNodeOrderState OS ON OS.OrderStateID = O.OrderStateID
                INNER JOIN ZNodeShipping S ON S.ShippingID = O.ShippingID
                INNER JOIN ZNodeShippingType ST ON ST.ShippingTypeID = S.ShippingTypeID
                INNER JOIN ZNodePaymentType PT ON PT.PaymentTypeID = O.PaymentTypeId
        WHERE   O.OrderDate BETWEEN @FromDate AND @ToDate
               AND (P.PortalID = @PortalId OR @PortalId = '0')
               AND (OS.OrderStateID = @OrderStateId OR @OrderStateId = '0')               
                
        -- Select the order line item
        SELECT  Ol.[OrderID],
                Ol.[Name],
                Ol.[ProductNum],
                Ol.SKU,
                Ol.[Quantity],
                Ol.[Price]
        FROM    ZNodeOrderLineItem Ol,
                ZNodeOrder O
        WHERE   O.OrderID = Ol.OrderID
                AND Ol.OrderID in (
                SELECT  OrderID
                FROM    ZNodeOrder Orders
                        INNER JOIN ZNodePortal P ON Orders.PortalId = P.PortalID
                        INNER JOIN ZNodeOrderState Os ON Orders.OrderStateID = Os.OrderStateID
                WHERE   O.OrderDate BETWEEN @FromDate AND @ToDate
						AND (P.PortalID = @PortalId OR @PortalId = '0')
						AND (OS.OrderStateID = @OrderStateId OR @OrderStateId = '0'))		
                
    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_DeletePortalByPortalID]'
GO
-- Delete Portal Related Information  
ALTER PROCEDURE [dbo].[ZNode_DeletePortalByPortalID](@PortalID INT)          
AS    
BEGIN  
  
	-- Delete Notes only for this portal ID  
	DELETE ZNodeNote FROM ZNodeNote WITH (ROWLOCK) inner join 
	ZNodeCaseRequest on ZNodeNote.CaseID = ZNodeCaseRequest.CaseID 
	WHERE ZNodeCaseRequest.PortalID = @PortalID  
  
 -- Delete Case Request for this portal ID   
    DELETE FROM ZNodeCaseRequest WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete Content Page Revision for this portal ID  
    DELETE ZNodeContentPageRevision  
    FROM ZNodeContentPageRevision WITH (ROWLOCK)   
       inner join ZNodeContentPage on ZNodeContentPageRevision.ContentPageID = ZNodeContentPage.ContentPageID  
    WHERE ZNodeContentPage.PortalID = @PortalID  
      
    -- Delete Content Page for this portal ID  
    DELETE FROM ZNodeContentPage WITH (ROWLOCK) WHERE PortalID = @PortalID  
     
    -- Delete IPCommmerce for this portal ID  
    DELETE FROM ZNodeIPCommerce WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete Account Payments for this portal ID  
    DELETE ZNodeAccountPayment  
    FROM ZNodeAccountPayment WITH (ROWLOCK)   
       inner join ZNodeOrder on ZNodeAccountPayment.OrderID = ZNodeOrder.OrderID  
    WHERE ZNodeOrder.PortalID = @PortalID  
      
    -- Delete Digital Assets for this portal ID  
    DELETE ZNodeDigitalAsset  
    FROM ZNodeDigitalAsset WITH (ROWLOCK)   
       inner join ZNodeOrderLineItem on ZNodeDigitalAsset.OrderLineItemID = ZNodeOrderLineItem.OrderLineItemID  
       inner join ZNodeOrder on ZNodeOrderLineItem.OrderID = ZNodeOrder.OrderID  
    WHERE ZNodeOrder.PortalID = @PortalID  
      
    -- Delete Order Line Items for this portal ID  
    DELETE ZNodeOrderLineItem  
    FROM ZNodeOrderLineItem WITH (ROWLOCK)   
       inner join ZNodeOrder on ZNodeOrderLineItem.OrderID = ZNodeOrder.OrderID  
    WHERE ZNodeOrder.PortalID = @PortalID  
      
    -- Delete Orders for this portal ID  
    DELETE FROM ZNodeOrder WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete Portal Catalog for this portal ID  
    DELETE FROM ZNodePortalCatalog WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete Portal Country for this portal ID  
    DELETE FROM ZNodePortalCountry WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete Portal Profile for this portal ID  
    DELETE FROM ZNodePortalProfile WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete Portal Tax Rules for this portal ID  
    DELETE FROM ZNodeTaxRule WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete Tracking Events for this portal ID  
    DELETE ZNodeTrackingEvent  
    FROM ZNodeTrackingEvent WITH (ROWLOCK)   
       inner join ZNodeTracking on ZNodeTrackingEvent.TrackingID = ZNodeTracking.TrackingID  
    WHERE ZNodeTracking.PortalID = @PortalID  
      
    -- Delete Tracking for this portal ID  
    DELETE FROM ZNodeTracking WITH (ROWLOCK) WHERE PortalID = @PortalID  
      
    -- Delete the portal for this portal ID  
    DELETE FROM ZNodePortal WITH (ROWLOCK) WHERE PortalID = @PortalID  
  
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductByProductID_XML]'
GO

-- ==========================================================   
-- Create date: July 02, 2006
-- Update date: May 26,2010        
-- Description: Retrieve product and return a serialized xml                          
-- ==========================================================
ALTER PROCEDURE [dbo].[ZNode_GetProductByProductID_XML]
    @ProductId int = 0,
    @LocaleId int = 0,
    @PortalId int = 0
AS 
    BEGIN                                
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;

        CREATE TABLE #PRODUCTCATEGORYLIST
            (
              ProductId int,
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )
                WITH ( IGNORE_DUP_KEY = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY] ;
     
		
		SELECT  ZNodeProductCrossSell.RelatedProductID AS ProductID
				INTO #CrossSellItem
		FROM	ZNodeProductCrossSell ZNodeProductCrossSell
		WHERE	ZNodeProductCrossSell.ProductID = @ProductId


		IF ((SELECT COUNT(1) FROM #CrossSellItem) > 0)
		BEGIN
			INSERT  INTO #PRODUCTCATEGORYLIST ( ProductId )
                SELECT  ZPC.ProductID
                FROM 
                        ZNodeProductCategory ZPC
                        INNER JOIN ZNodeCategoryNode ZCN ON ZPC.CategoryID = ZCN.CategoryID
                        INNER JOIN ZNodePortalCatalog PC ON ZCN.CatalogID = PC.CatalogID
                WHERE
						ZPC.ProductID IN  (SELECT ProductID FROM #CrossSellItem)
						AND	ZCN.ActiveInd = 1
                        AND ZPC.ActiveInd = 1
                        AND PC.LocaleID = @LocaleId
                        AND PC.PortalID = @PortalId
		END

     
        SELECT  [ProductID],
                [Name],
                [ShortDescription],
                [Description],
                [FeaturesDesc],
                [ProductNum],
                [ProductTypeID],
                [RetailPrice],
                [SalePrice],
                [WholesalePrice],
                [ImageFile],
                [ImageAltTag],
                [Weight],
                [Length],
                [Width],
                [Height],
                [BeginActiveDate],
                [EndActiveDate],
                [DisplayOrder],
                [ActiveInd],
                [CallForPricing],
                [HomepageSpecial],
                [CategorySpecial],
                [InventoryDisplay],
                [Keywords],
                [ManufacturerID],
                [AdditionalInfoLink],
                [AdditionalInfoLinkLabel],
                [ShippingRuleTypeID],
                [SEOTitle],
                [SEOKeywords],
                [SEODescription],
                [Custom1],
                [Custom2],
                [Custom3],
                [ShipEachItemSeparately],
                [SKU],
                [AllowBackOrder],
                [BackOrderMsg],
                [DropShipInd],
                [DropShipEmailID],
                [Specifications],
                [AdditionalInformation],
                [InStockMsg],
                [OutOfStockMsg],
                [TrackInventoryInd],
                [DownloadLink],
                [FreeShippingInd],
                [NewProductInd],
                [SEOURL],
                [MaxQty],
                [ShipSeparately],
                [FeaturedInd],
                [WebServiceDownloadDte],
                [UpdateDte],
                [SupplierID],
                [RecurringBillingInd],
                [RecurringBillingInstallmentInd],
                [RecurringBillingPeriod],
                [RecurringBillingFrequency],
                [RecurringBillingTotalCycles],
                [RecurringBillingInitialAmount],
                [TaxClassID],
                QuantityOnHand = ( SELECT   QuantityOnHand
                                   FROM     ZNodeProductInventory
                                   WHERE    ZNodeProductInventory.ProductNum = ZNodeProduct.ProductNum
                                 ),
                ReorderLevel = ( SELECT ReorderLevel
                                 FROM   ZNodeProductInventory
                                 WHERE  ZNodeProductInventory.ProductNum = ZNodeProduct.ProductNum
                               ),
                ManufacturerName = ( Select Name
                                     from   ZNodeManufacturer
                                     where  ManufacturerId = ZNodeProduct.ManufacturerId AND ActiveInd = 1
                                   ),
                ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                            TotalReviews = IsNull(COUNT(ProductId), 0)
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.Status = 'A'
                FOR
                  XML PATH(''),
                      TYPE
                ),
                ( SELECT    [ProductCategoryID],
                            ZNodeProductCategory.[ProductID],
                            [Theme],
                            [MasterPage],
                            [CSS],
                            [CategoryID],
                            [BeginDate],
                            [EndDate],
                            [DisplayOrder],
                            [ActiveInd]
                  FROM      ZNodeProductCategory ZNodeProductCategory
                  WHERE     ZNodeProductCategory.ProductId = ZNodeProduct.productid
                  ORDER BY  ZNodeProductCategory.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    ZNodeHighlight.[HighlightID],
                            [ImageFile],
                            [ImageAltTag],
                            [Name],
                            [Description],
                            [DisplayPopup],
                            [Hyperlink],
                            [HyperlinkNewWinInd],
                            [HighlightTypeID],
                            [ActiveInd],
                            ZNodeHighlight.[DisplayOrder],
                            [ShortDescription],
                            [LocaleId]
                  FROM      ZNodehighlight ZNodeHighlight
                            INNER JOIN ZNodeProductHighlight ZNodeProductHighlight ON ZNodeHighlight.[HighlightID] = ZNodeProductHighlight.[HighlightID]
                  WHERE     ZNodeProductHighlight.productid = ZNodeProduct.productid
                  ORDER BY  ZNodeProductHighlight.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    [ReviewID],
                            ZNodeReview.[ProductID],
                            [AccountID],
                            [Subject],
                            [Pros],
                            [Cons],
                            [Comments],
                            [CreateUser],
                            [UserLocation],
                            [Rating],
                            [Status],
                            [CreateDate],
                            [Custom1],
                            [Custom2],
                            [Custom3]
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.[Status] = 'A'
                  ORDER BY  ZNodeReview.ReviewId desc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( select    ZNodeCrossSellItem.ProductId,
                            ZNodeCrossSellItem.Name,
                            ZNodeCrossSellItem.ShortDescription,
                            ZNodeCrossSellItem.ImageFile,
                            ZNodeCrossSellItem.SeoURL,
                            ZNodeCrossSellItem.ImageAltTag,
                            ZNodeCrossSellItem.RetailPrice,
                            ZNodeCrossSellItem.SalePrice,
                            ZNodeCrossSellItem.WholesalePrice,
                            ZNodeCrossSellItem.CallForPricing,
                            ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                                        TotalReviews = IsNull(COUNT(ProductId), 0)
                              FROM      ZNodeReview ZNodeReview
                              WHERE     ZNodeReview.ProductId = ZNodeCrossSellItem.ProductId
                                        AND ZNodeReview.[Status] = 'A'
                            FOR
                              XML PATH(''),
                                  TYPE
                            )
                  FROM     ProductsView ZNodeCrossSellItem
                  WHERE 
							ZNodeCrossSellItem.ProductId IN (
                            SELECT  ProductId
                            from    #PRODUCTCATEGORYLIST )
                            AND ZNodeCrossSellItem.ActiveInd = 1
                   
                  ORDER BY  ZNodeCrossSellItem.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( Select    ZNodeAttributeType.[AttributeTypeId],
                            ZNodeAttributeType.[Name],
                            ZNodeAttributeType.[Description],
                            ZNodeAttributeType.[DisplayOrder],
                            [IsPrivate],
                            [LocaleId],
                            ( SELECT
      Distinct                          ( ZNodeAttribute.AttributeId ),
                                        ZNodeAttribute.Name,
                                        ZNodeAttribute.DisplayOrder
                              FROM      ZNodeskuattribute skua
                                        INNER JOIN ZNodeProductattribute ZNodeAttribute ON skua.attributeid = ZNodeAttribute.attributeid
                              WHERE     ZNodeAttribute.attributetypeid = ZNodeAttributeType.attributetypeid
                                        and ZNodeAttribute.IsActive = 1
                                        and SKUID in (
                                        SELECT  SKUID
                                        FROM    ZNodeSKU
                                        WHERE   ProductID = @ProductId
                                                and ActiveInd = 1 )
                              Group By  ZNodeAttribute.AttributeId,
                                        ZNodeAttribute.Name,
                                        ZNodeAttribute.DisplayOrder
                              Order By  ZNodeAttribute.DisplayOrder
                            FOR
                              XML AUTO,
                                  TYPE,
                                  ELEMENTS
                            )
                  FROM      ZNodeAttributeType ZNodeAttributeType
                            INNER JOIN ZNodeProductTypeAttribute ProductTypeAttribute 
                            ON ZNodeAttributeType.attributetypeid = ProductTypeAttribute.attributetypeid
                            AND ProductTypeAttribute.ProductTypeId = ZNodeProduct.ProductTypeID
                  Order By  ZNodeAttributeType.DisplayOrder
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    ZNodeAddOn.[AddOnID],
                            ZNodeAddOn.[ProductID],
                            [Title],
                            [Name],
                            [Description],
                            [DisplayOrder],
                            [DisplayType],
                            [OptionalInd],
                            [AllowBackOrder],
                            [InStockMsg],
                            [OutOfStockMsg],
                            [BackOrderMsg],
                            [PromptMsg],
                            [TrackInventoryInd],
                            [LocaleId],
                            ( SELECT Distinct
                                        ( ZNodeAddonValue.AddOnValueID ),
                                        ZNodeAddonValue.Name,
                                        ZNodeAddonValue.DisplayOrder,
                                        ZNodeAddonValue.RetailPrice as RetailPrice,
                                        ZNodeAddonValue.SalePrice as SalePrice,
                                        ZNodeAddonValue.WholeSalePrice as WholesalePrice,
                                        ZNodeAddonValue.DefaultInd as IsDefault,
                                        QuantityOnHand = ( SELECT   QuantityOnHand
                                                           FROM     ZNodeAddOnValueInventory
                                                           WHERE    SKU = ZNodeAddOnValue.SKU
                                                         ),
                                        ZNodeAddonValue.weight as WeightAdditional,
                                        ZNodeAddonValue.ShippingRuleTypeID,
                                        ZNodeAddonValue.FreeShippingInd,
                                        ZNodeAddonValue.SupplierID,
                                        ZNodeAddonValue.TaxClassID
                              FROM      ZNodeAddOnValue ZNodeAddOnValue
                              WHERE     ZNodeAddOnValue.AddOnId = ZNodeAddOn.AddOnId
                              Order By  ZNodeAddonValue.DisplayOrder
                            FOR
                              XML AUTO,
                                  TYPE,
                                  ELEMENTS
                            )
                  FROM      ZNodeAddOn ZNodeAddOn
                            INNER JOIN ZNodeProductAddOn ZNodeProductAddOn ON ZNodeAddOn.AddOnId = ZNodeProductAddOn.AddOnId
                  WHERE     ZNodeProductAddOn.ProductId = @ProductId
                  Order By  ZNodeAddon.DisplayOrder
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    [ProductTierID],
                            [ProductID],
                            [ProfileID],
                            [TierStart],
                            [TierEnd],
                            [Price]
                  FROM      ZNodeProductTier ZNodeProductTier
                  WHERE     ZNodeProductTier.ProductId = ZNodeProduct.ProductId
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                )
        FROM    ZNodeProduct ZNodeProduct
        WHERE   ZNodeProduct.ProductId = @ProductId
        FOR     XML AUTO,
                    TYPE,
                    ELEMENTS                  
    END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_WS_GetSKUPricingListByFilter]'
GO

ALTER PROCEDURE [ZNode_WS_GetSKUPricingListByFilter](@Flag       INT = NULL,
                                                      @PortalId   INT = NULL,
                                                      @LocaleCode NVARCHAR(2000))
AS
  BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from            
      -- interfering with SELECT statements.
      SET Nocount ON;
      
      DECLARE @LocaleID INT;
      
      SET @LocaleID = (SELECT LocaleID FROM ZNodeLocale WHERE LocaleCode = @LocaleCode);
      
      CREATE TABLE #temp
        (	ProductId int,
			SKUId int, AddOnvalueId int,
			Name nvarchar(max),
			ProductNum nvarchar(max),
			Sku nvarchar(max),
			RetailPrice money,
			SalePrice money,
			WholesalePrice money, 
			ActiveInd bit,
			StoreName  nvarchar(2000),
			LocaleCode nvarchar(2000)
        )

      IF ( @Flag = 0 )
        BEGIN
            INSERT INTO #temp
                        (Productid,
                         Name,
                         Productnum,
                         Sku,
                         Retailprice,
                         Saleprice,
                         Wholesaleprice,
                         Activeind,
                         Storename,
                         Localecode)
             SELECT DISTINCT product.ProductID,
					Name,
					ProductNum,
					Sku,
					RetailPrice,
					SalePrice,
					WholeSalePrice,
					product.ActiveInd,
					[dbo].[FN_ConcatStoreName](product.ProductID, @PortalID, null),
					[dbo].[FN_ConcatLocaleCode](product.ProductID, @LocaleID, null)
			   FROM
					ZNodeProduct product
						INNER JOIN 
						Znodeproductcategory Zc ON product.ProductID = Zc.ProductID
					  INNER JOIN Znodecategorynode Zcn
						ON Zc.Categoryid = Zcn.Categoryid
					  INNER JOIN Znodeportalcatalog Zpc
						ON Zcn.Catalogid = Zpc.Catalogid
			   WHERE  (Zpc.PortalID = @PortalId OR (@PortalId IS NULL AND Zpc.PortalID > 0))
						AND (Zpc.LocaleID = @LocaleID OR @LocaleID IS NULL)
						AND product.ProductId NOT IN (SELECT Productid FROM Znodesku)

			-- SKU
            INSERT INTO #temp
                        (Skuid,
                         Sku,
                         Retailprice,
                         Saleprice,
                         Wholesaleprice,
                         Activeind,
                         Storename,
                         Localecode)

			SELECT SKUID,
                    productSku.Sku,
                    Retailpriceoverride,
                    Salepriceoverride,
                    Wholesalepriceoverride,
                    productSku.Activeind,					
					[dbo].[FN_ConcatStoreName](productSku.ProductID, @PortalID, null),
					[dbo].[FN_ConcatLocaleCode](productSku.ProductID, @LocaleID, null)
			   FROM   
					ZNodeSku productSku INNER JOIN
						ZNodeProduct product
						ON productSku.ProductID = product.ProductID
						INNER JOIN 
						Znodeproductcategory Zc ON product.ProductID = Zc.ProductID
					  INNER JOIN Znodecategorynode Zcn
						ON Zc.Categoryid = Zcn.Categoryid
					  INNER JOIN Znodeportalcatalog Zpc
						ON Zcn.Catalogid = Zpc.Catalogid
			   WHERE  (Zpc.PortalID = @PortalId OR @PortalId IS NULL)
						AND (Zpc.LocaleID = @LocaleID OR @LocaleID IS NULL)



            INSERT INTO #temp
                        (Addonvalueid,
                         Name,
                         Sku,
                         Retailprice,
                         Saleprice,
                         Wholesaleprice,
                         Activeind,
                         LocaleCode)
            (SELECT Addonvalueid,
                    val.Name,
                    Sku,
                    Retailprice,
                    Saleprice,
                    Wholesaleprice,
                    1,
                    locale.LocaleCode
             FROM Znodeaddonvalue val INNER JOIN 
             ZnodeAddOn addOn  on val.AddOnID = addOn.AddOnID 
             INNER JOIN ZNodeLocale locale ON addOn.LocaleId = locale.LocaleID
             WHERE (locale.LocaleID = @LocaleID OR @LocaleID IS NULL))
             
        END
      ELSE
        IF ( @Flag = 1 )
          BEGIN
              INSERT INTO #temp
                          (Productid,
                           Name,
                           Productnum,
                           Sku,
                           Retailprice,
                           Saleprice,
                           ActiveInd,
                           Wholesaleprice,
                           Storename,
                           Localecode)
                           
                           (SELECT DISTINCT product.ProductID,
					Name,
					ProductNum,
					Sku,
					RetailPrice,
					SalePrice,
					WholeSalePrice,
					product.ActiveInd,
					[dbo].[FN_ConcatStoreName](product.ProductID, @PortalID, null),
					[dbo].[FN_ConcatLocaleCode](product.ProductID, @LocaleID, null)
			   FROM   
						ZNodeProduct product
						INNER JOIN 
						Znodeproductcategory Zc ON product.ProductID = Zc.ProductID
					  INNER JOIN Znodecategorynode Zcn
						ON Zc.Categoryid = Zcn.Categoryid
					  INNER JOIN Znodeportalcatalog Zpc
						ON Zcn.Catalogid = Zpc.Catalogid
			   WHERE  (Zpc.PortalID = @PortalId OR @PortalId IS NULL)
						AND (Zpc.LocaleID = @LocaleID OR @LocaleID IS NULL)
						AND product.ProductId NOT IN (SELECT Productid FROM Znodesku)
						AND [WebServiceDownloadDte] IS NULL)

              
              
               INSERT INTO #temp
                          (SKUId,              
                           Sku,
                           Retailprice,
                           Saleprice,
                           ActiveInd,
                           Wholesaleprice,
                           Storename,
                           Localecode)
              (SELECT Skuid,
                    productSku.Sku,                    
                    Retailpriceoverride,
                    Salepriceoverride,
                    Wholesalepriceoverride,
                    productSku.Activeind,
					[dbo].[FN_ConcatStoreName](productSku.ProductID, @PortalID, null),
					[dbo].[FN_ConcatLocaleCode](productSku.ProductID, @LocaleID, null)
			   FROM   
					ZNodeSku productSku INNER JOIN
						ZNodeProduct product
						ON productSku.ProductID = product.ProductID
						INNER JOIN 
						Znodeproductcategory Zc ON product.ProductID = Zc.ProductID
					  INNER JOIN Znodecategorynode Zcn
						ON Zc.Categoryid = Zcn.Categoryid
					  INNER JOIN Znodeportalcatalog Zpc
						ON Zcn.Catalogid = Zpc.Catalogid					  
			   WHERE  (Zpc.PortalID = @PortalId OR @PortalId IS NULL) 
						AND (Zpc.LocaleID = @LocaleID OR @LocaleID IS NULL)
						AND productSku.[WebServiceDownloadDte] IS NULL)



              INSERT INTO #temp
                        (Addonvalueid,
                         Name,
                         Sku,
                         Retailprice,
                         Saleprice,
                         Wholesaleprice,
                         Activeind,
                         LocaleCode)
						(SELECT Addonvalueid,
								val.Name,
								Sku,
								Retailprice,
								Saleprice,
								Wholesaleprice,
								1,locale.LocaleCode
						 FROM Znodeaddonvalue val INNER JOIN 
						 ZnodeAddOn addOn  on val.AddOnID = addOn.AddOnID 
						 INNER JOIN ZNodeLocale locale ON addOn.LocaleId = locale.LocaleID
						 WHERE (locale.LocaleID = @LocaleID OR @LocaleID IS NULL)
						   AND  [WebServiceDownloadDte] IS NULL)
          END
        ELSE
          IF ( @Flag = 2 )
            BEGIN
                INSERT INTO #temp
                            (Productid,
                             Name,
                             Productnum,
                             Sku,
                             Retailprice,
                             Saleprice,
                             Wholesaleprice,
                             StoreName,
                             LocaleCode)
                (SELECT DISTINCT product.Productid,
                        Name,
                        Productnum,
                        Sku,
                        Retailprice,
                        Saleprice,
                        Wholesaleprice,
                        [dbo].[FN_ConcatStoreName](product.ProductID, @PortalID, null),
						[dbo].[FN_ConcatLocaleCode](product.ProductID, @LocaleID, null)
				FROM   
					ZNodeProduct product
						INNER JOIN 
						Znodeproductcategory Zc ON product.ProductID = Zc.ProductID
					  INNER JOIN Znodecategorynode Zcn
						ON Zc.Categoryid = Zcn.Categoryid
					  INNER JOIN Znodeportalcatalog Zpc
						ON Zcn.Catalogid = Zpc.Catalogid					 			 
			   WHERE  (Zpc.PortalID = @PortalId OR (@PortalId IS NULL AND Zpc.PortalID > 0))
						AND (Zpc.LocaleID = @LocaleID OR @LocaleID IS NULL)
						AND product.ProductId NOT IN (SELECT Productid FROM Znodesku)
						AND [WebServiceDownloadDte] < [UpdateDte])
 
 
				-- SKU
                INSERT INTO #temp
                            (Skuid,
                             Sku,
                             Retailprice,
                             Saleprice,
                             Wholesaleprice,
                             ActiveInd,
                             StoreName,
                             LocaleCode)
                             
                             SELECT SKUID,
									productSku.Sku,
									Retailpriceoverride,
									Salepriceoverride,
									Wholesalepriceoverride,
									productSku.Activeind,
									[dbo].[FN_ConcatStoreName](productSku.ProductID, @PortalID, null),
									[dbo].[FN_ConcatLocaleCode](productSku.ProductID, @LocaleID, null)
							FROM   
								ZNodeSku productSku INNER JOIN
									ZNodeProduct product
									ON productSku.ProductID = product.ProductID
									INNER JOIN 
									Znodeproductcategory Zc ON product.ProductID = Zc.ProductID
								  INNER JOIN Znodecategorynode Zcn
									ON Zc.Categoryid = Zcn.Categoryid
								  INNER JOIN Znodeportalcatalog Zpc
									ON Zcn.Catalogid = Zpc.Catalogid								  
							WHERE  (Zpc.PortalID = @PortalId OR @PortalId IS NULL)
									AND (Zpc.LocaleID = @LocaleID OR @LocaleID IS NULL)
									AND productSku.[WebServiceDownloadDte]  < productSku.[UpdateDte]


					-- AddOnValue
					INSERT INTO #temp
                        (Addonvalueid,
                         Name,
                         Sku,
                         Retailprice,
                         Saleprice,
                         Wholesaleprice,
                         Activeind,
                         LocaleCode)
						(SELECT Addonvalueid,
								val.Name,
								Sku,
								Retailprice,
								Saleprice,
								Wholesaleprice,
								1,
								locale.LocaleCode
						 FROM Znodeaddonvalue val INNER JOIN 
						 ZnodeAddOn addOn  on val.AddOnID = addOn.AddOnID 
						 INNER JOIN ZNodeLocale locale ON addOn.LocaleId = locale.LocaleID
						 WHERE (locale.LocaleID = @LocaleID OR @LocaleID IS NULL)
						 AND [WebServiceDownloadDte] < [UpdateDte])
            END

      -- select result        
      SELECT ProductId,
			SKUId, AddOnvalueId,
			Name,
			ProductNum,
			Sku,
			RetailPrice,
			SalePrice,
			WholesalePrice, 
			ActiveInd,
			StoreName,
			LocaleCode  
      FROM   #temp Pricing
      FOR XML AUTO, TYPE, ELEMENTS

      -- Drop temporary table        
      DROP TABLE #temp
  END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetProductsBySearch0_XML]'
GO
-- ========================================================== 
-- Create date: July 20,2006      
-- Update date: June 11,2010  
-- Description: search products and return a serialized xml  
-- ==========================================================
CREATE PROCEDURE [dbo].[ZNode_GetProductsBySearch0_XML]
       @PortalId INT = 0 ,
       @Keyword VARCHAR(4000) = NULL ,
       @Delimiter VARCHAR(10) = ' ' ,
       @CategoryId INT = 0 ,
       @SKU VARCHAR(50) = NULL ,
       @ProductNum VARCHAR(50) = NULL ,
       @CatalogId INT = NULL
AS
BEGIN
      SET NOCOUNT ON ;

      CREATE TABLE #CategoryProductList
      (
        ProductId INT ,
        CategoryID INT )
	  
	  CREATE UNIQUE INDEX IX_CategoryProductList ON #CategoryProductList (ProductId, CategoryID) WITH (IGNORE_DUP_KEY = ON);
	
      CREATE TABLE #ProductList
      ( ProductId INT )
      
      CREATE UNIQUE INDEX IX_ProductList ON #ProductList (ProductId) WITH (IGNORE_DUP_KEY = ON);

      DECLARE @Count INT ;
      SET @Count = 0 ;


      CREATE TABLE #CategoryList
      ( CategoryID INT )
      
      CREATE UNIQUE INDEX IX_CategoryList ON #CategoryList (CategoryID) WITH (IGNORE_DUP_KEY = ON);
           
      -- Get all matching categories and their children (all levels)                              
      IF @CategoryId > 0
         BEGIN

               INSERT INTO
                   #CategoryList
                   SELECT
                       CategoryID
                   FROM
                       [dbo].[ZNode_GetCategoryHierarchy](@PortalID , @CategoryId , @CatalogId)

         END
      ELSE
         BEGIN
               INSERT INTO
                   #CategoryList
                   SELECT
                       CategoryID
                   FROM
                       ZNodeCategoryNode
                   WHERE
                       CatalogID = @CatalogId
         END


      IF ( LEN(@Keyword) <> 0 )
         BEGIN

               INSERT
                   #CategoryProductList
                   (
                     ProductId ,
                     CategoryID )
                   ( SELECT
                         ProductID ,
                         CategoryID
                     FROM
                         ZNodeProductCategory ZPC
                     WHERE
                         EXISTS ( SELECT
                                      1
                                  FROM
                                      #CategoryList
                                  WHERE
                                      ZPC.CategoryID = CategoryID ) AND ZPC.ActiveInd = 1 ) ;

               WITH CategoryList( ProductID )
                   AS ( SELECT
                            ProductID
                        FROM
                            #CategoryProductList
                        WHERE
                            CategoryID IN ( SELECT
                                                ZC.CategoryID
                                            FROM
                                                ZNodeCategory ZC
                                            WHERE
                                                EXISTS ( SELECT
                                                             Value
                                                         FROM
                                                             SC_Splitter(@Keyword , ' ')
                                                         WHERE
                                                             PATINDEX('%' + Value + '%' , ZC.Name) > 0 )
                                                AND ZC.CategoryID IN ( SELECT
                                                                           CategoryID
                                                                       FROM
                                                                           #CategoryList ) ) )

                   INSERT
                       #ProductList
                       SELECT
                           pr.ProductID
                       FROM
                           ProductsView pr
                       LEFT OUTER JOIN ZNodeManufacturer m
                       ON  m.ManufacturerID = pr.ManufacturerID
                       LEFT OUTER JOIN ZNodeProductType t
                       ON  t.ProductTypeId = pr.ProductTypeID
                       WHERE
                           EXISTS ( SELECT
                                        1
                                    FROM
                                        #CategoryProductList
                                    WHERE
                                        pr.ProductID = ProductId )
                           AND ( PATINDEX('%' + @Keyword + '%' , Pr.Name) > 0
                                 OR PATINDEX('%' + @Keyword + '%' , pr.SEOTitle) > 0
                                 OR PATINDEX('%' + @Keyword + '%' , pr.SEODescription) > 0
                                 OR PATINDEX('%' + @Keyword + '%' , pr.ProductNum) > 0
                                 OR PATINDEX('%' + @Keyword + '%' , m.Name) > 0
                                 OR PATINDEX('%' + @Keyword + '%' , t.Name) > 0
                                 OR ProductID IN ( SELECT
                                                       ProductID
                                                   FROM
                                                       CategoryList ) ) ;
         END

      IF ( LEN(@Keyword) = 0 )
         BEGIN

               INSERT
                   #ProductList
                   (
                     ProductId )
                   ( SELECT
                         ProductID
                     FROM
                         ZNodeProductCategory ZPC
                     WHERE
                         EXISTS ( SELECT
                                      1
                                  FROM
                                      #CategoryList
                                  WHERE
                                      ZPC.CategoryID = #CategoryList.CategoryID ) AND ZPC.ActiveInd = 1 ) ;

         END
            
		-- Create temp table to hold skuid
      IF LEN(@SKU) <> 0
         BEGIN

		-- Get all matching skus        
               DELETE  FROM
                       #ProductList
               WHERE
                       ProductId NOT IN ( SELECT
                                              ProductId
                                          FROM
                                              ZNodeSKU
                                          WHERE
                                              Sku LIKE @SKU + '%'
                                          UNION ALL
                                          SELECT
                                              ProductId
                                          FROM
                                              ProductsView
                                          WHERE
                                              Sku LIKE @SKU + '%' )
         END

      SELECT
          P.ProductID ,
          Row_number() OVER (
          ORDER BY
          P.Displayorder ,
          P.ProductID ASC ) AS 'DisplayOrder' ,
          Row_number() OVER (
          ORDER BY
          P.RetailPrice ASC ) AS 'RetailPrice',
          Row_number() OVER (
          ORDER BY
          P.RetailPrice DESC ) AS 'RetailPriceDESC'
      FROM
          ProductsView P
      WHERE
          ( LEN(@ProductNum) = 0
          OR P.ProductNum LIKE @ProductNum + '%' )
          AND EXISTS ( SELECT
                           1
                       FROM
                           #ProductList
                       WHERE
                           P.ProductID = ProductID
                       GROUP BY
                           ProductId
                       HAVING
                           COUNT(ProductID) > @Count )
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductsBySearch_XML]'
GO
  
-- =============================================                
-- Create date: July 20,2006        
-- Update date: April 20,2010    
-- Description: search products and return a serialized xml    
-- =============================================    
ALTER PROCEDURE [dbo].[ZNode_GetProductsBySearch_XML]                                 
 -- Add the parameters for the stored procedure here                                
    @PortalId int = 0,
    @Keywords varchar(4000) = null,
    @Delimiter varchar(10) = ' ',
    @CategoryId int = 0,
    @SearchOption int = 0,
    @SKU varchar(50) = null,
    @ProductNum varchar(50) = null,
    @CatalogId int = null
AS /*    
    
EXEC [ZNode_GetProductsBySearch_XML] 0,'Apple',' ',0,1,'','',1,1,10,'RetailPrice','DESC'  
  
*/    
    
    BEGIN   
	 -- SET NOCOUNT ON added to prevent extra result sets from                                
	 -- interfering with SELECT statements.                                
        SET NOCOUNT ON ; 
        
        IF ( @SearchOption = 0 )        
        BEGIN  
			    -- Exact match  
            EXEC dbo.ZNode_GetProductsBySearch0_XML @PortalId, @Keywords,
                @Delimiter, @CategoryId, @SKU, @ProductNum, @CatalogId
        END  
    
 
        IF ( @SearchOption = 1) 
            BEGIN    
				-- OR MATCH  
                EXEC dbo.ZNode_GetProductsBySearch1_XML @PortalId, @Keywords,
                    @Delimiter, @CategoryId, @SKU, @ProductNum, @CatalogId
            END                                
                
        IF ( @SearchOption = 2) 
            BEGIN          
				-- AND MATCH
                EXEC dbo.ZNode_GetProductsBySearch2_XML @PortalId, @Keywords,
                    @Delimiter, @CategoryId, @SKU, @ProductNum, @CatalogId
            END  
    END    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeTagGroup_GetPaged]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeTagGroup table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeTagGroup_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[TagGroupID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [TagGroupID]'
				SET @SQL = @SQL + ', [TagGroupLabel]'
				SET @SQL = @SQL + ', [ControlTypeID]'
				SET @SQL = @SQL + ', [CatalogID]'
				SET @SQL = @SQL + ', [DisplayOrder]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeTagGroup]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [TagGroupID],'
				SET @SQL = @SQL + ' [TagGroupLabel],'
				SET @SQL = @SQL + ' [ControlTypeID],'
				SET @SQL = @SQL + ' [CatalogID],'
				SET @SQL = @SQL + ' [DisplayOrder]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeTagGroup]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetTagsByCategoryId]'
GO
SET ANSI_NULLS ON
GO



ALTER PROCEDURE [dbo].[ZNode_GetTagsByCategoryId]
    (
      @CategoryID int,
      @TagIds varchar(max)
    )
AS 
    BEGIN

        IF ( Len(@TagIds) = 0 ) 
            BEGIN
				EXEC [ZNode_GetTagsByCategory] @CategoryId
            END
        ELSE 
            BEGIN
                EXEC [ZNode_GetTagsByCategoryIdTagIds] @CategoryID, @TagIds
            END
    END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeCategoryNode_ActiveInd] on [dbo].[ZNodeCategoryNode]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeCategoryNode_ActiveInd] ON [dbo].[ZNodeCategoryNode] ([ActiveInd]) INCLUDE ([CatalogID], [CategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeCategoryNode_CatalogID] on [dbo].[ZNodeCategoryNode]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeCategoryNode_CatalogID] ON [dbo].[ZNodeCategoryNode] ([CatalogID]) INCLUDE ([CategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZnodeCategoryNode_CatalogID_Include_Parent] on [dbo].[ZNodeCategoryNode]'
GO
CREATE NONCLUSTERED INDEX [IX_ZnodeCategoryNode_CatalogID_Include_Parent] ON [dbo].[ZNodeCategoryNode] ([CatalogID]) INCLUDE ([CategoryID], [CategoryNodeID], [ParentCategoryNodeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeCategoryNode_CatalogParentActive] on [dbo].[ZNodeCategoryNode]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeCategoryNode_CatalogParentActive] ON [dbo].[ZNodeCategoryNode] ([CatalogID], [ParentCategoryNodeID], [ActiveInd])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeCategoryNode_CategoryID] on [dbo].[ZNodeCategoryNode]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeCategoryNode_CategoryID] ON [dbo].[ZNodeCategoryNode] ([CategoryID]) INCLUDE ([CatalogID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZnodeCategoryNode_ParentCategoryNodeID] on [dbo].[ZNodeCategoryNode]'
GO
CREATE NONCLUSTERED INDEX [IX_ZnodeCategoryNode_ParentCategoryNodeID] ON [dbo].[ZNodeCategoryNode] ([ParentCategoryNodeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeOrder_PortalId] on [dbo].[ZNodeOrder]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeOrder_PortalId] ON [dbo].[ZNodeOrder] ([PortalId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodePortalCatalog_PortalID_CatalogID] on [dbo].[ZNodePortalCatalog]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ZNodePortalCatalog_PortalID_CatalogID] ON [dbo].[ZNodePortalCatalog] ([PortalID], [CatalogID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeProductCategory_ActiveInd] on [dbo].[ZNodeProductCategory]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeProductCategory_ActiveInd] ON [dbo].[ZNodeProductCategory] ([ActiveInd]) INCLUDE ([CategoryID], [ProductID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeProductCategory_CategoryProductActive] on [dbo].[ZNodeProductCategory]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeProductCategory_CategoryProductActive] ON [dbo].[ZNodeProductCategory] ([CategoryID]) INCLUDE ([ActiveInd], [ProductID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeProductCategory_ProductID] on [dbo].[ZNodeProductCategory]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeProductCategory_ProductID] ON [dbo].[ZNodeProductCategory] ([ProductID]) INCLUDE ([CategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Product_Review] on [dbo].[ZNodeReview]'
GO
CREATE NONCLUSTERED INDEX [IX_Product_Review] ON [dbo].[ZNodeReview] ([ProductID], [Status]) INCLUDE ([Rating])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeReview_ProductID_Status] on [dbo].[ZNodeReview]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeReview_ProductID_Status] ON [dbo].[ZNodeReview] ([ProductID], [Status]) INCLUDE ([Rating])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZnodeTag_TagGroupId] on [dbo].[ZNodeTag]'
GO
CREATE NONCLUSTERED INDEX [IX_ZnodeTag_TagGroupId] ON [dbo].[ZNodeTag] ([TagGroupID]) INCLUDE ([IconPath], [TagDisplayOrder], [TagID], [TagName])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_TagGroupCategory] on [dbo].[ZNodeTagGroupCategory]'
GO
CREATE NONCLUSTERED INDEX [IX_TagGroupCategory] ON [dbo].[ZNodeTagGroupCategory] ([CategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeTagProductSKU_TagId] on [dbo].[ZNodeTagProductSKU]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeTagProductSKU_TagId] ON [dbo].[ZNodeTagProductSKU] ([ProductID]) INCLUDE ([TagID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeTagProductSKU_SKUID] on [dbo].[ZNodeTagProductSKU]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeTagProductSKU_SKUID] ON [dbo].[ZNodeTagProductSKU] ([SKUID]) INCLUDE ([TagID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ZNodeTagProductSKU_TagID_ProductID] on [dbo].[ZNodeTagProductSKU]'
GO
CREATE NONCLUSTERED INDEX [IX_ZNodeTagProductSKU_TagID_ProductID] ON [dbo].[ZNodeTagProductSKU] ([TagID], [ProductID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeSKU]'
GO
ALTER TABLE [dbo].[ZNodeSKU] ADD
CONSTRAINT [FK_ZNodeSKU_ZNodeSupplier] FOREIGN KEY ([SupplierID]) REFERENCES [dbo].[ZNodeSupplier] ([SupplierID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeTagGroupCategory]'
GO
ALTER TABLE [dbo].[ZNodeTagGroupCategory] ADD
CONSTRAINT [FK__ZNodeTagGroupCategory__ZNodeCategory] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[ZNodeCategory] ([CategoryID]) ON DELETE CASCADE,
CONSTRAINT [FK__ZNodeTagGroupCategory__ZNodeTagGroup] FOREIGN KEY ([TagGroupID]) REFERENCES [dbo].[ZNodeTagGroup] ([TagGroupID]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating extended properties'
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[5] 2[36] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'TagProductView', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 1, 'SCHEMA', N'dbo', 'VIEW', N'TagProductView', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

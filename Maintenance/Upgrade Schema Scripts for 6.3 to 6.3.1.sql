SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Refreshing [dbo].[ProductsView]'
GO
EXEC sp_refreshview N'[dbo].[ProductsView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_DeleteCatalog]'
GO

  
  
ALTER PROCEDURE [dbo].[ZNode_DeleteCatalog]      
(@CatalogID INT)  
AS   
BEGIN        
  
CREATE TABLE #CategoryList (CategoryID INT)  
  
INSERT INTO #CategoryList  
SELECT CategoryID   
FROM   
ZNodeCategoryNode ZCN  
WHERE  
ZCN.CatalogID = @CatalogID  
GROUP BY CategoryID;  
  
CREATE TABLE #ProductList (ProductID INT)  
  
INSERT INTO #ProductList  
SELECT ProductID   
FROM   
#CategoryList ZCN INNER JOIN  
ZNodeProductCategory ZPC ON ZPC.CategoryID = ZCN.CategoryID  
GROUP BY ProductID;  
  
  
 DECLARE  @ProductTypeTable Table([ID] INT);  
     
 INSERT INTO @ProductTypeTable (ID)  
 SELECT ProductTypeID FROM ZnodeProduct  
  WHERE  
 ProductID IN (SELECT ProductID FROM #ProductList)  
 GROUP BY ProductTypeID;  
  
DECLARE @SkuExists BIT;  
DECLARE @AddOnExists BIT;  
DECLARE @TagExists BIT;  
DECLARE @ProductTypeExists BIT;  
DECLARE @ManufacturerExists BIT;  
  
SET @AddOnExists = 0;  
SET @SkuExists = 0;  
SET @TagExists = 0;  
SET @ProductTypeExists = 0;  
  
-- AddOn   
SET @AddOnExists  
= (SELECT COUNT(1) FROM ZNodeProductAddOn ZPA  
INNER JOIN ZNodeAddOn ZA ON ZPA.AddOnID = ZA.AddOnID  
INNER JOIN #ProductList PL ON ZPA.ProductID != PL.ProductID)  
  
-- SKU  
SET @SkuExists  
= (SELECT COUNT(1) FROM ZNodeProductAttribute ZPA  
INNER JOIN ZNodeSKUAttribute ZSA ON ZSA.AttributeId = ZPA.AttributeId  
INNER JOIN ZNodeSKU ZS ON ZSA.SKUID = ZS.SKUID  
INNER JOIN #ProductList PL ON ZS.ProductID != PL.ProductID)  
  
  
SET @TagExists  
= (SELECT Count(1)  
FROM ZNodeTagProductSKU TPS  
INNER JOIN #ProductList PL ON TPS.ProductID != PL.ProductID)  
  
SET @ProductTypeExists  
= (SELECT Count(1) FROM ZNodeProduct p  
WHERE   
p.ProductID NOT IN (SELECT ProductID FROM #ProductList)  
AND  
p.ProductTypeID IN (SELECT ID FROM @ProductTypeTable))  
  
--  
IF @AddOnExists = 0  
BEGIN  
 DELETE FROM ZNodeAddOnValue WHERE AddOnID IN (   
 (SELECT ZPA.AddOnID FROM ZNodeProductAddOn ZPA  
  INNER JOIN ZNodeAddOn ZA ON ZPA.AddOnID = ZA.AddOnID  
  INNER JOIN #ProductList PL ON ZPA.ProductID = PL.ProductID))  
    
  DELETE FROM ZNodeAddOn WHERE AddOnID IN (   
 (SELECT ZPA.AddOnID FROM   
  ZNodeProductAddOn ZPA  
  INNER JOIN #ProductList PL ON ZPA.ProductID = PL.ProductID))  
END  
  
DELETE FROM ZNodeProductAddOn WHERE ProductID  
IN (SELECT ProductID FROM #ProductList);  
  
IF @SkuExists = 0  
BEGIN  
 DELETE FROM ZNodeAddOnValue WHERE AddOnID IN (   
 SELECT COUNT(1) FROM ZNodeProductAttribute ZPA  
 INNER JOIN ZNodeSKUAttribute ZSA ON ZSA.AttributeId = ZPA.AttributeId  
 INNER JOIN ZNodeSKU ZS ON ZSA.SKUID = ZS.SKUID  
 INNER JOIN #ProductList PL ON ZS.ProductID != PL.ProductID)  
  
END  
  
DELETE FROM ZNodeSKUAttribute WHERE  
SKUID IN (  
SELECT SKUID FROM ZNodeSKU WHERE ProductID  
IN (SELECT ProductID FROM #ProductList))  
  
DELETE FROM ZNodeSKU WHERE ProductID  
IN (SELECT ProductID FROM #ProductList)  
  
  
IF @TagExists = 0  
BEGIN  
  
 CREATE TABLE #TagList (TagID INT)  
   
 INSERT INTO #TagList   
 SELECT TagID FROM   
 ZNodeTagProductSKU TPS  
 WHERE TPS.ProductID IN (SELECT ProductID FROM #ProductList);  
   
 DELETE FROM #TagList;  
 DELETE FROM ZNodeTag  
 WHERE TagID IN (SELECT TagID FROM #TagList);  
  
END  
ELSE  
BEGIN  
 DELETE FROM ZNodeTagProductSKU  
 WHERE ProductID IN (SELECT ProductID FROM #ProductList);  
END  
  
  
-- DELETE product reviews  
DELETE FROM ZNodeReview  
WHERE ProductID IN (SELECT ProductID FROM #ProductList);  
  
-- Delete Product Highlights  
DELETE FROM ZNodeProductHighlight  
WHERE ProductID IN (SELECT ProductID FROM #ProductList);  
  
-- Delete Product alternate and swatch images  
DELETE FROM ZNodeProductImage  
WHERE ProductID IN (SELECT ProductID FROM #ProductList);  
  
-- Delete Product Digital Assets  
DELETE FROM ZNodeDigitalAsset  
WHERE ProductID IN (SELECT ProductID FROM #ProductList);  
  
-- Delete Product category assocation  
DELETE FROM ZNodeProductCategory WHERE ProductID  
IN (SELECT ProductID FROM #ProductList)  
  
-- Delete Product records  
DELETE FROM ZNodeProduct WHERE ProductID  
IN (SELECT ProductID FROM #ProductList);  
  
IF @ProductTypeExists = 0  
BEGIN  
  
 SELECT AttributeTypeID INTO #AttributeTypeData FROM ZNodeProductTypeAttribute   
 WHERE ProductTypeId IN (SELECT ID FROM @ProductTypeTable);  
    
 DELETE FROM ZNodeProductTypeAttribute   
 WHERE ProductTypeId IN (SELECT ID FROM @ProductTypeTable);  
   
 DELETE FROM ZNodeProductTypeAttribute   
 WHERE AttributeTypeId IN (  
 SELECT AttributeTypeId FROM #AttributeTypeData);  
   
 DELETE FROM ZNodeProductType WHERE ProductTypeId  
 IN (SELECT ID FROM @ProductTypeTable)  
   
END  
  
-- Delete Catalog category records and Category

SELECT CategoryID INTO #CategoryData FROM ZNodeCategoryNode WHERE CatalogID = @CatalogID;
DELETE FROM ZNodeCategory WHERE CategoryID IN (SELECT CategoryID FROM #CategoryData)
DELETE FROM ZNodeCategoryNode WHERE CatalogID = @CatalogID;  
-- Delete Portal Catalog assocaition  
DELETE FROM ZNodePortalCatalog WHERE CatalogID = @CatalogID;  
  
-- Delete Catalog  
DELETE FROM ZNodeCatalog WHERE CatalogID = @CatalogID;  
  
END  
  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductByProductID_XML]'
GO



-- ==========================================================   
-- Create date: July 02, 2006
-- Update date: May 26,2010        
-- Description: Retrieve product and return a serialized xml
-- ==========================================================
ALTER PROCEDURE [dbo].[ZNode_GetProductByProductID_XML]
    @ProductId int = 0,
    @LocaleId int = 0,
    @PortalId int = 0
AS 
    BEGIN                                
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;

        CREATE TABLE #PRODUCTCATEGORYLIST
            (
              ProductId int,
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )
                WITH ( IGNORE_DUP_KEY = ON ) 
            )
		
		CREATE TABLE #CrossSellItem
            (
              ProductId int,
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )
                WITH ( IGNORE_DUP_KEY = ON ) 
            )
		INSERT  INTO #CrossSellItem ( ProductId )
		SELECT  ZNodeProductCrossSell.RelatedProductID AS ProductID				
		FROM	ZNodeProductCrossSell ZNodeProductCrossSell
		INNER JOIN ZnodeProduct ZP ON ZNodeProductCrossSell.RelatedProductID = ZP.ProductID
		WHERE	ZNodeProductCrossSell.ProductID = @ProductId
				AND ZP.ReviewStateID = 20

		IF ((SELECT COUNT(1) FROM #CrossSellItem) > 0)
		BEGIN
			INSERT  INTO #PRODUCTCATEGORYLIST ( ProductId )
                SELECT  ZPC.ProductID
                FROM 
                        ZNodeProductCategory ZPC
                        INNER JOIN ZNodeCategoryNode ZCN ON ZPC.CategoryID = ZCN.CategoryID
                        INNER JOIN ZNodePortalCatalog PC ON ZCN.CatalogID = PC.CatalogID
                WHERE
						ZPC.ProductID IN  (SELECT ProductID FROM #CrossSellItem)
						AND	ZCN.ActiveInd = 1
                        AND ZPC.ActiveInd = 1
                        AND PC.LocaleID = @LocaleId
                        AND PC.PortalID = @PortalId
		END;
		
										
        SELECT  [ProductID],
                [Name],
                [ShortDescription],
                [Description],
                [FeaturesDesc],
                [ProductNum],
                [ProductTypeID],
                [RetailPrice],
                [SalePrice],
                [WholesalePrice],
                [ImageFile],
                [ImageAltTag],
                [Weight],
                [Length],
                [Width],
                [Height],
                [BeginActiveDate],
                [EndActiveDate],
                [DisplayOrder],
                [ActiveInd],
                [CallForPricing],
                [HomepageSpecial],
                [CategorySpecial],
                [InventoryDisplay],
                [Keywords],
                [ManufacturerID],
                [AdditionalInfoLink],
                [AdditionalInfoLinkLabel],
                [ShippingRuleTypeID],
                [ShippingRate],
                [SEOTitle],
                [SEOKeywords],
                [SEODescription],
                [Custom1],
                [Custom2],
                [Custom3],
                [ShipEachItemSeparately],
                [AllowBackOrder],
                [BackOrderMsg],
                [DropShipInd],
                [DropShipEmailID],
                [Specifications],
                [AdditionalInformation],
                [InStockMsg],
                [OutOfStockMsg],
                [TrackInventoryInd],
                [DownloadLink],
                [FreeShippingInd],
                [NewProductInd],
                [SEOURL],
                [MinQty],
                [MaxQty],
                [ShipSeparately],
                [FeaturedInd],
                [WebServiceDownloadDte],
                [UpdateDte],
                [SupplierID],
                [RecurringBillingInd],
                [RecurringBillingInstallmentInd],
                [RecurringBillingPeriod],
                [RecurringBillingFrequency],
                [RecurringBillingTotalCycles],
                [RecurringBillingInitialAmount],
                [TaxClassID],
                [PortalID],
                [AffiliateUrl],
                (SELECT TOP 1 sku.SKU, QuantityOnHand,ExternalProductID
										FROM ZNodeSKU sku
											INNER JOIN ZNodeSKUInventory SkuInventory 
										ON sku.SKU = SkuInventory.SKU
										WHERE 
											EXISTS
											(SELECT 1 FROM ZNodeSKUAttribute
												WHERE SKUID = sku.SKUID having COUNT(1) = 0)
												AND	sku.ProductID = @ProductID
												
										FOR	XML PATH(''),TYPE
								),
                ManufacturerName = ( Select Name
                                     from   ZNodeManufacturer
                                     where  ManufacturerId = ZNodeProduct.ManufacturerId AND ActiveInd = 1
                                   ),
                ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                            TotalReviews = IsNull(COUNT(ProductId), 0)
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.Status = 'A'
                FOR
                  XML PATH(''),
                      TYPE
                ),
                ( SELECT    [ProductCategoryID],
                            ZNodeProductCategory.[ProductID],
                            [Theme],
                            [MasterPage],
                            [CSS],
                            [CategoryID],
                            [BeginDate],
                            [EndDate],
                            [DisplayOrder],
                            [ActiveInd]
                  FROM      ZNodeProductCategory ZNodeProductCategory
                  WHERE     ZNodeProductCategory.ProductId = ZNodeProduct.productid
                  ORDER BY  ZNodeProductCategory.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    ZNodeHighlight.[HighlightID],
                            [ImageFile],
                            [ImageAltTag],
                            [Name],
                            [Description],
                            [DisplayPopup],
                            [Hyperlink],
                            [HyperlinkNewWinInd],
                            [HighlightTypeID],
                            [ActiveInd],
                            ZNodeHighlight.[DisplayOrder],
                            [ShortDescription],
                            [LocaleId]
                  FROM      ZNodehighlight ZNodeHighlight
                            INNER JOIN ZNodeProductHighlight ZNodeProductHighlight ON ZNodeHighlight.[HighlightID] = ZNodeProductHighlight.[HighlightID]
                  WHERE     ZNodeProductHighlight.productid = ZNodeProduct.productid
                  ORDER BY  ZNodeProductHighlight.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    [ReviewID],
                            ZNodeReview.[ProductID],
                            [AccountID],
                            [Subject],
                            [Pros],
                            [Cons],
                            [Comments],
                            [CreateUser],
                            [UserLocation],
                            [Rating],
                            [Status],
                            [CreateDate],
                            [Custom1],
                            [Custom2],
                            [Custom3]
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.[Status] = 'A'
                  ORDER BY  ZNodeReview.ReviewId desc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( select    ZNodeCrossSellItem.ProductId,
                            ZNodeCrossSellItem.Name,
                            ZNodeCrossSellItem.ShortDescription,
                            ZNodeCrossSellItem.ImageFile,
                            ZNodeCrossSellItem.SeoURL,
                            ZNodeCrossSellItem.ImageAltTag,
                            ZNodeCrossSellItem.RetailPrice,
                            ZNodeCrossSellItem.SalePrice,
                            ZNodeCrossSellItem.WholesalePrice,
                            ZNodeCrossSellItem.CallForPricing,
                            ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                                        TotalReviews = IsNull(COUNT(ProductId), 0)
                              FROM      ZNodeReview ZNodeReview
                              WHERE     ZNodeReview.ProductId = ZNodeCrossSellItem.ProductId
                                        AND ZNodeReview.[Status] = 'A'
                            FOR
                              XML PATH(''),
                                  TYPE
                            )
                  FROM     ProductsView ZNodeCrossSellItem
                  WHERE 
							ZNodeCrossSellItem.ProductId IN (
                            SELECT  ProductId
                            from    #PRODUCTCATEGORYLIST )
                            AND ZNodeCrossSellItem.ActiveInd = 1
                   
                  ORDER BY  ZNodeCrossSellItem.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( Select    ZNodeAttributeType.[AttributeTypeId],
                            ZNodeAttributeType.[Name],
                            ZNodeAttributeType.[Description],
                            ZNodeAttributeType.[DisplayOrder],
                            [IsPrivate],
                            [LocaleId],
                            ( SELECT
      Distinct                          ( ZNodeAttribute.AttributeId ),
                                        ZNodeAttribute.Name,
                                        ZNodeAttribute.DisplayOrder
                              FROM      ZNodeskuattribute skua
                                        INNER JOIN ZNodeProductattribute ZNodeAttribute ON skua.attributeid = ZNodeAttribute.attributeid
                              WHERE     ZNodeAttribute.attributetypeid = ZNodeAttributeType.attributetypeid
                                        and ZNodeAttribute.IsActive = 1
                                        and SKUID in (
                                        SELECT  SKUID
                                        FROM    ZNodeSKU
                                        WHERE   ProductID = @ProductId
                                                and ActiveInd = 1 )
                              Group By  ZNodeAttribute.AttributeId,
                                        ZNodeAttribute.Name,
                                        ZNodeAttribute.DisplayOrder
                              Order By  ZNodeAttribute.DisplayOrder
                            FOR
                              XML AUTO,
                                  TYPE,
                                  ELEMENTS
                            )
                  FROM      ZNodeAttributeType ZNodeAttributeType
                            INNER JOIN ZNodeProductTypeAttribute ProductTypeAttribute 
                            ON ZNodeAttributeType.attributetypeid = ProductTypeAttribute.attributetypeid
                            AND ProductTypeAttribute.ProductTypeId = ZNodeProduct.ProductTypeID
                  Order By  ZNodeAttributeType.DisplayOrder
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    ZNodeAddOn.[AddOnID],
                            ZNodeAddOn.[ProductID],
                            [Title],
                            [Name],
                            [Description],
                            [DisplayOrder],
                            [DisplayType],
                            [OptionalInd],
                            [AllowBackOrder],
                            [InStockMsg],
                            [OutOfStockMsg],
                            [BackOrderMsg],
                            [PromptMsg],
                            [TrackInventoryInd],
                            [LocaleId],
                            ( SELECT Distinct
                                        ( ZNodeAddonValue.AddOnValueID ),
                                        ZNodeAddonValue.Name,
                                        ZNodeAddonValue.[Description],
                                        ZNodeAddonValue.DisplayOrder,
                                        ZNodeAddonValue.RetailPrice as RetailPrice,
                                        ZNodeAddonValue.SalePrice as SalePrice,
                                        ZNodeAddonValue.WholeSalePrice as WholesalePrice,
                                        ZNodeAddonValue.DefaultInd as IsDefault,
                                        QuantityOnHand = ( SELECT   QuantityOnHand
                                                           FROM     ZNodeSKUInventory
                                                           WHERE    SKU = ZNodeAddOnValue.SKU
                                                         ),
                                        ZNodeAddonValue.weight as WeightAdditional,
                                        ZNodeAddonValue.ShippingRuleTypeID,
                                        ZNodeAddonValue.FreeShippingInd,
                                        ZNodeAddonValue.SupplierID,
                                        ZNodeAddonValue.TaxClassID,
                                        ZNodeAddonValue.ExternalProductID
                              FROM      ZNodeAddOnValue ZNodeAddOnValue
                              WHERE     ZNodeAddOnValue.AddOnId = ZNodeAddOn.AddOnId
                              Order By  ZNodeAddonValue.DisplayOrder
                            FOR
                              XML AUTO,
                                  TYPE,
                                  ELEMENTS
                            )
                  FROM      ZNodeAddOn ZNodeAddOn
                            INNER JOIN ZNodeProductAddOn ZNodeProductAddOn ON ZNodeAddOn.AddOnId = ZNodeProductAddOn.AddOnId
                  WHERE     ZNodeProductAddOn.ProductId = @ProductId
                  Order By  ZNodeAddon.DisplayOrder
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    [ProductTierID],
                            [ProductID],
                            [ProfileID],
                            [TierStart],
                            [TierEnd],
                            [Price]
                  FROM      ZNodeProductTier ZNodeProductTier
                  WHERE     ZNodeProductTier.ProductId = ZNodeProduct.ProductId
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT TOP 1 *  
                  FROM      ZNodeSKU
                  INNER JOIN ZnodeSKUInventory ON ZNodeSKU.SKU = ZNodeSKUInventory.SKU                     
                  WHERE     ZNodeSKU.ProductId = ZNodeProduct.ProductId  
                FOR  
                  XML PATH('ZNodeSKU'),  
                      TYPE,  
                      ELEMENTS  
                )  
        FROM    ZNodeProduct ZNodeProduct			    
        WHERE   ZNodeProduct.ProductId = @ProductId
        FOR     XML AUTO,
                    TYPE,
                    ELEMENTS                  
    END  



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductCategoryByCategoryID]'
GO

  
ALTER PROCEDURE [dbo].[ZNode_GetProductCategoryByCategoryID] 
(@CATEGORYID INT)      
AS      
BEGIN      
SELECT PRODUCTCATEGORYID,product.NAME,category.PRODUCTID,CATEGORYID,category.DISPLAYORDER,category.ActiveInd,product.PortalID 
FROM znodePRODUCTCATEGORY category      
JOIN znodePRODUCT product ON product.PRODUCTID = category.PRODUCTID    
WHERE CATEGORYID = @CATEGORYID;      
END;        
  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetWishListByAccountID_XML]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetWishListByAccountID_XML](@AccountId int = 0, @PortalId int = 0)          
AS              
BEGIN              
 -- SET NOCOUNT ON added to prevent extra result sets from              
 -- interfering with SELECT statements.              
 SET NOCOUNT ON;              
            
SELECT  
 ZNodeProduct.ProductID,  
 ZNodeProduct.[Name],     
 ZNodeProduct.ProductNum,          
 ZNodeProduct.RetailPrice,            
 ZNodeProduct.SalePrice,          
 ZNodeProduct.WholeSalePrice,          
 ZNodeProduct.ShortDescription,          
 ZNodeProduct.CallForPricing,            
 ZNodeProduct.ImageFile,  
 ZNodeProduct.SEOURL,       
 ZNodeProduct.NewProductInd,  
 ZNodeProduct.FeaturedInd,  
 ZNodeProduct.DisplayOrder,  
 ZNodeProduct.ImageAltTag,       
 ZNodeProduct.TaxClassID          
 FROM ZNodeProduct WHERE ZNodeProduct.ActiveInd = 1 AND ProductID IN   
 ((SELECT ProductID FROM ZNodeWishList WHERE AccountID = @AccountId)    
 INTERSECT  
 (Select ProductID from ZnodeProductCategory Where CategoryID in                 
 (Select CategoryId from ZNodeCategoryNode, ZNodePortalCatalog, ZNodeCatalog where               
 ZNodeCatalog.CatalogId = ZNodeCategoryNode.CatalogId and              
 ZNodePortalCatalog.CatalogId = ZNodeCatalog.CatalogId and              
 ZNodePortalCatalog.PortalId=@PortalId)))  
 ORDER BY ZnodeProduct.DisplayOrder         
 FOR XML AUTO, TYPE, ELEMENTS            
END     
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchBestSellerList]'
GO





-- ==========================================================   
-- Create date: July 02, 2006
-- Update date: May 26,2010        
-- Description: Retrieve product and return a serialized xml                          
-- ==========================================================
ALTER PROCEDURE [dbo].[ZNode_SearchBestSellerList] 
    (
      @CatalogID INT,
      @DisplayItem INT = 0,
      @CategoryId INT,
      @PortalId INT
    )
AS 
    BEGIN         
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.                    
        SET NOCOUNT ON ;                     

    
        WITH PRODUCTCATEGORYLIST AS
        (
			SELECT DISTINCT
					ZNodeProduct.ProductID
            FROM    
					ProductsView ZNodeProduct
            JOIN 
					ZNodeProductCategory ZPC 
			ON 
					ZNodeProduct.ProductID = ZPC.ProductID
            JOIN 
					ZNodeCategoryNode ZCN 
			ON 
					ZPC.CategoryID = ZCN.CategoryID
            JOIN 
					ZNodePortalCatalog PC ON ZCN.CatalogID = PC.CatalogID
            WHERE   
					ZPC.ActiveInd = 1
			AND
					ZCN.ActiveInd = 1
            AND 
					PC.PortalID = @PortalId
			AND 
					PC.CatalogID = @CatalogID
            AND 
					ZCN.CategoryID = 
						CASE WHEN 
							@CategoryId > 0
                        THEN 
							@CategoryId
                        ELSE 
							ZCN.CategoryID
                        END
            AND 
					ZNodeProduct.ProductNum IN (
                        SELECT TOP (@DisplayItem)
							ProductNum
						FROM    
							ZNodeOrderLineItem x
                        JOIN 
								ZNodeOrder y 
						ON 
								x.OrderId = y.OrderId
                        WHERE   
								y.PortalId = @PortalId
                        AND 
								x.ParentOrderLineItemID IS NULL
						AND
								x.ProductNum IS NOT NULL
                        GROUP BY 
								ProductNum
                        HAVING  SUM
								(Quantity) > 0
                        ORDER BY 
								SUM(x.Quantity) DESC )

		) 
        SELECT  TOP 
				(@DisplayItem) ZNodeProduct.ProductID,
                ZNodeProduct.[Name],
                ZNodeProduct.ProductNum,
                ZNodeProduct.RetailPrice,
                ZNodeProduct.SalePrice,
                ZNodeProduct.WholesalePrice,
                ZNodeProduct.ShortDescription,
                ZNodeProduct.CallForPricing,
                ZNodeProduct.ImageFile,
                ZNodeProduct.SEOURL,
                ZNodeProduct.NewProductInd,
                ZNodeProduct.FeaturedInd,
                ZNodeProduct.DisplayOrder,
                ZNodeProduct.ImageAltTag,
                ZNodeProduct.TaxClassID,
                ( SELECT    ReviewRating = ISNULL(AVG(Rating), 0),
                            TotalReviews = ISNULL(COUNT(ProductId), 0)
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.Status = 'A'
                FOR
                  XML PATH(''),
                      TYPE
                )
        FROM    
				ProductsView ZNodeProduct
        JOIN
				PRODUCTCATEGORYLIST PCL
		ON
				PCL.ProductID = ZNodeProduct.ProductID
        FOR     XML AUTO,
                    TYPE,
       
            ELEMENTS
    END





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchProductByNames]'
GO

ALTER PROCEDURE [dbo].[ZNode_SearchProductByNames]  
(            
@NAME VARCHAR(MAX) = NULL,            
@PRODUCTNUM VARCHAR(MAX) = NULL,            
@SKU VARCHAR(MAX) = NULL,            
@MANUFACTURERNAME VARCHAR(MAX) = NULL,            
@PRODUCTTYPEID VARCHAR(MAX) = NULL,  
@CATEGORYNAME VARCHAR(MAX) = NULL,            
@portalIds VARCHAR(MAX) = '0'            
)AS            
BEGIN     
 SET NOCOUNT ON;  
   
 CREATE TABLE #ProductList  
    ( ProductId INT,  
      SKU NVARCHAR(1000)  
     )  
        
 CREATE UNIQUE INDEX IX_ProductList ON #ProductList (ProductId) WITH (IGNORE_DUP_KEY = ON);  
        
 INSERT INTO #ProductList  
  SELECT PRODUCTID, SKU FROM ZNodeSKU WHERE ((SKU Like '%' + @SKU + '%') OR Len(@SKU) = 0)  
    
 SELECT product.[ProductID]  
      ,[Name]  
      ,[ShortDescription]        
      ,[ProductNum]  
      ,[ProductTypeID]  
      ,[RetailPrice]  
      ,[SalePrice]  
      ,[WholesalePrice]  
      ,[ImageFile]  
      ,product.[ImageAltTag]  
      ,[CallForPricing]  
      ,[HomepageSpecial]  
      ,[CategorySpecial]        
      ,[TaxClassID]  
      ,[PortalID]   
      ,product.[ActiveInd]  
      ,product.[Franchisable]  
 FROM [ZNodeProduct] product  
 INNER JOIN #ProductList sku  
 ON product.ProductID = sku.ProductID  
 WHERE   
 (NAME LIKE '%' + @NAME + '%' OR LEN(@NAME) = 0) AND  
 (PRODUCTNUM LIKE '%' + @PRODUCTNUM + '%' OR LEN(@PRODUCTNUM) = 0) AND   
 (ManufacturerID IN (SELECT MANUFACTURERID FROM ZNODEMANUFACTURER WHERE NAME LIKE @MANUFACTURERNAME + '%') OR (LEN(@MANUFACTURERNAME) = 0)) AND  
 (ProductTypeID = @PRODUCTTYPEID OR LEN(@PRODUCTTYPEID) = 0 OR @PRODUCTTYPEID = '0') AND  
 ((product.ProductID IN (SELECT PRODUCTID FROM ZNODEPRODUCTCATEGORY WHERE CATEGORYID IN           
 (SELECT CATEGORYID FROM ZNODECATEGORY WHERE NAME LIKE '%' + @CATEGORYNAME +'%'))) OR LEN(@CATEGORYNAME) = 0)   
 ORDER BY product.Displayorder, Name  
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_UpsertProduct]'
GO

  
  
ALTER PROCEDURE [dbo].[ZNode_UpsertProduct]  
(     
 @ProductID int = 0,  
 @Name nvarchar(MAX) = null,  
 @ShortDescription nvarchar(MAX) = null,  
 @Description nvarchar(MAX) = null,  
 @FeaturesDesc nvarchar(MAX) = null,  
 @ProductNum nvarchar(100) = null,  
 @ProductTypeID int = null,  
 @RetailPrice money = null,  
 @SalePrice money = null,  
 @WholesalePrice money = null,  
 @ImageFile nvarchar(MAX) = null,  
 @ImageAltTag nvarchar(MAX) = null,  
 @Weight decimal(18, 2) = null,  
 @Length decimal(18, 2) = null,  
 @Width decimal(18, 2) = null,  
 @Height decimal(18, 2) = null,  
 @BeginActiveDate datetime = null,  
 @EndActiveDate datetime = null,  
 @DisplayOrder int = null,  
 @ActiveInd bit = null,  
 @CallForPricing bit = null,  
 @HomepageSpecial bit = 0,  
 @CategorySpecial bit = 0,  
 @InventoryDisplay tinyint = null,  
 @Keywords nvarchar(MAX) = null,  
 @ManufacturerID int = null,  
 @AdditionalInfoLink nvarchar(MAX) = null,  
 @AdditionalInfoLinkLabel nvarchar(MAX) = null,  
 @ShippingRuleTypeID int = null,  
 @ShippingRate money = null,  
 @SEOTitle nvarchar(MAX) = null,  
 @SEOKeywords nvarchar(MAX) = null,  
 @SEODescription nvarchar(MAX) = null,  
 @Custom1 nvarchar(MAX) = null,  
 @Custom2 nvarchar(MAX) = null,  
 @Custom3 nvarchar(MAX) = null,  
 @ShipEachItemSeparately bit = null,  
 @AllowBackOrder bit = null,  
 @BackOrderMsg nvarchar(MAX) = null,  
 @DropShipInd bit = null,  
 @DropShipEmailID nvarchar(MAX) = null,  
 @Specifications nvarchar(MAX) = null ,  
 @AdditionalInformation nvarchar(MAX) = null,  
 @InStockMsg nvarchar(MAX) =null ,  
 @OutOfStockMsg nvarchar(MAX) =null ,  
 @TrackInventoryInd bit = null ,  
 @DownloadLink nvarchar(MAX) = null,  
 @FreeShippingInd bit = null,  
 @NewProductInd bit = null,  
 @SEOURL nvarchar(100) = null,  
 @MaxQty int = null,  
 @ShipSeparately bit = null,  
 @FeaturedInd bit = null,  
 @WebServiceDownloadDte datetime = null,  
 @UpdateDte datetime = null,  
 @SupplierID int = null,  
 @RecurringBillingInd bit = null,  
 @RecurringBillingInstallmentInd bit = null,  
 @RecurringBillingPeriod nvarchar (50)  =null,  
 @RecurringBillingFrequency nvarchar(MAX) = null ,  
 @RecurringBillingTotalCycles int = null,  
 @RecurringBillingInitialAmount money = null,  
 @TaxClassID int =null,  
 @MinQty int  = null,  
 @ReviewStateID int = null,  
 @AffiliateUrl varchar (512) = null,  
 @IsShippable bit = null ,  
 @AccountID int = null,  
 @PortalID int = null  
)  
AS   
BEGIN  
DECLARE @Franchisable bit 
DECLARE @ExistingReviewStateID INT  
								
 IF(Exists(SELECT ProductID From ZNodeProduct WHERE ProductId = @ProductID))  
  BEGIN  
   SELECT @Franchisable = Franchisable, @ExistingReviewStateID = ReviewStateID FROM ZNODEPRODUCT WHERE ProductId=@ProductID;
IF(@ReviewStateID  IS NULL)
BEGIN
SET @ExistingReviewStateID = @ExistingReviewStateID
END
ELSE
BEGIN
SET @ExistingReviewStateID = @ReviewStateID
END
   Exec ZNode_NT_ZNodeProduct_Update    
   @ProductID ,  
   @Name ,  
   @ShortDescription,  
   @Description,  
   @FeaturesDesc ,  
   @ProductNum,  
   @ProductTypeID,  
   @RetailPrice,  
   @SalePrice,  
   @WholesalePrice,  
   @ImageFile ,  
   @ImageAltTag,  
   @Weight,  
   @Length,  
   @Width,  
   @Height ,  
   @BeginActiveDate,  
   @EndActiveDate ,  
   @DisplayOrder,  
   @ActiveInd ,  
   @CallForPricing,  
   @HomepageSpecial,  
   @CategorySpecial,  
   @InventoryDisplay,  
   @Keywords,  
   @ManufacturerID,  
   @AdditionalInfoLink,  
   @AdditionalInfoLinkLabel,  
   @ShippingRuleTypeID,  
   @ShippingRate,  
   @SEOTitle ,  
   @SEOKeywords ,  
   @SEODescription ,  
   @Custom1,   
   @Custom2 ,  
   @Custom3,  
   @ShipEachItemSeparately,  
   @AllowBackOrder,  
   @BackOrderMsg,  
   @DropShipInd ,  
   @DropShipEmailID,  
   @Specifications,  
   @AdditionalInformation,  
   @InStockMsg ,  
   @OutOfStockMsg,  
   @TrackInventoryInd ,  
   @DownloadLink ,  
   @FreeShippingInd,  
   @NewProductInd,  
   @SEOURL ,  
   @MaxQty ,  
   @ShipSeparately ,  
   @FeaturedInd ,  
   @WebServiceDownloadDte,  
   @UpdateDte,  
   @SupplierID ,  
   @RecurringBillingInd ,  
   @RecurringBillingInstallmentInd ,  
   @RecurringBillingPeriod ,  
   @RecurringBillingFrequency,  
   @RecurringBillingTotalCycles ,  
   @RecurringBillingInitialAmount,  
   @TaxClassID,   
   @MinQty, 
   @ExistingReviewStateID,  					
   @AffiliateUrl ,  
   @IsShippable,  
   @AccountID,  
   @PortalID,  
   @Franchisable  
   
        
  END   
 ELSE  
  BEGIN   
   Exec ZNode_NT_ZNodeProduct_Insert    
   @ProductID out,  
   @Name ,  
   @ShortDescription,  
   @Description,  
   @FeaturesDesc ,  
   @ProductNum,  
   @ProductTypeID,  
   @RetailPrice,  
   @SalePrice,  
   @WholesalePrice,  
   @ImageFile ,  
   @ImageAltTag,  
   @Weight,  
   @Length,  
   @Width,  
   @Height ,  
   @BeginActiveDate,  
   @EndActiveDate ,  
   @DisplayOrder,  
   @ActiveInd ,  
   @CallForPricing,  
   @HomepageSpecial,  
   @CategorySpecial,  
   @InventoryDisplay,  
   @Keywords,  
   @ManufacturerID,  
   @AdditionalInfoLink,  
   @AdditionalInfoLinkLabel,  
   @ShippingRuleTypeID,  
   @ShippingRate,  
   @SEOTitle ,  
   @SEOKeywords ,  
   @SEODescription ,  
   @Custom1,  
   @Custom2 ,  
   @Custom3,  
   @ShipEachItemSeparately,  
   @AllowBackOrder,  
   @BackOrderMsg,  
   @DropShipInd ,  
   @DropShipEmailID,  
   @Specifications,  
   @AdditionalInformation,  
   @InStockMsg ,  
   @OutOfStockMsg,  
   @TrackInventoryInd ,  
   @DownloadLink ,  
   @FreeShippingInd,  
   @NewProductInd,  
   @SEOURL ,  
   @MaxQty ,  
   @ShipSeparately ,  
   @FeaturedInd ,  
   @WebServiceDownloadDte,  
   @UpdateDte,  
   @SupplierID ,  
   @RecurringBillingInd ,  
   @RecurringBillingInstallmentInd ,  
   @RecurringBillingPeriod ,  
   @RecurringBillingFrequency,  
   @RecurringBillingTotalCycles ,  
   @RecurringBillingInitialAmount,  
   @TaxClassID,   
   @MinQty,  
   @ReviewStateID,  
   @AffiliateUrl ,  
   @IsShippable,  
   @AccountID,  
   @PortalID,  
   @Franchisable  
     
  END  
END  
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZNode_DeleteProductCategoryByProductID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ZNode_DeleteProductCategoryByProductID]
GO



/****** Object:  StoredProcedure [dbo].[ZNode_DeleteProductCategoryByProductID]    Script Date: 01/25/2011 18:30:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ZNode_DeleteProductCategoryByProductID]
(@ProductID INT)      
AS      
BEGIN 
DELETE FROM ZNodeProductCategory 
WHERE ProductID = @ProductID
AND CategoryID IN (SELECT CategoryID FROM ZNodeCategory WHERE ISNULL(PortalID, 0) <> (SELECT ISNULL(PORTALID,0) FROM ZNodeProduct WHERE ProductID=@ProductID))
END;

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
update znodemultifront set majorversion=6,minorversion=3,build=1 where id=1
GO
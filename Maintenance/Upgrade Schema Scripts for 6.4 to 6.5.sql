/*
Run this script on:

      Znode Multifront 6.4
      
to upgrade this to:

        Znode Multifront 6.5

You are recommended to back up your database before running this script

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[ZNodeOrderLineItem]'
GO
ALTER TABLE [dbo].[ZNodeOrderLineItem] DROP
CONSTRAINT [FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[ZNodeReferralCommission]'
GO
ALTER TABLE [dbo].[ZNodeReferralCommission] DROP
CONSTRAINT [FK_ZnodeReferralCommission_ZnodeReferralCommissionType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[ZNodeSavedCartLineItem]'
GO
ALTER TABLE [dbo].[ZNodeSavedCartLineItem] DROP
CONSTRAINT [FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodePromotion]'
GO
ALTER TABLE [dbo].[ZNodePromotion] ADD
[EnableCouponUrl] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodePromotion table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_Get_List

AS


				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl]
				FROM
					[dbo].[ZNodePromotion]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodePromotion table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_Insert
(

	@PromotionID int    OUTPUT,

	@Name nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@ProfileID int   ,

	@AccountID int   ,

	@ProductID int   ,

	@AddOnValueID int   ,

	@SKUID int   ,

	@DiscountTypeID int   ,

	@Discount decimal (18, 2)  ,

	@StartDate datetime   ,

	@EndDate datetime   ,

	@CouponInd bit   ,

	@CouponCode nvarchar (MAX)  ,

	@CouponQuantityAvailable int   ,

	@PromotionMessage nvarchar (MAX)  ,

	@OrderMinimum money   ,

	@QuantityMinimum int   ,

	@PromotionProductQty int   ,

	@PromotionProductID int   ,

	@DisplayOrder int   ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@EnableCouponUrl bit   
)
AS


				
				INSERT INTO [dbo].[ZNodePromotion]
					(
					[Name]
					,[Description]
					,[ProfileID]
					,[AccountID]
					,[ProductID]
					,[AddOnValueID]
					,[SKUID]
					,[DiscountTypeID]
					,[Discount]
					,[StartDate]
					,[EndDate]
					,[CouponInd]
					,[CouponCode]
					,[CouponQuantityAvailable]
					,[PromotionMessage]
					,[OrderMinimum]
					,[QuantityMinimum]
					,[PromotionProductQty]
					,[PromotionProductID]
					,[DisplayOrder]
					,[Custom1]
					,[Custom2]
					,[Custom3]
					,[EnableCouponUrl]
					)
				VALUES
					(
					@Name
					,@Description
					,@ProfileID
					,@AccountID
					,@ProductID
					,@AddOnValueID
					,@SKUID
					,@DiscountTypeID
					,@Discount
					,@StartDate
					,@EndDate
					,@CouponInd
					,@CouponCode
					,@CouponQuantityAvailable
					,@PromotionMessage
					,@OrderMinimum
					,@QuantityMinimum
					,@PromotionProductQty
					,@PromotionProductID
					,@DisplayOrder
					,@Custom1
					,@Custom2
					,@Custom3
					,@EnableCouponUrl
					)
				
				-- Get the identity value
				SET @PromotionID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodePromotion table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_Update
(

	@PromotionID int   ,

	@Name nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@ProfileID int   ,

	@AccountID int   ,

	@ProductID int   ,

	@AddOnValueID int   ,

	@SKUID int   ,

	@DiscountTypeID int   ,

	@Discount decimal (18, 2)  ,

	@StartDate datetime   ,

	@EndDate datetime   ,

	@CouponInd bit   ,

	@CouponCode nvarchar (MAX)  ,

	@CouponQuantityAvailable int   ,

	@PromotionMessage nvarchar (MAX)  ,

	@OrderMinimum money   ,

	@QuantityMinimum int   ,

	@PromotionProductQty int   ,

	@PromotionProductID int   ,

	@DisplayOrder int   ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@EnableCouponUrl bit   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodePromotion]
				SET
					[Name] = @Name
					,[Description] = @Description
					,[ProfileID] = @ProfileID
					,[AccountID] = @AccountID
					,[ProductID] = @ProductID
					,[AddOnValueID] = @AddOnValueID
					,[SKUID] = @SKUID
					,[DiscountTypeID] = @DiscountTypeID
					,[Discount] = @Discount
					,[StartDate] = @StartDate
					,[EndDate] = @EndDate
					,[CouponInd] = @CouponInd
					,[CouponCode] = @CouponCode
					,[CouponQuantityAvailable] = @CouponQuantityAvailable
					,[PromotionMessage] = @PromotionMessage
					,[OrderMinimum] = @OrderMinimum
					,[QuantityMinimum] = @QuantityMinimum
					,[PromotionProductQty] = @PromotionProductQty
					,[PromotionProductID] = @PromotionProductID
					,[DisplayOrder] = @DisplayOrder
					,[Custom1] = @Custom1
					,[Custom2] = @Custom2
					,[Custom3] = @Custom3
					,[EnableCouponUrl] = @EnableCouponUrl
				WHERE
[PromotionID] = @PromotionID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByAccountID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByAccountID
(

	@AccountID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[AccountID] = @AccountID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByAddOnValueID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByAddOnValueID
(

	@AddOnValueID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[AddOnValueID] = @AddOnValueID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByDiscountTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByDiscountTypeID
(

	@DiscountTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[DiscountTypeID] = @DiscountTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByProductID
(

	@ProductID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[ProductID] = @ProductID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByProfileID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByProfileID
(

	@ProfileID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[ProfileID] = @ProfileID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetBySKUID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetBySKUID
(

	@SKUID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[SKUID] = @SKUID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByPromotionID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByPromotionID
(

	@PromotionID int   
)
AS


				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[PromotionID] = @PromotionID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodePromotion table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_Find
(

	@SearchUsingOR bit   = null ,

	@PromotionID int   = null ,

	@Name nvarchar (MAX)  = null ,

	@Description nvarchar (MAX)  = null ,

	@ProfileID int   = null ,

	@AccountID int   = null ,

	@ProductID int   = null ,

	@AddOnValueID int   = null ,

	@SKUID int   = null ,

	@DiscountTypeID int   = null ,

	@Discount decimal (18, 2)  = null ,

	@StartDate datetime   = null ,

	@EndDate datetime   = null ,

	@CouponInd bit   = null ,

	@CouponCode nvarchar (MAX)  = null ,

	@CouponQuantityAvailable int   = null ,

	@PromotionMessage nvarchar (MAX)  = null ,

	@OrderMinimum money   = null ,

	@QuantityMinimum int   = null ,

	@PromotionProductQty int   = null ,

	@PromotionProductID int   = null ,

	@DisplayOrder int   = null ,

	@Custom1 nvarchar (MAX)  = null ,

	@Custom2 nvarchar (MAX)  = null ,

	@Custom3 nvarchar (MAX)  = null ,

	@EnableCouponUrl bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PromotionID]
	, [Name]
	, [Description]
	, [ProfileID]
	, [AccountID]
	, [ProductID]
	, [AddOnValueID]
	, [SKUID]
	, [DiscountTypeID]
	, [Discount]
	, [StartDate]
	, [EndDate]
	, [CouponInd]
	, [CouponCode]
	, [CouponQuantityAvailable]
	, [PromotionMessage]
	, [OrderMinimum]
	, [QuantityMinimum]
	, [PromotionProductQty]
	, [PromotionProductID]
	, [DisplayOrder]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [EnableCouponUrl]
    FROM
	[dbo].[ZNodePromotion]
    WHERE 
	 ([PromotionID] = @PromotionID OR @PromotionID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([ProfileID] = @ProfileID OR @ProfileID IS NULL)
	AND ([AccountID] = @AccountID OR @AccountID IS NULL)
	AND ([ProductID] = @ProductID OR @ProductID IS NULL)
	AND ([AddOnValueID] = @AddOnValueID OR @AddOnValueID IS NULL)
	AND ([SKUID] = @SKUID OR @SKUID IS NULL)
	AND ([DiscountTypeID] = @DiscountTypeID OR @DiscountTypeID IS NULL)
	AND ([Discount] = @Discount OR @Discount IS NULL)
	AND ([StartDate] = @StartDate OR @StartDate IS NULL)
	AND ([EndDate] = @EndDate OR @EndDate IS NULL)
	AND ([CouponInd] = @CouponInd OR @CouponInd IS NULL)
	AND ([CouponCode] = @CouponCode OR @CouponCode IS NULL)
	AND ([CouponQuantityAvailable] = @CouponQuantityAvailable OR @CouponQuantityAvailable IS NULL)
	AND ([PromotionMessage] = @PromotionMessage OR @PromotionMessage IS NULL)
	AND ([OrderMinimum] = @OrderMinimum OR @OrderMinimum IS NULL)
	AND ([QuantityMinimum] = @QuantityMinimum OR @QuantityMinimum IS NULL)
	AND ([PromotionProductQty] = @PromotionProductQty OR @PromotionProductQty IS NULL)
	AND ([PromotionProductID] = @PromotionProductID OR @PromotionProductID IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([Custom1] = @Custom1 OR @Custom1 IS NULL)
	AND ([Custom2] = @Custom2 OR @Custom2 IS NULL)
	AND ([Custom3] = @Custom3 OR @Custom3 IS NULL)
	AND ([EnableCouponUrl] = @EnableCouponUrl OR @EnableCouponUrl IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PromotionID]
	, [Name]
	, [Description]
	, [ProfileID]
	, [AccountID]
	, [ProductID]
	, [AddOnValueID]
	, [SKUID]
	, [DiscountTypeID]
	, [Discount]
	, [StartDate]
	, [EndDate]
	, [CouponInd]
	, [CouponCode]
	, [CouponQuantityAvailable]
	, [PromotionMessage]
	, [OrderMinimum]
	, [QuantityMinimum]
	, [PromotionProductQty]
	, [PromotionProductID]
	, [DisplayOrder]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [EnableCouponUrl]
    FROM
	[dbo].[ZNodePromotion]
    WHERE 
	 ([PromotionID] = @PromotionID AND @PromotionID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([ProfileID] = @ProfileID AND @ProfileID is not null)
	OR ([AccountID] = @AccountID AND @AccountID is not null)
	OR ([ProductID] = @ProductID AND @ProductID is not null)
	OR ([AddOnValueID] = @AddOnValueID AND @AddOnValueID is not null)
	OR ([SKUID] = @SKUID AND @SKUID is not null)
	OR ([DiscountTypeID] = @DiscountTypeID AND @DiscountTypeID is not null)
	OR ([Discount] = @Discount AND @Discount is not null)
	OR ([StartDate] = @StartDate AND @StartDate is not null)
	OR ([EndDate] = @EndDate AND @EndDate is not null)
	OR ([CouponInd] = @CouponInd AND @CouponInd is not null)
	OR ([CouponCode] = @CouponCode AND @CouponCode is not null)
	OR ([CouponQuantityAvailable] = @CouponQuantityAvailable AND @CouponQuantityAvailable is not null)
	OR ([PromotionMessage] = @PromotionMessage AND @PromotionMessage is not null)
	OR ([OrderMinimum] = @OrderMinimum AND @OrderMinimum is not null)
	OR ([QuantityMinimum] = @QuantityMinimum AND @QuantityMinimum is not null)
	OR ([PromotionProductQty] = @PromotionProductQty AND @PromotionProductQty is not null)
	OR ([PromotionProductID] = @PromotionProductID AND @PromotionProductID is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([Custom1] = @Custom1 AND @Custom1 is not null)
	OR ([Custom2] = @Custom2 AND @Custom2 is not null)
	OR ([Custom3] = @Custom3 AND @Custom3 is not null)
	OR ([EnableCouponUrl] = @EnableCouponUrl AND @EnableCouponUrl is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[TagProductView]'
GO
EXEC sp_refreshview N'[dbo].[TagProductView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeProductType]'
GO
ALTER TABLE [dbo].[ZNodeProductType] ADD
[Franchisable] [bit] NOT NULL CONSTRAINT [DF_ZNodeProductType_Franchisable] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProductType_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeProductType table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProductType_Update
(

	@ProductTypeId int   ,

	@Name nvarchar (100)  ,

	@Description nvarchar (MAX)  ,

	@DisplayOrder int   ,

	@PortalID int   ,

	@IsGiftCard bit   ,

	@Franchisable bit   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeProductType]
				SET
					[Name] = @Name
					,[Description] = @Description
					,[DisplayOrder] = @DisplayOrder
					,[PortalID] = @PortalID
					,[IsGiftCard] = @IsGiftCard
					,[Franchisable] = @Franchisable
				WHERE
[ProductTypeId] = @ProductTypeId 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProductType_GetByPortalID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProductType table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProductType_GetByPortalID
(

	@PortalID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ProductTypeId],
					[Name],
					[Description],
					[DisplayOrder],
					[PortalID],
					[IsGiftCard],
					[Franchisable]
				FROM
					[dbo].[ZNodeProductType]
				WHERE
					[PortalID] = @PortalID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProductType_GetByName]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProductType table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProductType_GetByName
(

	@Name nvarchar (100)  
)
AS


				SELECT
					[ProductTypeId],
					[Name],
					[Description],
					[DisplayOrder],
					[PortalID],
					[IsGiftCard],
					[Franchisable]
				FROM
					[dbo].[ZNodeProductType]
				WHERE
					[Name] = @Name
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProductType_GetByProductTypeId]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProductType table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProductType_GetByProductTypeId
(

	@ProductTypeId int   
)
AS


				SELECT
					[ProductTypeId],
					[Name],
					[Description],
					[DisplayOrder],
					[PortalID],
					[IsGiftCard],
					[Franchisable]
				FROM
					[dbo].[ZNodeProductType]
				WHERE
					[ProductTypeId] = @ProductTypeId
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodePortal]'
GO
ALTER TABLE [dbo].[ZNodePortal] ADD
[DefaultProductReviewStateID] [int] NULL,
[UseDynamicDisplayOrder] [bit] NULL CONSTRAINT [DF_ZNodePortal_UseDynamicDisplayOrder] DEFAULT ((0)),
[EnableAddressValidation] [bit] NULL,
[RequireValidatedAddress] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProductType_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeProductType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProductType_Find
(

	@SearchUsingOR bit   = null ,

	@ProductTypeId int   = null ,

	@Name nvarchar (100)  = null ,

	@Description nvarchar (MAX)  = null ,

	@DisplayOrder int   = null ,

	@PortalID int   = null ,

	@IsGiftCard bit   = null ,

	@Franchisable bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ProductTypeId]
	, [Name]
	, [Description]
	, [DisplayOrder]
	, [PortalID]
	, [IsGiftCard]
	, [Franchisable]
    FROM
	[dbo].[ZNodeProductType]
    WHERE 
	 ([ProductTypeId] = @ProductTypeId OR @ProductTypeId IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([PortalID] = @PortalID OR @PortalID IS NULL)
	AND ([IsGiftCard] = @IsGiftCard OR @IsGiftCard IS NULL)
	AND ([Franchisable] = @Franchisable OR @Franchisable IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ProductTypeId]
	, [Name]
	, [Description]
	, [DisplayOrder]
	, [PortalID]
	, [IsGiftCard]
	, [Franchisable]
    FROM
	[dbo].[ZNodeProductType]
    WHERE 
	 ([ProductTypeId] = @ProductTypeId AND @ProductTypeId is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([PortalID] = @PortalID AND @PortalID is not null)
	OR ([IsGiftCard] = @IsGiftCard AND @IsGiftCard is not null)
	OR ([Franchisable] = @Franchisable AND @Franchisable is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeProduct]'
GO
ALTER TABLE [dbo].[ZNodeProduct] ADD
[ExpirationPeriod] [int] NULL,
[ExpirationFrequency] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ProductsView]'
GO
EXEC sp_refreshview N'[dbo].[ProductsView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeOrderLineItem]'
GO
ALTER TABLE [dbo].[ZNodeOrderLineItem] ADD
[OrderLineItemStateID] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeOrderLineItem table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_Get_List

AS


				
				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3],
					[OrderLineItemStateID]
				FROM
					[dbo].[ZNodeOrderLineItem]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductsByCatalogID]'
GO
SET ANSI_NULLS ON
GO


ALTER PROCEDURE [dbo].[ZNode_GetProductsByCatalogID] 
(@CatalogId int)    
As     
Begin    
SELECT [ProductID]  
      ,[Name]
      ,[ShortDescription]  
      ,[Description]  
      ,[FeaturesDesc]  
      ,[ProductNum]  
      ,[ProductTypeID]  
      ,[RetailPrice]  
      ,[SalePrice]  
      ,[WholesalePrice]  
      ,[ImageFile]  
      ,[ImageAltTag]  
      ,[Weight]  
      ,[Length]  
      ,[Width]  
      ,[Height]  
      ,[BeginActiveDate]  
      ,[EndActiveDate]  
      ,[DisplayOrder]  
      ,[ActiveInd]  
      ,[CallForPricing]  
      ,[HomepageSpecial]  
      ,[CategorySpecial]  
      ,[InventoryDisplay]  
      ,[Keywords]  
      ,[ManufacturerID]  
      ,[AdditionalInfoLink]  
      ,[AdditionalInfoLinkLabel]  
      ,[ShippingRuleTypeID]  
      ,[SEOTitle]  
      ,[SEOKeywords]  
      ,[SEODescription]  
      ,[Custom1]  
      ,[Custom2]  
      ,[Custom3]  
      ,[ShipEachItemSeparately]      
      ,[AllowBackOrder]  
      ,[BackOrderMsg]  
      ,[DropShipInd]  
      ,[DropShipEmailID]  
      ,[Specifications]  
      ,[AdditionalInformation]  
      ,[InStockMsg]  
      ,[OutOfStockMsg]  
      ,[TrackInventoryInd]  
      ,[DownloadLink]  
      ,[FreeShippingInd]  
      ,[NewProductInd]  
      ,[SEOURL]  
      ,[MaxQty]  
      ,[ShipSeparately]  
      ,[FeaturedInd]  
      ,[WebServiceDownloadDte]  
      ,[UpdateDte]  
      ,[SupplierID]  
      ,[RecurringBillingInd]  
      ,[RecurringBillingInstallmentInd]  
      ,[RecurringBillingPeriod]  
      ,[RecurringBillingFrequency]  
      ,[RecurringBillingTotalCycles]  
      ,[RecurringBillingInitialAmount]  
      ,[TaxClassID] 
      ,[PortalID]
      ,[ReviewStateID]
      ,[Franchisable]
      ,[ExpirationPeriod]
      ,[ExpirationFrequency]
  FROM [ZNodeProduct]  
 WHERE ProductID in    
  (SELECT ProductID   
     FROM ZNodeProductCategory   
    WHERE CategoryID in    
    (SELECT CategoryID   
       FROM ZNodeCategoryNode   
      WHERE CatalogID = @CatalogId)) 
End       
  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_Insert]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeOrderLineItem table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_Insert
(

	@OrderLineItemID int    OUTPUT,

	@OrderID int   ,

	@ShipmentID int   ,

	@ProductNum nvarchar (MAX)  ,

	@Name nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@Quantity int   ,

	@Price money   ,

	@Weight decimal (18, 2)  ,

	@SKU nvarchar (MAX)  ,

	@ParentOrderLineItemID int   ,

	@OrderLineItemRelationshipTypeID int   ,

	@DownloadLink nvarchar (MAX)  ,

	@DiscountAmount money   ,

	@ShipSeparately bit   ,

	@ShipDate datetime   ,

	@ReturnDate datetime   ,

	@ShippingCost money   ,

	@PromoDescription nvarchar (MAX)  ,

	@SalesTax money   ,

	@VAT money   ,

	@GST money   ,

	@PST money   ,

	@HST money   ,

	@TransactionNumber nvarchar (MAX)  ,

	@PaymentStatusID int   ,

	@TrackingNumber nvarchar (MAX)  ,

	@AutoGeneratedTracking bit   ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@OrderLineItemStateID int   
)
AS


				
				INSERT INTO [dbo].[ZNodeOrderLineItem]
					(
					[OrderID]
					,[ShipmentID]
					,[ProductNum]
					,[Name]
					,[Description]
					,[Quantity]
					,[Price]
					,[Weight]
					,[SKU]
					,[ParentOrderLineItemID]
					,[OrderLineItemRelationshipTypeID]
					,[DownloadLink]
					,[DiscountAmount]
					,[ShipSeparately]
					,[ShipDate]
					,[ReturnDate]
					,[ShippingCost]
					,[PromoDescription]
					,[SalesTax]
					,[VAT]
					,[GST]
					,[PST]
					,[HST]
					,[TransactionNumber]
					,[PaymentStatusID]
					,[TrackingNumber]
					,[AutoGeneratedTracking]
					,[Custom1]
					,[Custom2]
					,[Custom3]
					,[OrderLineItemStateID]
					)
				VALUES
					(
					@OrderID
					,@ShipmentID
					,@ProductNum
					,@Name
					,@Description
					,@Quantity
					,@Price
					,@Weight
					,@SKU
					,@ParentOrderLineItemID
					,@OrderLineItemRelationshipTypeID
					,@DownloadLink
					,@DiscountAmount
					,@ShipSeparately
					,@ShipDate
					,@ReturnDate
					,@ShippingCost
					,@PromoDescription
					,@SalesTax
					,@VAT
					,@GST
					,@PST
					,@HST
					,@TransactionNumber
					,@PaymentStatusID
					,@TrackingNumber
					,@AutoGeneratedTracking
					,@Custom1
					,@Custom2
					,@Custom3
					,@OrderLineItemStateID
					)
				
				-- Get the identity value
				SET @OrderLineItemID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeMessageConfig]'
GO
ALTER TABLE [dbo].[ZNodeMessageConfig] ADD
[PageSEOName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeContentPage]'
GO
ALTER TABLE [dbo].[ZNodeContentPage] ADD
[MetaTagAdditional] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeProduct table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_Update
(

	@ProductID int   ,

	@Name nvarchar (MAX)  ,

	@ShortDescription nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@FeaturesDesc nvarchar (MAX)  ,

	@ProductNum nvarchar (100)  ,

	@ProductTypeID int   ,

	@RetailPrice money   ,

	@SalePrice money   ,

	@WholesalePrice money   ,

	@ImageFile nvarchar (MAX)  ,

	@ImageAltTag nvarchar (MAX)  ,

	@Weight decimal (18, 2)  ,

	@Length decimal (18, 2)  ,

	@Width decimal (18, 2)  ,

	@Height decimal (18, 2)  ,

	@BeginActiveDate datetime   ,

	@EndActiveDate datetime   ,

	@DisplayOrder int   ,

	@ActiveInd bit   ,

	@CallForPricing bit   ,

	@HomepageSpecial bit   ,

	@CategorySpecial bit   ,

	@InventoryDisplay tinyint   ,

	@Keywords nvarchar (MAX)  ,

	@ManufacturerID int   ,

	@AdditionalInfoLink nvarchar (MAX)  ,

	@AdditionalInfoLinkLabel nvarchar (MAX)  ,

	@ShippingRuleTypeID int   ,

	@ShippingRate money   ,

	@SEOTitle nvarchar (MAX)  ,

	@SEOKeywords nvarchar (MAX)  ,

	@SEODescription nvarchar (MAX)  ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@ShipEachItemSeparately bit   ,

	@AllowBackOrder bit   ,

	@BackOrderMsg nvarchar (MAX)  ,

	@DropShipInd bit   ,

	@DropShipEmailID nvarchar (MAX)  ,

	@Specifications nvarchar (MAX)  ,

	@AdditionalInformation nvarchar (MAX)  ,

	@InStockMsg nvarchar (MAX)  ,

	@OutOfStockMsg nvarchar (MAX)  ,

	@TrackInventoryInd bit   ,

	@DownloadLink nvarchar (MAX)  ,

	@FreeShippingInd bit   ,

	@NewProductInd bit   ,

	@SEOURL nvarchar (100)  ,

	@MaxQty int   ,

	@ShipSeparately bit   ,

	@FeaturedInd bit   ,

	@WebServiceDownloadDte datetime   ,

	@UpdateDte datetime   ,

	@SupplierID int   ,

	@RecurringBillingInd bit   ,

	@RecurringBillingInstallmentInd bit   ,

	@RecurringBillingPeriod nvarchar (50)  ,

	@RecurringBillingFrequency nvarchar (MAX)  ,

	@RecurringBillingTotalCycles int   ,

	@RecurringBillingInitialAmount money   ,

	@TaxClassID int   ,

	@MinQty int   ,

	@ReviewStateID int   ,

	@AffiliateUrl varchar (512)  ,

	@IsShippable bit   ,

	@AccountID int   ,

	@PortalID int   ,

	@Franchisable bit   ,

	@ExpirationPeriod int   ,

	@ExpirationFrequency int   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeProduct]
				SET
					[Name] = @Name
					,[ShortDescription] = @ShortDescription
					,[Description] = @Description
					,[FeaturesDesc] = @FeaturesDesc
					,[ProductNum] = @ProductNum
					,[ProductTypeID] = @ProductTypeID
					,[RetailPrice] = @RetailPrice
					,[SalePrice] = @SalePrice
					,[WholesalePrice] = @WholesalePrice
					,[ImageFile] = @ImageFile
					,[ImageAltTag] = @ImageAltTag
					,[Weight] = @Weight
					,[Length] = @Length
					,[Width] = @Width
					,[Height] = @Height
					,[BeginActiveDate] = @BeginActiveDate
					,[EndActiveDate] = @EndActiveDate
					,[DisplayOrder] = @DisplayOrder
					,[ActiveInd] = @ActiveInd
					,[CallForPricing] = @CallForPricing
					,[HomepageSpecial] = @HomepageSpecial
					,[CategorySpecial] = @CategorySpecial
					,[InventoryDisplay] = @InventoryDisplay
					,[Keywords] = @Keywords
					,[ManufacturerID] = @ManufacturerID
					,[AdditionalInfoLink] = @AdditionalInfoLink
					,[AdditionalInfoLinkLabel] = @AdditionalInfoLinkLabel
					,[ShippingRuleTypeID] = @ShippingRuleTypeID
					,[ShippingRate] = @ShippingRate
					,[SEOTitle] = @SEOTitle
					,[SEOKeywords] = @SEOKeywords
					,[SEODescription] = @SEODescription
					,[Custom1] = @Custom1
					,[Custom2] = @Custom2
					,[Custom3] = @Custom3
					,[ShipEachItemSeparately] = @ShipEachItemSeparately
					,[AllowBackOrder] = @AllowBackOrder
					,[BackOrderMsg] = @BackOrderMsg
					,[DropShipInd] = @DropShipInd
					,[DropShipEmailID] = @DropShipEmailID
					,[Specifications] = @Specifications
					,[AdditionalInformation] = @AdditionalInformation
					,[InStockMsg] = @InStockMsg
					,[OutOfStockMsg] = @OutOfStockMsg
					,[TrackInventoryInd] = @TrackInventoryInd
					,[DownloadLink] = @DownloadLink
					,[FreeShippingInd] = @FreeShippingInd
					,[NewProductInd] = @NewProductInd
					,[SEOURL] = @SEOURL
					,[MaxQty] = @MaxQty
					,[ShipSeparately] = @ShipSeparately
					,[FeaturedInd] = @FeaturedInd
					,[WebServiceDownloadDte] = @WebServiceDownloadDte
					,[UpdateDte] = @UpdateDte
					,[SupplierID] = @SupplierID
					,[RecurringBillingInd] = @RecurringBillingInd
					,[RecurringBillingInstallmentInd] = @RecurringBillingInstallmentInd
					,[RecurringBillingPeriod] = @RecurringBillingPeriod
					,[RecurringBillingFrequency] = @RecurringBillingFrequency
					,[RecurringBillingTotalCycles] = @RecurringBillingTotalCycles
					,[RecurringBillingInitialAmount] = @RecurringBillingInitialAmount
					,[TaxClassID] = @TaxClassID
					,[MinQty] = @MinQty
					,[ReviewStateID] = @ReviewStateID
					,[AffiliateUrl] = @AffiliateUrl
					,[IsShippable] = @IsShippable
					,[AccountID] = @AccountID
					,[PortalID] = @PortalID
					,[Franchisable] = @Franchisable
					,[ExpirationPeriod] = @ExpirationPeriod
					,[ExpirationFrequency] = @ExpirationFrequency
				WHERE
[ProductID] = @ProductID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeProduct table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_Insert
(

	@ProductID int    OUTPUT,

	@Name nvarchar (MAX)  ,

	@ShortDescription nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@FeaturesDesc nvarchar (MAX)  ,

	@ProductNum nvarchar (100)  ,

	@ProductTypeID int   ,

	@RetailPrice money   ,

	@SalePrice money   ,

	@WholesalePrice money   ,

	@ImageFile nvarchar (MAX)  ,

	@ImageAltTag nvarchar (MAX)  ,

	@Weight decimal (18, 2)  ,

	@Length decimal (18, 2)  ,

	@Width decimal (18, 2)  ,

	@Height decimal (18, 2)  ,

	@BeginActiveDate datetime   ,

	@EndActiveDate datetime   ,

	@DisplayOrder int   ,

	@ActiveInd bit   ,

	@CallForPricing bit   ,

	@HomepageSpecial bit   ,

	@CategorySpecial bit   ,

	@InventoryDisplay tinyint   ,

	@Keywords nvarchar (MAX)  ,

	@ManufacturerID int   ,

	@AdditionalInfoLink nvarchar (MAX)  ,

	@AdditionalInfoLinkLabel nvarchar (MAX)  ,

	@ShippingRuleTypeID int   ,

	@ShippingRate money   ,

	@SEOTitle nvarchar (MAX)  ,

	@SEOKeywords nvarchar (MAX)  ,

	@SEODescription nvarchar (MAX)  ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@ShipEachItemSeparately bit   ,

	@AllowBackOrder bit   ,

	@BackOrderMsg nvarchar (MAX)  ,

	@DropShipInd bit   ,

	@DropShipEmailID nvarchar (MAX)  ,

	@Specifications nvarchar (MAX)  ,

	@AdditionalInformation nvarchar (MAX)  ,

	@InStockMsg nvarchar (MAX)  ,

	@OutOfStockMsg nvarchar (MAX)  ,

	@TrackInventoryInd bit   ,

	@DownloadLink nvarchar (MAX)  ,

	@FreeShippingInd bit   ,

	@NewProductInd bit   ,

	@SEOURL nvarchar (100)  ,

	@MaxQty int   ,

	@ShipSeparately bit   ,

	@FeaturedInd bit   ,

	@WebServiceDownloadDte datetime   ,

	@UpdateDte datetime   ,

	@SupplierID int   ,

	@RecurringBillingInd bit   ,

	@RecurringBillingInstallmentInd bit   ,

	@RecurringBillingPeriod nvarchar (50)  ,

	@RecurringBillingFrequency nvarchar (MAX)  ,

	@RecurringBillingTotalCycles int   ,

	@RecurringBillingInitialAmount money   ,

	@TaxClassID int   ,

	@MinQty int   ,

	@ReviewStateID int   ,

	@AffiliateUrl varchar (512)  ,

	@IsShippable bit   ,

	@AccountID int   ,

	@PortalID int   ,

	@Franchisable bit   ,

	@ExpirationPeriod int   ,

	@ExpirationFrequency int   
)
AS


				
				INSERT INTO [dbo].[ZNodeProduct]
					(
					[Name]
					,[ShortDescription]
					,[Description]
					,[FeaturesDesc]
					,[ProductNum]
					,[ProductTypeID]
					,[RetailPrice]
					,[SalePrice]
					,[WholesalePrice]
					,[ImageFile]
					,[ImageAltTag]
					,[Weight]
					,[Length]
					,[Width]
					,[Height]
					,[BeginActiveDate]
					,[EndActiveDate]
					,[DisplayOrder]
					,[ActiveInd]
					,[CallForPricing]
					,[HomepageSpecial]
					,[CategorySpecial]
					,[InventoryDisplay]
					,[Keywords]
					,[ManufacturerID]
					,[AdditionalInfoLink]
					,[AdditionalInfoLinkLabel]
					,[ShippingRuleTypeID]
					,[ShippingRate]
					,[SEOTitle]
					,[SEOKeywords]
					,[SEODescription]
					,[Custom1]
					,[Custom2]
					,[Custom3]
					,[ShipEachItemSeparately]
					,[AllowBackOrder]
					,[BackOrderMsg]
					,[DropShipInd]
					,[DropShipEmailID]
					,[Specifications]
					,[AdditionalInformation]
					,[InStockMsg]
					,[OutOfStockMsg]
					,[TrackInventoryInd]
					,[DownloadLink]
					,[FreeShippingInd]
					,[NewProductInd]
					,[SEOURL]
					,[MaxQty]
					,[ShipSeparately]
					,[FeaturedInd]
					,[WebServiceDownloadDte]
					,[UpdateDte]
					,[SupplierID]
					,[RecurringBillingInd]
					,[RecurringBillingInstallmentInd]
					,[RecurringBillingPeriod]
					,[RecurringBillingFrequency]
					,[RecurringBillingTotalCycles]
					,[RecurringBillingInitialAmount]
					,[TaxClassID]
					,[MinQty]
					,[ReviewStateID]
					,[AffiliateUrl]
					,[IsShippable]
					,[AccountID]
					,[PortalID]
					,[Franchisable]
					,[ExpirationPeriod]
					,[ExpirationFrequency]
					)
				VALUES
					(
					@Name
					,@ShortDescription
					,@Description
					,@FeaturesDesc
					,@ProductNum
					,@ProductTypeID
					,@RetailPrice
					,@SalePrice
					,@WholesalePrice
					,@ImageFile
					,@ImageAltTag
					,@Weight
					,@Length
					,@Width
					,@Height
					,@BeginActiveDate
					,@EndActiveDate
					,@DisplayOrder
					,@ActiveInd
					,@CallForPricing
					,@HomepageSpecial
					,@CategorySpecial
					,@InventoryDisplay
					,@Keywords
					,@ManufacturerID
					,@AdditionalInfoLink
					,@AdditionalInfoLinkLabel
					,@ShippingRuleTypeID
					,@ShippingRate
					,@SEOTitle
					,@SEOKeywords
					,@SEODescription
					,@Custom1
					,@Custom2
					,@Custom3
					,@ShipEachItemSeparately
					,@AllowBackOrder
					,@BackOrderMsg
					,@DropShipInd
					,@DropShipEmailID
					,@Specifications
					,@AdditionalInformation
					,@InStockMsg
					,@OutOfStockMsg
					,@TrackInventoryInd
					,@DownloadLink
					,@FreeShippingInd
					,@NewProductInd
					,@SEOURL
					,@MaxQty
					,@ShipSeparately
					,@FeaturedInd
					,@WebServiceDownloadDte
					,@UpdateDte
					,@SupplierID
					,@RecurringBillingInd
					,@RecurringBillingInstallmentInd
					,@RecurringBillingPeriod
					,@RecurringBillingFrequency
					,@RecurringBillingTotalCycles
					,@RecurringBillingInitialAmount
					,@TaxClassID
					,@MinQty
					,@ReviewStateID
					,@AffiliateUrl
					,@IsShippable
					,@AccountID
					,@PortalID
					,@Franchisable
					,@ExpirationPeriod
					,@ExpirationFrequency
					)
				
				-- Get the identity value
				SET @ProductID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeOrderLineItem table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_Update
(

	@OrderLineItemID int   ,

	@OrderID int   ,

	@ShipmentID int   ,

	@ProductNum nvarchar (MAX)  ,

	@Name nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@Quantity int   ,

	@Price money   ,

	@Weight decimal (18, 2)  ,

	@SKU nvarchar (MAX)  ,

	@ParentOrderLineItemID int   ,

	@OrderLineItemRelationshipTypeID int   ,

	@DownloadLink nvarchar (MAX)  ,

	@DiscountAmount money   ,

	@ShipSeparately bit   ,

	@ShipDate datetime   ,

	@ReturnDate datetime   ,

	@ShippingCost money   ,

	@PromoDescription nvarchar (MAX)  ,

	@SalesTax money   ,

	@VAT money   ,

	@GST money   ,

	@PST money   ,

	@HST money   ,

	@TransactionNumber nvarchar (MAX)  ,

	@PaymentStatusID int   ,

	@TrackingNumber nvarchar (MAX)  ,

	@AutoGeneratedTracking bit   ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@OrderLineItemStateID int   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeOrderLineItem]
				SET
					[OrderID] = @OrderID
					,[ShipmentID] = @ShipmentID
					,[ProductNum] = @ProductNum
					,[Name] = @Name
					,[Description] = @Description
					,[Quantity] = @Quantity
					,[Price] = @Price
					,[Weight] = @Weight
					,[SKU] = @SKU
					,[ParentOrderLineItemID] = @ParentOrderLineItemID
					,[OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID
					,[DownloadLink] = @DownloadLink
					,[DiscountAmount] = @DiscountAmount
					,[ShipSeparately] = @ShipSeparately
					,[ShipDate] = @ShipDate
					,[ReturnDate] = @ReturnDate
					,[ShippingCost] = @ShippingCost
					,[PromoDescription] = @PromoDescription
					,[SalesTax] = @SalesTax
					,[VAT] = @VAT
					,[GST] = @GST
					,[PST] = @PST
					,[HST] = @HST
					,[TransactionNumber] = @TransactionNumber
					,[PaymentStatusID] = @PaymentStatusID
					,[TrackingNumber] = @TrackingNumber
					,[AutoGeneratedTracking] = @AutoGeneratedTracking
					,[Custom1] = @Custom1
					,[Custom2] = @Custom2
					,[Custom3] = @Custom3
					,[OrderLineItemStateID] = @OrderLineItemStateID
				WHERE
[OrderLineItemID] = @OrderLineItemID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetVendorOrderLineItemsByOrderId]'
GO
SET ANSI_NULLS ON
GO

  
/*  
----------------------------------------------------------------------------------------------------  
  
-- Created By: Znode Inc (http://www.znode.com)  
-- Purpose: Select records from the ZNodeOrderLineItem table through a foreign key  
----------------------------------------------------------------------------------------------------  
*/  
  
  
CREATE PROCEDURE [dbo].[ZNode_GetVendorOrderLineItemsByOrderId]
(  
 @OrderID int,
 @LoggedAccountId nvarchar(MAX) = null
)  
AS  
  
  
    SET ANSI_NULLS OFF  
      
    SELECT  
     ZOI.OrderLineItemID,  
     ZOI.OrderID,  
     ZOI.ShipmentID,  
     ZOI.ProductNum,  
     ZOI.Name,  
     ZOI.Description,  
     ZOI.Quantity,  
     ZOI.Price,  
     ZOI.Weight,  
     ZOI.SKU,  
     ZOI.ParentOrderLineItemID,  
     ZOI.OrderLineItemRelationshipTypeID,  
     ZOI.DownloadLink,  
     ZOI.DiscountAmount,  
     ZOI.ShipSeparately,  
     ZOI.ShipDate,  
     ZOI.ReturnDate,  
     ZOI.ShippingCost,  
     ZOI.PromoDescription,  
     ZOI.SalesTax,  
     ZOI.VAT,  
     ZOI.GST,  
     ZOI.PST,  
     ZOI.HST,  
     ZOI.TransactionNumber,  
     ZOI.PaymentStatusID,  
     ZOI.TrackingNumber,  
     ZOI.AutoGeneratedTracking,  
     ZOI.Custom1,  
     ZOI.Custom2,  
     ZOI.Custom3,
     ZOI.OrderLineItemStateID  
    FROM  
     [dbo].ZNodeOrderLineItem ZOI
     INNER JOIN ZNodeOrder ZO ON ZO.OrderID = ZOI.OrderID     
	 INNER JOIN ZNodeSKU ON ZOI.SKU = ZNodeSKU.SKU    
	 INNER JOIN ZNodeProduct ON ZNodeSKU.ProductID = ZNodeProduct.ProductID AND ZNodeProduct.ProductNum = ZOI.ProductNum    
     WHERE 
     ZnodeProduct.AccountID = @LoggedAccountId AND  
     ZOI.OrderID = @OrderID  
      
    SELECT @@ROWCOUNT  
    SET ANSI_NULLS ON  
     
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetVendorOrderLineItemByOrderId]'
GO

    
-- =========================================================                  
-- Description: RETURNS OrderLineItems based on the OrderId                
-- =========================================================                  
CREATE PROCEDURE [dbo].[ZNode_GetVendorOrderLineItemByOrderId](                  
@OrderId VARCHAR(100),      
@OrderStatusId INT = NULL ,  
@LoggedAccountId nvarchar(MAX) = null     
)      
AS                  
BEGIN                  
-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM                  
-- INTERFERING WITH SELECT STATEMENTS.                  
SET NOCOUNT ON;                
        
-- Select Query --                
SELECT DISTINCT [OrderLineItemId]  
      ,ZOI.SKU  
      ,ZOI.OrderID                         
      ,ZOI.ProductNum  
      ,ZOI.Name  
      ,(SELECT REPLACE(ZOI.Description,'<br />',' ')) Description                
      ,[Quantity]                
      ,[Price]     
      ,ZO.PortalId                 
  FROM ZNodeOrderLineItem ZOI     
  INNER JOIN ZNodeOrder ZO ON ZO.OrderID = ZOI.OrderID     
  INNER JOIN ZNodeSKU ON ZOI.SKU = ZNodeSKU.SKU    
  INNER JOIN ZNodeProduct ON ZNodeSKU.ProductID = ZNodeProduct.ProductID AND ZNodeProduct.ProductNum = ZOI.ProductNum    
  WHERE ZnodeProduct.AccountID = @LoggedAccountId AND ZOI.OrderID  in (SELECT OrderID FROM ZnodeOrder WHERE (OrderID >= @OrderId) AND OrderStateID = ISNULL(@OrderStatusId, OrderStateID))         
END; 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_GetByOrderID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetByOrderID
(

	@OrderID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3],
					[OrderLineItemStateID]
				FROM
					[dbo].[ZNodeOrderLineItem]
				WHERE
					[OrderID] = @OrderID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_GetByParentOrderLineItemID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetByParentOrderLineItemID
(

	@ParentOrderLineItemID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3],
					[OrderLineItemStateID]
				FROM
					[dbo].[ZNodeOrderLineItem]
				WHERE
					[ParentOrderLineItemID] = @ParentOrderLineItemID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_SearchVendorOrder]'
GO
SET ANSI_NULLS ON
GO

    
CREATE PROCEDURE [dbo].[ZNode_SearchVendorOrder](                  
@ORDERID int = null,                  
@BILLINGFIRSTNAME nvarchar(MAX) = null,                  
@BILLINGLASTNAME nvarchar(MAX) = null,                               
@BILLINGCOMPANYNAME nvarchar(MAX) = null,                
@ACCOUNTID nvarchar(MAX) = null,  
@STARTDATE DateTime = null,             
@ENDDATE DateTime = null,                  
@ORDERSTATEID int = null,                  
@PORTALID int = null,  
@portalIds Varchar(Max)= '0',
@LoggedAccountId nvarchar(MAX) = null)  
AS                  
BEGIN          
    
IF (@ENDDATE IS NOT NULL AND @STARTDATE IS NULL)    
BEGIN    
 SET @STARTDATE = '1/1/1901'    
END    
IF (@ENDDATE IS NULL AND @STARTDATE IS NOT NULL)    
BEGIN    
 SET @ENDDATE = GETDATE()    
END    
    
SET @ENDDATE = (SELECT DateAdd(dd, 1, @ENDDATE))    
       
SELECT Distinct ZC.OrderID                  
      ,ZC.PortalId                  
      ,ZC.AccountID                  
      ,OrderStateID                  
      ,ShippingID                    
      ,PaymentTypeId          
      ,ZC.PaymentStatusID                
      ,(SELECT OrderStateName FROM ZNodeOrderState WHERE OrderStateID = ZC.OrderStateID) AS 'OrderStatus'                        
      ,(SELECT Description FROM ZNodeShipping WHERE ShippingID = ZC.ShippingID) AS 'ShippingTypeName'                          
      ,(SELECT Name FROM ZNodePaymentType WHERE PaymentTypeID = ZC.PaymentTypeId) AS 'PaymentTypeName'                        
      ,(SELECT PaymentStatusName FROM ZNodePaymentStatus WHERE PaymentStatusID = ZC.PaymentStatusID) AS 'PaymentStatusName'                         
      ,BillingFirstName                  
      ,BillingLastName                             
      ,Total  
      ,OrderDate        
      ,ZC.ShipDate      
      ,ZC.TrackingNumber                          
FROM                    
ZNodeOrder ZC    
INNER JOIN ZnodeOrderLineItem ON ZC.OrderId = ZNodeOrderLineItem.OrderID    
INNER JOIN ZNodeSKU ON ZNodeOrderLineItem.SKU = ZNodeSKU.SKU    
INNER JOIN ZNodeProduct ON ZNodeSKU.ProductID = ZNodeProduct.ProductID AND ZNodeProduct.ProductNum = ZNodeOrderLineItem.ProductNum    
WHERE                 
(ZnodeProduct.AccountID = @LoggedAccountId OR @LoggedAccountId IS NULL)      
 AND   
 (ZC.PortalID in (SELECT [value] from sc_splitter(@portalIds,',')) OR  @portalIds = '0')  
 AND (ZC.BillingFirstName LIKE @BILLINGFIRSTNAME + '%' OR ShipFirstName LIKE @BILLINGFIRSTNAME + '%' OR @BILLINGFIRSTNAME IS NULL)          
 AND (ZC.BillingLastName LIKE @BILLINGLASTNAME + '%' OR ShipLastName LIKE @BILLINGLASTNAME + '%' OR @BILLINGLASTNAME IS NULL)          
 AND (BillingCompanyName LIKE @BILLINGCOMPANYNAME + '%' OR ShipCompanyName LIKE @BILLINGCOMPANYNAME + '%' OR @BILLINGCOMPANYNAME IS NULL)        
 AND (ZnodeOrderLineItem.OrderLineItemStateID = @ORDERID OR @ORDERID IS NULL)          
 AND (ZNodeProduct.AccountID = @AccountID OR @AccountID IS NULL)          
 AND (ZC.PortalID = @PortalID OR @PortalID IS NULL)          
 AND (ZnodeOrderLineItem.OrderLineItemStateID = @ORDERSTATEID OR (@ORDERSTATEID IS NULL) OR (@ORDERSTATEID = 1000 AND ZnodeOrderLineItem.OrderLineItemStateID IN (20,30,40)))           
 AND (ZC.OrderDate BETWEEN @STARTDATE AND @ENDDATE OR (@STARTDATE IS NULL OR @ENDDATE IS NULL))    
ORDER BY ZC.OrderID DESC    
  
  
SELECT    
 ZnodeOrderLineItem.OrderID  
 ,ZnodeOrderLineItem.OrderLineItemID  
 ,ZnodeOrderLineItem.Price  
 ,ZnodeOrderLineItem.OrderLineItemStateID  
FROM                    
ZNodeOrder ZC    
INNER JOIN ZnodeOrderLineItem ON ZC.OrderId = ZNodeOrderLineItem.OrderID    
INNER JOIN ZNodeSKU ON ZNodeOrderLineItem.SKU = ZNodeSKU.SKU    
INNER JOIN ZNodeProduct ON ZNodeSKU.ProductID = ZNodeProduct.ProductID AND ZNodeProduct.ProductNum = ZNodeOrderLineItem.ProductNum    
WHERE                 
(ZnodeProduct.AccountID = @LoggedAccountId OR @LoggedAccountId IS NULL)      
 AND   
 (ZC.PortalID in (SELECT [value] from sc_splitter(@portalIds,',')) OR  @portalIds = '0')  
 AND (ZC.BillingFirstName LIKE @BILLINGFIRSTNAME + '%' OR ShipFirstName LIKE @BILLINGFIRSTNAME + '%' OR @BILLINGFIRSTNAME IS NULL)          
 AND (ZC.BillingLastName LIKE @BILLINGLASTNAME + '%' OR ShipLastName LIKE @BILLINGLASTNAME + '%' OR @BILLINGLASTNAME IS NULL)          
 AND (BillingCompanyName LIKE @BILLINGCOMPANYNAME + '%' OR ShipCompanyName LIKE @BILLINGCOMPANYNAME + '%' OR @BILLINGCOMPANYNAME IS NULL)        
 AND (ZnodeOrderLineItem.OrderLineItemStateID = @ORDERID OR @ORDERID IS NULL)          
 AND (ZNodeProduct.AccountID = @AccountID OR @AccountID IS NULL)          
 AND (ZC.PortalID = @PortalID OR @PortalID IS NULL)          
 AND (ZnodeOrderLineItem.OrderLineItemStateID = @ORDERSTATEID OR (@ORDERSTATEID IS NULL) OR (@ORDERSTATEID = 1000 AND ZnodeOrderLineItem.OrderLineItemStateID IN (20,30,40)))
 AND (ZC.OrderDate BETWEEN @STARTDATE AND @ENDDATE OR (@STARTDATE IS NULL OR @ENDDATE IS NULL))    
ORDER BY ZNodeOrderLineItem.OrderLineItemID DESC    

END      
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_GetByOrderLineItemRelationshipTypeID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetByOrderLineItemRelationshipTypeID
(

	@OrderLineItemRelationshipTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3],
					[OrderLineItemStateID]
				FROM
					[dbo].[ZNodeOrderLineItem]
				WHERE
					[OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeOrderLineItem_GetByOrderLineItemStateID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetByOrderLineItemStateID
(

	@OrderLineItemStateID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3],
					[OrderLineItemStateID]
				FROM
					[dbo].[ZNodeOrderLineItem]
				WHERE
					[OrderLineItemStateID] = @OrderLineItemStateID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportsProductSoldOnVendorSites]'
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[ZNode_ReportsProductSoldOnVendorSites]
    (
      @FromDate DATETIME = NULL,
      @ToDate DATETIME = NULL,
      @PortalId VARCHAR(100) = '0',      
      @AccountId VARCHAR(100) = '0'      
    )
AS    
    BEGIN                              
    
    IF (@FromDate IS NULL) SET @FromDate = GETDATE()
	IF (@ToDate IS NULL) SET @ToDate = DATEADD(D,1,GETDATE())
				
    IF(@AccountId<>'0')
		BEGIN
			-- Mall Admin Product Sold On Vendor Site Reprot
			-- SET TODAY IF NO DATE CONSTRAINT PASSED
				
						
				SELECT 
				A.CompanyName,
				P.Name,
				OT.FromUrl,
				OT.ToUrl,
				OT.Price,
				OT.Date
				
			FROM   
				ZNodeAccount AS A 
				INNER JOIN ZNodeProduct AS P ON P.AccountID = A.AccountId 
				INNER JOIN ZNodeTrackingOutbound AS OT ON P.ProductID = OT.ProductID 
			WHERE  
				OT.[Date]>= @FromDate 
				AND OT.[Date]<=@ToDate
				AND (@AccountId='0'  OR  P.AccountID=@AccountId)								
		END
	ELSE
		BEGIN		
			-- Franchise Admin Product Sold On Vendor Site Reprot
			-- SET TODAY IF NO DATE CONSTRAINT PASSED
				SELECT 
				A.CompanyName AS CompanyName,
				P.Name,
				OT.FromUrl,
				OT.ToUrl,
				OT.Price
			FROM  ZNodeProduct AS P
				INNER JOIN ZNodeTrackingOutbound AS OT ON P.ProductID = OT.ProductID 
				INNER JOIN ZNodeProductCategory AS PC ON PC.ProductID = P.ProductID
				INNER JOIN ZNodeCategoryNode AS ZC ON ZC.CategoryID = PC.CategoryID
				INNER JOIN ZNodePortalCatalog AS ZPC ON ZPC.CatalogID = ZC.CatalogID
				LEFT JOIN ZNodeAccount AS A ON P.AccountID = A.AccountId				
			WHERE  
				OT.[Date]>= @FromDate 
				AND OT.[Date]<=@ToDate				
				AND (@PortalId = '0' OR ZPC.PortalID=@PortalId)		
			END
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_GetByPaymentStatusID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItem table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetByPaymentStatusID
(

	@PaymentStatusID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3],
					[OrderLineItemStateID]
				FROM
					[dbo].[ZNodeOrderLineItem]
				WHERE
					[PaymentStatusID] = @PaymentStatusID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductByProductID_XML]'
GO
SET ANSI_NULLS ON
GO
-- ==========================================================   
-- Create date: July 02, 2006
-- Update date: May 26,2010        
-- Description: Retrieve product and return a serialized xml
-- ==========================================================
ALTER PROCEDURE [dbo].[ZNode_GetProductByProductID_XML]
    @ProductId int = 0,
    @LocaleId int = 0,
    @PortalId int = 0
AS 
    BEGIN                                
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;
		
		    CREATE TABLE #TmpPortal  
            (  
              PortalID int,  
              PRIMARY KEY CLUSTERED ( [PortalID] ASC )  
                WITH ( IGNORE_DUP_KEY = ON )   
            )  
  		
		-- Get SiteAdmin PortalID, if not franchise product        
        DECLARE @OwnPortalID INT
		INSERT  INTO #TmpPortal ( PortalID )
        SELECT ISNULL(A.PortalID,0) PortalID
			FROM ZNodePortal A
			WHERE A.PortalID NOT IN (SELECT ISNULL(PortalID, 0) FROM ZNodeCatalog)
			AND PortalID IN 
			(SELECT PortalID FROM ZNodePortalCatalog WHERE CatalogID IN
			(SELECT ZC.CatalogID FROM ZNodeCatalog ZC
			INNER JOIN ZNodeCategoryNode ZCN ON ZC.CatalogID = ZCN.CatalogID
			INNER JOIN ZNodeProductCategory ZPC ON ZPC.CategoryID = ZCN.CategoryID
			WHERE ZPC.ProductID = @ProductId))
			
		IF (EXISTS(SELECT PortalID FROM #TmpPortal WHERE PortalID = @PortalID))
		BEGIN
			SET @OwnPortalID = @PortalID
		END
		ELSE
		BEGIN
			SELECT TOP 1 @OwnPortalID = PortalID FROM #TmpPortal
		END

        CREATE TABLE #PRODUCTCATEGORYLIST
            (
              ProductId int,
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )
                WITH ( IGNORE_DUP_KEY = ON ) 
            )
		
		CREATE TABLE #CrossSellItem
            (
              ProductId int,
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )
                WITH ( IGNORE_DUP_KEY = ON ) 
            )
		INSERT  INTO #CrossSellItem ( ProductId )
		SELECT  ZNodeProductCrossSell.RelatedProductID AS ProductID				
		FROM	ZNodeProductCrossSell ZNodeProductCrossSell
		INNER JOIN ZnodeProduct ZP ON ZNodeProductCrossSell.RelatedProductID = ZP.ProductID
		WHERE	ZNodeProductCrossSell.ProductID = @ProductId
				AND ZP.ReviewStateID = 20

		IF ((SELECT COUNT(1) FROM #CrossSellItem) > 0)
		BEGIN
			INSERT  INTO #PRODUCTCATEGORYLIST ( ProductId )
                SELECT  ZPC.ProductID
                FROM 
                        ZNodeProductCategory ZPC
                        INNER JOIN ZNodeCategoryNode ZCN ON ZPC.CategoryID = ZCN.CategoryID
                        INNER JOIN ZNodePortalCatalog PC ON ZCN.CatalogID = PC.CatalogID
                WHERE
						ZPC.ProductID IN  (SELECT ProductID FROM #CrossSellItem)
						AND	ZCN.ActiveInd = 1
                        AND ZPC.ActiveInd = 1
                        AND PC.LocaleID = @LocaleId
                        AND PC.PortalID = @PortalId
		END;
		
										
        SELECT  [ProductID],
                [Name],
                [ShortDescription],
                [Description],
                [FeaturesDesc],
                [ProductNum],
                [ProductTypeID],
                [RetailPrice],
                [SalePrice],
                [WholesalePrice],
                [ImageFile],
                [ImageAltTag],
                [Weight],
                [Length],
                [Width],
                [Height],
                [BeginActiveDate],
                [EndActiveDate],
                [DisplayOrder],
                [ActiveInd],
                [CallForPricing],
                [HomepageSpecial],
                [CategorySpecial],
                [InventoryDisplay],
                [Keywords],
                [ManufacturerID],
                [AdditionalInfoLink],
                [AdditionalInfoLinkLabel],
                [ShippingRuleTypeID],
                [ShippingRate],
                [SEOTitle],
                [SEOKeywords],
                [SEODescription],
                [Custom1],
                [Custom2],
                [Custom3],
                [ShipEachItemSeparately],
                [AllowBackOrder],
                [BackOrderMsg],
                [DropShipInd],
                [DropShipEmailID],
                [Specifications],
                [AdditionalInformation],
                [InStockMsg],
                [OutOfStockMsg],
                [TrackInventoryInd],
                [DownloadLink],
                [FreeShippingInd],
                [NewProductInd],
                [SEOURL],
                [MinQty],
                [MaxQty],
                [ShipSeparately],
                [FeaturedInd],
                [WebServiceDownloadDte],
                [UpdateDte],
                [SupplierID],
                [RecurringBillingInd],
                [RecurringBillingInstallmentInd],
                [RecurringBillingPeriod],
                [RecurringBillingFrequency],
                [RecurringBillingTotalCycles],
                [RecurringBillingInitialAmount],
                [TaxClassID],
                ISNULL([PortalID], @OwnPortalID) PortalID,
                [AffiliateUrl],
                (SELECT TOP 1 sku.SKU, QuantityOnHand,ExternalProductID
										FROM ZNodeSKU sku
											INNER JOIN ZNodeSKUInventory SkuInventory 
										ON sku.SKU = SkuInventory.SKU
										WHERE 
											EXISTS
											(SELECT 1 FROM ZNodeSKUAttribute
												WHERE SKUID = sku.SKUID having COUNT(1) = 0)
												AND	sku.ProductID = @ProductID
												
										FOR	XML PATH(''),TYPE
								),
                ManufacturerName = ( Select Name
                                     from   ZNodeManufacturer
                                     where  ManufacturerId = ZNodeProduct.ManufacturerId AND ActiveInd = 1
                                   ),
                ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                            TotalReviews = IsNull(COUNT(ProductId), 0)
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.Status = 'A'
                FOR
                  XML PATH(''),
                      TYPE
                ),
                ( SELECT    [ProductCategoryID],
                            ZNodeProductCategory.[ProductID],
                            [Theme],
                            [MasterPage],
                            [CSS],
                            [CategoryID],
                            [BeginDate],
                            [EndDate],
                            [DisplayOrder],
                            [ActiveInd]
                  FROM      ZNodeProductCategory ZNodeProductCategory
                  WHERE     ZNodeProductCategory.ProductId = ZNodeProduct.productid
                  ORDER BY  ZNodeProductCategory.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    ZNodeHighlight.[HighlightID],
                            [ImageFile],
                            [ImageAltTag],
                            [Name],
                            [Description],
                            [DisplayPopup],
                            [Hyperlink],
                            [HyperlinkNewWinInd],
                            [HighlightTypeID],
                            [ActiveInd],
                            ZNodeHighlight.[DisplayOrder],
                            [ShortDescription],
                            [LocaleId]
                  FROM      ZNodehighlight ZNodeHighlight
                            INNER JOIN ZNodeProductHighlight ZNodeProductHighlight ON ZNodeHighlight.[HighlightID] = ZNodeProductHighlight.[HighlightID]
                  WHERE     ZNodeProductHighlight.productid = ZNodeProduct.productid
                  ORDER BY  ZNodeProductHighlight.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    [ReviewID],
                            ZNodeReview.[ProductID],
                            [AccountID],
                            [Subject],
                            [Pros],
                            [Cons],
                            [Comments],
                            [CreateUser],
                            [UserLocation],
                            [Rating],
                            [Status],
                            [CreateDate],
                            [Custom1],
                            [Custom2],
                            [Custom3]
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.[Status] = 'A'
                  ORDER BY  ZNodeReview.ReviewId desc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( select    ZNodeCrossSellItem.ProductId,
                            ZNodeCrossSellItem.Name,
                            ZNodeCrossSellItem.ShortDescription,
                            ZNodeCrossSellItem.ImageFile,
                            ZNodeCrossSellItem.SeoURL,
                            ZNodeCrossSellItem.ImageAltTag,
                            ZNodeCrossSellItem.RetailPrice,
                            ZNodeCrossSellItem.SalePrice,
                            ZNodeCrossSellItem.WholesalePrice,
                            ZNodeCrossSellItem.CallForPricing,
                            ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                                        TotalReviews = IsNull(COUNT(ProductId), 0)
                              FROM      ZNodeReview ZNodeReview
                              WHERE     ZNodeReview.ProductId = ZNodeCrossSellItem.ProductId
                                        AND ZNodeReview.[Status] = 'A'
                            FOR
                              XML PATH(''),
                                  TYPE
                            )
                  FROM     ProductsView ZNodeCrossSellItem
                  WHERE 
							ZNodeCrossSellItem.ProductId IN (
                            SELECT  ProductId
                            from    #PRODUCTCATEGORYLIST )
                            AND ZNodeCrossSellItem.ActiveInd = 1
                   
                  ORDER BY  ZNodeCrossSellItem.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( Select    ZNodeAttributeType.[AttributeTypeId],
                            ZNodeAttributeType.[Name],
                            ZNodeAttributeType.[Description],
                            ZNodeAttributeType.[DisplayOrder],
                            [IsPrivate],
                            [LocaleId],
                            ( SELECT
      Distinct                          ( ZNodeAttribute.AttributeId ),
                                        ZNodeAttribute.Name,
                                        ZNodeAttribute.DisplayOrder
                              FROM      ZNodeskuattribute skua
                                        INNER JOIN ZNodeProductattribute ZNodeAttribute ON skua.attributeid = ZNodeAttribute.attributeid
                              WHERE     ZNodeAttribute.attributetypeid = ZNodeAttributeType.attributetypeid
                                        and ZNodeAttribute.IsActive = 1
                                        and SKUID in (
                                        SELECT  SKUID
                                        FROM    ZNodeSKU
                                        WHERE   ProductID = @ProductId
                                                and ActiveInd = 1 )
                              Group By  ZNodeAttribute.AttributeId,
                                        ZNodeAttribute.Name,
                                        ZNodeAttribute.DisplayOrder
                              Order By  ZNodeAttribute.DisplayOrder
                            FOR
                              XML AUTO,
                                  TYPE,
                                  ELEMENTS
                            )
                  FROM      ZNodeAttributeType ZNodeAttributeType
                            INNER JOIN ZNodeProductTypeAttribute ProductTypeAttribute 
                            ON ZNodeAttributeType.attributetypeid = ProductTypeAttribute.attributetypeid
                            AND ProductTypeAttribute.ProductTypeId = ZNodeProduct.ProductTypeID
                  Order By  ZNodeAttributeType.DisplayOrder
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    ZNodeAddOn.[AddOnID],
                            ZNodeAddOn.[ProductID],
                            [Title],
                            [Name],
                            [Description],
                            [DisplayOrder],
                            [DisplayType],
                            [OptionalInd],
                            [AllowBackOrder],
                            [InStockMsg],
                            [OutOfStockMsg],
                            [BackOrderMsg],
                            [PromptMsg],
                            [TrackInventoryInd],
                            [LocaleId],
                            ( SELECT Distinct
                                        ( ZNodeAddonValue.AddOnValueID ),
                                        ZNodeAddonValue.Name,
                                        ZNodeAddonValue.[Description],
                                        ZNodeAddonValue.DisplayOrder,
                                        ZNodeAddonValue.RetailPrice as RetailPrice,
                                        ZNodeAddonValue.SalePrice as SalePrice,
                                        ZNodeAddonValue.WholeSalePrice as WholesalePrice,
                                        ZNodeAddonValue.DefaultInd as IsDefault,
                                        QuantityOnHand = ( SELECT   QuantityOnHand
                                                           FROM     ZNodeSKUInventory
                                                           WHERE    SKU = ZNodeAddOnValue.SKU
                                                         ),
                                        ZNodeAddonValue.weight as WeightAdditional,
                                        ZNodeAddonValue.ShippingRuleTypeID,
                                        ZNodeAddonValue.FreeShippingInd,
                                        ZNodeAddonValue.SupplierID,
                                        ZNodeAddonValue.TaxClassID,
                                        ZNodeAddonValue.ExternalProductID
                              FROM      ZNodeAddOnValue ZNodeAddOnValue
                              WHERE     ZNodeAddOnValue.AddOnId = ZNodeAddOn.AddOnId
                              Order By  ZNodeAddonValue.DisplayOrder
                            FOR
                              XML AUTO,
                                  TYPE,
                                  ELEMENTS
                            )
                  FROM      ZNodeAddOn ZNodeAddOn
                            INNER JOIN ZNodeProductAddOn ZNodeProductAddOn ON ZNodeAddOn.AddOnId = ZNodeProductAddOn.AddOnId
                  WHERE     ZNodeProductAddOn.ProductId = @ProductId
                  Order By  ZNodeAddon.DisplayOrder
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    [ProductTierID],
                            [ProductID],
                            [ProfileID],
                            [TierStart],
                            [TierEnd],
                            [Price]
                  FROM      ZNodeProductTier ZNodeProductTier
                  WHERE     ZNodeProductTier.ProductId = ZNodeProduct.ProductId
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT TOP 1 *  
                  FROM      ZNodeSKU
                  INNER JOIN ZnodeSKUInventory ON ZNodeSKU.SKU = ZNodeSKUInventory.SKU                     
                  WHERE     ZNodeSKU.ProductId = ZNodeProduct.ProductId  
                FOR  
                  XML PATH('ZNodeSKU'),  
                      TYPE,  
                      ELEMENTS  
                ),
                (SELECT  [ProductID],
                [Name],
                [ShortDescription],
                [Description],
                [FeaturesDesc],
                [ProductNum],
                [ProductTypeID],
                [RetailPrice],
                [SalePrice],
                [WholesalePrice],
                [ImageFile],
                [ImageAltTag],
                [Weight],
                [Length],
                [Width],
                [Height],
                [BeginActiveDate],
                [EndActiveDate],
                [DisplayOrder],
                [ActiveInd],
                [CallForPricing],
                [HomepageSpecial],
                [CategorySpecial],
                [InventoryDisplay],
                [Keywords],
                [ManufacturerID],
                [AdditionalInfoLink],
                [AdditionalInfoLinkLabel],
                [ShippingRuleTypeID],
                [ShippingRate],
                [SEOTitle],
                [SEOKeywords],
                [SEODescription],
                [Custom1],
                [Custom2],
                [Custom3],
                [ShipEachItemSeparately],
                [AllowBackOrder],
                [BackOrderMsg],
                [DropShipInd],
                [DropShipEmailID],
                [Specifications],
                [AdditionalInformation],
                [InStockMsg],
                [OutOfStockMsg],
                [TrackInventoryInd],
                [DownloadLink],
                [FreeShippingInd],
                [NewProductInd],
                [SEOURL],
                [MinQty],
                [MaxQty],
                [ShipSeparately],
                [FeaturedInd],
                [WebServiceDownloadDte],
                [UpdateDte],
                [SupplierID],
                [RecurringBillingInd],
                [RecurringBillingInstallmentInd],
                [RecurringBillingPeriod],
                [RecurringBillingFrequency],
                [RecurringBillingTotalCycles],
                [RecurringBillingInitialAmount],
                [TaxClassID],
                [PortalID],
                [AffiliateUrl],
                (SELECT TOP 1 sku.SKU, QuantityOnHand,ExternalProductID
										FROM ZNodeSKU sku
											INNER JOIN ZNodeSKUInventory SkuInventory 
										ON sku.SKU = SkuInventory.SKU
										WHERE 
											EXISTS
											(SELECT 1 FROM ZNodeSKUAttribute
												WHERE SKUID = sku.SKUID having COUNT(1) = 0)
												AND	sku.ProductID = ZNodeBundleProduct.ProductID
												
										FOR	XML PATH(''),TYPE
								),
                ManufacturerName = ( Select Name
                                     from   ZNodeManufacturer
                                     where  ManufacturerId = ZNodeBundleProduct.ManufacturerId AND ActiveInd = 1
                                   ),
                                   ( SELECT TOP 1 *  
                  FROM      ZNodeSKU
                  INNER JOIN ZnodeSKUInventory ON ZNodeSKU.SKU = ZNodeSKUInventory.SKU                     
                  WHERE     ZNodeSKU.ProductId = ZNodeBundleProduct.ProductID  
                FOR  
                  XML PATH('ZNodeSKU'),  
                      TYPE,  
                      ELEMENTS  
                )       
                FROM ZNodeProduct ZNodeBundleProduct INNER JOIN ZNodeParentChildProduct B
                ON ZNodeBundleProduct.ProductID = B.ChildProductID WHERE   B.ParentProductID = @ProductId AND ZNodeBundleProduct.ActiveInd = 1 FOR XML AUTO,
                    TYPE,
                    ELEMENTS )
                  
        FROM    ZNodeProduct ZNodeProduct			    
        WHERE   ZNodeProduct.ProductId = @ProductId
        FOR     XML AUTO,
                    TYPE,
                    ELEMENTS                  
    END  




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_GetByOrderLineItemID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeOrderLineItem table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetByOrderLineItemID
(

	@OrderLineItemID int   
)
AS


				SELECT
					[OrderLineItemID],
					[OrderID],
					[ShipmentID],
					[ProductNum],
					[Name],
					[Description],
					[Quantity],
					[Price],
					[Weight],
					[SKU],
					[ParentOrderLineItemID],
					[OrderLineItemRelationshipTypeID],
					[DownloadLink],
					[DiscountAmount],
					[ShipSeparately],
					[ShipDate],
					[ReturnDate],
					[ShippingCost],
					[PromoDescription],
					[SalesTax],
					[VAT],
					[GST],
					[PST],
					[HST],
					[TransactionNumber],
					[PaymentStatusID],
					[TrackingNumber],
					[AutoGeneratedTracking],
					[Custom1],
					[Custom2],
					[Custom3],
					[OrderLineItemStateID]
				FROM
					[dbo].[ZNodeOrderLineItem]
				WHERE
					[OrderLineItemID] = @OrderLineItemID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductByProductID]'
GO
SET ANSI_NULLS ON
GO
  
--[ZNode_GetProductByProductID] 302  
ALTER PROCEDURE [dbo].[ZNode_GetProductByProductID]                        
 -- Add the parameters for the stored procedure here                            
 @ProductId int = 0  
AS                            
BEGIN                            
 -- SET NOCOUNT ON added to prevent extra result sets from  
  -- interfering with SELECT statements.  
        SET NOCOUNT ON ;  
        
        -- Get SiteAdmin PortalID, if not franchise product
        DECLARE @OwnPortalID INT
        SELECT @OwnPortalID = ISNULL(A.PortalID,0) FROM ZNodePortal A
			WHERE A.PortalID NOT IN (SELECT ISNULL(PortalID, 0) FROM ZNodeCatalog)
			AND PortalID IN 
			(SELECT PortalID FROM ZNodePortalCatalog WHERE CatalogID IN
			(SELECT ZC.CatalogID FROM ZNodeCatalog ZC
			INNER JOIN ZNodeCategoryNode ZCN ON ZC.CatalogID = ZCN.CatalogID
			INNER JOIN ZNodeProductCategory ZPC ON ZPC.CategoryID = ZCN.CategoryID
			WHERE ZPC.ProductID = @ProductID))

    
        CREATE TABLE #PRODUCTCATEGORYLIST
            (
              ProductId int,
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )
                WITH ( IGNORE_DUP_KEY = ON ) 
            )
    
		 CREATE TABLE #CrossSellItem  
            (  
              ProductId int,  
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )  
                WITH ( IGNORE_DUP_KEY = ON )   
            )  
	
		INSERT  INTO #CrossSellItem ( ProductId )  
  		SELECT  ZNodeProductCrossSell.RelatedProductID AS ProductID
		FROM	ZNodeProductCrossSell ZNodeProductCrossSell
		INNER JOIN ZnodeProduct ZP ON ZNodeProductCrossSell.RelatedProductID = ZP.ProductID
		WHERE	ZNodeProductCrossSell.ProductID = @ProductId
			

		IF ((SELECT COUNT(1) FROM #CrossSellItem) > 0)
		BEGIN
			INSERT  INTO #PRODUCTCATEGORYLIST ( ProductId )
                SELECT  ZPC.ProductID
                FROM 
                        ZNodeProductCategory ZPC
                        INNER JOIN ZNodeCategoryNode ZCN ON ZPC.CategoryID = ZCN.CategoryID
                        INNER JOIN ZNodePortalCatalog PC ON ZCN.CatalogID = PC.CatalogID
                WHERE
						ZPC.ProductID IN  (SELECT ProductID FROM #CrossSellItem)
						AND	ZCN.ActiveInd = 1
                        AND ZPC.ActiveInd = 1
                      
		END;
		
        SELECT  [ProductID],  
                [Name],  
                [ShortDescription],  
                [Description],  
                [FeaturesDesc],  
                [ProductNum],  
                [ProductTypeID],
                [ExpirationPeriod],  
                [ExpirationFrequency],
                [RetailPrice],  
                [SalePrice],  
                [WholesalePrice],  
                [ImageFile],  
                [ImageAltTag],  
                [Weight],  
                [Length],  
                [Width],  
                [Height],  
                [BeginActiveDate],  
                [EndActiveDate],  
                [DisplayOrder],  
                [ActiveInd],  
                [CallForPricing],  
                [HomepageSpecial],  
                [CategorySpecial],  
                [InventoryDisplay],  
                [Keywords],  
                [ManufacturerID],  
                [AdditionalInfoLink],  
                [AdditionalInfoLinkLabel],  
                [ShippingRuleTypeID],  
                [ShippingRate],  
                [SEOTitle],  
                [SEOKeywords],  
                [SEODescription],  
                [Custom1],  
                [Custom2],  
                [Custom3],  
                [ShipEachItemSeparately],  
                [AllowBackOrder],  
                [BackOrderMsg],  
                [DropShipInd],  
                [DropShipEmailID],  
                [Specifications],  
                [AdditionalInformation],  
                [InStockMsg],  
                [OutOfStockMsg],  
                [TrackInventoryInd],  
                [DownloadLink],  
                [FreeShippingInd],  
                [NewProductInd],  
                [SEOURL],  
                [MinQty],  
                [MaxQty],  
                [ShipSeparately],  
                [FeaturedInd],  
                [WebServiceDownloadDte],  
                [UpdateDte],  
                [SupplierID],  
                [RecurringBillingInd],  
                [RecurringBillingInstallmentInd],  
                [RecurringBillingPeriod],  
                [RecurringBillingFrequency],  
                [RecurringBillingTotalCycles],  
                [RecurringBillingInitialAmount],  
                [TaxClassID],  
                ISNULL([PortalID], @OwnPortalID),  
                [AffiliateUrl],  
                (SELECT TOP 1 sku.SKU, QuantityOnHand, ExternalProductID  
          FROM ZNodeSKU sku  
           INNER JOIN ZNodeSKUInventory SkuInventory   
          ON sku.SKU = SkuInventory.SKU  
          WHERE   
           EXISTS  
           (SELECT 1 FROM ZNodeSKUAttribute  
            WHERE SKUID = sku.SKUID having COUNT(1) = 0)  
            AND sku.ProductID = @ProductID  
              
          FOR XML PATH(''),TYPE  
        ),               
                ( Select    ZNodeAttributeType.[AttributeTypeId],  
                            ZNodeAttributeType.[Name],  
                            ZNodeAttributeType.[Description],  
                            ZNodeAttributeType.[DisplayOrder],  
                            [IsPrivate],  
                            [LocaleId],  
                            ( SELECT  
        Distinct ( ZNodeAttribute.AttributeId ),  
                                        ZNodeAttribute.Name,  
                                        ZNodeAttribute.DisplayOrder  
                              FROM      ZNodeskuattribute skua  
                                        INNER JOIN ZNodeProductattribute ZNodeAttribute ON skua.attributeid = ZNodeAttribute.attributeid  
                              WHERE     ZNodeAttribute.attributetypeid = ZNodeAttributeType.attributetypeid  
                 and ZNodeAttribute.IsActive = 1  
                                        and SKUID in (  
                                        SELECT  SKUID  
                                        FROM    ZNodeSKU  
                                        WHERE   ProductID = @ProductId  
                                                and ActiveInd = 1 )  
                              Group By  ZNodeAttribute.AttributeId,  
                                        ZNodeAttribute.Name,  
                                        ZNodeAttribute.DisplayOrder  
                              Order By  ZNodeAttribute.DisplayOrder  
                            FOR  
                              XML AUTO,  
                                  TYPE,  
                                  ELEMENTS  
                            )  
                  FROM      ZNodeAttributeType ZNodeAttributeType  
                            INNER JOIN ZNodeProductTypeAttribute ProductTypeAttribute   
                            ON ZNodeAttributeType.attributetypeid = ProductTypeAttribute.attributetypeid  
                            AND ProductTypeAttribute.ProductTypeId = ZNodeProduct.ProductTypeID  
                  Order By  ZNodeAttributeType.DisplayOrder  
                  FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( select    ZNodeCrossSellItem.ProductId,
                            ZNodeCrossSellItem.Name,
                            ZNodeCrossSellItem.ShortDescription,
                            ZNodeCrossSellItem.ImageFile,
                            ZNodeCrossSellItem.SeoURL,
                            ZNodeCrossSellItem.ImageAltTag,
                            ZNodeCrossSellItem.RetailPrice,
                            ZNodeCrossSellItem.SalePrice,
                            ZNodeCrossSellItem.WholesalePrice,
                            ZNodeCrossSellItem.CallForPricing,
                            ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                                        TotalReviews = IsNull(COUNT(ProductId), 0)
                              FROM      ZNodeReview ZNodeReview
                              WHERE     ZNodeReview.ProductId = ZNodeCrossSellItem.ProductId
                                        AND ZNodeReview.[Status] = 'A'
                            FOR
                              XML PATH(''),
                                  TYPE
                            )
                  FROM     ProductsView ZNodeCrossSellItem
                  WHERE 
							ZNodeCrossSellItem.ProductId IN (
                            SELECT  ProductId
                            from    #PRODUCTCATEGORYLIST )
                            AND ZNodeCrossSellItem.ActiveInd = 1
                                   
                  ORDER BY  ZNodeCrossSellItem.DisplayOrder asc
                  
                FOR  
                  XML AUTO,  
                      TYPE,  
                      ELEMENTS  
                ),  
                ( SELECT    ZNodeAddOn.[AddOnID],  
                            ZNodeAddOn.[ProductID],  
                            [Title],  
                            [Name],  
                            [Description],  
                            [DisplayOrder],  
                            [DisplayType],  
                            [OptionalInd],  
                            [AllowBackOrder],  
                            [InStockMsg],  
                            [OutOfStockMsg],  
                            [BackOrderMsg],  
                            [PromptMsg],  
                            [TrackInventoryInd],  
                            [LocaleId],  
                            ( SELECT Distinct  
                                        ( ZNodeAddonValue.AddOnValueID ),  
                                        ZNodeAddonValue.Name,  
                                        ZNodeAddonValue.DisplayOrder,  
                                        ZNodeAddonValue.RetailPrice as RetailPrice,  
                                        ZNodeAddonValue.SalePrice as SalePrice,  
                                        ZNodeAddonValue.WholeSalePrice as WholesalePrice,  
                                        ZNodeAddonValue.DefaultInd as IsDefault,  
                                        QuantityOnHand = ( SELECT   QuantityOnHand  
                                                           FROM     ZNodeSKUInventory  
                                                           WHERE    SKU = ZNodeAddOnValue.SKU  
                                                         ),  
                                        ZNodeAddonValue.weight as WeightAdditional,  
                                        ZNodeAddonValue.ShippingRuleTypeID,  
                                        ZNodeAddonValue.FreeShippingInd,  
                                        ZNodeAddonValue.SupplierID,  
                                        ZNodeAddonValue.TaxClassID,  
                                        ZNodeAddOnValue.ExternalProductID  
                              FROM      ZNodeAddOnValue ZNodeAddOnValue  
                              WHERE     ZNodeAddOnValue.AddOnId = ZNodeAddOn.AddOnId  
                              Order By  ZNodeAddonValue.DisplayOrder  
                            FOR  
                              XML AUTO,  
                                  TYPE,  
                                  ELEMENTS  
                            )  
                  FROM      ZNodeAddOn ZNodeAddOn  
                            INNER JOIN ZNodeProductAddOn ZNodeProductAddOn ON ZNodeAddOn.AddOnId = ZNodeProductAddOn.AddOnId  
                  WHERE     ZNodeProductAddOn.ProductId = @ProductId  
                  Order By  ZNodeAddon.DisplayOrder  
                FOR  
                  XML AUTO,  
                      TYPE,  
                      ELEMENTS  
                ),  
                ( SELECT    [ProductTierID],  
                            [ProductID],  
                            [ProfileID],  
                            [TierStart],  
                            [TierEnd],  
                            [Price]  
                  FROM      ZNodeProductTier ZNodeProductTier  
                  WHERE     ZNodeProductTier.ProductId = ZNodeProduct.ProductId  
                FOR  
                  XML AUTO,  
                      TYPE,  
                      ELEMENTS  
                ),  
                ( SELECT TOP 1 *  
                  FROM      ZNodeSKU   
                  WHERE     ZNodeSKU.ProductId = ZNodeProduct.ProductId  
                FOR  
                  XML AUTO,  
                      TYPE,  
                      ELEMENTS  
                ),
                (SELECT  [ProductID],
                [Name],
                [ShortDescription],
                [Description],
                [FeaturesDesc],
                [ProductNum],
                [ProductTypeID],
                [RetailPrice],
                [SalePrice],
                [WholesalePrice],
                [ImageFile],
                [ImageAltTag],
                [Weight],
                [Length],
                [Width],
                [Height],
                [BeginActiveDate],
                [EndActiveDate],
                [DisplayOrder],
                [ActiveInd],
                [CallForPricing],
                [HomepageSpecial],
                [CategorySpecial],
                [InventoryDisplay],
                [Keywords],
                [ManufacturerID],
                [AdditionalInfoLink],
                [AdditionalInfoLinkLabel],
                [ShippingRuleTypeID],
                [ShippingRate],
                [SEOTitle],
                [SEOKeywords],
                [SEODescription],
                [Custom1],
                [Custom2],
                [Custom3],
                [ShipEachItemSeparately],
                [AllowBackOrder],
                [BackOrderMsg],
                [DropShipInd],
                [DropShipEmailID],
                [Specifications],
                [AdditionalInformation],
                [InStockMsg],
                [OutOfStockMsg],
                [TrackInventoryInd],
                [DownloadLink],
                [FreeShippingInd],
                [NewProductInd],
                [SEOURL],
                [MinQty],
                [MaxQty],
                [ShipSeparately],
                [FeaturedInd],
                [WebServiceDownloadDte],
                [UpdateDte],
                [SupplierID],
                [RecurringBillingInd],
                [RecurringBillingInstallmentInd],
                [RecurringBillingPeriod],
                [RecurringBillingFrequency],
                [RecurringBillingTotalCycles],
                [RecurringBillingInitialAmount],
                [TaxClassID],
                [PortalID],
                [AffiliateUrl],
                (SELECT TOP 1 sku.SKU, QuantityOnHand,ExternalProductID
										FROM ZNodeSKU sku
											INNER JOIN ZNodeSKUInventory SkuInventory 
										ON sku.SKU = SkuInventory.SKU
										WHERE 
											EXISTS
											(SELECT 1 FROM ZNodeSKUAttribute
												WHERE SKUID = sku.SKUID having COUNT(1) = 0)
												AND	sku.ProductID = ZNodeBundleProduct.ProductID
												
										FOR	XML PATH(''),TYPE
								),
                ManufacturerName = ( Select Name
                                     from   ZNodeManufacturer
                                     where  ManufacturerId = ZNodeBundleProduct.ManufacturerId AND ActiveInd = 1
                                   ),
                                   ( SELECT TOP 1 *  
                  FROM      ZNodeSKU
                  INNER JOIN ZnodeSKUInventory ON ZNodeSKU.SKU = ZNodeSKUInventory.SKU                     
                  WHERE     ZNodeSKU.ProductId = ZNodeBundleProduct.ProductID  
                FOR  
                  XML PATH('ZNodeSKU'),  
                      TYPE,  
                      ELEMENTS  
                )       
                FROM ZNodeProduct ZNodeBundleProduct INNER JOIN ZNodeParentChildProduct B
                ON ZNodeBundleProduct.ProductID = B.ChildProductID WHERE   B.ParentProductID = @ProductId AND ZNodeBundleProduct.ActiveInd = 1 FOR     XML AUTO,
                    TYPE,
                    ELEMENTS )  
        FROM    ZNodeProduct ZNodeProduct         
        WHERE   ZNodeProduct.ProductId = @ProductId  
        FOR     XML AUTO,  
                    TYPE,  
                    ELEMENTS  
    END    
  
  
  
  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_Find]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeOrderLineItem table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_Find
(

	@SearchUsingOR bit   = null ,

	@OrderLineItemID int   = null ,

	@OrderID int   = null ,

	@ShipmentID int   = null ,

	@ProductNum nvarchar (MAX)  = null ,

	@Name nvarchar (MAX)  = null ,

	@Description nvarchar (MAX)  = null ,

	@Quantity int   = null ,

	@Price money   = null ,

	@Weight decimal (18, 2)  = null ,

	@SKU nvarchar (MAX)  = null ,

	@ParentOrderLineItemID int   = null ,

	@OrderLineItemRelationshipTypeID int   = null ,

	@DownloadLink nvarchar (MAX)  = null ,

	@DiscountAmount money   = null ,

	@ShipSeparately bit   = null ,

	@ShipDate datetime   = null ,

	@ReturnDate datetime   = null ,

	@ShippingCost money   = null ,

	@PromoDescription nvarchar (MAX)  = null ,

	@SalesTax money   = null ,

	@VAT money   = null ,

	@GST money   = null ,

	@PST money   = null ,

	@HST money   = null ,

	@TransactionNumber nvarchar (MAX)  = null ,

	@PaymentStatusID int   = null ,

	@TrackingNumber nvarchar (MAX)  = null ,

	@AutoGeneratedTracking bit   = null ,

	@Custom1 nvarchar (MAX)  = null ,

	@Custom2 nvarchar (MAX)  = null ,

	@Custom3 nvarchar (MAX)  = null ,

	@OrderLineItemStateID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [OrderLineItemID]
	, [OrderID]
	, [ShipmentID]
	, [ProductNum]
	, [Name]
	, [Description]
	, [Quantity]
	, [Price]
	, [Weight]
	, [SKU]
	, [ParentOrderLineItemID]
	, [OrderLineItemRelationshipTypeID]
	, [DownloadLink]
	, [DiscountAmount]
	, [ShipSeparately]
	, [ShipDate]
	, [ReturnDate]
	, [ShippingCost]
	, [PromoDescription]
	, [SalesTax]
	, [VAT]
	, [GST]
	, [PST]
	, [HST]
	, [TransactionNumber]
	, [PaymentStatusID]
	, [TrackingNumber]
	, [AutoGeneratedTracking]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [OrderLineItemStateID]
    FROM
	[dbo].[ZNodeOrderLineItem]
    WHERE 
	 ([OrderLineItemID] = @OrderLineItemID OR @OrderLineItemID IS NULL)
	AND ([OrderID] = @OrderID OR @OrderID IS NULL)
	AND ([ShipmentID] = @ShipmentID OR @ShipmentID IS NULL)
	AND ([ProductNum] = @ProductNum OR @ProductNum IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([Quantity] = @Quantity OR @Quantity IS NULL)
	AND ([Price] = @Price OR @Price IS NULL)
	AND ([Weight] = @Weight OR @Weight IS NULL)
	AND ([SKU] = @SKU OR @SKU IS NULL)
	AND ([ParentOrderLineItemID] = @ParentOrderLineItemID OR @ParentOrderLineItemID IS NULL)
	AND ([OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID OR @OrderLineItemRelationshipTypeID IS NULL)
	AND ([DownloadLink] = @DownloadLink OR @DownloadLink IS NULL)
	AND ([DiscountAmount] = @DiscountAmount OR @DiscountAmount IS NULL)
	AND ([ShipSeparately] = @ShipSeparately OR @ShipSeparately IS NULL)
	AND ([ShipDate] = @ShipDate OR @ShipDate IS NULL)
	AND ([ReturnDate] = @ReturnDate OR @ReturnDate IS NULL)
	AND ([ShippingCost] = @ShippingCost OR @ShippingCost IS NULL)
	AND ([PromoDescription] = @PromoDescription OR @PromoDescription IS NULL)
	AND ([SalesTax] = @SalesTax OR @SalesTax IS NULL)
	AND ([VAT] = @VAT OR @VAT IS NULL)
	AND ([GST] = @GST OR @GST IS NULL)
	AND ([PST] = @PST OR @PST IS NULL)
	AND ([HST] = @HST OR @HST IS NULL)
	AND ([TransactionNumber] = @TransactionNumber OR @TransactionNumber IS NULL)
	AND ([PaymentStatusID] = @PaymentStatusID OR @PaymentStatusID IS NULL)
	AND ([TrackingNumber] = @TrackingNumber OR @TrackingNumber IS NULL)
	AND ([AutoGeneratedTracking] = @AutoGeneratedTracking OR @AutoGeneratedTracking IS NULL)
	AND ([Custom1] = @Custom1 OR @Custom1 IS NULL)
	AND ([Custom2] = @Custom2 OR @Custom2 IS NULL)
	AND ([Custom3] = @Custom3 OR @Custom3 IS NULL)
	AND ([OrderLineItemStateID] = @OrderLineItemStateID OR @OrderLineItemStateID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [OrderLineItemID]
	, [OrderID]
	, [ShipmentID]
	, [ProductNum]
	, [Name]
	, [Description]
	, [Quantity]
	, [Price]
	, [Weight]
	, [SKU]
	, [ParentOrderLineItemID]
	, [OrderLineItemRelationshipTypeID]
	, [DownloadLink]
	, [DiscountAmount]
	, [ShipSeparately]
	, [ShipDate]
	, [ReturnDate]
	, [ShippingCost]
	, [PromoDescription]
	, [SalesTax]
	, [VAT]
	, [GST]
	, [PST]
	, [HST]
	, [TransactionNumber]
	, [PaymentStatusID]
	, [TrackingNumber]
	, [AutoGeneratedTracking]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [OrderLineItemStateID]
    FROM
	[dbo].[ZNodeOrderLineItem]
    WHERE 
	 ([OrderLineItemID] = @OrderLineItemID AND @OrderLineItemID is not null)
	OR ([OrderID] = @OrderID AND @OrderID is not null)
	OR ([ShipmentID] = @ShipmentID AND @ShipmentID is not null)
	OR ([ProductNum] = @ProductNum AND @ProductNum is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([Quantity] = @Quantity AND @Quantity is not null)
	OR ([Price] = @Price AND @Price is not null)
	OR ([Weight] = @Weight AND @Weight is not null)
	OR ([SKU] = @SKU AND @SKU is not null)
	OR ([ParentOrderLineItemID] = @ParentOrderLineItemID AND @ParentOrderLineItemID is not null)
	OR ([OrderLineItemRelationshipTypeID] = @OrderLineItemRelationshipTypeID AND @OrderLineItemRelationshipTypeID is not null)
	OR ([DownloadLink] = @DownloadLink AND @DownloadLink is not null)
	OR ([DiscountAmount] = @DiscountAmount AND @DiscountAmount is not null)
	OR ([ShipSeparately] = @ShipSeparately AND @ShipSeparately is not null)
	OR ([ShipDate] = @ShipDate AND @ShipDate is not null)
	OR ([ReturnDate] = @ReturnDate AND @ReturnDate is not null)
	OR ([ShippingCost] = @ShippingCost AND @ShippingCost is not null)
	OR ([PromoDescription] = @PromoDescription AND @PromoDescription is not null)
	OR ([SalesTax] = @SalesTax AND @SalesTax is not null)
	OR ([VAT] = @VAT AND @VAT is not null)
	OR ([GST] = @GST AND @GST is not null)
	OR ([PST] = @PST AND @PST is not null)
	OR ([HST] = @HST AND @HST is not null)
	OR ([TransactionNumber] = @TransactionNumber AND @TransactionNumber is not null)
	OR ([PaymentStatusID] = @PaymentStatusID AND @PaymentStatusID is not null)
	OR ([TrackingNumber] = @TrackingNumber AND @TrackingNumber is not null)
	OR ([AutoGeneratedTracking] = @AutoGeneratedTracking AND @AutoGeneratedTracking is not null)
	OR ([Custom1] = @Custom1 AND @Custom1 is not null)
	OR ([Custom2] = @Custom2 AND @Custom2 is not null)
	OR ([Custom3] = @Custom3 AND @Custom3 is not null)
	OR ([OrderLineItemStateID] = @OrderLineItemStateID AND @OrderLineItemStateID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductCategoryByProductID]'
GO
SET ANSI_NULLS ON
GO

---Get ProductCategory for particular Productid---   
    
ALTER PROCEDURE [dbo].[ZNode_GetProductCategoryByProductID](@PRODUCTID INT)    
AS    
BEGIN    
SELECT 
	ProductCategoryID,
	PC.ProductID,
	PC.CategoryID ,
	C.Name,
	C.Title
FROM
	ZNodeProductCategory PC 
	INNER JOIN ZNodeProduct P ON  P.ProductID=PC.ProductID 
	INNER JOIN ZNodeCategory C ON C.CategoryID=PC.CategoryID WHERE PC.ProductID = @PRODUCTID;    
END;  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchProduct]'
GO

  
    
    
ALTER PROCEDURE [dbo].[ZNode_SearchProduct]    
 @Name VARCHAR (MAX)=NULL,     
 @ProductNum VARCHAR (MAX)=NULL,     
 @Sku VARCHAR (MAX)=NULL,     
 @ManufacturerId VARCHAR (MAX)=NULL,     
 @ProductTypeId VARCHAR (MAX)=NULL,     
 @CategoryId VARCHAR (MAX)=NULL,     
 @CatalogId INT=NULL,     
 @SupplierID INT=NULL,
 @PortalID INT = NULL    
AS    
BEGIN    
    SET NOCOUNT ON;    
    CREATE TABLE #ProductList    
    (    
            ProductId INT,    
            SKU       NVARCHAR (1000)    
    );    
        
    CREATE UNIQUE INDEX IX_ProductList    
        ON #ProductList(ProductId) WITH (IGNORE_DUP_KEY = ON);    
            
    INSERT INTO #ProductList    
    SELECT PRODUCTID,    
           SKU    
    FROM   ZNodeSKU    
    WHERE  ((SKU LIKE '%' + @Sku + '%')    
            OR Len(@Sku) = 0);    
                
                
    SELECT (p.ProductID),    
           ImageFile,    
           p.Name,    
           p.ProductNum,    
           RetailPrice,    
           SalePrice,    
           WholeSalePrice,    
           p.DisplayOrder,    
           p.ActiveInd,   
           p.PortalID, 
           p.Franchisable,
           p.ReviewStateID,  
           (SELECT TOP 1 QuantityOnHand    
            FROM   ZNodeSKUInventory AS SkuInventory    
            WHERE  SkuInventory.SKU = sku.SKU) AS 'QuantityOnHand'    
    FROM   ZNodeProduct AS P    
           INNER JOIN    
           #ProductList AS sku    
           ON p.ProductID = sku.ProductID    
    WHERE  (p.Name LIKE '%' + @Name + '%'    
            OR Len(@Name) = 0)    
           AND (p.ProductNum LIKE '%' + @ProductNum + '%'    
                OR Len(@ProductNum) = 0)    
           AND (p.ProductTypeID = @ProductTypeId    
                OR Len(@ProductTypeId) = 0    
                OR @ProductTypeId = 0    
                OR @ProductTypeId = '0')    
           AND (p.ManufacturerID = @ManufacturerId    
                OR Len(@ManufacturerId) = 0    
                OR @ManufacturerId = '0')    
            AND (p.AccountID = @SupplierID
                OR Len(@SupplierID) = 0 
                OR @SupplierID= '0' or  @SupplierID is null) 
            AND(P.PortalID = @PortalID OR Len(@PortalID) = 0    
                OR @PortalID= '0' OR @PortalID = 0) 
           AND (p.ProductID IN (SELECT ProductId    
                                FROM   ZNODEPRODUCTCATEGORY    
                                WHERE  CATEGORYID IN (SELECT DISTINCT CategoryID    
                                                      FROM   ZNodeCategoryNode    
                                                      WHERE  CatalogID = @CatalogId))    
                OR @CatalogId = 0)    
           AND (p.ProductID IN (SELECT ProductId    
                                FROM   ZNODEPRODUCTCATEGORY    
                                WHERE  CATEGORYID = @CategoryId)    
                OR LEN(@CategoryId) = 0    
                OR @CategoryId = '0');    
END      
    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_Search_Addons]'
GO

ALTER PROCEDURE [dbo].[ZNode_Search_Addons]  
@NAME VARCHAR (MAX), @TITLE VARCHAR (MAX), @SKUPRODUCT VARCHAR (MAX), @LocaleId INT=NULL, @AccountId INT=NULL, @PortalID int=NULL  
AS  
BEGIN  
    SET NOCOUNT ON;  
    DECLARE @ADDONTITLE AS VARCHAR (MAX);  
    DECLARE @ADDONNAME AS VARCHAR (MAX);  
    IF (LEN(@NAME) = 0)  
        BEGIN  
            SET @ADDONNAME = '%';  
        END  
    ELSE  
        BEGIN  
            SET @ADDONNAME = '%' + @NAME + '%';  
        END  
    IF (LEN(@TITLE) = 0)  
        BEGIN  
            SET @ADDONTITLE = '%';  
        END  
    ELSE  
        BEGIN  
            SET @ADDONTITLE = '%' + @TITLE + '%';  
        END  
    IF (LEN(@LocaleId) = 0  
        OR @LocaleId = 0)  
        SET @LocaleId = NULL;  
    IF (LEN(@AccountId) = 0)  
        SET @AccountId = NULL;  
    CREATE TABLE #TEMP_ZNODEADDONS  
    (  
        ADDONID INT  
    );  
    INSERT INTO #TEMP_ZNODEADDONS (ADDONID)  
    (SELECT ADDONID  
     FROM   [ZNodeAddOnValue]  
     WHERE  SKU LIKE '%' + @SKUPRODUCT + '%');  
    INSERT INTO #TEMP_ZNODEADDONS (ADDONID)  
    ((SELECT ADDONID  
      FROM   ZNODEPRODUCTADDON  
      WHERE  PRODUCTID IN (SELECT PRODUCTID  
                           FROM   ZNODEPRODUCT  
                           WHERE  PRODUCTNUM LIKE '%' + @SKUPRODUCT + '%')));  
    IF LEN(@SKUPRODUCT) <> 0  
        BEGIN  
            SELECT ADDONID,  
                   NAME,  
                   TITLE,  
                   DESCRIPTION,  
                   DISPLAYORDER, 
                   DisplayType, 
                   OPTIONALIND,  
                   PortalID  
            FROM   ZNODEADDON  
            WHERE  NAME LIKE @ADDONNAME  
                   AND (AccountID IN (SELECT value  
                                      FROM   sc_splitter (@AccountId, ',')) OR AccountID IS NULL AND (PortalID = @PortalID OR (PortalID IS NULL AND (@PortalID IS NULL OR @PortalID = '0'))))
                   AND TITLE LIKE @ADDONTITLE  
                   AND ADDONID IN (SELECT ADDONID  
                                   FROM   #TEMP_ZNODEADDONS)  
                   AND LocaleId = COALESCE (@LocaleId, LocaleId);  
        END  
    ELSE  
        BEGIN  
            SELECT ADDONID,  
                   NAME,  
                   TITLE,  
                   DESCRIPTION,  
                   DISPLAYORDER, 
                   DisplayType, 
                   OPTIONALIND,  
                   PortalID  
            FROM   ZNODEADDON  
            WHERE  NAME LIKE @ADDONNAME  
                   AND (AccountID IN (SELECT value  
                                      FROM   sc_splitter (@AccountId, ',')) OR AccountID IS NULL AND (PortalID = @PortalID OR (PortalID IS NULL AND (@PortalID IS NULL OR @PortalID = '0'))))  
                   AND TITLE LIKE @ADDONTITLE  
                   AND LocaleId = COALESCE (@LocaleId, LocaleId);  
        END  
END  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetProductListXMLSiteMap]'
GO
 
Create PROCEDURE [dbo].[ZNode_GetProductListXMLSiteMap]
 @PortalID INT = NULL      
AS      
BEGIN      
    SET NOCOUNT ON;      
          
    SELECT DISTINCT
		P.SEOURL as loc,
		--CAST(P.UpdateDte AS Varchar(100)) As lastmod,
		P.UpdateDte As lastmod,
        P.ImageFile As [g:image_link],
        P.ProductID As [g:id],
        P.Name As title,
        '' as link,
        P.RetailPrice as [g:price],
        p.Description as [description],
        'new' As [g:condition],
        ZM.Name As [g:brand],
        'g:quantity' =
			CASE 
				WHEN P.AllowBackOrder = 0 AND P.TrackInventoryInd = 1 Then ZSI.QuantityOnHand
				ELSE 1
			END
			
    FROM
	ZNodeProduct P
	INNER JOIN ZNodeManufacturer ZM ON P.ManufacturerID = ZM.ManufacturerID
	INNER JOIN ZNodeProductCategory ZPC ON ZPC.ProductID = P.ProductID 
	INNER JOIN ZNodeCategoryNode ZCN ON ZCN.CategoryID = ZPC.CategoryID
	INNER JOIN ZNodePortalCatalog ZP ON ZP.CatalogID = zcn.CatalogID
	INNER JOIN ZNodeSKU ZS ON ZS.ProductID = P.ProductID
	INNER JOIN ZNodeSKUInventory ZSI ON ZSI.SKU = ZS.SKU
    WHERE  
		(ZP.PortalID = @PortalID OR Len(@PortalID) = 0 OR @PortalID= '0' OR @PortalID = 0)
		AND P.ActiveInd = 1
END        
      
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetPortalsByProduct]'
GO

-- ==========================================================   
-- Create date: May 29, 2010
-- Update date: July 05,2010        
-- Description: Retrieve product detail and return a serialized xml
-- ==========================================================
CREATE PROCEDURE [dbo].[ZNode_GetPortalsByProduct]  
(@PortalIds NVARCHAR(MAX))   
AS  
BEGIN  

  SELECT PortalID,StoreName FROM ZNodePortal WHERE PortalID IN (SELECT DISTINCT PortalID FROM ZNodeProduct)
     
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetContentListXMLSiteMap]'
GO
  
CREATE PROCEDURE [dbo].[ZNode_GetContentListXMLSiteMap]
 @PortalID INT = NULL      
AS      
BEGIN      
    SET NOCOUNT ON;      
          
    SELECT DISTINCT
		ZC.SEOURL AS loc,
		'' AS lastmod,
        '' AS [image],
        ZC.Name
    FROM  
		ZNodeContentPage ZC	
    WHERE  
		(ZC.PortalID = @PortalID OR Len(@PortalID) = 0 OR @PortalID= '0' OR @PortalID = 0)
		AND ZC.ActiveInd = 1
END        
      
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetCategoryListXMLSiteMap]'
GO
  
Create PROCEDURE [dbo].[ZNode_GetCategoryListXMLSiteMap]
 @PortalID INT = NULL      
AS      
BEGIN      
    SET NOCOUNT ON;      
          
    SELECT DISTINCT
		ZC.SEOURL AS loc,
		'' AS lastmod,
        '' AS [image],
        ZC.CategoryID
    FROM  
		ZNodeCategory ZC	
		INNER JOIN ZNodeCategoryNode ZCN ON ZC.CategoryID = ZCN.CategoryID
		INNER JOIN ZNodeCatalog ZCG ON ZCN.CatalogID = ZCG.CatalogID
		INNER JOIN ZNodePortalCatalog ZPC ON ZCG.CatalogID = ZPC.CatalogID
    WHERE  
		(ZPC.PortalID = @PortalID OR Len(@PortalID) = 0 OR @PortalID= '0' OR @PortalID = 0)
		AND ZC.VisibleInd = 1
END        
      
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchCountry]'
GO

ALTER PROCEDURE [dbo].[ZNode_SearchCountry]
@keyword NVARCHAR (MAX)
AS
BEGIN
    SELECT   Code,
             Name,
             'false' AS 'EnableCountry',
             'false' AS 'EnableShipping',
             ActiveInd
    FROM     ZNodeCountry
    WHERE    PATINDEX('%'+ @keyword+ '%',CODE )>0
             OR PATINDEX('%'+ @keyword + '%', Name)>0
    ORDER BY Name;
END 


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetStores]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetStores]
@keyword NVARCHAR (MAX)
AS
BEGIN
    SELECT   *
    FROM     ZNodePortal
    WHERE    PATINDEX('%'+ @keyword + '%', CompanyName)>0
             OR PATINDEX('%'+ @keyword+ '%',StoreName )>0
    ORDER BY StoreName;
END 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportsSupplierList]'
GO


ALTER PROCEDURE [dbo].[ZNode_ReportsSupplierList]
    (
      @FromDate DateTime = NULL,
      @ToDate DateTime = NULL,
      @PortalId VARCHAR(10),
      @SupplierId int	
    )
AS 
    BEGIN                                      
        SET NOCOUNT ON ;                           

		CREATE TABLE #SupplierTable (SKU NVARCHAR(MAX), SupplierID INT)
		-- Fetch only SupplierId assigned items (Product, Sku, AddOnValue)
				
			 INSERT INTO #SupplierTable
			 SELECT SKU, SupplierID			 
			 FROM ZNodeSku
			 WHERE (SupplierId IS NOT NULL) AND (SupplierId = @SupplierId OR @SupplierId = 0)
			 UNION
			 SELECT SKU, SupplierId
			 FROM ZNodeAddOnValue
			 Where (SupplierId IS NOT NULL) AND (SupplierId = @SupplierId OR @SupplierId = 0)
		

		CREATE TABLE #OrderLineItem (ProductNum NVARCHAR(MAX), SKU NVARCHAR(MAX), OrderID INT, OrderLineItemID INT)
										 
		INSERT INTO #OrderLineItem										 
		SELECT ProductNum, SKU, OrderID, OrderLineItemID 		
		FROM ZNodeOrderLineItem OL

		CREATE TABLE #OrderLineItemTable (ProductNum NVARCHAR(MAX), SKU NVARCHAR(MAX), OrderID INT, OrderLineItemID INT)
		
		INSERT INTO #OrderLineItemTable
		SELECT ProductNum, Sku, OrderID, OrderLineItemID 		
		FROM #OrderLineItem OL
		WHERE OL.Sku IN (SELECT Sku
								FROM #SupplierTable)
								
print @PortalId

		SELECT  O.[OrderID],
                O.OrderDate,
                O.[ShipFirstName],
                O.[ShipLastName],
                O.[ShipCompanyName],
                O.[ShipStreet],
                O.[ShipStreet1],
                O.[ShipCity],
                O.[ShipStateCode],
                O.[ShipPostalCode],
                O.[ShipCountry],
                O.[ShipPhoneNumber],
                P.[StoreName] AS 'StoreName',
                ( SELECT TOP 1
                            SU.[Name]
                  FROM      ZNodeSupplier SU
                  WHERE     SU.SupplierId = S.SupplierID
                ) AS 'Custom1',
                ISNULL(S.SupplierID,-1) AS SupplierID
        FROM ZNodeOrder O
        INNER JOIN #OrderLineItemTable OL ON O.OrderID = OL.OrderID         
        INNER JOIN ZNodePortal P ON P.PortalId = O.PortalId
        INNER JOIN #SupplierTable S ON S.Sku = OL.Sku         
        WHERE   (P.PortalID = @PortalId OR @PortalId = '0')
                AND O.OrderDate BETWEEN @FromDate AND @ToDate                
                ORDER BY O.OrderID DESC

                
		-- Select Order Line Item                
        SELECT  Ol.OrderId,
                Ol.SKU,
                Ol.Quantity,
                Ol.Name,
                Ol.Price,
                ISNULL(S.SupplierID,-1) AS SupplierID
        FROM    ZNodeOrderLineItem Ol            
        INNER JOIN #SupplierTable S ON S.Sku = OL.Sku         
        WHERE   Ol.OrderLineItemID IN (
                SELECT  O.[OrderLineItemID]
                FROM    #OrderLineItemTable O)
        ORDER BY Ol.OrderID DESC
    END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeProduct table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_Get_List

AS


				
				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetByManufacturerID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetByManufacturerID
(

	@ManufacturerID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[ManufacturerID] = @ManufacturerID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetByProductTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetByProductTypeID
(

	@ProductTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[ProductTypeID] = @ProductTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetByShippingRuleTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetByShippingRuleTypeID
(

	@ShippingRuleTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[ShippingRuleTypeID] = @ShippingRuleTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetByAccountID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetByAccountID
(

	@AccountID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[AccountID] = @AccountID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetByPortalID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetByPortalID
(

	@PortalID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[PortalID] = @PortalID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetByReviewStateID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetByReviewStateID
(

	@ReviewStateID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[ReviewStateID] = @ReviewStateID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetBySupplierID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetBySupplierID
(

	@SupplierID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[SupplierID] = @SupplierID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetByTaxClassID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetByTaxClassID
(

	@TaxClassID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[TaxClassID] = @TaxClassID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_DeleteCatalog]'
GO
SET ANSI_NULLS ON
GO


ALTER PROCEDURE [dbo].[ZNode_DeleteCatalog]        
 @CatalogID INT
,@Preserve BIT = 1
   
AS     
BEGIN          
       

IF (@Preserve = 0)
BEGIN

	CREATE TABLE #CategoryList (CategoryID INT)    
	    
	INSERT INTO #CategoryList    
		SELECT CategoryID FROM ZNodeCategoryNode ZCN    
			WHERE ZCN.CatalogID = @CatalogID
			AND ZCN.CategoryID NOT IN (SELECT CategoryID FROM ZNodeCategoryNode WHERE CatalogID != @CatalogID)  
			GROUP BY CategoryID;    
	    
	CREATE TABLE #ProductList (ProductID INT)    
	    
	INSERT INTO #ProductList    
		SELECT ProductID FROM #CategoryList ZCN 
			INNER JOIN ZNodeProductCategory ZPC ON ZPC.CategoryID = ZCN.CategoryID
			AND ProductID NOT IN (SELECT ProductID FROM ZNodeProductCategory  
									WHERE CategoryID IN (SELECT CategoryID FROM ZNodeCategoryNode WHERE CatalogID != @CatalogID))    
			GROUP BY ProductID;    
	    	    
	DECLARE  @ProductTypeTable Table([ID] INT);    
	       
	INSERT INTO @ProductTypeTable (ID)    
		SELECT ProductTypeID FROM ZnodeProduct    
		WHERE    
		ProductID IN (SELECT ProductID FROM #ProductList)    
		AND ProductTypeID NOT IN (SELECT ProductTypeId FROM ZNodeProductType WHERE IsGiftCard=1)
		GROUP BY ProductTypeID;    
	    
	DECLARE @SkuExists BIT;    
	DECLARE @AddOnExists BIT;    
	DECLARE @TagExists BIT;    
	DECLARE @ProductTypeExists BIT;    
	DECLARE @ManufacturerExists BIT;    
	    
	SET @AddOnExists = 0;    
	SET @SkuExists = 0;    
	SET @TagExists = 0;    
	SET @ProductTypeExists = 0;    
	    
	-- AddOn     
	CREATE TABLE #AddOnId (AddOnId INT)
	INSERT INTO #AddOnId SELECT AddOnID FROM (SELECT ZPA.AddOnID FROM ZNodeProductAddOn ZPA    
						INNER JOIN ZNodeAddOn ZA ON ZPA.AddOnID = ZA.AddOnID    
						WHERE ZPA.ProductID IN (SELECT ProductID FROM #ProductList)) tmpPAddOn
						WHERE tmpPAddOn.AddOnID NOT IN
						(SELECT ZPA.AddOnID FROM ZNodeProductAddOn ZPA    
						INNER JOIN ZNodeAddOn ZA ON ZPA.AddOnID = ZA.AddOnID    
						WHERE ZPA.ProductID NOT IN (SELECT ProductID FROM #ProductList))
	    
	-- SKU    
	CREATE TABLE #AttributeTypeId (AttributeTypeId INT)
	INSERT INTO #AttributeTypeId
					SELECT DISTINCT AttributeTypeId FROM (SELECT AttributeTypeId FROM ZNodeProductAttribute ZPA    
						INNER JOIN ZNodeSKUAttribute ZSA ON ZSA.AttributeId = ZPA.AttributeId    
						INNER JOIN ZNodeSKU ZS ON ZSA.SKUID = ZS.SKUID    
						WHERE ZS.ProductID IN (SELECT ProductID FROM #ProductList)) tmpPA
					WHERE tmpPA.AttributeTypeId						
						NOT IN (SELECT AttributeTypeId FROM ZNodeProductAttribute ZPA    
						INNER JOIN ZNodeSKUAttribute ZSA ON ZSA.AttributeId = ZPA.AttributeId    
						INNER JOIN ZNodeSKU ZS ON ZSA.SKUID = ZS.SKUID    
						WHERE ZS.ProductID NOT IN (SELECT ProductID FROM #ProductList))
						    
	CREATE TABLE #TagID (TagID INT)
	INSERT INTO #TagID SELECT TagID FROM (SELECT TagID
						FROM ZNodeTagProductSKU TPS    
						WHERE TPS.ProductID IN (SELECT ProductID FROM #ProductList)) tmpPTag
						WHERE tmpPTag.TagID NOT IN
						(SELECT TagID
						FROM ZNodeTagProductSKU TPS    
						WHERE TPS.ProductID NOT IN (SELECT ProductID FROM #ProductList))

	CREATE TABLE #TagGroupID (TagGroupID INT)
	INSERT INTO #TagGroupID SELECT TagGroupID FROM (SELECT TagGroupID
						FROM ZNodeTag TPS    
						WHERE TPS.TagID IN (SELECT TagID FROM #TagID)) tmpPTag
						WHERE tmpPTag.TagGroupID NOT IN
						(SELECT TagGroupID
						FROM ZNodeTag TP    
						WHERE TP.TagID IN (SELECT TagID
						FROM ZNodeTagProductSKU TPS WHERE TPS.ProductID NOT IN (SELECT ProductID FROM #ProductList)))


	SET @ProductTypeExists = (SELECT Count(1) FROM ZNodeProduct p    
								WHERE p.ProductID NOT IN (SELECT ProductID FROM #ProductList)    
								AND p.ProductTypeID IN (SELECT ID FROM @ProductTypeTable))    

	-- Delete the data for persistent cart
	DELETE FROM ZNODESAVEDCARTLINEITEM 
			WHERE SKUID IN 
				(SELECT AddOnValueId FROM ZNodeAddOnValue WHERE AddOnID IN 
				(SELECT ZPA.AddOnID FROM ZNodeProductAddOn ZPA    
				  INNER JOIN ZNodeAddOn ZA ON ZPA.AddOnID = ZA.AddOnID    
				  INNER JOIN #ProductList PL ON ZPA.ProductID = PL.ProductID))
			AND OrderLineItemRelationshipTypeID = 2

	DELETE FROM ZNODESAVEDCARTLINEITEM 
			WHERE SKUID IN (SELECT SKUID FROM ZNodeSKU WHERE ProductID IN (SELECT ProductID FROM #ProductList))    
			AND OrderLineItemRelationshipTypeID = 1

	DELETE FROM ZNODESAVEDCARTLINEITEM 
			WHERE SKUID IN (SELECT SKUID FROM ZNodeSKU WHERE ProductID IN (SELECT ProductID FROM #ProductList))    
			AND OrderLineItemRelationshipTypeID IS NULL

	DELETE FROM ZNODESAVEDCART WHERE SavedCartID NOT IN (Select SavedCartID FROM ZNodeSavedCartLineItem)
	 
	DELETE FROM ZNODECOOKIEMAPPING WHERE CookieMappingID NOT IN (Select CookieMappingID FROM ZNODESAVEDCART)
	    
	-- Delete AddOn Records				    
	DELETE FROM ZNodeProductAddOn WHERE ProductID    
		IN (SELECT ProductID FROM #ProductList);  
		
	DELETE FROM ZNodeAddOnValue WHERE AddOnID IN (SELECT AddOnID FROM #AddOnId)    
	  
	DELETE FROM ZNodeAddOn WHERE AddOnID IN (SELECT AddOnID FROM #AddOnId)    
	  

	-- Delete Atttibute Records       
	DELETE FROM ZNodeSKUAttribute 
		WHERE SKUID IN (SELECT SKUID FROM ZNodeSKU WHERE ProductID IN (SELECT ProductID FROM #ProductList))    
		
	DELETE FROM ZNodeProductAttribute WHERE AttributeTypeId IN   
			(SELECT AttributeTypeId FROM #AttributeTypeId)    
	    
	DELETE FROM ZNodeAttributeType WHERE AttributeTypeId IN   
			(SELECT AttributeTypeId FROM #AttributeTypeId)   
	    
	DELETE FROM ZNodeSKU 
		WHERE ProductID IN (SELECT ProductID FROM #ProductList)    

	DELETE FROM ZNodeTagProductSKU    
			WHERE ProductID IN (SELECT ProductID FROM #ProductList);    
	        
	DELETE FROM ZNodeTag    
			WHERE TagID IN (SELECT TagID FROM #TagID)

	DELETE FROM ZNodeTagGroup   
			WHERE TagGroupID IN (SELECT TagGroupID FROM #TagGroupID)    
	    
	-- DELETE product reviews    
	DELETE FROM ZNodeReview    
		WHERE ProductID IN (SELECT ProductID FROM #ProductList);    
	    
	-- Delete Product Highlights    
	DELETE FROM ZNodeProductHighlight    
		WHERE ProductID IN (SELECT ProductID FROM #ProductList);    
	    
	-- Delete Product alternate and swatch images    
	DELETE FROM ZNodeProductImage    
		WHERE ProductID IN (SELECT ProductID FROM #ProductList);    
	    
	-- Delete Product Digital Assets    
	DELETE FROM ZNodeDigitalAsset    
		WHERE ProductID IN (SELECT ProductID FROM #ProductList);    
	    
	-- Delete Product category assocation    
	DELETE FROM ZNodeProductCategory 
		WHERE ProductID IN (SELECT ProductID FROM #ProductList)    
	    
	-- Delete Product Review History
	DELETE FROM ZNODEPRODUCTREVIEWHISTORY 
		WHERE ProductID IN (SELECT ProductID FROM #ProductList)    
	    
	-- Delete Product records    
	DELETE FROM ZNodeProduct 
		WHERE ProductID IN (SELECT ProductID FROM #ProductList);    
	    
	IF @ProductTypeExists = 0    
	BEGIN        
		CREATE TABLE #AttributeTypeData (AttributeTypeID INT)
		
		INSERT INTO #AttributeTypeData (AttributeTypeID)
			SELECT AttributeTypeID FROM ZNodeProductTypeAttribute     
					WHERE ProductTypeId IN (SELECT ID FROM @ProductTypeTable);    
		  
		DELETE FROM ZNodeProductTypeAttribute     
			WHERE ProductTypeId IN (SELECT ID FROM @ProductTypeTable);    
		 
		DELETE FROM ZNodeProductTypeAttribute     
			WHERE AttributeTypeId IN (SELECT AttributeTypeId FROM #AttributeTypeData);    
		 
		DELETE FROM ZNodeProductType 
			WHERE ProductTypeId IN (SELECT ID FROM @ProductTypeTable)
	 
	END    
	    
	-- Delete Catalog category records and Category  
	  
	CREATE TABLE #CategoryData (CategoryID INT)

	INSERT INTO #CategoryData (CategoryID)  
		SELECT CategoryID FROM ZNodeCategoryNode WHERE CatalogID = @CatalogID		    
		AND CategoryID NOT IN (SELECT CategoryID FROM ZNodeCategoryNode WHERE CatalogID != @CatalogID)
	DELETE FROM ZNodeCategory 
		WHERE CategoryID IN (SELECT CategoryID FROM #CategoryData)		
				
END

DELETE FROM ZNodeCategoryNode WHERE CatalogID = @CatalogID;    

-- Delete Portal Catalog assocaition    
DELETE FROM ZNodePortalCatalog WHERE CatalogID = @CatalogID;    
    
-- Delete Catalog    
DELETE FROM ZNodeCatalog WHERE CatalogID = @CatalogID;    
    
END    
        
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetBySEOURL]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetBySEOURL
(

	@SEOURL nvarchar (100)  
)
AS


				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[SEOURL] = @SEOURL
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportsTaxFiltered]'
GO
SET ANSI_NULLS ON
GO



ALTER PROCEDURE [dbo].[ZNode_ReportsTaxFiltered]
    (
      @FromDate DATETIME,
      @ToDate DATETIME,
      @Interval Varchar(50),
      @PortalId VARCHAR(10) = '0'
    )
AS 
    BEGIN                                      
        SET NOCOUNT ON ; 
       
        IF ( LEN(@Interval) > 0 ) 
            BEGIN
                SELECT  P.[StoreName] AS 'StoreName',
                        O.OrderDate,
                        O.ShipStateCode,
                        SUM(O.subtotal) AS 'SubTotal',
                        SUM(ISNULL(O.salestax,0)) AS 'TaxCost',
                        SPACE(10) AS 'Title',
                        CASE @Interval
                          WHEN 'month' THEN DATENAME(month, O.OrderDate)
                          WHEN 'quarter' THEN DATENAME(quarter, O.OrderDate)
                        END AS 'Custom1'
                FROM    ZNodeOrder O
                        INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
                WHERE   O.OrderDate >= @FromDate
                        AND O.OrderDate <= @ToDate
                        AND (P.PortalID = @PortalId OR @PortalId = '0')
                GROUP BY P.StoreName,
                        O.OrderDate,
                        O.ShipStateCode
                HAVING SUM(O.SalesTax) > 0 
                ORDER BY O.OrderDate
            END
        ELSE 
            BEGIN
                SELECT  P.[StoreName] AS 'StoreName',
                        O.OrderDate,
                        O.ShipStateCode,
                        SUM(O.subtotal) AS 'SubTotal',
                        SUM(ISNULL(O.salestax,0)) AS 'TaxCost',
                        SPACE(30) As 'Custom1'
                FROM    ZNodeOrder O
                        INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
                GROUP BY P.StoreName,
                        O.OrderDate,
                        O.ShipStateCode
                HAVING  SUM(O.SalesTax) > 0 
            END
                                
    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetByActiveInd]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetByActiveInd
(

	@ActiveInd bit   
)
AS


				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[ActiveInd] = @ActiveInd
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetByProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeProduct table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetByProductID
(

	@ProductID int   
)
AS


				SELECT
					[ProductID],
					[Name],
					[ShortDescription],
					[Description],
					[FeaturesDesc],
					[ProductNum],
					[ProductTypeID],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[ImageFile],
					[ImageAltTag],
					[Weight],
					[Length],
					[Width],
					[Height],
					[BeginActiveDate],
					[EndActiveDate],
					[DisplayOrder],
					[ActiveInd],
					[CallForPricing],
					[HomepageSpecial],
					[CategorySpecial],
					[InventoryDisplay],
					[Keywords],
					[ManufacturerID],
					[AdditionalInfoLink],
					[AdditionalInfoLinkLabel],
					[ShippingRuleTypeID],
					[ShippingRate],
					[SEOTitle],
					[SEOKeywords],
					[SEODescription],
					[Custom1],
					[Custom2],
					[Custom3],
					[ShipEachItemSeparately],
					[AllowBackOrder],
					[BackOrderMsg],
					[DropShipInd],
					[DropShipEmailID],
					[Specifications],
					[AdditionalInformation],
					[InStockMsg],
					[OutOfStockMsg],
					[TrackInventoryInd],
					[DownloadLink],
					[FreeShippingInd],
					[NewProductInd],
					[SEOURL],
					[MaxQty],
					[ShipSeparately],
					[FeaturedInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[TaxClassID],
					[MinQty],
					[ReviewStateID],
					[AffiliateUrl],
					[IsShippable],
					[AccountID],
					[PortalID],
					[Franchisable],
					[ExpirationPeriod],
					[ExpirationFrequency]
				FROM
					[dbo].[ZNodeProduct]
				WHERE
					[ProductID] = @ProductID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_CopyCatalog]'
GO
SET ANSI_NULLS ON
GO
  
ALTER PROCEDURE [dbo].[ZNode_CopyCatalog]   
 @CatalogID int,    
 @CatalogName nvarchar(max),    
 @LocaleID int = NULL     
AS    
BEGIN     
SET NOCOUNT ON;    
    
/* Insert a new Catalog Record */    
DECLARE @NewCatalogId int, @ErrCode int, @ErrMessage nvarchar(4000);    
  
INSERT INTO [ZNodeCatalog]      
           ([Name]    
           ,[IsActive])    
SELECT     
   @CatalogName,     
   IsActive    
FROM    
   ZNodeCatalog     
WHERE     
   CatalogId = @CatalogID;    
      
   SELECT @NewCatalogID = @@IDENTITY;     
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    


/* Get the LocaleCode */
DECLARE @LocaleCode VARCHAR(10) = NULL
SET @LocaleCode = (SELECT LocaleCode FROM ZNodeLocale WHERE LocaleID = @LocaleID)

       
/* Create copy of the PortalCatalog record, of existing catalog for new catalog */          
    
-- This block is commented because right now we are not using locale    
-- Comment Start    
/*     
INSERT INTO     
   [ZNodePortalCatalog]    
           ([PortalID]    
           ,[CatalogID]    
           ,[LocaleID]    
           ,[Theme]    
           ,[CSS])               
SELECT     
   PortalID,     
   @NewCatalogId,     
   @LocaleID,     
   Theme,     
   CSS    
FROM    
   ZNodePortalCatalog ZP     
WHERE    
   ZP.CatalogID = @CatalogID;        
       
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
   */    
--Comment End    
-- PRINT @NewCatalogId    
/* Table variable to record the existing productid and link to the newly copied product */        
DECLARE  @ProductIDTable TABLE(    
   [KeyField] [int] IDENTITY(1,1),    
   [OldProductID] [int],    
   [NewProductID] [int]);    
       
INSERT INTO     
   @ProductIDTable ([OldProductID])    
SELECT    
   ProductID    
FROM    
   ZNodeProduct     
WHERE    
   ProductID IN    
   (SELECT    
    ProductID     
    FROM    
    ZNodeProductCategory     
    WHERE    
    CategoryID IN    
    (SELECT    
     CategoryID     
     FROM     
     ZNodeCategoryNode     
    WHERE     
     CatalogID = @CatalogID));    
    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
    
/******************************************************************************    
 Iterate through the table variable created above, insert new copy product,     
 update table variable with new product id     
 *****************************************************************************/    
DECLARE  @i [int], @maxI [int], @newID [int]    
SET @i = 1;    
    
SELECT     
   @maxI = Max([KeyField])     
FROM     
   @ProductIDTable;    
    
WHILE  @i <= @maxI    
BEGIN    
   INSERT INTO [ZNodeProduct]    
           ([Name]    
           ,[ShortDescription]    
           ,[Description]    
           ,[FeaturesDesc]    
           ,[ProductNum]    
           ,[ProductTypeID]    
           ,[RetailPrice]    
           ,[SalePrice]    
           ,[WholesalePrice]    
           ,[ImageFile]    
           ,[ImageAltTag]    
           ,[Weight]    
           ,[Length]    
           ,[Width]    
           ,[Height]    
           ,[BeginActiveDate]    
           ,[EndActiveDate]    
           ,[DisplayOrder]    
           ,[ActiveInd]    
           ,[CallForPricing]    
           ,[HomepageSpecial]    
           ,[CategorySpecial]    
           ,[InventoryDisplay]    
           ,[Keywords]    
           ,[ManufacturerID]    
           ,[AdditionalInfoLink]    
           ,[AdditionalInfoLinkLabel]    
           ,[ShippingRuleTypeID]    
           ,[SEOTitle]    
           ,[SEOKeywords]    
           ,[SEODescription]    
           ,[Custom1]    
           ,[Custom2]    
           ,[Custom3]    
           ,[ShipEachItemSeparately]
           ,[AllowBackOrder]    
           ,[BackOrderMsg]    
           ,[DropShipInd]    
           ,[DropShipEmailID]    
           ,[Specifications]    
           ,[AdditionalInformation]    
           ,[InStockMsg]    
           ,[OutOfStockMsg]    
           ,[TrackInventoryInd]    
           ,[DownloadLink]    
           ,[FreeShippingInd]    
           ,[NewProductInd]    
           ,[SEOURL]    
           ,[MaxQty]    
           ,[ShipSeparately]    
           ,[FeaturedInd]    
           ,[WebServiceDownloadDte]    
           ,[UpdateDte]    
           ,[SupplierID]    
           ,[RecurringBillingInd]    
           ,[RecurringBillingInstallmentInd]    
		   ,[RecurringBillingPeriod]    
           ,[RecurringBillingFrequency]    
           ,[RecurringBillingTotalCycles]    
           ,[RecurringBillingInitialAmount]    
           ,[TaxClassID]
           ,[ReviewStateID]
        ,[AffiliateUrl]
		,[IsShippable]
		,[AccountID]
		,[PortalID]
		,[Franchisable]
		,[ExpirationPeriod]
		,[ExpirationFrequency])    
SELECT     
   [Name],     
   [ShortDescription],    
   [Description],    
   [FeaturesDesc],    
   [ProductNum],    
   [ProductTypeID],    
   [RetailPrice],    
   [SalePrice],    
   [WholesalePrice],    
   [ImageFile],    
   [ImageAltTag],    
   [Weight],    
   [Length],    
   [Width],    
   [Height],    
   [BeginActiveDate],    
   [EndActiveDate],    
   [DisplayOrder],    
   [ActiveInd],    
   [CallForPricing],    
   [HomepageSpecial],     
   [CategorySpecial],    
   [InventoryDisplay],    
   [Keywords],    
   [ManufacturerID],     
   [AdditionalInfoLink],     
   [AdditionalInfoLinkLabel],    
   [ShippingRuleTypeID],     
   [SEOTitle],    
   [SEOKeywords],    
   [SEODescription],    
   [Custom1],    
   [Custom2],     
   [Custom3],     
   [ShipEachItemSeparately],
   [AllowBackOrder],    
   [BackOrderMsg],     
   [DropShipInd],    
   [DropShipEmailID],     
   [Specifications],    
   [AdditionalInformation],    
   [InStockMsg],    
   [OutOfStockMsg],    
   [TrackInventoryInd],    
   [DownloadLink],     
   [FreeShippingInd],     
   [NewProductInd],     
   null,     
   [MaxQty],    
   [ShipSeparately],     
   [FeaturedInd],     
   [WebServiceDownloadDte],     
   [UpdateDte],     
   [SupplierID],     
   [RecurringBillingInd],     
   [RecurringBillingInstallmentInd],     
   [RecurringBillingPeriod],    
   [RecurringBillingFrequency],    
   [RecurringBillingTotalCycles],
   [RecurringBillingInitialAmount],     
   [TaxClassID],
   [ReviewStateId]
    ,[AffiliateUrl]
	,[IsShippable]
	,[AccountID]
	,[PortalID]
	,[Franchisable]
	,[ExpirationPeriod]
	,[ExpirationFrequency]
FROM    
   ZNodeProduct P    
INNER JOIN    
   @ProductIDTable PT    
ON    
   P.ProductID = PT.OldProductID    
WHERE    
   PT.KeyField = @i;    
    
   SELECT @newID = @@IDENTITY    
      
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
       
   UPDATE     
     @ProductIDTable    
   SET    
     NewProductID = @newID    
   WHERE    
     KeyField = @i;    
         
   SELECT @i = @i + 1;    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
END    
          
       
/* Create table variable to record existing categoryid, linked to new categoryid*/       
DECLARE  @CategoryIDTable TABLE (    
   [KeyField] [int] IDENTITY(1,1),    
   [OldCategoryID] [int],    
   [NewCategoryID] [int]);    
       
INSERT INTO @CategoryIDTable    
           ([OldCategoryID])    
SELECT    
   [CategoryId]    
FROM    
   ZNodeCategory    
WHERE    
   CategoryID IN    
   (SELECT    
    CategoryID     
   FROM    
    ZNodeCategoryNode     
   WHERE    
    CatalogID = @CatalogID);      
      
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
    
/******************************************************************************    
 Iterate through the table variable created above, insert new copy category,     
 update table variable with new category id     
 *****************************************************************************/     
DECLARE  @intCounter int, @int int, @new int;    
SET @int = 1;    
    
SELECT     
   @intCounter = Max([KeyField])     
FROM     
   @CategoryIDTable;    
    
WHILE  @int <= @intCounter     
BEGIN    
  INSERT INTO [ZNodeCategory]    
           ([Name]    
           ,[Title]    
           ,[ShortDescription]    
           ,[Description]    
           ,[ImageFile]    
           ,[ImageAltTag]    
           ,[VisibleInd]    
           ,[SubCategoryGridVisibleInd]    
		   ,[SEOTitle]    
           ,[SEOKeywords]    
           ,[SEODescription]    
           ,[AlternateDescription]    
           ,[DisplayOrder]    
           ,[Custom1]    
           ,[Custom2]    
           ,[Custom3]    
           ,[SEOURL])    
SELECT    
   [Name],    
   [Title],    
   [ShortDescription],    
   [Description],    
   [ImageFile],    
   [ImageAltTag],    
   [VisibleInd],    
   [SubCategoryGridVisibleInd],     
   [SEOTitle],    
   [SEOKeywords],    
   [SEODescription],    
   [AlternateDescription],    
   [DisplayOrder],    
   [Custom1],     
   [Custom2],     
   [Custom3],     
   null    
FROM    
   ZNodeCategory C    
INNER JOIN    
   @CategoryIDTable CT    
ON    
   CT.OldCategoryID = C.CategoryID    
WHERE    
   KeyField = @int;    
       
   SELECT @new = @@IDENTITY;    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
   UPDATE    
     @CategoryIDTable    
   SET    
     NewCategoryID = @new    
   WHERE     
     KeyField = @int;    
     
   SELECT @int = @int + 1;    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
END    
    
/*Insert newly created product and categories */       
INSERT INTO [ZNodeProductCategory]    
   ([ProductID]     
   ,[Theme]     
   ,[MasterPage]     
   ,[CSS]     
   ,[CategoryID]     
   ,[BeginDate]    
   ,[EndDate]    
   ,[DisplayOrder]    
   ,[ActiveInd])    
SELECT     
   PT.NewProductID,     
   ZP.Theme,     
   ZP.MasterPage,     
   ZP.CSS,     
   CT.NewCategoryID,     
   ZP.BeginDate,     
   ZP.EndDate,     
   ZP.DisplayOrder,     
   ZP.ActiveInd    
FROM    
   ZNodeProductCategory ZP    
INNER JOIN    
   @CategoryIDTable CT     
ON     
   CT.OldCategoryID = ZP.CategoryID    
INNER JOIN    
   @ProductIDTable PT     
ON    
   PT.OldProductID = ZP.ProductID;    
    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
    
/* Create table variable to record existing categoryid, linked to new categoryid*/       
DECLARE  @CategoryNodeIDTable TABLE (    
   [KeyField] [int] IDENTITY(1,1),    
   [OldID] [int],    
   [ParentID] [int],    
   [NewID] [int]);    
    
INSERT INTO @CategoryNodeIDTable    
           ([OldID],    
            [ParentID])    
SELECT    
   [CategoryNodeId],    
   [ParentCategoryNodeID]    
FROM    
   [ZNodeCategoryNode]    
WHERE        
   CatalogID = @CatalogID;     
       
          
DECLARE  @CNCount int, @CNIndex int, @CNNew int;    
SET @CNIndex = 1;    
    
SELECT     
   @CNCount = Max([KeyField])     
FROM     
   @CategoryNodeIDTable;    
    
WHILE  @CNIndex <= @CNCount    
BEGIN    
 INSERT INTO [ZNodeCategoryNode]    
    ([CatalogID]    
    ,[CategoryID]    
    ,[ParentCategoryNodeID]        
    ,[BeginDate]    
    ,[EndDate]    
    ,[Theme]    
    ,[MasterPage]    
    ,[CSS]    
    ,[DisplayOrder]    
    ,[ActiveInd])    
 SELECT     
    @NewCatalogID,     
    NewCategoryID,    
    ParentCategoryNodeID,    
    BeginDate,     
    EndDate,     
    Theme,     
    MasterPage,     
    CSS,     
    CN.DisplayOrder,     
    ActiveInd    
 FROM    
    ZnodeCategoryNode CN    
 INNER JOIN    
    @CategoryIDTable CT    
 ON    
    CT.OldCategoryID = CN.CategoryID    
 WHERE        
    CategoryNodeID = (SELECT OldId FROM @CategoryNodeIDTable WHERE KeyField = @CNIndex); --Added this line to avoid the duplicate    
    
 IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END     
        
 SELECT @CNNew = @@IDENTITY;    
      
 UPDATE @CategoryNodeIDTable     
 SET  [NewID] = @CNNew     
 WHERE KeyField = @CNIndex;    
     
     
 SELECT @CNIndex = @CNIndex + 1;    
           
END             
            
-- Update ParentCategoryNodeId with New CategoryNodeId    
UPDATE dbo.ZNodeCategoryNode    
SET  ParentCategoryNodeID = D.[NewID]    
FROM @CategoryNodeIDTable D     
WHERE ZNodeCategoryNode.ParentCategoryNodeID = D.OldID    
  AND ZNodeCategoryNode.[CatalogID] = @NewCatalogID;    
       
IF @@ERROR != 0     
BEGIN    
 SET @ErrMessage = ERROR_MESSAGE()    
 RETURN @ErrMessage    
END    
        
/* Table variable to record existing taggroupid's linked to new taggroupids */    
DECLARE  @TagGroupTable Table(    
   KeyField int IDENTITY(1,1),     
   OldTagGroupID int,     
   NewTagGroupID int);    
    
INSERT INTO    
   @TagGroupTable     
   (OldTagGroupID)    
SELECT     
   TG.TagGroupID    
FROM     
   ZNodeTagGroup TG    
WHERE    
   TG.TagGroupID IN    
   (SELECT     
     TagGroupID     
   FROM    
     ZNodeTagGroup TG    
   WHERE    
     TG.CatalogID = @CatalogID);    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
/******************************************************************************    
 Iterate through the table variable created above, insert new copy tag groups,     
 update table variable with new taggroup id     
 *****************************************************************************/    
DECLARE  @intMaxID int, @NewIdentity int, @intCount int;    
SET @intCount = 1;    
    
SELECT     
   @intMaxID = MAX(KeyField)     
FROM    
   @TagGroupTable;    
    
WHILE  @intCount <= @intMaxID    
BEGIN    
    
 INSERT INTO ZNodeTagGroup     
    (TagGroupLabel,     
    CatalogID,    
    ControlTypeID)    
 SELECT     
    TagGroupLabel,     
    @NewCatalogID,    
    ControlTypeID    
 FROM     
    ZNodeTagGroup T    
 INNER JOIN    
    @TagGroupTable TT    
 ON    
    TT.OldTagGroupID = T.TagGroupID    
 WHERE    
    KeyField = @intCount;    
        
 SELECT  @newIdentity = @@IDENTITY;    
     
 IF @@ERROR != 0     
 BEGIN    
   SET @ErrMessage = ERROR_MESSAGE()    
   RETURN @ErrMessage    
 END    
       
 UPDATE    
    @TagGroupTable     
 SET     
    NewTagGroupID = @newIdentity    
 WHERE     
    KeyField = @intCount;    
        
 IF @@ERROR != 0     
  BEGIN    
   SET @ErrMessage = ERROR_MESSAGE()    
   RETURN @ErrMessage    
  END    
        
     
         
 SELECT @intCount = @intCount + 1;    
     
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
END    
    
    
-- Update Association    
INSERT INTO [ZNodeTagGroupCategory]    
     ([TagGroupID]    
     ,[CategoryID]    
     ,[CategoryDisplayOrder])    
 SELECT      
    T.NewTagGroupID,     
    C.NewCategoryID,     
    TG.CategoryDisplayOrder    
 FROM     
    ZNodeTagGroupCategory TG    
 INNER JOIN    
    @TagGroupTable T     
 ON    
    T.OldTagGroupID = TG.TagGroupID    
 INNER JOIN    
    @CategoryIDTable C    
 ON    
    C.OldCategoryID = TG.CategoryID    
    
    
/* Table variable to record existing tag ids linked to new tag id's */    
DECLARE  @TagTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewTagID] int,     
   [OldTagID] int);    
    
    
INSERT INTO @TagTable     
   (OldTagID)    
SELECT      
   T.TagID    
FROM    
   ZnodeTag T    
WHERE    
   T.TagGroupID IN    
   (SELECT     
     TagGroupID    
   FROM    
     @TagGroupTable TG    
   INNER JOIN    
     ZNodeTag T    
   ON    
     TG.OldTagGroupID = T.TagGroupID);    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
        
/******************************************************************************    
 Iterate through the table variable created above, insert new copy tags,     
 update table variable with new tag id     
 *****************************************************************************/    
DECLARE @intMID int, @nId int, @iCount int;    
SET @iCount = 1;    
    
SELECT @intMID = MAX(KeyField) From @TagTable;    
    
WHILE @iCount <= @intMID    
BEGIN    
  INSERT INTO ZNodeTag     
     (TagGroupId    
     ,TagName    
     ,TagDisplayOrder    
     ,IconPath)    
  SELECT     
     TG.NewTagGroupID,     
     T.TagName,     
     T.TagDisplayOrder,     
     IconPath    
  FROM     
     ZNodeTag T    
  INNER JOIN    
     @TagTable TT    
  ON    
   TT.OldTagID = T.TagId    
  INNER JOIN    
     @TagGroupTable TG    
  ON    
     T.TagGroupID = TG.OldTagGroupID    
  WHERE    
     TT.KeyField = @iCount;    
    
  SELECT  @nId = @@IDENTITY;    
      
  IF @@ERROR != 0     
  BEGIN    
   SET @ErrMessage = ERROR_MESSAGE()    
   RETURN @ErrMessage    
  END    
     
  UPDATE      
     @TagTable     
  SET       
     NewTagID = @nId    
  WHERE     
     KeyField = @iCount;    
    
  SELECT  @iCount = @iCount + 1;    
      
  IF @@ERROR != 0     
  BEGIN    
   SET @ErrMessage = ERROR_MESSAGE()    
   RETURN @ErrMessage    
  END    
        
END    
    
/* Insert new tag and product records in the znodetagproductsku table */       
INSERT INTO [ZNodeTagProductSKU]    
   ([TagID]    
   ,[ProductID])    
SELECT     
   T.NewTagID,     
   P.NewProductID    
FROM    
   @TagTable T    
INNER JOIN    
   ZNodeTagProductSKU TPS    
ON    
   TPS.TagID = T.OldTagID    
INNER JOIN    
   @ProductIDTable P    
ON    
   P.OldProductID = TPS.ProductID;    
    
       
   IF @@ERROR != 0     
   BEGIN    
    SET @ErrMessage = ERROR_MESSAGE()    
    RETURN @ErrMessage    
   END    
     
         
    
/* Variables for Iterations */    
DECLARE @recCount INT    
DECLARE @recIndex INT    
DECLARE @recID INT    
    
------------------------------    
-- Copy ZNodeHighlight records    
------------------------------    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @HighlightTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
INSERT INTO @HighlightTable     
   (OldID)    
SELECT      
   HighLightID    
FROM    
   ZNodeHighlight     
WHERE    
   HighLightID IN    
   (SELECT     
     PH.HighLightId    
   FROM    
     @ProductIDTable PT    
   INNER JOIN    
     ZNodeProductHighlight PH    
   ON    
     PT.OldProductID = PH.ProductID);          
    
-- Loop through each record and insert new highlight record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @HighlightTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    
     
 INSERT INTO ZNodeHighlight     
	  ([ImageFile]    
      ,[ImageAltTag]    
      ,[Name]    
      ,[Description]    
      ,[DisplayPopup]    
      ,[Hyperlink]    
      ,[HyperlinkNewWinInd]    
      ,[HighlightTypeID]    
      ,[ActiveInd]    
      ,[DisplayOrder]    
      ,[ShortDescription]    
      ,[LocaleId])    
 SELECT     
       [ImageFile]    
      ,[ImageAltTag]    
      ,[Name]    
      ,[Description]    
      ,[DisplayPopup]    
      ,[Hyperlink]    
      ,[HyperlinkNewWinInd]    
      ,[HighlightTypeID]    
      ,[ActiveInd]    
      ,[DisplayOrder]    
      ,[ShortDescription]    
      ,COALESCE(@LocaleID, [LocaleId])         
 FROM ZNodeHighlight    
 WHERE HighlightID = (SELECT OldID FROM @HighlightTable WHERE KeyField=@recIndex);    
     
 UPDATE @HighlightTable    
 SET NewID = @@IDENTITY    
 WHERE KeyField = @recIndex;     
     
 SET @recIndex = @recIndex + 1;    
     
END    
    
-- Insert new highlight association for new products.    
INSERT INTO ZNodeProductHighlight     
    ([ProductID]    
    ,[HighlightID]    
    ,[DisplayOrder])    
      SELECT    
     A.NewProductID    
    ,C.NewID    
    ,B.DisplayOrder     
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductHighlight B ON A.OldProductID = B.ProductID    
   INNER JOIN @HighlightTable C ON C.OldID = B.HighlightID;    
    
    
---------------------------    
-- Copy ZNodeAddOns records    
---------------------------    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @AddOnTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
INSERT INTO @AddOnTable     
   (OldID)    
SELECT      
   AddOnId    
FROM    
   ZNodeAddOn     
WHERE    
   AddOnId IN    
   (SELECT     
     B.AddOnId    
   FROM    
     @ProductIDTable A    
   INNER JOIN    
     ZNodeProductAddOn B    
   ON    
     A.OldProductID = B.ProductID);    
         
-- Loop through each record and insert new highlight record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @AddOnTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    
     
 -- Duplicate the ZNodeAddOn records.    
 INSERT INTO ZNodeAddOn     
   ([ProductID]    
      ,[Title]    
      ,[Name]    
      ,[Description]    
      ,[DisplayOrder]    
      ,[DisplayType]    
      ,[OptionalInd]    
      ,[AllowBackOrder]    
      ,[InStockMsg]    
      ,[OutOfStockMsg]    
      ,[BackOrderMsg]    
      ,[PromptMsg]    
      ,[TrackInventoryInd]    
      ,[LocaleId])    
 SELECT     
       [ProductID]    
      ,[Title]    
      ,[Name]    
      ,[Description]    
      ,[DisplayOrder]    
      ,[DisplayType]    
      ,[OptionalInd]    
      ,[AllowBackOrder]    
      ,[InStockMsg]    
      ,[OutOfStockMsg]    
      ,[BackOrderMsg]    
      ,[PromptMsg]    
      ,[TrackInventoryInd]    
      ,COALESCE(@LocaleID, [LocaleId])    
 FROM ZNodeAddOn    
 WHERE AddOnID = (SELECT OldID FROM @AddOnTable WHERE KeyField=@recIndex);    
     
 SET @recID = @@IDENTITY;    
        
 UPDATE @AddOnTable    
 SET NewID = @recID    
 WHERE KeyField = @recIndex;      
     
 -- Duplicate the ZNodeAddOnValue records based on new AddOn record.    
 INSERT INTO ZNodeAddOnValue    
    ([AddOnID]    
    ,[Name]    
    ,[Description]    
    ,[SKU]    
    ,[DefaultInd]    
    ,[DisplayOrder]    
    ,[ImageFile]    
    ,[ImageAltTag]    
    ,[RetailPrice]    
    ,[SalePrice]    
    ,[WholesalePrice]    
    ,[RecurringBillingInd]    
    ,[RecurringBillingInstallmentInd]    
    ,[RecurringBillingPeriod]    
    ,[RecurringBillingFrequency]    
    ,[RecurringBillingTotalCycles]    
    ,[RecurringBillingInitialAmount]    
    ,[Weight]    
    ,[Length]    
    ,[Height]    
    ,[Width]    
    ,[ShippingRuleTypeID]    
    ,[FreeShippingInd]    
    ,[WebServiceDownloadDte]    
    ,[UpdateDte]    
    ,[SupplierID]    
    ,[TaxClassID])    
 SELECT      
     @recID    
    ,[Name]    
    ,[Description]    
    ,[SKU]    
    ,[DefaultInd]    
    ,[DisplayOrder]    
    ,[ImageFile]    
    ,[ImageAltTag]    
    ,[RetailPrice]    
    ,[SalePrice]    
    ,[WholesalePrice]    
    ,[RecurringBillingInd]    
    ,[RecurringBillingInstallmentInd]    
    ,[RecurringBillingPeriod]    
    ,[RecurringBillingFrequency]    
    ,[RecurringBillingTotalCycles]    
    ,[RecurringBillingInitialAmount]    
    ,[Weight]    
    ,[Length]    
    ,[Height]    
    ,[Width]    
    ,[ShippingRuleTypeID]    
    ,[FreeShippingInd]    
    ,[WebServiceDownloadDte]    
    ,[UpdateDte]    
    ,[SupplierID]    
    ,[TaxClassID]    
 FROM ZNodeAddOnValue     
 WHERE AddOnID = (SELECT OldID FROM @AddOnTable WHERE KeyField=@recIndex);    
     
 SET @recIndex = @recIndex + 1;    
       
END    
    
-- Insert new AddOn association for new products.    
INSERT INTO ZNodeProductAddOn    
    ([ProductID]    
    ,AddOnID)    
      SELECT    
     A.NewProductID    
    ,C.NewID    
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductAddOn B ON A.OldProductID = B.ProductID    
   INNER JOIN @AddOnTable C ON C.OldID = B.AddOnID;       
            
              
-----------------------------------    
-- Copy ZNodeProductProfile records    
-----------------------------------    
-- Insert new Profile association for new products.    
INSERT INTO ZNodeProductProfile    
    ([ProductID]    
    ,ProfileID    
    ,IncludeInd)    
      SELECT    
    DISTINCT    
     A.NewProductID    
    ,B.ProfileID    
    ,B.IncludeInd    
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductProfile B ON A.OldProductID = B.ProductID    
         
         
--------------------------------    
-- Copy ZNodeProductTier records    
--------------------------------    
-- Insert new Tier association for new products.    
INSERT INTO ZNodeProductTier    
    ([ProductID]    
    ,[ProfileID]    
    ,[TierStart]    
    ,[TierEnd]    
    ,[Price])    
      SELECT    
      DISTINCT    
     A.NewProductID    
    ,B.ProfileID    
    ,B.TierStart    
    ,B.TierEnd    
    ,B.Price    
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductTier B ON A.OldProductID = B.ProductID    
       
       
-------------------------------------    
-- Copy ZNodeProductCrossSell records    
-------------------------------------    
-- Insert new CrossSell association for new products.    
INSERT INTO ZNodeProductCrossSell    
    ([ProductID]    
    ,[RelatedProductId]    
    ,[DisplayOrder])    
      SELECT    
      DISTINCT    
     A.NewProductID    
    ,C.NewProductID    
    ,B.DisplayOrder    
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductCrossSell B ON A.OldProductID = B.ProductID    
   INNER JOIN @ProductIDTable C ON B.RelatedProductId = C.OldProductID  
   WHERE A.NewProductID IS NOT NULL  
   AND C.NewProductID IS NOT NULL;  
       
       
---------------------------------    
-- Copy ZNodeProductImage records    
---------------------------------    
-- Insert new Image association for new products.    
INSERT INTO ZNodeProductImage    
    ([ProductID]    
    ,[Name]    
    ,[ImageFile]    
    ,[ImageAltTag]    
    ,[AlternateThumbnailImageFile]    
    ,[ActiveInd]    
    ,[ShowOnCategoryPage]    
    ,[ProductImageTypeID]    
    ,[DisplayOrder])    
      SELECT    
      DISTINCT    
     A.NewProductID        
    ,B.[Name]    
    ,B.[ImageFile]    
    ,B.[ImageAltTag]    
    ,B.[AlternateThumbnailImageFile]    
    ,B.[ActiveInd]    
    ,B.[ShowOnCategoryPage]    
    ,B.[ProductImageTypeID]    
    ,B.[DisplayOrder]    
   FROM @ProductIDTable A    
   INNER JOIN ZNodeProductImage B ON A.OldProductID = B.ProductID    
   WHERE A.NewProductID IS NOT NULL;     
               
--------------------------------------------------------------------    
-- Copy ZNodeProductAttribute / ZNodeSKU / ZNodeSKUAttribute records    
--------------------------------------------------------------------    
    
-- Duplicate ZNodeAttributeType    
    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @AttributeTypeTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
    
INSERT INTO @AttributeTypeTable    
  (OldID)    
 SELECT    
  DISTINCT A.AttributeTypeId    
 FROM ZNodeAttributeType A    
 INNER JOIN ZNodeProductTypeAttribute B ON A.AttributeTypeId = B.AttributeTypeId    
 INNER JOIN ZNodeProductType C ON C.ProductTypeId = B.ProductTypeId    
 INNER JOIN ZNodeProduct D ON D.ProductTypeID = C.ProductTypeId    
 INNER JOIN @ProductIDTable E ON D.ProductID = E.OldProductID;    
    
-- Loop through each record and insert new record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @AttributeTypeTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    
    
 INSERT INTO ZNodeAttributeType     
   ([Name]    
   ,[Description]    
   ,[DisplayOrder]    
   ,[IsPrivate]    
   ,[LocaleId])    
 SELECT      
    [Name]    
      ,[Description]    
      ,[DisplayOrder]    
      ,[IsPrivate]    
      ,COALESCE(@LocaleID, [LocaleId])     
    FROM ZNodeAttributeType A    
    INNER JOIN @AttributeTypeTable B ON A.AttributeTypeId = B.OldID    
    WHERE B.KeyField = @recIndex;    
    
 SET @recID = @@IDENTITY;    
    
 UPDATE @AttributeTypeTable     
 SET NewID = @recID    
 WHERE KeyField = @recIndex;    
     
 SET @recIndex = @recIndex + 1;    
     
END    
    
-- Duplicate ZNodeProductAttribute    
    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @ProductAttributeTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
    
INSERT INTO @ProductAttributeTable    
  (OldID)    
 SELECT    
  DISTINCT A.AttributeId    
 FROM ZNodeProductAttribute A    
 INNER JOIN ZNodeSKUAttribute B ON A.AttributeId = B.AttributeId    
 INNER JOIN ZNodeSKU C ON C.SKUID = B.SKUID    
 INNER JOIN @ProductIDTable E ON C.ProductID = E.OldProductID;    
    
-- Loop through each record and insert new record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @ProductAttributeTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    
    
 INSERT INTO ZNodeProductAttribute     
	  ([AttributeTypeId]    
      ,[Name]    
      ,[ExternalId]    
      ,[DisplayOrder]    
      ,[IsActive]    
      ,[OldAttributeId])    
 SELECT      
       C.NewID    
      ,[Name]    
      ,[ExternalId]    
      ,[DisplayOrder]    
      ,[IsActive]    
      ,[OldAttributeId]    
    FROM ZNodeProductAttribute A        
    INNER JOIN @ProductAttributeTable B ON A.AttributeId = B.OldID    
    INNER JOIN @AttributeTypeTable C ON A.AttributeTypeId = C.OldID    
    WHERE B.KeyField = @recIndex AND C.NewID IS NOT NULL;  
    
    
 SET @recID = @@IDENTITY;    
    
 UPDATE @ProductAttributeTable     
 SET NewID = @recID    
 WHERE KeyField = @recIndex;    
     
 SET @recIndex = @recIndex + 1;    
     
END    
    
        
-- Duplicate ZNodeSKU    
    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @SkuTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
    
INSERT INTO @SkuTable    
  (OldID)    
 SELECT    
  DISTINCT A.SKUID    
 FROM ZNodeSKU A     
 INNER JOIN @ProductIDTable E ON A.ProductID = E.OldProductID;    
    
-- Loop through each record and insert new record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @SkuTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    
    
 INSERT INTO ZNodeSKU     
   ([ProductID]    
      ,[SKU]    
      ,[SupplierID]    
      ,[Note]    
      ,[WeightAdditional]    
      ,[SKUPicturePath]    
      ,[ImageAltTag]    
      ,[DisplayOrder]    
      ,[RetailPriceOverride]    
      ,[SalePriceOverride]    
      ,[WholesalePriceOverride]    
      ,[RecurringBillingPeriod]    
      ,[RecurringBillingFrequency]    
      ,[RecurringBillingTotalCycles]    
      ,[RecurringBillingInitialAmount]    
      ,[ActiveInd]    
      ,[Custom1]    
      ,[Custom2]    
      ,[Custom3]    
      ,[WebServiceDownloadDte]    
      ,[UpdateDte])    
 SELECT      
    C.NewProductID    
      ,[SKU]    
      ,[SupplierID]    
      ,[Note]    
      ,[WeightAdditional]    
      ,[SKUPicturePath]    
      ,[ImageAltTag]    
      ,[DisplayOrder]    
      ,[RetailPriceOverride]    
      ,[SalePriceOverride]    
      ,[WholesalePriceOverride]    
      ,[RecurringBillingPeriod]    
      ,[RecurringBillingFrequency]    
      ,[RecurringBillingTotalCycles]    
      ,[RecurringBillingInitialAmount]    
      ,[ActiveInd]    
      ,[Custom1]    
      ,[Custom2]    
      ,[Custom3]    
      ,[WebServiceDownloadDte]    
      ,[UpdateDte]    
    FROM ZNodeSKU A        
    INNER JOIN @SkuTable B ON A.SKUID = B.OldID    
    INNER JOIN @ProductIDTable C ON A.ProductID = C.OldProductID    
    WHERE B.KeyField = @recIndex          
    AND C.NewProductID IS NOT NULL;  
    
 SET @recID = @@IDENTITY;    
    
 UPDATE @SkuTable     
 SET NewID = @recID    
 WHERE KeyField = @recIndex;    
     
 SET @recIndex = @recIndex + 1;    
     
END            
    
-- Duplicate ZNodeSKUAttribute    
    
INSERT INTO ZNodeSKUAttribute     
 ([SKUID]    
 ,[AttributeId])    
SELECT    
 DISTINCT    
  B.NewID    
 ,C.NewID    
FROM ZNodeSKUAttribute A    
INNER JOIN @SkuTable B ON A.SKUID = B.OldID    
INNER JOIN @ProductAttributeTable C ON A.AttributeId = C.OldID               
WHERE C.NEWID IS NOT NULL AND B.NewID IS NOT NULL;    
    
-- Duplicate ZNodeProductType    
    
/* Table variable to record existing ids linked to new id's */    
DECLARE  @ProductTypeTable Table(    
   [KeyField] int IDENTITY(1,1),     
   [NewID] int,     
   [OldID] int);    
    
    
INSERT INTO @ProductTypeTable    
  (OldID)    
 SELECT    
  DISTINCT A.ProductTypeId    
 FROM ZNodeProductType A    
 INNER JOIN ZNodeProduct C ON C.ProductTypeID = A.ProductTypeID    
 INNER JOIN @ProductIDTable E ON C.ProductID = E.OldProductID;    
    
-- Loop through each record and insert new record and update generate id.    
SET @recCount = (SELECT MAX(KeyField) FROM @ProductTypeTable);    
SET @recIndex = 1    
    
WHILE (@recIndex <= @recCount)    
BEGIN    

IF (EXISTS(SELECT [Name] FROM ZNodeProductType 
			WHERE [Name] IN (
					SELECT      
						[Name] + ISNULL(' - ' + (CASE WHEN ISNULL(A.IsGiftCard, 0) != 1 THEN @LocaleCode ELSE NULL END), '')
					FROM ZNodeProductType A        
					INNER JOIN @ProductTypeTable B ON A.ProductTypeId = B.OldID
					WHERE B.KeyField = @recIndex)))
BEGIN

	SET @recID = (SELECT TOP 1 ProductTypeID FROM ZNodeProductType 
					WHERE [Name] IN (SELECT [Name] + ISNULL(' - ' + (CASE WHEN ISNULL(A.IsGiftCard, 0) != 1 THEN @LocaleCode ELSE NULL END), '')
										FROM ZNodeProductType A        
										INNER JOIN @ProductTypeTable B ON A.ProductTypeId = B.OldID 
										WHERE B.KeyField = @recIndex));    
END
ELSE
BEGIN							    
 INSERT INTO ZNodeProductType    
      ([Name]     
      ,[Description]    
      ,[DisplayOrder])    
 SELECT      
       [Name] + ISNULL(' - ' + @LocaleCode, '')
      ,[Description]    
      ,[DisplayOrder]    
    FROM ZNodeProductType A        
    INNER JOIN @ProductTypeTable B ON A.ProductTypeId = B.OldID    
    WHERE B.KeyField = @recIndex;    
    
 SET @recID = @@IDENTITY;    
END    
 UPDATE @ProductTypeTable     
 SET NewID = @recID    
 WHERE KeyField = @recIndex;    
     
 SET @recIndex = @recIndex + 1;    
     
END    
    
-- Duplicate ZNodeProductTypeAttribute    
INSERT INTO ZNodeProductTypeAttribute     
 (ProductTypeId    
 ,[AttributeTypeId])    
SELECT    
  B.NewID    
 ,C.NewID    
FROM ZNodeProductTypeAttribute A    
INNER JOIN @ProductTypeTable B ON A.ProductTypeId = B.OldID    
INNER JOIN @AttributeTypeTable C ON A.AttributeTypeId = C.OldID              
WHERE C.NEWID IS NOT NULL AND B.NewID IS NOT NULL;    
     
-- Update ZNodeProduct table with new ProductTypeId for new Products    
    
UPDATE ZNodeProduct    
SET ProductTypeID = B.NewID    
FROM @ProductTypeTable B    
INNER JOIN ZNodeProduct C ON C.ProductTypeID = B.OldID    
INNER JOIN @ProductIDTable D ON C.ProductID = D.NewProductID    
WHERE ProductID = C.ProductID AND B.NewID IS NOT NULL;    
    
END  -- end tag for BEGIN (root)    
               


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_Find]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeProduct table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_Find
(

	@SearchUsingOR bit   = null ,

	@ProductID int   = null ,

	@Name nvarchar (MAX)  = null ,

	@ShortDescription nvarchar (MAX)  = null ,

	@Description nvarchar (MAX)  = null ,

	@FeaturesDesc nvarchar (MAX)  = null ,

	@ProductNum nvarchar (100)  = null ,

	@ProductTypeID int   = null ,

	@RetailPrice money   = null ,

	@SalePrice money   = null ,

	@WholesalePrice money   = null ,

	@ImageFile nvarchar (MAX)  = null ,

	@ImageAltTag nvarchar (MAX)  = null ,

	@Weight decimal (18, 2)  = null ,

	@Length decimal (18, 2)  = null ,

	@Width decimal (18, 2)  = null ,

	@Height decimal (18, 2)  = null ,

	@BeginActiveDate datetime   = null ,

	@EndActiveDate datetime   = null ,

	@DisplayOrder int   = null ,

	@ActiveInd bit   = null ,

	@CallForPricing bit   = null ,

	@HomepageSpecial bit   = null ,

	@CategorySpecial bit   = null ,

	@InventoryDisplay tinyint   = null ,

	@Keywords nvarchar (MAX)  = null ,

	@ManufacturerID int   = null ,

	@AdditionalInfoLink nvarchar (MAX)  = null ,

	@AdditionalInfoLinkLabel nvarchar (MAX)  = null ,

	@ShippingRuleTypeID int   = null ,

	@ShippingRate money   = null ,

	@SEOTitle nvarchar (MAX)  = null ,

	@SEOKeywords nvarchar (MAX)  = null ,

	@SEODescription nvarchar (MAX)  = null ,

	@Custom1 nvarchar (MAX)  = null ,

	@Custom2 nvarchar (MAX)  = null ,

	@Custom3 nvarchar (MAX)  = null ,

	@ShipEachItemSeparately bit   = null ,

	@AllowBackOrder bit   = null ,

	@BackOrderMsg nvarchar (MAX)  = null ,

	@DropShipInd bit   = null ,

	@DropShipEmailID nvarchar (MAX)  = null ,

	@Specifications nvarchar (MAX)  = null ,

	@AdditionalInformation nvarchar (MAX)  = null ,

	@InStockMsg nvarchar (MAX)  = null ,

	@OutOfStockMsg nvarchar (MAX)  = null ,

	@TrackInventoryInd bit   = null ,

	@DownloadLink nvarchar (MAX)  = null ,

	@FreeShippingInd bit   = null ,

	@NewProductInd bit   = null ,

	@SEOURL nvarchar (100)  = null ,

	@MaxQty int   = null ,

	@ShipSeparately bit   = null ,

	@FeaturedInd bit   = null ,

	@WebServiceDownloadDte datetime   = null ,

	@UpdateDte datetime   = null ,

	@SupplierID int   = null ,

	@RecurringBillingInd bit   = null ,

	@RecurringBillingInstallmentInd bit   = null ,

	@RecurringBillingPeriod nvarchar (50)  = null ,

	@RecurringBillingFrequency nvarchar (MAX)  = null ,

	@RecurringBillingTotalCycles int   = null ,

	@RecurringBillingInitialAmount money   = null ,

	@TaxClassID int   = null ,

	@MinQty int   = null ,

	@ReviewStateID int   = null ,

	@AffiliateUrl varchar (512)  = null ,

	@IsShippable bit   = null ,

	@AccountID int   = null ,

	@PortalID int   = null ,

	@Franchisable bit   = null ,

	@ExpirationPeriod int   = null ,

	@ExpirationFrequency int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ProductID]
	, [Name]
	, [ShortDescription]
	, [Description]
	, [FeaturesDesc]
	, [ProductNum]
	, [ProductTypeID]
	, [RetailPrice]
	, [SalePrice]
	, [WholesalePrice]
	, [ImageFile]
	, [ImageAltTag]
	, [Weight]
	, [Length]
	, [Width]
	, [Height]
	, [BeginActiveDate]
	, [EndActiveDate]
	, [DisplayOrder]
	, [ActiveInd]
	, [CallForPricing]
	, [HomepageSpecial]
	, [CategorySpecial]
	, [InventoryDisplay]
	, [Keywords]
	, [ManufacturerID]
	, [AdditionalInfoLink]
	, [AdditionalInfoLinkLabel]
	, [ShippingRuleTypeID]
	, [ShippingRate]
	, [SEOTitle]
	, [SEOKeywords]
	, [SEODescription]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [ShipEachItemSeparately]
	, [AllowBackOrder]
	, [BackOrderMsg]
	, [DropShipInd]
	, [DropShipEmailID]
	, [Specifications]
	, [AdditionalInformation]
	, [InStockMsg]
	, [OutOfStockMsg]
	, [TrackInventoryInd]
	, [DownloadLink]
	, [FreeShippingInd]
	, [NewProductInd]
	, [SEOURL]
	, [MaxQty]
	, [ShipSeparately]
	, [FeaturedInd]
	, [WebServiceDownloadDte]
	, [UpdateDte]
	, [SupplierID]
	, [RecurringBillingInd]
	, [RecurringBillingInstallmentInd]
	, [RecurringBillingPeriod]
	, [RecurringBillingFrequency]
	, [RecurringBillingTotalCycles]
	, [RecurringBillingInitialAmount]
	, [TaxClassID]
	, [MinQty]
	, [ReviewStateID]
	, [AffiliateUrl]
	, [IsShippable]
	, [AccountID]
	, [PortalID]
	, [Franchisable]
	, [ExpirationPeriod]
	, [ExpirationFrequency]
    FROM
	[dbo].[ZNodeProduct]
    WHERE 
	 ([ProductID] = @ProductID OR @ProductID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([ShortDescription] = @ShortDescription OR @ShortDescription IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([FeaturesDesc] = @FeaturesDesc OR @FeaturesDesc IS NULL)
	AND ([ProductNum] = @ProductNum OR @ProductNum IS NULL)
	AND ([ProductTypeID] = @ProductTypeID OR @ProductTypeID IS NULL)
	AND ([RetailPrice] = @RetailPrice OR @RetailPrice IS NULL)
	AND ([SalePrice] = @SalePrice OR @SalePrice IS NULL)
	AND ([WholesalePrice] = @WholesalePrice OR @WholesalePrice IS NULL)
	AND ([ImageFile] = @ImageFile OR @ImageFile IS NULL)
	AND ([ImageAltTag] = @ImageAltTag OR @ImageAltTag IS NULL)
	AND ([Weight] = @Weight OR @Weight IS NULL)
	AND ([Length] = @Length OR @Length IS NULL)
	AND ([Width] = @Width OR @Width IS NULL)
	AND ([Height] = @Height OR @Height IS NULL)
	AND ([BeginActiveDate] = @BeginActiveDate OR @BeginActiveDate IS NULL)
	AND ([EndActiveDate] = @EndActiveDate OR @EndActiveDate IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([ActiveInd] = @ActiveInd OR @ActiveInd IS NULL)
	AND ([CallForPricing] = @CallForPricing OR @CallForPricing IS NULL)
	AND ([HomepageSpecial] = @HomepageSpecial OR @HomepageSpecial IS NULL)
	AND ([CategorySpecial] = @CategorySpecial OR @CategorySpecial IS NULL)
	AND ([InventoryDisplay] = @InventoryDisplay OR @InventoryDisplay IS NULL)
	AND ([Keywords] = @Keywords OR @Keywords IS NULL)
	AND ([ManufacturerID] = @ManufacturerID OR @ManufacturerID IS NULL)
	AND ([AdditionalInfoLink] = @AdditionalInfoLink OR @AdditionalInfoLink IS NULL)
	AND ([AdditionalInfoLinkLabel] = @AdditionalInfoLinkLabel OR @AdditionalInfoLinkLabel IS NULL)
	AND ([ShippingRuleTypeID] = @ShippingRuleTypeID OR @ShippingRuleTypeID IS NULL)
	AND ([ShippingRate] = @ShippingRate OR @ShippingRate IS NULL)
	AND ([SEOTitle] = @SEOTitle OR @SEOTitle IS NULL)
	AND ([SEOKeywords] = @SEOKeywords OR @SEOKeywords IS NULL)
	AND ([SEODescription] = @SEODescription OR @SEODescription IS NULL)
	AND ([Custom1] = @Custom1 OR @Custom1 IS NULL)
	AND ([Custom2] = @Custom2 OR @Custom2 IS NULL)
	AND ([Custom3] = @Custom3 OR @Custom3 IS NULL)
	AND ([ShipEachItemSeparately] = @ShipEachItemSeparately OR @ShipEachItemSeparately IS NULL)
	AND ([AllowBackOrder] = @AllowBackOrder OR @AllowBackOrder IS NULL)
	AND ([BackOrderMsg] = @BackOrderMsg OR @BackOrderMsg IS NULL)
	AND ([DropShipInd] = @DropShipInd OR @DropShipInd IS NULL)
	AND ([DropShipEmailID] = @DropShipEmailID OR @DropShipEmailID IS NULL)
	AND ([Specifications] = @Specifications OR @Specifications IS NULL)
	AND ([AdditionalInformation] = @AdditionalInformation OR @AdditionalInformation IS NULL)
	AND ([InStockMsg] = @InStockMsg OR @InStockMsg IS NULL)
	AND ([OutOfStockMsg] = @OutOfStockMsg OR @OutOfStockMsg IS NULL)
	AND ([TrackInventoryInd] = @TrackInventoryInd OR @TrackInventoryInd IS NULL)
	AND ([DownloadLink] = @DownloadLink OR @DownloadLink IS NULL)
	AND ([FreeShippingInd] = @FreeShippingInd OR @FreeShippingInd IS NULL)
	AND ([NewProductInd] = @NewProductInd OR @NewProductInd IS NULL)
	AND ([SEOURL] = @SEOURL OR @SEOURL IS NULL)
	AND ([MaxQty] = @MaxQty OR @MaxQty IS NULL)
	AND ([ShipSeparately] = @ShipSeparately OR @ShipSeparately IS NULL)
	AND ([FeaturedInd] = @FeaturedInd OR @FeaturedInd IS NULL)
	AND ([WebServiceDownloadDte] = @WebServiceDownloadDte OR @WebServiceDownloadDte IS NULL)
	AND ([UpdateDte] = @UpdateDte OR @UpdateDte IS NULL)
	AND ([SupplierID] = @SupplierID OR @SupplierID IS NULL)
	AND ([RecurringBillingInd] = @RecurringBillingInd OR @RecurringBillingInd IS NULL)
	AND ([RecurringBillingInstallmentInd] = @RecurringBillingInstallmentInd OR @RecurringBillingInstallmentInd IS NULL)
	AND ([RecurringBillingPeriod] = @RecurringBillingPeriod OR @RecurringBillingPeriod IS NULL)
	AND ([RecurringBillingFrequency] = @RecurringBillingFrequency OR @RecurringBillingFrequency IS NULL)
	AND ([RecurringBillingTotalCycles] = @RecurringBillingTotalCycles OR @RecurringBillingTotalCycles IS NULL)
	AND ([RecurringBillingInitialAmount] = @RecurringBillingInitialAmount OR @RecurringBillingInitialAmount IS NULL)
	AND ([TaxClassID] = @TaxClassID OR @TaxClassID IS NULL)
	AND ([MinQty] = @MinQty OR @MinQty IS NULL)
	AND ([ReviewStateID] = @ReviewStateID OR @ReviewStateID IS NULL)
	AND ([AffiliateUrl] = @AffiliateUrl OR @AffiliateUrl IS NULL)
	AND ([IsShippable] = @IsShippable OR @IsShippable IS NULL)
	AND ([AccountID] = @AccountID OR @AccountID IS NULL)
	AND ([PortalID] = @PortalID OR @PortalID IS NULL)
	AND ([Franchisable] = @Franchisable OR @Franchisable IS NULL)
	AND ([ExpirationPeriod] = @ExpirationPeriod OR @ExpirationPeriod IS NULL)
	AND ([ExpirationFrequency] = @ExpirationFrequency OR @ExpirationFrequency IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ProductID]
	, [Name]
	, [ShortDescription]
	, [Description]
	, [FeaturesDesc]
	, [ProductNum]
	, [ProductTypeID]
	, [RetailPrice]
	, [SalePrice]
	, [WholesalePrice]
	, [ImageFile]
	, [ImageAltTag]
	, [Weight]
	, [Length]
	, [Width]
	, [Height]
	, [BeginActiveDate]
	, [EndActiveDate]
	, [DisplayOrder]
	, [ActiveInd]
	, [CallForPricing]
	, [HomepageSpecial]
	, [CategorySpecial]
	, [InventoryDisplay]
	, [Keywords]
	, [ManufacturerID]
	, [AdditionalInfoLink]
	, [AdditionalInfoLinkLabel]
	, [ShippingRuleTypeID]
	, [ShippingRate]
	, [SEOTitle]
	, [SEOKeywords]
	, [SEODescription]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [ShipEachItemSeparately]
	, [AllowBackOrder]
	, [BackOrderMsg]
	, [DropShipInd]
	, [DropShipEmailID]
	, [Specifications]
	, [AdditionalInformation]
	, [InStockMsg]
	, [OutOfStockMsg]
	, [TrackInventoryInd]
	, [DownloadLink]
	, [FreeShippingInd]
	, [NewProductInd]
	, [SEOURL]
	, [MaxQty]
	, [ShipSeparately]
	, [FeaturedInd]
	, [WebServiceDownloadDte]
	, [UpdateDte]
	, [SupplierID]
	, [RecurringBillingInd]
	, [RecurringBillingInstallmentInd]
	, [RecurringBillingPeriod]
	, [RecurringBillingFrequency]
	, [RecurringBillingTotalCycles]
	, [RecurringBillingInitialAmount]
	, [TaxClassID]
	, [MinQty]
	, [ReviewStateID]
	, [AffiliateUrl]
	, [IsShippable]
	, [AccountID]
	, [PortalID]
	, [Franchisable]
	, [ExpirationPeriod]
	, [ExpirationFrequency]
    FROM
	[dbo].[ZNodeProduct]
    WHERE 
	 ([ProductID] = @ProductID AND @ProductID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([ShortDescription] = @ShortDescription AND @ShortDescription is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([FeaturesDesc] = @FeaturesDesc AND @FeaturesDesc is not null)
	OR ([ProductNum] = @ProductNum AND @ProductNum is not null)
	OR ([ProductTypeID] = @ProductTypeID AND @ProductTypeID is not null)
	OR ([RetailPrice] = @RetailPrice AND @RetailPrice is not null)
	OR ([SalePrice] = @SalePrice AND @SalePrice is not null)
	OR ([WholesalePrice] = @WholesalePrice AND @WholesalePrice is not null)
	OR ([ImageFile] = @ImageFile AND @ImageFile is not null)
	OR ([ImageAltTag] = @ImageAltTag AND @ImageAltTag is not null)
	OR ([Weight] = @Weight AND @Weight is not null)
	OR ([Length] = @Length AND @Length is not null)
	OR ([Width] = @Width AND @Width is not null)
	OR ([Height] = @Height AND @Height is not null)
	OR ([BeginActiveDate] = @BeginActiveDate AND @BeginActiveDate is not null)
	OR ([EndActiveDate] = @EndActiveDate AND @EndActiveDate is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([ActiveInd] = @ActiveInd AND @ActiveInd is not null)
	OR ([CallForPricing] = @CallForPricing AND @CallForPricing is not null)
	OR ([HomepageSpecial] = @HomepageSpecial AND @HomepageSpecial is not null)
	OR ([CategorySpecial] = @CategorySpecial AND @CategorySpecial is not null)
	OR ([InventoryDisplay] = @InventoryDisplay AND @InventoryDisplay is not null)
	OR ([Keywords] = @Keywords AND @Keywords is not null)
	OR ([ManufacturerID] = @ManufacturerID AND @ManufacturerID is not null)
	OR ([AdditionalInfoLink] = @AdditionalInfoLink AND @AdditionalInfoLink is not null)
	OR ([AdditionalInfoLinkLabel] = @AdditionalInfoLinkLabel AND @AdditionalInfoLinkLabel is not null)
	OR ([ShippingRuleTypeID] = @ShippingRuleTypeID AND @ShippingRuleTypeID is not null)
	OR ([ShippingRate] = @ShippingRate AND @ShippingRate is not null)
	OR ([SEOTitle] = @SEOTitle AND @SEOTitle is not null)
	OR ([SEOKeywords] = @SEOKeywords AND @SEOKeywords is not null)
	OR ([SEODescription] = @SEODescription AND @SEODescription is not null)
	OR ([Custom1] = @Custom1 AND @Custom1 is not null)
	OR ([Custom2] = @Custom2 AND @Custom2 is not null)
	OR ([Custom3] = @Custom3 AND @Custom3 is not null)
	OR ([ShipEachItemSeparately] = @ShipEachItemSeparately AND @ShipEachItemSeparately is not null)
	OR ([AllowBackOrder] = @AllowBackOrder AND @AllowBackOrder is not null)
	OR ([BackOrderMsg] = @BackOrderMsg AND @BackOrderMsg is not null)
	OR ([DropShipInd] = @DropShipInd AND @DropShipInd is not null)
	OR ([DropShipEmailID] = @DropShipEmailID AND @DropShipEmailID is not null)
	OR ([Specifications] = @Specifications AND @Specifications is not null)
	OR ([AdditionalInformation] = @AdditionalInformation AND @AdditionalInformation is not null)
	OR ([InStockMsg] = @InStockMsg AND @InStockMsg is not null)
	OR ([OutOfStockMsg] = @OutOfStockMsg AND @OutOfStockMsg is not null)
	OR ([TrackInventoryInd] = @TrackInventoryInd AND @TrackInventoryInd is not null)
	OR ([DownloadLink] = @DownloadLink AND @DownloadLink is not null)
	OR ([FreeShippingInd] = @FreeShippingInd AND @FreeShippingInd is not null)
	OR ([NewProductInd] = @NewProductInd AND @NewProductInd is not null)
	OR ([SEOURL] = @SEOURL AND @SEOURL is not null)
	OR ([MaxQty] = @MaxQty AND @MaxQty is not null)
	OR ([ShipSeparately] = @ShipSeparately AND @ShipSeparately is not null)
	OR ([FeaturedInd] = @FeaturedInd AND @FeaturedInd is not null)
	OR ([WebServiceDownloadDte] = @WebServiceDownloadDte AND @WebServiceDownloadDte is not null)
	OR ([UpdateDte] = @UpdateDte AND @UpdateDte is not null)
	OR ([SupplierID] = @SupplierID AND @SupplierID is not null)
	OR ([RecurringBillingInd] = @RecurringBillingInd AND @RecurringBillingInd is not null)
	OR ([RecurringBillingInstallmentInd] = @RecurringBillingInstallmentInd AND @RecurringBillingInstallmentInd is not null)
	OR ([RecurringBillingPeriod] = @RecurringBillingPeriod AND @RecurringBillingPeriod is not null)
	OR ([RecurringBillingFrequency] = @RecurringBillingFrequency AND @RecurringBillingFrequency is not null)
	OR ([RecurringBillingTotalCycles] = @RecurringBillingTotalCycles AND @RecurringBillingTotalCycles is not null)
	OR ([RecurringBillingInitialAmount] = @RecurringBillingInitialAmount AND @RecurringBillingInitialAmount is not null)
	OR ([TaxClassID] = @TaxClassID AND @TaxClassID is not null)
	OR ([MinQty] = @MinQty AND @MinQty is not null)
	OR ([ReviewStateID] = @ReviewStateID AND @ReviewStateID is not null)
	OR ([AffiliateUrl] = @AffiliateUrl AND @AffiliateUrl is not null)
	OR ([IsShippable] = @IsShippable AND @IsShippable is not null)
	OR ([AccountID] = @AccountID AND @AccountID is not null)
	OR ([PortalID] = @PortalID AND @PortalID is not null)
	OR ([Franchisable] = @Franchisable AND @Franchisable is not null)
	OR ([ExpirationPeriod] = @ExpirationPeriod AND @ExpirationPeriod is not null)
	OR ([ExpirationFrequency] = @ExpirationFrequency AND @ExpirationFrequency is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_WS_GetMostPopularProducts]'
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[ZNode_WS_GetMostPopularProducts]
@PortalID INT=NULL, 
@LocaleId INT=NULL
AS
BEGIN
    SET NOCOUNT ON;
    SELECT DISTINCT TOP 20
		P.ProductID,
		P.Name,
		P.ShortDescription,
		P.[Description],
		P.FeaturesDesc,
		P.ProductNum,
		P.RetailPrice,
		P.SalePrice,
		(SELECT TOP 1 QuantityOnHand FROM ZNodeSKUInventory I, ZNodeSku S WHERE I.Sku=S.Sku AND S.ProductID=P.ProductID ) AS QuantityOnHand,
		(SELECT TOP 1 Sku FROM ZNodeSku S WHERE ProductID=P.ProductID) AS Sku,		
		P.DisplayOrder,
		P.ImageFile,  
		Portal.SplashCategoryID,
		ISNULL(Portal.SplashImageFile,'') AS SplashImageFile,
		(SELECT Name FROM ZNodeCategory WHERE CategoryId=Portal.SplashCategoryID) AS SplashCategoryName,   				
		TotalReviews=(SELECT ISNULL(COUNT(1),0) FROM ZNodeReview R WHERE R.ProductID=P.ProductID), 
        ReviewRating=(SELECT ISNULL(AVG(Rating),0) FROM ZNodeReview R WHERE R.ProductID=P.ProductID  AND R.Status='A') 
	 FROM ZNodeProduct AS P
                   INNER JOIN
                   ZNodeProductCategory AS PC
                   ON P.ProductID = PC.ProductID
                   INNER JOIN
                   ZNodeCategoryNode AS ZCN
                   ON ZCN.CategoryID = PC.CategoryID
                   INNER JOIN
                   ZNodePortalCatalog AS ZPOC
                   ON ZPOC.CatalogID = ZCN.CatalogID
                   INNER JOIN
                   ZNodeCategory AS C
                   ON C.CategoryID = PC.CategoryID                   
                   INNER JOIN ZNodePortal Portal 
                   ON Portal.PortalID=@PortalID
            WHERE  (ZPOC.PortalID = @PortalID
                    AND ZPOC.LocaleID = @LocaleId)
                    AND P.ActiveInd=1 
					AND ZCN.ActiveInd=1
			ORDER BY P.DisplayOrder DESC    
END  
 




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNODE_WS_GetMobileCategories]'
GO

ALTER PROCEDURE [dbo].[ZNODE_WS_GetMobileCategories]
@PortalID INT=0, 
@LocaleID INT=0, 
@CategoryID INT=0
AS
BEGIN
    SET NOCOUNT ON;
    IF (@CategoryID <> 0)
        BEGIN
            SELECT Category.[CategoryId],
                   Category.[Name],
                   Category.[Title],
                   CAST (Category.[ShortDescription] AS NVARCHAR (MAX)) AS 'ShortDescription',
                   CAST (Category.[Description] AS NVARCHAR (MAX)) AS 'Description',
                   CAST (Category.[AlternateDescription] AS NVARCHAR (MAX)) AS 'AlternateDescription',
                   Category.[DisplayOrder],
                   Category.[ImageFile],
                   Category.[ImageAltTag],
                   Category.[VisibleInd],
                   Category.[SubCategoryGridVisibleInd],
                   Category.[SeoTitle],
                   Category.[SeoKeywords],
                   Category.[SeoDescription],
                   Category.[SeoURL],
                   Category.[Custom1],
                   Category.[Custom2],
                   Category.[Custom3],
                   Portal.SplashCategoryID,
				   Portal.SplashImageFile  
            FROM   ZNodeCategory AS Category 
            INNER JOIN ZNodePortal Portal ON Portal.PortalID=@PortalID
            WHERE  Category.CategoryID IN (SELECT ZCN.CategoryID
                                           FROM   ZNodeCategoryNode AS ZCN, ZNodePortalCatalog AS ZPOC
                                           WHERE  ZCN.CatalogID = ZPOC.CatalogID
                                                  AND ZPOC.LocaleID = @LocaleID
                                                  AND ZPOC.PortalID = @PortalID
                                                  AND Category.VisibleInd = 1
                                                  AND ZCN.ActiveInd=1
                                                  AND ZCN.ParentCategoryNodeID IN (SELECT CategoryNodeID
                                                                                   FROM   ZNodeCategoryNode
                                                                                   WHERE  CategoryID IN (@CategoryID)))
           
            
            SELECT P.ProductID,
                   P.Name,
                   P.ShortDescription,
                   P.[Description],
                   P.FeaturesDesc,
                   P.ProductNum,
                   P.RetailPrice,
                   P.SalePrice,
                   (SELECT TOP 1 QuantityOnHand FROM ZNodeSKUInventory I, ZNodeSku S WHERE I.Sku=S.Sku  AND S.ProductID=P.ProductID) AS QuantityOnHand,
				   (SELECT TOP 1 Sku FROM ZNodeSku S WHERE ProductID=P.ProductID) AS Sku,				   
                   P.ImageFile,  
                   Portal.SplashCategoryID,
				   ISNULL(Portal.SplashImageFile,'') AS SplashImageFile,
				 (SELECT Name FROM ZNodeCategory WHERE CategoryId=Portal.SplashCategoryID) AS SplashCategoryName,  
                   P.DisplayOrder,              
                   TotalReviews=(SELECT ISNULL(COUNT(1),0) FROM ZNodeReview R WHERE R.ProductID=P.ProductID), 
				   ReviewRating=(SELECT ISNULL(AVG(Rating),0) FROM ZNodeReview R WHERE R.ProductID=P.ProductID  AND R.Status='A')
            FROM   ZNodeProduct AS P
                   INNER JOIN
                   ZNodeProductCategory AS PC
                   ON P.ProductID = PC.ProductID
                   INNER JOIN
                   ZNodeCategoryNode AS ZCN
                   ON ZCN.CategoryID = PC.CategoryID
                   INNER JOIN
                   ZNodePortalCatalog AS ZPOC
                   ON ZPOC.CatalogID = ZCN.CatalogID
                   INNER JOIN
                   ZNodeCategory AS C
                   ON C.CategoryID = PC.CategoryID                   
                   INNER JOIN ZNodePortal Portal
                   ON Portal.PortalID=@PortalID
            WHERE  (ZPOC.PortalID = @PortalID
                    AND (C.CategoryID = @CategoryID)
                   AND ZPOC.LocaleID = @LocaleID)          
                   AND P.ActiveInd=1 
				   AND ZCN.ActiveInd=1
        END
    ELSE
        BEGIN
            SELECT C.[CategoryId],
                   C.[Name],
                   [Title],
                   CAST ([ShortDescription] AS NVARCHAR (MAX)) AS 'ShortDescription',
                   CAST ([Description] AS NVARCHAR (MAX)) AS 'Description',
                   CAST ([AlternateDescription] AS NVARCHAR (MAX)) AS 'AlternateDescription',
                   C.[DisplayOrder],
                   [ImageFile],
                   [ImageAltTag],
                   [VisibleInd],
                   [SubCategoryGridVisibleInd],
                   [SeoTitle],
                   [SeoKeywords],
                   [SeoDescription],
                   [SeoURL],
                   [Custom1],
                   [Custom2],
                   [Custom3],
                   Portal.SplashCategoryID,
				   Portal.SplashImageFile  
            FROM   ZNodeCategory AS C            
                   INNER JOIN
                   ZNodeCategoryNode AS ZCN
                   ON ZCN.CategoryID = C.CategoryID
                   INNER JOIN
                   ZNodePortalCatalog AS ZPOC
                   ON ZPOC.CatalogID = ZCN.CatalogID
                   INNER JOIN ZNodePortal Portal 
                   ON Portal.PortalID=@PortalID
            WHERE  C.CategoryID IN (SELECT CategoryID
                                    FROM   ZNodeCategoryNode
                                    WHERE  ParentCategoryNodeID IS NULL)
                   AND ZPOC.PortalID = @PortalID
                   AND C.VisibleInd = 1
                   AND ZCN.ActiveInd=1
                   AND ZPOC.LocaleID = @LocaleID
                      
            SELECT  P.ProductID,
                   P.Name,
                   P.ShortDescription,
                   P.[Description],
                   P.FeaturesDesc,
                   P.ProductNum,
                   P.RetailPrice,
                   P.SalePrice,
                   (SELECT TOP 1 QuantityOnHand FROM ZNodeSKUInventory I, ZNodeSku S WHERE I.Sku=S.Sku  AND S.ProductID=P.ProductID) AS QuantityOnHand,
				   (SELECT TOP 1 Sku FROM ZNodeSku S WHERE ProductID=P.ProductID) AS Sku,				   
                   P.ImageFile, 
                   Portal.SplashCategoryID,
				   ISNULL(Portal.SplashImageFile,'') AS SplashImageFile,
				  (SELECT Name FROM ZNodeCategory WHERE CategoryId=Portal.SplashCategoryID) AS SplashCategoryName,
                   P.DisplayOrder,
                   TotalReviews=(SELECT ISNULL(COUNT(1),0) FROM ZNodeReview R WHERE R.ProductID=P.ProductID), 
				   ReviewRating=(SELECT ISNULL(AVG(Rating),0) FROM ZNodeReview R WHERE R.ProductID=P.ProductID  AND R.Status='A') 
            FROM   ZNodeProduct AS P
                   INNER JOIN
                   ZNodeProductCategory AS PC
                   ON P.ProductID = PC.ProductID
                   INNER JOIN
                   ZNodeCategoryNode AS ZCN
                   ON ZCN.CategoryID = PC.CategoryID
                   INNER JOIN
                   ZNodePortalCatalog AS ZPOC
                   ON ZPOC.CatalogID = ZCN.CatalogID
                   INNER JOIN
                   ZNodeCategory AS C
                   ON C.CategoryID = PC.CategoryID  
                   INNER JOIN ZNodePortal Portal
                   ON Portal.PortalID=@PortalID                  
            WHERE  C.CategoryID IN (SELECT CategoryID
                                   FROM   ZNodeCategoryNode
                                    WHERE  ParentCategoryNodeID IS NULL)
                   AND ZPOC.PortalID = @PortalID
                   AND ZPOC.LocaleID = @LocaleID  
                   AND P.ActiveInd=1 
				   AND ZCN.ActiveInd=1         
        END
END  






GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_WS_GetFeaturedProducts]'
GO

ALTER PROCEDURE [dbo].[ZNode_WS_GetFeaturedProducts]
(
@FeatureInd BIT,
@PortalID int = null,
@LocaleId int = null)
AS            
 BEGIN            
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
	SELECT DISTINCT TOP 20
		P.ProductID,
		P.[Name],       
		P.[ShortDescription],        
		P.[Description],        
		[FeaturesDesc],        
		P.[ProductNum],        
		[RetailPrice],        
		[SalePrice],        
		(SELECT TOP 1 QuantityOnHand FROM ZNodeSKUInventory I, ZNodeSku S WHERE I.Sku=S.Sku  AND S.ProductID=P.ProductID) AS QuantityOnHand,
		(SELECT TOP 1 Sku FROM ZNodeSku S WHERE ProductID=P.ProductID) AS Sku,		
		P.[ImageFile],        
		Portal.SplashCategoryID,
		ISNULL(Portal.SplashImageFile,'') AS SplashImageFile,
		(SELECT Name FROM ZNodeCategory WHERE CategoryId=Portal.SplashCategoryID) AS SplashCategoryName,
		P.[DisplayOrder],        		
		TotalReviews=(SELECT ISNULL(COUNT(1),0) FROM ZNodeReview R WHERE R.ProductID=P.ProductID), 
        ReviewRating=(SELECT ISNULL(AVG(Rating),0) FROM ZNodeReview R WHERE R.ProductID=P.ProductID AND R.Status='A')         
	 FROM ZNodeProduct AS P
                   INNER JOIN
                   ZNodeProductCategory AS PC
                   ON P.ProductID = PC.ProductID                   
                   INNER JOIN
                   ZNodeCategoryNode AS ZCN
                   ON ZCN.CategoryID = PC.CategoryID
                   INNER JOIN
                   ZNodePortalCatalog AS ZPOC
                   ON ZPOC.CatalogID = ZCN.CatalogID
                   INNER JOIN
                   ZNodeCategory AS C
                   ON C.CategoryID = PC.CategoryID
                   INNER JOIN ZNodePortal Portal
                   ON Portal.PortalID=@PortalID
            WHERE  (ZPOC.PortalID = @PortalID
                    AND ZPOC.LocaleID = @LocaleId)
					AND P.FeaturedInd=@FeatureInd 
					AND P.ActiveInd=1 
					AND ZCN.ActiveInd=1					
					
 END





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetDashboardItemsByPortal]'
GO


ALTER PROCEDURE [dbo].[ZNode_GetDashboardItemsByPortal]

AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @TotalProducts AS INT;
    DECLARE @TotalCategories AS INT;
    DECLARE @TotalInventory AS INT;
    DECLARE @TotalOutOfStock AS INT;
    DECLARE @YTDRevenue AS SMALLMONEY;
    DECLARE @MTDRevenue AS SMALLMONEY;
    DECLARE @TodayRevenue AS SMALLMONEY;
    DECLARE @ShippedToday AS INT;
    DECLARE @ReturnedToday AS INT;
    DECLARE @TotalOrders AS INT;
    DECLARE @TotalOrdersMTD AS INT;
    DECLARE @TotalNewOrders AS INT;
    DECLARE @TotalSubmittedOrders AS INT;
    DECLARE @TotalShippedOrders AS INT;
    DECLARE @TotalPaymentPendingOrders AS INT;
    DECLARE @TotalAccounts AS INT;
    DECLARE @TotalAccountsMTD AS INT;
    DECLARE @TotalPages AS INT;
    DECLARE @TotalShippingOptions AS INT;
    DECLARE @PaymentGateway AS VARCHAR (MAX);
    DECLARE @TotalPendingCases AS INT;
    DECLARE @TotalReviewsToApprove AS INT;
    DECLARE @TotalAffiliatesToApprove AS INT;
    DECLARE @TotalDeclinedTransactions AS INT;
    DECLARE @TotalLoginFailedToday AS INT;
    DECLARE @TotalLowInventoryItems AS INT;    
    DECLARE @MonthFirstDate AS DATETIME;
    SET @PaymentGateway = 'None';
    
    SELECT @TotalProducts = Count(1)
    FROM   ZNodeProduct
    WHERE  ZnodeProduct.ProductId IN (SELECT ProductId
                                      FROM   ZnodeProductCategory
                                      WHERE  CategoryID IN (SELECT CategoryId
                                                            FROM   ZNodeCategoryNode));
    SELECT @TotalCategories = Count(1)
    FROM   ZNodeCategory
    WHERE  CategoryId IN (SELECT CategoryId
                          FROM   ZNodeCategoryNode, ZNodePortalCatalog, ZNodeCatalog
                          WHERE  ZNodeCatalog.CatalogId = ZNodeCategoryNode.CatalogId
                                 AND ZNodePortalCatalog.CatalogId = ZNodeCatalog.CatalogId);
    SELECT @TotalInventory = isnull(sum(ZNodeSKUInventory.QuantityOnHand), 0)
    FROM   ZNodeSKU
           INNER JOIN
           ZNodeSKUInventory
           ON ZNodeSKUInventory.SKU = ZNodeSKU.SKU
           INNER JOIN
           ZNodeProduct
           ON ZNodeSKU.ProductID = ZNodeProduct.ProductID
    WHERE  ZnodeProduct.ProductId IN (SELECT ProductId
                                      FROM   ZnodeProductCategory
                                      WHERE  CategoryID IN (SELECT CategoryId
                                                            FROM   ZNodeCategoryNode));
    SELECT @TotalOutOfStock = COUNT(1) FROM 
	(SELECT ZNodeSKUInventory.SKU
    FROM   ZNodeSKUInventory
           INNER JOIN
           ZNodeSKU
           ON ZNodeSKUInventory.SKU = ZNodeSKU.SKU
           INNER JOIN
           ZNodeProduct
           ON ZNodeSKU.ProductID = ZNodeProduct.ProductID
    WHERE  ZnodeProduct.ProductId IN (SELECT ProductId
                                      FROM   ZnodeProductCategory
                                      WHERE  CategoryID IN (SELECT CategoryId
                                                            FROM   ZNodeCategoryNode))
           AND ZNodeSKUInventory.QuantityOnHand = 0
			GROUP BY ZNodeSKUInventory.SKU) TABLE1
           
           
    SELECT @YTDRevenue = isnull(sum(total), 0)
    FROM   ZNodeOrder
    WHERE  year(OrderDate) = year(GetDate());
    
    -- Get current month first date.
    SET @MonthFirstDate=CAST(MONTH(GETDATE()) AS VARCHAR(10)) + '/01/' + CAST(YEAR(GETDATE()) AS VARCHAR(10));
    
    
    SELECT @MTDRevenue = ISNULL(SUM(total), 0)
    FROM   ZNodeOrder
    WHERE  CAST (CONVERT (VARCHAR (10),OrderDate, 101) AS DATETIME) >= CAST (CONVERT (VARCHAR (10),@MonthFirstDate, 101) AS DATETIME)
           AND CAST (CONVERT (VARCHAR (10),OrderDate, 101) AS DATETIME) <= CAST(CONVERT(VARCHAR(10), GETDATE(),101) AS DateTime);
            
           
    SELECT @TodayRevenue = isnull(sum(total), 0)
    FROM   ZNodeOrder
    WHERE  DATEADD(dd, 0, DATEDIFF(dd, 0, orderdate)) = CONVERT (DATETIME, FLOOR(CONVERT (FLOAT (24), GETDATE())));
    SELECT @ShippedToday = COUNT(1)
    FROM   ZNodeOrder
    WHERE  DATEADD(dd, 0, DATEDIFF(dd, 0, orderdate)) = CONVERT (DATETIME, FLOOR(CONVERT (FLOAT (24), GETDATE())))
           AND OrderStateID = 20;
           
           
    SELECT @ReturnedToday = COUNT(1)
    FROM   ZNodeOrder
    WHERE  DATEADD(dd, 0, DATEDIFF(dd, 0, orderdate)) = CONVERT (DATETIME, FLOOR(CONVERT (FLOAT (24), GETDATE())))
           AND OrderStateID = 30;
           
    SELECT @TotalOrders = COUNT(1)
    FROM   ZNodeOrder;
    
    
    SELECT @TotalOrdersMTD = COUNT(1)
    FROM   ZNodeOrder
    WHERE  CAST (CONVERT (VARCHAR (10),OrderDate, 101) AS DATETIME) >= CAST (CONVERT (VARCHAR (10),@MonthFirstDate, 101) AS DATETIME)
           AND CAST (CONVERT (VARCHAR (10),OrderDate, 101) AS DATETIME) <= CAST(CONVERT(VARCHAR(10), GETDATE(),101) AS DateTime);
           
    SELECT @TotalNewOrders = COUNT(1)
    FROM   ZNodeOrder
    WHERE  OrderDate > GetDate() - 10;
    
    SELECT @TotalPaymentPendingOrders = COUNT(1)
    FROM   ZNodeOrder
    WHERE  OrderStateID = 1;
    
    SELECT @TotalSubmittedOrders = COUNT(1)
    FROM   ZNodeOrder
    WHERE  OrderStateID = 2;
    
    SELECT @TotalShippedOrders = COUNT(1)
    FROM   ZNodeOrder
    WHERE  OrderStateID = 3;
    
    SELECT @TotalAccounts = count(DISTINCT (ZA.AccountId))
    FROM   ZNODEACCOUNT AS ZA, ZNODEACCOUNTPROFILE AS ZAP, ZNODEPROFILE AS ZP
    WHERE  ZA.ACCOUNTID = ZAP.ACCOUNTID
           AND ZAP.PROFILEID = ZP.PROFILEID;
           
    SELECT @TotalAccountsMTD = count(DISTINCT (ZA.AccountId))
    FROM   ZNODEACCOUNT AS ZA, ZNODEACCOUNTPROFILE AS ZAP, ZNODEPROFILE AS ZP
    WHERE  ZA.ACCOUNTID = ZAP.ACCOUNTID
           AND ZAP.PROFILEID = ZP.PROFILEID
           AND CAST (CONVERT (VARCHAR (10),CreateDte, 101) AS DATETIME)>= CAST (CONVERT (VARCHAR (10),@MonthFirstDate, 101) AS DATETIME)
           AND CAST (CONVERT (VARCHAR (10),CreateDte, 101) AS DATETIME) <= CAST(CONVERT(VARCHAR(10), GETDATE(),101) AS DateTime);
           
    SELECT @TotalPages = COUNT(1)
    FROM   ZNodeContentPage;
    
    SELECT @TotalShippingOptions = COUNT(1)
    FROM   ZNodeShipping
           INNER JOIN
           ZNodeProfile
           ON ZNodeShipping.ProfileID = ZNodeProfile.ProfileID;
           
    IF EXISTS (SELECT *
               FROM   ZNodePaymentSetting
                      INNER JOIN
                      ZNodeGateway
                      ON ZNodeGateway.GatewayTypeID = ZNodePaymentSetting.GatewayTypeID
                      INNER JOIN
                      ZNodeProfile
                      ON ZNodePaymentSetting.ProfileID = ZNodeProfile.ProfileID
               WHERE  ZNodePaymentSetting.PaymentTypeID = 0)
        BEGIN
            SELECT @PaymentGateway = isnull(ZNodeGateway.GatewayName, 'None')
            FROM   ZNodePaymentSetting
                   INNER JOIN
                   ZNodeGateway
                   ON ZNodeGateway.GatewayTypeID = ZNodePaymentSetting.GatewayTypeID
                   INNER JOIN
                   ZNodeProfile
                   ON ZNodePaymentSetting.ProfileID = ZNodeProfile.ProfileID
            WHERE  ZNodePaymentSetting.PaymentTypeID = 0;
        END
    SET @TotalPendingCases = (SELECT COUNT(1)
                              FROM   ZNodeCaseRequest
                              WHERE  CaseStatusId = 1);
    SET @TotalReviewsToApprove = (SELECT COUNT(1)
                                  FROM   ZNodeReview
                                  WHERE  Status = 'N');
    SET @TotalAffiliatesToApprove = (SELECT COUNT(1)
                                     FROM   ZNodeAccount
                                     WHERE  ReferralStatus = 'N');
    SET @TotalDeclinedTransactions = (SELECT COUNT(1)
                                      FROM   ZNodeActivityLog
                                      WHERE  ActivityLogTypeId = 5001);
    SET @TotalLoginFailedToday = (SELECT COUNT(1)
                                  FROM   ZNodeActivityLog
                                  WHERE  (ActivityLogTypeId = 1001
                                          OR ActivityLogTypeId = 1107
                                          OR ActivityLogTypeId = 1109)
                                         AND (DateDiff(dd, CreateDte, GetDate()) = 0));
    SELECT @TotalLowInventoryItems = COUNT(1) FROM 
	(SELECT ZNodeSKUInventory.SKU
    FROM   ZNodeSKUInventory
           INNER JOIN
           ZNodeSKU
           ON ZNodeSKUInventory.SKU = ZNodeSKU.SKU
           INNER JOIN
           ZNodeProduct
           ON ZNodeSKU.ProductID = ZNodeProduct.ProductID
    WHERE  ZnodeProduct.ProductId IN (SELECT ProductId
                                      FROM   ZnodeProductCategory
                                      WHERE  CategoryID IN (SELECT CategoryId
                                                            FROM   ZNodeCategoryNode))
		   AND (ZNodeSKUInventory.QuantityOnHand < ZNodeSKUInventory.ReOrderLevel                                                           
           OR ZNodeSKUInventory.QuantityOnHand <= 0)
	GROUP BY ZNodeSKUInventory.SKU) TABLE1                                                                                 
    
    SELECT @TotalProducts AS 'TotalProducts',
           @TotalCategories AS 'TotalCategories',
           @TotalInventory AS 'TotalInventory',
           @TotalOutOfStock AS 'TotalOutOfStock',
           @YTDRevenue AS 'YTDRevenue',
           @MTDRevenue AS 'MTDRevenue',
           @TodayRevenue AS 'TodayRevenue',
           @ShippedToday AS 'ShippedToday',
           @ReturnedToday AS 'ReturnedToday',
           @TotalOrders AS 'TotalOrders',
           @TotalOrdersMTD AS 'TotalOrdersMTD',
           @TotalNewOrders AS 'TotalNewOrders',
           @TotalPaymentPendingOrders AS 'TotalPaymentPendingOrders',
           @TotalSubmittedOrders AS 'TotalSubmittedOrders',
           @TotalShippedOrders AS 'TotalShippedOrders',
           @TotalAccounts AS 'TotalAccounts',
           @TotalAccountsMTD AS 'TotalAccountsMTD',
           @TotalPages AS 'TotalPages',
           @TotalShippingOptions AS 'TotalShippingOptions',
           @PaymentGateway AS 'PaymentGateway',
           @TotalPendingCases AS 'TotalPendingServiceRequests',
           @TotalReviewsToApprove AS 'TotalReviewstoApprove',
           @TotalAffiliatesToApprove AS 'TotalAffiliatesToApprove',
           @TotalDeclinedTransactions AS 'TotalDeclinedTransactions',
           @TotalLoginFailedToday AS 'TotalLoginFailedToday',
           @TotalLowInventoryItems AS 'TotalLowInventoryItems';
           
    (SELECT  TOP 7 Data1,
                    COUNT(Data1)
     FROM     ZNodeActivityLog
     WHERE    (ActivityLogTypeId = 9500
               OR ActivityLogTypeId = 9501
               OR ActivityLogTypeId = 9502)
     GROUP BY Data1)
    ORDER BY  COUNT(Data1) DESC;
END  
 


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductsByTagIDs]'
GO



ALTER PROCEDURE [dbo].[ZNode_GetProductsByTagIDs](@TagIds varchar(Max),@CategoryId int)  
AS   
BEGIN  

	DECLARE	@ZNodeTagSplit TABLE(TagId INT); 

	INSERT INTO 
		@ZNodeTagSplit (TagId)
	(SELECT
			[Value]	
	FROM 
			SC_SPLITTER(@TagIds,','))
 
  CREATE TABLE #TEMP (ProductID INT) 
	
  -- Select Products that matches tags.
  INSERT INTO #TEMP
  SELECT ProductID 
	FROM ZNodeTagProductSKU 
	WHERE TagId IN (SELECT TagID FROM @ZNodeTagSplit) AND ProductID IS NOT NULL   
  UNION ALL
  SELECT ProductID 
	FROM ZNODESKU 
	WHERE SKUID IN(SELECT SkuID FROM ZNodeTagProductSKU 
					WHERE TagId IN (SELECT TagID FROM @ZNodeTagSplit) AND SkuID IS NOT NULL )

 -- Select Products that matches all tags.
  SELECT ProductId,
             ( Row_number() OVER (ORDER BY Displayorder, ProductID ASC) ) 'Displayorder',
             ( Row_number() OVER (ORDER BY RetailPrice ASC) )  'RetailPrice',
             ( Row_number() OVER (ORDER BY RetailPrice DESC) )  'RetailPriceDESC'
      FROM   ProductsView ZNodeProduct
  WHERE ProductID IN     
	 (SELECT ProductID from ZNodeProductCategory WHERE ProductID IN   
			(SELECT ProductID FROM #TEMP GROUP BY ProductID HAVING Count(ProductID) = (SELECT Count(TagID) FROM @ZNodeTagSplit))
	 AND ActiveInd = 1 AND CategoryID IN (SELECT CategoryID FROM ZNodeCategoryNode 
												WHERE CategoryID = @CategoryID
												OR ParentCategoryNodeID IN (SELECT CategoryNodeID FROM ZNodeCategoryNode 
												WHERE CategoryID = @CategoryID)))
 AND ZNodeProduct.ActiveInd = 1 ORDER BY ZNodeProduct.DisplayOrder ASC
 
 
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetTagsByCategoryIdTagIds]'
GO


ALTER PROCEDURE [dbo].[ZNode_GetTagsByCategoryIdTagIds]  
(  
 @CategoryID INT,  
 @TagIds varchar(max)  
)    

AS    
  
BEGIN    


CREATE TABLE #ZnodeProducts (ProductID INT)

INSERT INTO #ZnodeProducts
SELECT 
		    A.ProductID AS 'ProductId'	
FROM 
			ZNodeProductCategory A
INNER JOIN
			ProductsView B
ON
			B.ProductID = A.ProductID
WHERE 
			A.CategoryId  IN (SELECT CategoryID FROM ZNodeCategoryNode 
												WHERE CategoryID = @CategoryID
												OR ParentCategoryNodeID IN (SELECT CategoryNodeID FROM ZNodeCategoryNode 
												WHERE CategoryID = @CategoryID))
AND
			A.ActiveInd = 1
AND
			B.ActiveInd = 1

CREATE TABLE #ZnodeTagProduct (TagId INT, ProductId INT)
					
INSERT INTO 
		#ZnodeTagProduct
SELECT 
		TPS.TagId, 
		TPS.ProductId
FROM 
		TagProductView TPS
INNER JOIN
		#ZNodeProducts P
ON
		P.ProductId = TPS.ProductId

CREATE TABLE #ZNodeTagSplit (TagId INT)

INSERT INTO 
		#ZNodeTagSplit
SELECT
		[Value]	AS 'TagId'
FROM 
		SC_SPLITTER(@TagIds,',') 
 
CREATE TABLE #ZNodeTag (TagID INT
						,TagGroupId INT 
						,TagName  NVARCHAR(MAX)
						,IconPath VARCHAR(512) 
						,TagCount INT
						,DisplayOrder INT)
 
INSERT INTO #ZNodeTag (TagID, TagGroupId, TagName, IconPath, TagCount, DisplayOrder)
	SELECT 
		TagID
		,TagGroupId
		,TagName
		,IconPath		
		,ISNULL((SELECT COUNT(TP.TagId)
					FROM
						#ZnodeTagProduct TP
					WHERE
						TP.TagId = ZNodeTag.TagID
					AND
						ProductId IN 
									(SELECT 
										ProductID
				 					FROM 
										#ZnodeTagProduct TP 
									WHERE
										TP.TagId IN 
											(SELECT
												TagId 
											FROM 
												#ZNodeTagSplit)
									GROUP BY 
										ProductId
									HAVING COUNT(ProductId) = (SELECT
																COUNT(TagID) 
															   FROM 
																#ZNodeTagSplit))
					AND
						TP.TagId
					NOT IN
						(SELECT TagId FROM #ZNodeTagSplit)
					GROUP BY
						TP.TagID), 0)
				,TagDisplayOrder
    FROM 
		ZNodeTag		
	WHERE
		ZNodeTag.TagID 
					IN 
					(SELECT 
							TagID
					FROM
						#ZnodeTagProduct	
					WHERE
						ProductId 
					IN
						(SELECT 
							ProductId
						FROM
							#ZnodeTagProduct 
						WHERE 
							TagId
						IN
							(SELECT 
								TagId
							FROM
								#ZNodeTagSplit)))
	AND
		ZNodeTag.TagGroupID
	NOT IN
		(SELECT TagGroupID FROM ZNodeTag INNER JOIN #ZnodeTagSplit TS ON TS.TagId = ZNodeTag.TagID)

	
	SELECT
		ZnodeTagGroup.TagGroupId
		,TagGroupLabel
		,ControlTypeId
		,0 AS 'TagGroupProductCount'
		,ZnodeTagGroup.DisplayOrder
		INTO #ZNodeTagGroup
	FROM
		ZnodeTagGroup
		INNER JOIN ZNodeTagGroupCategory TGC ON ZNodeTagGroup.TagGroupID = TGC.TagGroupID AND TGC.CategoryID  IN (SELECT CategoryID FROM ZNodeCategoryNode 
												WHERE CategoryID = @CategoryID
												OR ParentCategoryNodeID IN (SELECT CategoryNodeID FROM ZNodeCategoryNode 
												WHERE CategoryID = @CategoryID))
	WHERE
		ZnodeTagGroup.TagGroupID IN 
			(SELECT 
				TagGroupID 
			FROM
				ZNodeTag
			WHERE 
				TagID 
			IN
				(SELECT 
					TagID 
				FROM 
					#ZnodeTag)
			AND
				TagGroupID 
			NOT IN
				(SELECT 
					TagGroupID 
				FROM
					ZNodeTag
				INNER JOIN	
					#ZNodeTagSplit TS
				ON
					TS.TagId = ZNodeTag.TagID))
	
	
		SELECT
			TagGroupId
			,TagGroupLabel
			,ControlTypeId
			,TagGroupProductCount
		FROM
			#ZNodeTagGroup ORDER BY DisplayOrder;
					
		SELECT
				TagId,
				TagGroupID,
				TagName,
				IconPath,
				TagCount AS 'TagProductCount'
		FROM
				#ZNodeTag
		WHERE
				EXISTS
				(SELECT 1 FROM #ZNodeTagGroup WHERE TagGroupId = #ZNodeTag.TagGroupId)	
				AND TagCount > 0
		ORDER BY DisplayOrder;
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetTagsByCategory]'
GO


ALTER PROCEDURE [dbo].[ZNode_GetTagsByCategory]  
(  
 @CategoryID INT 
)    

AS
BEGIN    

	SET NOCOUNT ON;
	
	CREATE TABLE #ZNodeTagGroup
		(
			TagGroupID INT,   
            TagGroupLabel NVARCHAR(MAX),   
            ControlTypeID INT,  
            DisplayOrder INT 
		)

	
	     
	INSERT INTO #ZNodeTagGroup
	SELECT DISTINCT  TG.TagGroupID,  
                        TagGroupLabel,  
                        ControlTypeID,  
                        DisplayOrder                  
                FROM    ZNodeTagGroup TG  
                        INNER JOIN ZNodeTagGroupCategory TGC ON TG.TagGroupID = TGC.TagGroupID  
                WHERE   TGC.CategoryID IN ( SELECT CategoryID FROM ZNodeCategoryNode 
												WHERE CategoryID = @CategoryID
												OR ParentCategoryNodeID IN (SELECT CategoryNodeID FROM ZNodeCategoryNode 
												WHERE CategoryID = @CategoryID))
                ORDER BY DisplayOrder ;  
  
				CREATE TABLE #CategoryProduct (ProductID INT)
  
                 INSERT INTO #CategoryProduct  
					SELECT  
					DISTINCT A.ProductID                    
                  FROM      ZNodeProductCategory A  
                  INNER JOIN ProductsView B ON A.ProductID = B.ProductID  
                  WHERE     A.ActiveInd = 1  
                            AND A.CategoryId IN (SELECT CategoryID FROM ZNodeCategoryNode 
												WHERE CategoryID = @CategoryID
												OR ParentCategoryNodeID IN (SELECT CategoryNodeID FROM ZNodeCategoryNode 
												WHERE CategoryID = @CategoryID))
                
  
                SELECT  TagGroupID,  
                        TagGroupLabel,  
                        ControlTypeID,  
                        DisplayOrder  
                FROM    #ZNodeTagGroup  
  
                SELECT  Tag.TagID,  
                        Tag.TagName,  
                        Tag.TagGroupID,  
                        Tag.IconPath,  
                        Tag.TagDisplayOrder,  
                        COUNT(TP.ProductID) AS TagProductCount  
                FROM    ZNodeTag Tag  
                        INNER JOIN #ZNodeTagGroup TG ON Tag.TagGroupID = TG.TagGroupID  
                        INNER JOIN TagProductView TP ON Tag.TagID = TP.TagID
				WHERE	TP.ProductId IN (SELECT ProductID FROM #CategoryProduct) 
                GROUP BY	Tag.TagID,  
							Tag.TagName,  
							Tag.TagGroupID,  
							Tag.IconPath,  
							Tag.TagDisplayOrder
				ORDER By Tag.TagDisplayOrder
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchCustomer]'
GO


ALTER PROCEDURE [dbo].[ZNode_SearchCustomer]    
(            
-- ADD THE PARAMETERS FOR THE STORED PROCEDURE HERE            
@FirstName VARCHAR(MAX) = '',            
@LastName VARCHAR(MAX) = '',            
@CompanyName VARCHAR(MAX) = '',            
@ProfileID int = 0,            
@StartDate VARCHAR(MAX),            
@EndDate VARCHAR(MAX),            
@LoginName VARCHAR(MAX) = '',            
@AccountID VARCHAR(MAX) = '',            
@ExternalAccountNum VARCHAR(MAX) = '',            
@PhoneNum VARCHAR(MAX) = '',            
@EmailID VARCHAR(MAX) = '',            
@ReferralStatus VARCHAR(MAX) = '',            
@PortalID INT=0,
@RoleName VARCHAR(MAX) = ''           
)            
AS            
BEGIN            
 -- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM            
 -- INTERFERING WITH SELECT STATEMENTS.            
	SET NOCOUNT ON;      
	
	DECLARE	@VARStateDate DATETIME;
	DECLARE @VAREndDate DATETIME;
		
	IF(LEN(@StartDate) = 0 OR @StartDate IS NULL)        
		BEGIN		   
			SET @VARStateDate =(SELECT Min([CreateDte])FROM ZNODEACCOUNT)        
		END
	ELSE
		BEGIN        
			SET @VARStateDate = CAST(@StartDate AS DATETIME);
		END	
		
	IF(LEN(@EndDate) = 0 OR @EndDate IS NULL)        
		BEGIN        
			SET @VAREndDate = (SELECT Max([CreateDte])FROM ZNODEACCOUNT)        
		END
	ELSE
		BEGIN        
			SET @VAREndDate =CAST(@EndDate  AS DATETIME);
		END		
		
	SET @VAREndDate = (SELECT DateAdd(dd, 1, @VAREndDate));  
	
	
	CREATE TABLE #TempAccount (AccountID INT);

	IF (@PROFILEID > 0 AND @PORTALID = 0) 
	BEGIN
			INSERT INTO #TempAccount (AccountID)
			(SELECT ACCOUNTID 			
			FROM ZNODEACCOUNTPROFILE 
			WHERE ProfileID = @PROFILEID)
	END
	ELSE IF (@PORTALID > 0 AND @PROFILEID = 0) 
	BEGIN
			INSERT INTO #TempAccount (AccountID)
			(SELECT ACCOUNTID 
			FROM ZNODEACCOUNTPROFILE ZAP
			INNER JOIN ZNODEPORTALPROFILE ZPP ON ZAP.ProfileID = ZPP .ProfileID
			WHERE ZPP.PORTALID = @PORTALID)
	END
    ELSE IF (@PORTALID > 0 AND @PROFILEID > 0) 
    BEGIN
			INSERT INTO #TempAccount (AccountID)
			(SELECT ACCOUNTID
			FROM ZNODEACCOUNTPROFILE ZAP
			INNER JOIN ZNODEPORTALPROFILE ZPP ON ZAP.ProfileID = ZPP .ProfileID
			WHERE  ZPP.PORTALID = @PORTALID AND ZPP.PROFILEID = @PROFILEID)
	END
	ELSE
	BEGIN
			INSERT INTO #TempAccount (AccountID)
			(SELECT ACCOUNTID
				FROM ZNODEACCOUNT)
	END
      
       
	SELECT ZA.[ACCOUNTID]
			,[USERID] 
			,A.FirstName AS [BILLINGFIRSTNAME]          
			,A.LastName AS [BILLINGLASTNAME]                  
			,A.PhoneNumber AS [BILLINGPHONENUMBER]          
			,ZA.Email AS [BILLINGEMAILID]          			
			FROM ZNodeAccount ZA LEFT OUTER JOIN ZNodeAddress A 
			ON ZA.AccountID=A.AccountID AND A.IsDefaultBilling=1
	WHERE  EXISTS(SELECT AccountID FROM #TempAccount TA WHERE TA.AccountID = ZA.AccountID)		
		AND ((A.FirstName LIKE '%' + @FirstName + '%') OR LEN(@FirstName) = 0) 
		AND	((A.LastName LIKE '%' + @LastName + '%') OR LEN(@LastName) = 0) 
		AND ((ZA.CompanyName LIKE '%' + @CompanyName + '%') OR (A.CompanyName LIKE '%' + @CompanyName + '%') 
			OR (A.CompanyName LIKE '%' + @CompanyName + '%') OR LEN(@CompanyName) = 0) 
		AND (ZA.CreateDte BETWEEN @VARStateDate AND @VAREndDate)
		AND (ZA.UserID IN (SELECT UserID FROM Aspnet_Users WHERE UserName LIKE @LOGINNAME + '%') OR LEN(@LOGINNAME)= 0)
		AND (ZA.UserID IN (SELECT AUS.UserID FROM Aspnet_Users AUS
								LEFT JOIN aspnet_UsersInRoles AUR ON AUS.UserId = AUR.UserId
								LEFT JOIN aspnet_Roles AR ON AUR.RoleId = AR.RoleId
							WHERE AR.RoleName = @RoleName) OR LEN(@RoleName) = 0)
		AND	(ZA.AccountID LIKE @AccountID + '%' OR LEN(@ACCOUNTID) = 0)
		AND	(ZA.ExternalAccountNo LIKE @ExternalAccountNum + '%' OR LEN(@ExternalAccountNum) = 0)
		AND (A.PhoneNumber LIKE @PHONENUM + '%' OR LEN(@PHONENUM) = 0
				OR A.PhoneNumber LIKE @PHONENUM + '%')
		AND	(ZA.Email LIKE @EmailID + '%' OR LEN(@EmailID) = 0	OR ZA.Email  LIKE @EmailID + '%')
		AND	(ZA.ReferralStatus LIKE @ReferralStatus + '%' OR LEN(@ReferralStatus) = 0)
	
	ORDER BY [ACCOUNTID] DESC; 
     
END  
  



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeContentPage_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeContentPage table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeContentPage_Get_List

AS


				
				SELECT
					[ContentPageID],
					[Name],
					[PortalID],
					[Theme],
					[MasterPage],
					[CSS],
					[Title],
					[SEOTitle],
					[SEOMetaKeywords],
					[SEOMetaDescription],
					[AllowDelete],
					[TemplateName],
					[ActiveInd],
					[AnalyticsCode],
					[Custom1],
					[Custom2],
					[Custom3],
					[SEOURL],
					[LocaleId],
					[MetaTagAdditional]
				FROM
					[dbo].[ZNodeContentPage]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeContentPage_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeContentPage table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeContentPage_Insert
(

	@ContentPageID int    OUTPUT,

	@Name nvarchar (50)  ,

	@PortalID int   ,

	@Theme nvarchar (MAX)  ,

	@MasterPage nvarchar (MAX)  ,

	@CSS nvarchar (MAX)  ,

	@Title nvarchar (MAX)  ,

	@SEOTitle nvarchar (MAX)  ,

	@SEOMetaKeywords nvarchar (MAX)  ,

	@SEOMetaDescription nvarchar (MAX)  ,

	@AllowDelete bit   ,

	@TemplateName nvarchar (MAX)  ,

	@ActiveInd bit   ,

	@AnalyticsCode varchar (MAX)  ,

	@Custom1 varchar (MAX)  ,

	@Custom2 varchar (MAX)  ,

	@Custom3 varchar (MAX)  ,

	@SEOURL nvarchar (100)  ,

	@LocaleId int   ,

	@MetaTagAdditional nvarchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[ZNodeContentPage]
					(
					[Name]
					,[PortalID]
					,[Theme]
					,[MasterPage]
					,[CSS]
					,[Title]
					,[SEOTitle]
					,[SEOMetaKeywords]
					,[SEOMetaDescription]
					,[AllowDelete]
					,[TemplateName]
					,[ActiveInd]
					,[AnalyticsCode]
					,[Custom1]
					,[Custom2]
					,[Custom3]
					,[SEOURL]
					,[LocaleId]
					,[MetaTagAdditional]
					)
				VALUES
					(
					@Name
					,@PortalID
					,@Theme
					,@MasterPage
					,@CSS
					,@Title
					,@SEOTitle
					,@SEOMetaKeywords
					,@SEOMetaDescription
					,@AllowDelete
					,@TemplateName
					,@ActiveInd
					,@AnalyticsCode
					,@Custom1
					,@Custom2
					,@Custom3
					,@SEOURL
					,@LocaleId
					,@MetaTagAdditional
					)
				
				-- Get the identity value
				SET @ContentPageID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeContentPage_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeContentPage table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeContentPage_Update
(

	@ContentPageID int   ,

	@Name nvarchar (50)  ,

	@PortalID int   ,

	@Theme nvarchar (MAX)  ,

	@MasterPage nvarchar (MAX)  ,

	@CSS nvarchar (MAX)  ,

	@Title nvarchar (MAX)  ,

	@SEOTitle nvarchar (MAX)  ,

	@SEOMetaKeywords nvarchar (MAX)  ,

	@SEOMetaDescription nvarchar (MAX)  ,

	@AllowDelete bit   ,

	@TemplateName nvarchar (MAX)  ,

	@ActiveInd bit   ,

	@AnalyticsCode varchar (MAX)  ,

	@Custom1 varchar (MAX)  ,

	@Custom2 varchar (MAX)  ,

	@Custom3 varchar (MAX)  ,

	@SEOURL nvarchar (100)  ,

	@LocaleId int   ,

	@MetaTagAdditional nvarchar (MAX)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeContentPage]
				SET
					[Name] = @Name
					,[PortalID] = @PortalID
					,[Theme] = @Theme
					,[MasterPage] = @MasterPage
					,[CSS] = @CSS
					,[Title] = @Title
					,[SEOTitle] = @SEOTitle
					,[SEOMetaKeywords] = @SEOMetaKeywords
					,[SEOMetaDescription] = @SEOMetaDescription
					,[AllowDelete] = @AllowDelete
					,[TemplateName] = @TemplateName
					,[ActiveInd] = @ActiveInd
					,[AnalyticsCode] = @AnalyticsCode
					,[Custom1] = @Custom1
					,[Custom2] = @Custom2
					,[Custom3] = @Custom3
					,[SEOURL] = @SEOURL
					,[LocaleId] = @LocaleId
					,[MetaTagAdditional] = @MetaTagAdditional
				WHERE
[ContentPageID] = @ContentPageID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeMessageConfig table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_Get_List

AS


				
				SELECT
					[MessageID],
					[Key],
					[PortalID],
					[LocaleID],
					[Description],
					[Value],
					[MessageTypeID],
					[PageSEOName]
				FROM
					[dbo].[ZNodeMessageConfig]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeContentPage_GetByPortalID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeContentPage table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeContentPage_GetByPortalID
(

	@PortalID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ContentPageID],
					[Name],
					[PortalID],
					[Theme],
					[MasterPage],
					[CSS],
					[Title],
					[SEOTitle],
					[SEOMetaKeywords],
					[SEOMetaDescription],
					[AllowDelete],
					[TemplateName],
					[ActiveInd],
					[AnalyticsCode],
					[Custom1],
					[Custom2],
					[Custom3],
					[SEOURL],
					[LocaleId],
					[MetaTagAdditional]
				FROM
					[dbo].[ZNodeContentPage]
				WHERE
					[PortalID] = @PortalID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeContentPage_GetBySEOURL]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeContentPage table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeContentPage_GetBySEOURL
(

	@SEOURL nvarchar (100)  
)
AS


				SELECT
					[ContentPageID],
					[Name],
					[PortalID],
					[Theme],
					[MasterPage],
					[CSS],
					[Title],
					[SEOTitle],
					[SEOMetaKeywords],
					[SEOMetaDescription],
					[AllowDelete],
					[TemplateName],
					[ActiveInd],
					[AnalyticsCode],
					[Custom1],
					[Custom2],
					[Custom3],
					[SEOURL],
					[LocaleId],
					[MetaTagAdditional]
				FROM
					[dbo].[ZNodeContentPage]
				WHERE
					[SEOURL] = @SEOURL
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeMessageConfig table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_Insert
(

	@MessageID int    OUTPUT,

	@Key nvarchar (100)  ,

	@PortalID int   ,

	@LocaleID int   ,

	@Description nvarchar (MAX)  ,

	@Value nvarchar (MAX)  ,

	@MessageTypeID int   ,

	@PageSEOName nvarchar (250)  
)
AS


				
				INSERT INTO [dbo].[ZNodeMessageConfig]
					(
					[Key]
					,[PortalID]
					,[LocaleID]
					,[Description]
					,[Value]
					,[MessageTypeID]
					,[PageSEOName]
					)
				VALUES
					(
					@Key
					,@PortalID
					,@LocaleID
					,@Description
					,@Value
					,@MessageTypeID
					,@PageSEOName
					)
				
				-- Get the identity value
				SET @MessageID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeContentPage_GetByNamePortalIDLocaleId]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeContentPage table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeContentPage_GetByNamePortalIDLocaleId
(

	@Name nvarchar (50)  ,

	@PortalID int   ,

	@LocaleId int   
)
AS


				SELECT
					[ContentPageID],
					[Name],
					[PortalID],
					[Theme],
					[MasterPage],
					[CSS],
					[Title],
					[SEOTitle],
					[SEOMetaKeywords],
					[SEOMetaDescription],
					[AllowDelete],
					[TemplateName],
					[ActiveInd],
					[AnalyticsCode],
					[Custom1],
					[Custom2],
					[Custom3],
					[SEOURL],
					[LocaleId],
					[MetaTagAdditional]
				FROM
					[dbo].[ZNodeContentPage]
				WHERE
					[Name] = @Name
					AND [PortalID] = @PortalID
					AND [LocaleId] = @LocaleId
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeMessageConfig table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_Update
(

	@MessageID int   ,

	@Key nvarchar (100)  ,

	@PortalID int   ,

	@LocaleID int   ,

	@Description nvarchar (MAX)  ,

	@Value nvarchar (MAX)  ,

	@MessageTypeID int   ,

	@PageSEOName nvarchar (250)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeMessageConfig]
				SET
					[Key] = @Key
					,[PortalID] = @PortalID
					,[LocaleID] = @LocaleID
					,[Description] = @Description
					,[Value] = @Value
					,[MessageTypeID] = @MessageTypeID
					,[PageSEOName] = @PageSEOName
				WHERE
[MessageID] = @MessageID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeContentPage_GetByContentPageID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeContentPage table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeContentPage_GetByContentPageID
(

	@ContentPageID int   
)
AS


				SELECT
					[ContentPageID],
					[Name],
					[PortalID],
					[Theme],
					[MasterPage],
					[CSS],
					[Title],
					[SEOTitle],
					[SEOMetaKeywords],
					[SEOMetaDescription],
					[AllowDelete],
					[TemplateName],
					[ActiveInd],
					[AnalyticsCode],
					[Custom1],
					[Custom2],
					[Custom3],
					[SEOURL],
					[LocaleId],
					[MetaTagAdditional]
				FROM
					[dbo].[ZNodeContentPage]
				WHERE
					[ContentPageID] = @ContentPageID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeContentPage_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeContentPage table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeContentPage_Find
(

	@SearchUsingOR bit   = null ,

	@ContentPageID int   = null ,

	@Name nvarchar (50)  = null ,

	@PortalID int   = null ,

	@Theme nvarchar (MAX)  = null ,

	@MasterPage nvarchar (MAX)  = null ,

	@CSS nvarchar (MAX)  = null ,

	@Title nvarchar (MAX)  = null ,

	@SEOTitle nvarchar (MAX)  = null ,

	@SEOMetaKeywords nvarchar (MAX)  = null ,

	@SEOMetaDescription nvarchar (MAX)  = null ,

	@AllowDelete bit   = null ,

	@TemplateName nvarchar (MAX)  = null ,

	@ActiveInd bit   = null ,

	@AnalyticsCode varchar (MAX)  = null ,

	@Custom1 varchar (MAX)  = null ,

	@Custom2 varchar (MAX)  = null ,

	@Custom3 varchar (MAX)  = null ,

	@SEOURL nvarchar (100)  = null ,

	@LocaleId int   = null ,

	@MetaTagAdditional nvarchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ContentPageID]
	, [Name]
	, [PortalID]
	, [Theme]
	, [MasterPage]
	, [CSS]
	, [Title]
	, [SEOTitle]
	, [SEOMetaKeywords]
	, [SEOMetaDescription]
	, [AllowDelete]
	, [TemplateName]
	, [ActiveInd]
	, [AnalyticsCode]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [SEOURL]
	, [LocaleId]
	, [MetaTagAdditional]
    FROM
	[dbo].[ZNodeContentPage]
    WHERE 
	 ([ContentPageID] = @ContentPageID OR @ContentPageID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([PortalID] = @PortalID OR @PortalID IS NULL)
	AND ([Theme] = @Theme OR @Theme IS NULL)
	AND ([MasterPage] = @MasterPage OR @MasterPage IS NULL)
	AND ([CSS] = @CSS OR @CSS IS NULL)
	AND ([Title] = @Title OR @Title IS NULL)
	AND ([SEOTitle] = @SEOTitle OR @SEOTitle IS NULL)
	AND ([SEOMetaKeywords] = @SEOMetaKeywords OR @SEOMetaKeywords IS NULL)
	AND ([SEOMetaDescription] = @SEOMetaDescription OR @SEOMetaDescription IS NULL)
	AND ([AllowDelete] = @AllowDelete OR @AllowDelete IS NULL)
	AND ([TemplateName] = @TemplateName OR @TemplateName IS NULL)
	AND ([ActiveInd] = @ActiveInd OR @ActiveInd IS NULL)
	AND ([AnalyticsCode] = @AnalyticsCode OR @AnalyticsCode IS NULL)
	AND ([Custom1] = @Custom1 OR @Custom1 IS NULL)
	AND ([Custom2] = @Custom2 OR @Custom2 IS NULL)
	AND ([Custom3] = @Custom3 OR @Custom3 IS NULL)
	AND ([SEOURL] = @SEOURL OR @SEOURL IS NULL)
	AND ([LocaleId] = @LocaleId OR @LocaleId IS NULL)
	AND ([MetaTagAdditional] = @MetaTagAdditional OR @MetaTagAdditional IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ContentPageID]
	, [Name]
	, [PortalID]
	, [Theme]
	, [MasterPage]
	, [CSS]
	, [Title]
	, [SEOTitle]
	, [SEOMetaKeywords]
	, [SEOMetaDescription]
	, [AllowDelete]
	, [TemplateName]
	, [ActiveInd]
	, [AnalyticsCode]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [SEOURL]
	, [LocaleId]
	, [MetaTagAdditional]
    FROM
	[dbo].[ZNodeContentPage]
    WHERE 
	 ([ContentPageID] = @ContentPageID AND @ContentPageID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([PortalID] = @PortalID AND @PortalID is not null)
	OR ([Theme] = @Theme AND @Theme is not null)
	OR ([MasterPage] = @MasterPage AND @MasterPage is not null)
	OR ([CSS] = @CSS AND @CSS is not null)
	OR ([Title] = @Title AND @Title is not null)
	OR ([SEOTitle] = @SEOTitle AND @SEOTitle is not null)
	OR ([SEOMetaKeywords] = @SEOMetaKeywords AND @SEOMetaKeywords is not null)
	OR ([SEOMetaDescription] = @SEOMetaDescription AND @SEOMetaDescription is not null)
	OR ([AllowDelete] = @AllowDelete AND @AllowDelete is not null)
	OR ([TemplateName] = @TemplateName AND @TemplateName is not null)
	OR ([ActiveInd] = @ActiveInd AND @ActiveInd is not null)
	OR ([AnalyticsCode] = @AnalyticsCode AND @AnalyticsCode is not null)
	OR ([Custom1] = @Custom1 AND @Custom1 is not null)
	OR ([Custom2] = @Custom2 AND @Custom2 is not null)
	OR ([Custom3] = @Custom3 AND @Custom3 is not null)
	OR ([SEOURL] = @SEOURL AND @SEOURL is not null)
	OR ([LocaleId] = @LocaleId AND @LocaleId is not null)
	OR ([MetaTagAdditional] = @MetaTagAdditional AND @MetaTagAdditional is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_GetByMessageTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeMessageConfig table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_GetByMessageTypeID
(

	@MessageTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[MessageID],
					[Key],
					[PortalID],
					[LocaleID],
					[Description],
					[Value],
					[MessageTypeID],
					[PageSEOName]
				FROM
					[dbo].[ZNodeMessageConfig]
				WHERE
					[MessageTypeID] = @MessageTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_GetByKeyPortalIDLocaleIDMessageTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeMessageConfig table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_GetByKeyPortalIDLocaleIDMessageTypeID
(

	@Key nvarchar (100)  ,

	@PortalID int   ,

	@LocaleID int   ,

	@MessageTypeID int   
)
AS


				SELECT
					[MessageID],
					[Key],
					[PortalID],
					[LocaleID],
					[Description],
					[Value],
					[MessageTypeID],
					[PageSEOName]
				FROM
					[dbo].[ZNodeMessageConfig]
				WHERE
					[Key] = @Key
					AND [PortalID] = @PortalID
					AND [LocaleID] = @LocaleID
					AND [MessageTypeID] = @MessageTypeID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_GetByMessageID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeMessageConfig table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_GetByMessageID
(

	@MessageID int   
)
AS


				SELECT
					[MessageID],
					[Key],
					[PortalID],
					[LocaleID],
					[Description],
					[Value],
					[MessageTypeID],
					[PageSEOName]
				FROM
					[dbo].[ZNodeMessageConfig]
				WHERE
					[MessageID] = @MessageID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeMessageConfig table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_Find
(

	@SearchUsingOR bit   = null ,

	@MessageID int   = null ,

	@Key nvarchar (100)  = null ,

	@PortalID int   = null ,

	@LocaleID int   = null ,

	@Description nvarchar (MAX)  = null ,

	@Value nvarchar (MAX)  = null ,

	@MessageTypeID int   = null ,

	@PageSEOName nvarchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [MessageID]
	, [Key]
	, [PortalID]
	, [LocaleID]
	, [Description]
	, [Value]
	, [MessageTypeID]
	, [PageSEOName]
    FROM
	[dbo].[ZNodeMessageConfig]
    WHERE 
	 ([MessageID] = @MessageID OR @MessageID IS NULL)
	AND ([Key] = @Key OR @Key IS NULL)
	AND ([PortalID] = @PortalID OR @PortalID IS NULL)
	AND ([LocaleID] = @LocaleID OR @LocaleID IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([Value] = @Value OR @Value IS NULL)
	AND ([MessageTypeID] = @MessageTypeID OR @MessageTypeID IS NULL)
	AND ([PageSEOName] = @PageSEOName OR @PageSEOName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [MessageID]
	, [Key]
	, [PortalID]
	, [LocaleID]
	, [Description]
	, [Value]
	, [MessageTypeID]
	, [PageSEOName]
    FROM
	[dbo].[ZNodeMessageConfig]
    WHERE 
	 ([MessageID] = @MessageID AND @MessageID is not null)
	OR ([Key] = @Key AND @Key is not null)
	OR ([PortalID] = @PortalID AND @PortalID is not null)
	OR ([LocaleID] = @LocaleID AND @LocaleID is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([Value] = @Value AND @Value is not null)
	OR ([MessageTypeID] = @MessageTypeID AND @MessageTypeID is not null)
	OR ([PageSEOName] = @PageSEOName AND @PageSEOName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodePortal table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Get_List

AS


				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress]
				FROM
					[dbo].[ZNodePortal]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodePortal table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Insert
(

	@PortalID int    OUTPUT,

	@CompanyName nvarchar (MAX)  ,

	@StoreName nvarchar (MAX)  ,

	@LogoPath nvarchar (MAX)  ,

	@UseSSL bit   ,

	@AdminEmail nvarchar (MAX)  ,

	@SalesEmail nvarchar (MAX)  ,

	@CustomerServiceEmail nvarchar (MAX)  ,

	@SalesPhoneNumber nvarchar (MAX)  ,

	@CustomerServicePhoneNumber nvarchar (MAX)  ,

	@ImageNotAvailablePath nvarchar (MAX)  ,

	@MaxCatalogDisplayColumns tinyint   ,

	@MaxCatalogDisplayItems int   ,

	@MaxCatalogCategoryDisplayThumbnails int   ,

	@MaxCatalogItemSmallThumbnailWidth int   ,

	@MaxCatalogItemSmallWidth int   ,

	@MaxCatalogItemMediumWidth int   ,

	@MaxCatalogItemThumbnailWidth int   ,

	@MaxCatalogItemLargeWidth int   ,

	@MaxCatalogItemCrossSellWidth int   ,

	@ShowSwatchInCategory bit   ,

	@ShowAlternateImageInCategory bit   ,

	@ActiveInd bit   ,

	@SMTPServer nvarchar (MAX)  ,

	@SMTPUserName nvarchar (MAX)  ,

	@SMTPPassword nvarchar (MAX)  ,

	@SMTPPort int   ,

	@SiteWideBottomJavascript ntext   ,

	@SiteWideTopJavascript ntext   ,

	@OrderReceiptAffiliateJavascript ntext   ,

	@SiteWideAnalyticsJavascript ntext   ,

	@GoogleAnalyticsCode ntext   ,

	@UPSUserName nvarchar (MAX)  ,

	@UPSPassword nvarchar (MAX)  ,

	@UPSKey nvarchar (MAX)  ,

	@ShippingOriginZipCode nvarchar (50)  ,

	@MasterPage nvarchar (MAX)  ,

	@ShopByPriceMin int   ,

	@ShopByPriceMax int   ,

	@ShopByPriceIncrement int   ,

	@FedExAccountNumber nvarchar (MAX)  ,

	@FedExMeterNumber nvarchar (MAX)  ,

	@FedExProductionKey nvarchar (MAX)  ,

	@FedExSecurityCode nvarchar (MAX)  ,

	@FedExCSPKey nvarchar (MAX)  ,

	@FedExCSPPassword nvarchar (MAX)  ,

	@FedExClientProductId nvarchar (MAX)  ,

	@FedExClientProductVersion nvarchar (MAX)  ,

	@FedExDropoffType nvarchar (MAX)  ,

	@FedExPackagingType nvarchar (MAX)  ,

	@FedExUseDiscountRate bit   ,

	@FedExAddInsurance bit   ,

	@ShippingOriginAddress1 nvarchar (MAX)  ,

	@ShippingOriginAddress2 nvarchar (MAX)  ,

	@ShippingOriginCity nvarchar (MAX)  ,

	@ShippingOriginStateCode nvarchar (MAX)  ,

	@ShippingOriginCountryCode nvarchar (MAX)  ,

	@ShippingOriginPhone nvarchar (MAX)  ,

	@CurrencyTypeID int   ,

	@WeightUnit nvarchar (MAX)  ,

	@DimensionUnit nvarchar (MAX)  ,

	@EmailListLogin nvarchar (100)  ,

	@EmailListPassword nvarchar (100)  ,

	@EmailListDefaultList nvarchar (MAX)  ,

	@ShippingTaxable bit   ,

	@DefaultOrderStateID int   ,

	@DefaultReviewStatus nvarchar (1)  ,

	@DefaultAnonymousProfileID int   ,

	@DefaultRegisteredProfileID int   ,

	@InclusiveTax bit   ,

	@SeoDefaultProductTitle nvarchar (MAX)  ,

	@SeoDefaultProductDescription nvarchar (MAX)  ,

	@SeoDefaultProductKeyword nvarchar (MAX)  ,

	@SeoDefaultCategoryTitle nvarchar (MAX)  ,

	@SeoDefaultCategoryDescription nvarchar (MAX)  ,

	@SeoDefaultCategoryKeyword nvarchar (MAX)  ,

	@SeoDefaultContentTitle nvarchar (MAX)  ,

	@SeoDefaultContentDescription nvarchar (MAX)  ,

	@SeoDefaultContentKeyword nvarchar (MAX)  ,

	@TimeZoneOffset nvarchar (50)  ,

	@LocaleID int   ,

	@SplashCategoryID int   ,

	@SplashImageFile nvarchar (MAX)  ,

	@MobileTheme nvarchar (MAX)  ,

	@PersistentCartEnabled bit   ,

	@DefaultProductReviewStateID int   ,

	@UseDynamicDisplayOrder bit   ,

	@EnableAddressValidation bit   ,

	@RequireValidatedAddress bit   
)
AS


				
				INSERT INTO [dbo].[ZNodePortal]
					(
					[CompanyName]
					,[StoreName]
					,[LogoPath]
					,[UseSSL]
					,[AdminEmail]
					,[SalesEmail]
					,[CustomerServiceEmail]
					,[SalesPhoneNumber]
					,[CustomerServicePhoneNumber]
					,[ImageNotAvailablePath]
					,[MaxCatalogDisplayColumns]
					,[MaxCatalogDisplayItems]
					,[MaxCatalogCategoryDisplayThumbnails]
					,[MaxCatalogItemSmallThumbnailWidth]
					,[MaxCatalogItemSmallWidth]
					,[MaxCatalogItemMediumWidth]
					,[MaxCatalogItemThumbnailWidth]
					,[MaxCatalogItemLargeWidth]
					,[MaxCatalogItemCrossSellWidth]
					,[ShowSwatchInCategory]
					,[ShowAlternateImageInCategory]
					,[ActiveInd]
					,[SMTPServer]
					,[SMTPUserName]
					,[SMTPPassword]
					,[SMTPPort]
					,[SiteWideBottomJavascript]
					,[SiteWideTopJavascript]
					,[OrderReceiptAffiliateJavascript]
					,[SiteWideAnalyticsJavascript]
					,[GoogleAnalyticsCode]
					,[UPSUserName]
					,[UPSPassword]
					,[UPSKey]
					,[ShippingOriginZipCode]
					,[MasterPage]
					,[ShopByPriceMin]
					,[ShopByPriceMax]
					,[ShopByPriceIncrement]
					,[FedExAccountNumber]
					,[FedExMeterNumber]
					,[FedExProductionKey]
					,[FedExSecurityCode]
					,[FedExCSPKey]
					,[FedExCSPPassword]
					,[FedExClientProductId]
					,[FedExClientProductVersion]
					,[FedExDropoffType]
					,[FedExPackagingType]
					,[FedExUseDiscountRate]
					,[FedExAddInsurance]
					,[ShippingOriginAddress1]
					,[ShippingOriginAddress2]
					,[ShippingOriginCity]
					,[ShippingOriginStateCode]
					,[ShippingOriginCountryCode]
					,[ShippingOriginPhone]
					,[CurrencyTypeID]
					,[WeightUnit]
					,[DimensionUnit]
					,[EmailListLogin]
					,[EmailListPassword]
					,[EmailListDefaultList]
					,[ShippingTaxable]
					,[DefaultOrderStateID]
					,[DefaultReviewStatus]
					,[DefaultAnonymousProfileID]
					,[DefaultRegisteredProfileID]
					,[InclusiveTax]
					,[SeoDefaultProductTitle]
					,[SeoDefaultProductDescription]
					,[SeoDefaultProductKeyword]
					,[SeoDefaultCategoryTitle]
					,[SeoDefaultCategoryDescription]
					,[SeoDefaultCategoryKeyword]
					,[SeoDefaultContentTitle]
					,[SeoDefaultContentDescription]
					,[SeoDefaultContentKeyword]
					,[TimeZoneOffset]
					,[LocaleID]
					,[SplashCategoryID]
					,[SplashImageFile]
					,[MobileTheme]
					,[PersistentCartEnabled]
					,[DefaultProductReviewStateID]
					,[UseDynamicDisplayOrder]
					,[EnableAddressValidation]
					,[RequireValidatedAddress]
					)
				VALUES
					(
					@CompanyName
					,@StoreName
					,@LogoPath
					,@UseSSL
					,@AdminEmail
					,@SalesEmail
					,@CustomerServiceEmail
					,@SalesPhoneNumber
					,@CustomerServicePhoneNumber
					,@ImageNotAvailablePath
					,@MaxCatalogDisplayColumns
					,@MaxCatalogDisplayItems
					,@MaxCatalogCategoryDisplayThumbnails
					,@MaxCatalogItemSmallThumbnailWidth
					,@MaxCatalogItemSmallWidth
					,@MaxCatalogItemMediumWidth
					,@MaxCatalogItemThumbnailWidth
					,@MaxCatalogItemLargeWidth
					,@MaxCatalogItemCrossSellWidth
					,@ShowSwatchInCategory
					,@ShowAlternateImageInCategory
					,@ActiveInd
					,@SMTPServer
					,@SMTPUserName
					,@SMTPPassword
					,@SMTPPort
					,@SiteWideBottomJavascript
					,@SiteWideTopJavascript
					,@OrderReceiptAffiliateJavascript
					,@SiteWideAnalyticsJavascript
					,@GoogleAnalyticsCode
					,@UPSUserName
					,@UPSPassword
					,@UPSKey
					,@ShippingOriginZipCode
					,@MasterPage
					,@ShopByPriceMin
					,@ShopByPriceMax
					,@ShopByPriceIncrement
					,@FedExAccountNumber
					,@FedExMeterNumber
					,@FedExProductionKey
					,@FedExSecurityCode
					,@FedExCSPKey
					,@FedExCSPPassword
					,@FedExClientProductId
					,@FedExClientProductVersion
					,@FedExDropoffType
					,@FedExPackagingType
					,@FedExUseDiscountRate
					,@FedExAddInsurance
					,@ShippingOriginAddress1
					,@ShippingOriginAddress2
					,@ShippingOriginCity
					,@ShippingOriginStateCode
					,@ShippingOriginCountryCode
					,@ShippingOriginPhone
					,@CurrencyTypeID
					,@WeightUnit
					,@DimensionUnit
					,@EmailListLogin
					,@EmailListPassword
					,@EmailListDefaultList
					,@ShippingTaxable
					,@DefaultOrderStateID
					,@DefaultReviewStatus
					,@DefaultAnonymousProfileID
					,@DefaultRegisteredProfileID
					,@InclusiveTax
					,@SeoDefaultProductTitle
					,@SeoDefaultProductDescription
					,@SeoDefaultProductKeyword
					,@SeoDefaultCategoryTitle
					,@SeoDefaultCategoryDescription
					,@SeoDefaultCategoryKeyword
					,@SeoDefaultContentTitle
					,@SeoDefaultContentDescription
					,@SeoDefaultContentKeyword
					,@TimeZoneOffset
					,@LocaleID
					,@SplashCategoryID
					,@SplashImageFile
					,@MobileTheme
					,@PersistentCartEnabled
					,@DefaultProductReviewStateID
					,@UseDynamicDisplayOrder
					,@EnableAddressValidation
					,@RequireValidatedAddress
					)
				
				-- Get the identity value
				SET @PortalID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodePortal table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Update
(

	@PortalID int   ,

	@CompanyName nvarchar (MAX)  ,

	@StoreName nvarchar (MAX)  ,

	@LogoPath nvarchar (MAX)  ,

	@UseSSL bit   ,

	@AdminEmail nvarchar (MAX)  ,

	@SalesEmail nvarchar (MAX)  ,

	@CustomerServiceEmail nvarchar (MAX)  ,

	@SalesPhoneNumber nvarchar (MAX)  ,

	@CustomerServicePhoneNumber nvarchar (MAX)  ,

	@ImageNotAvailablePath nvarchar (MAX)  ,

	@MaxCatalogDisplayColumns tinyint   ,

	@MaxCatalogDisplayItems int   ,

	@MaxCatalogCategoryDisplayThumbnails int   ,

	@MaxCatalogItemSmallThumbnailWidth int   ,

	@MaxCatalogItemSmallWidth int   ,

	@MaxCatalogItemMediumWidth int   ,

	@MaxCatalogItemThumbnailWidth int   ,

	@MaxCatalogItemLargeWidth int   ,

	@MaxCatalogItemCrossSellWidth int   ,

	@ShowSwatchInCategory bit   ,

	@ShowAlternateImageInCategory bit   ,

	@ActiveInd bit   ,

	@SMTPServer nvarchar (MAX)  ,

	@SMTPUserName nvarchar (MAX)  ,

	@SMTPPassword nvarchar (MAX)  ,

	@SMTPPort int   ,

	@SiteWideBottomJavascript ntext   ,

	@SiteWideTopJavascript ntext   ,

	@OrderReceiptAffiliateJavascript ntext   ,

	@SiteWideAnalyticsJavascript ntext   ,

	@GoogleAnalyticsCode ntext   ,

	@UPSUserName nvarchar (MAX)  ,

	@UPSPassword nvarchar (MAX)  ,

	@UPSKey nvarchar (MAX)  ,

	@ShippingOriginZipCode nvarchar (50)  ,

	@MasterPage nvarchar (MAX)  ,

	@ShopByPriceMin int   ,

	@ShopByPriceMax int   ,

	@ShopByPriceIncrement int   ,

	@FedExAccountNumber nvarchar (MAX)  ,

	@FedExMeterNumber nvarchar (MAX)  ,

	@FedExProductionKey nvarchar (MAX)  ,

	@FedExSecurityCode nvarchar (MAX)  ,

	@FedExCSPKey nvarchar (MAX)  ,

	@FedExCSPPassword nvarchar (MAX)  ,

	@FedExClientProductId nvarchar (MAX)  ,

	@FedExClientProductVersion nvarchar (MAX)  ,

	@FedExDropoffType nvarchar (MAX)  ,

	@FedExPackagingType nvarchar (MAX)  ,

	@FedExUseDiscountRate bit   ,

	@FedExAddInsurance bit   ,

	@ShippingOriginAddress1 nvarchar (MAX)  ,

	@ShippingOriginAddress2 nvarchar (MAX)  ,

	@ShippingOriginCity nvarchar (MAX)  ,

	@ShippingOriginStateCode nvarchar (MAX)  ,

	@ShippingOriginCountryCode nvarchar (MAX)  ,

	@ShippingOriginPhone nvarchar (MAX)  ,

	@CurrencyTypeID int   ,

	@WeightUnit nvarchar (MAX)  ,

	@DimensionUnit nvarchar (MAX)  ,

	@EmailListLogin nvarchar (100)  ,

	@EmailListPassword nvarchar (100)  ,

	@EmailListDefaultList nvarchar (MAX)  ,

	@ShippingTaxable bit   ,

	@DefaultOrderStateID int   ,

	@DefaultReviewStatus nvarchar (1)  ,

	@DefaultAnonymousProfileID int   ,

	@DefaultRegisteredProfileID int   ,

	@InclusiveTax bit   ,

	@SeoDefaultProductTitle nvarchar (MAX)  ,

	@SeoDefaultProductDescription nvarchar (MAX)  ,

	@SeoDefaultProductKeyword nvarchar (MAX)  ,

	@SeoDefaultCategoryTitle nvarchar (MAX)  ,

	@SeoDefaultCategoryDescription nvarchar (MAX)  ,

	@SeoDefaultCategoryKeyword nvarchar (MAX)  ,

	@SeoDefaultContentTitle nvarchar (MAX)  ,

	@SeoDefaultContentDescription nvarchar (MAX)  ,

	@SeoDefaultContentKeyword nvarchar (MAX)  ,

	@TimeZoneOffset nvarchar (50)  ,

	@LocaleID int   ,

	@SplashCategoryID int   ,

	@SplashImageFile nvarchar (MAX)  ,

	@MobileTheme nvarchar (MAX)  ,

	@PersistentCartEnabled bit   ,

	@DefaultProductReviewStateID int   ,

	@UseDynamicDisplayOrder bit   ,

	@EnableAddressValidation bit   ,

	@RequireValidatedAddress bit   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodePortal]
				SET
					[CompanyName] = @CompanyName
					,[StoreName] = @StoreName
					,[LogoPath] = @LogoPath
					,[UseSSL] = @UseSSL
					,[AdminEmail] = @AdminEmail
					,[SalesEmail] = @SalesEmail
					,[CustomerServiceEmail] = @CustomerServiceEmail
					,[SalesPhoneNumber] = @SalesPhoneNumber
					,[CustomerServicePhoneNumber] = @CustomerServicePhoneNumber
					,[ImageNotAvailablePath] = @ImageNotAvailablePath
					,[MaxCatalogDisplayColumns] = @MaxCatalogDisplayColumns
					,[MaxCatalogDisplayItems] = @MaxCatalogDisplayItems
					,[MaxCatalogCategoryDisplayThumbnails] = @MaxCatalogCategoryDisplayThumbnails
					,[MaxCatalogItemSmallThumbnailWidth] = @MaxCatalogItemSmallThumbnailWidth
					,[MaxCatalogItemSmallWidth] = @MaxCatalogItemSmallWidth
					,[MaxCatalogItemMediumWidth] = @MaxCatalogItemMediumWidth
					,[MaxCatalogItemThumbnailWidth] = @MaxCatalogItemThumbnailWidth
					,[MaxCatalogItemLargeWidth] = @MaxCatalogItemLargeWidth
					,[MaxCatalogItemCrossSellWidth] = @MaxCatalogItemCrossSellWidth
					,[ShowSwatchInCategory] = @ShowSwatchInCategory
					,[ShowAlternateImageInCategory] = @ShowAlternateImageInCategory
					,[ActiveInd] = @ActiveInd
					,[SMTPServer] = @SMTPServer
					,[SMTPUserName] = @SMTPUserName
					,[SMTPPassword] = @SMTPPassword
					,[SMTPPort] = @SMTPPort
					,[SiteWideBottomJavascript] = @SiteWideBottomJavascript
					,[SiteWideTopJavascript] = @SiteWideTopJavascript
					,[OrderReceiptAffiliateJavascript] = @OrderReceiptAffiliateJavascript
					,[SiteWideAnalyticsJavascript] = @SiteWideAnalyticsJavascript
					,[GoogleAnalyticsCode] = @GoogleAnalyticsCode
					,[UPSUserName] = @UPSUserName
					,[UPSPassword] = @UPSPassword
					,[UPSKey] = @UPSKey
					,[ShippingOriginZipCode] = @ShippingOriginZipCode
					,[MasterPage] = @MasterPage
					,[ShopByPriceMin] = @ShopByPriceMin
					,[ShopByPriceMax] = @ShopByPriceMax
					,[ShopByPriceIncrement] = @ShopByPriceIncrement
					,[FedExAccountNumber] = @FedExAccountNumber
					,[FedExMeterNumber] = @FedExMeterNumber
					,[FedExProductionKey] = @FedExProductionKey
					,[FedExSecurityCode] = @FedExSecurityCode
					,[FedExCSPKey] = @FedExCSPKey
					,[FedExCSPPassword] = @FedExCSPPassword
					,[FedExClientProductId] = @FedExClientProductId
					,[FedExClientProductVersion] = @FedExClientProductVersion
					,[FedExDropoffType] = @FedExDropoffType
					,[FedExPackagingType] = @FedExPackagingType
					,[FedExUseDiscountRate] = @FedExUseDiscountRate
					,[FedExAddInsurance] = @FedExAddInsurance
					,[ShippingOriginAddress1] = @ShippingOriginAddress1
					,[ShippingOriginAddress2] = @ShippingOriginAddress2
					,[ShippingOriginCity] = @ShippingOriginCity
					,[ShippingOriginStateCode] = @ShippingOriginStateCode
					,[ShippingOriginCountryCode] = @ShippingOriginCountryCode
					,[ShippingOriginPhone] = @ShippingOriginPhone
					,[CurrencyTypeID] = @CurrencyTypeID
					,[WeightUnit] = @WeightUnit
					,[DimensionUnit] = @DimensionUnit
					,[EmailListLogin] = @EmailListLogin
					,[EmailListPassword] = @EmailListPassword
					,[EmailListDefaultList] = @EmailListDefaultList
					,[ShippingTaxable] = @ShippingTaxable
					,[DefaultOrderStateID] = @DefaultOrderStateID
					,[DefaultReviewStatus] = @DefaultReviewStatus
					,[DefaultAnonymousProfileID] = @DefaultAnonymousProfileID
					,[DefaultRegisteredProfileID] = @DefaultRegisteredProfileID
					,[InclusiveTax] = @InclusiveTax
					,[SeoDefaultProductTitle] = @SeoDefaultProductTitle
					,[SeoDefaultProductDescription] = @SeoDefaultProductDescription
					,[SeoDefaultProductKeyword] = @SeoDefaultProductKeyword
					,[SeoDefaultCategoryTitle] = @SeoDefaultCategoryTitle
					,[SeoDefaultCategoryDescription] = @SeoDefaultCategoryDescription
					,[SeoDefaultCategoryKeyword] = @SeoDefaultCategoryKeyword
					,[SeoDefaultContentTitle] = @SeoDefaultContentTitle
					,[SeoDefaultContentDescription] = @SeoDefaultContentDescription
					,[SeoDefaultContentKeyword] = @SeoDefaultContentKeyword
					,[TimeZoneOffset] = @TimeZoneOffset
					,[LocaleID] = @LocaleID
					,[SplashCategoryID] = @SplashCategoryID
					,[SplashImageFile] = @SplashImageFile
					,[MobileTheme] = @MobileTheme
					,[PersistentCartEnabled] = @PersistentCartEnabled
					,[DefaultProductReviewStateID] = @DefaultProductReviewStateID
					,[UseDynamicDisplayOrder] = @UseDynamicDisplayOrder
					,[EnableAddressValidation] = @EnableAddressValidation
					,[RequireValidatedAddress] = @RequireValidatedAddress
				WHERE
[PortalID] = @PortalID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByCurrencyTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByCurrencyTypeID
(

	@CurrencyTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[CurrencyTypeID] = @CurrencyTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByLocaleID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByLocaleID
(

	@LocaleID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[LocaleID] = @LocaleID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByDefaultOrderStateID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByDefaultOrderStateID
(

	@DefaultOrderStateID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[DefaultOrderStateID] = @DefaultOrderStateID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodePortal_GetByDefaultProductReviewStateID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByDefaultProductReviewStateID
(

	@DefaultProductReviewStateID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[DefaultProductReviewStateID] = @DefaultProductReviewStateID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByActiveInd]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByActiveInd
(

	@ActiveInd bit   
)
AS


				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[ActiveInd] = @ActiveInd
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByPortalID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByPortalID
(

	@PortalID int   
)
AS


				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[PortalID] = @PortalID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodePortal table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Find
(

	@SearchUsingOR bit   = null ,

	@PortalID int   = null ,

	@CompanyName nvarchar (MAX)  = null ,

	@StoreName nvarchar (MAX)  = null ,

	@LogoPath nvarchar (MAX)  = null ,

	@UseSSL bit   = null ,

	@AdminEmail nvarchar (MAX)  = null ,

	@SalesEmail nvarchar (MAX)  = null ,

	@CustomerServiceEmail nvarchar (MAX)  = null ,

	@SalesPhoneNumber nvarchar (MAX)  = null ,

	@CustomerServicePhoneNumber nvarchar (MAX)  = null ,

	@ImageNotAvailablePath nvarchar (MAX)  = null ,

	@MaxCatalogDisplayColumns tinyint   = null ,

	@MaxCatalogDisplayItems int   = null ,

	@MaxCatalogCategoryDisplayThumbnails int   = null ,

	@MaxCatalogItemSmallThumbnailWidth int   = null ,

	@MaxCatalogItemSmallWidth int   = null ,

	@MaxCatalogItemMediumWidth int   = null ,

	@MaxCatalogItemThumbnailWidth int   = null ,

	@MaxCatalogItemLargeWidth int   = null ,

	@MaxCatalogItemCrossSellWidth int   = null ,

	@ShowSwatchInCategory bit   = null ,

	@ShowAlternateImageInCategory bit   = null ,

	@ActiveInd bit   = null ,

	@SMTPServer nvarchar (MAX)  = null ,

	@SMTPUserName nvarchar (MAX)  = null ,

	@SMTPPassword nvarchar (MAX)  = null ,

	@SMTPPort int   = null ,

	@SiteWideBottomJavascript ntext   = null ,

	@SiteWideTopJavascript ntext   = null ,

	@OrderReceiptAffiliateJavascript ntext   = null ,

	@SiteWideAnalyticsJavascript ntext   = null ,

	@GoogleAnalyticsCode ntext   = null ,

	@UPSUserName nvarchar (MAX)  = null ,

	@UPSPassword nvarchar (MAX)  = null ,

	@UPSKey nvarchar (MAX)  = null ,

	@ShippingOriginZipCode nvarchar (50)  = null ,

	@MasterPage nvarchar (MAX)  = null ,

	@ShopByPriceMin int   = null ,

	@ShopByPriceMax int   = null ,

	@ShopByPriceIncrement int   = null ,

	@FedExAccountNumber nvarchar (MAX)  = null ,

	@FedExMeterNumber nvarchar (MAX)  = null ,

	@FedExProductionKey nvarchar (MAX)  = null ,

	@FedExSecurityCode nvarchar (MAX)  = null ,

	@FedExCSPKey nvarchar (MAX)  = null ,

	@FedExCSPPassword nvarchar (MAX)  = null ,

	@FedExClientProductId nvarchar (MAX)  = null ,

	@FedExClientProductVersion nvarchar (MAX)  = null ,

	@FedExDropoffType nvarchar (MAX)  = null ,

	@FedExPackagingType nvarchar (MAX)  = null ,

	@FedExUseDiscountRate bit   = null ,

	@FedExAddInsurance bit   = null ,

	@ShippingOriginAddress1 nvarchar (MAX)  = null ,

	@ShippingOriginAddress2 nvarchar (MAX)  = null ,

	@ShippingOriginCity nvarchar (MAX)  = null ,

	@ShippingOriginStateCode nvarchar (MAX)  = null ,

	@ShippingOriginCountryCode nvarchar (MAX)  = null ,

	@ShippingOriginPhone nvarchar (MAX)  = null ,

	@CurrencyTypeID int   = null ,

	@WeightUnit nvarchar (MAX)  = null ,

	@DimensionUnit nvarchar (MAX)  = null ,

	@EmailListLogin nvarchar (100)  = null ,

	@EmailListPassword nvarchar (100)  = null ,

	@EmailListDefaultList nvarchar (MAX)  = null ,

	@ShippingTaxable bit   = null ,

	@DefaultOrderStateID int   = null ,

	@DefaultReviewStatus nvarchar (1)  = null ,

	@DefaultAnonymousProfileID int   = null ,

	@DefaultRegisteredProfileID int   = null ,

	@InclusiveTax bit   = null ,

	@SeoDefaultProductTitle nvarchar (MAX)  = null ,

	@SeoDefaultProductDescription nvarchar (MAX)  = null ,

	@SeoDefaultProductKeyword nvarchar (MAX)  = null ,

	@SeoDefaultCategoryTitle nvarchar (MAX)  = null ,

	@SeoDefaultCategoryDescription nvarchar (MAX)  = null ,

	@SeoDefaultCategoryKeyword nvarchar (MAX)  = null ,

	@SeoDefaultContentTitle nvarchar (MAX)  = null ,

	@SeoDefaultContentDescription nvarchar (MAX)  = null ,

	@SeoDefaultContentKeyword nvarchar (MAX)  = null ,

	@TimeZoneOffset nvarchar (50)  = null ,

	@LocaleID int   = null ,

	@SplashCategoryID int   = null ,

	@SplashImageFile nvarchar (MAX)  = null ,

	@MobileTheme nvarchar (MAX)  = null ,

	@PersistentCartEnabled bit   = null ,

	@DefaultProductReviewStateID int   = null ,

	@UseDynamicDisplayOrder bit   = null ,

	@EnableAddressValidation bit   = null ,

	@RequireValidatedAddress bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalID]
	, [CompanyName]
	, [StoreName]
	, [LogoPath]
	, [UseSSL]
	, [AdminEmail]
	, [SalesEmail]
	, [CustomerServiceEmail]
	, [SalesPhoneNumber]
	, [CustomerServicePhoneNumber]
	, [ImageNotAvailablePath]
	, [MaxCatalogDisplayColumns]
	, [MaxCatalogDisplayItems]
	, [MaxCatalogCategoryDisplayThumbnails]
	, [MaxCatalogItemSmallThumbnailWidth]
	, [MaxCatalogItemSmallWidth]
	, [MaxCatalogItemMediumWidth]
	, [MaxCatalogItemThumbnailWidth]
	, [MaxCatalogItemLargeWidth]
	, [MaxCatalogItemCrossSellWidth]
	, [ShowSwatchInCategory]
	, [ShowAlternateImageInCategory]
	, [ActiveInd]
	, [SMTPServer]
	, [SMTPUserName]
	, [SMTPPassword]
	, [SMTPPort]
	, [SiteWideBottomJavascript]
	, [SiteWideTopJavascript]
	, [OrderReceiptAffiliateJavascript]
	, [SiteWideAnalyticsJavascript]
	, [GoogleAnalyticsCode]
	, [UPSUserName]
	, [UPSPassword]
	, [UPSKey]
	, [ShippingOriginZipCode]
	, [MasterPage]
	, [ShopByPriceMin]
	, [ShopByPriceMax]
	, [ShopByPriceIncrement]
	, [FedExAccountNumber]
	, [FedExMeterNumber]
	, [FedExProductionKey]
	, [FedExSecurityCode]
	, [FedExCSPKey]
	, [FedExCSPPassword]
	, [FedExClientProductId]
	, [FedExClientProductVersion]
	, [FedExDropoffType]
	, [FedExPackagingType]
	, [FedExUseDiscountRate]
	, [FedExAddInsurance]
	, [ShippingOriginAddress1]
	, [ShippingOriginAddress2]
	, [ShippingOriginCity]
	, [ShippingOriginStateCode]
	, [ShippingOriginCountryCode]
	, [ShippingOriginPhone]
	, [CurrencyTypeID]
	, [WeightUnit]
	, [DimensionUnit]
	, [EmailListLogin]
	, [EmailListPassword]
	, [EmailListDefaultList]
	, [ShippingTaxable]
	, [DefaultOrderStateID]
	, [DefaultReviewStatus]
	, [DefaultAnonymousProfileID]
	, [DefaultRegisteredProfileID]
	, [InclusiveTax]
	, [SeoDefaultProductTitle]
	, [SeoDefaultProductDescription]
	, [SeoDefaultProductKeyword]
	, [SeoDefaultCategoryTitle]
	, [SeoDefaultCategoryDescription]
	, [SeoDefaultCategoryKeyword]
	, [SeoDefaultContentTitle]
	, [SeoDefaultContentDescription]
	, [SeoDefaultContentKeyword]
	, [TimeZoneOffset]
	, [LocaleID]
	, [SplashCategoryID]
	, [SplashImageFile]
	, [MobileTheme]
	, [PersistentCartEnabled]
	, [DefaultProductReviewStateID]
	, [UseDynamicDisplayOrder]
	, [EnableAddressValidation]
	, [RequireValidatedAddress]
    FROM
	[dbo].[ZNodePortal]
    WHERE 
	 ([PortalID] = @PortalID OR @PortalID IS NULL)
	AND ([CompanyName] = @CompanyName OR @CompanyName IS NULL)
	AND ([StoreName] = @StoreName OR @StoreName IS NULL)
	AND ([LogoPath] = @LogoPath OR @LogoPath IS NULL)
	AND ([UseSSL] = @UseSSL OR @UseSSL IS NULL)
	AND ([AdminEmail] = @AdminEmail OR @AdminEmail IS NULL)
	AND ([SalesEmail] = @SalesEmail OR @SalesEmail IS NULL)
	AND ([CustomerServiceEmail] = @CustomerServiceEmail OR @CustomerServiceEmail IS NULL)
	AND ([SalesPhoneNumber] = @SalesPhoneNumber OR @SalesPhoneNumber IS NULL)
	AND ([CustomerServicePhoneNumber] = @CustomerServicePhoneNumber OR @CustomerServicePhoneNumber IS NULL)
	AND ([ImageNotAvailablePath] = @ImageNotAvailablePath OR @ImageNotAvailablePath IS NULL)
	AND ([MaxCatalogDisplayColumns] = @MaxCatalogDisplayColumns OR @MaxCatalogDisplayColumns IS NULL)
	AND ([MaxCatalogDisplayItems] = @MaxCatalogDisplayItems OR @MaxCatalogDisplayItems IS NULL)
	AND ([MaxCatalogCategoryDisplayThumbnails] = @MaxCatalogCategoryDisplayThumbnails OR @MaxCatalogCategoryDisplayThumbnails IS NULL)
	AND ([MaxCatalogItemSmallThumbnailWidth] = @MaxCatalogItemSmallThumbnailWidth OR @MaxCatalogItemSmallThumbnailWidth IS NULL)
	AND ([MaxCatalogItemSmallWidth] = @MaxCatalogItemSmallWidth OR @MaxCatalogItemSmallWidth IS NULL)
	AND ([MaxCatalogItemMediumWidth] = @MaxCatalogItemMediumWidth OR @MaxCatalogItemMediumWidth IS NULL)
	AND ([MaxCatalogItemThumbnailWidth] = @MaxCatalogItemThumbnailWidth OR @MaxCatalogItemThumbnailWidth IS NULL)
	AND ([MaxCatalogItemLargeWidth] = @MaxCatalogItemLargeWidth OR @MaxCatalogItemLargeWidth IS NULL)
	AND ([MaxCatalogItemCrossSellWidth] = @MaxCatalogItemCrossSellWidth OR @MaxCatalogItemCrossSellWidth IS NULL)
	AND ([ShowSwatchInCategory] = @ShowSwatchInCategory OR @ShowSwatchInCategory IS NULL)
	AND ([ShowAlternateImageInCategory] = @ShowAlternateImageInCategory OR @ShowAlternateImageInCategory IS NULL)
	AND ([ActiveInd] = @ActiveInd OR @ActiveInd IS NULL)
	AND ([SMTPServer] = @SMTPServer OR @SMTPServer IS NULL)
	AND ([SMTPUserName] = @SMTPUserName OR @SMTPUserName IS NULL)
	AND ([SMTPPassword] = @SMTPPassword OR @SMTPPassword IS NULL)
	AND ([SMTPPort] = @SMTPPort OR @SMTPPort IS NULL)
	AND ([UPSUserName] = @UPSUserName OR @UPSUserName IS NULL)
	AND ([UPSPassword] = @UPSPassword OR @UPSPassword IS NULL)
	AND ([UPSKey] = @UPSKey OR @UPSKey IS NULL)
	AND ([ShippingOriginZipCode] = @ShippingOriginZipCode OR @ShippingOriginZipCode IS NULL)
	AND ([MasterPage] = @MasterPage OR @MasterPage IS NULL)
	AND ([ShopByPriceMin] = @ShopByPriceMin OR @ShopByPriceMin IS NULL)
	AND ([ShopByPriceMax] = @ShopByPriceMax OR @ShopByPriceMax IS NULL)
	AND ([ShopByPriceIncrement] = @ShopByPriceIncrement OR @ShopByPriceIncrement IS NULL)
	AND ([FedExAccountNumber] = @FedExAccountNumber OR @FedExAccountNumber IS NULL)
	AND ([FedExMeterNumber] = @FedExMeterNumber OR @FedExMeterNumber IS NULL)
	AND ([FedExProductionKey] = @FedExProductionKey OR @FedExProductionKey IS NULL)
	AND ([FedExSecurityCode] = @FedExSecurityCode OR @FedExSecurityCode IS NULL)
	AND ([FedExCSPKey] = @FedExCSPKey OR @FedExCSPKey IS NULL)
	AND ([FedExCSPPassword] = @FedExCSPPassword OR @FedExCSPPassword IS NULL)
	AND ([FedExClientProductId] = @FedExClientProductId OR @FedExClientProductId IS NULL)
	AND ([FedExClientProductVersion] = @FedExClientProductVersion OR @FedExClientProductVersion IS NULL)
	AND ([FedExDropoffType] = @FedExDropoffType OR @FedExDropoffType IS NULL)
	AND ([FedExPackagingType] = @FedExPackagingType OR @FedExPackagingType IS NULL)
	AND ([FedExUseDiscountRate] = @FedExUseDiscountRate OR @FedExUseDiscountRate IS NULL)
	AND ([FedExAddInsurance] = @FedExAddInsurance OR @FedExAddInsurance IS NULL)
	AND ([ShippingOriginAddress1] = @ShippingOriginAddress1 OR @ShippingOriginAddress1 IS NULL)
	AND ([ShippingOriginAddress2] = @ShippingOriginAddress2 OR @ShippingOriginAddress2 IS NULL)
	AND ([ShippingOriginCity] = @ShippingOriginCity OR @ShippingOriginCity IS NULL)
	AND ([ShippingOriginStateCode] = @ShippingOriginStateCode OR @ShippingOriginStateCode IS NULL)
	AND ([ShippingOriginCountryCode] = @ShippingOriginCountryCode OR @ShippingOriginCountryCode IS NULL)
	AND ([ShippingOriginPhone] = @ShippingOriginPhone OR @ShippingOriginPhone IS NULL)
	AND ([CurrencyTypeID] = @CurrencyTypeID OR @CurrencyTypeID IS NULL)
	AND ([WeightUnit] = @WeightUnit OR @WeightUnit IS NULL)
	AND ([DimensionUnit] = @DimensionUnit OR @DimensionUnit IS NULL)
	AND ([EmailListLogin] = @EmailListLogin OR @EmailListLogin IS NULL)
	AND ([EmailListPassword] = @EmailListPassword OR @EmailListPassword IS NULL)
	AND ([EmailListDefaultList] = @EmailListDefaultList OR @EmailListDefaultList IS NULL)
	AND ([ShippingTaxable] = @ShippingTaxable OR @ShippingTaxable IS NULL)
	AND ([DefaultOrderStateID] = @DefaultOrderStateID OR @DefaultOrderStateID IS NULL)
	AND ([DefaultReviewStatus] = @DefaultReviewStatus OR @DefaultReviewStatus IS NULL)
	AND ([DefaultAnonymousProfileID] = @DefaultAnonymousProfileID OR @DefaultAnonymousProfileID IS NULL)
	AND ([DefaultRegisteredProfileID] = @DefaultRegisteredProfileID OR @DefaultRegisteredProfileID IS NULL)
	AND ([InclusiveTax] = @InclusiveTax OR @InclusiveTax IS NULL)
	AND ([SeoDefaultProductTitle] = @SeoDefaultProductTitle OR @SeoDefaultProductTitle IS NULL)
	AND ([SeoDefaultProductDescription] = @SeoDefaultProductDescription OR @SeoDefaultProductDescription IS NULL)
	AND ([SeoDefaultProductKeyword] = @SeoDefaultProductKeyword OR @SeoDefaultProductKeyword IS NULL)
	AND ([SeoDefaultCategoryTitle] = @SeoDefaultCategoryTitle OR @SeoDefaultCategoryTitle IS NULL)
	AND ([SeoDefaultCategoryDescription] = @SeoDefaultCategoryDescription OR @SeoDefaultCategoryDescription IS NULL)
	AND ([SeoDefaultCategoryKeyword] = @SeoDefaultCategoryKeyword OR @SeoDefaultCategoryKeyword IS NULL)
	AND ([SeoDefaultContentTitle] = @SeoDefaultContentTitle OR @SeoDefaultContentTitle IS NULL)
	AND ([SeoDefaultContentDescription] = @SeoDefaultContentDescription OR @SeoDefaultContentDescription IS NULL)
	AND ([SeoDefaultContentKeyword] = @SeoDefaultContentKeyword OR @SeoDefaultContentKeyword IS NULL)
	AND ([TimeZoneOffset] = @TimeZoneOffset OR @TimeZoneOffset IS NULL)
	AND ([LocaleID] = @LocaleID OR @LocaleID IS NULL)
	AND ([SplashCategoryID] = @SplashCategoryID OR @SplashCategoryID IS NULL)
	AND ([SplashImageFile] = @SplashImageFile OR @SplashImageFile IS NULL)
	AND ([MobileTheme] = @MobileTheme OR @MobileTheme IS NULL)
	AND ([PersistentCartEnabled] = @PersistentCartEnabled OR @PersistentCartEnabled IS NULL)
	AND ([DefaultProductReviewStateID] = @DefaultProductReviewStateID OR @DefaultProductReviewStateID IS NULL)
	AND ([UseDynamicDisplayOrder] = @UseDynamicDisplayOrder OR @UseDynamicDisplayOrder IS NULL)
	AND ([EnableAddressValidation] = @EnableAddressValidation OR @EnableAddressValidation IS NULL)
	AND ([RequireValidatedAddress] = @RequireValidatedAddress OR @RequireValidatedAddress IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalID]
	, [CompanyName]
	, [StoreName]
	, [LogoPath]
	, [UseSSL]
	, [AdminEmail]
	, [SalesEmail]
	, [CustomerServiceEmail]
	, [SalesPhoneNumber]
	, [CustomerServicePhoneNumber]
	, [ImageNotAvailablePath]
	, [MaxCatalogDisplayColumns]
	, [MaxCatalogDisplayItems]
	, [MaxCatalogCategoryDisplayThumbnails]
	, [MaxCatalogItemSmallThumbnailWidth]
	, [MaxCatalogItemSmallWidth]
	, [MaxCatalogItemMediumWidth]
	, [MaxCatalogItemThumbnailWidth]
	, [MaxCatalogItemLargeWidth]
	, [MaxCatalogItemCrossSellWidth]
	, [ShowSwatchInCategory]
	, [ShowAlternateImageInCategory]
	, [ActiveInd]
	, [SMTPServer]
	, [SMTPUserName]
	, [SMTPPassword]
	, [SMTPPort]
	, [SiteWideBottomJavascript]
	, [SiteWideTopJavascript]
	, [OrderReceiptAffiliateJavascript]
	, [SiteWideAnalyticsJavascript]
	, [GoogleAnalyticsCode]
	, [UPSUserName]
	, [UPSPassword]
	, [UPSKey]
	, [ShippingOriginZipCode]
	, [MasterPage]
	, [ShopByPriceMin]
	, [ShopByPriceMax]
	, [ShopByPriceIncrement]
	, [FedExAccountNumber]
	, [FedExMeterNumber]
	, [FedExProductionKey]
	, [FedExSecurityCode]
	, [FedExCSPKey]
	, [FedExCSPPassword]
	, [FedExClientProductId]
	, [FedExClientProductVersion]
	, [FedExDropoffType]
	, [FedExPackagingType]
	, [FedExUseDiscountRate]
	, [FedExAddInsurance]
	, [ShippingOriginAddress1]
	, [ShippingOriginAddress2]
	, [ShippingOriginCity]
	, [ShippingOriginStateCode]
	, [ShippingOriginCountryCode]
	, [ShippingOriginPhone]
	, [CurrencyTypeID]
	, [WeightUnit]
	, [DimensionUnit]
	, [EmailListLogin]
	, [EmailListPassword]
	, [EmailListDefaultList]
	, [ShippingTaxable]
	, [DefaultOrderStateID]
	, [DefaultReviewStatus]
	, [DefaultAnonymousProfileID]
	, [DefaultRegisteredProfileID]
	, [InclusiveTax]
	, [SeoDefaultProductTitle]
	, [SeoDefaultProductDescription]
	, [SeoDefaultProductKeyword]
	, [SeoDefaultCategoryTitle]
	, [SeoDefaultCategoryDescription]
	, [SeoDefaultCategoryKeyword]
	, [SeoDefaultContentTitle]
	, [SeoDefaultContentDescription]
	, [SeoDefaultContentKeyword]
	, [TimeZoneOffset]
	, [LocaleID]
	, [SplashCategoryID]
	, [SplashImageFile]
	, [MobileTheme]
	, [PersistentCartEnabled]
	, [DefaultProductReviewStateID]
	, [UseDynamicDisplayOrder]
	, [EnableAddressValidation]
	, [RequireValidatedAddress]
    FROM
	[dbo].[ZNodePortal]
    WHERE 
	 ([PortalID] = @PortalID AND @PortalID is not null)
	OR ([CompanyName] = @CompanyName AND @CompanyName is not null)
	OR ([StoreName] = @StoreName AND @StoreName is not null)
	OR ([LogoPath] = @LogoPath AND @LogoPath is not null)
	OR ([UseSSL] = @UseSSL AND @UseSSL is not null)
	OR ([AdminEmail] = @AdminEmail AND @AdminEmail is not null)
	OR ([SalesEmail] = @SalesEmail AND @SalesEmail is not null)
	OR ([CustomerServiceEmail] = @CustomerServiceEmail AND @CustomerServiceEmail is not null)
	OR ([SalesPhoneNumber] = @SalesPhoneNumber AND @SalesPhoneNumber is not null)
	OR ([CustomerServicePhoneNumber] = @CustomerServicePhoneNumber AND @CustomerServicePhoneNumber is not null)
	OR ([ImageNotAvailablePath] = @ImageNotAvailablePath AND @ImageNotAvailablePath is not null)
	OR ([MaxCatalogDisplayColumns] = @MaxCatalogDisplayColumns AND @MaxCatalogDisplayColumns is not null)
	OR ([MaxCatalogDisplayItems] = @MaxCatalogDisplayItems AND @MaxCatalogDisplayItems is not null)
	OR ([MaxCatalogCategoryDisplayThumbnails] = @MaxCatalogCategoryDisplayThumbnails AND @MaxCatalogCategoryDisplayThumbnails is not null)
	OR ([MaxCatalogItemSmallThumbnailWidth] = @MaxCatalogItemSmallThumbnailWidth AND @MaxCatalogItemSmallThumbnailWidth is not null)
	OR ([MaxCatalogItemSmallWidth] = @MaxCatalogItemSmallWidth AND @MaxCatalogItemSmallWidth is not null)
	OR ([MaxCatalogItemMediumWidth] = @MaxCatalogItemMediumWidth AND @MaxCatalogItemMediumWidth is not null)
	OR ([MaxCatalogItemThumbnailWidth] = @MaxCatalogItemThumbnailWidth AND @MaxCatalogItemThumbnailWidth is not null)
	OR ([MaxCatalogItemLargeWidth] = @MaxCatalogItemLargeWidth AND @MaxCatalogItemLargeWidth is not null)
	OR ([MaxCatalogItemCrossSellWidth] = @MaxCatalogItemCrossSellWidth AND @MaxCatalogItemCrossSellWidth is not null)
	OR ([ShowSwatchInCategory] = @ShowSwatchInCategory AND @ShowSwatchInCategory is not null)
	OR ([ShowAlternateImageInCategory] = @ShowAlternateImageInCategory AND @ShowAlternateImageInCategory is not null)
	OR ([ActiveInd] = @ActiveInd AND @ActiveInd is not null)
	OR ([SMTPServer] = @SMTPServer AND @SMTPServer is not null)
	OR ([SMTPUserName] = @SMTPUserName AND @SMTPUserName is not null)
	OR ([SMTPPassword] = @SMTPPassword AND @SMTPPassword is not null)
	OR ([SMTPPort] = @SMTPPort AND @SMTPPort is not null)
	OR ([UPSUserName] = @UPSUserName AND @UPSUserName is not null)
	OR ([UPSPassword] = @UPSPassword AND @UPSPassword is not null)
	OR ([UPSKey] = @UPSKey AND @UPSKey is not null)
	OR ([ShippingOriginZipCode] = @ShippingOriginZipCode AND @ShippingOriginZipCode is not null)
	OR ([MasterPage] = @MasterPage AND @MasterPage is not null)
	OR ([ShopByPriceMin] = @ShopByPriceMin AND @ShopByPriceMin is not null)
	OR ([ShopByPriceMax] = @ShopByPriceMax AND @ShopByPriceMax is not null)
	OR ([ShopByPriceIncrement] = @ShopByPriceIncrement AND @ShopByPriceIncrement is not null)
	OR ([FedExAccountNumber] = @FedExAccountNumber AND @FedExAccountNumber is not null)
	OR ([FedExMeterNumber] = @FedExMeterNumber AND @FedExMeterNumber is not null)
	OR ([FedExProductionKey] = @FedExProductionKey AND @FedExProductionKey is not null)
	OR ([FedExSecurityCode] = @FedExSecurityCode AND @FedExSecurityCode is not null)
	OR ([FedExCSPKey] = @FedExCSPKey AND @FedExCSPKey is not null)
	OR ([FedExCSPPassword] = @FedExCSPPassword AND @FedExCSPPassword is not null)
	OR ([FedExClientProductId] = @FedExClientProductId AND @FedExClientProductId is not null)
	OR ([FedExClientProductVersion] = @FedExClientProductVersion AND @FedExClientProductVersion is not null)
	OR ([FedExDropoffType] = @FedExDropoffType AND @FedExDropoffType is not null)
	OR ([FedExPackagingType] = @FedExPackagingType AND @FedExPackagingType is not null)
	OR ([FedExUseDiscountRate] = @FedExUseDiscountRate AND @FedExUseDiscountRate is not null)
	OR ([FedExAddInsurance] = @FedExAddInsurance AND @FedExAddInsurance is not null)
	OR ([ShippingOriginAddress1] = @ShippingOriginAddress1 AND @ShippingOriginAddress1 is not null)
	OR ([ShippingOriginAddress2] = @ShippingOriginAddress2 AND @ShippingOriginAddress2 is not null)
	OR ([ShippingOriginCity] = @ShippingOriginCity AND @ShippingOriginCity is not null)
	OR ([ShippingOriginStateCode] = @ShippingOriginStateCode AND @ShippingOriginStateCode is not null)
	OR ([ShippingOriginCountryCode] = @ShippingOriginCountryCode AND @ShippingOriginCountryCode is not null)
	OR ([ShippingOriginPhone] = @ShippingOriginPhone AND @ShippingOriginPhone is not null)
	OR ([CurrencyTypeID] = @CurrencyTypeID AND @CurrencyTypeID is not null)
	OR ([WeightUnit] = @WeightUnit AND @WeightUnit is not null)
	OR ([DimensionUnit] = @DimensionUnit AND @DimensionUnit is not null)
	OR ([EmailListLogin] = @EmailListLogin AND @EmailListLogin is not null)
	OR ([EmailListPassword] = @EmailListPassword AND @EmailListPassword is not null)
	OR ([EmailListDefaultList] = @EmailListDefaultList AND @EmailListDefaultList is not null)
	OR ([ShippingTaxable] = @ShippingTaxable AND @ShippingTaxable is not null)
	OR ([DefaultOrderStateID] = @DefaultOrderStateID AND @DefaultOrderStateID is not null)
	OR ([DefaultReviewStatus] = @DefaultReviewStatus AND @DefaultReviewStatus is not null)
	OR ([DefaultAnonymousProfileID] = @DefaultAnonymousProfileID AND @DefaultAnonymousProfileID is not null)
	OR ([DefaultRegisteredProfileID] = @DefaultRegisteredProfileID AND @DefaultRegisteredProfileID is not null)
	OR ([InclusiveTax] = @InclusiveTax AND @InclusiveTax is not null)
	OR ([SeoDefaultProductTitle] = @SeoDefaultProductTitle AND @SeoDefaultProductTitle is not null)
	OR ([SeoDefaultProductDescription] = @SeoDefaultProductDescription AND @SeoDefaultProductDescription is not null)
	OR ([SeoDefaultProductKeyword] = @SeoDefaultProductKeyword AND @SeoDefaultProductKeyword is not null)
	OR ([SeoDefaultCategoryTitle] = @SeoDefaultCategoryTitle AND @SeoDefaultCategoryTitle is not null)
	OR ([SeoDefaultCategoryDescription] = @SeoDefaultCategoryDescription AND @SeoDefaultCategoryDescription is not null)
	OR ([SeoDefaultCategoryKeyword] = @SeoDefaultCategoryKeyword AND @SeoDefaultCategoryKeyword is not null)
	OR ([SeoDefaultContentTitle] = @SeoDefaultContentTitle AND @SeoDefaultContentTitle is not null)
	OR ([SeoDefaultContentDescription] = @SeoDefaultContentDescription AND @SeoDefaultContentDescription is not null)
	OR ([SeoDefaultContentKeyword] = @SeoDefaultContentKeyword AND @SeoDefaultContentKeyword is not null)
	OR ([TimeZoneOffset] = @TimeZoneOffset AND @TimeZoneOffset is not null)
	OR ([LocaleID] = @LocaleID AND @LocaleID is not null)
	OR ([SplashCategoryID] = @SplashCategoryID AND @SplashCategoryID is not null)
	OR ([SplashImageFile] = @SplashImageFile AND @SplashImageFile is not null)
	OR ([MobileTheme] = @MobileTheme AND @MobileTheme is not null)
	OR ([PersistentCartEnabled] = @PersistentCartEnabled AND @PersistentCartEnabled is not null)
	OR ([DefaultProductReviewStateID] = @DefaultProductReviewStateID AND @DefaultProductReviewStateID is not null)
	OR ([UseDynamicDisplayOrder] = @UseDynamicDisplayOrder AND @UseDynamicDisplayOrder is not null)
	OR ([EnableAddressValidation] = @EnableAddressValidation AND @EnableAddressValidation is not null)
	OR ([RequireValidatedAddress] = @RequireValidatedAddress AND @RequireValidatedAddress is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProductType_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeProductType table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProductType_Get_List

AS


				
				SELECT
					[ProductTypeId],
					[Name],
					[Description],
					[DisplayOrder],
					[PortalID],
					[IsGiftCard],
					[Franchisable]
				FROM
					[dbo].[ZNodeProductType]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProductType_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeProductType table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProductType_Insert
(

	@ProductTypeId int    OUTPUT,

	@Name nvarchar (100)  ,

	@Description nvarchar (MAX)  ,

	@DisplayOrder int   ,

	@PortalID int   ,

	@IsGiftCard bit   ,

	@Franchisable bit   
)
AS


				
				INSERT INTO [dbo].[ZNodeProductType]
					(
					[Name]
					,[Description]
					,[DisplayOrder]
					,[PortalID]
					,[IsGiftCard]
					,[Franchisable]
					)
				VALUES
					(
					@Name
					,@Description
					,@DisplayOrder
					,@PortalID
					,@IsGiftCard
					,@Franchisable
					)
				
				-- Get the identity value
				SET @ProductTypeId = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductDetails]'
GO
SET ANSI_NULLS ON
GO

-- =============================================    
-- Upated date: Jan 17,2007    
-- Description: returns a product details (manufacturer name,product type)    
-- =============================================    
ALTER PROCEDURE [dbo].[ZNode_GetProductDetails](@PRODUCT_ID INT)    
AS    
BEGIN    
-- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
SELECT NAME AS 'PRODUCTTYPE NAME',
	   IsGiftCard,		
       [DESCRIPTION],
       PRODUCTTYPEID,
       (SELECT NAME
        FROM   ZNODEMANUFACTURER
        WHERE  MANUFACTURERID = (SELECT MANUFACTURERID
                                 FROM   ZNODEPRODUCT
                                 WHERE  PRODUCTID = @PRODUCT_ID)) AS 'MANUFACTURER NAME',
       (SELECT NAME
        FROM   ZNODESUPPLIER
        WHERE  SUPPLIERID = (SELECT SUPPLIERID
                             FROM   ZNODEPRODUCT
                             WHERE  PRODUCTID = @PRODUCT_ID)) AS 'SUPPLIER NAME'
FROM   ZNODEPRODUCTTYPE
WHERE  PRODUCTTYPEID = (SELECT PRODUCTTYPEID
                        FROM   ZNODEPRODUCT
                        WHERE  PRODUCTID = @PRODUCT_ID);  
                        
END;  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_UpsertProduct]'
GO
  
  
ALTER PROCEDURE [dbo].[ZNode_UpsertProduct]  
(
 @ProductID int = 0,  
 @Name nvarchar(MAX) = null,  
 @ShortDescription nvarchar(MAX) = null,  
 @Description nvarchar(MAX) = null,  
 @FeaturesDesc nvarchar(MAX) = null,  
 @ProductNum nvarchar(100) = null,  
 @ProductTypeID int = null,  
 @ExpirationPeriod int = null,  
 @ExpirationFrequency int = null,  
 @RetailPrice money = null,  
 @SalePrice money = null,  
 @WholesalePrice money = null,  
 @ImageFile nvarchar(MAX) = null,  
 @ImageAltTag nvarchar(MAX) = null,  
 @Weight decimal(18, 2) = null,  
 @Length decimal(18, 2) = null,  
 @Width decimal(18, 2) = null,  
 @Height decimal(18, 2) = null,  
 @BeginActiveDate datetime = null,  
 @EndActiveDate datetime = null,  
 @DisplayOrder int = null,  
 @ActiveInd bit = null,  
 @CallForPricing bit = null,  
 @HomepageSpecial bit = 0,  
 @CategorySpecial bit = 0,  
 @InventoryDisplay tinyint = null,  
 @Keywords nvarchar(MAX) = null,  
 @ManufacturerID int = null,  
 @AdditionalInfoLink nvarchar(MAX) = null,  
 @AdditionalInfoLinkLabel nvarchar(MAX) = null,  
 @ShippingRuleTypeID int = null,  
 @ShippingRate money = null,  
 @SEOTitle nvarchar(MAX) = null,  
 @SEOKeywords nvarchar(MAX) = null,  
 @SEODescription nvarchar(MAX) = null,  
 @Custom1 nvarchar(MAX) = null,  
 @Custom2 nvarchar(MAX) = null,  
 @Custom3 nvarchar(MAX) = null,  
 @ShipEachItemSeparately bit = null,  
 @AllowBackOrder bit = null,  
 @BackOrderMsg nvarchar(MAX) = null,  
 @DropShipInd bit = null,  
 @DropShipEmailID nvarchar(MAX) = null,  
 @Specifications nvarchar(MAX) = null ,  
 @AdditionalInformation nvarchar(MAX) = null,  
 @InStockMsg nvarchar(MAX) =null ,  
 @OutOfStockMsg nvarchar(MAX) =null ,  
 @TrackInventoryInd bit = null ,  
 @DownloadLink nvarchar(MAX) = null,  
 @FreeShippingInd bit = null,  
 @NewProductInd bit = null,  
 @SEOURL nvarchar(100) = null,  
 @MaxQty int = null,  
 @ShipSeparately bit = null,  
 @FeaturedInd bit = null,  
 @WebServiceDownloadDte datetime = null,  
 @UpdateDte datetime = null,  
 @SupplierID int = null,  
 @RecurringBillingInd bit = null,  
 @RecurringBillingInstallmentInd bit = null,  
 @RecurringBillingPeriod nvarchar (50)  =null,  
 @RecurringBillingFrequency nvarchar(MAX) = null ,  
 @RecurringBillingTotalCycles int = null,  
 @RecurringBillingInitialAmount money = null,  
 @TaxClassID int =null,  
 @MinQty int  = null,  
 @ReviewStateID int = null,  
 @AffiliateUrl varchar (512) = null,  
 @IsShippable bit = null ,  
 @AccountID int = null,  
 @PortalID int = null ,
 @Franchisable bit =0,
 @SKU nvarchar(100) = null,
 @QuantityOnHand int = null
)  
AS   
BEGIN  
DECLARE @ExistingReviewStateID INT  
								
 IF(Exists(SELECT ProductID From ZNodeProduct WHERE ProductId = @ProductID))  
  BEGIN  
   SELECT @ExistingReviewStateID = ReviewStateID FROM ZNODEPRODUCT WHERE ProductId=@ProductID;
IF(@ReviewStateID  IS NULL)
BEGIN
SET @ExistingReviewStateID = @ExistingReviewStateID
END
ELSE
BEGIN
SET @ExistingReviewStateID = @ReviewStateID
END
   Exec ZNode_NT_ZNodeProduct_Update    
   @ProductID ,  
   @Name ,  
   @ShortDescription,  
   @Description,  
   @FeaturesDesc ,  
   @ProductNum,  
   @ProductTypeID,  
   @RetailPrice,  
   @SalePrice,  
   @WholesalePrice,  
   @ImageFile ,  
   @ImageAltTag,  
   @Weight,  
   @Length,  
   @Width,  
   @Height ,  
   @BeginActiveDate,  
   @EndActiveDate ,  
   @DisplayOrder,  
   @ActiveInd ,  
   @CallForPricing,  
   @HomepageSpecial,  
   @CategorySpecial,  
   @InventoryDisplay,  
   @Keywords,  
   @ManufacturerID,  
   @AdditionalInfoLink,  
   @AdditionalInfoLinkLabel,  
   @ShippingRuleTypeID,  
   @ShippingRate,  
   @SEOTitle ,  
   @SEOKeywords ,  
   @SEODescription ,  
   @Custom1,   
   @Custom2 ,  
   @Custom3,  
   @ShipEachItemSeparately,  
   @AllowBackOrder,  
   @BackOrderMsg,  
   @DropShipInd ,  
   @DropShipEmailID,  
   @Specifications,  
   @AdditionalInformation,  
   @InStockMsg ,  
   @OutOfStockMsg,  
   @TrackInventoryInd ,  
   @DownloadLink ,  
   @FreeShippingInd,  
   @NewProductInd,  
   @SEOURL ,  
   @MaxQty ,  
   @ShipSeparately ,  
   @FeaturedInd ,  
   @WebServiceDownloadDte,  
   @UpdateDte,  
   @SupplierID ,  
   @RecurringBillingInd ,  
   @RecurringBillingInstallmentInd ,  
   @RecurringBillingPeriod ,  
   @RecurringBillingFrequency,  
   @RecurringBillingTotalCycles ,  
   @RecurringBillingInitialAmount,  
   @TaxClassID,   
   @MinQty, 
   @ExistingReviewStateID,  					
   @AffiliateUrl ,  
   @IsShippable,  
   @AccountID,  
   @PortalID,  
   @Franchisable,
   @ExpirationPeriod,
   @ExpirationFrequency    
   
        
  END   
 ELSE  
  BEGIN   
   EXEC ZNode_NT_ZNodeSKUInventory_Insert null,@SKU,@QuantityOnHand,null

   Exec ZNode_NT_ZNodeProduct_Insert    
   @ProductID out,  
   @Name ,  
   @ShortDescription,  
   @Description,  
   @FeaturesDesc ,  
   @ProductNum,  
   @ProductTypeID,  
   @RetailPrice,  
   @SalePrice,  
   @WholesalePrice,  
   @ImageFile ,  
   @ImageAltTag,  
   @Weight,  
   @Length,  
   @Width,  
   @Height ,  
   @BeginActiveDate,  
   @EndActiveDate ,  
   @DisplayOrder,  
   @ActiveInd ,  
   @CallForPricing,  
   @HomepageSpecial,  
   @CategorySpecial,  
   @InventoryDisplay,  
   @Keywords,  
   @ManufacturerID,  
   @AdditionalInfoLink,  
   @AdditionalInfoLinkLabel,  
   @ShippingRuleTypeID,  
   @ShippingRate,  
   @SEOTitle ,  
   @SEOKeywords ,  
   @SEODescription ,  
   @Custom1,  
   @Custom2 ,  
   @Custom3,  
   @ShipEachItemSeparately,  
   @AllowBackOrder,  
   @BackOrderMsg,  
   @DropShipInd ,  
   @DropShipEmailID,  
   @Specifications,  
   @AdditionalInformation,  
   @InStockMsg ,  
   @OutOfStockMsg,  
   @TrackInventoryInd ,  
   @DownloadLink ,  
   @FreeShippingInd,  
   @NewProductInd,  
   @SEOURL ,  
   @MaxQty ,  
   @ShipSeparately ,  
   @FeaturedInd ,  
   @WebServiceDownloadDte,  
   @UpdateDte,  
   @SupplierID ,  
   @RecurringBillingInd ,  
   @RecurringBillingInstallmentInd ,  
   @RecurringBillingPeriod ,  
   @RecurringBillingFrequency,  
   @RecurringBillingTotalCycles ,  
   @RecurringBillingInitialAmount,  
   @TaxClassID,   
   @MinQty,  
   @ReviewStateID,  
   @AffiliateUrl ,  
   @IsShippable,  
   @AccountID,  
   @PortalID,  
   @Franchisable,  
   @ExpirationPeriod,
   @ExpirationFrequency
   
      EXEC	[dbo].[ZNode_NT_ZNodeSKU_Insert]
		@SKUID = null,
		@ProductID = @ProductID,
		@SKU = @SKU,
		@SupplierID = NULL,
		@Note = NULL,
		@WeightAdditional = NULL,
		@SKUPicturePath = NULL,
		@ImageAltTag = NULL,
		@DisplayOrder = NULL,
		@RetailPriceOverride = NULL,
		@SalePriceOverride = NULL,
		@WholesalePriceOverride = NULL,
		@RecurringBillingPeriod = NULL,
		@RecurringBillingFrequency = NULL,
		@RecurringBillingTotalCycles = NULL,
		@RecurringBillingInitialAmount = NULL,
		@ActiveInd = 1,
		@Custom1 = NULL,
		@Custom2 = NULL,
		@Custom3 = NULL,
		@WebServiceDownloadDte = NULL,
		@UpdateDte = null,
		@ExternalProductID = NULL,
		@ExternalProductAPIID = NULL

     
  END  
END  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetVendorOrderByOrderId]'
GO

  
CREATE PROCEDURE [dbo].[ZNode_GetVendorOrderByOrderId](                
@ORDERID VARCHAR(100),
@LoggedAccountId nvarchar(MAX) = null)
            
AS                
BEGIN                
-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM                
-- INTERFERING WITH SELECT STATEMENTS.                
SET NOCOUNT ON;              
      
-- Select Query --              
SELECT DISTINCT         
  ZNodeOrder.[OrderID] as OrderId,   
  ZNodeOrder.PortalId as PortalId,           
  CONVERT(varchar,ZNodeOrder.[OrderDate],101) as OrderDate,            
  IsNull(ZNodeAccount.Externalaccountno, '') as AccountNumber,            
  IsNull(ZNodeOrder.[OrderStateID],'') as 'Order Status',            
  ZNodeOrder.[ShippingID] as 'Shipping Type',            
  ZNodeOrder.[PaymentTypeId] as 'Payment Type',          
  IsNull(ZNodeOrder.CardTransactionID,'') as TransactionKey,          
  IsNull(ZNodeOrder.ShipFirstName, '') as ShippingFirstName,                
  IsNull(ZNodeOrder.ShipLastName, '') as ShippingLastName,                
  IsNull(ZNodeOrder.ShipCompanyName, '') as ShippingCompanyName,                
  IsNull(ZNodeOrder.ShipStreet, '') as ShippingStreet,                
  IsNull(ZNodeOrder.ShipStreet1, '') as ShippingStreet1,                
  IsNull(ZNodeOrder.ShipCity, '') as ShippingCity,                
  IsNull(ZNodeOrder.ShipStateCode, '') as ShippingStateCode,                
  IsNull(ZNodeOrder.ShipPostalCode, '') as ShippingPostalCode,                
  IsNull(ZNodeOrder.ShipCountry, '') as ShippingCountry,                
  IsNull(ZNodeOrder.ShipPhoneNumber, '') as ShippingPhoneNumber,                
  IsNull(ZNodeOrder.ShipEmailID, '') as ShipEmailID,                
  IsNull(ZNodeOrder.BillingFirstName, '') as BillingFirstName,                
  IsNull(ZNodeOrder.BillingLastName, '') as BillingLastName,                
  IsNull(ZNodeOrder.BillingCompanyName, '') as BillingCompanyName,                
  IsNull(ZNodeOrder.BillingStreet, '') as BillingStreet,                
  IsNull(ZNodeOrder.BillingStreet1, '') as BillingStreet1,                
  IsNull(ZNodeOrder.BillingCity, '') as BillingCity,                
  IsNull(ZNodeOrder.BillingStateCode, '') as BillingStateCode,                
  IsNull(ZNodeOrder.BillingPostalCode, '') as BillingPostalCode,                
  IsNull(ZNodeOrder.BillingCountry, '') as BillingCountry,                
  IsNull(ZNodeOrder.BillingPhoneNumber, '') as BillingPhoneNumber,            
  IsNull(ZNodeOrder.BillingEmailId, '') as BillingEmailId,      
  IsNull(ZNodeOrder.ShippingCost, 0) as ShippingCost,                
  IsNull(ZNodeOrder.SalesTax, 0) as SalesTax,    
  IsNull(ZNodeOrder.VAT, 0) as VAT,    
  IsNull(ZNodeOrder.GST, 0) as GST,    
  IsNull(ZNodeOrder.PST, 0) as PST,    
  IsNull(ZNodeOrder.HST, 0) as HST,    
  IsNull(ZNodeOrder.Total, 0) as Total,                
  IsNull(ZNodeOrder.SubTotal, 0) as SubTotal,          
  ZNodeOrder.[Custom1],       
  ZNodeOrder.[Custom2],      
  ZNodeOrder.[Custom3],        
  ZNodeOrder.[AdditionalInstructions]      
FROM                  
  ZNodeAccount, ZNodeOrder
  INNER JOIN ZnodeOrderLineItem ON ZNodeOrder.OrderId = ZNodeOrderLineItem.OrderID  
  INNER JOIN ZNodeSKU ON ZNodeOrderLineItem.SKU = ZNodeSKU.SKU  
  INNER JOIN ZNodeProduct ON ZNodeSKU.ProductID = ZNodeProduct.ProductID AND ZNodeProduct.ProductNum = ZNodeOrderLineItem.ProductNum  
WHERE
ZnodeProduct.AccountID = @LoggedAccountId AND
ZNodeOrder.AccountId = ZNodeAccount.AccountId AND      
ZNodeOrder.OrderID >= @ORDERID
END;   
  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNODE_WS_GetAccountsByFilter_XML]'
GO


ALTER PROCEDURE [dbo].[ZNODE_WS_GetAccountsByFilter_XML]   
(    
 @Filter int = NULL,    
 @BeginDate DateTime = NULL,    
 @EndDate DateTime = NULL,    
 @FirstName Varchar(1000) = NULL,    
 @LastName Varchar(1000) = NULL,    
 @AccountNumber Varchar(1000) = NULL,    
 @CompanyName Varchar(1000) = NULL,    
 @ZipCode Varchar(1000) = NULL,    
 @Phone Varchar(1000) = NULL,    
 @PortalID Varchar(1000) = NULL        
)     
AS            
 BEGIN            
  -- SET NOCOUNT ON added to prevent extra result sets from        
    -- interfering with SELECT statements.        
    SET NOCOUNT ON;
    WITH   Account(AccountId, 
			ParentAccountId, 
			UserId, 
			ExternalAccountNo, 
			CompanyName, 
			AccountTypeID, 
			ProfileId, 
			AccountProfileCode, 
			SubAccountLimit, 
			BillingFirstName, 
			BillingLastName, 
			BillingCompanyName, 
			BillingStreet, 
			BillingStreet1, 
			BillingCity, 
			BillingStateCode, 
			BillingPostalCode, 
			BillingCountryCode, 
			BillingPhoneNumber, 
			BillingEmailId, 
			ShipFirstName, 
			ShipLastName, 
			ShipCompanyName, 
			ShipStreet, 
			ShipStreet1, 
			ShipCity, 
			ShipStateCode, 
			ShipPostalCode, 
			ShipCountryCode, 
			ShipEmailID, 
			ShipPhoneNumber, 
			[Description], 
			CreateUser, 
			CreateDte, 
			UpdateUser, 
			UpdateDte, 
			ActiveInd, 
			Website, 
			[Source], 
			Custom1, 
			Custom2, 
			Custom3, 
			EmailOptIn, 
			WebServiceDownloadDte, 
			Filter)
    AS     (SELECT ZA.[AccountID],
                   ZA.[ParentAccountId],
                   ZA.[UserID],
                   ZA.[ExternalAccountNo],
                   ZA.[CompanyName],
                   ZA.[AccountTypeID],
                   ZA.[ProfileId],
                   ZA.[AccountProfileCode],                   
                   ZA.[SubAccountLimit],
                   A.FirstName,
                   A.LastName,
                   A.CompanyName,
                   A.Street,
                   A.Street1,
                   A.City,
                   A.StateCode,
                   A.PostalCode,
                   A.CountryCode,
                   A.Phonenumber,
                   ZA.Email,
                   AD.FirstName,
                   AD.LastName,
                   AD.CompanyName,
                   AD.Street,
                   AD.Street1,
                   AD.City,
                   AD.StateCode,
                   AD.PostalCode,
                   AD.CountryCode,
                   ZA.Email,
                   AD.Phonenumber,                   
                   ZA.[Description],
                   ZA.[CreateUser],
                   ZA.[CreateDte],
                   ZA.[UpdateUser],
                   ZA.[UpdateDte],
                   ZA.[ActiveInd],
                   ZA.[Website],
                   ZA.[Source],
                   ZA.[Custom1],
                   ZA.[Custom2],
                   ZA.[Custom3],
                   ZA.[EmailOptIn],
                   [WebServiceDownloadDte],
                   CASE 
						WHEN WebServiceDownloadDte IS NULL AND @Filter = 1 THEN 1 
						WHEN WebServiceDownloadDte < UpdateDte AND @Filter = 2 THEN 2 
						WHEN @Filter = 0 THEN 0 
						WHEN @Filter IS NULL THEN NULL 
						END AS 'Filter'
            FROM   ZNodeAccount AS ZA 
					LEFT OUTER JOIN ZNodeAddress AS A ON ZA.AccountID = A.AccountID AND IsDefaultBilling = 1
					LEFT OUTER JOIN ZNodeAddress AS AD ON ZA.AccountID = AD.AccountID AND AD.IsDefaultShipping = 1
            WHERE  ((CreateDte BETWEEN @BeginDate AND @EndDate)
                    OR (@BeginDate IS NULL
                        OR @EndDate IS NULL))
                   AND (IsNull(A.FirstName, '') LIKE @FirstName + '%'
                        OR IsNull(AD.FirstName, '') LIKE @FirstName + '%'
                        OR @FirstName IS NULL)
                   AND (IsNull(A.LastName, '') LIKE @LastName + '%'
                        OR IsNull(AD.LastName, '') LIKE @LastName + '%'
                        OR @LastName IS NULL)
                   AND (IsNull(ZA.CompanyName, '') LIKE @CompanyName + '%'
                        OR IsNull(A.CompanyName, '') LIKE @CompanyName + '%'
                        OR IsNull(AD.CompanyName, '') LIKE @CompanyName + '%'
                        OR @CompanyName IS NULL)
                   AND (IsNull(ExternalAccountNo, '') LIKE @AccountNumber + '%'
                        OR @AccountNumber IS NULL)
                   AND (IsNull(A.PostalCode, '') LIKE @ZipCode + '%'
                        OR IsNull(AD.PostalCode, '') LIKE @ZipCode + '%'
                        OR @ZipCode IS NULL)
                   AND (IsNull(A.Phonenumber, '') LIKE @Phone + '%'
                        OR IsNull(AD.Phonenumber, '') LIKE @Phone + '%'
                        OR @Phone IS NULL)
                   AND ZA.ACCOUNTID IN (SELECT ACCOUNTID
                                        FROM   ZNODEACCOUNTPROFILE, ZNODEPORTALPROFILE
                                        WHERE  ZNODEPORTALPROFILE.PROFILEID = ZNODEACCOUNTPROFILE.ProfileID
                                               AND ZNODEPORTALPROFILE.PORTALID = @PortalID
                                               OR @PortalID IS NULL))
    SELECT *
    FROM   Account
    WHERE  Filter = @Filter
    FOR    XML AUTO, TYPE, ELEMENTS;
END      


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_WS_GetProductsByFilter_XML]'
GO


ALTER PROCEDURE [dbo].[ZNode_WS_GetProductsByFilter_XML]
(
@Filter int = 0,
@PortalID int = null,
@LocaleId int = null)
AS            
 BEGIN            
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
	CREATE TABLE #ProductList
    ( ProductId INT,
      SKU NVARCHAR(1000),
      QuantityOnHand INT,
      ReorderLevel INT
     )
      
	CREATE UNIQUE INDEX IX_ProductList ON #ProductList (ProductId) WITH (IGNORE_DUP_KEY = ON);
    
	INSERT INTO #ProductList
		SELECT sku.PRODUCTID, inventory.SKU, inventory.QuantityOnHand,inventory.ReOrderLevel FROM ZNodeSKU sku 
		INNER JOIN ZNodeSKUInventory inventory ON sku.SKU = inventory.SKU
        INNER JOIN ZNodeProductCategory ZPC ON sku.ProductID = ZPC.ProductID
		INNER JOIN ZNodeCategoryNode ZCN ON ZCN.CategoryID = ZPC.CategoryID
		INNER JOIN ZNodePortalCatalog ZPOC ON ZPOC.CatalogID = ZCN.CatalogID
		WHERE 
			(ZPOC.PortalId=@PortalID OR @PortalID IS NULL OR @PortalID = 0)
			AND (ZPOC.LocaleID = @LocaleId OR @LocaleId IS NULL);
      
	 WITH Product (ProductID, SKU , QuantityOnHand, ReorderLevel, StoreName, LocaleCode, Filter)
	   AS
	   (
			SELECT
				Product.[ProductId],
				sku.SKU,
				sku.[QuantityOnHand],
				sku.[ReorderLevel],
				[dbo].[FN_ConcatStoreName](Product.ProductID, @PortalID, null),
				[dbo].[FN_ConcatLocaleCode](Product.ProductID, @LocaleId, null),
				Filter = (CASE WHEN product.WebServiceDownloadDte IS NULL AND @Filter = 1 THEN 1
							 WHEN product.WebServiceDownloadDte < product.UpdateDte AND @Filter = 2 THEN 2
							 WHEN @Filter = 0 THEN 0
							 WHEN @Filter IS NULL THEN NULL
						   End)
			FROM ZNodeProduct Product  
			INNER JOIN #ProductList sku ON Product.ProductID = sku.ProductID
	 )    
	 
	 SELECT 
		ZProd.[ProductID],        
		[Name],        
		[ShortDescription],        
		[Description],        
		[FeaturesDesc],        
		ZProd.[ProductNum],        
		[ProductTypeId],        
		[RetailPrice],        
		[SalePrice],        
		[WholesalePrice],        
		[ImageFile],        
		[Weight],        
		[Length],        
		[Width],        
		[Height],        
		ZProd.[ActiveInd],        
		ZProd.[DisplayOrder],        
		[CallForPricing],        
		[HomepageSpecial],        
		[CategorySpecial],        
		[InventoryDisplay],        
		[Keywords],        
		[ManufacturerID],        
		[AdditionalInfoLink],        
		[AdditionalInfoLinkLabel],        
		[ShippingRuleTypeId],        
		[SEOTitle],        
		[SEOKeywords],        
		[SEODescription],        
		ZProd.[Custom1],        
		ZProd.[Custom2],        
		ZProd.[Custom3],        
		[ShipEachItemSeparately],
		prod.[SKU] AS Sku,
		[QuantityOnHand],        
		[AllowBackOrder],        
		[BackOrderMsg],        
		[DropShipInd],        
		[DropShipEmailId],        
		[Specifications],        
		[AdditionalInformation],        
		[InStockMsg],        
		[OutOfStockMsg],        
		[TrackInventoryInd],        
		[DownloadLink],        
		[FreeShippingInd],  
		[NewProductInd],        
		[SEOURL],        
		[MaxQty],        
		[ShipSeparately],        
		[FeaturedInd],        
		[Filter],
		prod.StoreName,
		prod.LocaleCode,
		prod.ReOrderLevel,
		ZProd.PortalID AS 'PortalId',
		ZProd.AccountID,
		ZProd.ReviewStateID,
		ZProd.Franchisable,
		ZProd.ExpirationPeriod,
		ZProd.ExpirationFrequency
	 FROM ZNodeProduct ZProd
	 INNER JOIN Product prod ON prod.ProductID = ZProd.ProductID
	 WHERE prod.Filter = @Filter  
	 FOR XML PATH('Product') , TYPE , ELEMENTS
 END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_Reports]'
GO


ALTER PROCEDURE [dbo].[ZNode_Reports]  
    (  
      @Filter VARCHAR(MAX) = NULL,  
      @FromDate VARCHAR(MAX) = NULL,  
      @ToDate VARCHAR(MAX) = NULL,  
      @SupplierID VARCHAR(MAX) = NULL,  
      @Custom1 VARCHAR(MAX) = '',  
      @Custom2 VARCHAR(MAX) = ''  
    )  
AS   
    BEGIN                                        
        SET NOCOUNT ON ;                                        
        -- Report Name  
          
        DECLARE @Orders VARCHAR(100)= 'Orders'  
          
        DECLARE @Picklist VARCHAR(100)= 'Picklist'  
          
        DECLARE @EmailOptInCustomer VARCHAR(100)= 'EmailOptInCustomer'  
          
        DECLARE @Accounts VARCHAR(100)= 'Accounts'  
          
        DECLARE @ActivityLog VARCHAR(100)= 'ActivityLog'  
          
        DECLARE @ReOrder VARCHAR(100)= 'ReOrder'  
          
        DECLARE @ServiceRequest VARCHAR(100)= 'ServiceRequest'  
          
        DECLARE @SupplierList VARCHAR(100)= 'SupplierList'  
          
        -- Coupon Usage  
        DECLARE @CouponUsage VARCHAR(100)= 'CouponUsage'  
        DECLARE @CouponUsageByDay VARCHAR(100)= 'CouponUsageByDay'  
        DECLARE @CouponUsageByWeek VARCHAR(100)= 'CouponUsageByWeek'  
        DECLARE @CouponUsageByMonth VARCHAR(100)= 'CouponUsageByMonth'  
        DECLARE @CouponUsageByQuarter VARCHAR(100)= 'CouponUsageByQuarter'  
        DECLARE @CouponUsageByYear VARCHAR(100)= 'CouponUsageByYear'  
          
        -- Sales Tax  
        DECLARE @SalesTax VARCHAR(100)= 'SalesTax'  
        DECLARE @SalesTaxByMonth VARCHAR(100)= 'SalesTaxByMonth'  
        DECLARE @SalesTaxByQuarter VARCHAR(100)= 'SalesTaxByQuarter'  
          
        -- Frequent Customer  
        DECLARE @FrequentCustomer VARCHAR(100)= 'FrequentCustomer'  
        DECLARE @FrequentCustomerByDay VARCHAR(100)= 'FrequentCustomerByDay'  
        DECLARE @FrequentCustomerByWeek VARCHAR(100)= 'FrequentCustomerByWeek'  
        DECLARE @FrequentCustomerByMonth VARCHAR(100)= 'FrequentCustomerByMonth'  
        DECLARE @FrequentCustomerByQuarter VARCHAR(100)= 'FrequentCustomerByQuarter'  
        DECLARE @FrequentCustomerByYear VARCHAR(100)= 'FrequentCustomerByYear'  
          
        DECLARE @TopSpendingCustomer VARCHAR(100)= 'TopSpendingCustomer'  
        DECLARE @TopSpendingCustomerByDay VARCHAR(100)= 'TopSpendingCustomerByDay'  
        DECLARE @TopSpendingCustomerByWeek VARCHAR(100)= 'TopSpendingCustomerByWeek'  
        DECLARE @TopSpendingCustomerByMonth VARCHAR(100)= 'TopSpendingCustomerByMonth'  
        DECLARE @TopSpendingCustomerByQuarter VARCHAR(100)= 'TopSpendingCustomerByQuarter'  
        DECLARE @TopSpendingCustomerByYear VARCHAR(100)= 'TopSpendingCustomerByYear'  
          
        DECLARE @TopEarningProduct VARCHAR(100)= 'TopEarningProduct'  
        DECLARE @TopEarningProductByDay VARCHAR(100)= 'TopEarningProductByDay'  
        DECLARE @TopEarningProductByWeek VARCHAR(100)= 'TopEarningProductByWeek'  
        DECLARE @TopEarningProductByMonth VARCHAR(100)= 'TopEarningProductByMonth'  
        DECLARE @TopEarningProductByQuarter VARCHAR(100)= 'TopEarningProductByQuarter'  
        DECLARE @TopEarningProductByYear VARCHAR(100)= 'TopEarningProductByYear'  
          
        -- Best Seller   
        DECLARE @BestSeller VARCHAR(100)= 'BestSeller'  
        DECLARE @BestSellerByDay VARCHAR(100)= 'BestSellerByDay'  
        DECLARE @BestSellerByWeek VARCHAR(100)= 'BestSellerByWeek'  
        DECLARE @BestSellerByMonth VARCHAR(100)= 'BestSellerByMonth'  
        DECLARE @BestSellerByQuarter VARCHAR(100)= 'BestSellerByQuarter'  
        DECLARE @BestSellerByYear VARCHAR(100)= 'BestSellerByYear'  
          
        -- Affiliate Order   
        DECLARE @AffiliateOrder VARCHAR(100)= 'AffiliateOrder'  
        DECLARE @AffiliateOrderByDay VARCHAR(100)= 'AffiliateOrderByDay'  
        DECLARE @AffiliateOrderByWeek VARCHAR(100)= 'AffiliateOrderByWeek'  
        DECLARE @AffiliateOrderByMonth VARCHAR(100)= 'AffiliateOrderByMonth'  
        DECLARE @AffiliateOrderByQuarter VARCHAR(100)= 'AffiliateOrderByQuarter'  
        DECLARE @AffiliateOrderByYear VARCHAR(100)= 'AffiliateOrderByYear' 
        DECLARE @VendorRevenue VARCHAR(100)= 'VendorRevenue' 
        DECLARE @VendorProductRevenue VARCHAR(100)= 'VendorProductRevenue'
          
        -- Popular Search    
        DECLARE @PopularSearch VARCHAR(100)= 'PopularSearch'  
        DECLARE @PopularSearchByDay VARCHAR(100)= 'PopularSearchByDay'  
        DECLARE @PopularSearchByWeek VARCHAR(100)= 'PopularSearchByWeek'  
        DECLARE @PopularSearchByMonth VARCHAR(100)= 'PopularSearchByMonth'  
        DECLARE @PopularSearchByQuarter VARCHAR(100)= 'PopularSearchByQuarter'  
        DECLARE @PopularSearchByYear VARCHAR(100)= 'PopularSearchByYear'  
        
        -- Products sold on vendor sites.
        DECLARE @ProductSoldOnVendorSites VARCHAR(100)= 'ProductSoldOnVendorSites'  
        
                  
        DECLARE @Diff int ;                                        
        DECLARE @Diff2 int ;                                        
        DECLARE @StartDate DATETIME ;                                        
        DECLARE @Year NVARCHAR(MAX) ;  
        DECLARE @FinalDate NVARCHAR(MAX) ;       
        DECLARE @CompareDate DATETIME ;    
        DECLARE @CompareMonthDate DATETIME ;                                      
        DECLARE @CompareFinalDate NVARCHAR(MAX) ;  
        DECLARE @TodayDate DATETIME ;  
  
        SELECT  @TodayDate = GetDate() ;                                        
              
        SET @Diff = ( SELECT    DATEPART(dayofyear, @TodayDate)  
                    ) - ( SELECT    DATEPART(day, @TodayDate)  
                        ) ;                                          
        SET @Diff2 = @Diff + ( SELECT   DATEPART(day, @TodayDate)  
                             ) - 1 ;                                          
        SET @StartDate = ( SELECT   DateAdd(dd, -@Diff2, @TodayDate)  
                         ) ;                                          
        SET @FinalDate = ( SELECT   CONVERT(Varchar(10), @StartDate, 101)  
                         ) ;                                          
        SET @CompareDate = ( SELECT DateAdd(dd, 1, @TodayDate)  
                           ) ;                                          
        SET @CompareFinalDate = ( SELECT    CONVERT(Varchar(10), @CompareDate, 101)  
                                ) ;        
        SET @Year = YEAR(@TodayDate) ;                      
  
  
  IF @ToDate IS NOT NULL  
  BEGIN  
   SET @ToDate = (SELECT DateAdd(dd, 1, @ToDate));  
  END;     
    
        IF @Filter = '0'   
            BEGIN                            
                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate  
            END  
   
        IF @Filter = '1'   
            BEGIN                                                          
                SELECT  Prod.ProductId,  
                        Prod.[Name],  
                        Prod.ProductNum,  
                        Prod.RetailPrice,  
                        Prod.SalePrice,  
                        SUM(ord.Quantity)  
                FROM    ZNodeProduct Prod  
                        INNER JOIN ZNodeOrderLineItem ord ON Prod.ProductNum = ord.ProductNum  
                WHERE   LEN(ParentOrderLineItemID) = 0  
                GROUP BY Prod.ProductID,  
                        Prod.[Name],  
                        Prod.ProductNum,  
                        Prod.RetailPrice,  
                        Prod.SalePrice,  
                        Ord.ProductNum  
                HAVING  SUM(Quantity) > 0  
                ORDER BY SUM(ord.Quantity) DESC ;                            
    
                SET @Diff = ( SELECT    DATEPART(day, @TodayDate)  
                            ) - 1 ;                                        
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                          
  
                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate  
    
            END  
   
        IF @Filter = '2'   
            BEGIN  
                SET @StartDate = ( SELECT   DateAdd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareDate = ( SELECT DateAdd(dd, 1, @TodayDate)  
                                   ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
                                          
                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate  
            END  
  
        IF @Filter = '3'   
            BEGIN  
                SET @StartDate = @TodayDate ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                                                               
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                          
    
                EXEC ZNode_ReportsMain @FinalDate, @CompareFinalDate  
            END  
   
        IF @Filter = @Orders   
            BEGIN  
  -- Custom 1: Portal Id  
  -- Custom 2: Order State  
                EXEC ZNode_ReportsCustom @FromDate, @ToDate, @Custom1,  
                    @Custom2  
            END  
   
        IF @Filter = @Picklist   
            BEGIN  
                EXEC ZNode_ReportsPicklist @Custom1  
            END  
   
        IF @Filter = @EmailOptInCustomer   
            BEGIN  
                EXEC dbo.ZNode_ReportsOptIn @Custom1  
            END  
   
        IF @Filter = @FrequentCustomer   
            BEGIN  
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder  
    SET @CompareDate = ( SELECT DateAdd(dd, 1, @CompareDate)) ;  
                SET @CompareFinalDate = ( SELECT Convert(varchar(10), @CompareDate, 101)) ;   
                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END  
  
  
        IF @Filter = @FrequentCustomerByDay   
            BEGIN  
                SET @StartDate = @TodayDate ;                 
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;       
   
                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
    
            END  
   
        IF @Filter = @FrequentCustomerByWeek   
            BEGIN                       
                SET @StartDate = ( SELECT   DateAdd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                             
    EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END  
              
        IF @Filter = @FrequentCustomerByMonth   
            BEGIN                                   
                SET @Diff = ( SELECT    DatePart(day, @TodayDate)  
                            ) - 1 ;                                          
                SET @StartDate = ( SELECT   DateAdd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                    
  
                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END  
   
        IF @Filter = @FrequentCustomerByQuarter   
            BEGIN                                   
               
				SET @Diff = (Select DatePart(QQ, GetDate())) ;  
				Set @FinalDate = Convert(datetime, 
						CASE WHEN @Diff = 1 THEN '01/01/' + @Year
							 WHEN @Diff = 2 THEN '04/01/' + @Year
							 WHEN @Diff = 3 THEN '07/01/' + @Year
							 WHEN @Diff = 4 THEN '10/01/' + @Year
						END
						)
				SET @CompareFinalDate = Convert(datetime,                                   
						 CASE WHEN @Diff = 1 THEN '03/31/' + @Year
						 WHEN @Diff = 2 THEN '06/30/' + @Year
						 WHEN @Diff = 3 THEN '09/30/' + @Year
						 WHEN @Diff = 4 THEN '12/31/' + @Year
						END
						)                            
	  
					EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
						@CompareFinalDate, @Custom1  
           
            END                       
  
        IF @Filter = @FrequentCustomerByYear   
            BEGIN                                       
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)  
                                     ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                          
  
                EXEC dbo.ZNode_ReportsFrequentFiltered @FinalDate,  
                    @CompareFinalDate,@Custom1  
            END  
       
        IF @Filter = @TopSpendingCustomer   
            BEGIN                             
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder  
    SET @CompareDate = ( SELECT DateAdd(dd, 1, @CompareDate)) ;  
                SET @CompareFinalDate = ( SELECT Convert(varchar(10), @CompareDate, 101)) ;   
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
                                    
-- For Most Volume Customers and day --                                          
        IF @Filter = @TopSpendingCustomerByDay   
            BEGIN                  
                SET @StartDate = @TodayDate ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                            ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                     
    
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
                                     
-- For Most Volume Customers and week --                                          
        IF @Filter = @TopSpendingCustomerByWeek   
            BEGIN                                  
                SET @StartDate = ( SELECT   Dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                         
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
          EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
    
                                    
-- For Most Volume Customers and month --                                          
        IF @Filter = @TopSpendingCustomerByMonth   
            BEGIN                                      
                SET @Diff = ( SELECT    Datepart(day, @TodayDate)  
                            ) - 1 ;                            
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
   
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END                                
             
-- For Most Volume Customers and quater--                                          
        IF @Filter = @TopSpendingCustomerByQuarter  
            BEGIN    
                                              
                SET @Diff = (Select DatePart(QQ, GetDate())) ;  
				Set @FinalDate = Convert(datetime, 
						CASE WHEN @Diff = 1 THEN '01/01/' + @Year
							 WHEN @Diff = 2 THEN '04/01/' + @Year
							 WHEN @Diff = 3 THEN '07/01/' + @Year
							 WHEN @Diff = 4 THEN '10/01/' + @Year
						END
						)
				SET @CompareFinalDate = Convert(datetime,                                   
						CASE WHEN @Diff = 1 THEN '03/31/' + @Year
						 WHEN @Diff = 2 THEN '06/30/' + @Year
						 WHEN @Diff = 3 THEN '09/30/' + @Year
						 WHEN @Diff = 4 THEN '12/31/' + @Year
						END
						)                            
	                         
  
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END                                    
                                    
-- For Most Volume Customers and year--                                          
        IF @Filter = @TopSpendingCustomerByYear   
            BEGIN                                     
                SET @Diff = ( SELECT    Datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    Datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   Datepart(day, @TodayDate)  
                               ) - 1 ;                                          
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                           
  
                EXEC dbo.ZNode_ReportsVolumeFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
                                       
                                    
-- For Most Popular Product --                                          
        IF @Filter = @TopEarningProduct   
            BEGIN                                
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
   
            END                                    
                                    
-- For Most Popular Product and day --                                          
        IF @Filter = @TopEarningProductByDay   
            BEGIN  
    SET @StartDate = @TodayDate;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101))  
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;       
     
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                                    
                                    
-- For Most Popular Product and week --                                          
        IF @Filter = @TopEarningProductByWeek   
            BEGIN                            
                SET @StartDate = ( SELECT   Dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                                   
                                    
-- For Most Popular Product and month --                                          
        IF @Filter = @TopEarningProductByMonth   
            BEGIN                                       
                SET @Diff = ( SELECT    Datepart(day, @TodayDate)  
                            ) - 1 ;                 
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;  
    
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END  
                                    
                                    
-- For Most Popular Product and Quater--                      
        IF @Filter = @TopEarningProductByQuarter   
            BEGIN                            
                
                SET @Diff = (Select DatePart(QQ, GetDate())) ;  
				Set @FinalDate = Convert(datetime, 
						CASE WHEN @Diff = 1 THEN '01/01/' + @Year
							 WHEN @Diff = 2 THEN '04/01/' + @Year
							 WHEN @Diff = 3 THEN '07/01/' + @Year
							 WHEN @Diff = 4 THEN '10/01/' + @Year
						END
						)
				SET @CompareFinalDate = Convert(datetime,                                   
						 CASE WHEN @Diff = 1 THEN '03/31/' + @Year
						 WHEN @Diff = 2 THEN '06/30/' + @Year
						 WHEN @Diff = 3 THEN '09/30/' + @Year
						 WHEN @Diff = 4 THEN '12/31/' + @Year
						END
						)                            
	                                      
  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END  
   
-- For Most Popular Product and Year--                                          
        IF @Filter = @TopEarningProductByYear   
            BEGIN                                       
                SET @Diff = ( SELECT    Datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    Datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   Datepart(day, @TodayDate)  
                                     ) - 1 ;                                          
                SET @StartDate = ( SELECT   Dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                       
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                           
      
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                              
                            
-- For Best Sellers                              
        IF @Filter = @BestSeller   
            BEGIN                             
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                              
                            
-- For BestSellers and day --                                          
        IF @Filter = @BestSellerByDay   
            BEGIN                                       
                SET @StartDate = @TodayDate ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                     
   
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                                    
                                    
-- For BestSellers and week --                                          
        IF @Filter = @BestSellerByWeek   
            BEGIN                                       
                SET @StartDate = ( SELECT   Dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                    
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
     
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                                    
                                    
-- For BestSellers and month --                                          
        If @Filter = @BestSellerByMonth   
            BEGIN                                       
                SET @Diff = ( SELECT    datepart(day, @TodayDate)  
                            ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;              
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
            END                                        
                                    
-- For BestSellers and Quater--                                          
        IF @Filter = @BestSellerByQuarter   
            BEGIN                                       
                SET @Diff = ( SELECT    Datepart(month, @TodayDate)  
                            ) - 4 ;                                          
                SET @StartDate = ( SELECT   Dateadd(month, -@Diff,  
                                                    @TodayDate + 1)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
  
            END                       
                                    
-- For BestSellers and Year--                      
        IF @Filter = @BestSellerByYear   
            BEGIN                                       
                SET @Diff = ( SELECT    Datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)  
                                     ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;    
  
                EXEC ZNode_ReportsPopularFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1  
                  
            END                       
                            
-- For Most Popular Searches --                                          
        IF @Filter = @PopularSearch   
            BEGIN  
                SELECT  @FinalDate = (convert(varchar(10), MIN(CreateDte),101)),  
                        @CompareFinalDate =(convert(varchar(10), DATEADD(Day,1, MAX(CReateDte)),101))  
                FROM    ZNodeActivityLog  
        EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate, @Custom1                                          
            END  
                                     
-- For Most Popular Searches and day --                                          
        IF @Filter = @PopularSearchByDay   
            BEGIN                                      
                SET @StartDate = @TodayDate ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                               
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                     
    
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate,@Custom1  
            END                                    
                                    
-- For Most Popular Searches and week --                                          
        IF @Filter = @PopularSearchByWeek   
            BEGIN                                      
                SET @StartDate = ( SELECT   dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
    
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate , @Custom1 
            END                                    
                                    
-- For Most Popular Searches and month --                                          
        If @Filter = @PopularSearchByMonth   
            BEGIN                                      
                SET @Diff = ( SELECT    datepart(day, @TodayDate)  
                            ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate, @Custom1  
            END  
  
                                    
-- For Most Popular Searches and quater--                                          
        IF ( @Filter = @PopularSearchByQuarter )   
            BEGIN                          
                SET @StartDate = CAST(YEAR(getdate()) AS VARCHAR(4))  
                    + CASE WHEN MONTH(getdate()) IN ( 1, 2, 3 ) THEN '/01/01'  
                           WHEN MONTH(getdate()) IN ( 4, 5, 6 ) THEN '/04/01'  
                           WHEN MONTH(getdate()) IN ( 7, 8, 9 ) THEN '/07/01'  
                           WHEN MONTH(getdate()) IN ( 10, 11, 12 )  
                           THEN '/10/01'  
                      END    
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareMonthDate = ( SELECT    dateadd(Month, 3,  
                                                            @StartDate)  
                                        ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareMonthDate, 101)  
                                        ) ;                                   
    
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate,@Custom1  
            END                                   
                                    
-- For Most Popular Searches and year--                                          
        IF @Filter = @PopularSearchByYear   
            BEGIN                                     
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)  
                                     ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                           
     
                EXEC ZNode_ReportsSEOFiltered @FinalDate, @CompareFinalDate,@Custom1  
            END  
                       
-- For ActivityLog                             
        IF @Filter = @ActivityLog   
            BEGIN  
  -- Custom1 : ZnodeActivity log category  
  -- Custom2 : Znode Store name  
                EXEC ZNode_ReportsActivityLog @FromDate, @Custom1, @Custom2   
            END                            
                            
-- For Inventory Re-Orderlevel                            
        IF @Filter = @ReOrder   
            BEGIN                                  
                EXEC ZNode_ReportsReOrder @Custom1  
            END  
                                 
-- For Customer Feedback                            
        IF @Filter = @ServiceRequest   
            BEGIN                   
                EXEC ZNode_ReportsFeedback @Custom1  
            END                               
                                    
-- For Accounts                            
        IF @Filter = @Accounts   
            BEGIN                            
  -- Custom 1: Store name  
                EXEC ZNode_ReportsAccounts @FromDate, @ToDate, @Custom1 ;                                 
            END                            
                          
-- For Coupon Usage                            
        IF @Filter = @CouponUsage   
            BEGIN  
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder   
    SET @CompareDate = ( SELECT DateAdd(dd, 1, @CompareDate)) ;  
                SET @CompareFinalDate = ( SELECT Convert(varchar(10), @CompareDate, 101)) ;   
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END                            
                          
-- For Coupon Usage and day --                             
        IF @Filter = @CouponUsageByDay   
            BEGIN                                      
                SET @StartDate = @TodayDate ;        
                                                   
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                           
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                      
  
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END  
                          
-- For Coupon Usage and week --         
        IF @Filter = @CouponUsageByWeek   
            BEGIN                         
                SET @StartDate = ( SELECT   dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END                              
                                    
-- For Coupon Usage and month --                                          
        IF @Filter = @CouponUsageByMonth   
            BEGIN                                      
                SET @Diff = ( SELECT    datepart(day, @TodayDate)  
                            ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                        
  
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END                                                               
                                    
-- For Coupon Usage and quater--                                          
        IF @Filter = @CouponUsageByQuarter   
            BEGIN                                      
                SET @Diff = ( SELECT    datepart(month, @TodayDate)  
                            ) - 4 ;                                         
                SET @StartDate = ( SELECT   dateadd(month, -@Diff,  
                                                    @TodayDate + 1)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                   
  
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END                                
                                    
-- For Coupon Usage and year--                                          
        IF @Filter = @CouponUsageByYear   
            BEGIN                                     
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    datepart(day, @TodayDate)  
                                ) ;  
                set @Diff2 = @Diff  
                    + ( SELECT  datepart(day, ( select  getdate()  
                                              ))  
                      ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                           
                EXEC ZNode_ReportsCouponFiltered @FinalDate, @CompareFinalDate,  
                    @Custom1  
            END                                
          
-- For Affiliate Customers                  
        IF @Filter = @AffiliateOrder   
            BEGIN              
                SELECT  @FinalDate = MIN(OrderDate),  
                        @CompareFinalDate = MAX(OrderDate)  
                FROM    ZNodeOrder  
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
                  
-- For Affiliate Customers by day --                                          
        IF @Filter = @AffiliateOrderByDay   
            BEGIN  
                SET @StartDate = @TodayDate ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    Convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                     
    
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END                                    
                                    
-- For Affiliate Customers by week --                                          
        IF @Filter = @AffiliateOrderByWeek   
            BEGIN  
                SET @StartDate = ( SELECT   Dateadd(week,  
                                                    datediff(week, 0,  
                                                             @TodayDate) - 1,  
                                                    6)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   Convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                             
  
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END  
                                    
-- For Affiliate Customers by month --                                        
        IF @Filter = @AffiliateOrderByMonth   
            BEGIN                           
                SET @Diff = ( SELECT    datepart(day, @TodayDate)  
                            ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                    
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END                                    
  
-- For Affiliate Customers by quater--                                          
        IF @Filter = @AffiliateOrderByQuarter   
            BEGIN                           
                
                SET @Diff = (Select DatePart(QQ, GetDate())) ;  
				Set @FinalDate = Convert(datetime, 
						CASE WHEN @Diff = 1 THEN '01/01/' + @Year
							 WHEN @Diff = 2 THEN '04/01/' + @Year
							 WHEN @Diff = 3 THEN '07/01/' + @Year
							 WHEN @Diff = 4 THEN '10/01/' + @Year
						END
						)
				SET @CompareFinalDate = Convert(datetime,                                   
						 CASE WHEN @Diff = 1 THEN '03/31/' + @Year
						 WHEN @Diff = 2 THEN '06/30/' + @Year
						 WHEN @Diff = 3 THEN '09/30/' + @Year
						 WHEN @Diff = 4 THEN '12/31/' + @Year
						END
						)    
  
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
            END                                                
                                    
-- For Affiliate Customers by year --                                          
        IF @Filter = @AffiliateOrderByYear   
            BEGIN                                      
                SET @Diff = ( SELECT    datepart(dayofyear, @TodayDate)  
                            ) - ( SELECT    datepart(day, @TodayDate)  
                                ) ;                                          
                SET @Diff2 = @Diff + ( SELECT   datepart(day, @TodayDate)  
                                     ) - 1 ;                                          
                SET @StartDate = ( SELECT   dateadd(dd, -@Diff2, @TodayDate)  
                                 ) ;                                          
                SET @FinalDate = ( SELECT   convert(varchar(10), @StartDate, 101)  
                                 ) ;                                          
                SET @CompareFinalDate = ( SELECT    convert(varchar(10), @CompareDate, 101)  
                                        ) ;                                          
    
                EXEC ZNode_ReportsAffiliateFiltered @FinalDate,  
                    @CompareFinalDate, @Custom1   
                  
            END  
            
-- For Vendor Revenue                                       
        IF @Filter = @VendorRevenue   
            BEGIN                           
                EXEC ZNode_ReportVendorRevenue @FromDate, @ToDate,  @Custom1                   
            END       

-- For Vendor Revenue                                       
        IF @Filter = @VendorProductRevenue   
            BEGIN                           
                EXEC [ZNode_ReportVendorProductRevenue] @FromDate, @ToDate, @Custom1                  
            END             
                   
                        
                        
 --For Tax Detail and Year--                      
        IF @Filter = @SalesTax 
            BEGIN           
                EXEC ZNode_ReportsTaxFiltered @FinalDate, @CompareFinalDate,
                    'month',@Custom1
            END                 
                      
--For Tax Detail and Month--                     
        IF @Filter = @SalesTaxByMonth 
            BEGIN       
                SELECT  @FinalDate = convert(varchar(10), '01/01/' + @Year, 101)
                SELECT  @CompareFinalDate = convert(varchar(10), '12/31/'
                        + @Year, 101)         
		
                EXEC ZNode_ReportsTaxFiltered @FinalDate, @CompareFinalDate,
                    'month',@Custom1
            END                   
                         
--For Tax Detail and Quarter--                      
        IF @Filter = @SalesTaxByQuarter 
            BEGIN                    
                SELECT  @FinalDate = convert(varchar(10), '01/01/' + @Year, 101)
                SELECT  @CompareFinalDate = convert(varchar(10), '12/31/'
                        + @Year, 101)         
		
                EXEC ZNode_ReportsTaxFiltered @FinalDate, @CompareFinalDate,
                    'quarter',@Custom1
            END                   
                
-- For Supplier --                                            
        IF @Filter = @SupplierList 
            BEGIN                       
							                      
				-- Custom1 : Store name.
                EXEC ZNode_ReportsSupplierList @FromDate, @ToDate, @Custom1,
                    @SupplierID
            END               
            
        -- For product sold on vendor sites. --                                            
        IF @Filter = @ProductSoldOnVendorSites
            BEGIN                       
							                      
				-- Custom1 : Store name.
                EXEC ZNode_ReportsProductSoldOnVendorSites @FromDate, @ToDate, @Custom1
            END               
           
    END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_EmptyCatalog]'
GO



ALTER PROCEDURE [dbo].[ZNode_EmptyCatalog]    
(@UserName Varchar(1000))    
AS    
BEGIN            
-- DOES NOT DELETE CONTENT PAGES    
 
  
DELETE FROM ZNODETAGGROUPCATEGORY  
DELETE FROM ZNODETAGGROUP  
DELETE FROM ZNODETAG  
DELETE FROM ZNODECATEGORYNODE  
DELETE FROM ZNODEWISHLIST      
DELETE FROM ZNODEREVIEW      
DELETE FROM ZNODESKUATTRIBUTE              
DELETE FROM ZNODEPRODUCTTYPEATTRIBUTE              
DELETE FROM ZNODEPRODUCTATTRIBUTE              
DELETE FROM ZNODEATTRIBUTETYPE              
DELETE FROM ZNODEADDONVALUE  
DELETE FROM ZNODEPRODUCTADDON              
DELETE FROM ZNODEADDON      
DELETE FROM ZNODEPRODUCTCATEGORY              
DELETE FROM ZNODEPRODUCTIMAGE              
DELETE FROM ZNODECATEGORY              
DELETE FROM ZNODEPRODUCTCROSSSELL              
DELETE FROM ZNODEPRODUCTHIGHLIGHT        
DELETE FROM ZNODEDIGITALASSET        
DELETE FROM ZNODEPRODUCTTIER        
DELETE FROM ZNODEORDERLINEITEM               
DELETE FROM ZNODEORDER              
DELETE FROM ZNODESAVEDCARTLINEITEM
DELETE FROM ZNODESAVEDCART
DELETE FROM ZNODECOOKIEMAPPING
DELETE FROM ZNODESKU  
DELETE FROM ZNODESKUINVENTORY           
DELETE FROM ZNODEPROMOTION         
DELETE FROM ZNODEPARENTCHILDPRODUCT  
DELETE FROM ZNODEPRODUCTREVIEWHISTORY
DELETE FROM ZNODEPRODUCT  
DELETE FROM ZNODEMANUFACTURER            
DELETE FROM ZNODEPRODUCTTYPE              
DELETE FROM ZNODEHIGHLIGHT      
DELETE FROM ZNODESHIPPINGRULE              
DELETE FROM ZNODESHIPPING              
DELETE FROM ZNODETAXRULE              
DELETE FROM ZNODENOTE              
DELETE FROM ZNODECASEREQUEST              
DELETE FROM ZNODEPAYMENTTOKEN
DELETE FROM ZNODEPAYMENTSETTING
DELETE FROM ZNODESTORE      
DELETE FROM ZNODEPASSWORDLOG WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName)      
DELETE FROM ZNODEADDRESS WHERE AccountID NOT IN(SELECT AccountID FROM ZnodeAccount WHERE USERID IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName))
DELETE FROM ZNODEACCOUNTPROFILE WHERE AccountID NOT IN(SELECT AccountID FROM ZnodeAccount WHERE USERID IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName))
DELETE FROM ZNODEACCOUNT WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName) OR USERID IS NULL      
DELETE FROM ASPNET_MEMBERSHIP WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName) OR USERID IS NULL            
DELETE FROM ASPNET_USERSINROLES WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName) OR USERID IS NULL        
DELETE FROM ASPNET_PROFILE WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName) OR USERID IS NULL            
DELETE FROM ASPNET_USERS WHERE USERID NOT IN(SELECT USERID FROM ASPNET_USERS WHERE USERNAME =@UserName) OR USERID IS NULL            
            
INSERT INTO ZNODEPRODUCTTYPE (NAME, DESCRIPTION, DISPLAYORDER) VALUES ('DEFAULT','DEFAULT PRODUCT', 1)                
END  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProductType_GetPaged]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeProductType table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProductType_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[ProductTypeId]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [ProductTypeId]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ', [Description]'
				SET @SQL = @SQL + ', [DisplayOrder]'
				SET @SQL = @SQL + ', [PortalID]'
				SET @SQL = @SQL + ', [IsGiftCard]'
				SET @SQL = @SQL + ', [Franchisable]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeProductType]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [ProductTypeId],'
				SET @SQL = @SQL + ' [Name],'
				SET @SQL = @SQL + ' [Description],'
				SET @SQL = @SQL + ' [DisplayOrder],'
				SET @SQL = @SQL + ' [PortalID],'
				SET @SQL = @SQL + ' [IsGiftCard],'
				SET @SQL = @SQL + ' [Franchisable]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeProductType]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeOrderLineItem_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeOrderLineItem table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeOrderLineItem_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[OrderLineItemID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [OrderLineItemID]'
				SET @SQL = @SQL + ', [OrderID]'
				SET @SQL = @SQL + ', [ShipmentID]'
				SET @SQL = @SQL + ', [ProductNum]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ', [Description]'
				SET @SQL = @SQL + ', [Quantity]'
				SET @SQL = @SQL + ', [Price]'
				SET @SQL = @SQL + ', [Weight]'
				SET @SQL = @SQL + ', [SKU]'
				SET @SQL = @SQL + ', [ParentOrderLineItemID]'
				SET @SQL = @SQL + ', [OrderLineItemRelationshipTypeID]'
				SET @SQL = @SQL + ', [DownloadLink]'
				SET @SQL = @SQL + ', [DiscountAmount]'
				SET @SQL = @SQL + ', [ShipSeparately]'
				SET @SQL = @SQL + ', [ShipDate]'
				SET @SQL = @SQL + ', [ReturnDate]'
				SET @SQL = @SQL + ', [ShippingCost]'
				SET @SQL = @SQL + ', [PromoDescription]'
				SET @SQL = @SQL + ', [SalesTax]'
				SET @SQL = @SQL + ', [VAT]'
				SET @SQL = @SQL + ', [GST]'
				SET @SQL = @SQL + ', [PST]'
				SET @SQL = @SQL + ', [HST]'
				SET @SQL = @SQL + ', [TransactionNumber]'
				SET @SQL = @SQL + ', [PaymentStatusID]'
				SET @SQL = @SQL + ', [TrackingNumber]'
				SET @SQL = @SQL + ', [AutoGeneratedTracking]'
				SET @SQL = @SQL + ', [Custom1]'
				SET @SQL = @SQL + ', [Custom2]'
				SET @SQL = @SQL + ', [Custom3]'
				SET @SQL = @SQL + ', [OrderLineItemStateID]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeOrderLineItem]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [OrderLineItemID],'
				SET @SQL = @SQL + ' [OrderID],'
				SET @SQL = @SQL + ' [ShipmentID],'
				SET @SQL = @SQL + ' [ProductNum],'
				SET @SQL = @SQL + ' [Name],'
				SET @SQL = @SQL + ' [Description],'
				SET @SQL = @SQL + ' [Quantity],'
				SET @SQL = @SQL + ' [Price],'
				SET @SQL = @SQL + ' [Weight],'
				SET @SQL = @SQL + ' [SKU],'
				SET @SQL = @SQL + ' [ParentOrderLineItemID],'
				SET @SQL = @SQL + ' [OrderLineItemRelationshipTypeID],'
				SET @SQL = @SQL + ' [DownloadLink],'
				SET @SQL = @SQL + ' [DiscountAmount],'
				SET @SQL = @SQL + ' [ShipSeparately],'
				SET @SQL = @SQL + ' [ShipDate],'
				SET @SQL = @SQL + ' [ReturnDate],'
				SET @SQL = @SQL + ' [ShippingCost],'
				SET @SQL = @SQL + ' [PromoDescription],'
				SET @SQL = @SQL + ' [SalesTax],'
				SET @SQL = @SQL + ' [VAT],'
				SET @SQL = @SQL + ' [GST],'
				SET @SQL = @SQL + ' [PST],'
				SET @SQL = @SQL + ' [HST],'
				SET @SQL = @SQL + ' [TransactionNumber],'
				SET @SQL = @SQL + ' [PaymentStatusID],'
				SET @SQL = @SQL + ' [TrackingNumber],'
				SET @SQL = @SQL + ' [AutoGeneratedTracking],'
				SET @SQL = @SQL + ' [Custom1],'
				SET @SQL = @SQL + ' [Custom2],'
				SET @SQL = @SQL + ' [Custom3],'
				SET @SQL = @SQL + ' [OrderLineItemStateID]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeOrderLineItem]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchVendorProduct]'
GO
SET ANSI_NULLS ON
GO


ALTER PROCEDURE [dbo].[ZNode_SearchVendorProduct]
@ProductName VARCHAR (MAX)='', 
@ProductId VARCHAR (MAX)='', 
@Vendor VARCHAR (MAX)='', 
@VendorId VARCHAR (MAX)='', 
@ProductStatus VARCHAR (MAX)='', 
@COId VARCHAR (MAX)=''
AS
BEGIN
	DECLARE @Query NVARCHAR(3000)
	DECLARE @WhereClause NVARCHAR(1000)
	
    SET @Query='SELECT P.ProductID,
           ImageFile,
           P.Name,
           P.ProductNum,
           RetailPrice,
           SalePrice,
           P.DisplayOrder,
           P.ActiveInd,
           P.PortalID,
           (FirstName + '' ''+ LastName + '' '' + CompanyName) AS Vendor,
           (SELECT TOP 1 QuantityOnHand
            FROM   ZNodeSKUInventory AS SkuInventory
            WHERE  SkuInventory.SKU IN (SELECT SKU
                                        FROM   ZNodeSKU
                                        WHERE  ProductID = P.ProductID)) AS ''QuantityOnHand'',
           P.ReviewStateId,
           R.ReviewStateName AS ProductStatus
           FROM   
			ZNodeProduct AS P LEFT OUTER JOIN ZNodeProductReviewState R ON P.ReviewStateID=R.ReviewStateID 
			LEFT OUTER JOIN ZNodeAddress A ON P.AccountID=A.AccountID AND A.IsDefaultBilling=1   '
    
    SET @WhereClause='';
    IF(LEN(@ProductName)>0)
		BEGIN
	 	    SET @ProductName = '%' + @ProductName + '%'
			SET @WhereClause=@WhereClause+' AND P.Name LIKE @ProductName'

		END
	IF(LEN(@ProductId)>0)
		BEGIN	
			SET @WhereClause=@WhereClause+' AND P.ProductNum = @ProductId';
		END
    
    IF(LEN(@ProductStatus)>0)
		BEGIN	
			SET @WhereClause=@WhereClause+' AND P.ReviewStateId = @ProductStatus';
		END
	IF(LEN(@VendorId)>0)
		BEGIN	
			SET @WhereClause=@WhereClause+' AND P.AccountId = @VendorId';
		END		
		
	IF(LEN(@Vendor)>0)
		BEGIN	
			SET @Vendor='%'+ @Vendor + '%'
			SET @WhereClause=@WhereClause+' AND P.AccountId IN 
					(SELECT 
						AccountID 
					FROM 
						ZNodeAddress 
					WHERE FirstName LIKE @Vendor OR LastName LIKE @Vendor)';
		END		
    IF(LEN(@COId)>0)
		BEGIN	
			SET @WhereClause = @WhereClause+' AND P.ProductID IN (SELECT ProductID FROM ZNodeSKU WHERE SKU = @COId)'
		END
     
     
     IF(RTRIM(LTRIM(@WhereClause))<>'')
		BEGIN
		-- Replace the left most AND with WHERE clause.
			SET @WhereClause=' WHERE '+ SUBSTRING(LTRIM(@WhereClause), 4, LEN(@WhereClause));
		END
     
     SET @Query =@Query + @WhereClause;     
     
     EXEC SP_EXECUTESQL @Query, N'@ProductName nvarchar(max),@ProductId varchar(max),@Vendor nvarchar(max),@VendorId varchar(max),@ProductStatus INT,@COId varchar(max)',
    @ProductName = @ProductName,@ProductId = @ProductId,@Vendor = @Vendor,@VendorId = @VendorId,@ProductStatus = @ProductStatus,@COId = @COId;	
           
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_WS_GetProductsByKeyword]'
GO

ALTER PROCEDURE [dbo].[ZNode_WS_GetProductsByKeyword]
@ProductIds VARCHAR(500)=NULL,
@PortalID INT=NULL, 
@LocaleId INT=NULL
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Query NVARCHAR(4000)=''
    SET @Query ='
     SELECT DISTINCT TOP 20
		P.ProductID,
		P.Name,
		P.ShortDescription,
		P.[Description],
		P.FeaturesDesc,
		P.ProductNum,
		P.RetailPrice,
		P.SalePrice,
		(SELECT TOP 1 QuantityOnHand FROM ZNodeSKUInventory I, ZNodeSku S WHERE I.Sku=S.Sku AND S.ProductID=P.ProductID) AS QuantityOnHand,
		(SELECT TOP 1 Sku FROM ZNodeSku S WHERE ProductID=P.ProductID) AS Sku,		
		P.ImageFile,  
		Portal.SplashCategoryID,
		ISNULL(Portal.SplashImageFile,'''') AS SplashImageFile,
		(SELECT Name FROM ZNodeCategory WHERE CategoryId=Portal.SplashCategoryID) AS SplashCategoryName,
		ISNULL(P.DisplayOrder,500) AS DisplayOrder,      				
		TotalReviews=(SELECT ISNULL(COUNT(1),0) FROM ZNodeReview R WHERE R.ProductID=P.ProductID), 
        ReviewRating=(SELECT ISNULL(AVG(Rating),0) FROM ZNodeReview R WHERE R.ProductID=P.ProductID AND R.Status=''A'') 
	 FROM ZNodeProduct AS P
                   INNER JOIN
                   ZNodeProductCategory AS PC
                   ON P.ProductID = PC.ProductID
                   INNER JOIN
                   ZNodeCategoryNode AS ZCN
                   ON ZCN.CategoryID = PC.CategoryID
                   INNER JOIN
                   ZNodePortalCatalog AS ZPOC
                   ON ZPOC.CatalogID = ZCN.CatalogID
                   INNER JOIN
                   ZNodeCategory AS C
                   ON C.CategoryID = PC.CategoryID
                   INNER JOIN ZNodePortal Portal 
                   ON Portal.PortalID='+ CAST(@PortalID AS VARCHAR(10))+ '
            WHERE  (ZPOC.PortalID ='+ CAST(@PortalID AS VARCHAR(10))+ '
                    AND ZPOC.LocaleID ='+ CAST(@LocaleId AS VARCHAR(10)) + ')
				AND P.ProductID IN ('+ @ProductIds +')
				AND P.ActiveInd=1 
				AND ZCN.ActiveInd=1
			ORDER BY DisplayOrder DESC '
			
		
	EXEC SP_EXECUTESQL @Query;		
END 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetPaged]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodePromotion table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[PromotionID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [PromotionID]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ', [Description]'
				SET @SQL = @SQL + ', [ProfileID]'
				SET @SQL = @SQL + ', [AccountID]'
				SET @SQL = @SQL + ', [ProductID]'
				SET @SQL = @SQL + ', [AddOnValueID]'
				SET @SQL = @SQL + ', [SKUID]'
				SET @SQL = @SQL + ', [DiscountTypeID]'
				SET @SQL = @SQL + ', [Discount]'
				SET @SQL = @SQL + ', [StartDate]'
				SET @SQL = @SQL + ', [EndDate]'
				SET @SQL = @SQL + ', [CouponInd]'
				SET @SQL = @SQL + ', [CouponCode]'
				SET @SQL = @SQL + ', [CouponQuantityAvailable]'
				SET @SQL = @SQL + ', [PromotionMessage]'
				SET @SQL = @SQL + ', [OrderMinimum]'
				SET @SQL = @SQL + ', [QuantityMinimum]'
				SET @SQL = @SQL + ', [PromotionProductQty]'
				SET @SQL = @SQL + ', [PromotionProductID]'
				SET @SQL = @SQL + ', [DisplayOrder]'
				SET @SQL = @SQL + ', [Custom1]'
				SET @SQL = @SQL + ', [Custom2]'
				SET @SQL = @SQL + ', [Custom3]'
				SET @SQL = @SQL + ', [EnableCouponUrl]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePromotion]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [PromotionID],'
				SET @SQL = @SQL + ' [Name],'
				SET @SQL = @SQL + ' [Description],'
				SET @SQL = @SQL + ' [ProfileID],'
				SET @SQL = @SQL + ' [AccountID],'
				SET @SQL = @SQL + ' [ProductID],'
				SET @SQL = @SQL + ' [AddOnValueID],'
				SET @SQL = @SQL + ' [SKUID],'
				SET @SQL = @SQL + ' [DiscountTypeID],'
				SET @SQL = @SQL + ' [Discount],'
				SET @SQL = @SQL + ' [StartDate],'
				SET @SQL = @SQL + ' [EndDate],'
				SET @SQL = @SQL + ' [CouponInd],'
				SET @SQL = @SQL + ' [CouponCode],'
				SET @SQL = @SQL + ' [CouponQuantityAvailable],'
				SET @SQL = @SQL + ' [PromotionMessage],'
				SET @SQL = @SQL + ' [OrderMinimum],'
				SET @SQL = @SQL + ' [QuantityMinimum],'
				SET @SQL = @SQL + ' [PromotionProductQty],'
				SET @SQL = @SQL + ' [PromotionProductID],'
				SET @SQL = @SQL + ' [DisplayOrder],'
				SET @SQL = @SQL + ' [Custom1],'
				SET @SQL = @SQL + ' [Custom2],'
				SET @SQL = @SQL + ' [Custom3],'
				SET @SQL = @SQL + ' [EnableCouponUrl]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePromotion]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeProduct_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeProduct table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeProduct_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[ProductID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [ProductID]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ', [ShortDescription]'
				SET @SQL = @SQL + ', [Description]'
				SET @SQL = @SQL + ', [FeaturesDesc]'
				SET @SQL = @SQL + ', [ProductNum]'
				SET @SQL = @SQL + ', [ProductTypeID]'
				SET @SQL = @SQL + ', [RetailPrice]'
				SET @SQL = @SQL + ', [SalePrice]'
				SET @SQL = @SQL + ', [WholesalePrice]'
				SET @SQL = @SQL + ', [ImageFile]'
				SET @SQL = @SQL + ', [ImageAltTag]'
				SET @SQL = @SQL + ', [Weight]'
				SET @SQL = @SQL + ', [Length]'
				SET @SQL = @SQL + ', [Width]'
				SET @SQL = @SQL + ', [Height]'
				SET @SQL = @SQL + ', [BeginActiveDate]'
				SET @SQL = @SQL + ', [EndActiveDate]'
				SET @SQL = @SQL + ', [DisplayOrder]'
				SET @SQL = @SQL + ', [ActiveInd]'
				SET @SQL = @SQL + ', [CallForPricing]'
				SET @SQL = @SQL + ', [HomepageSpecial]'
				SET @SQL = @SQL + ', [CategorySpecial]'
				SET @SQL = @SQL + ', [InventoryDisplay]'
				SET @SQL = @SQL + ', [Keywords]'
				SET @SQL = @SQL + ', [ManufacturerID]'
				SET @SQL = @SQL + ', [AdditionalInfoLink]'
				SET @SQL = @SQL + ', [AdditionalInfoLinkLabel]'
				SET @SQL = @SQL + ', [ShippingRuleTypeID]'
				SET @SQL = @SQL + ', [ShippingRate]'
				SET @SQL = @SQL + ', [SEOTitle]'
				SET @SQL = @SQL + ', [SEOKeywords]'
				SET @SQL = @SQL + ', [SEODescription]'
				SET @SQL = @SQL + ', [Custom1]'
				SET @SQL = @SQL + ', [Custom2]'
				SET @SQL = @SQL + ', [Custom3]'
				SET @SQL = @SQL + ', [ShipEachItemSeparately]'
				SET @SQL = @SQL + ', [AllowBackOrder]'
				SET @SQL = @SQL + ', [BackOrderMsg]'
				SET @SQL = @SQL + ', [DropShipInd]'
				SET @SQL = @SQL + ', [DropShipEmailID]'
				SET @SQL = @SQL + ', [Specifications]'
				SET @SQL = @SQL + ', [AdditionalInformation]'
				SET @SQL = @SQL + ', [InStockMsg]'
				SET @SQL = @SQL + ', [OutOfStockMsg]'
				SET @SQL = @SQL + ', [TrackInventoryInd]'
				SET @SQL = @SQL + ', [DownloadLink]'
				SET @SQL = @SQL + ', [FreeShippingInd]'
				SET @SQL = @SQL + ', [NewProductInd]'
				SET @SQL = @SQL + ', [SEOURL]'
				SET @SQL = @SQL + ', [MaxQty]'
				SET @SQL = @SQL + ', [ShipSeparately]'
				SET @SQL = @SQL + ', [FeaturedInd]'
				SET @SQL = @SQL + ', [WebServiceDownloadDte]'
				SET @SQL = @SQL + ', [UpdateDte]'
				SET @SQL = @SQL + ', [SupplierID]'
				SET @SQL = @SQL + ', [RecurringBillingInd]'
				SET @SQL = @SQL + ', [RecurringBillingInstallmentInd]'
				SET @SQL = @SQL + ', [RecurringBillingPeriod]'
				SET @SQL = @SQL + ', [RecurringBillingFrequency]'
				SET @SQL = @SQL + ', [RecurringBillingTotalCycles]'
				SET @SQL = @SQL + ', [RecurringBillingInitialAmount]'
				SET @SQL = @SQL + ', [TaxClassID]'
				SET @SQL = @SQL + ', [MinQty]'
				SET @SQL = @SQL + ', [ReviewStateID]'
				SET @SQL = @SQL + ', [AffiliateUrl]'
				SET @SQL = @SQL + ', [IsShippable]'
				SET @SQL = @SQL + ', [AccountID]'
				SET @SQL = @SQL + ', [PortalID]'
				SET @SQL = @SQL + ', [Franchisable]'
				SET @SQL = @SQL + ', [ExpirationPeriod]'
				SET @SQL = @SQL + ', [ExpirationFrequency]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeProduct]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [ProductID],'
				SET @SQL = @SQL + ' [Name],'
				SET @SQL = @SQL + ' [ShortDescription],'
				SET @SQL = @SQL + ' [Description],'
				SET @SQL = @SQL + ' [FeaturesDesc],'
				SET @SQL = @SQL + ' [ProductNum],'
				SET @SQL = @SQL + ' [ProductTypeID],'
				SET @SQL = @SQL + ' [RetailPrice],'
				SET @SQL = @SQL + ' [SalePrice],'
				SET @SQL = @SQL + ' [WholesalePrice],'
				SET @SQL = @SQL + ' [ImageFile],'
				SET @SQL = @SQL + ' [ImageAltTag],'
				SET @SQL = @SQL + ' [Weight],'
				SET @SQL = @SQL + ' [Length],'
				SET @SQL = @SQL + ' [Width],'
				SET @SQL = @SQL + ' [Height],'
				SET @SQL = @SQL + ' [BeginActiveDate],'
				SET @SQL = @SQL + ' [EndActiveDate],'
				SET @SQL = @SQL + ' [DisplayOrder],'
				SET @SQL = @SQL + ' [ActiveInd],'
				SET @SQL = @SQL + ' [CallForPricing],'
				SET @SQL = @SQL + ' [HomepageSpecial],'
				SET @SQL = @SQL + ' [CategorySpecial],'
				SET @SQL = @SQL + ' [InventoryDisplay],'
				SET @SQL = @SQL + ' [Keywords],'
				SET @SQL = @SQL + ' [ManufacturerID],'
				SET @SQL = @SQL + ' [AdditionalInfoLink],'
				SET @SQL = @SQL + ' [AdditionalInfoLinkLabel],'
				SET @SQL = @SQL + ' [ShippingRuleTypeID],'
				SET @SQL = @SQL + ' [ShippingRate],'
				SET @SQL = @SQL + ' [SEOTitle],'
				SET @SQL = @SQL + ' [SEOKeywords],'
				SET @SQL = @SQL + ' [SEODescription],'
				SET @SQL = @SQL + ' [Custom1],'
				SET @SQL = @SQL + ' [Custom2],'
				SET @SQL = @SQL + ' [Custom3],'
				SET @SQL = @SQL + ' [ShipEachItemSeparately],'
				SET @SQL = @SQL + ' [AllowBackOrder],'
				SET @SQL = @SQL + ' [BackOrderMsg],'
				SET @SQL = @SQL + ' [DropShipInd],'
				SET @SQL = @SQL + ' [DropShipEmailID],'
				SET @SQL = @SQL + ' [Specifications],'
				SET @SQL = @SQL + ' [AdditionalInformation],'
				SET @SQL = @SQL + ' [InStockMsg],'
				SET @SQL = @SQL + ' [OutOfStockMsg],'
				SET @SQL = @SQL + ' [TrackInventoryInd],'
				SET @SQL = @SQL + ' [DownloadLink],'
				SET @SQL = @SQL + ' [FreeShippingInd],'
				SET @SQL = @SQL + ' [NewProductInd],'
				SET @SQL = @SQL + ' [SEOURL],'
				SET @SQL = @SQL + ' [MaxQty],'
				SET @SQL = @SQL + ' [ShipSeparately],'
				SET @SQL = @SQL + ' [FeaturedInd],'
				SET @SQL = @SQL + ' [WebServiceDownloadDte],'
				SET @SQL = @SQL + ' [UpdateDte],'
				SET @SQL = @SQL + ' [SupplierID],'
				SET @SQL = @SQL + ' [RecurringBillingInd],'
				SET @SQL = @SQL + ' [RecurringBillingInstallmentInd],'
				SET @SQL = @SQL + ' [RecurringBillingPeriod],'
				SET @SQL = @SQL + ' [RecurringBillingFrequency],'
				SET @SQL = @SQL + ' [RecurringBillingTotalCycles],'
				SET @SQL = @SQL + ' [RecurringBillingInitialAmount],'
				SET @SQL = @SQL + ' [TaxClassID],'
				SET @SQL = @SQL + ' [MinQty],'
				SET @SQL = @SQL + ' [ReviewStateID],'
				SET @SQL = @SQL + ' [AffiliateUrl],'
				SET @SQL = @SQL + ' [IsShippable],'
				SET @SQL = @SQL + ' [AccountID],'
				SET @SQL = @SQL + ' [PortalID],'
				SET @SQL = @SQL + ' [Franchisable],'
				SET @SQL = @SQL + ' [ExpirationPeriod],'
				SET @SQL = @SQL + ' [ExpirationFrequency]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeProduct]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchGiftCards]'
GO

ALTER PROCEDURE [dbo].[ZNode_SearchGiftCards]
@Name VARCHAR (512)='', 
@Amount DECIMAL (18, 2)=0, 
@CardNumber VARCHAR (100), 
@AccountID INT=NULL, 
@DontShowExpired BIT=1
AS
BEGIN
    DECLARE @Query AS NVARCHAR (1000);
    DECLARE @OrderBy AS NVARCHAR (100);
    DECLARE @Filter AS NVARCHAR (4000);
    SET @Query = 'SELECT 
            GC.GiftCardId,
            GC.PortalId,
            GC.Name,
            GC.CardNumber,
            GC.CreatedBy,
            GC.CreateDate,
            GC.OrderItemId,
            GC.AccountId,
            GC.ExpirationDate,
            GC.Amount
        FROM 
            ZNodeGiftCard GC ';
    SET @Filter = '';
    IF (LEN(@Name) > 0)
        BEGIN
            SET @Filter =@Filter + ' AND GC.Name LIKE ''%' + @Name + '%''';
        END
    IF (@Amount > 0)
        BEGIN
            SET @Filter =@Filter+ ' AND GC.Amount=' + CAST(@Amount  AS VARCHAR(10))+ '';
        END
    IF (LEN(@CardNumber) > 0)
        BEGIN
            SET @Filter =@Filter+ ' AND GC.CardNumber LIKE ''%' + @CardNumber + '%''';
        END
    IF (@AccountID > 0 AND @AccountID IS NOT NULL)
        BEGIN
            SET @Filter =@Filter+ ' AND GC.AccountID=' + CAST(@AccountID  AS VARCHAR(10))+ '';
        END
    IF (@DontShowExpired = 1)
        BEGIN
            SET @Filter =@Filter+ ' AND GC.ExpirationDate>='''+  CONVERT(Varchar(10), GETDATE(), 101) +'''';
        END
        
    IF (RTRIM(LTRIM(@Filter)) <> '')
        BEGIN
            SET @Filter =' WHERE ' + SUBSTRING(LTRIM(@Filter), 4, LEN(@Filter));
        END
        
    SET @OrderBy = ' ORDER BY GC.GiftCardId DESC';
    
    SET @Query = @Query + @Filter + @OrderBY;
    
    -- PRINT @Query
    EXECUTE SP_EXECUTESQL @Query;
END  


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeContentPage_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeContentPage table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeContentPage_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[ContentPageID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [ContentPageID]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ', [PortalID]'
				SET @SQL = @SQL + ', [Theme]'
				SET @SQL = @SQL + ', [MasterPage]'
				SET @SQL = @SQL + ', [CSS]'
				SET @SQL = @SQL + ', [Title]'
				SET @SQL = @SQL + ', [SEOTitle]'
				SET @SQL = @SQL + ', [SEOMetaKeywords]'
				SET @SQL = @SQL + ', [SEOMetaDescription]'
				SET @SQL = @SQL + ', [AllowDelete]'
				SET @SQL = @SQL + ', [TemplateName]'
				SET @SQL = @SQL + ', [ActiveInd]'
				SET @SQL = @SQL + ', [AnalyticsCode]'
				SET @SQL = @SQL + ', [Custom1]'
				SET @SQL = @SQL + ', [Custom2]'
				SET @SQL = @SQL + ', [Custom3]'
				SET @SQL = @SQL + ', [SEOURL]'
				SET @SQL = @SQL + ', [LocaleId]'
				SET @SQL = @SQL + ', [MetaTagAdditional]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeContentPage]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [ContentPageID],'
				SET @SQL = @SQL + ' [Name],'
				SET @SQL = @SQL + ' [PortalID],'
				SET @SQL = @SQL + ' [Theme],'
				SET @SQL = @SQL + ' [MasterPage],'
				SET @SQL = @SQL + ' [CSS],'
				SET @SQL = @SQL + ' [Title],'
				SET @SQL = @SQL + ' [SEOTitle],'
				SET @SQL = @SQL + ' [SEOMetaKeywords],'
				SET @SQL = @SQL + ' [SEOMetaDescription],'
				SET @SQL = @SQL + ' [AllowDelete],'
				SET @SQL = @SQL + ' [TemplateName],'
				SET @SQL = @SQL + ' [ActiveInd],'
				SET @SQL = @SQL + ' [AnalyticsCode],'
				SET @SQL = @SQL + ' [Custom1],'
				SET @SQL = @SQL + ' [Custom2],'
				SET @SQL = @SQL + ' [Custom3],'
				SET @SQL = @SQL + ' [SEOURL],'
				SET @SQL = @SQL + ' [LocaleId],'
				SET @SQL = @SQL + ' [MetaTagAdditional]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeContentPage]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeMessageConfig_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeMessageConfig table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeMessageConfig_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[MessageID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [MessageID]'
				SET @SQL = @SQL + ', [Key]'
				SET @SQL = @SQL + ', [PortalID]'
				SET @SQL = @SQL + ', [LocaleID]'
				SET @SQL = @SQL + ', [Description]'
				SET @SQL = @SQL + ', [Value]'
				SET @SQL = @SQL + ', [MessageTypeID]'
				SET @SQL = @SQL + ', [PageSEOName]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeMessageConfig]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [MessageID],'
				SET @SQL = @SQL + ' [Key],'
				SET @SQL = @SQL + ' [PortalID],'
				SET @SQL = @SQL + ' [LocaleID],'
				SET @SQL = @SQL + ' [Description],'
				SET @SQL = @SQL + ' [Value],'
				SET @SQL = @SQL + ' [MessageTypeID],'
				SET @SQL = @SQL + ' [PageSEOName]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeMessageConfig]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodePortal table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[PortalID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [PortalID]'
				SET @SQL = @SQL + ', [CompanyName]'
				SET @SQL = @SQL + ', [StoreName]'
				SET @SQL = @SQL + ', [LogoPath]'
				SET @SQL = @SQL + ', [UseSSL]'
				SET @SQL = @SQL + ', [AdminEmail]'
				SET @SQL = @SQL + ', [SalesEmail]'
				SET @SQL = @SQL + ', [CustomerServiceEmail]'
				SET @SQL = @SQL + ', [SalesPhoneNumber]'
				SET @SQL = @SQL + ', [CustomerServicePhoneNumber]'
				SET @SQL = @SQL + ', [ImageNotAvailablePath]'
				SET @SQL = @SQL + ', [MaxCatalogDisplayColumns]'
				SET @SQL = @SQL + ', [MaxCatalogDisplayItems]'
				SET @SQL = @SQL + ', [MaxCatalogCategoryDisplayThumbnails]'
				SET @SQL = @SQL + ', [MaxCatalogItemSmallThumbnailWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemSmallWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemMediumWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemThumbnailWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemLargeWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemCrossSellWidth]'
				SET @SQL = @SQL + ', [ShowSwatchInCategory]'
				SET @SQL = @SQL + ', [ShowAlternateImageInCategory]'
				SET @SQL = @SQL + ', [ActiveInd]'
				SET @SQL = @SQL + ', [SMTPServer]'
				SET @SQL = @SQL + ', [SMTPUserName]'
				SET @SQL = @SQL + ', [SMTPPassword]'
				SET @SQL = @SQL + ', [SMTPPort]'
				SET @SQL = @SQL + ', [SiteWideBottomJavascript]'
				SET @SQL = @SQL + ', [SiteWideTopJavascript]'
				SET @SQL = @SQL + ', [OrderReceiptAffiliateJavascript]'
				SET @SQL = @SQL + ', [SiteWideAnalyticsJavascript]'
				SET @SQL = @SQL + ', [GoogleAnalyticsCode]'
				SET @SQL = @SQL + ', [UPSUserName]'
				SET @SQL = @SQL + ', [UPSPassword]'
				SET @SQL = @SQL + ', [UPSKey]'
				SET @SQL = @SQL + ', [ShippingOriginZipCode]'
				SET @SQL = @SQL + ', [MasterPage]'
				SET @SQL = @SQL + ', [ShopByPriceMin]'
				SET @SQL = @SQL + ', [ShopByPriceMax]'
				SET @SQL = @SQL + ', [ShopByPriceIncrement]'
				SET @SQL = @SQL + ', [FedExAccountNumber]'
				SET @SQL = @SQL + ', [FedExMeterNumber]'
				SET @SQL = @SQL + ', [FedExProductionKey]'
				SET @SQL = @SQL + ', [FedExSecurityCode]'
				SET @SQL = @SQL + ', [FedExCSPKey]'
				SET @SQL = @SQL + ', [FedExCSPPassword]'
				SET @SQL = @SQL + ', [FedExClientProductId]'
				SET @SQL = @SQL + ', [FedExClientProductVersion]'
				SET @SQL = @SQL + ', [FedExDropoffType]'
				SET @SQL = @SQL + ', [FedExPackagingType]'
				SET @SQL = @SQL + ', [FedExUseDiscountRate]'
				SET @SQL = @SQL + ', [FedExAddInsurance]'
				SET @SQL = @SQL + ', [ShippingOriginAddress1]'
				SET @SQL = @SQL + ', [ShippingOriginAddress2]'
				SET @SQL = @SQL + ', [ShippingOriginCity]'
				SET @SQL = @SQL + ', [ShippingOriginStateCode]'
				SET @SQL = @SQL + ', [ShippingOriginCountryCode]'
				SET @SQL = @SQL + ', [ShippingOriginPhone]'
				SET @SQL = @SQL + ', [CurrencyTypeID]'
				SET @SQL = @SQL + ', [WeightUnit]'
				SET @SQL = @SQL + ', [DimensionUnit]'
				SET @SQL = @SQL + ', [EmailListLogin]'
				SET @SQL = @SQL + ', [EmailListPassword]'
				SET @SQL = @SQL + ', [EmailListDefaultList]'
				SET @SQL = @SQL + ', [ShippingTaxable]'
				SET @SQL = @SQL + ', [DefaultOrderStateID]'
				SET @SQL = @SQL + ', [DefaultReviewStatus]'
				SET @SQL = @SQL + ', [DefaultAnonymousProfileID]'
				SET @SQL = @SQL + ', [DefaultRegisteredProfileID]'
				SET @SQL = @SQL + ', [InclusiveTax]'
				SET @SQL = @SQL + ', [SeoDefaultProductTitle]'
				SET @SQL = @SQL + ', [SeoDefaultProductDescription]'
				SET @SQL = @SQL + ', [SeoDefaultProductKeyword]'
				SET @SQL = @SQL + ', [SeoDefaultCategoryTitle]'
				SET @SQL = @SQL + ', [SeoDefaultCategoryDescription]'
				SET @SQL = @SQL + ', [SeoDefaultCategoryKeyword]'
				SET @SQL = @SQL + ', [SeoDefaultContentTitle]'
				SET @SQL = @SQL + ', [SeoDefaultContentDescription]'
				SET @SQL = @SQL + ', [SeoDefaultContentKeyword]'
				SET @SQL = @SQL + ', [TimeZoneOffset]'
				SET @SQL = @SQL + ', [LocaleID]'
				SET @SQL = @SQL + ', [SplashCategoryID]'
				SET @SQL = @SQL + ', [SplashImageFile]'
				SET @SQL = @SQL + ', [MobileTheme]'
				SET @SQL = @SQL + ', [PersistentCartEnabled]'
				SET @SQL = @SQL + ', [DefaultProductReviewStateID]'
				SET @SQL = @SQL + ', [UseDynamicDisplayOrder]'
				SET @SQL = @SQL + ', [EnableAddressValidation]'
				SET @SQL = @SQL + ', [RequireValidatedAddress]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePortal]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [PortalID],'
				SET @SQL = @SQL + ' [CompanyName],'
				SET @SQL = @SQL + ' [StoreName],'
				SET @SQL = @SQL + ' [LogoPath],'
				SET @SQL = @SQL + ' [UseSSL],'
				SET @SQL = @SQL + ' [AdminEmail],'
				SET @SQL = @SQL + ' [SalesEmail],'
				SET @SQL = @SQL + ' [CustomerServiceEmail],'
				SET @SQL = @SQL + ' [SalesPhoneNumber],'
				SET @SQL = @SQL + ' [CustomerServicePhoneNumber],'
				SET @SQL = @SQL + ' [ImageNotAvailablePath],'
				SET @SQL = @SQL + ' [MaxCatalogDisplayColumns],'
				SET @SQL = @SQL + ' [MaxCatalogDisplayItems],'
				SET @SQL = @SQL + ' [MaxCatalogCategoryDisplayThumbnails],'
				SET @SQL = @SQL + ' [MaxCatalogItemSmallThumbnailWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemSmallWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemMediumWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemThumbnailWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemLargeWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemCrossSellWidth],'
				SET @SQL = @SQL + ' [ShowSwatchInCategory],'
				SET @SQL = @SQL + ' [ShowAlternateImageInCategory],'
				SET @SQL = @SQL + ' [ActiveInd],'
				SET @SQL = @SQL + ' [SMTPServer],'
				SET @SQL = @SQL + ' [SMTPUserName],'
				SET @SQL = @SQL + ' [SMTPPassword],'
				SET @SQL = @SQL + ' [SMTPPort],'
				SET @SQL = @SQL + ' [SiteWideBottomJavascript],'
				SET @SQL = @SQL + ' [SiteWideTopJavascript],'
				SET @SQL = @SQL + ' [OrderReceiptAffiliateJavascript],'
				SET @SQL = @SQL + ' [SiteWideAnalyticsJavascript],'
				SET @SQL = @SQL + ' [GoogleAnalyticsCode],'
				SET @SQL = @SQL + ' [UPSUserName],'
				SET @SQL = @SQL + ' [UPSPassword],'
				SET @SQL = @SQL + ' [UPSKey],'
				SET @SQL = @SQL + ' [ShippingOriginZipCode],'
				SET @SQL = @SQL + ' [MasterPage],'
				SET @SQL = @SQL + ' [ShopByPriceMin],'
				SET @SQL = @SQL + ' [ShopByPriceMax],'
				SET @SQL = @SQL + ' [ShopByPriceIncrement],'
				SET @SQL = @SQL + ' [FedExAccountNumber],'
				SET @SQL = @SQL + ' [FedExMeterNumber],'
				SET @SQL = @SQL + ' [FedExProductionKey],'
				SET @SQL = @SQL + ' [FedExSecurityCode],'
				SET @SQL = @SQL + ' [FedExCSPKey],'
				SET @SQL = @SQL + ' [FedExCSPPassword],'
				SET @SQL = @SQL + ' [FedExClientProductId],'
				SET @SQL = @SQL + ' [FedExClientProductVersion],'
				SET @SQL = @SQL + ' [FedExDropoffType],'
				SET @SQL = @SQL + ' [FedExPackagingType],'
				SET @SQL = @SQL + ' [FedExUseDiscountRate],'
				SET @SQL = @SQL + ' [FedExAddInsurance],'
				SET @SQL = @SQL + ' [ShippingOriginAddress1],'
				SET @SQL = @SQL + ' [ShippingOriginAddress2],'
				SET @SQL = @SQL + ' [ShippingOriginCity],'
				SET @SQL = @SQL + ' [ShippingOriginStateCode],'
				SET @SQL = @SQL + ' [ShippingOriginCountryCode],'
				SET @SQL = @SQL + ' [ShippingOriginPhone],'
				SET @SQL = @SQL + ' [CurrencyTypeID],'
				SET @SQL = @SQL + ' [WeightUnit],'
				SET @SQL = @SQL + ' [DimensionUnit],'
				SET @SQL = @SQL + ' [EmailListLogin],'
				SET @SQL = @SQL + ' [EmailListPassword],'
				SET @SQL = @SQL + ' [EmailListDefaultList],'
				SET @SQL = @SQL + ' [ShippingTaxable],'
				SET @SQL = @SQL + ' [DefaultOrderStateID],'
				SET @SQL = @SQL + ' [DefaultReviewStatus],'
				SET @SQL = @SQL + ' [DefaultAnonymousProfileID],'
				SET @SQL = @SQL + ' [DefaultRegisteredProfileID],'
				SET @SQL = @SQL + ' [InclusiveTax],'
				SET @SQL = @SQL + ' [SeoDefaultProductTitle],'
				SET @SQL = @SQL + ' [SeoDefaultProductDescription],'
				SET @SQL = @SQL + ' [SeoDefaultProductKeyword],'
				SET @SQL = @SQL + ' [SeoDefaultCategoryTitle],'
				SET @SQL = @SQL + ' [SeoDefaultCategoryDescription],'
				SET @SQL = @SQL + ' [SeoDefaultCategoryKeyword],'
				SET @SQL = @SQL + ' [SeoDefaultContentTitle],'
				SET @SQL = @SQL + ' [SeoDefaultContentDescription],'
				SET @SQL = @SQL + ' [SeoDefaultContentKeyword],'
				SET @SQL = @SQL + ' [TimeZoneOffset],'
				SET @SQL = @SQL + ' [LocaleID],'
				SET @SQL = @SQL + ' [SplashCategoryID],'
				SET @SQL = @SQL + ' [SplashImageFile],'
				SET @SQL = @SQL + ' [MobileTheme],'
				SET @SQL = @SQL + ' [PersistentCartEnabled],'
				SET @SQL = @SQL + ' [DefaultProductReviewStateID],'
				SET @SQL = @SQL + ' [UseDynamicDisplayOrder],'
				SET @SQL = @SQL + ' [EnableAddressValidation],'
				SET @SQL = @SQL + ' [RequireValidatedAddress]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePortal]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeOrderLineItem]'
GO
ALTER TABLE [dbo].[ZNodeOrderLineItem] ADD
CONSTRAINT [FK_ZNodeOrderLineItem_ZNodeOrderState] FOREIGN KEY ([OrderLineItemStateID]) REFERENCES [dbo].[ZNodeOrderState] ([OrderStateID]),
CONSTRAINT [FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType] FOREIGN KEY ([OrderLineItemRelationshipTypeID]) REFERENCES [dbo].[ZNodeOrderLineItemRelationshipType] ([OrderLineItemRelationshipTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodePortal]'
GO
ALTER TABLE [dbo].[ZNodePortal] ADD
CONSTRAINT [FK_ZNodePortal_ZNodeProductReviewState] FOREIGN KEY ([DefaultProductReviewStateID]) REFERENCES [dbo].[ZNodeProductReviewState] ([ReviewStateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeReferralCommission]'
GO
ALTER TABLE [dbo].[ZNodeReferralCommission] ADD
CONSTRAINT [FK_ZnodeReferralCommission_ZnodeReferralCommissionType] FOREIGN KEY ([ReferralCommissionTypeID]) REFERENCES [dbo].[ZNodeReferralCommissionType] ([ReferralCommissionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeSavedCartLineItem]'
GO
ALTER TABLE [dbo].[ZNodeSavedCartLineItem] ADD
CONSTRAINT [FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType] FOREIGN KEY ([OrderLineItemRelationshipTypeID]) REFERENCES [dbo].[ZNodeOrderLineItemRelationshipType] ([OrderLineItemRelationshipTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO



/*

DataFix Upgrade script.

Script created by SQL Data Compare version 9.0.0 from Red Gate Software Ltd at 7/28/2011 7:15:10 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraint FK_SC_Shipping_SC_ShippingType from [dbo].[ZNodeShipping]
ALTER TABLE [dbo].[ZNodeShipping] DROP CONSTRAINT [FK_SC_Shipping_SC_ShippingType]

-- Drop constraint FK_ZNodeShippingServiceCode_ZNodeShippingType from [dbo].[ZNodeShippingServiceCode]
ALTER TABLE [dbo].[ZNodeShippingServiceCode] DROP CONSTRAINT [FK_ZNodeShippingServiceCode_ZNodeShippingType]

-- Drop constraints from [dbo].[ZNodeProductType]
ALTER TABLE [dbo].[ZNodeProductType] DROP CONSTRAINT [FK_ZNodeProductType_ZNodePortal]

-- Drop constraint FK_SC_Product_SC_ProductType from [dbo].[ZNodeProduct]
ALTER TABLE [dbo].[ZNodeProduct] DROP CONSTRAINT [FK_SC_Product_SC_ProductType]

-- Drop constraint FK_SC_ProductTypeAttribute_SC_ProductType from [dbo].[ZNodeProductTypeAttribute]
ALTER TABLE [dbo].[ZNodeProductTypeAttribute] DROP CONSTRAINT [FK_SC_ProductTypeAttribute_SC_ProductType]

-- Drop constraint FK_ZNodeOrder_ZNodePaymentStatus from [dbo].[ZNodeOrder]
ALTER TABLE [dbo].[ZNodeOrder] DROP CONSTRAINT [FK_ZNodeOrder_ZNodePaymentStatus]

-- Drop constraint FK_ZNodeOrderLineItem_ZNodePaymentStatus from [dbo].[ZNodeOrderLineItem]
ALTER TABLE [dbo].[ZNodeOrderLineItem] DROP CONSTRAINT [FK_ZNodeOrderLineItem_ZNodePaymentStatus]

-- Drop constraint FK_ZNodePromotion_ZNodeDiscountType from [dbo].[ZNodePromotion]
ALTER TABLE [dbo].[ZNodePromotion] DROP CONSTRAINT [FK_ZNodePromotion_ZNodeDiscountType]

-- Add row to [dbo].[ZNodeDiscountType]
INSERT INTO [dbo].[ZNodeDiscountType] ([DiscountTypeID], [ClassType], [ClassName], [Name], [Description], [ActiveInd]) VALUES (11, N'PRICE', N'ZNodePromotionProduct', 'ZNode Promotion Product', N'Override Displayed Product Details', 1)

-- Add row to [dbo].[ZNodePaymentStatus]
INSERT INTO [dbo].[ZNodePaymentStatus] ([PaymentStatusID], [PaymentStatusName], [Description]) VALUES (5, N'CC_PENDING', N'Credit Card Payment was Pending')

-- Add row to [dbo].[ZNodeProductType]
INSERT INTO [dbo].[ZNodeProductType] ([Name], [Description], [DisplayOrder], [PortalID], [IsGiftCard]) VALUES (N'Gift Card Type', N'The default product type to use to sell giftcards.', 10, NULL, 1)

-- Add row to [dbo].[ZNodeShippingType]
INSERT INTO [dbo].[ZNodeShippingType] ([ShippingTypeID], [ClassName], [Name], [Description], [IsActive]) VALUES (4, N'ZNodeUSPS', N'USPS', N'Retrieves shipping rates from www.ups.com', 1)

-- Add constraint FK_SC_Shipping_SC_ShippingType to [dbo].[ZNodeShipping]
ALTER TABLE [dbo].[ZNodeShipping] WITH NOCHECK ADD CONSTRAINT [FK_SC_Shipping_SC_ShippingType] FOREIGN KEY ([ShippingTypeID]) REFERENCES [dbo].[ZNodeShippingType] ([ShippingTypeID])

-- Add constraint FK_ZNodeShippingServiceCode_ZNodeShippingType to [dbo].[ZNodeShippingServiceCode]
ALTER TABLE [dbo].[ZNodeShippingServiceCode] WITH NOCHECK ADD CONSTRAINT [FK_ZNodeShippingServiceCode_ZNodeShippingType] FOREIGN KEY ([ShippingTypeID]) REFERENCES [dbo].[ZNodeShippingType] ([ShippingTypeID])

-- Add constraints to [dbo].[ZNodeProductType]
ALTER TABLE [dbo].[ZNodeProductType] ADD CONSTRAINT [FK_ZNodeProductType_ZNodePortal] FOREIGN KEY ([PortalID]) REFERENCES [dbo].[ZNodePortal] ([PortalID])

-- Add constraint FK_SC_Product_SC_ProductType to [dbo].[ZNodeProduct]
ALTER TABLE [dbo].[ZNodeProduct] WITH NOCHECK ADD CONSTRAINT [FK_SC_Product_SC_ProductType] FOREIGN KEY ([ProductTypeID]) REFERENCES [dbo].[ZNodeProductType] ([ProductTypeId])

-- Add constraint FK_SC_ProductTypeAttribute_SC_ProductType to [dbo].[ZNodeProductTypeAttribute]
ALTER TABLE [dbo].[ZNodeProductTypeAttribute] WITH NOCHECK ADD CONSTRAINT [FK_SC_ProductTypeAttribute_SC_ProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [dbo].[ZNodeProductType] ([ProductTypeId])

-- Add constraint FK_ZNodeOrder_ZNodePaymentStatus to [dbo].[ZNodeOrder]
ALTER TABLE [dbo].[ZNodeOrder] WITH NOCHECK ADD CONSTRAINT [FK_ZNodeOrder_ZNodePaymentStatus] FOREIGN KEY ([PaymentStatusID]) REFERENCES [dbo].[ZNodePaymentStatus] ([PaymentStatusID])

-- Add constraint FK_ZNodeOrderLineItem_ZNodePaymentStatus to [dbo].[ZNodeOrderLineItem]
ALTER TABLE [dbo].[ZNodeOrderLineItem] WITH NOCHECK ADD CONSTRAINT [FK_ZNodeOrderLineItem_ZNodePaymentStatus] FOREIGN KEY ([PaymentStatusID]) REFERENCES [dbo].[ZNodePaymentStatus] ([PaymentStatusID])

-- Add constraint FK_ZNodePromotion_ZNodeDiscountType to [dbo].[ZNodePromotion]
ALTER TABLE [dbo].[ZNodePromotion] WITH NOCHECK ADD CONSTRAINT [FK_ZNodePromotion_ZNodeDiscountType] FOREIGN KEY ([DiscountTypeID]) REFERENCES [dbo].[ZNodeDiscountType] ([DiscountTypeID])
COMMIT TRANSACTION
GO


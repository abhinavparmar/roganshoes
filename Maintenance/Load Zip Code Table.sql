--====================================================
-- INSERT THE VALUES FROM TEXT FILE  
--====================================================

--====================================================
-- CREATED BY : Lekha
-- DATE		  : 29-NOV-2007
--=====================================================

-- Truncate the table before you insert
-- Removes all rows from a table 

TRUNCATE TABLE ZNODEZIPCODE


-- This script will insert the data from a text file into ZNodeZipCode table.
-- Just set the correct path of the text file and execute.

BULK INSERT ZNODEZIPCODE FROM '<YOUR PATH TO ZIP CODE DATA FILE>\5-digit Premiumdata.txt' WITH (FIELDTERMINATOR = ',')



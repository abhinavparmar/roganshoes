﻿namespace ZNode.Libraries.ECommerce.SEO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class RedirectRecord
    {
        private string mappedURL = string.Empty;
        
        private bool isRedirect = false;
        
        public string MappedURL
        {
            get
            {
                return this.mappedURL;
            }
            
            set
            {
                this.mappedURL = value;
            }
        }
        
        public bool IsRedirect
        {
            get
            {
                return this.isRedirect;
            }
            
            set
            {
                this.isRedirect = value;
            }
        }
    }
}

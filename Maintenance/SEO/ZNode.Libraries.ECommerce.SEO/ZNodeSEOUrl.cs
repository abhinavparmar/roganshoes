using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using ZNode.Libraries.DataAccess.Entities;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace ZNode.Libraries.ECommerce.SEO
{

    /// <summary>
    ///  Provides methods to rewrite SEO friendly url
    /// </summary>
    public class ZNodeSEOUrl : ZNodeSeoUrlBase
    {
        # region Constructor
        static DataTable dataTable;
        public ZNodeSEOUrl() { }
        #endregion

        # region Public static methods
        /// <summary>
        ///  Returns the internal rewrite path (real path for seo url path)
        /// </summary>
        /// <param name="currentURL"></param>
        /// <returns></returns>
        public static string RewriteUrlPath(string CurrentUrl, out int localeID)
        {
            string path = System.Web.HttpContext.Current.Request.Url.PathAndQuery.ToLower();
            string queryStrings = System.Web.HttpContext.Current.Request.QueryString.ToString();
            string fileName = CurrentUrl;
            string realUrlPath = "";
            bool isRedirect = false;
            int position = fileName.LastIndexOf("/");

            localeID = ZNodeSEOUrl.LocaleID;

            if (position > 0)
                fileName = CurrentUrl.Substring(position);


            // if product page is requested
            if (path.IndexOf("product.aspx?zpid=") > -1)
            {
                System.Collections.Specialized.NameValueCollection queryStringCollection =
                                                System.Web.HttpContext.Current.Request.QueryString;

                string productId = queryStringCollection["zpid"];

                queryStrings = queryStringCollection.ToString().ToLower().Replace("zpid=" + productId, "");

                fileName += "?zpid=" + productId;
            }
            else if (path.Contains("category.aspx?zcid=")) // category page
            {
                System.Collections.Specialized.NameValueCollection queryStringCollection =
                                                System.Web.HttpContext.Current.Request.QueryString;

                string categoryId = queryStringCollection["zcid"];
                queryStrings = queryStringCollection.ToString().ToLower().Replace("zcid=" + categoryId, "");

                fileName += "?zcid=" + categoryId;
            }
            else if (path.Contains("content.aspx?page=")) //Content page
            {
                System.Collections.Specialized.NameValueCollection queryStringCollection =
                                                System.Web.HttpContext.Current.Request.QueryString;

                string pageName = queryStringCollection["page"];
                queryStrings = queryStringCollection.ToString().ToLower().Replace("page=" + pageName, "");

                fileName += "?page=" + pageName;
            }
            else if (path.Contains("content.aspx?zpgid=")) //Content page
            {
                System.Collections.Specialized.NameValueCollection queryStringCollection =
                                                System.Web.HttpContext.Current.Request.QueryString;

                string pageName = queryStringCollection["zpgid"];
                queryStrings = queryStringCollection.ToString().ToLower().Replace("zpgid=" + pageName, "");

                fileName += "?zpgid=" + pageName;
            }

            // Get real url to rewrite the path
            ZNodeSEOUrl seoFriendlyURL = new ZNodeSEOUrl();
            realUrlPath = seoFriendlyURL.SeoUrlToReal("~" + fileName, out isRedirect);


            if (realUrlPath.Length == 0)
            {
                string regexPattern = "-(c|p|pg)(\\d+)\\.aspx";
                Match matchIds = Regex.Match(fileName, regexPattern);
                string type = string.Empty;
                string id = "0";
                if (matchIds.Success)
                {
                    type = matchIds.Groups[1].ToString();
                    id = matchIds.Groups[2].ToString();
                    // string pageName = matchIds.Groups[3].ToString();

                    switch (type)
                    {
                        case "p":
                            realUrlPath = CheckforAnotherLocale(id, SEOUrlType.Product, out localeID);
                            break;
                        case "c":
                            realUrlPath = CheckforAnotherLocale(id, SEOUrlType.Category, out localeID);
                            break;
                        case "pg":
                            realUrlPath = CheckforAnotherLocale(id, SEOUrlType.ContentPage, out localeID);
                            break;
                    }
                }
            }

            string applicationPath = System.Web.HttpContext.Current.Request.ApplicationPath.ToLower();

            if (realUrlPath.Length > 0)
            {
                // Check for proper Url                    
                if (applicationPath.Equals("/")) { fileName = fileName.Replace("/", ""); }

                StringBuilder properUrl = new StringBuilder();

                if (!isRedirect && queryStrings.Length > 0)
                    queryStrings = "?" + queryStrings;

                properUrl.Append(System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority));
                properUrl.Append(applicationPath);
                properUrl.Append(fileName);
                properUrl.Append(queryStrings.ToLower());

                // Don't rewrite if its not a proper URL
                if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToLower() != (properUrl.ToString()))
                    realUrlPath = "";
            }

            // 301 redirect section
            if (realUrlPath.Length > 0 && isRedirect)
            {
                StringBuilder rewriteUrl = new StringBuilder();

                rewriteUrl.Append(System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority));

                if (!applicationPath.Equals("/"))
                    rewriteUrl.Append(applicationPath);

                rewriteUrl.Append(realUrlPath.Replace("~", ""));
                rewriteUrl.Append(queryStrings.Replace("&", "?"));

                System.Web.HttpContext.Current.Response.Clear();

                // Redirect  
                System.Web.HttpContext.Current.Response.Status = "301 Moved Permanently";

                // Rewrite the Header URL
                System.Web.HttpContext.Current.Response.AddHeader("Location", rewriteUrl.ToString());

                realUrlPath = "";
            }

            return realUrlPath;
        }

        /// <summary>
        /// Caches the URL Mappings in memory for perormance.
        /// </summary>
        public static void CacheUrlMappings()
        {
            // Cache url mapping memory table
            CacheSEOFriendlyUrlMappings();
        }

        /// <summary>
        /// Formats a product/category/content page internal URL link. 
        /// </summary>
        /// <param name="ID">The ProductID, CategoryID or PageID.</param>
        /// <param name="type">Prouct, Category or Page.</param>
        /// <param name="seoURL">An SEO URL to format. Set to an empty string if no SEO URL is available.</param>
        /// <returns>The complete URL with the proper parameters and extensions.</returns>
        public new static string MakeURL(string ID, SEOUrlType type, string seoURL)
        {
            StringBuilder mapUrl = new StringBuilder();

            if (seoURL.Length > 0)
            {
                switch (type)
                {
                    case SEOUrlType.Product:
                        mapUrl.Append(string.Format("~/{0}-p{1}.aspx", RemoveSpecialCharacters(seoURL).Replace(" ", "-"), ID));
                        break;
                    case SEOUrlType.Category:
                        mapUrl.Append(string.Format("~/{0}-c{1}.aspx", RemoveSpecialCharacters(seoURL).Replace(" ", "-"), ID));
                        break;
                    case SEOUrlType.ContentPage:
                        ZNode.Libraries.DataAccess.Service.ContentPageService objService = new ZNode.Libraries.DataAccess.Service.ContentPageService();
                        ContentPage obj = objService.GetByNamePortalIDLocaleId(ID, ZNodeConfigManager.SiteConfig.PortalID, LocaleID);
                        if (obj != null)
                        {
                            mapUrl.Append(string.Format("~/{0}-pg{1}.aspx", RemoveSpecialCharacters(obj.Name).Replace(" ", "-"), obj.ContentPageID));
                        }
                        break;
                }

            }
            else
            {
                switch (type)
                {
                    case SEOUrlType.Product:
                        mapUrl.Append("~/product.aspx?zpid=" + ID); break;

                    case SEOUrlType.Category:
                        mapUrl.Append("~/category.aspx?zcid=" + ID); break;

                    case SEOUrlType.ContentPage:
                        ZNode.Libraries.DataAccess.Service.ContentPageService objService = new ZNode.Libraries.DataAccess.Service.ContentPageService();
                        ContentPage obj = objService.GetByNamePortalIDLocaleId(ID, ZNodeConfigManager.SiteConfig.PortalID, LocaleID);
                        if (obj != null)
                        {
                            mapUrl.Append("~/content.aspx?zpgid=" + obj.ContentPageID);
                        }
                        break;
                }
            }


            return mapUrl.ToString();
        }

        public static string RemoveSpecialCharacters(string str)
        {
            str = System.Web.HttpUtility.HtmlDecode(str);
            str = Regex.Replace(str, "[^a-zA-Z0-9_&\\-]+", " ", RegexOptions.Compiled);
            return Regex.Replace(str, "[&]", "and", RegexOptions.Compiled);
        }
        #endregion

        #region Base Override methods

        /// <summary>
        /// Retrives or sets the data table to/from Cache object
        /// </summary>
        protected new DataTable SEOUrlMapping
        {
            get
            {
                if (System.Web.HttpRuntime.Cache[CacheKey()] == null)
                {
                    // if cache object is null, then load data table from database and add it to the cache object
                    CacheSEOFriendlyUrlMappings();
                }

                dataTable = (DataTable)System.Web.HttpRuntime.Cache.Get(CacheKey());

                return dataTable;
            }
            set
            {
                ZNodeCacheDependencyManager.Insert(CacheKey(), value, "ZNodeProduct", "ZNodeCategory", "ZNodeContentPage", "ZNodeProductCategory", "ZNodeCategoryNode", "ZNodePortalCatalog", "ZNodeUrlRedirect");
            }
        }

        # region Protected methods

        /// <summary>
        /// Creates SEO url mapping data table and add to the cache object
        /// </summary>
        public new static DataTable CreateSeoFirendlyUrlTable()
        {
            // Initialize new datatable
            DataTable seoUrlDataTable = new DataTable("SeoUrlMappings");
            DataColumn[] keys = new DataColumn[1];

            // initiazlie new data columns
            DataColumn dataColumn1 = new DataColumn("SeoUrl", System.Type.GetType("System.String"));
            DataColumn dataColumn2 = new DataColumn("MappedUrl", System.Type.GetType("System.String"));
            DataColumn dataColumn3 = new DataColumn("IsRedirect", System.Type.GetType("System.Boolean"));
            dataColumn3.DefaultValue = "false"; // Default value for the column 'IsRedirect'            

            // Add the column to the DataTable.Columns collection.
            seoUrlDataTable.Columns.Add(dataColumn1);
            seoUrlDataTable.Columns.Add(dataColumn2);
            seoUrlDataTable.Columns.Add(dataColumn3);

            // Add the column to the array.
            keys[0] = dataColumn1;

            // Set the PrimaryKey property to the array
            seoUrlDataTable.PrimaryKey = keys;

            return seoUrlDataTable;
        }

        /// <summary>
        /// Add new row to the SEOUrlMapping table
        /// </summary>
        /// <param name="Id">The ProductID, CategoryID or PageID.</param>
        /// <param name="UrlType">Product, Category or Page.</param>
        /// <param name="SeoUrl">The SEO URL.</param>
        /// <param name="DataTable">The in memory data table.</param>
        /// <returns>True if the URL mapping was successfully added.</returns>
        private static bool Add(string Id, SEOUrlType UrlType, string SeoUrl, DataTable DataTable)
        {
            Add(Id, UrlType, SeoUrl, DataTable, false);

            return true;
        }

        /// <summary>
        /// Add 301 Redirect Url to the memory table
        /// </summary>
        /// <param name="OldUrl">The old URL to map from.</param>
        /// <param name="NewUrl">The new URL to map to.</param>
        /// <param name="DataTable">A reference to the data table where the URL rewrites are kept.</param>
        /// <returns></returns>
        protected new static bool AddRedirectUrl(string OldUrl, string NewUrl, DataTable DataTable)
        {
            DataRow dataRow = dataTable.NewRow();
            DataRow dr = dataTable.Rows.Find(OldUrl);
            if (dr != null)
            {
                dr["MappedUrl"] = NewUrl;
                dr["IsRedirect"] = true;
                dr.AcceptChanges();
            }
            else
            {
                dataRow["SeoUrl"] = OldUrl;
                dataRow["MappedUrl"] = NewUrl;
                dataRow["IsRedirect"] = true;

                DataTable.Rows.Add(dataRow);
            }
            return true;
        }

        /// <summary>
        /// Add new row to the SEOUrlMapping table
        /// </summary>
        /// <param name="Id">The ProductID, CategoryID or PageID.</param>
        /// <param name="UrlType">Product, Category or Page.</param>
        /// <param name="SeoUrl">The SEO URL.</param>
        /// <param name="DataTable">The in memory data table.</param>
        /// <param name="IsRedirect">Set to True to indicate that this is a 301 redirect.</param>
        /// <returns>True if the URL mapping was successfully added.</returns>
        protected new static bool Add(string Id, SEOUrlType UrlType, string SeoUrl, DataTable DataTable, bool IsRedirect)
        {
            DataRow dataRow = DataTable.NewRow();

            dataRow["SeoUrl"] = MakeURL(Id, UrlType, SeoUrl);
            dataRow["MappedUrl"] = MakeURL(Id, UrlType, "");
            dataRow["IsRedirect"] = IsRedirect.ToString();

            DataTable.Rows.Add(dataRow);

            return true;
        }

        /// <summary>
        /// Retrives SEO Url field value from Product, Category, and Content tables and added to the memory table
        /// </summary>
        protected new static void CacheSEOFriendlyUrlMappings()
        {

            if (HttpRuntime.Cache[CacheKey()] == null)
            {
                // Create memory table and add it to cache object
                dataTable = CreateSeoFirendlyUrlTable();
            }
            else
            {
                dataTable = (DataTable)HttpRuntime.Cache[CacheKey()];
            }

            // If total no.of rows in the datable is 0,then fill it up with database values
            if (dataTable.Rows.Count == 0)
            {
                if (ZNodeConfigManager.SiteConfig != null)
                {
                    # region Get Seo friendly urls from database

                    DataSet SeoUrlMappings = GetSEOUrlListByPortalId(ZNodeConfigManager.SiteConfig.PortalID, LocaleID);

                    foreach (DataRow productRow in SeoUrlMappings.Tables[0].Rows)
                    {
                        string seoURL = productRow["SEOURL"].ToString();
                        if (seoURL.Length > 0)
                        {
                            Add(productRow["ProductID"].ToString(), SEOUrlType.Product, seoURL, dataTable);
                        }
                    }

                    foreach (DataRow categoryRow in SeoUrlMappings.Tables[1].Rows)
                    {
                        string seoURL = categoryRow["SEOURL"].ToString();
                        if (seoURL.Length > 0)
                        {
                            Add(categoryRow["CategoryID"].ToString(), SEOUrlType.Category, seoURL, dataTable);
                        }
                    }

                    foreach (DataRow contentPageRow in SeoUrlMappings.Tables[2].Rows)
                    {
                        string seoURL = contentPageRow["SEOURL"].ToString();
                        if (seoURL.Length > 0)
                        {
                            Add(contentPageRow["Name"].ToString(), SEOUrlType.ContentPage, seoURL, dataTable);
                        }
                    }

                    foreach (DataRow urlRedirectRow in SeoUrlMappings.Tables[3].Rows)
                    {
                        AddRedirectUrl(urlRedirectRow["OldUrl"].ToString(), urlRedirectRow["NewUrl"].ToString(), dataTable);
                    }
                    #endregion
                }

                // Adds the Urlmappings table item to the Cache object            
                ZNodeCacheDependencyManager.Insert(CacheKey(), dataTable, "ZNodeProduct", "ZNodeCategory", "ZNodeContentPage", "ZNodeProductCategory", "ZNodeCategoryNode", "ZNodePortalCatalog", "ZNodeUrlRedirect", "ZNodeDomain");
            }
        }

        # endregion

        # region Public Methods

        /// <summary>
        /// Add new row to the SEOUrlMapping table
        /// </summary>
        /// <param name="ID">The ProductID, CategoryID or PageID.</param>
        /// <param name="type">Product, Cateogry or Page.</param>
        /// <param name="SeoUrl">The SEO URL.</param>        
        /// <returns>True if the SEO URL was successfully added.</returns>
        public new bool Add(string ID, SEOUrlType type, string SeoUrl)
        {
            return Add(ID, type, SeoUrl, false);
        }

        /// <summary>
        /// Returns actual product/category/content page url for this SEO Url
        /// </summary>
        /// <param name="SeoUrl">The friendly URL to translate into a real URL.</param>
        /// <param name="IsRedirect">Set to true if this should be a 301 redirect instead of a rewrite of the URL.</param>
        /// <returns>The real URL or the SEOUrl. If a real URL can't be found and empty string is returned.</returns>
        public new string SeoUrlToReal(string SeoUrl, out bool IsRedirect)
        {
            dataTable = SEOUrlMapping;

            if (dataTable != null)
            {
                DataRow dr = dataTable.Rows.Find(SeoUrl);

                if (dr != null)
                {
                    //                     
                    IsRedirect = bool.Parse(dr["IsRedirect"].ToString());

                    return dr["MappedUrl"].ToString();
                }
            }

            IsRedirect = false;
            return string.Empty;
        }

        #endregion

        #endregion

        #region Check for Another Locale

        /// <summary>
        /// Returns actual product/category/content page url for this SEO Url
        /// </summary>
        /// <param name="SeoUrl">The friendly URL to translate into a real URL.</param>
        /// <param name="IsRedirect">Set to true if this should be a 301 redirect instead of a rewrite of the URL.</param>
        /// <returns>The real URL or the SEOUrl. If a real URL can't be found and empty string is returned.</returns>
        public static string CheckforAnotherLocale(string ID, SEOUrlType type, out int localeID)
        {
            localeID = ZNodeSeoUrlBase.LocaleID;
            string mappedUrl = string.Empty;

            DataSet dataSet = CacheUrlMappingsByPortal();

            switch (type)
            {
                case SEOUrlType.Product:
                    DataTable dataTable = dataSet.Tables[0];
                    if (dataTable != null)
                    {
                        DataRow[] dr = dataTable.Select(string.Format("[ProductID]='{0}'", ID));

                        if (dr.Length > 0)
                        {
                            if (dr[0]["LocaleID"] != System.DBNull.Value)
                                localeID = int.Parse(dr[0]["LocaleID"].ToString());

                            return System.Web.HttpContext.Current.Request.Url.ToString();
                        }
                    }
                    break;
                case SEOUrlType.Category:
                    dataTable = dataSet.Tables[1];
                    if (dataTable != null)
                    {
                        DataRow[] dr = dataTable.Select(string.Format("[CategoryID]='{0}'", ID));

                        if (dr.Length > 0)
                        {

                            if (dr[0]["LocaleID"] != System.DBNull.Value)
                                localeID = int.Parse(dr[0]["LocaleID"].ToString());

                            return System.Web.HttpContext.Current.Request.Url.ToString();
                        }
                    }
                    break;
                case SEOUrlType.ContentPage:
                    dataTable = dataSet.Tables[2];
                    if (dataTable != null)
                    {
                        DataRow[] dr = dataTable.Select(string.Format("[ContentPageID]='{0}'", ID));

                        if (dr.Length > 0)
                        {
                            if (dr[0]["LocaleID"] != System.DBNull.Value)
                                localeID = int.Parse(dr[0]["LocaleID"].ToString());

                            return System.Web.HttpContext.Current.Request.Url.ToString();
                        }
                    }
                    break;
            }
            return mappedUrl;
        }

        /// <summary>
        /// Caches the URL Mappings in memory for perormance.
        /// </summary>
        private static DataSet CacheUrlMappingsByPortal()
        {
            DataSet dataSet;
            if (System.Web.HttpRuntime.Cache["PortalSEOUrlMappingsCache" + ZNodeConfigManager.SiteConfig.PortalID.ToString()] == null)
            {
                // if cache object is null, then load data table from database and add it to the cache object
                dataSet = GetSEOUrlListByPortalId(ZNodeConfigManager.SiteConfig.PortalID, 0);

                //// Adds the Urlmappings table item to the Cache object
                ZNodeCacheDependencyManager.Insert("PortalSEOUrlMappingsCache" + ZNodeConfigManager.SiteConfig.PortalID.ToString(), dataSet, "ZNodeProduct", "ZNodeCategory", "ZNodeContentPage", "ZNodeProductCategory", "ZNodeCategoryNode", "ZNodePortalCatalog", "ZNodeUrlRedirect", "ZNodeDomain");
            }

            dataSet = (DataSet)System.Web.HttpRuntime.Cache.Get("PortalSEOUrlMappingsCache" + ZNodeConfigManager.SiteConfig.PortalID.ToString());

            return dataSet;

        }

        #endregion
    }
}

namespace RogansShoes.Web.API.Configuration
{
    public interface IIocConfiguration
    {
        void Configure();
    }
}
﻿namespace RogansShoes.Web.API.Configuration
{
    public static class Bootstrapper
    {
        public static void Bootstrap(IIocConfiguration iocConfiguration)
        {
            iocConfiguration.Configure();
        }
    }
}

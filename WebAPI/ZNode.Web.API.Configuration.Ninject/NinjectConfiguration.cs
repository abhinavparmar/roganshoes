﻿using System;
using Microsoft.Practices.ServiceLocation;
using NinjectAdapter;
using Ninject;

namespace RogansShoes.Web.API.Configuration.Ninject
{
    public class NinjectConfiguration : IIocConfiguration
    {
        public void Configure()
        {
            IKernel kernel = new StandardKernel(new SerializationModule());
            ServiceLocator.SetLocatorProvider(() => new NinjectServiceLocator(kernel));
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace RogansShoes.Web.API.Support
{
    public static class ApiModelExtensions
    {
        public static string DecodeAsUrlSafeBase64(this string text)
        {
            return Base64UrlService.Default.Decode(text);
        }

        public static string EncodeAsUrlSafeBase64(this string text)
        {
            return Base64UrlService.Default.Encode(text);
        }

        public static IEnumerable<T> GetItemsOrEmpty<T>(this IEnumerable<T> instance) 
            where T : class 
        {
            var result = instance ?? Enumerable.Empty<T>();
            return result;
        }

        public static TValue ValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> instance, TKey key)
        {
            return instance.ContainsKey(key) ? instance[key] : default(TValue);
        }
    }
}
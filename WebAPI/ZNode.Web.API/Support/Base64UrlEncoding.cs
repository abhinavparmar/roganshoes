﻿using System;
using System.Text;

namespace RogansShoes.Web.API.Support
{
	/// <summary>
	/// Encoding/decoding uses the url & filename safe alphabet as defined in RFC4648.
	/// <see cref="">http://www.ietf.org/rfc/rfc4648.txt</see>
	/// </summary>
	public class Base64UrlService
	{
		#region properties

		public static Base64UrlService Default { get; private set; }
		
		#endregion

		#region ctor

		static Base64UrlService()
		{
			Default = new Base64UrlService();
		}
		
		#endregion

		public string Encode(string text)
		{
			var buffer = ASCIIEncoding.Default.GetBytes(text);
			return GetString(buffer);
		}

		public string Decode(string text)
		{
			var buffer = GetBytes(text);
			return ASCIIEncoding.Default.GetString(buffer);
		}

		#region methods

		protected string GetString(byte[] buffer)
		{
			var text = Convert.ToBase64String(buffer, 0, buffer.Length);
			text = text.Replace("+", "-").Replace("/", "_");

			return text;
		}

		protected byte[] GetBytes(string text)
		{
			text = text.Replace("-", "+").Replace("_", "/");
			var buffer = Convert.FromBase64String(text);

			return buffer;
		}
		 
		#endregion
	}
}
﻿using System;

namespace RogansShoes.Web.API.Support
{
    public class BadDataException : Exception
    {
        public BadDataException() { }
        public BadDataException(string message) : base(message) { }
    }
    
    public class CircularReferenceException : Exception
    {
        public CircularReferenceException() { }
        public CircularReferenceException(string message) : base(message) { }
    }

    public class ResourceNotFoundException : Exception
    {
        public ResourceNotFoundException() { }
        public ResourceNotFoundException(string message) : base(message) { }
    }

    public class RouteEntityIdMismatchException : Exception
    {
        public RouteEntityIdMismatchException() { }
        public RouteEntityIdMismatchException(string message) : base(message) { }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZNode.Libraries.Framework.Business;
using RogansShoes.Web.API.Data;

namespace RogansShoes.Web.API.Support
{
    public static class Utility
    {
        public static TResult SafeGet<T, TResult>(this T instance, Func<T, TResult> nonNullResultFunction, Func<TResult> nullResultFunction)
            where T: class
        {
            var result = default(TResult);

            if (instance != null && nonNullResultFunction != null)
                result = nonNullResultFunction(instance);
            else if (instance == null && nullResultFunction != null)
                result = nullResultFunction();
            return result;
        }

        public static TResult SafeGet<T, TResult>(this T instance, Func<T, TResult> nonNullResultFunction)
            where T : class
        {
            var result = default(TResult);

            if (instance != null && nonNullResultFunction != null)
                result = nonNullResultFunction(instance);
            return result;
        }

        public static T SafeUse<T>(this T instance, Action<T> nonNullAction)
        {
            if (instance != null && nonNullAction != null)
                nonNullAction(instance);
            return instance;
        }

        public static IEnumerable<T> GetPage<T>(this IEnumerable<T> source, int pageSize, int pageNumber)
        {
            if (pageNumber < 1) pageNumber = 1;
            var pageContent = source.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return pageContent;
        }

        private static ZNodeEncryption _zNodeEncryption = new ZNodeEncryption();

        public static string Decrypt(string value)
        {
            return _zNodeEncryption.DecryptData(value);
        }

        public static string Encrypt(string value)
        {
            return _zNodeEncryption.EncryptData(value);
        }

        public static List<T> OrderByDescOrAsc<T>( List<T> entityList,List<string> ordering )
        {
            var returnList = entityList;
            if (ordering.First() == string.Empty) return entityList;
            var orderby = OrderBy.ASC;
            if (ordering.Last().Equals(OrderBy.DESC, StringComparison.InvariantCultureIgnoreCase))
            {
                orderby = OrderBy.DESC;
                ordering.Remove(ordering.Last());
            }

            if (ordering.Any(order => typeof(T).GetProperties().FirstOrDefault(x => x.Name.Equals(order, StringComparison.InvariantCultureIgnoreCase)) == null))
            {
                throw new InvalidOperationException("Ordering type is invalid");
            }

            var sanitizedInput = (from propertyName in ordering
                                  from propertyInfo in typeof(T).GetProperties()
                                  where propertyName.Equals(propertyInfo.Name, StringComparison.InvariantCultureIgnoreCase)
                                  select propertyInfo.Name).ToList();

            if (sanitizedInput.Any())
            {
                if (orderby.Equals(OrderBy.ASC, StringComparison.InvariantCultureIgnoreCase))
                {
                    var ascending = entityList.OrderBy(x => x.GetType().GetProperty(sanitizedInput.First()).GetValue(x, null));
                    for (var i = 1; i < sanitizedInput.Count; i++)
                    {
                        var j = i;
                        ascending = ascending.ThenBy(x => x.GetType().GetProperty(sanitizedInput[j]).GetValue(x, null));
                    }
                    return ascending.ToList();
                }
                var descending =
                    entityList.OrderByDescending(x => x.GetType().GetProperty(sanitizedInput.First()).GetValue(x, null));
                for (var i = 1; i < sanitizedInput.Count; i++)
                {
                    var j = i;
                    @descending = @descending.ThenByDescending(x => x.GetType().GetProperty(sanitizedInput[j]).GetValue(x, null));
                }
                return @descending.ToList();
            }
            return returnList;
        }
    }
}
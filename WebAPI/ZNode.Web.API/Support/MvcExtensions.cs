﻿using System.Web;
using System.Web.Mvc;

namespace RogansShoes.Web.API.Support
{
    public static class MvcExtensions
    {
        public static string GetHeaderValue(this HttpContextBase httpContext, string headerKey)
        {
            var headers = httpContext.SafeGet(c => c.Request).SafeGet(r => r.Headers);

            var headerValue = headers.SafeGet(h => h[headerKey]);

            return headerValue;
        }

        public static string GetHeaderValue(this ControllerBase controller, string headerKey)
        {
            var httpContext = controller.SafeGet(c => c.ControllerContext).SafeGet(cc => cc.HttpContext);

            var headerValue = httpContext.GetHeaderValue(headerKey);

            return headerValue;
        }
    }
}
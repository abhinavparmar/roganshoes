﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace RogansShoes.Web.API.Support
{
    public class CryptoService
    {
        #region fields & constants

        protected const string DEFAULT_KEY = "1b20059f-70ef-4501-870b-c1ebd94d8cd2";
        protected const string DEFAULT_IV = "a16ef4b5-6054-48d8-acaf-702590bcc117";

        protected SymmetricAlgorithm _crypto;

        protected byte[] _key;
        protected byte[] _iv;

        #endregion

        #region properties

        public static CryptoService Default { get; private set; }

        #endregion

        #region ctor

        static CryptoService()
        {
            Default = new CryptoService();
        }

        public CryptoService()
            : this(new DESCryptoServiceProvider())
        {
        }

        public CryptoService(SymmetricAlgorithm provider)
            : this(provider, DEFAULT_KEY, DEFAULT_IV)
        {
        }

        public CryptoService(SymmetricAlgorithm provider, string key, string iv)
        {
            _crypto = provider;

            int keySize = _crypto.KeySize / 8;

            _key = Sha1(key).Take(keySize).ToArray();
            _iv = Sha1(iv).Take(keySize).ToArray();
        }

        #endregion

        public string Encrypt(string text)
        {
            var transform = _crypto.CreateEncryptor(_key, _iv);
            var result = ApplyCryptoTransform(transform, text);
            return result;
        }

        public string Decrypt(string text)
        {
            var transform = _crypto.CreateDecryptor(_key, _iv);
            var result = ApplyCryptoTransform(transform, text);
            return result;
        }

        #region methods

        /// <summary>
        /// Get the SHA1 hash of the clear text.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        protected byte[] Sha1(string text)
        {
            var crypto = new SHA1CryptoServiceProvider();
            var buffer = ASCIIEncoding.Default.GetBytes(text);
            return crypto.ComputeHash(buffer);
        }

        /// <summary>
        /// Apply the cryptographic transform to the text.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="crypto"></param>
        /// <returns></returns>
        protected string ApplyCryptoTransform(ICryptoTransform crypto, string text)
        {
            var inBuffer = ASCIIEncoding.Default.GetBytes(text);
            var outBuffer = crypto.TransformFinalBlock(inBuffer, 0, inBuffer.Length);

            return ASCIIEncoding.Default.GetString(outBuffer);
        }

        #endregion
    }
}
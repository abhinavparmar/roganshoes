﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RogansShoes.Web.API.Helper
{
    public class OrderDBHelper
    {
        /// <summary>
        /// Update Error Log Instance
        /// </summary>
        /// <param name="orderID">int</param>
        /// <param name="orderLineItemId">int</param>
        /// <param name="sku">string</param>
        /// <param name="exception">string</param>
        public static void ZeonApiOrderUpdateErrorLogInsert(int orderID, int orderLineItemId, string sku, string exception)
        {
            try 
            {
                SQLHelper sqlHelper = new SQLHelper();
                sqlHelper.AddParameterToSQLCommand("@OrderID", orderID);
                sqlHelper.AddParameterToSQLCommand("@OrderLineItemID", orderLineItemId);
                sqlHelper.AddParameterToSQLCommand("@SKU", sku);
                sqlHelper.AddParameterToSQLCommand("@Exception", exception);
                sqlHelper.GetExecuteNonQueryByCommand("Zeon_ZeonApiOrderUpdateErrorLogInsert");
            }
            catch (Exception ex)
            {
                ZNode.Libraries.Framework.Business.ZNodeLoggingBase.LogMessage("Error in ZeonApiOrderUpdateErrorLogInsert::Message::" + ex.ToString());
            }

        }

        /// <summary>
        /// Get Get API Order By Status
        /// </summary>
        /// <param name="orderStateID">string</param>
        /// <returns>List<string</returns>
        public static List<string> GetAPIOrderByStatus(string orderStateID)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var orderIds = new List<string>();
                DataTable dtOrderIDs = new DataTable();
                int ordStateID = int.Parse(orderStateID.ToString());
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetAPIOrderByStatus", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@orderStateID", ordStateID);

                connection.Open();
                adapter.Fill(dtOrderIDs);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Read the datatable.
                if (dtOrderIDs != null && dtOrderIDs.Rows != null && dtOrderIDs.Rows.Count > 0)
                {
                    foreach (DataRow row in dtOrderIDs.Rows)
                    {
                        if (row["OrderID"] != null)
                        {
                            orderIds.Add(row["OrderID"].ToString());
                        }
                    }
                }
                return orderIds;
            }
        }

        /// <summary>
        /// Retrive All Order within Date Range
        /// </summary>
        /// <param name="startDate">string</param>
        /// <param name="endDate">string</param>
        /// <returns>List<string></returns>
        public static List<string> GetOrderIDSByDateRange(DateTime ordFrom, DateTime ordTo)
        {
            using (SqlConnection sqlCon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                List<string> ordIDList = new List<string>();
                DataTable dtOrdList = new DataTable();

                //Create Commnad object Instance
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetAPIOrderIDByDateRange", sqlCon);
                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@StartDate", ordFrom);
                adapter.SelectCommand.Parameters.AddWithValue("@EndDate", ordTo);

                sqlCon.Open();
                adapter.Fill(dtOrdList);

                // Release the resources
                adapter.Dispose();
                sqlCon.Close();

                //Readind Data From Data Table and Insert Into List
                // Read the datatable.
                if (dtOrdList != null && dtOrdList.Rows != null && dtOrdList.Rows.Count > 0)
                {
                    foreach (DataRow row in dtOrdList.Rows)
                    {
                        if (row["OrderID"] != null)
                        {
                            ordIDList.Add(row["OrderID"].ToString());
                        }
                    }
                }
                return ordIDList;
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace RogansShoes.Web.API.Helper
{
    /// <summary>
    /// Handles the parsing of HTML templates.
    /// </summary>
    /// <example>
    ///     HTMLTemplate tmpl = new HTMLTemplate;
    ///     tmpl.Path = @"C:\mytemplate.html";
    ///     template.Parse("LineItems", dsLineItems.CreateDataReader());
    /// </example>
    /// <remarks>You should always parse the repeating sections first!</remarks>
    public class ZNodeHtmlTemplate 
    {
        protected String _templatePath = "";
        protected Object[,] _code;
        protected String _output = "";

        /// <summary>
        /// The path to the template.
        /// </summary>
        public string Path
        {
            get { return _templatePath; }
            set
            {
                _templatePath = value;

                StreamReader sr;
                sr = new StreamReader(_templatePath);
                _output = sr.ReadToEnd();
                sr.Close();
            }
        }

        /// <summary>
        /// The parsed string.
        /// </summary>
        public string Output
        {
            get { return _output; }
        }


        /// <summary>
        /// Substitues the data into template variables.
        /// </summary>
        /// <param name="rdrData">The data to substitue in. Assumes there is only one row.</param>
        public void Parse(DataTableReader rdrData)
        {
            if (rdrData.Read())
            {
                Replace(ref _output, ref rdrData);
                
            }
        }

        /// <summary>
        /// Replaces a single string value without any formatting
        /// </summary>
        /// <param name="rdrData"></param>
        public void Parse(string fieldName, string fieldValue)
        {
            string tempFieldName = "{" + fieldName + "}";

            _output = _output.Replace(tempFieldName, fieldValue);
        }


        /// <summary>
        /// Replaces template variables with a repeating block of data.
        /// </summary>
        /// <param name="RepeatSection">The HTML ID of the element that you would like to repeat.
        ///     Everything within the element's block will be repeated including other tags.</param>
        /// <param name="rdrData">The data to be subtituted in.</param>
        public void Parse(string RepeatSection, DataTableReader rdrData)
        {
            string origHtmlBlock;
            string repeatBlock;
            HtmlParser origHtml = new HtmlParser (_output);

            RepeatSection = "{" + RepeatSection + "}";

            // Find where we are to repeat.
            int curIndx = _output.IndexOf(RepeatSection);
            origHtml.CurIndex = curIndx;
            origHtml.ParseHtml();

            // Save off our original block of code.
            origHtmlBlock = origHtml.HtmlBlock;

            // Get the block of code that will be repeated.
            // repeatBlock = origHtml.HtmlBlockContents;
            repeatBlock = origHtmlBlock.Replace(RepeatSection, "");

            // Start off a buffer for out output.
            StringBuilder newOutput = new StringBuilder(_output.Length);
            //newOutput.Append(origHtml.BeginTag);

            // Add the repeat blocks and substitue the data.
            string tempRepeat = "";
            while (rdrData.Read())
            {
                tempRepeat = repeatBlock;
                Replace(ref tempRepeat, ref rdrData);

                newOutput.Append(tempRepeat);
            }

            newOutput.Append(origHtml.EndTag);

            // Write our new block back to our output string.
            if (origHtmlBlock.Length > 0 && newOutput.Length>0)
            {
                _output = _output.Replace(origHtmlBlock, newOutput.ToString());
            }
        }

        /// <summary>
        /// Replaces template variables in the string.
        /// </summary>
        /// <param name="output">The string in which you want to make the substitutions.</param>
        /// <param name="rdrData">The data you would like to substitue in.</param>
        protected void Replace(ref string output, ref DataTableReader rdrData)
        {
            for (int i = 0; i < rdrData.FieldCount; i++)
            {
                string fieldName = rdrData.GetName(i);
                string baseFieldName = fieldName;

                // Build up a regular expression to replace the field name.
                fieldName = @"\{" + fieldName + @".*?\}";
                Regex regex = new Regex(fieldName, RegexOptions.IgnoreCase);

                MatchCollection matches = regex.Matches(output);

                foreach (Match match in matches)
                {
                    // Get the formatting.
                    string type = "";
                    char[] sep = { '.' };
                    string[] field = match.ToString().Split(sep);
                    if (field.Length > 1)
                    {
                        type = field[field.Length - 1];

                        // Remove the trailing "}".
                        type = type.Substring(0, type.Length - 1);
                    }

                    // Try and format the value based on the template. This may fail if the 
                    // template writer has specified an inapproprate format type 
                    // (i.e. formatting a string to currancy).
                    string newVal = " *** " + match.ToString() + " *** ";
                    try
                    {
                        if (type.Length > 0)
                        {
                            if (DBNull.Value != rdrData[i])
                            {
                                newVal = String.Format("{0:" + type + "}", rdrData[i]);
                            }
                        }
                        else
                        {
                            newVal = rdrData[i].ToString();
                        }
                    }
                    catch
                    {
                    }

                    output = output.Replace(match.ToString(), newVal);
                }
            }
        }

    }

    /// <summary>
    /// Various functions for parsing HTML blocks.
    /// </summary>
    /// <example>
    ///     Html origHtml = new Html(_output);
    ///     origHtml.CurIndex = curIndx; // Set where to be within the block we want to parse.
    ///     origHtml.ParseHtml();
    ///     string myBlock = origHtml.HtmlBlock;
    /// </example>
    public class HtmlParser
    {
        string _html = "";
        int _curIndex = 0;
        int _blockStart = 0;
        int _blockEnd = 0;
        int _contentStart = 0;
        int _contentEnd = 0;

        /// <summary>
        /// The HTML we are going to parse.
        /// </summary>
        public string HtmlTxt
        {
            get { return _html; }
            set 
            { 
                _html = value;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="html">The original HTML string to parse.</param>
        public HtmlParser(string html)
        {
            HtmlTxt = html;
        }

        /// <summary>
        /// An index into the HTML to start our parsing. 
        /// </summary>
        public int CurIndex
        {
            get { return _curIndex; }
            set { _curIndex = value; }
        }

        /// <summary>
        /// Returns the HTML block that surrounds the CurIndex.
        /// </summary>
        public string HtmlBlock
        {
            get
            {
                string ret = "";
                if (_blockEnd > _blockStart)
                {
                    ret = _html.Substring(_blockStart, _blockEnd - _blockStart);
                }

                return ret;
            }
        }

        /// <summary>
        /// The HTML surrounded by the HTML Block.
        /// </summary>
        public string HtmlBlockContents
        {
            get
            {
                string ret = "";
                if (_contentEnd > _blockStart)
                {
                    ret = _html.Substring(_contentStart, _contentEnd - _contentStart);
                }

                return ret;
            }
        }

        /// <summary>
        /// The beginning tag of the HTML Block.
        /// </summary>
        public string BeginTag
        {
            get
            {
                string ret = "";
                if (_contentEnd > _blockStart)
                {
                    ret = _html.Substring(_blockStart, _contentStart - _blockStart);
                }

                return ret;
            }
        }

        /// <summary>
        /// The ending tag of the HTML Block.
        /// </summary>
        public string EndTag
        {
            get
            {
                string ret = "";
                if (_blockEnd > _contentEnd)
                {
                    ret = _html.Substring(_contentEnd + 1, _blockEnd - _contentEnd + 1);
                }

                return ret;
            }
        }

        /// <summary>
        /// Given an index into an HTML string, this function finds the surrounding
        /// HTML block (that part including and between "<tag>" and "</tag>".
        ///
        /// </summary>
        /// <param name="html">The HTML you want to parse.</param>
        /// <param name="index">The index into the HTML that you want to start at.</param>
        /// <returns>The HTML block surrounding the the index.</returns>
        public void ParseHtml()
        {
            Regex regexStart;
            Regex regexEnd;
            string substr;

            // As an example, assume our HTML block looks like:
            // <table><tr bgcolor='white'><td>abcd</td></tr>
            //            ^
            // Assume our _curIndex is 11 which points to the "b" in bgcolor.

            // Back up to the beginning of our section and find its _curIndex.
            // Should point to the beginning of "<tr" in our example.
            if (_curIndex > 0)
            {
                substr = _html.Substring(0, _curIndex);
                _blockStart = substr.LastIndexOf('<');
            }
            else
            {
                return;
            }

            // Calculate where the beginning  of the content is (<td>abcd</td>).
            _contentStart = _html.IndexOf('>', _blockStart) + 1;

            // Get the tag name of this section ("tr" in our example).
            regexStart = new Regex(@"\w*");
            Match startTag = regexStart.Match(_html, _blockStart + 1);

            // Now build the end tag ("</tr>" in our example) and find its _curIndex.
            // Make sure we pick up the last index and not just a nested tag.
            regexEnd = new Regex("</" + startTag + ">", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            Match endTag;
            int tempStartInd = _blockStart;
            {
                // Find the end tag.
                endTag = regexEnd.Match(_html, tempStartInd);

                // Now check to see if there is a start tag in there. If so then we just
                // found the mate to a nested tag.
                tempStartInd = _html.IndexOf("<" + startTag, tempStartInd + 1, _blockEnd, StringComparison.OrdinalIgnoreCase);

            } while (tempStartInd > 0 && tempStartInd < endTag.Index) ;

            _blockEnd = endTag.Index;

            if (_blockEnd <= _blockStart)
            {
                Exception ex = new Exception("Couldn't find matching \"" + endTag.Value + "\" to \"" + startTag.Value + "\"");
                throw ex;
            }

            // Adjust our pointers so that they are correct.
            _contentEnd = _blockEnd - 1;
            _blockEnd += endTag.Length;
        }

    }
}

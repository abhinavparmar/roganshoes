﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Xml;

/// <summary>
/// Summary description for SerializationDeserialization
/// </summary>
namespace RogansShoes.Web.API.Helper
{
    public class SerializationDeserialization
    {
        public SerializationDeserialization()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string Serialization(object className)
        {
            if (className != null)
            {
                XmlSerializer ser = new XmlSerializer(className.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, className); 	// Here Classes are converted to XML String. 
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(sb.ToString());
                return writer.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public object Deserialize(string serializaeString, object className)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serializaeString);
            XmlNodeReader reader = new XmlNodeReader(doc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(className.GetType());
            object obj = ser.Deserialize(reader);
            return obj;
        }
    }
}
#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Siesta;
using System.Text;
using RogansShoes.Web.API.Data;

#endregion

namespace RogansShoes.Web.API.Controllers
{   
    public abstract class ApiControllerBase : Controller
	{
        protected ApiContext ApiContext { get; private set; }

        protected ApiControllerBase()
        {
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            ApiContext = ApiContext.GetApiContext(this);
        }

		public ModelResult<T> Model<T>(T model)
		{
			return new ModelResult<T>(model);
		}

		public ModelResult<T> Model<T>(T model, ModelFormatting formatting)
		{
			return new ModelResult<T>(model, formatting);
		}

        public List<QueryBuilder.WhereCondition> GetFilters(string filter)
        {
            var filters = (filter ?? string.Empty).Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Split(new[] { ' ' }, 3, StringSplitOptions.RemoveEmptyEntries));
            var conditions = new List<QueryBuilder.WhereCondition>();
            foreach (var a in filters)
            {
                var condition = new QueryBuilder.WhereCondition();
                for (var i = 0; i < a.Length; i++)
                {
                    if (i == 0)
                        condition.FieldName = a[i];
                    if (i == 1)
                        condition.Operator = a[i];
                    if (i == 2)
                        condition.MatchValue = a[i];
                }
                conditions.Add(condition);
            }

            return conditions;
        }

        public List<string> GetOrdering(string filter)
        {
            var splitFilter = (filter ?? string.Empty).Split(',').Select(s => s.Split(' ')).ToList();

            splitFilter = splitFilter.Count == 0 ? new List<string[]>(new List<string[]>( )): splitFilter;
            var ordering = splitFilter.Select(x => x[0]).ToList();

            if (splitFilter.Last().Length == 2)
            {
                ordering.Add(splitFilter.Last()[1]);
            }

            return ordering;
        }

        public List<string> GetExpands(string expand)
        {
            var expands = (expand ?? string.Empty).Split(',').Select(s => s.Trim().ToLower()).ToList();
            return expands;
        }

		public EmptyResult OK()
		{
			return new EmptyResult();
		}

		public HttpException NotFound(string message = "Resource not found.")
		{
			return new HttpException(404, message);
		}

        public HttpException NotAllowed(string message = "Operation not allowed.")
        {
            return new HttpException(405, message);
        }

        protected string FromBase64(string base64String)
        {
            try
            {
                base64String = base64String.Replace("-", "+").Replace("_", "/");
                var temp = (new ASCIIEncoding()).GetString(Convert.FromBase64String(base64String));
                return temp;
            }
            catch (FormatException)
            {
                return null;
            }
        }

        protected  string ToBase64(string startingValue)
        {
            byte[] bytes = (new System.Text.ASCIIEncoding()).GetBytes(startingValue);
            var result = System.Convert.ToBase64String(bytes, 0, bytes.Length);
            result = result.Replace("+", "-").Replace("/", "_");
            return result;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RogansShoes.Web.API.Controllers
{
    public class StatusController : ApiControllerBase
    {
        public ActionResult Get()
        {
            return OK();
        }
    }
}

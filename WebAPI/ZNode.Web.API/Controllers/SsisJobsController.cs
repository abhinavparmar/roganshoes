﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Ssis;
using RogansShoes.Web.API.Support;
using RogansShoes.Web.API.Controllers;
using ZNode.Libraries.DataAccess.Custom;

namespace RogansShoes.Web.API.Controllers
{
    public class SsisJobsController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<SsisJobResult> ExecutePackage(SsisJobRequest request)
        {
            var result = new SsisJobResult();
            try
            {
                string packageName = System.Configuration.ConfigurationManager.AppSettings["SSISPackageName"] != null ?
                    System.Configuration.ConfigurationManager.AppSettings["SSISPackageName"].ToString() : string.Empty;
                if (!string.IsNullOrEmpty(packageName))
                {
                    DataManagerHelper dataMangerHelper = new DataManagerHelper();
                    dataMangerHelper.ExecuteSQLAgentJob(packageName);
                    result.IsError = false;
                    result.Message = "Job started successfully.";
                }
                else
                {
                    result.IsError = true;
                    result.Message = "Could not start job! Invalid Job Name: " + packageName;
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = "Could not start package! The following error occured: " + ex.Message;
            }

            return Model(result);
        }

    }
}

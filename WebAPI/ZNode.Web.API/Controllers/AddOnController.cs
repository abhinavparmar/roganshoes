﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.AddOns;
using RogansShoes.Web.API.Support;
using EmptyResult = RogansShoes.Web.API.Models.Messaging.EmptyResult;

namespace RogansShoes.Web.API.Controllers
{
    public class AddOnController : ApiControllerBase
    {
        [HttpPost]
        public ModelResult<AddOnResult> Create([BindRestModel] AddOn addOn, AddOnSaveRequest request)
        {
            var result = new AddOnResult();
            try
            {
                var addOnId = AddOnDataBroker.SaveAddOn(addOn);
                result.AddOn = AddOnDataBroker.GetAddOnById(addOnId);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<AddOnResult>> CreateMany([BindRestModel] Collection<AddOn> addOns, AddOnSaveRequest request)
        {
            var results = new Collection<AddOnResult>((addOns ?? new Collection<AddOn>()).Select(p => Create(p, request).Model).ToList());

            return Model(results);
        }

        [HttpPost]
        public ModelResult<AddOnValueResult> CreateAddOnValue([BindRestModel] AddOnValue addOnValue, AddOnValueSaveRequest request, string id )
        {
            var result = new AddOnValueResult();
            try
            {
                addOnValue.AddOnId = id;
                var addOnValueId = AddOnDataBroker.SaveAddOnValue(addOnValue);
                result.AddOnValue = AddOnDataBroker.GetAddOnValueById(addOnValueId);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<AddOnValueResult>> CreateManyAddOnValues([BindRestModel] Collection<AddOnValue> addOnValues, AddOnValueSaveRequest request, string id )
        {
            var result = new Collection<AddOnValueResult>((addOnValues ?? new Collection<AddOnValue>()).Select(p => CreateAddOnValue(p, request, id).Model).ToList());

            return Model(result);
        }
        
        [HttpPut]
        public ModelResult<AddOnResult> Update([BindRestModel] AddOn addOn, AddOnUpdateRequest request)
        {
            var result = new AddOnResult();
            try
            {
                AddOnDataBroker.UpdateAddOn(addOn, request.IdType);
                result.AddOn = AddOnDataBroker.GetAddOnByAddOn(addOn, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<AddOnResult> Get(AddOnGetRequest request)
        {
            var result = new AddOnResult();
            try
            {
                result.AddOn = AddOnDataBroker.GetAddOnById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<AddOnValueResult> GetAddOnValue(AddOnValueGetRequest request)
        {
            var result = new AddOnValueResult();
            try
            {
                result.AddOnValue = AddOnDataBroker.GetAddOnValueById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<AddOnPageResult> List(AddOnListRequest request)
        {
            var result = new AddOnPageResult();
            try
            {
                int count;
                var addOns = AddOnDataBroker.GetAllAddOns(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.AddOnPage = new AddOnPage(request.Page, request.PageSize, count, addOns);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<AddOnValuePageResult> ListAddOnValues(AddOnValueListRequest request)
        {
            var result = new AddOnValuePageResult();
            try
            {
                int count;
                var addOnValues = AddOnDataBroker.GetAllAddOnValues(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.AddOnValuePage = new AddOnValuePage(request.Page, request.PageSize, count, addOnValues);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<AddOnResult>> UpdateMany([BindRestModel] Collection<AddOn> addOns, AddOnUpdateRequest request)
        {
            var results = new Collection<AddOnResult>((addOns ?? new Collection<AddOn>()).Select(p => Update(p, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<AddOnValueResult> UpdateAddOnValue([BindRestModel] AddOnValue addOnValue, AddOnValueUpdateRequest request, string id)
        {
            var result = new AddOnValueResult();
            try
            {
                addOnValue.AddOnId = id;
                AddOnDataBroker.UpdateAddOnValue(addOnValue, request.IdType);
                result.AddOnValue = AddOnDataBroker.GetAddOnValueByAddOnValue(addOnValue, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<AddOnValueResult>> UpdateManyAddOnValues([BindRestModel] Collection<AddOnValue> addOnValues, AddOnValueUpdateRequest request, string id)
        {
            var results = new Collection<AddOnValueResult>((addOnValues ?? new Collection<AddOnValue>()).Select(p => UpdateAddOnValue(p, request, id).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> Delete(AddOnDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                AddOnDataBroker.DeleteAddOnById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> DeleteAddOnValue(AddOnValueDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                AddOnDataBroker.DeleteAddOnValueById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }
    }
}

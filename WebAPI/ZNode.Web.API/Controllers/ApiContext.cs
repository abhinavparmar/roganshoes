﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using ZNode.Libraries.Core;
using ZNode.Libraries.DataAccess.Entities;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using ZNode;
using ZNode.Web.API;


namespace RogansShoes.Web.API.Controllers
{
    public class ApiContext
    {
        public string ZnodeApiKey { get; private set; }

        public Domain CurrentDomainConfig
        {
            get
            {
                var domainConfig = ZNode.Libraries.Framework.Business.ZNodeConfigManager.DomainConfig;
                return domainConfig;
            }
        }

        public ZNode.Libraries.DataAccess.Entities.Portal CurrentSiteConfig
        {
            get
            {
                var siteConfig = ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig;
                return siteConfig;
            }
        }

        internal ApiContext() {}

        internal static ApiContext GetApiContext(Controller controller)
        {
            var apiKeyToken = controller.GetHeaderValue(AppConstants.HeaderKey_ZnodeApiKey).SafeGet(h => h.Trim());

            var apiContext = new ApiContext {ZnodeApiKey = apiKeyToken};

            return apiContext;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Promotions;
using RogansShoes.Web.API.Support;
using EmptyResult = RogansShoes.Web.API.Models.Messaging.EmptyResult;

namespace RogansShoes.Web.API.Controllers
{
    public class PromotionController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<PromotionResult> Get(PromotionGetRequest request)
        {
            var result = new PromotionResult();
            try
            {
                result.Promotion = PromotionDataBroker.GetPromotionById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<PromotionPageResult> List(PromotionListRequest request)
        {
            var result = new PromotionPageResult();
            try
            {
                int count;
                var promotions = PromotionDataBroker.GetAllPromotions(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.PromotionPage = new PromotionPage(request.Page, request.PageSize, count, promotions);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<PromotionResult> Create([BindRestModel] Promotion promotion, PromotionSaveRequest request)
        {
            var result = new PromotionResult();
            try
            {
                var promotionId = PromotionDataBroker.SavePromotion(promotion);
                result.Promotion = PromotionDataBroker.GetPromotionById(promotionId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<PromotionResult>> CreateMany([BindRestModel] Collection<Promotion> promotions, PromotionSaveRequest request)
        {
            var results = new Collection<PromotionResult>((promotions ?? new Collection<Promotion>()).Select(c => Create(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<PromotionResult> Update([BindRestModel] Promotion promotion, PromotionUpdateRequest request)
        {
            var result = new PromotionResult();
            try
            {
                PromotionDataBroker.UpdatePromotion(promotion, request.IdType);
                result.Promotion = PromotionDataBroker.GetPromotionByPromotion(promotion, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<PromotionResult>> UpdateMany([BindRestModel] Collection<Promotion> promotions, PromotionUpdateRequest request)
        {
            var results = new Collection<PromotionResult>((promotions ?? new Collection<Promotion>()).Select(c => Update(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> Delete(PromotionDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                PromotionDataBroker.DeletePromotionById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<DiscountTypeResult> GetDiscountType(DiscountTypeGetRequest request)
        {
            var result = new DiscountTypeResult();
            try
            {
                result.DiscountType = PromotionDataBroker.GetDiscountTypeById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<DiscountTypePageResult> GetDiscountTypeList(DiscountTypeListRequest request)
        {
            var result = new DiscountTypePageResult();
            try
            {
                int count;
                var discountTypes = PromotionDataBroker.GetAllDiscountTypes(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.DiscountTypePage = new DiscountTypePage(request.Page, request.PageSize, count, discountTypes);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<DiscountTypeResult> CreateDiscountType([BindRestModel] DiscountType discountType, DiscountTypeSaveRequest request)
        {
            var result = new DiscountTypeResult();
            try
            {
                var discountTypeId = PromotionDataBroker.SaveDiscountType(discountType);
                result.DiscountType = PromotionDataBroker.GetDiscountTypeById(discountTypeId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<DiscountTypeResult>> CreateManyDiscountTypes([BindRestModel] Collection<DiscountType> discountTypes, DiscountTypeSaveRequest request)
        {
            var results = new Collection<DiscountTypeResult>((discountTypes ?? new Collection<DiscountType>()).Select(c => CreateDiscountType(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<DiscountTypeResult> UpdateDiscountType([BindRestModel] DiscountType discountType, DiscountTypeUpdateRequest request)
        {
            var result = new DiscountTypeResult();
            try
            {
                PromotionDataBroker.UpdateDiscountType(discountType, request.IdType);
                result.DiscountType = PromotionDataBroker.GetDiscountTypeById(discountType.DiscountTypeId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<DiscountTypeResult>> UpdateManyDiscountTypes([BindRestModel] Collection<DiscountType> discountTypes, DiscountTypeUpdateRequest request)
        {
            var results = new Collection<DiscountTypeResult>((discountTypes ?? new Collection<DiscountType>()).Select(c => UpdateDiscountType(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> DeleteDiscountType(DiscountTypeDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                PromotionDataBroker.DeleteDiscountTypeById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Orders;

namespace RogansShoes.Web.API.Controllers
{
    public class OrderSavedCartController : ApiControllerBase
    {
        public ModelResult<OrderResult> Create([BindRestModel] Order order, OrderSaveRequest request)
        {
            var result = new OrderResult();
            try
            {
                var orderId = OrderSavedCartDataBroker.SaveOrder(order, request.SavedCartId);
                result.Order = OrderSavedCartDataBroker.GetByOrderId(orderId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);

        }

        [HttpPost]
        public ModelResult<Collection<OrderResult>> CreateMany([BindRestModel] Collection<Order> orders, OrderSaveRequest request)
        {
            var results = new Collection<OrderResult>((orders ?? new Collection<Order>()).Select(p => Create(p, request).Model).ToList());

            return Model(results);
        }

        [HttpGet]
        public ModelResult<OrderResult> Get(OrderGetRequest request)
        {
            var result = new OrderResult();
            try
            {
                result.Order = OrderSavedCartDataBroker.GetByOrderId(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<OrderPageResult> List(OrderListRequest request)
        {
            var result = new OrderPageResult();
            try
            {
                int count;
                var orders = OrderSavedCartDataBroker.GetAllOrders(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.OrderPage = new OrderPage(request.Page, request.PageSize, count, orders);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<OrderResult> Update([BindRestModel] Order order, OrderUpdateRequest request)
        {
            var result = new OrderResult();
            try
            {
                OrderSavedCartDataBroker.UpdateOrder(order, request.IdType);
                result.Order = OrderSavedCartDataBroker.GetOrderByOrder(order, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<OrderResult>> UpdateMany([BindRestModel] Collection<Order> orders, OrderUpdateRequest request)
        {
            var results = new Collection<OrderResult>((orders ?? new Collection<Order>()).Select(p => Update(p, request).Model).ToList());

            return Model(results);
        }

        [HttpPost]
        public ModelResult<OrderResult> CreateLines([BindRestModel] Collection<OrderLineItem> orderLineItems, OrderLineItemSaveRequest request)
        {
            var result = new OrderResult();
            try
            {
                foreach (var orderLineItem in orderLineItems)
                {
                    OrderSavedCartDataBroker.SaveOrderLineItem(orderLineItem, request.OrderId, request.IdType);
                }
                result.Order = OrderSavedCartDataBroker.GetByOrderId(request.OrderId, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<OrderResult> UpdateLines([BindRestModel] Collection<OrderLineItem> orderLineItems, OrderLineItemUpdateRequest request)
        {
            var result = new OrderResult();
            try
            {
                foreach (var orderLineItem in orderLineItems)
                {
                    OrderSavedCartDataBroker.UpdateOrderLineItem(orderLineItem, request.OrderId, request.IdType);
                }
                result.Order = OrderSavedCartDataBroker.GetByOrderId(request.OrderId, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }
    }
}
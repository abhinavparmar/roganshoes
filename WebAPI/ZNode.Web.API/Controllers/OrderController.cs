﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Orders;
using RogansShoes.Web.API.Support;

namespace RogansShoes.Web.API.Controllers
{
    public partial class OrderController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<OrderResult> Get(OrderGetRequest request)
        {
            var result = new OrderResult();
            try
            {
                result.Order = OrderDataBroker.GetOrderById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
                result.StackTrace = e.ToString();
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<OrderPageResult> List(OrderListRequest request)
        {
            var result = new OrderPageResult();
            try
            {
                int count;
                var orders = OrderDataBroker.GetAllOrders(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.OrderPage = new OrderPage(request.Page, request.PageSize, count, orders);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
                result.StackTrace = e.ToString();
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<OrderResult> Create([BindRestModel] Order order, OrderSaveRequest request)
        {
            var result = new OrderResult();
            try
            {
                var orderId = OrderDataBroker.SaveOrder(order);
                result.Order = OrderDataBroker.GetOrderById(orderId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<OrderResult>> CreateMany([BindRestModel] Collection<Order> orders, OrderSaveRequest request)
        {
            var results = new Collection<OrderResult>((orders ?? new Collection<Order>()).Select(p => Create(p, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<OrderResult> Update([BindRestModel] Order order, OrderUpdateRequest request)
        {
            var result = new OrderResult();
            try
            {
                OrderDataBroker.UpdateOrder(order, request.IdType);
                result.Order = OrderDataBroker.GetOrderByOrder(order, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<OrderResult>> UpdateMany([BindRestModel] Collection<Order> orders, OrderUpdateRequest request)
        {
            var results = new Collection<OrderResult>((orders ?? new Collection<Order>()).Select(p => Update(p, request).Model).ToList());

            return Model(results);
        }

        [HttpPost]
        public ModelResult<OrderResult> CreateLines([BindRestModel] Collection<OrderLineItem> orderLineItems, OrderLineItemSaveRequest request)
        {
            var result = new OrderResult();
            try
            {
                foreach (var orderLineItem in orderLineItems)
                {
                    OrderDataBroker.SaveOrderLineItem(orderLineItem, request.OrderId, request.IdType);
                }
                result.Order = OrderDataBroker.GetOrderById(request.OrderId, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<OrderResult> UpdateLines([BindRestModel] Collection<OrderLineItem> orderLineItems, OrderLineItemUpdateRequest request)
        {
            var result = new OrderResult();
            try
            {
                foreach (var orderLineItem in orderLineItems)
                {
                    OrderDataBroker.UpdateOrderLineItem(orderLineItem, request.OrderId, request.IdType);
                }
                result.Order = OrderDataBroker.GetOrderById(request.OrderId, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<OrderStateResult> GetOrderState(OrderStateGetRequest request)
        {
            var result = new OrderStateResult();
            try
            {
                result.OrderState = OrderDataBroker.GetOrderStateById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<OrderStatePageResult> ListOrderStates(OrderStateListRequest request)
        {
            var result = new OrderStatePageResult();
            try
            {
                int count;
                var orderStates = OrderDataBroker.GetAllOrderStates(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.OrderStatePage = new OrderStatePage(request.Page, request.PageSize, count, orderStates);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

    }
}
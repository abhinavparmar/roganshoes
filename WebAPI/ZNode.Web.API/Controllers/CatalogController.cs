﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Catalogs;
using RogansShoes.Web.API.Support;

namespace RogansShoes.Web.API.Controllers
{
    public class CatalogController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<CatalogResult> Get(CatalogGetRequest request)
        {
            var result = new CatalogResult();
            try
            {
                result.Catalog = CatalogDataBroker.GetCatalogById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<CatalogPageResult> List(CatalogListRequest request)
        {
            var result = new CatalogPageResult();
            try
            {
                int count;
                var catalogs = CatalogDataBroker.GetAllCatalogs(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.CatalogPage = new CatalogPage(request.Page, request.PageSize, count, catalogs);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<CatalogResult> Create([BindRestModel] Catalog catalog, CatalogSaveRequest request)
        {
            var result = new CatalogResult();
            try
            {
                var catalogId = CatalogDataBroker.SaveCatalog(catalog);
                result.Catalog = CatalogDataBroker.GetCatalogById(catalogId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<CatalogResult>> CreateMany([BindRestModel] Collection<Catalog> catalogs, CatalogSaveRequest request)
        {
            var results = new Collection<CatalogResult>((catalogs ?? new Collection<Catalog>()).Select(c => Create(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<CatalogResult> Update([BindRestModel] Catalog catalog, CatalogUpdateRequest request)
        {
            var result = new CatalogResult();
            try
            {
                CatalogDataBroker.UpdateCatalog(catalog, request.IdType);
                result.Catalog = CatalogDataBroker.GetCatalogByCatalog(catalog, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<CatalogResult>> UpdateMany([BindRestModel] Collection<Catalog> catalogs, CatalogUpdateRequest request)
        {
            var results = new Collection<CatalogResult>((catalogs ?? new Collection<Catalog>()).Select(c => Update(c, request).Model).ToList());

            return Model(results);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Skus;
using RogansShoes.Web.API.Support;

namespace RogansShoes.Web.API.Controllers
{
    public class InventoryController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<InventoryResult> Get(InventoryGetRequest request)
        {
            var result = new InventoryResult();
            try
            {
                result.Inventory = InventoryDataBroker.GetInventoryBySku(request.Id);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<InventoryPageResult> List(InventoryListRequest request)
        {
            var result = new InventoryPageResult();
            try
            {
                int count;
                var skus = InventoryDataBroker.GetAllInventory(request.PageSize, request.Page, out count, GetFilters(request.Filter),GetOrdering(request.OrderBy));

                result.InventoryPage = new InventoryPage(request.Page, request.PageSize, count, skus);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<InventoryResult> Update([BindRestModel] Inventory inventory, InventoryUpdateRequest request)
        {
            var result = new InventoryResult();
            try
            {
                InventoryDataBroker.UpdateInventory(inventory);
                result.Inventory = InventoryDataBroker.GetInventoryBySku(inventory.Sku);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<InventoryResult>> UpdateMany([BindRestModel] Collection<Inventory> inventorys, InventoryUpdateRequest request)
        {
            var results = new Collection<InventoryResult>((inventorys ?? new Collection<Inventory>()).Select(c => Update(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPost]
        public ModelResult<InventoryResult> CreateTransaction([BindRestModel] InventoryTransaction transaction, InventoryRequest request)
        {
            var result = new InventoryResult();
            try
            {
                InventoryDataBroker.CreateTransaction(transaction);
                result.Inventory = InventoryDataBroker.GetInventoryBySku(transaction.Sku);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }
    }
}

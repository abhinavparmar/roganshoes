﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Products;
using RogansShoes.Web.API.Support;

namespace RogansShoes.Web.API.Controllers
{
    public class ProductController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<ProductResult> Get(ProductGetRequest request)
        {
            var result = new ProductResult();
            try
            {
                result.Product = ProductDataBroker.GetProductById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<ProductPageResult> List(ProductListRequest request)
        {
            var result = new ProductPageResult();
            try
            {
                int count;
                var products = ProductDataBroker.GetAllProducts(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter),GetOrdering(request.OrderBy));

                result.ProductPage = new ProductPage(request.Page, request.PageSize, count, products);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<ProductResult> Create([BindRestModel] Product product, ProductSaveRequest request)
        {
            var result = new ProductResult();
            try
            {
                var productId = ProductDataBroker.SaveProduct(product);
                foreach (var sku in product.Skus)
                {
                    ProductDataBroker.SaveProductSku(sku, productId);
                }
                result.Product = ProductDataBroker.GetProductById(productId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<ProductResult>> CreateMany([BindRestModel] Collection<Product> products, ProductSaveRequest request)
        {
            var results = new Collection<ProductResult>((products ?? new Collection<Product>()).Select(p => Create(p, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<ProductResult> Update([BindRestModel] Product product, ProductUpdateRequest request)
        {
            var result = new ProductResult();
            try
            {
                ProductDataBroker.UpdateProduct(product, request.IdType);
                foreach (var sku in product.Skus)
                {
                    ProductDataBroker.UpdateProductSku(sku, product, request.IdType);
                }
                result.Product = ProductDataBroker.GetProductByProduct(product, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<ProductResult>> UpdateMany([BindRestModel] Collection<Product> products, ProductUpdateRequest request)
        {
            var results = new Collection<ProductResult>((products ?? new Collection<Product>()).Select(p => Update(p, request).Model).ToList());

            return Model(results);
        }

        [HttpPost]
        public ModelResult<ProductResult> CreateAddOns([BindRestModel] Collection<AddOn> addOns, ProductAddOnSaveRequest request)
        {
            var result = new ProductResult();
            try
            {
                foreach (var addOn in addOns)
                {
                    if (AddOnDataBroker.GetAddOnByAddOn(addOn) == null)
                    {
                        AddOnDataBroker.SaveAddOn(addOn);
                    }
                    ProductDataBroker.SaveProductAddOn(addOn, request.ProductId, request.IdType);
                }
                var expands = GetExpands(request.Expand);
                expands.Add(Expands.AddOns);
                result.Product = ProductDataBroker.GetProductById(request.ProductId, expands, GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpDelete]
        public ModelResult<ProductResult> DeleteAddOn(ProductAddOnDeleteRequest request)
        {
            var result = new ProductResult();
            try
            {
                ProductDataBroker.DeleteProductAddOn(request.Id, request.ProductId, request.IdType);
                var expands = GetExpands(request.Expand);
                expands.Add(Expands.AddOns);
                result.Product = ProductDataBroker.GetProductById(request.ProductId, expands, GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<ProductResult> CreateTieredPricing([BindRestModel] Collection<TieredPricing> productProfiles, ProductTieredPricingSaveRequest request)
        {
            var result = new ProductResult();
            try
            {
                foreach (var productProfile in productProfiles)
                {
                    ProductDataBroker.SaveProductTieredPricing(productProfile, request.ProductId, request.IdType);
                }
                var expands = GetExpands(request.Expand);
                expands.Add(Expands.TieredPricing);
                result.Product = ProductDataBroker.GetProductById(request.ProductId, expands, GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<ProductResult> UpdateTieredPricing([BindRestModel] Collection<TieredPricing> productProfiles, ProductTieredPricingUpdateRequest request)
        {
            var result = new ProductResult();
            try
            {
                foreach (var productProfile in productProfiles)
                {
                    ProductDataBroker.UpdateProductTieredPricing(productProfile, request.ProductId, request.IdType);
                }
                var expands = GetExpands(request.Expand);
                expands.Add(Expands.TieredPricing);
                result.Product = ProductDataBroker.GetProductById(request.ProductId, expands, GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<ProductReviewStateResult> GetProductReview(ProductReviewStateGetRequest request)
        {
            var result = new ProductReviewStateResult();
            try
            {
                result.ProductReviewState = ProductDataBroker.GetProductReviewStateById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<ProductReviewStatePageResult> ListProductReview(ProductReviewStateListRequest request)
        {
            var result = new ProductReviewStatePageResult();
            try
            {
                int count;
                var productReviewStates = ProductDataBroker.GetAllProductReviewStates(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.ProductReviewStatePage = new ProductReviewStatePage(request.Page, request.PageSize, count, productReviewStates);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }
    }
}

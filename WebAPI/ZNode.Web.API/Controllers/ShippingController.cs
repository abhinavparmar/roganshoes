﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Shipping;
using RogansShoes.Web.API.Support;
using EmptyResult = RogansShoes.Web.API.Models.Messaging.EmptyResult;


namespace RogansShoes.Web.API.Controllers
{
    public class ShippingController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<ShippingResult> Get(ShippingGetRequest request)
        {
            var result = new ShippingResult();
            try
            {
                result.Shipping = ShippingDataBroker.GetShippingById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<ShippingPageResult> List(ShippingListRequest request)
        {
            var result = new ShippingPageResult();
            try
            {
                int count;
                var shipping = ShippingDataBroker.GetAllShipping(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.ShippingPage = new ShippingPage(request.Page, request.PageSize, count, shipping);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<ShippingResult> Create([BindRestModel] Shipping shipping, ShippingSaveRequest request)
        {
            var result = new ShippingResult();
            try
            {
                var shippingId = ShippingDataBroker.SaveShipping(shipping);
                result.Shipping = ShippingDataBroker.GetShippingById(shippingId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<ShippingResult>> CreateMany([BindRestModel] Collection<Shipping> shipping, ShippingSaveRequest request)
        {
            var results = new Collection<ShippingResult>((shipping ?? new Collection<Shipping>()).Select(c => Create(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<ShippingResult> Update([BindRestModel] Shipping shipping, ShippingUpdateRequest request)
        {
            var result = new ShippingResult();
            try
            {
                ShippingDataBroker.UpdateShipping(shipping, request.IdType);
                result.Shipping = ShippingDataBroker.GetShippingByShipping(shipping, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<ShippingResult>> UpdateMany([BindRestModel] Collection<Shipping> shipping, ShippingUpdateRequest request)
        {
            var results = new Collection<ShippingResult>((shipping ?? new Collection<Shipping>()).Select(c => Update(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> Delete(ShippingDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                ShippingDataBroker.DeleteShippingById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<ShippingRuleResult> GetShippingRule(ShippingRuleGetRequest request)
        {
            var result = new ShippingRuleResult();
            try
            {
                result.ShippingRule = ShippingDataBroker.GetShippingRuleById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<ShippingRulePageResult> ListShippingRules(ShippingRuleListRequest request)
        {
            var result = new ShippingRulePageResult();
            try
            {
                int count;
                var shippingRules = ShippingDataBroker.GetAllShippingRules(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.ShippingRulePage = new ShippingRulePage(request.Page, request.PageSize, count, shippingRules);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<ShippingRuleResult> CreateShippingRule([BindRestModel] ShippingRule shippingRule, ShippingRuleSaveRequest request)
        {
            var result = new ShippingRuleResult();
            try
            {
                var shippingRuleId = ShippingDataBroker.SaveShippingRule(shippingRule);
                result.ShippingRule = ShippingDataBroker.GetShippingRuleById(shippingRuleId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<ShippingRuleResult>> CreateManyShippingRules([BindRestModel] Collection<ShippingRule> shippingRule, ShippingRuleSaveRequest request)
        {
            var results = new Collection<ShippingRuleResult>((shippingRule ?? new Collection<ShippingRule>()).Select(c => CreateShippingRule(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<ShippingRuleResult> UpdateShippingRule([BindRestModel] ShippingRule shippingRule, ShippingRuleUpdateRequest request)
        {
            var result = new ShippingRuleResult();
            try
            {
                ShippingDataBroker.UpdateShippingRule(shippingRule, request.IdType);
                result.ShippingRule = ShippingDataBroker.GetShippingRuleByShippingRule(shippingRule, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<ShippingRuleResult>> UpdateManyShippingRules([BindRestModel] Collection<ShippingRule> shippingRules, ShippingRuleUpdateRequest request)
        {
            var results = new Collection<ShippingRuleResult>((shippingRules ?? new Collection<ShippingRule>()).Select(c => UpdateShippingRule(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> DeleteShippingRule(ShippingRuleDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                ShippingDataBroker.DeleteShippingRuleById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<ShippingRuleTypeResult> GetShippingRuleType(ShippingRuleTypeGetRequest request)
        {
            var result = new ShippingRuleTypeResult();
            try
            {
                result.ShippingRuleType = ShippingDataBroker.GetShippingRuleTypeById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<ShippingRuleTypePageResult> ListShippingRuleTypes(ShippingRuleTypeListRequest request)
        {
            var result = new ShippingRuleTypePageResult();
            try
            {
                int count;
                var shippingRuleTypes = ShippingDataBroker.GetAllShippingRuleTypes(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.ShippingRuleTypePage = new ShippingRuleTypePage(request.Page, request.PageSize, count, shippingRuleTypes);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<ShippingRuleTypeResult> CreateShippingRuleType([BindRestModel] ShippingRuleType shippingRuleType, ShippingRuleTypeSaveRequest request)
        {
            var result = new ShippingRuleTypeResult();
            try
            {
                var shippingRuleTypeId = ShippingDataBroker.SaveShippingRuleType(shippingRuleType);
                result.ShippingRuleType = ShippingDataBroker.GetShippingRuleTypeById(shippingRuleTypeId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<ShippingRuleTypeResult>> CreateManyShippingRuleTypes([BindRestModel] Collection<ShippingRuleType> shippingRuleType, ShippingRuleTypeSaveRequest request)
        {
            var results = new Collection<ShippingRuleTypeResult>((shippingRuleType ?? new Collection<ShippingRuleType>()).Select(c => CreateShippingRuleType(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<ShippingRuleTypeResult> UpdateShippingRuleType([BindRestModel] ShippingRuleType shippingRuleType, ShippingRuleTypeUpdateRequest request)
        {
            var result = new ShippingRuleTypeResult();
            try
            {
                ShippingDataBroker.UpdateShippingRuleType(shippingRuleType, request.IdType);
                result.ShippingRuleType = ShippingDataBroker.GetShippingRuleTypeByShippingRuleType(shippingRuleType, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<ShippingRuleTypeResult>> UpdateManyShippingRuleTypes([BindRestModel] Collection<ShippingRuleType> shippingRuleTypes, ShippingRuleTypeUpdateRequest request)
        {
            var results = new Collection<ShippingRuleTypeResult>((shippingRuleTypes ?? new Collection<ShippingRuleType>()).Select(c => UpdateShippingRuleType(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> DeleteShippingRuleType(ShippingRuleTypeDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                ShippingDataBroker.DeleteShippingRuleTypeById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<ShippingTypeResult> GetShippingType(ShippingTypeGetRequest request)
        {
            var result = new ShippingTypeResult();
            try
            {
                result.ShippingType = ShippingDataBroker.GetShippingTypeById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<ShippingTypePageResult> ListShippingTypes(ShippingTypeListRequest request)
        {
            var result = new ShippingTypePageResult();
            try
            {
                int count;
                var shippingTypes = ShippingDataBroker.GetAllShippingTypes(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.ShippingTypePage = new ShippingTypePage(request.Page, request.PageSize, count, shippingTypes);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<ShippingTypeResult> CreateShippingType([BindRestModel] ShippingType shippingType, ShippingTypeSaveRequest request)
        {
            var result = new ShippingTypeResult();
            try
            {
                var shippingTypeId = ShippingDataBroker.SaveShippingType(shippingType);
                result.ShippingType = ShippingDataBroker.GetShippingTypeById(shippingTypeId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<ShippingTypeResult>> CreateManyShippingTypes([BindRestModel] Collection<ShippingType> shippingType, ShippingTypeSaveRequest request)
        {
            var results = new Collection<ShippingTypeResult>((shippingType ?? new Collection<ShippingType>()).Select(c => CreateShippingType(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<ShippingTypeResult> UpdateShippingType([BindRestModel] ShippingType shippingType, ShippingTypeUpdateRequest request)
        {
            var result = new ShippingTypeResult();
            try
            {
                ShippingDataBroker.UpdateShippingType(shippingType, request.IdType);
                result.ShippingType = ShippingDataBroker.GetShippingTypeByShippingType(shippingType, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<ShippingTypeResult>> UpdateManyShippingTypes([BindRestModel] Collection<ShippingType> shippingTypes, ShippingTypeUpdateRequest request)
        {
            var results = new Collection<ShippingTypeResult>((shippingTypes ?? new Collection<ShippingType>()).Select(c => UpdateShippingType(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> DeleteShippingType(ShippingTypeDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                ShippingDataBroker.DeleteShippingTypeById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }
    }
}

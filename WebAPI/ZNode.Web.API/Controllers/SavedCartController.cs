﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.SavedCarts;
using RogansShoes.Web.API.Support;
using EmptyResult = RogansShoes.Web.API.Models.Messaging.EmptyResult;

namespace RogansShoes.Web.API.Controllers
{
    public class SavedCartController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<SavedCartResult> Get(SavedCartGetRequest request)
        {
            var result = new SavedCartResult();
            try
            {
                result.SavedCart = SavedCartDataBroker.GetSavedCartById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<SavedCartPageResult> List(SavedCartListRequest request)
        {
            var result = new SavedCartPageResult();
            try
            {
                int count;
                var savedCarts = SavedCartDataBroker.GetAllSavedCarts(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter),GetOrdering(request.OrderBy));

                result.SavedCartPage = new SavedCartPage(request.Page, request.PageSize, count, savedCarts);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<SavedCartResult> Create([BindRestModel] SavedCart savedCart, SavedCartSaveRequest request)
        {
            var result = new SavedCartResult();
            try
            {
                var savedCartId = SavedCartDataBroker.SaveSavedCart(savedCart);
                result.SavedCart = SavedCartDataBroker.GetSavedCartById(savedCartId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<SavedCartResult>> CreateMany([BindRestModel] Collection<SavedCart> savedCarts, SavedCartSaveRequest request)
        {
            var results = new Collection<SavedCartResult>((savedCarts ?? new Collection<SavedCart>()).Select(p => Create(p, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<SavedCartResult> Update([BindRestModel] SavedCart savedCart, SavedCartUpdateRequest request)
        {
            var result = new SavedCartResult();
            try
            {
                SavedCartDataBroker.UpdateSavedCart(savedCart, request.IdType);
                result.SavedCart = SavedCartDataBroker.GetSavedCartBySavedCart(savedCart, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<SavedCartResult>> UpdateMany([BindRestModel] Collection<SavedCart> savedCarts, SavedCartUpdateRequest request)
        {
            var results = new Collection<SavedCartResult>((savedCarts ?? new Collection<SavedCart>()).Select(p => Update(p, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> Delete(SavedCartDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                var savedCart = SavedCartDataBroker.GetSavedCartById(request.Id, null, null, request.IdType);
                foreach (var savedCartLineItem in savedCart.SavedCartLineItems)
                {
                    SavedCartDataBroker.DeleteSavedCartLineItemById(savedCartLineItem.SavedCartLineItemId);
                }
                SavedCartDataBroker.DeleteSavedCartById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<SavedCartResult> CreateLineItemByProduct([BindRestModel] Product product,SavedCartLineItemSaveRequest request)
        {
            var result = new SavedCartResult();
            try
            {
                var savedCartId = SavedCartDataBroker.SaveProductSavedCartLineItem(product,Convert.ToInt32(request.Quantity), request.SavedCartId, request.IdType);

                result.SavedCart = SavedCartDataBroker.GetSavedCartById(savedCartId, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<SavedCartResult> CreateLines([BindRestModel] Collection<SavedCartLineItem> savedCartLineItems, SavedCartLineItemSaveRequest request)
        {
            var result = new SavedCartResult();
            try
            {
                foreach (var savedCartLineItem in savedCartLineItems)
                {
                    SavedCartDataBroker.SaveSavedCartLineItem(savedCartLineItem, request.SavedCartId, request.IdType);
                }
                result.SavedCart = SavedCartDataBroker.GetSavedCartById(request.SavedCartId, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<SavedCartResult> UpdateLines([BindRestModel] Collection<SavedCartLineItem> savedCartLineItems, SavedCartLineItemUpdateRequest request)
        {
            var result = new SavedCartResult();
            try
            {
                foreach (var savedCartLineItem in savedCartLineItems)
                {
                    SavedCartDataBroker.UpdateSavedCartLineItem(savedCartLineItem, request.SavedCartId, request.IdType);
                }
                result.SavedCart = SavedCartDataBroker.GetSavedCartById(request.SavedCartId, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpDelete]
        public ModelResult<SavedCartResult> DeleteLine(SavedCartLineItemDeleteRequest request)
        {
            var result = new SavedCartResult();
            try
            {
                SavedCartDataBroker.DeleteSavedCartLineItemById(request.Id);
                result.SavedCart = SavedCartDataBroker.GetSavedCartById(request.SavedCartId, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }
    }
}

﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Web.API.Controllers
{
    public static class DataAccessExtensions
    {
        public static string GetString(this SqlDataReader dr, string columnName)
        {
            object val = dr[columnName];
            if (val == DBNull.Value)
                return null;
            return (string)val;
        }

        public static Int32 GetInt32(this SqlDataReader dr, string columnName)
        {
            object val = dr[columnName];
            if (val == DBNull.Value)
                return 0;
            return (Int32)val;
        }

        public static bool GetBoolean(this SqlDataReader dr, string columnName)
        {
            object val = dr[columnName];
            if (val == DBNull.Value)
                return false;
            return (bool)val;
        }

        public static SqlCommand CreateTextCommand(this SqlConnection conn)
        {
            if (conn != null && conn.State == ConnectionState.Open)
            {
                var cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                return cmd;
            }
            throw new InvalidOperationException("Connection is not open.");
        }

        public static Decimal GetDecimal(this SqlDataReader dr, string columnName)
        {
            object val = dr[columnName];
            if (val == DBNull.Value)
                return 0;
            return (decimal)val;
        }

        public static DateTime? GetDateTime(this SqlDataReader dr, string columnName)
        {
            object val = dr[columnName];
            if (val == DBNull.Value)
                return null;
            return (DateTime)val;
        }
    }
}
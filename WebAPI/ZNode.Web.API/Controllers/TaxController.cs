﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Taxes;
using RogansShoes.Web.API.Support;
using EmptyResult = RogansShoes.Web.API.Models.Messaging.EmptyResult;

namespace RogansShoes.Web.API.Controllers
{
    public class TaxController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<TaxClassResult> Get(TaxClassGetRequest request)
        {
            var result = new TaxClassResult();
            try
            {
                result.TaxClass = TaxDataBroker.GetTaxClassById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<TaxClassPageResult> List(TaxClassListRequest request)
        {
            var result = new TaxClassPageResult();
            try
            {
                int count;
                var taxClasses = TaxDataBroker.GetAllTaxClasses(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.TaxClassPage = new TaxClassPage(request.Page, request.PageSize, count, taxClasses);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<TaxClassResult> Create([BindRestModel] TaxClass taxClass, TaxClassSaveRequest request)
        {
            var result = new TaxClassResult();
            try
            {
                var taxClassId = TaxDataBroker.SaveTaxClass(taxClass);
                result.TaxClass = TaxDataBroker.GetTaxClassById(taxClassId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<TaxClassResult>> CreateMany([BindRestModel] Collection<TaxClass> taxClasss, TaxClassSaveRequest request)
        {
            var results = new Collection<TaxClassResult>((taxClasss ?? new Collection<TaxClass>()).Select(c => Create(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<TaxClassResult> Update([BindRestModel] TaxClass taxClass, TaxClassUpdateRequest request)
        {
            var result = new TaxClassResult();
            try
            {
                TaxDataBroker.UpdateTaxClass(taxClass, request.IdType);
                result.TaxClass = TaxDataBroker.GetTaxClassByTaxClass(taxClass, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<TaxClassResult>> UpdateMany([BindRestModel] Collection<TaxClass> taxClass, TaxClassUpdateRequest request)
        {
            var results = new Collection<TaxClassResult>((taxClass ?? new Collection<TaxClass>()).Select(c => Update(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> Delete(TaxClassDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                TaxDataBroker.DeleteTaxClassById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<TaxRuleResult> GetTaxRule(TaxRuleGetRequest request)
        {
            var result = new TaxRuleResult();
            try
            {
                result.TaxRule = TaxDataBroker.GetTaxRuleById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<TaxRulePageResult> ListTaxRules(TaxRuleListRequest request)
        {
            var result = new TaxRulePageResult();
            try
            {
                int count;
                var taxRules = TaxDataBroker.GetAllTaxRules(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.TaxRulePage = new TaxRulePage(request.Page, request.PageSize, count, taxRules);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<TaxRuleResult> CreateTaxRule([BindRestModel] TaxRule taxRule, TaxRuleSaveRequest request)
        {
            var result = new TaxRuleResult();
            try
            {
                var taxRuleId = TaxDataBroker.SaveTaxRule(taxRule);
                result.TaxRule = TaxDataBroker.GetTaxRuleById(taxRuleId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<TaxRuleResult>> CreateManyTaxRules([BindRestModel] Collection<TaxRule> taxRules, TaxRuleSaveRequest request)
        {
            var results = new Collection<TaxRuleResult>((taxRules ?? new Collection<TaxRule>()).Select(c => CreateTaxRule(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<TaxRuleResult> UpdateTaxRule([BindRestModel] TaxRule taxRule, TaxRuleUpdateRequest request)
        {
            var result = new TaxRuleResult();
            try
            {
                TaxDataBroker.UpdateTaxRule(taxRule, request.IdType);
                result.TaxRule = TaxDataBroker.GetTaxRuleByTaxRule(taxRule, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<TaxRuleResult>> UpdateManyTaxRules([BindRestModel] Collection<TaxRule> taxRule, TaxRuleUpdateRequest request)
        {
            var results = new Collection<TaxRuleResult>((taxRule ?? new Collection<TaxRule>()).Select(c => UpdateTaxRule(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> DeleteTaxRule(TaxRuleDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                TaxDataBroker.DeleteTaxRuleById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<TaxRuleTypeResult> GetTaxRuleType(TaxRuleTypeGetRequest request)
        {
            var result = new TaxRuleTypeResult();
            try
            {
                result.TaxRuleType = TaxDataBroker.GetTaxRuleTypeById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<TaxRuleTypePageResult> ListTaxRuleTypes(TaxRuleTypeListRequest request)
        {
            var result = new TaxRuleTypePageResult();
            try
            {
                int count;
                var taxRuleTypes = TaxDataBroker.GetAllTaxRuleTypes(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.TaxRuleTypePage = new TaxRuleTypePage(request.Page, request.PageSize, count, taxRuleTypes);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<TaxRuleTypeResult> CreateTaxRuleType([BindRestModel] TaxRuleType taxRuleType, TaxRuleTypeSaveRequest request)
        {
            var result = new TaxRuleTypeResult();
            try
            {
                var taxRuleTypeId = TaxDataBroker.SaveTaxRuleType(taxRuleType);
                result.TaxRuleType = TaxDataBroker.GetTaxRuleTypeById(taxRuleTypeId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<TaxRuleTypeResult>> CreateManyTaxRuleTypes([BindRestModel] Collection<TaxRuleType> taxRuleTypes, TaxRuleTypeSaveRequest request)
        {
            var results = new Collection<TaxRuleTypeResult>((taxRuleTypes ?? new Collection<TaxRuleType>()).Select(c => CreateTaxRuleType(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<TaxRuleTypeResult> UpdateTaxRuleType([BindRestModel] TaxRuleType taxRuleType, TaxRuleTypeUpdateRequest request)
        {
            var result = new TaxRuleTypeResult();
            try
            {
                TaxDataBroker.UpdateTaxRuleType(taxRuleType, request.IdType);
                result.TaxRuleType = TaxDataBroker.GetTaxRuleTypeByTaxRuleType(taxRuleType, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<TaxRuleTypeResult>> UpdateManyTaxRuleTypes([BindRestModel] Collection<TaxRuleType> taxRuleType, TaxRuleTypeUpdateRequest request)
        {
            var results = new Collection<TaxRuleTypeResult>((taxRuleType ?? new Collection<TaxRuleType>()).Select(c => UpdateTaxRuleType(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> DeleteTaxRuleType(TaxRuleTypeDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                TaxDataBroker.DeleteTaxRuleTypeById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Suppliers;
using RogansShoes.Web.API.Support;
using EmptyResult = RogansShoes.Web.API.Models.Messaging.EmptyResult;

namespace RogansShoes.Web.API.Controllers
{
    public class SupplierController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<SupplierResult> Get(SupplierGetRequest request)
        {
            var result = new SupplierResult();
            try
            {
                result.Supplier = SupplierDataBroker.GetSupplierById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<SupplierPageResult> List(SupplierListRequest request)
        {
            var result = new SupplierPageResult();
            try
            {
                int count;
                var Supplieres = SupplierDataBroker.GetAllSuppliers(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.SupplierPage = new SupplierPage(request.Page, request.PageSize, count, Supplieres);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<SupplierResult> Create([BindRestModel] Supplier Supplier, SupplierSaveRequest request)
        {
            var result = new SupplierResult();
            try
            {
                var SupplierId = SupplierDataBroker.SaveSupplier(Supplier);
                result.Supplier = SupplierDataBroker.GetSupplierById(SupplierId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<SupplierResult>> CreateMany([BindRestModel] Collection<Supplier> Suppliers, SupplierSaveRequest request)
        {
            var results = new Collection<SupplierResult>((Suppliers ?? new Collection<Supplier>()).Select(c => Create(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<SupplierResult> Update([BindRestModel] Supplier supplier, SupplierUpdateRequest request)
        {
            var result = new SupplierResult();
            try
            {
                SupplierDataBroker.UpdateSupplier(supplier, request.IdType);
                result.Supplier = SupplierDataBroker.GetSupplierBySupplier(supplier, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<SupplierResult>> UpdateMany([BindRestModel] Collection<Supplier> supplier, SupplierUpdateRequest request)
        {
            var results = new Collection<SupplierResult>((supplier ?? new Collection<Supplier>()).Select(c => Update(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> Delete(SupplierDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                SupplierDataBroker.DeleteSupplierById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<SupplierTypeResult> GetSupplierType(SupplierTypeGetRequest request)
        {
            var result = new SupplierTypeResult();
            try
            {
                result.SupplierType = SupplierDataBroker.GetSupplierTypeById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<SupplierTypePageResult> ListSupplierTypes(SupplierTypeListRequest request)
        {
            var result = new SupplierTypePageResult();
            try
            {
                int count;
                var SupplierTypees = SupplierDataBroker.GetAllSupplierTypes(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.SupplierTypePage = new SupplierTypePage(request.Page, request.PageSize, count, SupplierTypees);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<SupplierTypeResult> CreateSupplierType([BindRestModel] SupplierType supplierType, SupplierTypeSaveRequest request)
        {
            var result = new SupplierTypeResult();
            try
            {
                var supplierTypeId = SupplierDataBroker.SaveSupplierType(supplierType);
                result.SupplierType = SupplierDataBroker.GetSupplierTypeById(supplierTypeId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<SupplierTypeResult>> CreateManySupplierTypes([BindRestModel] Collection<SupplierType> supplierTypes, SupplierTypeSaveRequest request)
        {
            var results = new Collection<SupplierTypeResult>((supplierTypes ?? new Collection<SupplierType>()).Select(c => CreateSupplierType(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<SupplierTypeResult> UpdateSupplierType([BindRestModel] SupplierType supplierType, SupplierTypeUpdateRequest request)
        {
            var result = new SupplierTypeResult();
            try
            {
                SupplierDataBroker.UpdateSupplierType(supplierType, request.IdType);
                result.SupplierType = SupplierDataBroker.GetSupplierTypeBySupplierType(supplierType, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<SupplierTypeResult>> UpdateManySupplierTypes([BindRestModel] Collection<SupplierType> supplierTypes, SupplierTypeUpdateRequest request)
        {
            var results = new Collection<SupplierTypeResult>((supplierTypes ?? new Collection<SupplierType>()).Select(c => UpdateSupplierType(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> DeleteSupplierType(SupplierTypeDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                SupplierDataBroker.DeleteSupplierTypeById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }
    }
}

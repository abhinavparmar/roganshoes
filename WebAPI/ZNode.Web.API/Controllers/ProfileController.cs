﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Profiles;
using RogansShoes.Web.API.Support;

namespace RogansShoes.Web.API.Controllers
{
    public class ProfileController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<ProfileResult> Get(ProfileGetRequest request)
        {
            var result = new ProfileResult();
            try
            {
                result.Profile = ProfileDataBroker.GetProfileById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<ProfilePageResult> List(ProfileListRequest request)
        {
            var result = new ProfilePageResult();
            try
            {
                int count;
                var profiles = ProfileDataBroker.GetAllProfiles(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.ProfilePage = new ProfilePage(request.Page, request.PageSize, count, profiles);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<ProfileResult> Create([BindRestModel] Profile profile, ProfileSaveRequest request)
        {
            var result = new ProfileResult();
            try
            {
                var profileId = ProfileDataBroker.SaveProfile(profile);
                result.Profile = ProfileDataBroker.GetProfileById(profileId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<ProfileResult>> CreateMany([BindRestModel] Collection<Profile> profiles, ProfileSaveRequest request)
        {
            var results = new Collection<ProfileResult>((profiles ?? new Collection<Profile>()).Select(c => Create(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<ProfileResult> Update([BindRestModel] Profile profile, ProfileUpdateRequest request)
        {
            var result = new ProfileResult();
            try
            {
                ProfileDataBroker.UpdateProfile(profile, request.IdType);
                result.Profile = ProfileDataBroker.GetProfileByProfile(profile, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<ProfileResult>> UpdateMany([BindRestModel] Collection<Profile> profiles, ProfileUpdateRequest request)
        {
            var results = new Collection<ProfileResult>((profiles ?? new Collection<Profile>()).Select(c => Update(c, request).Model).ToList());

            return Model(results);
        }
    }
}

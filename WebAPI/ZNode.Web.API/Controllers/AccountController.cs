﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Accounts;
using RogansShoes.Web.API.Support;

namespace RogansShoes.Web.API.Controllers
{
    public class AccountController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<AccountResult> Get(AccountGetRequest request)
        {
            var result = new AccountResult();
            try
            {
                result.Account = AccountDataBroker.GetAccountById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<AccountPageResult> List(AccountListRequest request)
        {
            var result = new AccountPageResult();
            try
            {
                int count;
                var accounts = AccountDataBroker.GetAllAccounts(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.AccountPage = new AccountPage(request.Page, request.PageSize, count, accounts);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<AccountResult> Create([BindRestModel] Account account, AccountSaveRequest request)
        {
            var result = new AccountResult();
            try
            {
                var accountId = AccountDataBroker.SaveAccount(account);
                result.Account = AccountDataBroker.GetAccountById(accountId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<AccountResult>> CreateMany([BindRestModel] Collection<Account> accounts, AccountSaveRequest request)
        {
            var results = new Collection<AccountResult>((accounts ?? new Collection<Account>()).Select(c => Create(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<AccountResult> Update([BindRestModel] Account account, AccountUpdateRequest request)
        {
            var result = new AccountResult();
            try
            {
                AccountDataBroker.UpdateAccount(account, request.IdType);
                result.Account = AccountDataBroker.GetAccountByAccount(account, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<AccountResult>> UpdateMany([BindRestModel] Collection<Account> accounts, AccountUpdateRequest request)
        {
            var results = new Collection<AccountResult>((accounts ?? new Collection<Account>()).Select(c => Update(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPost]
        public ModelResult<AccountResult> CreateAddresses([BindRestModel] Collection<AccountAddress> accountAddresses, AccountAddressSaveRequest request)
        {
            var result = new AccountResult();
            try
            {
                foreach (var accountAddress in accountAddresses)
                {
                    AccountDataBroker.SaveAccountAddress(accountAddress, request.AccountId, request.IdType);
                }
                var expands = GetExpands(request.Expand);
                expands.Add(Expands.Addresses);
                result.Account = AccountDataBroker.GetAccountById(request.AccountId, expands, GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<AccountResult> UpdateAddresses([BindRestModel]Collection<AccountAddress> accountAddresses, AccountAddressUpdateRequest request)
        {
            var result = new AccountResult();
            try
            {
                foreach (var accountAddress in accountAddresses)
                {
                    AccountDataBroker.UpdateAccountAddress(accountAddress, request.AccountId, request.IdType);
                }
                var expands = GetExpands(request.Expand);
                expands.Add(Expands.Addresses);
                result.Account = AccountDataBroker.GetAccountById(request.AccountId, expands, GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }
            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<AccountResult> CreateProfiles([BindRestModel] Collection<AccountProfile> accountProfiles, AccountProfileSaveRequest request)
        {
            var result = new AccountResult();
            try
            {
                foreach (var accountProfile in accountProfiles)
                {
                    AccountDataBroker.SaveAccountProfile(accountProfile, request.AccountId, request.IdType);
                }
                var expands = GetExpands(request.Expand);
                expands.Add(Expands.Profiles);
                result.Account = AccountDataBroker.GetAccountById(request.AccountId, expands, GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<AccountResult> UpdateProfiles([BindRestModel] Collection<AccountProfile> accountProfiles, AccountProfileUpdateRequest request)
        {
            var result = new AccountResult();
            try
            {
                foreach (var accountProfile in accountProfiles)
                {
                    AccountDataBroker.UpdateAccountProfile(accountProfile, request.AccountId, request.IdType);
                }
                var expands = GetExpands(request.Expand);
                expands.Add(Expands.Profiles);
                result.Account = AccountDataBroker.GetAccountById(request.AccountId, expands, GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Orders;
using RogansShoes.Web.API.Support;
using RogansShoes.Web.API.Controllers;
using ZNode.Libraries.DataAccess.Custom;
using RogansShoes.Web.API.Helper;

namespace RogansShoes.Web.API.Controllers
{
    public partial class OrderController : ApiControllerBase
    {
        [HttpPut]
        public ModelResult<OrderUpdateResult> UpdateStatus([BindRestModel]OrderUpdate ordUpdate, OrderSaveRequest request)
        {
            var result = new OrderUpdateResult();
            try
            {
                if (ordUpdate.OrderID > 0)
                {
                    //OrderDataBroker.UpdateOrderDetails(ordUpdate.OrderID, ordUpdate.OrderStatus, ordUpdate.TrackingNumber, ordUpdate.ShipDate, ordUpdate.LineItemUpdate);
                    OrderDataBroker.UpdateOrder(ordUpdate.OrderID, ordUpdate.OrderStatus, ordUpdate.TrackingNumber, ordUpdate.ShipDate, ordUpdate.ShippingInfo);

                    result.OrderUpdate = OrderDataBroker.GetUpdateOrderById(ordUpdate.OrderID.ToString(), GetExpands(request.Expand), GetFilters(request.Filter));
                }
                else
                {
                    result.IsError = true;
                    result.Message = "Please enter correct Order ID";
                }
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
                result.StackTrace = e.ToString();
                string errorMessage = "Error in Updating Order ID:" + ordUpdate.OrderID + "!!Error while updating order!!Method Name!!UpdateStatus!!" + e.ToString();
                OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ordUpdate.OrderID, 0, string.Empty, errorMessage);
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<OrderUpdateResult>> UpdateManyStatus([BindRestModel]Collection<OrderUpdate> updateOrders, OrderSaveRequest request)
        {
            var results = new Collection<OrderUpdateResult>((updateOrders ?? new Collection<OrderUpdate>()).Select(ord => UpdateStatus(ord, request).Model).ToList());
            return Model(results);
        }

        [HttpGet]
        public ModelResult<OrderPageResult> GetAllByDateRange(OrderListRequest request)
        {
            var result = new OrderPageResult();
            try
            {
                int count;
                var orders = OrderDataBroker.GetAllOrdersByDateRange(request.StartDate, request.EndDate, GetFilters(request.Filter), GetOrdering(request.OrderBy), out count);

                result.OrderPage = new OrderPage(request.Page, request.Page, count, orders);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
                result.StackTrace = e.ToString();
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<OrderPageResult> GetAllByStatus(OrderListRequest request)
        {
            var result = new OrderPageResult();
            try
            {
                int count;
                var orders = OrderDataBroker.GetAllOrdersByStatus(request.OrderStatus, GetFilters(request.Filter), GetOrdering(request.OrderBy), out count);

                result.OrderPage = new OrderPage(request.Page, request.Page, count, orders);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
                result.StackTrace = e.ToString();
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<OrderPageResult> ExecutePackage(OrderListRequest request)
        {
            var result = new OrderPageResult();
            try
            {
                string packageName = System.Configuration.ConfigurationManager.AppSettings["SSISPackageName"] != null ?
                    System.Configuration.ConfigurationManager.AppSettings["SSISPackageName"].ToString() : string.Empty;
                if (!string.IsNullOrEmpty(packageName))
                {
                    DataManagerHelper dataMangerHelper = new DataManagerHelper();
                    dataMangerHelper.ExecuteSQLAgentJob(packageName);
                    result.IsError = false;
                    result.Message = "The job was executed successfully.";
                }
                else
                {
                    result.IsError = true;
                    result.Message = "Could not execute package! Invalid Package Name: " + packageName;
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = "Could not execute package! The following error occured: " + ex.Message;
            }

            return Model(result);
        }

        //[HttpPost]
        //public ModelResult<OrderUpdateLineItemResult> UpdateLineItemStatus([BindRestModel]Collection<OrderUpdateLineItem> updateOrdLineItems, OrderUpdateLineItemRequest request)
        //{
        //    var result = new OrderUpdateLineItemResult();
        //    try
        //    {
        //        foreach (var lines in updateOrdLineItems)
        //        {
        //            OrderDataBroker.UpdateOrderLineDetails(lines.OrderID, lines.ZNodeSKU, lines.LineItemTrackingNumber, lines.LineItemShipServiceName, lines.LineItemTrasactionID);
        //        }
        //        result.OrderUpdateLineItem = OrderDataBroker.GetUpdateOrderLineItemByOrderID(request.OrderID, GetExpands(request.Expand), GetFilters(request.Filter));
        //    }
        //    catch (Exception e)
        //    {
        //        result.IsError = true;
        //        result.Message = e.Message;
        //    }

        //    return Model(result, request.Formatting);
        //}

    }
}
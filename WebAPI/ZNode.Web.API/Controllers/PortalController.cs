﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Portals;
using RogansShoes.Web.API.Support;
using EmptyResult = RogansShoes.Web.API.Models.Messaging.EmptyResult;


namespace RogansShoes.Web.API.Controllers
{
    public class PortalController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<PortalResult> Get(PortalGetRequest request)
        {
            var result = new PortalResult();
            try
            {
                result.Portal = PortalDataBroker.GetPortalById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<PortalPageResult> List(PortalListRequest request)
        {
            var result = new PortalPageResult();
            try
            {
                int count;
                var portals = PortalDataBroker.GetAllPortals(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter), GetOrdering(request.OrderBy));

                result.PortalPage = new PortalPage(request.Page, request.PageSize, count, portals);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<PortalResult> Create([BindRestModel] Portal portal, PortalSaveRequest request)
        {
            var result = new PortalResult();
            try
            {
                var portalId = PortalDataBroker.SavePortal(portal);
                result.Portal = PortalDataBroker.GetPortalById(portalId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<PortalResult>> CreateMany([BindRestModel] Collection<Portal> portals, PortalSaveRequest request)
        {
            var results = new Collection<PortalResult>((portals ?? new Collection<Portal>()).Select(c => Create(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<PortalResult> Update([BindRestModel] Portal portal, PortalUpdateRequest request)
        {
            var result = new PortalResult();
            try
            {
                PortalDataBroker.UpdatePortal(portal, request.IdType);
                result.Portal = PortalDataBroker.GetPortalByPortal(portal, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<PortalResult>> UpdateMany([BindRestModel] Collection<Portal> portals, PortalUpdateRequest request)
        {
            var results = new Collection<PortalResult>((portals ?? new Collection<Portal>()).Select(c => Update(c, request).Model).ToList());

            return Model(results);
        }

        [HttpDelete]
        public ModelResult<EmptyResult> Delete(PortalDeleteRequest request)
        {
            var result = new EmptyResult();
            try
            {
                PortalDataBroker.DeletePortalById(request.Id, request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Siesta;
using RogansShoes.Web.API.Data;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Models.Messaging.Categories;
using RogansShoes.Web.API.Support;

namespace RogansShoes.Web.API.Controllers
{
    public class CategoryController : ApiControllerBase
    {
        [HttpGet]
        public ModelResult<CategoryResult> Get(CategoryGetRequest request)
        {
            var result = new CategoryResult();
            try
            {
                result.Category = CategoryDataBroker.GetCategoryById(request.Id, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpGet]
        public ModelResult<CategoryPageResult> List(CategoryListRequest request)
        {
            var result = new CategoryPageResult();
            try
            {
                int count;
                var categories = CategoryDataBroker.GetAllCategories(request.PageSize, request.Page, out count, GetExpands(request.Expand), GetFilters(request.Filter),GetOrdering(request.OrderBy));

                result.CategoryPage = new CategoryPage(request.Page, request.PageSize, count, categories);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<CategoryResult> Create([BindRestModel] Category category, CategorySaveRequest request)
        {
            var result = new CategoryResult();
            try
            {
                var categoryId = CategoryDataBroker.SaveCategory(category);
                result.Category = CategoryDataBroker.GetCategoryById(categoryId, GetExpands(request.Expand), GetFilters(request.Filter));
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPost]
        public ModelResult<Collection<CategoryResult>> CreateMany([BindRestModel] Collection<Category> categories, CategorySaveRequest request)
        {
            var results = new Collection<CategoryResult>((categories ?? new Collection<Category>()).Select(c => Create(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPut]
        public ModelResult<CategoryResult> Update([BindRestModel] Category category, CategoryUpdateRequest request)
        {
            var result = new CategoryResult();
            try
            {
                CategoryDataBroker.UpdateCategory(category, request.IdType);
                result.Category = CategoryDataBroker.GetCategoryByCategory(category, GetExpands(request.Expand), GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<Collection<CategoryResult>> UpdateMany([BindRestModel] Collection<Category> categories, CategoryUpdateRequest request)
        {
            var results = new Collection<CategoryResult>((categories ?? new Collection<Category>()).Select(c => Update(c, request).Model).ToList());

            return Model(results);
        }

        [HttpPost]
        public ModelResult<CategoryResult> CreateNodes([BindRestModel] Collection<CategoryNode> categoryNodes, CategoryNodeSaveRequest request)
        {
            var result = new CategoryResult();
            try
            {
                foreach (var categoryNode in categoryNodes)
                {
                    CategoryDataBroker.SaveCategoryNode(categoryNode, request.CategoryId, request.IdType);
                }
                var expands = GetExpands(request.Expand);
                expands.Add(Expands.CategoryNodes);
                result.Category = CategoryDataBroker.GetCategoryById(request.CategoryId, expands, GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }

        [HttpPut]
        public ModelResult<CategoryResult> UpdateNodes([BindRestModel] Collection<CategoryNode> categoryNodes, CategoryNodeUpdateRequest request)
        {
            var result = new CategoryResult();
            try
            {
                foreach (var categoryNode in categoryNodes)
                {
                    CategoryDataBroker.UpdateCategoryNode(categoryNode, request.CategoryId, request.IdType);
                }
                var expands = GetExpands(request.Expand);
                expands.Add(Expands.CategoryNodes);
                result.Category = CategoryDataBroker.GetCategoryById(request.CategoryId, expands, GetFilters(request.Filter), request.IdType);
            }
            catch (Exception e)
            {
                result.IsError = true;
                result.Message = e.Message;
            }

            return Model(result, request.Formatting);
        }
    }
}

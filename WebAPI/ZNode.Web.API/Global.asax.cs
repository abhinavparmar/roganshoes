﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using RogansShoes.Web.API.Configuration;
using RogansShoes.Web.API.Configuration.Ninject;
using RogansShoes.Web.API.Support;
using RogansShoes.Web.API.ValueProviders;
using ZNode.Libraries.Core;
using ZNode.Web.API;

namespace RogansShoes.Web.API
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApi : System.Web.HttpApplication //Ninject.Web.Common.NinjectHttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // status

            routes.MapRoute(
                "get-status", "status",
                new { controller = "status", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            // accounts

            routes.MapRoute(
                "get-account", "accounts/{id}",
                new { controller = "account", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-account", "accounts",
                new { controller = "account", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "create-account", "accounts",
                new { controller = "account", action = "createmany" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-account", "accounts",
                new { controller = "account", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "create-accountaddress", "accounts/{accountid}/addresses",
                new { controller = "account", action = "createaddresses" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-accountaddress", "accounts/{accountid}/addresses",
                new { controller = "account", action = "updateaddresses" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "create-accountprofile", "accounts/{accountid}/profiles",
                new { controller = "account", action = "createprofiles" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-accountprofile", "accounts/{accountid}/profiles",
                new { controller = "account", action = "updateprofiles" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            // catalogs

            routes.MapRoute(
                "get-catalog", "catalogs/{id}",
                new { controller = "catalog", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-catalog", "catalogs",
                new { controller = "catalog", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-catalog", "catalogs",
                 new { controller = "catalog", action = "createmany" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-catalog", "catalogs",
                new { controller = "catalog", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            // categories

            routes.MapRoute(
                "get-category", "categories/{id}",
                new { controller = "category", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-category", "categories",
                new { controller = "category", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-category", "categories",
                 new { controller = "category", action = "createmany" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-category", "categories",
                new { controller = "category", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "create-categorynode", "categories/{categoryid}/nodes",
                new { controller = "category", action = "createnodes" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-categorynode", "categories/{categoryid}/nodes",
                new { controller = "category", action = "updatenodes" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            // inventory

            routes.MapRoute(
                "get-inventory", "inventory/{id}",
                new { controller = "inventory", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-inventory", "inventory",
                new { controller = "inventory", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "update-inventory", "inventory",
                new { controller = "inventory", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "create-inventorytransaction", "inventory",
                new { controller = "inventory", action = "createtransaction" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            // portal

            routes.MapRoute(
                "get-portal", "portals/{id}",
                new { controller = "portal", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-portal", "portals",
                new { controller = "portal", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-portal", "portals",
                 new { controller = "portal", action = "createmany" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-portal", "portals",
                new { controller = "portal", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-portal", "portals/{id}",
                new { controller = "portal", action = "delete" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            // product

            routes.MapRoute(
                "get-product", "products/{id}",
                new { controller = "product", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-product", "products",
                new { controller = "product", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-product", "products",
                 new { controller = "product", action = "createmany" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-product", "products",
                new { controller = "product", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "create-productaddon", "products/{productid}/addons",
                new { controller = "product", action = "createaddons" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "delete-productaddon", "products/{productid}/addons/{id}",
                new { controller = "product", action = "deleteaddon" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            routes.MapRoute(
                "create-producttierdpricing", "products/{productid}/tieredpricing",
                new { controller = "product", action = "createtieredpricing" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-producttieredpricing", "products/{productid}/tieredpricing",
                new { controller = "product", action = "updatetieredpricing" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            // promotion

            routes.MapRoute(
                "get-promotion", "promotions/{id}",
                new { controller = "promotion", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-promotion", "promotions",
                new { controller = "promotion", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-promotion", "promotions",
                 new { controller = "promotion", action = "createmany" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-promotion", "promotions",
                new { controller = "promotion", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-promotion", "promotions/{id}",
                new { controller = "promotion", action = "delete" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            routes.MapRoute(
                "get-discounttype", "discounttypes/{id}",
                new { controller = "promotion", action = "getdiscounttype" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-discounttypes", "discounttypes",
                new { controller = "promotion", action = "getdiscounttypelist" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-discounttypes", "discounttypes",
                 new { controller = "promotion", action = "createmanydiscounttypes" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-discounttypse", "discounttypes",
                new { controller = "promotion", action = "updatemanydiscounttypes" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-discounttype", "discounttypes/{id}",
                new { controller = "promotion", action = "deletediscounttype" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            // add on

            routes.MapRoute(
                "get-addon", "addons/{id}",
                new { controller = "addon", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-addon", "addons",
                new { controller = "addon", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "create-addon", "addons",
                new { controller = "addon", action = "createmany" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-addon", "addons",
                new { controller = "addon", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-addon", "addons/{id}",
                new { controller = "addon", action = "delete" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            routes.MapRoute(
                "get-addonvalue", "addonvalues/{id}",
                new { controller = "addon", action = "getaddonvalue" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-addonvalue", "addonvalues",
                new { controller = "addon", action = "listaddonvalues" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "create-addonvalue", "addons/{id}/addonvalues",
                new { controller = "addon", action = "createmanyaddonvalues" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-addonvalue", "addons/{id}/addonvalues",
                new { controller = "addon", action = "updatemanyaddonvalues" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
               "delete-addonvalue", "addonvalues/{id}",
               new { controller = "addon", action = "deleteaddonvalue" },
               new { httpMethod = new HttpMethodConstraint("DELETE") });

            // saved cart

            routes.MapRoute(
                "get-savedcart", "savedcarts/{id}",
                new { controller = "savedcart", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-savedcart", "savedcarts",
                new { controller = "savedcart", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-savedcart", "savedcarts",
                 new { controller = "savedcart", action = "createmany" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-savedcart", "savedcarts",
                new { controller = "savedcart", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-savedcart", "savedcarts/{id}",
                new { controller = "savedcart", action = "delete" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            routes.MapRoute(
                "create-savedcartlineitem", "savedcarts/{savedcartid}/lines",
                new { controller = "savedcart", action = "createlines" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-savedcartlineitem", "savedcarts/{savedcartid}/lines",
                new { controller = "savedcart", action = "updatelines" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-savedcartlineitem", "savedcarts/{savedcartid}/lines/{id}",
                new { controller = "savedcart", action = "deleteline" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            routes.MapRoute(
                 "create-savedcartlineitembyproduct", "savedcarts/{savedcartid}/product",
                 new { controller = "savedcart", action = "createlineitembyproduct" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            // shipping

            routes.MapRoute(
                "get-shipping", "shipping/{id}",
                new { controller = "shipping", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-shipping", "shipping",
                new { controller = "shipping", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-shipping", "shipping",
                 new { controller = "shipping", action = "createmany" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-shipping", "shipping",
                new { controller = "shipping", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-shipping", "shipping/{id}",
                new { controller = "shipping", action = "delete" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            routes.MapRoute(
                "get-shippingrule", "shippingrules/{id}",
                new { controller = "shipping", action = "getshippingrule" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-shippingrules", "shippingrules",
                new { controller = "shipping", action = "listshippingrules" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            //This table has a 0 id row which breaks our current pattern if you update that row to be a value greater than 1 and fix any related relationships you can uncomment this and it will work
            //routes.MapRoute(
            //     "create-shippingrules", "shippingrules",
            //     new { controller = "shipping", action = "createmanyshippingrules" },
            //     new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-shippingrules", "shippingrules",
                new { controller = "shipping", action = "updatemanyshippingrules" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-shippingrule", "shippingrules/{id}",
                new { controller = "shipping", action = "deleteshippingrule" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            routes.MapRoute(
                "get-shippingruletype", "shippingruletypes/{id}",
                new { controller = "shipping", action = "getshippingruletype" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-shippingruletypes", "shippingruletypes",
                new { controller = "shipping", action = "listshippingruletypes" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-shippingruletypes", "shippingruletypes",
                 new { controller = "shipping", action = "createmanyshippingruletypes" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-shippingruletypes", "shippingruletypes",
                new { controller = "shipping", action = "updatemanyshippingruletypes" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-shippingruletypes", "shippingruletypes/{id}",
                new { controller = "shipping", action = "deleteshippingruletype" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            routes.MapRoute(
                "get-shippingtype", "shippingtypes/{id}",
                new { controller = "shipping", action = "getshippingtype" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-shippingtypes", "shippingtypes",
                new { controller = "shipping", action = "listshippingtypes" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            //This table needs to have its PK set to be an identity column that seeds starting at least by 1 (no rows can start with an id of 0 with our nettiers pattern), if this is done you can uncomment this and be able to create
            //routes.MapRoute(
            //     "create-shippingtypes", "shippingtypes",
            //     new { controller = "shipping", action = "createmanyshippingtypes" },
            //     new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-shippingtypes", "shippingtypes",
                new { controller = "shipping", action = "updatemanyshippingtypes" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-shippingtypes", "shippingtypes/{id}",
                new { controller = "shipping", action = "deleteshippingtype" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            // order

            #region Zeon Custom Code

            routes.MapRoute(
                "GetAllByDateRange-order", "orders/getallbydaterange",
                new { controller = "order", action = "GetAllByDateRange" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
               "GetAllByStatus-order", "orders/getallbystatus",
               new { controller = "order", action = "GetAllByStatus" },
               new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "UpdateStatus-order", "orders/updatestatus",
                new { controller = "order", action = "UpdateStatus" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "UpdateManyStatus-order", "orders/updatemanystatus",
                new { controller = "order", action = "UpdateManyStatus" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "UpdateLineItemStatus-order", "orders/updatelineitemstatus",
                new { controller = "order", action = "updatelineitemstatus" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
              "ExecuteSSISPackage-order", "ssisjobs/startreplicationdbtoznodedb",
              new { controller = "ssisjobs", action = "executepackage" },
              new { httpMethod = new HttpMethodConstraint("GET") });

            #endregion

            routes.MapRoute(
               "get-order", "orders/{id}",
               new { controller = "order", action = "get" },
               new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-order", "orders",
                new { controller = "order", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-order", "orders",
                 new { controller = "order", action = "createmany" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-order", "orders",
                new { controller = "order", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "create-orderlineitem", "orders/{orderid}/lines",
                new { controller = "order", action = "createlines" },
                new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-orderlineitem", "orders/{orderid}/lines",
                new { controller = "order", action = "updatelines" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            // Saved cart to order
            routes.MapRoute(
                 "create-ordersavedCart", "orders/savedcarts/{savedcartId}",
                 new { controller = "ordersavedcart", action = "createmany" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                 "get-ordersavedCart", "orders/savedcarts/{Id}",
                 new { controller = "ordersavedcart", action = "GET" },
                 new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "update-ordersavedCart", "orders/savedcarts",
                 new { controller = "ordersavedcart", action = "updatemany" },
                 new { httpMethod = new HttpMethodConstraint("PUT") });

            // Supplier

            routes.MapRoute(
                "get-supplier", "suppliers/{id}",
                new { controller = "supplier", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-suppliers", "suppliers",
                new { controller = "supplier", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-suppliers", "suppliers",
                 new { controller = "supplier", action = "createmany" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-suppliers", "suppliers",
                new { controller = "supplier", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
               "delete-supplier", "suppliers/{id}",
               new { controller = "supplier", action = "delete" },
               new { httpMethod = new HttpMethodConstraint("DELETE") });

            routes.MapRoute(
                "get-suppliertype", "suppliertypes/{id}",
                new { controller = "supplier", action = "getsuppliertype" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-suppliertypes", "suppliertypes",
                new { controller = "supplier", action = "listsuppliertypes" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-suppliertypes", "suppliertypes",
                 new { controller = "supplier", action = "createmanysuppliertypes" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-suppliertypes", "suppliertypes",
                new { controller = "supplier", action = "updatemanysuppliertypes" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
               "delete-suppliertype", "suppliertypes/{id}",
               new { controller = "supplier", action = "deletesuppliertype" },
               new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Taxes

            routes.MapRoute(
                "get-taxclass", "taxclasses/{id}",
                new { controller = "tax", action = "get" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-taxclasses", "taxclasses",
                new { controller = "tax", action = "list" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-taxclasses", "taxclasses",
                 new { controller = "tax", action = "createmany" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-taxclasses", "taxclasses",
                new { controller = "tax", action = "updatemany" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-taxclass", "taxclasses/{id}",
                new { controller = "tax", action = "delete" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            routes.MapRoute(
                "get-taxrule", "taxrules/{id}",
                new { controller = "tax", action = "gettaxrule" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-taxrules", "taxrules",
                new { controller = "tax", action = "listtaxrules" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-taxrules", "taxrules",
                 new { controller = "tax", action = "createmanytaxrules" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-taxrules", "taxrules",
                new { controller = "tax", action = "updatemanytaxrules" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-taxrule", "taxrules/{id}",
                new { controller = "tax", action = "deletetaxrule" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });

            routes.MapRoute(
                "get-taxruletype", "taxruletypes/{id}",
                new { controller = "tax", action = "gettaxruletype" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                "list-taxruletypes", "taxruletypes",
                new { controller = "tax", action = "listtaxruletypes" },
                new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute(
                 "create-taxruletypes", "taxruletypes",
                 new { controller = "tax", action = "createmanytaxruletypes" },
                 new { httpMethod = new HttpMethodConstraint("POST") });

            routes.MapRoute(
                "update-taxruletypes", "taxruletypes",
                new { controller = "tax", action = "updatemanytaxruletypes" },
                new { httpMethod = new HttpMethodConstraint("PUT") });

            routes.MapRoute(
                "delete-taxruletype", "taxruletypes/{id}",
                new { controller = "tax", action = "deletetaxruletype" },
                new { httpMethod = new HttpMethodConstraint("DELETE") });
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            Bootstrapper.Bootstrap(new NinjectConfiguration());

            // ZNodeApplicationContext.SetApplicationItem(AppConstants.ItemKey_LicenseKey, 32);

            ValueProviderFactories.Factories.Add(new ApiAttributeValueProviderFactory());
        }

        protected void Application_BeginRequest()
        {
            // Set the store config for the URL that has been requested. This call must be placed in the Begin Request for Multi Stores to work.
            if (ZNode.Libraries.Framework.Business.ZNodeConfigManager.CheckSiteConfigCache() == false)
            {
                if (ZNode.Libraries.Framework.Business.ZNodeConfigManager.SetSiteConfig() == false)
                {
                    EndRequestWithStatus((int)HttpStatusCode.InternalServerError, "ZNode buisness framework site config error.");
                    return;
                }
            }

            var apiKeyIsOK = CheckZnodeApiKey();
            if (!apiKeyIsOK)
            {
                EndRequestWithStatus((int)HttpStatusCode.Forbidden, "Incorrect or nonexistent API key.");
            }

            InitializeApplicationContext();
        }

        private void EndRequestWithStatus(int statusCode, string message = null, bool suppressContent = true)
        {
            var context = HttpContext.Current;

            var response = context.SafeGet(c => c.Response);
            if (response != null)
            {
                response.StatusCode = statusCode;
                response.SuppressContent = suppressContent;

                if (message != null)
                {
                    response.StatusDescription = message;
                }
            }

            var appInstance = context.SafeGet(c => c.ApplicationInstance);
            if (appInstance != null)
            {
                appInstance.CompleteRequest();
            }
        }

        private bool CheckZnodeApiKey()
        {
            var headers = HttpContext.Current
                .SafeGet(c => c.Request)
                .SafeGet(r => r.Headers, () => new NameValueCollection());

            var apiKey =
                (headers.AllKeys.Contains(AppConstants.HeaderKey_ZnodeApiKey))
                    ? headers[AppConstants.HeaderKey_ZnodeApiKey]
                    : null;

            string configuredApiKey = GetConfiguredApiKey();

            var result =
                string.Compare(apiKey, configuredApiKey, StringComparison.InvariantCulture) == 0;

            return result;
        }

        private string GetConfiguredApiKey()
        {
            var domainConfig = ZNode.Libraries.Framework.Business.ZNodeConfigManager.DomainConfig;

            var apiKey = domainConfig.SafeGet(dc => dc.ApiKey);

            return apiKey;
        }

        private void InitializeApplicationContext()
        {
            var httpContext = new HttpContextWrapper(this.Context);
            ZNodeApplicationContext.Initialize(httpContext);
        }

    }
}
﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace RogansShoes.Web.API.ValueProviders
{
    public class ApiAttributeValueProviderFactory : ValueProviderFactory
    {
        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            return new ApiAttributeValueProvider(controllerContext.HttpContext.Request.QueryString);
        }

        private class ApiAttributeValueProvider : IValueProvider
        {
            private const string ApiPrefix = "$";
            
            private readonly NameValueCollection _queryString;
            private readonly string[] _supportedApiAttributes = new[]{"expand", "filter"};

            public ApiAttributeValueProvider(NameValueCollection queryString)
            {
                _queryString = queryString;
            }

            public bool ContainsPrefix(string prefix)
            {
                var apiAttributeKey = ConvertToApiAttributeKey(prefix);
                return _supportedApiAttributes.Any(supportedApiAttribute => supportedApiAttribute.Equals(prefix, StringComparison.InvariantCultureIgnoreCase))
                    && _queryString.AllKeys.Any(key => key.Equals(apiAttributeKey, StringComparison.InvariantCultureIgnoreCase));
            }

            public ValueProviderResult GetValue(string key)
            {
                var val = _queryString[ConvertToApiAttributeKey(key)];
                return new ValueProviderResult(val, val, CultureInfo.InvariantCulture);
            }

            private string ConvertToApiAttributeKey(string key)
            {
                return string.Concat(ApiPrefix, key);
            }
        }
    }
}
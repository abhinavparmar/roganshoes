﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using Account = RogansShoes.Web.API.Models.Account;
using AccountProfile = RogansShoes.Web.API.Models.AccountProfile;

namespace RogansShoes.Web.API.Data
{
    public static class AccountDataBroker
    {
        private static Account GetAccountFromEntity(IAccount accountEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (accountEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var account = DataConverter.ToAccountDto(accountEntity);

            if (!account.UpdateDate.HasValue)
            {
                account.UpdateDate = DateTime.Now;
            }

            if (expands == null)
            {
                expands = new List<string>();
            }

            if (expands.Contains(Expands.Addresses))
            {
                var ads = new AddressService();

                account.Addresses = new Collection<AccountAddress>(ads.GetByAccountID(accountEntity.AccountID)
                    .Where(ac => FilterAddresses(ac, filters)).Select(DataConverter.ToAccountAddressDto).ToList());
            }

            if (expands.Contains(Expands.Profiles))
            {
                var aps = new AccountProfileService();
                var ps = new ProfileService();

                account.Profiles = new Collection<AccountProfile>(aps.GetByAccountID(accountEntity.AccountID)
                    .Where(ap => FilterProfiles(ap, filters)).Select(p =>
                    {
                        var profile = DataConverter.ToAccountProfileDto(p);

                        if (p.ProfileID != null)
                        {
                            profile.Name = ps.GetByProfileID(p.ProfileID.Value).Name;
                        }

                        return profile;
                    }).ToList());
            }

            return account;
        }

        private static bool FilterAddresses(IAddress addressEntity, IDictionary<string, string> filters)
        {
            if (filters.ContainsKey(Filters.FirstName) && addressEntity.FirstName != filters[Filters.FirstName])
            {
                return false;
            }
            if (filters.ContainsKey(Filters.LastName) && addressEntity.LastName != filters[Filters.LastName])
            {
                return false;
            }
            if (filters.ContainsKey(Filters.Company) && addressEntity.CompanyName != filters[Filters.Company])
            {
                return false;
            }
            if (filters.ContainsKey(Filters.PostalCode) && addressEntity.PostalCode != filters[Filters.PostalCode])
            {
                return false;
            }
            if (filters.ContainsKey(Filters.Phone) && addressEntity.PhoneNumber != filters[Filters.Phone])
            {
                return false;
            }
            return true;
        }

        private static bool FilterProfiles(IAccountProfile accountProfileEntity, IDictionary<string, string> filters)
        {
            if (filters == null)
            {
                return true;
            }
            var aps = new AccountProfileService();
            var pps = new PortalProfileService();
            var portalIds = aps.GetByAccountID(accountProfileEntity.AccountID).Where(ap => ap.ProfileID.HasValue)
                .SelectMany(ap => pps.GetByProfileID(ap.ProfileID.Value)).ToList();
            if (filters.ContainsKey(Filters.PortalId) && portalIds.All(pp => pp.PortalID != int.Parse(filters[Filters.PortalId])))
            {
                return false;
            }
            return true;
        }

        private static int GetAccountId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var ah = new AccountHelper();
                var accountId = ah.GetAccountIDByExternalID(id);
                return int.Parse(accountId);
            }
            return int.Parse(id);
        }

        private static int GetAccountId(string idType, Account account)
        {
            if (idType == IdTypes.External)
            {
                return GetAccountId(idType, account.ExternalId);
            }
            return int.Parse(account.AccountId);
        }

        public static Account GetAccountById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null?null:
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var acs = new AccountService();
            var accountEntity = acs.GetByAccountID(GetAccountId(idType, id));

            if (accountEntity == null)
            {
                throw new ResourceNotFoundException("Account not found.");
            }

            return GetAccountFromEntity(accountEntity, expands, filters);
        }

        public static Account GetAccountByAccount(Account account, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var acs = new AccountService();
            var accountEntity = acs.GetByAccountID(GetAccountId(idType, account));

            if (accountEntity == null)
            {
                throw new ResourceNotFoundException("Account not found.");
            }

            var filters = conditions == null?null:
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetAccountFromEntity(accountEntity, expands, filters);
        }

        public static List<Account> GetAllAccounts(int pageSize, int page, out int count, IList<string> expands,
                                                   List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var ah = new AccountHelper();

            filters = InitializeFilters(filters);

            foreach (var filter in filters)
            {
                if (filter.FieldName.Equals(Filters.ModifyDate, StringComparison.InvariantCultureIgnoreCase))
                    filter.FieldName = Filters.UpdateDte;
                if (filter.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
                    filter.FieldName = Filters.CreateDte;
            }

            var queryBuilder = new QueryBuilder("Account", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count,2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = ah.GetAccountIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetAccountById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();

            if (!fieldNames.Contains(Filters.Company))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.Company });
            }
            if (!fieldNames.Contains(Filters.FirstName))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.FirstName });
            }
            if (!fieldNames.Contains(Filters.LastName))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.LastName });
            }
            if (!fieldNames.Contains(Filters.Phone))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.Phone });
            }
            if (!fieldNames.Contains(Filters.PostalCode))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.PostalCode });
            }
            if (!fieldNames.Contains(Filters.PortalId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.PortalId });
            }
            return list;
        }

        public static string SaveAccount(Account account)
        {
            if (!String.IsNullOrEmpty(account.AccountId))
            {
                throw new InvalidOperationException("Posting a new account ID is not allowed.");
            }
            if (!String.IsNullOrEmpty(account.ExternalId))
            {
                var ah = new AccountHelper();
                var temp = ah.GetAccountIDByExternalID(account.ExternalId);
                if (temp != null)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var acs = new AccountService();
            var accountEntity = DataConverter.ToAccountEntity(account, acs);

            accountEntity.CreateDte = DateTime.Now;
            accountEntity.UpdateDte = DateTime.Now;

            var result = acs.Save(accountEntity);
            return result.AccountID.ToString();
        }

        public static void UpdateAccount(Account account, string idType = null)
        {
            account.AccountId = GetAccountId(idType, account).ToString();

            if (!String.IsNullOrEmpty(account.ExternalId))
            {
                var ah = new AccountHelper();
                var temp = ah.GetAccountIDByExternalID(account.ExternalId);
                if (temp != null && temp != account.AccountId)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var acs = new AccountService();
            var accountEntity = DataConverter.ToAccountEntity(account, acs);

            accountEntity.UpdateDte = DateTime.Now;

            if (!acs.Update(accountEntity))
            {
                throw new InvalidOperationException("Specfied account does not exist.");
            }
        }

        public static void SaveAccountAddress(AccountAddress accountAddress, string accountId, string accountIdType = null)
        {
            if (!string.IsNullOrEmpty(accountAddress.AddressId))
            {
                throw new InvalidOperationException("Posting a new account address ID is not allowed.");
            }

            accountAddress.AccountId = GetAccountId(accountIdType, accountId).ToString();

            var ads = new AddressService();
            var addressEntity = DataConverter.ToAccountAddressEntity(accountAddress, ads);

            ads.Save(addressEntity);

            var acs = new AccountService();
            var accountEntity = acs.GetByAccountID(addressEntity.AccountID);
            accountEntity.UpdateDte = DateTime.Now;
            acs.Update(accountEntity);
        }

        public static void UpdateAccountAddress(AccountAddress accountAddress, string accountId, string accountIdType = null)
        {
            accountAddress.AccountId = GetAccountId(accountIdType, accountId).ToString();

            var ads = new AddressService();
            var addressEntity = DataConverter.ToAccountAddressEntity(accountAddress, ads);

            if (!ads.Update(addressEntity))
            {
                throw new InvalidOperationException("Specfied account address does not exist.");
            }

            var acs = new AccountService();
            var accountEntity = acs.GetByAccountID(addressEntity.AccountID);
            accountEntity.UpdateDte = DateTime.Now;
            acs.Update(accountEntity);
        }

        public static void SaveAccountProfile(AccountProfile accountProfile, string accountId, string accountIdType = null)
        {
            if (!string.IsNullOrEmpty(accountProfile.AccountProfileId))
            {
                throw new InvalidOperationException("Posting a new account profile ID is not allowed.");
            }

            accountProfile.AccountId = GetAccountId(accountIdType, accountId).ToString();

            var aps = new AccountProfileService();
            var profileEntity = DataConverter.ToAccountProfileEntity(accountProfile, aps);

            aps.Save(profileEntity);

            var acs = new AccountService();
            var accountEntity = acs.GetByAccountID(profileEntity.AccountID);
            accountEntity.UpdateDte = DateTime.Now;
            acs.Update(accountEntity);
        }

        public static void UpdateAccountProfile(AccountProfile accountProfile, string accountId, string accountIdType = null)
        {
            accountProfile.AccountId = GetAccountId(accountIdType, accountId).ToString();

            var aps = new AccountProfileService();
            var profileEntity = DataConverter.ToAccountProfileEntity(accountProfile, aps);

            if (!aps.Update(profileEntity))
            {
                throw new InvalidOperationException("Specfied account profile does not exist.");
            }

            var acs = new AccountService();
            var accountEntity = acs.GetByAccountID(profileEntity.AccountID);
            accountEntity.UpdateDte = DateTime.Now;
            acs.Update(accountEntity);
        }
    }
}

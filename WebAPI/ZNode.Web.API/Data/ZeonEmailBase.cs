﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace RogansShoes.Web.API.Data
{
    public class ZeonEmailBase
    {
        /// <summary>
        /// Sends email using SMTP, Uses default network credentials(Embed inline image)
        /// </summary>
        /// <param name="To">Email address of the recipient.</param>
        /// <param name="From">Email address of the sender.</param>
        /// <param name="BCC">Blind carbon copy email address.</param>
        /// <param name="Subject">The subject line of the email.</param>
        /// <param name="Body">The body of the email.</param>
        /// <param name="IsBodyHtml">Set to True to send this email in HTML format.</param>
        public static void SendEmail(string recepientEmail, string senderEmail, string bccEmail, string subject, string messageText, bool isBodyHtml)
        {
            //create mail message
            MailMessage message = new MailMessage(senderEmail, recepientEmail);
            if (bccEmail.Length > 0)
            {
                message.Bcc.Add(bccEmail);
            }

            message.IsBodyHtml = isBodyHtml;
            message.Subject = subject;
            message.Body = messageText;

            //create mail client and send email
            SmtpClient emailClient = new SmtpClient();

            int port = 0;
            int.TryParse(ConfigurationManager.AppSettings["SMTPPort"].ToString(), out port);
            emailClient.Host = ConfigurationManager.AppSettings["SMTPServer"].ToString();
            emailClient.Port = port;
            emailClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());

            bool isSendAsync = ConfigurationManager.AppSettings["SendAsync"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["SendAsync"].ToString())
                ? bool.Parse(ConfigurationManager.AppSettings["SendAsync"].ToString()) : false;
            if (isSendAsync)
            {
                emailClient.SendAsync(message, null);
            }
            else
            {
                emailClient.Send(message);
            }
        }
    }
}
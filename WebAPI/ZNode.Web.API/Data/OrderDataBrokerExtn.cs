﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using Order = RogansShoes.Web.API.Models.Order;
using OrderLineItem = RogansShoes.Web.API.Models.OrderLineItem;
using OrderState = RogansShoes.Web.API.Models.OrderState;
using ZNode.Libraries.ECommerce.Catalog;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;
using ZNode.Libraries.Framework.Business;
using System.Web;
using RogansShoes.Web.API.Helper;
using System.Text;

namespace RogansShoes.Web.API.Data
{
    public static partial class OrderDataBroker
    {
        /// <summary>
        /// Get All Orders By Order Status
        /// </summary>
        /// <param name="orderStatus"></param>
        /// <param name="filters"></param>
        /// <param name="ordering"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static List<Order> GetAllOrdersByStatus(string orderStatus, List<QueryBuilder.WhereCondition> filters, List<string> ordering, out int count)
        {
            //var oh = new OrderHelper();
            //var initFilters = InitializeFilters(filters);
            string portalSKUStr = GetPortalWiseSKUAddtionalString();

            //foreach (var condition in initFilters)
            //{
            //    if (condition.FieldName.Equals(Filters.UpdateDate, StringComparison.InvariantCultureIgnoreCase))
            //    {
            //        condition.FieldName = Filters.Modified;
            //    }
            //    if (condition.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
            //    {
            //        condition.FieldName = Filters.Created;
            //    }
            //    //Zeon Custom Code to add Status condition:Starts
            //    if (condition.FieldName.Equals(Filters.Status, StringComparison.InvariantCultureIgnoreCase))
            //    {
            //        condition.FieldName = Filters.Status;
            //        condition.MatchValue = GetOrderStateIDByStatus(orderStatus);
            //    }
            //    //Zeon Custom Code to add Status condition:Starts
            //}

            //var queryBuilder = new QueryBuilder("Orders", initFilters);
            //var query = queryBuilder.GetQuery();

            //var arrayFilters = new string[initFilters.Count, 2];

            //#region Populate array

            //for (var i = 0; i < initFilters.Count; i++)
            //{
            //    for (var j = 0; j < 2; j++)
            //    {
            //        if (j.Equals(0))
            //        {
            //            if (initFilters[i].FieldName.Equals(Filters.Created) ||
            //                initFilters[i].FieldName.Equals(Filters.Modified))
            //            {
            //                arrayFilters[i, j] = "@p" + i;
            //            }
            //            else
            //            {
            //                arrayFilters[i, j] = "@" + initFilters[i].FieldName;
            //            }
            //        }
            //        if (j.Equals(1))
            //        {
            //            arrayFilters[i, j] = initFilters[i].MatchValue;
            //        }
            //    }
            //}

            //#endregion

            ////var ids = oh.GetOrderIDsByFilters(query, arrayFilters);
            var ids = OrderDBHelper.GetAPIOrderByStatus(GetOrderStateIDByStatus(orderStatus));

            count = ids.Count;
            var tempList = ids.Select(id => GetOrderById(id, null, filters)).ToList();
            tempList = tempList.Where(res => res.OrderStatus.ToLower().Contains(orderStatus.ToLower())).ToList();
            count = tempList.Count;
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);

            foreach (Order ord in returnList)
            {
                int lineItemRef = 1;
                foreach (OrderLineItem lineItms in ord.OrderLineItems)
                {
                    lineItms.LineItemRef = lineItemRef;
                    lineItemRef++;
                    if (!string.IsNullOrWhiteSpace(portalSKUStr))
                    {
                        lineItms.ItemSku = lineItms.ItemSku.StartsWith(portalSKUStr, StringComparison.OrdinalIgnoreCase) ? lineItms.ItemSku.Substring(portalSKUStr.Length) : lineItms.ItemSku;
                    }
                }
            }

            return returnList.ToList();
        }

        /// <summary>
        /// Get All Order By Order Date Range
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="filters"></param>
        /// <param name="ordering"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static List<Order> GetAllOrdersByDateRange(DateTime dateFrom, DateTime dateTo, List<QueryBuilder.WhereCondition> filters, List<string> ordering, out int count)
        {
            //var oh = new OrderHelper();
            //var initFilters = InitializeFilters(filters);
            string portalSKUStr = GetPortalWiseSKUAddtionalString();

            //foreach (var condition in initFilters)
            //{
            //    if (condition.FieldName.Equals(Filters.UpdateDate, StringComparison.InvariantCultureIgnoreCase))
            //    {
            //        condition.FieldName = Filters.Modified;
            //    }
            //    if (condition.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
            //    {
            //        condition.FieldName = Filters.Created;
            //    } 

            //}

            //var queryBuilder = new QueryBuilder("Orders", initFilters);
            //var query = queryBuilder.GetQuery();

            //var arrayFilters = new string[initFilters.Count, 2];

            //#region Populate array

            //for (var i = 0; i < initFilters.Count; i++)
            //{
            //    for (var j = 0; j < 2; j++)
            //    {
            //        if (j.Equals(0))
            //        {
            //            if (initFilters[i].FieldName.Equals(Filters.Created) ||
            //                initFilters[i].FieldName.Equals(Filters.Modified))
            //            {
            //                arrayFilters[i, j] = "@p" + i;
            //            }
            //            else
            //            {
            //                arrayFilters[i, j] = "@" + initFilters[i].FieldName;
            //            }
            //        }
            //        if (j.Equals(1))
            //        {
            //            arrayFilters[i, j] = initFilters[i].MatchValue;
            //        }
            //    }
            //}

            //#endregion

            //var ids = oh.GetOrderIDsByFilters(query, arrayFilters);
            var ids = OrderDBHelper.GetOrderIDSByDateRange(dateFrom, dateTo);

            count = ids.Count;
            var tempList = ids.Select(id => GetOrderById(id, null, filters)).ToList();
            // tempList = tempList.Where(res => res.OrderDate >= dateFrom && res.OrderDate <= dateTo).ToList();
            count = tempList.Count;
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);

            foreach (Order ord in returnList)
            {
                int lineItemRef = 1;
                foreach (OrderLineItem lineItms in ord.OrderLineItems)
                {
                    lineItms.LineItemRef = lineItemRef;
                    lineItemRef++;
                    if (!string.IsNullOrWhiteSpace(portalSKUStr))
                    {
                        lineItms.ItemSku = lineItms.ItemSku.StartsWith(portalSKUStr, StringComparison.OrdinalIgnoreCase) ? lineItms.ItemSku.Substring(portalSKUStr.Length) : lineItms.ItemSku;
                    }
                }
            }

            return returnList.ToList();
        }

        /// <summary>
        /// Get New Order Collection by ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="expands"></param>
        /// <param name="conditions"></param>
        /// <param name="idType"></param>
        /// <returns></returns>
        /*public static OrderUpdate GetUpdateOrderById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.Created, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.Modified, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var os = new OrderService();
            var orderEntity = os.GetByOrderID(GetOrderId(idType, id));

            if (orderEntity == null)
            {
                throw new ResourceNotFoundException("Order not found.");
            }

            Order ord = GetOrderFromEntity(orderEntity, expands, filters);

            OrderUpdate ordUpdateEntity = new OrderUpdate();
            ordUpdateEntity.OrderID = int.Parse(ord.OrderId);
            ordUpdateEntity.OrderStatus = ord.OrderStatus;
            ordUpdateEntity.TrackingNumber = ord.TrackingNumber;
            ordUpdateEntity.ShipDate = orderEntity.ShipDate;

            Collection<OrderUpdateLineItem> updateItems = new Collection<OrderUpdateLineItem>();

            var olis = new OrderLineItemService();
            TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> lineItems = olis.GetByOrderID(int.Parse(id));
            foreach (ZNode.Libraries.DataAccess.Entities.OrderLineItem ordLineItem in lineItems)
            {
                OrderUpdateLineItem updateLine = new OrderUpdateLineItem();
                updateLine.OrderID = ordLineItem.OrderID;
                updateLine.ZNodeSKU = ordLineItem.SKU;
                updateLine.LineItemTrasactionID = ordLineItem.TransactionNumber;
                updateLine.LineItemTrackingNumber = ordLineItem.TrackingNumber;

                var olisExt = new OrderLineItemExtnService();
                TList<ZNode.Libraries.DataAccess.Entities.OrderLineItemExtn> extList = olisExt.GetByOrderLineItemID(ordLineItem.OrderLineItemID);
                if (extList != null && extList.Count > 0)
                {
                    updateLine.LineItemShipServiceName = extList[0].ShipServiceName;
                }
                updateItems.Add(updateLine);
            }

            ordUpdateEntity.LineItemUpdate = updateItems;

            return ordUpdateEntity;
        }
        */


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="expands"></param>
        /// <param name="conditions"></param>
        /// <param name="idType"></param>
        /// <returns></returns>
        public static OrderUpdate GetUpdateOrderById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.Created, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.Modified, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var os = new OrderService();
            var orderEntity = os.GetByOrderID(GetOrderId(idType, id));

            if (orderEntity == null)
            {
                throw new ResourceNotFoundException("Order not found.");
            }

            Order ord = GetOrderFromEntity(orderEntity, expands, filters);

            OrderUpdate ordUpdateEntity = new OrderUpdate();
            ordUpdateEntity.OrderID = int.Parse(ord.OrderId);
            ordUpdateEntity.OrderStatus = ord.OrderStatus;
            ordUpdateEntity.TrackingNumber = ord.TrackingNumber;
            ordUpdateEntity.ShipDate = orderEntity.ShipDate;

            Collection<OrderUpdateLineItem> updateItems = new Collection<OrderUpdateLineItem>();

            var olis = new OrderLineItemService();
            TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> lineItems = olis.GetByOrderID(int.Parse(id));
            foreach (ZNode.Libraries.DataAccess.Entities.OrderLineItem ordLineItem in lineItems)
            {
                OrderUpdateLineItem updateLine = new OrderUpdateLineItem();
                updateLine.OrderID = ordLineItem.OrderID;
                updateLine.ZNodeSKU = ordLineItem.SKU;
                updateLine.LineItemTrasactionID = ordLineItem.TransactionNumber;
                updateLine.LineItemTrackingNumber = ordLineItem.TrackingNumber;

                var olisExt = new OrderLineItemExtnService();
                TList<ZNode.Libraries.DataAccess.Entities.OrderLineItemExtn> extList = olisExt.GetByOrderLineItemID(ordLineItem.OrderLineItemID);
                if (extList != null && extList.Count > 0)
                {
                    updateLine.LineItemShipServiceName = extList[0].ShipServiceName;
                }
                updateItems.Add(updateLine);
            }

            //ordUpdateEntity.ShippingInfo = new Collection<Models.ShippingInformation>();
            return ordUpdateEntity;
        }

        /// <summary>
        /// Get New Collection Of Orer By OrderID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="expands"></param>
        /// <param name="conditions"></param>
        /// <param name="idType"></param>
        /// <returns></returns>
        public static Collection<OrderUpdateLineItem> GetUpdateOrderLineItemByOrderID(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.Created, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.Modified, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            var os = new OrderService();
            var orderEntity = os.GetByOrderID(GetOrderId(idType, id));

            if (orderEntity == null)
            {
                throw new ResourceNotFoundException("Order not found.");
            }

            Collection<OrderUpdateLineItem> updatedLines = new Collection<OrderUpdateLineItem>();

            foreach (var line in orderEntity.OrderLineItemCollection)
            {
                OrderUpdateLineItem updatedLine = new OrderUpdateLineItem();
                updatedLine.OrderID = Convert.ToInt32(id);
                updatedLine.ZNodeSKU = line.SKU;
                updatedLine.LineItemTrackingNumber = line.TrackingNumber;
                updatedLine.LineItemTrasactionID = line.TransactionNumber;

                OrderLineItemExtnService lineExtService = new OrderLineItemExtnService();
                TList<OrderLineItemExtn> lineExtnList = lineExtService.GetByOrderLineItemID(line.OrderLineItemID);
                if (lineExtnList != null && lineExtnList.Count > 0)
                {
                    updatedLine.LineItemShipServiceName = lineExtnList[0].ShipServiceName;
                }

                updatedLines.Add(updatedLine);
            }

            return updatedLines;
        }

        /// <summary>
        /// Update the Order Details
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="orderStatus"></param>
        /// <param name="trackingNumber"></param>
        /// <param name="shipDate"></param>
        /// <param name="lineItems"></param>
        public static void UpdateOrderDetails(int orderID, string orderStatus, string trackingNumber, DateTime? shipDate, Collection<OrderUpdateLineItem> lineItems)
        {
            var os = new OrderService();
            var ord = os.GetByOrderID(orderID);
            int orderStatusID = 50;

            if (!string.IsNullOrEmpty(orderStatus))
            {
                switch (orderStatus.ToLower())
                {
                    case "cancelled":
                        orderStatusID = 40;
                        break;
                    case "returned":
                        orderStatusID = 30;
                        break;
                    case "shipped":
                        orderStatusID = 20;
                        break;
                    case "submitted":
                        orderStatusID = 10;
                        break;
                    default:
                        orderStatusID = 50;
                        break;
                }
                if (ord != null && ord.OrderID > 0)
                {
                    ord.OrderStateID = orderStatusID;
                    ord.Modified = DateTime.Now;
                }
                else
                {
                    OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(orderID, 0, string.Empty, "Order id does not exists");
                    throw new InvalidOperationException("Order id does not exists");
                }
            }

            if (!string.IsNullOrEmpty(trackingNumber))
            {
                ord.TrackingNumber = trackingNumber;
                ord.ShipDate = shipDate;
                ord.Modified = DateTime.Now;
            }

            if (!string.IsNullOrEmpty(orderStatus))
            {
                if (!os.Update(ord))
                {
                    string errorMessage = "<OrderDetail><Message>Specified order does not exist.</Message>";
                    errorMessage = errorMessage + "<TrackingNumber>" + ord.TrackingNumber + "</TrackingNumber>";
                    errorMessage = errorMessage + "<ShipDate>" + ord.ShipDate + "</ShipDate>";
                    errorMessage = errorMessage + "<OrderStateID>" + ord.OrderStateID + "</OrderStateID>";
                    errorMessage = errorMessage + "</OrderDetail>";

                    OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ord.OrderID, 0, string.Empty, errorMessage);

                    throw new InvalidOperationException("Specified order does not exist.");
                }
                else
                {
                    try
                    {

                        foreach (OrderUpdateLineItem updItem in lineItems)
                        {
                            try
                            {
                                UpdateOrderLineDetails(updItem.OrderID, updItem.ZNodeSKU, updItem.LineItemTrackingNumber, updItem.LineItemShipServiceName, updItem.LineItemTrasactionID, ord.BillingEmailId);
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = "<OrderLineItem><Message>" + ex.Message + "</Message>";
                                errorMessage = errorMessage + "<LineItemTrackingNumber>" + updItem.LineItemTrackingNumber + "</LineItemTrackingNumber>";
                                errorMessage = errorMessage + "<LineItemShipServiceName>" + updItem.LineItemShipServiceName + "</LineItemShipServiceName>";
                                errorMessage = errorMessage + "<LineItemTrasactionID>" + updItem.LineItemTrasactionID + "</LineItemTrasactionID>";
                                errorMessage = errorMessage + "<BillingEmailId>" + ord.BillingEmailId + "</BillingEmailId>";
                                errorMessage = errorMessage + "</OrderLineItem>";

                                OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ord.OrderID, 0, string.Empty, errorMessage);
                            }
                        }
                        try
                        {
                            EmailSentFunctionality(ord, lineItems);
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.Framework.Business.ZNodeLoggingBase.LogMessage("Error While SendingMail::BillingEmailID" + ord.BillingEmailId);
                            string errorMessage = "Error  While SendingMail::BillingEmailID in Updating Order ID:" + ord.OrderID + "!!Error while genrating the email template!!Method Name!!UpdateOrderDetails!!" + ex.ToString();
                            OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ord.OrderID, 0, string.Empty, errorMessage);

                        }
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidOperationException("Error while updating order lines. Error:- " + ex.StackTrace);
                        string errorMessage = "Error  While SendingMail::BillingEmailID in Updating Order ID:" + ord.OrderID + "!!Error while genrating the email template!!Method Name!!UpdateOrderDetails!!" + ex.ToString();
                        OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ord.OrderID, 0, string.Empty, errorMessage);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="orderStatus"></param>
        /// <param name="trackingNumber"></param>
        /// <param name="shipDate"></param>
        /// <param name="lineItems"></param>
        public static void UpdateOrder(int orderID, string orderStatus, string trackingNumber, DateTime? shipDate, Collection<ShippingInformation> shippingInformation)
        {
            var os = new OrderService();
            var ord = os.GetByOrderID(orderID);
            int orderStatusID = 50;
            StringBuilder shippingInfo = new StringBuilder();
            if (!string.IsNullOrEmpty(orderStatus))
            {
                switch (orderStatus.ToLower())
                {
                    case "cancelled":
                        orderStatusID = 40;
                        break;
                    case "returned":
                        orderStatusID = 30;
                        break;
                    case "shipped":
                        orderStatusID = 20;
                        break;
                    case "submitted":
                        orderStatusID = 10;
                        break;
                    default:
                        orderStatusID = 50;
                        break;
                }
                if (ord != null && ord.OrderID > 0)
                {
                    ord.OrderStateID = orderStatusID;
                    ord.Modified = DateTime.Now;
                }
                else
                {
                    OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(orderID, 0, string.Empty, "Order id does not exists");
                    throw new InvalidOperationException("Order id does not exists");
                }
            }


            //Changes done on date :: 11/25/2014::Add Ship Date and Modified Date ::No matter what the tracking number is- start
            ord.ShipDate = shipDate;
            ord.Modified = DateTime.Now;
            //Changes done on date :: 11/25/2014::Add Ship Date and Modified Date ::No matter what the tracking number is- end

            if (!string.IsNullOrEmpty(trackingNumber))
            {
                ord.TrackingNumber = trackingNumber;
            }
            if (shippingInformation != null && shippingInformation.Count > 0)
            {
                shippingInfo.Append("<ShippingInformation>");
                foreach (ShippingInformation updItem in shippingInformation)
                {
                    shippingInfo.Append("<ShippingInfo>");
                    shippingInfo.Append("<TrackingNumber>" + updItem.TrackingNumber + "</TrackingNumber>");
                    shippingInfo.Append("<ShippingName>" + updItem.Name + "</ShippingName>");
                    shippingInfo.Append("<TransactionID>" + updItem.TransactionID + "</TransactionID>");
                    shippingInfo.Append("</ShippingInfo>");
                }
                shippingInfo.Append("</ShippingInformation>");

                ord.Custom1 = shippingInfo.ToString();
            }
            if (!string.IsNullOrEmpty(orderStatus))
            {
                bool orderUpdateStatus = os.Update(ord);
                if (orderUpdateStatus)
                {
                    try
                    {
                        //var orderLineItemService = new OrderLineItemService();
                        //var lineItems = orderLineItemService.GetByOrderID(ord.OrderID);
                        if (orderStatusID == 20)
                        {
                            EmailSentFunctionality(ord);
                        }
                        else
                        {
                            //Send Order Status Changes mail
                            SendOrderStatusMail(ord, orderStatus.ToLower());
                        }
                    }
                    catch (Exception ex)
                    {
                        ZNode.Libraries.Framework.Business.ZNodeLoggingBase.LogMessage("Error While SendingMail::BillingEmailID" + ord.BillingEmailId);
                        string errorMessage = "Error While SendingMail in Updating Order ID:" + ord.OrderID + "!!Error while genrating the email template!!Method Name!!UpdateOrder!!" + ex.ToString();
                        OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ord.OrderID, 0, string.Empty, errorMessage);

                    }
                }
                else if (!orderUpdateStatus)
                {
                    string errorMessage = "<OrderDetail><Message>Specified order does not exist.</Message>";
                    errorMessage = errorMessage + "<TrackingNumber>" + ord.TrackingNumber + "</TrackingNumber>";
                    errorMessage = errorMessage + "<ShipDate>" + ord.ShipDate + "</ShipDate>";
                    errorMessage = errorMessage + "<OrderStateID>" + ord.OrderStateID + "</OrderStateID>";
                    errorMessage = errorMessage + "</OrderDetail>";

                    OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ord.OrderID, 0, string.Empty, errorMessage);

                    throw new InvalidOperationException("Specified order does not exist.");
                }
            }
        }


        /// <summary>
        /// Update the Order Line Item Details
        /// </summary>
        /// <param name="ordID"></param>
        /// <param name="sku"></param>
        /// <param name="lineItemTrackingNo"></param>
        /// <param name="lineItemShipName"></param>
        /// <param name="lineItemTransID"></param>
        /// <param name="emailID"></param>
        public static void UpdateOrderLineDetails(int ordID, string sku, string lineItemTrackingNo, string lineItemShipName, string lineItemTransID, string emailID)
        {
            var olis = new OrderLineItemService();
            var olisExt = new OrderLineItemExtnService();

            TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> itmList = olis.Find("OrderID = " + ordID + " AND SKU= " + sku + ""); //.GetByOrderLineItemID(ordLineItemID);
            if (itmList != null && itmList.Count > 0)
            {
                int ordLineItemID = itmList[0].OrderLineItemID;
                ZNode.Libraries.DataAccess.Entities.OrderLineItem itm = itmList[0];
                itm.TrackingNumber = lineItemTrackingNo;
                itm.TransactionNumber = lineItemTransID;

                if (olis.Update(itm))
                {
                    TList<OrderLineItemExtn> lineExtList = olisExt.GetByOrderLineItemID(ordLineItemID);
                    if (lineExtList != null && lineExtList.Count > 0)
                    {
                        lineExtList[0].ShipServiceName = lineItemShipName;

                        if (!olisExt.Update(lineExtList[0]))
                        {
                            string errorMessage = "<OrderLineItem>";
                            errorMessage = errorMessage + "<Message>Error while updating Ship Service Name</Message>";
                            errorMessage = errorMessage + "<TrackingNumber>" + lineItemTrackingNo + "</TrackingNumber>";
                            errorMessage = errorMessage + "<TransactionNumber>" + lineItemTransID + "</TransactionNumber>";
                            errorMessage = errorMessage + "</OrderLineItem>";
                            OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ordID, ordLineItemID, sku, errorMessage);
                            throw new InvalidOperationException("Error while updating Ship Service Name");
                        }
                    }
                    else
                    {
                        OrderLineItemExtn ordLineExt = new OrderLineItemExtn();
                        ordLineExt.OrderLineItemID = ordLineItemID;
                        ordLineExt.ShipServiceName = lineItemShipName;

                        if (!olisExt.Insert(ordLineExt))
                        {
                            string errorMessage = "<OrderLineItem>";
                            errorMessage = errorMessage + "<Message>Error while adding Ship Service Name</Message>";
                            errorMessage = errorMessage + "<ShipServiceName>" + lineItemShipName + "</ShipServiceName>";
                            errorMessage = errorMessage + "</OrderLineItem>";

                            OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ordID, ordLineItemID, sku, errorMessage);
                            throw new InvalidOperationException("Error while adding Ship Service Name");
                        }
                    }
                }
                else
                {
                    string errorMessage = "<OrderLineItem>";
                    errorMessage = errorMessage + "<Message>Error while updating the order line item</Message>";
                    errorMessage = errorMessage + "</OrderLineItem>";

                    OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ordID, ordLineItemID, sku, errorMessage);
                    throw new InvalidOperationException("Specfied order line item does not exist.");
                }
            }
            else
            {
                OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ordID, 0, sku, "Order id does not exists");
            }
        }

        #region Complete Order Email Send

        /// <summary>
        /// Sends the email
        /// </summary>
        /// <param name="ord"></param>
        /// <param name="lineItems"></param>
        private static void EmailSentFunctionality(ZNode.Libraries.DataAccess.Entities.Order ord, Collection<OrderUpdateLineItem> lineItems)
        {
            try
            {
                var olis = new OrderLineItemService();
                TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> ordItems = olis.GetByOrderID(ord.OrderID);
                if (ordItems != null)
                {
                    if (ordItems.Count.Equals(lineItems.Count))
                    {
                        SendOrderShippedEmailWithLines(ord.OrderID, ord.BillingEmailId, ord.TrackingNumber, lineItems, ordItems, ord.PortalId.GetValueOrDefault(0));
                    }
                    else
                    {
                        int alreadySent = 0;
                        int remainingItem = 0;
                        foreach (ZNode.Libraries.DataAccess.Entities.OrderLineItem lineItm in ordItems)
                        {
                            if (!string.IsNullOrEmpty(lineItm.TrackingNumber.Trim()))
                            {
                                alreadySent++;
                            }
                        }

                        remainingItem = ordItems.Count - alreadySent;

                        if (remainingItem.Equals(alreadySent))
                        {
                            SendOrderShippedEmailWithLines(ord.OrderID, ord.BillingEmailId, ord.TrackingNumber, lineItems, ordItems, ord.PortalId.GetValueOrDefault(0));
                        }
                        else
                        {
                            SendOrderLineItemsShippedEmail(ord.OrderID, ord.BillingEmailId, lineItems, ordItems, ord.PortalId.GetValueOrDefault(0));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Error in Updating Order ID:" + ord.OrderID + "!!Error while genrating the email template!!Method Name!!EmailSentFunctionality!!" + ex.ToString();
                OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ord.OrderID, 0, string.Empty, errorMessage);
                throw ex;
            }

        }


        /// <summary>
        /// Sends the email
        /// </summary>
        /// <param name="ord"></param>
        /// <param name="lineItems"></param>
        private static void EmailSentFunctionality(ZNode.Libraries.DataAccess.Entities.Order ord)
        {
            try
            {
                var olis = new OrderLineItemService();
                TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> ordItems = olis.GetByOrderID(ord.OrderID);
                if (ordItems != null && ordItems.Count > 0)
                {
                    SendOrderShippingEmailStatus(ord, ordItems);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Error in Updating Order ID:" + ord.OrderID + "!!Error while genrating the email template!!Method Name!!EmailSentFunctionality!!" + ex.ToString();
                OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ord.OrderID, 0, string.Empty, errorMessage);
                throw ex;
            }

        }

        /// <summary>
        /// Send Complete Order Sent email notification
        /// </summary>
        /// <param name="ordID"></param>
        /// <param name="emailID"></param>
        /// <param name="trackingNum"></param>
        /// <param name="lineItems"></param>
        /// <param name="ordItems"></param>
        private static void SendOrderShippedEmailWithLines(int ordID, string emailID, string trackingNum, Collection<OrderUpdateLineItem> lineItems, TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> ordItems, int portalID)
        {
            string orderConfirmationEmailTemplate = GetOrderEmailTemplate(Convert.ToString(ordID), trackingNum, lineItems, ordItems);
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            if (!string.IsNullOrEmpty(orderConfirmationEmailTemplate))
            {
                string subject = ZNodeCatalogManager.MessageConfig.GetMessage("WebAPIOrderEmailConfirmationSubject", portalID, ZNodeCatalogManager.CatalogConfig.LocaleID);
                string bcc = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"] != null && !string.IsNullOrEmpty(System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"].ToString()))
                {
                    bcc += "," + System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"].ToString();
                }
                SendAPIEmail.SendEmail(emailID, ZNodeConfigManager.SiteConfig.CustomerServiceEmail, bcc, subject, orderConfirmationEmailTemplate, true);
            }
        }

        /// <summary>
        /// Send Order line item(s) Sent email notification
        /// </summary>
        /// <param name="ordID"></param>
        /// <param name="emailID"></param>
        /// <param name="lineItems"></param>
        /// <param name="ordItems"></param>
        private static void SendOrderLineItemsShippedEmail(int ordID, string emailID, Collection<OrderUpdateLineItem> lineItems, TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> ordItems, int portalID)
        {
            string orderConfirmationEmailTemplate = GetOrderEmailTemplate(Convert.ToString(ordID), string.Empty, lineItems, ordItems);
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            if (!string.IsNullOrEmpty(orderConfirmationEmailTemplate))
            {
                string subject = ZNodeCatalogManager.MessageConfig.GetMessage("WebAPILineItemEmailConfirmationSubject", portalID, ZNodeCatalogManager.CatalogConfig.LocaleID);
                string bcc = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"] != null && !string.IsNullOrEmpty(System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"].ToString()))
                {
                    bcc += "," + System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"].ToString();
                }
                SendAPIEmail.SendEmail(emailID, ZNodeConfigManager.SiteConfig.CustomerServiceEmail, bcc, subject, orderConfirmationEmailTemplate, true);
            }
        }

        /// <summary>
        /// Send Order line item(s) Sent email notification
        /// </summary>
        /// <param name="ordID"></param>
        /// <param name="emailID"></param>
        /// <param name="lineItems"></param>
        /// <param name="ordItems"></param>
        private static void SendOrderShippingEmailStatus(ZNode.Libraries.DataAccess.Entities.Order ord, TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> ordItems)
        {
            PortalService service = new PortalService();
            ZNode.Libraries.DataAccess.Entities.Portal portal = service.GetByPortalID(ord.PortalId.GetValueOrDefault(0));
            string customerServiceEmail = portal != null ? portal.CustomerServiceEmail : string.Empty;
            string orderConfirmationEmailTemplate = GetOrderEmailTemplate(ord, ordItems, portal);

            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            if (!string.IsNullOrEmpty(orderConfirmationEmailTemplate))
            {
                string subject = ZNodeCatalogManager.MessageConfig.GetMessage("WebAPILineItemEmailConfirmationSubject", ord.PortalId.GetValueOrDefault(0), ZNodeCatalogManager.CatalogConfig.LocaleID);
                string bcc = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"] != null && !string.IsNullOrEmpty(System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"].ToString()))
                {
                    bcc += "," + System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"].ToString();
                }
                SendAPIEmail.SendEmail(ord.BillingEmailId, customerServiceEmail, bcc, subject, orderConfirmationEmailTemplate, true);
            }
        }

        /// <summary>
        /// Get the email template
        /// </summary>
        /// <param name="ordID"></param>
        /// <param name="trackingNo"></param>
        /// <param name="lines"></param>
        /// <param name="ordItems"></param>
        /// <returns></returns>
        private static string GetOrderEmailTemplate(string ordID, string trackingNo, Collection<OrderUpdateLineItem> lines, TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> ordItems)
        {
            string emailTemplatePath = ConfigurationManager.AppSettings["EmailTemplatePath"] != null ? ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() : "~/Data/default/config/OrderStatusNotification.html";
            if (!string.IsNullOrEmpty(emailTemplatePath))
            {
                string defaultTemplatePath = HttpContext.Current.Server.MapPath(emailTemplatePath);

                DataTable OrderTable = new DataTable();
                OrderTable.Columns.Add("OrderID");
                if (!string.IsNullOrEmpty(trackingNo))
                {
                    OrderTable.Columns.Add("TrackingNumber");
                }

                DataRow orderDBRow = OrderTable.NewRow();
                orderDBRow["OrderID"] = ordID;
                if (!string.IsNullOrEmpty(trackingNo))
                {
                    orderDBRow["TrackingNumber"] = trackingNo;
                }
                OrderTable.Rows.Add(orderDBRow);

                DataTable OrderLinesTable = new DataTable();
                OrderLinesTable.Columns.Add("ProductName");
                OrderLinesTable.Columns.Add("SKU");
                OrderLinesTable.Columns.Add("TrackingNumber");
                OrderLinesTable.Columns.Add("ShipServiceName");

                BuildOrderLineItemTable(OrderLinesTable, lines, ordItems);

                RogansShoes.Web.API.Helper.ZNodeHtmlTemplate template = new RogansShoes.Web.API.Helper.ZNodeHtmlTemplate();
                template.Path = defaultTemplatePath;
                template.Parse(OrderTable.CreateDataReader());
                template.Parse("LineItems", OrderLinesTable.CreateDataReader());
                return template.Output;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get the email template
        /// </summary>
        /// <param name="ordID"></param>
        /// <param name="trackingNo"></param>
        /// <param name="lines"></param>
        /// <param name="ordItems"></param>
        /// <returns></returns>
        private static string GetOrderEmailTemplate(ZNode.Libraries.DataAccess.Entities.Order orderDetail, TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> ordItems, ZNode.Libraries.DataAccess.Entities.Portal portal)
        {
            try
            {
                string emailTemplatePath = ConfigurationManager.AppSettings["OrderShippedEmailTemplatePath"] != null
                    ? ConfigurationManager.AppSettings["OrderShippedEmailTemplatePath"].ToString() : "~/Data/default/config/OrderShippedStatusNotification.html"; ;
                string spacerImagePath = string.Empty;
                if (!string.IsNullOrEmpty(emailTemplatePath))
                {
                    string defaultTemplatePath = HttpContext.Current.Server.MapPath(emailTemplatePath);

                    // Order DataTable
                    // Create Order Table
                    DataTable OrderTable = new DataTable();

                    // Add a columns to the ordertable
                    // Additional Info
                    OrderTable.Columns.Add("SiteName");
                    OrderTable.Columns.Add("ReceiptText");
                    //OrderTable.Columns.Add("AdditionalInstructions");
                    //OrderTable.Columns.Add("AdditionalInstructLabel");

                    // Payment Info
                    OrderTable.Columns.Add("TrackingInformation");
                    //OrderTable.Columns.Add("ShippingName");
                    OrderTable.Columns.Add("PONumber");
                    OrderTable.Columns.Add("PurchaseNumberLabel");

                    // Customer Info
                    OrderTable.Columns.Add("OrderId");
                    OrderTable.Columns.Add("OrderDate");
                    OrderTable.Columns.Add("BillingAddress");
                    OrderTable.Columns.Add("ShippingAddress");
                    OrderTable.Columns.Add("TotalCost");
                    OrderTable.Columns.Add("StyleSheetPath");
                    OrderTable.Columns.Add("FooterText");

                    //Spacer Image
                    OrderTable.Columns.Add("SpacerImage");


                    // Create Order Amount Table
                    DataTable OrderAmountTable = new DataTable();
                    OrderAmountTable.Columns.Add("Title");
                    OrderAmountTable.Columns.Add("Amount");


                    // Create a NewRow.
                    DataRow orderDBRow = OrderTable.NewRow();


                    //Spacer Image
                    //spacerImagePath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
                    spacerImagePath = ZNodeCatalogManager.MessageConfig.GetMessage("APIOrderReceiptSpaceImagePath", orderDetail.PortalId.GetValueOrDefault(0), ZNodeCatalogManager.CatalogConfig.LocaleID);
                    if (!string.IsNullOrEmpty(spacerImagePath))
                    {
                        orderDBRow["SpacerImage"] = spacerImagePath;
                    }
                    else
                    {
                        orderDBRow["SpacerImage"] = spacerImagePath;
                    }


                    //PortalService service = new PortalService();
                    //ZNode.Libraries.DataAccess.Entities.Portal portal = service.GetByPortalID(orderDetail.PortalId.GetValueOrDefault(0));

                    // Additional Info
                    orderDBRow["SiteName"] = portal != null && !string.IsNullOrEmpty(portal.StoreName) ? portal.StoreName : string.Empty;
                    orderDBRow["ReceiptText"] = ZNodeCatalogManager.MessageConfig.GetMessage("ShippingEmailReceiptText", orderDetail.PortalId.GetValueOrDefault(0), ZNodeCatalogManager.CatalogConfig.LocaleID);


                    // Customer Info
                    orderDBRow["OrderId"] = orderDetail.OrderID.ToString();
                    try
                    {
                        orderDBRow["OrderDate"] = orderDetail.ShipDate != null && !string.IsNullOrWhiteSpace(orderDetail.ShipDate.ToString()) ? orderDetail.ShipDate.Value.ToShortDateString() : string.Empty;
                        //Convert.ToDateTime(orderDetail.ShipDate.ToString()).ToShortDateString();
                    }
                    catch (Exception ex)
                    {
                        ZNodeLoggingBase.LogMessage("Error in Updating Order ID:" + orderDetail.OrderID + "!!Error while genrating the email template!!Method Name!!GetOrderEmailTemplate!!in ShipDate" + ex.ToString());
                        string errorMessage = "Error in Updating Order ID:" + orderDetail.OrderID + "!!Error while genrating the email template!!Method Name!!GetOrderEmailTemplate!! in ShipDate" + ex.ToString();
                        OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(orderDetail.OrderID, 0, string.Empty, errorMessage);

                    }

                    orderDBRow["BillingAddress"] = GetFormattedBillingAddress(orderDetail);//Zeon Custom Code
                    orderDBRow["ShippingAddress"] = GetFormattedShippingAddress(orderDetail);

                    var shippingInfo = new RogansShoes.Web.API.Helper.zShippingInformation();
                    string carriername = string.Empty;
                    if (!string.IsNullOrEmpty(orderDetail.Custom1))
                    {
                        RogansShoes.Web.API.Helper.zShippingInformation shippingInformation = new RogansShoes.Web.API.Helper.zShippingInformation();
                        shippingInfo = shippingInformation.GetShippingInformation(orderDetail.Custom1);

                        if (shippingInfo != null && shippingInfo.ShippingInfo != null && shippingInfo.ShippingInfo.Count > 0)
                        {
                            StringBuilder strShippingInformation = new StringBuilder();
                            foreach (RogansShoes.Web.API.Helper.ShippingInfo ShippingInfo in shippingInfo.ShippingInfo)
                            {
                                string trackingURL = string.Empty;
                                string anchor = "<a target=\"_blank\" href=\"#href#\">#TrackingNumber# #Name#</a>";
                                carriername = carriername + ShippingInfo.Name + ",";
                                if (ShippingInfo.Name.ToLower().Contains("ups"))
                                {
                                    trackingURL = HttpContext.Current.Server.HtmlDecode(ConfigurationManager.AppSettings["UPS"].ToString());

                                }
                                else if (ShippingInfo.Name.ToLower().Contains("speedee"))
                                {
                                    trackingURL = HttpContext.Current.Server.HtmlDecode(ConfigurationManager.AppSettings["SPEEDY"].ToString());

                                }
                                else if (ShippingInfo.Name.ToLower().Contains("usps"))
                                {
                                    trackingURL = HttpContext.Current.Server.HtmlDecode(ConfigurationManager.AppSettings["USPS"].ToString());

                                }
                                else if (ShippingInfo.Name.ToLower().Contains("fedex"))
                                {
                                    trackingURL = HttpContext.Current.Server.HtmlDecode(ConfigurationManager.AppSettings["FEDEX"].ToString());
                                }
                                anchor = anchor.Replace("#href#", trackingURL);
                                anchor = anchor.Replace("#TrackingNumber#", ShippingInfo.TrackingNumber).Replace("#Name#", " (" + ShippingInfo.Name + ")");
                                strShippingInformation.Append(anchor);
                                strShippingInformation.Append("<br/>");
                            }

                            orderDBRow["TrackingInformation"] = strShippingInformation.ToString();
                        }
                    }

                    BuildOrderAmountInfo("SubTotal", orderDetail.SubTotal.Value, OrderAmountTable);
                    BuildOrderAmountInfo("TaxCost", orderDetail.TaxCost.Value, OrderAmountTable);
                    BuildOrderAmountInfo("SalesTax", orderDetail.SalesTax.Value, OrderAmountTable);
                    BuildOrderAmountInfo("DiscountAmount", -orderDetail.DiscountAmount.Value, OrderAmountTable);
                    BuildOrderAmountInfo("DiscountAmount", -orderDetail.DiscountAmount.Value, OrderAmountTable);
                    BuildOrderAmountInfo("ShippingCost", orderDetail.ShippingCost.Value, OrderAmountTable);

                    orderDBRow["TotalCost"] = orderDetail.Total.Value.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();

                    var msg = ZNodeCatalogManager.MessageConfig.GetMessage("ShippedFooterHTML", orderDetail.PortalId.GetValueOrDefault(0), ZNodeCatalogManager.CatalogConfig.LocaleID);
                    if (!string.IsNullOrEmpty(msg))
                    {
                        if (carriername.EndsWith(","))
                        {
                            char[] trimchar = { ',' };
                            carriername = carriername.TrimEnd(trimchar);
                        }
                        orderDBRow["FooterText"] = msg.Replace("#carriername#", carriername);
                    }
                    else
                    {
                        orderDBRow["FooterText"] = string.Empty;
                    }


                    // Create OrderlineItem Table
                    DataTable OrderlineItemTable = new DataTable();


                    OrderlineItemTable.Columns.Add("Quantity");
                    //OrderlineItemTable.Columns.Add("SKU");
                    OrderlineItemTable.Columns.Add("IMAGE");
                    OrderlineItemTable.Columns.Add("Description");
                    OrderlineItemTable.Columns.Add("Name");
                    OrderlineItemTable.Columns.Add("Price");
                    OrderlineItemTable.Columns.Add("ExtendedPrice");



                    BuildOrderLineItemTable(OrderlineItemTable, ordItems);

                    // Add rows to Order datatable.
                    OrderTable.Rows.Add(orderDBRow);

                    // Html Parser
                    // Parse the template
                    RogansShoes.Web.API.Helper.ZNodeHtmlTemplate template = new RogansShoes.Web.API.Helper.ZNodeHtmlTemplate();

                    // set the html template path
                    template.Path = defaultTemplatePath;

                    // Parse Order
                    template.Parse(OrderTable.CreateDataReader());

                    // Parse OrderLineItem
                    template.Parse("LineItems", OrderlineItemTable.CreateDataReader());

                    // Parse OrderLineItem
                    template.Parse("AmountLineItems", OrderAmountTable.CreateDataReader());

                    // template html output
                    return template.Output;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                ZNodeLoggingBase.LogMessage("Error in Updating Order ID:" + orderDetail.OrderID + "!!Error while genrating the email template!!Method Name!!GetOrderEmailTemplate!!" + ex.ToString());
                string errorMessage = "Error in Updating Order ID:" + orderDetail.OrderID + "!!Error while genrating the email template!!Method Name!!GetOrderEmailTemplate!! in ShipDate" + ex.ToString();
                OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(orderDetail.OrderID, 0, string.Empty, errorMessage);
                return string.Empty;
            }
        }

        /// <summary>
        /// Prepare line items table
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="lines"></param>
        /// <param name="ordItems"></param>
        private static void BuildOrderLineItemTable(DataTable dt, Collection<OrderUpdateLineItem> lines, TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> ordItems)
        {
            foreach (OrderUpdateLineItem itm in lines)
            {
                string prodName = string.Empty;
                ZNode.Libraries.DataAccess.Entities.OrderLineItem ordItm = ordItems.Find("SKU", itm.ZNodeSKU);
                if (ordItm != null)
                {
                    prodName = ordItm.Name;
                }
                DataRow dr = dt.NewRow();
                dr["ProductName"] = prodName;
                dr["SKU"] = itm.ZNodeSKU;
                dr["TrackingNumber"] = itm.LineItemTrackingNumber;
                dr["ShipServiceName"] = itm.LineItemShipServiceName;

                dt.Rows.Add(dr);
            }
        }

        /// <summary>
        /// Prepare line items table
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="lines"></param>
        /// <param name="ordItems"></param>
        private static void BuildOrderLineItemTable(DataTable dt, TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> orderLineItems)
        {
            foreach (ZNode.Libraries.DataAccess.Entities.OrderLineItem item in orderLineItems)
            {
                string prodName = string.Empty;
                //ZNode.Libraries.DataAccess.Entities.OrderLineItem ordItm = orderLineItems.Find("SKU", item.ZNodeSKU);
                DataRow dr = dt.NewRow();
                dr["Quantity"] = item.Quantity;
                //dr["SKU"] = item.SKU;
                dr["IMAGE"] = "<img src='" + item.Custom2 + "' alt=''>";  //"<img src=\"http://prod.rogansshoes.zeondemo.com/data/default/images/catalog/55/KS_82441424_BLK1.JPG\" alt=''>"; //item.SKU;
                dr["Description"] = item.Description;
                dr["Name"] = item.Name + "<br/>Item#" + item.SKU;
                dr["Price"] = item.Price.GetValueOrDefault(0).ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();
                dr["ExtendedPrice"] = decimal.Parse((item.Price * item.Quantity).ToString()).ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();

                dt.Rows.Add(dr);
            }
        }

        /// <summary>
        /// Build the order amount information
        /// </summary>
        /// <param name="title">Title of the order</param>
        /// <param name="amount">Amount of the order</param>
        /// <param name="table">data table </param>
        private static void BuildOrderAmountInfo(string title, decimal amount, DataTable table)
        {
            // Amount Info
            if (amount != 0)
            {
                DataRow orderDBRow = table.NewRow();
                orderDBRow["Title"] = title;
                orderDBRow["Amount"] = amount.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();
                table.Rows.Add(orderDBRow);
            }

        }

        public static string GetFormattedBillingAddress(ZNode.Libraries.DataAccess.Entities.Order orderDetail)
        {
            StringBuilder sbHTMLAddress = new StringBuilder();
            if (orderDetail != null)
            {
                string addressName = string.Empty;
                string postalDetails = string.Empty;

                sbHTMLAddress.Append(orderDetail.BillingFirstName);
                sbHTMLAddress.Append(" ");
                sbHTMLAddress.Append(orderDetail.BillingLastName);
                sbHTMLAddress.Append(GetFormattedText(orderDetail.BillingCompanyName));
                sbHTMLAddress.Append(GetFormattedText(orderDetail.BillingStreet));
                sbHTMLAddress.Append(GetFormattedText(orderDetail.BillingStreet1));
                postalDetails = orderDetail.BillingCity + "," + orderDetail.BillingStateCode + "," + orderDetail.BillingPostalCode;
                postalDetails = postalDetails.Trim(',');
                sbHTMLAddress.Append(GetFormattedText(postalDetails));
                sbHTMLAddress.Append(GetFormattedText(orderDetail.BillingCountry));
                sbHTMLAddress.Append(GetFormattedPhoneNumberText(orderDetail.BillingPhoneNumber));
            }

            return sbHTMLAddress.ToString();
        }

        public static string GetFormattedShippingAddress(ZNode.Libraries.DataAccess.Entities.Order orderDetail)
        {
            StringBuilder sbHTMLAddress = new StringBuilder();
            if (orderDetail != null)
            {
                string addressName = string.Empty;
                string postalDetails = string.Empty;

                sbHTMLAddress.Append(orderDetail.ShipFirstName);
                sbHTMLAddress.Append(" ");
                sbHTMLAddress.Append(orderDetail.ShipLastName);
                sbHTMLAddress.Append(GetFormattedText(orderDetail.ShipCompanyName));
                sbHTMLAddress.Append(GetFormattedText(orderDetail.ShipStreet));
                sbHTMLAddress.Append(GetFormattedText(orderDetail.ShipStreet1));
                postalDetails = orderDetail.ShipCity + "," + orderDetail.ShipStateCode + "," + orderDetail.ShipPostalCode;
                postalDetails = postalDetails.Trim(',');
                sbHTMLAddress.Append(GetFormattedText(postalDetails));
                sbHTMLAddress.Append(GetFormattedText(orderDetail.ShipCountry));
                sbHTMLAddress.Append(GetFormattedPhoneNumberText(orderDetail.ShipPhoneNumber));
            }

            return sbHTMLAddress.ToString();
        }


        /// <summary>
        /// Get Formated Text
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public static string GetFormattedText(string strText)
        {
            if (!string.IsNullOrEmpty(strText))
            {
                return "<br>" + strText;
            }
            return string.Empty;
        }

        /// <summary>
        /// Get Formates Phone Number
        /// </summary>
        /// <param name="strPhone"></param>
        /// <returns></returns>
        public static string GetFormattedPhoneNumberText(string strPhone)
        {
            if (!string.IsNullOrEmpty(strPhone))
            {
                return "<br>" + "Tel:" + " " + strPhone;
            }
            return string.Empty;
        }



        #endregion

        #region Order Status Update(other than "Shipped") Email Implementation

        /// <summary>
        /// Sends the email
        /// </summary>
        /// <param name="ord"></param>
        /// <param name="lineItems"></param>
        private static void SendOrderStatusMail(ZNode.Libraries.DataAccess.Entities.Order ord, string orderUpdatedState)
        {
            try
            {
                var olis = new OrderLineItemService();
                TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> ordItems = olis.GetByOrderID(ord.OrderID);
                if (ordItems != null && ordItems.Count > 0 && !string.IsNullOrWhiteSpace(orderUpdatedState))
                {
                    SendOrderStatusEmail(ord, ordItems, orderUpdatedState);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Error in Updating Order ID:" + ord.OrderID + "!!Error while genrating the email template!!Method Name!!SendOrderStatusMail!! " + ex.ToString();
                OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(ord.OrderID, 0, string.Empty, errorMessage);
                throw ex;
            }

        }


        /// <summary>
        /// Send Order Status Update  email 
        /// </summary>
        /// <param name="ordID"></param>
        /// <param name="emailID"></param>
        /// <param name="lineItems"></param>
        /// <param name="ordItems"></param>
        private static void SendOrderStatusEmail(ZNode.Libraries.DataAccess.Entities.Order ord, TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> ordItems, string orderUpdatedState)
        {
            PortalService service = new PortalService();
            ZNode.Libraries.DataAccess.Entities.Portal portal = service.GetByPortalID(ord.PortalId.GetValueOrDefault(0));
            string customerServiceEmail = portal != null ? portal.CustomerServiceEmail : string.Empty;
            string bcc = customerServiceEmail ;
            if (System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"] != null && !string.IsNullOrEmpty(System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"].ToString()))
            {
                bcc += "," + System.Web.Configuration.WebConfigurationManager.AppSettings["BCCEmailId"].ToString();
            }
            if (!string.IsNullOrWhiteSpace(orderUpdatedState))
            {
                string orderStatusEmailTemplate = GetOrderStatusUpdateEmailTemplate(ord, ordItems, portal, orderUpdatedState);
                ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
                if (!string.IsNullOrEmpty(orderStatusEmailTemplate))
                {
                    string subject = ZNodeCatalogManager.MessageConfig.GetMessage("WebAPIOrderStatusEmailSubject", ord.PortalId.GetValueOrDefault(0), ZNodeCatalogManager.CatalogConfig.LocaleID);
                    SendAPIEmail.SendEmail(ord.BillingEmailId, customerServiceEmail, bcc, string.Format(subject, orderUpdatedState), orderStatusEmailTemplate, true);
                }
            }
        }

        /// <summary>
        /// Get the email template
        /// </summary>
        /// <param name="ordID"></param>
        /// <param name="trackingNo"></param>
        /// <param name="lines"></param>
        /// <param name="ordItems"></param>
        /// <returns></returns>
        private static string GetOrderStatusUpdateEmailTemplate(ZNode.Libraries.DataAccess.Entities.Order orderDetail, TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> ordItems, ZNode.Libraries.DataAccess.Entities.Portal portal, string orderUpdatedState)
        {
            try
            {
                string emailTemplatePath = ConfigurationManager.AppSettings["OrderStatusEmailTemplatePath"] != null
                    ? ConfigurationManager.AppSettings["OrderStatusEmailTemplatePath"].ToString() : "~/Data/default/config/OrderStatusUpdateNotification.html"; ;
                string spacerImagePath = string.Empty;
                if (!string.IsNullOrEmpty(emailTemplatePath))
                {
                    string defaultTemplatePath = HttpContext.Current.Server.MapPath(emailTemplatePath);

                    // Order DataTable
                    // Create Order Table
                    DataTable OrderTable = new DataTable();

                    // Add a columns to the ordertable
                    // Additional Info
                    OrderTable.Columns.Add("SiteName");
                    OrderTable.Columns.Add("ReceiptText");

                    // Payment Info
                    OrderTable.Columns.Add("PONumber");
                    OrderTable.Columns.Add("PurchaseNumberLabel");

                    // Customer Info
                    OrderTable.Columns.Add("OrderId");
                    OrderTable.Columns.Add("OrderDate");
                    OrderTable.Columns.Add("BillingAddress");
                    OrderTable.Columns.Add("ShippingAddress");
                    OrderTable.Columns.Add("TotalCost");
                    OrderTable.Columns.Add("StyleSheetPath");
                    OrderTable.Columns.Add("FooterText");
                    OrderTable.Columns.Add("OrderState");

                    //Spacer Image
                    OrderTable.Columns.Add("SpacerImage");


                    // Create Order Amount Table
                    DataTable OrderAmountTable = new DataTable();
                    OrderAmountTable.Columns.Add("Title");
                    OrderAmountTable.Columns.Add("Amount");


                    // Create a NewRow.
                    DataRow orderDBRow = OrderTable.NewRow();


                    //Spacer Image
                    //spacerImagePath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";

                    spacerImagePath = ZNodeCatalogManager.MessageConfig.GetMessage("APIOrderReceiptSpaceImagePath", orderDetail.PortalId.GetValueOrDefault(0), ZNodeCatalogManager.CatalogConfig.LocaleID);

                    if (!string.IsNullOrEmpty(spacerImagePath))
                    {
                        orderDBRow["SpacerImage"] = spacerImagePath;
                    }
                    else
                    {
                        orderDBRow["SpacerImage"] = spacerImagePath;
                    }
                    // Additional Info
                    orderDBRow["SiteName"] = portal != null && !string.IsNullOrEmpty(portal.StoreName) ? portal.StoreName : string.Empty;
                    orderDBRow["ReceiptText"] = ZNodeCatalogManager.MessageConfig.GetMessage("ShippingEmailReceiptText", orderDetail.PortalId.GetValueOrDefault(0), ZNodeCatalogManager.CatalogConfig.LocaleID);
                    // Customer Info
                    orderDBRow["OrderId"] = orderDetail.OrderID.ToString();
                    try
                    {
                        orderDBRow["OrderDate"] = orderDetail.ShipDate != null && !string.IsNullOrWhiteSpace(orderDetail.ShipDate.ToString()) ? orderDetail.ShipDate.Value.ToShortDateString() : string.Empty;
                    }
                    catch (Exception ex)
                    {
                        ZNodeLoggingBase.LogMessage("Error in Updating Order ID:" + orderDetail.OrderID + "!!Error while genrating the email template!!Method Name!!GetOrderEmailTemplate!! in ShipDate" + ex.ToString());
                        string errorMessage = "Error in Updating Order ID:" + orderDetail.OrderID + "!!Error while genrating the email template!!Method Name!!GetOrderEmailTemplate!! in ShipDate" + ex.ToString();
                        OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(orderDetail.OrderID, 0, string.Empty, errorMessage);
                    }

                    orderDBRow["BillingAddress"] = GetFormattedBillingAddress(orderDetail);//Zeon Custom Code
                    orderDBRow["ShippingAddress"] = GetFormattedShippingAddress(orderDetail);

                    var shippingInfo = new RogansShoes.Web.API.Helper.zShippingInformation();
                    string carriername = string.Empty;
                    BuildOrderAmountInfo("SubTotal", orderDetail.SubTotal.Value, OrderAmountTable);
                    BuildOrderAmountInfo("TaxCost", orderDetail.TaxCost.Value, OrderAmountTable);
                    BuildOrderAmountInfo("SalesTax", orderDetail.SalesTax.Value, OrderAmountTable);
                    BuildOrderAmountInfo("DiscountAmount", -orderDetail.DiscountAmount.Value, OrderAmountTable);
                    BuildOrderAmountInfo("DiscountAmount", -orderDetail.DiscountAmount.Value, OrderAmountTable);
                    BuildOrderAmountInfo("ShippingCost", orderDetail.ShippingCost.Value, OrderAmountTable);

                    orderDBRow["TotalCost"] = orderDetail.Total.Value.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();

                    var msg = ZNodeCatalogManager.MessageConfig.GetMessage("OrderStatuUpdateEmailFooter", orderDetail.PortalId.GetValueOrDefault(0), ZNodeCatalogManager.CatalogConfig.LocaleID);
                    if (!string.IsNullOrEmpty(msg))
                    {
                        if (carriername.EndsWith(","))
                        {
                            char[] trimchar = { ',' };
                            carriername = carriername.TrimEnd(trimchar);
                        }
                        orderDBRow["FooterText"] = msg.Replace("#carriername#", carriername);
                    }
                    else
                    {
                        orderDBRow["FooterText"] = string.Empty;
                    }

                    orderDBRow["OrderState"] = orderUpdatedState;//Add Order Status


                    // Create OrderlineItem Table
                    DataTable OrderlineItemTable = new DataTable();


                    OrderlineItemTable.Columns.Add("Quantity");
                    OrderlineItemTable.Columns.Add("IMAGE");
                    OrderlineItemTable.Columns.Add("Description");
                    OrderlineItemTable.Columns.Add("Name");
                    OrderlineItemTable.Columns.Add("Price");
                    OrderlineItemTable.Columns.Add("ExtendedPrice");



                    BuildOrderLineItemTable(OrderlineItemTable, ordItems);

                    // Add rows to Order datatable.
                    OrderTable.Rows.Add(orderDBRow);

                    // Html Parser
                    // Parse the template
                    RogansShoes.Web.API.Helper.ZNodeHtmlTemplate template = new RogansShoes.Web.API.Helper.ZNodeHtmlTemplate();

                    // set the html template path
                    template.Path = defaultTemplatePath;

                    // Parse Order
                    template.Parse(OrderTable.CreateDataReader());

                    // Parse OrderLineItem
                    template.Parse("LineItems", OrderlineItemTable.CreateDataReader());

                    // Parse OrderLineItem
                    template.Parse("AmountLineItems", OrderAmountTable.CreateDataReader());

                    // template html output
                    return template.Output;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                ZNodeLoggingBase.LogMessage("Error in Updating Order ID:" + orderDetail.OrderID + "!!Error while genrating the email template!!Method Name!!GetOrderEmailTemplate!!" + ex.ToString());
                string errorMessage = "Error in Updating Order ID:" + orderDetail.OrderID + "!!Error while genrating the email template!!Method Name!!GetOrderEmailTemplate!!" + ex.ToString();
                OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(orderDetail.OrderID, 0, string.Empty, errorMessage);
                return string.Empty;
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Get Portal wise Addtional string in sku
        /// </summary>
        /// <returns>string</returns>
        private static string GetPortalWiseSKUAddtionalString()
        {
            string skuPrfix = string.Empty;
            if (System.Configuration.ConfigurationManager.AppSettings["CheerAndPomSKUPrefix"] != null)
            {
                skuPrfix = System.Configuration.ConfigurationManager.AppSettings["CheerAndPomSKUPrefix"] != null ? System.Configuration.ConfigurationManager.AppSettings["CheerAndPomSKUPrefix"] : string.Empty;
            }
            return skuPrfix;
        }

        /// <summary>
        /// Get Order Status and Return Order State ID
        /// </summary>
        /// <param name="orderStatus">string</param>
        /// <returns>string</returns>
        public static string GetOrderStateIDByStatus(string orderStatus)
        {
            int orderStatusID = 0;
            switch (orderStatus.ToLower())
            {
                case "cancelled":
                    orderStatusID = 40;
                    break;
                case "returned":
                    orderStatusID = 30;
                    break;
                case "shipped":
                    orderStatusID = 20;
                    break;
                case "submitted":
                    orderStatusID = 10;
                    break;
                default:
                    orderStatusID = 50;
                    break;
            }
            return orderStatusID.ToString();
        }

        #endregion
    }
}
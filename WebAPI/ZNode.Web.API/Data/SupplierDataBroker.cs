﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using Supplier = RogansShoes.Web.API.Models.Supplier;
using SupplierType = RogansShoes.Web.API.Models.SupplierType;
using ZNode.Libraries.Framework.Business;
using Utility = RogansShoes.Web.API.Support.Utility;

namespace RogansShoes.Web.API.Data
{
    public static class SupplierDataBroker
    {
        public static void DeleteSupplierById(string id, string idType = null)
        {
            var ss = new SupplierService();
            var supplierEntity = ss.GetBySupplierID(GetSupplierId(idType, id));

            if (!ss.Delete(supplierEntity))
            {
                throw new InvalidOperationException("Specified supplier does not exist.");
            }
        }

        public static List<Supplier> GetAllSuppliers(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var sh = new SupplierHelper();

            filters = new List<QueryBuilder.WhereCondition>();

            var queryBuilder = new QueryBuilder("Supplier", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = sh.GetSupplierIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetSupplierById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }


        public static Supplier GetSupplierById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var ss = new SupplierService();
            var supplierEntity = ss.GetBySupplierID(GetSupplierId(idType, id));

            if (supplierEntity == null)
            {
                throw new ResourceNotFoundException("Supplier not found.");
            }

            return GetSupplierFromEntity(supplierEntity, expands, filters);
        }

        private static Supplier GetSupplierFromEntity(ISupplier supplierEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (supplierEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var supplier = DataConverter.ToSupplierDto(supplierEntity);

            if (expands == null)
            {
                expands = new List<string>();
            }

            return supplier;
        }

        private static int GetSupplierId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var th = new SupplierHelper();
                var supplierId = th.GetSupplierIDByExternalID(id);
                return Convert.ToInt32(supplierId);
            }
            return Convert.ToInt32(id);
        }

        private static int GetSupplierId(string idType, Supplier supplier)
        {
            if (idType == IdTypes.External)
            {
                return GetSupplierId(idType, supplier.ExternalId);
            }
            return Convert.ToInt32(supplier.SupplierId);
        }

        public static Supplier GetSupplierBySupplier(Supplier supplier, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var ss = new SupplierService();
            var supplierEntity = ss.GetBySupplierID(GetSupplierId(idType, supplier));

            if (supplierEntity == null)
            {
                throw new ResourceNotFoundException("Supplier not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetSupplierFromEntity(supplierEntity, expands, filters);
        }

        public static string SaveSupplier(Supplier supplier)
        {
            if (!String.IsNullOrEmpty(supplier.SupplierId))
            {
                throw new InvalidOperationException("Posting a new Tax Class ID is not allowed.");
            }
            if (!String.IsNullOrEmpty(supplier.ExternalId))
            {
                var sh = new SupplierHelper();
                var temp = sh.GetSupplierIDByExternalID(supplier.ExternalId);
                if (temp != null)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var ss = new SupplierService();
            var supplierEntity = DataConverter.ToSupplierEntity(supplier, ss);
            var result = ss.Save(supplierEntity);

            return result.SupplierID.ToString();
        }

        public static void UpdateSupplier(Supplier supplier, string idType = null)
        {
            supplier.SupplierId = GetSupplierId(idType, supplier).ToString();
            
            var ss = new SupplierService();
            var supplierEntity = DataConverter.ToSupplierEntity(supplier, ss);

            if (!ss.Update(supplierEntity))
            {
                throw new InvalidOperationException("Specfied Supplier does not exist.");
            }
        }

        public static void DeleteSupplierTypeById(string id, string idType = null)
        {
            var sts = new SupplierTypeService();
            var supplierTypeEntity = sts.GetBySupplierTypeID(GetSupplierTypeId(idType, id));

            if (!sts.Delete(supplierTypeEntity))
            {
                throw new InvalidOperationException("Specified supplier type does not exist.");
            }
        }

        public static List<SupplierType> GetAllSupplierTypes(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var sh = new SupplierHelper();

            filters = new List<QueryBuilder.WhereCondition>();

            var queryBuilder = new QueryBuilder("SupplierType", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = sh.GetSupplierTypeIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetSupplierTypeById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }


        public static SupplierType GetSupplierTypeById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var sts = new SupplierTypeService();
            var supplierTypeEntity = sts.GetBySupplierTypeID(GetSupplierTypeId(idType, id));

            if (supplierTypeEntity == null)
            {
                throw new ResourceNotFoundException("SupplierType not found.");
            }

            return GetSupplierTypeFromEntity(supplierTypeEntity, expands, filters);
        }

        private static SupplierType GetSupplierTypeFromEntity(ISupplierType supplierTypeEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (supplierTypeEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var supplierType = DataConverter.ToSupplierTypeDto(supplierTypeEntity);

            if (expands == null)
            {
                expands = new List<string>();
            }

            return supplierType;
        }

        private static int GetSupplierTypeId(string idType, string id)
        {
            return Convert.ToInt32(id);
        }

        private static int GetSupplierTypeId(string idType, SupplierType supplierType)
        {
            return Convert.ToInt32(supplierType.SupplierTypeId);
        }

        public static SupplierType GetSupplierTypeBySupplierType(SupplierType supplierType, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var sts = new SupplierTypeService();
            var supplierTypeEntity = sts.GetBySupplierTypeID(GetSupplierTypeId(idType, supplierType));

            if (supplierTypeEntity == null)
            {
                throw new ResourceNotFoundException("SupplierType not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetSupplierTypeFromEntity(supplierTypeEntity, expands, filters);
        }

        public static string SaveSupplierType(SupplierType supplierType)
        {
            if (!String.IsNullOrEmpty(supplierType.SupplierTypeId))
            {
                throw new InvalidOperationException("Posting a new Tax Class ID is not allowed.");
            }

            var sts = new SupplierTypeService();
            var supplierTypeEntity = DataConverter.ToSupplierTypeEntity(supplierType, sts);
            var result = sts.Save(supplierTypeEntity);

            return result.SupplierTypeID.ToString();
        }

        public static void UpdateSupplierType(SupplierType supplierType, string idType = null)
        {
            supplierType.SupplierTypeId = GetSupplierTypeId(idType, supplierType).ToString();
            
            var sts = new SupplierTypeService();
            var supplierTypeEntity = DataConverter.ToSupplierTypeEntity(supplierType, sts);

            if (!sts.Update(supplierTypeEntity))
            {
                throw new InvalidOperationException("Specfied SupplierType does not exist.");
            }
        }
    }
}
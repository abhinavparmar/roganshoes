﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RogansShoes.Web.API.Data
{
    public class OrderBy
    {
        public const string ASC = "asc";
        public const string DESC = "desc";
    }
}
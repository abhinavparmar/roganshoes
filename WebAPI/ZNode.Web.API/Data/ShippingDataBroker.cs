﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using Shipping = RogansShoes.Web.API.Models.Shipping;
using ShippingRule = RogansShoes.Web.API.Models.ShippingRule;
using ShippingRuleType = RogansShoes.Web.API.Models.ShippingRuleType;
using ShippingType = RogansShoes.Web.API.Models.ShippingType;
using ZNode.Libraries.Framework.Business;
using Utility = RogansShoes.Web.API.Support.Utility;

namespace RogansShoes.Web.API.Data
{
    public static class ShippingDataBroker
    {
        public static void DeleteShippingById(string id, string idType = null)
        {
            var ss = new ShippingService();
            var shippingEntity = ss.GetByShippingID(GetShippingId(idType, id));

            if (!ss.Delete(shippingEntity))
            {
                throw new InvalidOperationException("Specified shipping does not exist.");
            }
        }

        public static List<Shipping> GetAllShipping(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var sh = new ShippingHelper();

            filters = InitializeFilters(filters);

            var queryBuilder = new QueryBuilder("Shipping", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = sh.GetShippingIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetShippingById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static Shipping GetShippingById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var ss = new ShippingService();
            var shippingEntity = ss.GetByShippingID(GetShippingId(idType, id));

            if (shippingEntity == null)
            {
                throw new ResourceNotFoundException("Shipping not found.");
            }

            return GetShippingFromEntity(shippingEntity, expands, filters);
        }

        public static Shipping GetShippingByShipping(Shipping shipping, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var ss = new ShippingService();
            var shippingEntity = ss.GetByShippingID(GetShippingId(idType, shipping));

            if (shippingEntity == null)
            {
                throw new ResourceNotFoundException("Shipping not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetShippingFromEntity(shippingEntity, expands, filters);
        }

        private static Shipping GetShippingFromEntity(IShipping shippingEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (shippingEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var shipping = DataConverter.ToShippingDto(shippingEntity);

            return shipping;
        }

        private static int GetShippingId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var sh = new ShippingHelper();
                var shippingId = sh.GetShippingIDByExternalID(id);
                return Convert.ToInt32(shippingId);
            }
            return Convert.ToInt32(id);
        }

        private static int GetShippingId(string idType, Shipping shipping)
        {
            if (idType == IdTypes.External)
            {
                return GetShippingId(idType, shipping.ExternalId);
            }
            return Convert.ToInt32(shipping.ShippingId);
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();
            return list;
        }

        public static string SaveShipping(Shipping shipping)
        {
            if (!String.IsNullOrEmpty(shipping.ShippingId))
            {
                throw new InvalidOperationException("Posting a new Shipping ID is not allowed.");
            }
            if (!String.IsNullOrEmpty(shipping.ExternalId))
            {
                var sh = new ShippingHelper();
                var temp = sh.GetShippingIDByExternalID(shipping.ExternalId);
                if (temp != null)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var ss = new ShippingService();
            var shippingEntity = DataConverter.ToShippingEntity(shipping, ss);

            shippingEntity.DisplayOrder = 1;

            var result = ss.Save(shippingEntity);
            return result.ShippingID.ToString();
        }

        public static void UpdateShipping(Shipping shipping, string idType = null)
        {
            shipping.ShippingId = GetShippingId(idType, shipping).ToString();

            var ss = new ShippingService();
            var shippingEntity = DataConverter.ToShippingEntity(shipping, ss);

            if (!ss.Update(shippingEntity))
            {
                throw new InvalidOperationException("Specfied Shipping does not exist.");
            }
        }

        public static void DeleteShippingRuleById(string id, string idType = null)
        {
            var srs = new ShippingRuleService();
            var shippingRuleEntity = srs.GetByShippingRuleID(GetShippingRuleId(idType, id));

            if (!srs.Delete(shippingRuleEntity))
            {
                throw new InvalidOperationException("Specified shipping rule does not exist.");
            }
        }

        public static List<ShippingRule> GetAllShippingRules(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var sh = new ShippingHelper();

            filters = new List<QueryBuilder.WhereCondition>();

            var queryBuilder = new QueryBuilder("ShippingRule", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = sh.GetShippingRuleIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetShippingRuleById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static ShippingRule GetShippingRuleById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var srs = new ShippingRuleService();
            var shippingRuleEntity = srs.GetByShippingRuleID(GetShippingRuleId(idType, id));

            if (shippingRuleEntity == null)
            {
                throw new ResourceNotFoundException("Shipping not found.");
            }

            return GetShippingRuleFromEntity(shippingRuleEntity, expands, filters);
        }

        public static ShippingRule GetShippingRuleByShippingRule(ShippingRule shippingRule, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var srs = new ShippingRuleService();
            var shippingRuleEntity = srs.GetByShippingRuleID(GetShippingRuleId(idType, shippingRule));

            if (shippingRuleEntity == null)
            {
                throw new ResourceNotFoundException("Shipping Rule not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetShippingRuleFromEntity(shippingRuleEntity, expands, filters);
        }

        private static ShippingRule GetShippingRuleFromEntity(IShippingRule shippingRuleEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (shippingRuleEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var shippingRule = DataConverter.ToShippingRuleDto(shippingRuleEntity);

            return shippingRule;
        }

        private static int GetShippingRuleId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var sh = new ShippingHelper();
                var shippingRuleId = sh.GetShippingRuleIDByExternalID(id);
                return Convert.ToInt32(shippingRuleId);
            }
            return Convert.ToInt32(id);
        }

        private static int GetShippingRuleId(string idType, ShippingRule shippingRule)
        {
            if (idType == IdTypes.External)
            {
                return GetShippingRuleId(idType, shippingRule.ExternalId);
            }
            return Convert.ToInt32(shippingRule.ShippingId);
        }

        public static string SaveShippingRule(ShippingRule shippingRule)
        {
            if (!String.IsNullOrEmpty(shippingRule.ShippingRuleId))
            {
                throw new InvalidOperationException("Posting a new Shipping Rule ID is not allowed.");
            }
            if (!String.IsNullOrEmpty(shippingRule.ExternalId))
            {
                var sh = new ShippingHelper();
                var temp = sh.GetShippingIDByExternalID(shippingRule.ExternalId);
                if (temp != null)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var srs = new ShippingRuleService();
            var shippingRuleEntity = DataConverter.ToShippingRuleEntity(shippingRule, srs);


            var result = srs.Save(shippingRuleEntity);
            return result.ShippingRuleID.ToString();
        }

        public static void UpdateShippingRule(ShippingRule shippingRule, string idType = null)
        {
            shippingRule.ShippingRuleId = GetShippingRuleId(idType, shippingRule).ToString();
            
            var srs = new ShippingRuleService();
            var shippingRuleEntity = DataConverter.ToShippingRuleEntity(shippingRule, srs);

            if (!srs.Update(shippingRuleEntity))
            {
                throw new InvalidOperationException("Specfied Shipping Rule does not exist.");
            }
        }

        public static void DeleteShippingRuleTypeById(string id, string idType = null)
        {
            var srts = new ShippingRuleTypeService();
            var shippingRuleTypeEntity = srts.GetByShippingRuleTypeID(Convert.ToInt32(id));

            if (!srts.Delete(shippingRuleTypeEntity))
            {
                throw new InvalidOperationException("Specified shipping rule type does not exist.");
            }
        }

        public static List<ShippingRuleType> GetAllShippingRuleTypes(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var sh = new ShippingHelper();

            filters = new List<QueryBuilder.WhereCondition>();

            var queryBuilder = new QueryBuilder("ShippingRuleType", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = sh.GetShippingRuleTypeIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetShippingRuleTypeById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static ShippingRuleType GetShippingRuleTypeById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var srts = new ShippingRuleTypeService();
            var shippingRuleTypeEntity = srts.GetByShippingRuleTypeID(Convert.ToInt32(id));

            if (shippingRuleTypeEntity == null)
            {
                throw new ResourceNotFoundException("Shipping Rule Type not found.");
            }

            return GetShippingRuleTypeFromEntity(shippingRuleTypeEntity, expands, filters);
        }

        public static ShippingRuleType GetShippingRuleTypeByShippingRuleType(ShippingRuleType shippingRuleType, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var srts = new ShippingRuleTypeService();
            var shippingRuleTypeEntity = srts.GetByShippingRuleTypeID(Convert.ToInt32(shippingRuleType.ShippingRuleTypeId));

            if (shippingRuleTypeEntity == null)
            {
                throw new ResourceNotFoundException("Shipping Rule Type not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetShippingRuleTypeFromEntity(shippingRuleTypeEntity, expands, filters);
        }

        private static ShippingRuleType GetShippingRuleTypeFromEntity(IShippingRuleType shippingRuleTypeEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (shippingRuleTypeEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var shippingRuleType = DataConverter.ToShippingRuleTypeDto(shippingRuleTypeEntity);

            return shippingRuleType;
        }

        public static string SaveShippingRuleType(ShippingRuleType shippingRuleType)
        {
            if (!String.IsNullOrEmpty(shippingRuleType.ShippingRuleTypeId))
            {
                throw new InvalidOperationException("Posting a new Shipping Rule Type ID is not allowed.");
            }

            var srts = new ShippingRuleTypeService();
            var shippingRuleTypeEntity = DataConverter.ToShippingRuleTypeEntity(shippingRuleType, srts);


            var result = srts.Save(shippingRuleTypeEntity);
            return result.ShippingRuleTypeID.ToString();
        }

        public static void UpdateShippingRuleType(ShippingRuleType shippingRuleType, string idType = null)
        {
            var srts = new ShippingRuleTypeService();
            var shippingRuleTypeEntity = DataConverter.ToShippingRuleTypeEntity(shippingRuleType, srts);

            if (!srts.Update(shippingRuleTypeEntity))
            {
                throw new InvalidOperationException("Specfied Shipping Rule Type does not exist.");
            }
        }

        public static void DeleteShippingTypeById(string id, string idType = null)
        {
            var srts = new ShippingTypeService();
            var shippingTypeEntity = srts.GetByShippingTypeID(Convert.ToInt32(id));

            if (!srts.Delete(shippingTypeEntity))
            {
                throw new InvalidOperationException("Specified shipping type does not exist.");
            }
        }

        public static List<ShippingType> GetAllShippingTypes(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var sh = new ShippingHelper();

            filters = new List<QueryBuilder.WhereCondition>();

            var queryBuilder = new QueryBuilder("ShippingType", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = sh.GetShippingTypeIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetShippingTypeById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static ShippingType GetShippingTypeById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var srts = new ShippingTypeService();
            var shippingTypeEntity = srts.GetByShippingTypeID(Convert.ToInt32(id));

            if (shippingTypeEntity == null)
            {
                throw new ResourceNotFoundException("Shipping Type not found.");
            }

            return GetShippingTypeFromEntity(shippingTypeEntity, expands, filters);
        }

        public static ShippingType GetShippingTypeByShippingType(ShippingType shippingType, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var srts = new ShippingTypeService();
            var shippingTypeEntity = srts.GetByShippingTypeID(Convert.ToInt32(shippingType.ShippingTypeId));

            if (shippingTypeEntity == null)
            {
                throw new ResourceNotFoundException("Shipping Type not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetShippingTypeFromEntity(shippingTypeEntity, expands, filters);
        }

        private static ShippingType GetShippingTypeFromEntity(IShippingType shippingTypeEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (shippingTypeEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var shippingType = DataConverter.ToShippingTypeDto(shippingTypeEntity);

            return shippingType;
        }

        public static string SaveShippingType(ShippingType shippingType)
        {
            if (!String.IsNullOrEmpty(shippingType.ShippingTypeId))
            {
                throw new InvalidOperationException("Posting a new Shipping Type ID is not allowed.");
            }

            var srts = new ShippingTypeService();
            var shippingTypeEntity = DataConverter.ToShippingTypeEntity(shippingType, srts);


            var result = srts.Save(shippingTypeEntity);
            return result.ShippingTypeID.ToString();
        }

        public static void UpdateShippingType(ShippingType shippingType, string idType = null)
        {
            var srts = new ShippingTypeService();
            var shippingTypeEntity = DataConverter.ToShippingTypeEntity(shippingType, srts);

            if (!srts.Update(shippingTypeEntity))
            {
                throw new InvalidOperationException("Specfied Shipping Type does not exist.");
            }
        }
    }
}
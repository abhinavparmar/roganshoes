﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using Product = RogansShoes.Web.API.Models.Product;
using SavedCart = RogansShoes.Web.API.Models.SavedCart;
using SavedCartLineItem = RogansShoes.Web.API.Models.SavedCartLineItem;
using Utility = RogansShoes.Web.API.Support.Utility;

namespace RogansShoes.Web.API.Data
{
    public static class SavedCartDataBroker
    {
        private static SavedCart GetSavedCartFromEntity(ISavedCart savedCartEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (savedCartEntity == null)
            {
                return null;
            }

            var savedCart = DataConverter.ToSavedCartDto(savedCartEntity);

            var sclis = new SavedCartLineItemService();

            savedCart.SavedCartLineItems = new Collection<SavedCartLineItem>(sclis.GetBySavedCartID(savedCartEntity.SavedCartID)
                .Select(DataConverter.ToSavedCartLineItemDto).ToList());

            return savedCart;
        }

        private static int GetSavedCartId(string idType, string id)
        {
            if (idType == IdTypes.CookieMapping)
            {
                var scs = new SavedCartService();
                var savedCart = scs.GetByCookieMappingID(int.Parse(id)).Single();
                return savedCart.SavedCartID;
            }
            return int.Parse(id);
        }

        private static int GetSavedCartId(string idType, SavedCart savedCart)
        {
            if (idType == IdTypes.CookieMapping)
            {
                return GetSavedCartId(idType, savedCart.CookieMappingId);
            }
            return int.Parse(savedCart.SavedCartId);
        }

        public static SavedCart GetSavedCartById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var scs = new SavedCartService();
            var savedCartEntity = scs.GetBySavedCartID(GetSavedCartId(idType, id));

            if (savedCartEntity == null)
            {
                throw new ResourceNotFoundException("Saved cart not found.");
            }

            var filters = conditions == null
                              ? null
                              : conditions.Where(
                                  x =>
                                  !x.FieldName.Equals(Filters.LastUpdatedDate,
                                                      StringComparison.InvariantCultureIgnoreCase))
                                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetSavedCartFromEntity(savedCartEntity, expands, filters);
        }

        public static SavedCart GetSavedCartBySavedCart(SavedCart savedCart, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var scs = new SavedCartService();
            var savedCartEntity = scs.GetBySavedCartID(GetSavedCartId(idType, savedCart));

            if (savedCartEntity == null)
            {
                throw new ResourceNotFoundException("Saved cart not found.");
            }
            var filters =
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.LastUpdatedDate, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetSavedCartFromEntity(savedCartEntity, expands, filters);
        }

        public static List<SavedCart> GetAllSavedCarts(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, List<string> ordering)
        {
            var oh = new OrderHelper();
            conditions = InitializeFilters(conditions);
            foreach (var condition in conditions)
            {
                if (condition.FieldName.Equals(Filters.UpdateDate, StringComparison.InvariantCultureIgnoreCase))
                {
                    condition.FieldName = Filters.LastUpdatedDate;
                }
                if (condition.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
                {
                    condition.FieldName = Filters.CreatedDate;
                }
            }

            var queryBuilder = new QueryBuilder("SavedCart", conditions);
            var query = queryBuilder.GetQuery();
            var arrayFilters = new string[conditions.Count, 2];

            #region Populate array
            for (var i = 0; i < conditions.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (conditions[i].FieldName.Equals(Filters.LastUpdatedDate) ||
                            conditions[i].FieldName.Equals(Filters.CreatedDate))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + conditions[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = conditions[i].MatchValue;
                    }
                }
            }
            #endregion

            var ids = oh.GetSavedCartIDsByFilters(query, arrayFilters);

            count = ids.Count;

            var tempList = ids.Select(id => GetSavedCartById(id, expands, conditions)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();

            if (!fieldNames.Contains(Filters.PortalId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.PortalId });
            }
            if (!fieldNames.Contains(Filters.StoreName))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.StoreName });
            }

            return list;
        }

        public static string SaveProductSavedCartLineItem(Product product,int quantity, string savedCartId, string idType = null)
        {
            ProductService productService = new ProductService();
            ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
            item.Product = DataConverter.ToZnodeProduct(product);
            item.Quantity = quantity;

            // Add product to cart
            ZNodeShoppingCart shoppingCart = shoppingCart = new ZNodeShoppingCart();
                        
            // Add item to cart
            if (shoppingCart.AddToCart(item))
            {
                savedCartId = ZNodeSavedCart.AddToWebAPISavedCart(item, Convert.ToInt32(savedCartId));
            }
            else
            {
                //Better error details will be added in a future release
                throw new InvalidOperationException("Unable to add product to shopping cart.");
            }

            return savedCartId;
        }

        public static string SaveSavedCart(SavedCart savedCart)
        {
            if (!String.IsNullOrEmpty(savedCart.SavedCartId))
            {
                throw new InvalidOperationException("Posting a new saved cart ID is not allowed.");
            }

            if (string.IsNullOrEmpty(savedCart.CookieMappingId))
            {
                var cms = new CookieMappingService();
                var cookieMappingEntity = new CookieMapping
                {
                    PortalID = ZNode.Libraries.Framework.Business.ZNodeConfigManager.DomainConfig.PortalID,
                    CreatedDate = DateTime.Now,
                    LastUpdatedDate = DateTime.Now
                };

                cookieMappingEntity = cms.Save(cookieMappingEntity);
                savedCart.CookieMappingId = cookieMappingEntity.CookieMappingID.ToString();
            }

            var scs = new SavedCartService();
            var savedCartEntity = DataConverter.ToSavedCartEntity(savedCart, scs);

            savedCartEntity.CreatedDate = DateTime.Now;
            savedCartEntity.LastUpdatedDate = DateTime.Now;

            var result = scs.Save(savedCartEntity);
            return result.SavedCartID.ToString();
        }

        public static void UpdateSavedCart(SavedCart savedCart, string idType = null)
        {
            savedCart.SavedCartId = GetSavedCartId(idType, savedCart).ToString();

            var scs = new SavedCartService();
            var savedCartEntity = DataConverter.ToSavedCartEntity(savedCart, scs);

            savedCartEntity.LastUpdatedDate = DateTime.Now;

            if (!scs.Update(savedCartEntity))
            {
                throw new InvalidOperationException("Specified saved cart does not exist.");
            }
        }

        public static void DeleteSavedCartById(string id, string idType = null)
        {
            var scs = new SavedCartService();
            var savedCartEntity = scs.GetBySavedCartID(GetSavedCartId(idType, id));

            if (!scs.Delete(savedCartEntity))
            {
                throw new InvalidOperationException("Specified saved cart does not exist.");
            }
        }

        public static string SaveSavedCartLineItem(SavedCartLineItem savedCartLineItem, string savedCartId, string savedCartIdType = null)
        {
            if (!string.IsNullOrEmpty(savedCartLineItem.SavedCartLineItemId))
            {
                throw new InvalidOperationException("Posting a new saved cart line item ID is not allowed.");
            }

            savedCartLineItem.SavedCartId = GetSavedCartId(savedCartIdType, savedCartId).ToString();

            if (!CheckInventory(savedCartLineItem))
            {
                throw new InvalidOperationException("Not Enough Inventory");
            }
            var sclis = new SavedCartLineItemService();
            var savedCartLineItemEntity = DataConverter.ToSavedCartLineItemEntity(savedCartLineItem, sclis);

            sclis.Save(savedCartLineItemEntity);

            var scs = new SavedCartService();
            var savedCartEntity = scs.GetBySavedCartID(savedCartLineItemEntity.SavedCartID);
            savedCartEntity.LastUpdatedDate = DateTime.Now;
            scs.Update(savedCartEntity);

            return savedCartLineItemEntity.SavedCartLineItemID.ToString();
        }

        public static void UpdateSavedCartLineItem(SavedCartLineItem savedCartLineItem, string savedCartId, string savedCartIdType = null)
        {
            savedCartLineItem.SavedCartId = GetSavedCartId(savedCartIdType, savedCartId).ToString();

            var sclis = new SavedCartLineItemService();
            var lineItem = sclis.GetBySavedCartLineItemID(Convert.ToInt32(savedCartLineItem.SavedCartLineItemId));
            if (lineItem == null)
            {
                throw new InvalidOperationException("Line item does not exist");
            }
            var savedCartLineItemEntity = DataConverter.ToSavedCartLineItemEntity(savedCartLineItem, sclis);

            if (!CheckInventory(savedCartLineItem))
            {
                throw new InvalidOperationException("Not Enough Inventory");
            }

            if (!sclis.Update(savedCartLineItemEntity))
            {
                throw new InvalidOperationException("Specfied saved cart line item does not exist.");
            }

            var scs = new SavedCartService();
            var savedCartEntity = scs.GetBySavedCartID(savedCartLineItemEntity.SavedCartID);
            savedCartEntity.LastUpdatedDate = DateTime.Now;
            scs.Update(savedCartEntity);
        }

        public static void DeleteSavedCartLineItemById(string id)
        {
            var scl = new SavedCartLineItemService();
            var lineItem = scl.GetBySavedCartLineItemID(Convert.ToInt32(id));
            if (lineItem == null)
            {
                throw new InvalidOperationException("Specfied saved cart line item does not exist.");
            }

            var cartLineItems = scl.GetBySavedCartID(lineItem.SavedCartID);
            var lineItemsToDelete =
                cartLineItems.Where(
                    savedCartLineItem => savedCartLineItem.ParentSavedCartLineItemID == lineItem.SavedCartLineItemID)
                                .ToList();
            if (!lineItemsToDelete.Any())
            {
                scl.Delete(lineItem);
            }
            else
            {
                // if child is also a parent delete children first
                foreach (var savedCartLineItem in lineItemsToDelete)
                {
                    var localSavedCartLineItem = savedCartLineItem;
                    var childrenOfChildern =
                        cartLineItems.Where(
                            x => x.ParentSavedCartLineItemID == localSavedCartLineItem.SavedCartLineItemID);
                    foreach (var cartLineItem in childrenOfChildern)
                    {
                        scl.Delete(cartLineItem);
                    }
                    scl.Delete(savedCartLineItem);
                }
                scl.Delete(lineItem);
            }
            var cart = new SavedCartService().GetBySavedCartID(lineItem.SavedCartID);
            cart.LastUpdatedDate = DateTime.Now;
            new SavedCartService().Update(cart);
        }

        private static bool CheckInventory(SavedCartLineItem lineItem)
        {
            var sku = new SKUService().GetBySKUID(Convert.ToInt32(lineItem.SkuId));
            var skuInvenSvc = new SKUInventoryService();
            var skuInven = skuInvenSvc.GetBySKU(sku.SKU);

            return !(skuInven.QuantityOnHand < lineItem.Quantity);
        }
    }
}
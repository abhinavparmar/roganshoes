﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using Category = RogansShoes.Web.API.Models.Category;
using CategoryNode = RogansShoes.Web.API.Models.CategoryNode;

namespace RogansShoes.Web.API.Data
{
    public static class CategoryDataBroker
    {
        private static Category GetCategoryFromEntity(ICategory categoryEntity, IList<string> expands, IDictionary<string, string> filters,
            IDictionary<string, Category> archive = null, IList<string> seen = null)
        {   
            if (categoryEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);
            
            var category = DataConverter.ToCategoryDto(categoryEntity);

            if ((archive = archive ?? new Dictionary<string, Category>()).ContainsKey(category.CategoryId))
            {
                return archive[category.CategoryId];
            }

            if ((seen = seen ?? new List<string>()).Contains(category.CategoryId))
            {
                throw new CircularReferenceException();
            }
            seen.Add(category.CategoryId);

            if (expands == null)
            {
                expands = new List<string>();
            }

            if (!expands.Contains(Expands.Image))
            {
                category.Image = null;
            }
            if (!expands.Contains(Expands.Seo))
            {
                category.Seo = null;
            }

            if (expands.Contains(Expands.CategoryNodes))
            {
                var cns = new CategoryNodeService();
                
                category.CategoryNodes = new Collection<CategoryNode>(cns.GetByCategoryID(categoryEntity.CategoryID)
                    .Where(cn => FilterCategoryNodes(cn, filters)).Select(DataConverter.ToCategoryNodeDto).ToList());
            }

            if (expands.Contains(Expands.Subcategories))
            {
                var cs = new CategoryService();
                var ch = new CategoryHelper();

                var ids = ch.GetCategoryIDsByFilters(QueryBuilder.GetDefaultQuery("Category"),CreateArrayFromDictionary(filters),category.CategoryId);
                
                category.Subcategories = new Collection<Category>(ids.Select(id => GetCategoryFromEntity(cs.GetByCategoryID(int.Parse(id)),
                    expands, filters, archive, new List<string>(seen))).ToList());
            }

            archive.Add(category.CategoryId, category);

            return category;
        }

        private static bool FilterCategoryNodes(ICategoryNode categoryNodeEntity, IDictionary<string, string> filters)
        {
            if (filters == null)
            {
                return true;
            }
            if (filters.ContainsKey(Filters.CatalogId) && categoryNodeEntity.CatalogID != int.Parse(filters[Filters.CatalogId]))
            {
                return false;
            }
            if (!filters.ContainsKey(Filters.PortalId) && !filters.ContainsKey(Filters.LocaleId) && !filters.ContainsKey(Filters.StoreName))
            {
                return true;
            }
            if (!categoryNodeEntity.CatalogID.HasValue)
            {
                return false;
            }
            var pcs = new PortalCatalogService();
            var portalIds = pcs.GetByCatalogID(categoryNodeEntity.CatalogID.Value).ToList();
            if (filters.ContainsKey(Filters.PortalId) && portalIds.All(c => c.PortalID != int.Parse(filters[Filters.PortalId])))
            {
                return false;
            }
            if (!filters.ContainsKey(Filters.LocaleId) && !filters.ContainsKey(Filters.StoreName))
            {
                return true;
            }
            var ps = new PortalService();
            var portals = portalIds.Select(c => ps.GetByPortalID(c.PortalID)).ToList();
            if (filters.ContainsKey(Filters.LocaleId) && portals.All(p => p.LocaleID != int.Parse(filters[Filters.PortalId])))
            {
                return false;
            }
            if (filters.ContainsKey(Filters.StoreName) && portals.All(p => p.StoreName != filters[Filters.StoreName]))
            {
                return false;
            }
            return true;
        }

        private static int GetCategoryId(string idType, string id)
        {
            return int.Parse(id);
        }

        private static int GetCategoryId(string idType, Category category)
        {
            return int.Parse(category.CategoryId);
        }

        public static Category GetCategoryById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null:
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDate, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var cs = new CategoryService();
            var categoryEntity = cs.GetByCategoryID(GetCategoryId(idType, id));

            if (categoryEntity == null)
            {
                throw new ResourceNotFoundException("Category not found.");
            }

            return GetCategoryFromEntity(categoryEntity, expands, filters);
        }

        public static Category GetCategoryByCategory(Category category, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var cs = new CategoryService();
            var categoryEntity = cs.GetByCategoryID(GetCategoryId(idType, category));

            if (categoryEntity == null)
            {
                throw new ResourceNotFoundException("Category not found.");
            }
            var filters = conditions == null?null:
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDate, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            return GetCategoryFromEntity(categoryEntity, expands, filters);
        }

        public static List<Category> GetAllCategories(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters,List<string> ordering )
        {
            var ch = new CategoryHelper();

            var intFilters = InitializeFilters(filters);

            foreach (var condition in intFilters.Where(condition => condition.FieldName.Equals(Filters.ModifyDate, StringComparison.InvariantCultureIgnoreCase)))
            {
                condition.FieldName = Filters.UpdateDate;
            }

            var queryBuilder = new QueryBuilder("Category", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array
            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDate) ||
                            filters[i].FieldName.Equals(Filters.UpdateDate))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }
            #endregion

            var ids = ch.GetCategoryIDsByFilters(query,arrayFilters);

            count = ids.Count;

            var tempList = ids.Select(id => GetCategoryById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();

            if (!fieldNames.Contains(Filters.CatalogId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.CatalogId });
            }
            if (!fieldNames.Contains(Filters.PortalId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.PortalId });
            }
            if (!fieldNames.Contains(Filters.LocaleId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.LocaleId });
            }
            if (!fieldNames.Contains(Filters.StoreName))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.StoreName });
            }

            return list;
        }

        public static string SaveCategory(Category category)
        {
            if (!String.IsNullOrEmpty(category.CategoryId))
            {
                throw new InvalidOperationException("Posting a new category ID is not allowed.");
            }
            if (String.IsNullOrEmpty(category.Name))
            {
                throw new InvalidOperationException("Must post a cateogry name.");
            }
            
            var cs = new CategoryService();
            var categoryEntity = DataConverter.ToCategoryEntity(category, cs);

            categoryEntity.VisibleInd = true;
            categoryEntity.SubCategoryGridVisibleInd = false;
            categoryEntity.CreateDate = DateTime.Now;
            categoryEntity.UpdateDate = DateTime.Now;

            var result = cs.Save(categoryEntity);
            return result.CategoryID.ToString();
        }

        public static void UpdateCategory(Category category, string idType = null)
        {
            category.CategoryId = GetCategoryId(idType, category).ToString();
            
            var cs = new CategoryService();
            var categoryEntity = DataConverter.ToCategoryEntity(category, cs);

            categoryEntity.UpdateDate = DateTime.Now;

            if (!cs.Update(categoryEntity))
            {
                throw new InvalidOperationException("Specfied category does not exist.");
            }
        }

        public static void SaveCategoryNode(CategoryNode categoryNode, string categoryId, string categoryIdType = null)
        {
            if (!string.IsNullOrEmpty(categoryNode.CategoryNodeId))
            {
                throw new InvalidOperationException("Posting a new category node ID is not allowed.");
            }

            categoryNode.CategoryId = GetCategoryId(categoryIdType, categoryId).ToString();

            var cns = new CategoryNodeService();
            var categoryNodeEntity = DataConverter.ToCategoryNodeEntity(categoryNode, cns);

            cns.Save(categoryNodeEntity);

            var cs = new CategoryService();
            var categoryEntity = cs.GetByCategoryID(categoryNodeEntity.CategoryID);
            categoryEntity.UpdateDate = DateTime.Now;
            cs.Update(categoryEntity);
        }

        public static void UpdateCategoryNode(CategoryNode categoryNode, string categoryId, string categoryIdType = null)
        {
            categoryNode.CategoryId = GetCategoryId(categoryIdType, categoryId).ToString();

            var cns = new CategoryNodeService();
            var categoryNodeEntity = DataConverter.ToCategoryNodeEntity(categoryNode, cns);

            if (!cns.Update(categoryNodeEntity))
            {
                throw new InvalidOperationException("Specfied category node does not exist.");
            }

            var cs = new CategoryService();
            var categoryEntity = cs.GetByCategoryID(categoryNodeEntity.CategoryID);
            categoryEntity.UpdateDate = DateTime.Now;
            cs.Update(categoryEntity);
        }

        /// <summary>
        /// create two dimentionary array from a dictionary
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        private static string[,] CreateArrayFromDictionary(IDictionary<string, string> filters)
        {
            var array = new string[filters.Keys.Count, 2];

            var keys = filters.Keys.ToArray();
            var values = filters.Values.ToArray();

            for (var i = 0; i < filters.Keys.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        array[i, j] = keys[i];
                    }
                    if (j == 1)
                    {
                        array[i, j] = values[i];
                    }
                }
            }

            return array;
        }
    }
}

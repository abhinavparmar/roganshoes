﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using Portal = RogansShoes.Web.API.Models.Portal;
using PortalCatalog = RogansShoes.Web.API.Models.PortalCatalog;
using ZNode.Libraries.Framework.Business;
using Utility = RogansShoes.Web.API.Support.Utility;

namespace RogansShoes.Web.API.Data
{
    public static class PortalDataBroker
    {
        public static void DeletePortalById(string id, string idType = null)
        {
            var ps = new PortalService();
            var portalEntity = ps.GetByPortalID(GetPortalId(idType, id));

            if (!ps.Delete(portalEntity))
            {
                throw new InvalidOperationException("Specified Portal does not exist.");
            }
        }

        public static List<Portal> GetAllPortals(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var ph = new PortalHelper();

            filters = InitializeFilters(filters);

            var queryBuilder = new QueryBuilder("Portal", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = ph.GetPortalIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetPortalById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static Portal GetPortalById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var pcs = new PortalService();
            var portalEntity = pcs.GetByPortalID(GetPortalId(idType, id));

            if (portalEntity == null)
            {
                throw new ResourceNotFoundException("Portal not found.");
            }

            return GetPortalFromEntity(portalEntity, expands, filters);
        }

        public static Portal GetPortalByPortal(Portal portal, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var ps = new PortalService();
            var portalEntity = ps.GetByPortalID(GetPortalId(idType, portal));

            if (portalEntity == null)
            {
                throw new ResourceNotFoundException("Portal not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetPortalFromEntity(portalEntity, expands, filters);
        }

        private static Portal GetPortalFromEntity(IPortal portalEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (portalEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var portal = DataConverter.ToPortalDto(portalEntity);
            portal.PortalCatalogs = new Collection<PortalCatalog>();

            PortalCatalogService pcs = new PortalCatalogService();

            portalEntity.PortalCatalogCollection = pcs.GetByPortalID(portalEntity.PortalID);

            foreach (var portalCatalog in portalEntity.PortalCatalogCollection)
            {
                portal.PortalCatalogs.Add(DataConverter.ToPortalCatalogDto(portalCatalog));
            }

            if (expands == null)
            {
                expands = new List<string>();
            }

            return portal;
        }

        private static int GetPortalId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var ph = new PortalHelper();
                var portalId = ph.GetPortalIDByExternalID(id);
                return int.Parse(portalId);
            }
            return int.Parse(id);
        }

        private static int GetPortalId(string idType, Portal portal)
        {
            if (idType == IdTypes.External)
            {
                return GetPortalId(idType, portal.ExternalId);
            }
            return int.Parse(portal.PortalId);
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();
            return list;
        }

        public static string SavePortal(Portal portal)
        {
            if (!String.IsNullOrEmpty(portal.PortalId))
            {
                throw new InvalidOperationException("Posting a new portal ID is not allowed.");
            }
            if (!String.IsNullOrEmpty(portal.ExternalId))
            {
                var ph = new PortalHelper();
                var temp = ph.GetPortalIDByExternalID(portal.ExternalId);
                if (temp != null)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var ps = new PortalService();

            return PortalSaveWorkflow(portal);
        }

        public static void UpdatePortal(Portal portal, string idType = null)
        {
            portal.PortalId = GetPortalId(idType, portal).ToString();

            if (!String.IsNullOrEmpty(portal.ExternalId))
            {
                var ph = new PortalHelper();
                var temp = ph.GetPortalIDByExternalID(portal.ExternalId);
                if (temp != null && temp != portal.PortalId)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var ps = new PortalService();
            var portalEntity = DataConverter.ToPortalEntity(portal, ps);

            if (!ps.Update(portalEntity))
            {
                throw new InvalidOperationException("Specfied portal does not exist.");
            }
            var pcs = new PortalCatalogService();

            if (portal.PortalCatalogs != null)
            {
                foreach (var portalCatalog in portal.PortalCatalogs)
                {
                    var portalCatalogEntity = DataConverter.ToPortalCatalogEntity(portalCatalog, pcs);

                    if (!pcs.Update(portalCatalogEntity))
                    {
                        throw new InvalidOperationException("Specfied portal catalog does not exist.");
                    }
                }
            }
        }

        private static string PortalSaveWorkflow(Portal portal)
        {

            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            ZNode.Libraries.DataAccess.Entities.Portal portalEntity = new ZNode.Libraries.DataAccess.Entities.Portal();

            // Get Exisitng Portal Settings By portalID
            ZNode.Libraries.DataAccess.Entities.Portal existingPortal = storeAdmin.GetByPortalId(ZNodeConfigManager.SiteConfig.PortalID);

            // Get Fedex Key and Password from Existing Portal
            StoreSettingsHelper storeHelper = new StoreSettingsHelper();
            DataSet ds = storeHelper.GetFedExKey();

            if (ds.Tables[0].Rows.Count > 0)
            {
                portalEntity.FedExCSPKey = ds.Tables[0].Rows[0]["FedExCSPKey"].ToString();
                portalEntity.FedExCSPPassword = ds.Tables[0].Rows[0]["FedExCSPPassword"].ToString();
                portalEntity.FedExClientProductVersion = ds.Tables[0].Rows[0]["FedExClientProductVersion"].ToString();
                portalEntity.FedExClientProductId = ds.Tables[0].Rows[0]["FedExClientProductId"].ToString();
            }

            portalEntity.PortalID = 0;
            portalEntity.AdminEmail = portal.AdminEmail;
            portalEntity.CompanyName = portal.CompanyName;
            portalEntity.CustomerServiceEmail = portal.CustomerServiceEmail;
            portalEntity.CustomerServicePhoneNumber = portal.CustomerServiceEmail;
            portalEntity.InclusiveTax = portal.InclusiveTax;
            portalEntity.ActiveInd = true;
            portalEntity.SalesEmail = portal.SalesEmail;
            portalEntity.SalesPhoneNumber = portal.SalesEmail;
            portalEntity.StoreName = portal.StoreName;
            portalEntity.UseSSL = portal.UseSSL;
            portalEntity.DefaultReviewStatus = portal.DefaultReviewStatus;

            //In 7.0 locale functionality has been removed so this will be defaulted until future release where we add globalization
            const int localeId = 43; //English default

            portalEntity.LocaleID = localeId;
            portalEntity.PersistentCartEnabled = portal.PersistentCartEnabled;

            portalEntity.FedExAccountNumber = string.Empty;
            portalEntity.FedExAddInsurance = false;
            portalEntity.FedExProductionKey = string.Empty;
            portalEntity.FedExSecurityCode = string.Empty;
            portalEntity.InclusiveTax = false;
            portalEntity.SMTPPassword = string.Empty;
            portalEntity.SMTPServer = string.Empty;
            portalEntity.SMTPUserName = string.Empty;

            portalEntity.SiteWideAnalyticsJavascript = string.Empty;
            portalEntity.SiteWideBottomJavascript = string.Empty;
            portalEntity.SiteWideTopJavascript = string.Empty;
            portalEntity.OrderReceiptAffiliateJavascript = string.Empty;
            portalEntity.GoogleAnalyticsCode = string.Empty;
            portalEntity.UPSKey = string.Empty;
            portalEntity.UPSPassword = string.Empty;
            portalEntity.UPSUserName = string.Empty;

            // Default values from existing portal
            portalEntity.SMTPPort = existingPortal.SMTPPort;
            portalEntity.WeightUnit = existingPortal.WeightUnit;
            portalEntity.DimensionUnit = existingPortal.DimensionUnit;
            portalEntity.CurrencyTypeID = existingPortal.CurrencyTypeID;
            portalEntity.DefaultOrderStateID = Convert.ToInt32(portal.DefaultOrderStateId);

            portalEntity.MaxCatalogCategoryDisplayThumbnails = existingPortal.MaxCatalogCategoryDisplayThumbnails;
            portalEntity.MaxCatalogDisplayColumns = existingPortal.MaxCatalogDisplayColumns;
            portalEntity.MaxCatalogDisplayItems = existingPortal.MaxCatalogDisplayItems;
            portalEntity.MaxCatalogItemCrossSellWidth = existingPortal.MaxCatalogItemCrossSellWidth;
            portalEntity.MaxCatalogItemLargeWidth = existingPortal.MaxCatalogItemLargeWidth;
            portalEntity.MaxCatalogItemMediumWidth = existingPortal.MaxCatalogItemMediumWidth;
            portalEntity.MaxCatalogItemSmallThumbnailWidth = existingPortal.MaxCatalogItemSmallThumbnailWidth;
            portalEntity.MaxCatalogItemSmallWidth = existingPortal.MaxCatalogItemSmallWidth;
            portalEntity.MaxCatalogItemThumbnailWidth = existingPortal.MaxCatalogItemThumbnailWidth;
            portalEntity.ImageNotAvailablePath = existingPortal.ImageNotAvailablePath;

            portalEntity.ShopByPriceMax = existingPortal.ShopByPriceMax;
            portalEntity.ShopByPriceMin = existingPortal.ShopByPriceMin;
            portalEntity.ShopByPriceIncrement = existingPortal.ShopByPriceIncrement;

            portalEntity.EnableAddressValidation = portal.EnableAddressValidation;
            portalEntity.RequireValidatedAddress = portal.RequireValidatedAddress;
            portalEntity.EnablePIMS = portal.EnablePIMS;
            portalEntity.DefaultProductReviewStateID = Convert.ToInt32(portal.DefaultProductReviewStateId);
            portalEntity.MobileTheme = "Mobile";
            // Set logo path
            string fileName = string.Empty;

            portalEntity.LogoPath = string.Empty;

            bool check = false;
            check = storeAdmin.InsertStore(portalEntity);

            if (check)
            {
                // Portal Country Service
                PortalCountryService portalCountryService = new PortalCountryService();
                PortalCountry portalCountry = new PortalCountry();

                // Add PortalCountry List
                portalCountry.PortalID = portalEntity.PortalID;
                portalCountry.BillingActive = true;
                portalCountry.ShippingActive = true;
                portalCountry.CountryCode = "US";
                check = portalCountryService.Insert(portalCountry);

                CatalogAdmin catalogAdmin = new CatalogAdmin();

                // Delete the portalCatalog
                catalogAdmin.DeleteportalCatalog(portalEntity.PortalID);

                ZNode.Libraries.DataAccess.Entities.PortalCatalog portalCatalogEntity = new ZNode.Libraries.DataAccess.Entities.PortalCatalog();

                foreach (var portalCatalog in portal.PortalCatalogs)
                {
                    // Add the new Selection
                    portalCatalogEntity.PortalID = portalEntity.PortalID;
                    portalCatalogEntity.CatalogID = Convert.ToInt32(portalCatalog.CatalogId);
                    if (!string.IsNullOrEmpty(portalCatalog.Theme))
                    {
                        portalCatalogEntity.Theme = portalCatalog.Theme;
                    }

                    if (!string.IsNullOrEmpty(portalCatalog.CSS))
                    {
                        portalCatalogEntity.CSS = portalCatalog.CSS;
                    }
                    portalCatalogEntity.LocaleID = localeId;

                    catalogAdmin.AddPortalCatalog(portalCatalogEntity);

                    // Add Default Content Pages
                    ContentPageService contentPageService = new ContentPageService();
                    ContentPage contentPage = new ContentPage();
                    ContentPageAdmin contentPageAdmin = new ContentPageAdmin();
                    ContentPageQuery filter = new ContentPageQuery();

                    // Get ContentPages list using existing portalID and LocaleId
                    filter.Append(ContentPageColumn.LocaleId, existingPortal.LocaleID.ToString());
                    filter.Append(ContentPageColumn.PortalID, existingPortal.PortalID.ToString());
                    TList<ContentPage> contentPageList = contentPageService.Find(filter.GetParameters());

                    if (contentPageList.Count > 0)
                    {
                        foreach (ContentPage page in contentPageList)
                        {
                            contentPage = page.Clone() as ContentPage;
                            contentPage.ContentPageID = -1;
                            contentPage.PortalID = portalEntity.PortalID;
                            contentPage.LocaleId = portalCatalogEntity.LocaleID;
                            contentPage.SEOURL = null;
                            contentPage.Theme = portalCatalogEntity.Theme;
                            contentPageAdmin.AddPage(contentPage, string.Empty, portalEntity.PortalID,
                                                     portalCatalogEntity.LocaleID.ToString(),
                                                     HttpContext.Current.User.Identity.Name, null, false);
                        }
                    }
                }

                // Create Message Config
                storeAdmin.CreateMessage(portalEntity.PortalID.ToString(), localeId.ToString());

                return portalEntity.PortalID.ToString();
            }
            else
            {
                throw new InvalidOperationException("Portal could not be added.");
            }
        }

    }
}
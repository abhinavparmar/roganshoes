﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using Order = RogansShoes.Web.API.Models.Order;
using OrderLineItem = RogansShoes.Web.API.Models.OrderLineItem;
using OrderState = RogansShoes.Web.API.Models.OrderState;

namespace RogansShoes.Web.API.Data
{
    public static partial class OrderDataBroker
    {
        private static Order GetOrderFromEntity(IOrder orderEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (orderEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var order = DataConverter.ToOrderDto(orderEntity);

            if (!order.UpdateDate.HasValue)
            {
                order.UpdateDate = DateTime.Now;
            }

            var olis = new OrderLineItemService();
            var olirts = new OrderLineItemRelationshipTypeService();
            var sh = new SKUHelper();
            
            order.OrderLineItems = new Collection<OrderLineItem>(olis.GetByOrderID(orderEntity.OrderID).Where(oli => !oli.OrderLineItemRelationshipTypeID.HasValue
                || !olirts.GetByOrderLineItemRelationshipTypeID(oli.OrderLineItemRelationshipTypeID.Value).Name.Equals("Add-Ons",StringComparison.InvariantCultureIgnoreCase)).Select(oli =>
            {
                var orderLineItem = DataConverter.ToOrderLineItemDto(oli);

                if (!orderLineItem.UpdateDate.HasValue)
                {
                    orderLineItem.UpdateDate = DateTime.Now;
                }
                if (!orderLineItem.ShipDate.HasValue)
                {
                    orderLineItem.ShipDate = DateTime.Now;
                }

                orderLineItem.SkuId = sh.GetSkuIdBySkuProductNum(oli.SKU, oli.ProductNum);
                if (orderLineItem.SkuId == null)
                {
                    throw new BadDataException("Invalid SKU or ProductNum on order line item with ID " + oli.OrderLineItemID);
                }

                //Zeon Custom Code : Start
                orderLineItem.PortalID = order.PortalID;
                orderLineItem.AccountID = order.AccountId;
                //Zeon Custom Code : End

                return orderLineItem;
            }).ToList());            

            //Zeon Custom Code : Start
            if (!string.IsNullOrEmpty(order.PaymentType))
            {
                var paymentService = new PaymentTypeService();
                string paymentName = string.Empty;
                ZNode.Libraries.DataAccess.Entities.PaymentType payTyp = paymentService.GetByPaymentTypeID(int.Parse(order.PaymentType));
                if(payTyp != null)
                {
                    switch (payTyp.Name.ToLower())
                    {
                        case "credit card":
                            paymentName = "C";
                            break;
                        case "purchase order":
                            paymentName = "P";
                            break;
                        case "paypal":
                            paymentName = "L";
                            break;
                        case "money order":
                            paymentName = "M";
                            break;
                        default:
                            paymentName = "NA";
                            break;
                    }
                }

                order.PaymentType = paymentName;
            }

            if (!string.IsNullOrEmpty(order.ShippingMethod))
            {
                var shippingService = new ShippingService();
                var shippingTypeService = new ShippingTypeService();
                ZNode.Libraries.DataAccess.Entities.Shipping ship = shippingService.GetByShippingID(int.Parse(order.ShippingMethod));
                if (ship != null)
                {
                    order.ShippingMethod = ship.Description;
                    order.Shipper = shippingTypeService.GetByShippingTypeID(ship.ShippingTypeID).Name;
                }
            }

            if (orderEntity.OrderStateID != null && Convert.ToInt32(orderEntity.OrderStateID) > 0)
            {
                var orderStateService = new OrderStateService();
                var orderState = orderStateService.GetByOrderStateID(Convert.ToInt32(orderEntity.OrderStateID));
                if (orderState != null)
                {
                    order.OrderStatus = orderState.OrderStateName;
                }
            }
            //Zeon Custom Code : End

            return order;
        }

        private static int GetOrderId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var oh = new OrderHelper();
                var orderId = oh.GetOrderIDByExternalID(id);
                return int.Parse(orderId);
            }
            return int.Parse(id);
        }

        private static int GetOrderId(string idType, Order order)
        {
            if (idType == IdTypes.External)
            {
                return GetOrderId(idType, order.ExternalId);
            }
            return int.Parse(order.OrderId);
        }

        public static Order GetOrderById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null:
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.Created, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.Modified, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var os = new OrderService();
            var orderEntity = os.GetByOrderID(GetOrderId(idType, id));

            if (orderEntity == null)
            {
                throw new ResourceNotFoundException("Order not found.");
            }

            return GetOrderFromEntity(orderEntity, expands, filters);
        }

        public static Order GetOrderByOrder(Order order, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var os = new OrderService();
            var orderEntity = os.GetByOrderID(GetOrderId(idType, order));

            if (orderEntity == null)
            {
                throw new ResourceNotFoundException("Order not found.");
            }
            var filters = conditions == null?null:
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.Created, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.Modified, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            return GetOrderFromEntity(orderEntity, expands, filters);
        }

        public static List<Order> GetAllOrders(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters,List<string> ordering)
        {
            var oh = new OrderHelper();
            var initFilters = InitializeFilters(filters);

            foreach (var condition in initFilters)
            {
                if (condition.FieldName.Equals(Filters.UpdateDate, StringComparison.InvariantCultureIgnoreCase))
                {
                    condition.FieldName = Filters.Modified;
                }
                if (condition.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
                {
                    condition.FieldName = Filters.Created;
                }
            }

            var queryBuilder = new QueryBuilder("Orders", initFilters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[initFilters.Count, 2];

            #region Populate array
            for (var i = 0; i < initFilters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (initFilters[i].FieldName.Equals(Filters.Created) ||
                            initFilters[i].FieldName.Equals(Filters.Modified))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + initFilters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = initFilters[i].MatchValue;
                    }
                }
            }
            #endregion

            var ids = oh.GetOrderIDsByFilters(query,arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetOrderById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();

            if (!fieldNames.Contains(Filters.Company))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.Company });
            }
            if (!fieldNames.Contains(Filters.FirstName))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.FirstName });
            }
            if (!fieldNames.Contains(Filters.LastName))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.LastName });
            }
            if (!fieldNames.Contains(Filters.Phone))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.Phone });
            }
            if (!fieldNames.Contains(Filters.PostalCode))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.PostalCode });
            }
            if (!fieldNames.Contains(Filters.PortalId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.PortalId });
            }
            if (!fieldNames.Contains(Filters.StoreName))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.StoreName });
            }
            if (!fieldNames.Contains(Filters.Status))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.Status });
            }
            if (!fieldNames.Contains(Filters.TrackingNumber))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.TrackingNumber });
            } 
            if (!fieldNames.Contains(Filters.AccountId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.AccountId });
            }
            return list;
        }

        public static string SaveOrder(Order order)
        {
            if (!String.IsNullOrEmpty(order.OrderId))
            {
                throw new InvalidOperationException("Posting a new order ID is not allowed.");
            }
            if (!String.IsNullOrEmpty(order.ExternalId))
            {
                var oh = new OrderHelper();
                var temp = oh.GetOrderIDByExternalID(order.ExternalId);
                if (temp != null)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var oss = new OrderStateService();
            if (String.IsNullOrEmpty(order.OrderStateId) || oss.GetByOrderStateID(int.Parse(order.OrderStateId)) == null)
            {
                throw new DataException("Invalid order state ID.");
            }

            var os = new OrderService();
            var orderEntity = DataConverter.ToOrderEntity(order, os);

            orderEntity.OrderDate = order.CreateDate ?? DateTime.Now;
            orderEntity.PortalId = 1;

            orderEntity.Created = DateTime.Now;
            orderEntity.Modified = DateTime.Now;

            var result = os.Save(orderEntity);
            return result.OrderID.ToString();
        }

        public static void UpdateOrder(Order order, string idType = null)
        {
            order.OrderId = GetOrderId(idType, order).ToString();

            var os = new OrderService();
            var orderEntity = DataConverter.ToOrderEntity(order, os);

            orderEntity.Modified = DateTime.Now;

            if (!os.Update(orderEntity))
            {
                throw new InvalidOperationException("Specified order does not exist.");
            }
        }

        public static void SaveOrderLineItem(OrderLineItem orderLineItem, string orderId, string orderIdType = null)
        {
            if (!string.IsNullOrEmpty(orderLineItem.OrderLineItemId))
            {
                throw new InvalidOperationException("Posting a new order line item ID is not allowed.");
            }

            orderLineItem.OrderId = GetOrderId(orderIdType, orderId).ToString();

            //if (orderIdType == IdTypes.External)
            //{
            //    var oh = new OrderHelper();
            //    orderLineItem.OrderLineItemId = oh.GetOrderLineItemIDByExternalID(orderLineItem.ExternalId);
            //}

            //if (!String.IsNullOrEmpty(orderLineItem.ExternalId))
            //{
            //    var oh = new OrderHelper();
            //    var temp = oh.GetOrderLineItemIDByExternalID(orderLineItem.ExternalId);
            //    if (temp != null && temp != orderLineItem.OrderLineItemId)
            //    {
            //        throw new InvalidOperationException("External ID already exists.");
            //    }
            //}

            var olis = new OrderLineItemService();
            var orderLineItemEntity = DataConverter.ToOrderLineItemEntity(orderLineItem, olis);

            var ss = new SKUService();
            var sku = ss.GetBySKUID(int.Parse(orderLineItem.SkuId));

            var ps = new ProductService();
            var product = ps.GetByProductID(sku.ProductID);

            orderLineItemEntity.ProductNum = product.ProductNum;
            orderLineItemEntity.SKU = sku.SKU;

            orderLineItemEntity.Created = DateTime.Now;
            orderLineItemEntity.Modified = DateTime.Now;

            olis.Save(orderLineItemEntity);

            var os = new OrderService();
            var orderEntity = os.GetByOrderID(orderLineItemEntity.OrderID);
            orderEntity.Modified = DateTime.Now;
            os.Update(orderEntity);
        }

        public static void UpdateOrderLineItem(OrderLineItem orderLineItem, string orderId, string orderIdType = null)
        {
            orderLineItem.OrderId = GetOrderId(orderIdType, orderId).ToString();

            if (orderIdType == IdTypes.External)
            {
                var oh = new OrderHelper();
                orderLineItem.OrderLineItemId = oh.GetOrderLineItemIDByExternalID(orderLineItem.OrderId, orderLineItem.ExternalId);
            }

            //if (!String.IsNullOrEmpty(orderLineItem.ExternalId))
            //{
            //    var oh = new OrderHelper();
            //    var temp = oh.GetOrderLineItemIDByExternalID(orderLineItem.ExternalId);
            //    if (temp != null && temp != orderLineItem.OrderLineItemId)
            //    {
            //        throw new InvalidOperationException("External ID already exists.");
            //    }
            //}

            var olis = new OrderLineItemService();
            var orderLineItemEntity = DataConverter.ToOrderLineItemEntity(orderLineItem, olis);

            var ss = new SKUService();
            var sku = ss.GetBySKUID(int.Parse(orderLineItem.SkuId));

            var ps = new ProductService();
            var product = ps.GetByProductID(sku.ProductID);

            orderLineItemEntity.ProductNum = product.ProductNum;
            orderLineItemEntity.SKU = sku.SKU;

            if (!olis.Update(orderLineItemEntity))
            {
                throw new InvalidOperationException("Specfied order line item does not exist.");
            }

            var os = new OrderService();
            var orderEntity = os.GetByOrderID(orderLineItemEntity.OrderID);
            orderEntity.Modified = DateTime.Now;
            os.Update(orderEntity);
        }

        public static List<OrderState> GetAllOrderStates(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var oh = new OrderHelper();
            var initFilters = new BindingList<QueryBuilder.WhereCondition>();

            foreach (var condition in initFilters)
            {
                if (condition.FieldName.Equals(Filters.UpdateDate, StringComparison.InvariantCultureIgnoreCase))
                {
                    condition.FieldName = Filters.Modified;
                }
                if (condition.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
                {
                    condition.FieldName = Filters.Created;
                }
            }

            var queryBuilder = new QueryBuilder("OrderState", initFilters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[initFilters.Count, 2];

            #region Populate array
            for (var i = 0; i < initFilters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (initFilters[i].FieldName.Equals(Filters.Created) ||
                            initFilters[i].FieldName.Equals(Filters.Modified))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + initFilters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = initFilters[i].MatchValue;
                    }
                }
            }
            #endregion

            var ids = oh.GetOrderStateIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetOrderStateById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        private static OrderState GetOrderStateFromEntity(IOrderState orderStateEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (orderStateEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var orderState = DataConverter.ToOrderStateDto(orderStateEntity);


            return orderState;
        }

        public static OrderState GetOrderStateById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.Created, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.Modified, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var oss = new OrderStateService();
            var orderStateEntity = oss.GetByOrderStateID(Convert.ToInt32(id));

            if (orderStateEntity == null)
            {
                throw new ResourceNotFoundException("Order State not found.");
            }

            return GetOrderStateFromEntity(orderStateEntity, expands, filters);
        }
    }
}
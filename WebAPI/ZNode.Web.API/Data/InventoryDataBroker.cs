﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;

namespace RogansShoes.Web.API.Data
{
    public static class InventoryDataBroker
    {
        private static Inventory GetInventoryFromEntity(ISKUInventory skuInventoryEntity)
        {
            return DataConverter.ToInventoryDto(skuInventoryEntity);
        }

        public static Inventory GetInventoryBySku(string sku)
        {
            var sis = new SKUInventoryService();
            var inventoryEntity = sis.GetBySKU(sku);

            if (inventoryEntity == null)
            {
                throw new ResourceNotFoundException("SKU not found.");
            }

            return GetInventoryFromEntity(inventoryEntity);
        }

        public static List<Inventory> GetAllInventory(int pageSize, int page, out int count, List<QueryBuilder.WhereCondition> conditions,List<string> ordering)
        {
            var sis = new SKUInventoryService();
            var sh = new SKUHelper();

            var initFilters = InitializeFilters(conditions);

            var query = QueryBuilder.GetDefaultQuery("SKU");
            
            foreach (var condition in initFilters.Where(condition => condition.FieldName.Equals(Filters.UpdateDate, StringComparison.InvariantCultureIgnoreCase)))
            {
                condition.FieldName = Filters.UpdateDte;
                
            }
            
            if (initFilters.Any(x => x.FieldName.Equals(Filters.UpdateDte)))
            {
                var builder = new QueryBuilder("SKU", conditions);
                query +=
                    string.Format(
                        " AND EXISTS( SELECT 1 FROM [dbo].[ZNodeSKU] sku WHERE sku.SKU = SKUInventory.SKU AND {0} )",
                        builder.BuildWhereClause());
            }

            var arrayFilters = new string[initFilters.Count, 2];

            #region Populate array
            for (var i = 0; i < initFilters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (initFilters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + initFilters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = initFilters[i].MatchValue;
                    }
                }
            }
            #endregion

            var skus = sh.GetInventoryByFilters(query,arrayFilters);

            count = skus.Count;

            var tempList = skus.Select(sku => GetInventoryFromEntity(sis.GetBySKU(sku))).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();

            if (!fieldNames.Contains(Filters.PortalId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.PortalId });
            }
            return list;
        }

        public static void UpdateInventory(Inventory inventory)
        {
            var sis = new SKUInventoryService();
            var skuInventoryEntity = sis.GetBySKU(inventory.Sku);

            if (skuInventoryEntity == null)
            {
                throw new InvalidOperationException("Specified SKU does not exist.");
            }

            skuInventoryEntity.QuantityOnHand = inventory.QuantityOnHand;
            sis.Update(skuInventoryEntity);
        }

        public static void CreateTransaction(InventoryTransaction transaction)
        {
            var sis = new SKUInventoryService();
            var skuInventoryEntity = sis.GetBySKU(transaction.Sku);

            if (skuInventoryEntity == null)
            {
                throw new InvalidOperationException("Specified SKU does not exist.");
            }

            skuInventoryEntity.QuantityOnHand += transaction.AmountToChange;
            sis.Update(skuInventoryEntity);
        }
    }
}
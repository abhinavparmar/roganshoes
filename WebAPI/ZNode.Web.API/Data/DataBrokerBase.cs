﻿using System;
using System.Collections.Generic;
using System.Linq;
using RogansShoes.Web.API.Support;

namespace RogansShoes.Web.API.Data
{
    public abstract class DataBrokerBase<T>
    {
        protected T SafeGetFromEntity(string id, Func<string, IList<string>, IDictionary<string, string>, IDictionary<string, T>, IList<string>, T> getFromEntity,
            Func<T, string> getId, IList<string> expands, IDictionary<string, string> filters, IDictionary<string, T> archive = null, IList<string> seen = null)
        {
            if ((archive = archive ?? new Dictionary<string, T>()).ContainsKey(id))
            {
                return archive[id];
            }

            if ((seen = seen ?? new List<string>()).Contains(id))
            {
                throw new CircularReferenceException();
            }
            seen.Add(id);

            var item = getFromEntity(id, expands, filters, archive, new List<string>(seen));

            archive.Add(getId(item), item);
            return item;
        }
    }
}
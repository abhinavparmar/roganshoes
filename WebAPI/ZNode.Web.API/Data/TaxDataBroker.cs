﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using TaxClass = RogansShoes.Web.API.Models.TaxClass;
using TaxRule = RogansShoes.Web.API.Models.TaxRule;
using TaxRuleType = RogansShoes.Web.API.Models.TaxRuleType;
using ZNode.Libraries.Framework.Business;
using Utility = RogansShoes.Web.API.Support.Utility;

namespace RogansShoes.Web.API.Data
{
    public static class TaxDataBroker
    {
        public static void DeleteTaxClassById(string id, string idType = null)
        {
            var tcs = new TaxClassService();
            var taxClassEntity = tcs.GetByTaxClassID(GetTaxClassId(idType, id));

            if (!tcs.Delete(taxClassEntity))
            {
                throw new InvalidOperationException("Specified TaxClass does not exist.");
            }
        }

        public static List<TaxClass> GetAllTaxClasses(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var th = new TaxHelper();

            filters = new List<QueryBuilder.WhereCondition>();

            var queryBuilder = new QueryBuilder("TaxClass", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = th.GetTaxClassIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetTaxClassById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }


        public static TaxClass GetTaxClassById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var pcs = new TaxClassService();
            var taxClassEntity = pcs.GetByTaxClassID(GetTaxClassId(idType, id));

            if (taxClassEntity == null)
            {
                throw new ResourceNotFoundException("TaxClass not found.");
            }

            return GetTaxClassFromEntity(taxClassEntity, expands, filters);
        }

        private static TaxClass GetTaxClassFromEntity(ITaxClass taxClassEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (taxClassEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var taxClass = DataConverter.ToTaxClassDto(taxClassEntity);

            if (expands == null)
            {
                expands = new List<string>();
            }

            return taxClass;
        }

        private static int GetTaxClassId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var th = new TaxHelper();
                var taxClassId = th.GetTaxClassIDByExternalID(id);
                return Convert.ToInt32(taxClassId);
            }
            return Convert.ToInt32(id);
        }

        private static int GetTaxClassId(string idType, TaxClass taxClass)
        {
            if (idType == IdTypes.External)
            {
                return GetTaxClassId(idType, taxClass.ExternalId);
            }
            return Convert.ToInt32(taxClass.TaxClassId);
        }

        public static TaxClass GetTaxClassByTaxClass(TaxClass taxClass, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var tcs = new TaxClassService();
            var taxClassEntity = tcs.GetByTaxClassID(GetTaxClassId(idType, taxClass));

            if (taxClassEntity == null)
            {
                throw new ResourceNotFoundException("TaxClass not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetTaxClassFromEntity(taxClassEntity, expands, filters);
        }

        public static string SaveTaxClass(TaxClass taxClass)
        {
            if (!String.IsNullOrEmpty(taxClass.TaxClassId))
            {
                throw new InvalidOperationException("Posting a new Tax Class ID is not allowed.");
            }
            if (!String.IsNullOrEmpty(taxClass.ExternalId))
            {
                var th = new TaxHelper();
                var temp = th.GetTaxClassIDByExternalID(taxClass.ExternalId);
                if (temp != null)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var tcs = new TaxClassService();
            var taxClassEntity = DataConverter.ToTaxClassEntity(taxClass, tcs);
            var result = tcs.Save(taxClassEntity);

            return result.TaxClassID.ToString();
        }

        public static void UpdateTaxClass(TaxClass taxClass, string idType = null)
        {
            taxClass.TaxClassId = GetTaxClassId(idType, taxClass).ToString();
            
            var ps = new TaxClassService();
            var taxClassEntity = DataConverter.ToTaxClassEntity(taxClass, ps);

            if (!ps.Update(taxClassEntity))
            {
                throw new InvalidOperationException("Specfied TaxClass does not exist.");
            }
        }

        public static void DeleteTaxRuleById(string id, string idType = null)
        {
            var trs = new TaxRuleService();
            var taxRuleEntity = trs.GetByTaxRuleID(GetTaxRuleId(idType, id));

            if (!trs.Delete(taxRuleEntity))
            {
                throw new InvalidOperationException("Specified TaxRule does not exist.");
            }
        }

        public static List<TaxRule> GetAllTaxRules(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var th = new TaxHelper();

            filters = new List<QueryBuilder.WhereCondition>();

            var queryBuilder = new QueryBuilder("TaxRule", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = th.GetTaxRuleIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetTaxRuleById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static TaxRule GetTaxRuleById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var trs = new TaxRuleService();
            var taxRuleEntity = trs.GetByTaxRuleID(GetTaxRuleId(idType, id));

            if (taxRuleEntity == null)
            {
                throw new ResourceNotFoundException("TaxRule not found.");
            }

            return GetTaxRuleFromEntity(taxRuleEntity, expands, filters);
        }

        private static int GetTaxRuleId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var th = new TaxHelper();
                var taxRuleId = th.GetTaxRuleIDByExternalID(id);
                return Convert.ToInt32(taxRuleId);
            }
            return Convert.ToInt32(id);
        }

        private static int GetTaxRuleId(string idType, TaxRule taxRule)
        {
            if (idType == IdTypes.External)
            {
                return GetTaxRuleId(idType, taxRule.ExternalId);
            }
            return Convert.ToInt32(taxRule.TaxRuleId);
        }

        private static TaxRule GetTaxRuleFromEntity(ITaxRule taxRuleEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (taxRuleEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var taxRule = DataConverter.ToTaxRuleDto(taxRuleEntity);

            if (expands == null)
            {
                expands = new List<string>();
            }

            return taxRule;
        }

        public static TaxRule GetTaxRuleByTaxRule(TaxRule taxRule, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var trs = new TaxRuleService();
            var taxRuleEntity = trs.GetByTaxRuleID(GetTaxRuleId(idType, taxRule));

            if (taxRuleEntity == null)
            {
                throw new ResourceNotFoundException("TaxRule not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetTaxRuleFromEntity(taxRuleEntity, expands, filters);
        }

        public static string SaveTaxRule(TaxRule taxRule)
        {
            if (!String.IsNullOrEmpty(taxRule.TaxRuleId))
            {
                throw new InvalidOperationException("Posting a new Tax Class ID is not allowed.");
            }
            if (!String.IsNullOrEmpty(taxRule.ExternalId))
            {
                var th = new TaxHelper();
                var temp = th.GetTaxRuleIDByExternalID(taxRule.ExternalId);
                if (temp != null)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var tcs = new TaxRuleService();
            var taxRuleEntity = DataConverter.ToTaxRuleEntity(taxRule, tcs);
            var result = tcs.Save(taxRuleEntity);

            return result.TaxRuleID.ToString();
        }

        public static void UpdateTaxRule(TaxRule taxRule, string idType = null)
        {
            taxRule.TaxRuleId = GetTaxRuleId(idType, taxRule).ToString();

            var ps = new TaxRuleService();
            var taxRuleEntity = DataConverter.ToTaxRuleEntity(taxRule, ps);

            if (!ps.Update(taxRuleEntity))
            {
                throw new InvalidOperationException("Specfied TaxRule does not exist.");
            }
        }

        public static void DeleteTaxRuleTypeById(string id, string idType = null)
        {
            var trts = new TaxRuleTypeService();
            var taxRuleTypeEntity = trts.GetByTaxRuleTypeID(GetTaxRuleId(idType, id));

            if (!trts.Delete(taxRuleTypeEntity))
            {
                throw new InvalidOperationException("Specified TaxRuleType does not exist.");
            }
        }

        public static List<TaxRuleType> GetAllTaxRuleTypes(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var th = new TaxHelper();

            filters = new List<QueryBuilder.WhereCondition>();

            var queryBuilder = new QueryBuilder("TaxRuleType", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = th.GetTaxRuleTypeIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetTaxRuleTypeById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static TaxRuleType GetTaxRuleTypeById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var trs = new TaxRuleTypeService();
            var taxRuleTypeEntity = trs.GetByTaxRuleTypeID(GetTaxRuleTypeId(idType, id));

            if (taxRuleTypeEntity == null)
            {
                throw new ResourceNotFoundException("TaxRuleType not found.");
            }

            return GetTaxRuleTypeFromEntity(taxRuleTypeEntity, expands, filters);
        }

        private static int GetTaxRuleTypeId(string idType, string id)
        {
            return Convert.ToInt32(id);
        }

        private static int GetTaxRuleTypeId(string idType, TaxRuleType taxRuleType)
        {
            return Convert.ToInt32(taxRuleType.TaxRuleTypeId);
        }

        private static TaxRuleType GetTaxRuleTypeFromEntity(ITaxRuleType taxRuleTypeEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (taxRuleTypeEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var taxRuleType = DataConverter.ToTaxRuleTypeDto(taxRuleTypeEntity);

            if (expands == null)
            {
                expands = new List<string>();
            }

            return taxRuleType;
        }

        public static TaxRuleType GetTaxRuleTypeByTaxRuleType(TaxRuleType taxRuleType, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var trs = new TaxRuleTypeService();
            var taxRuleTypeEntity = trs.GetByTaxRuleTypeID(GetTaxRuleTypeId(idType, taxRuleType));

            if (taxRuleTypeEntity == null)
            {
                throw new ResourceNotFoundException("TaxRuleType not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetTaxRuleTypeFromEntity(taxRuleTypeEntity, expands, filters);
        }

        public static string SaveTaxRuleType(TaxRuleType taxRuleType)
        {
            if (!String.IsNullOrEmpty(taxRuleType.TaxRuleTypeId))
            {
                throw new InvalidOperationException("Posting a new Tax Class ID is not allowed.");
            }

            var tcs = new TaxRuleTypeService();
            var taxRuleTypeEntity = DataConverter.ToTaxRuleTypeEntity(taxRuleType, tcs);
            var result = tcs.Save(taxRuleTypeEntity);

            return result.TaxRuleTypeID.ToString();
        }

        public static void UpdateTaxRuleType(TaxRuleType taxRuleType, string idType = null)
        {
            taxRuleType.TaxRuleTypeId = GetTaxRuleTypeId(idType, taxRuleType).ToString();

            var ps = new TaxRuleTypeService();
            var taxRuleTypeEntity = DataConverter.ToTaxRuleTypeEntity(taxRuleType, ps);

            if (!ps.Update(taxRuleTypeEntity))
            {
                throw new InvalidOperationException("Specfied TaxRuleType does not exist.");
            }
        }
    }
}
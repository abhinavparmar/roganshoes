﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using Catalog = RogansShoes.Web.API.Models.Catalog;

namespace RogansShoes.Web.API.Data
{
    public static class CatalogDataBroker
    {
        public static List<Catalog> GetAllCatalogs(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var ch = new CatalogHelper();

            filters = InitializeFilters(filters);

            var queryBuilder = new QueryBuilder("Catalog", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = ch.GetCatalogIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetCatalogById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static Catalog GetCatalogById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var cs = new CatalogService();
            var catalogEntity = cs.GetByCatalogID(GetCatalogId(idType, id));

            if (catalogEntity == null)
            {
                throw new ResourceNotFoundException("Catalog not found.");
            }

            return GetCatalogFromEntity(catalogEntity, expands, filters);
        }

        public static Catalog GetCatalogByCatalog(Catalog catalog, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var cs = new CatalogService();
            var catalogEntity = cs.GetByCatalogID(GetCatalogId(idType, catalog));

            if (catalogEntity == null)
            {
                throw new ResourceNotFoundException("Catalog not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetCatalogFromEntity(catalogEntity, expands, filters);
        }

        private static Catalog GetCatalogFromEntity(ICatalog catalogEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (catalogEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var catalog = DataConverter.ToCatalogDto(catalogEntity);

            if (expands == null)
            {
                expands = new List<string>();
            }

            return catalog;
        }

        private static int GetCatalogId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var ch = new CatalogHelper();
                var catalogId = ch.GetCatalogIDByExternalID(id);
                return Convert.ToInt32(catalogId);
            }
            return Convert.ToInt32(id);
        }

        private static int GetCatalogId(string idType, Catalog catalog)
        {
            if (idType == IdTypes.External)
            {
                return GetCatalogId(idType, catalog.ExternalId);
            }
            return Convert.ToInt32(catalog.CatalogId);
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();
            return list;
        }

        public static string SaveCatalog(Catalog catalog)
        {
            if (!String.IsNullOrEmpty(catalog.CatalogId))
            {
                throw new InvalidOperationException("Posting a new catalog ID is not allowed.");
            }
            if (!String.IsNullOrEmpty(catalog.ExternalId))
            {
                var ch = new CatalogHelper();
                var temp = ch.GetCatalogIDByExternalID(catalog.ExternalId);
                if (temp != null)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var cs = new CatalogService();
            var catalogEntity = DataConverter.ToCatalogEntity(catalog, cs);

            var result = cs.Save(catalogEntity);
            return result.CatalogID.ToString();
        }

        public static void UpdateCatalog(Catalog catalog, string idType = null)
        {
            catalog.CatalogId = GetCatalogId(idType, catalog).ToString();

            if (!String.IsNullOrEmpty(catalog.ExternalId))
            {
                var ch = new CatalogHelper();
                var temp = ch.GetCatalogIDByExternalID(catalog.ExternalId);
                if (temp != null && temp != catalog.CatalogId)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var cs = new CatalogService();
            var catalogEntity = DataConverter.ToCatalogEntity(catalog, cs);

            if (!cs.Update(catalogEntity))
            {
                throw new InvalidOperationException("Specfied catalog does not exist.");
            }
        }
    }
}
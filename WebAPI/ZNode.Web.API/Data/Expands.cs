﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RogansShoes.Web.API.Data
{
    public static class Expands
    {
        public const string AddOns = "addons";
        public const string Addresses = "addresses";
        public const string Attributes = "attributes";
        public const string BillingInformation = "billinginformation";
        public const string CategoryNodes = "categorynodes";
        public const string CrossSells = "crosssells";
        public const string Highlights = "highlights";
        public const string Image = "image";
        public const string Pricing = "pricing";
        public const string Profiles = "profiles";
        public const string Reviews = "reviews";
        public const string Seo = "seo";
        public const string ShippingInformation = "shippinginformation";
        public const string Skus = "skus";
        public const string Subcategories = "subcategories";
        public const string TieredPricing = "tieredpricing";
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RogansShoes.Web.API.Data
{
    public class QueryBuilder
    {
        private readonly List<WhereCondition> _whereConditions;

        private readonly string _tableName;

        public QueryBuilder(string tableName, IEnumerable<WhereCondition> conditions)
        {
            _tableName = tableName;
            _whereConditions =
                conditions.Where(
                    x =>
                    x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    x.FieldName.Equals(Filters.CreateDte, StringComparison.InvariantCultureIgnoreCase) ||
                    x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase) ||
                    x.FieldName.Equals(Filters.Created, StringComparison.InvariantCultureIgnoreCase) ||
                    x.FieldName.Equals(Filters.Modified, StringComparison.InvariantCultureIgnoreCase) ||
                    x.FieldName.Equals(Filters.LastUpdatedDate, StringComparison.InvariantCultureIgnoreCase) ||
                    x.FieldName.Equals(Filters.CreatedDate, StringComparison.InvariantCultureIgnoreCase) ||
                    x.FieldName.Equals(Filters.UpdateDate, StringComparison.InvariantCultureIgnoreCase)).ToList();
        }

        public class WhereCondition
        {
            public string FieldName { get; set; }
            public string Operator { get; set; }
            public bool UseQuotes { get; set; }
            public string MatchValue { get; set; }
        }

        public string BuildWhereClause()
        {
            var whereClauses = string.Empty;
            if (_whereConditions.Count > 0)
            {
                whereClauses = string.Join(" AND ", _whereConditions.Select(GetFilterCondition).ToArray());
            }
            return whereClauses;
        }

        public string GetFilterCondition(WhereCondition whereCondition)
        {
            var operatorText = string.Empty;

            switch (whereCondition.Operator)
            {
                case QueryFilterOperators.EqualTo:
                    operatorText = "= {0}";
                    break;
                case QueryFilterOperators.NotEqualTo:
                    operatorText = "!= {0}";
                    break;
                case QueryFilterOperators.GreaterThan:
                    operatorText = "> {0}";
                    break;
                case QueryFilterOperators.GreaterThanOrEqualTo:
                    operatorText = ">= {0}";
                    break;
                case QueryFilterOperators.LessThanOrEqualTo:
                    operatorText = "<= {0}";
                    break;
                case QueryFilterOperators.LessThan:
                    operatorText = "< {0}";
                    break;
            }

            var filterCondition = _tableName + "." + whereCondition.FieldName + " " + string.Format(operatorText, "@p" + _whereConditions.IndexOf(whereCondition));

            return filterCondition;
        }

        public string GetQuery()
        {
            var query = string.Empty;
            var partOfWhereClause = BuildWhereClause();
            switch (_tableName)
            {
                case "Account":
                    query = Entity.AccountQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "AddOn":
                    query = Entity.AddOnQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "AddOnValue":
                    query = Entity.AddOnValueQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "Catalog":
                    query = Entity.CatalogQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "Category":
                    query = Entity.CategoryQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "DiscountType":
                    query = Entity.DiscountTypeQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "Orders":
                    query = Entity.OrderQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "OrderState":
                    query = Entity.OrderStateQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "Portal":
                    query = Entity.PortalQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "Product":
                    query = Entity.ProductQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "ProductReviewState":
                    query = Entity.ProductReviewStateQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "Profile":
                    query = Entity.ProfileQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "Promotion":
                    query = Entity.PromotionQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "Shipping":
                    query = Entity.ShippingQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "ShippingRule":
                    query = Entity.ShippingRuleQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "ShippingRuleType":
                    query = Entity.ShippingRuleTypeQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "ShippingType":
                    query = Entity.ShippingTypeQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "SKU":
                    query = Entity.SKU;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "SavedCart":
                    query = Entity.SavedCart;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "Supplier":
                    query = Entity.SupplierQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "SupplierType":
                    query = Entity.SupplierTypeQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "TaxClass":
                    query = Entity.TaxClassQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "TaxRule":
                    query = Entity.TaxRuleQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
                case "TaxRuleType":
                    query = Entity.TaxRuleTypeQuery;
                    if (partOfWhereClause != string.Empty)
                    {
                        query += " AND " + partOfWhereClause;
                    }
                    return query;
            }
            return query;
        }

        public static string GetDefaultQuery(string tableName)
        {
            switch (tableName)
            {
                case "Account":
                    return Entity.AccountQuery;
                case "AddOn":
                    return Entity.AddOnQuery;
                case "AddOnValue":
                    return Entity.AddOnValueQuery;
                case "Catalog":
                    return Entity.CatalogQuery;
                case "Category":
                    return Entity.CategoryQuery;
                case "DiscountType":
                    return Entity.DiscountTypeQuery;
                case "Orders":
                    return Entity.OrderQuery;
                case "OrderState":
                    return Entity.OrderStateQuery;
                case "Portal":
                    return Entity.PortalQuery;
                case "Product":
                    return Entity.ProductQuery;
                case "ProductReviewState":
                    return Entity.ProductReviewStateQuery;
                case "Profile":
                    return Entity.ProfileQuery;
                case "Promotion":
                    return Entity.PromotionQuery;
                case "SKU":
                    return Entity.SKU;
                case "SavedCart":
                    return Entity.SavedCart;
                case "Shipping":
                    return Entity.ShippingQuery;
                case "ShippingRule":
                    return Entity.ShippingRuleQuery;
                case "ShippingRuleType":
                    return Entity.ShippingRuleTypeQuery;
                case "ShippingType":
                    return Entity.ShippingTypeQuery;
                case "Supplier":
                    return Entity.SupplierQuery;
                case "SupplierType":
                    return Entity.SupplierTypeQuery;
                case "TaxClass":
                    return Entity.TaxClassQuery;
                case "TaxRule":
                    return Entity.TaxRuleQuery;
                case "TaxRuleType":
                    return Entity.TaxRuleTypeQuery;
            }
            return "";
        }
    }

    public class Entity
    {


        #region Account

        public const string AccountQuery = "SELECT Account.accountid " +
                                            "FROM [dbo].[ZNodeAccount] Account " +
                                            "WHERE (@firstname IS NULL OR EXISTS( " +
                                            "SELECT 1 FROM [dbo].[ZNodeAddress] addr " +
                                            "WHERE addr.AccountID = Account.AccountId " +
                                            "AND addr.FirstName = @firstname )) " +

                                            "AND " +

                                            "(@lastname IS NULL OR EXISTS( " +
                                            "SELECT 1 FROM [dbo].[ZNodeAddress] addr " +
                                            "WHERE addr.AccountID = Account.AccountId " +
                                            "AND addr.lastname = @lastname )) " +

                                            "AND " +

                                            "(@company IS NULL OR EXISTS( " +
                                            "SELECT 1 FROM [dbo].[ZNodeAddress] addr " +
                                            "WHERE addr.AccountID = Account.AccountId " +
                                            "AND addr.companyname = @company )) " +

                                            "AND " +

                                            "(@postalcode IS NULL OR EXISTS( " +
                                            "SELECT 1 FROM [dbo].[ZNodeAddress] addr " +
                                            "WHERE addr.AccountID = Account.AccountId " +
                                            "AND addr.postalcode = @postalcode )) " +

                                            "AND " +

                                            "(@phone IS NULL OR EXISTS( " +
                                            "SELECT 1 FROM [dbo].[ZNodeAddress] addr " +
                                            "WHERE addr.AccountID = Account.AccountId " +
                                            "AND addr.phonenumber = @phone )) " +

                                            "AND " +

                                            "(@portalid IS NULL OR EXISTS( " +
                                            "SELECT 1 FROM [dbo].[ZNodeAccountProfile] aprof " +
                                            "JOIN [dbo].[ZNodePortalProfile] pprof " +
                                            "ON aprof.ProfileId = pprof.ProfileId " +
                                            "WHERE aprof.AccountID = Account.AccountID " +
                                            "AND pprof.PortalID = @portalid )) ";
        #endregion Catalog

        public const string AddOnQuery = "SELECT AddOn.AddOnID " +
                                         "FROM [dbo].[ZNodeAddOn] AddOn";

        public const string AddOnValueQuery = "SELECT AddOnValue.AddOnValueID " +
                                              "FROM [dbo].[ZNodeAddOnValue] AddOnValue";

        public const string CatalogQuery = "SELECT Catalog.CatalogID " +
                                           "FROM [dbo].[ZNodeCatalog] Catalog";

        #region

        #endregion

        #region Category

        public const string CategoryQuery = "SELECT category.CategoryID " +
                                            "FROM [dbo].[ZNodeCategory] category " +
                                            "WHERE (@relatedparent IS NULL OR EXISTS( " +
                                            "SELECT 1 FROM [dbo].[ZNodeCategoryNode] catnode " +
                                            "WHERE catnode.ParentCategoryNodeID = category.CategoryID " +
                                            "AND catnode.CategoryID = @relatedparent )) " +

                                            "AND " +

                                            "(@catalogid IS NULL OR EXISTS( " +
                                            "SELECT 1 FROM [dbo].[ZNodeCategoryNode] catnode " +
                                            "WHERE catnode.CategoryID = category.CategoryID " +
                                            "AND catnode.CatalogID = @catalogid )) " +

                                            "AND " +

                                            "(@portalid IS NULL OR EXISTS( " +
                                            "SELECT 1 FROM [dbo].[ZNodeCategoryNode] catnode " +
                                            "JOIN [dbo].[ZNodePortalCatalog] portcat " +
                                            "ON catnode.CatalogID = portcat.CatalogID " +
                                            "WHERE catnode.CategoryID = category.CategoryID " +
                                            "AND portcat.PortalID = @portalid )) " +

                                            "AND " +

                                            "(@localeid IS NULL OR EXISTS( " +
                                            "SELECT 1 FROM [dbo].[ZNodeCategoryNode] catnode " +
                                            "JOIN [dbo].[ZNodePortalCatalog] portcat " +
                                            "ON catnode.CatalogID = portcat.CatalogID " +
                                            "JOIN [dbo].[ZNodePortal] port " +
                                            "ON portcat.PortalID = port.PortalID " +
                                            "WHERE catnode.CategoryID = category.CategoryID " +
                                            "AND port.LocaleID = @localeid )) " +

                                            "AND " +

                                            "(@storename IS NULL OR EXISTS( " +
                                            "SELECT 1 FROM [dbo].[ZNodeCategoryNode] catnode " +
                                            "JOIN [dbo].[ZNodePortalCatalog] portcat " +
                                            "ON catnode.CatalogID = portcat.CatalogID " +
                                            "JOIN [dbo].[ZNodePortal] port " +
                                            "ON portcat.PortalID = port.PortalID " +
                                            "WHERE catnode.CategoryID = category.CategoryID " +
                                            "AND port.StoreName = @storename )) ";

        #endregion

        public const string DiscountTypeQuery = "SELECT DiscountType.DiscountTypeID " +
                                                "FROM [dbo].[ZNodeDiscountType] DiscountType";

        #region Order

        public const string OrderQuery = "SELECT orders.orderid " +
                                         "FROM [dbo].[ZNodeOrder] orders " +
                                         "WHERE (@AccountID is null or " +
                                         "orders.AccountID = @AccountID ) " +

                                         "AND " +
                                         "(@firstname is null or " +
                                         "orders.BillingFirstName = @firstname or " +
                                         "orders.ShipFirstName = @firstname ) " +

                                         "AND " +

                                         "(@lastname is null or " +
                                         "orders.BillingLastName = @lastname or " +
                                         "orders.ShipLastName = @lastname ) " +

                                         "AND " +

                                         "(@phone is null or " +
                                         "orders.BillingPhoneNumber = @phone or " +
                                         "orders.ShipPhoneNumber = @phone ) " +

                                         "AND " +

                                         "(@postalcode is null or " +
                                         "orders.BillingPostalCode = @postalcode or " +
                                         "orders.ShipPostalCode = @postalcode ) " +

                                         "AND " +

                                         "(@portalid is null or " +
                                         "orders.PortalID = @portalid ) " +

                                         "AND " +

                                         "(@status is null or " +
                                         "orders.OrderStateID = @status ) " +

                                         "AND " +

                                         "(@trackingnumber is null or " +
                                         "orders.TrackingNumber = @trackingnumber ) " +

                                        "AND " +

                                        "(@storename IS NULL OR EXISTS( " +
                                        "SELECT 1 FROM [dbo].[ZNodePortal] port " +
                                        "WHERE port.PortalID = orders.PortalID " +
                                        "AND port.StoreName = @storename ))";

        #endregion

        public const string OrderStateQuery = "SELECT OrderState.OrderStateID " +
                                              "FROM [dbo].[ZNodeOrderState] OrderState";

        #region Portal

        public const string PortalQuery = "SELECT Portal.PortalID " +
                                          "FROM [dbo].[ZNodePortal] Portal";
        #endregion

        #region Product
        public const string ProductQuery = "SELECT product.ProductID " +
                                           "FROM [dbo].[ZNodeProduct] product " +
                                           "WHERE (@relatedparent IS NULL " +
                                           "OR EXISTS(SELECT 1 " +
                                           "FROM [dbo].[ZNodeProductCrossSell] prodcrssll " +
                                           "WHERE prodcrssll.RelatedProductID = product.ProductID " +
                                           "AND prodcrssll.ProductID = @relatedparent)) " +

                                           "AND " +

                                           "(@sku IS NULL OR EXISTS( " +
                                           "SELECT 1 FROM [dbo].[ZNodeSKU] sku " +
                                           "WHERE sku.ProductID = product.ProductID " +
                                           "AND sku.SKU = @sku)) " +

                                           "AND " +

                                           "(@categoryid IS NULL OR EXISTS( " +
                                           "SELECT 1 FROM [dbo].[ZNodeProductCategory] prodcat " +
                                           "WHERE prodcat.ProductID = product.ProductID " +
                                           "AND prodcat.CategoryID = @categoryid)) " +

                                           "AND " +

                                           "(@portalid IS NULL OR EXISTS( " +
                                           "SELECT 1 FROM [dbo].[ZNodeProductCategory] prodcat " +
                                           "JOIN [dbo].[ZNodeCategoryNode] catnode " +
                                           "ON prodcat.CategoryID = catnode.CategoryID " +
                                           "JOIN [dbo].[ZNodePortalCatalog] portcat " +
                                           "ON catnode.CatalogID = portcat.CatalogID " +
                                           "WHERE prodcat.ProductID = product.ProductID " +
                                           "AND portcat.PortalID = @portalid)) " +

                                           "AND " +

                                           "(@localeid IS NULL OR EXISTS( " +
                                           "SELECT 1 FROM [dbo].[ZNodeProductCategory] prodcat " +
                                           "JOIN [dbo].[ZNodeCategoryNode] catnode " +
                                           "ON prodcat.CategoryID = catnode.CategoryID " +
                                           "JOIN [dbo].[ZNodePortalCatalog] portcat " +
                                           "ON catnode.CatalogID = portcat.CatalogID " +
                                           "JOIN [dbo].[ZNodePortal] port " +
                                           "ON portcat.PortalID = port.PortalID " +
                                           "WHERE prodcat.ProductID = product.ProductID " +
                                           "AND port.LocaleID = @localeid)) " +

                                           "AND " +

                                           "(@storename IS NULL OR EXISTS( " +
                                           "SELECT 1 FROM [dbo].[ZNodeProductCategory] prodcat " +
                                           "JOIN [dbo].[ZNodeCategoryNode] catnode " +
                                           "ON prodcat.CategoryID = catnode.CategoryID " +
                                           "JOIN [dbo].[ZNodePortalCatalog] portcat " +
                                           "ON catnode.CatalogID = portcat.CatalogID " +
                                           "JOIN [dbo].[ZNodePortal] port " +
                                           "ON portcat.PortalID = port.PortalID " +
                                           "WHERE prodcat.ProductID = product.ProductID " +
                                           "AND port.StoreName = @storename)) ";
        #endregion

        public const string ProductReviewStateQuery = "SELECT ProductReviewState.ReviewStateID " +
                                                      "FROM [dbo].[ZNodeProductReviewState] ProductReviewState";

        #region Promotion

        public const string PromotionQuery = "SELECT Promotion.PromotionID " +
                                          "FROM [dbo].[ZNodePromotion] Promotion";

        #endregion

        public const string ProfileQuery = "SELECT Profile.ProfileID " +
                                           "FROM [dbo].[ZNodeProfile] Profile";

        #region SKU

        public const string SKU = "SELECT SKUInventory.SKU " +
                                    "FROM [dbo].[ZNodeSKUInventory] SKUInventory " +
                                    "WHERE (@portalid IS NULL OR EXISTS( " +
                                    "SELECT 1 FROM [dbo].[ZNodeSKU] sku " +
                                    "JOIN [dbo].[ZNodeProductCategory] prodcat " +
                                    "ON sku.ProductID = prodcat.ProductID " +
                                    "JOIN [dbo].[ZNodeCategoryNode] catnode " +
                                    "ON prodcat.CategoryID = catnode.CategoryID " +
                                    "JOIN [dbo].[ZNodePortalCatalog] portcat " +
                                    "ON catnode.CatalogID = portcat.CatalogID " +
                                    "WHERE sku.SKU = SKUInventory.SKU " +
                                    "AND portcat.PortalID = @portalid )) ";
        #endregion

        #region SavedCart

        public const string SavedCart = "SELECT SavedCart.savedCartid " +
                                        "FROM [dbo].[ZNodeSavedCart] SavedCart " +
                                        "WHERE SavedCart.SavedCartId is not null " +

                                        "AND " +

                                        "(@portalid IS NULL OR EXISTS( " +
                                        "SELECT 1 FROM [dbo].[ZNodeCookieMapping] cookieMapping " +
                                        "inner join [dbo].[ZNodePortal] portal on portal.PortalID = cookieMapping.PortalID " +
                                        "WHERE portal.PortalID = @portalid )) " +

                                        "AND " +

                                        "(@storename IS NULL OR EXISTS( " +
                                        "SELECT 1 FROM [dbo].[ZNodeCookieMapping] cookieMapping " +
                                        "inner join [dbo].[ZNodePortal] portal on portal.PortalID = cookieMapping.PortalID " +
                                        "WHERE portal.StoreName = @storename )) ";

        #endregion

        public const string ShippingQuery = "SELECT Shipping.ShippingID " +
                                           "FROM [dbo].[ZNodeShipping] Shipping";

        public const string ShippingRuleQuery = "SELECT ShippingRule.ShippingRuleID " +
                                                "FROM [dbo].[ZNodeShippingRule] ShippingRule";

        public const string ShippingRuleTypeQuery = "SELECT ShippingRuleType.ShippingRuleTypeID " +
                                                    "FROM [dbo].[ZNodeShippingRuleType] ShippingRuleType";

        public const string ShippingTypeQuery = "SELECT ShippingType.ShippingTypeID " +
                                                    "FROM [dbo].[ZNodeShippingType] ShippingType";

        public const string SupplierQuery = "SELECT Supplier.SupplierID " +
                                            "FROM [dbo].[ZNodeSupplier] Supplier";

        public const string SupplierTypeQuery = "SELECT SupplierType.SupplierTypeID " +
                                            "FROM [dbo].[ZNodeSupplierType] SupplierType";

        public const string TaxClassQuery = "SELECT TaxClass.TaxClassID " +
                                            "FROM [dbo].[ZNodeTaxClass] TaxClass";

        public const string TaxRuleQuery = "SELECT TaxRule.TaxRuleID " +
                                            "FROM [dbo].[ZNodeTaxRule] TaxRule";

        public const string TaxRuleTypeQuery = "SELECT TaxRuleType.TaxRuleTypeID " +
                                                 "FROM [dbo].[ZNodeTaxRuleType] TaxRuleType";

    }

    public class QueryFilterOperators
    {
        public const string EqualTo = "eq";
        public const string NotEqualTo = "ne";
        public const string GreaterThan = "gt";
        public const string GreaterThanOrEqualTo = "ge";
        public const string LessThan = "lt";
        public const string LessThanOrEqualTo = "le";
    }
}
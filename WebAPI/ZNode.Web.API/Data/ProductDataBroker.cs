﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using AddOn = RogansShoes.Web.API.Models.AddOn;
using AttributeType = RogansShoes.Web.API.Models.AttributeType;
using Highlight = RogansShoes.Web.API.Models.Highlight;
using Product = RogansShoes.Web.API.Models.Product;
using ProductReviewState = RogansShoes.Web.API.Models.ProductReviewState;
using Review = RogansShoes.Web.API.Models.Review;

namespace RogansShoes.Web.API.Data
{
    public static class ProductDataBroker
    {        
        private static Product GetProductFromEntity(IProduct productEntity, IList<string> expands, IDictionary<string, string> filters,
            IDictionary<string, Product> archive = null, IList<string> seen = null)
        {
            if (productEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var product = DataConverter.ToProductDto(productEntity);

            // .NET cannot deserialize null dates, so make sure to never send dates that are null
            if (!product.CreateDate.HasValue)
            {
                product.CreateDate = DateTime.Now;
            }
            if (!product.UpdateDate.HasValue)
            {
                product.UpdateDate = DateTime.Now;
            }

            if ((archive = archive ?? new Dictionary<string, Product>()).ContainsKey(product.ProductId))
            {
                return archive[product.ProductId];
            }

            if ((seen = seen ?? new List<string>()).Contains(product.ProductId))
            {
                throw new CircularReferenceException();
            }
            seen.Add(product.ProductId);

            if (expands == null)
            {
                expands = new List<string>();
            }

            if (!expands.Contains(Expands.Image))
            {
                product.Image = null;
            }
            if (!expands.Contains(Expands.Pricing))
            {
                product.Pricing = null;
            }
            if (!expands.Contains(Expands.Seo))
            {
                product.Seo = null;
            }

            if (expands.Contains(Expands.AddOns))
            {
                var paos = new ProductAddOnService();
                var aos = new AddOnService();

                product.AddOns = new Collection<AddOn>(paos.GetByProductID(productEntity.ProductID)
                    .Select(pao => aos.GetByAddOnID(pao.AddOnID)).Select(DataConverter.ToAddOnDto).ToList());
            }

            if (expands.Contains(Expands.Attributes))
            {
                var ss = new SKUService();
                var sas = new SKUAttributeService();
                var pas = new ProductAttributeService();

                var skuFilter = new Func<SKU, bool>(s => true);
                if (filters.ContainsKey(Filters.Sku))
                {
                    var value = filters[Filters.Sku];
                    skuFilter = s => s.SKU == value;
                }

                var productAttributes = ss.GetByProductID(productEntity.ProductID).Where(skuFilter)
                    .SelectMany(s => sas.GetBySKUID(s.SKUID)).Select(sa => pas.GetByAttributeId(sa.AttributeId));

                product.AttributeTypes = new Collection<AttributeType>(GetAttributesFromEntities(productAttributes));
            }

            if (expands.Contains(Expands.CrossSells))
            {
                var ps = new ProductService();
                var ph = new ProductHelper();

                var ids = ph.GetProductIDsByFilters(QueryBuilder.GetDefaultQuery("Product"), CreateArrayFromDictionary(filters), product.ProductId);

                product.CrossSells = new Collection<Product>(ids.Select(id => ps.GetByProductID(int.Parse(id)))
                    .Select(p => GetProductFromEntity(p, expands, filters, archive, new List<string>(seen))).ToList());
            }

            if (expands.Contains(Expands.Highlights))
            {
                var hs = new HighlightService();
                var phs = new ProductHighlightService();

                product.Highlights = new Collection<Highlight>(phs.GetByProductID(productEntity.ProductID)
                    .Select(ph => hs.GetByHighlightID(ph.HighlightID)).Select(DataConverter.ToHighlightDto).ToList());
            }

            if (expands.Contains(Expands.Reviews))
            {
                var rs = new ReviewService();

                product.Reviews = new Collection<Review>(rs.GetByProductID(productEntity.ProductID).Select(DataConverter.ToReviewDto).ToList());
            }

            if (expands.Contains(Expands.Skus))
            {
                var ss = new SKUService();
                var sas = new SKUAttributeService();
                var pas = new ProductAttributeService();

                product.Skus = new Collection<Sku>(ss.GetByProductID(productEntity.ProductID).Where(s => FilterSkus(s, filters)).Select(s =>
                {
                    var sku = DataConverter.ToSkuDto(s);

                    var skuAttributes = sas.GetBySKUID(s.SKUID).Select(sa => pas.GetByAttributeId(sa.AttributeId));

                    sku.AttributeTypes = new Collection<AttributeType>(GetAttributesFromEntities(skuAttributes));

                    return sku;
                }).ToList());
            }

            if (expands.Contains(Expands.TieredPricing))
            {
                var pts = new ProductTierService();

                product.TieredPricing = new Collection<TieredPricing>(pts.GetByProductID(productEntity.ProductID).Select(DataConverter.ToTieredPricingDto).ToList());
            }

            archive.Add(product.ProductId, product);

            return product;
        }

        /// <summary>
        /// create two dimentionary array from a dictionary
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        private static string[,] CreateArrayFromDictionary(IDictionary<string, string> filters)
        {
            var array = new string[filters.Keys.Count, 2];

            var keys = filters.Keys.ToArray();
            var values = filters.Values.ToArray();

            for (var i = 0; i < filters.Keys.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        array[i, j] = keys[i];
                    }
                    if (j == 1)
                    {
                        array[i, j] = values[i];
                    }
                }
            }

            return array;
        }

        private static List<AttributeType> GetAttributesFromEntities(IEnumerable<ProductAttribute> productAttributes)
        {
            var ats = new AttributeTypeService();
            
            var attributeTypes = new Dictionary<int, AttributeType>();
            var seenAttributeValues = new List<int>();
            
            foreach (var attributeValue in productAttributes.Where(av => !seenAttributeValues.Contains(av.AttributeId)))
            {
                seenAttributeValues.Add(attributeValue.AttributeId);

                if (!attributeTypes.ContainsKey(attributeValue.AttributeTypeId))
                {
                    var attributeType = ats.GetByAttributeTypeId(attributeValue.AttributeTypeId);
                    attributeTypes.Add(attributeType.AttributeTypeId, DataConverter.ToAttributeTypeDto(attributeType));
                }
                attributeTypes[attributeValue.AttributeTypeId].AttributeValues.Add(DataConverter.ToAttributeValueDto(attributeValue));
            }

            return attributeTypes.Values.ToList();
        }

        private static bool FilterSkus(ISKU skuEntity, IDictionary<string, string> filters)
        {
            if (filters.ContainsKey(Filters.Sku) && skuEntity.SKU != filters[Filters.Sku])
            {
                return false;
            }
            return true;
        }

        public static int GetProductId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var ph = new ProductHelper();
                var productId = ph.GetProductIDByExternalID(id);
                return int.Parse(productId);
            }
            if (idType == IdTypes.SkuId)
            {
                var ss = new SKUService();
                var productId = ss.GetBySKUID(int.Parse(id)).ProductID;
                return productId;
            }
            if (idType == IdTypes.ExternalSkuId)
            {
                var ph = new ProductHelper();
                var productId = ph.GetProductIDByExternalSKUID(id);
                return int.Parse(productId);
            }
            return int.Parse(id);
        }

        public static int GetProductId(string idType, Product product)
        {
            if (idType == IdTypes.External)
            {
                return GetProductId(idType, product.ExternalId);
            }
            if (idType == IdTypes.ExternalSkuId)
            {
                return GetProductId(idType, product.Skus.First().ExternalId);
            }
            return int.Parse(product.ProductId);
        }

        public static Product GetProductById(string id, IList<string> expands, IDictionary<string, string> filters, string idType = null)
        {
            var ps = new ProductService();
            var productEntity = ps.GetByProductID(GetProductId(idType, id));

            if (productEntity == null)
            {
                throw new ResourceNotFoundException("Product not found.");
            }

            return GetProductFromEntity(productEntity, expands, filters);
        }

        public static Product GetProductById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null?null:
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var ps = new ProductService();
            var productEntity = ps.GetByProductID(GetProductId(idType, id));

            if (productEntity == null)
            {
                throw new ResourceNotFoundException("Product not found.");
            }

            return GetProductFromEntity(productEntity, expands, filters);
        }
        
        public static Product GetProductByProduct(Product product, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var ps = new ProductService();
            var productEntity = ps.GetByProductID(GetProductId(idType, product));

            if (productEntity == null)
            {
                throw new ResourceNotFoundException("Product not found.");
            }

            var filters = conditions == null?null:
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetProductFromEntity(productEntity, expands, filters);
        }

        public static List<Product> GetAllProducts(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            filters = InitializeFilters(filters);

            foreach (var filter in filters.Where(filter => filter.FieldName.Equals(Filters.ModifyDate, StringComparison.InvariantCultureIgnoreCase)))
            {
                filter.FieldName = Filters.UpdateDte;
            }
            var queryBuilder = new QueryBuilder("Product", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array
            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDate) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }
            #endregion

            var ph = new ProductHelper();

            var ids = ph.GetProductIDsByFilters(query, arrayFilters);


            count = ids.Count;

            var tempList = ids.Select(id => GetProductById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();

            if (!fieldNames.Contains(Filters.Sku))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.Sku });
            }
            if (!fieldNames.Contains(Filters.CategoryId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.CategoryId });
            }
            if (!fieldNames.Contains(Filters.PortalId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.PortalId });
            }
            if (!fieldNames.Contains(Filters.StoreName))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.StoreName });
            }
            if (!fieldNames.Contains(Filters.LocaleId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.LocaleId });
            }

            return list;
        }

        public static string SaveProduct(Product product)
        {
            if (!String.IsNullOrEmpty(product.ProductId))
            {
                throw new InvalidOperationException("Posting a new product ID is not allowed.");
            }

            if (string.IsNullOrEmpty(product.Name) || string.IsNullOrEmpty(product.Description))
            {
                throw new InvalidOperationException("Product name and description must not be null.");
            }

            var ps = new ProductService();
            var productEntity = DataConverter.ToProductEntity(product, ps);

            var pts = new ProductTypeService();
            productEntity.ProductTypeID = pts.GetByName("Default").ProductTypeId;

            productEntity.ActiveInd = true;
            productEntity.CreateDate = DateTime.Now;
            productEntity.UpdateDte = DateTime.Now;

            var result = ps.Save(productEntity);
            return result.ProductID.ToString();
        }

        public static void UpdateProduct(Product product, string idType = null)
        {
            product.ProductId = GetProductId(idType, product).ToString();
            
            var ps = new ProductService();
            var productEntity = DataConverter.ToProductEntity(product, ps);

            productEntity.UpdateDte = DateTime.Now;

            if (!ps.Update(productEntity))
            {
                throw new InvalidOperationException("Specified product does not exist.");
            }
        }

        public static void SaveProductSku(Sku sku, string productId, string productIdType = null)
        {
            if (!String.IsNullOrEmpty(sku.SkuId))
            {
                throw new InvalidOperationException("Posting a new SKU ID is not allowed.");
            }

            sku.ProductId = GetProductId(productIdType, productId).ToString();

            var sis = new SKUInventoryService();
            if (sis.GetBySKU(sku.SKU) == null)
            {
                var skuInventory = new SKUInventory
                {
                    SKU = sku.SKU
                };
                sis.Save(skuInventory);
            }

            var ss = new SKUService();
            var skuEntity = DataConverter.ToSkuEntity(sku, ss);

            ss.Save(skuEntity);
        }

        public static void UpdateProductSku(Sku sku, Product product, string productIdType = null)
        {
            if (string.IsNullOrEmpty(sku.SkuId))
            {
                var ph = new ProductHelper();
                sku.SkuId = ph.GetSKUIDByExternalSKUID(sku.ExternalId);
            }

            sku.ProductId = GetProductId(productIdType, product).ToString();

            var ss = new SKUService();
            var skuEntity = DataConverter.ToSkuEntity(sku, ss);

            ss.Update(skuEntity);
        }

        public static void SaveProductAddOn(AddOn addOn, string productId, string productIdType = null)
        {
            var paos = new ProductAddOnService();
            var productAddOnEntity = new ProductAddOn
            {
                AddOnID = int.Parse(addOn.AddOnId),
                ProductID = GetProductId(productIdType, productId)
            };

            paos.Save(productAddOnEntity);

            var ps = new ProductService();
            var productEntity = ps.GetByProductID(productAddOnEntity.ProductID);
            productEntity.UpdateDte = DateTime.Now;
            ps.Update(productEntity);
        }

        public static void DeleteProductAddOn(string addOnId, string productId, string productIdType = null)
        {
            var ph = new ProductHelper();
            var pid = GetProductId(productIdType, productId);
            var productAddonId = ph.GetProductAddOnIDByProductIDAddOnID(pid.ToString(), addOnId);
            
            var paos = new ProductAddOnService();
            var productAddOnEntity = new ProductAddOn
            {
                ProductAddOnID = int.Parse(productAddonId),
                AddOnID = int.Parse(addOnId),
                ProductID = pid
            };

            paos.Delete(productAddOnEntity);

            var ps = new ProductService();
            var productEntity = ps.GetByProductID(productAddOnEntity.ProductID);
            productEntity.UpdateDte = DateTime.Now;
            ps.Update(productEntity);
        }

        public static void SaveProductTieredPricing(TieredPricing tieredPricing, string productId, string productIdType = null)
        {
            if (!string.IsNullOrEmpty(tieredPricing.TieredPricingId))
            {
                throw new InvalidOperationException("Posting a new product tiered pricing ID is not allowed.");
            }

            tieredPricing.ProductId = GetProductId(productIdType, productId).ToString();

            var pts = new ProductTierService();
            var productTierEntity = DataConverter.ToTieredPricingEntity(tieredPricing, pts);

            pts.Save(productTierEntity);

            var ps = new ProductService();
            var productEntity = ps.GetByProductID(productTierEntity.ProductID);
            productEntity.UpdateDte = DateTime.Now;
            ps.Update(productEntity);
        }

        public static void UpdateProductTieredPricing(TieredPricing tieredPricing, string productId, string productIdType = null)
        {
            tieredPricing.ProductId = GetProductId(productIdType, productId).ToString();

            var pts = new ProductTierService();
            var productTierEntity = DataConverter.ToTieredPricingEntity(tieredPricing, pts);

            if (!pts.Update(productTierEntity))
            {
                throw new InvalidOperationException("Specfied product tiered pricing does not exist.");
            }

            var ps = new ProductService();
            var productEntity = ps.GetByProductID(productTierEntity.ProductID);
            productEntity.UpdateDte = DateTime.Now;
            ps.Update(productEntity);
        }

        private static ProductReviewState GetProductReviewStateFromEntity(IProductReviewState productReviewStateEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (productReviewStateEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var productReviewState = DataConverter.ToProductReviewStateDto(productReviewStateEntity);


            return productReviewState;
        }

        public static ProductReviewState GetProductReviewStateById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var ps = new ProductReviewStateService();
            var productReviewStateEntity = ps.GetByReviewStateID(Convert.ToInt32(id));

            if (productReviewStateEntity == null)
            {
                throw new ResourceNotFoundException("ProductReviewState not found.");
            }

            return GetProductReviewStateFromEntity(productReviewStateEntity, expands, filters);
        }

        public static List<ProductReviewState> GetAllProductReviewStates(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            filters = new List<QueryBuilder.WhereCondition>();

            foreach (var filter in filters.Where(filter => filter.FieldName.Equals(Filters.ModifyDate, StringComparison.InvariantCultureIgnoreCase)))
            {
                filter.FieldName = Filters.UpdateDte;
            }
            var queryBuilder = new QueryBuilder("ProductReviewState", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array
            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDate) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }
            #endregion

            var ph = new ProductHelper();

            var ids = ph.GetProductReviewStateIDsByFilters(query, arrayFilters);


            count = ids.Count;

            var tempList = ids.Select(id => GetProductReviewStateById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }
    }
}

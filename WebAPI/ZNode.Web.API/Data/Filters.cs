﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RogansShoes.Web.API.Data
{
    public class Filters
    {
        public const string AccountId = "accountid";
        public const string CatalogId = "catalogid";
        public const string CategoryId = "categoryid";
        public const string Company = "company";
        public const string CreateDate = "createdate";
        public const string CreateDte = "createdte";
        public const string FirstName = "firstname";
        public const string LastName = "lastname";
        public const string LocaleId = "localeid";
        public const string Phone = "phone";
        public const string PortalId = "portalid";
        public const string PostalCode = "postalcode";
        public const string ProductId = "productid";
        public const string Sku = "sku";
        public const string Status = "status";
        public const string StoreName = "storename";
        public const string TrackingNumber = "trackingnumber";
        public const string UpdateDate = "updatedate";
        public const string UpdateDte = "updatedte";
        public const string ModifyDate = "modifydate";
        public const string Modified = "modified";
        public const string Created = "created";
        public const string LastUpdatedDate = "lastupdateddate";
        public const string CreatedDate = "createddate";
    }
}
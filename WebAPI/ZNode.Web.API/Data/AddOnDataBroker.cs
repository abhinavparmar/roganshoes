﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using AddOn = RogansShoes.Web.API.Models.AddOn;
using AddOnValue = RogansShoes.Web.API.Models.AddOnValue;

namespace RogansShoes.Web.API.Data
{
    public static class AddOnDataBroker
    {
        private static AddOn GetAddOnFromEntity(IAddOn addOnEntity)
        {
            var addOn = DataConverter.ToAddOnDto(addOnEntity);

            addOn.AddOnValues = new Collection<AddOnValue>();

            AddOnValueService aovs = new AddOnValueService();


            foreach (var addOnValueEntity in aovs.GetByAddOnID(addOnEntity.AddOnID))
            {
                addOn.AddOnValues.Add(DataConverter.ToAddOnValueDto(addOnValueEntity));
            }

            return addOn;
        }

        private static AddOnValue GetAddOnValueFromEntity(IAddOnValue addOnValueEntity)
        {
            return DataConverter.ToAddOnValueDto(addOnValueEntity);
        }
        
        private static int GetAddOnId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var ah = new AddOnHelper();
                var addOnId = ah.GetAddOnIDByExternalID(id);
                return Convert.ToInt32(addOnId);
            }
            return Convert.ToInt32(id);
        }

        private static int GetAddOnId(string idType, AddOn addOn)
        {

            if (idType == IdTypes.External)
            {
                return GetAddOnId(idType, addOn.ExternalAPIID);
            }
            return Convert.ToInt32(addOn.AddOnId);
        }

        private static int GetAddOnValueId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var ah = new AddOnHelper();
                var addOnId = ah.GetAddOnValueIDByExternalID(id);
                return Convert.ToInt32(addOnId);
            }
            return Convert.ToInt32(id);
        }

        private static int GetAddOnValueId(string idType, AddOnValue addonValue)
        {
            if (idType == IdTypes.External)
            {
                return GetAddOnValueId(idType, addonValue.ExternalId);
            }
            return Convert.ToInt32(addonValue.AddOnValueId);
        }


        public static AddOn GetAddOnById(string id, string idType = null)
        {
            var aos = new AddOnService();

            return GetAddOnFromEntity(aos.GetByAddOnID(GetAddOnId(idType, id)));
        }

        public static AddOnValue GetAddOnValueById(string id, string idType = null)
        {
            var aovs = new AddOnValueService();

            return GetAddOnValueFromEntity(aovs.GetByAddOnValueID(GetAddOnValueId(idType, id)));
        }

        public static AddOn GetAddOnByAddOn(AddOn addOn, string idType = null)
        {
            var aos = new AddOnService();

            return GetAddOnFromEntity(aos.GetByAddOnID(GetAddOnId(idType, addOn)));
        }

        public static AddOnValue GetAddOnValueByAddOnValue(AddOnValue addOnValue, string idType = null)
        {
            var aovs = new AddOnValueService();

            return GetAddOnValueFromEntity(aovs.GetByAddOnValueID(GetAddOnValueId(idType, addOnValue)));
        }

        public static List<AddOn> GetAllAddOns(int pageSize, int page, out int count, IList<string> expands,
                                                   List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var aoh = new AddOnHelper();

            filters = InitializeFilters(filters);

            foreach (var filter in filters)
            {
                if (filter.FieldName.Equals(Filters.ModifyDate, StringComparison.InvariantCultureIgnoreCase))
                    filter.FieldName = Filters.UpdateDte;
                if (filter.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
                    filter.FieldName = Filters.CreateDte;
            }

            var queryBuilder = new QueryBuilder("AddOn", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = aoh.GetAddOnIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetAddOnById(id, null)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static List<AddOnValue> GetAllAddOnValues(int pageSize, int page, out int count, IList<string> expands,
                                                   List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var aoh = new AddOnHelper();

            filters = InitializeFilters(filters);

            foreach (var filter in filters)
            {
                if (filter.FieldName.Equals(Filters.ModifyDate, StringComparison.InvariantCultureIgnoreCase))
                    filter.FieldName = Filters.UpdateDte;
                if (filter.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
                    filter.FieldName = Filters.CreateDte;
            }

            var queryBuilder = new QueryBuilder("AddOnValue", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = aoh.GetAddOnValueIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetAddOnValueById(id,null)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();

            return list;
        }

        public static string SaveAddOn(AddOn addOn)
        {
            if (!String.IsNullOrEmpty(addOn.AddOnId))
            {
                throw new InvalidOperationException("Posting a new add on ID is not allowed.");
            }

            var aos = new AddOnService();
            var addOnEntity = DataConverter.ToAddOnEntity(addOn, aos);

            //Defaulting values that are no longer used but still remain as required in current table schema
            addOnEntity.ProductID = 0;
            addOnEntity.LocaleId = 43;

            //Default values that are not used by Web API
            addOnEntity.DisplayOrder = 1;
            addOnEntity.DisplayType = "DropDownList";

            var result = aos.Save(addOnEntity);
            return result.AddOnID.ToString();
        }

        public static string SaveAddOnValue(AddOnValue addOnValue)
        {
            if (!String.IsNullOrEmpty(addOnValue.AddOnValueId))
            {
                throw new InvalidOperationException("Posting a new AddOnValue ID is not allowed.");
            }

            var aovs = new AddOnValueService();
            var addOnValueEntity = DataConverter.ToAddOnValueEntity(addOnValue, aovs);
            //Defaulting unused columns may be included in future release
            addOnValueEntity.RecurringBillingInd = false;
            addOnValueEntity.RecurringBillingInstallmentInd = false;

            var result = aovs.Save(addOnValueEntity);
            return result.AddOnValueID.ToString();
        }

        public static void UpdateAddOn(AddOn addOn, string idType = null)
        {
            addOn.AddOnId = GetAddOnId(idType, addOn).ToString();

            var aos = new AddOnService();
            var addOnEntity = DataConverter.ToAddOnEntity(addOn, aos);

            if (!aos.Update(addOnEntity))
            {
                throw new InvalidOperationException("Specified add on does not exist.");
            }
        }

        public static void UpdateAddOnValue(AddOnValue addOnValue, string idType = null)
        {
            addOnValue.AddOnValueId = GetAddOnValueId(idType, addOnValue).ToString();

            var aovs = new AddOnValueService();
            var addOnValueEntity = DataConverter.ToAddOnValueEntity(addOnValue, aovs);

            if (!aovs.Update(addOnValueEntity))
            {
                throw new InvalidOperationException("Specified AddOnValue does not exist.");
            }

        }

        public static void DeleteAddOnById(string id, string idType = null)
        {
            var aos = new AddOnService();
            var addOnEntity = aos.GetByAddOnID(GetAddOnId(idType, id));

            if (!aos.Delete(addOnEntity))
            {
                throw new InvalidOperationException("Specified add on does not exist.");
            }
        }

        public static void DeleteAddOnValueById(string id, string idType = null)
        {
            var aovs = new AddOnValueService();
            var addOnValueEntity = aovs.GetByAddOnValueID(GetAddOnId(idType, id));

            if (!aovs.Delete(addOnValueEntity))
            {
                throw new InvalidOperationException("Specified add on does not exist.");
            }
        }
    }
}
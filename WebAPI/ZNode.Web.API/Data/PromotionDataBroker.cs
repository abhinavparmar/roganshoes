﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using DiscountType = RogansShoes.Web.API.Models.DiscountType;
using Promotion = RogansShoes.Web.API.Models.Promotion;
using ZNode.Libraries.Framework.Business;
using Utility = RogansShoes.Web.API.Support.Utility;

namespace RogansShoes.Web.API.Data
{
    public static class PromotionDataBroker
    {
        public static void DeletePromotionById(string id, string idType = null)
        {
            var ps = new PromotionService();
            var promotionEntity = ps.GetByPromotionID(GetPromotionId(idType, id));

            if (!ps.Delete(promotionEntity))
            {
                throw new InvalidOperationException("Specified Promotion does not exist.");
            }
        }

        public static List<Promotion> GetAllPromotions(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var ph = new PromotionHelper();

            filters = InitializeFilters(filters);

            var queryBuilder = new QueryBuilder("Promotion", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = ph.GetPromotionIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetPromotionById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static Promotion GetPromotionById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var pcs = new PromotionService();
            var promotionEntity = pcs.GetByPromotionID(GetPromotionId(idType, id));

            if (promotionEntity == null)
            {
                throw new ResourceNotFoundException("Promotion not found.");
            }

            return GetPromotionFromEntity(promotionEntity, expands, filters);
        }

        public static Promotion GetPromotionByPromotion(Promotion promotion, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var ps = new PromotionService();
            var promotionEntity = ps.GetByPromotionID(GetPromotionId(idType, promotion));

            if (promotionEntity == null)
            {
                throw new ResourceNotFoundException("Promotion not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetPromotionFromEntity(promotionEntity, expands, filters);
        }

        private static Promotion GetPromotionFromEntity(IPromotion promotionEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (promotionEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var promotion = DataConverter.ToPromotionDto(promotionEntity);

            if (expands == null)
            {
                expands = new List<string>();
            }

            return promotion;
        }

        private static int GetPromotionId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var ph = new PromotionHelper();
                var promotionId = ph.GetPromotionIDByExternalID(id);
                return int.Parse(promotionId);
            }
            return int.Parse(id);
        }

        private static int GetPromotionId(string idType, Promotion promotion)
        {
            if (idType == IdTypes.External)
            {
                return GetPromotionId(idType, promotion.ExternalId);
            }
            return int.Parse(promotion.PromotionId);
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            return list;
        }

        public static string SavePromotion(Promotion promotion)
        {
            if (!String.IsNullOrEmpty(promotion.PromotionId))
            {
                throw new InvalidOperationException("Posting a new Promotion ID is not allowed.");
            }
            if (!String.IsNullOrEmpty(promotion.ExternalId))
            {
                var ph = new PromotionHelper();
                var temp = ph.GetPromotionIDByExternalID(promotion.ExternalId);
                if (temp != null)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var ps = new PromotionService();
            var promotionEntity = DataConverter.ToPromotionEntity(promotion, ps);
            var result = ps.Save(promotionEntity);

            return result.PromotionID.ToString();
        }

        public static void UpdatePromotion(Promotion promotion, string idType = null)
        {
            promotion.PromotionId = GetPromotionId(idType, promotion).ToString();

            if (!String.IsNullOrEmpty(promotion.ExternalId))
            {
                var ph = new PromotionHelper();
                var temp = ph.GetPromotionIDByExternalID(promotion.ExternalId);
                if (temp != null && temp != promotion.PromotionId)
                {
                    throw new InvalidOperationException("External ID already exists.");
                }
            }

            var ps = new PromotionService();
            var promotionEntity = DataConverter.ToPromotionEntity(promotion, ps);

            if (!ps.Update(promotionEntity))
            {
                throw new InvalidOperationException("Specfied Promotion does not exist.");
            }
        }

        public static DiscountType GetDiscountTypeById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var dts = new DiscountTypeService();
            var discountTypeEntity = dts.GetByDiscountTypeID(Convert.ToInt32(id));

            if (discountTypeEntity == null)
            {
                throw new ResourceNotFoundException("DiscountType not found.");
            }

            return GetDiscountTypeFromEntity(discountTypeEntity, expands, filters);
        }

        private static DiscountType GetDiscountTypeFromEntity(IDiscountType discountTypeEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (discountTypeEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var discountType = DataConverter.ToDiscountTypeDto(discountTypeEntity);

            if (expands == null)
            {
                expands = new List<string>();
            }

            return discountType;
        }

        public static List<DiscountType> GetAllDiscountTypes(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var ph = new PromotionHelper();

            filters = InitializeFilters(filters);

            var queryBuilder = new QueryBuilder("DiscountType", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = ph.GetDiscountTypeIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetDiscountTypeById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static string SaveDiscountType(DiscountType discountType)
        {
            if (!String.IsNullOrEmpty(discountType.DiscountTypeId))
            {
                throw new InvalidOperationException("Posting a new DiscountType ID is not allowed.");
            }

            var ps = new DiscountTypeService();
            var discountTypeEntity = DataConverter.ToDiscountTypeEntity(discountType, ps);
            var result = ps.Save(discountTypeEntity);

            return result.DiscountTypeID.ToString();
        }

        public static void UpdateDiscountType(DiscountType discountType, string idType = null)
        {
            var ps = new DiscountTypeService();
            var discountTypeEntity = DataConverter.ToDiscountTypeEntity(discountType, ps);

            if (!ps.Update(discountTypeEntity))
            {
                throw new InvalidOperationException("Specfied DiscountType does not exist.");
            }
        }

        public static void DeleteDiscountTypeById(string id, string idType = null)
        {
            var dts = new DiscountTypeService();
            var discountTypeEntity = dts.GetByDiscountTypeID(Convert.ToInt32(id));

            if (!dts.Delete(discountTypeEntity))
            {
                throw new InvalidOperationException("Specified DiscountType does not exist.");
            }
        }

    }
}
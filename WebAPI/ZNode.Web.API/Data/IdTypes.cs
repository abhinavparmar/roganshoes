﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RogansShoes.Web.API.Data
{
    public class IdTypes
    {
        public const string CookieMapping = "cookiemapping";
        public const string External = "external";
        public const string SkuId = "skuid";
        public const string ExternalSkuId = "externalskuid";
    }
}
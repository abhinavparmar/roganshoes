﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RogansShoes.Web.API.Data
{
    public static class PaymentType
    {
        public const string CreditCard = "CreditCard";
        public const string PurchaseOrder = "Purchase Order";
        public const string PaypalExpressCheckout = "Paypal Express Checkout";
        public const string GoogleCheckout = "Google Checkout";
        public const string ChargeOnDelivery = "COD";
        public const string TwoCo = "TwoCo";
    }
}
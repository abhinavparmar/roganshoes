﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Web;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Suppliers;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using Utility = RogansShoes.Web.API.Support.Utility;
using ZNodeShoppingCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart;

namespace RogansShoes.Web.API.Data
{
    public static class OrderSavedCartDataBroker
    {

        public static string SaveOrder(Models.Order order, string savedcartId)
        {
            var zNodeShoppingCart = GetSavedCart(savedcartId);
            if (zNodeShoppingCart != null)
            {
                if (string.IsNullOrEmpty(order.SavedCartId))
                {
                    order.SavedCartId = savedcartId;
                }

                var account = (new AccountService()).GetByAccountID(string.IsNullOrEmpty(order.AccountId) ? 0 : Convert.ToInt32(order.AccountId));

                var userAccount = account == null ? new ZNodeUserAccount() : new ZNodeUserAccount(account);
                if (userAccount.AccountID == 0) userAccount.AddUserAccount();
                zNodeShoppingCart.Payment.BillingAddress = DataConverter.ToAddressEntity(order.BillingInformation,
                                                                                         userAccount.AccountID);
                if (zNodeShoppingCart.PreSubmitOrderProcess())
                {
                    var orderId = 0;
                    var zco = new ZNodeCheckout();
                    var oFulfillment = zco.GetOrderFullfillment(userAccount, zNodeShoppingCart, ZNodeConfigManager.DomainConfig.PortalID);
                    oFulfillment.Email = order.Email;
                    oFulfillment.BillingAddress = DataConverter.ToAddressEntity(order.BillingInformation, userAccount.AccountID);
                    oFulfillment.ShippingAddress = DataConverter.ToAddressEntity(order.ShippingInformation,
                                                                                 userAccount.AccountID);
                    if (String.IsNullOrEmpty(order.PaymentInformation.PaymentType) ||
                        !order.PaymentInformation.PaymentType.Equals("CreditCard",
                                                                     StringComparison.InvariantCultureIgnoreCase))
                    {
                        throw new InvalidOperationException("A Creditcard Payment type must be specified");
                    }

                    TransactionManager tranManager = null;
                    try
                    {
                        tranManager = ConnectionScope.CreateTransaction();
                        orderId = oFulfillment.AddOrderToDatabase(ZNodeOrderState.SUBMITTED);
                        var response = SendAuthorizeNetPayment(oFulfillment, orderId, order.PaymentInformation);
                        if (response.IsSuccess)
                        {
                            tranManager.Commit();
                            var encypt = new ZNodeEncryption();
                            var os = new OrderService();
                            var o = os.GetByOrderID(orderId);
                            o.CardTransactionID = response.TransactionId;
                            o.CardExp = encypt.EncryptData(order.PaymentInformation.CreditCardInformation.CardExp);
                            o.Created = DateTime.Now;
                            o.Modified = DateTime.Now;
                            os.Update(o);
                            PostSubmitOrderProcess(oFulfillment, zNodeShoppingCart, order.SavedCartId);
                        }
                        else
                        {
                            tranManager.Rollback();
                        }
                    }
                    catch (ZNodePaymentException)
                    {
                        if (tranManager != null) tranManager.Rollback();
                        throw new ZNodePaymentException("Error while processing payment");
                    }
                    catch (Exception)
                    {
                        if (tranManager != null) tranManager.Rollback();
                        throw new Exception("Error while processing payment");
                    }
                    return Convert.ToString(orderId);
                }
            }
            return null;
        }

        public static Models.Order GetByOrderId(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.Created, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.Modified, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var os = new OrderService();

            var orderEntity = os.GetByOrderID(GetOrderId(idType, id));

            if (orderEntity == null)
            {
                throw new ResourceNotFoundException("Order not found.");
            }

            return GetOrderFromEntity(orderEntity, expands, filters);
        }

        public static ZNodeShoppingCart GetSavedCart(string savedCartId)
        {
            var scs = new SavedCartService();
            var savedCart = scs.GetBySavedCartID(Convert.ToInt32(savedCartId));
            ZNodeShoppingCart zNodeShoppingCart = null;
            if (savedCart != null)
            {
                var zNodeSavedCart = new ZNodeSavedCart();
                scs.DeepLoad(savedCart);
                zNodeSavedCart.InitializeShoppingCart(savedCart.SavedCartID, out zNodeShoppingCart);
            }
            return zNodeShoppingCart;
        }

        public static List<Models.Order> GetAllOrders(int pageSize, int page, out int count, IList<string> expands, List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var oh = new OrderHelper();

            var initFilters = InitializeFilters(filters);

            foreach (var condition in initFilters)
            {
                if (condition.FieldName.Equals(Filters.UpdateDate, StringComparison.InvariantCultureIgnoreCase))
                {
                    condition.FieldName = Filters.Modified;
                }
                if (condition.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
                {
                    condition.FieldName = Filters.Created;
                }
            }

            var queryBuilder = new QueryBuilder("Orders", initFilters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[initFilters.Count, 2];

            #region Populate array
            for (var i = 0; i < initFilters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (initFilters[i].FieldName.Equals(Filters.Created) ||
                            initFilters[i].FieldName.Equals(Filters.Modified))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + initFilters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = initFilters[i].MatchValue;
                    }
                }
            }
            #endregion

            var ids = oh.GetOrderIDsByFilters(query, arrayFilters);


            count = ids.Count;
            var tempList = ids.Select(id => GetByOrderId(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();

            if (!fieldNames.Contains(Filters.Company))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.Company });
            }
            if (!fieldNames.Contains(Filters.FirstName))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.FirstName });
            }
            if (!fieldNames.Contains(Filters.LastName))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.LastName });
            }
            if (!fieldNames.Contains(Filters.Phone))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.Phone });
            }
            if (!fieldNames.Contains(Filters.PostalCode))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.PostalCode });
            }
            if (!fieldNames.Contains(Filters.PortalId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.PortalId });
            }
            if (!fieldNames.Contains(Filters.StoreName))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.StoreName });
            }
            if (!fieldNames.Contains(Filters.Status))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.Status });
            }
            if (!fieldNames.Contains(Filters.TrackingNumber))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.TrackingNumber });
            }
            if (!fieldNames.Contains(Filters.AccountId))
            {
                list.Add(new QueryBuilder.WhereCondition { FieldName = Filters.AccountId });
            }
            return list;
        }

        public static void UpdateOrder(Models.Order order, string idType = null)
        {
            order.OrderId = Convert.ToString(GetOrderId(idType, order));

            var os = new OrderService();

            var orderEntity = DataConverter.ToOrderEntity(order, os);

            orderEntity.Modified = DateTime.Now;

            if (!os.Update(orderEntity))
            {
                throw new InvalidOperationException("Specified order does not exist.");
            }
        }

        public static Models.Order GetOrderByOrder(Models.Order order, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var os = new OrderService();
            var orderEntity = os.GetByOrderID(GetOrderId(idType, order));

            if (orderEntity == null)
            {
                throw new ResourceNotFoundException("Order not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.Created, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.Modified, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            return GetOrderFromEntity(orderEntity, expands, filters);
        }

        public static void SaveOrderLineItem(Models.OrderLineItem orderLineItem, string orderId, string orderIdType = null)
        {
            if (!string.IsNullOrEmpty(orderLineItem.OrderLineItemId))
            {
                throw new InvalidOperationException("Posting a new order line item ID is not allowed.");
            }

            orderLineItem.OrderId = Convert.ToString(GetOrderId(orderIdType, orderId));

            var olis = new OrderLineItemService();
            var orderLineItemEntity = DataConverter.ToOrderLineItemEntity(orderLineItem, olis);

            var ss = new SKUService();
            var sku = ss.GetBySKUID(int.Parse(orderLineItem.SkuId));

            var ps = new ProductService();
            var product = ps.GetByProductID(sku.ProductID);

            orderLineItemEntity.ProductNum = product.ProductNum;
            orderLineItemEntity.SKU = sku.SKU;

            olis.Save(orderLineItemEntity);

            var os = new OrderService();
            var orderEntity = os.GetByOrderID(orderLineItemEntity.OrderID);
            orderEntity.Modified = DateTime.Now;
            os.Update(orderEntity);
        }

        public static void UpdateOrderLineItem(Models.OrderLineItem orderLineItem, string orderId, string orderIdType = null)
        {
            orderLineItem.OrderId = Convert.ToString(GetOrderId(orderIdType, orderId));

            var olis = new OrderLineItemService();
            var orderLineItemEntity = DataConverter.ToOrderLineItemEntity(orderLineItem, olis);

            var ss = new SKUService();
            var sku = ss.GetBySKUID(int.Parse(orderLineItem.SkuId));

            var ps = new ProductService();
            var product = ps.GetByProductID(sku.ProductID);

            orderLineItemEntity.ProductNum = product.ProductNum;
            orderLineItemEntity.SKU = sku.SKU;

            if (!olis.Update(orderLineItemEntity))
            {
                throw new InvalidOperationException("Specfied order line item does not exist.");
            }

            var os = new OrderService();
            var orderEntity = os.GetByOrderID(orderLineItemEntity.OrderID);
            orderEntity.Modified = DateTime.Now;
            os.Update(orderEntity);
        }

        private static int GetOrderId(string idType, string id)
        {
            return int.Parse(id);
        }

        private static int GetOrderId(string idType, Models.Order order)
        {
            return int.Parse(order.OrderId);
        }

        private static Models.Order GetOrderFromEntity(IOrder orderEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (orderEntity == null)
            {
                return null;
            }

            var order = DataConverter.ToOrderDto(orderEntity);

            var olis = new OrderLineItemService();
            var olirts = new OrderLineItemRelationshipTypeService();
            var sh = new SKUHelper();

            order.OrderLineItems = new Collection<Models.OrderLineItem>(olis.GetByOrderID(orderEntity.OrderID).Where(oli => !oli.OrderLineItemRelationshipTypeID.HasValue
                || olirts.GetByOrderLineItemRelationshipTypeID(oli.OrderLineItemRelationshipTypeID.Value).Name != "Add-Ons").Select(oli =>
                {
                    var orderLineItem = DataConverter.ToOrderLineItemDto(oli);

                    orderLineItem.SkuId = sh.GetSkuIdBySkuProductNum(oli.SKU, oli.ProductNum);
                    if (orderLineItem.SkuId == null)
                    {
                        throw new BadDataException("Invalid SKU or ProductNum on order line item with ID " + oli.OrderLineItemID);
                    }

                    return orderLineItem;
                }).ToList());

            if (expands == null)
            {
                expands = new List<string>();
            }

            if (!expands.Contains(Expands.BillingInformation))
            {
                order.BillingInformation = null;
            }
            if (!expands.Contains(Expands.ShippingInformation))
            {
                order.ShippingInformation = null;
            }

            return order;
        }

        private static void PostSubmitOrderProcess(ZNodeOrderFulfillment orderFulfillment, ZNodeShoppingCart cart, string cartId)
        {
            // TODO: tracking

            //cart.Payment.TransactionID = order.CardTransactionID;
            //cart.Payment.AuthCode = order.CardAuthCode;
            //cart.Payment.SubscriptionID = order.Custom2;
            cart.PostSubmitOrderProcess();

            var supplierOption = new ZNodeSupplierOption();
            supplierOption.SubmitOrder(orderFulfillment, cart);

            var sc = new SavedCartService();
            var savedCart = sc.GetBySavedCartID(Convert.ToInt32(cartId));
            sc.DeepLoad(savedCart);
            var scl = new SavedCartLineItemService();
            foreach (var savedCartLineItem in savedCart.SavedCartLineItemCollection)
            {
                SavedCartDataBroker.DeleteSavedCartLineItemById((savedCartLineItem.SavedCartLineItemID.ToString()));
            }
            sc.Delete(savedCart);
        }

        private static GatewayResponse SendAuthorizeNetPayment(ZNodeOrderFulfillment order, int orderId, PaymentInformation payInfo)
        {
            var ps = new PaymentSettingService();
            var paymentSetting = ps.GetByPaymentTypeID(0)[0];
            var creditCard = new CreditCard
            {
                OrderID = orderId,
                Amount = Math.Round(order.Total, 2),
                SubTotal = Math.Round(order.SubTotal, 2),
                TaxCost = Math.Round(order.SalesTax, 2),
                CardType = payInfo.CreditCardInformation.CardType,
                CardHolderName = payInfo.CreditCardInformation.CardHolderName,
                CreditCardExp = payInfo.CreditCardInformation.CardExp,
                CardSecurityCode = payInfo.CreditCardInformation.CardSecurityCode,
                CardNumber = payInfo.CreditCardInformation.CardNumber
            };

            // Get Gateway Info From Payment Settings
            var gatewayInfo = new GatewayInfo();
            var decrypt = new ZNodeEncryption();
            gatewayInfo.GatewayLoginID = decrypt.DecryptData(paymentSetting.GatewayUsername);
            gatewayInfo.GatewayPassword = decrypt.DecryptData(paymentSetting.GatewayPassword);
            gatewayInfo.TransactionKey = decrypt.DecryptData(paymentSetting.TransactionKey);

            gatewayInfo.Vendor = paymentSetting.Vendor;
            gatewayInfo.Partner = paymentSetting.Partner;
            gatewayInfo.TestMode = paymentSetting.TestMode;
            gatewayInfo.PreAuthorize = paymentSetting.PreAuthorize;
            gatewayInfo.CurrencyCode = ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.CurrencyCode();
            gatewayInfo.Gateway = GatewayType.AUTHORIZE;


            // set billing address
            var gatewayBillingAddress = new ZNode.Libraries.ECommerce.Entities.Address
            {
                City = order.BillingAddress.City,
                CompanyName = order.BillingAddress.CompanyName,
                CountryCode = order.BillingAddress.CountryCode,
                FirstName = order.BillingAddress.FirstName,
                LastName = order.BillingAddress.LastName,
                PhoneNumber = order.BillingAddress.PhoneNumber,
                PostalCode = order.BillingAddress.PostalCode,
                StateCode = order.BillingAddress.StateCode,
                Street1 = order.BillingAddress.Street,
                Street2 = order.BillingAddress.Street1
            };

            // set shipping address
            var gatewayShippingAddress = new ZNode.Libraries.ECommerce.Entities.Address
            {
                City =
                    order.ShippingAddress.
                    City,
                CompanyName =
                    order.ShippingAddress.
                    CompanyName,
                CountryCode =
                    order.ShippingAddress.
                    CountryCode,
                FirstName =
                    order.ShippingAddress.
                    FirstName,
                LastName =
                    order.ShippingAddress.
                    LastName,
                PhoneNumber =
                    order.ShippingAddress.
                    PhoneNumber,
                PostalCode =
                    order.ShippingAddress.
                    PostalCode,
                StateCode =
                    order.ShippingAddress.
                    StateCode,
                Street1 =
                    order.ShippingAddress.Street,
                Street2 =
                    order.ShippingAddress.Street1
            };

            var submitToAuthorizeNet = new GatewayAuthorize();
            var resp = submitToAuthorizeNet.SubmitPayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, creditCard);
            return resp;
        }

    }
}
﻿using System;
using RogansShoes.Web.API.Helper;
using ZNode.Libraries.Framework.Business;

namespace RogansShoes.Web.API.Data
{
    public static class SendAPIEmail
    {
        /// <summary>
        /// Send the Order Status Email
        /// </summary>
        /// <param name="recepientEmail"></param>
        /// <param name="senderEmail"></param>
        /// <param name="bccEmail"></param>
        /// <param name="subject"></param>
        /// <param name="messageText"></param>
        /// <param name="IsBodyHtml"></param>
        public static void SendEmail(string recepientEmail, string senderEmail, string bccEmail, string subject, string messageText, bool IsBodyHtml)
        {
            try
            {
                //ZNodeEmailBase.SendEmail(recepientEmail, senderEmail, bccEmail, subject, messageText, IsBodyHtml);

                if (!string.IsNullOrEmpty(messageText))
                {
                    ZeonEmailBase.SendEmail(recepientEmail, senderEmail, bccEmail, subject, messageText, IsBodyHtml);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Error in sending email. To :" + senderEmail + " Sending From:" + recepientEmail + " bcc added :" + bccEmail + "!!Error while sending the email!!Method Name!!SendEmail!! in ShipDate" + ex.ToString();
                OrderDBHelper.ZeonApiOrderUpdateErrorLogInsert(0, 0, string.Empty, errorMessage);
                throw new Exception("Error in sending email. Error: " + ex.StackTrace);
            }
        }
    }
}
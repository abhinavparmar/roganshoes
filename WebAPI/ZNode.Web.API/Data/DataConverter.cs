﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using Account = RogansShoes.Web.API.Models.Account;
using AccountProfile = RogansShoes.Web.API.Models.AccountProfile;
using Address = RogansShoes.Web.API.Models.Address;
using AddOn = RogansShoes.Web.API.Models.AddOn;
using AddOnValue = RogansShoes.Web.API.Models.AddOnValue;
using AttributeType = RogansShoes.Web.API.Models.AttributeType;
using Catalog = RogansShoes.Web.API.Models.Catalog;
using Category = RogansShoes.Web.API.Models.Category;
using CategoryNode = RogansShoes.Web.API.Models.CategoryNode;
using DiscountType = RogansShoes.Web.API.Models.DiscountType;
using Highlight = RogansShoes.Web.API.Models.Highlight;
using Order = RogansShoes.Web.API.Models.Order;
using OrderLineItem = RogansShoes.Web.API.Models.OrderLineItem;
using OrderState = RogansShoes.Web.API.Models.OrderState;
using Portal = RogansShoes.Web.API.Models.Portal;
using PortalCatalog = RogansShoes.Web.API.Models.PortalCatalog;
using Profile = RogansShoes.Web.API.Models.Profile;
using Product = RogansShoes.Web.API.Models.Product;
using ProductReviewState = RogansShoes.Web.API.Models.ProductReviewState;
using Promotion = RogansShoes.Web.API.Models.Promotion;
using Review = RogansShoes.Web.API.Models.Review;
using SavedCart = RogansShoes.Web.API.Models.SavedCart;
using SavedCartLineItem = RogansShoes.Web.API.Models.SavedCartLineItem;
using Shipping = RogansShoes.Web.API.Models.Shipping;
using ShippingRule = RogansShoes.Web.API.Models.ShippingRule;
using ShippingRuleType = RogansShoes.Web.API.Models.ShippingRuleType;
using ShippingType = RogansShoes.Web.API.Models.ShippingType;
using Supplier = RogansShoes.Web.API.Models.Supplier;
using SupplierType = RogansShoes.Web.API.Models.SupplierType;
using TaxClass = RogansShoes.Web.API.Models.TaxClass;
using TaxRule = RogansShoes.Web.API.Models.TaxRule;
using TaxRuleType = RogansShoes.Web.API.Models.TaxRuleType;
using ZNode;

namespace RogansShoes.Web.API.Data
{
    public class DataConverter
    {
        public static Account ToAccountDto(IAccount accountEntity)
        {
            return accountEntity == null ? null : new Account
            {
                AccountId = Convert.ToString(accountEntity.AccountID),
                Email = accountEntity.Email,
                ExternalId = accountEntity.ExternalAccountNo,
                CreateDate = accountEntity.CreateDte,
                UpdateDate = accountEntity.UpdateDte
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.Account ToAccountEntity(Account account, AccountService accountService)
        {
            var accountId = string.IsNullOrEmpty(account.AccountId) ? 0 : int.Parse(account.AccountId);
            var accountEntity = accountService.GetByAccountID(accountId) ?? new ZNode.Libraries.DataAccess.Entities.Account();
            accountEntity.AccountID = accountId;
            accountEntity.ExternalAccountNo = account.ExternalId;
            accountEntity.Email = account.Email;
            if (account.CreateDate.HasValue)
                accountEntity.CreateDte = account.CreateDate.Value;
            accountEntity.UpdateDte = account.UpdateDate;
            return accountEntity;
        }

        public static AccountAddress ToAccountAddressDto(IAddress addressEntity)
        {
            return addressEntity == null ? null : new AccountAddress
            {
                AddressId = addressEntity.AddressID.ToString(),
                AccountId = addressEntity.AccountID.ToString(),
                Address = new Address
                {
                    FirstName = addressEntity.FirstName,
                    LastName = addressEntity.LastName,
                    CompanyName = addressEntity.CompanyName,
                    StreetAddress1 = addressEntity.Street,
                    StreetAddress2 = addressEntity.Street1,
                    City = addressEntity.City,
                    StateCode = addressEntity.StateCode,
                    PostalCode = addressEntity.PostalCode,
                    CountryCode = addressEntity.CountryCode,
                    PhoneNumber = addressEntity.PhoneNumber
                },
                AddressName = addressEntity.Name,
                IsDefaultBilling = addressEntity.IsDefaultBilling,
                IsDefaultShipping = addressEntity.IsDefaultShipping
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.Address ToAccountAddressEntity(AccountAddress accountAddress, AddressService addressService)
        {
            var addressId = string.IsNullOrEmpty(accountAddress.AddressId) ? 0 : int.Parse(accountAddress.AddressId);
            var addressEntity = addressService.GetByAddressID(addressId) ?? new ZNode.Libraries.DataAccess.Entities.Address();
            addressEntity.AddressID = addressId;
            addressEntity.AccountID = int.Parse(accountAddress.AccountId);
            addressEntity.FirstName = accountAddress.Address.FirstName;
            addressEntity.LastName = accountAddress.Address.LastName;
            addressEntity.CompanyName = accountAddress.Address.CompanyName;
            addressEntity.Street = accountAddress.Address.StreetAddress1;
            addressEntity.Street1 = accountAddress.Address.StreetAddress2;
            addressEntity.City = accountAddress.Address.City;
            addressEntity.StateCode = accountAddress.Address.StateCode;
            addressEntity.PostalCode = accountAddress.Address.PostalCode;
            addressEntity.CountryCode = accountAddress.Address.CountryCode;
            addressEntity.PhoneNumber = accountAddress.Address.PhoneNumber;
            addressEntity.Name = accountAddress.AddressName;
            addressEntity.IsDefaultBilling = accountAddress.IsDefaultBilling;
            addressEntity.IsDefaultShipping = accountAddress.IsDefaultShipping;
            return addressEntity;
        }

        public static ZNode.Libraries.DataAccess.Entities.Address ToAddressEntity(Address address, int accountid)
        {
            return new ZNode.Libraries.DataAccess.Entities.Address
                       {
                           City = address.City,
                           CompanyName = address.CompanyName,
                           AccountID = Convert.ToInt32(accountid),
                           FirstName = address.FirstName,
                           LastName = address.LastName,
                           PhoneNumber = address.PhoneNumber,
                           PostalCode = address.PostalCode,
                           StateCode = address.StateCode,
                           CountryCode = address.CountryCode,
                           Street = address.StreetAddress1,
                           Street1 = address.StreetAddress2
                       };
        }

        public static AccountProfile ToAccountProfileDto(IAccountProfile accountProfileEntity)
        {
            return accountProfileEntity == null ? null : new AccountProfile
            {
                AccountProfileId = accountProfileEntity.AccountProfileID.ToString(),
                AccountId = accountProfileEntity.AccountID.ToString(),
                ProfileId = accountProfileEntity.ProfileID.ToString()
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.AccountProfile ToAccountProfileEntity(AccountProfile accountProfile, AccountProfileService accountProfileService)
        {
            var accountProfileId = string.IsNullOrEmpty(accountProfile.AccountProfileId) ? 0 : int.Parse(accountProfile.AccountProfileId);
            var accountProfileEntity = accountProfileService.GetByAccountProfileID(accountProfileId) ?? new ZNode.Libraries.DataAccess.Entities.AccountProfile();
            accountProfileEntity.AccountProfileID = accountProfileId;
            accountProfileEntity.AccountID = int.Parse(accountProfile.AccountId);
            accountProfileEntity.ProfileID = string.IsNullOrEmpty(accountProfile.ProfileId) ? (int?)null : int.Parse(accountProfile.ProfileId);
            return accountProfileEntity;
        }

        public static Catalog ToCatalogDto(ICatalog catalogEntity)
        {
            return catalogEntity == null ? null : new Catalog
            {
                CatalogId = catalogEntity.CatalogID.ToString(),
                ExternalId = catalogEntity.ExternalID,
                Name = catalogEntity.Name,
                IsActive = catalogEntity.IsActive
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.Catalog ToCatalogEntity(Catalog catalog, CatalogService catalogService)
        {
            var catalogId = string.IsNullOrEmpty(catalog.CatalogId) ? 0 : int.Parse(catalog.CatalogId);
            var catalogEntity = catalogService.GetByCatalogID(catalogId) ?? new ZNode.Libraries.DataAccess.Entities.Catalog();
            catalogEntity.CatalogID = catalogId;
            catalogEntity.ExternalID = catalog.ExternalId;
            catalogEntity.Name = catalog.Name;
            catalogEntity.IsActive = catalog.IsActive;

            return catalogEntity;
        }

        public static Category ToCategoryDto(ICategory categoryEntity)
        {
            return categoryEntity == null ? null : new Category
            {
                CategoryId = categoryEntity.CategoryID.ToString(),
                AlternateDescription = categoryEntity.AlternateDescription,
                Description = categoryEntity.Description,
                DisplayOrder = categoryEntity.DisplayOrder,
                IsVisible = categoryEntity.VisibleInd,
                Name = categoryEntity.Name,
                ShortDescription = categoryEntity.ShortDescription,
                Title = categoryEntity.Title,
                Image = new Image
                {
                    AltTag = categoryEntity.ImageAltTag,
                    Path = categoryEntity.ImageFile
                },
                Seo = new Seo
                {
                    Description = categoryEntity.SEODescription,
                    Keywords = categoryEntity.SEOKeywords,
                    Title = categoryEntity.SEOTitle,
                    Url = categoryEntity.SEOURL
                },
                CreateDate = categoryEntity.CreateDate,
                UpdateDate = categoryEntity.UpdateDate
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.Category ToCategoryEntity(Category category, CategoryService categoryService)
        {
            var categoryId = string.IsNullOrEmpty(category.CategoryId) ? 0 : int.Parse(category.CategoryId);
            var categoryEntity = categoryService.GetByCategoryID(categoryId) ?? new ZNode.Libraries.DataAccess.Entities.Category();
            categoryEntity.CategoryID = categoryId;
            categoryEntity.AlternateDescription = category.AlternateDescription;
            categoryEntity.Description = category.Description;
            categoryEntity.DisplayOrder = category.DisplayOrder;
            categoryEntity.VisibleInd = category.IsVisible;
            categoryEntity.Name = category.Name;
            categoryEntity.ShortDescription = category.ShortDescription;
            categoryEntity.Title = category.Title;
            if (category.Image != null)
            {
                categoryEntity.ImageAltTag = category.Image.AltTag;
                categoryEntity.ImageFile = category.Image.Path;
            }
            if (category.Seo != null)
            {
                categoryEntity.SEODescription = category.Seo.Description;
                categoryEntity.SEOKeywords = category.Seo.Keywords;
                categoryEntity.SEOTitle = category.Seo.Title;
                categoryEntity.SEOURL = category.Seo.Url;
            }
            categoryEntity.CreateDate = category.CreateDate;
            categoryEntity.UpdateDate = category.UpdateDate;
            return categoryEntity;
        }

        public static CategoryNode ToCategoryNodeDto(ICategoryNode categoryNodeEntity)
        {
            return categoryNodeEntity == null ? null : new CategoryNode
            {
                CategoryNodeId = categoryNodeEntity.CategoryNodeID.ToString(),
                CatalogId = categoryNodeEntity.CatalogID.ToString(),
                CategoryId = categoryNodeEntity.CategoryID.ToString(),
                DisplayOrder = categoryNodeEntity.DisplayOrder,
                ParentCategoryNodeId = categoryNodeEntity.ParentCategoryNodeID.ToString()
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.CategoryNode ToCategoryNodeEntity(CategoryNode categoryNode, CategoryNodeService categoryNodeService)
        {
            var categoryNodeId = string.IsNullOrEmpty(categoryNode.CategoryNodeId) ? 0 : int.Parse(categoryNode.CategoryNodeId);
            var categoryNodeEntity = categoryNodeService.GetByCategoryNodeID(categoryNodeId) ?? new ZNode.Libraries.DataAccess.Entities.CategoryNode();
            categoryNodeEntity.CategoryNodeID = categoryNodeId;
            categoryNodeEntity.CatalogID = string.IsNullOrEmpty(categoryNode.CatalogId) ? (int?)null : int.Parse(categoryNode.CatalogId);
            categoryNodeEntity.CategoryID = int.Parse(categoryNode.CategoryId);
            categoryNodeEntity.DisplayOrder = categoryNode.DisplayOrder;
            categoryNodeEntity.ParentCategoryNodeID = string.IsNullOrEmpty(categoryNode.ParentCategoryNodeId) ? (int?)null : int.Parse(categoryNode.ParentCategoryNodeId);
            return categoryNodeEntity;
        }

        public static DiscountType ToDiscountTypeDto(IDiscountType discountTypeEntity)
        {
            return discountTypeEntity == null ? null : new DiscountType
            {
                DiscountTypeId = discountTypeEntity.DiscountTypeID.ToString(),
                ActiveInd = discountTypeEntity.ActiveInd,
                ClassName = discountTypeEntity.ClassName,
                ClassType = discountTypeEntity.ClassType,
                Description = discountTypeEntity.Description,
                Name = discountTypeEntity.Name
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.DiscountType ToDiscountTypeEntity(DiscountType discountType, DiscountTypeService discountTypeService)
        {
            var discountTypeId = string.IsNullOrEmpty(discountType.DiscountTypeId) ? 0 : Convert.ToInt32(discountType.DiscountTypeId);
            var discountTypeEntity = discountTypeService.GetByDiscountTypeID(discountTypeId) ?? new ZNode.Libraries.DataAccess.Entities.DiscountType();
            discountTypeEntity.ActiveInd = discountType.ActiveInd;
            discountType.ClassName = discountType.ClassName;
            discountType.ClassType = discountType.ClassType;
            discountType.Description = discountType.Description;
            discountType.Name = discountType.Name;
            return discountTypeEntity;
        }

        public static Inventory ToInventoryDto(ISKUInventory skuInventoryEntity)
        {
            return skuInventoryEntity == null ? null : new Inventory
            {
                SkuInventoryId = skuInventoryEntity.SKUInventoryID.ToString(),
                Sku = skuInventoryEntity.SKU,
                QuantityOnHand = skuInventoryEntity.QuantityOnHand
            };
        }

        public static Portal ToPortalDto(IPortal portalEntity)
        {
            return portalEntity == null ? null : new Portal
            {
                PortalId = Convert.ToString(portalEntity.PortalID),
                ExternalId = portalEntity.ExternalID,
                AdminEmail = portalEntity.AdminEmail,
                CompanyName = portalEntity.CompanyName,
                CustomerServiceEmail = portalEntity.CustomerServiceEmail,
                CustomerServicePhoneNumber = portalEntity.CustomerServicePhoneNumber,
                DefaultOrderStateId = portalEntity.DefaultOrderStateID.ToString(),
                DefaultReviewStatus = portalEntity.DefaultReviewStatus,
                DefaultProductReviewStateId = portalEntity.DefaultProductReviewStateID.ToString(),
                EnableAddressValidation = portalEntity.EnableAddressValidation,
                EnablePIMS = portalEntity.EnablePIMS,
                InclusiveTax = portalEntity.InclusiveTax,
                LogoPath = portalEntity.LogoPath,
                PersistentCartEnabled = portalEntity.PersistentCartEnabled,
                RequireValidatedAddress = portalEntity.RequireValidatedAddress,
                SalesEmail = portalEntity.SalesEmail,
                SalesPhoneNumber = portalEntity.SalesPhoneNumber,
                StoreName = portalEntity.StoreName,
                UseSSL = portalEntity.UseSSL
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.Portal ToPortalEntity(Portal portal, PortalService portalService)
        {
            var portalId = string.IsNullOrEmpty(portal.PortalId) ? 0 : int.Parse(portal.PortalId);
            var portalEntity = portalService.GetByPortalID(portalId) ?? new ZNode.Libraries.DataAccess.Entities.Portal();
            portalEntity.ExternalID = portal.ExternalId;
            portalEntity.AdminEmail = portal.AdminEmail;
            portalEntity.CompanyName = portal.CompanyName;
            portalEntity.CustomerServiceEmail = portal.CustomerServiceEmail;
            portalEntity.CustomerServicePhoneNumber = portal.CustomerServicePhoneNumber;
            portalEntity.DefaultOrderStateID = ToNullableIntField(portal.DefaultOrderStateId);
            portalEntity.DefaultReviewStatus = portal.DefaultReviewStatus;
            portalEntity.DefaultProductReviewStateID = ToNullableIntField(portal.DefaultProductReviewStateId);
            portalEntity.EnableAddressValidation = portal.EnableAddressValidation;
            portalEntity.EnablePIMS = portal.EnablePIMS;
            portalEntity.InclusiveTax = portal.InclusiveTax;
            portalEntity.LogoPath = portal.LogoPath;
            portalEntity.PersistentCartEnabled = portal.PersistentCartEnabled;
            portalEntity.RequireValidatedAddress = portal.RequireValidatedAddress;
            portalEntity.SalesEmail = portal.SalesEmail;
            portalEntity.SalesPhoneNumber = portal.SalesPhoneNumber;
            portalEntity.StoreName = portal.StoreName;
            portalEntity.UseSSL = portal.UseSSL;
            return portalEntity;
        }

        public static PortalCatalog ToPortalCatalogDto(IPortalCatalog portalCatalogEntity)
        {
            return portalCatalogEntity == null ? null : new PortalCatalog
            {
                PortalCatalogId = portalCatalogEntity.PortalCatalogID.ToString(),
                PortalId = Convert.ToString(portalCatalogEntity.PortalID),
                CatalogId = portalCatalogEntity.CatalogID.ToString(),
                CSS = portalCatalogEntity.CSS,
                Theme = portalCatalogEntity.Theme
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.PortalCatalog ToPortalCatalogEntity(PortalCatalog portalCatalog, PortalCatalogService portalCatalogService)
        {
            var portalCatalogId = string.IsNullOrEmpty(portalCatalog.PortalCatalogId) ? 0 : int.Parse(portalCatalog.PortalCatalogId);
            var portalCatalogEntity = portalCatalogService.GetByPortalCatalogID(portalCatalogId) ?? new ZNode.Libraries.DataAccess.Entities.PortalCatalog();
            portalCatalogEntity.PortalID = Convert.ToInt32(portalCatalog.PortalId);
            portalCatalogEntity.CatalogID = Convert.ToInt32(portalCatalog.CatalogId);
            portalCatalogEntity.CSS = portalCatalog.CSS;
            portalCatalogEntity.Theme = portalCatalog.Theme;
            return portalCatalogEntity;
        }

        public static Profile ToProfileDto(IProfile profileEntity)
        {
            return profileEntity == null ? null : new Profile
            {
                ProfileId = profileEntity.ProfileID.ToString(),
                DefaultExternalAccountNo = profileEntity.DefaultExternalAccountNo,
                EmailList = profileEntity.EmailList,
                Name = profileEntity.Name,
                ShowOnPartnerSignup = profileEntity.ShowOnPartnerSignup,
                ShowPricing = profileEntity.ShowPricing,
                TaxClassId = profileEntity.TaxClassID.ToString(),
                TaxExempt = profileEntity.TaxExempt,
                Weighting = profileEntity.Weighting
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.Profile ToProfileEntity(Profile profile, ProfileService profileService)
        {
            var profileId = string.IsNullOrEmpty(profile.ProfileId) ? 0 : Convert.ToInt32(profile.ProfileId);
            var profileEntity = profileService.GetByProfileID(profileId) ?? new ZNode.Libraries.DataAccess.Entities.Profile();
            profileEntity.DefaultExternalAccountNo = profile.DefaultExternalAccountNo;
            profileEntity.EmailList = profile.EmailList;
            profileEntity.Name = profile.Name;
            profileEntity.ShowOnPartnerSignup = profile.ShowOnPartnerSignup;
            profileEntity.ShowPricing = profile.ShowPricing;
            profileEntity.TaxClassID = ToNullableIntField(profile.TaxClassId);
            profileEntity.TaxExempt = profile.TaxExempt;
            profileEntity.Weighting = profile.Weighting;
            return profileEntity;
        }

        public static Product ToProductDto(IProduct productEntity)
        {
            return productEntity == null ? null : new Product
            {
                ProductId = productEntity.ProductID.ToString(),
                Description = productEntity.Description,
                Image = new Image
                {
                    AltTag = productEntity.ImageAltTag,
                    Path = productEntity.ImageFile
                },
                IsActive = productEntity.ActiveInd,
                Keywords = productEntity.Keywords,
                Name = productEntity.Name,
                Order = productEntity.DisplayOrder,
                Pricing = new Pricing
                {
                    IsCallForPricing = productEntity.CallForPricing,
                    Retail = productEntity.RetailPrice,
                    Sale = productEntity.SalePrice,
                    Wholesale = productEntity.WholesalePrice
                },
                ProductNumber = productEntity.ProductNum,
                Seo = new Seo
                {
                    Description = productEntity.SEODescription,
                    Keywords = productEntity.SEOKeywords,
                    Title = productEntity.SEOTitle,
                    Url = productEntity.SEOURL
                },
                ShortDescription = productEntity.ShortDescription,
                CreateDate = productEntity.CreateDate,
                UpdateDate = productEntity.UpdateDte,
                ExternalId = productEntity.ExternalID
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.Product ToProductEntity(Product product, ProductService productService)
        {
            var productId = string.IsNullOrEmpty(product.ProductId) ? 0 : int.Parse(product.ProductId);
            var productEntity = productService.GetByProductID(productId) ?? new ZNode.Libraries.DataAccess.Entities.Product();
            productEntity.ProductID = productId;
            productEntity.Description = product.Description;
            if (product.Image != null)
            {
                productEntity.ImageAltTag = product.Image.AltTag;
                productEntity.ImageFile = product.Image.Path;
            }
            productEntity.ActiveInd = product.IsActive;
            productEntity.Keywords = product.Keywords;
            productEntity.Name = product.Name;
            productEntity.DisplayOrder = product.Order;
            if (product.Pricing != null)
            {
                productEntity.CallForPricing = product.Pricing.IsCallForPricing;
                productEntity.RetailPrice = product.Pricing.Retail;
                productEntity.SalePrice = product.Pricing.Sale;
                productEntity.WholesalePrice = product.Pricing.Wholesale;
            }
            productEntity.ProductNum = product.ProductNumber;
            if (product.Seo != null)
            {
                productEntity.SEODescription = product.Seo.Description;
                productEntity.SEOKeywords = product.Seo.Keywords;
                productEntity.SEOTitle = product.Seo.Title;
                productEntity.SEOURL = product.Seo.Url;
            }
            productEntity.ShortDescription = product.ShortDescription;
            productEntity.CreateDate = product.CreateDate;
            productEntity.UpdateDte = product.UpdateDate;
            productEntity.ExternalID = product.ExternalId;
            return productEntity;
        }

        public static Shipping ToShippingDto(IShipping shippingEntity)
        {
            return shippingEntity == null ? null : new Shipping
            {
                ShippingId = shippingEntity.ShippingID.ToString(),
                ExternalId = shippingEntity.ExternalID,
                ActiveInd = shippingEntity.ActiveInd,
                Description = shippingEntity.Description,
                DestinationCountryCode = shippingEntity.DestinationCountryCode,
                HandlingCharge = shippingEntity.HandlingCharge,
                ProfileId = shippingEntity.ProfileID.ToString(),
                ShippingCode = shippingEntity.ShippingCode,
                ShippingTypeId = shippingEntity.ShippingTypeID.ToString()
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.Shipping ToShippingEntity(Shipping shipping, ShippingService shippingService)
        {
            var shippingId = string.IsNullOrEmpty(shipping.ShippingId) ? 0 : Convert.ToInt32(shipping.ShippingId);
            var shippingEntity = shippingService.GetByShippingID(shippingId) ?? new ZNode.Libraries.DataAccess.Entities.Shipping();
            shippingEntity.ActiveInd = shipping.ActiveInd;
            shippingEntity.Description = shipping.Description;
            shippingEntity.DestinationCountryCode = shipping.DestinationCountryCode;
            shippingEntity.ExternalID = shipping.ExternalId;
            shippingEntity.HandlingCharge = shipping.HandlingCharge;
            shippingEntity.ProfileID = ToNullableIntField(shipping.ProfileId);
            shippingEntity.ShippingCode = shipping.ShippingCode;
            shippingEntity.ShippingTypeID = Convert.ToInt32(shipping.ShippingTypeId);
            return shippingEntity;
        }

        public static ShippingRule ToShippingRuleDto(IShippingRule shippingRuleEntity)
        {
            return shippingRuleEntity == null ? null : new ShippingRule
            {
                ShippingRuleId = shippingRuleEntity.ShippingRuleID.ToString(),
                ExternalId = shippingRuleEntity.ExternalID,
                ShippingId = shippingRuleEntity.ShippingID.ToString(),
                ShippingRuleTypeId = shippingRuleEntity.ShippingRuleTypeID.ToString(),
                BaseCost = shippingRuleEntity.BaseCost,
                ClassName = shippingRuleEntity.ClassName,
                LowerLimit = shippingRuleEntity.LowerLimit,
                PerItemCost = shippingRuleEntity.PerItemCost,
                UpperLimit = shippingRuleEntity.UpperLimit
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.ShippingRule ToShippingRuleEntity(ShippingRule shippingRule, ShippingRuleService shippingRuleService)
        {
            var shippingRuleId = string.IsNullOrEmpty(shippingRule.ShippingRuleId) ? 0 : Convert.ToInt32(shippingRule.ShippingRuleId);
            var shippingRuleEntity = shippingRuleService.GetByShippingRuleID(shippingRuleId) ?? new ZNode.Libraries.DataAccess.Entities.ShippingRule();
            shippingRuleEntity.ExternalID = shippingRule.ExternalId;
            shippingRuleEntity.ShippingID = Convert.ToInt32(shippingRule.ShippingId);
            shippingRuleEntity.ShippingRuleTypeID = Convert.ToInt32(shippingRule.ShippingRuleTypeId);
            shippingRuleEntity.BaseCost = shippingRule.BaseCost;
            shippingRuleEntity.ClassName = shippingRule.ClassName;
            shippingRuleEntity.LowerLimit = shippingRule.LowerLimit;
            shippingRuleEntity.PerItemCost = shippingRule.PerItemCost;
            shippingRuleEntity.UpperLimit = shippingRule.UpperLimit;
            return shippingRuleEntity;
        }

        public static ShippingRuleType ToShippingRuleTypeDto(IShippingRuleType shippingRuleTypeEntity)
        {
            return shippingRuleTypeEntity == null ? null : new ShippingRuleType
            {
                ShippingRuleTypeId = shippingRuleTypeEntity.ShippingRuleTypeID.ToString(),
                ActiveInd = shippingRuleTypeEntity.ActiveInd,
                ClassName = shippingRuleTypeEntity.ClassName,
                Description = shippingRuleTypeEntity.Description,
                Name = shippingRuleTypeEntity.Name
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.ShippingRuleType ToShippingRuleTypeEntity(ShippingRuleType shippingRuleType, ShippingRuleTypeService shippingRuleTypeService)
        {
            var shippingRuleTypeId = string.IsNullOrEmpty(shippingRuleType.ShippingRuleTypeId) ? 0 : Convert.ToInt32(shippingRuleType.ShippingRuleTypeId);
            var shippingRuleTypeEntity = shippingRuleTypeService.GetByShippingRuleTypeID(shippingRuleTypeId) ?? new ZNode.Libraries.DataAccess.Entities.ShippingRuleType();
            shippingRuleTypeEntity.ActiveInd = shippingRuleType.ActiveInd;
            shippingRuleTypeEntity.ClassName = shippingRuleType.ClassName;
            shippingRuleTypeEntity.Description = shippingRuleType.Description;
            shippingRuleTypeEntity.Name = shippingRuleType.Name;
            return shippingRuleTypeEntity;
        }

        public static ShippingType ToShippingTypeDto(IShippingType shippingTypeEntity)
        {
            return shippingTypeEntity == null ? null : new ShippingType
            {
                ShippingTypeId = shippingTypeEntity.ShippingTypeID.ToString(),
                ActiveInd = shippingTypeEntity.IsActive,
                ClassName = shippingTypeEntity.ClassName,
                Description = shippingTypeEntity.Description,
                Name = shippingTypeEntity.Name
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.ShippingType ToShippingTypeEntity(ShippingType shippingType, ShippingTypeService shippingTypeService)
        {
            var shippingTypeId = string.IsNullOrEmpty(shippingType.ShippingTypeId) ? 0 : Convert.ToInt32(shippingType.ShippingTypeId);
            var shippingTypeEntity = shippingTypeService.GetByShippingTypeID(shippingTypeId) ?? new ZNode.Libraries.DataAccess.Entities.ShippingType();
            shippingTypeEntity.IsActive = shippingType.ActiveInd;
            shippingTypeEntity.ClassName = shippingType.ClassName;
            shippingTypeEntity.Description = shippingType.Description;
            shippingTypeEntity.Name = shippingType.Name;
            return shippingTypeEntity;
        }


        public static ZNodeProduct ToZnodeProduct(Product product)
        {
            ZNodeProduct znodeProduct = new ZNodeProduct();
            znodeProduct.Name = product.Name;
            //znodeProduct.PortalID = product.PortalId;
            znodeProduct.ProductID = Convert.ToInt32(product.ProductId);
            znodeProduct.ProductNum = product.ProductNumber;
            //znodeProduct.ProductTypeID = Convert.ToInt32(product.ProductTypeId);
            //znodeProduct.ExpirationPeriod = product.ExpirationPeriod;
            //znodeProduct.ExpirationFrequency = product.ExpirationFrequency;
            //znodeProduct.ImageFile = product._ImageFile;
            znodeProduct.IsActive = product.IsActive;
            //znodeProduct.ManufacturerID = product.ManufacturerID;
            // znodeProduct.ManufacturerPartNum = product.ManufacturerPartNum;
            // znodeProduct.MaxQty = product.MaxQty;
            //znodeProduct.MinQty = product.MinQty;
            znodeProduct.ShortDescription = product.ShortDescription;
            znodeProduct.SKU = product.SKU;
            //znodeProduct.addOnDescription = product._addOnDescription;
            //znodeProduct.DisplayOrder = product.DisplayOrder;
            //znodeProduct.downloadLink = product._downloadLink;

            SKUService skuService = new SKUService();

            znodeProduct.SelectedSKU = DataConverter.ToZnodeSKU(skuService.GetBySKU(product.SKU).FirstOrDefault());

            List<ZNode.Libraries.DataAccess.Entities.AddOn> addOnList = new List<ZNode.Libraries.DataAccess.Entities.AddOn>();
            if (product.SelectedAddons != null)
            {
                foreach (var addOn in product.SelectedAddons)
                {
                    znodeProduct.SelectedAddOns.AddOnCollection.Add(DataConverter.ToZnodeAddOnEntity(addOn));
                }
            }

            // Inventory Settings
            //this._QuantityOnHand = product._QuantityOnHand;
            //this._AllowBackOrder = product._AllowBackOrder;
            //this._TrackInventoryInd = product._TrackInventoryInd;
            //this._BackOrderMsg = product._BackOrderMsg;
            //this._CallMessage = product._CallMessage;
            //this._AffiliateUrl = product._AffiliateUrl;

            // Shipping & tax
            //this._shippingCost = product._shippingCost;
            //this._ShippingRuleTypeID = product._ShippingRuleTypeID;
            //this._shipSeparately = product._shipSeparately;
            //this.shoppingCartQuantity = product.shoppingCartQuantity;
            //this._freeShippingInd = product._freeShippingInd;
            //this._TaxClassID = product._TaxClassID;
            //this._ShippingRate = product.ShippingRate;

            // Product pricing
            znodeProduct.RetailPrice = product.Pricing.Retail ?? Convert.ToDecimal(0.00);
            znodeProduct.SalePrice = product.Pricing.Sale ?? Convert.ToDecimal(0.00);
            znodeProduct.WholesalePrice = product.Pricing.Wholesale ?? Convert.ToDecimal(0.00);

            // Product Dimensions
            //this._height = product._height;
            //this._length = product._length;
            //this._width = product._width;
            //this._Weight = product._Weight;

            // Custom Properties
            //this._Custom1 = product._Custom1;
            //this._Custom2 = product._Custom2;
            //this._Custom3 = product._Custom3;
            //this._ImageAltTag = product._ImageAltTag;

            // Customer Review details
            //this._reviewRating = product._reviewRating;
            //this._totalReviews = product._totalReviews;

            // Recurring Billing
            //this._RecurringBillingInd = product._RecurringBillingInd;
            //this._RecurringBillingTotalCycles = product._RecurringBillingTotalCycles;
            //this._RecurringBillingPeriod = product._RecurringBillingPeriod;
            //this._RecurringBillingFrequency = product._RecurringBillingFrequency;
            //this._RecurringBillingInstallmentInd = product._RecurringBillingInstallmentInd;
            //this._RecurringBillingInitialAmount = product._RecurringBillingInitialAmount;

            // Supplier Detail
            //this._SupplierID = product._SupplierID;

            // Bundle Product Collection
            //this.ZNodeBundleProductCollection = product.ZNodeBundleProductCollection;

            //this.isPromotionApplied = product.IsPromotionApplied;

            //this._skuProfile = product._skuProfile;

            return znodeProduct;
        }

        public static Promotion ToPromotionDto(IPromotion promotionEntity)
        {
            return promotionEntity == null ? null : new Promotion
            {
                PromotionId = Convert.ToString(promotionEntity.PromotionID),
                //ExternalId = promotionEntity.ExternalID,
                ProfileId = promotionEntity.ProfileID.ToString(),
                AccountId = promotionEntity.AccountID.ToString(),
                ProductId = promotionEntity.ProductID.ToString(),
                AddOnValueId = promotionEntity.AddOnValueID.ToString(),
                SKUId = promotionEntity.SKUID.ToString(),
                CouponCode = promotionEntity.CouponCode,
                CouponInd = promotionEntity.CouponInd,
                CouponMessage = promotionEntity.PromotionMessage,
                CouponQuantityAvailable = promotionEntity.CouponQuantityAvailable,
                Custom1 = promotionEntity.Custom1,
                Custom2 = promotionEntity.Custom2,
                Custom3 = promotionEntity.Custom3,
                Description = promotionEntity.Description,
                Discount = promotionEntity.Discount,
                DiscountTypeId = promotionEntity.DiscountTypeID.ToString(),
                EnableCouponUrl = promotionEntity.EnableCouponUrl,
                EndDate = promotionEntity.EndDate,
                Name = promotionEntity.Name,
                OrderMinimum = promotionEntity.OrderMinimum,
                PromoCode = promotionEntity.PromoCode,
                PromotionProductId = promotionEntity.PromotionProductID.ToString(),
                PromotionProductQty = promotionEntity.PromotionProductQty,
                QuantityMinimum = promotionEntity.QuantityMinimum,
                StartDate = promotionEntity.StartDate
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.Promotion ToPromotionEntity(Promotion promotion, PromotionService promotionService)
        {
            var promotionId = string.IsNullOrEmpty(promotion.PromotionId) ? 0 : int.Parse(promotion.PromotionId);
            var promotionEntity = promotionService.GetByPromotionID(promotionId) ?? new ZNode.Libraries.DataAccess.Entities.Promotion();
            //promotionEntity.ExternalID = promotion.ExternalId;
            promotionEntity.ProfileID = ToNullableIntField(promotion.ProfileId);
            promotionEntity.AccountID = ToNullableIntField(promotion.AccountId);
            promotionEntity.ProductID = ToNullableIntField(promotion.ProductId);
            promotionEntity.AddOnValueID = ToNullableIntField(promotion.AddOnValueId);
            promotionEntity.SKUID = ToNullableIntField(promotion.SKUId);
            promotionEntity.CouponInd = promotion.CouponInd;
            if (promotion.CouponInd)
            {
                promotionEntity.CouponCode = promotion.CouponCode;
                promotionEntity.CouponQuantityAvailable = promotion.CouponQuantityAvailable;
                promotionEntity.EnableCouponUrl = promotion.EnableCouponUrl;
                promotionEntity.PromotionMessage = promotion.CouponMessage;
            }
            promotionEntity.Custom1 = promotion.Custom1;
            promotionEntity.Custom2 = promotion.Custom2;
            promotionEntity.Custom3 = promotion.Custom3;
            promotionEntity.Description = promotion.Description;
            promotionEntity.Discount = promotion.Discount;
            promotionEntity.DiscountTypeID = Convert.ToInt32(promotion.DiscountTypeId);
            promotionEntity.DisplayOrder = 0;
            promotionEntity.EndDate = promotion.EndDate;
            promotionEntity.Name = promotion.Name;
            promotionEntity.OrderMinimum = promotion.OrderMinimum;
            promotionEntity.PromoCode = promotion.PromoCode;
            promotionEntity.PromotionProductID = Convert.ToInt32(promotion.PromotionProductId);
            promotionEntity.PromotionProductQty = promotion.PromotionProductQty;
            promotionEntity.QuantityMinimum = promotion.QuantityMinimum;
            promotionEntity.StartDate = promotion.StartDate;
            return promotionEntity;
        }

        public static SavedCart ToSavedCartDto(ISavedCart savedCartEntity)
        {
            return savedCartEntity == null ? null : new SavedCart
            {
                SavedCartId = savedCartEntity.SavedCartID.ToString(),
                CookieMappingId = savedCartEntity.CookieMappingID.ToString(),
                CreateDate = savedCartEntity.CreatedDate,
                UpdateDate = savedCartEntity.LastUpdatedDate
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.SavedCart ToSavedCartEntity(SavedCart savedCart, SavedCartService savedCartService)
        {
            var savedCartId = string.IsNullOrEmpty(savedCart.SavedCartId) ? 0 : int.Parse(savedCart.SavedCartId);
            var savedCartEntity = savedCartService.GetBySavedCartID(savedCartId) ?? new ZNode.Libraries.DataAccess.Entities.SavedCart();
            savedCartEntity.SavedCartID = savedCartId;
            savedCartEntity.CookieMappingID = int.Parse(savedCart.CookieMappingId);
            savedCartEntity.CreatedDate = savedCart.CreateDate;
            savedCartEntity.LastUpdatedDate = savedCart.UpdateDate;
            return savedCartEntity;
        }

        public static SavedCartLineItem ToSavedCartLineItemDto(ISavedCartLineItem savedCartLineItemEntity)
        {
            return savedCartLineItemEntity == null ? null : new SavedCartLineItem
            {
                SavedCartLineItemId = savedCartLineItemEntity.SavedCartLineItemID.ToString(),
                SavedCartId = savedCartLineItemEntity.SavedCartID.ToString(),
                OrderLineItemRelationshipTypeId = savedCartLineItemEntity.OrderLineItemRelationshipTypeID.ToString(),
                ParentLineItem = savedCartLineItemEntity.ParentSavedCartLineItemID.ToString(),
                Quantity = savedCartLineItemEntity.Quantity,
                SkuId = savedCartLineItemEntity.SKUID.ToString()
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.SavedCartLineItem ToSavedCartLineItemEntity(SavedCartLineItem savedCartLineItem, SavedCartLineItemService savedCartLineItemService)
        {
            var savedCartLineItemId = string.IsNullOrEmpty(savedCartLineItem.SavedCartLineItemId) ? 0 : int.Parse(savedCartLineItem.SavedCartLineItemId);
            var savedCartLineItemEntity = savedCartLineItemService.GetBySavedCartLineItemID(savedCartLineItemId) ?? new ZNode.Libraries.DataAccess.Entities.SavedCartLineItem();
            savedCartLineItemEntity.SavedCartLineItemID = savedCartLineItemId;
            savedCartLineItemEntity.SavedCartID = int.Parse(savedCartLineItem.SavedCartId);
            savedCartLineItemEntity.OrderLineItemRelationshipTypeID = string.IsNullOrEmpty(savedCartLineItem.OrderLineItemRelationshipTypeId)
                ? (int?)null : int.Parse(savedCartLineItem.OrderLineItemRelationshipTypeId);
            savedCartLineItemEntity.ParentSavedCartLineItemID = string.IsNullOrEmpty(savedCartLineItem.ParentLineItem)
                ? (int?)null : int.Parse(savedCartLineItem.ParentLineItem);
            savedCartLineItemEntity.Quantity = savedCartLineItem.Quantity;
            savedCartLineItemEntity.SKUID = int.Parse(savedCartLineItem.SkuId);
            return savedCartLineItemEntity;
        }

        public static Order ToOrderDto(IOrder orderEntity)
        {
            //Zeon Custom Code : Start
            var orderExtService = new OrderExtnService();
            OrderExtn orderExt = orderExtService.GetByOrderID(orderEntity.OrderID).Find("OrderID", orderEntity.OrderID, true);

            string ccNum = string.Empty;
            string ccType = string.Empty;

            System.Collections.ObjectModel.Collection<CardInformation> cardlist = new System.Collections.ObjectModel.Collection<CardInformation>();

            if (orderExt != null && orderExt.OrderID > 0)
            {
                ccNum = orderExt.CardNumber;
                ccType = orderExt.CardType;

                //Perficient Customization : Bind All Card Info.
                BindCardInfo(orderExt, cardlist);

            }
           

            //Zeon Custom Code : Ends

            return orderEntity == null ? null : new Order
            {
                OrderId = orderEntity.OrderID.ToString(),
                AccountId = orderEntity.AccountID.ToString(),
                BillingInformation = new Address
                {
                    FirstName = orderEntity.BillingFirstName,
                    LastName = orderEntity.BillingLastName,
                    CompanyName = orderEntity.BillingCompanyName,
                    StreetAddress1 = orderEntity.BillingStreet,
                    StreetAddress2 = orderEntity.BillingStreet1,
                    City = orderEntity.BillingCity,
                    StateCode = orderEntity.BillingStateCode,
                    PostalCode = orderEntity.BillingPostalCode,
                    CountryCode = orderEntity.BillingCountry,
                    PhoneNumber = orderEntity.BillingPhoneNumber, //Zeon Custom Code
                    Email = orderEntity.BillingEmailId //Zeon Custom Code
                },
                Discount = orderEntity.DiscountAmount,
                OrderStateId = orderEntity.OrderStateID.ToString(),
                SalesTax = orderEntity.SalesTax,
                ShippingInformation = new Address
                {
                    FirstName = orderEntity.ShipFirstName,
                    LastName = orderEntity.ShipLastName,
                    CompanyName = orderEntity.ShipCompanyName,
                    StreetAddress1 = orderEntity.ShipStreet,
                    StreetAddress2 = orderEntity.ShipStreet1,
                    City = orderEntity.ShipCity,
                    StateCode = orderEntity.ShipStateCode,
                    PostalCode = orderEntity.ShipPostalCode,
                    CountryCode = orderEntity.ShipCountry,
                    PhoneNumber = orderEntity.ShipPhoneNumber,//Zeon Custom Code
                    Email = orderEntity.ShipEmailID //Zeon Custom Code
                },
                Subtotal = orderEntity.SubTotal,
                Total = orderEntity.Total,
                TrackingNumber = orderEntity.TrackingNumber,
                TransactionId = orderEntity.CardTransactionID,
                CreateDate = orderEntity.OrderDate,
                UpdateDate = orderEntity.Modified,
                ExternalId = orderEntity.ExternalID,
                //Zeon Custom Code : Start
                PortalID = orderEntity.PortalId.ToString(),
                PaymentType = orderEntity.PaymentTypeId.ToString(),
                ShippingMethod = orderEntity.ShippingID.ToString(),
                //CreditCardAmount = orderEntity.Total,
                AuthorizationCode = orderEntity.CardAuthCode,
                Notes = orderEntity.AdditionalInstructions,
                OrderDate = orderEntity.OrderDate,
                PromoCode = orderEntity.CouponCode,
                PurchaseOrder = orderEntity.PurchaseOrderNumber,
                TotalShippingCost = orderEntity.ShippingCost,
                CreditCardNumber = ccNum,
                CreditCardType = ccType,
                //Perft Custom Code to save all Data of all used  crediat card in order
                #region[Code Commented: Wraping up in collection]
                //CreditCardAmount = orderExtEntity.CardAmount,
                //Card1TransactionID = orderExtEntity.Card1TransactionID,
                //Card1AuthCode = orderExtEntity.Card1AuthCode,
                //Card1Number = orderExtEntity.Card1Number,
                //Card1Type = orderExtEntity.Card1Type,
                //Card1Amount = orderExtEntity.Card1Amount,
                //Card2TransactionID = orderExtEntity.Card2TransactionID,
                //Card2AuthCode = orderExtEntity.Card2AuthCode,
                //Card2Number = orderExtEntity.Card2Number,
                //Card2Type = orderExtEntity.Card2Type,
                //Card2Amount = orderExtEntity.Card2Amount,
                #endregion

                CreditCardList = cardlist

                //Perft Custom Code to save all Data of all used  crediat card in order
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.Order ToOrderEntity(Order order, OrderService orderService)
        {
            var orderId = string.IsNullOrEmpty(order.OrderId) ? 0 : int.Parse(order.OrderId);
            var orderEntity = orderService.GetByOrderID(orderId) ?? new ZNode.Libraries.DataAccess.Entities.Order();
            orderEntity.OrderID = orderId;
            orderEntity.AccountID = string.IsNullOrEmpty(order.AccountId) ? (int?)null : int.Parse(order.AccountId);
            orderEntity.BillingFirstName = order.BillingInformation.FirstName;
            orderEntity.BillingLastName = order.BillingInformation.LastName;
            orderEntity.BillingCompanyName = order.BillingInformation.CompanyName;
            orderEntity.BillingStreet = order.BillingInformation.StreetAddress1;
            orderEntity.BillingStreet1 = order.BillingInformation.StreetAddress2;
            orderEntity.BillingCity = order.BillingInformation.City;
            orderEntity.BillingStateCode = order.BillingInformation.StateCode;
            orderEntity.BillingPostalCode = order.BillingInformation.PostalCode;
            orderEntity.BillingCountry = order.BillingInformation.CountryCode;
            orderEntity.BillingPhoneNumber = order.BillingInformation.PhoneNumber;
            orderEntity.DiscountAmount = order.Discount;
            orderEntity.OrderStateID = string.IsNullOrEmpty(order.OrderStateId) ? (int?)null : int.Parse(order.OrderStateId);
            orderEntity.SalesTax = order.SalesTax;
            orderEntity.ShipFirstName = order.ShippingInformation.FirstName;
            orderEntity.ShipLastName = order.ShippingInformation.LastName;
            orderEntity.ShipCompanyName = order.ShippingInformation.CompanyName;
            orderEntity.ShipStreet = order.ShippingInformation.StreetAddress1;
            orderEntity.ShipStreet1 = order.ShippingInformation.StreetAddress2;
            orderEntity.ShipCity = order.ShippingInformation.City;
            orderEntity.ShipStateCode = order.ShippingInformation.StateCode;
            orderEntity.ShipPostalCode = order.ShippingInformation.PostalCode;
            orderEntity.ShipCountry = order.ShippingInformation.CountryCode;
            orderEntity.ShipPhoneNumber = order.ShippingInformation.PhoneNumber;
            orderEntity.SubTotal = order.Subtotal;
            orderEntity.Total = order.Total;
            orderEntity.TrackingNumber = order.TrackingNumber;
            orderEntity.CardTransactionID = order.TransactionId;
            orderEntity.OrderDate = order.CreateDate;
            orderEntity.Modified = order.UpdateDate;
            orderEntity.ExternalID = order.ExternalId;
            return orderEntity;
        }

        public static OrderLineItem ToOrderLineItemDto(IOrderLineItem orderLineItemEntity)
        {
            //Zeon Custom Code : Start
            string notes = string.Empty;
            var orderLineItemExtnService = new OrderLineItemExtnService();
            TList<OrderLineItemExtn> orderLineItemExtList = orderLineItemExtnService.GetByOrderLineItemID(orderLineItemEntity.OrderLineItemID);
            if (orderLineItemExtList != null && orderLineItemExtList.Count > 0)
            {
                OrderLineItemExtn orderLineItemExt = orderLineItemExtList[0];

                if (orderLineItemExt != null)
                {
                    notes = orderLineItemExt.Notes;
                }
            }
            //Zeon Custom Code : Start

            return orderLineItemEntity == null ? null : new OrderLineItem
            {
                OrderLineItemId = orderLineItemEntity.OrderLineItemID.ToString(),
                OrderId = orderLineItemEntity.OrderID.ToString(),
                OrderLineItemRelationshipTypeId = orderLineItemEntity.OrderLineItemRelationshipTypeID.ToString(),
                DiscountAmount = orderLineItemEntity.DiscountAmount,
                ParentLineItem = orderLineItemEntity.ParentOrderLineItemID.ToString(),
                Price = orderLineItemEntity.Price,
                Quantity = orderLineItemEntity.Quantity,
                SalesTax = orderLineItemEntity.SalesTax,
                ShipDate = orderLineItemEntity.ShipDate,
                ShipmentId = orderLineItemEntity.ShipmentID.ToString(),
                ShippingCost = orderLineItemEntity.ShippingCost,
                TrackingNumber = orderLineItemEntity.TrackingNumber,
                CreateDate = orderLineItemEntity.Created,
                UpdateDate = orderLineItemEntity.Modified,
                ExternalId = orderLineItemEntity.ExternalID,
                //Zeon Custom Code : Start
                ItemSku = orderLineItemEntity.SKU,
                ProductName = orderLineItemEntity.Name,
                PromoCode = orderLineItemEntity.PromoDescription,
                Notes = notes
                //Zeon Custom Code : Start
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.OrderLineItem ToOrderLineItemEntity(OrderLineItem orderLineItem, OrderLineItemService orderLineItemService)
        {
            var orderLineItemId = string.IsNullOrEmpty(orderLineItem.OrderLineItemId) ? 0 : int.Parse(orderLineItem.OrderLineItemId);
            var orderLineItemEntity = orderLineItemService.GetByOrderLineItemID(orderLineItemId) ?? new ZNode.Libraries.DataAccess.Entities.OrderLineItem();
            orderLineItemEntity.OrderLineItemID = orderLineItemId;
            orderLineItemEntity.OrderID = int.Parse(orderLineItem.OrderId);
            orderLineItemEntity.OrderLineItemRelationshipTypeID = string.IsNullOrEmpty(orderLineItem.OrderLineItemRelationshipTypeId)
                ? (int?)null : int.Parse(orderLineItem.OrderLineItemRelationshipTypeId);
            orderLineItemEntity.ParentOrderLineItemID = string.IsNullOrEmpty(orderLineItem.ParentLineItem)
                ? (int?)null : int.Parse(orderLineItem.ParentLineItem);
            orderLineItemEntity.DiscountAmount = orderLineItem.DiscountAmount;
            orderLineItemEntity.Quantity = orderLineItem.Quantity;
            orderLineItemEntity.Price = orderLineItem.Price;
            orderLineItemEntity.SalesTax = orderLineItem.SalesTax;
            orderLineItemEntity.ShipDate = orderLineItem.ShipDate;
            orderLineItemEntity.ShipmentID = string.IsNullOrEmpty(orderLineItem.ShipmentId)
                ? (int?)null : int.Parse(orderLineItem.ShipmentId);
            orderLineItemEntity.ShippingCost = orderLineItem.ShippingCost;
            orderLineItemEntity.TrackingNumber = orderLineItem.TrackingNumber;
            orderLineItemEntity.Created = orderLineItem.CreateDate;
            orderLineItemEntity.ExternalID = orderLineItem.ExternalId;
            return orderLineItemEntity;
        }

        public static OrderState ToOrderStateDto(IOrderState orderStateEntity)
        {
            return orderStateEntity == null ? null : new OrderState
            {
                OrderStateId = orderStateEntity.OrderStateID.ToString(),
                OrderStateName = orderStateEntity.OrderStateName,
                Description = orderStateEntity.Description
            };
        }

        public static ProductReviewState ToProductReviewStateDto(IProductReviewState productReviewStateEntity)
        {
            return productReviewStateEntity == null ? null : new ProductReviewState
            {
                ReviewStateId = productReviewStateEntity.ReviewStateID.ToString(),
                ReviewStateName = productReviewStateEntity.ReviewStateName,
                Description = productReviewStateEntity.Description
            };
        }

        public static AddOn ToAddOnDto(IAddOn addOnEntity)
        {
            return addOnEntity == null ? null : new AddOn
            {
                AddOnId = addOnEntity.AddOnID.ToString(),
                ExternalAPIID = addOnEntity.ExternalAPIID,
                AllowBackOrder = addOnEntity.AllowBackOrder,
                Description = addOnEntity.Description,
                Name = addOnEntity.Name,
                OptionalInd = addOnEntity.OptionalInd,
                Title = addOnEntity.Title,
                TrackInventoryInd = addOnEntity.TrackInventoryInd
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.AddOn ToAddOnEntity(AddOn addOn, AddOnService addOnService)
        {
            var addOnId = string.IsNullOrEmpty(addOn.AddOnId) ? 0 : int.Parse(addOn.AddOnId);
            var addOnEntity = addOnService.GetByAddOnID(addOnId) ?? new ZNode.Libraries.DataAccess.Entities.AddOn();
            addOnEntity.AddOnID = addOnId;
            addOnEntity.ExternalAPIID = addOn.ExternalAPIID;
            addOnEntity.AllowBackOrder = addOn.AllowBackOrder;
            addOnEntity.Description = addOn.Description;
            addOnEntity.Name = addOn.Name;
            addOnEntity.OptionalInd = addOn.OptionalInd;
            addOnEntity.Title = addOn.Title;
            addOnEntity.TrackInventoryInd = addOn.TrackInventoryInd;
            return addOnEntity;
        }

        public static ZNodeAddOnEntity ToZnodeAddOnEntity(AddOn addOn)
        {
            ZNodeAddOnEntity znodeAddOnEntity = new ZNodeAddOnEntity();
            znodeAddOnEntity.AddOnID = Convert.ToInt32(addOn.AddOnId);
            znodeAddOnEntity.ProductId = 0;
            znodeAddOnEntity.Name = addOn.Name;
            znodeAddOnEntity.Title = addOn.Title;
            znodeAddOnEntity.Description = addOn.Description;
            znodeAddOnEntity.AllowBackOrder = addOn.AllowBackOrder;
            znodeAddOnEntity.TrackInventoryInd = addOn.TrackInventoryInd;
            znodeAddOnEntity.OptionalInd = addOn.OptionalInd;
            znodeAddOnEntity.DisplayOrder = 1;
            znodeAddOnEntity.DisplayType = "DROPDOWN";
            znodeAddOnEntity.OutOfStockMsg = string.Empty;
            znodeAddOnEntity.InStockMsg = string.Empty;
            znodeAddOnEntity.BackOrderMsg = string.Empty;
            znodeAddOnEntity.AddOnValueCollection = new ZNodeGenericCollection<ZNodeAddOnValueEntity>();
            foreach (var addOnValue in addOn.AddOnValues)
            {
                znodeAddOnEntity.AddOnValueCollection.Add(DataConverter.ToZnodeAddOnValueEntity(addOnValue));
            }
            return znodeAddOnEntity;
        }

        public static AddOnValue ToAddOnValueDto(IAddOnValue addOnValueEntity)
        {
            return addOnValueEntity == null ? null : new AddOnValue
                           {
                               AddOnValueId = addOnValueEntity.AddOnValueID.ToString(),
                               AddOnId = addOnValueEntity.AddOnID.ToString(),
                               DefaultInd = addOnValueEntity.DefaultInd,
                               Description = addOnValueEntity.Description,
                               DisplayOrder = addOnValueEntity.DisplayOrder,
                               ExternalId = addOnValueEntity.ExternalID,
                               FreeShippingInd = addOnValueEntity.FreeShippingInd,
                               Name = addOnValueEntity.Name,
                               RetailPrice = addOnValueEntity.RetailPrice,
                               SKU = addOnValueEntity.SKU,
                               SalePrice = addOnValueEntity.SalePrice,
                               Weight = addOnValueEntity.Weight,
                               WholesalePrice = addOnValueEntity.WholesalePrice
                           };
        }

        public static ZNode.Libraries.DataAccess.Entities.AddOnValue ToAddOnValueEntity(AddOnValue addOnValue, AddOnValueService addOnValueService)
        {
            var addOnValueId = string.IsNullOrEmpty(addOnValue.AddOnValueId) ? 0 : Convert.ToInt32(addOnValue.AddOnValueId);
            var addOnValueEntity = addOnValueService.GetByAddOnValueID(addOnValueId) ?? new ZNode.Libraries.DataAccess.Entities.AddOnValue();
            addOnValueEntity.AddOnValueID = addOnValueId;
            addOnValueEntity.AddOnID = Convert.ToInt32(addOnValue.AddOnId);
            addOnValueEntity.DefaultInd = addOnValue.DefaultInd;
            addOnValueEntity.Description = addOnValue.Description;
            addOnValueEntity.DisplayOrder = addOnValue.DisplayOrder;
            addOnValueEntity.ExternalID = addOnValue.ExternalId;
            addOnValueEntity.FreeShippingInd = addOnValue.FreeShippingInd;
            addOnValueEntity.Name = addOnValue.Name;
            addOnValueEntity.RetailPrice = addOnValue.RetailPrice;
            addOnValueEntity.SKU = addOnValue.SKU;
            addOnValueEntity.SalePrice = addOnValue.SalePrice;
            addOnValueEntity.Weight = addOnValue.Weight;
            addOnValueEntity.WholesalePrice = addOnValue.WholesalePrice;
            return addOnValueEntity;
        }

        public static ZNodeAddOnValueEntity ToZnodeAddOnValueEntity(AddOnValue addOnValue)
        {
            ZNodeAddOnValueEntity znodeAddOnValueEntity = new ZNodeAddOnValueEntity();
            znodeAddOnValueEntity.AddOnId = Convert.ToInt32(addOnValue.AddOnId);
            znodeAddOnValueEntity.AddOnValueID = Convert.ToInt32(addOnValue.AddOnValueId);
            znodeAddOnValueEntity.SKU = addOnValue.SKU;
            znodeAddOnValueEntity.Name = addOnValue.Name;
            znodeAddOnValueEntity.Description = addOnValue.Description;
            //znodeAddOnValueEntity.QuantityOnHand = addOnValue.QuantityOnHand; 
            znodeAddOnValueEntity.Weight = addOnValue.Weight;
            znodeAddOnValueEntity.DisplayOrder = 1;
            znodeAddOnValueEntity.RetailPrice = addOnValue.RetailPrice;
            znodeAddOnValueEntity.SalePrice = addOnValue.SalePrice;
            znodeAddOnValueEntity.WholesalePrice = addOnValue.WholesalePrice;
            znodeAddOnValueEntity.IsDefault = false;
            znodeAddOnValueEntity.Height = 0;
            znodeAddOnValueEntity.Width = 0;
            znodeAddOnValueEntity.Length = 0;
            //znodeAddOnValueEntity.ShippingRuleTypeId = addOnValue.ShippingRuleTypeId;
            znodeAddOnValueEntity.ShippingCost = 0;
            znodeAddOnValueEntity.FreeShippingInd = addOnValue.FreeShippingInd ?? false;
            znodeAddOnValueEntity.DiscountAmount = 0;
            //znodeAddOnValueEntity.SupplierId = addOnValue.SupplierId;
            znodeAddOnValueEntity.TaxClassID = 0;
            znodeAddOnValueEntity.GST = 0;
            znodeAddOnValueEntity.HST = 0;
            znodeAddOnValueEntity.PST = 0;
            znodeAddOnValueEntity.VAT = 0;
            znodeAddOnValueEntity.SalesTax = 0;
            znodeAddOnValueEntity.RecurringBillingInd = false;
            znodeAddOnValueEntity.RecurringBillingInstallmentInd = false;
            znodeAddOnValueEntity.RecurringBillingPeriod = string.Empty;
            znodeAddOnValueEntity.RecurringBillingFrequency = string.Empty;
            znodeAddOnValueEntity.RecurringBillingTotalCycles = 0;
            znodeAddOnValueEntity.RecurringBillingInitialAmount = 0;
            znodeAddOnValueEntity.CustomText = string.Empty;
            znodeAddOnValueEntity.ExternalProductID = null;
            return znodeAddOnValueEntity;
        }


        public static AttributeType ToAttributeTypeDto(IAttributeType attributeTypeEntity)
        {
            return attributeTypeEntity == null ? null : new AttributeType
            {
                AttributeTypeId = attributeTypeEntity.AttributeTypeId.ToString(),
                Description = attributeTypeEntity.Description,
                DisplayName = attributeTypeEntity.Name,
            };
        }

        public static AttributeValue ToAttributeValueDto(IProductAttribute productAttributeEntity)
        {
            return productAttributeEntity == null ? null : new AttributeValue
            {
                AttributeValueId = productAttributeEntity.AttributeId.ToString(),
                DisplayName = productAttributeEntity.Name,
                IsActive = productAttributeEntity.IsActive,
                Order = productAttributeEntity.DisplayOrder
            };
        }

        public static Highlight ToHighlightDto(IHighlight highlightEntity)
        {
            return highlightEntity == null ? null : new Highlight
            {
                HighlightId = highlightEntity.HighlightID.ToString(),
                Description = highlightEntity.Description,
                Name = highlightEntity.Name
            };
        }

        public static Review ToReviewDto(IReview reviewEntity)
        {
            return reviewEntity == null ? null : new Review
            {
                ReviewId = reviewEntity.ReviewID.ToString(),
                AccountId = reviewEntity.AccountID.ToString(),
                Comments = reviewEntity.Comments,
                CreateUser = reviewEntity.CreateUser,
                ProductId = reviewEntity.ProductID.ToString(),
                Subject = reviewEntity.Subject,
                UserLocation = reviewEntity.UserLocation
            };
        }

        public static TieredPricing ToTieredPricingDto(IProductTier productTierEntity)
        {
            return productTierEntity == null ? null : new TieredPricing
            {
                TieredPricingId = productTierEntity.ProductTierID.ToString(),
                ProductId = productTierEntity.ProductID.ToString(),
                ProfileId = productTierEntity.ProfileID.ToString(),
                TierEnd = productTierEntity.TierEnd,
                TierStart = productTierEntity.TierStart,
                Price = productTierEntity.Price
            };
        }

        public static ProductTier ToTieredPricingEntity(TieredPricing tieredPricing, ProductTierService productTierService)
        {
            var productTierId = string.IsNullOrEmpty(tieredPricing.TieredPricingId) ? 0 : int.Parse(tieredPricing.TieredPricingId);
            var productTierEntity = productTierService.GetByProductTierID(productTierId) ?? new ProductTier();
            productTierEntity.ProductTierID = productTierId;
            productTierEntity.ProductID = int.Parse(tieredPricing.ProductId);
            productTierEntity.ProfileID = string.IsNullOrEmpty(tieredPricing.TieredPricingId) ? 0 : int.Parse(tieredPricing.ProfileId);
            productTierEntity.TierEnd = tieredPricing.TierEnd;
            productTierEntity.TierStart = tieredPricing.TierStart;
            productTierEntity.Price = tieredPricing.Price;
            return productTierEntity;
        }

        public static Sku ToSkuDto(ISKU skuEntity)
        {
            return skuEntity == null ? null : new Sku
            {
                SkuId = skuEntity.SKUID.ToString(),
                ExternalId = skuEntity.ExternalProductAPIID,
                Image = new Image
                {
                    AltTag = skuEntity.ImageAltTag,
                    Path = skuEntity.SKUPicturePath
                },
                Pricing = new Pricing
                {
                    // IsCallForPricing?
                    Retail = skuEntity.RetailPriceOverride,
                    Sale = skuEntity.SalePriceOverride,
                    Wholesale = skuEntity.WholesalePriceOverride
                },
                ProductId = skuEntity.ProductID.ToString(),
                SKU = skuEntity.SKU
            };
        }

        public static SKU ToSkuEntity(Sku sku, SKUService skuService)
        {
            var skuId = string.IsNullOrEmpty(sku.SkuId) ? 0 : int.Parse(sku.SkuId);
            var skuEntity = skuService.GetBySKUID(skuId) ?? new SKU();
            skuEntity.SKUID = skuId;
            skuEntity.ExternalProductAPIID = sku.ExternalId;
            skuEntity.ImageAltTag = sku.Image.AltTag;
            skuEntity.SKUPicturePath = sku.Image.Path;
            skuEntity.RetailPriceOverride = sku.Pricing.Retail;
            skuEntity.SalePriceOverride = sku.Pricing.Sale;
            skuEntity.WholesalePriceOverride = sku.Pricing.Wholesale;
            skuEntity.ProductID = int.Parse(sku.ProductId);
            skuEntity.SKU = sku.SKU;
            return skuEntity;
        }

        public static Supplier ToSupplierDto(ISupplier supplierEntity)
        {
            return supplierEntity == null ? null : new Supplier
            {
                SupplierId = supplierEntity.SupplierID.ToString(),
                ExternalId = supplierEntity.ExternalID,
                ExternalSupplierNo = supplierEntity.ExternalSupplierNo,
                ActiveInd = supplierEntity.ActiveInd,
                ContactEmail = supplierEntity.ContactEmail,
                ContactFirstName = supplierEntity.ContactFirstName,
                ContactLastName = supplierEntity.ContactLastName,
                ContactPhone = supplierEntity.ContactPhone,
                Description = supplierEntity.Description,
                EnableEmailNotification = supplierEntity.EnableEmailNotification,
                Name = supplierEntity.Name,

            };
        }

        public static ZNode.Libraries.DataAccess.Entities.Supplier ToSupplierEntity(Supplier supplier, SupplierService supplierService)
        {
            var supplierId = string.IsNullOrEmpty(supplier.SupplierId) ? 0 : Convert.ToInt32(supplier.SupplierId);
            var supplierEntity = supplierService.GetBySupplierID(supplierId) ?? new ZNode.Libraries.DataAccess.Entities.Supplier();
            supplierEntity.ExternalID = supplier.ExternalId;
            supplierEntity.ExternalSupplierNo = supplier.ExternalSupplierNo;
            supplierEntity.ActiveInd = supplier.ActiveInd;
            supplierEntity.ContactEmail = supplier.ContactEmail;
            supplierEntity.ContactFirstName = supplier.ContactFirstName;
            supplierEntity.ContactLastName = supplier.ContactLastName;
            supplierEntity.ContactPhone = supplier.ContactPhone;
            supplierEntity.Description = supplier.Description;
            supplierEntity.EnableEmailNotification = supplier.EnableEmailNotification;
            supplierEntity.Name = supplier.Name;
            return supplierEntity;
        }

        public static SupplierType ToSupplierTypeDto(ISupplierType supplierTypeEntity)
        {
            return supplierTypeEntity == null ? null : new SupplierType
            {
                SupplierTypeId = supplierTypeEntity.SupplierTypeID.ToString(),
                ActiveInd = supplierTypeEntity.ActiveInd,
                ClassName = supplierTypeEntity.ClassName,
                Description = supplierTypeEntity.Description,
                Name = supplierTypeEntity.Name
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.SupplierType ToSupplierTypeEntity(SupplierType supplierType, SupplierTypeService supplierTypeService)
        {
            var supplierTypeId = string.IsNullOrEmpty(supplierType.SupplierTypeId) ? 0 : Convert.ToInt32(supplierType.SupplierTypeId);
            var supplierTypeEntity = supplierTypeService.GetBySupplierTypeID(supplierTypeId) ?? new ZNode.Libraries.DataAccess.Entities.SupplierType();
            supplierTypeEntity.ActiveInd = supplierType.ActiveInd;
            supplierTypeEntity.ClassName = supplierType.ClassName;
            supplierTypeEntity.Description = supplierType.Description;
            supplierTypeEntity.Name = supplierType.Name;
            return supplierTypeEntity;
        }

        public static TaxClass ToTaxClassDto(ITaxClass taxClassEntity)
        {
            return taxClassEntity == null ? null : new TaxClass
            {
                TaxClassId = taxClassEntity.TaxClassID.ToString(),
                ExternalId = taxClassEntity.ExternalID,
                ActiveInd = taxClassEntity.ActiveInd,
                Name = taxClassEntity.Name,
                PortalId = taxClassEntity.PortalID.ToString()
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.TaxClass ToTaxClassEntity(TaxClass taxClass, TaxClassService taxClassService)
        {
            var taxClassId = string.IsNullOrEmpty(taxClass.TaxClassId) ? 0 : Convert.ToInt32(taxClass.TaxClassId);
            var taxClassEntity = taxClassService.GetByTaxClassID(taxClassId) ?? new ZNode.Libraries.DataAccess.Entities.TaxClass();
            taxClassEntity.ExternalID = taxClass.ExternalId;
            taxClassEntity.ActiveInd = taxClass.ActiveInd;
            taxClassEntity.Name = taxClass.Name;
            taxClassEntity.PortalID = ToNullableIntField(taxClass.PortalId);
            return taxClassEntity;
        }

        public static TaxRule ToTaxRuleDto(ITaxRule taxRuleEntity)
        {
            return taxRuleEntity == null ? null : new TaxRule
            {
                TaxRuleId = taxRuleEntity.TaxRuleID.ToString(),
                ExternalId = taxRuleEntity.ExternalID,
                TaxRuleTypeId = taxRuleEntity.TaxRuleTypeID.ToString(),
                PortalId = taxRuleEntity.PortalID.ToString(),
                TaxClassId = taxRuleEntity.TaxClassID.ToString(),
                DestinationCountryCode = taxRuleEntity.DestinationCountryCode,
                DestinationStateCode = taxRuleEntity.DestinationStateCode,
                CountyFIPS = taxRuleEntity.CountyFIPS,
                Precedence = taxRuleEntity.Precedence,
                SalesTax = taxRuleEntity.SalesTax,
                VAT = taxRuleEntity.VAT,
                GST = taxRuleEntity.GST,
                PST = taxRuleEntity.PST,
                HST = taxRuleEntity.HST,
                TaxShipping = taxRuleEntity.TaxShipping,
                InclusiveInd = taxRuleEntity.InclusiveInd
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.TaxRule ToTaxRuleEntity(TaxRule taxRule, TaxRuleService taxRuleService)
        {
            var taxRuleId = string.IsNullOrEmpty(taxRule.TaxRuleId) ? 0 : Convert.ToInt32(taxRule.TaxRuleId);
            var taxRuleEntity = taxRuleService.GetByTaxRuleID(taxRuleId) ?? new ZNode.Libraries.DataAccess.Entities.TaxRule();
            taxRuleEntity.ExternalID = taxRule.ExternalId;
            taxRuleEntity.TaxRuleTypeID = ToNullableIntField(taxRule.TaxRuleTypeId);
            taxRuleEntity.PortalID = Convert.ToInt32(taxRule.PortalId);
            taxRuleEntity.TaxClassID = ToNullableIntField(taxRule.TaxClassId);
            taxRuleEntity.DestinationCountryCode = taxRule.DestinationCountryCode;
            taxRuleEntity.DestinationStateCode = taxRule.DestinationStateCode;
            taxRuleEntity.CountyFIPS = taxRule.CountyFIPS;
            taxRuleEntity.Precedence = taxRule.Precedence;
            taxRuleEntity.SalesTax = taxRule.SalesTax;
            taxRuleEntity.VAT = taxRule.VAT;
            taxRuleEntity.GST = taxRule.GST;
            taxRuleEntity.PST = taxRule.PST;
            taxRuleEntity.HST = taxRule.HST;
            taxRuleEntity.TaxShipping = taxRule.TaxShipping;
            taxRuleEntity.InclusiveInd = taxRule.InclusiveInd;
            return taxRuleEntity;
        }

        public static TaxRuleType ToTaxRuleTypeDto(ITaxRuleType taxRuleTypeEntity)
        {
            return taxRuleTypeEntity == null ? null : new TaxRuleType
            {
                TaxRuleTypeId = taxRuleTypeEntity.TaxRuleTypeID.ToString(),
                ActiveInd = taxRuleTypeEntity.ActiveInd,
                ClassName = taxRuleTypeEntity.ClassName,
                Description = taxRuleTypeEntity.Description,
                Name = taxRuleTypeEntity.Name,
                PortalId = taxRuleTypeEntity.PortalID.ToString()
            };
        }

        public static ZNode.Libraries.DataAccess.Entities.TaxRuleType ToTaxRuleTypeEntity(TaxRuleType taxRuleType, TaxRuleTypeService taxRuleTypeService)
        {
            var taxRuleTypeId = string.IsNullOrEmpty(taxRuleType.TaxRuleTypeId) ? 0 : Convert.ToInt32(taxRuleType.TaxRuleTypeId);
            var taxRuleTypeEntity = taxRuleTypeService.GetByTaxRuleTypeID(taxRuleTypeId) ?? new ZNode.Libraries.DataAccess.Entities.TaxRuleType();
            taxRuleTypeEntity.ActiveInd = taxRuleType.ActiveInd;
            taxRuleTypeEntity.ClassName = taxRuleType.ClassName;
            taxRuleTypeEntity.Description = taxRuleType.Description;
            taxRuleTypeEntity.Name = taxRuleType.Name;
            taxRuleTypeEntity.PortalID = ToNullableIntField(taxRuleType.PortalId);
            return taxRuleTypeEntity;
        }

        public static ZNodeSKU ToZnodeSKU(ZNode.Libraries.DataAccess.Entities.SKU sku)
        {
            ZNodeSKU znodeSKU = new ZNodeSKU();
            //znodeSKU.AttributesDescription = sku.AttributesDescription;
            //znodeSKU.AttributesValue = sku.AttributesValue;
            znodeSKU.Custom1 = sku.Custom1;
            znodeSKU.Custom2 = sku.Custom2;
            znodeSKU.Custom3 = sku.Custom3;
            //znodeSKU.DisplayOrder = sku.DisplayOrder;
            znodeSKU.ImageAltTag = sku.ImageAltTag;
            //znodeSKU.IsActive = sku.IsActive;
            znodeSKU.Note = sku.Note;
            znodeSKU.ProductID = sku.ProductID;
            //znodeSKU.QuantityBuffer = sku.QuantityBuffer;
            //znodeSKU.QuantityOnHand = sku.QuantityOnHand;
            znodeSKU.RecurringBillingFrequency = sku.RecurringBillingFrequency;
            //znodeSKU.RecurringBillingInitialAmount = sku.RecurringBillingInitialAmount;
            znodeSKU.RecurringBillingPeriod = sku.RecurringBillingPeriod;
            //znodeSKU.RecurringBillingTotalCycles = sku.RecurringBillingTotalCycles;
            //znodeSKU.ReorderLevel = sku.ReorderLevel;
            znodeSKU.RetailPriceOverride = sku.RetailPriceOverride;
            znodeSKU.SalePriceOverride = sku.SalePriceOverride;
            znodeSKU.SKU = sku.SKU;
            znodeSKU.SKUID = sku.SKUID;
            znodeSKU.SKUPicturePath = sku.SKUPicturePath;
            //znodeSKU.SupplierID = sku.SupplierID;
            //znodeSKU.Surcharge = sku.Surcharge;
            //znodeSKU.WarehouseNo = sku.WarehouseNo;
            //znodeSKU.WeightAdditional = sku.WeightAdditional;
            znodeSKU.WholesalePriceOverride = sku.WholesalePriceOverride;
            //znodeSKU.SkuProfileCollection = sku.SkuProfileCollection;
            return znodeSKU;
        }

        public static int? ToNullableIntField(string stringInt)
        {
            if (string.IsNullOrEmpty(stringInt))
            {
                return null;
            }
            else
            {
                return Convert.ToInt32(stringInt);
            }
        }

        public static void BindCardDetail(string amount, string transactionId, string authCode, string cardNumber, string type, string expDate, System.Collections.ObjectModel.Collection<CardInformation> cardlist)
        {
            CardInformation cardInformation = new CardInformation();
            cardInformation.Amount = amount;
            cardInformation.CardExp = expDate;
            cardInformation.CardNumber = cardNumber;
            cardInformation.CardType = type;
            cardInformation.TransactionId = transactionId;
            cardInformation.CardAuthCode = authCode;
            cardlist.Add(cardInformation);
        }

        private static void BindCardInfo(OrderExtn orderExt, System.Collections.ObjectModel.Collection<CardInformation> cardlist)
        {
            if (orderExt.CardAmount.GetValueOrDefault(0) > 0 && !string.IsNullOrWhiteSpace(orderExt.CardTransactionID))
            {
                BindCardDetail(orderExt.CardAmount.GetValueOrDefault(0).ToString(),
                    orderExt.CardTransactionID, orderExt.CardAuthCode, orderExt.CardNumber, orderExt.CardType, orderExt.CardExp, cardlist);
            }

            if (orderExt.Card1Amount.GetValueOrDefault(0) > 0 && !string.IsNullOrWhiteSpace(orderExt.Card1TransactionID))
            {
                BindCardDetail(orderExt.Card1Amount.GetValueOrDefault(0).ToString(),
                    orderExt.Card1TransactionID, orderExt.Card1AuthCode, orderExt.Card1Number, orderExt.Card1Type, orderExt.Card1Exp, cardlist);
            }

            if (orderExt.Card2Amount.GetValueOrDefault(0) > 0 && !string.IsNullOrWhiteSpace(orderExt.Card2TransactionID))
            {
                BindCardDetail(orderExt.Card2Amount.GetValueOrDefault(0).ToString(),
                    orderExt.Card2TransactionID, orderExt.Card2AuthCode, orderExt.Card2Number, orderExt.Card2Type, orderExt.Card2Exp, cardlist);
            }
        }
    }
}
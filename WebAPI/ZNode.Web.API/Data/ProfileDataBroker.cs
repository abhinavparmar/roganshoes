﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using RogansShoes.Web.API.Models;
using RogansShoes.Web.API.Support;
using Profile = RogansShoes.Web.API.Models.Profile;

namespace RogansShoes.Web.API.Data
{
    public class ProfileDataBroker
    {
        public static List<Profile> GetAllProfiles(int pageSize, int page, out int count, IList<string> expands,
                                                   List<QueryBuilder.WhereCondition> filters, List<string> ordering)
        {
            var ph = new ProfileHelper();

            filters = InitializeFilters(filters);

            foreach (var filter in filters)
            {
                if (filter.FieldName.Equals(Filters.ModifyDate, StringComparison.InvariantCultureIgnoreCase))
                    filter.FieldName = Filters.UpdateDte;
                if (filter.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase))
                    filter.FieldName = Filters.CreateDte;
            }

            var queryBuilder = new QueryBuilder("Profile", filters);
            var query = queryBuilder.GetQuery();

            var arrayFilters = new string[filters.Count, 2];

            #region Populate array

            for (var i = 0; i < filters.Count; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        if (filters[i].FieldName.Equals(Filters.CreateDte) ||
                            filters[i].FieldName.Equals(Filters.UpdateDte))
                        {
                            arrayFilters[i, j] = "@p" + i;
                        }
                        else
                        {
                            arrayFilters[i, j] = "@" + filters[i].FieldName;
                        }
                    }
                    if (j == 1)
                    {
                        arrayFilters[i, j] = filters[i].MatchValue;
                    }
                }
            }

            #endregion

            var ids = ph.GetProfileIDsByFilters(query, arrayFilters);

            count = ids.Count;
            var tempList = ids.Select(id => GetProfileById(id, expands, filters)).ToList();
            var returnList = Utility.OrderByDescOrAsc(tempList, ordering);
            return returnList.GetPage(pageSize, page).ToList();
        }

        public static Profile GetProfileById(string id, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);
            var acs = new ProfileService();
            var profileEntity = acs.GetByProfileID(GetProfileId(idType, id));

            if (profileEntity == null)
            {
                throw new ResourceNotFoundException("Profile not found.");
            }

            return GetProfileFromEntity(profileEntity, expands, filters);
        }

        private static Profile GetProfileFromEntity(IProfile profileEntity, IList<string> expands, IDictionary<string, string> filters)
        {
            if (profileEntity == null)
            {
                return null;
            }

            filters = filters.Where(f => f.Value != null).ToDictionary(f => f.Key, f => f.Value);

            var profile = DataConverter.ToProfileDto(profileEntity);

            return profile;
        }

        private static int GetProfileId(string idType, Profile profile)
        {
            if (idType == IdTypes.External)
            {
                return GetProfileId(idType, profile.DefaultExternalAccountNo);
            }
            return int.Parse(profile.ProfileId);
        }

        private static int GetProfileId(string idType, string id)
        {
            if (idType == IdTypes.External)
            {
                var ph = new ProfileHelper();
                var profileId = ph.GetProfileIDByExternalID(id);
                return int.Parse(profileId);
            }
            return int.Parse(id);
        }

        public static Profile GetProfileByProfile(Profile profile, IList<string> expands, List<QueryBuilder.WhereCondition> conditions, string idType = null)
        {
            var ps = new ProfileService();
            var profileEntity = ps.GetByProfileID(GetProfileId(idType, profile));

            if (profileEntity == null)
            {
                throw new ResourceNotFoundException("Profile not found.");
            }

            var filters = conditions == null ? null :
                conditions.Where(
                    x =>
                    !x.FieldName.Equals(Filters.CreateDate, StringComparison.InvariantCultureIgnoreCase) ||
                    !x.FieldName.Equals(Filters.UpdateDte, StringComparison.InvariantCultureIgnoreCase))
                          .ToDictionary(k => k.FieldName, v => v.MatchValue);

            return GetProfileFromEntity(profileEntity, expands, filters);
        }

        public static List<QueryBuilder.WhereCondition> InitializeFilters(List<QueryBuilder.WhereCondition> filters)
        {
            var list = filters;

            var fieldNames = filters.Select(x => x.FieldName.ToLower()).ToList();

            return list;
        }

        public static string SaveProfile(Profile profile)
        {
            if (!String.IsNullOrEmpty(profile.ProfileId))
            {
                throw new InvalidOperationException("Posting a new Profile ID is not allowed.");
            }
            if (!String.IsNullOrEmpty(profile.DefaultExternalAccountNo))
            {
                var ph = new ProfileHelper();
                var temp = ph.GetProfileIDByExternalID(profile.DefaultExternalAccountNo);
                if (temp != null)
                {
                    throw new InvalidOperationException("DefaultExternalAccountNo already exists.");
                }
            }

            var ps = new ProfileService();
            var profileEntity = DataConverter.ToProfileEntity(profile, ps);

            var result = ps.Save(profileEntity);
            return result.ProfileID.ToString();
        }

        public static void UpdateProfile(Profile profile, string idType = null)
        {
            profile.ProfileId = GetProfileId(idType, profile).ToString();

            if (!String.IsNullOrEmpty(profile.DefaultExternalAccountNo))
            {
                var ph = new ProfileHelper();
                var temp = ph.GetProfileIDByExternalID(profile.DefaultExternalAccountNo);
                if (temp != null && temp != profile.ProfileId)
                {
                    throw new InvalidOperationException("DefaultExternalAccountNo already exists.");
                }
            }

            var ps = new ProfileService();
            var profileEntity = DataConverter.ToProfileEntity(profile, ps);


            if (!ps.Update(profileEntity))
            {
                throw new InvalidOperationException("Specfied Profile does not exist.");
            }
        }
    }
}
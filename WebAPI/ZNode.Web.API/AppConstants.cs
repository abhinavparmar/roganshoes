﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZNode.Web.API
{
    internal static class AppConstants
    {
        public const string HeaderKey_ZnodeApiKey = "ZNode-ApiKey";
        //public const string HeaderKey_ZnodeApiKey = "0F39E5C4-5925-491A-8714-B2A6E3D164C2";
        // placeholder constant - will be refactored/removed...
        public const string ItemKey_LicenseKey = "ZNode-LicenseKey";
        //public const string ItemKey_LicenseKey = "Znode.cer";
    }
}
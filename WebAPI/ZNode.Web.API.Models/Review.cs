﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Review", Namespace = ""), Serializable, XmlRoot(ElementName = "Review")]
    public class Review
    {
        [DataMember(Order = 99, Name = "ReviewId"), XmlElement(Order = 0, ElementName = "ReviewId")]
        public string ReviewId { get; set; }

        [DataMember(Order = 99, Name = "AccountId"), XmlElement(Order = 1, ElementName = "AccountId")]
        public string AccountId { get; set; }

        [DataMember(Order = 99, Name = "Comments"), XmlElement(Order = 2, ElementName = "Comments")]
        public string Comments { get; set; }

        [DataMember(Order = 99, Name = "CreateUser"), XmlElement(Order = 3, ElementName = "CreateUser")]
        public string CreateUser { get; set; }

        [DataMember(Order = 99, Name = "ProductId"), XmlElement(Order = 4, ElementName = "ProductId")]
        public string ProductId { get; set; }

        [DataMember(Order = 99, Name = "Subject"), XmlElement(Order = 5, ElementName = "Subject")]
        public string Subject { get; set; }

        [DataMember(Order = 99, Name = "UserLocation"), XmlElement(Order = 6, ElementName = "UserLocation")]
        public string UserLocation { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Promotion", Namespace = ""), Serializable, XmlRoot(ElementName = "Promotion")]
    public class Promotion
    {
        [DataMember(Order = 99, Name = "PromotionId"), XmlElement(Order = 0, ElementName = "PromotionId")]
        public string PromotionId { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 1, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 2, ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement(Order = 3, ElementName = "Description")]
        public string Description { get; set; }

        [DataMember(Order = 99, Name = "DiscountTypeId"), XmlElement(Order = 4, ElementName = "DiscountTypeId")]
        public string DiscountTypeId { get; set; }

        [DataMember(Order = 99, Name = "Discount"), XmlElement(Order = 5, ElementName = "Discount")]
        public decimal Discount { get; set; }

        [DataMember(Order = 99, Name = "StartDate"), XmlElement(Order = 6, ElementName = "StartDate")]
        public DateTime StartDate { get; set; }

        [DataMember(Order = 99, Name = "EndDate"), XmlElement(Order = 7, ElementName = "EndDate")]
        public DateTime EndDate { get; set; }

        [DataMember(Order = 99, Name = "CouponInd"), XmlElement(Order = 8, ElementName = "CouponInd")]
        public bool CouponInd { get; set; }

        [DataMember(Order = 99, Name = "CouponCode"), XmlElement(Order = 9, ElementName = "CouponCode")]
        public string CouponCode { get; set; }

        [DataMember(Order = 99, Name = "CouponQuantityAvailable"), XmlElement(Order = 10, ElementName = "CouponQuantityAvailable")]
        public int? CouponQuantityAvailable { get; set; }

        [DataMember(Order = 99, Name = "CouponMessage"), XmlElement(Order = 11, ElementName = "CouponMessage")]
        public string CouponMessage { get; set; }

        [DataMember(Order = 99, Name = "OrderMinimum"), XmlElement(Order = 12, ElementName = "OrderMinimum")]
        public decimal? OrderMinimum { get; set; }

        [DataMember(Order = 99, Name = "QuantityMinimum"), XmlElement(Order = 13, ElementName = "QuantityMinimum")]
        public int? QuantityMinimum { get; set; }

        [DataMember(Order = 99, Name = "PromotionProductQty"), XmlElement(Order = 14, ElementName = "PromotionProductQty")]
        public int? PromotionProductQty { get; set; }

        [DataMember(Order = 99, Name = "PromotionProductId"), XmlElement(Order = 15, ElementName = "PromotionProductId")]
        public string PromotionProductId { get; set; }

        [DataMember(Order = 99, Name = "Custom1"), XmlElement(Order = 16, ElementName = "Custom1")]
        public string Custom1 { get; set; }

        [DataMember(Order = 99, Name = "Custom2"), XmlElement(Order = 17, ElementName = "Custom2")]
        public string Custom2 { get; set; }

        [DataMember(Order = 99, Name = "Custom3"), XmlElement(Order = 18, ElementName = "Custom3")]
        public string Custom3 { get; set; }

        [DataMember(Order = 99, Name = "EnableCouponUrl"), XmlElement(Order = 19, ElementName = "EnableCouponUrl")]
        public bool? EnableCouponUrl { get; set; }

        [DataMember(Order = 99, Name = "PromoCode"), XmlElement(Order = 20, ElementName = "PromoCode")]
        public string PromoCode { get; set; }

        [DataMember(Order = 99, Name = "ProfileId"), XmlElement(Order = 21, ElementName = "ProfileId")]
        public string ProfileId { get; set; }

        [DataMember(Order = 99, Name = "AccountId"), XmlElement(Order = 22, ElementName = "AccountId")]
        public string AccountId { get; set; }

        [DataMember(Order = 99, Name = "ProductId"), XmlElement(Order = 23, ElementName = "ProductId")]
        public string ProductId { get; set; }

        [DataMember(Order = 99, Name = "AddOnValueId"), XmlElement(Order = 24, ElementName = "AddOnValueId")]
        public string AddOnValueId { get; set; }

        [DataMember(Order = 99, Name = "SKUId"), XmlElement(Order = 25, ElementName = "SKUId")]
        public string SKUId { get; set; }
    }
}

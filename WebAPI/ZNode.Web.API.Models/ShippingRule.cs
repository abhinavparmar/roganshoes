﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "ShippingRule", Namespace = ""), Serializable, XmlRoot(ElementName = "ShippingRule")]
    public class ShippingRule
    {
        [DataMember(Order = 99, Name = "ShippingRuleId"), XmlElement(Order = 0, ElementName = "ShippingRuleId")]
        public string ShippingRuleId { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 1, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "ShippingId"), XmlElement(Order = 2, ElementName = "ShippingId")]
        public string ShippingId { get; set; }

        [DataMember(Order = 99, Name = "ShippingRuleTypeId"), XmlElement(Order = 3, ElementName = "ShippingRuleTypeId")]
        public string ShippingRuleTypeId { get; set; }

        [DataMember(Order = 99, Name = "ClassName"), XmlElement(Order = 4, ElementName = "ClassName")]
        public string ClassName { get; set; }

        [DataMember(Order = 99, Name = "LowerLimit"), XmlElement(Order = 5, ElementName = "LowerLimit")]
        public decimal? LowerLimit { get; set; }

        [DataMember(Order = 99, Name = "UpperLimit"), XmlElement(Order = 6, ElementName = "UpperLimit")]
        public decimal? UpperLimit { get; set; }

        [DataMember(Order = 99, Name = "BaseCost"), XmlElement(Order = 7, ElementName = "BaseCost")]
        public decimal BaseCost { get; set; }

        [DataMember(Order = 99, Name = "PerItemCost"), XmlElement(Order = 8, ElementName = "PerItemCost")]
        public decimal PerItemCost { get; set; }
    }
}

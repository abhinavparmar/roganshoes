﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "CreditCardInformation", Namespace = ""), Serializable]
    public class CreditCardInformation
    {
        [DataMember(Order = 99, Name = "CardExp", EmitDefaultValue = false), XmlElement(Order = 0, ElementName = "CardExp", IsNullable = true)]
        public string CardExp { get; set; }

        [DataMember(Order = 99, Name = "CardHolderName", EmitDefaultValue = false), XmlElement(Order = 1, ElementName = "CardHolderName", IsNullable = true)]
        public string CardHolderName { get; set; }

        [DataMember(Order = 99, Name = "CardNumber", EmitDefaultValue = false), XmlElement(Order = 2, ElementName = "CardNumber", IsNullable = true)]
        public string CardNumber { get; set; }

        [DataMember(Order = 99, Name = "CardSecurityCode", EmitDefaultValue = false), XmlElement(Order = 3, ElementName = "CardSecurityCode", IsNullable = true)]
        public string CardSecurityCode { get; set; }

        [DataMember(Order = 99, Name = "CardType", EmitDefaultValue = false), XmlElement(Order = 4, ElementName = "CardType", IsNullable = true)]
        public string CardType { get; set; }
    }
}

﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "CategoryNode", Namespace = ""), Serializable, XmlRoot(ElementName = "CategoryNode")]
    public class CategoryNode
    {
        [DataMember(Order = 99, Name = "CategoryNodeId"), XmlElement(Order = 0, ElementName = "CategoryNodeId")]
        public string CategoryNodeId { get; set; }

        [DataMember(Order = 99, Name = "CatalogId"), XmlElement(Order = 1, ElementName = "CatalogId")]
        public string CatalogId { get; set; }

        [DataMember(Order = 99, Name = "CategoryId"), XmlElement(Order = 2, ElementName = "CategoryId")]
        public string CategoryId { get; set; }

        [DataMember(Order = 99, Name = "DisplayOrder", EmitDefaultValue = false), XmlElement(Order = 3, ElementName = "DisplayOrder", IsNullable = true)]
        public int? DisplayOrder { get; set; }

        [DataMember(Order = 99, Name = "ParentCategoryNodeId"), XmlElement(Order = 4, ElementName = "ParentCategoryNodeId")]
        public string ParentCategoryNodeId { get; set; }
    }
}
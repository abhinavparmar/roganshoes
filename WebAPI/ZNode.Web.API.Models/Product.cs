﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Product", Namespace = ""), Serializable,XmlRoot(ElementName = "Product")]
    public class Product
    {
        [DataMember(Order = 99, Name = "ProductId"), XmlElement(Order = 0, ElementName = "ProductId")]
        public string ProductId { get; set; }

        [DataMember(Order = 99, Name = "AddOns"), XmlArray(Order = 1, ElementName = "AddOns", IsNullable = true), XmlArrayItem(Type = typeof(AddOn), ElementName = "AddOn", IsNullable = true)]
        public Collection<AddOn> AddOns { get; set; }

        [DataMember(Order = 99, Name = "AttributeTypes"), XmlArray(Order = 2, ElementName = "AttributeTypes", IsNullable = true), XmlArrayItem(Type = typeof(AttributeType), ElementName = "AttributeType", IsNullable = true)]
        public Collection<AttributeType> AttributeTypes { get; set; }

        [DataMember(Order = 99, Name = "CreateDate"), XmlElement(Order = 3, ElementName = "CreateDate")]
        public DateTime? CreateDate { get; set; }

        [DataMember(Order = 99, Name = "CrossSells"), XmlArray(Order = 4, ElementName = "CrossSells", IsNullable = true), XmlArrayItem(Type = typeof(Product), ElementName = "CrossSell", IsNullable = true)]
        public Collection<Product> CrossSells { get; set; }

        [DataMember(Order = 99,Name = "Description"), XmlElement( Order = 5, ElementName = "Description")]
        public string Description { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 6, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "Highlights"), XmlArray(Order = 7, ElementName = "Highlights", IsNullable = true), XmlArrayItem(Type = typeof(Highlight), ElementName = "Highlight", IsNullable = true)]
        public Collection<Highlight> Highlights { get; set; }

        [DataMember(Order = 99,Name = "Image"), XmlElement( Order = 8, ElementName = "Image")]
        public Image Image { get; set; }

        [DataMember(Order = 99, Name = "IsActive"), XmlElement(Order = 9, ElementName = "IsActive")]
        public bool IsActive { get; set; }

        [DataMember(Order = 99,Name = "Keywords"), XmlElement( Order = 10, ElementName = "Keywords")]
        public string Keywords { get; set; }

        [DataMember(Order = 99,Name = "Name"), XmlElement( Order = 11, ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "Order", EmitDefaultValue = false), XmlElement( Order = 12, ElementName = "Order", IsNullable = true)]
        public int? Order { get; set; }
        
        [DataMember(Order = 99,Name = "Pricing"), XmlElement( Order = 13, ElementName = "Pricing")]
        public Pricing Pricing { get; set; }

        [DataMember(Order = 99, Name = "ProductNumber"), XmlElement(Order = 14, ElementName = "ProductNumber")]
        public string ProductNumber { get; set; }

        [DataMember(Order = 99, Name = "Reviews"), XmlArray(Order = 15, ElementName = "Reviews", IsNullable = true), XmlArrayItem(Type = typeof(Review), ElementName = "Review", IsNullable = true)]
        public Collection<Review> Reviews { get; set; }

        [DataMember(Order = 99,Name = "Seo"), XmlElement( Order = 16, ElementName = "Seo")]
        public Seo Seo { get; set; }

        [DataMember(Order = 99,Name = "ShortDescription"), XmlElement( Order = 17, ElementName = "ShortDescription")]
        public string ShortDescription { get; set; }

        [DataMember(Order = 99, Name = "SKU"), XmlElement(Order = 18, ElementName = "SKU")]
        public string SKU { get; set; }

        [DataMember(Order = 99, Name = "Skus"), XmlArray(Order = 19, ElementName = "Skus", IsNullable = true), XmlArrayItem(Type = typeof(Sku), ElementName = "Sku", IsNullable = true)]
        public Collection<Sku> Skus { get; set; }

        [DataMember(Order = 99, Name = "TieredPricing"), XmlArray(Order = 20, ElementName = "TieredPricing", IsNullable = true), XmlArrayItem(Type = typeof(TieredPricing), ElementName = "TieredPricing", IsNullable = true)]
        public Collection<TieredPricing> TieredPricing { get; set; }

        [DataMember(Order = 99, Name = "UpdateDate"), XmlElement(Order = 21, ElementName = "UpdateDate")]
        public DateTime? UpdateDate { get; set; }

        [DataMember(Order = 99, Name = "SelectedAddons"), XmlElement(Order = 22, ElementName = "SelectedAddons")]
        public Collection<AddOn> SelectedAddons { get; set; }


        public Product()
        {
            Pricing = new Pricing();
            Image = new Image();
            Seo = new Seo();
            AddOns = new Collection<AddOn>();
            AttributeTypes = new Collection<AttributeType>();
            CrossSells = new Collection<Product>();
            Highlights = new Collection<Highlight>();
            Reviews = new Collection<Review>();
            Skus = new Collection<Sku>();
            TieredPricing = new Collection<TieredPricing>();
        }
    }
}
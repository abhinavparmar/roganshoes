﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Pricing", Namespace = ""), Serializable, XmlRoot(ElementName = "Pricing")]
    public class Pricing
    {
        [DataMember(Order = 99, Name = "IsCallForPricing"), XmlElement( Order=1,ElementName = "IsCallForPricing")]
        public bool IsCallForPricing { get; set; }

        [DataMember(Order = 99, Name = "Retail", EmitDefaultValue = false), XmlElement( Order=2,ElementName = "Retail", IsNullable = true)]
        public decimal? Retail { get; set; }

        [DataMember(Order = 99, Name = "Sale", EmitDefaultValue = false), XmlElement( Order=3,ElementName = "Sale", IsNullable = true)]
        public decimal? Sale { get; set; }

        [DataMember(Order = 99, Name = "Wholesale", EmitDefaultValue = false), XmlElement( Order=4,ElementName = "Wholesale", IsNullable = true)]
        public decimal? Wholesale { get; set; }
    }
}
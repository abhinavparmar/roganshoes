﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "TaxClass", Namespace = ""), Serializable, XmlRoot(ElementName = "TaxClass")]
    public class TaxClass
    {
        [DataMember(Order = 99, Name = "TaxClassId"), XmlElement(Order = 0, ElementName = "TaxClassId")]
        public string TaxClassId { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 1, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "PortalId"), XmlElement(Order = 2, ElementName = "PortalId")]
        public string PortalId { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 3, ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "ActiveInd"), XmlElement(Order = 4, ElementName = "ActiveInd")]
        public bool ActiveInd { get; set; }
    }
}

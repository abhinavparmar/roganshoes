﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "InventoryTransaction", Namespace = ""), Serializable, XmlRoot(ElementName = "InventoryTransaction")]
    public class InventoryTransaction
    {
        [DataMember(Order = 99, Name = "Sku"), XmlElement(Order = 0, ElementName = "Sku")]
        public string Sku { get; set; }

        [DataMember(Order = 99, Name = "AmountToChange"), XmlElement(Order = 1, ElementName = "AmountToChange")]
        public int AmountToChange { get; set; }
    }
}

﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Profile", Namespace = ""), XmlRoot(ElementName = "Profile"), Serializable]
    public class AccountProfile
    {
        [DataMember(Order = 99, Name = "AccountProfileId"), XmlElement(Order = 0, ElementName = "AccountProfileId")]
        public string AccountProfileId { get; set; }
        
        [DataMember(Order = 99, Name = "AccountId"), XmlElement(Order = 1, ElementName = "AccountId")]
        public string AccountId { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 2, ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "ProfileId"), XmlElement(Order = 3, ElementName = "ProfileId")]
        public string ProfileId { get; set; }
    }
}
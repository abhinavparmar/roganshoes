﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "OrderUpdate", Namespace = ""), Serializable]
    public class OrderUpdate
    {
        [DataMember(Order = 1, Name = "OrderId"), XmlElement(Order = 0, ElementName = "OrderId")]
        public int OrderID { get; set; }

        [DataMember(Order = 2, Name = "OrderStatus"), XmlElement(Order = 0, ElementName = "OrderStatus")]
        public string OrderStatus { get; set; }

        [DataMember(Order = 3, Name = "TrackingNumber"), XmlElement(Order = 0, ElementName = "TrackingNumber")]
        public string TrackingNumber { get; set; }

        [DataMember(Order = 4, Name = "ShipDate"), XmlElement(Order = 0, ElementName = "ShipDate")]
        public DateTime? ShipDate { get; set; }

        [DataMember(Order = 5, Name = "ShippingInfo"), XmlElement(Order = 0, ElementName = "ShippingInfo")]
        public Collection<ShippingInformation> ShippingInfo { get; set; }

        public OrderUpdate()
        {
            ShippingInfo = new Collection<ShippingInformation>();
        }

    }
}

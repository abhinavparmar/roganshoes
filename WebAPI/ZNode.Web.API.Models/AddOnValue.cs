﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "AddOnValue", Namespace = ""), Serializable, XmlRoot(ElementName = "AddOnValue")]
    public class AddOnValue
    {
        [DataMember(Order = 99, Name = "AddOnValueId"), XmlElement(Order = 0, ElementName = "AddOnValueId")]
        public string AddOnValueId { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 1, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "AddOnId"), XmlElement(Order = 2, ElementName = "AddOnId")]
        public string AddOnId { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 3, ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement(Order = 4, ElementName = "Description")]
        public string Description { get; set; }

        [DataMember(Order = 99, Name = "SKU"), XmlElement(Order = 5, ElementName = "SKU")]
        public string SKU { get; set; }

        [DataMember(Order = 99, Name = "DefaultInd"), XmlElement(Order = 6, ElementName = "DefaultInd")]
        public bool DefaultInd { get; set; }

        [DataMember(Order = 99, Name = "DisplayOrder"), XmlElement(Order = 7, ElementName = "DisplayOrder")]
        public int DisplayOrder { get; set; }
        
        [DataMember(Order = 99, Name = "RetailPrice"), XmlElement(Order = 8, ElementName = "RetailPrice")]
        public decimal RetailPrice { get; set; }

        [DataMember(Order = 99, Name = "SalePrice"), XmlElement(Order = 9, ElementName = "SalePrice")]
        public decimal? SalePrice { get; set; }

        [DataMember(Order = 99, Name = "WholesalePrice"), XmlElement(Order = 10, ElementName = "WholesalePrice")]
        public decimal? WholesalePrice { get; set; }

        [DataMember(Order = 99, Name = "Weight"), XmlElement(Order = 11, ElementName = "Weight")]
        public decimal Weight { get; set; }

        [DataMember(Order = 99, Name = "FreeShippingInd"), XmlElement(Order = 12, ElementName = "FreeShippingInd")]
        public bool? FreeShippingInd { get; set; }
    }
}

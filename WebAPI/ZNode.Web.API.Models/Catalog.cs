﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Catalog", Namespace = ""), Serializable, XmlRoot(ElementName = "Catalog")]
    public class Catalog
    {
        [DataMember(Order = 99, Name = "CatalogId"), XmlElement(Order = 0, ElementName = "CatalogId")]
        public string CatalogId { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 1, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 2, ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "IsActive"), XmlElement(Order = 3, ElementName = "IsActive")]
        public bool IsActive { get; set; }
    }
}

﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Account", Namespace = ""), XmlRoot(ElementName = "Account"), Serializable]
    public class Account
    {
        [DataMember(Order = 99, Name = "AccountId"), XmlElement(Order = 0, ElementName = "AccountId")]
        public string AccountId { get; set; }

        [DataMember(Order = 99, Name = "Addresses"), XmlArray(ElementName = "Addresses", IsNullable = true, Order = 1), XmlArrayItem(Type = typeof(AccountAddress), ElementName = "Address", IsNullable = true)]
        public Collection<AccountAddress> Addresses { get; set; }

        [DataMember(Order = 99, Name = "CreateDate"), XmlElement(Order = 2, ElementName = "CreateDate")]
        public DateTime? CreateDate { get; set; }

        [DataMember(Order = 99, Name = "Email"), XmlElement(Order = 3, ElementName = "Email")]
        public string Email { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 4, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "Profiles", EmitDefaultValue = false), XmlElement(Order = 5, ElementName = "Profiles", IsNullable = true)]
        public Collection<AccountProfile> Profiles { get; set; }

        [DataMember(Order = 99, Name = "UpdateDate"), XmlElement(Order = 6, ElementName = "UpdateDate", IsNullable = true)]
        public DateTime? UpdateDate { get; set; }

        public Account()
        {
            Addresses = new Collection<AccountAddress>();
            Profiles = new Collection<AccountProfile>();
        }
    }
}
﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Category", Namespace = ""), Serializable, XmlRoot(ElementName = "Category")]
    public class Category
    {
        [DataMember(Order = 99, Name = "AlternateDescription"), XmlElement(Order = 0, ElementName = "AlternateDescription")]
        public string AlternateDescription { get; set; }

        [DataMember(Order = 99, Name = "CategoryId"), XmlElement(Order = 1, ElementName = "CategoryId")]
        public string CategoryId { get; set; }

        [DataMember(Order = 99, Name = "CategoryNodes"), XmlArray(Order = 2, ElementName = "CategoryNodes", IsNullable = true), XmlArrayItem(Type = typeof(CategoryNode), ElementName = "CategoryNode", IsNullable = true)]
        public Collection<CategoryNode> CategoryNodes { get; set; }

        [DataMember(Order = 99, Name = "CreateDate"), XmlElement(Order = 3, ElementName = "CreateDate", IsNullable = true)]
        public DateTime? CreateDate { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement(Order = 4, ElementName = "Description")]
        public string Description { get; set; }

        [DataMember(Order = 99, Name = "DisplayOrder", EmitDefaultValue = false), XmlElement(Order = 5, ElementName = "Order", IsNullable = true)]
        public int? DisplayOrder { get; set; }

        [DataMember(Order = 99, Name = "Image"), XmlElement(Order = 6, ElementName = "Image")]
        public Image Image { get; set; }

        [DataMember(Order = 99, Name = "IsVisible"), XmlElement(Order = 7, ElementName = "IsVisible")]
        public bool IsVisible { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 8, ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "Seo"), XmlElement(Order = 9, ElementName = "Seo")]
        public Seo Seo { get; set; }

        [DataMember(Order = 99, Name = "ShortDescription"), XmlElement(Order = 10, ElementName = "ShortDescription")]
        public string ShortDescription { get; set; }

        [DataMember(Order = 99, Name = "Subcategories"), XmlArray(Order = 11, ElementName = "Subcategories", IsNullable = true), XmlArrayItem(Type = typeof(Category), ElementName = "Subcategory", IsNullable = true)]
        public Collection<Category> Subcategories { get; set; }

        [DataMember(Order = 99, Name = "Title"), XmlElement(Order = 12, ElementName = "Title")]
        public string Title { get; set; }

        [DataMember(Order = 99, Name = "UpdateDate"), XmlElement(Order = 13, ElementName = "UpdateDate",IsNullable = true)]
        public DateTime? UpdateDate { get; set; }

        public Category()
        {
            CategoryNodes = new Collection<CategoryNode>();
            Image = new Image();
            Seo = new Seo();
            Subcategories = new Collection<Category>();
        }
    }
}
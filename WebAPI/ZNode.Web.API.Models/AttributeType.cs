﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "AttributeType", Namespace = ""), Serializable, XmlRoot(ElementName = "AttributeType")]
    public class AttributeType
    {
        [DataMember(Order = 99, Name = "AttributeTypeId"), XmlElement(Order = 0, ElementName = "AttributeTypeId")]
        public string AttributeTypeId { get; set; }

        [DataMember(Order = 99, Name = "AttributeValues"), XmlArray(Order=1,ElementName = "AttributeValues", IsNullable = true), XmlArrayItem(Type = typeof(AttributeValue), ElementName = "AttributeValue", IsNullable = true)]
        public Collection<AttributeValue> AttributeValues { get; set; }

        [DataMember(Order = 99,Name = "Description"), XmlElement( Order=2,ElementName = "Description")]
        public string Description { get; set; }

        [DataMember(Order = 99,Name = "DisplayName"), XmlElement( Order=3,ElementName = "DisplayName")]
        public string DisplayName { get; set; }

        public AttributeType()
        {
            AttributeValues = new Collection<AttributeValue>();
        }
    }
}
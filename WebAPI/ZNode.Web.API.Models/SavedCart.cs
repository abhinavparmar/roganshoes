﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "SavedCart", Namespace = ""), XmlRoot(ElementName = "SavedCart"), Serializable]
    public class SavedCart
    {
        [DataMember(Order = 99, Name = "SavedCartId"), XmlElement(Order = 0, ElementName = "SavedCartId")]
        public string SavedCartId { get; set; }

        [DataMember(Order = 99, Name = "CookieMappingId"), XmlElement(Order = 1, ElementName = "CookieMappingId")]
        public string CookieMappingId { get; set; }

        [DataMember(Order = 99, Name = "CreateDate"), XmlElement(Order = 2, ElementName = "CreateDate")]
        public DateTime CreateDate { get; set; }

        [DataMember(Order = 99, Name = "LineItems"), XmlArray(Order = 3, ElementName = "LineItems", IsNullable = true), XmlArrayItem(Type = typeof(SavedCartLineItem), ElementName = "SavedCartLineItem", IsNullable = true)]
        public Collection<SavedCartLineItem> SavedCartLineItems { get; set; }

        [DataMember(Order = 99, Name = "UpdateDate"), XmlElement(Order = 4, ElementName = "UpdateDate")]
        public DateTime UpdateDate { get; set; }

        public SavedCart()
        {
            SavedCartLineItems = new Collection<SavedCartLineItem>();
        }
    }
}
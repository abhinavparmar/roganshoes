﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Highlight", Namespace = ""), Serializable, XmlRoot(ElementName = "Highlight")]
    public class Highlight
    {
        [DataMember(Order = 99, Name = "HighlightId"), XmlElement(Order = 0, ElementName = "HighlightId")]
        public string HighlightId { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement(Order = 1, ElementName = "Descriptions")]
        public string Description { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 2, ElementName = "Name")]
        public string Name { get; set; }
    }
}

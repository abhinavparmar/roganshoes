﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Sku", Namespace = ""), Serializable, XmlRoot(ElementName = "Sku")]
    public class Sku
    {
        [DataMember(Order = 99, Name = "SkuId"), XmlElement(Order = 0, ElementName = "SkuId")]
        public string SkuId { get; set; }

        [DataMember(Order = 99, Name = "AttributeTypes"), XmlArray(Order = 1, ElementName = "AttributeTypes", IsNullable = true), XmlArrayItem(Type = typeof(AttributeType), ElementName = "AttributeType", IsNullable = true)]
        public Collection<AttributeType> AttributeTypes { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 2, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "Image"), XmlElement(Order = 3, ElementName = "Image")]
        public Image Image { get; set; }

        [DataMember(Order = 99, Name = "Pricing"), XmlElement(Order = 4, ElementName = "Pricing")]
        public Pricing Pricing { get; set; }

        [DataMember(Order = 99, Name = "ProductId"), XmlElement(Order = 5, ElementName = "ProductId")]
        public string ProductId { get; set; }

        [DataMember(Order = 99, Name = "SKU"), XmlElement(Order = 6, ElementName = "SKU")]
        public string SKU { get; set; }

        public Sku()
        {
            Image = new Image();
            Pricing = new Pricing();
            AttributeTypes = new Collection<AttributeType>();
        }
    }
}
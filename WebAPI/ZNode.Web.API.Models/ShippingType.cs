﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "ShippingType", Namespace = ""), Serializable, XmlRoot(ElementName = "ShippingType")]
    public class ShippingType
    {
        [DataMember(Order = 99, Name = "ShippingTypeId"), XmlElement(Order = 0, ElementName = "ShippingTypeId")]
        public string ShippingTypeId { get; set; }

        [DataMember(Order = 99, Name = "ClassName"), XmlElement(Order = 1, ElementName = "ClassName")]
        public string ClassName { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 2, ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement(Order = 3, ElementName = "Description")]
        public string Description { get; set; }

        [DataMember(Order = 99, Name = "ActiveInd"), XmlElement(Order = 4, ElementName = "ActiveInd")]
        public bool ActiveInd { get; set; }

    }
}

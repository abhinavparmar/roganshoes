﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Profile", Namespace = ""), XmlRoot(ElementName = "Profile"), Serializable]
    public class Profile
    {
        [DataMember(Order = 99, Name = "ProfileId"), XmlElement(Order = 0, ElementName = "ProfileId")]
        public string ProfileId { get; set; }

        [DataMember(Order = 99, Name = "DefaultExternalAccountNo"), XmlElement(Order = 1, ElementName = "DefaultExternalAccountNo")]
        public string DefaultExternalAccountNo { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 2, ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "EmailList"), XmlElement(Order = 3, ElementName = "EmailList")]
        public string EmailList { get; set; }

        [DataMember(Order = 99, Name = "TaxExempt"), XmlElement(Order = 4, ElementName = "TaxExempt")]
        public bool TaxExempt { get; set; }

        [DataMember(Order = 99, Name = "TaxClassId"), XmlElement(Order = 5, ElementName = "TaxClassId")]
        public string TaxClassId { get; set; }

        [DataMember(Order = 99, Name = "ShowPricing"), XmlElement(Order = 6, ElementName = "ShowPricing")]
        public bool ShowPricing { get; set; }

        [DataMember(Order = 99, Name = "ShowOnPartnerSignup"), XmlElement(Order = 7, ElementName = "ShowOnPartnerSignup")]
        public bool ShowOnPartnerSignup { get; set; }

        [DataMember(Order = 99, Name = "Weighting"), XmlElement(Order = 8, ElementName = "Weighting")]
        public decimal? Weighting { get; set; }
    }
}

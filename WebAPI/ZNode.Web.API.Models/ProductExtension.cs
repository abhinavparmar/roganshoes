﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Extension", Namespace = ""),Serializable]
    public class ProductExtension
    {
        [DataMember(Order = 99, Name = "Id"), XmlElement( Order=0,ElementName = "Id")]
        public string Id { get; set; }

        [DataMember(Order = 99, Name = "Attributes"), XmlArray(Order=1,ElementName = "Attributes", IsNullable = true), XmlArrayItem(Type = typeof(AttributeType), ElementName = "AttributeType", IsNullable = true)]
        public Collection<AttributeType> Attributes { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement( Order=2,ElementName = "Description")]
        public string Description { get; set; }

        [DataMember(Order = 99, Name = "EffectiveBegin", EmitDefaultValue = false), XmlElement(Order = 3, ElementName = "EffectiveBegin", IsNullable = true)]
        public DateTime? EffectiveBegin { get; set; }

        [DataMember(Order = 99, Name = "EffectiveEnd", EmitDefaultValue = false), XmlElement(Order = 4, ElementName = "EffectiveEnd", IsNullable = true)]
        public DateTime? EffectiveEnd { get; set; }

        [DataMember(Order = 99, Name = "IsActive", EmitDefaultValue = false), XmlElement(Order = 5, ElementName = "IsActive", IsNullable = true)]
        public bool? IsActive { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement( Order=6,ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "Options"), XmlArray(Order=7,ElementName = "Options", IsNullable = true), XmlArrayItem(Type = typeof(Option), ElementName = "Option", IsNullable = true)]
        public Collection<Option> Options { get; set; }

        [DataMember(Order = 99, Name = "Price", EmitDefaultValue = false), XmlElement(Order = 8, ElementName = "Price", IsNullable = true)]
        public decimal? Price { get; set; }

        [DataMember(Order = 99, Name = "PriceList"), XmlElement( Order=9,ElementName = "PriceList")]
        public PriceList PriceList { get; set; }

        [DataMember(Order = 99, Name = "RelatedExtensions"), XmlArray(Order=10,ElementName = "RelatedExtensions", IsNullable = true), XmlArrayItem(Type = typeof(RelatedExtension), ElementName = "RelatedExtension", IsNullable = true)]
        public Collection<RelatedExtension> RelatedExtensions { get; set; }

        [DataMember(Order = 99, Name = "Sku"), XmlElement( Order=11,ElementName = "Sku")]
        public string Sku { get; set; }

        public ProductExtension()
        {
            PriceList = new PriceList();
            Attributes = new Collection<AttributeType>();
            Options = new Collection<Option>();
            RelatedExtensions = new Collection<RelatedExtension>();
        }
    }
}
﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Option", Namespace = ""), Serializable, XmlRoot(ElementName = "Option")]
    public class Option
    {
        [DataMember(Order = 99, Name = "IsRequired", EmitDefaultValue = false), XmlElement( Order=1,ElementName = "IsRequired", IsNullable = true)]
        public bool? IsRequired { get; set; }

        [DataMember(Order = 99,Name = "LabelName"), XmlElement( Order=2,ElementName = "LabelName")]
        public string LabelName { get; set; }

        [DataMember(Order = 99,Name = "ListData"), XmlElement( Order=3,ElementName = "ListData")]
        public string ListData { get; set; }

        [DataMember(Order = 99, Name = "OptionType", EmitDefaultValue = false), XmlElement( Order=4,ElementName = "OptionType", IsNullable = true)]
        public int? OptionType { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "OrderState", Namespace = ""), Serializable, XmlRoot(ElementName = "OrderState")]
    public class OrderState
    {
        [DataMember(Order = 99, Name = "OrderStateId"), XmlElement(Order = 0, ElementName = "OrderStateId")]
        public string OrderStateId { get; set; }

        [DataMember(Order = 99, Name = "OrderStateName"), XmlElement(Order = 1, ElementName = "OrderStateName")]
        public string OrderStateName { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement(Order = 2, ElementName = "Description")]
        public string Description { get; set; }
    }
}

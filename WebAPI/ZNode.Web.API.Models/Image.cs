﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name="Image", Namespace=""),Serializable,XmlRoot(ElementName = "Image")]
    public class Image
    {
        [DataMember(Order = 99, Name = "AltTag"), XmlElement(Order = 1, ElementName = "AltTag")]
        public string AltTag { get; set; }
        
        [DataMember(Order = 99, Name = "Path"), XmlElement( Order = 2,ElementName = "Path")]
        public string Path { get; set; }
    }
}
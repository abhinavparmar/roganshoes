﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "TieredPricing", Namespace = ""), Serializable, XmlRoot(ElementName = "TieredPricing")]
    public class TieredPricing
    {
        [DataMember(Order = 99, Name = "TieredPricingId"), XmlElement(Order = 0, ElementName = "TieredPricingId")]
        public string TieredPricingId { get; set; }

        [DataMember(Order = 99, Name = "ProductId"), XmlElement(Order = 1, ElementName = "ProductId")]
        public string ProductId { get; set; }

        [DataMember(Order = 99, Name = "ProfileId"), XmlElement(Order = 2, ElementName = "ProfileId")]
        public string ProfileId { get; set; }

        [DataMember(Order = 99, Name = "TierStart"), XmlElement(Order = 3, ElementName = "TierStart")]
        public int TierStart { get; set; }

        [DataMember(Order = 99, Name = "TierEnd"), XmlElement(Order = 4, ElementName = "TierEnd")]
        public int TierEnd { get; set; }

        [DataMember(Order = 99, Name = "Price"), XmlElement(Order = 5, ElementName = "Price")]
        public decimal Price { get; set; }
    }
}

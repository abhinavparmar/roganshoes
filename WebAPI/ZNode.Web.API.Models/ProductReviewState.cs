﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "ProductReviewState", Namespace = ""), XmlRoot(ElementName = "ProductReviewState"), Serializable]
    public class ProductReviewState
    {
        [DataMember(Order = 99, Name = "ReviewStateId"), XmlElement(Order = 0, ElementName = "ReviewStateId")]
        public string ReviewStateId { get; set; }

        [DataMember(Order = 99, Name = "ReviewStateName"), XmlElement(Order = 1, ElementName = "ReviewStateName")]
        public string ReviewStateName { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement(Order = 2, ElementName = "Description")]
        public string Description { get; set; }
    }
}

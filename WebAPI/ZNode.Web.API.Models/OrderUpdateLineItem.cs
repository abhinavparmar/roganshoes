﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "OrderLineItemUpdate", Namespace = ""), Serializable]
    public class OrderUpdateLineItem
    {
        [DataMember(Order = 1, Name = "OrderId"), XmlElement(Order = 0, ElementName = "OrderId")]
        public int OrderID { get; set; }

        [DataMember(Order = 2, Name = "ZNodeSKU", EmitDefaultValue = false), XmlElement(Order = 1, ElementName = "ZNodeSKU", IsNullable = true)]
        public string ZNodeSKU { get; set; }

        [DataMember(Order = 3, Name = "LineItemTrackingNumber", EmitDefaultValue = false), XmlElement(Order = 1, ElementName = "LineItemTrackingNumber", IsNullable = true)]
        public string LineItemTrackingNumber { get; set; }

        [DataMember(Order = 4, Name = "LineItemShipServiceName", EmitDefaultValue = false), XmlElement(Order = 1, ElementName = "LineItemShipServiceName", IsNullable = true)]
        public string LineItemShipServiceName { get; set; }

        [DataMember(Order = 5, Name = "LineItemTrasactionID", EmitDefaultValue = false), XmlElement(Order = 1, ElementName = "LineItemTrasactionID", IsNullable = true)]
        public string LineItemTrasactionID { get; set; }
    }
}

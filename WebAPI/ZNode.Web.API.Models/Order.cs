﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Order", Namespace = ""), Serializable]
    public class Order
    {
        [DataMember(Order = 99, Name = "OrderId"), XmlElement(Order = 0, ElementName = "OrderId")]
        public string OrderId { get; set; }

        [DataMember(Order = 99, Name = "AccountId"), XmlElement(Order = 1, ElementName = "AccountId")]
        public string AccountId { get; set; }

        [DataMember(Order = 99, Name = "BillingInformation"), XmlElement(Order = 2, ElementName = "BillingInformation")]
        public Address BillingInformation { get; set; }

        [DataMember(Order = 99, Name = "CreateDate"), XmlElement(Order = 3, ElementName = "CreateDate")]
        public DateTime? CreateDate { get; set; }

        [DataMember(Order = 99, Name = "Discount", EmitDefaultValue = false), XmlElement(Order = 4, ElementName = "Discount", IsNullable = true)]
        public decimal? Discount { get; set; }

        [DataMember(Order = 99, Name = "Email", EmitDefaultValue = false), XmlElement(Order = 5, ElementName = "Email", IsNullable = true)]
        public string Email { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 6, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "OrderLineItems"), XmlArray(Order = 7, ElementName = "OrderLineItems", IsNullable = true), XmlArrayItem(Type = typeof(OrderLineItem), ElementName = "OrderLineItem", IsNullable = true)]
        public Collection<OrderLineItem> OrderLineItems { get; set; }

        [DataMember(Order = 99, Name = "OrderStateId", EmitDefaultValue = false), XmlElement(Order = 8, ElementName = "OrderStateId", IsNullable = true)]
        public string OrderStateId { get; set; }

        [DataMember(Order = 99, Name = "PaymentInformation", EmitDefaultValue = false), XmlElement(Order = 9, ElementName = "PaymentInformation", IsNullable = true)]
        public PaymentInformation PaymentInformation { get; set; }

        [DataMember(Order = 99, Name = "TaxCost", EmitDefaultValue = false), XmlElement(Order = 10, ElementName = "TaxCost", IsNullable = true)]
        public decimal? SalesTax { get; set; }

        [DataMember(Order = 99, Name = "ShippingInformation"), XmlElement(Order = 11, ElementName = "ShippingInformation")]
        public Address ShippingInformation { get; set; }

        [DataMember(Order = 99, Name = "SavedCartId"), XmlElement(Order = 12, ElementName = "SavedCartId")]
        public string SavedCartId { get; set; }

        [DataMember(Order = 99, Name = "Subtotal", EmitDefaultValue = false), XmlElement(Order = 13, ElementName = "Subtotal", IsNullable = true)]
        public decimal? Subtotal { get; set; }

        [DataMember(Order = 99, Name = "Total", EmitDefaultValue = false), XmlElement(Order = 14, ElementName = "Total", IsNullable = true)]
        public decimal? Total { get; set; }

        [DataMember(Order = 99, Name = "TrackingNumber"), XmlElement(Order = 15, ElementName = "TrackingNumber")]
        public string TrackingNumber { get; set; }

        [DataMember(Order = 99, Name = "PNRefCode"), XmlElement(Order = 16, ElementName = "PNRefCode")]
        public string TransactionId { get; set; }

        [DataMember(Order = 99, Name = "UpdateDate"), XmlElement(Order = 17, ElementName = "UpdateDate")]
        public DateTime? UpdateDate { get; set; }

        #region Zeon Custom Code

        [DataMember(Order = 99, Name = "PortalID"), XmlElement(Order = 18, ElementName = "PortalID")]
        public string PortalID { get; set; }

        [DataMember(Order = 99, Name = "PaymentType"), XmlElement(Order = 19, ElementName = "PaymentType")]
        public string PaymentType { get; set; }

        [DataMember(Order = 99, Name = "ShippingMethod"), XmlElement(Order = 20, ElementName = "ShippingMethod")]
        public string ShippingMethod { get; set; }

        [DataMember(Order = 99, Name = "Shipper"), XmlElement(Order = 21, ElementName = "Shipper")]
        public string Shipper { get; set; }

        [DataMember(Order = 99, Name = "CreditCardAmount"), XmlElement(Order = 22, ElementName = "CreditCardAmount", IsNullable = true)]
        public decimal? CreditCardAmount { get; set; }

        [DataMember(Order = 99, Name = "AuthorizationCode"), XmlElement(Order = 23, ElementName = "AuthorizationCode")]
        public string AuthorizationCode { get; set; }

        [DataMember(Order = 99, Name = "Notes"), XmlElement(Order = 24, ElementName = "Notes")]
        public string Notes { get; set; }

        [DataMember(Order = 99, Name = "OrderDate"), XmlElement(Order = 25, ElementName = "OrderDate", IsNullable = true)]
        public DateTime? OrderDate { get; set; }

        [DataMember(Order = 99, Name = "PromoCode"), XmlElement(Order = 26, ElementName = "PromoCode")]
        public string PromoCode { get; set; }

        [DataMember(Order = 99, Name = "PurchaseOrder"), XmlElement(Order = 27, ElementName = "PurchaseOrder")]
        public string PurchaseOrder { get; set; }

        [DataMember(Order = 99, Name = "TotalShippingCost"), XmlElement(Order = 28, ElementName = "TotalShippingCost", IsNullable = true)]
        public decimal? TotalShippingCost { get; set; }

        [DataMember(Order = 99, Name = "CreditCardNumber"), XmlElement(Order = 29, ElementName = "CreditCardNumber")]
        public string CreditCardNumber { get; set; }

        [DataMember(Order = 99, Name = "CreditCardType"), XmlElement(Order = 30, ElementName = "CreditCardType")]
        public string CreditCardType { get; set; }

        [DataMember(Order = 99, Name = "OrderStatus"), XmlElement(Order = 31, ElementName = "OrderStatus")]
        public string OrderStatus { get; set; }

        //Perficient Custom Properties:Start
        #region[Code Commented: Waraping it in collection instead on individual property]
        /*
        [DataMember(Order = 99, Name = "PNRefCode1"), XmlElement(Order = 32, ElementName = "PNRefCode1")]
        public string Card1TransactionID { get; set; }

        [DataMember(Order = 99, Name = "CreditCard1AuthCode"), XmlElement(Order = 33, ElementName = "Card1AuthCode")]
        public string Card1AuthCode { get; set; }

        [DataMember(Order = 99, Name = "CreditCard1Number"), XmlElement(Order = 35, ElementName = "Card1Number")]
        public string Card1Number { get; set; }

        [DataMember(Order = 99, Name = "CreditCard1Type"), XmlElement(Order = 36, ElementName = "Card1Type")]
        public string Card1Type { get; set; }

        [DataMember(Order = 99, Name = "CreditCard1Amount"), XmlElement(Order = 37, ElementName = "Card1Amount")]
        public decimal? Card1Amount { get; set; }

        [DataMember(Order = 99, Name = "PNRefCode2"), XmlElement(Order = 38, ElementName = "Card2TransactionID")]
        public string Card2TransactionID { get; set; }

        [DataMember(Order = 99, Name = "CreditCard2AuthCode"), XmlElement(Order = 39, ElementName = "Card2AuthCode")]
        public string Card2AuthCode { get; set; }

        [DataMember(Order = 99, Name = "CreditCard2Number"), XmlElement(Order = 41, ElementName = "Card2Number")]
        public string Card2Number { get; set; }

        [DataMember(Order = 99, Name = "CreditCard2Type"), XmlElement(Order = 42, ElementName = "Card2Type")]
        public string Card2Type { get; set; }

        [DataMember(Order = 99, Name = "CreditCard2Amount"), XmlElement(Order = 43, ElementName = "Card2Amount")]
        public decimal? Card2Amount { get; set; }
        */

        #endregion

        //07/27/15:Adding new property to contail list of all the credit card used when order is placed.
        [DataMember(Order = 99, Name = "CreditCardList", EmitDefaultValue = false), XmlElement(Order = 1, ElementName = "CreditCardList", IsNullable = true)]
        public Collection<CardInformation> CreditCardList { get; set; }

        //Perficient Custom Properties:Ends

       

        #endregion

        public Order()
        {
            BillingInformation = new Address();
            ShippingInformation = new Address();
            OrderLineItems = new Collection<OrderLineItem>();
            CreditCardList = new Collection<CardInformation>();
        }
    }
}
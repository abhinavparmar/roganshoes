﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "PortalCatalog", Namespace = ""), Serializable, XmlRoot(ElementName = "PortalCatalog")]
    public class PortalCatalog
    {
        [DataMember(Order = 99, Name = "PortalCatalogId"), XmlElement(Order = 0, ElementName = "PortalCatalogId")]
        public string PortalCatalogId { get; set; }

        [DataMember(Order = 99, Name = "PortalId"), XmlElement(Order = 1, ElementName = "PortalId")]
        public string PortalId { get; set; }

        [DataMember(Order = 99, Name = "CatalogId"), XmlElement(Order = 2, ElementName = "CatalogId")]
        public string CatalogId { get; set; }

        [DataMember(Order = 99, Name = "Theme"), XmlElement(Order = 3, ElementName = "Theme")]
        public string Theme { get; set; }

        [DataMember(Order = 99, Name = "CSS"), XmlElement(Order = 4, ElementName = "CSS")]
        public string CSS { get; set; }

    }
}

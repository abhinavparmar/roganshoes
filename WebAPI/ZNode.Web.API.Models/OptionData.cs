﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "OptionDataItem", Namespace = ""), Serializable,XmlRoot(ElementName = "OptionDataItem")]
    public class OptionData
    {
        [DataMember(Order = 99,Name = "Data"), XmlElement( Order=1,ElementName = "Data")]
        public string Data { get; set; }

        [DataMember(Order = 99,Name = "Option"), XmlElement( Order=2,ElementName = "Option")]
        public string Option { get; set; }
    }
}
﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Inventory", Namespace = ""), Serializable, XmlRoot(ElementName = "Inventory")]
    public class Inventory
    {
        [DataMember(Order = 99, Name = "SkuInventoryId"), XmlElement(Order = 0, ElementName = "SkuInventoryId")]
        public string SkuInventoryId { get; set; }

        [DataMember(Order = 99, Name = "Sku"), XmlElement(Order = 1, ElementName = "Sku")]
        public string Sku { get; set; }

        [DataMember(Order = 99, Name = "QuantityOnHand", EmitDefaultValue = false), XmlElement(Order = 2, ElementName = "QuantityOnHand", IsNullable = true)]
        public int? QuantityOnHand { get; set; }
    }
}

﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "AttributeValue", Namespace = ""), Serializable, XmlRoot(ElementName = "AttributeValue")]
    public class AttributeValue
    {
        [DataMember(Order = 99, Name = "AttributeValueId"), XmlElement(Order = 0, ElementName = "AttributeValueId")]
        public string AttributeValueId { get; set; }

        [DataMember(Order = 99,Name = "DisplayName"), XmlElement( Order=1,ElementName = "DisplayName")]
        public string DisplayName { get; set; }

        [DataMember(Order = 99,Name = "IsActive"), XmlElement( Order=2,ElementName = "IsActive")]
        public bool IsActive { get; set; }

        [DataMember(Order = 99,Name = "Order"), XmlElement( Order=3,ElementName = "Order")]
        public int Order { get; set; }
    }
}
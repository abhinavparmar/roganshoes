﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "RecurringBilling", Namespace = ""),Serializable]
    public class RecurringBilling
    {
        [DataMember(Order = 99, Name = "Frequency"), XmlElement( Order=1,ElementName = "Frequency")]
        public string Frequency { get; set; }

        [DataMember(Order = 99, Name = "InitialAmount",EmitDefaultValue = false), XmlElement( Order=2,ElementName = "InitialAmount",IsNullable = true)]
        public decimal? InitialAmount { get; set; }

        [DataMember(Order = 99, Name = "IsRecurringInstallment"), XmlElement( Order=3,ElementName = "IsRecurringInstallment")]
        public bool IsRecurringInstallment { get; set; }

        [DataMember(Order = 99, Name = "IsRecurringPayment"), XmlElement( Order=4,ElementName = "IsRecurringPayment")]
        public bool IsRecurringPayment { get; set; }

        [DataMember(Order = 99, Name = "Period"), XmlElement( Order=5,ElementName = "Period")]
        public string Period { get; set; }

        [DataMember(Order = 99, Name = "TotalCycles",EmitDefaultValue = false), XmlElement( Order=6,ElementName = "TotalCycles",IsNullable = true)]
        public int? TotalCycles { get; set; }
    }
}
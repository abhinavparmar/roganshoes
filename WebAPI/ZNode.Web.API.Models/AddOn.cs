﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "AddOn", Namespace = ""), Serializable, XmlRoot(ElementName = "AddOn")]
    public class AddOn
    {
        [DataMember(Order = 99, Name = "AddOnId"), XmlElement(Order = 0, ElementName = "AddOnId")]
        public string AddOnId { get; set; }

        [DataMember(Order = 99, Name = "ExternalAPIID"), XmlElement(Order = 1, ElementName = "ExternalAPIID")]
        public string ExternalAPIID { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement(Order = 2, ElementName = "Description")]
        public string Description { get; set; }
        
        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 3, ElementName = "Name")]
        public string Name { get; set; }
        
        [DataMember(Order = 99, Name = "Title"), XmlElement(Order = 5, ElementName = "Title")]
        public string Title { get; set; }

        [DataMember(Order = 99, Name = "OptionalInd"), XmlElement(Order = 6, ElementName = "OptionalInd")]
        public bool OptionalInd { get; set; }

        [DataMember(Order = 99, Name = "AllowBackOrder"), XmlElement(Order = 7, ElementName = "AllowBackOrder")]
        public bool AllowBackOrder { get; set; }

        [DataMember(Order = 99, Name = "TrackInventoryInd"), XmlElement(Order = 8, ElementName = "TrackInventoryInd")]
        public bool TrackInventoryInd { get; set; }

        [DataMember(Order = 99, Name = "AddOnValues"), XmlArray(ElementName = "AddOnValues", IsNullable = true, Order = 9), XmlArrayItem(Type = typeof(AddOnValue), ElementName = "AddOnValue", IsNullable = true)]
        public Collection<AddOnValue> AddOnValues { get; set; }
    }
}
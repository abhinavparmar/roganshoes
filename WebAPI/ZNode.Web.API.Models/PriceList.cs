﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "PriceList", Namespace = ""),Serializable]
    public class PriceList
    {
        [DataMember(Order = 99, Name = "Locale"), XmlElement( Order=1,ElementName = "Locale")]
        public string Locale { get; set; }

        [DataMember(Order = 99,Name = "Name"),XmlElement( Order=2,ElementName = "Order")]
        public string Name { get; set; }
    }
}
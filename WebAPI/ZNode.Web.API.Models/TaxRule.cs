﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "TaxRule", Namespace = ""), Serializable, XmlRoot(ElementName = "TaxRule")]
    public class TaxRule
    {
        [DataMember(Order = 99, Name = "TaxRuleId"), XmlElement(Order = 0, ElementName = "TaxRuleId")]
        public string TaxRuleId { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 1, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "TaxRuleTypeId"), XmlElement(Order = 2, ElementName = "TaxRuleTypeId")]
        public string TaxRuleTypeId { get; set; }

        [DataMember(Order = 99, Name = "PortalId"), XmlElement(Order = 3, ElementName = "PortalId")]
        public string PortalId { get; set; }

        [DataMember(Order = 99, Name = "TaxClassId"), XmlElement(Order = 4, ElementName = "TaxClassId")]
        public string TaxClassId { get; set; }

        [DataMember(Order = 99, Name = "DestinationCountryCode"), XmlElement(Order = 5, ElementName = "DestinationCountryCode")]
        public string DestinationCountryCode { get; set; }

        [DataMember(Order = 99, Name = "DestinationStateCode"), XmlElement(Order = 6, ElementName = "DestinationStateCode")]
        public string DestinationStateCode { get; set; }

        [DataMember(Order = 99, Name = "CountyFIPS"), XmlElement(Order = 7, ElementName = "CountyFIPS")]
        public string CountyFIPS { get; set; }

        [DataMember(Order = 99, Name = "Precedence"), XmlElement(Order = 8, ElementName = "Precedence")]
        public int Precedence { get; set; }

        [DataMember(Order = 99, Name = "SalesTax"), XmlElement(Order = 9, ElementName = "SalesTax")]
        public decimal? SalesTax { get; set; }

        [DataMember(Order = 99, Name = "VAT"), XmlElement(Order = 10, ElementName = "VAT")]
        public decimal? VAT { get; set; }

        [DataMember(Order = 99, Name = "GST"), XmlElement(Order = 11, ElementName = "GST")]
        public decimal? GST { get; set; }

        [DataMember(Order = 99, Name = "PST"), XmlElement(Order = 12, ElementName = "PST")]
        public decimal? PST { get; set; }

        [DataMember(Order = 99, Name = "HST"), XmlElement(Order = 13, ElementName = "HST")]
        public decimal? HST { get; set; }

        [DataMember(Order = 99, Name = "TaxShipping"), XmlElement(Order = 14, ElementName = "TaxShipping")]
        public bool TaxShipping { get; set; }

        [DataMember(Order = 99, Name = "InclusiveInd"), XmlElement(Order = 15, ElementName = "InclusiveInd")]
        public bool InclusiveInd { get; set; }
        
    }
}

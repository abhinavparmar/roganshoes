﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Portal", Namespace = ""), Serializable, XmlRoot(ElementName = "Portal")]
    public class Portal
    {
        [DataMember(Order = 99, Name = "PortalId"), XmlElement(Order = 0, ElementName = "PortalId")]
        public string PortalId { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 1, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "CompanyName"), XmlElement(Order = 2, ElementName = "CompanyName")]
        public string CompanyName { get; set; }

        [DataMember(Order = 99, Name = "StoreName"), XmlElement(Order = 3, ElementName = "StoreName")]
        public string StoreName { get; set; }

        [DataMember(Order = 99, Name = "LogoPath"), XmlElement(Order = 4, ElementName = "LogoPath")]
        public string LogoPath { get; set; }

        [DataMember(Order = 99, Name = "UseSSL"), XmlElement(Order = 5, ElementName = "UseSSL")]
        public bool UseSSL { get; set; }

        [DataMember(Order = 99, Name = "AdminEmail"), XmlElement(Order = 6, ElementName = "AdminEmail")]
        public string AdminEmail { get; set; }

        [DataMember(Order = 99, Name = "SalesEmail"), XmlElement(Order = 7, ElementName = "SalesEmail")]
        public string SalesEmail { get; set; }

        [DataMember(Order = 99, Name = "CustomerServiceEmail"), XmlElement(Order = 8, ElementName = "CustomerServiceEmail")]
        public string CustomerServiceEmail { get; set; }

        [DataMember(Order = 99, Name = "SalesPhoneNumber"), XmlElement(Order = 9, ElementName = "SalesPhoneNumber")]
        public string SalesPhoneNumber { get; set; }

        [DataMember(Order = 99, Name = "CustomerServicePhoneNumber"), XmlElement(Order = 10, ElementName = "CustomerServicePhoneNumber")]
        public string CustomerServicePhoneNumber { get; set; }

        [DataMember(Order = 99, Name = "DefaultOrderStateId"), XmlElement(Order = 11, ElementName = "DefaultOrderState")]
        public string DefaultOrderStateId { get; set; }

        [DataMember(Order = 99, Name = "DefaultReviewStatus"), XmlElement(Order = 12, ElementName = "DefaultReviewStatus")]
        public string DefaultReviewStatus { get; set; }

        [DataMember(Order = 99, Name = "InclusiveTax"), XmlElement(Order = 13, ElementName = "InclusiveTax")]
        public bool InclusiveTax { get; set; }

        [DataMember(Order = 99, Name = "PersistentCartEnabled"), XmlElement(Order = 14, ElementName = "PersistentCartEnabled")]
        public bool? PersistentCartEnabled { get; set; }

        [DataMember(Order = 99, Name = "DefaultProductReviewStateId"), XmlElement(Order = 15, ElementName = "DefaultProductReviewState")]
        public string DefaultProductReviewStateId { get; set; }

        [DataMember(Order = 99, Name = "EnableAddressValidation"), XmlElement(Order = 16, ElementName = "EnableAddressValidation")]
        public bool? EnableAddressValidation { get; set; }

        [DataMember(Order = 99, Name = "RequireValidatedAddress"), XmlElement(Order = 17, ElementName = "RequireValidatedAddress")]
        public bool? RequireValidatedAddress { get; set; }

        [DataMember(Order = 99, Name = "EnablePIMS"), XmlElement(Order = 18, ElementName = "EnablePIMS")]
        public bool? EnablePIMS { get; set; }

        [DataMember(Order = 99, Name = "PortalCatalogs"), XmlElement(Order = 19, ElementName = "PortalCatalogs", IsNullable = false), XmlArrayItem(Type = typeof(PortalCatalog), ElementName = "PortalCatalog", IsNullable = true)]
        public Collection<PortalCatalog> PortalCatalogs { get; set; }
    }
}

﻿#region Using Directives
using System;
#endregion

namespace ZNode.Web.API.Models.Messaging.Categories
{
    public class CategoryNodePayloadRequest : CreateRequest
    {
        public CategoryNode CategoryNode;
        public string Id;
    }
}

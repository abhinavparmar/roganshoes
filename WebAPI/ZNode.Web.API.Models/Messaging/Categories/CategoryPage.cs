﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Categories
{
    [DataContract(Name = "CategoryPage", Namespace = ""),Serializable]
    public class CategoryPage : Page<Category>
    {
        public CategoryPage() { }
        public CategoryPage(int page, int pageSize, int totalItems, IEnumerable<Category> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
﻿#region Using Directives
using System;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Categories
{
    public class CategoryNodeSaveRequest : SaveRequest
    {
        public string CategoryId { get; set; }
        public string IdType { get; set; }
    }
}

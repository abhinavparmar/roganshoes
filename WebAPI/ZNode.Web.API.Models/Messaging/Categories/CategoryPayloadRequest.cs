﻿#region Using Directives
using System;
#endregion

namespace ZNode.Web.API.Models.Messaging.Categories
{
    public class CategoryPayloadRequest : CreateRequest
    {
        public Category Category;
    }
}

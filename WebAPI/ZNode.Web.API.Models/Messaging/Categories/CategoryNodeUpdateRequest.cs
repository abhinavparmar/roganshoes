﻿#region Using Directives
using System;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Categories
{
    public class CategoryNodeUpdateRequest : UpdateRequest
    {
        public string CategoryId { get; set; }
        public string IdType { get; set; }
    }
}

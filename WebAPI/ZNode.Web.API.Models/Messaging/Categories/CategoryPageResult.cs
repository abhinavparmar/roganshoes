﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Categories
{
    [DataContract(Name = "CategoryPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "CategoryPageResult")]
    public class CategoryPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "CategoryPage"), XmlElement(Order = 0, ElementName = "CategoryPage")]
        public CategoryPage CategoryPage { get; set; }

        public CategoryPageResult() { }
        public CategoryPageResult(int page, int pageSize, int totalItems, IEnumerable<Category> items)
        {
            CategoryPage = new CategoryPage(page, pageSize, totalItems, items);
        }
    }
}

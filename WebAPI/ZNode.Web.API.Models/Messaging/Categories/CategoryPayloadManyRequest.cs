﻿#region Using Directives
using System;
using System.Collections.ObjectModel;
#endregion

namespace ZNode.Web.API.Models.Messaging.Categories
{
    public class CategoryPayloadManyRequest : CreateRequest
    {
        public Collection<Category> Categories;
    }
}

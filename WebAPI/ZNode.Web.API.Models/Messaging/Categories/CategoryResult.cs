﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Categories
{
    [DataContract(Name = "CategoryResult", Namespace = ""), Serializable, XmlRoot(ElementName = "CategoryResult")]
    public class CategoryResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Category"), XmlElement(Order = 0, ElementName = "Category")]
        public Category Category { get; set; }
    }
}

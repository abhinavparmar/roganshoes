﻿#region Using Directives
using System;
using System.Collections.ObjectModel;
#endregion

namespace ZNode.Web.API.Models.Messaging.Categories
{
    public class CategoryNodePayloadManyRequest : CreateRequest
    {
        public Collection<CategoryNode> CategoryNodes;
        public string Id;
    }
}

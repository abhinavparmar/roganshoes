﻿#region Using Directives
using System;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    public class OrderSaveRequest : SaveRequest
    {
        public string SavedCartId { get; set; }
    }
}
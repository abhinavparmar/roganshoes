﻿#region Using Directives
using System.Collections.Generic;
using System.Runtime.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    [DataContract(Name = "OrderPage", Namespace = "")]
    public class OrderPage : Page<Order>
    {
        public OrderPage() { }
        public OrderPage(int page, int pageSize, int totalItems, IEnumerable<Order> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
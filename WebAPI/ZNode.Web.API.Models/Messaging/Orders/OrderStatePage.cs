﻿#region Using Directives
using System.Collections.Generic;
using System.Runtime.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    [DataContract(Name = "OrderStatePage", Namespace = "")]
    public class OrderStatePage : Page<OrderState>
    {
        public OrderStatePage() { }
        public OrderStatePage(int page, int pageSize, int totalItems, IEnumerable<OrderState> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    [DataContract(Name = "OrderResult", Namespace = ""), Serializable, XmlRoot(ElementName = "OrderResult")]
    public class OrderResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Order"), XmlElement(Order = 0, ElementName = "Order")]
        public Order Order { get; set; }
    }
}

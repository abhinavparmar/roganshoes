﻿#region Using Directives
using System;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    public class OrderListRequest : ListRequest
    {
        //Zeon Custom Code : Start
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string OrderStatus { get; set; }
        //Zeon Custom Code : End
    }
}
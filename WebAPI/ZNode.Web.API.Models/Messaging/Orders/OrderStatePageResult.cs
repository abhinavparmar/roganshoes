﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    [DataContract(Name = "OrderStatePageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "OrderStatePageResult")]
    public class OrderStatePageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "OrderStatePage"), XmlElement(Order = 0, ElementName = "OrderStatePage")]
        public OrderStatePage OrderStatePage { get; set; }

        public OrderStatePageResult() { }
        public OrderStatePageResult(int page, int pageSize, int totalItems, IEnumerable<OrderState> items)
        {
            OrderStatePage = new OrderStatePage(page, pageSize, totalItems, items);
        }
    }
}

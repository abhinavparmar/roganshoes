﻿#region Using Directives
using System;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    public class OrderLineItemUpdateRequest : UpdateRequest
    {
        public string OrderId { get; set; }
        public string IdType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    [DataContract(Name = "OrderUpdateResult", Namespace = ""), Serializable, XmlRoot(ElementName = "OrderUpdateResult")]
    public class OrderUpdateResult : ResultBase
    {
        [DataMember(Order = 99, Name = "OrderUpdate"), XmlElement(Order = 0, ElementName = "OrderUpdate")]
        public OrderUpdate OrderUpdate { get; set; }
    }
}

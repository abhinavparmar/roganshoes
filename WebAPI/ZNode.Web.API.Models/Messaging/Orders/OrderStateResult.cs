﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    [DataContract(Name = "OrderStateResult", Namespace = ""), Serializable, XmlRoot(ElementName = "OrderStateResult")]
    public class OrderStateResult : ResultBase
    {
        [DataMember(Order = 99, Name = "OrderState"), XmlElement(Order = 0, ElementName = "OrderState")]
        public OrderState OrderState { get; set; }
    }
}

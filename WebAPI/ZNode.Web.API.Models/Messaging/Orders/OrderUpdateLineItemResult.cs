﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    [DataContract(Name = "OrderUpdateLineItemResult", Namespace = ""), Serializable, XmlRoot(ElementName = "OrderUpdateLineItemResult")]
    public class OrderUpdateLineItemResult : ResultBase
    {
        [DataMember(Order = 1, Name = "OrderUpdateLineItem"), XmlElement(Order = 0, ElementName = "OrderUpdateLineItem")]
        public Collection<OrderUpdateLineItem> OrderUpdateLineItem { get; set; }
    }
}

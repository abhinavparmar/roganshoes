﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    public class OrderUpdateLineItemRequest : UpdateRequest
    {
        public string OrderID { get; set; }
    }
}

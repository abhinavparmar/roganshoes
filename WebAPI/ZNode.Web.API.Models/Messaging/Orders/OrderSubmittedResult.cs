﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    [DataContract(Name = "OrderSubmittedResult", Namespace = ""), Serializable, XmlRoot(ElementName = "OrderSubmittedResult")]
    public class OrderSubmittedResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Order"), XmlElement(Order = 0, ElementName = "Order")]
        public Order Order { get; set; }

        [DataMember(Order = 99, Name = "OrderReceipt"), XmlElement(Order = 0, ElementName = "OrderReceipt")]
        public string OrderReceipt { get; set; }
    }
}

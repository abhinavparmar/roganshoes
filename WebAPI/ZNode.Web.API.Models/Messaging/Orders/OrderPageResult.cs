﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Orders
{
    [DataContract(Name = "OrderPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "OrderPageResult")]
    public class OrderPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "OrderPage"), XmlElement(Order = 0, ElementName = "OrderPage")]
        public OrderPage OrderPage { get; set; }

        public OrderPageResult() { }
        public OrderPageResult(int page, int pageSize, int totalItems, IEnumerable<Order> items)
        {
            OrderPage = new OrderPage(page, pageSize, totalItems, items);
        }
    }
}

﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingPage", Namespace = ""), Serializable]
    public class ShippingPage : Page<Models.Shipping>
    {
        public ShippingPage() { }
        public ShippingPage(int page, int pageSize, int totalItems, IEnumerable<Models.Shipping> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
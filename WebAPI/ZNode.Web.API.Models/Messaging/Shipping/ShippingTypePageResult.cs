﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingTypePageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ShippingTypePageResult")]
    public class ShippingTypePageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "ShippingTypePage"), XmlElement(Order = 0, ElementName = "ShippingTypePage")]
        public ShippingTypePage ShippingTypePage { get; set; }

        public ShippingTypePageResult() { }
        public ShippingTypePageResult(int page, int pageSize, int totalItems, IEnumerable<Models.ShippingType> items)
        {
            ShippingTypePage = new ShippingTypePage(page, pageSize, totalItems, items);
        }
    }
}

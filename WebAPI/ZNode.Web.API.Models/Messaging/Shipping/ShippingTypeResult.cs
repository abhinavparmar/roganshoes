﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingTypeResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ShippingTypeResult")]
    public class ShippingTypeResult : ResultBase
    {
        [DataMember(Order = 99, Name = "ShippingType"), XmlElement(Order = 0, ElementName = "ShippingType")]
        public ShippingType ShippingType { get; set; }
    }
}
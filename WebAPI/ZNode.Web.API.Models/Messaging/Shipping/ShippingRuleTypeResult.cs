﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingRuleTypeResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ShippingRuleTypeResult")]
    public class ShippingRuleTypeResult : ResultBase
    {
        [DataMember(Order = 99, Name = "ShippingRuleType"), XmlElement(Order = 0, ElementName = "ShippingRuleType")]
        public ShippingRuleType ShippingRuleType { get; set; }
    }
}
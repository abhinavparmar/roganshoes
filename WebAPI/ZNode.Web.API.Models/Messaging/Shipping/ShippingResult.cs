﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ShippingResult")]
    public class ShippingResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Shipping"), XmlElement(Order = 0, ElementName = "Shipping")]
        public Models.Shipping Shipping { get; set; }
    }
}
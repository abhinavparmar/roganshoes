﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingRuleTypePageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ShippingRuleTypePageResult")]
    public class ShippingRuleTypePageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "ShippingRuleTypePage"), XmlElement(Order = 0, ElementName = "ShippingRuleTypePage")]
        public ShippingRuleTypePage ShippingRuleTypePage { get; set; }

        public ShippingRuleTypePageResult() { }
        public ShippingRuleTypePageResult(int page, int pageSize, int totalItems, IEnumerable<Models.ShippingRuleType> items)
        {
            ShippingRuleTypePage = new ShippingRuleTypePage(page, pageSize, totalItems, items);
        }
    }
}

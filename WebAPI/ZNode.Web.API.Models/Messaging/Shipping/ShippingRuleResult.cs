﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingRuleResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ShippingRuleResult")]
    public class ShippingRuleResult : ResultBase
    {
        [DataMember(Order = 99, Name = "ShippingRule"), XmlElement(Order = 0, ElementName = "ShippingRule")]
        public Models.ShippingRule ShippingRule { get; set; }
    }
}
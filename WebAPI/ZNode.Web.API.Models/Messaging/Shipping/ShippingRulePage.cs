﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingRulePage", Namespace = ""), Serializable]
    public class ShippingRulePage : Page<Models.ShippingRule>
    {
        public ShippingRulePage() { }
        public ShippingRulePage(int page, int pageSize, int totalItems, IEnumerable<Models.ShippingRule> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
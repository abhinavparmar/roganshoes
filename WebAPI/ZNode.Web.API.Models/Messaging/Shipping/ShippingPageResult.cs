﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ShippingPageResult")]
    public class ShippingPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "ShippingPage"), XmlElement(Order = 0, ElementName = "ShippingPage")]
        public ShippingPage ShippingPage { get; set; }

        public ShippingPageResult() { }
        public ShippingPageResult(int page, int pageSize, int totalItems, IEnumerable<Models.Shipping> items)
        {
            ShippingPage = new ShippingPage(page, pageSize, totalItems, items);
        }
    }
}

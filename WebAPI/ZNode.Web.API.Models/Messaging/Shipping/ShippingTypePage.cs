﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingTypePage", Namespace = ""), Serializable]
    public class ShippingTypePage : Page<Models.ShippingType>
    {
        public ShippingTypePage() { }
        public ShippingTypePage(int page, int pageSize, int totalItems, IEnumerable<Models.ShippingType> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
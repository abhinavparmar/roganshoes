﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingRulePageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ShippingRulePageResult")]
    public class ShippingRulePageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "ShippingRulePage"), XmlElement(Order = 0, ElementName = "ShippingRulePage")]
        public ShippingRulePage ShippingRulePage { get; set; }

        public ShippingRulePageResult() { }
        public ShippingRulePageResult(int page, int pageSize, int totalItems, IEnumerable<Models.ShippingRule> items)
        {
            ShippingRulePage = new ShippingRulePage(page, pageSize, totalItems, items);
        }
    }
}

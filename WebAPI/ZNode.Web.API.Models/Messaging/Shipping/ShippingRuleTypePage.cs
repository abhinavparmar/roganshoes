﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Shipping
{
    [DataContract(Name = "ShippingRuleTypePage", Namespace = ""), Serializable]
    public class ShippingRuleTypePage : Page<Models.ShippingRuleType>
    {
        public ShippingRuleTypePage() { }
        public ShippingRuleTypePage(int page, int pageSize, int totalItems, IEnumerable<Models.ShippingRuleType> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
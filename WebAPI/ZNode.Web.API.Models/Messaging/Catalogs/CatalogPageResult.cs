﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Catalogs
{
    [DataContract(Name = "CatalogPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "CatalogPageResult")]
    public class CatalogPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "CatalogPage"), XmlElement(Order = 0, ElementName = "CatalogPage")]
        public CatalogPage CatalogPage { get; set; }

        public CatalogPageResult() { }
        public CatalogPageResult(int page, int pageSize, int totalItems, IEnumerable<Catalog> items)
        {
            CatalogPage = new CatalogPage(page, pageSize, totalItems, items);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Catalogs
{
    [DataContract(Name = "CatalogResult", Namespace = ""), Serializable, XmlRoot(ElementName = "CatalogResult")]
    public class CatalogResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Catalog"), XmlElement(Order = 0, ElementName = "Catalog")]
        public Catalog Catalog { get; set; }
    }
}
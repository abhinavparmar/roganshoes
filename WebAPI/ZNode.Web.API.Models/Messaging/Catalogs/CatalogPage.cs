﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Catalogs
{
    [DataContract(Name = "CatalogPage", Namespace = ""), Serializable]
    public class CatalogPage : Page<Catalog>
    {
        public CatalogPage() { }
        public CatalogPage(int page, int pageSize, int totalItems, IEnumerable<Catalog> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
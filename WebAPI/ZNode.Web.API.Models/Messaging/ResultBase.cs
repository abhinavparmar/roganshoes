﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging
{
    [DataContract(Name = "ResultBase", Namespace = ""), Serializable, XmlRoot(ElementName = "ResultBase")]
    public abstract class ResultBase
    {
        [DataMember(Order = 99, Name = "IsError"), XmlElement(Order = 1, ElementName = "IsError")]
        public bool IsError { get; set; }

        [DataMember(Order = 99, Name = "Message"), XmlElement(Order = 2, ElementName = "Message")]
        public string Message { get; set; }

        [DataMember(Order = 99, Name = "StackTrace"), XmlElement(Order = 3, ElementName = "StackTrace")]
        public string StackTrace { get; set; }
    }
}

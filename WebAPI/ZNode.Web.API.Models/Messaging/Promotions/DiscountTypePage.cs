﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Promotions
{
    [DataContract(Name = "DiscountTypePage", Namespace = ""), Serializable]
    public class DiscountTypePage : Page<DiscountType>
    {
        public DiscountTypePage() { }
        public DiscountTypePage(int page, int pageSize, int totalItems, IEnumerable<DiscountType> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
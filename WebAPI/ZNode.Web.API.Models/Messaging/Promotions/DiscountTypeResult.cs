﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Promotions
{
    [DataContract(Name = "DiscountTypeResult", Namespace = ""), Serializable, XmlRoot(ElementName = "DiscountTypeResult")]
    public class DiscountTypeResult : ResultBase
    {
        [DataMember(Order = 99, Name = "DiscountType"), XmlElement(Order = 0, ElementName = "DiscountType")]
        public DiscountType DiscountType { get; set; }
    }
}
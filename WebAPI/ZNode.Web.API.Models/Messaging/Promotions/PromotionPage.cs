﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Promotions
{
    [DataContract(Name = "PromotionPage", Namespace = ""), Serializable]
    public class PromotionPage : Page<Promotion>
    {
        public PromotionPage() { }
        public PromotionPage(int page, int pageSize, int totalItems, IEnumerable<Promotion> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
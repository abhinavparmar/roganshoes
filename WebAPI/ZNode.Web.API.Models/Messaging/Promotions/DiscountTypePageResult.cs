﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Promotions
{
    [DataContract(Name = "DiscountTypePageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "DiscountTypePageResult")]
    public class DiscountTypePageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "DiscountTypePage"), XmlElement(Order = 0, ElementName = "DiscountTypePage")]
        public DiscountTypePage DiscountTypePage { get; set; }

        public DiscountTypePageResult() { }
        public DiscountTypePageResult(int page, int pageSize, int totalItems, IEnumerable<DiscountType> items)
        {
            DiscountTypePage = new DiscountTypePage(page, pageSize, totalItems, items);
        }
    }
}

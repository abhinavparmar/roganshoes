﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Promotions
{
    [DataContract(Name = "PromotionResult", Namespace = ""), Serializable, XmlRoot(ElementName = "PromotionResult")]
    public class PromotionResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Promotion"), XmlElement(Order = 0, ElementName = "Promotion")]
        public Promotion Promotion { get; set; }
    }
}
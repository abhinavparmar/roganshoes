﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Promotions
{
    [DataContract(Name = "PromotionPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "PromotionPageResult")]
    public class PromotionPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "PromotionPage"), XmlElement(Order = 0, ElementName = "PromotionPage")]
        public PromotionPage PromotionPage { get; set; }

        public PromotionPageResult() { }
        public PromotionPageResult(int page, int pageSize, int totalItems, IEnumerable<Promotion> items)
        {
            PromotionPage = new PromotionPage(page, pageSize, totalItems, items);
        }
    }
}

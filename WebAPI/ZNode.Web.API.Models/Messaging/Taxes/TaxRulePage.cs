﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Taxes
{
    [DataContract(Name = "TaxRulePage", Namespace = ""), Serializable]
    public class TaxRulePage : Page<TaxRule>
    {
        public TaxRulePage() { }
        public TaxRulePage(int page, int pageSize, int totalItems, IEnumerable<TaxRule> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Taxes
{
    [DataContract(Name = "TaxClassResult", Namespace = ""), Serializable, XmlRoot(ElementName = "TaxClassResult")]
    public class TaxClassResult : ResultBase
    {
        [DataMember(Order = 99, Name = "TaxClass"), XmlElement(Order = 0, ElementName = "TaxClass")]
        public TaxClass TaxClass { get; set; }
    }
}
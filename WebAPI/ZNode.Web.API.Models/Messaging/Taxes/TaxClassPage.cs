﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Taxes
{
    [DataContract(Name = "TaxClassPage", Namespace = ""), Serializable]
    public class TaxClassPage : Page<TaxClass>
    {
        public TaxClassPage() { }
        public TaxClassPage(int page, int pageSize, int totalItems, IEnumerable<TaxClass> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
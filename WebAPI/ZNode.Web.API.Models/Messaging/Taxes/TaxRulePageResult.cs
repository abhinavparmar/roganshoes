﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Taxes
{
    [DataContract(Name = "TaxRulePageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "TaxRulePageResult")]
    public class TaxRulePageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "TaxRulePage"), XmlElement(Order = 0, ElementName = "TaxRulePage")]
        public TaxRulePage TaxRulePage { get; set; }

        public TaxRulePageResult() { }
        public TaxRulePageResult(int page, int pageSize, int totalItems, IEnumerable<TaxRule> items)
        {
            TaxRulePage = new TaxRulePage(page, pageSize, totalItems, items);
        }
    }
}

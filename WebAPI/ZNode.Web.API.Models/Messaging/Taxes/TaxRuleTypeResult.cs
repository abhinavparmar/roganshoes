﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Taxes
{
    [DataContract(Name = "TaxRuleTypeResult", Namespace = ""), Serializable, XmlRoot(ElementName = "TaxRuleTypeResult")]
    public class TaxRuleTypeResult : ResultBase
    {
        [DataMember(Order = 99, Name = "TaxRuleType"), XmlElement(Order = 0, ElementName = "TaxRuleType")]
        public TaxRuleType TaxRuleType { get; set; }
    }
}
﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Taxes
{
    [DataContract(Name = "TaxRuleTypePage", Namespace = ""), Serializable]
    public class TaxRuleTypePage : Page<TaxRuleType>
    {
        public TaxRuleTypePage() { }
        public TaxRuleTypePage(int page, int pageSize, int totalItems, IEnumerable<TaxRuleType> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
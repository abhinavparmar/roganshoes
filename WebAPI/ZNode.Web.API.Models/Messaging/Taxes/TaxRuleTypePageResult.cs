﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Taxes
{
    [DataContract(Name = "TaxRuleTypePageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "TaxRuleTypePageResult")]
    public class TaxRuleTypePageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "TaxRuleTypePage"), XmlElement(Order = 0, ElementName = "TaxRuleTypePage")]
        public TaxRuleTypePage TaxRuleTypePage { get; set; }

        public TaxRuleTypePageResult() { }
        public TaxRuleTypePageResult(int page, int pageSize, int totalItems, IEnumerable<TaxRuleType> items)
        {
            TaxRuleTypePage = new TaxRuleTypePage(page, pageSize, totalItems, items);
        }
    }
}

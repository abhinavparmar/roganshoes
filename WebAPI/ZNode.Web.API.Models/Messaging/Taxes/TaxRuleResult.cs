﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Taxes
{
    [DataContract(Name = "TaxRuleResult", Namespace = ""), Serializable, XmlRoot(ElementName = "TaxRuleResult")]
    public class TaxRuleResult : ResultBase
    {
        [DataMember(Order = 99, Name = "TaxRule"), XmlElement(Order = 0, ElementName = "TaxRule")]
        public TaxRule TaxRule { get; set; }
    }
}
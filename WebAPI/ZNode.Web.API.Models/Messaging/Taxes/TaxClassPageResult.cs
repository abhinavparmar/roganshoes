﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Taxes
{
    [DataContract(Name = "TaxClassPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "TaxClassPageResult")]
    public class TaxClassPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "TaxClassPage"), XmlElement(Order = 0, ElementName = "TaxClassPage")]
        public TaxClassPage TaxClassPage { get; set; }

        public TaxClassPageResult() { }
        public TaxClassPageResult(int page, int pageSize, int totalItems, IEnumerable<TaxClass> items)
        {
            TaxClassPage = new TaxClassPage(page, pageSize, totalItems, items);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Products
{
    [DataContract(Name = "ProductPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ProductPageResult")]
    public class ProductPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "ProductPage"), XmlElement(Order = 0, ElementName = "ProductPage")]
        public ProductPage ProductPage { get; set; }

        public ProductPageResult() { }
        public ProductPageResult(int page, int pageSize, int totalItems, IEnumerable<Product> items)
        {
            ProductPage = new ProductPage(page, pageSize, totalItems, items);
        }
    }
}

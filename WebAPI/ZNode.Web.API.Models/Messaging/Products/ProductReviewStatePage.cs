﻿#region Using Directives
using System.Collections.Generic;
using System.Runtime.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Products
{
    [DataContract(Name = "ProductReviewStatePage", Namespace = "")]
    public class ProductReviewStatePage : Page<ProductReviewState>
    {
        public ProductReviewStatePage() { }
        public ProductReviewStatePage(int page, int pageSize, int totalItems, IEnumerable<ProductReviewState> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
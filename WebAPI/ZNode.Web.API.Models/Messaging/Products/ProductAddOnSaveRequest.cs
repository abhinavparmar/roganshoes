﻿#region Using Directives



#endregion

namespace RogansShoes.Web.API.Models.Messaging.Products
{
    public class ProductAddOnSaveRequest : SaveRequest
    {
        public string ProductId { get; set; }
        public string IdType { get; set; }
    }
}

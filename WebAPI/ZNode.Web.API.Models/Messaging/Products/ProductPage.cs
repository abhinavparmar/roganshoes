﻿#region Using Directives
using System.Collections.Generic;
using System.Runtime.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Products
{
    [DataContract(Name = "ProductPage", Namespace = "")]
    public class ProductPage : Page<Product>
    {
        public ProductPage() { }
        public ProductPage(int page, int pageSize, int totalItems, IEnumerable<Product> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
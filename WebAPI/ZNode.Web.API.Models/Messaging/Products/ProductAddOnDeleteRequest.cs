﻿#region Using Directives
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Products
{
    public class ProductAddOnDeleteRequest : DeleteRequest
    {
        public string ProductId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Products
{
    [DataContract(Name = "ProductReviewStateResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ProductReviewStateResult")]
    public class ProductReviewStateResult : ResultBase
    {
        [DataMember(Order = 99, Name = "ProductReviewState"), XmlElement(Order = 0, ElementName = "ProductReviewState")]
        public ProductReviewState ProductReviewState { get; set; }
    }
}

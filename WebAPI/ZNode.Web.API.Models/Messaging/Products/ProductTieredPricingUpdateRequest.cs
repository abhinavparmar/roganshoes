﻿#region Using Directives
using System;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Products
{
    public class ProductTieredPricingUpdateRequest : UpdateRequest
    {
        public string ProductId { get; set; }
        public string IdType { get; set; }
    }
}

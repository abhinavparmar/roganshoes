﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Products
{
    [DataContract(Name = "ProductResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ProductResult")]
    public class ProductResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Product"), XmlElement(Order = 0, ElementName = "Product")]
        public Product Product { get; set; }
    }
}

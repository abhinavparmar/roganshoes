﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Products
{
    [DataContract(Name = "ProductReviewStatePageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ProductReviewStatePageResult")]
    public class ProductReviewStatePageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "ProductReviewStatePage"), XmlElement(Order = 0, ElementName = "ProductReviewStatePage")]
        public ProductReviewStatePage ProductReviewStatePage { get; set; }

        public ProductReviewStatePageResult() { }
        public ProductReviewStatePageResult(int page, int pageSize, int totalItems, IEnumerable<ProductReviewState> items)
        {
            ProductReviewStatePage = new ProductReviewStatePage(page, pageSize, totalItems, items);
        }
    }
}

﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.AddOns
{
    [DataContract(Name = "AddOnPage", Namespace = ""), Serializable]
    public class AddOnPage : Page<AddOn>
    {
        public AddOnPage() { }
        public AddOnPage(int page, int pageSize, int totalItems, IEnumerable<AddOn> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
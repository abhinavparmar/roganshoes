﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.AddOns
{
    [DataContract(Name = "AddOnPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "AddOnPageResult")]
    public class AddOnPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "AddOnPage"), XmlElement(Order = 0, ElementName = "AddOnPage")]
        public AddOnPage AddOnPage { get; set; }

        public AddOnPageResult() { }
        public AddOnPageResult(int page, int pageSize, int totalItems, IEnumerable<AddOn> items)
        {
            AddOnPage = new AddOnPage(page, pageSize, totalItems, items);
        }
    }
}

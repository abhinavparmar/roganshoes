﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.AddOns
{
    [DataContract(Name = "AddOnValueResult", Namespace = ""), Serializable, XmlRoot(ElementName = "AddOnValueResult")]
    public class AddOnValueResult : ResultBase
    {
        [DataMember(Order = 99, Name = "AddOnValue"), XmlElement(Order = 0, ElementName = "AddOnValue")]
        public AddOnValue AddOnValue { get; set; }
    }
}

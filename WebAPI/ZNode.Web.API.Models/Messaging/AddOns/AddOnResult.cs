﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.AddOns
{
    [DataContract(Name = "AddOnResult", Namespace = ""), Serializable, XmlRoot(ElementName = "AddOnResult")]
    public class AddOnResult : ResultBase
    {
        [DataMember(Order = 99, Name = "AddOn"), XmlElement(Order = 0, ElementName = "AddOn")]
        public AddOn AddOn { get; set; }
    }
}

﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.AddOns
{
    [DataContract(Name = "AddOnValuePage", Namespace = ""), Serializable]
    public class AddOnValuePage : Page<AddOnValue>
    {
        public AddOnValuePage() { }
        public AddOnValuePage(int page, int pageSize, int totalItems, IEnumerable<AddOnValue> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
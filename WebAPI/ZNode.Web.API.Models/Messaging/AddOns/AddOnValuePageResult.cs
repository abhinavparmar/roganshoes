﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.AddOns
{
    [DataContract(Name = "AddOnValuePageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "AddOnValuePageResult")]
    public class AddOnValuePageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "AddOnValuePage"), XmlElement(Order = 0, ElementName = "AddOnValuePage")]
        public AddOnValuePage AddOnValuePage { get; set; }

        public AddOnValuePageResult() { }
        public AddOnValuePageResult(int page, int pageSize, int totalItems, IEnumerable<AddOnValue> items)
        {
            AddOnValuePage = new AddOnValuePage(page, pageSize, totalItems, items);
        }
    }
}

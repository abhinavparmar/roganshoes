﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Skus
{
    [DataContract(Name = "InventoryPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "InventoryPageResult")]
    public class InventoryPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "InventoryPage"), XmlElement(Order = 0, ElementName = "InventoryPage")]
        public InventoryPage InventoryPage { get; set; }

        public InventoryPageResult() { }
        public InventoryPageResult(int page, int pageSize, int totalItems, IEnumerable<Inventory> items)
        {
            InventoryPage = new InventoryPage(page, pageSize, totalItems, items);
        }
    }
}

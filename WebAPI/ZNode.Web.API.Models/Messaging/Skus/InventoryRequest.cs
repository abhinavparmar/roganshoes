﻿#region Using Directives
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Skus
{
    public class InventoryRequest : RequestBase
    {
        public InventoryTransaction InventoryTransaction { get; set; }
    }
}

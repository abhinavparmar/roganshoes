﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Skus
{
    [DataContract(Name = "InventoryResult", Namespace = ""), Serializable, XmlRoot(ElementName = "InventoryResult")]
    public class InventoryResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Inventory"), XmlElement(Order = 0, ElementName = "Inventory")]
        public Inventory Inventory { get; set; }
    }
}

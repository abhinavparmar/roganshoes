﻿#region Using Directives



#endregion

namespace ZNode.Web.API.Models.Messaging.Skus
{
    public class InventoryTransactionPayloadRequest : CreateRequest
    {
        public InventoryTransaction InventoryTransaction { get; set; }
    }
}

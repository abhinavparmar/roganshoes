﻿#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Siesta;

#endregion

namespace RogansShoes.Web.API.Models.Messaging.Skus
{
    [DataContract(Name = "InventoryPage", Namespace = ""),Serializable]
    public class InventoryPage : Page<Inventory>
    {
        public InventoryPage() { }
        public InventoryPage(int page, int pageSize, int totalItems, IEnumerable<Inventory> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
﻿#region Using Directives
using System;
#endregion

namespace RogansShoes.Web.API.Models.Messaging
{
    public class DeleteRequest : RequestBase
    {
        public string Id { get; set; }
        public string IdType { get; set; }
    }
}
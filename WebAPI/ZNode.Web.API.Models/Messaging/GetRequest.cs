#region Using Directives
using System;
#endregion

namespace RogansShoes.Web.API.Models.Messaging
{
	public class GetRequest : RequestBase
	{
		public string Id { get; set; }
        public string IdType { get; set; }
	}
}
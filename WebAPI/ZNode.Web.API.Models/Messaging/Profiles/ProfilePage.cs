﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Profiles
{
    [DataContract(Name = "ProfilePage", Namespace = ""), Serializable]
    public class ProfilePage : Page<Profile>
    {
        public ProfilePage() { }
        public ProfilePage(int page, int pageSize, int totalItems, IEnumerable<Profile> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
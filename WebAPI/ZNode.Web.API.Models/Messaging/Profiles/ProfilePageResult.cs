﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Profiles
{
    [DataContract(Name = "ProfilePageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ProfilePageResult")]
    public class ProfilePageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "ProfilePage"), XmlElement(Order = 0, ElementName = "ProfilePage")]
        public ProfilePage ProfilePage { get; set; }

        public ProfilePageResult() { }
        public ProfilePageResult(int page, int pageSize, int totalItems, IEnumerable<Profile> items)
        {
            ProfilePage = new ProfilePage(page, pageSize, totalItems, items);
        }
    }
}

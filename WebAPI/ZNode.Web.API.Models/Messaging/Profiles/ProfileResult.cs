﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Profiles
{
    [DataContract(Name = "ProfileResult", Namespace = ""), Serializable, XmlRoot(ElementName = "ProfileResult")]
    public class ProfileResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Profile"), XmlElement(Order = 0, ElementName = "Profile")]
        public Profile Profile { get; set; }
    }
}
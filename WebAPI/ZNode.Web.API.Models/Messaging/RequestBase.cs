#region Using Directives
using System;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging
{
	public abstract class RequestBase
	{
		public bool Indent { get; set; }
        public string Expand { get; set; }
        public string Filter { get; set; }
        public string OrderBy { get; set; }

		public ModelFormatting Formatting
		{
			get { return Indent ? ModelFormatting.HumanReadable : ModelFormatting.Normal; }
		}
	}
}
﻿#region Using Directives
#endregion

namespace RogansShoes.Web.API.Models.Messaging.SavedCarts
{
    public class SavedCartLineItemDeleteRequest : DeleteRequest
    {
        public string SavedCartId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.SavedCarts
{
    [DataContract(Name = "SavedCartPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "SavedCartPageResult")]
    public class SavedCartPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "SavedCartPage"), XmlElement(Order = 0, ElementName = "SavedCartPage")]
        public SavedCartPage SavedCartPage { get; set; }

        public SavedCartPageResult() { }
        public SavedCartPageResult(int page, int pageSize, int totalItems, IEnumerable<SavedCart> items)
        {
            SavedCartPage = new SavedCartPage(page, pageSize, totalItems, items);
        }
    }
}

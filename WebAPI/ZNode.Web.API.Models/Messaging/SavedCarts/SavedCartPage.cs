﻿#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Siesta;

#endregion

namespace RogansShoes.Web.API.Models.Messaging.SavedCarts
{
    [DataContract(Name = "SavedCartsPage", Namespace = "")]
    public class SavedCartPage : Page<SavedCart>
    {
        public SavedCartPage() { }
        public SavedCartPage(int page, int pageSize, int totalItems, IEnumerable<SavedCart> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
﻿#region Using Directives



#endregion

namespace RogansShoes.Web.API.Models.Messaging.SavedCarts
{
    public class SavedCartLineItemSaveRequest : SaveRequest
    {
        public string Quantity { get; set; }
        public string SavedCartId { get; set; }
        public string IdType { get; set; }
    }
}

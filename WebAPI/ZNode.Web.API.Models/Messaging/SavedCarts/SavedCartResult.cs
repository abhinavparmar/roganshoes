﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.SavedCarts
{
    [DataContract(Name = "SavedCartResult", Namespace = ""), Serializable, XmlRoot(ElementName = "SavedCartResult")]
    public class SavedCartResult : ResultBase
    {
        [DataMember(Order = 99, Name = "SavedCart"), XmlElement(Order = 0, ElementName = "SavedCart")]
        public SavedCart SavedCart { get; set; }
    }
}

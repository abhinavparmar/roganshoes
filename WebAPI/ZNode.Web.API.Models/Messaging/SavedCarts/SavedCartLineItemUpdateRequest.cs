﻿#region Using Directives



#endregion

namespace RogansShoes.Web.API.Models.Messaging.SavedCarts
{
    public class SavedCartLineItemUpdateRequest : UpdateRequest
    {
        public string SavedCartId { get; set; }
        public string IdType { get; set; }
    }
}

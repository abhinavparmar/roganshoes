﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Portals
{
    [DataContract(Name = "PortalPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "PortalPageResult")]
    public class PortalPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "PortalPage"), XmlElement(Order = 0, ElementName = "PortalPage")]
        public PortalPage PortalPage { get; set; }

        public PortalPageResult() { }
        public PortalPageResult(int page, int pageSize, int totalItems, IEnumerable<Portal> items)
        {
            PortalPage = new PortalPage(page, pageSize, totalItems, items);
        }
    }
}

﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Portals
{
    [DataContract(Name = "PortalPage", Namespace = ""), Serializable]
    public class PortalPage : Page<Portal>
    {
        public PortalPage() { }
        public PortalPage(int page, int pageSize, int totalItems, IEnumerable<Portal> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
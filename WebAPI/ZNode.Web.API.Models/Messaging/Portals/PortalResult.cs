﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Portals
{
    [DataContract(Name = "PortalResult", Namespace = ""), Serializable, XmlRoot(ElementName = "PortalResult")]
    public class PortalResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Portal"), XmlElement(Order = 0, ElementName = "Portal")]
        public Portal Portal { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Accounts
{
    [DataContract(Name = "AccountResult", Namespace = ""), Serializable, XmlRoot(ElementName = "AccountResult")]
    public class AccountResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Account"), XmlElement( Order = 0,ElementName = "Account")]
        public Account Account { get; set; }
    }
}
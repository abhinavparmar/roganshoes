﻿#region Using Directives
using System;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Accounts
{
    public class AccountAddressUpdateRequest : UpdateRequest
    {
        public string AccountId { get; set; }
        public string IdType { get; set; }
    }
}

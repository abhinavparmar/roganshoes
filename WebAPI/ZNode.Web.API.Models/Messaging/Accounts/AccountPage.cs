﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Accounts
{
    [DataContract(Name = "AccountPage", Namespace = ""), Serializable]
    public class AccountPage : Page<Account>
    {
        public AccountPage() { }
        public AccountPage(int page, int pageSize, int totalItems, IEnumerable<Account> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
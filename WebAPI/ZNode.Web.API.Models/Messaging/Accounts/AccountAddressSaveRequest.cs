﻿#region Using Directives
using System;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Accounts
{
    public class AccountAddressSaveRequest : SaveRequest
    {
        public string AccountId { get; set; }
        public string IdType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Accounts
{
    [DataContract(Name = "AccountPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "AccountPageResult")]
    public class AccountPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "AccountPage"), XmlElement(Order = 0, ElementName = "AccountPage")]
        public AccountPage AccountPage { get; set; }

        public AccountPageResult() { }
        public AccountPageResult(int page, int pageSize, int totalItems, IEnumerable<Account> items)
        {
            AccountPage = new AccountPage(page, pageSize, totalItems, items);
        }
    }
}

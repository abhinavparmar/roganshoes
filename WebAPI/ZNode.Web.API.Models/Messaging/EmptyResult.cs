﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging
{
    [DataContract(Name = "EmptyResult", Namespace = ""), Serializable, XmlRoot(ElementName = "EmptyResult")]
    public class EmptyResult : ResultBase
    {

    }
}

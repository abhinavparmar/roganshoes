﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Suppliers
{
    [DataContract(Name = "SupplierTypePage", Namespace = ""), Serializable]
    public class SupplierTypePage : Page<SupplierType>
    {
        public SupplierTypePage() { }
        public SupplierTypePage(int page, int pageSize, int totalItems, IEnumerable<SupplierType> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
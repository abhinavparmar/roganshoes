﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Suppliers
{
    [DataContract(Name = "SupplierTypeResult", Namespace = ""), Serializable, XmlRoot(ElementName = "SupplierTypeResult")]
    public class SupplierTypeResult : ResultBase
    {
        [DataMember(Order = 99, Name = "SupplierType"), XmlElement(Order = 0, ElementName = "SupplierType")]
        public SupplierType SupplierType { get; set; }
    }
}
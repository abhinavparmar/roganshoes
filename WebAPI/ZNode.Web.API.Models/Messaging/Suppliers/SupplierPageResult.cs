﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Suppliers
{
    [DataContract(Name = "SupplierPageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "SupplierPageResult")]
    public class SupplierPageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "SupplierPage"), XmlElement(Order = 0, ElementName = "SupplierPage")]
        public SupplierPage SupplierPage { get; set; }

        public SupplierPageResult() { }
        public SupplierPageResult(int page, int pageSize, int totalItems, IEnumerable<Supplier> items)
        {
            SupplierPage = new SupplierPage(page, pageSize, totalItems, items);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Suppliers
{
    [DataContract(Name = "SupplierTypePageResult", Namespace = ""), Serializable, XmlRoot(ElementName = "SupplierTypePageResult")]
    public class SupplierTypePageResult : ResultBase
    {
        [DataMember(Order = 99, Name = "SupplierTypePage"), XmlElement(Order = 0, ElementName = "SupplierTypePage")]
        public SupplierTypePage SupplierTypePage { get; set; }

        public SupplierTypePageResult() { }
        public SupplierTypePageResult(int page, int pageSize, int totalItems, IEnumerable<SupplierType> items)
        {
            SupplierTypePage = new SupplierTypePage(page, pageSize, totalItems, items);
        }
    }
}

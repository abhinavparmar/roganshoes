﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Siesta;
#endregion

namespace RogansShoes.Web.API.Models.Messaging.Suppliers
{
    [DataContract(Name = "SupplierPage", Namespace = ""), Serializable]
    public class SupplierPage : Page<Supplier>
    {
        public SupplierPage() { }
        public SupplierPage(int page, int pageSize, int totalItems, IEnumerable<Supplier> items)
            : base(page, pageSize, totalItems, items) { }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models.Messaging.Suppliers
{
    [DataContract(Name = "SupplierResult", Namespace = ""), Serializable, XmlRoot(ElementName = "SupplierResult")]
    public class SupplierResult : ResultBase
    {
        [DataMember(Order = 99, Name = "Supplier"), XmlElement(Order = 0, ElementName = "Supplier")]
        public Supplier Supplier { get; set; }
    }
}
#region Using Directives
using System;
#endregion

namespace RogansShoes.Web.API.Models.Messaging
{
	public class ListRequest : RequestBase
	{   
        public const int DefaultPageSize = 10;

		public int Page { get; set; }
		public int PageSize { get; set; }

		public ListRequest()
		{
			Page = 1;
			PageSize = DefaultPageSize;
		}
	}
}
﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "OrderLineItem", Namespace = ""), Serializable]
    public class OrderLineItem
    {
        [DataMember(Order = 99, Name = "OrderLineItemId"), XmlElement(Order = 0, ElementName = "OrderLineItemId")]
        public string OrderLineItemId { get; set; }

        [DataMember(Order = 99, Name = "CreateDate"), XmlElement(Order = 1, ElementName = "CreateDate")]
        public DateTime? CreateDate { get; set; }

        [DataMember(Order = 99, Name = "Discount"), XmlElement(Order = 2, ElementName = "Discount")]
        public decimal? DiscountAmount { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 3, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "OrderId"), XmlElement(Order = 4, ElementName = "OrderId")]
        public string OrderId { get; set; }

        [DataMember(Order = 99, Name = "OrderLineItemRelationshipTypeId"), XmlElement(Order = 5, ElementName = "OrderLineItemRelationshipTypeId")]
        public string OrderLineItemRelationshipTypeId { get; set; }

        [DataMember(Order = 99, Name = "ParentLineItem", EmitDefaultValue = false), XmlElement(Order = 6, ElementName = "ParentLineItem", IsNullable = true)]
        public string ParentLineItem { get; set; }

        [DataMember(Order = 99, Name = "Price", EmitDefaultValue = false), XmlElement(Order = 7, ElementName = "Price", IsNullable = true)]
        public decimal? Price { get; set; }

        [DataMember(Order = 99, Name = "Quantity", EmitDefaultValue = false), XmlElement(Order = 8, ElementName = "Quantity", IsNullable = true)]
        public int? Quantity { get; set; }

        [DataMember(Order = 99, Name = "SalesTax", EmitDefaultValue = false), XmlElement(Order = 9, ElementName = "SalesTax", IsNullable = true)]
        public decimal? SalesTax { get; set; }

        [DataMember(Order = 99, Name = "ShipDate"), XmlElement(Order = 10, ElementName = "ShipDate")]
        public DateTime? ShipDate { get; set; }

        [DataMember(Order = 99, Name = "ShipmentId"), XmlElement(Order = 11, ElementName = "ShipmentId")]
        public string ShipmentId { get; set; }

        [DataMember(Order = 99, Name = "ShippingCost"), XmlElement(Order = 12, ElementName = "ShippingCost")]
        public decimal? ShippingCost { get; set; }

        [DataMember(Order = 99, Name = "SkuId"), XmlElement(Order = 13, ElementName = "SkuId")]
        public string SkuId { get; set; }

        [DataMember(Order = 99, Name = "TrackingNumber", EmitDefaultValue = false), XmlElement(Order = 14, ElementName = "TrackingNumber", IsNullable = true)]
        public string TrackingNumber { get; set; }

        [DataMember(Order = 99, Name = "UpdateDate"), XmlElement(Order = 15, ElementName = "UpdateDate")]
        public DateTime? UpdateDate { get; set; }

        [DataMember(Order = 99, Name = "LineItemRef"), XmlElement(Order = 16, ElementName = "LineItemRef")]
        public int LineItemRef { get; set; }

        #region Zeon Custom Code
        
        [DataMember(Order = 99, Name = "PortalID"), XmlElement(Order = 16, ElementName = "PortalID")]
        public string PortalID { get; set; }

        [DataMember(Order = 99, Name = "AccountID"), XmlElement(Order = 17, ElementName = "AccountID")]
        public string AccountID { get; set; }

        [DataMember(Order = 99, Name = "ItemSku"), XmlElement(Order = 18, ElementName = "ItemSku")]
        public string ItemSku { get; set; }

        [DataMember(Order = 99, Name = "ProductName"), XmlElement(Order = 19, ElementName = "ProductName")]
        public string ProductName { get; set; }

        [DataMember(Order = 99, Name = "PromoCode"), XmlElement(Order = 20, ElementName = "PromoCode")]
        public string PromoCode { get; set; }

        [DataMember(Order = 99, Name = "Notes"), XmlElement(Order = 21, ElementName = "Notes")]
        public string Notes { get; set; }

        #endregion

        public OrderLineItem()
        {

        }
    }
}

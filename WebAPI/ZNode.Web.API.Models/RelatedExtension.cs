﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "RelatedExtension", Namespace = ""),Serializable]
    public class RelatedExtension
    {
        [DataMember(Order = 99, Name = "ProductExtension"), XmlElement( Order=1,ElementName = "ProductExtension")]
        public ProductExtension ProductExtension { get; set; }

        [DataMember(Order = 99, Name = "Type"), XmlElement( Order=2,ElementName = "Type")]
        public string Type { get; set; }
    }
}
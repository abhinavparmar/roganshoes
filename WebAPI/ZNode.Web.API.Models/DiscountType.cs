﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "DiscountType", Namespace = ""), XmlRoot(ElementName = "DiscountType"), Serializable]
    public class DiscountType
    {
        [DataMember(Order = 99, Name = "DiscountTypeId"), XmlElement(Order = 0, ElementName = "DiscountTypeId")]
        public string DiscountTypeId { get; set; }

        [DataMember(Order = 99, Name = "ClassType"), XmlElement(Order = 1, ElementName = "ClassType")]
        public string ClassType { get; set; }

        [DataMember(Order = 99, Name = "ClassName"), XmlElement(Order = 2, ElementName = "ClassName")]
        public string ClassName { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 3, ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement(Order = 4, ElementName = "Description")]
        public string Description { get; set; }

        [DataMember(Order = 99, Name = "ActiveInd"), XmlElement(Order = 5, ElementName = "ActiveInd")]
        public bool ActiveInd { get; set; }
    }
}

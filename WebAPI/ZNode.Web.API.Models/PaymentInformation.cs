﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "PaymentInformation", Namespace = ""), Serializable]
    public class PaymentInformation
    {
        [DataMember(Order = 99, Name = "CreditCardInformation", EmitDefaultValue = false), XmlElement(Order = 0, ElementName = "CreditCardInformation", IsNullable = true)]
        public CreditCardInformation CreditCardInformation { get; set; }

        [DataMember(Order = 99, Name = "PaymentType", EmitDefaultValue = false), XmlElement(Order = 1, ElementName = "PaymentType", IsNullable = true)]
        public string PaymentType { get; set; }

        [DataMember(Order = 99, Name = "TransactionId", EmitDefaultValue = false), XmlElement(Order = 2, ElementName = "TransactionId", IsNullable = true)]
        public String TransactionId { get; set; }

    }


    //Perficient Customization   : Done on Date : 07/27/15 : Purpose : Add new class for handling multiple credit card response.
    [DataContract(Name = "CardInformation", Namespace = ""), Serializable]
    public class CardInformation
    {
        [DataMember(Order = 99, Name = "CardExp", EmitDefaultValue = false), XmlElement(Order = 0, ElementName = "CardExp", IsNullable = true)]
        public string CardExp { get; set; }

        [DataMember(Order = 99, Name = "CardNumber", EmitDefaultValue = false), XmlElement(Order = 1, ElementName = "CardNumber", IsNullable = true)]
        public string CardNumber { get; set; }

        [DataMember(Order = 99, Name = "CardType", EmitDefaultValue = false), XmlElement(Order = 2, ElementName = "CardType", IsNullable = true)]
        public string CardType { get; set; }

        [DataMember(Order = 99, Name = "CardAuthCode", EmitDefaultValue = false), XmlElement(Order = 3, ElementName = "CardAuthCode", IsNullable = true)]
        public string CardAuthCode { get; set; }

        [DataMember(Order = 99, Name = "Amount", EmitDefaultValue = false), XmlElement(Order = 4, ElementName = "Amount", IsNullable = true)]
        public String Amount { get; set; }

        [DataMember(Order = 99, Name = "PNRefCode", EmitDefaultValue = false), XmlElement(Order = 5, ElementName = "PNRefCode", IsNullable = true)]
        public String TransactionId { get; set; }

    }

}

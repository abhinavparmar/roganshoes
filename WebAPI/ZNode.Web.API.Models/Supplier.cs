﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Supplier", Namespace = ""), Serializable, XmlRoot(ElementName = "Supplier")]
    public class Supplier
    {
        [DataMember(Order = 99, Name = "SupplierId"), XmlElement(Order = 0, ElementName = "SupplierId")]
        public string SupplierId { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 1, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "ExternalSupplierNo"), XmlElement(Order = 2, ElementName = "ExternalSupplierNo")]
        public string ExternalSupplierNo { get; set; }

        [DataMember(Order = 99, Name = "Name"), XmlElement(Order = 3, ElementName = "Name")]
        public string Name { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement(Order = 4, ElementName = "Description")]
        public string Description { get; set; }

        [DataMember(Order = 99, Name = "ContactFirstName"), XmlElement(Order = 5, ElementName = "ContactFirstName")]
        public string ContactFirstName { get; set; }

        [DataMember(Order = 99, Name = "ContactLastName"), XmlElement(Order = 6, ElementName = "ContactLastName")]
        public string ContactLastName { get; set; }

        [DataMember(Order = 99, Name = "ContactPhone"), XmlElement(Order = 7, ElementName = "ContactPhone")]
        public string ContactPhone { get; set; }

        [DataMember(Order = 99, Name = "ContactEmail"), XmlElement(Order = 8, ElementName = "ContactEmail")]
        public string ContactEmail { get; set; }

        [DataMember(Order = 99, Name = "EnableEmailNotification"), XmlElement(Order = 9, ElementName = "EnableEmailNotification")]
        public bool EnableEmailNotification { get; set; }

        [DataMember(Order = 99, Name = "ActiveInd"), XmlElement(Order = 10, ElementName = "ActiveInd")]
        public bool ActiveInd { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Shipping", Namespace = ""), Serializable, XmlRoot(ElementName = "Shipping")]
    public class Shipping
    {
        [DataMember(Order = 99, Name = "ShippingId"), XmlElement(Order = 0, ElementName = "ShippingId")]
        public string ShippingId { get; set; }

        [DataMember(Order = 99, Name = "ExternalId"), XmlElement(Order = 1, ElementName = "ExternalId")]
        public string ExternalId { get; set; }

        [DataMember(Order = 99, Name = "ShippingTypeId"), XmlElement(Order = 2, ElementName = "ShippingTypeId")]
        public string ShippingTypeId { get; set; }

        [DataMember(Order = 99, Name = "ProfileId"), XmlElement(Order = 3, ElementName = "ProfileId")]
        public string ProfileId { get; set; }

        [DataMember(Order = 99, Name = "ShippingCode"), XmlElement(Order = 4, ElementName = "ShippingCode")]
        public string ShippingCode { get; set; }

        [DataMember(Order = 99, Name = "HandlingCharge"), XmlElement(Order = 5, ElementName = "HandlingCharge")]
        public decimal HandlingCharge { get; set; }

        [DataMember(Order = 99, Name = "DestinationCountryCode"), XmlElement(Order = 6, ElementName = "DestinationCountryCode")]
        public string DestinationCountryCode { get; set; }

        [DataMember(Order = 99, Name = "Description"), XmlElement(Order = 7, ElementName = "Description")]
        public string Description { get; set; }

        [DataMember(Order = 99, Name = "ActiveInd"), XmlElement(Order = 8, ElementName = "ActiveInd")]
        public bool ActiveInd { get; set; }
    }
}

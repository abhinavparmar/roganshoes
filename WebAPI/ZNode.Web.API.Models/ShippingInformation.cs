﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "ShippingInformation", Namespace = ""), Serializable]
    public class ShippingInformation
    {
        [DataMember(Order = 1, Name = "TrackingNumber", EmitDefaultValue = false), XmlElement(Order = 1, ElementName = "TrackingNumber", IsNullable = true)]
        public string TrackingNumber { get; set; }

        [DataMember(Order = 2, Name = "Name", EmitDefaultValue = false), XmlElement(Order = 2, ElementName = "Name", IsNullable = true)]
        public string Name { get; set; }

        [DataMember(Order = 3, Name = "TransactionID", EmitDefaultValue = false), XmlElement(Order = 3, ElementName = "TransactionID", IsNullable = true)]
        public string TransactionID { get; set; }
    }
}


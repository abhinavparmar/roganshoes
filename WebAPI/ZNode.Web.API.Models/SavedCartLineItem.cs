﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "SavedCartLineItem", Namespace = ""), Serializable, XmlRoot(ElementName = "SavedCartLineItem")]
    public class SavedCartLineItem
    {
        [DataMember(Order = 99, Name = "SavedCartLineItemId"), XmlElement(Order = 0, ElementName = "SavedCartLineItemId")]
        public string SavedCartLineItemId { get; set; }

        [DataMember(Order = 99, Name = "OrderLineItemRelationshipTypeId"), XmlElement(Order = 1, ElementName = "OrderLineItemRelationshipTypeId")]
        public string OrderLineItemRelationshipTypeId { get; set; }

        [DataMember(Order = 99, Name = "ParentLineItem"), XmlElement(Order = 2, ElementName = "ParentLineItem")]
        public string ParentLineItem { get; set; }

        [DataMember(Order = 99, Name = "Quantity"), XmlElement(Order = 3, ElementName = "Quantity")]
        public int Quantity { get; set; }

        [DataMember(Order = 99, Name = "SavedCartId"), XmlElement(Order = 4, ElementName = "SavedCartId")]
        public string SavedCartId { get; set; }

        [DataMember(Order = 99, Name = "SkuId"), XmlElement(Order = 5, ElementName = "SkuId")]
        public string SkuId { get; set; }

        public SavedCartLineItem()
        {

        }
    }
}
﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Seo", Namespace = ""), Serializable]
    public class Seo
    {
        [DataMember(Order = 99,Name = "Description"), XmlElement( Order=1,ElementName = "Description")]
        public string Description { get; set; }

        [DataMember(Order = 99,Name = "Keywords"), XmlElement( Order=2,ElementName = "Keywords")]
        public string Keywords { get; set; }

        [DataMember(Order = 99,Name = "Title"), XmlElement( Order=3,ElementName = "Title")]
        public string Title { get; set; }

        [DataMember(Order = 99,Name = "Url"), XmlElement( Order=4,ElementName = "Url")]
        public string Url { get; set; }
    }
}

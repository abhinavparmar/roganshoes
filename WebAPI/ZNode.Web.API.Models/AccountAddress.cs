﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "AccountAddress", Namespace = ""), Serializable, XmlRoot(ElementName = "AccountAddress")]
    public class AccountAddress
    {
        [DataMember(Order = 99, Name = "AccountId"), XmlElement(Order = 0, ElementName = "AccountId")]
        public string AccountId { get; set; }

        [DataMember(Order = 99,Name = "Address"),XmlElement(Order = 1, ElementName = "Address")]
        public Address Address { get; set; }

        [DataMember(Order = 99, Name = "AddressId"), XmlElement(Order = 2, ElementName = "AddressId")]
        public string AddressId { get; set; }

        [DataMember(Order = 99, Name = "AddressName"), XmlElement(Order = 3, ElementName = "AddressName")]
        public string AddressName { get; set; }

        [DataMember(Order = 99, Name = "IsDefaultBilling"), XmlElement(Order = 4, ElementName = "IsDefaultBilling")]
        public bool IsDefaultBilling { get; set; }

        [DataMember(Order = 99, Name = "IsDefaultShipping"), XmlElement(Order = 5, ElementName = "IsDefaultShipping")]
        public bool IsDefaultShipping { get; set; }

        public AccountAddress()
        {
            Address = new Address();
        }
    }
}
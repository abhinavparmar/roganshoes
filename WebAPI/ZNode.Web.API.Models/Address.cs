﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
#endregion

namespace RogansShoes.Web.API.Models
{
    [DataContract(Name = "Address", Namespace = ""),Serializable,XmlRoot(ElementName = "Address")]
    public class Address
    {
        [DataMember(Order = 99, Name = "City"), XmlElement(Order = 1, ElementName = "City")]
        public string City { get; set; }

        [DataMember(Order = 99, Name = "CompanyName"), XmlElement(Order = 2, ElementName = "CompanyName")]
        public string CompanyName { get; set; }

        [DataMember(Order = 99, Name = "CountryCode"), XmlElement(Order = 3, ElementName = "CountryCode")]
        public string CountryCode { get; set; }

        [DataMember(Order = 99, Name = "FirstName"), XmlElement(Order = 4, ElementName = "FirstName")]
        public string FirstName { get; set; }

        [DataMember(Order = 99, Name = "LastName"), XmlElement(Order = 5, ElementName = "LastName")]
        public string LastName { get; set; }

        [DataMember(Order = 99, Name = "PhoneNumber"), XmlElement(Order = 6, ElementName = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Order = 99, Name = "PostalCode"), XmlElement(Order = 7, ElementName = "PostalCode")]
        public string PostalCode { get; set; }

        [DataMember(Order = 99, Name = "StateCode"), XmlElement(Order = 8, ElementName = "StateCode")]
        public string StateCode { get; set; }

        [DataMember(Order = 99, Name = "StreetAddress1"), XmlElement(Order = 9, ElementName = "StreetAddress1")]
        public string StreetAddress1 { get; set; }

        [DataMember(Order = 99, Name = "StreetAddress2"), XmlElement(Order = 10, ElementName = "StreetAddress2")]
        public string StreetAddress2 { get; set; }

        //Zeon Custom Code : Start
        [DataMember(Order = 99, Name = "Email"), XmlElement(Order = 10, ElementName = "Email")]
        public string Email { get; set; }
        //Zeon Custom Code : End
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Management;
using System.Text;
using System.Web;

namespace ZNode.Libraries.ECommerce.Utilities
{
    public class Zeon_ErrorDiagnostics
    {
        #region Public Method

        /// <summary>
        /// Get Cpu usage Pecentage
        /// </summary>
        /// <returns>string</returns>
        public string GetCPUUsagePercentage()
        {
            StringBuilder sbString = new StringBuilder();
            string cpuLoadPercentage = string.Empty;
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Processor");
                if (searcher != null)
                {
                    foreach (ManagementObject queryObj in searcher.Get())
                    {
                        if (queryObj != null)
                        {

                            cpuLoadPercentage = Convert.ToString(queryObj["LoadPercentage"]).Trim();
                            if (string.IsNullOrEmpty(cpuLoadPercentage))
                            {
                                cpuLoadPercentage = "0";
                            }
                            sbString.Append("CPU Load Percentage= ");
                            sbString.Append(cpuLoadPercentage);
                            sbString.Append(" %<br/>");
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            return sbString.ToString();
        }

        /// <summary>
        /// Get Physical Memory Usage
        /// </summary>
        /// <returns>string</returns>
        public string GetPhysicalMemoryUsage()
        {
            StringBuilder sbString = new StringBuilder();
            try
            {
                ManagementObjectSearcher searcher1 = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_OperatingSystem");
                if (searcher1 != null)
                {
                    foreach (ManagementObject queryObj in searcher1.Get())
                    {
                        if (queryObj != null)
                        {
                            //kb
                            //physical memory
                            string FreePhysicalMemory = Convert.ToString(queryObj["FreePhysicalMemory"]).Trim();
                            sbString.Append("Free Physical Memory= ");
                            sbString.Append(FreePhysicalMemory);
                            sbString.Append(" kb<br/>");

                            string TotalVisibleMemorySize = Convert.ToString(queryObj["TotalVisibleMemorySize"]).Trim();
                            sbString.Append("Total Physical Memory= ");
                            sbString.Append(TotalVisibleMemorySize);
                            sbString.Append(" kb<br/>");

                            //virtual memory
                            string FreeVirtualMemory = Convert.ToString(queryObj["FreeVirtualMemory"]).Trim();
                            sbString.Append("Free Virtual Memory= ");
                            sbString.Append(FreeVirtualMemory);
                            sbString.Append(" kb<br/>");

                            string TotalVirtualMemorySize = Convert.ToString(queryObj["TotalVirtualMemorySize"]).Trim();
                            sbString.Append("Total Virtual Memory= ");
                            sbString.Append(TotalVirtualMemorySize);
                            sbString.Append(" kb<br/>");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            return sbString.ToString();
        }

        /// <summary>
        /// Get SQL server usage
        /// </summary>
        /// <returns>string</returns>
        public string GetSQLServerUsage()
        {
            StringBuilder sbString = new StringBuilder();
            int iTotalWorkingSetSize = 0;
            int iTotalPeakWorkingSetSize = 0;
            int iTotalPageFileUsage = 0;
            int iTotalPeakPageFileUsage = 0;

            try
            {
                ManagementObjectSearcher searcher1 = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Process WHERE name='sqlservr.exe'");
                if (searcher1 != null)
                {
                    foreach (ManagementObject queryObj in searcher1.Get())
                    {
                        if (queryObj != null)
                        {
                            //bytes
                            //Memory Usage = WorkingSetSize (WMI Property)
                            //Peak Memory Usage = PeakWorkingSetSize (WMI Property)
                            //VM Size = PageFileUsage
                            //Peak VM Size = PeakPageFileUsage

                            int WorkingSetSize = 0;
                            if (queryObj["WorkingSetSize"] != null)
                            {
                                int.TryParse(queryObj["WorkingSetSize"].ToString(), out WorkingSetSize);
                            }

                            iTotalWorkingSetSize = iTotalWorkingSetSize + (WorkingSetSize / 1024);


                            int PeakWorkingSetSize = 0;
                            if (queryObj["PeakWorkingSetSize"] != null)
                            {
                                int.TryParse(queryObj["PeakWorkingSetSize"].ToString(), out PeakWorkingSetSize);
                            }
                            iTotalPeakWorkingSetSize = iTotalPeakWorkingSetSize + (PeakWorkingSetSize / 1024);


                            int PageFileUsage = 0;
                            if (queryObj["PageFileUsage"] != null)
                            {
                                int.TryParse(queryObj["PageFileUsage"].ToString(), out PageFileUsage);
                            }
                            iTotalPageFileUsage = iTotalPageFileUsage + (PageFileUsage / 1024);


                            int PeakPageFileUsage = 0;
                            if (queryObj["PeakPageFileUsage"] != null)
                            {
                                int.TryParse(queryObj["PeakPageFileUsage"].ToString(), out PeakPageFileUsage);
                            }
                            iTotalPeakPageFileUsage = iTotalPeakPageFileUsage + (PeakPageFileUsage / 1024);

                        }
                    }


                    //
                    sbString.Append("Memory Usage= ");
                    sbString.Append(iTotalWorkingSetSize.ToString());
                    sbString.Append(" kb<br/>");

                    sbString.Append("Peak Memory Usage= ");
                    sbString.Append(iTotalPeakWorkingSetSize.ToString());
                    sbString.Append(" kb<br/>");

                    sbString.Append("VM Usage= ");
                    sbString.Append(iTotalPageFileUsage.ToString());
                    sbString.Append(" kb<br/>");

                    sbString.Append("Peak VM Usage= ");
                    sbString.Append(iTotalPeakPageFileUsage.ToString());
                    sbString.Append(" kb<br/>");

                    //using (PerformanceCounter pcProcess1 = new PerformanceCounter("Process", "% Processor Time", "sqlservr"))
                    //{
                    //    pcProcess1.NextValue();
                    //    System.Threading.Thread.Sleep(1000);
                    //    sbString.Append("SQL Process CPU %= ");
                    //    sbString.Append(pcProcess1.NextValue());
                    //    sbString.Append("<br/>");
                    //}
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            return sbString.ToString();
        }

        /// <summary>
        /// Get Web site worker memory usage
        /// </summary>
        /// <returns>string</returns>
        public string GetWebSiteWorkerMemoryUsage()
        {
            StringBuilder sbString = new StringBuilder();

            try
            {
                System.Diagnostics.Process currProcess = System.Diagnostics.Process.GetCurrentProcess();
                if (currProcess != null)
                {
                    sbString.Append("Process ID= ");
                    sbString.Append(currProcess.Id);
                    sbString.Append("<br/>");

                    sbString.Append("Process Name= ");
                    sbString.Append(currProcess.ProcessName);
                    sbString.Append("<br/>");

                    sbString.Append("Memory Usage= ");
                    sbString.Append((currProcess.WorkingSet64 / 1024).ToString());
                    sbString.Append(" kb<br/>");


                    sbString.Append("Peak Memory Usage= ");
                    sbString.Append((currProcess.PeakWorkingSet64 / 1024).ToString());
                    sbString.Append(" kb<br/>");


                    sbString.Append("VM Usage= ");
                    sbString.Append((currProcess.VirtualMemorySize64 / 1024).ToString());
                    sbString.Append(" kb<br/>");


                    sbString.Append("Peak VM Usage= ");
                    sbString.Append((currProcess.PeakVirtualMemorySize64 / 1024).ToString());
                    sbString.Append(" kb<br/>");


                    //sbString.Append("Process Start Time= ");
                    //sbString.Append(currProcess.StartTime);
                    //sbString.Append("<br/>");

                    //sbString.Append("Process Responding= ");
                    //sbString.Append(currProcess.Responding);
                    //sbString.Append("<br/>");


                    //using (PerformanceCounter pcProcess = new PerformanceCounter("Process", "% Processor Time", currProcess.ProcessName))
                    //{
                    //    pcProcess.NextValue();
                    //    System.Threading.Thread.Sleep(1000);
                    //    sbString.Append("Process CPU %= ");
                    //    sbString.Append(pcProcess.NextValue());
                    //    sbString.Append("<br/>");
                    //}

                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            return sbString.ToString();
        }

        /// <summary>
        /// Get Process Memory Usage for particular Process Name
        /// </summary>
        /// <param name="processName">processName for search</param>
        /// <returns></returns>
        public string GetProcessMemoryUsage(string processName)
        {
            StringBuilder sbString = new StringBuilder();

            try
            {
                if (!string.IsNullOrEmpty(processName))
                {
                    ManagementObjectSearcher searcher1 = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Process WHERE name='" + processName + "'");
                    if (searcher1 != null)
                    {
                        foreach (ManagementObject queryObj in searcher1.Get())
                        {
                            if (queryObj != null)
                            {
                                //bytes
                                //Memory Usage = WorkingSetSize (WMI Property)
                                //Peak Memory Usage = PeakWorkingSetSize (WMI Property)
                                //VM Size = PageFileUsage
                                //Peak VM Size = PeakPageFileUsage

                                if (queryObj["ProcessID"] != null)
                                {
                                    sbString.Append("Process ID= ");
                                    sbString.Append(queryObj["ProcessID"].ToString());
                                    sbString.Append("<br/>");
                                }

                                int WorkingSetSize = 0;
                                if (queryObj["WorkingSetSize"] != null)
                                {
                                    int.TryParse(queryObj["WorkingSetSize"].ToString(), out WorkingSetSize);
                                }
                                sbString.Append("Memory Usage= ");
                                sbString.Append((WorkingSetSize / 1024).ToString());
                                sbString.Append(" kb<br/>");

                                int PeakWorkingSetSize = 0;
                                if (queryObj["PeakWorkingSetSize"] != null)
                                {
                                    int.TryParse(queryObj["PeakWorkingSetSize"].ToString(), out PeakWorkingSetSize);
                                }
                                sbString.Append("Peak Memory Usage= ");
                                sbString.Append((PeakWorkingSetSize / 1024).ToString());
                                sbString.Append(" kb<br/>");

                                int PageFileUsage = 0;
                                if (queryObj["PageFileUsage"] != null)
                                {
                                    int.TryParse(queryObj["PageFileUsage"].ToString(), out PageFileUsage);
                                }
                                sbString.Append("VM Usage= ");
                                sbString.Append((PageFileUsage / 1024).ToString());
                                sbString.Append(" kb<br/>");

                                int PeakPageFileUsage = 0;
                                if (queryObj["PeakPageFileUsage"] != null)
                                {
                                    int.TryParse(queryObj["PeakPageFileUsage"].ToString(), out PeakPageFileUsage);
                                }
                                sbString.Append("Peak VM Usage= ");
                                sbString.Append((PeakPageFileUsage / 1024).ToString());
                                sbString.Append(" kb<br/>");

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            return sbString.ToString();
        }

        /// <summary>
        /// Get SEO URL Details of current SEO URL
        /// </summary>
        /// <param name="currentSEOURL">Current url</param>
        /// <returns>Data Set</returns>
        public DataSet GetSEOURLDetailsBySEOURL(string currentSEOURL)
        {
            // Create Instance of Connection     
            using (SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create Instance of Command Object
                SqlDataAdapter mySqlAdapter = new SqlDataAdapter("Zeon_GetSEOURLDetails", myConnection);

                // Mark the Command as a SPROC
                mySqlAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySqlAdapter.SelectCommand.CommandTimeout = 0;

                // Add Parameters to SPROC
                SqlParameter parameterSEOURL = new SqlParameter("@SEOURL", SqlDbType.NVarChar);
                parameterSEOURL.Value = currentSEOURL;
                mySqlAdapter.SelectCommand.Parameters.Add(parameterSEOURL);

                // Create and Fill the DataSet
                DataSet myDataSet = new DataSet();
                myConnection.Open();
                mySqlAdapter.Fill(myDataSet);

                //Release Resources
                mySqlAdapter.Dispose();
                myConnection.Close();

                ///Logging Error Into DataBase table type "404-PageNotFound"                                                                                                                                                                                                                                               
                if (myDataSet != null && myDataSet.Tables.Count <= 0)
                {
                    string URL = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.HttpContext.Current.Request.ApplicationPath;
                    string brokenLink = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToString();
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.PageNotFound_404, "404-Page Not Found Error", "Site URL:" + URL, "Missing/Broken Link:" + brokenLink, "Broken Item SEO Link:" + GetRequestedSEOUrlLink(brokenLink), "Page Not Found", GetRequestedSEOUrlLink(brokenLink),brokenLink);
                }
                //Return DataSet
                return myDataSet;
            }

        }

        #endregion

        #region Private Helper Methods

        /// <summary>
        /// Request for SEO URL
        /// </summary>
        /// <param name="brokenLinkUrl">brokenLinkUrl used to generate SEo url</param>
        /// <returns>SEO url in string</returns>
        private string GetRequestedSEOUrlLink(string brokenLinkUrl)
        {
            string requestedSEOUrl = string.Empty;
            string leftURL = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.HttpContext.Current.Request.ApplicationPath;
            if (leftURL.Trim().EndsWith("/") == false)
            {
                leftURL = leftURL.Trim() + "/";
            }
            requestedSEOUrl = leftURL + GetSEOUrlNameFromBrokenLink(brokenLinkUrl) + ".aspx";
            return requestedSEOUrl;
        }

        /// <summary>
        /// Get SEO URL Name
        /// </summary>
        /// <param name="brokenLinkUrl">brokenLinkUrl used to retrive URL name</param>
        /// <returns>SEO URL name in String</returns>
        private string GetSEOUrlNameFromBrokenLink(string brokenLinkUrl)
        {
            string seoUrlName = string.Empty;
            int startIndex = 0;
            int endIndex = 0;

            startIndex = brokenLinkUrl.LastIndexOf("/");
            if (startIndex > 0)
            {
                endIndex = brokenLinkUrl.LastIndexOf(".aspx");
                if (endIndex > startIndex && endIndex > 0)
                {
                    int length = endIndex - startIndex;
                    seoUrlName = brokenLinkUrl.Substring(startIndex, length);
                    seoUrlName = seoUrlName.Trim('/');
                }
            }
            return seoUrlName;
        }

        #endregion
    }
}

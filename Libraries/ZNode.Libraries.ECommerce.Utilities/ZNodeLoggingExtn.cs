﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Utilities
{
    public partial class ZNodeLogging : ZNodeLoggingBase
    {
        public new enum ErrorNumExtn
        {
            /// <summary>
            /// Represents the Avalar Tax failed error
            /// </summary>
            AvalaraTaxFailed = 10000,

            TriggerEmailFailed = 10001,
            
            /// <summary>
            /// Represents the Page Not Found trigger error
            /// </summary>
            PageNotFound_404=10002
        }
    }
}

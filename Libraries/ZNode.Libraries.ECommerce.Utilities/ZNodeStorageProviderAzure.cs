using System;
using System.Configuration;
using System.Configuration.Provider;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Utilities
{
    public class ZNodeStorageProviderAzure : ZNodeStorageProviderBase
    {
        /// <summary>
        /// Blob connection string.
        /// </summary>
        private string connectionString;

        #region Public Methods

        /// <summary>
        /// Initialize the Provider base.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if ((config == null) || (config.Count == 0))
            {
                throw new ArgumentNullException("You must supply a valid configuration dictionary.");
            }

            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Default File storage provider ");
            }

            // Let ProviderBase perform the basic initialization
            base.Initialize(name, config);

            // Get the connection string
            string connectionStringName = config["connectionStringName"];
            if (String.IsNullOrEmpty(connectionStringName))
            {
                throw new ProviderException("You must specify a connectionStringName attribute.");
            }

            ConnectionStringsSection cs =
                (ConnectionStringsSection)ConfigurationManager.GetSection("connectionStrings");

            if (cs == null)
            {
                throw new ProviderException("An error occurred retrieving the connection strings section.");
            }

            if (cs.ConnectionStrings[connectionStringName] == null)
            {
                throw new ProviderException("The connection string could not be found in the connection strings section.");
            }
            else
            {
                this.connectionString = cs.ConnectionStrings[connectionStringName].ConnectionString;
            }

            if (String.IsNullOrEmpty(this.connectionString))
            {
                throw new ProviderException("The connection string is invalid. It should be valid Windows Azure Blob connection string");
            }

            config.Remove("connectionStringName");

            // Check to see if any attributes were set in configuration that does not need for file writing.
            if (config.Count > 0)
            {
                string extraAttribute = config.GetKey(0);
                if (!String.IsNullOrEmpty(extraAttribute))
                {
                    throw new ProviderException("The following unrecognized attribute was found in " + Name + "'s configuration: '" +
                                                extraAttribute + "'");
                }
                else
                {
                    throw new ProviderException("An unrecognized attribute was found in the provider's configuration.");
                }
            }
        }
                
        /// <summary>
        /// Reads binary file (such as Images) from persistant storage.
        /// </summary>
        /// <param name="filePath">Specify the relative path of the file.</param>
        /// <returns>Returns the binary content of the file.</returns>
        public override byte[] ReadBinaryStorage(string filePath)
        {
            try
            {
                CloudBlob myBlob = this.GetBlob(filePath);
                
                myBlob.FetchAttributes();

                byte[] fileData = myBlob.DownloadByteArray();                

                return fileData;
            }
            catch (Exception)
            {                
                throw;
            }    
        }
       
        /// <summary>
        /// Writes binary file (such as Images) to persistant storage.
        /// </summary>
        /// <param name="fileData">Specify the binrary file content.</param>
        /// <param name="filePath">Specify the relative file path.</param>
        public override void WriteBinaryStorage(byte[] fileData, string filePath)
        {
            try
            {
                CloudBlob myBlob = this.GetBlob(filePath);

                myBlob.DeleteIfExists();

                // Create blob stream                
                myBlob.UploadByteArray(fileData);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Reads text file from persistant storage.
        /// </summary>
        /// <param name="filePath">Specify the relative file path.</param>
        /// <returns>returns the text content of the file.</returns>
        public override string ReadTextStorage(string filePath)
        {
            try
            {
                CloudBlob myBlob = this.GetBlob(filePath);
                
                myBlob.FetchAttributes();

                string fileData = myBlob.DownloadText();

                return fileData;
            }
            catch (Exception)
            {
                throw;
            }    
        }
        
        /// <summary>
        /// Writes text file to persistant storage.
        /// </summary>
        /// <param name="fileData">Specify the string that has the file content.</param>
        /// <param name="filePath">Specify the relative file path.</param>
        /// <param name="fileMode">Specify the file write mode operatation. </param>
        public override void WriteTextStorage(string fileData, string filePath, Mode fileMode)
        {
            try
            {
                CloudBlob myBlob = this.GetBlob(filePath);

                if (fileMode == Mode.Append && this.Exists(filePath))
                {
                    fileData = string.Concat(this.ReadTextStorage(filePath), fileData);
                }
                
                myBlob.DeleteIfExists();
               
                myBlob.UploadText(fileData);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Deletes the file from persistant storage.
        /// </summary>
        /// <param name="filePath">Specify the relative file path.</param>
        public override bool DeleteStorage(string filePath)
        {
            try
            {
                CloudBlob myBlob = this.GetBlob(filePath);

                myBlob.DeleteIfExists();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Check file exists in persistant storage.
        /// </summary>
        /// <param name="filePath">Specify the relative file path.</param>
        /// <returns>Returns boolean indicating the file exists (TRUE) or not (FALSE).</returns>
        public override bool Exists(string filePath)
        {            
            try
            {
                CloudStorageAccount myStorageAccount = CloudStorageAccount.Parse(this.connectionString);
                CloudBlobClient myBlobClient = myStorageAccount.CreateCloudBlobClient();

                filePath = filePath.ToLower().Replace("\\", "/");
                filePath = filePath.Contains("~/") ? filePath.Remove(0, filePath.IndexOf("~/") + 2) : filePath;

                // List all the blobs with prefix, in our case we are passing full path and it should be 1 all time if exists.
                foreach (IListBlobItem item in myBlobClient.ListBlobsWithPrefix(filePath))
                {
                    // if found and loop iterates, return true;
                    return true;
                }

                // if not found any blobs, return false
                return false;                
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get HttpPath of file from file storage.
        /// </summary>
        /// <param name="filePath">Specify the relative file path.</param>
        /// <returns>Returns HttpPath of file.</returns>
        public override string HttpPath(string filePath)
        {
            try
            {
                string httpUrl = string.Empty;
                if (!filePath.StartsWith("http://") && !filePath.StartsWith("https://") && Exists(filePath))
                {
                    CloudBlob myBlob = this.GetBlob(filePath);

                    myBlob.FetchAttributes();

                    httpUrl = myBlob.Uri.ToString();
                }
                else
                {
                    httpUrl = filePath;
                }

                return httpUrl;

            }
            catch (Exception)
            {
                throw;
            }    
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Gets the blob container using the connection string.
        /// </summary>
        /// <param name="filePath">Specify the relative path of the file.</param>
        /// <returns>Returns the blob container.</returns>
        private CloudBlobContainer GetBlobContainer(string filePath)
        {
            try
            {
                CloudStorageAccount myStorageAccount = CloudStorageAccount.Parse(this.connectionString);

                CloudBlobClient myBlobClient = myStorageAccount.CreateCloudBlobClient();                

                filePath = filePath.ToLower().Replace("\\", "/");
                string dirPath = filePath.Contains("~/") ? filePath.Remove(0, filePath.IndexOf("~/") + 2) : filePath;
                dirPath = dirPath.Substring(0, 1) == "/" ? dirPath.Remove(0, 1) : dirPath;
                dirPath = dirPath.Substring(0, dirPath.IndexOf("/"));
                CloudBlobContainer myBlobContainer = myBlobClient.GetContainerReference(dirPath);
                
                // Create the blob container in cloud storage if it does not already exist
                myBlobContainer.CreateIfNotExist();

                // Set the Blob access to Public.
                BlobContainerPermissions permis = new BlobContainerPermissions();
                permis.PublicAccess = BlobContainerPublicAccessType.Blob;
                myBlobContainer.SetPermissions(permis);

                return myBlobContainer;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get the Cloud blob reference.
        /// </summary>
        /// <param name="filePath">Specify the relative file path.</param>
        /// <returns>Returns the CloudBlob object</returns>
        private CloudBlob GetBlob(string filePath)
        {
            // Convert all paths to lowercase, to ignore case-sensitive problem.
            string tmpfilePath = filePath.ToLower().Replace("\\", "/");

            // Get the storage container.
            CloudBlobContainer cbc = this.GetBlobContainer(tmpfilePath);

            tmpfilePath = tmpfilePath.Contains("~/") ? tmpfilePath.Remove(0, tmpfilePath.IndexOf("~/") + 2) : tmpfilePath;
            string blobName = tmpfilePath.Remove(0, tmpfilePath.IndexOf("/", 2) + 1);

            CloudBlob myBlob = cbc.GetBlobReference(blobName);

            // Create blob stream
            myBlob.Metadata["FileName"] = blobName;


            return myBlob;
        }

        #endregion
    }
}

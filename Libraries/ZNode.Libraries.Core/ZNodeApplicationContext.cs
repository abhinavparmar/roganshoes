﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;

namespace ZNode.Libraries.Core
{
    public static class ZNodeApplicationContext
    {
        private const string ItemKey_LocalContext = "Znode.LocalContext";
        private const string ItemKey_CurrentHttpContext = "Znode.CurrentHttpContext";

        public static ContextDictionary LocalContext
        {
            get
            {
                ContextDictionary ctx = GetLocalContext();
                if (ctx == null)
                {
                    ctx = new ContextDictionary();
                    SetLocalContext(ctx);
                }
                return ctx;
            }
        }

        private static ContextDictionary GetLocalContext()
        {
            if (HttpContext.Current == null)
            {
                LocalDataStoreSlot slot = Thread.GetNamedDataSlot(ItemKey_LocalContext);
                return (ContextDictionary)Thread.GetData(slot);
            }
            else
                return (ContextDictionary)HttpContext.Current.Items[ItemKey_LocalContext];
        }

        private static void SetLocalContext(ContextDictionary localContext)
        {
            if (HttpContext.Current == null)
            {
                LocalDataStoreSlot slot = Thread.GetNamedDataSlot(ItemKey_LocalContext);
                Thread.SetData(slot, localContext);
            }
            else
                HttpContext.Current.Items[ItemKey_LocalContext] = localContext;
        }

        public static void Initialize(HttpContextBase httpContext)
        {
            var ctx = LocalContext;

            ctx[ItemKey_CurrentHttpContext] = httpContext;
        }

        public static void SetApplicationItem(string key, object itemValue)
        {
            var application = CurrentApplication;
            application[key] = itemValue;
        }

        public static object GetApplicationItem(string key)
        {
            var application = CurrentApplication;
            var result = application[key];
            return result;
        }

        public static T GetApplicationItem<T>(string key)
        {
            var result = (T)GetApplicationItem(key);
            return result;
        }

        public static void SetSessionItem(string key, object itemValue)
        {
            var session = CurrentSession;
            session[key] = itemValue;
        }

        public static object GetSessionItem(string key)
        {
            var session = CurrentSession;
            var result = session[key];
            return result;
        }

        public static T GetSessionItem<T>(string key)
        {
            var result = (T)GetSessionItem(key);
            return result;
        }

        internal static HttpContextBase CurrentHttpContext
        {
            get
            {
                var httpContext = (HttpContextBase)LocalContext[ItemKey_CurrentHttpContext];
                httpContext = httpContext ?? new HttpContextWrapper(HttpContext.Current);
                if(httpContext == null)
                    throw new InvalidOperationException("HttpContext is not initialized. Pass an HttpContextBase instance to the Initialize() method.");
                return httpContext;
            }
        }

        internal static HttpApplicationStateBase CurrentApplication
        {
            get
            {
                var application = CurrentHttpContext.Application;
                return application;
            }
        }

        internal static HttpSessionStateBase CurrentSession
        {
            get
            {
                var session = CurrentHttpContext.Session;
                return session;
            }
        }
    }
}

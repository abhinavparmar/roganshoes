using System;
using System.Data;
using System.Text;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Taxes
{
    /// <summary>
    /// Class factory for creating Tax Rules based on a specific Tax Option selected.
    /// </summary>
    public class ZNodeTaxOption : ZNodeBusinessBase
    {
        #region Private Member Variables
        private const string _ErrorMessage = "Error on Tax Rules: Invalid Destination Country or State code.";
        private ZNodeShoppingCart _ShoppingCart;
        private ZNodeGenericCollection<IZNodeTaxRule> _TaxRules = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeTaxOption class.
        /// </summary>
        /// <param name="ShoppingCart">Shopping Cart details</param>
        public ZNodeTaxOption(ZNodeShoppingCart ShoppingCart)
        {
            this._ShoppingCart = ShoppingCart;
            this.BindTaxRules();
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeTaxOption class.
        /// </summary>
        protected ZNodeTaxOption()
        {
            this.BindTaxRules();
        }
        #endregion

        #region Protected Properties
        /// <summary>
        /// Gets a collection of Tax Rules from the database.
        /// </summary>
        protected DataView TaxRules
        {
            get
            {
                // get all tax rules
                DataView dataView = new DataView();
                string countryCode = string.Empty;
                string stateCode = string.Empty;
                StringBuilder taxClassIds = new StringBuilder();//Zeon custom code

                if (this._ShoppingCart != null)
                {
                    //Zeon Custom Code : Start
                    //countryCode = this._ShoppingCart.Payment.BillingAddress.CountryCode;
                    countryCode = this._ShoppingCart.Payment.ShippingAddress.CountryCode;
                    stateCode = this._ShoppingCart.Payment.ShippingAddress.StateCode;
                    foreach (ZNodeShoppingCartItem item in this._ShoppingCart.ShoppingCartItems)
                    {                      
                        taxClassIds.Append(item.Product.TaxClassID + ",");                              
                    }
                    //Zeon Custom Code : End
                }

                //Zeon Custom Code : Start
                if (!string.IsNullOrEmpty(countryCode) && !string.IsNullOrEmpty(stateCode))
                {
                    ZNode.Libraries.DataAccess.Custom.TaxHelper taxHelper = new ZNode.Libraries.DataAccess.Custom.TaxHelper();
                    //DataTable dt = taxHelper.GetActiveTaxRulesByPortalId(ZNodeConfigManager.SiteConfig.PortalID, countryCode);
                    DataTable dt = taxHelper.GetActiveTaxRulesByPortalId(ZNodeConfigManager.SiteConfig.PortalID, countryCode, stateCode, taxClassIds.ToString());

                    dataView = new DataView(dt);

                    if (ZNodeConfigManager.SiteConfig.InclusiveTax)
                    {
                        dataView.RowFilter = "InclusiveInd = false";
                    }
                }
                //Zeon Custom Code : End
                return dataView;
            }
        }

        /// <summary>
        /// Gets a value indicating whether TaxExempt value for the profile
        /// </summary>
        protected bool TaxExemptProfile
        {
            get
            {

                //Zeon Custom Code : Start
                // Get user profile from ProfileCache Session object
                //if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
                //{
                //    ZNode.Libraries.DataAccess.Entities.Profile customerProfile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];

                //    if (customerProfile != null)
                //    {
                //        return customerProfile.TaxExempt;
                //    }
                //}

                int accountId = ZNodeUserAccount.CurrentAccount() != null ? ZNodeUserAccount.CurrentAccount().AccountID : 0;
                ZNodeUserAccount buyer = null;
                if (HttpContext.Current.Session["BuyerUser"] != null)
                {
                    buyer = (ZNodeUserAccount)HttpContext.Current.Session["BuyerUser"];
                    if (buyer != null && buyer.AccountID > 0)
                    {
                        accountId = buyer.AccountID;
                    }
                }
                AccountProfileService service = new AccountProfileService();
                TList<AccountProfile> lstProfile = service.GetByAccountID(accountId);
                foreach (AccountProfile item in lstProfile)
                {
                    ProfileService profileInfoService = new ProfileService();
                    Profile profileInfo = profileInfoService.GetByProfileID(item.ProfileID == null ? 0 : Convert.ToInt32(item.ProfileID));

                    if (profileInfo != null && profileInfo.TaxExempt)
                    {
                        return true;
                    }
                }
                return false;
                //Zeon Custom Code : End
            }
        }

        /// <summary>
        /// Gets or sets the shopping cart
        /// </summary>
        protected ZNodeShoppingCart ShoppingCart
        {
            get { return this._ShoppingCart; }
            set { this._ShoppingCart = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method will update all the appropriate  sales tax cost value to the ZNodeShoppingCart and ShoppingCartItem object.
        /// </summary>
        /// <param name="_ShoppingCart">Shopping cart object for which shipping needs to be calculated</param>
        public void Calculate(ZNodeShoppingCart _ShoppingCart)
        {
            // Reset values            
            this._ShoppingCart.OrderLevelTaxes = 0;
            this._ShoppingCart.TaxRate = 0;

            this._ShoppingCart.PST = 0;
            this._ShoppingCart.HST = 0;
            this._ShoppingCart.GST = 0;
            this._ShoppingCart.VAT = 0;
            this._ShoppingCart.SalesTax = 0;

            // loop through shopping cart
            foreach (ZNodeShoppingCartItem cartItem in this._ShoppingCart.ShoppingCartItems)
            {
                cartItem.IsTaxCalculated = false;
                cartItem.Product.PST = 0;
                cartItem.Product.HST = 0;
                cartItem.Product.GST = 0;
                cartItem.Product.VAT = 0;
                cartItem.Product.SalesTax = 0;

                // To check AddOn 
                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        addOnValue.IsTaxCalculated = false;
                        addOnValue.PST = 0;
                        addOnValue.HST = 0;
                        addOnValue.GST = 0;
                        addOnValue.VAT = 0;
                        addOnValue.SalesTax = 0;
                    }
                }
            }

            foreach (ZNodeTaxRule taxRule in this._TaxRules)
            {
                taxRule.Calculate();
            }
        }

        /// <summary>
        /// Process anything that must be done before the order is submitted.
        /// </summary>
        /// <param name="ShoppingCart">The shopping cart to be updated.</param>
        /// <returns>True if successfull, False if the order should not be submitted.</returns>
        public bool PreSubmitOrderProcess(ZNodeShoppingCart ShoppingCart)
        {
            ZNode.Libraries.DataAccess.Entities.Address destinationAddress = ShoppingCart.Payment.BillingAddress;
            string destinationCountry = destinationAddress.CountryCode;
            string destinationState = destinationAddress.StateCode;

            if (destinationCountry.Length == 0 || destinationState.Length == 0)
            {
                // set shopping cart tax
                ShoppingCart.TaxRate = 0;

                ShoppingCart.AddErrorMessage = _ErrorMessage;

                return false;
            }

            return true;
        }

        /// <summary>
        /// Process anything that must be done after the order has been submitted.
        /// </summary>
        /// <param name="ShoppingCart">The shopping cart to be updated.</param>
        public void PostSubmitOrderProcess(ZNodeShoppingCart ShoppingCart)
        {
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Reads tax rules in from the database and instantiates the appropriate Tax Rule object and binds it to the cart.
        /// </summary>
        protected void BindTaxRules()
        {
            this._TaxRules = new ZNodeGenericCollection<IZNodeTaxRule>();

            if (this.TaxExemptProfile)
            {
                return;
            }

            DataView taxRules = this.TaxRules;

            if (taxRules.Count > 0)
            {
                // apply sorting based on precedence 
                taxRules.Sort = "Precedence";

                System.Reflection.Assembly taxRuleAssebmly = System.Reflection.Assembly.GetExecutingAssembly();

                string assemblyName = taxRuleAssebmly.GetName().Name;

                // loop through tax rules and apply to cart based on precedence
                foreach (DataRowView rule in taxRules)
                {
                    ZNodeTaxProperties taxProperty = new ZNodeTaxProperties();

                    taxProperty.DestinationCountryCode = string.IsNullOrEmpty(rule["DestinationCountryCode"].ToString()) ? null : rule["DestinationCountryCode"].ToString();
                    taxProperty.DestinationStateCode = string.IsNullOrEmpty(rule["DestinationStateCode"].ToString()) ? null : rule["DestinationStateCode"].ToString();
                    taxProperty.CountyFIPS = string.IsNullOrEmpty(rule["CountyFIPS"].ToString()) ? null : rule["CountyFIPS"].ToString();
                    taxProperty.SalesTax = string.IsNullOrEmpty(rule["SalesTax"].ToString()) ? 0 : decimal.Parse(rule["SalesTax"].ToString());
                    taxProperty.ShippingTaxInd = bool.Parse(rule["TaxShipping"].ToString());
                    taxProperty.VAT = string.IsNullOrEmpty(rule["VAT"].ToString()) ? 0 : decimal.Parse(rule["VAT"].ToString());
                    taxProperty.TaxClassId = int.Parse(rule["TaxClassID"].ToString());
                    taxProperty.GST = string.IsNullOrEmpty(rule["GST"].ToString()) ? 0 : decimal.Parse(rule["GST"].ToString());
                    taxProperty.PST = string.IsNullOrEmpty(rule["PST"].ToString()) ? 0 : decimal.Parse(rule["PST"].ToString());
                    taxProperty.HST = string.IsNullOrEmpty(rule["HST"].ToString()) ? 0 : decimal.Parse(rule["HST"].ToString());
                    taxProperty.Custom1 = Convert.ToString(rule["Custom1"]);
                    taxProperty.InclusiveInd = bool.Parse(rule["InclusiveInd"].ToString());

                    string className = rule["ClassName"].ToString();

                    // Instantiate the tax rule based on class name.
                    if (!string.IsNullOrEmpty(className))
                    {
                        try
                        {
                            IZNodeTaxRule taxRule = (IZNodeTaxRule)taxRuleAssebmly.CreateInstance(assemblyName + "." + className);
                            taxRule.Bind(this._ShoppingCart, taxProperty);
                            this._TaxRules.Add(taxRule);
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Error while instantiating tax rule: " + className);
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogObject(typeof(Exception), ex);
                        }
                    }
                }
            }
        }
        #endregion
    }
}


using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Taxes
{
    /// <summary>
    /// Property bac containing various tax settings.
    /// </summary>
	public class ZNodeTaxProperties : ZNodeBusinessBase
    {
        #region Private Member Variables
        private decimal _SalesTax = 0;
        private string _StateCode = string.Empty;
        private string _CountryCode = string.Empty;
        private string _CountyFIPS = string.Empty;
        private bool _ShippingTaxInd = false;
        private decimal _VAT = 0;
        private int _TaxClassId = 0;
        private decimal _GST = 0;
        private decimal _PST = 0;
        private decimal _HST = 0;
        private string _Custom1 = string.Empty;
        private bool _InclusiveInd = false;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the sales tax
        /// </summary>
        public decimal SalesTax
        {
            get { return this._SalesTax; }
            set { this._SalesTax = value; }
        }

        /// <summary>
        /// Gets or sets the VAT
        /// </summary>
        public decimal VAT
        {
            get { return this._VAT; }
            set { this._VAT = value; }
        }

        /// <summary>
        /// Gets the tax rate
        /// </summary>
        public decimal TaxRate
        {
            get { return this._SalesTax + this._VAT + this._GST + this._HST + this._PST; }            
        }

        /// <summary>
        /// Gets or sets the destination state code
        /// </summary>
        public string DestinationStateCode
        {
            get { return this._StateCode; }
            set { this._StateCode = value; }
        }

        /// <summary>
        /// Gets or sets Destination country code
        /// </summary>
        public string DestinationCountryCode 
        {
            get { return this._CountryCode; }
            set { this._CountryCode = value; }
        }

        /// <summary>
        /// Gets or sets the countyFIPS
        /// </summary>
        public string CountyFIPS
        {
            get { return this._CountyFIPS; }
            set { this._CountyFIPS = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether Shipping tax indicator is true or false
        /// </summary>
        public bool ShippingTaxInd
        {
            get { return this._ShippingTaxInd; }
            set { this._ShippingTaxInd = value; }
        }

        /// <summary>
        /// Gets or sets the tax class id
        /// </summary>
        public int TaxClassId
        {
            get { return this._TaxClassId; }
            set { this._TaxClassId = value; }
        }

        /// <summary>
        /// Gets or sets the GST
        /// </summary>
        public decimal GST
        {
            get { return this._GST; }
            set { this._GST = value; }
        }

        /// <summary>
        /// Gets or sets the PST
        /// </summary>
        public decimal PST
        {
            get { return this._PST; }
            set { this._PST = value; }
        }

        /// <summary>
        /// Gets or sets the HST
        /// </summary>
        public decimal HST
        {
            get { return this._HST; }
            set { this._HST = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether inclusive indicator is true or false
        /// </summary>
        public bool InclusiveInd
        {
            get { return this._InclusiveInd; }
            set { this._InclusiveInd = value; }
        }

        /// <summary>
        /// Gets or sets the custom1
        /// </summary>
        public string Custom1
        {
            get { return this._Custom1; }
            set { this._Custom1 = value; }
        }

        #endregion
    }
}

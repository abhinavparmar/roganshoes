using System;
using System.Collections.Generic;
using System.Text;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.DataAccess.Entities;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.ECommerce.Utilities;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Creates a cached object that will calculate and return inclusive/exclusive pricing for a product.
    /// </summary>
    [Serializable()]
    public class ZNodeInculsiveTax : ZNodeBusinessBase
    {

        /// <summary>
        /// Checks to see if the current profile is tax exempt.
        /// </summary>
        protected bool TaxExemptProfile
        {
            get
            {
                // Get user profile from ProfileCache Session object
                if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
                {
                    ZNode.Libraries.DataAccess.Entities.Profile customerProfile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];

                    if (customerProfile != null)
                        return customerProfile.TaxExempt;
                }

                return false;
            }
        }


        /// <summary>
        /// Cache the inclusive tax rules in memory.
        /// </summary>
        public void CacheInclusiveTax()
        {
            if (ZNodeConfigManager.SiteConfig.InclusiveTax && !TaxExemptProfile)
            {
                if (System.Web.HttpContext.Current.Cache["InclusiveTaxRules"] == null)
                {
                    TaxHelper taxHelper = new TaxHelper();

                    DataTable dataTable = taxHelper.GetInclusiveTaxRules();
                    dataTable.TableName = "TaxRules";

                    // Adds the Urlmappings table item to the Cache object
                    ZNodeCacheDependencyManager.Insert("InclusiveTaxRules", dataTable, "ZNodeTaxRule");

                    // Release memory
                    dataTable.Dispose();
                }
            }
        }

        /// <summary>
        /// Gets data from the inclusive tax cache.
        /// </summary>
        private DataTable InclusiveTaxTable
        {
            get
            {
                DataTable dataTable = (DataTable)System.Web.HttpRuntime.Cache.Get("InclusiveTaxRules");

                if (dataTable == null)
                {
                    CacheInclusiveTax();
                    dataTable = (DataTable)System.Web.HttpRuntime.Cache.Get("InclusiveTaxRules");
                }

                return dataTable;
            }
        }


        private DataRowCollection GetRulesByTaxClassId(int TaxClassId)
        {

            DataRowCollection rows = null;

            if (InclusiveTaxTable != null)
            {
                string filterExpression = "TaxClassId=" + TaxClassId;
                DataTable tempTable = new DataTable();
                tempTable = InclusiveTaxTable.Clone();

                DataRow drow = tempTable.NewRow();

                ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();

                if (userAccount != null)
                {
                    filterExpression += " AND (DestinationCountryCode=''";
                    filterExpression += " OR (DestinationCountryCode='" + userAccount.BillingAddress.CountryCode + "'";
                    filterExpression += " AND DestinationStateCode='')";
                    filterExpression += " OR (DestinationCountryCode='" + userAccount.BillingAddress.CountryCode + "'";
                    filterExpression += " AND DestinationStateCode='" + userAccount.BillingAddress.StateCode + "'))";

                    DataRow[] filterRows = InclusiveTaxTable.Select(filterExpression, "CountyFIPS,DestinationStateCode,DestinationCountryCode,Precedence desc");

                    foreach (DataRow dataRow in filterRows)
                    {
                        if (dataRow["DestinationStateCode"].ToString() == "")
                        {
                            drow = dataRow;

                        }
                        else
                        {
                            if (dataRow["CountyFIPS"].ToString() == "")
                            {
                                drow = dataRow;

                            }
                            else
                            {
                                // ZipCode Service
                                ZipCodeService ZipService = new ZipCodeService();
                                ZipCodeQuery filters = new ZipCodeQuery();

                                // Parameters
                                filters.Append(ZipCodeColumn.CountyFIPS, dataRow["CountyFIPS"].ToString());
                                filters.Append(ZipCodeColumn.CityName, userAccount.BillingAddress.City);
                                filters.Append(ZipCodeColumn.StateAbbr, userAccount.BillingAddress.StateCode);
                                filters.Append(ZipCodeColumn.ZIP, userAccount.BillingAddress.PostalCode);

                                // Get ZipCode List
                                TList<ZipCode> ZipCodeList = ZipService.Find(filters.GetParameters());

                                if (ZipCodeList.Count != 0)
                                {
                                    drow = dataRow;

                                }
                            }
                        }
                    }
                    if (drow.RowState != DataRowState.Detached)
                    {
                        tempTable.Rows.Add(drow.ItemArray);
                    }
                    rows = tempTable.Rows;
                }
                else
                {
                    filterExpression += " AND DestinationCountryCode = ''";

                    DataRow[] filterrows = InclusiveTaxTable.Select(filterExpression, "Precedence asc");

                    foreach (DataRow dataRow in filterrows)
                    {
                        drow = dataRow;
                        break;
                    }
                    if (drow.RowState != DataRowState.Detached)
                    {
                        tempTable.Rows.Add(drow.ItemArray);
                    }
                    rows = tempTable.Rows;
                }
            }

            return rows;
        }

        /// <summary>
        /// Gets the product price including the added tax.
        /// </summary>
        /// <param name="ProductPrice"></param>
        /// <returns></returns>
        public decimal GetInclusivePrice(int TaxClassId, decimal ProductPrice)
        {
            decimal totalTax = 0;

            if (ZNodeConfigManager.SiteConfig.InclusiveTax && !TaxExemptProfile)
            {
                if (TaxClassId > 0 && ProductPrice > 0)
                {
                    DataRowCollection rows = GetRulesByTaxClassId(TaxClassId);

                    if (rows != null)
                    {
                        foreach (DataRow row in rows)
                        {
                            decimal taxRate = decimal.Parse(row["TaxRate"].ToString());

                            totalTax += ProductPrice * (taxRate / 100);
                        }
                    }
                }
            }

            totalTax = Math.Round(totalTax, 2);
            return ProductPrice + totalTax;
        }

        /// <summary>
        /// Gets the total tax rate for the given tax Class Id.
        /// </summary>
        /// <param name="ProductPrice"></param>
        /// <returns></returns>
        public decimal GetInclusiveTaxRate(int TaxClassId)
        {
            decimal totalTaxRate = 0;

            if (ZNodeConfigManager.SiteConfig.InclusiveTax && !TaxExemptProfile)
            {
                if (TaxClassId > 0)
                {
                    DataRowCollection rows = GetRulesByTaxClassId(TaxClassId);

                    if (rows != null)
                    {
                        foreach (DataRow row in rows)
                        {
                            decimal taxRate = decimal.Parse(row["TaxRate"].ToString());

                            totalTaxRate += taxRate;
                        }
                    }
                }
            }

            return totalTaxRate;
        }
    }
}

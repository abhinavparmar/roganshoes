﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeon.Libraries.Avatax;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Taxes
{
    class ZeonTaxAvatax : ZNodeTaxRule
    {
        /// <summary>
        /// Override Tax Calculation in Shopping Cart
        /// </summary>
        public override void Calculate()
        {
            if (IsValid)
            {
                int accountId = ZNodeUserAccount.CurrentAccount() != null ? ZNodeUserAccount.CurrentAccount().AccountID : 0;
                Avatax avaTaxCalculation = new Avatax();
                avaTaxCalculation.CalculateTax(_ShoppingCart, accountId, ZNodeConfigManager.SiteConfig.PortalID);
            }
        }
    }
} 
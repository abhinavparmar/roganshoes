﻿using System;

namespace ZNode.Libraries.ECommerce.Taxes
{
	public interface IZNodeTaxRule
    {
        string ClassName { get; }

        string Description { get; }

        string Name { get; }

        int Precedence { get; set; }

        void Bind(ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCart ShoppingCart, ZNodeTaxProperties TaxProperty);

        void Calculate();

        void PostSubmitOrderProcess();
    
        bool PreSubmitOrderProcess();
    }
}

using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Taxes
{
    /// <summary>
    /// Provides tax calculations for stanard Sales, VAT, PST, GST and HST.
    /// </summary>
	public class ZNodeTaxSales : ZNodeTaxRule
    {
        /// <summary>
        /// Calculate this Sale Tax and update the Shopping Cart with the value in the appropriate place.
        /// </summary>
        public override void Calculate()
        {
            if (IsValid)
            {
                decimal taxGST = 0;
                decimal taxPST = 0;
                decimal taxHST = 0;
                decimal taxVAT = 0;
                decimal taxSalesTax = 0;

                // loop through shopping cart
                foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
                {
                    decimal basePrice = cartItem.PromotionalPrice;                   

                    decimal extendedPrice = (cartItem.Product.DiscountAmount > basePrice ? 0 : basePrice - cartItem.Product.DiscountAmount) * cartItem.Quantity;

                    if (cartItem.ExtendedPriceDiscount > extendedPrice)
                    {
                        extendedPrice = 0;
                    }
                    else
                    {
                        extendedPrice -= cartItem.ExtendedPriceDiscount;
                    }

                    if (TaxProperties.ShippingTaxInd)
                    {
                        extendedPrice += cartItem.ShippingCost;
                    }

                    if (cartItem.Product.TaxClassID == TaxProperties.TaxClassId && cartItem.IsTaxCalculated == false)
                    {
                        taxGST = extendedPrice * (TaxProperties.GST / 100);
                        taxPST = extendedPrice * (TaxProperties.PST / 100);
                        taxHST = extendedPrice * (TaxProperties.HST / 100);
                        taxVAT = extendedPrice * (TaxProperties.VAT / 100);
                        taxSalesTax = extendedPrice * (TaxProperties.SalesTax / 100);

                        cartItem.Product.GST += taxGST;
                        cartItem.Product.HST += taxHST;
                        cartItem.Product.PST += taxPST;
                        cartItem.Product.VAT += taxVAT;
                        cartItem.Product.SalesTax += taxSalesTax;                        

                        ShoppingCart.PST += taxPST;
                        ShoppingCart.HST += taxHST;
                        ShoppingCart.GST += taxGST;
                        ShoppingCart.VAT += taxVAT;
                        ShoppingCart.SalesTax += taxSalesTax;

                        cartItem.IsTaxCalculated = true;
                    }

                    // To check AddOn 
                    foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                        {
                            if (addOnValue.TaxClassID == TaxProperties.TaxClassId && addOnValue.IsTaxCalculated == false)
                            {
                                extendedPrice = (addOnValue.DiscountAmount > addOnValue.FinalPrice ? 0 : addOnValue.FinalPrice - addOnValue.DiscountAmount) * cartItem.Quantity;

                                if (TaxProperties.ShippingTaxInd)
                                {
                                    extendedPrice += addOnValue.ShippingCost;
                                }

                                taxGST = extendedPrice * (TaxProperties.GST / 100);
                                taxPST = extendedPrice * (TaxProperties.PST / 100);
                                taxHST = extendedPrice * (TaxProperties.HST / 100);
                                taxVAT = extendedPrice * (TaxProperties.VAT / 100);
                                taxSalesTax = extendedPrice * (TaxProperties.SalesTax / 100);

                                addOnValue.GST += taxGST;
                                addOnValue.HST += taxHST;
                                addOnValue.PST += taxPST;
                                addOnValue.VAT += taxVAT;
                                addOnValue.SalesTax += taxSalesTax;
                                
                                ShoppingCart.PST += taxPST;
                                ShoppingCart.HST += taxHST;
                                ShoppingCart.GST += taxGST;
                                ShoppingCart.VAT += taxVAT;
                                ShoppingCart.SalesTax += taxSalesTax;

                                addOnValue.IsTaxCalculated = true;
                            }
                        }
                    }
                }               
            }            
        }
    }
}

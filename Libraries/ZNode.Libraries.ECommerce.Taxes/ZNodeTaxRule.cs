using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.UserAccount;

namespace ZNode.Libraries.ECommerce.Taxes
{
    /// <summary>
    /// Generic base class for creating tax rules.
    /// </summary>
	public class ZNodeTaxRule : IZNodeTaxRule
    {
        #region Protected Member Variables
        protected ZNodeShoppingCart _ShoppingCart;
        protected ZNodeTaxProperties _TaxProperties;
        protected ZNodeUserAccount _UserAccount;
        protected int _Precedence = 0;
        protected string _ClassName = string.Empty;
        protected string _Name = string.Empty;
        protected string _Description = string.Empty;

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeTaxRule class, By default hide this constructor since the Shopping cart is required for this class.
        /// </summary>
        protected ZNodeTaxRule()
        {
            // Debug.Assert(true, "This class requires a ShoppingCart in the constructor.");
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the current class name.
        /// </summary>
        public string ClassName
        {
            get { return this._ClassName; }
        }

        /// <summary>
        /// Gets the name of this tax rule.
        /// </summary>
        public string Name
        {
            get { return this._Name; }
        }

        /// <summary>
        /// Gets the description for this taxrule.
        /// </summary>
        public string Description
        {
            get { return this._Description; }
        }

        /// <summary>
        /// Gets or sets the order of precedence that this tax rules should be applied.
        /// </summary>
        public int Precedence
        {
            get { return this._Precedence; }
            set { this._Precedence = value; }
        }

        /// <summary>
        /// Gets or sets the Shopping Cart
        /// </summary>
        protected ZNodeShoppingCart ShoppingCart
        {
            get { return this._ShoppingCart; }
            set { this._ShoppingCart = value; }
        }

        /// <summary>
        /// Gets or sets the Tax Properties
        /// </summary>
        protected ZNodeTaxProperties TaxProperties
        {
            get { return this._TaxProperties; }
            set { this._TaxProperties = value; }
        }

        /// <summary>
        /// Gets or sets the user account.
        /// </summary>
        protected ZNodeUserAccount UserAccount
        {
            get { return this._UserAccount; }
            set { this._UserAccount = value; }
        }

        /// <summary>
        /// Gets a value indicating whether true if this tax rule should be applied to this shopping cart.
        /// </summary>
        protected bool IsValid
        {
            get
            {
                bool isValid = false;
                
                //ZNode.Libraries.DataAccess.Entities.Address destinationAddress = this._ShoppingCart.Payment.BillingAddress; //znode old code
                ZNode.Libraries.DataAccess.Entities.Address destinationAddress = this._ShoppingCart.Payment.ShippingAddress; //zeon custom code
                // check if this applies to all countries
                if (this._TaxProperties.DestinationCountryCode == null || destinationAddress.CountryCode == this._TaxProperties.DestinationCountryCode)
                {
                    // check if this applies to all states
                    if (this._TaxProperties.DestinationStateCode == null)
                    {
                        isValid = true;
                    }
                    else if (this._TaxProperties.DestinationStateCode == destinationAddress.StateCode)
                    {
                        // check if this applies to specific state
                        if (this._TaxProperties.CountyFIPS == null)
                        {
                            isValid = true;
                        }
                        else
                        {
                            // ZipCode Service
                            ZipCodeService ZipService = new ZipCodeService();
                            ZipCodeQuery filters = new ZipCodeQuery();

                            // Parameters
                            filters.AppendEquals(ZipCodeColumn.CountyFIPS, this._TaxProperties.CountyFIPS);
                            filters.AppendEquals(ZipCodeColumn.CityName, destinationAddress.City);
                            filters.AppendEquals(ZipCodeColumn.StateAbbr, destinationAddress.StateCode);
                            filters.AppendEquals(ZipCodeColumn.ZIP, destinationAddress.PostalCode);

                            // Get ZipCode List
                            TList<ZipCode> ZipCodeList = ZipService.Find(filters.GetParameters());

                            if (ZipCodeList.Count != 0)
                            {
                                isValid = true;
                            }
                        }
                    }
                }

                return isValid;
            }    
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Binds the Shopping Cart and Promtion data to the Promotion Rule.
        /// </summary>
        /// <param name="ShoppingCart">The current shopping cart.</param>
        /// <param name="TaxProperties">The tax properties.</param>
        public void Bind(ZNodeShoppingCart ShoppingCart, ZNodeTaxProperties TaxProperties)
        {
            this._ShoppingCart = ShoppingCart;
            this._TaxProperties = TaxProperties;
        }

        #endregion

        #region Virtual Public Methods
        /// <summary>
        /// Do any prepration needed before the order is submitted.        
        /// </summary>
        /// <returns>True if everything is good for submitting the order.</returns>
        /// <remarks>Declared virtual so it can be overridden.</remarks>
        public virtual bool PreSubmitOrderProcess()
        {
            // Most promotion rules don't need any special verification.
            return true;
        }

        /// <summary>
        /// Calculate this promotion and update the Shopping Cart with the value in the appropriate place.
        /// </summary>
        public virtual void Calculate()
        {
        }

        /// <summary>
        /// Does any post purchase cleanup such as decreasing coupon counts.
        /// </summary>
        public virtual void PostSubmitOrderProcess()
        {
            // Most promotion rules will not need any further processing after the order is submitted.
        }
        #endregion
    }
}

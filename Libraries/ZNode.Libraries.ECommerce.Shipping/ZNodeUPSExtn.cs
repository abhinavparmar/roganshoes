﻿using System.Collections.Generic;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Shipping;
using System.Linq;

namespace ZNode.Libraries.ECommerce.Shipping
{
    public partial class ZNodeUPS : ZNodeShippingRule
    {
        #region[Methods]

        /// <summary>
        /// Returns shipping cost from all Ups Service on the basis of weight of shoppingcart and postal code
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="shippingAddress"></param>
        /// <returns></returns>
        public List<zUPSRate> GetUPSShippingRate(ZNodeShoppingCart shoppingCart, ZNode.Libraries.DataAccess.Entities.Address shippingAddress, TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList)
        {
            List<zUPSRate> shippingRates = new List<zUPSRate>();
            ZNode.Libraries.Shipping.UPS objGetUPSRate = new ZNode.Libraries.Shipping.UPS();

            ZNodeEncryption encrypt = new ZNodeEncryption();
            objGetUPSRate.ShipperZipCode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
            objGetUPSRate.ShipperCountryCode = ZNodeConfigManager.SiteConfig.ShippingOriginCountryCode;
            objGetUPSRate.UPSKey = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.UPSKey);
            objGetUPSRate.UPSUserID = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.UPSUserName);
            objGetUPSRate.UPSPassword = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.UPSPassword);

            objGetUPSRate.ShipToAddressType = "Commercial"; // ship to address type
            //objGetUPSRate.ShipToAddressType = GetAddressClassificationType(shippingAddress);  //as per uat 0029032 default address type Residential
            objGetUPSRate.PickupType = "06"; // One time pickup
            objGetUPSRate.PackageTypeCode = "02"; // Package 

            // Get Rate
            // Seperate Out Ship Seperately Items from Ship Together Items
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipSeperatelyItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipTogetherItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();


            foreach (ZNodeShoppingCartItem cartItem in shoppingCart.ShoppingCartItems)
            {
                bool IsWeightExist = false;
                if (cartItem.Product.FreeShippingInd)
                {
                    foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            if (!AddOnValue.FreeShippingInd && AddOnValue.Weight > 0)
                            {
                                IsWeightExist = true;
                                break;
                            }
                        }
                    }
                }

                if (cartItem.Product.Weight > 0 && cartItem.Product.ShipSeparately)
                {
                    #region[ZNode Default Code]
                    //if (!cartItem.Product.FreeShippingInd || IsWeightExist)
                    //    ShipSeperatelyItems.Add(cartItem);
                    #endregion

                    #region[Zeon Custom Code]
                    //Purpose :Mantis Issue 0041585: Checkout - Don’t hide shipping options in any case. 
                    ShipSeperatelyItems.Add(cartItem);
                    
                    #endregion
                }
                else if (cartItem.Product.Weight > 0 && !cartItem.Product.ShipSeparately)
                {
                    #region[Znode default code]

                    //if (!cartItem.Product.FreeShippingInd || IsWeightExist)
                    //    ShipTogetherItems.Add(cartItem);

                    #endregion

                    #region[Zeon customization]
                    //Purpose :Mantis Issue 0041585: Checkout - Don’t hide shipping options in any case. 
                    ShipTogetherItems.Add(cartItem);
                    #endregion

                }
            }


            // Shipping Estimate for Ship Together Package
            if (ShipTogetherItems.Count > 0)
            {

                ZNodeShippingPackage ShipTogetherPackage = new ZNodeShippingPackage(ShipTogetherItems, false, true);//ZNode Old Code

                decimal shipTogetherWeight = 0;
                decimal shipTogetherLength = 0;
                decimal shipTogetherWidth = 0;
                decimal shipTogetherHeight = 0;

                // Set weight,length,width and Height             
                //shipTogetherWeight = ShipTogetherPackage.Weight;
                shipTogetherLength = ShipTogetherPackage.Length;
                shipTogetherWidth = ShipTogetherPackage.Width;
                shipTogetherHeight = ShipTogetherPackage.Height;
                

                string package = ShipTogetherPackage.SetShipTogetherPackageProperties(ShipTogetherPackage, out shipTogetherWeight, false);
                shippingRates = objGetUPSRate.GetShippingRate(package, shippingAddress);
                shippingRates = GetShipTogetherItemsRateIncludingHandlingCharge(shippingRates, shippingList);
            }

            // Shipping Estimate for Ship Separately packages
            if (ShipSeperatelyItems.Count > 0)
            {
                foreach (ZNodeShoppingCartItem seperateItem in ShipSeperatelyItems)
                {
                    ZNodeGenericCollection<ZNodeShoppingCartItem> singleItemlist = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
                    singleItemlist.Add(seperateItem);

                    ZNodeShippingPackage seperateItemPackage = new ZNodeShippingPackage(singleItemlist, false, false);

                    // Set weight
                    List<zUPSRate> separateItemShippingRates = new List<zUPSRate>();
                    string package = seperateItemPackage.SetShipSeperatePackageProperties(seperateItemPackage, false);
                    separateItemShippingRates = objGetUPSRate.GetShippingRate(package, shippingAddress);

                    //Set MonetaryValue and Handling charge for each item
                    separateItemShippingRates = GetShipRateListForMultipleQuantity(separateItemShippingRates, seperateItem.Quantity, shippingList);

                    if (shippingRates != null && shippingRates.Count > 0)
                    {
                        shippingRates = MergeShippingRateList(shippingRates, separateItemShippingRates);
                    }
                    else
                    {
                        shippingRates = separateItemShippingRates;
                    }
                }
            }

            if (shippingRates == null)
            {
                return null;
            }
            else
            {
                return shippingRates;
            }
        }

        public List<zUPSRate> GetShipRateListForMultipleQuantity(List<zUPSRate> seperateItemShipRate, int quantity, TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList)
        {

            foreach (zUPSRate upsrate in seperateItemShipRate)
            {
                upsrate.MonetaryValue = upsrate.MonetaryValue * quantity;

                var shippingDetail = shippingList.OfType<ZNode.Libraries.DataAccess.Entities.Shipping>().AsQueryable().SingleOrDefault(i => i.ShippingCode.Equals(upsrate.Code));
                if (shippingDetail != null && shippingDetail.HandlingCharge > 0)
                {
                    upsrate.MonetaryValue = upsrate.MonetaryValue + shippingDetail.HandlingCharge * quantity;
                }
            }

            return seperateItemShipRate;
        }

        public List<zUPSRate> MergeShippingRateList(List<zUPSRate> shippingRates, List<zUPSRate> separateItemShippingRates)
        {
            for (int counter = 0; counter < separateItemShippingRates.Count; counter++)
            {
                shippingRates[counter].MonetaryValue = shippingRates[counter].MonetaryValue + separateItemShippingRates[counter].MonetaryValue;
            }
            return shippingRates;
        }

        public List<zUPSRate> GetShipTogetherItemsRateIncludingHandlingCharge(List<zUPSRate> shippingRates, TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList)
        {
            List<zUPSRate> upsRateList = new List<zUPSRate>();
            if (shippingRates != null && shippingRates.Count > 0)
            {
                foreach (zUPSRate zUpsRate in shippingRates)
                {
                    var shippingDetail = shippingList.OfType<ZNode.Libraries.DataAccess.Entities.Shipping>().AsQueryable().SingleOrDefault(i => i.ShippingCode.Equals(zUpsRate.Code));
                    if (shippingDetail != null && shippingDetail.HandlingCharge > 0)
                    {
                        zUpsRate.MonetaryValue = zUpsRate.MonetaryValue + shippingDetail.HandlingCharge;
                    }
                    upsRateList.Add(zUpsRate);
                }
            }
            return upsRateList;
        }

        #endregion
    }
}

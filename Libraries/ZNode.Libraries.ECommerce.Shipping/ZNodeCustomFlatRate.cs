using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Helper class for calculating Shipping for Flat Rate items.
    /// </summary>
    public partial class ZNodeCustomFlatRate : ZNodeBusinessBase
    {
        #region Public Methods
        /// <summary>
        /// Returns total shipping cost which includes handling charge + shipping rules rate
        /// </summary>
        /// <param name="ShoppingCart">Shopping Cart</param>
        /// <param name="ShippingProperty">Shipping Property</param>
        public void Calculate(ZNodeShoppingCart ShoppingCart, ZNodeShippingProperty ShippingProperty)
        {
            // Local Member Variables
            int totalItemFlatQuantity = 0;
            int addonItemFlatQuantity = 0;
            decimal itemShippingCost = 0;
            int shippingRuleTypeID = 0;
            bool freeShippingInd = false;
            int quantity = 0;
            bool isRuleApplied = false;

            // Filter the rules collection based on shippingruletypeid    
            TList<ShippingRule> filteredShippingRules = ShippingProperty.ShippingRules.FindAll(ShippingRuleColumn.ShippingRuleTypeID, (int)ZNodeShippingRuleType.FlatRatePerItem); // Flat Rate

            if (filteredShippingRules.Count > 0)
            {
                // Loop through each item to find custom shipping cost
                foreach (ZNodeShoppingCartItem cartItem in ShippingProperty.ShoppingCart.ShoppingCartItems)
                {
                    itemShippingCost = 0;
                    totalItemFlatQuantity = 0;
                    isRuleApplied = false;

                    freeShippingInd = cartItem.Product.FreeShippingInd;
                    shippingRuleTypeID = cartItem.Product.ShippingRuleTypeID.GetValueOrDefault(-1);
                    quantity = cartItem.Quantity;

                    // flat
                    if (shippingRuleTypeID == (int)ZNodeShippingRuleType.FlatRatePerItem && (!freeShippingInd))
                    {
                        totalItemFlatQuantity += quantity;
                    }

                    if (totalItemFlatQuantity > 0)
                    {
                        isRuleApplied = this.ApplyRule(filteredShippingRules, totalItemFlatQuantity, out itemShippingCost);

                        if (isRuleApplied && cartItem.Product.ShipSeparately)
                        {
                            cartItem.Product.ShippingCost = itemShippingCost + ShippingProperty.HandlingCharge;
                        }
                        else if (isRuleApplied)
                        {
                            cartItem.Product.ShippingCost = itemShippingCost;
                        }
                    }

                    isRuleApplied = true;
                    bool applyHandlingCharge = false;

                    // Calculate add-Ons Shipping calculation
                    foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            freeShippingInd = AddOnValue.FreeShippingInd;
                            shippingRuleTypeID = AddOnValue.ShippingRuleTypeID.GetValueOrDefault(-1);
                            itemShippingCost = 0;

                            // Flat rate
                            if (shippingRuleTypeID == 0 && !freeShippingInd)
                            {
                                addonItemFlatQuantity = quantity;

                                isRuleApplied &= this.ApplyRule(filteredShippingRules, addonItemFlatQuantity, out itemShippingCost);

                                applyHandlingCharge |= isRuleApplied;

                                if (isRuleApplied)
                                {
                                    AddOnValue.ShippingCost = itemShippingCost;
                                }
                            }
                        }
                    }

                    if (applyHandlingCharge && cartItem.Product.ShipSeparately)
                    {
                        ShoppingCart.OrderLevelShipping += ShippingProperty.HandlingCharge;
                    }
                }
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Apply the rule or not
        /// </summary>
        /// <param name="FilteredShippingRules">Filtered Shipping Rules</param>
        /// <param name="ItemQuantity">Item Quantity</param>
        /// <param name="ItemShippingCost">Item Shipping Cost</param>
        /// <returns>Return true if rule is valid else false</returns>
        private bool ApplyRule(TList<ShippingRule> FilteredShippingRules, int ItemQuantity, out decimal ItemShippingCost)
        {
            bool ruleValid = false;
            ItemShippingCost = 0;

            // foreach item, get shipping rule type
            foreach (ShippingRule rule in FilteredShippingRules)
            {
                ItemShippingCost += rule.BaseCost + (rule.PerItemCost * ItemQuantity);
                ruleValid = true;
            }

            return ruleValid;
        }
        #endregion
    }
}

using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Base class for Shipping Rules
    /// </summary>
    public partial class ZNodeShippingRule : ZNodeBusinessBase, IZNodeShippingRule
    {
        #region Protected Member Variables
        private ZNodeShoppingCart _ShoppingCart;
        private ZNodeShippingProperty _ShippingProperty;
        private string _ClassName = string.Empty;
        private string _Name = string.Empty;
        private string _Description = string.Empty;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeShippingRule class.
        /// </summary>
        protected ZNodeShippingRule()
        {
            // Debug.Assert(true, "This class requires a ShoppingCart in the constructor.");
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the current class name.
        /// </summary>
        public string ClassName
        {
            get { return this._ClassName; }
        }

        /// <summary>
        /// Gets the name of this promotion.
        /// </summary>
        public string Name
        {
            get { return this._Name; }
        }

        /// <summary>
        /// Gets the description for this promotion.
        /// </summary>
        public string Description
        {
            get { return this._Description; }
        }

        /// <summary>
        /// Gets or sets the Shipping Property
        /// </summary>
        protected ZNodeShippingProperty ShippingProperty
        {
            get { return this._ShippingProperty; }
            set { this._ShippingProperty = value; }
        }

        /// <summary>
        /// Gets or sets the Shopping Cart
        /// </summary>
        protected ZNodeShoppingCart ShoppingCart
        {
            get { return this._ShoppingCart; }
            set { this._ShoppingCart = value; }
        }

        /// <summary>
        /// Gets the generic shipping error message 'Unable to calculate shipping rates at this time. Please try again later.'
        /// </summary>
        public string GenericShippingErrorMessage
        {
            get { return "Unable to calculate shipping rates at this time. Please try again later."; }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Binds the Shopping Cart and Promtion data to the Promotion Rule.
        /// </summary>
        /// <param name="ShoppingCart">The current shopping cart.</param>
        /// <param name="ShippingProperty">The shipping properties.</param>
        public void Bind(ZNodeShoppingCart ShoppingCart, ZNodeShippingProperty ShippingProperty)
        {
            this._ShoppingCart = ShoppingCart;
            this._ShippingProperty = ShippingProperty;
        }

        #endregion

        #region Virtual Public Methods
        /// <summary>
        /// Do any prepration needed before the order is submitted.        
        /// </summary>
        /// <returns>True if everything is good for submitting the order.</returns>
        /// <remarks>Declared virtual so it can be overridden.</remarks>
        public virtual bool PreSubmitOrderProcess()
        {
            // Most promotion rules don't need any special verification.
            return true;
        }

        /// <summary>
        /// Calculate this promotion and update the Shopping Cart with the value in the appropriate place.
        /// </summary>
        public virtual void Calculate()
        {
        }

        /// <summary>
        /// Does any post purchase cleanup such as decreasing coupon counts.
        /// </summary>
        public virtual void PostSubmitOrderProcess()
        {
            // Most promotion rules will not need any further processing after the order is submitted.
        }
        #endregion
    }
}

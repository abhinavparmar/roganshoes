﻿using System;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Represents the ZNodeUSPS class
    /// </summary>
    public partial class ZNodeUSPS : ZNodeShippingRule
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeUSPS class.
        /// </summary>
        public ZNodeUSPS()
            : base()
        {
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Returns shipping cost from Ups Service for the items in the cart
        /// </summary>
        public override void Calculate()
        {
            // Local Variables
            ZNodeEncryption encrypt = new ZNodeEncryption();
            ZNode.Libraries.Shipping.USPS shippingService = new ZNode.Libraries.Shipping.USPS();
            decimal itemShippingRate = 0;

            // Set USPS general settings
            shippingService.ShippingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
            shippingService.OriginZipCode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
            shippingService.PDUZip5 = ShoppingCart.Payment.ShippingAddress.PostalCode;
            shippingService.ShippingAddress = ShoppingCart.Payment.ShippingAddress;

            // Seperate Out Ship Seperately Items from Ship Together Items
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipSeperatelyItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipTogetherItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();

            foreach (ZNodeShoppingCartItem cartItem in ShippingProperty.ShoppingCart.ShoppingCartItems)
            {
                bool IsWeightExist = false;
                if (cartItem.Product.FreeShippingInd)
                {
                    foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            if (!AddOnValue.FreeShippingInd && AddOnValue.Weight > 0)
                            {
                                IsWeightExist = true;
                                break;
                            }
                        }
                    }
                }

                if (cartItem.Product.Weight > 0 && cartItem.Product.ShipSeparately)
                {
                    if (!cartItem.Product.FreeShippingInd || IsWeightExist)
                        ShipSeperatelyItems.Add(cartItem);
                }
                else if (cartItem.Product.Weight > 0 && !cartItem.Product.ShipSeparately)
                {
                    if (!cartItem.Product.FreeShippingInd || IsWeightExist)
                        ShipTogetherItems.Add(cartItem);
                }
            }

            // Shipping Estimate for Ship Together Package
            if (ShipTogetherItems.Count > 0)
            {
                ZNodeShippingPackage shipTogetherPackage = new ZNodeShippingPackage(ShipTogetherItems, false, true);

                shippingService.SKU = ShipTogetherItems[0].Product.SKU;
                string[] weightArray = shipTogetherPackage.Weight.ToString("N2").Split('.');
                shippingService.WeightInPounds = weightArray[0].ToString();
                decimal remainder = shipTogetherPackage.Weight - Convert.ToDecimal(weightArray[0]);
                decimal ounces = remainder * 16;
                shippingService.WeightInOunces = ounces.ToString();

                // Get shipping rate for each item
                bool isCalculated = shippingService.CalculateShippingRate();
                if (isCalculated)
                {
                    itemShippingRate += shippingService.ShippingRate;
                }

                // Add handling charge for Ship Together Package
                itemShippingRate += ShippingProperty.HandlingCharge;

                if (shippingService.ErrorCode != "0")
                {
                    ShoppingCart.Shipping.ResponseCode = shippingService.ErrorCode;
                    ShoppingCart.AddErrorMessage = this.GenericShippingErrorMessage;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, "Shipping Error: " + shippingService.ErrorCode + " " + shippingService.ErrorDescription);
                }
            }

            // Shipping Estimate for Ship Separately packages
            if (ShipSeperatelyItems.Count > 0)
            {
                foreach (ZNodeShoppingCartItem seperateItem in ShipSeperatelyItems)
                {
                    ZNodeGenericCollection<ZNodeShoppingCartItem> singleItemlist = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
                    singleItemlist.Add(seperateItem);

                    ZNodeShippingPackage seperateItemPackage = new ZNodeShippingPackage(singleItemlist, false, false);

                    shippingService.SKU = seperateItem.Product.SKU;
                    string[] weightArray = seperateItemPackage.Weight.ToString("N2").Split('.');
                    shippingService.WeightInPounds = weightArray[0].ToString();
                    decimal remainder = seperateItemPackage.Weight - Convert.ToDecimal(weightArray[0]);
                    decimal ounces = remainder * 16;
                    shippingService.WeightInOunces = ounces.ToString();

                    bool isCalculated = shippingService.CalculateShippingRate();
                    if (isCalculated)
                    {
                        itemShippingRate = shippingService.ShippingRate;
                    }

                    // Get shipping rate for each single item and multiple with quantity to total charges of order line item.
                    itemShippingRate += itemShippingRate * seperateItem.Quantity;

                    // Add handling charge for ship seperately item and multiple with quantity to total charges of order line item.
                    itemShippingRate += ShippingProperty.HandlingCharge * seperateItem.Quantity;

                    if (shippingService.ErrorCode != "0")
                    {
                        ShoppingCart.Shipping.ResponseCode = shippingService.ErrorCode;
                        ShoppingCart.AddErrorMessage = this.GenericShippingErrorMessage;

                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, string.Format("Shipping Error for {0} : {1} {2}", seperateItem.Product.Name, shippingService.ErrorCode, shippingService.ErrorDescription));
                    }
                }
            }

            ShoppingCart.OrderLevelShipping += itemShippingRate;
        }

        /// <summary>
        /// Process anything that must be done before the order is submitted.
        /// </summary>
        /// <returns>Returns true if shippign error else false</returns>
        public override bool PreSubmitOrderProcess()
        {
            if (ShoppingCart.Shipping.ResponseCode != "0")
            {
                ShoppingCart.AddErrorMessage = this.GenericShippingErrorMessage;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, "Shipping Error: " + ShoppingCart.Shipping.ResponseCode + " " + ShoppingCart.Shipping.ResponseMessage);
                return false;
            }

            return true;
        }

        #endregion
    }
}

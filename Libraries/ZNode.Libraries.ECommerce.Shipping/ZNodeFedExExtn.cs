﻿using System;
using System.Collections.Generic;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Shipping;
using System.Linq;
using System.Web.Configuration;


namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Represents methods for FedEx shipping rule
    /// </summary>
    public partial class ZNodeFedEx : ZNodeShippingRule
    {
        /// <summary>
        /// Calculate the shipping.
        /// </summary>
        public List<PeftFedEXRates> GetFedExShippingRateList(ZNodeShoppingCart shoppingCart, ZNode.Libraries.DataAccess.Entities.Address shippingAddress, TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList)
        {
            // Local Variables
            string packageType = ZNodeConfigManager.SiteConfig.FedExPackagingType;
            decimal itemShippingRate = 0;
            decimal itemTotalValue = 0;
            decimal shipTogetherWeight = 0;
            decimal shipSeperatelyWeight = 0;
            string fedExPackageType = WebConfigurationManager.AppSettings["FedExPackageType"] != null && string.IsNullOrEmpty(WebConfigurationManager.AppSettings["FedExPackageType"].ToString()) ? WebConfigurationManager.AppSettings["FedExPackageType"].ToString() : "YOUR_PACKAGING";
                
            // Seperate Out Ship Seperately Items from Ship Together Items
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipSeperatelyItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipTogetherItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
            List<PeftFedEXRates> shippingRateList = new List<PeftFedEXRates>();
            string weightUnit = ZNodeConfigManager.SiteConfig.WeightUnit;
            string dimensionUnit = ZNodeConfigManager.SiteConfig.DimensionUnit;

            // Decrypt FedEx account info - Client Detail,Access Key and password
            ZNodeEncryption encrypt = new ZNodeEncryption();

            ZNode.Libraries.Shipping.FedEx shippingFedEx = new ZNode.Libraries.Shipping.FedEx();
            shippingFedEx.ErrorCode = "0";


            // FedEx Service Settings
            // Set FedEx Account Info porperties
            try
            {
                shippingFedEx.FedExAccessKey = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExProductionKey);
                shippingFedEx.FedExAccountNumber = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExAccountNumber);
                shippingFedEx.FedExMeterNumber = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExMeterNumber);
                shippingFedEx.FedExSecurityCode = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExSecurityCode);
            }
            catch
            {
                // Ignore decryption issues
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("ZnodeFedEx.cs - Error decryptin FedEx keys");
            }

            // CSP Account Info
            //shippingFedEx.CSPAccessKey = ZNodeConfigManager.SiteConfig.FedExCSPKey;
            //shippingFedEx.CSPPassword = ZNodeConfigManager.SiteConfig.FedExCSPPassword;
            //shippingFedEx.ClientProductId = ZNodeConfigManager.SiteConfig.FedExClientProductId;
            //shippingFedEx.ClientProductVersion = ZNodeConfigManager.SiteConfig.FedExClientProductVersion;

            // Service type
            shippingFedEx.PackageTypeCode = ZNodeConfigManager.SiteConfig.FedExPackagingType; // Packaging type
            shippingFedEx.DropOffType = ZNodeConfigManager.SiteConfig.FedExDropoffType; // drop-off Type

            // Set Shipping Origin properties
            shippingFedEx.ShipperAddress1 = ZNodeConfigManager.SiteConfig.ShippingOriginAddress1;
            shippingFedEx.ShipperAddress2 = ZNodeConfigManager.SiteConfig.ShippingOriginAddress2;
            shippingFedEx.ShipperCity = ZNodeConfigManager.SiteConfig.ShippingOriginCity;
            shippingFedEx.ShipperZipCode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
            shippingFedEx.ShipperStateCode = ZNodeConfigManager.SiteConfig.ShippingOriginStateCode;
            shippingFedEx.ShipperCountryCode = ZNodeConfigManager.SiteConfig.ShippingOriginCountryCode;

            // Set Destination properties
            shippingFedEx.ShipToAddress1 = shoppingCart.Payment.ShippingAddress.Street;
            shippingFedEx.ShipToAddress2 = shoppingCart.Payment.ShippingAddress.Street1;
            shippingFedEx.ShipToCity = shoppingCart.Payment.ShippingAddress.City;
            shippingFedEx.ShipToZipCode = shoppingCart.Payment.ShippingAddress.PostalCode;
            shippingFedEx.ShipToStateCode = shoppingCart.Payment.ShippingAddress.StateCode;
            shippingFedEx.ShipToCountryCode = shoppingCart.Payment.ShippingAddress.CountryCode;

            // Ship to residence
            #region[ZNode Default Code: Commented:Purpose:Set Always Commercial]
            /*if (!string.IsNullOrEmpty(shoppingCart.Payment.ShippingAddress.CompanyName) && shoppingCart.Payment.ShippingAddress.CompanyName.Length == 0)
            {
                shippingFedEx.ShipToAddressType = true;
            }
            else//
            {
                shippingFedEx.ShipToAddressType = false;
            }*/
            #endregion

            shippingFedEx.ShipToAddressType = false;

            // Apply FedExDiscount rates
            if (ZNodeConfigManager.SiteConfig.FedExUseDiscountRate.HasValue)
            {
                shippingFedEx.UseDiscountRate = ZNodeConfigManager.SiteConfig.FedExUseDiscountRate.Value;
            }

            if (weightUnit.Length > 0)
            {
                shippingFedEx.WeightUnit = weightUnit.TrimEnd(new char[] { 'S' });
            }

            if (dimensionUnit.Length > 0)
            {
                shippingFedEx.DimensionUnit = dimensionUnit;
            }

            shippingFedEx.CurrencyCode = ZNodeCurrencyManager.CurrencyCode();
            bool? AddInsurance = ZNodeConfigManager.SiteConfig.FedExAddInsurance;


            foreach (ZNodeShoppingCartItem cartItem in shoppingCart.ShoppingCartItems)
            {

                if (cartItem.Product.FreeShippingInd)
                {
                    foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            shipTogetherWeight += AddOnValue.Weight;
                        }

                    }
                }
                if (cartItem.Product.Weight > 0 && cartItem.Product.ShipSeparately)
                {
                    //if (!cartItem.Product.FreeShippingInd || IsWeightExist)
                    ShipSeperatelyItems.Add(cartItem);
                }
                else if (cartItem.Product.Weight > 0 && !cartItem.Product.ShipSeparately)
                {
                    //if (!cartItem.Product.FreeShippingInd || IsWeightExist)
                    ShipTogetherItems.Add(cartItem);

                    //Calculating Weight Of Each Item
                    shipTogetherWeight += (cartItem.Product.Weight * cartItem.Quantity);
                    //Calculating Weight Of Each Item
                }


            }

            // Shipping Estimate for Ship Together Package
            if (ShipTogetherItems.Count > 0)
            {
                ZNodeShippingPackage ShipTogetherPackage = new ZNodeShippingPackage(ShipTogetherItems, false, true);

                if (packageType != null && packageType.ToUpper().Equals(fedExPackageType))
                {
                    shippingFedEx.PackageLength = Convert.ToInt32(ShipTogetherPackage.Length).ToString();
                    shippingFedEx.PackageHeight = Convert.ToInt32(ShipTogetherPackage.Height).ToString();
                    shippingFedEx.PackageWidth = Convert.ToInt32(ShipTogetherPackage.Width).ToString();
                }

                // Set weight
                shippingFedEx.PackageWeight = shipTogetherWeight;

                // Add insurance
                itemTotalValue = ShipTogetherPackage.Value;

                if (AddInsurance.GetValueOrDefault(false))
                {
                    shippingFedEx.TotalInsuredValue = itemTotalValue;
                }

                // Add customs
                shippingFedEx.TotalCustomsValue = itemTotalValue;

                // Get shipping rate for each item
                //Creating weight based  packages
                Dictionary<int, decimal> packageCollection = ShipTogetherPackage.GetFedExPackageWithWeight(shippingFedEx.PackageWeight);
                // Get shipping rate for each item
                shippingRateList = shippingFedEx.GetAllShippingRates(packageCollection);
                shippingRateList = GetShipTogetherItemsRateIncludingHandlingCharge(shippingRateList, shippingList);

            }

            // Shipping Estimate for Ship Separately packages
            if (ShipSeperatelyItems.Count > 0)
            {
                foreach (ZNodeShoppingCartItem seperateItem in ShipSeperatelyItems)
                {
                    shipSeperatelyWeight = 0;
                    ZNodeGenericCollection<ZNodeShoppingCartItem> singleItemlist = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
                    singleItemlist.Add(seperateItem);

                    ZNodeShippingPackage seperateItemPackage = new ZNodeShippingPackage(singleItemlist, false, false);

                    if (packageType.ToUpper().Equals(fedExPackageType))
                    {
                        shippingFedEx.PackageLength = Convert.ToInt32(seperateItemPackage.Length).ToString();
                        shippingFedEx.PackageHeight = Convert.ToInt32(seperateItemPackage.Height).ToString();
                        shippingFedEx.PackageWidth = Convert.ToInt32(seperateItemPackage.Width).ToString();
                    }

                    // Set weight of ship sapreately Item
                    shipSeperatelyWeight = (seperateItem.Product.Weight * seperateItem.Quantity);
                    shippingFedEx.PackageWeight = shipSeperatelyWeight;

                    // Add insurance
                    itemTotalValue = seperateItemPackage.Value;

                    if (AddInsurance.GetValueOrDefault(false))
                    {
                        shippingFedEx.TotalInsuredValue = itemTotalValue;
                    }

                    // Add customs
                    shippingFedEx.TotalCustomsValue = itemTotalValue;

                    // Get shipping rate for each item
                    List<PeftFedEXRates> saprateShippingRateList = new List<PeftFedEXRates>();
                    itemShippingRate += shippingFedEx.GetShippingRate();
                    //Creating weight based  packages
                    Dictionary<int, decimal> packageCollection = seperateItemPackage.GetFedExPackageWithWeight(shippingFedEx.PackageWeight);
                    saprateShippingRateList = shippingFedEx.GetAllShippingRates(packageCollection);

                    //Set MonetaryValue and Handling charge for each item
                    saprateShippingRateList = GetShipRateListForMultipleQuantity(saprateShippingRateList, seperateItem.Quantity, shippingList);

                    if (shippingRateList != null && shippingRateList.Count > 0)
                    {
                        shippingRateList = MergeShippingRateList(shippingRateList, saprateShippingRateList);
                    }
                    else
                    {
                        shippingRateList = saprateShippingRateList;
                    }

                    // Add handling charge for ship seperately item
                    itemShippingRate += ShippingProperty.HandlingCharge;
                }
            }
            if (shippingRateList == null)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, "Fed Ex Shipping Error: " + shippingFedEx.ErrorCode + " " + shippingFedEx.ErrorDescription.ToString());
                return null;
            }
            else
            {
                return shippingRateList;
            }
        }

        /// <summary>
        /// Merge Shipping Rate List in Shopping Cart for mixed shipping items
        /// </summary>
        /// <param name="shipTogetherRateList">List<peftFedEXRates></param>
        /// <param name="shipSaprateRateList">List<peftFedEXRates></param>
        /// <returns></returns>
        public List<PeftFedEXRates> MergeShippingRateList(List<PeftFedEXRates> shipTogetherRateList, List<PeftFedEXRates> shipSaprateRateList)
        {
            List<PeftFedEXRates> mergeRateList = new List<PeftFedEXRates>();
            foreach (PeftFedEXRates rateOuter in shipTogetherRateList)
            {
                PeftFedEXRates newRate = new PeftFedEXRates();
                newRate.Code = Convert.ToString(rateOuter.Code);
                newRate.DeliveryTime = rateOuter.DeliveryTime;

                foreach (PeftFedEXRates rateInner in shipSaprateRateList)
                {
                    if (rateOuter.Code.Equals(rateInner.Code))
                    {
                        newRate.MonetaryValue = rateOuter.MonetaryValue + rateInner.MonetaryValue;
                    }
                }
                mergeRateList.Add(newRate);
            }
            return mergeRateList;
        }

        /// <summary>
        /// Add Handeling Charges into Shipping Rate
        /// </summary>
        /// <param name="shippingRates"></param>
        /// <param name="shippingList"></param>
        /// <returns></returns>
        private List<PeftFedEXRates> GetShipTogetherItemsRateIncludingHandlingCharge(List<PeftFedEXRates> shippingRates, TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList)
        {
            List<PeftFedEXRates> upsRateList = new List<PeftFedEXRates>();
            if (shippingRates != null && shippingRates.Count > 0)
            {
                foreach (PeftFedEXRates fedExRate in shippingRates)
                {
                    var shippingDetail = shippingList.OfType<ZNode.Libraries.DataAccess.Entities.Shipping>().AsQueryable().SingleOrDefault(i => i.ShippingCode.Equals(fedExRate.Code));
                    if (shippingDetail != null && shippingDetail.HandlingCharge > 0)
                    {
                        fedExRate.MonetaryValue = fedExRate.MonetaryValue + shippingDetail.HandlingCharge;
                    }
                    upsRateList.Add(fedExRate);
                }
            }
            return upsRateList;
        }

        /// <summary>
        /// Add Handeling Charges into Shipping Rate
        /// </summary>
        /// <param name="seperateItemShipRate"></param>
        /// <param name="quantity"></param>
        /// <param name="shippingList"></param>
        /// <returns></returns>
        public List<PeftFedEXRates> GetShipRateListForMultipleQuantity(List<PeftFedEXRates> seperateItemShipRate, int quantity, TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList)
        {
            foreach (PeftFedEXRates fedExRate in seperateItemShipRate)
            {
                fedExRate.MonetaryValue = fedExRate.MonetaryValue * quantity;

                var shippingDetail = shippingList.OfType<ZNode.Libraries.DataAccess.Entities.Shipping>().AsQueryable().SingleOrDefault(i => i.ShippingCode.Equals(fedExRate.Code));
                if (shippingDetail != null && shippingDetail.HandlingCharge > 0)
                {
                    fedExRate.MonetaryValue = fedExRate.MonetaryValue + shippingDetail.HandlingCharge * quantity;
                }
            }

            return seperateItemShipRate;
        }

    }
}

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Helper class for calculating Custom shipping based on weight.
    /// </summary>
	public class ZNodeCustomWeight : ZNodeBusinessBase
    {
        #region Public Methods
        /// <summary>
        /// Returns total shipping cost which includes handling charge + shipping rules rate
        /// </summary>
        /// <param name="ShoppingCart">Shopping Cart</param>
        /// <param name="ShippingProperty">Shipping Property</param>
        public void Calculate(ZNodeShoppingCart ShoppingCart, ZNodeShippingProperty ShippingProperty)
        {
            // Local Member Variables
            decimal totalItemWeightQuantity = 0;
            decimal addOnItemWeightQuantity = 0;
            decimal itemShippingCost = 0;
            int shippingRuleTypeID = 0;
            bool freeShippingInd = false;
            int quantity = 0;
            bool isRuleApplied = false;

            // Filter the rules collection based on shippingruletypeid    
            TList<ShippingRule> filteredShippingRules = ShippingProperty.ShippingRules.FindAll(ShippingRuleColumn.ShippingRuleTypeID, (int)ZNodeShippingRuleType.RateBasedonWEIGHT);

            if (filteredShippingRules.Count > 0)
            {                
                // Loop through each item to find custom shipping cost
                foreach (ZNodeShoppingCartItem cartItem in ShippingProperty.ShoppingCart.ShoppingCartItems)
                {
                    itemShippingCost = 0;
                    totalItemWeightQuantity = 0;
                    isRuleApplied = false;

                    freeShippingInd = cartItem.Product.FreeShippingInd;
                    shippingRuleTypeID = cartItem.Product.ShippingRuleTypeID.GetValueOrDefault(-1);
                    quantity = cartItem.Quantity;

                    if (shippingRuleTypeID == (int)ZNodeShippingRuleType.RateBasedonWEIGHT && (!freeShippingInd))
                    {
                        totalItemWeightQuantity = cartItem.Product.Weight * quantity;
                    }

                    if (totalItemWeightQuantity > 0)
                    {
                        isRuleApplied = this.ApplyRule(filteredShippingRules, totalItemWeightQuantity, out itemShippingCost);

                        if (isRuleApplied && cartItem.Product.ShipSeparately)
                        {
                            cartItem.ShippingCost = itemShippingCost + ShippingProperty.HandlingCharge;
                        }
                        else if (isRuleApplied)
                        {
                            cartItem.ShippingCost = itemShippingCost;
                        }
                    }

                    isRuleApplied = true;
                    bool applyHandlingCharge = false;

                    // Calculate add-Ons Shipping calculation
                    foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            // Reset 
                            itemShippingCost = 0;
                            freeShippingInd = AddOnValue.FreeShippingInd;
                            shippingRuleTypeID = AddOnValue.ShippingRuleTypeID.GetValueOrDefault(-1);

                            // Weight based rule
                            if (shippingRuleTypeID == 2 && freeShippingInd == false) 
                            {
                                addOnItemWeightQuantity = AddOnValue.Weight * quantity;

                                isRuleApplied = this.ApplyRule(filteredShippingRules, addOnItemWeightQuantity, out itemShippingCost);

                                applyHandlingCharge |= isRuleApplied;

                                if (isRuleApplied)
                                {
                                    AddOnValue.ShippingCost = itemShippingCost;                                    
                                }
                            }
                        }
                    }

                    if (applyHandlingCharge && cartItem.Product.ShipSeparately)
                    {
                        ShoppingCart.OrderLevelShipping += ShippingProperty.HandlingCharge;
                    }
                }
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Apply the rule or not
        /// </summary>
        /// <param name="FilteredShippingRules">Filtered Shipping Rules</param>
        /// <param name="ItemWeightQuantity">Item  Weight Quantity</param>
        /// <param name="ItemShippingCost">Item Shipping Cost</param>
        /// <returns>Return true if rule is valid else false</returns>
        private bool ApplyRule(TList<ShippingRule> FilteredShippingRules, decimal ItemWeightQuantity, out decimal ItemShippingCost)
        {
            bool isRuleApplied = false;
            ItemShippingCost = 0;

            // foreach item, get shipping rule type
            foreach (ShippingRule rule in FilteredShippingRules)
            {
                if (ItemWeightQuantity >= rule.LowerLimit && ItemWeightQuantity <= rule.UpperLimit)
                {
                    ItemShippingCost += rule.BaseCost + (rule.PerItemCost * ItemWeightQuantity);
                    isRuleApplied = true;
                }
            }

            return isRuleApplied;
        }
        #endregion
    }
}

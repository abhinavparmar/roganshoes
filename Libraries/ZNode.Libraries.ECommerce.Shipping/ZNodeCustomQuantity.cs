using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Helper class for calculating Custom Shipping based on Quantity.
    /// </summary>
	public class ZNodeCustomQuantity : ZNodeBusinessBase
    {
        #region Public Methods
        /// <summary>
        /// Returns total shipping cost which includes handling charge + shipping rules rate
        /// </summary>
        /// <param name="ShoppingCart">Shopping Cart</param>
        /// <param name="ShippingProperty">Shipping Property</param>
        public void Calculate(ZNodeShoppingCart ShoppingCart, ZNodeShippingProperty ShippingProperty)
        {
            int totalItemQuantity = 0;
            int addOnItemQuantity = 0;
            decimal itemShippingCost = 0;
            int shippingRuleTypeID = 0;
            bool freeShippingInd = false;
            int quantity = 0;
            bool isRuleApplied = false;
            TList<ShippingRule> filteredShippingRules = ShippingProperty.ShippingRules.FindAll(ShippingRuleColumn.ShippingRuleTypeID, (int)ZNodeShippingRuleType.RateBasedonQUANTITY); // Quantity

            if (filteredShippingRules.Count > 0)
            {
                // Loop through each item to find custom shipping cost
                foreach (ZNodeShoppingCartItem cartItem in ShippingProperty.ShoppingCart.ShoppingCartItems)
                {
                    itemShippingCost = 0;
                    totalItemQuantity = 0;
                    isRuleApplied = false;

                    freeShippingInd = cartItem.Product.FreeShippingInd;
                    shippingRuleTypeID = cartItem.Product.ShippingRuleTypeID.GetValueOrDefault(-1);
                    quantity = cartItem.Quantity;

                    if (shippingRuleTypeID == (int)ZNodeShippingRuleType.RateBasedonQUANTITY && (!freeShippingInd))
                    {
                        totalItemQuantity += quantity;
                    }

                    if (totalItemQuantity > 0)
                    {
                        isRuleApplied = this.ApplyRule(filteredShippingRules, totalItemQuantity, out itemShippingCost);

                        if (isRuleApplied && cartItem.Product.ShipSeparately)
                        {
                            cartItem.Product.ShippingCost = itemShippingCost + ShippingProperty.HandlingCharge;
                        }
                        else if (isRuleApplied)
                        {
                            cartItem.Product.ShippingCost += itemShippingCost;
                        }
                    }

                    isRuleApplied = true;
                    bool applyHandlingCharge = false;

                    // Calculate add-Ons Shipping calculation
                    foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            freeShippingInd = AddOnValue.FreeShippingInd;
                            shippingRuleTypeID = AddOnValue.ShippingRuleTypeID.GetValueOrDefault(-1);

                            if (shippingRuleTypeID == 1 && !freeShippingInd)
                            {
                                addOnItemQuantity = quantity;

                                isRuleApplied &= this.ApplyRule(filteredShippingRules, addOnItemQuantity, out itemShippingCost);

                                applyHandlingCharge |= isRuleApplied;

                                if (isRuleApplied)
                                {
                                    AddOnValue.ShippingCost = itemShippingCost;                                    
                                }
                            }
                        }
                    }

                    if (applyHandlingCharge && cartItem.Product.ShipSeparately)
                    {
                        // Set Handling charge
                        ShoppingCart.OrderLevelShipping += ShippingProperty.HandlingCharge;
                    }
                }
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Apply the rule or not
        /// </summary>
        /// <param name="FilteredShippingRules">Filtered Shipping Rules</param>
        /// <param name="ItemQuantity">Item Quantity</param>
        /// <param name="ItemShippingCost">Item Shipping Cost</param>
        /// <returns>Return true if rule is valid else false</returns>
        private bool ApplyRule(TList<ShippingRule> FilteredShippingRules, int ItemQuantity, out decimal ItemShippingCost)
        {
            bool isRuleApplied = false;
            ItemShippingCost = 0;

            foreach (ShippingRule rule in FilteredShippingRules)
            {
                if (ItemQuantity >= rule.LowerLimit && ItemQuantity <= rule.UpperLimit)
                {
                    ItemShippingCost += rule.BaseCost + (rule.PerItemCost * ItemQuantity);
                    isRuleApplied = true;
                }
            }

            return isRuleApplied;
        }
        #endregion
    }
}

﻿namespace ZNode.Libraries.ECommerce.Shipping
{
	public interface IZNodeShippingRule
    {
        string ClassName { get; }

        string Description { get; }

        string Name { get; }

        void Calculate();

        void Bind(ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCart ShoppingCart, ZNodeShippingProperty ShippingProperty);        

        void PostSubmitOrderProcess();

        bool PreSubmitOrderProcess();
    }
}

using System;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Calculates the size of a package from a list of products. 
    /// </summary>    
    public partial class ZNodeShippingPackage : ZNodeBusinessBase
    {
        #region Private member variables
        // The calculated dimentions, value, and weight.
        private decimal _Height;
        private decimal _Width;
        private decimal _Length;
        private decimal _Weight;
        private decimal _Value;
        private bool _RoundweightToNextIncrement = false;
        private bool _UseGrossValue = true;
        private ZNodeShoppingCartItem _ShoppingCartItem = new ZNodeShoppingCartItem();
        private TList<AddOnValue> _Addons = new TList<AddOnValue>();
        private TList<Product> _Products = new TList<Product>();
        private ZNodeAddOnValueEntity _AddOnValue = new ZNodeAddOnValueEntity();
        private ZNodeGenericCollection<ZNodeShoppingCartItem> _ShoppingCartItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeShippingPackage class.
        /// </summary>
        /// <param name="ShoppingCartItem">Shopping Cart Item</param>
        public ZNodeShippingPackage(ZNodeShoppingCartItem ShoppingCartItem)
        {
            this._ShoppingCartItem = ShoppingCartItem;
            this.GetItemProperties();
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeShippingPackage class.
        /// </summary>
        /// <param name="AddOnValue">Add on Value</param>
        /// <param name="ShoppingCartQuantity">Shopping Cart Quantity</param>
        public ZNodeShippingPackage(ZNodeAddOnValueEntity AddOnValue, int ShoppingCartQuantity)
        {
            this._AddOnValue = AddOnValue;

            this.GetAddOnProperties(ShoppingCartQuantity);
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeShippingPackage class.
        /// </summary>
        /// <param name="ShoppingCartItems">Shopping Cart Items</param>
        public ZNodeShippingPackage(ZNodeGenericCollection<ZNodeShoppingCartItem> ShoppingCartItems)
            : this(ShoppingCartItems, false, false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeShippingPackage class.
        /// </summary>
        /// <param name="ShoppingCartItems">Shopping Cart Items</param>
        /// <param name="RoundWeightToNextIncrement">Round Weight To Next Increment</param>
        public ZNodeShippingPackage(ZNodeGenericCollection<ZNodeShoppingCartItem> ShoppingCartItems, bool RoundWeightToNextIncrement)
            : this(ShoppingCartItems, RoundWeightToNextIncrement, false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeShippingPackage class.
        /// </summary>
        /// <param name="ShoppingCartItems">Shopping Cart Items</param>
        /// <param name="RoundWeightToNextIncrement">Set true to round the weight up to the next pound (or kg).Otherwise set false to return the true weight</param>
        /// <param name="UseGrossValue">Use Gross Value</param>
        public ZNodeShippingPackage(ZNodeGenericCollection<ZNodeShoppingCartItem> ShoppingCartItems, bool RoundWeightToNextIncrement, bool UseGrossValue)
        {
            this._ShoppingCartItems = ShoppingCartItems;
            this._RoundweightToNextIncrement = RoundWeightToNextIncrement;
            this._UseGrossValue = UseGrossValue;

            this.GetPackageProperties();
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeShippingPackage class.
        /// </summary>
        /// <param name="Products">Product details</param>
        /// <param name="Addons">Add ons for the product</param>
        public ZNodeShippingPackage(TList<Product> Products, TList<AddOnValue> Addons)
        {
            this._Addons = Addons;
            this._Products = Products;
            this._RoundweightToNextIncrement = true;
            this._UseGrossValue = false;
            this.EstimateShipmentPackage();
        }

        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the calculated height of the package
        /// </summary>
        public decimal Height
        {
            get { return this._Height; }
            set { this._Height = value; }
        }

        /// <summary>
        /// Gets or sets the calculated width of the package
        /// </summary>
        public decimal Width
        {
            get { return this._Width; }
            set { this._Width = value; }
        }

        /// <summary>
        /// Gets or sets the calculated length of the package
        /// </summary>
        public decimal Length
        {
            get { return this._Length; }
            set { this._Length = value; }
        }

        /// <summary>
        /// Gets or sets the calculated weight of the package
        /// </summary>
        public decimal Weight
        {
            get { return this._Weight; }
            set { this._Weight = value; }
        }

        /// <summary>
        /// Gets or sets the calculated value of the package
        /// </summary>
        public decimal Value
        {
            get { return this._Value; }
            set { this._Value = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether use gross value
        /// </summary>
        protected bool UseGrossValue
        {
            get { return this._UseGrossValue; }
            set { this._UseGrossValue = value; }
        }

        /// <summary>
        /// Gets or sets the Shopping Cart Item
        /// </summary>
        protected ZNodeShoppingCartItem ShoppingCartItem
        {
            get { return this._ShoppingCartItem; }
            set { this._ShoppingCartItem = value; }
        }

        /// <summary>
        /// Gets or sets the Add ons
        /// </summary>
        protected TList<AddOnValue> Addons
        {
            get { return this._Addons; }
            set { this._Addons = value; }
        }

        /// <summary>
        /// Gets or sets the products
        /// </summary>
        protected TList<Product> Products
        {
            get { return this._Products; }
            set { this._Products = value; }
        }

        /// <summary>
        /// Gets or sets the Add on value
        /// </summary>
        protected ZNodeAddOnValueEntity AddOnValue
        {
            get { return this._AddOnValue; }
            set { this._AddOnValue = value; }
        }

        /// <summary>
        /// Gets or sets the Shopping Cart Items
        /// </summary>
        protected ZNodeGenericCollection<ZNodeShoppingCartItem> ShoppingCartItems
        {
            get { return this._ShoppingCartItems; }
            set { this._ShoppingCartItems = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether Round to Weight To Next Increment
        /// </summary>
        protected bool RoundweightToNextIncrement
        {
            get { return this._RoundweightToNextIncrement; }
            set { this._RoundweightToNextIncrement = value; }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Estimates the a package size.
        /// </summary>
        /// <param name="Weight">Weight of the package</param>
        /// <param name="Height">Height of the package</param>
        /// <param name="Width">Width of the package</param>
        /// <param name="Length">Length of the package</param>
        /// <param name="Price">Price of the package</param>
        /// <param name="quantity">Quantity of the package</param>
        /// <param name="totalVolume">Total Volume</param>
        /// <param name="totalWeight">Total Weight</param>
        /// <param name="totalValue">Total Value</param>
        /// <param name="_itemWeight">Item Weight</param>
        /// <param name="FreeShippingInd">Free Shipping Ind</param>
        private void EstimatePackageDemensions(decimal Weight, decimal Height, decimal Width, decimal Length, decimal Price, ref int quantity, ref decimal totalVolume, ref decimal totalWeight, ref decimal totalValue, decimal _itemWeight, bool FreeShippingInd)
        {
            if (!FreeShippingInd)
            {
                // Get dimensions
                totalVolume += (Height * Width * Length) * quantity;

                // check this property to round the weight up to the next pound (or kg). 
                if (this._RoundweightToNextIncrement)
                {
                    // Round to the next highest pound.
                    _itemWeight += Math.Round(Weight + 0.51m, 0);
                }
                else
                {
                    // get product weight
                    _itemWeight += Weight;
                }

                if (this._UseGrossValue)
                {
                    // get product value
                    totalValue += Price * quantity;

                    // get product weight
                    _itemWeight *= quantity;
                }
                else
                {
                    totalValue += Price;
                }

                totalWeight += _itemWeight;
            }
        }

        /// <summary>
        /// Estimates the total package size and weight from a list of Products and their Add-Ons.
        /// </summary>
        private void EstimateShipmentPackage()
        {
            // We are going to get a very aproximate package dimension by taking the total volume of all items in the cart
            // and then getting the cube root of that volume. This will give us a minimum package size.
            // NOTE: This will under estimate the total package volume since packages will rarely be cubes!
            decimal totalVolume = 0;
            decimal totalWeight = 0;
            decimal totalValue = 0;

            // Get each products dimensions,weight            
            foreach (Product p in this._Products)
            {
                decimal _itemWeight = 0;

                // Quantity is set to 1 because we're sending in a product row for every single item in the order.
                int q = 1;

                this.EstimatePackageDemensions(p.Weight.Value, p.Height.Value, p.Width.Value, p.Length.Value, p.RetailPrice.Value, ref q, ref totalVolume, ref totalWeight, ref totalValue, _itemWeight, false);
            }

            foreach (AddOnValue a in this._Addons)
            {
                decimal _itemWeight = 0;

                if (!a.Length.HasValue)
                {
                    a.Length = 0M;
                }

                if (!a.Width.HasValue)
                {
                    a.Width = 0M;
                }

                if (!a.Height.HasValue)
                {
                    a.Height = 0M;
                }

                // Quantity is set to 1 because we're sending in a product row for every single item in the order.
                int q = 1;
                this.EstimatePackageDemensions(a.Weight, a.Height.Value, a.Width.Value, a.Length.Value, a.RetailPrice, ref q, ref totalVolume, ref totalWeight, ref totalValue, _itemWeight, false);
            }

            // Let's aproximate dimentions by taking the qube root of the total volume.
            decimal dimension = (decimal)Math.Pow(Convert.ToDouble(totalVolume), 1.0 / 3.0);
            dimension = Math.Round(dimension);
            this._Height = dimension;
            this._Length = dimension;
            this._Width = dimension;
            this._Weight = totalWeight;
            this._Value = totalValue; // total product and add-on value
        }

        /// <summary>
        /// Sets package properties for the entire cart items or specified cart item
        /// </summary>
        private void GetItemProperties()
        {
            // Get Product total weight.
            int quantity = this._ShoppingCartItem.Quantity;

            decimal totalVolume = 0;
            decimal totalWeight = 0;
            decimal totalValue = 0;
            decimal _itemWeight = 0;

            this.EstimatePackageDemensions(this._ShoppingCartItem.Product.Weight, this._ShoppingCartItem.Product.Height, this._ShoppingCartItem.Product.Width, this._ShoppingCartItem.Product.Length, this._ShoppingCartItem.TieredPricing, ref quantity, ref totalVolume, ref totalWeight, ref totalValue, _itemWeight, this._ShoppingCartItem.Product.FreeShippingInd);

            // Let's aproximate dimentions by taking the qube root of the total volume.
            decimal dimension = (decimal)Math.Pow(Convert.ToDouble(totalVolume), 1.0 / 3.0);
            dimension = Math.Round(dimension);

            this._Height = dimension;
            this._Length = dimension;
            this._Width = dimension;
            this._Weight = totalWeight;
            this._Value = totalValue; // total product value
        }

        /// <summary>
        /// Sets package properties for the specified cart item
        /// </summary>
        /// <param name="Quantity">Quantity of the package</param>
        private void GetAddOnProperties(int Quantity)
        {
            decimal totalVolume = 0;
            decimal totalWeight = 0;
            decimal totalValue = 0;
            decimal _itemWeight = 0;

            this.EstimatePackageDemensions(this._AddOnValue.Weight, this._AddOnValue.Height, this._AddOnValue.Width, this._AddOnValue.Length, this._AddOnValue.FinalPrice, ref Quantity, ref totalVolume, ref totalWeight, ref totalValue, _itemWeight, this._AddOnValue.FreeShippingInd);

            // Let's aproximate dimentions by taking the qube root of the total volume.
            decimal dimension = (decimal)Math.Pow(Convert.ToDouble(totalVolume), 1.0 / 3.0);
            dimension = Math.Round(dimension);

            this._Height = dimension;
            this._Length = dimension;
            this._Width = dimension;
            this._Weight = totalWeight;
            this._Value = totalValue; // total product add-on value
        }

        private void GetPackageProperties()
        {
            // We are going to get a very aproximate package dimension by taking the total volume of all items in the cart
            // and then getting the cube root of that volume. This will give us a minimum package size.
            // NOTE: This will under estimate the total package volume since packages will rarely be cubes!
            decimal totalVolume = 0;
            decimal totalWeight = 0;
            decimal totalValue = 0;

            // Get each products dimensions,weight
            foreach (ZNodeShoppingCartItem cartItem in this._ShoppingCartItems)
            {
                // Get Product total weight.
                int quantity = cartItem.Quantity;
                decimal _itemWeight = 0;
                this.EstimatePackageDemensions(cartItem.Product.Weight, cartItem.Product.Height, cartItem.Product.Width, cartItem.Product.Length, cartItem.TieredPricing, ref quantity, ref totalVolume, ref totalWeight, ref totalValue, _itemWeight, cartItem.Product.FreeShippingInd);

                // Get each add-ons dimentions
                foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        this.EstimatePackageDemensions(AddOnValue.Weight, AddOnValue.Height, AddOnValue.Width, AddOnValue.Length, AddOnValue.FinalPrice, ref quantity, ref totalVolume, ref totalWeight, ref totalValue, _itemWeight, AddOnValue.FreeShippingInd);
                    }
                }
            }

            // Let's aproximate dimentions by taking the qube root of the total volume.
            decimal dimension = (decimal)Math.Pow(Convert.ToDouble(totalVolume), 1.0 / 3.0);
            dimension = Math.Round(dimension);
            this._Height = dimension;
            this._Length = dimension;
            this._Width = dimension;
            this._Weight = totalWeight;
            this._Value = totalValue; // total product and add-on value
        }
        #endregion
    }
}

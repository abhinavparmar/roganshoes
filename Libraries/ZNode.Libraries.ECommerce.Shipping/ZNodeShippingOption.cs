using System;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Class factory for Shipping Rules. Instantiates the appropriate rules needed for calculating shipping.
    /// </summary>
    public partial class ZNodeShippingOption : ZNodeBusinessBase
    {
        #region Private Member Variables
        private ZNodeShoppingCart _ShoppingCart;
        private ZNodeGenericCollection<IZNodeShippingRule> _ShippingRules = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeShippingOption class.
        /// </summary>
        public ZNodeShippingOption()
        {
            System.Diagnostics.Debug.Assert(true, "This class requires a ShoppingCart in the constructor.");
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeShippingOption class.
        /// </summary>
        /// <param name="ShoppingCart">Shopping cart object for which shipping needs to be calculated</param>        
        public ZNodeShippingOption(ZNodeShoppingCart ShoppingCart)
        {
            this._ShoppingCart = ShoppingCart;
            this._ShippingRules = new ZNodeGenericCollection<IZNodeShippingRule>();

            int shippingID = ShoppingCart.Shipping.ShippingID;

            if (shippingID > 0)
            {
                System.Reflection.Assembly shippingRuleAssebmly = System.Reflection.Assembly.GetExecutingAssembly();
                string assemblyName = shippingRuleAssebmly.GetName().Name;

                ShippingService shippingService = new ShippingService();
                ZNodeShippingOption shippingOptionType = new ZNodeShippingOption();
                ZNode.Libraries.DataAccess.Entities.Shipping shippingOption = shippingService.DeepLoadByShippingID(shippingID, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(ShippingType), typeof(TList<ShippingRule>));

                if (shippingOption != null)
                {
                    ZNodeShippingProperty shippingProperty = new ZNodeShippingProperty();
                    shippingProperty.ShoppingCart = this._ShoppingCart;
                    shippingProperty.ShippingCode = shippingOption.ShippingCode;
                    shippingProperty.HandlingCharge = shippingOption.HandlingCharge;
                    shippingProperty.ShippingRules = shippingOption.ShippingRuleCollection;

                    string className = shippingOption.ShippingTypeIDSource.ClassName;

                    // Instantiate the shipping based on class name.
                    if (!string.IsNullOrEmpty(className))
                    {
                        try
                        {
                            IZNodeShippingRule shippingRule = (IZNodeShippingRule)shippingRuleAssebmly.CreateInstance(assemblyName + "." + className);
                            shippingRule.Bind(this._ShoppingCart, shippingProperty);
                            this._ShippingRules.Add(shippingRule);
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Error while instantiating shipping: " + className);
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogObject(typeof(Exception), ex);
                        }
                    }
                }
            }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Method will update all the appropriate  shipping cost value to the ZNodeShoppingCart and ShoppingCartItem object.
        /// </summary>
        public void Calculate()
        {
            this._ShoppingCart.OrderLevelShipping = 0;
            this._ShoppingCart.Shipping.ShippingHandlingCharge = 0;
            this._ShoppingCart.Shipping.ResponseCode = "0";
            this._ShoppingCart.Shipping.ResponseMessage = string.Empty;

            foreach (ZNodeShoppingCartItem cartItem in this._ShoppingCart.ShoppingCartItems)
            {
                // Reset each line item shipping cost
                cartItem.ShippingCost = 0;
                cartItem.Product.ShippingCost = 0;

                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        addOnValue.ShippingCost = 0;
                    }
                }
            }

            foreach (ZNodeShippingRule shippingRule in this._ShippingRules)
            {
                shippingRule.Calculate();
            }
        }

        /// <summary>
        /// Process anything that must be done before the order is submitted.
        /// </summary>        
        /// <returns>True if successfull, False otherwise.</returns>
        public bool PreSubmitOrderProcess()
        {
            bool allPreConditionsOk = true;

            foreach (ZNodeShippingRule shippingRule in this._ShippingRules)
            {
                allPreConditionsOk &= shippingRule.PreSubmitOrderProcess();
            }

            return allPreConditionsOk;
        }

        /// <summary>
        /// Process anything that must be done after the order has been submitted.
        /// </summary>        
        public void PostSubmitOrderProcess()
        {
        }
        #endregion
    }
}

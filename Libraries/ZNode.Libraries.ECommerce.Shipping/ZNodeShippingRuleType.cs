﻿
namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Shipping Rule Type
    /// </summary>
    public enum ZNodeShippingRuleType
    {
        /// <summary>
        /// Represents the Flat Rate per Item
        /// </summary>
        FlatRatePerItem = 0,
        
        /// <summary>
        /// Represents the Rate based on quantity
        /// </summary>
        RateBasedonQUANTITY = 1,
        
        /// <summary>
        /// Represents the Rate based on weight
        /// </summary>
        RateBasedonWEIGHT = 2,
        
        /// <summary>
        /// Represents the Fixed Rate per Item
        /// </summary>
        FixedRatePerItem = 3,
    }
}

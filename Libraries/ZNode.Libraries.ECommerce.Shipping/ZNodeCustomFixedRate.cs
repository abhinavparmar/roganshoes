﻿using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Represents the custom fixed rate
    /// </summary>
	public class ZNodeCustomFixedRate : ZNodeBusinessBase
    {
        /// <summary>
        /// Calculate the Shipping rate
        /// </summary>
        /// <param name="ShippingProperty">Shipping Property</param>
        public void Calculate(ZNodeShippingProperty ShippingProperty)
        {
            int shippingRuleTypeID = -1;
            decimal itemShippingCost = 0;

            // Filter the rules collection based on shippingruletypeid - 3 (Fixed Rate Per Item)   
            TList<ShippingRule> fixedShippingRules = ShippingProperty.ShippingRules.FindAll(ShippingRuleColumn.ShippingRuleTypeID, (int)ZNodeShippingRuleType.FixedRatePerItem); // Fixed Rate
                        
            // Loop through each item to find custom shipping cost
            foreach (ZNodeShoppingCartItem cartItem in ShippingProperty.ShoppingCart.ShoppingCartItems)
            {
                shippingRuleTypeID = cartItem.Product.ShippingRuleTypeID.GetValueOrDefault(-1);
                itemShippingCost = 0;

                if (shippingRuleTypeID == (int)ZNodeShippingRuleType.FixedRatePerItem && !cartItem.Product.FreeShippingInd)
                {
                    if (fixedShippingRules != null && fixedShippingRules.Count > 0)
                    {
                        // foreach item, get shipping rule type
                        foreach (ShippingRule rule in fixedShippingRules)
                        {
                            itemShippingCost += rule.BaseCost;
                        }
                    }

                    itemShippingCost += cartItem.Product.ShippingRate;

                    cartItem.Product.ShippingCost = itemShippingCost;

                    if (cartItem.Product.ShipSeparately)
                    {
                        ShippingProperty.ShoppingCart.OrderLevelShipping += ShippingProperty.HandlingCharge;
                    }
                }
            }
        }
    }
}

﻿using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;


namespace ZNode.Libraries.ECommerce.Shipping
{
    public partial class ZNodeCustomFlatRate : ZNodeBusinessBase
    {
        #region Public Methods
        /// <summary>
        /// Returns total shipping cost which includes handling charge + shipping rules rate
        /// </summary>
        /// <param name="ShoppingCart">Shopping Cart</param>
        /// <param name="ShippingProperty">Shipping Property</param>
        public decimal Calculate(ZNodeShoppingCart ShoppingCart, ZNodeShippingProperty ShippingProperty, out decimal shippingCost)
        {

            // Local Member Variables
            int totalItemFlatQuantity = 0;
            int addonItemFlatQuantity = 0;
            decimal itemShippingCost = 0;
            int shippingRuleTypeID = 0;
            bool freeShippingInd = false;
            int quantity = 0;
            bool isRuleApplied = false;

            // Filter the rules collection based on shippingruletypeid    
            TList<ShippingRule> filteredShippingRules = ShippingProperty.ShippingRules.FindAll(ShippingRuleColumn.ShippingRuleTypeID, (int)ZNodeShippingRuleType.FlatRatePerItem); // Flat Rate

            if (filteredShippingRules.Count > 0)
            {
                // Loop through each item to find custom shipping cost
                foreach (ZNodeShoppingCartItem cartItem in ShippingProperty.ShoppingCart.ShoppingCartItems)
                {
                    itemShippingCost = 0;
                    totalItemFlatQuantity = 0;
                    isRuleApplied = false;

                    freeShippingInd = cartItem.Product.FreeShippingInd;
                    shippingRuleTypeID = cartItem.Product.ShippingRuleTypeID.GetValueOrDefault(-1);
                    quantity = cartItem.Quantity;

                    // flat
                    #region[ ZNode Default Code ]
                    //if (shippingRuleTypeID == (int)ZNodeShippingRuleType.FlatRatePerItem && (!freeShippingInd))
                    //{
                    //    totalItemFlatQuantity += quantity;
                    //}
                    #endregion

                    /**********************************************************************************************************************
                     * Zeon Customization ::Remove check that product belongs to free shipping or not to apply ground flat shipping rule*
                     * Check only freeShippingInd 
                     *********************************************************************************************************************/
                    if ((!freeShippingInd))
                    {
                        totalItemFlatQuantity += quantity;
                    }

                    if (totalItemFlatQuantity > 0)
                    {
                        isRuleApplied = this.ApplyRule(filteredShippingRules, totalItemFlatQuantity, out itemShippingCost);
                        #region[Znode Default Code]
                        //if (isRuleApplied && cartItem.Product.ShipSeparately)
                        //{
                        //    cartItem.Product.ShippingCost = itemShippingCost + ShippingProperty.HandlingCharge;
                        //}
                        //else if (isRuleApplied)
                        //{
                        //    cartItem.Product.ShippingCost = itemShippingCost;
                        //}
                        #endregion
                    }

                    isRuleApplied = true;
                    bool applyHandlingCharge = false;

                    // Calculate add-Ons Shipping calculation
                    foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            freeShippingInd = AddOnValue.FreeShippingInd;
                            shippingRuleTypeID = AddOnValue.ShippingRuleTypeID.GetValueOrDefault(-1);
                            itemShippingCost = 0;

                            // Flat rate
                            if (shippingRuleTypeID == 0 && !freeShippingInd)
                            {
                                addonItemFlatQuantity = quantity;

                                isRuleApplied &= this.ApplyRule(filteredShippingRules, addonItemFlatQuantity, out itemShippingCost);

                                applyHandlingCharge |= isRuleApplied;

                                if (isRuleApplied)
                                {
                                    AddOnValue.ShippingCost = itemShippingCost;
                                }
                            }
                        }
                    }

                    if (applyHandlingCharge && cartItem.Product.ShipSeparately)
                    {
                        ShoppingCart.OrderLevelShipping += ShippingProperty.HandlingCharge;
                    }
                }
            }

            return shippingCost = itemShippingCost;
        }


        #endregion


    }
}

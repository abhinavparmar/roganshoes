﻿
namespace ZNode.Libraries.ECommerce.Shipping
{
    public partial class ZNodeCustomShipping : ZNodeShippingRule
    {
        /// <summary>
        /// Zeon Custom Method to Calculate and Return Flat Rate Shipping Cost
        /// </summary>
        /// <param name="shippingCost"></param>
        public override void Calculate(out decimal shippingCost)
        {
            ZNodeCustomFlatRate flatRateShipping = new ZNodeCustomFlatRate();
            flatRateShipping.Calculate(ShoppingCart, ShippingProperty, out shippingCost);
        }
    }
}

﻿using System;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    public partial class ZNodeShippingOption : ZNodeBusinessBase
    {
        /// <summary>
        /// Method will update all the appropriate  shipping cost value to the ZNodeShoppingCart and ShoppingCartItem object.
        /// </summary>
        public void Calculate(out decimal totalFlatRateShippingCost)
        {
            this._ShoppingCart.OrderLevelShipping = 0;
            this._ShoppingCart.Shipping.ShippingHandlingCharge = 0;
            this._ShoppingCart.Shipping.ResponseCode = "0";
            this._ShoppingCart.Shipping.ResponseMessage = string.Empty;
            totalFlatRateShippingCost = 0;

            foreach (ZNodeShoppingCartItem cartItem in this._ShoppingCart.ShoppingCartItems)
            {
                // Reset each line item shipping cost
                cartItem.ShippingCost = 0;
                cartItem.Product.ShippingCost = 0;

                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        addOnValue.ShippingCost = 0;
                    }
                }
            }

            foreach (ZNodeShippingRule shippingRule in this._ShippingRules)
            {
                shippingRule.Calculate(out totalFlatRateShippingCost);
            }

            
        }
    }
}

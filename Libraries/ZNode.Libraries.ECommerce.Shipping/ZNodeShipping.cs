using System;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Property bag for settings used by the Shipping Rules.
    /// </summary>
    [Serializable()]
    public class ZNodeShipping : ZNode.Libraries.ECommerce.Entities.ZNodeShipping
    {
    }
}

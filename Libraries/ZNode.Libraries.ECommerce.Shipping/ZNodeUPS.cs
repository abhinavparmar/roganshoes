using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Represents methods for UPS shipping option
    /// </summary>
    public partial class ZNodeUPS : ZNodeShippingRule
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeUPS class.
        /// </summary>
        public ZNodeUPS()
            : base()
        {
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Returns shipping cost from Ups Service for the items in the cart
        /// </summary>
        public override void Calculate()
        {
            // Local Variables
            ZNodeEncryption encrypt = new ZNodeEncryption();
            ZNode.Libraries.Shipping.UPS shippingService = new ZNode.Libraries.Shipping.UPS();
            decimal itemShippingRate = 0;

            // UPS Service Settings
            // Decrypt UPS account info                
            shippingService.UPSUserID = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.UPSUserName);
            shippingService.UPSPassword = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.UPSPassword);
            shippingService.UPSKey = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.UPSKey);

            // Set UPS general settings
            shippingService.ShipperZipCode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
            shippingService.ShipperCountryCode = ZNodeConfigManager.SiteConfig.ShippingOriginCountryCode;
            shippingService.ShipToZipCode = ShoppingCart.Payment.ShippingAddress.PostalCode;
            shippingService.ShipToCountryCode = ShoppingCart.Payment.ShippingAddress.CountryCode;
            shippingService.PackageTypeCode = "02"; // Package 
            shippingService.UPSServiceCode = ShippingProperty.ShippingCode; // Service code
            shippingService.PickupType = "06"; // One time pickup
            shippingService.ShipToAddressType = "Commercial"; // ship to address type
            string weightUnit = ZNodeConfigManager.SiteConfig.WeightUnit;
            if (weightUnit.Length > 0)
            {
                shippingService.WeightUnit = weightUnit; // Set weight units
            }

            // Get Rate
            // Seperate Out Ship Seperately Items from Ship Together Items
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipSeperatelyItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipTogetherItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();

            foreach (ZNodeShoppingCartItem cartItem in ShippingProperty.ShoppingCart.ShoppingCartItems)
            {
                bool IsWeightExist = false;
                if (cartItem.Product.FreeShippingInd)
                {
                    foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            if (!AddOnValue.FreeShippingInd && AddOnValue.Weight > 0)
                            {
                                IsWeightExist = true;
                                break;
                            }
                        }
                    }
                }

                if (cartItem.Product.Weight > 0 && cartItem.Product.ShipSeparately)
                {
                    if (!cartItem.Product.FreeShippingInd || IsWeightExist)
                        ShipSeperatelyItems.Add(cartItem);
                }
                else if (cartItem.Product.Weight > 0 && !cartItem.Product.ShipSeparately)
                {
                    if (!cartItem.Product.FreeShippingInd || IsWeightExist)
                        ShipTogetherItems.Add(cartItem);
                }
            }

            // Shipping Estimate for Ship Together Package
            if (ShipTogetherItems.Count > 0)
            {
                ZNodeShippingPackage ShipTogetherPackage = new ZNodeShippingPackage(ShipTogetherItems, false, true);

                // Set weight                
                shippingService.PackageWeight = ShipTogetherPackage.Weight;
                shippingService.PackageWidth = ShipTogetherPackage.Width;
                shippingService.PackageLength = ShipTogetherPackage.Length;
                shippingService.PackageHeight = ShipTogetherPackage.Height;

                // Get shipping rate for each item
                itemShippingRate += shippingService.GetShippingRate();

                // Add handling charge for Ship Together Package
                itemShippingRate += ShippingProperty.HandlingCharge;

                if (shippingService.ErrorCode != "0")
                {
                    ShoppingCart.Shipping.ResponseCode = shippingService.ErrorCode;
                    ShoppingCart.AddErrorMessage = this.GenericShippingErrorMessage;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, "Shipping Error: " + shippingService.ErrorCode + " " + shippingService.ErrorDescription);
                }
            }

            // Shipping Estimate for Ship Separately packages
            if (ShipSeperatelyItems.Count > 0)
            {
                foreach (ZNodeShoppingCartItem seperateItem in ShipSeperatelyItems)
                {
                    ZNodeGenericCollection<ZNodeShoppingCartItem> singleItemlist = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
                    singleItemlist.Add(seperateItem);

                    ZNodeShippingPackage seperateItemPackage = new ZNodeShippingPackage(singleItemlist, false, false);

                    // Set weight
                    shippingService.PackageWeight = seperateItemPackage.Weight;
                    shippingService.PackageWidth = seperateItemPackage.Width;
                    shippingService.PackageLength = seperateItemPackage.Length;
                    shippingService.PackageHeight = seperateItemPackage.Height;

                    // Get shipping rate for each single item and multiple with quantity to total charges of order line item.
                    itemShippingRate += shippingService.GetShippingRate() * seperateItem.Quantity;

                    // Add handling charge for ship seperately item and multiple with quantity to total charges of order line item.
                    itemShippingRate += ShippingProperty.HandlingCharge * seperateItem.Quantity;

                    if (shippingService.ErrorCode != "0")
                    {
                        ShoppingCart.Shipping.ResponseCode = shippingService.ErrorCode;
                        ShoppingCart.AddErrorMessage = this.GenericShippingErrorMessage;

                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, string.Format("Shipping Error for {0} : {1} {2}", seperateItem.Product.Name, shippingService.ErrorCode, shippingService.ErrorDescription));
                    }
                }
            }

            ShoppingCart.OrderLevelShipping += itemShippingRate;
        }

        /// <summary>
        /// Process anything that must be done before the order is submitted.
        /// </summary>
        /// <returns>Returns true if shippign error else false</returns>
        public override bool PreSubmitOrderProcess()
        {
            if (ShoppingCart.Shipping.ResponseCode != "0")
            {
                ShoppingCart.AddErrorMessage = this.GenericShippingErrorMessage;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, "Shipping Error: " + ShoppingCart.Shipping.ResponseCode + " " + ShoppingCart.Shipping.ResponseMessage);
                return false;
            }

            return true;
        }
        #endregion
    }
}

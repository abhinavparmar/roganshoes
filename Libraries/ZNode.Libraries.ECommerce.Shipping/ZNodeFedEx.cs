using System;
using System.Web;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Represents methods for FedEx shipping rule
    /// </summary>
    public partial class ZNodeFedEx : ZNodeShippingRule
    { 
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeFedEx class.
        /// </summary>
        public ZNodeFedEx()
            : base()
        {
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Calculate the shipping.
        /// </summary>
        public override void Calculate()
        {
            // Local Variables
            string packageType = ZNodeConfigManager.SiteConfig.FedExPackagingType;

            // Decrypt FedEx account info - Client Detail,Access Key and password
            ZNodeEncryption encrypt = new ZNodeEncryption();

            ZNode.Libraries.Shipping.FedEx shippingFedEx = new ZNode.Libraries.Shipping.FedEx();
            shippingFedEx.ErrorCode ="0";

            // FedEx Service Settings
            // Set FedEx Account Info porperties
            try
            {
                shippingFedEx.FedExAccessKey = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExProductionKey);
                shippingFedEx.FedExAccountNumber = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExAccountNumber);
                shippingFedEx.FedExMeterNumber = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExMeterNumber);
                shippingFedEx.FedExSecurityCode = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExSecurityCode);
            }
            catch
            {
                // Ignore decryption issues
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("ZnodeFedEx.cs - Error decryptin FedEx keys");
            }

            // CSP Account Info
            shippingFedEx.CSPAccessKey = ZNodeConfigManager.SiteConfig.FedExCSPKey;
            shippingFedEx.CSPPassword = ZNodeConfigManager.SiteConfig.FedExCSPPassword;
            shippingFedEx.ClientProductId = ZNodeConfigManager.SiteConfig.FedExClientProductId;
            shippingFedEx.ClientProductVersion = ZNodeConfigManager.SiteConfig.FedExClientProductVersion;

            // Service type
            shippingFedEx.FedExServiceType = ShippingProperty.ShippingCode; // Service Type
            shippingFedEx.PackageTypeCode = ZNodeConfigManager.SiteConfig.FedExPackagingType; // Packaging type
            shippingFedEx.DropOffType = ZNodeConfigManager.SiteConfig.FedExDropoffType; // drop-off Type

            // Set Shipping Origin properties
            shippingFedEx.ShipperAddress1= ZNodeConfigManager.SiteConfig.ShippingOriginAddress1;
            shippingFedEx.ShipperAddress2 = ZNodeConfigManager.SiteConfig.ShippingOriginAddress2;
            shippingFedEx.ShipperCity= ZNodeConfigManager.SiteConfig.ShippingOriginCity;
            shippingFedEx.ShipperZipCode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
            shippingFedEx.ShipperStateCode = ZNodeConfigManager.SiteConfig.ShippingOriginStateCode;
            shippingFedEx.ShipperCountryCode = ZNodeConfigManager.SiteConfig.ShippingOriginCountryCode;

            // Set Destination properties
            shippingFedEx.ShipToAddress1 = ShoppingCart.Payment.ShippingAddress.Street;
            shippingFedEx.ShipToAddress2 = ShoppingCart.Payment.ShippingAddress.Street1;
            shippingFedEx.ShipToCity= ShoppingCart.Payment.ShippingAddress.City;
            shippingFedEx.ShipToZipCode = ShoppingCart.Payment.ShippingAddress.PostalCode;
            shippingFedEx.ShipToStateCode = ShoppingCart.Payment.ShippingAddress.StateCode;
            shippingFedEx.ShipToCountryCode = ShoppingCart.Payment.ShippingAddress.CountryCode;

            // Ship to residence
            if (!string.IsNullOrEmpty(ShoppingCart.Payment.ShippingAddress.CompanyName) && ShoppingCart.Payment.ShippingAddress.CompanyName.Length == 0)
            {
                shippingFedEx.ShipToAddressType = true;
            }
            else
            {
                shippingFedEx.ShipToAddressType = false;
            }

            // Apply FedExDiscount rates
            if (ZNodeConfigManager.SiteConfig.FedExUseDiscountRate.HasValue)
            {
                shippingFedEx.UseDiscountRate = ZNodeConfigManager.SiteConfig.FedExUseDiscountRate.Value;
            }

            string weightUnit = ZNodeConfigManager.SiteConfig.WeightUnit;

            if (weightUnit.Length > 0)
            {
                shippingFedEx.WeightUnit = weightUnit.TrimEnd(new char[] { 'S' });
            }

            string dimensionUnit = ZNodeConfigManager.SiteConfig.DimensionUnit;

            if (dimensionUnit.Length > 0)
            {
                shippingFedEx.DimensionUnit = dimensionUnit;
            }

            shippingFedEx.CurrencyCode = ZNodeCurrencyManager.CurrencyCode();
            bool? AddInsurance = ZNodeConfigManager.SiteConfig.FedExAddInsurance;

            // Local Variables
            decimal itemShippingRate = 0;
            decimal itemTotalValue = 0;

            // Seperate Out Ship Seperately Items from Ship Together Items
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipSeperatelyItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipTogetherItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();

            foreach (ZNodeShoppingCartItem cartItem in ShippingProperty.ShoppingCart.ShoppingCartItems)
            {
                bool IsWeightExist = false;
                if (cartItem.Product.FreeShippingInd)
                {                   
                    foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            if (!AddOnValue.FreeShippingInd && AddOnValue.Weight > 0)
                            {
                                IsWeightExist = true;
                                break;
                            }
                        }
                    }
                }

                if (cartItem.Product.Weight > 0 && cartItem.Product.ShipSeparately)
                {
                    if(!cartItem.Product.FreeShippingInd || IsWeightExist)
                    ShipSeperatelyItems.Add(cartItem);
                }
                else if ( cartItem.Product.Weight > 0 && !cartItem.Product.ShipSeparately)
                {
                    if (!cartItem.Product.FreeShippingInd || IsWeightExist)
                        ShipTogetherItems.Add(cartItem);
                }

            }

            // Shipping Estimate for Ship Together Package
            if (ShipTogetherItems.Count > 0)
            {
                ZNodeShippingPackage ShipTogetherPackage = new ZNodeShippingPackage(ShipTogetherItems);

                if (packageType!=null && packageType.ToUpper().Equals("YOUR_PACKAGING"))
                {
                    shippingFedEx.PackageLength = Convert.ToInt32(ShipTogetherPackage.Length).ToString();
                    shippingFedEx.PackageHeight = Convert.ToInt32(ShipTogetherPackage.Height).ToString();
                    shippingFedEx.PackageWidth = Convert.ToInt32(ShipTogetherPackage.Width).ToString();
                }

                // Set weight
                shippingFedEx.PackageWeight = ShipTogetherPackage.Weight;

                // Add insurance
                itemTotalValue = ShipTogetherPackage.Value;

                if (AddInsurance.GetValueOrDefault(false))
                {
                    shippingFedEx.TotalInsuredValue = itemTotalValue;
                }

                // Add customs
                shippingFedEx.TotalCustomsValue = itemTotalValue;

                // Get shipping rate for each item
                itemShippingRate += shippingFedEx.GetShippingRate();

                // Add handling charge for Ship Together Package
                itemShippingRate += ShippingProperty.HandlingCharge;
            }

            // Shipping Estimate for Ship Separately packages
            if (ShipSeperatelyItems.Count > 0)
            {
                foreach (ZNodeShoppingCartItem seperateItem in ShipSeperatelyItems)
                {
                    ZNodeGenericCollection<ZNodeShoppingCartItem> singleItemlist = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
                    singleItemlist.Add(seperateItem);

                    ZNodeShippingPackage seperateItemPackage = new ZNodeShippingPackage(singleItemlist);

                    if (packageType.ToUpper().Equals("YOUR_PACKAGING"))
                    {
                        shippingFedEx.PackageLength = Convert.ToInt32(seperateItemPackage.Length).ToString();
                        shippingFedEx.PackageHeight = Convert.ToInt32(seperateItemPackage.Height).ToString();
                        shippingFedEx.PackageWidth = Convert.ToInt32(seperateItemPackage.Width).ToString();
                    }

                    // Set weight
                    shippingFedEx.PackageWeight = seperateItemPackage.Weight;

                    // Add insurance
                    itemTotalValue = seperateItemPackage.Value;

                    if (AddInsurance.GetValueOrDefault(false))
                    {
                        shippingFedEx.TotalInsuredValue = itemTotalValue;
                    }

                    // Add customs
                    shippingFedEx.TotalCustomsValue = itemTotalValue;

                    // Get shipping rate for each item
                    itemShippingRate += shippingFedEx.GetShippingRate();

                    // Add handling charge for ship seperately item
                    itemShippingRate += ShippingProperty.HandlingCharge;
                }
            }

            if (shippingFedEx.ErrorCode == null)
            {
                ShoppingCart.Shipping.ResponseCode = "-1";
                ShoppingCart.AddErrorMessage = "Shipping Error: Invalid option selected.";
            }
            else if (!shippingFedEx.ErrorCode.Equals("0") && (ShipSeperatelyItems.Count > 0 || ShipTogetherItems.Count > 0))
              {
                ShoppingCart.Shipping.ResponseCode = shippingFedEx.ErrorCode;
                ShoppingCart.AddErrorMessage = this.GenericShippingErrorMessage; 
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, "Shipping Error: " + shippingFedEx.ErrorCode + " " + shippingFedEx.ErrorDescription);
            }
            else
            {
                ShoppingCart.OrderLevelShipping += itemShippingRate;
            }
        }

        /// <summary>
        /// Process anything that needs to be done before the order is submitted.
        /// </summary>
        /// <returns>True if the order should be submitted. False if something will prevent the order from succeeding.</returns>
        public override bool PreSubmitOrderProcess()
        {
            if (ShoppingCart.Shipping.ResponseCode != "0")
            {
                ShoppingCart.AddErrorMessage = this.GenericShippingErrorMessage;                
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, "PreSubmitOrderProcess Shipping Error: " + ShoppingCart.Shipping.ResponseCode + " " + ShoppingCart.Shipping.ResponseMessage);
                return false;
            }

            return true;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.ECommerce.Shipping
{
  public partial  class ZNodeShippingRule
    {
        /// <summary>
        /// Calculate this promotion and update the Shopping Cart with the value in the appropriate place.
        /// </summary>
        public virtual void Calculate(out decimal shippingCost)
        {
            shippingCost = 0;
        }
    }
}

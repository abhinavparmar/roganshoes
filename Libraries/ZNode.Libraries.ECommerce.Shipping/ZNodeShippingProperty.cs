using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Property bag for shipping properties.
    /// </summary>
    public class ZNodeShippingProperty
    {
        #region Private Memeber variables
        private TList<ShippingRule> _shippingRules = null;
        private ZNodeShoppingCart _Shoppingcart = null;
        private ZNodeGenericCollection<ZNodeShoppingCartItem> _shipSeparatelyItemCollection = null;
        private ZNodeGenericCollection<ZNodeShoppingCartItem> _packageItemCollection = null;
        private ZNodeShippingPackage _ShippingPackages = null;
        private decimal _HandlingCharge = 0;
        private string _ShippingCode = string.Empty;
        private decimal _BaseCost = 0;
        private bool _ApplyPackageItemHandlingCharge = false;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the shipping rule base cost
        /// </summary>
        public decimal BaseCost
        {
            get { return this._BaseCost; }
            set { this._BaseCost = value; }
        }

        /// <summary>
        /// Gets or sets the shipping handling charge
        /// </summary>
        public decimal HandlingCharge
        {
            get { return this._HandlingCharge; }
            set { this._HandlingCharge = value; }
        }

        /// <summary>
        /// Gets or sets the shipping service code
        /// </summary>
        public string ShippingCode
        {
            get { return this._ShippingCode; }
            set { this._ShippingCode = value; }
        }
       
        /// <summary>
        /// Gets or sets the current shopping cart object
        /// </summary>
        public ZNodeShoppingCart ShoppingCart
        {
            get { return this._Shoppingcart; }
            set { this._Shoppingcart = value; }
        }

        /// <summary>
        /// Gets the item collection that shipped separately
        /// </summary>
        public ZNodeGenericCollection<ZNodeShoppingCartItem> ShipSeparatelyItemCollection
        {
            get
            {
                if (this._shipSeparatelyItemCollection == null)
                {
                    this._shipSeparatelyItemCollection = new ZNodeGenericCollection<ZNodeShoppingCartItem>();

                    foreach (ZNodeShoppingCartItem item in this._Shoppingcart.ShoppingCartItems)
                    {
                        if (item.Product.ShipSeparately && !item.Product.FreeShippingInd)
                        {
                            this._shipSeparatelyItemCollection.Add(item);
                        }
                    }
                }

                return this._shipSeparatelyItemCollection;
            }
        }

        /// <summary>
        /// Gets the item collection that shipping as whole package
        /// </summary>
        public ZNodeGenericCollection<ZNodeShoppingCartItem> PackageItemCollection
        {
            get
            {
                if (this._packageItemCollection == null)
                {
                    this._packageItemCollection = new ZNodeGenericCollection<ZNodeShoppingCartItem>();

                    foreach (ZNodeShoppingCartItem item in this._Shoppingcart.ShoppingCartItems)
                    {
                        if (!item.Product.ShipSeparately && !item.Product.FreeShippingInd)
                        {
                            this._packageItemCollection.Add(item);

                            this._ApplyPackageItemHandlingCharge = true;                           
                        }

                        // Check Add-on is Free Shipping
                        foreach (ZNodeAddOnEntity AddOn in item.Product.SelectedAddOns.AddOnCollection)
                        {
                            foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                            {
                                if (!AddOnValue.FreeShippingInd)
                                {
                                    this._ApplyPackageItemHandlingCharge = true;
                                }
                            }
                        } 
                    }
                }

                return this._packageItemCollection;
            }
        }

        /// <summary>
        /// Gets or sets the shipping custom rules for this option
        /// </summary>
        public TList<ShippingRule> ShippingRules
        {
            get { return this._shippingRules; }
            set { this._shippingRules = value; }
        }

        /// <summary>
        /// Gets or sets the shipping package
        /// </summary>
        public ZNodeShippingPackage ShippingPackage
        {
            get { return this._ShippingPackages; }
            set { this._ShippingPackages = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether ApplyPackageItemHandlingCharge value
        /// </summary>
        public bool ApplyPackageItemHandlingCharge
        {
            get { return this._ApplyPackageItemHandlingCharge; }
            set { this._ApplyPackageItemHandlingCharge = value; }
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Returns the total shipping cost based on the shipping option
        /// </summary>
        /// <returns>Returns the total shipping cost</returns>
        public virtual decimal TotalShippingCost()
        {
            return 0;
        }

        /// <summary>
        /// Returns the total handling charge of the shipping
        /// </summary>
        /// <returns>Returns the total handline charge</returns>
        public decimal TotalHandlingCharge()
        {
            return 0;
        }

        /// <summary>
        /// Returns the total base of the shipping
        /// </summary>
        /// <returns>Returns the total base charge</returns>
        public decimal TotalBaseCharge()
        {
            return 0;
        }

        /// <summary>
        /// Returns the total weight of the package
        /// </summary>
        /// <returns>Returns the total package weight</returns>
        public decimal TotalPackageWeight()
        {
            if (this._ShippingPackages == null)
            {
                this._ShippingPackages = new ZNodeShippingPackage(this._Shoppingcart.ShoppingCartItems, false, true);
            }

            return this._ShippingPackages.Weight;
        }

        /// <summary>
        /// Returns the total custom value of the package
        /// </summary>
        /// <returns>Returns the total package value</returns>
        public decimal TotalPackageValue()
        {
            if (this._ShippingPackages == null)
            {
                this._ShippingPackages = new ZNodeShippingPackage(this._Shoppingcart.ShoppingCartItems);
            }

            return this._ShippingPackages.Value;
        }
        #endregion
    }
}

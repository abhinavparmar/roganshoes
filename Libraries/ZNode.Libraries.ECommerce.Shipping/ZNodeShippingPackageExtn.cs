﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Configuration;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Shipping
{
    public partial class ZNodeShippingPackage : ZNodeBusinessBase
    {
        #region[UPS Packaging Methods]

        #region[Constant]

        int maxPackageWeight = int.Parse(WebConfigurationManager.AppSettings["UPSMaxPackageWeight"].ToString());
        int packageWeightLimit = int.Parse(WebConfigurationManager.AppSettings["UPSPackageWeightLimit"].ToString());
        bool addExtraTagInReq = WebConfigurationManager.AppSettings["USPSAddExtraTag"].ToString() != null && WebConfigurationManager.AppSettings["USPSAddExtraTag"].ToString() == "1" ? true : false;
        //const int MaxPackageWeight = 150;
        //const int PackageWeightLimit = 150;

        #endregion

        #region[Public Property]

        public int TotalPackage
        {
            get;
            set;
        }

        #endregion

        /// <summary>
        /// Set Ship Seperate Package Properties
        /// </summary>
        /// <param name="ShipTogetherPackage">ZNodeShippingPackage</param>
        /// <param name="isSignatureRequired">bool</param>
        /// <returns>string</returns>
        public string SetShipSeperatePackageProperties(ZNodeShippingPackage ShipTogetherPackage, bool isSignatureRequired)
        {
            string package = string.Empty;
            if (ShipTogetherPackage.Weight > maxPackageWeight)
            {
                package = GetPackageWithWeight(ShipTogetherPackage.Weight, packageWeightLimit, ShipTogetherPackage, isSignatureRequired);
            }
            else
            {
                package = CreatePackage(ShipTogetherPackage.Width, ShipTogetherPackage.Length, ShipTogetherPackage.Height, ShipTogetherPackage.Weight, isSignatureRequired);
            }
            return package;
        }

        /// <summary>
        /// Get aproximate package dimension for Ship Together product
        /// For setting weight,height,width & length
        /// </summary>
        /// <param name="ShipTogetherPackage"></param>
        /// <param name="weight"></param>
        /// <param name="isSignatureRequired"></param>
        /// <returns></returns>
        public string SetShipTogetherPackageProperties(ZNodeShippingPackage ShipTogetherPackage, out decimal weight, bool isSignatureRequired)
        {
            decimal totalWeight = 0;
            // Get each products dimensions,weight
            foreach (ZNodeShoppingCartItem cartItem in this._ShoppingCartItems)
            {
                // Get each add-ons dimentions
                foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        totalWeight += AddOnValue.Weight;
                    }
                }

                // Get Product total weight.
                totalWeight += cartItem.Product.Weight * cartItem.Quantity;
            }
            if (this._RoundweightToNextIncrement)
            {
                // Round to the next highest pound.
                totalWeight = Math.Round(totalWeight + 0.51m, 0);
            }
            weight = totalWeight;
            if (totalWeight > maxPackageWeight)
            {
                return GetPackageWithWeight(totalWeight, packageWeightLimit, ShipTogetherPackage, isSignatureRequired);
            }
            else
            {
                TotalPackage = 1;
                return CreatePackage(ShipTogetherPackage.Width, ShipTogetherPackage.Length, ShipTogetherPackage.Height, totalWeight, isSignatureRequired);
            }
        }

        /// <summary>
        /// create the package with the specify dimension and weight
        /// </summary>
        /// <param name="totalWeigth">total product weight</param>
        /// <param name="packageWeightLimit">package weight size</param>
        /// <returns></returns>
        private string GetPackageWithWeight(decimal totalWeigth, int packageWeightLimit, ZNodeShippingPackage shippingPackage, bool isSignatureRequired)
        {
            int totalPackage = 0; int totalWeightOfPackage, packageWeight;
            StringBuilder package = new StringBuilder();
            if ((totalWeigth % packageWeightLimit) > 0)
            {
                totalPackage = (int)(totalWeigth / packageWeightLimit) + 1;
            }
            else
            {
                totalPackage = (int)totalWeigth / packageWeightLimit;
            }
            totalWeightOfPackage = Convert.ToInt32(totalWeigth);
            packageWeight = packageWeightLimit;
            TotalPackage = totalPackage;
            for (int index = 0; index < totalPackage; index++)
            {
                if ((index + 1).Equals(totalPackage))
                {
                    int createdPackageWeight = packageWeightLimit * index;
                    packageWeight = totalWeightOfPackage - createdPackageWeight;
                }
                package.AppendLine(CreatePackage(shippingPackage.Width, shippingPackage.Length, shippingPackage.Height, packageWeight, isSignatureRequired));
            }
            return package.ToString();
        }

        /// <summary>
        /// Create Package
        /// </summary>
        /// <param name="height">height of package</param>
        /// <param name="width">package width </param>
        /// <param name="length">package length</param>
        /// <param name="weight">package weight</param>
        /// <param name="package">package</param>
        private string CreatePackage(decimal width, decimal lenght, decimal height, decimal weight, bool isSignatureRequired = false)
        {
            StringBuilder package = new StringBuilder();
            package.AppendLine("<Package>");
            package.AppendLine("<PackagingType><Code>{0}</Code>");
            package.AppendLine("</PackagingType>");
            package.AppendLine("   <Dimensions>  ");
            package.AppendLine("        <UnitOfMeasurement>  ");
            package.AppendLine("            <Code>IN</Code>  ");
            package.AppendLine("        </UnitOfMeasurement>  ");
            package.AppendLine("        <Length>" + lenght + "</Length>  ");
            package.AppendLine("        <Width>" + width + "</Width>  ");
            package.AppendLine("        <Height>" + height + "</Height>  ");
            package.AppendLine("    </Dimensions>");
            package.AppendLine("<PackageWeight>");
            package.AppendLine("<UnitOfMeasurement>");
            package.AppendLine("<Code>{1} </Code>");
            package.AppendLine("</UnitOfMeasurement>");
            package.AppendLine("<Weight>" + weight + "</Weight>");
            package.AppendLine("</PackageWeight>");
            if (isSignatureRequired)
            {
                package.AppendLine(GetSignatureRequestXMLNode());
            }
            package.AppendLine("</Package>");
            return package.ToString();
        }

        /// <summary>
        /// Get Signature XML
        /// </summary>
        /// <returns></returns>
        private string GetSignatureRequestXMLNode()
        {
            StringBuilder signatureXMLNode = new StringBuilder();
            signatureXMLNode.AppendLine("<PackageServiceOptions>");
            signatureXMLNode.AppendLine("     <DeliveryConfirmation>");
            signatureXMLNode.AppendLine("         <DCISType>2</DCISType>");
            signatureXMLNode.AppendLine("     </DeliveryConfirmation>");
            signatureXMLNode.AppendLine("</PackageServiceOptions>");
            return signatureXMLNode.ToString();
        }

        #endregion

        #region[USPS Packaging Methods]

        #region Constant

        //const int MaxUSPSPackageWeight = 70;
        //const int USPSPackageWeightLimit = 40;

        int maxUSPSPackageWeight = int.Parse(WebConfigurationManager.AppSettings["USPSMaxPackageWeight"].ToString());
        int uspsPackageWeightLimit = int.Parse(WebConfigurationManager.AppSettings["USPSPackageWeightLimit"].ToString());
        bool isRetriveALLResult = bool.Parse(WebConfigurationManager.AppSettings["IsRetriveALLUSPSRates"].ToString());
        int uspsDomesticPackageWeightLimit = int.Parse(WebConfigurationManager.AppSettings["USPSDomesticPackageWeightLimit"].ToString());
        #endregion

        /// <summary>
        /// Get Package
        /// </summary>
        /// <param name="totalWeigth"></param>
        /// <param name="shippingPackage"></param>
        /// <returns></returns>
        public string GetUSPSPackage(ZNodeShippingPackage shipTogetherPackage, ZNode.Libraries.Shipping.USPS shippingService, string country, decimal productPrice, bool isInternational, bool isSignatureRequired)
        {
            decimal totalWeigth = shipTogetherPackage.Weight;
            int totalWeightOfPackage, packageWeight;
            StringBuilder package = new StringBuilder();
            int totalPackage = GetTotalUSPSPackage(totalWeigth, isInternational); //GetTotalPackage(totalWeigth);
            totalWeightOfPackage = Convert.ToInt32(totalWeigth);
            packageWeight = isInternational ? uspsPackageWeightLimit : uspsDomesticPackageWeightLimit;
            int uspsPackageWtLmt = isInternational ? uspsPackageWeightLimit : uspsDomesticPackageWeightLimit;
            decimal ounceWeight = shipTogetherPackage.Weight - totalWeightOfPackage;
            for (int index = 0; index < totalPackage; index++)
            {
                if ((index + 1).Equals(totalPackage))
                {
                    int createdPackageWeight = uspsPackageWtLmt * index;
                    packageWeight = totalWeightOfPackage - createdPackageWeight;
                }
                USPSPackageDetails packageDetails = new USPSPackageDetails();
                packageDetails.SkuID = shippingService.SKU;
                packageDetails.Pounds = packageWeight.ToString();
                packageDetails.Ounces = "0";
                if (index == totalPackage - 1 && ounceWeight > 0 && ounceWeight <= 0.50m)
                {
                    packageDetails.Ounces = shippingService.WeightInOunces;
                }
                packageDetails.ProductPrice = productPrice.ToString();
                packageDetails.Country = country;
                packageDetails.Width = shippingService.Width;
                packageDetails.Height = shippingService.Height;
                packageDetails.Length = shippingService.Length;
                packageDetails.CommercialFlag = "Y";
                //Chages as per USPS Priority Mail For Canada:2015-June:Starts
                packageDetails.OriginZip = shippingService.OriginZipCode;
                packageDetails.DestinationPostalCode = shippingService.PDUZip5;
                //Chages as per USPS Priority Mail For Canada:2015-June:Ends
                if (isInternational)
                {
                    package.AppendLine(CreateUSPSInternationalPackage(packageDetails, index));
                }
                else
                {
                    package.AppendLine(CreateUSPSPackage(packageDetails, index, isSignatureRequired));
                }
            }
            return package.ToString();
        }

        /// <summary>
        /// Get Total Number of package
        /// </summary>
        /// <param name="totalWeigth"></param>
        /// <returns></returns>
        private int GetTotalPackage(decimal totalWeigth)
        {
            int totalPackage = 0;
            if ((totalWeigth % uspsPackageWeightLimit) > 0)
            {
                totalPackage = (int)(totalWeigth / uspsPackageWeightLimit) + 1;
            }
            else
            {
                totalPackage = (int)totalWeigth / uspsPackageWeightLimit;
            }
            return totalPackage;
        }

        /// <summary>
        /// Create USPS Package
        /// </summary>
        /// <param name="width"></param>
        /// <param name="length"></param>
        /// <param name="height"></param>
        /// <param name="weight"></param>
        /// <returns></returns>
        private string CreateUSPSInternationalPackage(USPSPackageDetails packageDetails, int index)
        {
            StringBuilder package = new StringBuilder();
            package.AppendFormat("<Package ID='{0}'>", index);
            package.AppendLine("    <Pounds>" + packageDetails.Pounds + "</Pounds>");
            package.AppendLine("    <Ounces>" + packageDetails.Ounces + "</Ounces>");
            package.AppendLine("    <MailType>Package</MailType>");
            package.AppendLine("    <ValueOfContents>" + packageDetails.ProductPrice + "</ValueOfContents>");
            package.AppendLine("    <Country>" + packageDetails.Country + "</Country>");
            package.AppendLine("    <Container>RECTANGULAR</Container>");
            package.AppendLine("    <Size>REGULAR</Size>");
            package.AppendLine("    <Width>" + packageDetails.Width + "</Width>");
            package.AppendLine("    <Length>" + packageDetails.Length + "</Length>");
            package.AppendLine("    <Height>" + packageDetails.Height + "</Height>");
            package.AppendLine("    <Girth>0</Girth>");
            //Chages as per USPS Priority Mail:2015-June:Starts
            package.AppendLine("    <OriginZip>" + packageDetails.OriginZip + "</OriginZip>");
            package.AppendLine("    <CommercialFlag>" + packageDetails.CommercialFlag + "</CommercialFlag>");
            if (addExtraTagInReq)
            {
                package.AppendLine("    <AcceptanceDateTime>" + System.DateTime.Now.ToString("o") + "</AcceptanceDateTime>");
                package.AppendLine("    <DestinationPostalCode>" + packageDetails.DestinationPostalCode + "</DestinationPostalCode>");
            }
            //Chages as per USPS Priority Mail :2015-June:Ends
            package.AppendLine("</Package>");
            return package.ToString();
        }

        /// <summary>
        /// Create Package
        /// USPS Priority Mail
        /// </summary>
        /// <param name="packageDetails"></param>
        /// <returns></returns>
        private string CreateUSPSPackage(USPSPackageDetails packageDetails, int index, bool isSignatureRequired)
        {
            //Changes Done on Date  : 20 Jun 2013
            //Purpose implement changes for Signature Confirmation
            return this.CreateUPSDomesticPackage(packageDetails, index, isSignatureRequired);

        }

        /// <summary>
        /// Create UPS Domestic Package
        /// </summary>
        /// <param name="packageDetails">USPSPackageDetails</param>
        /// <param name="index">int</param>
        /// <param name="isSignatureRequired">bool</param>
        /// <returns>string</returns>
        private string CreateUPSDomesticPackage(USPSPackageDetails packageDetails, int index, bool isSignatureRequired)
        {
            StringBuilder package = new StringBuilder();
            package.AppendLine(string.Format("<Package ID=\"{0}\">", index));
            if (isRetriveALLResult)
                package.AppendLine("    <Service>ALL</Service>");
            else
                package.AppendLine("    <Service>PRIORITY</Service>");
            package.AppendLine("    <ZipOrigination>{0}</ZipOrigination>");
            package.AppendLine("    <ZipDestination>{1}</ZipDestination>");
            package.AppendLine("    <Pounds>" + packageDetails.Pounds + "</Pounds>");
            package.AppendLine("    <Ounces>" + packageDetails.Ounces + "</Ounces>");
            package.AppendLine("    <Container />");
            package.AppendLine("    <Size>REGULAR</Size>");
            if (isSignatureRequired)
            {
                package.AppendLine("<SpecialServices>");
                package.AppendLine("  <SpecialService>15</SpecialService>");
                package.AppendLine("</SpecialServices>");
            }
            if (isRetriveALLResult)
            {
                package.AppendLine("<Machinable>false</Machinable>");
            }
            package.AppendLine("</Package>");

            return package.ToString();
        }

        /// <summary>
        /// Get Total Number of package
        /// </summary>
        /// <param name="totalWeigth"></param>
        /// <returns></returns>
        private int GetTotalUSPSPackage(decimal totalWeigth, bool isInternational)
        {
            int totalPackage = 0;
            int packageLimit = isInternational ? uspsPackageWeightLimit : uspsDomesticPackageWeightLimit;
            if ((totalWeigth % packageLimit) > 0)
            {
                totalPackage = (int)(totalWeigth / packageLimit) + 1;
            }
            else
            {
                totalPackage = (int)totalWeigth / packageLimit;
            }
            return totalPackage;
        }

        /// <summary>
        /// Get USPS Quantity Based Packagess
        /// </summary>
        /// <param name="shipTogetherPackage"></param>
        /// <param name="shippingService"></param>
        /// <param name="country"></param>
        /// <param name="productPrice"></param>
        /// <param name="isInternational"></param>
        /// <param name="isSignatureRequired"></param>
        /// <returns></returns>
        public string GetQuantityBasedUSPSPackage(ZNodeShippingPackage shipTogetherPackage, ZNode.Libraries.Shipping.USPS shippingService, string country, decimal productPrice, bool isInternational, bool isSignatureRequired)
        {
            StringBuilder package = new StringBuilder();
            string packageWeighStr = CreateQtyBasedPackage(shipTogetherPackage.ShoppingCartItems);
            if (packageWeighStr != null)
            {
                string[] packageArray = packageWeighStr.Split('|');
                decimal totalWeigth = shipTogetherPackage.Weight;
                int totalPackage = packageArray.Length;

                for (int index = 0; index < totalPackage; index++)
                {
                    string[] dotWeights = packageArray[index].ToString().Split('.');
                    decimal ounceWeight = Convert.ToDecimal(packageArray[index]) - Convert.ToDecimal(dotWeights[0]);

                    USPSPackageDetails packageDetails = new USPSPackageDetails();
                    packageDetails.SkuID = shippingService.SKU;
                    packageDetails.Pounds = dotWeights[0];
                    packageDetails.Ounces = "0";
                    if (ounceWeight > 0)
                    {
                        packageDetails.Ounces = (ounceWeight * 16).ToString();
                    }
                    packageDetails.ProductPrice = productPrice.ToString();
                    packageDetails.Country = country;
                    packageDetails.Width = shippingService.Width;
                    packageDetails.Height = shippingService.Height;
                    packageDetails.Length = shippingService.Length;
                    packageDetails.CommercialFlag = "Y";
                    //Chages as per USPS Priority Mail For Canada:2015-June:Starts
                    packageDetails.OriginZip = shippingService.OriginZipCode;
                    packageDetails.DestinationPostalCode = shippingService.PDUZip5;
                    //Chages as per USPS Priority Mail For Canada:2015-June:Ends
                    if (isInternational)
                    {
                        //Chages as per USPS Priority Mail :2015-June:Starts
                        packageDetails.CommercialFlag = "N";
                        //Chages as per USPS Priority Mail :2015-June:Ends
                        package.AppendLine(CreateUSPSInternationalPackage(packageDetails, index));
                    }
                    else
                    {
                        package.AppendLine(CreateUSPSPackage(packageDetails, index, isSignatureRequired));
                    }
                }
            }
            return package.ToString();
        }

        /// <summary>
        /// Create Quantity Based Package
        /// </summary>
        /// <param name="carts">ZNodeShoppingCart</param>
        /// <returns>string</returns>
        private string CreateQtyBasedPackage(ZNodeGenericCollection<ZNodeShoppingCartItem> ShoppingCartItems)
        {
            string pckWgt = string.Empty;
            try
            {
                int qty = 0;
                decimal weight = 0;
                int cnt = 0;
                int maxQuantity = 12;
                int.TryParse(WebConfigurationManager.AppSettings["USPSPackageQTY"].ToString(), out maxQuantity);
                foreach (ZNodeShoppingCartItem cartItem in ShoppingCartItems)
                {
                    cnt++;
                    if ((qty + cartItem.Quantity) == maxQuantity)
                    {
                        qty = 0;
                        weight = weight + (cartItem.Product.Weight * cartItem.Quantity);
                        pckWgt = pckWgt + weight + "|";
                        weight = 0;
                    }
                    else if ((qty + cartItem.Quantity) > maxQuantity)
                    {
                        int remqty = 0;
                        if (cartItem.Quantity > maxQuantity)
                        {
                            int paktimes = (cartItem.Quantity / maxQuantity);
                            if (paktimes > 0)
                            {
                                for (int pkcnt = 1; pkcnt <= paktimes; pkcnt++)
                                {
                                    weight = 0;
                                    weight = weight + (cartItem.Product.Weight * maxQuantity);
                                    pckWgt = pckWgt + weight + "|";
                                }
                                remqty = cartItem.Quantity - (paktimes * maxQuantity);
                            }
                        }
                        else
                        {
                            int totqty = qty + cartItem.Quantity;
                            int needqty = maxQuantity - qty;
                            weight = weight + (cartItem.Product.Weight * needqty);
                            pckWgt = pckWgt + weight + "|";

                            remqty = totqty - maxQuantity;
                        }

                        //int totqty = qty + cartItem.Quantity;
                        //remqty = totqty - maxQuantity;
                        //int addqty = 0;
                        //if (remqty > cartItem.Quantity)
                        //{
                        //    addqty = remqty - cartItem.Quantity;
                        //}
                        //else
                        //{
                        //    addqty = cartItem.Quantity - remqty;
                        //}

                        //if (remqty == maxQuantity)
                        //{
                        //    addqty = remqty;
                        //}

                        //qty = qty + addqty;
                        //weight = weight + (cartItem.Product.Weight * addqty);
                        //pckWgt = pckWgt + weight + "|";

                        weight = 0;
                        qty = 0;
                        if (remqty > 0)
                        {
                            qty = qty + remqty;
                            weight = weight + (cartItem.Product.Weight * remqty);
                        }
                        if (cnt == ShoppingCartItems.Count)
                        {
                            pckWgt = pckWgt + weight + "|";
                        }
                    }
                    else if ((qty + cartItem.Quantity) < maxQuantity)
                    {
                        qty = qty + cartItem.Quantity;
                        weight = weight + (cartItem.Product.Weight * cartItem.Quantity);

                        if (cnt == ShoppingCartItems.Count)
                        {
                            pckWgt = pckWgt + weight + "|";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(pckWgt))
                {
                    return pckWgt.TrimEnd('|').Replace("-", "");
                }
            }
            catch (Exception ex)
            {
            }
            return pckWgt;
        }

        #region[USPS Package Class]

        /// <summary>
        /// USPS Package Details Class
        /// </summary>
        public class USPSPackageDetails
        {
            public string Width { get; set; }
            public string Length { get; set; }
            public string Height { get; set; }
            public string Pounds { get; set; }
            public string Ounces { get; set; }
            public string SkuID { get; set; }
            public string ProductPrice { get; set; }
            public string Country { get; set; }
            public string CommercialFlag { get; set; }
            //Chages as per USPS Priority Mail For Canada:2015-June:Starts
            public string OriginZip { get; set; }
            public string DestinationPostalCode { get; set; }
            //Chages as per USPS Priority Mail For Canada:2015-June:Ends
        }

        #endregion
        #endregion

        #region [FedEx Packaging Methods]
        #region[Constant]
        //FedEx Implementation :Starts
        int fedExMaxPackageWeight = int.Parse(WebConfigurationManager.AppSettings["FedExMaxPackageWeight"].ToString());
        int fedExpackageWeightLimit = int.Parse(WebConfigurationManager.AppSettings["FedExPackageWeightLimit"].ToString());
        #endregion

        #region Public and Private Methods

        /// <summary>
        /// create the package with the specify dimension and weight
        /// </summary>
        /// <param name="totalWeight">total product weight</param>
        /// <param name="packageWeightLimit">package weight size</param>
        /// <returns></returns>
        public Dictionary<int, decimal> GetFedExPackageWithWeight(decimal totalWeight)
        {
            decimal totalWeightOfPackage; 
            Dictionary<int, decimal> packageCollection = new Dictionary<int, decimal>();
            if (this._RoundweightToNextIncrement)
            {
                // Round to the next highest pound.
                totalWeight = Math.Round(totalWeight + 0.51m, 0);
            }
            totalWeightOfPackage = totalWeight;
            if (totalWeight > fedExMaxPackageWeight)
            {
                packageCollection = GetFedExMultiPackageWeightList(totalWeight);
            }
            else
            {
                TotalPackage = 1;
                packageCollection.Add(0, totalWeight);
            }
            return packageCollection;
        }

        /// <summary>
        /// create the package with the specify dimension and weight
        /// </summary>
        /// <param name="totalWeigth">total product weight</param>
        /// <param name="packageWeightLimit">package weight size</param>
        /// <returns></returns>
        private Dictionary<int, decimal> GetFedExMultiPackageWeightList(decimal totalWeigth)
        {
            Dictionary<int, decimal> packageCollection = new Dictionary<int, decimal>();
            int totalPackage = 0; int totalWeightOfPackage, packageWeight;
            StringBuilder package = new StringBuilder();
            if ((totalWeigth % fedExpackageWeightLimit) > 0)
            {
                totalPackage = (int)(totalWeigth / fedExpackageWeightLimit) + 1;
            }
            else
            {
                totalPackage = (int)totalWeigth / fedExpackageWeightLimit;
            }
            totalWeightOfPackage = Convert.ToInt32(totalWeigth);
            packageWeight = fedExpackageWeightLimit;
            TotalPackage = totalPackage;
            for (int index = 0; index < totalPackage; index++)
            {
                if ((index + 1).Equals(totalPackage))
                {
                    int createdPackageWeight = fedExpackageWeightLimit * index;
                    packageWeight = totalWeightOfPackage - createdPackageWeight;
                }
                packageCollection.Add(index, packageWeight);
            }
            return packageCollection;
        }
        #endregion
        #endregion
    }



}

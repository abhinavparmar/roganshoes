
namespace ZNode.Libraries.ECommerce.Shipping
{
    /// <summary>
    /// Provides calculations for doing custom shipping rules.
    /// </summary>
    public partial class ZNodeCustomShipping : ZNodeShippingRule
    {
        #region Public Methods
        /// <summary>
        /// Calculate the shipping costs.
        /// </summary>
        public override void Calculate()
        {
            ZNodeCustomFlatRate flatRateShipping = new ZNodeCustomFlatRate();
            flatRateShipping.Calculate(ShoppingCart, ShippingProperty);

            ZNodeCustomQuantity qtyBasedShipping = new ZNodeCustomQuantity();
            qtyBasedShipping.Calculate(ShoppingCart, ShippingProperty);

            ZNodeCustomWeight weightBasedShipping = new ZNodeCustomWeight();
            weightBasedShipping.Calculate(ShoppingCart, ShippingProperty);

            ZNodeCustomFixedRate fixedRateShipping = new ZNodeCustomFixedRate();
            fixedRateShipping.Calculate(ShippingProperty);

            // apply handling charge
            if (ShippingProperty.PackageItemCollection.Count == 0 && ShippingProperty.ShipSeparatelyItemCollection.Count == 0 && ShippingProperty.ApplyPackageItemHandlingCharge)
            {
                ShoppingCart.Shipping.ShippingHandlingCharge = ShippingProperty.HandlingCharge;
            }
            else if (ShippingProperty.PackageItemCollection.Count > 0 && ShippingProperty.ApplyPackageItemHandlingCharge)
            {
                ShoppingCart.Shipping.ShippingHandlingCharge = ShippingProperty.HandlingCharge;
            }
        }

        /// <summary>
        /// Do any processing that needs to be done before the order is submitted.
        /// </summary>
        /// <returns>Returns true</returns>
        public override bool PreSubmitOrderProcess()
        {
            return true;
        }
        #endregion
    }
}

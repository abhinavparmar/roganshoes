﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Shipping;
using System.Web.Configuration;

namespace ZNode.Libraries.ECommerce.Shipping
{
    public partial class ZNodeUSPS : ZNodeShippingRule
    {
        #region Zeon Custom Code Public Property

        /// <summary>
        /// get or set CountryName
        /// </summary>
        public string CountryName { get; set; }

        #endregion

        #region Public Method

        /// <summary>
        /// Get USPS Shipping Rate
        /// </summary>
        /// <param name="shoppingCart">ZNodeShoppingCart</param>
        /// <param name="shippingAddress">ZNode.Libraries.DataAccess.Entities.Address</param>
        /// <param name="countryName">string</param>
        /// <param name="shippingList">TList<ZNode.Libraries.DataAccess.Entities.Shipping></param>
        /// <returns>List<zUSPSRate></returns>
        public List<zUSPSRate> GetUSPSShippingRate(ZNodeShoppingCart shoppingCart, ZNode.Libraries.DataAccess.Entities.Address shippingAddress, string countryName, TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList)
        {
            List<zUSPSRate> shippingRates = new List<zUSPSRate>();

            USPS usps = new USPS();
            // Local Variables
            ZNodeEncryption encrypt = new ZNodeEncryption();
            ZNode.Libraries.Shipping.USPS shippingService = new ZNode.Libraries.Shipping.USPS();
            this.CountryName = this.GetCountryName(countryName);

            // Set USPS general settings
            shippingService.ShippingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
            shippingService.OriginZipCode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
            shippingService.PDUZip5 = shippingAddress.PostalCode;

            // Seperate Out Ship Seperately Items from Ship Together Items
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipSeperatelyItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
            ZNodeGenericCollection<ZNodeShoppingCartItem> ShipTogetherItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
            decimal shipTogetherProductPrice = 0;
                        
            foreach (ZNodeShoppingCartItem cartItem in shoppingCart.ShoppingCartItems)
            {
                bool IsWeightExist = false;
                if (cartItem.Product.FreeShippingInd)
                {
                    foreach (ZNodeAddOnEntity AddOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            if (!AddOnValue.FreeShippingInd && AddOnValue.Weight > 0)
                            {
                                IsWeightExist = true;
                                break;
                            }
                        }
                    }
                }

                if (cartItem.Product.Weight > 0 && cartItem.Product.ShipSeparately)
                {
                    //if (!cartItem.Product.FreeShippingInd || IsWeightExist)
                    cartItem.Product.FreeShippingInd = false;
                    ShipSeperatelyItems.Add(cartItem);
                }
                else if (cartItem.Product.Weight > 0 && !cartItem.Product.ShipSeparately)
                {
                    //if (!cartItem.Product.FreeShippingInd || IsWeightExist)
                    cartItem.Product.FreeShippingInd = false;
                    ShipTogetherItems.Add(cartItem);

                }
            }

            // Shipping Estimate for Ship Together Package
            if (ShipTogetherItems.Count > 0)
            {
                ZNodeShippingPackage shipTogetherPackage = new ZNodeShippingPackage(ShipTogetherItems, false, true);

                shippingService.SKU = ShipTogetherItems[0].Product.SKU;

                string[] weightArray = shipTogetherPackage.Weight.ToString("N2").Split('.');
                shippingService.WeightInPounds = weightArray[0].ToString();
                decimal remainder = shipTogetherPackage.Weight - Convert.ToDecimal(weightArray[0]);
                decimal ounces = remainder * 16;
                shippingService.WeightInOunces = ounces.ToString();

                // Get shipping rate for each item
                bool isCalculated = false;
                int uspsPortalID = 0;
                int.TryParse(WebConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString(), out uspsPortalID);

                if (shoppingCart != null && shoppingCart.Payment != null && !string.IsNullOrEmpty(shoppingCart.Payment.ShippingAddress.CountryCode) && shoppingCart.Payment.ShippingAddress.CountryCode != "US")
                {
                    string package = string.Empty;
                    if (uspsPortalID == ZNodeConfigManager.SiteConfig.PortalID)
                    {
                        package = shipTogetherPackage.GetQuantityBasedUSPSPackage(shipTogetherPackage, shippingService, this.CountryName, shipTogetherProductPrice, true, false);
                    }
                    else
                    {
                        package = shipTogetherPackage.GetUSPSPackage(shipTogetherPackage, shippingService, this.CountryName, shipTogetherProductPrice, true, false);
                    }

                    //Get List of USPS Rates for International Shipping
                    shippingRates = shippingService.CalculateUSPSInternationalShippingRate(package);

                    // Add handling charge for Ship Together Package
                    shippingRates = this.GetShipTogetherItemsRateIncludingHandlingCharge(shippingRates, shippingList);
                }
                else
                {
                    string package = string.Empty;
                    if (uspsPortalID == ZNodeConfigManager.SiteConfig.PortalID)
                    {
                        package = shipTogetherPackage.GetQuantityBasedUSPSPackage(shipTogetherPackage, shippingService, this.CountryName, shipTogetherProductPrice, false, false);
                    }
                    else
                    {
                        package = shipTogetherPackage.GetUSPSPackage(shipTogetherPackage, shippingService, this.CountryName, shipTogetherProductPrice, false, false);
                    }

                    //Get List of USPS Rates for Domestic Shipping
                    shippingRates = shippingService.CalculateShippingRate(package, false);

                    // Add handling charge for Ship Together Package
                    shippingRates = this.GetShipTogetherItemsRateIncludingHandlingCharge(shippingRates, shippingList);
                }

            }

            // Shipping Estimate for Ship Separately packages
            if (ShipSeperatelyItems.Count > 0)
            {
                foreach (ZNodeShoppingCartItem seperateItem in ShipSeperatelyItems)
                {
                    ZNodeGenericCollection<ZNodeShoppingCartItem> singleItemlist = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
                    singleItemlist.Add(seperateItem);

                    ZNodeShippingPackage seperateItemPackage = new ZNodeShippingPackage(singleItemlist, false, false);

                    shippingService.SKU = seperateItem.Product.SKU;
                    string[] weightArray = seperateItemPackage.Weight.ToString("N2").Split('.');
                    shippingService.WeightInPounds = weightArray[0].ToString();
                    decimal remainder = seperateItemPackage.Weight - Convert.ToDecimal(weightArray[0]);
                    decimal ounces = remainder * 16;
                    shippingService.WeightInOunces = ounces.ToString();

                    if (shippingAddress != null && !string.IsNullOrEmpty(shippingAddress.CountryCode) && shippingAddress.CountryCode != "US")
                    {
                        string package = seperateItemPackage.GetUSPSPackage(seperateItemPackage, shippingService, this.CountryName, seperateItem.Product.ProductPrice, true, false);

                        List<zUSPSRate> separateItemShippingRates = new List<zUSPSRate>();

                        //Get List of USPS Rates for International Shipping
                        separateItemShippingRates = shippingService.CalculateUSPSInternationalShippingRate(package);

                        //Set MonetaryValue and Handling charge for each item
                        separateItemShippingRates = GetShipRateListForMultipleQuantity(separateItemShippingRates, seperateItem.Quantity, shippingList);

                        if (shippingRates != null && shippingRates.Count > 0)
                        {
                            shippingRates = MergeShippingRateList(shippingRates, separateItemShippingRates);
                        }
                        else
                        {
                            shippingRates = separateItemShippingRates;
                        }

                    }
                    else
                    {
                        string package = seperateItemPackage.GetUSPSPackage(seperateItemPackage, shippingService, this.CountryName, seperateItem.Product.ProductPrice, false, false);

                        List<zUSPSRate> separateItemShippingRates = new List<zUSPSRate>();

                        //Get List of USPS Rates for Domestic Shipping
                        separateItemShippingRates = shippingService.CalculateShippingRate(package, false);

                        //Set MonetaryValue and Handling charge for each item
                        separateItemShippingRates = GetShipRateListForMultipleQuantity(separateItemShippingRates, seperateItem.Quantity, shippingList);

                        if (shippingRates != null && shippingRates.Count > 0)
                        {
                            shippingRates = MergeShippingRateList(shippingRates, separateItemShippingRates);
                        }
                        else
                        {
                            shippingRates = separateItemShippingRates;
                        }
                    }
                }
            }

            if (shippingRates == null)
            {
                return null;
            }
            else
            {
                return shippingRates;
            }
        }


        #endregion

        #region Private Methods

        /// <summary>
        /// Get Country Name
        /// </summary>
        /// <param name="countryCode">countryCode</param>
        /// <returns>string</returns>
        private string GetCountryName(string countryCode)
        {
            CountryService countryService = new CountryService();
            Country countryEntity = countryService.GetByCode(countryCode);
            if (countryEntity != null)
            {
                return countryEntity.Name;
            }
            return string.Empty;
        }

        /// <summary>
        /// Get ShipTogether Items Rate Including Handling Charge
        /// </summary>
        /// <param name="shippingRates">List<zUSPSRate></param>
        /// <param name="shippingList">TList<ZNode.Libraries.DataAccess.Entities.Shipping></param>
        /// <returns>List<zUSPSRate></returns>
        public List<zUSPSRate> GetShipTogetherItemsRateIncludingHandlingCharge(List<zUSPSRate> shippingRates, TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList)
        {
            List<zUSPSRate> uspsRateList = new List<zUSPSRate>();
            if (shippingRates != null && shippingRates.Count > 0)
            {
                foreach (zUSPSRate zUspsRate in shippingRates)
                {
                    var shippingDetail = shippingList.OfType<ZNode.Libraries.DataAccess.Entities.Shipping>().AsQueryable().SingleOrDefault(i => i.ShippingCode.Equals(zUspsRate.ServiceName));
                    if (shippingDetail != null && shippingDetail.HandlingCharge > 0)
                    {
                        zUspsRate.MonetaryValue = zUspsRate.MonetaryValue + shippingDetail.HandlingCharge;
                    }
                }
            }
            return shippingRates;
        }

        /// <summary>
        /// Get Ship Rate List For Multiple Quantity
        /// </summary>
        /// <param name="seperateItemShipRate">List<zUSPSRate></param>
        /// <param name="quantity">int</param>
        /// <param name="shippingList">TList<ZNode.Libraries.DataAccess.Entities.Shipping></param>
        /// <returns>List<zUSPSRate></returns>
        public List<zUSPSRate> GetShipRateListForMultipleQuantity(List<zUSPSRate> seperateItemShipRate, int quantity, TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList)
        {

            foreach (zUSPSRate upsrate in seperateItemShipRate)
            {
                upsrate.MonetaryValue = upsrate.MonetaryValue * quantity;

                var shippingDetail = shippingList.OfType<ZNode.Libraries.DataAccess.Entities.Shipping>().AsQueryable().SingleOrDefault(i => i.ShippingCode.Equals(upsrate.ServiceName));
                if (shippingDetail != null && shippingDetail.HandlingCharge > 0)
                {
                    upsrate.MonetaryValue = upsrate.MonetaryValue + shippingDetail.HandlingCharge * quantity;
                }
            }

            return seperateItemShipRate;
        }

        /// <summary>
        /// Merge Shipping Rate List
        /// </summary>
        /// <param name="shippingRates">List<zUSPSRate></param>
        /// <param name="separateItemShippingRates">List<zUSPSRate></param>
        /// <returns>List<zUSPSRate></returns>
        public List<zUSPSRate> MergeShippingRateList(List<zUSPSRate> shippingRates, List<zUSPSRate> separateItemShippingRates)
        {
            for (int counter = 0; counter < separateItemShippingRates.Count; counter++)
            {
                shippingRates[counter].MonetaryValue = shippingRates[counter].MonetaryValue + separateItemShippingRates[counter].MonetaryValue;
            }
            return shippingRates;
        }

        #endregion
    }
}

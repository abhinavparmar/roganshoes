﻿using System;
using System.Runtime.Serialization;

namespace ZNode.Libraries.ECommerce.Service.Business
{
    /// <summary>
    /// Custom Exception
    /// </summary>
    [Serializable]
    [DataContract(Namespace = "http://mobile.znode.com")]
    public class ServiceException
    {
        private string _message;
        private string _errorCode;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public ServiceException()
        {
            this._message = "Unexpected error occurred.";
        }

        /// <summary>
        /// Constructor with message
        /// </summary>
        /// <param name="message">Error message to set.</param>
        public ServiceException(string message)
        {
            this._errorCode = "0";
            this._message = message;
        }

        /// <summary>
        /// Constructor with message and error code
        /// </summary>
        /// <param name="errorCode">Error code to set.</param>
        /// <param name="message">Error message to set.</param>
        public ServiceException(string errorCode, string message)
        {
            this._errorCode = errorCode;
            this._message = message;
        }

        /// <summary>
        /// Gets the error code.
        /// </summary>
        [DataMember(Name = "ErrorCode")]
        public string ErrorCode
        {
            get
            {
                return this._errorCode;
            }
            
            private set
            {
                // No Operation
            }
        }

        /// <summary>
        /// Gets the error message.
        /// </summary>        
        [DataMember(Name = "ErrorMessage")]
        public string ErrorMessage
        {
            get
            {
                return this._message;
            }
            private set
            {
                // No Operation
            }
        }
    }
}

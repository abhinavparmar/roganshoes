﻿namespace ZNode.Libraries.ECommerce.Service.Business
{
    /// <summary>
    /// Represents the MobileProductSearchParameter class.
    /// </summary>
    public class MobileProductSearchParameter
    {
        private bool _FeaturedInd;
        private int _PortalId;
        private int? _LocaleId;
        private string _Keyword = string.Empty;
        private string _ProductIds = string.Empty;

        /// <summary>
        /// Gets or sets a value indicating whether product is featured or not.
        /// </summary>
        public bool FeaturedInd
        {
            get
            {
                return this._FeaturedInd;
            }
           
            set
            {
                this._FeaturedInd = value;
            }
        }

        /// <summary>
        /// Gets or sets the Portal Id
        /// </summary>
        public int PortalId
        {
            get
            {
                return this._PortalId;
            }
            
            set
            {
                this._PortalId = value;
            }
        }

        /// <summary>
        /// Gets or sets the Locale Id
        /// </summary>
        public int? LocaleId
        {
            get
            {
                return this._LocaleId;
            }
            
            set
            {
                this._LocaleId = value;
            }
        }

        /// <summary>
        /// Gets or sets the product search keyword.
        /// </summary>
        public string Keyword
        {
            get
            {
                return this._Keyword;
            }
           
            set
            {
                this._Keyword = value;
            }
        }

        /// <summary>
        /// Gets or sets the comma seperated product Ids
        /// </summary>
        public string ProductIds
        {
            get
            {
                return this._ProductIds;
            }
            
            set
            {
                this._ProductIds = value;
            }
        }
    }
}

﻿using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.ECommerce.Service.Business
{
    /// <summary>
    /// Represents the Portal Business Class.
    /// </summary>
    public class PortalBusiness
    {
        /// <summary>
        /// Get portal settings.
        /// </summary>
        /// <returns>Returns the portal object.</returns>
        public ZNode.Libraries.ECommerce.Service.Entities.Portal GetStoreSettings()
        {
            ZNode.Libraries.DataAccess.Service.PortalService portalService = new PortalService();

            // Get top 1 portal
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portalList = portalService.GetAll();
            if (portalList.Count > 0)
            {
                return this.GetMiddlewarePortal(portalList[0]);
            }
            else
            {
                return this.GetMiddlewarePortal(null);
            }
        }

        /// <summary>
        /// Convert Data Portal Entity to Middleware Portal Entity
        /// </summary>
        /// <param name="portal">Middleware portal object</param>
        /// <returns>Returns ZNode.Libraries.ECommerce.Service.Entities.Portal object.</returns>
        private ZNode.Libraries.ECommerce.Service.Entities.Portal GetMiddlewarePortal(ZNode.Libraries.DataAccess.Entities.Portal portal)
        {
            ZNode.Libraries.ECommerce.Service.Entities.Portal middlewarePortal = new ZNode.Libraries.ECommerce.Service.Entities.Portal();

            if (portal != null)
            {
                middlewarePortal.PortalID = portal.PortalID;
                middlewarePortal.CompanyName = portal.CompanyName;
                middlewarePortal.StoreName = portal.StoreName;
                middlewarePortal.Logo = portal.LogoPath;
            }

            return middlewarePortal;
        }
    }
}

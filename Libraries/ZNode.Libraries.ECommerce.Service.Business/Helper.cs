﻿using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.ECommerce.Service.Business
{
    /// <summary>
    /// Represents the Helper class.
    /// </summary>
    public class Helper
    {
        /// <summary>
        /// Get the category and its sub category list 
        /// </summary>
        /// <param name="portalId">Portal Id to get the categories.</param>
        /// <param name="localeId">Locale Id to get the categories.</param>
        /// <param name="categoryId">Category Id to get the categories.</param>
        /// <returns>Returns mobile category details datareader.</returns>
        public SqlDataReader GetMobileCategory(int portalId, int localeId, int categoryId)
        {
            // Create and Fill the DataSet
            DataSet ds = new DataSet();
            SqlDataReader reader = null;

            // Create Instance of Connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlConnection cloneConnection = new SqlConnection(connection.ConnectionString);

                // Create Instance of Command Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_WS_GetMobileCategories", cloneConnection);

                // Mark the Command as a SPROC
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleID", localeId);
                adapter.SelectCommand.Parameters.AddWithValue("@CategoryID", categoryId);

                cloneConnection.Open();
                reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }

            return reader;
        }

        #region Mobile Demo Methods
        /// <summary>
        /// Get the product details datareader.
        /// </summary>
        /// <param name="productId">The product Id to search by.</param>
        /// <param name="portalId">Portal Id to get the product detaikls.</param>
        /// <returns>Returns a product details datareader.</returns>
        public SqlDataReader GetProductDetailByID(int productId, int portalId)
        {
            return this.GetProductDetailByID(productId, portalId, null);
        }

        /// <summary>
        /// Get the product details datareader.
        /// </summary>
        /// <param name="productId">The Product Id to search by.</param>
        /// <param name="portalId">Portal Id to get the product detaikls.</param>
        /// <param name="localeId">Product locale Id.</param>
        /// <returns>Returns a product details datareader.</returns>
        public SqlDataReader GetProductDetailByID(int productId, int? portalId, int? localeId)
        {
            SqlDataReader reader = null;
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlConnection cloneConnection = new SqlConnection(connection.ConnectionString);

                // Create Instance of Adapter Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_WS_GetProductDetailByID", cloneConnection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductID", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                cloneConnection.Open();
                reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }

            return reader;
        }

        /// <summary>
        /// Get product review rating details.
        /// </summary>
        /// <param name="productId">The product Id to search by.</param>
        /// <param name="portalId">Portal Id of the product.</param>        
        /// <returns>Returns list of product review rating details datareader.</returns>
        public SqlDataReader GetProductReviews(int productId, int portalId)
        {
            SqlDataReader reader = null;
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlConnection cloneConnection = new SqlConnection(connection.ConnectionString);

                // Create Instance of Adapter Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_WS_GetProductReviews", cloneConnection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductID", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                cloneConnection.Open();
                reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }

            return reader;
        }

        /// <summary>
        /// Search products and return product Id table.
        /// </summary>
        /// <param name="portalId">The portal Id to search for.</param>        
        /// <param name="catalogId">The Catalog id to search for.</param>
        /// <param name="keywords">Keywords to search for.</param>
        /// <param name="delimiter">Delimiter string for the keyword.</param>
        /// <param name="categoryId">Category Id to search for.</param>
        /// <param name="searchOption">Search option.</param>
        /// <param name="sku">The product Sku to search for.</param>
        /// <param name="productNum">Product number to search for.</param>        
        /// <returns>Returns a product collection as xml string.</returns>
        public string SearchProducts(int portalId, int catalogId, string keywords, string delimiter, int categoryId, int searchOption, string sku, string productNum)
        {
            // Create Instance of Connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("ZNode_GetProductsBySearch_XML", connection);

                // Mark the Command as a stored procedure.
                sqlCommand.CommandType = CommandType.StoredProcedure;

                // Add Parameters to stored procedure
                sqlCommand.Parameters.AddWithValue("@PortalID", portalId);
                sqlCommand.Parameters.AddWithValue("@CatalogId", catalogId);
                sqlCommand.Parameters.AddWithValue("@Keywords", keywords);
                sqlCommand.Parameters.AddWithValue("@Delimiter", delimiter);
                sqlCommand.Parameters.AddWithValue("@CategoryId", categoryId);
                sqlCommand.Parameters.AddWithValue("@SearchOption", searchOption);
                sqlCommand.Parameters.AddWithValue("@SKU", sku);
                sqlCommand.Parameters.AddWithValue("@ProductNum", productNum);

                // Execute the command
                connection.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCommand;

                DataTable dataTable = new DataTable();

                adapter.Fill(dataTable);

                System.Web.HttpContext.Current.Session["ProductList"] = dataTable;

                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Search and get list of products based on the keyword.
        /// </summary>
        /// <param name="param">MobileProductSearchParameter object.</param>
        /// <returns>Returns list of products matching with the keyword.</returns>
        public SqlDataReader GetProductsByKeyword(MobileProductSearchParameter param)
        {
            SqlDataReader reader = null;
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlConnection cloneConnection = new SqlConnection(connection.ConnectionString);

                // Create Instance of Adapter Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_WS_GetProductsByKeyword", cloneConnection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductIds", param.ProductIds);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", param.PortalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", param.LocaleId);

                cloneConnection.Open();

                reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return reader;
            }
        }

        /// <summary>
        /// Get the mobile products
        /// </summary>
        /// <param name="param">MobileProductSearchParameter object.</param>
        /// <returns>Returns list of products</returns>
        public SqlDataReader GetFeaturedProducts(MobileProductSearchParameter param)
        {
            SqlDataReader reader = null;
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlConnection cloneConnection = new SqlConnection(connection.ConnectionString);

                // Create Instance of Adapter Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_WS_GetFeaturedProducts", cloneConnection);
                
                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@FeatureInd", param.FeaturedInd);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", param.PortalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", param.LocaleId);

                cloneConnection.Open();
                reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }

            return reader;
        }

        /// <summary>
        /// Get the top 20 products from znodeproduct order by displayorder desc.
        /// </summary>
        /// <param name="param">MobileProductSearchParameter object.</param>
        /// <returns>Return the top 20 products from znodeproduct order by displayorder desc and return a list of Product objects properly filled out with the image links and other properties.</returns>
        public SqlDataReader GetMostPopularProducts(MobileProductSearchParameter param)
        {
            SqlDataReader reader = null;
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlConnection cloneConnection = new SqlConnection(connection.ConnectionString);

                // Create Instance of Adapter Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_WS_GetMostPopularProducts", cloneConnection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", param.PortalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", param.LocaleId);

                cloneConnection.Open();
                reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }

            return reader;
        }
        #endregion
    }
}

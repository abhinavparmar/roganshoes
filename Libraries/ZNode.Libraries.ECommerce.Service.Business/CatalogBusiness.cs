﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Service.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Service.Business
{
    /// <summary>
    /// Encapsulates the business logic for managing the product catalog.
    /// </summary>
    public class CatalogBusiness
    {
        #region Member Variables
        private static NameValueCollection domainCollection = new NameValueCollection();
        private static NameValueCollection localeCollection = new NameValueCollection();
        private Helper _HelperAccess = new Helper();
        #endregion

        #region Default Constructor
        /// <summary>
        /// Initializes a new instance of the CatalogBusiness class.
        /// </summary>
        public CatalogBusiness()
        {
            this.Init();
        }
        #endregion        

        #region Mobile Demo Methods
        /// <summary>
        /// Get the categories and its product.
        /// </summary>
        /// <param name="localeCode">Locale code of catalog.</param>
        /// <param name="categoryName">Optional. If category name is empty then all categories and its products will be loaded.</param>
        /// <returns>Returns CategoryDetail object</returns>
        public ZNode.Libraries.ECommerce.Service.Entities.CategoryDetail GetCategoryDetailByName(string localeCode, string categoryName)
        {
            if (categoryName.ToLower() == "root")
            {
                return this.GetCategoryDetailById(localeCode, 0);
            }
            else
            {
                CategoryService categoryService = new CategoryService();
                string filterExpression = CategoryColumn.Name.ToString() + "='" + categoryName + "'";
                TList<ZNode.Libraries.DataAccess.Entities.Category> categoryList = categoryService.Find(filterExpression);
                if (categoryList != null && categoryList.Count > 0)
                {
                    return this.GetCategoryDetailById(localeCode, categoryList[0].CategoryID);
                }
            }

            return new ZNode.Libraries.ECommerce.Service.Entities.CategoryDetail();
        }

        /// <summary>
        /// Get the categories and its product.
        /// </summary>
        /// <param name="localeCode">Locale code of the catalog..</param>
        /// <param name="categoryId">Category Id to get the category details.</param>
        /// <returns>Returns CategoryDetail object</returns>
        public ZNode.Libraries.ECommerce.Service.Entities.CategoryDetail GetCategoryDetailById(string localeCode, int categoryId)
        {
            Helper categoryHelper = new Helper();
            ZNode.Libraries.ECommerce.Service.Entities.CategoryDetail categoryDetail = new ZNode.Libraries.ECommerce.Service.Entities.CategoryDetail();
            ZNode.Libraries.ECommerce.Service.Entities.CategoryList categoryList = new ZNode.Libraries.ECommerce.Service.Entities.CategoryList();
            ZNode.Libraries.ECommerce.Service.Entities.ProductList productList = new ZNode.Libraries.ECommerce.Service.Entities.ProductList();

            int portalId = this.GetDomainByRequest();
            int localeId = this.GetLocaleId(localeCode);
            string xmlOut = string.Empty;

            // If no catalog is found then return an error to the caller.
            if (!this.IsPortalCatalogExist(portalId, localeId))
            {
                throw new Exception(string.Format("Catalog doesn't exist for portal Id '{0}'", portalId));
            }

            // If GetMobileCategories called without a specified category name it should return all of the root level categories inside the catalog.
            // If the category can't be found it should return an error to the caller.(#5004) 
            // If the categoryId is 0 we'll just return all the root categories, otherwise we need to check to make sure the category exists first.
            if (categoryId != 0)
            {
                if (!this.IsCategoryExist(categoryId))
                {
                    throw new Exception("Category name '" + categoryId.ToString() + "' not found.");
                }
            }

            try
            {
                SqlDataReader reader = categoryHelper.GetMobileCategory(portalId, localeId, categoryId);
                categoryList.CategoryCollection = this.ReadCategories(reader);

                if (reader.NextResult())
                {
                    productList.ProductCollection = this.ReadProducts(reader);
                }

                // Safely close the data reader.
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                // Log the exception message.
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            categoryDetail.CategoryCollection = categoryList.CategoryCollection;
            categoryDetail.ProductCollection = productList.ProductCollection;

            return categoryDetail;
        }

        /// <summary>
        /// Get featured mobile products.
        /// </summary>
        /// <param name="localeCode">Locale code of the product.</param>
        /// <returns>Returns list of (Maximum 20 items) featured products.</returns>
        public ZNode.Libraries.ECommerce.Service.Entities.EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.Product> GetProductsFeatured(string localeCode)
        {
            int localeId = 0;

            // Domain lookup breaks on localhost due to port number in request URL. This needs to be fixed.
            int portalId = this.GetDomainByRequest();
            
            // Attempt to look up Locale
            localeId = this.GetLocaleId(localeCode);

            // If no catalog is found then return an error to the caller.
            if (!this.IsPortalCatalogExist(portalId, localeId))
            {
                throw new Exception(string.Format("Catalog doesn't exist for portal Id '{0}'", portalId));
            }
            
            Helper helper = new Helper();
            ZNode.Libraries.ECommerce.Service.Entities.ProductList productList = new ZNode.Libraries.ECommerce.Service.Entities.ProductList();
            MobileProductSearchParameter param = new MobileProductSearchParameter();
            param.PortalId = portalId;
            param.LocaleId = localeId;
            param.FeaturedInd = true;

            try
            {
                SqlDataReader reader = helper.GetFeaturedProducts(param);
                productList.ProductCollection = this.ReadProducts(reader);
            }
            catch (Exception ex)
            {
                // Log the exception message.
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            return productList.ProductCollection;
        }

        /// <summary>
        /// Get the top 20 products from znodeproduct order by displayorder desc.
        /// </summary>        
        /// <param name="localeCode">Locale code of the product.</param>
        /// <returns>Return the top 20 products from znodeproduct order by displayorder desc and return a list of Product objects properly filled out with the image links and other properties.</returns>
        public ZNode.Libraries.ECommerce.Service.Entities.EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.Product> GetProductsPopular(string localeCode)
        {
            int localeId = 0;

            // Domain lookup breaks on localhost due to port number in request URL. This needs to be fixed.
            int portalId = this.GetDomainByRequest();

            // Attempt to look up Locale
            localeId = this.GetLocaleId(localeCode);

            // If no catalog is found then return an error to the caller.
            if (!this.IsPortalCatalogExist(portalId, localeId))
            {
                throw new Exception(string.Format("Catalog doesn't exist for portal Id '{0}'", portalId));
            }

            Helper helper = new Helper();
            ZNode.Libraries.ECommerce.Service.Entities.ProductList productList = new ZNode.Libraries.ECommerce.Service.Entities.ProductList();

            MobileProductSearchParameter param = new MobileProductSearchParameter();
            param.PortalId = portalId;
            param.LocaleId = localeId;

            try
            {
                SqlDataReader reader = helper.GetMostPopularProducts(param);
                productList.ProductCollection = this.ReadProducts(reader);
            }
            catch (Exception ex)
            {
                // Log the exception message.
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            return productList.ProductCollection;
        }

        /// <summary>
        /// Search and get list of products based on the keyword.
        /// </summary>
        /// <param name="localeCode">Locale code as defined in storefront. If empty then default locale en is assumed.</param>
        /// <param name="keyword">Keyword to search.</param>
        /// <returns>Returns list of products matching with the keyword.</returns>
        public ZNode.Libraries.ECommerce.Service.Entities.EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.Product> GetProductsByKeyword(string localeCode, string keyword)
        {
            int localeId = 0;

            // Domain lookup breaks on localhost due to port number in request URL. This needs to be fixed.
            int portalId = this.GetDomainByRequest();

            // Attempt to look up Locale
            localeId = this.GetLocaleId(localeCode);

            PortalCatalog portalCatalog = this.GetPortalCatalog(portalId, localeId);
            if (portalCatalog == null)
            {
                throw new Exception(string.Format("Catalog doesn't exist for portal Id '{0}'", portalId));
            }

            Helper helper = new Helper();
            ZNode.Libraries.ECommerce.Service.Entities.ProductList productList = new ZNode.Libraries.ECommerce.Service.Entities.ProductList();
            List<string> productIds = new List<string>();
            DataTable productTable = null;
            StringBuilder csvProductId = new StringBuilder();

            string[] keywords = HttpContext.Current.Server.UrlDecode(keyword).Trim().Split(new char[] { ' ' });

            foreach (string keywordToSearch in keywords)
            {
                helper.SearchProducts(portalId, portalCatalog.CatalogID, keywordToSearch, string.Empty, 0, 0, string.Empty, string.Empty);
                
                // Get the product Id for current search term
                if (System.Web.HttpContext.Current.Session["ProductList"] != null)
                {
                    productTable = (DataTable)System.Web.HttpContext.Current.Session["ProductList"];
                    if (productTable.Rows.Count > 0)
                    {
                        foreach (DataRow dr in productTable.Rows)
                        {
                            string productId = dr["ProductID"].ToString();

                            // Do not add the duplicate product Id
                            if (!productIds.Contains(productId))
                            {
                                productIds.Add(productId);
                            }
                        }
                    }
                }
            }

            // If no product found then add -1 as product Id to return empty product list.
            if (productIds.Count == 0)
            {
                csvProductId.Append("-1");
            }
            else
            {
                // Build the comma seperated product Id string.
                foreach (string productId in productIds)
                {
                    csvProductId.Append(csvProductId.Length > 0 ? "," + productId : productId);
                }
            }

            MobileProductSearchParameter param = new MobileProductSearchParameter();
            param.PortalId = portalId;
            param.LocaleId = localeId;
            param.ProductIds = csvProductId.ToString();

            try
            {
                SqlDataReader reader = helper.GetProductsByKeyword(param);
                productList.ProductCollection = this.ReadProducts(reader);
            }
            catch (Exception ex)
            {
                // Log the exception message.
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            return productList.ProductCollection;
        }

        /// <summary>
        /// Gets a product details based on product ID.
        /// </summary>
        /// <param name="ProductId">The product Id to search for.</param>
        /// <returns>The product matching the product Sku or an empty product entity if not found.</returns>
        public ZNode.Libraries.ECommerce.Service.Entities.Product GetProductById(int ProductId)
        {
            ZNode.Libraries.ECommerce.Service.Entities.Product product = null;

            // Domain lookup breaks on localhost due to port number in request URL. This needs to be fixed.
            int portalId = this.GetDomainByRequest();

            if (ProductId > 0)
            {
                Helper helper = new Helper();
                SqlDataReader reader = helper.GetProductDetailByID(ProductId, portalId);
                product = this.ReadProduct(reader);
            }

            return product;
        }

        /// <summary>
        /// Get the product reviews.
        /// </summary>
        /// <param name="productId">Product Id to get the list of product reviews.</param>
        /// <returns>Returns the ProductReviewList object.</returns>
        public ZNode.Libraries.ECommerce.Service.Entities.ProductReviewList GetProductReviewsById(int productId)
        {
            ZNode.Libraries.ECommerce.Service.Entities.ProductReviewList productReviewList = new ZNode.Libraries.ECommerce.Service.Entities.ProductReviewList();

            int portalId = this.GetDomainByRequest();

            try
            {
                Helper helper = new Helper();
                SqlDataReader reader = helper.GetProductReviews(productId, portalId);
                productReviewList.ProductReviewCollection = this.ReadProductReviews(reader);
            }
            catch (Exception ex)
            {
                // Log the exception message.
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            return productReviewList;
        }

        /// <summary>
        /// Read the product from data reader.
        /// </summary>
        /// <param name="reader">SqlDataReader object</param>
        /// <returns>Return the product.</returns>
        private ZNode.Libraries.ECommerce.Service.Entities.Product ReadProduct(SqlDataReader reader)
        {
            ZNode.Libraries.ECommerce.Service.Entities.EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.Product> productCollection = this.ReadProducts(reader);
            if (productCollection.Count > 0)
            {
                return productCollection[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Read the list of products from SqlDataReader
        /// </summary>
        /// <param name="reader">SqlDataReader object</param>
        /// <returns>Returns list of products.</returns>
        private ZNode.Libraries.ECommerce.Service.Entities.EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.Product> ReadProducts(SqlDataReader reader)
        {
            ZNode.Libraries.ECommerce.Service.Entities.EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.Product> productCollection = new EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.Product>();
            ZNode.Libraries.ECommerce.Service.Entities.Product product = null;

            string buyNowBaseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    product = new ZNode.Libraries.ECommerce.Service.Entities.Product();
                    product.ProductID = Convert.ToInt32(reader[ProductColumn.ProductID.ToString()]);
                    product.Name = Convert.ToString(reader[ProductColumn.Name.ToString()]);
                    product.Description = Convert.ToString(reader[ProductColumn.Description.ToString()]);
                    product.ShortDescription = Convert.ToString(reader[ProductColumn.ShortDescription.ToString()]);
                    product.FeaturesDesc = Convert.ToString(reader[ProductColumn.FeaturesDesc.ToString()]);

                    product.DisplayOrder = Convert.ToInt32(reader[ProductColumn.DisplayOrder.ToString()]);

                    decimal retailPrice = Convert.ToDecimal(reader[ProductColumn.RetailPrice.ToString()]);
                    object salePrice = reader[ProductColumn.SalePrice.ToString()];
                    decimal displayPrice = salePrice != DBNull.Value ? Convert.ToDecimal(salePrice) : retailPrice;
                    product.DisplayPrice = displayPrice;

                    product.ImageFile = Convert.ToString(reader[ProductColumn.ImageFile.ToString()]);
                    product.ProductNum = Convert.ToString(reader[ProductColumn.ProductNum.ToString()]);
                    product.QuantityOnHand = Convert.ToInt32(reader["QuantityOnHand"]);
                    product.ReviewRating = Convert.ToInt32(reader["ReviewRating"]);
                    product.TotalReviews = Convert.ToInt32(reader["TotalReviews"]);
                    product.Sku = Convert.ToString(reader["Sku"]);
                    product.SplashImageUrlLarge = Convert.ToString(reader["SplashImageFile"]);
                    product.SplashImageUrlSmall = Convert.ToString(reader["SplashImageFile"]);
                    object splashCategoryId = reader["SplashCategoryID"];
                    product.SplashCategoryID = splashCategoryId != DBNull.Value ? Convert.ToInt32(splashCategoryId) : 0;
                    product.SplashCategoryName = Convert.ToString(reader["SplashCategoryName"]);

                    // Remove the last trailing slash
                    if (buyNowBaseUrl.EndsWith("/"))
                    {
                        int lastIndex = buyNowBaseUrl.LastIndexOf("/");
                        buyNowBaseUrl = buyNowBaseUrl.Remove(lastIndex, 1);
                    }

                    // Set BUY NOW button url
                    product.BuyButtonUrl = string.Format("{0}/addtocart.aspx?zpid={1}&cs=false", buyNowBaseUrl, product.ProductID);
                    
                    // Add the item to collection
                    productCollection.Add(product);
                }
            }
            
            // Safely close the data reader.
            if (!reader.IsClosed)
            {
                reader.Close();
            }

            return productCollection;
        }

        /// <summary>
        /// Read the category from data reader
        /// </summary>
        /// <param name="reader">SqlDataReader object</param>
        /// <returns>Return the category.</returns>
        private ZNode.Libraries.ECommerce.Service.Entities.Category ReadCategory(SqlDataReader reader)
        {
            ZNode.Libraries.ECommerce.Service.Entities.EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.Category> categoryCollection = this.ReadCategories(reader);
            if (categoryCollection.Count > 0)
            {
                return this.ReadCategories(reader)[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Read the category list from data reader.
        /// </summary>
        /// <param name="reader">SqlDataReader object.</param>
        /// <returns>Returns list of categories.</returns>
        private ZNode.Libraries.ECommerce.Service.Entities.EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.Category> ReadCategories(SqlDataReader reader)
        {
            ZNode.Libraries.ECommerce.Service.Entities.EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.Category> categoryCollection = new EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.Category>();
            ZNode.Libraries.ECommerce.Service.Entities.Category category = new ZNode.Libraries.ECommerce.Service.Entities.Category();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    category = new ZNode.Libraries.ECommerce.Service.Entities.Category();
                    category.CategoryId = Convert.ToInt32(reader["CategoryID"]);
                    category.Name = Convert.ToString(reader["Name"]);
                    category.Description = Convert.ToString(reader["Description"]);
                    category.ShortDescription = Convert.ToString(reader["ShortDescription"]);
                    category.DisplayOrder = Convert.ToInt32(reader["DisplayOrder"]);
                    category.ImageFile = Convert.ToString(reader["ImageFile"]);
                    category.SplashImageUrlLarge = Convert.ToString(reader["SplashImageFile"]);
                    category.SplashImageUrlSmall = Convert.ToString(reader["SplashImageFile"]);
                    
                    // Add the item to collection
                    categoryCollection.Add(category);
                }
            }

            return categoryCollection;
        }

        /// <summary>
        /// Read the list of product reivews from SqlDataReader
        /// </summary>
        /// <param name="reader">SqlDataReader object</param>
        /// <returns>Returns list of product reviews.</returns>
        private ZNode.Libraries.ECommerce.Service.Entities.EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.ProductReview> ReadProductReviews(SqlDataReader reader)
        {
            ZNode.Libraries.ECommerce.Service.Entities.EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.ProductReview> productReviewCollection = new EntityCollection<ZNode.Libraries.ECommerce.Service.Entities.ProductReview>();
            ZNode.Libraries.ECommerce.Service.Entities.ProductReview productReview = null;

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    productReview = new ZNode.Libraries.ECommerce.Service.Entities.ProductReview();
                    productReview.ProductID = Convert.ToInt32(reader[ProductColumn.ProductID.ToString()]);
                    productReview.ProductName = Convert.ToString(reader[ProductColumn.Name.ToString()]);
                    productReview.ReviewRating = Convert.ToInt32(reader["ReviewRating"]);
                    productReview.ReviewTitle = Convert.ToString(reader["Subject"]);
                    productReview.ReviewText = Convert.ToString(reader["Comments"]);
                    productReview.ReviewUser = Convert.ToString(reader["CreateUser"]);
                    productReview.ReviewUserLocation = Convert.ToString(reader["UserLocation"]);

                    // Add the product reivew item to the collection
                    productReviewCollection.Add(productReview);
                }
            }

            // Safely close the data reader.
            if (!reader.IsClosed)
            {
                reader.Close();
            }

            return productReviewCollection;
        }
        #endregion

        #region Helper methods

        /// <summary>
        /// Initialize required items for web services to work.
        /// </summary>
        private void Init()
        {
            // Check the license for the web services.
            ZNodeLicenseManager licenseManager;

            if (HttpContext.Current.Application["ZNODESERVICESLICENSE"] != null)
            {
                licenseManager = (ZNodeLicenseManager)HttpContext.Current.Application["ZNODESERVICESLICENSE"];
            }
            else
            {
                licenseManager = new ZNodeLicenseManager();
                licenseManager.ValidateForServices();
            }

            if (licenseManager.LicenseType == ZNodeLicenseType.Enterprise || licenseManager.LicenseType == ZNodeLicenseType.Invalid || licenseManager.LicenseType == ZNodeLicenseType.Marketplace || licenseManager.LicenseType == ZNodeLicenseType.Server)
            {
                throw new Exception("Znode Services License is not installed. Please install a license using your storefront activation page to enable these services.");
            }
        }

        /// <summary>
        /// Get the domain name from the current request.
        /// </summary>
        /// <returns>Returns the portal id if valid else throws exception.</returns>
        private int GetDomainByRequest()
        {
            if (HttpContext.Current == null)
            {
                throw new Exception("Service called from windows application.");
            }

            if (domainCollection.Count == 0)
            {
                this.PreLoadDomains();
            }

            string requestUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;
            string parsedDomainName = this.ParseDomainName(requestUrl);
            string cachedPortalId = domainCollection.Get(parsedDomainName);
            if (string.IsNullOrEmpty(cachedPortalId))
            {
                throw new Exception("Request domain name is invalid.");
            }
            else
            {
                return Convert.ToInt32(cachedPortalId);
            }
        }

        /// <summary>
        /// Load the domain names into static name value collection.
        /// </summary>
        private void PreLoadDomains()
        {
            DomainService domainService = new DomainService();
            TList<Domain> domainList = domainService.GetAll();

            // Clear the domain collection
            domainCollection.Clear();

            foreach (Domain currentItem in domainList)
            {
                domainCollection.Add(currentItem.DomainName, currentItem.PortalID.ToString());
            }
        }

        /// <summary>
        /// Parse the domain name.
        /// </summary>
        /// <param name="requestUrl">Requst Url</param>
        /// <returns>Returns the parsed domain name.</returns>
        private string ParseDomainName(string requestUrl)
        {
            string domainName = requestUrl;
            if (domainName.Contains("http://") || domainName.Contains("https://"))
            {
                domainName = domainName.Replace("http://", string.Empty).Replace("https://", string.Empty);
            }

            if (domainName.Contains(":"))
            {
                // Strip off the port number so we won't conflict with debuggers and other services that may specify a different port.
                Regex regex = new Regex(@":{1}[0-9]{1}\d*");
                domainName = regex.Replace(domainName, string.Empty);
            }

            if (domainName.StartsWith("www."))
            {
                domainName = domainName.Remove(0, 4);
            }

            // Remove any trailing "/"
            if (domainName.EndsWith("/"))
            {
                domainName = domainName.Remove(domainName.Length - 1);
            }

            return domainName;
        }

        /// <summary>
        /// Load the domain names into static name vlaue collection.
        /// </summary>
        private void PreLoadLocales()
        {
            LocaleService localeService = new LocaleService();
            TList<Locale> localeList = localeService.GetAll();

            // Clear the locale collection
            localeCollection.Clear();

            foreach (Locale currentItem in localeList)
            {
                localeCollection.Add(currentItem.LocaleCode, currentItem.LocaleID.ToString());
            }
        }

        /// <summary>
        /// Check whether the category name exist or not.
        /// </summary>
        /// <param name="categoryId">Category Id to check.</param>
        /// <returns>Returns true uf category name exist else false.</returns>
        private bool IsCategoryExist(int categoryId)
        {
            bool isExist = false;
            CategoryService categoryService = new CategoryService();

            ZNode.Libraries.DataAccess.Entities.Category categoryList = categoryService.GetByCategoryID(categoryId);
            if (categoryList != null)
            {
                isExist = true;
            }

            return isExist;
        }

        /// <summary>
        /// Check whether the portal catalog exist for the specified portal and Locale ID
        /// </summary>
        /// <param name="portalId">Portal Id to get the portal catalog.</param>
        /// <param name="localeId">Locale Id to get the portal catalog.</param>
        /// <returns>Returns true if portal catalog exist else false.</returns>
        private bool IsPortalCatalogExist(int portalId, int localeId)
        {
            bool isExist = false;
            PortalCatalogService portalCatalogService = new PortalCatalogService();
            string filterExpression = string.Format("{0}={1} AND {2}={3}", PortalCatalogColumn.PortalID.ToString().ToString(), portalId.ToString(), PortalCatalogColumn.LocaleID.ToString(), localeId.ToString());
            TList<PortalCatalog> portalCatalogList = portalCatalogService.Find(filterExpression);
            if (portalCatalogList != null && portalCatalogList.Count > 0)
            {
                isExist = true;
            }

            return isExist;
        }

        /// <summary>
        /// Get portal catalog details.
        /// </summary>
        /// <param name="portalId">Portal Id to get the portal catalog.</param>
        /// <param name="localeId">Locale Id to get the portal catalog.</param>
        /// <returns>Returns PortalCatalog object.</returns>
        private PortalCatalog GetPortalCatalog(int portalId, int localeId)
        {
            PortalCatalogService portalCatalogService = new PortalCatalogService();
            string filterExpression = string.Format("{0}={1} AND {2}={3}", PortalCatalogColumn.PortalID.ToString().ToString(), portalId.ToString(), PortalCatalogColumn.LocaleID.ToString(), localeId.ToString());
            TList<PortalCatalog> portalCatalogList = portalCatalogService.Find(filterExpression);
            if (portalCatalogList != null && portalCatalogList.Count > 0)
            {
                return portalCatalogList[0];
            }

            return null;
        }

        /// <summary>
        /// Get the LocaleId using locale code.
        /// </summary>
        /// <param name="localeCode">Locale Code</param>
        /// <returns>Returns Locale ID for the locale Code.</returns>
        private int GetLocaleId(string localeCode)
        {
            int localeId = 43;

            if (localeCode.Trim().Length == 0)
            {
                localeCode = "en";
            }

            if (localeCollection.Count == 0)
            {
                this.PreLoadLocales();
            }

            string cachedLocaleId = localeCollection.Get(localeCode);
            if (string.IsNullOrEmpty(cachedLocaleId))
            {
                throw new Exception(string.Format("Locale code '{0}' is invalid.", localeCode));
            }
            else
            {
                localeId = Convert.ToInt32(cachedLocaleId);
            }

            return localeId;
        }

        #endregion
    }
}

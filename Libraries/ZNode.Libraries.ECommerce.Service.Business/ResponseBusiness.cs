﻿using System;
using System.Xml;

namespace ZNode.Libraries.ECommerce.Service.Business
{
    /// <summary>
    /// Represents a request response.
    /// </summary>
    public class ResponseBusiness
    {
        #region Private Member Variables
        private static XmlDocument xmlDocument;
        #endregion

        #region Public Member Functions
        /// <summary>
        /// Looks up sets the response message of a Response based on the responseId.
        /// </summary>
        /// <param name="ResponseName">A name used to look up the response message.</param>
        /// <returns>A typed entity of Response</returns>
        public static ZNode.Libraries.ECommerce.Service.Entities.Response GetResponse(string ResponseName)
        {
            ZNode.Libraries.ECommerce.Service.Entities.Response response = new ZNode.Libraries.ECommerce.Service.Entities.Response();

            response.Name = ResponseName;

            // Get a message using it's key from the ErrorConfig.xml table
            try
            {
                if (xmlDocument == null)
                {
                    string configFilePath = System.Configuration.ConfigurationManager.AppSettings["ConfigPath"].ToString() + "ResponseMsgConfig.xml";

                    // Open config file
                    xmlDocument = new XmlDocument();

                    // Loads the specified XML data from a URL
                    xmlDocument.Load(configFilePath);
                }

                // Select attribute value from the Message key
                response.Message = xmlDocument.SelectSingleNode("//ResponseCodes/ResponseCode[@Name='" + response.Name + "']").Attributes["Value"].Value;
                response.ID = xmlDocument.SelectSingleNode("//ResponseCodes/ResponseCode[@Name='" + response.Name + "']").Attributes["ID"].Value;
            }
            catch (System.IO.DirectoryNotFoundException ex)
            {
                // Log the exception message.
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());

                xmlDocument = null;
            }
            catch (Exception ex)
            {
                // Log the exception message.
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());

                xmlDocument = null;
            }

            return response;
        }

        /// <summary>
        /// Looks up sets the Error response message of a Response based on the responseId
        /// </summary>
        /// <param name="responseId">Error number</param>
        /// <returns>A typed entity of Response</returns>
        public static ZNode.Libraries.ECommerce.Service.Entities.Response GetResponse(int responseId)
        {
            ZNode.Libraries.ECommerce.Service.Entities.Response response = new ZNode.Libraries.ECommerce.Service.Entities.Response();

            string MessageKey = responseId.ToString();
            response.ID = MessageKey;

            // Get a message using it's key from the ErrorConfig.xml table
            try
            {
                if (xmlDocument == null)
                {
                    string configFilePath = System.Configuration.ConfigurationManager.AppSettings["ConfigPath"].ToString() + "ResponseMsgConfig.xml";

                    // Open config file
                    xmlDocument = new XmlDocument();

                    // Loads the specified XML data from a URL
                    xmlDocument.Load(configFilePath);
                }

                // Select attribute value from the Message key
                response.Name = xmlDocument.SelectSingleNode("//ResponseCodes/ResponseCode[@ID='" + response.ID + "']").Attributes["Name"].Value;
                response.Message = xmlDocument.SelectSingleNode("//ResponseCodes/ResponseCode[@ID='" + response.ID + "']").Attributes["Value"].Value;
            }
            catch (System.IO.DirectoryNotFoundException ex)
            {
                // Log the exception message.
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());

                xmlDocument = null;
            }
            catch (Exception ex)
            {
                // Log the exception message.
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());

                xmlDocument = null;
            }

            return response;
        }

        /// <summary>
        /// Looks up sets the Error response message of a Response based on the responseId
        /// </summary>
        /// <param name="ResponseId">Error number</param>
        /// <param name="TransactionCode">Transaction code returned to be reported to user</param>
        /// <returns>A typed entity of Response</returns>
        public static ZNode.Libraries.ECommerce.Service.Entities.Response GetResponse(int ResponseId, string TransactionCode)
        {
            ZNode.Libraries.ECommerce.Service.Entities.Response returnResponse = GetResponse(ResponseId);
            returnResponse.TransactionCode = TransactionCode;

            return returnResponse;
        }

        /// <summary>
        /// Looks up sets the Error response message of a Response based on the responseId
        /// </summary>
        /// <param name="ResponseName">Error Response Name</param>
        /// <param name="TransactionCode">Transaction code returned to be reported to user</param>
        /// <returns>A typed entity of Response</returns>
        public static ZNode.Libraries.ECommerce.Service.Entities.Response GetResponse(string ResponseName, string TransactionCode)
        {
            ZNode.Libraries.ECommerce.Service.Entities.Response returnResponse = GetResponse(ResponseName);
            returnResponse.TransactionCode = TransactionCode;

            return returnResponse;
        }

        /// <summary>
        /// Looks up sets the Error response message of a Response based on the responseId
        /// </summary>
        /// <param name="ResponseId">Error number</param>
        /// <param name="TransactionCode">Transaction code returned to be reported to user</param>
        /// <param name="ErrorMessageAdditionalInfo">Additional error information to be appended to the error message</param>
        /// <returns>A typed entity of Response</returns>
        public static ZNode.Libraries.ECommerce.Service.Entities.Response GetResponse(int ResponseId, string TransactionCode, string ErrorMessageAdditionalInfo)
        {
            ZNode.Libraries.ECommerce.Service.Entities.Response returnResponse = GetResponse(ResponseId);
            returnResponse.TransactionCode = TransactionCode;
            returnResponse.Message = string.Format(returnResponse.Message, ErrorMessageAdditionalInfo);

            return returnResponse;
        }

        /// <summary>
        /// Looks up sets the Error response message of a Response based on the responseId
        /// </summary>
        /// <param name="ResponseName">Error response Name</param>
        /// <param name="TransactionCode">Transaction code returned to be reported to user</param>
        /// <param name="ErrorMessageAdditionalInfo">Additional error information to be appended to the error message</param>
        /// <returns>A typed entity of Response</returns>
        public static ZNode.Libraries.ECommerce.Service.Entities.Response GetResponse(string ResponseName, string TransactionCode, string ErrorMessageAdditionalInfo)
        {
            ZNode.Libraries.ECommerce.Service.Entities.Response returnResponse = GetResponse(ResponseName);
            returnResponse.TransactionCode = TransactionCode;
            returnResponse.Message = string.Format(returnResponse.Message, ErrorMessageAdditionalInfo);

            return returnResponse;
        }

        /// <summary>
        /// Looks up sets the Error response message of a Response based on the responseId
        /// </summary>
        /// <param name="ResponseId">Error number</param>
        /// <param name="SubSystem">The area of the program where the error occured (overrides the default message subsystem)</param>
        /// <param name="TransactionCode">Transaction code returned to be reported to user</param>
        /// <param name="ErrorMessageAdditionalInfo">Additional error information to be appended to the error message</param>
        /// <returns>A typed entity of Response</returns>
        public static ZNode.Libraries.ECommerce.Service.Entities.Response GetResponse(int ResponseId, string SubSystem, string TransactionCode, string ErrorMessageAdditionalInfo)
        {
            ZNode.Libraries.ECommerce.Service.Entities.Response returnResponse = GetResponse(ResponseId);
            returnResponse.Name = SubSystem;
            returnResponse.TransactionCode = TransactionCode;
            returnResponse.Message = string.Format(returnResponse.Message, ErrorMessageAdditionalInfo);

            return returnResponse;
        }

        #endregion
    }
}

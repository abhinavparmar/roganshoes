﻿using System.IdentityModel.Tokens;

namespace ZNode.Libraries.ECommerce.Service.Business
{
    /// <summary>
    /// Represents the MiddlewareCustomValidator class
    /// </summary>
    public class MiddlewareCustomValidator : System.IdentityModel.Selectors.UserNamePasswordValidator
    {
        /// <summary>
        /// Validate the user name and password against database.
        /// </summary>
        /// <param name="userName">Username to validate.</param>
        /// <param name="password">Password to validate.</param>
        public override void Validate(string userName, string password)
        {
            if ((userName != "znodeadmin1") || (password != "password1"))
            {
                throw new SecurityTokenException("Validation Failed!");
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Shipping
{
    /// <summary>
    /// UPS Shipping
    /// </summary>
    public partial class UPS : ZNodeBusinessBase
    {
        #region Protected Member Instance Variables
        private string _UPSGatewayURL = string.Empty;
        private string _UPSUserId;
        private string _UPSPassword;
        private string _UPSKey;
        private string _ShipperZipCode;
        private string _ShipperCountryCode;
        private string _ShipToZipCode;
        private string _ShipToCountryCode;
        private string _ShipToAddressType = "Residential";
        private decimal _PackageWeight = 0;
        private decimal _PackageHeight = 0;
        private decimal _PackageWidth = 0;
        private decimal _PackageLength = 0;
        private string _PickupType = "One Time Pickup";
        private string _weightUnit = "LBS";
        private string _UPSServiceCode;
        private string _PackageTypeCode;
        private string _ErrorCode;
        private string _ErrorDescription;        
        #endregion

        #region Contructors
        /// <summary>
        /// Initializes a new instance of the UPS class
        /// </summary>
        public UPS()
        {
            this._UPSGatewayURL = System.Configuration.ConfigurationManager.AppSettings["UPSGatewayURL"].ToString();
        }
        #endregion

        #region Public Instance Properties
        /// <summary>
        /// Gets or sets or retrieves the Merchant UPS LoginId
        /// </summary>
        public string UPSUserID
        {
            get { return this._UPSUserId; }
            set { this._UPSUserId = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Merchant UPS LoginPassword
        /// </summary>
        public string UPSPassword
        {
            get { return this._UPSPassword; }
            set { this._UPSPassword = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Merchant UPS Merchant Access Key
        /// </summary>
        public string UPSKey
        {
            get { return this._UPSKey; }
            set { this._UPSKey = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Shipper postal code
        /// </summary>
        public string ShipperZipCode
        {
            get { return this._ShipperZipCode; }
            set { this._ShipperZipCode = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Shipper Country code
        /// </summary>
        public string ShipperCountryCode
        {
            get { return this._ShipperCountryCode; }
            set { this._ShipperCountryCode = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Destination postal code
        /// </summary>
        public string ShipToZipCode
        {
            get { return this._ShipToZipCode; }
            set { this._ShipToZipCode = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Destination Country code
        /// </summary>
        public string ShipToCountryCode
        {
            get { return this._ShipToCountryCode; }
            set { this._ShipToCountryCode = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Destination Address type
        /// Default address type is 'Residential'
        /// </summary>
        public string ShipToAddressType
        {
            get { return this._ShipToAddressType; }
            set { this._ShipToAddressType = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the weight of the package
        /// </summary>
        public decimal PackageWeight
        {
            get { return this._PackageWeight; }
            set { this._PackageWeight = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Height of the package
        /// </summary>
        public decimal PackageHeight
        {
            get { return this._PackageHeight; }
            set { this._PackageHeight = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Length of the package
        /// </summary>
        public decimal PackageLength
        {
            get { return this._PackageLength; }
            set { this._PackageLength = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Width of the package
        /// </summary>
        public decimal PackageWidth
        {
            get { return this._PackageWidth; }
            set { this._PackageWidth = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Pickup type
        /// Bydefault pickup type is  'One Time Pickup'
        /// </summary>        
        public string PickupType
        {
            get { return this._PickupType; }
            set { this._PickupType = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the UPS Service code
        /// </summary>
        public string UPSServiceCode
        {
            get { return this._UPSServiceCode; }
            set { this._UPSServiceCode = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the UPS package type code
        /// </summary>
        public string PackageTypeCode
        {
            get { return this._PackageTypeCode; }
            set { this._PackageTypeCode = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Webservice response code 
        /// </summary>
        public string ErrorCode
        {
            get { return this._ErrorCode; }
            set { this._ErrorCode = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Webservice response code description
        /// </summary>
        public string ErrorDescription
        {
            get { return this._ErrorDescription; }
            set { this._ErrorDescription = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves weight unit (Lbs or Kgs)
        /// </summary>
        public string WeightUnit
        {
            get { return this._weightUnit; }
            set { this._weightUnit = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Submits Shipping details to UPS Web Service to get Rate in US dollars
        /// </summary>
        /// <returns>returns the shipping rate</returns>
        public decimal GetShippingRate()
        {
            WebClient objRequest = new WebClient();    
            byte[] objRetBytes;
            string strPayload = string.Empty;
            string addressTypeInd = "0"; // commercial

            if (this._ShipToAddressType == "Residential")
            {
                addressTypeInd = "1";
            }
                        
            // Setting Headers value
            strPayload = "<?xml version='1.0'?>";
            strPayload += "<AccessRequest xml:lang='en-US'>";
            strPayload += "<AccessLicenseNumber>" + this.UPSKey + "</AccessLicenseNumber>"; // UPS Access Key
            strPayload += "<UserId>" + this.UPSUserID + "</UserId>"; // UPS Login Account ID
            strPayload += "<Password>" + this.UPSPassword + "</Password>";
            strPayload += "</AccessRequest>";
            strPayload += "<?xml version='1.0'?>";
            strPayload += "<RatingServiceSelectionRequest xml:lang='en-US'>";
            strPayload += "<Request>";
            strPayload += "<TransactionReference>";
            strPayload += "<CustomerContext>Rating and Service</CustomerContext>";
            strPayload += "<XpciVersion>1.0001</XpciVersion>";
            strPayload += "</TransactionReference>";
            strPayload += "<RequestAction>Rate</RequestAction>";
            strPayload += "<RequestOption>Rate</RequestOption>";
            strPayload += "</Request>";
            strPayload += "<PickupType>";
            strPayload += "<Code>" + this.PickupType + "</Code>"; // Set Picktype code
            strPayload += "</PickupType>";
            strPayload += "<Shipment>";
            strPayload += "<Shipper>";
            strPayload += "<Address>";
            strPayload += "<PostalCode>" + this.ShipperZipCode + "</PostalCode>";
            strPayload += "<CountryCode>" + this.ShipperCountryCode + "</CountryCode>";
            strPayload += "</Address>";
            strPayload += "</Shipper>";
            strPayload += "<ShipTo>";
            strPayload += "<Address>";
            strPayload += "<PostalCode>" + this.ShipToZipCode + "</PostalCode>";
            strPayload += "<CountryCode>" + this.ShipToCountryCode + "</CountryCode>";
            strPayload += "<ResidentialAddress>";
            strPayload += addressTypeInd;
            strPayload += "</ResidentialAddress>";
            strPayload += "</Address>";            
            strPayload += "</ShipTo>";       
            strPayload += "<Service>";
            strPayload += "<Code>" + this.UPSServiceCode + "</Code>";
            strPayload += "</Service>";
            strPayload += "<Package>";
            strPayload += "<PackagingType><Code>" + this.PackageTypeCode + "</Code>";
            strPayload += "</PackagingType>";
            strPayload += "   <Dimensions>  ";
            strPayload += "        <UnitOfMeasurement>  ";
            strPayload += "            <Code>IN</Code>  ";
            strPayload += "        </UnitOfMeasurement>  ";
            strPayload += "        <Length>" + this.PackageLength.ToString() + "</Length>  ";
            strPayload += "        <Width>" + this.PackageWidth.ToString() + "</Width>  ";
            strPayload += "        <Height>" + this.PackageHeight.ToString() + "</Height>  ";
            strPayload += "    </Dimensions>"; 
            strPayload += "<PackageWeight>";
            strPayload += "<UnitOfMeasurement>";
            strPayload += "<Code>" + this.WeightUnit + "</Code>";
            strPayload += "</UnitOfMeasurement>";
            strPayload += "<Weight>" + this.PackageWeight.ToString() + "</Weight>";
            strPayload += "</PackageWeight>";
            strPayload += "</Package>";
            strPayload += "</Shipment>";
            strPayload += "</RatingServiceSelectionRequest>";
            
            // Convert the input string into byte object
            byte[] objRequestBytes = Encoding.ASCII.GetBytes(strPayload);                        
            objRequest.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

            try
            {
                // Post the values into UPS Web Service
                objRetBytes = objRequest.UploadData(this._UPSGatewayURL, "POST", objRequestBytes);
                string xmlResponse = System.Text.Encoding.ASCII.GetString(objRetBytes); // .Split("<*></*>".ToCharArray());
                decimal shippingRate = 0;
                System.Xml.XmlDocument xmlDocument = new System.Xml.XmlDocument();
                xmlDocument.LoadXml(xmlResponse);

                System.Xml.XmlNodeList nodeList = xmlDocument.SelectNodes("RatingServiceSelectionResponse/Response/ResponseStatusCode");

                if (nodeList.Count > 0 && nodeList[0].InnerText.Equals("1"))
                {
                    // Get Response charges
                    nodeList = xmlDocument.SelectNodes("RatingServiceSelectionResponse/RatedShipment/TotalCharges/MonetaryValue");
                    if (nodeList.Count > 0)
                    {
                        // Set ErrorCode to 0.
                        this.ErrorCode = "0";
                        shippingRate = Decimal.Parse(nodeList[0].InnerText);
                    }
                }
                else
                {
                    // Get Response Error code
                    this.ErrorCode = xmlDocument.SelectNodes("RatingServiceSelectionResponse/Response/Error/ErrorCode")[0].InnerText;

                    // Get Response Error Description
                    this.ErrorDescription = xmlDocument.SelectNodes("RatingServiceSelectionResponse/Response/Error/ErrorDescription")[0].InnerText;
                }

                return shippingRate;
            }
            catch (Exception)
            {
                this.ErrorCode = "Connection failed.";
                this.ErrorDescription = "Error while trying to connect with Host Server.Please try again."; // Response Error code Description
                return 0;
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Shipping
{
    public partial class UPS : ZNodeBusinessBase
    {
        #region[Private Methods]

        /// <summary>
        /// Create XML Request for UPS
        /// </summary>
        /// <param name="package"></param>
        /// <param name="addressTypeInd"></param>
        /// <param name="isSaturdayShippingExtis"></param>
        /// <returns></returns>
        private StringBuilder GetUPSRequestInXML(string package, string addressTypeInd, bool isSaturdayShippingExtis)
        {
            StringBuilder xmlConnect = new StringBuilder();

            xmlConnect.Append("<?xml version='1.0'?>");
            xmlConnect.Append("<AccessRequest xml:lang='en-US'>");
            xmlConnect.Append("<AccessLicenseNumber>" + this.UPSKey + "</AccessLicenseNumber>");// UPS Access Key
            xmlConnect.Append("<UserId>" + this.UPSUserID + "</UserId>");// UPS Login Account ID
            xmlConnect.Append("<Password>" + this.UPSPassword + "</Password>");
            xmlConnect.Append("</AccessRequest>");
            xmlConnect.Append("<?xml version='1.0'?>");
            xmlConnect.Append("<RatingServiceSelectionRequest xml:lang='en-US'>");
            xmlConnect.AppendLine("<Request><TransactionReference>");
            xmlConnect.AppendLine("    <CustomerContext>Rating and Service</CustomerContext><XpciVersion>1.0001</XpciVersion>" +
                                  "    </TransactionReference>" +
                                  "    <RequestAction>Rate</RequestAction><RequestOption>shop</RequestOption>" +
                                  "</Request>");
            xmlConnect.AppendLine("<PickupType><Code>" + this.PickupType + "</Code></PickupType>");// Set Picktype code
            xmlConnect.AppendLine("<Shipment>");
            //xmlConnect.AppendLine("<RateInformation>  <NegotiatedRatesIndicator/>  </RateInformation>"); //Used only when required negotiated rates
            xmlConnect.AppendLine("     <Shipper>");
            //xmlConnect.AppendLine("<ShipperNumber>" + System.Configuration.ConfigurationManager.AppSettings["UPSShipperNumber"].ToString() + "</ShipperNumber>");//Used only when required negotiated rates
            xmlConnect.AppendLine("         <Address>");
            xmlConnect.AppendLine("             <PostalCode>" + this.ShipperZipCode + "</PostalCode>");
            xmlConnect.AppendLine("             <CountryCode>" + this.ShipperCountryCode + "</CountryCode>");
            xmlConnect.AppendLine("         </Address>");
            xmlConnect.AppendLine("     </Shipper>");

            xmlConnect.AppendLine("     <ShipFrom>");
            xmlConnect.AppendLine("         <Address>");
            //xmlConnect.AppendLine("             <StateProvinceCode>OH</StateProvinceCode>");
            xmlConnect.AppendLine("             <PostalCode>" + this.ShipperZipCode + "</PostalCode>");
            xmlConnect.AppendLine("             <CountryCode>" + this.ShipperCountryCode + "</CountryCode>");
            xmlConnect.AppendLine("         </Address>");
            xmlConnect.AppendLine("     </ShipFrom>");

            xmlConnect.AppendLine("     <ShipTo>");
            xmlConnect.AppendLine("         <Address>");
            xmlConnect.AppendLine("             <PostalCode>" + this.ShipToZipCode + "</PostalCode>");
            xmlConnect.AppendLine("             <CountryCode>" + this.ShipToCountryCode + "</CountryCode>");
            xmlConnect.AppendLine("             <ResidentialAddress>" + addressTypeInd + "</ResidentialAddress>");
            xmlConnect.AppendLine("         </Address>");
            xmlConnect.AppendLine("     </ShipTo>");

            xmlConnect.AppendLine("     <Service><Code>" + this.UPSServiceCode + "</Code></Service>");
            xmlConnect.AppendLine(string.Format(package, this.PackageTypeCode, this.WeightUnit));
            xmlConnect.AppendLine("     <ShipmentServiceOptions>");
            xmlConnect.AppendLine(string.Format("<SaturdayDelivery>{0}</SaturdayDelivery>", isSaturdayShippingExtis ? "1" : "0"));
            xmlConnect.AppendLine("     </ShipmentServiceOptions>");
            xmlConnect.AppendLine("</Shipment>");
            xmlConnect.AppendLine("</RatingServiceSelectionRequest>");
            return xmlConnect;
        }


        /// <summary>
        /// Get UPS Response
        /// </summary>
        /// <param name="xmlConnect">string</param>
        /// <returns>string</returns>
        private System.Xml.XmlDocument GetUPSResponse(string xmlConnect)
        {
            System.Xml.XmlDocument _xmlRespone = new System.Xml.XmlDocument();
            try
            {
                WebClient objRequest = new WebClient(); byte[] objRetBytes;
                // Convert the input string into byte object
                byte[] objRequestBytes = Encoding.ASCII.GetBytes(xmlConnect);
                objRequest.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                objRetBytes = objRequest.UploadData(this._UPSGatewayURL, "POST", objRequestBytes);
                string xmlResponse = System.Text.Encoding.ASCII.GetString(objRetBytes);

                _xmlRespone.LoadXml(xmlResponse);
            }
            catch (Exception ex)
            {
                this.ErrorCode = "Connection failed.";
                this.ErrorDescription = "Error while trying to connect with Host Server.Please try again."; // Response Error code Description
                ZNodeLoggingBase.LogMessage("ZGetUPSRate >> GetUPSResponse >> " + ex.ToString());
                return null;
            }
            return _xmlRespone;
        }

        /// <summary>
        /// Get UPS Rate List
        /// </summary>
        /// <param name="_xmlRespone">XmlDocument</param>
        /// <returns>List<zUPSRate></returns>
        private List<zUPSRate> GetUPSRatesList(System.Xml.XmlDocument _xmlRespone)
        {
            List<zUPSRate> upsrate = new List<zUPSRate>();
            ZUPSRateEntities.RatingServiceSelectionResponse serviceResponse = new ZUPSRateEntities.RatingServiceSelectionResponse();
            serviceResponse = ConvertXmlToObject(_xmlRespone, serviceResponse) as ZUPSRateEntities.RatingServiceSelectionResponse;
            if (serviceResponse != null)
            {
                  int successCode = int.Parse(serviceResponse.Response.ResponseStatusCode);
                  if (successCode != 0)
                  {
                      foreach (ZUPSRateEntities.RatedShipment ratedShipment in serviceResponse.RatedShipmentList)
                      {
                          if (ratedShipment != null && ratedShipment.TotalCharges.MonetaryValue != 0)
                          {
                              zUPSRate objUPSRate = new zUPSRate();
                              objUPSRate.Code = ratedShipment.Service.Code;
                              if (ratedShipment.NegotiatedRates != null)
                              {
                                  if (ratedShipment.NegotiatedRates.NetSummaryCharges != null)
                                  {
                                      if (ratedShipment.NegotiatedRates.NetSummaryCharges.GrandTotal != null)
                                      {
                                          if (ratedShipment.NegotiatedRates.NetSummaryCharges.GrandTotal.MonetaryValue != 0)
                                          {
                                              objUPSRate.MonetaryValue = ratedShipment.NegotiatedRates.NetSummaryCharges.GrandTotal.MonetaryValue;
                                          }
                                          else
                                          {
                                              objUPSRate.MonetaryValue = ratedShipment.TotalCharges.MonetaryValue;
                                          }
                                      }
                                  }
                              }
                              else
                              {
                                  objUPSRate.MonetaryValue = ratedShipment.TotalCharges.MonetaryValue;
                              }

                              upsrate.Add(objUPSRate);
                          }
                      }
                  }
            }
            return upsrate;
        }
        #endregion

        #region[Public Methods]
        /// <summary>
        /// Returns the ShippingCost of all usp services 
        /// </summary>
        /// <param name="weight">string</param>
        /// <param name="shippingAdd">Address</param>
        /// <returns>List<zUPSRate></returns>
        public List<zUPSRate> GetShippingRate(string package, Address shippingAddress)
        {
            if (shippingAddress != null)
            {
                this.ShipToZipCode = shippingAddress.PostalCode;
            }

            if (shippingAddress.CountryCode != null && (!string.IsNullOrEmpty(shippingAddress.CountryCode)))
            {
                this.ShipToCountryCode = shippingAddress.CountryCode;
            }
            string addressTypeInd = "0"; // commercial

            if (this._ShipToAddressType == "Residential")
            {
                addressTypeInd = "1";
            }

            
            bool isSaturdayShippingExits = false;
            StringBuilder xmlConnect = GetUPSRequestInXML(package, addressTypeInd, isSaturdayShippingExits);
            System.Xml.XmlDocument _xmlRespone = GetUPSResponse(xmlConnect.ToString());

            try
            {
                List<zUPSRate> upsrate = new List<zUPSRate>();
                if (_xmlRespone != null && _xmlRespone.InnerXml != null)
                {
                    upsrate = GetUPSRatesList(_xmlRespone);
                }
                
                return upsrate;
            }
            catch (Exception ex)
            {
                this.ErrorCode = "Connection failed.";
                this.ErrorDescription = "Error while trying to connect with Host Server.Please try again."; // Response Error code Description
                ZNodeLoggingBase.LogMessage("ZGetUPSRate >> GetShippingRate >> " + ex.ToString());
                return null;
            }
        }
        #endregion

        #region[Helper Methods]

        /// <summary>
        /// Get UPS Service Name
        /// </summary>
        /// <param name="serviceCode">string</param>
        /// <returns>string</returns>
        public string GetUPSServiceName(string serviceCode)
        {
            string serviceName = string.Empty;
            switch (serviceCode)
            {
                case "01_SATURDAY":
                    serviceName = "UPS Next Day Air (Saturday Delivery)";
                    break;
                case "02_SATURDAY":
                    serviceName = "UPS 2nd Day Air (Saturday Delivery)";
                    break;
                case "01":
                    serviceName = "UPS Next Day Air";
                    break;
                case "02":
                    serviceName = "UPS 2nd Day Air";
                    break;
                case "03":
                    serviceName = "UPS Ground";
                    break;
                case "12":
                    serviceName = "UPS 3 Select";
                    break;
                case "13":
                    serviceName = "UPS Next Day Air Saver";
                    break;
                case "14":
                    serviceName = "UPS Next Day Air Early A.M. SM";
                    break;
                case "59":
                    serviceName = "UPS 2nd Day Air A.M.";
                    break;
                case "11":
                    serviceName = "UPS Standard";
                    break;
                case "65":
                    serviceName = "UPS Worldwide Saver";
                    break;
                case "54":
                    serviceName = "UPS Worldwide Express Plus";
                    break;
                case "08":
                    serviceName = "UPS Worldwide Expedited";
                    break;
                case "07":
                case "07_SATURDAY":
                    serviceName = "UPS Worldwide Express";
                    break;

            }
            return serviceName;
        }
        /// <summary>
        /// Sets the public properties
        /// </summary>
        private object ConvertXmlToObject(XmlDocument xmlDoc, object classObjectName)
        {
            object obj = null;
            if (xmlDoc != null)
            {
                XmlNodeReader reader = new XmlNodeReader(xmlDoc.DocumentElement);
                XmlSerializer ser = new XmlSerializer(classObjectName.GetType());
                obj = ser.Deserialize(reader);
            }

            return obj;
        }
        #endregion
    }
}

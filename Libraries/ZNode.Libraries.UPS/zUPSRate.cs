﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ZNode.Libraries.Shipping
{

    public class zUPSRate
    {
        public string Code { get; set; }
        public decimal MonetaryValue { get; set; }
        public string NumberOfDaysForDelivery { get; set; }
        public string DeliveryTime { get; set; }
        public bool IsSaturdayShipping { get; set; }

    }

    public class ZUPSRateEntities
    {
        [Serializable]
        [XmlRoot("RatingServiceSelectionResponse")]
        public class RatingServiceSelectionResponse
        {
            [XmlElement("Response")]
            public Response Response { get; set; }

            [XmlElement("RatedShipment")]
            public List<RatedShipment> RatedShipmentList { get; set; }
        }

        public class Response
        {
            [XmlElement("ResponseStatusCode")]
            public string ResponseStatusCode { get; set; }

            [XmlElement("ResponseStatusDescription")]
            public string ResponseStatusDescription { get; set; }

        }

        public class RatedShipment
        {
            [XmlElement("Service")]
            public Service Service { get; set; }

            [XmlElement("TotalCharges")]
            public TotalCharges TotalCharges { get; set; }

            [XmlElement("NegotiatedRates")]
            public NegotiatedRates NegotiatedRates { get; set; }

            //[XmlElement("GuaranteedDaysToDelivery")]
            public string GuaranteedDaysToDelivery { get; set; }

            //[XmlElement("ScheduledDeliveryTime")]
            public string ScheduledDeliveryTime { get; set; }

        }

        public class Service
        {
            [XmlElement("Code")]
            public string Code { get; set; }
        }

        public class TotalCharges
        {
            [XmlElement("CurrencyCode")]
            public string CurrencyCode { get; set; }

            [XmlElement("MonetaryValue")]
            public decimal MonetaryValue { get; set; }
        }

        public class NegotiatedRates
        {
            [XmlElement("NetSummaryCharges")]
            public NetSummaryCharges NetSummaryCharges { get; set; }
        }

        public class NetSummaryCharges
        {
            [XmlElement("GrandTotal")]
            public GrandTotal GrandTotal { get; set; }
        }

        public class GrandTotal
        {
            [XmlElement("CurrencyCode")]
            public string CurrencyCode { get; set; }

            [XmlElement("MonetaryValue")]
            public decimal MonetaryValue { get; set; }
        }
    }
}

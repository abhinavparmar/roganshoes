﻿using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;
using System.Configuration;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents a product items in the shopping cart
    /// </summary>

    public partial class ZNodeShoppingCartItem : ZNodeBusinessBase
    {
        /// <summary>
        /// Team Price Or Retail Price
        /// </summary>
        public decimal TeamPriceOrRetailPrice
        {
            get;
            set;
        }

        /// <summary>
        /// Returns Total Qunatity ordered
        /// </summary>       
        /// <returns>Returns the Quantty ordered</returns>
        public int GetTotalQuantityInCart()
        {
            int QuantityOrdered = 0;
            ZNodeShoppingCart carts = ZNodeShoppingCart.CurrentShoppingCart();
            // Loop through Shopping cart items
            if (carts != null && carts.ShoppingCartItems != null && carts.ShoppingCartItems.Count > 0)
            {
                foreach (ZNodeShoppingCartItem item in carts.ShoppingCartItems)
                {
                    if (item.IsProductTierPrice)
                    {
                        QuantityOrdered += item.Quantity;
                    }
                }
            }

            return QuantityOrdered;
        }

        /// <summary>
        /// Is this Product Tier Price
        /// </summary>
        public bool IsProductTierPrice
        {
            get
            {
                if (this.Product != null && this.Product.ZNodeTieredPriceCollection != null && this.Product.ZNodeTieredPriceCollection.Count > 0)
                {
                    if (this.Product.ZNodeTieredPriceCollection[0].TierStart == 10)
                    {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}

using System;
using System.Text;
using System.Web;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents Shopping cart and shopping cart items
    /// </summary>
    [Serializable()]
    public  partial class ZNodeShoppingCart : ZNodeBusinessBase
    {
        #region Member Variables
        private decimal _TaxRate = 0;
        private decimal _TaxCost = 0;
        protected ZNodeGenericCollection<ZNodeShoppingCartItem> _ShoppingCartItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
        private string _Coupon = string.Empty;
        private bool _CouponApplied = false;
        private bool _CouponValid = false;
        private ZNodeShipping _Shipping = new ZNodeShipping();
        private ZNodePayment _Payment = new ZNodePayment();
        private decimal _OrderLevelDiscount = 0;
        private decimal _OrderLevelShipping = 0;
        private decimal _OrderLevelTaxes = 0;
        private StringBuilder _PromoDescription = new StringBuilder();
        private StringBuilder _ErrorMessage = new StringBuilder();
        private decimal _GST;
        private decimal _HST;
        private decimal _PST;
        private decimal _VAT;
        private decimal _SalesTax;
        private decimal _GiftCardAmount = 0;
        private string _GiftCardNumber = string.Empty;
        private string _GiftCardMessage = string.Empty;
        private bool _IsGiftCardValid = false;
        private bool _IsGiftCardApplied = false;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeShoppingCart class
        /// </summary>
        public ZNodeShoppingCart()
        {
        }
        #endregion

        #region Public Properties       

        /// <summary>
        /// Gets the error messages that have been created in this cart.
        /// </summary>
        public string ErrorMessage
        {
            get { return this._ErrorMessage.ToString(); }
        }

        /// <summary>
        /// Gets or sets a collection of shopping cart items
        /// </summary>
        public ZNodeGenericCollection<ZNodeShoppingCartItem> ShoppingCartItems
        {
            get
            {
                return this._ShoppingCartItems;
            }

            set
            {
                this._ShoppingCartItems = value;
            }
        }

        /// <summary>
        /// Gets or sets the Znode coupon applied by the user.
        /// </summary>
        public string Coupon
        {
            get
            {
                return this._Coupon;
            }

            set
            {
                this._Coupon = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coupon applied
        /// </summary>
        public bool CouponApplied
        {
            get
            {
                return this._CouponApplied;
            }

            set
            {
                this._CouponApplied = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coupon valid.
        /// </summary>
        public bool CouponValid
        {
            get
            {
                return this._CouponValid;
            }

            set
            {
                this._CouponValid = value;
            }
        }

        /// <summary>
        /// Gets or sets the Shipping object
        /// </summary>
        public ZNodeShipping Shipping
        {
            get
            {
                return this._Shipping;
            }

            set
            {
                this._Shipping = value;
            }
        }

        /// <summary>
        /// Gets or sets the payment object
        /// </summary>
        public ZNodePayment Payment
        {
            get
            {
                return this._Payment;
            }

            set
            {
                this._Payment = value;
            }
        }

        /// <summary>
        /// Gets the total shipping cost of line items in the shopping cart.
        /// </summary>    
        public decimal ShippingCost
        {
            get
            {
                decimal totalShippingCost = 0;

                foreach (ZNodeShoppingCartItem item in this._ShoppingCartItems)
                {
                    totalShippingCost += item.ShippingCost;
                }

                totalShippingCost += this.OrderLevelShipping;

                decimal shippingDiscount = this._Shipping.ShippingDiscount;

                return totalShippingCost < shippingDiscount ? 0 : Math.Round((totalShippingCost - shippingDiscount), 2);
            }
        }

        /// <summary>
        /// Gets or sets the discounted applied to this order.
        /// </summary>    
        public decimal OrderLevelDiscount
        {
            get
            {
                return this._OrderLevelDiscount;
            }

            set
            {
                this._OrderLevelDiscount = value;
            }
        }

        /// <summary>
        /// Gets or sets the Gift Card Amount.
        /// </summary>
        public decimal GiftCardAmount
        {
            get
            {
                return Math.Round(this._GiftCardAmount, 2);
            }

            set
            {
                this._GiftCardAmount = value;
            }
        }

        /// <summary>
        /// Gets or sets the gift card number.
        /// </summary>
        public string GiftCardNumber
        {
            get { return this._GiftCardNumber; }
            set { this._GiftCardNumber = value; }
        }

        /// <summary>
        /// Gets or sets the Gift Card Message.
        /// </summary>
        public string GiftCardMessage
        {
            get { return this._GiftCardMessage; }
            set { this._GiftCardMessage = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the gift card number is valid.
        /// </summary>
        public bool IsGiftCardValid
        {
            get { return this._IsGiftCardValid; }
            set { this._IsGiftCardValid = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the gift card is applied to the shopping cart.
        /// </summary>
        public bool IsGiftCardApplied
        {
            get { return this._IsGiftCardApplied; }
            set { this._IsGiftCardApplied = value; }
        }

        /// <summary>
        /// Gets or sets the shipping cost for this order.
        /// </summary>     
        public decimal OrderLevelShipping
        {
            get
            {
                return this._OrderLevelShipping + this._Shipping.ShippingHandlingCharge;
            }

            set
            {
                this._OrderLevelShipping = value;
            }
        }

        /// <summary>
        /// Gets or sets the tax cost for this order.
        /// </summary>     
        public decimal OrderLevelTaxes
        {
            get
            {
                return Math.Round(this._OrderLevelTaxes + this._SalesTax + this._VAT + this._HST + this._PST + this._GST, 2);
            }

            set
            {
                this._OrderLevelTaxes = value;
            }
        }

        /// <summary>
        /// Gets or sets the total tax cost of items in the shopping cart.
        /// </summary>
        public decimal TaxCost
        {
            get
            {
                return Math.Round(this._TaxCost, 2);
            }

            set
            {
                this._TaxCost = value;
            }
        }

        /// <summary>
        /// Gets or sets the total sales tax cost of items in the shopping cart.
        /// </summary>
        public decimal SalesTax
        {
            get
            {
                return this._SalesTax;
            }

            set
            {
                this._SalesTax = value;
            }
        }

        /// <summary>
        /// Gets or sets the total tax cost of items in the shopping cart.
        /// </summary>
        public decimal VAT
        {
            get
            {
                return this._VAT;
            }

            set
            {
                this._VAT = value;
            }
        }

        /// <summary>
        /// Gets or sets the total tax cost of items in the shopping cart.
        /// </summary>
        public decimal PST
        {
            get
            {
                return this._PST;
            }

            set
            {
                this._PST = value;
            }
        }

        /// <summary>
        /// Gets or sets the total tax cost of items in the shopping cart.
        /// </summary>
        public decimal HST
        {
            get
            {
                return this._HST;
            }

            set
            {
                this._HST = value;
            }
        }

        /// <summary>
        /// Gets or sets the total tax cost of items in the shopping cart.
        /// </summary>
        public decimal GST
        {
            get
            {
                return this._GST;
            }

            set
            {
                this._GST = value;
            }
        }

        /// <summary>
        /// Gets or sets the sales tax rate (%)
        /// </summary>
        public decimal TaxRate
        {
            get
            {
                return this._TaxRate;
            }

            set
            {
                this._TaxRate = value;
            }
        }

        /// <summary>
        /// Gets the count of items in the shopping cart
        /// </summary>
        public int Count
        {
            get
            {
                return this._ShoppingCartItems.Count;
            }
        }

        /// <summary>
        /// Gets the total cost of items in the shopping cart before shipping and taxes
        /// </summary>
        public decimal SubTotal
        {
            get
            {
                decimal subTotal = 0;

                foreach (ZNodeShoppingCartItem item in this._ShoppingCartItems)
                {
                    decimal itemTotal = item.ExtendedPrice;
                    subTotal += itemTotal;
                }

                return subTotal;
            }
        }

        /// <summary>
        /// Gets total discount of applied to the items in the shopping cart.
        /// </summary>
        public decimal Discount
        {
            get
            {
                decimal subTotal = this.SubTotal;

                decimal totalDiscount = this._OrderLevelDiscount;

                foreach (ZNodeShoppingCartItem item in this._ShoppingCartItems)
                {
                    totalDiscount += item.DiscountAmount + item.ExtendedPriceDiscount;
                }

                if (totalDiscount > subTotal)
                {
                    return subTotal;
                }

                return totalDiscount;
            }
        }

        /// <summary>
        /// Gets the total cost after shipping, taxes and promotions
        /// </summary>        
        public decimal Total
        {
            get
            {
                decimal tempCalc = 0;
                tempCalc = (this.SubTotal - this.Discount) + this.ShippingCost + this._TaxCost + this.OrderLevelTaxes - this.GiftCardAmount;
                return tempCalc;
            }
        }

        /// <summary>
        /// Gets all of the promotion descriptions that have been added to the cart.
        /// </summary>        
        public string PromoDescription
        {
            get
            {
                StringBuilder promoDesc = new StringBuilder(this._PromoDescription.ToString());

                foreach (ZNodeShoppingCartItem item in this._ShoppingCartItems)
                {
                    if (promoDesc.Length > 0)
                    {
                        this._PromoDescription.Append("<br/>");
                    }

                    promoDesc.Append(item.PromoDescription.ToString());
                }

                return promoDesc.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the promotion description.
        /// </summary>
        public string AddPromoDescription
        {
            get
            {
                if (this._PromoDescription.Length > 0)
                {
                    this._PromoDescription.Append("<br/>");
                }

                this._PromoDescription.Append(this.PromoDescription);

                return this._PromoDescription.ToString();
            }

            set
            {
                this._PromoDescription.Append(value);
            }
        }

        /// <summary>
        /// Gets or sets a message to the shopping cart main error message.
        /// </summary>
        public string AddErrorMessage
        {
            get
            {
                if (this._ErrorMessage.Length > 0)
                {
                    this._ErrorMessage.Append("<br/>");
                }

                this._ErrorMessage.Append(this.ErrorMessage);

                return this._ErrorMessage.ToString();
            }

            set
            {
                this._ErrorMessage.Append(value);
            }
        }
        #endregion

        /// <summary>
        /// Get the current shopping cart.
        /// </summary>
        /// <returns>Returns the current ZNodeShoppingCart object.</returns>
        public static ZNodeShoppingCart CurrentShoppingCart()
        {
            ZNodeShoppingCart shoppingCart;

            // Get the user account from session
            shoppingCart = (ZNodeShoppingCart)HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()];

            if (shoppingCart == null)
            {
                return null;
            }
            else
            {
                return shoppingCart;
            }
        }

        /// <summary>
        /// Clear the previous error messages.
        /// </summary>
        public void ClearPreviousErrorMessges()
        {
            _ErrorMessage = new StringBuilder();
            _PromoDescription = new StringBuilder();
        }


        /// <summary>
        /// Returns Total Qunatity ordered
        /// </summary>       
        /// <returns>Returns the Quantty ordered</returns>
        public int GetTotalQuantityInCartForUSPS
        {
            get
            {
                int quantityOrdered = 0;

                // Loop through Shopping cart items
                if (_ShoppingCartItems != null && _ShoppingCartItems.Count > 0)
                {
                    foreach (ZNodeShoppingCartItem item in _ShoppingCartItems)
                    {
                        quantityOrdered += item.Quantity;
                    }
                }
                return quantityOrdered;
            }
        }
    }
}

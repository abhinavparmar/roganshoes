﻿using System;
using System.Xml.Serialization;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents a bundle product item
    /// </summary>    
    [Serializable()]
    [XmlRoot("ZNodeBundleProduct")]
    public class ZNodeBundleProductEntity : ZNodeProductBaseEntity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeBundleProductEntity class
        /// </summary>
        public ZNodeBundleProductEntity()
        {
        }
        #endregion
    }
}

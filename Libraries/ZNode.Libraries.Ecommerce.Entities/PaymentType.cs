
namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents the PaymentType enumeration
    /// </summary>
    public enum PaymentType 
    {
        /// <summary>
        /// Payment type is credir card.
        /// </summary>
        CREDIT_CARD = 0,

        /// <summary>
        /// Payment type is Purchase Order
        /// </summary>
        PURCHASE_ORDER = 1,

        /// <summary>
        /// Payment type is PayPal
        /// </summary>
        PAYPAL = 2,

        /// <summary>
        /// Payment type is Google Checkout
        /// </summary>
        GOOGLE_CHECKOUT = 3,

        /// <summary>
        /// Payment type is COD
        /// </summary>
        COD = 4,

        /// <summary>
        /// Payment type is 2Checout
        /// </summary>
        TwoCO = 5
    }
}

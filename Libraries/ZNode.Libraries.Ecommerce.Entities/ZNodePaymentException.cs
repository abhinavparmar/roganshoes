using System;
using System.Collections.Generic;
using System.Text;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents an exception thrown when attempting to pass payment gateway.
    /// </summary>
    public class ZNodePaymentException : System.Exception 
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePaymentException class
        /// </summary>
        /// <param name="message">Exception message.</param>
        public ZNodePaymentException(string message) : base(message)
        {
        }
    }
}

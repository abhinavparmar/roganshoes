﻿using System;
using System.Text;
using System.Web;
using ZNode.Libraries.Framework.Business;
namespace ZNode.Libraries.ECommerce.Entities
{
    public partial class ZNodeShoppingCart : ZNodeBusinessBase
    {
        #region Public Member

        /// <summary>
        /// Get or set  Extended shipping charges by admin
        /// </summary>
        public decimal ExtendedShippingCost { get; set; }

        /// <summary>
        /// Get or set  Extended shipping charges by admin
        /// </summary>
        public bool IsShipngChrgsExtended { get; set; }

        #endregion
    }
}

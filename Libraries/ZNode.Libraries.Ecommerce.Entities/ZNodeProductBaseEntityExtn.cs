﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;

namespace ZNode.Libraries.ECommerce.Entities
{
    public partial class ZNodeProductBaseEntity : ZNodeBusinessBase
    {
        /// <summary>
        /// Set true if Brand , Catalog, Category and Product Promotion is Appplied
        /// </summary>
        [XmlIgnore()]
        public bool IsCustomPromotionApplied
        {
            get;
            set;
        }

        /// <summary>
        /// Set True for Rename the add to cart button to pre order
        /// </summary>
        [XmlElement()]
        public bool PreOrderFlag
        {
            get;
            set;
        }

        /// <summary>
        /// If true product promotion  will not ablicable for that product
        /// </summary>
        [XmlElement()]
        public bool IsMapPrice
        {
            get;
            set;
        }

        /// <summary>
        /// set true if Product is Taxable
        /// </summary>
        [XmlIgnore()]
        public bool IsTaxable
        {
            get; 
            set;
        }

        /// <summary>
        /// get or set product discount percent
        /// </summary>
        [XmlIgnore()]
        public decimal PromotionDiscountPercent
        {
            get;
            set;
        }

        /// <summary>
        /// get or set product discount percent
        /// </summary>
        [XmlIgnore()]
        public decimal PricingDiscountPercent
        {
            get;
            set;
        }


        /// <summary>
        /// Get Or Set Comma Seperated Product Category IDs
        /// </summary>
        [XmlElement()]
        public string ProductCategoryHierarchy
        {
            get;
            set;
        }

        [XmlElement("ZNodeProductCategoryList")]
        public ZNodeGenericCollection<ZNodeProductCategoryList> ZNodeProductCategoryList
        {
            get;
            set;
        }

        [XmlElement("ZNodeManufacturerExtn")]
        public ZNodeGenericCollection<ManufacturerExtn> ZNodeManufacturerExtnList
        {
            get;
            set;
        }

        [XmlElement("DropShipInd")]
        public bool? DropShipInd
        {
            get;
            set;
        }

        [XmlElement("VideoUrl")]
        public string VideoUrl
        {
            get;
            set;
        }

        [XmlElement("ZNodeProductImage")]
        public ZNodeGenericCollection<ProductImage> ZNodeProductImage
        {
            get;
            set;
        }

        [XmlElement("ZeonProductSpecifications")]
        public ZNodeGenericCollection<ZeonProductSpecifications> ZeonProductSpecifications
        {
            get;
            set;
        }

        /// <summary>
        /// Get Or Set  is Product Has Indexed Enabled
        /// </summary>
        [XmlElement()]
        public bool IsIndexFollow
        {
            get;
            set;
        }
    }


    /// <summary>
    ///  Holds the Color Sku Picture Path
    /// </summary>
    [Serializable()]
    [XmlRoot("ZNodeProductCategoryList")]
    public class ZNodeProductCategoryList
    {
        [XmlElement()]
        public int CategoryID { get; set; }

    }

}

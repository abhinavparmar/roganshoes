using System;
using System.Text;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents the properties and methods of a product AddOn.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeAddOnListEntity : ZNodeBusinessBase
    {
        #region Private Memeber Variables
        private string _SelectedAddOnValues = string.Empty;
        private ZNodeGenericCollection<ZNodeAddOnEntity> _AddOnCollection = new ZNodeGenericCollection<ZNodeAddOnEntity>();
        #endregion

        /// <summary>
        /// Initializes a new instance of the ZNodeAddOnListEntity class
        /// </summary>
        public ZNodeAddOnListEntity()
        {
        }

        /// <summary>
        /// Gets or sets the products contained within this category
        /// </summary>
        [XmlElement("ZNodeAddOn")]
        public ZNodeGenericCollection<ZNodeAddOnEntity> AddOnCollection
        {
            get
            {
                return this._AddOnCollection;
            }

            set
            {
                this._AddOnCollection = value;
            }
        }

        /// <summary>
        /// Gets the total shipping cost of all the selected addon values
        /// </summary>
        [XmlIgnore()]
        public decimal TotalShippingCost
        {
            get
            {
                decimal shippingCost = 0;

                foreach (ZNodeAddOnEntity addOn in this._AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity value in addOn.AddOnValueCollection)
                    {
                        shippingCost += value.ShippingCost;
                    }
                }

                return shippingCost;
            }
        }

        /// <summary>
        /// Gets the total Add-on cost of all the selected addon values
        /// </summary>
        [XmlIgnore()]
        public decimal TotalAddOnCost
        {
            get
            {
                decimal addOnCost = 0;

                foreach (ZNodeAddOnEntity addOn in this._AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity value in addOn.AddOnValueCollection)
                    {
                        addOnCost += value.FinalPrice;
                    }
                }

                return addOnCost;
            }
        }

        /// <summary>
        /// Gets or sets the addonIs for all the selected addon values
        /// </summary>
        [XmlIgnore()]
        public string SelectedAddOnValues
        {
            get
            {
                return this._SelectedAddOnValues;
            }

            set
            {
                this._SelectedAddOnValues = value;
            }
        }

        /// <summary>
        /// Gets the shopping cart selected addons description        
        /// This metadata is displayed on the shopping cart list
        /// </summary>
        [XmlIgnore()]
        public string ShoppingCartAddOnsDescription
        {
            get
            {
                StringBuilder addOnDescription = new StringBuilder();

                foreach (ZNodeAddOnEntity addOn in this._AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        decimal decPrice = addOnValue.FinalPrice;

                        if (string.Compare(addOn.DisplayType, "TextBox", true) == 0)
                        {
                            addOnDescription.Append(addOnValue.Name);
                            addOnDescription.Append(" - ");
                            addOnDescription.Append(addOnValue.CustomText);
                        }
                        else
                        {
                            addOnDescription.Append(addOn.Name);
                            addOnDescription.Append(" - ");
                            addOnDescription.Append(addOnValue.Name);
                        }

                        addOnDescription.Append("<br />");
                    }
                }

                return addOnDescription.ToString();
            }
        }
    }
}

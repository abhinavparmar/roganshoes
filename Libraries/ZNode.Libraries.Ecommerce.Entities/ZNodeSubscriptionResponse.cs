using System;
using System.Collections.Generic;
using System.Text;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    [Serializable()]
    public class ZNodeSubscriptionResponse : ZNodeBusinessBase
    {
        private string _ResponseCode = string.Empty;
        private string _ResponseText = string.Empty;
        private string _SubscriptionId;
        private int _ReferenceId = 0;
        private int _ParentLineItemId = 0;
        private bool _IsSuccess = false;

        /// <summary>
        /// Gets or sets the Gateway Response code 
        /// </summary>
        public string ResponseCode
        {
            get { return this._ResponseCode; }
            set { this._ResponseCode = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway Response Text 
        /// </summary>
        public string ResponseText
        {
            get { return this._ResponseText; }
            set { this._ResponseText = value; }
        }      
        
        /// <summary>
        /// Gets or sets the Gateway TransactionID 
        /// </summary>
        public string SubscriptionId
        {
            get { return this._SubscriptionId; }
            set { this._SubscriptionId = value; }
        }

        /// <summary>
        /// Gets or sets the OrderLienItemId
        /// </summary>
        public int ReferenceId
        {
            get { return this._ReferenceId; }
            set { this._ReferenceId = value; }
        }        

        /// <summary>
        /// Gets or sets the parent OrderLineItemId
        /// </summary>
        public int ParentLineItemId
        {
            get { return this._ParentLineItemId; }
            set { this._ParentLineItemId = value; }
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether the gateway response is success
        /// </summary>
        public bool IsSuccess
        {
            get { return this._IsSuccess; }
            set { this._IsSuccess = value; }
        }
    }
}

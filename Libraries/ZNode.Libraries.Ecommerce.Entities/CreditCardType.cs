using System;
using System.Collections.Generic;
using System.Text;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    public enum CreditCardType
    {
        /// <summary>
        /// Cart type is Amex.
        /// </summary>        
        Amex,

        /// <summary>
        /// Cart type is Doners.
        /// </summary>
        Diners,

        /// <summary>
        /// Cart type is Discover.
        /// </summary>
        Discover,

        /// <summary>
        /// Cart type is JCB.
        /// </summary>
        JCB,       

        /// <summary>
        /// Cart type is Master Card.
        /// </summary>
        MasterCard,

        /// <summary>
        /// Cart type is Solo.
        /// </summary>
        Solo,

        /// <summary>
        /// Cart type is Maestro.
        /// </summary>
        Maestro,        

        /// <summary>
        /// Cart type is Visa.
        /// </summary>
        Visa
    }
}

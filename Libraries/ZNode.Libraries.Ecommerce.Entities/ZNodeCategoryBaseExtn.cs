﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents the properties and methods of a ZNodeCategory.
    /// </summary>
    public partial class ZNodeCategoryBase : ZNodeBusinessBase
    {
        private bool _IsIndexFollow;
        private string _CategoryBanner;
        
        /// <summary>
        /// Gets or sets the IsIndexFollow values
        /// </summary>
        [XmlElement("IsIndexFollow")]
        public bool IsIndexFollow
        {
            get { return _IsIndexFollow; }
            set { _IsIndexFollow = value; }
        }

        /// <summary>
        /// Gets or sets the CategoryBanner values
        /// </summary>
        [XmlElement("CategoryBanner")]
        public string CategoryBanner
        {
            get { return _CategoryBanner; }
            set { _CategoryBanner = value; }
        }

        /// <summary>
        /// Gets or sets the CategoryBanner values
        /// </summary>
        [XmlElement("ISShowSubCategory")]
        public string ISShowSubCategory
        {
            get;
            set;
        }

    }
}

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Provides an instance properties for customer Billing and Shipping address
    /// </summary>
    public class Address
    {
        #region Member Variables
        private string _FirstName = string.Empty;
        private string _MiddleName = string.Empty;
        private string _LastName = string.Empty;
        private string _CompanyName = string.Empty;
        private string _Street1 = string.Empty;
        private string _Street2 = string.Empty;
        private string _City = string.Empty;
        private string _StateCode = string.Empty;
        private string _PostalCode = string.Empty;
        private string _CountryCode = string.Empty;
        private string _PhoneNumber = string.Empty;
        private string _FaxNumber = string.Empty;
        private string _EmailId = string.Empty;
        #endregion

        #region Public Instance Properties
        /// <summary>
        /// Gets or sets the Customer FirstName
        /// </summary>
        public string FirstName
        {
            get { return this._FirstName; }
            set { this._FirstName = value; }
        }

        /// <summary>
        /// Gets or sets the Customer MiddleName
        /// </summary>
        public string MiddleName
        {
            get { return this._MiddleName; }
            set { this._MiddleName = value; }
        }

        /// <summary>
        /// Gets or sets the Customer LastName
        /// </summary>
        public string LastName
        {
            get { return this._LastName; }
            set { this._LastName = value; }
        }

        /// <summary>
        /// Gets or sets the Customer Company Name
        /// </summary>
        public string CompanyName
        {
            get { return this._CompanyName; }
            set { this._CompanyName = value; }
        }

        /// <summary>
        /// Gets or sets the Customer Address Line 1 
        /// </summary>
        public string Street1
        {
            get { return this._Street1; }
            set { this._Street1 = value; }
        }

        /// <summary>
        /// Gets or sets the Customer Address Line 2
        /// </summary>
        public string Street2
        {
            get { return this._Street2; }
            set { this._Street2 = value; }
        }

        /// <summary>
        /// Gets or sets the Customer City
        /// </summary>
        public string City
        {
            get { return this._City; }
            set { this._City = value; }
        }

        /// <summary>
        /// Gets or sets the Customer State Code
        /// </summary>
        public string StateCode
        {
            get { return this._StateCode; }
            set { this._StateCode = value; }
        }

        /// <summary>
        /// Gets or sets the Customer postal code
        /// </summary>
        public string PostalCode
        {
            get { return this._PostalCode; }
            set { this._PostalCode = value; }
        }

        /// <summary>
        /// Gets or sets the Customer Country Code
        /// </summary>
        public string CountryCode
        {
            get { return this._CountryCode; }
            set { this._CountryCode = value; }
        }

        /// <summary>
        /// Gets or sets the Customer Phone Number
        /// </summary>
        public string PhoneNumber
        {
            get { return this._PhoneNumber; }
            set { this._PhoneNumber = value; }
        }

        /// <summary>
        /// Gets or sets the Customer Fax Number
        /// </summary>
        public string FaxNumber
        {
            get { return this._FaxNumber; }
            set { this._FaxNumber = value; }
        }

        /// <summary>
        /// Gets or sets the Customer Email id
        /// </summary>
        public string EmailId
        {
            get { return this._EmailId; }
            set { this._EmailId = value; }
        }

        #endregion
    }
}

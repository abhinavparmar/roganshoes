﻿using System;
using System.Collections.Generic;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Processes different payment types
    /// </summary>
    public partial class ZNodePayment : ZNodeBusinessBase
    {
        #region Private Member
        private List<CreditCard> _CreditCardList = new List<CreditCard>();
        #endregion
        #region Public Properties

        /// <summary>
        /// Gets or sets the credit card list 
        /// </summary>
        public List<CreditCard> CreditCardList
        {
            get
            {
                return this._CreditCardList;
            }
            set
            {
                this._CreditCardList = value;
            }
        }

        #endregion
    }
}

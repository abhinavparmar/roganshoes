using System;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Property bag for settings used by the Shipping Rules.
    /// </summary>
    [Serializable()]
    public class ZNodeShipping : ZNodeBusinessBase
    {
        #region Member Variables
        private string _ShippingName = string.Empty;
        private int _ShippingId;
        private decimal _ShippingHandlingCharge = 0;
        private string _ResponseMessage = string.Empty;
        private decimal _ShippingDiscount = 0;
        private string _ResponseCode = "0";
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeShipping class
        /// </summary>
        public ZNodeShipping() 
        {
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the shipping option Id
        /// </summary>
        public int ShippingID
        {
            get
            {
                return this._ShippingId;
            }

            set
            {
                this._ShippingId = value;
            }
        }

        /// <summary>
        /// Gets or sets the shipping option name
        /// </summary>
        public string ShippingName
        {
            get
            {
                return this._ShippingName;
            }

            set
            {
                this._ShippingName = value;
            }
        }

        /// <summary>
        /// Gets or sets the discount shipping cost
        /// </summary>
        public decimal ShippingDiscount
        {
            get
            {
                return this._ShippingDiscount;
            }

            set
            {
                this._ShippingDiscount = value;
            }
        }

        /// <summary>
        /// Gets or sets the handling charge for shipping
        /// </summary>
        public decimal ShippingHandlingCharge
        {
            get { return this._ShippingHandlingCharge; }
            set { this._ShippingHandlingCharge = value; }
        }

        /// <summary>
        /// Gets or sets the shipping option response code
        /// </summary>
        public string ResponseCode
        {
            get
            {
                return this._ResponseCode;
            }

            set
            {
                this._ResponseCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the shipping option response description
        /// </summary>
        public string ResponseMessage
        {
            get
            {
                return this._ResponseMessage;
            }

            set
            {
                this._ResponseMessage = value;
            }
        }
        #endregion
    }
}

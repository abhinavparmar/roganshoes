﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Ecommerce.Entities
{
    public class ZNodeCategoryNavigation
    {
        public int NumberOfProducts { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public IEnumerable<ZNodeCategoryNavigation> CategoryHierarchy { get; set; }
    }
}

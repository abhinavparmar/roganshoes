﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Ecommerce.Entities
{
   /// <summary>
   /// Class for jquery Autocomplete 
   /// </summary>
    public  class ZnodeTypeAheadAutoComplete
   {
       /// <summary>
       /// Display in text box
       /// </summary>
        public string label;
        /// <summary>
        /// Category Name
        /// </summary>
       public string value;
        /// <summary>
        /// Product name
        /// </summary>
       public string name;
   }
}

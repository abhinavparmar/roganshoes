using System;
using System.Collections.Generic;
using System.Text;

namespace ZNode.Libraries.ECommerce.Entities
{
    public enum RecurringBillingUnit
    {
        /// <summary>
        /// Recurring Billing unit is 1 weekl.
        /// </summary>
        WEEK = 7,

        /// <summary>
        /// Recurring Billing unit is 2 week.
        /// </summary>
        BIWK = 14,

        /// <summary>
        /// Recurring Billing unit is 4 week.
        /// </summary>
        FRWK = 28,

        /// <summary>
        /// Recurring Billing unit is 1 month.
        /// </summary>
        MONTH = 30,

        /// <summary>
        /// Recurring Billing unit is 1 month.
        /// </summary>
        MONT = 30,

        /// <summary>
        /// Recurring Billing unit is 1 year.
        /// </summary>
        YEAR = 365,

        /// <summary>
        /// Recurring Billing unit is NULL.
        /// </summary>
        NULL = 0,
    }
}

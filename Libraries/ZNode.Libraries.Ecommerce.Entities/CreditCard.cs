using System;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Provides an instance properties for Credit card information
    /// </summary>
    [Serializable()]
    public class CreditCard : ZNodePaymentBase
    {
        #region Private Member Variables
        private string _CardNumber = string.Empty;
        private string _CardExp = string.Empty;
        private string _CardSecurityCode = string.Empty;
        private decimal _Amount = 0;
        private decimal _SubTotal = 0;
        private decimal _ShippingCharge = 0;
        private decimal _TaxCost = 0;
        private int _OrderId = 0;
        private string _TransactionId;
        private string _AuthCode = string.Empty;
        private string _Description = string.Empty;        
        private string _ProcTxnId = string.Empty;
        private string _CardDataToken = string.Empty;
        private string _CardType = string.Empty;
        private string _CardHolderName = string.Empty;
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Credit Card instance Properties

        /// <summary>
        /// Gets or sets the cart type.
        /// </summary>
        public string CardType
        {
            get { return this._CardType; }
            set { this._CardType = value; }
        }

        /// <summary>
        /// Gets or sets the credit card number
        /// </summary>
        public string CardNumber
        {
            get
            {
                return this.encryption.DecryptData(this._CardNumber);
            }

            set
            {
                this._CardNumber =this.encryption.EncryptData(value);
            }
        }

        /// <summary>
        /// Gets or sets the credit card expiration date
        /// </summary>
        public string CreditCardExp
        {
            get
            {
                return this.encryption.DecryptData(this._CardExp);
            }

            set
            {
                this._CardExp = value;
                string cardExp = this.encryption.EncryptData(this._CardExp);
                this._CardExp = cardExp;
            }
        }

        /// <summary>
        /// Gets or sets the credit card security code
        /// </summary>
        public string CardSecurityCode
        {
            get
            {
                return this.encryption.DecryptData(this._CardSecurityCode);
            }

            set
            {
                this._CardSecurityCode = value;
                string cardSecuritycode = this.encryption.EncryptData(this._CardSecurityCode);
                this._CardSecurityCode = cardSecuritycode;
            }
        }

        /// <summary>
        /// Gets or sets the Order ID
        /// </summary>
        public int OrderID
        {
            get { return this._OrderId; }
            set { this._OrderId = value; }
        }

        /// <summary>
        /// Gets or sets the ordered amount property
        /// </summary>
        public decimal Amount
        {
            get { return this._Amount; }
            set { this._Amount = value; }
        }

        /// <summary>
        /// Gets or sets the ordered Sub Total amount property
        /// </summary>
        public decimal SubTotal
        {
            get { return this._SubTotal; }
            set { this._SubTotal = value; }
        }

        /// <summary>
        /// Gets or sets the Shipping Charge property
        /// </summary>
        public decimal ShippingCharge
        {
            get { return this._ShippingCharge; }
            set { this._ShippingCharge = value; }
        }

        /// <summary>
        /// Gets or sets the tax cost property
        /// </summary>
        public decimal TaxCost
        {
            get { return this._TaxCost; }
            set { this._TaxCost = value; }
        }

        /// <summary>
        /// Gets or sets the transaction ID
        /// </summary>
        public string TransactionID
        {
            get { return this._TransactionId; }
            set { this._TransactionId = value; }
        }

        /// <summary>
        /// Gets or sets the authorization code
        /// </summary>
        public string AuthCode
        {
            get { return this._AuthCode; }
            set { this._AuthCode = value; }
        }

        /// <summary>
        /// Gets Credit Card Type
        /// </summary>
        public CreditCardType? CreditCardNumberType
        {
            get
            {
                return this.GetCardTypeFromNumber(this.CardNumber);
            }
        }

        /// <summary>
        /// Gets or sets the tax Ind.
        /// </summary>
        public string ProcTxnId
        {
            get { return this._ProcTxnId; }
            set { this._ProcTxnId = value; }
        }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }

        /// <summary>
        /// Gets or sets the card data token
        /// </summary>
        public string CardDataToken
        {
            get { return this._CardDataToken; }
            set { this._CardDataToken = value; }
        }

        /// <summary>
        /// Gets or sets the card honlder name.
        /// </summary>
        public string CardHolderName
        {
            get
            {
                return this._CardHolderName;
            }

            set
            {
                this._CardHolderName = value;
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Determines the credit card type using the card number
        /// </summary>
        /// <param name="cardNum">Card number to get the card type.</param>
        /// <returns>Returns the CreditCardType from card number.</returns>
        private CreditCardType? GetCardTypeFromNumber(string cardNum)
        {
            // Validation based on article from Wikipedia http://en.wikipedia.org/wiki/Creditcardnumbers
            if (cardNum.StartsWith("4"))
            {
                return CreditCardType.Visa;
            }
            else if (cardNum.StartsWith("34") || cardNum.StartsWith("37"))
            {
                return CreditCardType.Amex;
            }
            else if (cardNum.StartsWith("51") || cardNum.StartsWith("52") || cardNum.StartsWith("53") || cardNum.StartsWith("54") || cardNum.StartsWith("55"))
            {
                return CreditCardType.MasterCard;
            }
            else if (cardNum.StartsWith("6011") || cardNum.StartsWith("62") || cardNum.StartsWith("64") || cardNum.StartsWith("65"))
            {
                return CreditCardType.Discover;
            }
            else if (cardNum.StartsWith("36"))
            {
                return CreditCardType.Diners;
            }
            else if (cardNum.StartsWith("5018") || cardNum.StartsWith("5020") || cardNum.StartsWith("5038") || cardNum.StartsWith("6304") || cardNum.StartsWith("6759") || cardNum.StartsWith("6761"))
            {
                return CreditCardType.Maestro;
            }
            else if (cardNum.StartsWith("6334") || cardNum.StartsWith("6767"))
            {
                return CreditCardType.Solo;
            }
            else if (cardNum.StartsWith("3528") || cardNum.StartsWith("3589"))
            {
                return CreditCardType.JCB;
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}

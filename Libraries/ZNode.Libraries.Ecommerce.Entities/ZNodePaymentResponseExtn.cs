﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents a payment submission response including response codes and success flags
    /// </summary>
    public partial class ZNodePaymentResponse : ZNodeBusinessBase
    {
        public List<ZNodePaymentResponse> AdditionalTenderResponse { get; set; }

        public string MultipleTenderErrText { get; set; }

    }
}

using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;
using System.Configuration;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents a product items in the shopping cart
    /// </summary>
    [Serializable()]
    public partial class ZNodeShoppingCartItem : ZNodeBusinessBase
    {
        #region Private Member Variables
        protected ZNodeProductBaseEntity _Product = new ZNodeProductBaseEntity();
        private string _Guid = string.Empty;
        private decimal _UnitPrice = 0;
        private int _Quantity = 1;
        protected decimal _ExtendedPrice = 0;
        private decimal _ShippingCost = 0;
        private string _PromoDescription = string.Empty;
        private decimal _ExtendedPriceDiscount = 0;
        private decimal _TieredPrice = 0;
        private decimal _PromotionalPrice = 0;
        private int _lineItemId = 0;
        private bool _IsTaxCalculated = false;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeShoppingCartItem class. Create GUID value for the shopping cart item
        /// </summary>
        public ZNodeShoppingCartItem()
        {
            // Create GUID
            this._Guid = System.Guid.NewGuid().ToString();
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the database shopping cart line item ID for this cart item.
        /// </summary>
        public int LineItemId
        {
            get
            {
                return this._lineItemId;
            }
            set
            {
                this._lineItemId = value;
            }
        }

        /// <summary>
        /// Gets the GUID (unique value) for this Shopping cart item. 
        /// </summary>
        [XmlElement()]
        public string GUID
        {
            get
            {
                return this._Guid;
            }
        }

        /// <summary>
        /// Gets or sets the product object for this cart item
        /// </summary>        
        public ZNodeProductBaseEntity Product
        {
            get
            {
                return this._Product;
            }

            set
            {
                this._Product = value;
            }
        }

        /// <summary>
        /// Gets the unit price of this line item.
        /// </summary>        
        public decimal UnitPrice
        {
            get
            {
                decimal basePrice = this.TieredPricing;

                this._UnitPrice = basePrice + this._Product.AddOnPrice;

                return this._UnitPrice;
            }
        }

        /// <summary>
        /// Gets or sets the product extended price
        /// </summary>       
        public decimal ExtendedPrice
        {
            get
            {
                this._ExtendedPrice = this.UnitPrice * this._Quantity;

                return this._ExtendedPrice;
            }

            set
            {
                this._ExtendedPrice = value;
            }
        }
              

        /// <summary>
        /// Gets the product tiered pricing. Returns the tiered Price for this product based on the quantity.
        /// If no tiered pricing applied, it will return the product base price.
        /// </summary>
        public decimal TieredPricing
        {
            get
            {              

                int profileId = 0;
                ZNodeGenericCollection<ZNodeProductTierEntity> productTiers = this._Product.ZNodeTieredPriceCollection;
                int quantity = this._Quantity;

                //Perfi Custom Code:start               
                if (ConfigurationManager.AppSettings["CheerAndPomPortalID"] != null && !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString()) && ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString() == ZNodeConfigManager.SiteConfig.PortalID.ToString())
                {
                    quantity = GetTotalQuantityInCart();
                    if (quantity == 0)
                    {
                        quantity = this._Quantity;
                    }
                }
                //Perfi Custom Code:End

                decimal finalPrice = this._Product.FinalPrice;

                // Check product tiers list
                if (productTiers.Count > 0)
                {
                    // Get user profile from ProfileCache Session object
                    if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
                    {
                        ZNode.Libraries.DataAccess.Entities.Profile profile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];

                        if (profile != null)
                        {
                            profileId = profile.ProfileID;
                        }
                    }

                    foreach (ZNodeProductTierEntity productTieredPrice in productTiers)
                    {
                        if (quantity >= productTieredPrice.TierStart && productTieredPrice.TierEnd >= quantity)
                        {
                            if (productTieredPrice.ProfileID == 0)
                            {
                                finalPrice = productTieredPrice.Price;                               
                                break;
                            }
                            else
                            {
                                if (profileId == productTieredPrice.ProfileID)
                                {
                                    finalPrice = productTieredPrice.Price;                                   
                                    break;
                                }
                            }
                        }
                    }
                }

                this._TieredPrice = finalPrice;

                // Get Product tiered price if one exists, otherwise unit price
                return finalPrice;
            }
        }

        /// <summary>
        /// Gets or sets the qunatity of products for this line item.
        /// </summary>        
        public int Quantity
        {
            get
            {
                return this._Quantity;
            }

            set
            {
                this._Quantity = value;
            }
        }

        /// <summary>
        /// Gets or sets the shipping cost for this line item.
        /// </summary>        
        public decimal ShippingCost
        {
            get
            {
                decimal shipCost = 0;

                foreach (ZNodeAddOnEntity addOn in this._Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnvalue in addOn.AddOnValueCollection)
                    {
                        shipCost += addOnvalue.ShippingCost;
                    }
                }

                return this._ShippingCost + shipCost + this._Product.ShippingCost;
            }

            set
            {
                this._ShippingCost = value;
            }
        }

        /// <summary>
        ///  Gets the tax cost for this line item.
        /// </summary>        
        public decimal TaxCost
        {
            get
            {
                decimal taxGST = 0;
                decimal taxPST = 0;
                decimal taxHST = 0;
                decimal taxVAT = 0;
                decimal taxSalesTax = 0;
                decimal taxCost = this._Product.VAT + this._Product.GST + this._Product.HST + this._Product.PST + this._Product.SalesTax;

                foreach (ZNodeAddOnEntity addOn in this._Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        taxGST += addOnValue.HST;
                        taxPST += addOnValue.HST;
                        taxHST += addOnValue.HST;
                        taxVAT += addOnValue.HST;
                        taxSalesTax += addOnValue.HST;
                    }
                }

                return taxCost + taxGST + taxHST + taxPST + taxVAT + taxSalesTax;
            }
        }

        /// <summary>
        /// Gets the discount amount applied to this line item.
        /// </summary>        
        public decimal DiscountAmount
        {
            get
            {
                decimal discountAmount = 0;

                discountAmount += this._Product.DiscountAmount * this._Quantity;

                // Loop through the selected addOns for this product
                foreach (ZNodeAddOnEntity AddOn in this._Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        discountAmount += AddOnValue.DiscountAmount * this._Quantity;
                    }
                }

                return discountAmount;
            }
        }

        /// <summary>
        /// Gets or sets the price of product after applying promotions on tiered price
        /// </summary>        
        public decimal PromotionalPrice
        {
            get
            {
                return this._PromotionalPrice;
            }

            set
            {
                this._PromotionalPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the discount amount applied to the extended price of this line item.
        /// </summary>        
        public decimal ExtendedPriceDiscount
        {
            get
            {
                return this._ExtendedPriceDiscount;
            }

            set
            {
                this._ExtendedPriceDiscount = value;
            }
        }

        /// <summary>
        /// Gets the promotion description.
        /// </summary>        
        public string PromoDescription
        {
            get
            {
                return this._PromoDescription;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is tax already calculated for the current shopping cart.
        /// </summary>
        public bool IsTaxCalculated
        {
            get { return this._IsTaxCalculated; }
            set { this._IsTaxCalculated = value; }
        }      

        #endregion
    }
}

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage customer notes
    /// </summary>
    public class NoteAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Returns Cusomer Note for Account ID
        /// </summary>
        /// <param name="accountId">Account Id to get the list of note object.</param>
        /// <returns>Returns list of Note object by Account Id</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.Note> GetByAccountID(int accountId)
        {
            ZNode.Libraries.DataAccess.Service.NoteService noteService = new ZNode.Libraries.DataAccess.Service.NoteService();

            TList<ZNode.Libraries.DataAccess.Entities.Note> notesList = noteService.GetByAccountID(accountId);

            return notesList;
        }

        /// <summary>
        /// Returns Cusomer Note for Case ID
        /// </summary>
        /// <param name="caseId">Case Id to get the list of Note.</param>
        /// <returns>Returns list of Note object</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.Note> GetByCaseID(int caseId)
        {
            ZNode.Libraries.DataAccess.Service.NoteService noteService = new ZNode.Libraries.DataAccess.Service.NoteService();

            TList<ZNode.Libraries.DataAccess.Entities.Note> notesList = noteService.GetByCaseID(caseId);

            return notesList;
        }

        /// <summary>
        /// Add New customer notes.
        /// </summary>
        /// <param name="note">Note object to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Insert(Note note)
        {
            ZNode.Libraries.DataAccess.Service.NoteService noteService = new ZNode.Libraries.DataAccess.Service.NoteService();
            bool isAdded = noteService.Insert(note);
            return isAdded;
        }

        /// <summary>
        /// Delete the customer notes by account Id.
        /// </summary>
        /// <param name="accountId">Account Id to get the list of Notes for the account.</param>        
        public void DeleteByAccountId(int accountId)
        {
            ZNode.Libraries.DataAccess.Service.NoteService noteService = new ZNode.Libraries.DataAccess.Service.NoteService();

            TList<ZNode.Libraries.DataAccess.Entities.Note> notesList = noteService.GetByAccountID(accountId);

            noteService.Delete(notesList);
        }
    }
}

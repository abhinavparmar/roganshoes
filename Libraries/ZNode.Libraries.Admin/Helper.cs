﻿using System;
using System.IO;
using System.Web;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    public class Helper
    {
        /// <summary>
        /// Get the enable/disable checkmark icon
        /// </summary>
        /// <param name="isEnabled">Indicates to icon enabled or disabled status.</param>
        /// <returns>Returns enabled/disabled checkmark icon.</returns>
        public static string GetCheckMark(bool isEnabled)
        {
            string statusImgPath = string.Empty;
            if (isEnabled == true)
            {
                statusImgPath = "~/SiteAdmin/Themes/Images/206-enable.gif";
            }
            else
            {
                statusImgPath = "~/SiteAdmin/Themes/Images/202-delete.gif";
            }

            return statusImgPath;
        }

        /// <summary>
        /// Log the message in ActivityLog.txt text file.
        /// </summary>
        /// <param name="message">Message to write in text file.</param>
        public void LogMessage(string message)
        {
            string logData = string.Empty;
            string filepath = ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ActivityLog.txt";
            FileInfo file = new FileInfo(HttpContext.Current.Server.MapPath(filepath));

            if (file.Exists)
            {
                TextReader reader = new StreamReader(HttpContext.Current.Server.MapPath(filepath));
                logData = reader.ReadToEnd();
                reader.Close();
            }

            TextWriter writer = new StreamWriter(HttpContext.Current.Server.MapPath(filepath));
            if (!string.IsNullOrEmpty(logData))
            {
                writer.WriteLine(logData);
            }

            writer.WriteLine(new string('*', 50));
            writer.WriteLine("TimeStamp: " + DateTime.Now.ToString());
            writer.WriteLine(message);
            writer.WriteLine(new string('*', 50));
            writer.Close();
        }
    }
}

using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// This Class Manages the related to the store locator
    /// </summary>
    public class StoreLocatorAdmin : ZNodeBusinessBase
    {
        #region Public Methods - Related to Store Locator
        /// <summary>
        /// Update the store
        /// </summary>
        /// <param name="store">Store object to update.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdateStore(Store store)
        {
            ZNode.Libraries.DataAccess.Service.StoreService storeService = new StoreService();
            bool isUpdated = storeService.Update(store);

            return isUpdated;
        }

        /// <summary>
        /// Insert a store Detail
        /// </summary>
        /// <param name="store">Store object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool InsertStore(Store store)
        {
            ZNode.Libraries.DataAccess.Service.StoreService storeService = new StoreService();
            bool isAdded = storeService.Insert(store);

            return isAdded;
        }

        /// <summary>
        /// Delete a store Detail
        /// </summary>
        /// <param name="storeId">Store Id to delete the store object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteStore(int storeId)
        {
            ZNode.Libraries.DataAccess.Service.StoreService storeService = new StoreService();
            bool isDeleted = storeService.Delete(storeId);

            return isDeleted;
        }

        /// <summary>
        /// Get the store detail by Id.
        /// </summary>        
        /// <param name="storeId">Store Id to get the store object.</param>
        /// <returns>Returns the Store object.</returns>
        public Store GetByStoreID(int storeId)
        {
            ZNode.Libraries.DataAccess.Service.StoreService storeService = new StoreService();
            Store store = storeService.GetByStoreID(storeId);

            return store;
        }

        /// <summary>
        /// Get all stores.
        /// </summary>
        /// <returns>Returns list of Store object.</returns>
        public TList<Store> GetAllStore()
        {
            ZNode.Libraries.DataAccess.Service.StoreService storeService = new StoreService();
            TList<Store> storeList = storeService.GetAll();

            return storeList;
        }

        /// <summary>
        /// Search the store for the given input values
        /// </summary>
        /// <param name="storeName">Store name to search.</param>
        /// <param name="zipCode">Ziocode to search.</param>
        /// <param name="city">City name to search.</param>
        /// <param name="state">State to search.</param>
        /// <returns>Returns the search result dataset.</returns>
        public DataSet SearchStore(string storeName, string zipCode, string city, string state)
        {
            StoreLocatorHelper storeLocatorHelper = new StoreLocatorHelper();
            return storeLocatorHelper.SearchStore(storeName, zipCode, city, state);
        }

        #endregion
    }
}

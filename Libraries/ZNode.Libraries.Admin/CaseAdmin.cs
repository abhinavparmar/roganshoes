using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage Case, CasePriority , CaseStatus
    /// </summary>
    public class CaseAdmin : ZNodeBusinessBase
    {
        #region Public Methods

        /// <summary>
        ///  Get all the Cases
        /// </summary>
        /// <returns>Returns all list of CaseRequest object.</returns>   
        public TList<ZNode.Libraries.DataAccess.Entities.CaseRequest> GetAll()
        {
            ZNode.Libraries.DataAccess.Service.CaseRequestService caseService = new ZNode.Libraries.DataAccess.Service.CaseRequestService();
            TList<ZNode.Libraries.DataAccess.Entities.CaseRequest> caseList = caseService.GetAll();

            return caseList;
        }

        /// <summary>
        /// Get a Case
        /// </summary>
        /// <param name="caseId">Case Request Id to get the CaseRequest object.</param>
        /// <returns>Returns the CaseRequest object.</returns>
        public ZNode.Libraries.DataAccess.Entities.CaseRequest GetByCaseID(int caseId)
        {
            ZNode.Libraries.DataAccess.Service.CaseRequestService caseService = new ZNode.Libraries.DataAccess.Service.CaseRequestService();
            ZNode.Libraries.DataAccess.Entities.CaseRequest caseList = caseService.GetByCaseID(caseId);
            return caseList;
        }

        /// <summary>
        /// Get a case for this Account
        /// </summary>
        /// <param name="accountId">Account Id to get the list of case request object.</param>
        /// <returns>Returns the list of CaseRequest object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.CaseRequest> GetByAccountID(int accountId)
        {
            ZNode.Libraries.DataAccess.Service.CaseRequestService caseService = new ZNode.Libraries.DataAccess.Service.CaseRequestService();
            TList<ZNode.Libraries.DataAccess.Entities.CaseRequest> caseList = caseService.GetByAccountID(accountId);
            return caseList;
        }

        /// <summary>
        /// Add New Case 
        /// </summary>
        /// <param name="caseRequest">Case request object to add into data source.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Add(CaseRequest caseRequest)
        {
            ZNode.Libraries.DataAccess.Service.CaseRequestService caseService = new ZNode.Libraries.DataAccess.Service.CaseRequestService();
            return caseService.Insert(caseRequest);
        }

        /// <summary>
        /// Update the case request.
        /// </summary>
        /// <param name="caseRequest">Case request object to update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(CaseRequest caseRequest)
        {
            ZNode.Libraries.DataAccess.Service.CaseRequestService caseService = new ZNode.Libraries.DataAccess.Service.CaseRequestService();
            return caseService.Update(caseRequest);
        }

        /// <summary>
        /// Delete the case request by account Id
        /// </summary>
        /// <param name="accountId">Account Id to delete the case request.</param>        
        public void DeleteByAccountId(int accountId)
        {
            ZNode.Libraries.DataAccess.Service.CaseRequestService caseService = new ZNode.Libraries.DataAccess.Service.CaseRequestService();
            TList<ZNode.Libraries.DataAccess.Entities.CaseRequest> caseList = caseService.GetByAccountID(accountId);
            caseService.Delete(caseList);
        }

        /// <summary>
        /// Returns all AccountList for a portal
        /// </summary>        
        /// <returns>Returns all customer details dataset.</returns>
        public DataSet GetByPortalID()
        {
            AccountAdmin accountAdmin = new AccountAdmin();
            return accountAdmin.GetAllCustomers();
        }

        /// <summary>
        /// Returns Account type for this AccountTypeid
        /// </summary>
        /// <param name="accountTypeId">Account Type Id to get the AccountType object.</param>
        /// <returns>Returns the AccountType object for the account type Id.</returns>
        public AccountType GetByAccountTypeID(int accountTypeId)
        {
            AccountTypeAdmin accountTypeAdmin = new AccountTypeAdmin();
            AccountType accountType = accountTypeAdmin.GetByAccountTypeID(accountTypeId);
            return accountType;
        }

        /// <summary>
        /// Search the case request based on the parameter.
        /// </summary>
        /// <param name="caseStatusId">Case status Id</param>
        /// <param name="caseId">Case request Id to check</param>
        /// <param name="firstName">First name to check</param>
        /// <param name="lastName">Last name to check.</param>
        /// <param name="companyName">Company name to check</param>
        /// <param name="title">Case title to check.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <param name="portalIds">Portal Ids to search</param>
        /// <returns>Returns the dataset that meets the search criteria.</returns>
        public DataSet SearchCase(int caseStatusId, string caseId, string firstName, string lastName, string companyName, string title, int portalId, string portalIds)
        {
            CaseHelper caseHelper = new CaseHelper();
            return caseHelper.SearchCase(caseStatusId, caseId, firstName, lastName, companyName, title, portalId, portalIds);
        }

        #endregion

        #region CasePriority Public Methods

        /// <summary>
        ///  Get all the Case Priority
        /// </summary>
        /// <returns>Returns list of CasePriority object.</returns>   
        public TList<ZNode.Libraries.DataAccess.Entities.CasePriority> GetAllCasePriority()
        {
            ZNode.Libraries.DataAccess.Service.CasePriorityService casePriorityService = new ZNode.Libraries.DataAccess.Service.CasePriorityService();
            TList<ZNode.Libraries.DataAccess.Entities.CasePriority> casepriorityList = casePriorityService.GetAll();

            return casepriorityList;
        }

        /// <summary>
        /// Get a case priority by Id
        /// </summary>
        /// <param name="casePriorityId">Case priority Id to get the object.</param>
        /// <returns>Returns the case priority object.</returns>
        public CasePriority GetByCasePriorityID(int casePriorityId)
        {
            ZNode.Libraries.DataAccess.Service.CasePriorityService casePriorityService = new ZNode.Libraries.DataAccess.Service.CasePriorityService();
            ZNode.Libraries.DataAccess.Entities.CasePriority casePriority = casePriorityService.GetByCasePriorityID(casePriorityId);
            return casePriority;
        }

        #endregion

        #region CaseStatus Public Methods

        /// <summary>
        ///   Get all the Case status
        /// </summary>
        /// <returns>Returns list of CaseStatus object.</returns>   
        public TList<ZNode.Libraries.DataAccess.Entities.CaseStatus> GetAllCaseStatus()
        {
            ZNode.Libraries.DataAccess.Service.CaseStatusService caseStatusService = new ZNode.Libraries.DataAccess.Service.CaseStatusService();
            TList<ZNode.Libraries.DataAccess.Entities.CaseStatus> caseStatusList = caseStatusService.GetAll();
            return caseStatusList;
        }

        /// <summary>
        /// Get a case status by Id
        /// </summary>
        /// <param name="caseStatusId">Case status Id to get the CaseStatus object.</param>
        /// <returns>Returns the CaseStaus object.</returns>
        public ZNode.Libraries.DataAccess.Entities.CaseStatus GetByCaseStatusID(int caseStatusId)
        {
            ZNode.Libraries.DataAccess.Service.CaseStatusService caseStatusService = new ZNode.Libraries.DataAccess.Service.CaseStatusService();
            CaseStatus caseStatus = caseStatusService.GetByCaseStatusID(caseStatusId);
            return caseStatus;
        }
        #endregion
    }
}

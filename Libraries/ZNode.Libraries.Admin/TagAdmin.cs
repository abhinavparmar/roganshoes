using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    public class TagAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Get all tag control type.
        /// </summary>
        /// <returns>Returns list of TagControlType object.</returns>
        public TList<TagControlType> GetAllTagControlType()
        {
            TagControlTypeService tagControlTypeService = new TagControlTypeService();
            TList<TagControlType> tagControlTypeList = tagControlTypeService.GetAll();

            return tagControlTypeList;
        }
         

        /// <summary>
        /// Get a TagGroup for this group type
        /// </summary>
        /// <param name="tagGroupId">Tag group Id to get the tag group object.</param>
        /// <returns>Returns the TagGroup object.</returns>
        public TagGroup GetByTagGroupID(int tagGroupId)
        {
            TagGroupService tagGroupService = new TagGroupService();
            TagGroup tagGroup = tagGroupService.GetByTagGroupID(tagGroupId);
            if (tagGroup != null)
            {
                tagGroup.TagGroupCategoryCollection = new TagGroupCategoryService().GetByTagGroupID(tagGroupId);
            }
            return tagGroup;
        }
         


        /// <summary>
        /// Get a tag by tag Id 
        /// </summary>
        /// <param name="tagId">Tag Id to get the tag object.</param>
        /// <returns>Returns the Tag object.</returns>
        public Tag GetByTagID(int tagId)
        {
            TagService tagService = new TagService();
            Tag tag = tagService.GetByTagID(tagId);

            return tag;
        }

        /// <summary>
        /// Get a tag group by tag group Id.
        /// </summary>
        /// <param name="tagGroupId">Tag group Id to get the list of associated tags.</param>
        /// <returns>Returns list of Tag object associated with the tag group.</returns>
        public TList<Tag> GetTagsByTagGroupID(int tagGroupId)
        {
            TagService tagService = new TagService();
            TList<Tag> TagList = tagService.GetByTagGroupID(tagGroupId);

            return TagList;
        }

        /// <summary>
        /// Add a tag to data soruce.
        /// </summary>
        /// <param name="tag">Tag object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddTag(Tag tag)
        {
            TagService tagService = new TagService();
            bool isAdded = tagService.Insert(tag);

            return isAdded;
        }

        /// <summary>
        /// Update Tag
        /// </summary>
        /// <param name="tag">Tag object to update.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdateTag(Tag tag)
        {
            TagService tagService = new TagService();
            bool isUpdated = tagService.Update(tag);

            return isUpdated;
        }

        /// <summary>
        /// Delete Tag
        /// </summary>
        /// <param name="tag">Tag object to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteTag(Tag tag)
        {
            TagService tagService = new TagService();
            bool isDeleted = tagService.Delete(tag);

            return isDeleted;
        }

        /// <summary>
        /// Add a tag group.
        /// </summary>
        /// <param name="tagGroup">Tag group object to add.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddTagGroup(TagGroup tagGroup)
        {
            TagGroupService tagGroupService = new TagGroupService();

            bool isAdded = tagGroupService.Insert(tagGroup);

            return isAdded;
        }

        /// <summary>
        /// Update tag group.
        /// </summary>
        /// <param name="tagGroup">Tag group object to add.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdateTagGroup(TagGroup tagGroup)
        {
            TagGroupService tagGroupService = new TagGroupService();
            bool isUpdated = tagGroupService.Update(tagGroup);

            return isUpdated;
        }

        /// <summary>
        /// Delete tag group by tag group Id.
        /// </summary>
        /// <param name="tagGroup">Tag group object to add.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteTagGroup(TagGroup tagGroup)
        {
            TagGroupService tagGroupService = new TagGroupService();
            bool isDeleted = tagGroupService.Delete(tagGroup);

            return isDeleted;
        }

        /// <summary>
        /// Add TagGroupCategory
        /// </summary>
        /// <param name="tagGroupCategory">TagGroupCategory object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddTagGroupCategory(TagGroupCategory tagGroupCategory)
        {
            TagGroupCategoryService tagGroupCategoryService = new TagGroupCategoryService();
            bool isAdded = tagGroupCategoryService.Insert(tagGroupCategory);

            return isAdded;
        }

        /// <summary>
        /// Delete the TagGroupCategory by tag group category.
        /// </summary>
        /// <param name="tagGroupCategory">TagGroupCategory object to insert.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteTagGroupCategegory(TagGroupCategory tagGroupCategory)
        {
            TagGroupCategoryService tagGroupCategoryService = new TagGroupCategoryService();
            bool isDeleted = tagGroupCategoryService.Delete(tagGroupCategory);

            return isDeleted;
        }

        /// <summary>
        /// Search the tag
        /// </summary>
        /// <param name="name">Tag name to search.</param>
        /// <param name="localeId">Locale Id to search.</param>
        /// <returns>Returns the tag search result dataset.</returns>
        public DataSet GetTagGroupBySearchData(string name, int localeId)
        {
            TagHelper tagHelper = new TagHelper();
            return tagHelper.GetTagGroupBySearchData(name, localeId);
        }

        /// <summary>
        /// Get locale by associated tags dataset.
        /// </summary>        
        /// <returns>Returns the locale by associated dataset.</returns>
        public DataSet GetLocaleByTagGroup()
        {
            TagHelper tagHelper = new TagHelper();
            return tagHelper.GetLocaleByAssociatedTags();
        }

        /// <summary>
        /// Get categories by TagGroup Id
        /// </summary>
        /// <param name="tagGroupId">Tag group Id to search.</param>
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <param name="isTagGroup">Indicates whether tag group or not.</param>
        /// <returns>Returns the categories dataset.</returns>
        public DataSet GetCategoriesByTagGroupID(int tagGroupId, int catalogId, bool isTagGroup)
        {
            TagHelper tagHelper = new TagHelper();
            return tagHelper.GetCategoriesByTagGroupID(tagGroupId, catalogId, isTagGroup);
        }

        /// <summary>
        /// Get tag product Sku by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the tag product Sku.</param>
        /// <returns>Returns list of TagProductSKU object.</returns>
        public TList<TagProductSKU> GetTagsByProductId(int productId)
        {
            TagProductSKUService service = new TagProductSKUService();
            return service.GetByProductID(productId);
        }
    }
}

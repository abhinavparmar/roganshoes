using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represents the PortalProfileAdmin class.
    /// </summary>
    public class PortalProfileAdmin : ZNodeBusinessBase 
    {
        /// <summary>
        /// Get all portal profile by portal Id
        /// </summary>
        /// <param name="portalId">Portal Id to get the deep loaded portal profile.</param>
        /// <returns>Returns the deep loaded portal profile.</returns>
        public TList<PortalProfile> DeepLoadProfilesByPortalID(int portalId)
        {
            PortalProfileService portalProfileService = new PortalProfileService();
            TList<PortalProfile> profileList = portalProfileService.GetByPortalID(portalId);
            portalProfileService.DeepLoad(profileList, true, DeepLoadType.IncludeChildren, typeof(Profile));
            
            return profileList;
        }

        /// <summary>
        /// Add portal profile details
        /// </summary>
        /// <param name="portalProfile">PortalProfile object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Insert(PortalProfile portalProfile)
        {
            PortalProfileService portalProfileService = new PortalProfileService();
            bool isAdded = portalProfileService.Insert(portalProfile);

            return isAdded;
        }

        /// <summary>
        /// Delete Portal Profile details
        /// </summary>
        /// <param name="portalProfileId">Portal profile Id to delete the PortalProfile object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(int portalProfileId)
        {
            PortalProfileService portalProfileService = new PortalProfileService();
            bool isDeleted = portalProfileService.Delete(portalProfileId);

            return isDeleted;
        }

        /// <summary>
        /// Checks to see if a Profile exists for the given Store.
        /// </summary>
        /// <param name="portalId">Portal Id to check.</param>
        /// <param name="profileId">Profile Id to check.</param>
        /// <returns>Returns true if portal profile exists otherwise false.</returns>
        public bool ProfileExists(int portalId, int profileId)
        {
            PortalProfileService portalProfileService = new PortalProfileService();
            PortalProfileQuery query = new PortalProfileQuery();
            query.Append(PortalProfileColumn.PortalID, portalId.ToString());
            query.Append(PortalProfileColumn.ProfileID, profileId.ToString());
            TList<PortalProfile> portalProfile = portalProfileService.Find(query);
            if (portalProfile.Count > 0)
            {
                return true;
            }

            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represents the UrlRedirectAdmin class.
    /// </summary>
    public partial class UrlRedirectAdmin
    {
        /// <summary>
        /// check if seo url exist in Mnufacturer
        /// </summary>
        /// <param name="seoUrl"></param>
        /// <param name="manufacturerID"></param>
        /// <returns></returns>
        public bool SeoUrlExistsInBrands(string seoUrl)
        {
            ManufacturerExtnService mfrSer = new ManufacturerExtnService();
            bool isSEOExist = false;
            ManufacturerExtnQuery extnQuery = new ManufacturerExtnQuery();
            extnQuery.Append(ManufacturerExtnColumn.SEOURL, seoUrl);
            TList<ManufacturerExtn> manufacturerList = mfrSer.Find(extnQuery.GetParameters());
            manufacturerList= manufacturerList.FindAll(x => x.SEOURL.ToLower() == seoUrl.ToLower());
            if (manufacturerList != null && manufacturerList.Count > 0)
            {
                isSEOExist = true;
            }
            return isSEOExist;
            
        }
    }
}

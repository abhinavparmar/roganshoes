﻿using System;
using System.Text;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage Locale
    /// </summary>
    public class LocaleAdmin : ZNodeBusinessBase
    {
        #region Public Methods

        /// <summary>
        /// Get all Locale
        /// </summary>
        /// <returns>Returns list of Locale object.</returns>
        public TList<Locale> GetAllLocale()
        {
            LocaleService localeService = new LocaleService();
            TList<Locale> localeList = localeService.GetAll();

            return localeList;
        }

        /// <summary>
        /// Get the locale list by portal
        /// </summary>        
        /// <returns>Returns list of Locale object by Portal Id</returns>
        public TList<Locale> GetLocaleByPortalID()
        {
            // Set CultureValue in Profile               
            return this.GetLocaleByPortalID(ZNodeConfigManager.SiteConfig.PortalID);
        }

        /// <summary>
        /// Return the locale list by portal
        /// </summary>        
        /// <param name="portalId">Portal Id to get the Locale list.</param>
        /// <returns>Returns list of Locale object.</returns>
        public TList<Locale> GetLocaleByPortalID(int portalId)
        {
            // Set CultureValue in Profile               
            PortalCatalogService portalCatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalCatalogList = portalCatalogService.Find("PortalID = " + portalId.ToString());

            StringBuilder localeId = new StringBuilder();

            if (portalCatalogList.Count > 0)
            {
                foreach (PortalCatalog catalog in portalCatalogList)
                {
                    localeId.Append(catalog.LocaleID.ToString());
                    localeId.Append(",");
                }

                localeId = localeId.Remove(localeId.Length - 1, 1);

                LocaleService localeService = new LocaleService();
                LocaleQuery filter = new LocaleQuery();

                filter.AppendInQuery(LocaleColumn.LocaleID, localeId.ToString());

                TList<Locale> localeList = localeService.Find(filter.GetParameters());

                if (localeList.Count > 0)
                {
                    return localeList;
                }
            }

            return null;
        }
        
        /// <summary>
        /// Return the locale list by portal catalog.
        /// </summary>        
        /// <returns>Returns list of Locale by portal catalog.</returns>
        public TList<Locale> GetAllLocaleByPortalCatalog()
        {
            // Set CultureValue in Profile               
            PortalCatalogService portalCatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalCatalogList = portalCatalogService.GetAll();

            StringBuilder catalogLocaleId = new StringBuilder();

            if (portalCatalogList.Count > 0)
            {
                foreach (PortalCatalog catalog in portalCatalogList)
                {
                    catalogLocaleId.Append(catalog.LocaleID.ToString());
                    catalogLocaleId.Append(",");
                }

                catalogLocaleId = catalogLocaleId.Remove(catalogLocaleId.Length - 1, 1);

                LocaleService localeService = new LocaleService();
                LocaleQuery filter = new LocaleQuery();

                filter.AppendInQuery(LocaleColumn.LocaleID, catalogLocaleId.ToString());

                TList<Locale> localeList = localeService.Find(filter.GetParameters());

                if (localeList.Count > 0)
                {
                    return localeList;
                }
            }

            return null;
        }
        
        /// <summary>
        /// Returns the locale name for the locale Id.
        /// </summary>
        /// <param name="localeId">Locale Id to get local name.</param>
        /// <returns>Returns the locale name for locale Id.</returns>
        public string GetLocaleNameByLocaleID(string localeId)
        {
            LocaleService localService = new LocaleService();
            Locale locale = localService.GetByLocaleID(Convert.ToInt32(localeId));

            if (locale != null)
            {
                return locale.LocaleDescription;
            }

            return string.Empty;
        }
        #endregion
    }
}

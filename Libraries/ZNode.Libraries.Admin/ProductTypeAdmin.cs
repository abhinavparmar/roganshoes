using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage producttypes
    /// </summary>
    public class ProductTypeAdmin : ZNodeBusinessBase
    {
        #region Public Methods

        /// <summary>
        /// Get all product types. 
        /// </summary>
        /// <returns>Returns list of ProductType object.</returns>
        public TList<ProductType> GetAllProductTypes()
        {
            ZNode.Libraries.DataAccess.Service.ProductTypeService productTypeService = new ZNode.Libraries.DataAccess.Service.ProductTypeService();
            TList<ZNode.Libraries.DataAccess.Entities.ProductType> ProductTypeList = productTypeService.GetAll();

            return ProductTypeList;
        }

        /// <summary>
        /// Get all product types dataset.
        /// </summary>        
        /// <returns>Returns the produt type dataset.</returns>
        public DataSet GetAllProductType()
        {
            ZNode.Libraries.DataAccess.Custom.ProductTypeHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductTypeHelper();
            DataSet productTypeDataSet = productHelper.GetAllProductType();

            return productTypeDataSet;
        }

        /// <summary>
        /// Get a product type by product type Id.
        /// </summary>
        /// <param name="productTypeId">Product type Id to get the product type object.</param>
        /// <returns>Returns the ProductType object.</returns>
        public ProductType GetByProdTypeId(int productTypeId)
        {
            ZNode.Libraries.DataAccess.Service.ProductTypeService productTypeService = new ZNode.Libraries.DataAccess.Service.ProductTypeService();
            ProductType productType = productTypeService.GetByProductTypeId(productTypeId);

            return productType;
        }

        /// <summary>
        /// Add a new product type.
        /// </summary>
        /// <param name="productType">ProductType object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Add(ProductType productType)
        {
            ZNode.Libraries.DataAccess.Service.ProductTypeService productTypeService = new ZNode.Libraries.DataAccess.Service.ProductTypeService();
            return productTypeService.Insert(productType);
        }

        /// <summary>
        /// Update a productType
        /// </summary>
        /// <param name="productType">ProductType object to udpate.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool Update(ProductType productType)
        {
            ZNode.Libraries.DataAccess.Service.ProductTypeService productTypeService = new ZNode.Libraries.DataAccess.Service.ProductTypeService();

            return productTypeService.Update(productType);
        }

        /// <summary>
        /// Delete a productType
        /// </summary>
        /// <param name="productType">ProductType object to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(ProductType productType)
        {
            ZNode.Libraries.DataAccess.Service.ProductTypeService productTypeService = new ZNode.Libraries.DataAccess.Service.ProductTypeService();

            return productTypeService.Delete(productType);
        }

        /// <summary>
        /// Gets attribute types by product type Id.
        /// </summary>
        /// <param name="productTypeId">Product type Id to get the attribute type dataset.</param>
        /// <returns>Returns the attribute type dataset.</returns>
        public DataSet GetAttributeNamesByProductTypeid(int productTypeId)
        {
            AttributeTypeHelper attributeTypeHelper = new AttributeTypeHelper();
            DataSet attributeTypeDataSet = attributeTypeHelper.GetByProductTypeID(productTypeId);

            return attributeTypeDataSet;
        }

        /// <summary>
        /// Search the product type.
        /// </summary>
        /// <param name="name">Product type name to search.</param>
        /// <param name="description">Product type description to search.</param>
        /// <returns>Returns the product type search result dataset.</returns>
        public DataSet GetProductTypeBySearchData(string name, string description)
        {
            ProductTypeHelper attributeTypeHelper = new ProductTypeHelper();
            return attributeTypeHelper.GetProductTypeBySearchData(name, description);
        }

        #endregion
    }
}

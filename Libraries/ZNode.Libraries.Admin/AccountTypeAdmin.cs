using System;
using System.Collections.Generic;
using System.Text;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage Account Types
    /// </summary>
    public class AccountTypeAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Returns a AccounType for an Account 
        /// </summary>
        /// <param name="accountTypeId">Account type Id to get the AccountType</param>
        /// <returns>Returns the AccountType object</returns>
        public AccountType GetByAccountTypeID(int accountTypeId)
        {
            ZNode.Libraries.DataAccess.Service.AccountTypeService accountTypeService = new AccountTypeService();
            ZNode.Libraries.DataAccess.Entities.AccountType accountTypeList = accountTypeService.GetByAccountTypeID(accountTypeId);

            return accountTypeList;
        }

        /// <summary>
        /// Returms all the Account Types
        /// </summary>
        /// <returns>Returns list of AccountType object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.AccountType> GetAll()
        {
            ZNode.Libraries.DataAccess.Service.AccountTypeService accountTypeService = new AccountTypeService();
            TList<ZNode.Libraries.DataAccess.Entities.AccountType> accountTypeList = accountTypeService.GetAll();
            return accountTypeList;
        }
    }
}

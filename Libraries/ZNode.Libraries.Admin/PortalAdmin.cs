using System;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represents the PortalAdmin class.
    /// </summary>
    public class PortalAdmin : ZNodeBusinessBase
    {
        #region Related to ZNode Portal Table

        /// <summary>
        ///  Get all portals
        /// </summary>
        /// <returns>Returns list of Portal object.</returns>
        public TList<Portal> GetAllPortals()
        {
            ZNode.Libraries.DataAccess.Service.PortalService portalService = new ZNode.Libraries.DataAccess.Service.PortalService();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> PortalList = portalService.GetAll();

            return PortalList;
        }

        /// <summary>
        ///  Get a portal by portal Id
        /// </summary>
        /// <param name="portalId">Portal Id to get portal object.</param>
        /// <returns>Returns the Portal object for the portal Id.</returns>
        public Portal GetByPortalId(int portalId)
        {
            ZNode.Libraries.DataAccess.Service.PortalService portalService = new ZNode.Libraries.DataAccess.Service.PortalService();
            Portal portal = portalService.GetByPortalID(portalId);

            return portal;
        }

        /// <summary>
        /// Get the store name for the comma seperated portal Id.
        /// </summary>
        /// <param name="PortalIds">Comma seperated portal Ids.</param>
        /// <returns>Returns comma seperated portal name.</returns>
        public string GetStoreNameByPortalIDs(string PortalIds)
        {
            // PortalIds which is associated to the User.
            string[] portalIds = PortalIds.Split(',');
            string storeName = string.Empty;

            foreach (string portalId in portalIds)
            {
                Portal portal = this.GetByPortalId(Convert.ToInt32(portalId));
                storeName += portal.StoreName + ",";
            }

            // Remove the comma at the end of the string.
            storeName = storeName.Remove(storeName.Length - 1);

            return storeName;
        }

        /// <summary>
        /// Get the store name for the portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the store name.</param>
        /// <returns>Returns the store name for the portal Id.</returns>
        public string GetStoreNameByPortalID(string portalId)
        {
            Portal portal = this.GetByPortalId(Convert.ToInt32(portalId));

            if (portal != null)
            {
                return portal.StoreName;
            }

            return string.Empty;
        }

        #endregion

        #region Locale

        /// <summary>
        /// Get Language list associated with the given portal.
        /// </summary>
        /// <param name="portalId">Portal Id to get the associated locales.</param>
        /// <returns>Returns list of Locale object associated with the portal.</returns>
        public TList<Locale> GetLocaleListByPortalID(int portalId)
        {
            LocaleService localeService = new LocaleService();
            LocaleQuery localeQuery = new LocaleQuery();
            localeQuery.AppendInQuery("LocaleId", "SELECT LocaleId FROM ZNodePortalCatalog WHERE PortalId = " + portalId);

            TList<Locale> listLocale = localeService.Find(localeQuery.GetParameters());

            return listLocale;
        }

        /// <summary>
        /// Delete the portal catalog by portal Id and locale Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the portal catalog.</param>
        /// <param name="localeId">Locale Id to get the portal catalog.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool DeletePortalCatalog(int portalId, int localeId)
        {
            bool isDeleted = false;

            // Delete portal Catalog
            PortalCatalogService portalCatalogService = new PortalCatalogService();
            PortalCatalogQuery portalCatalogQuery = new PortalCatalogQuery();

            portalCatalogQuery.Append(PortalCatalogColumn.PortalID, portalId.ToString());
            portalCatalogQuery.Append(PortalCatalogColumn.LocaleID, localeId.ToString());

            TList<PortalCatalog> portalCatalogList = portalCatalogService.Find(portalCatalogQuery.GetParameters());

            if (portalCatalogList.Count > 0)
            {
                foreach (PortalCatalog portalCatalog in portalCatalogList)
                {
                    // Delete the portal Catalog
                    isDeleted = portalCatalogService.Delete(portalCatalog);
                }
            }

            // Delete Content Page
            ContentPageService contentPageService = new ContentPageService();
            ContentPageQuery contentPageQuery = new ContentPageQuery();
            ContentPageAdmin contentPageAdmin = new ContentPageAdmin();

            contentPageQuery.Append(ContentPageColumn.PortalID, portalId.ToString());
            contentPageQuery.Append(ContentPageColumn.LocaleId, localeId.ToString());

            // Get the content page list
            TList<ContentPage> ContentPageList = contentPageService.Find(contentPageQuery.GetParameters());

            if (ContentPageList.Count > 0)
            {
                foreach (ContentPage contentPage in ContentPageList)
                {
                    // Delete the content page.
                    isDeleted = contentPageAdmin.DeletePage(contentPage);
                }
            }

            if (isDeleted)
            {
                // Delete Custom messages
                StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
                settingsAdmin.DeleteCustomMessage(portalId.ToString(), localeId.ToString());
            }

            return isDeleted;
        }
        #endregion
    }
}

using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage User profiles
    /// </summary>
    public class ProfileAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Get the profiles which are associated with the portal
        /// </summary>
        /// <param name="portalId">Portal Id to get the portal associated profiles.</param>
        /// <returns>Returns the profiles dataset associated with the store.</returns>
        public DataSet GetProfilesByPortalID(int portalId)
        {
            ProfileHelper profileHelper = new ProfileHelper();
            DataSet profilesDataSet = profileHelper.GetProfileByPortalID(portalId);

            return profilesDataSet;
        }

        /// <summary>
        /// Get the profile associated with portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the associated profile.</param>
        /// <returns>Returns the profiiles dataset.</returns>
        public DataSet GetAssociatedProfilesByPortalID(int portalId)
        {
            ProfileHelper profileHelper = new ProfileHelper();
            DataSet profilesDataSet = profileHelper.GetAssociatedProfilesByPortalID(portalId);

            return profilesDataSet;
        }

        /// <summary>
        /// Get all the profiles
        /// </summary>
        /// <returns>Returns list of Profile object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.Profile> GetAll()
        {
            ZNode.Libraries.DataAccess.Service.ProfileService profileService = new ProfileService();
            TList<ZNode.Libraries.DataAccess.Entities.Profile> profileList = profileService.GetAll();

            return profileList;
        }

        /// <summary>
        /// Get all the profiles for a store.
        /// </summary>
        /// <param name="portalId">Protal Id to get the profiles.</param>
        /// <returns>Returns list of Profile object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.Profile> GetByPortalId(int portalId)
        {
            ZNode.Libraries.DataAccess.Service.ProfileService profileService = new ProfileService();
            TList<ZNode.Libraries.DataAccess.Entities.Profile> profileList = profileService.Find("PortalId=" + portalId.ToString());

            return profileList;
        }

        /// <summary>
        /// Get a profile by profile Id.
        /// </summary>
        /// <param name="profileId">Profile Id to get the profile object.</param>
        /// <returns>Returns the Profile object.</returns>
        public Profile GetByProfileID(int profileId)
        {
            ZNode.Libraries.DataAccess.Service.ProfileService profileService = new ProfileService();
            ZNode.Libraries.DataAccess.Entities.Profile profile = profileService.GetByProfileID(profileId);

            return profile;
        }

        /// <summary>
        /// Get the default profile for a store.
        /// </summary>
        /// <param name="portalId">Portal Id to get the default registered profile.</param>
        /// <returns>Returns the default registered Profile object.</returns>
        public Profile GetDefaultProfileByPortalID(int portalId)
        {            
            PortalService portalService = new PortalService();
            Portal currentPortal = portalService.GetByPortalID(portalId);

            // Get default registered profile
            ProfileService ProfileService = new ProfileService();
            Profile profile = ProfileService.GetByProfileID(currentPortal.DefaultRegisteredProfileID.GetValueOrDefault());

            return profile;
        }

        /// <summary>
        /// Add a profile
        /// </summary>
        /// <param name="profile">Profile object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Add(Profile profile)
        {
            ZNode.Libraries.DataAccess.Service.ProfileService profileService = new ProfileService();
            bool isAdded = profileService.Insert(profile);

            return isAdded;
        }

        /// <summary>
        /// Update the profile
        /// </summary>
        /// <param name="profile">Profile object to update.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool Update(Profile profile)
        {
            ZNode.Libraries.DataAccess.Service.ProfileService profileService = new ProfileService();
            bool isUpdated = profileService.Update(profile);

            return isUpdated;
        }

        /// <summary>
        /// Delete a profile
        /// </summary>
        /// <param name="profile">Profile object to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(Profile profile)
        {
            try
            {
                ZNode.Libraries.DataAccess.Service.ProfileService profileService = new ProfileService();
                return profileService.Delete(profile);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                return false;
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                return false;
            }
        }      

        /// <summary>
        /// Get profile name by profile Id
        /// </summary>
        /// <param name="profileId">Profile Id to get the profile object.</param>
        /// <returns>Returns the profile name.</returns>
        public string GetProfileNameByProfileID(int profileId)
        {
            ProfileService profileService = new ProfileService();
            Profile profile = profileService.GetByProfileID(profileId);

            return profile.Name;
        }

        /// <summary>
        /// Updates all other profiles in the database to set "Not default"
        /// </summary>
        /// <param name="profileService">ProfileService object to get and update the profile object.</param>
        /// <param name="profileId">Profile Id to compare.</param>
        protected void ResetProfileSetting(ProfileService profileService, int profileId)
        {
            foreach (Profile profile in profileService.GetAll())
            {
                if (profile.ProfileID != profileId)
                {
                    profileService.Update(profile);
                }
                else if (profile.ProfileID != profileId)
                {
                    profileService.Update(profile);
                }
                else if (profile.ProfileID != profileId)
                {
                    profileService.Update(profile);
                }
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage Product categories
    /// </summary>
    public class CategoryAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Returns a category hierarchical path (upto 3 levels)
        /// </summary>
        /// <param name="categoryName">Category name</param>
        /// <param name="parentCategoryName1">Parent category level1.</param>
        /// <param name="parentCategoryName2">Parent category level2.</param>
        /// <returns>Returns the category path.</returns>
        public static string GetCategoryPath(string categoryName, string parentCategoryName1, string parentCategoryName2)
        {
            System.Text.StringBuilder categoryPath = new StringBuilder();

            if (parentCategoryName2.Trim().Length > 0)
            {
                categoryPath.Append(parentCategoryName2);
                categoryPath.Append(" > ");
            }

            if (parentCategoryName1.Trim().Length > 0)
            {
                categoryPath.Append(parentCategoryName1);
                categoryPath.Append(" > ");
            }

            categoryPath.Append(categoryName);
            return categoryPath.ToString();
        }

        /// <summary>
        /// Get all categories for a portal
        /// </summary>
        /// <returns>Returns list of categories</returns>
        public TList<Category> GetAllCategories()
        {
            ZNode.Libraries.DataAccess.Service.CategoryService categoryService = new ZNode.Libraries.DataAccess.Service.CategoryService();
            TList<ZNode.Libraries.DataAccess.Entities.Category> categoryList = categoryService.GetAll();
            foreach (var item in categoryList)
            {
                item.Name = HttpUtility.HtmlDecode(item.Name);
            }
            return categoryList;
        }

        /// <summary>
        /// Get acategory by category Id
        /// </summary>
        /// <param name="categoryId">Category Id to get the Category object.</param>
        /// <returns>Returns the Category object</returns>
        public Category GetByCategoryId(int categoryId)
        {
            ZNode.Libraries.DataAccess.Service.CategoryService categoryService = new ZNode.Libraries.DataAccess.Service.CategoryService();
            Category category = categoryService.GetByCategoryID(categoryId);
            return category;
        }

        /// <summary>
        /// Add a new product category to data source.
        /// </summary>
        /// <param name="category">Category object to add.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Add(Category category)
        {
            // Clear all cache memory.
            CacheManager.Clear();

            ZNode.Libraries.DataAccess.Service.CategoryService categoryService = new ZNode.Libraries.DataAccess.Service.CategoryService();
            return categoryService.Insert(category);
        }

        /// <summary>
        /// Update a product category
        /// </summary>
        /// <param name="category">Category object to update.</param>
        /// <returns>Returns true if udpated else false.</returns>
        public bool Update(Category category)
        {
            // Clear all cache memory.
            CacheManager.Clear();

            ZNode.Libraries.DataAccess.Service.CategoryService categoryService = new ZNode.Libraries.DataAccess.Service.CategoryService();
            return categoryService.Update(category);
        }

        /// <summary>
        /// Delete a product category
        /// </summary>
        /// <param name="category">Category object to delete.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool Delete(Category category)
        {
            // Clear all cache memory.
            CacheManager.Clear();

            ZNode.Libraries.DataAccess.Service.CategoryService categoryService = new ZNode.Libraries.DataAccess.Service.CategoryService();
            return categoryService.Delete(category);
        }       

        /// <summary>
        /// Returns categories search data
        /// </summary>
        /// <param name="name">Category name to search</param>
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <returns>Returns the category search result dataset.</returns>
        public DataSet GetCategoriesBySearchData(string name, string catalogId)
        {
            ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();
            return categoryHelper.GetCategoriesBySearchData(name, catalogId);
        }

        /// <summary>
        /// Get categories based on search criteria.
        /// </summary>
        /// <param name="departmentName">Category name to search in Name and title column.</param>        
        /// <returns>Returns the list of category object.</returns>
        public TList<Category> GetCategoriesBySearchData(string departmentName)
        {
            CategoryService categoryService = new CategoryService();
            CategoryQuery query = new CategoryQuery();
            query.AppendContains(CategoryColumn.Name, departmentName);
            query.AppendContains(CategoryColumn.Title, departmentName);
            TList<Category> categoryList = categoryService.Find(query.GetParameters());

            return categoryList;
        }

        /// <summary>
        /// Returns categories search data
        /// </summary>
        /// <param name="name">Category name to search.</param>
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <param name="isAssigned">Indicates whether the category is assigned or not.</param>
        /// <returns>Returns the category search result dataset.</returns>
        public DataSet GetCategoriesBySearchData(string name, string catalogId, bool isAssigned)
        {
            ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();
            return categoryHelper.GetCategoriesBySearchData(name, catalogId, isAssigned);
        }

        /// <summary>
        /// Returns category search data
        /// </summary>
        /// <param name="name">Category name to search.</param>
        /// <param name="catalogId">Catalog Id to search.s</param>
        /// <returns>Returns the category search result dataset.</returns>
        public DataSet GetCategoriesSearchByCatalog(string name, int catalogId)
        {
            ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();
            return categoryHelper.GetCategoriesSearchByCatalog(name, catalogId);
        }

        /// <summary>
        /// Returns a category by categoryid
        /// </summary>
        /// <param name="categoryNodeId">Category node Id to get CategoryNode object</param>
        /// <returns>Returns the CategoryNode object for the categoryNodeId.</returns>
        public CategoryNode GetByCategoryNodeId(int categoryNodeId)
        {
            ZNode.Libraries.DataAccess.Service.CategoryNodeService categoryService = new ZNode.Libraries.DataAccess.Service.CategoryNodeService();
            CategoryNode categoryNode = categoryService.GetByCategoryNodeID(categoryNodeId);

            return categoryNode;
        }

        #region MultiStore Methods

        /// <summary>
        /// Add a new category Node
        /// </summary>
        /// <param name="categoryNode">Category node to add.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool AddNode(CategoryNode categoryNode)
        {
            // Clear all cache memory.
            CacheManager.Clear();

            CategoryHelper categoryHelper = new CategoryHelper();

            if (!categoryHelper.IsCategoryNodeExists(categoryNode.CatalogID.Value, categoryNode.CategoryID))
            {
                ZNode.Libraries.DataAccess.Service.CategoryNodeService categoryServ = new ZNode.Libraries.DataAccess.Service.CategoryNodeService();
                return categoryServ.Insert(categoryNode);
            }

            return false;
        }

        /// <summary>
        /// Check whether the child department exist or not.
        /// </summary>
        /// <param name="categoryId">category Id to check</param>
        /// <returns>Returns True if child node exist else False.</returns>
        public bool IsChildCategoryExist(int categoryId)
        {
            bool isExist = false;
            ZNode.Libraries.DataAccess.Service.CategoryNodeService categoryServ = new ZNode.Libraries.DataAccess.Service.CategoryNodeService();
            CategoryNodeQuery query = new CategoryNodeQuery();

            // Get list of child nodes with the given category Id
            query.Append(CategoryNodeColumn.ParentCategoryNodeID, categoryId.ToString());

            TList<CategoryNode> categoryNodeList = categoryServ.Find(query.GetParameters());
            if (categoryNodeList != null && categoryNodeList.Count > 0)
            {
                // Child nodes exist
                isExist = true;
            }

            return isExist;
        }

        /// <summary>
        /// Update the category node
        /// </summary>
        /// <param name="categoryNode">Category node to update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool UpdateNode(CategoryNode categoryNode)
        {
            // Clear all cache memory.
            CacheManager.Clear();

            CategoryHelper categoryHelper = new CategoryHelper();
            if (!categoryHelper.IsCategoryNodeExists(categoryNode.CatalogID.Value, categoryNode.CategoryID, categoryNode.CategoryNodeID))
            {
                ZNode.Libraries.DataAccess.Service.CategoryNodeService categoryServ = new ZNode.Libraries.DataAccess.Service.CategoryNodeService();
                return categoryServ.Update(categoryNode);
            }

            return false;
        }

        /// <summary>
        /// Get the CategoryNode detail dataset
        /// </summary>      
        /// <param name="categoryId">Category Id to get the category node details.</param>
        /// <returns>Returns the category node dataset.</returns>
        public DataSet GetNodeCategory(int categoryId)
        {
            ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();
            return categoryHelper.GetCategoryNodeDetail(categoryId);
        }

        /// <summary>
        /// Get all the catalogs
        /// </summary>
        /// <returns>Returns list of Catalog object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.Catalog> GetAllCatalog()
        {
            ZNode.Libraries.DataAccess.Service.CatalogService catalogService = new CatalogService();
            TList<ZNode.Libraries.DataAccess.Entities.Catalog> catalogList = catalogService.GetAll();

            return catalogList;
        }

        /// <summary>
        /// Get the Category detail
        /// </summary>
        /// <param name="categoryId">Category Id to get the category details.</param>
        /// <param name="catalogId">Catalog Id to get the category details.</param>
        /// <returns>Returns the Category detail dataset.</returns>
        public DataSet GetCategoryData(int categoryId, int catalogId)
        {
            ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();
            return categoryHelper.GetCategoryDetail(categoryId, catalogId);
        }

        /// <summary>
        /// Get categoryNode list by category Id
        /// </summary>
        /// <param name="categoryId">Category Id to get the category node list.</param>
        /// <returns>Returns list of CategoryNode object</returns>
        public TList<CategoryNode> GetCategoryNodeId(int categoryId)
        {
            ZNode.Libraries.DataAccess.Service.CategoryNodeService categoryServ = new ZNode.Libraries.DataAccess.Service.CategoryNodeService();
            TList<CategoryNode> categoryList = categoryServ.GetByCategoryID(categoryId);
            return categoryList;
        }

        /// <summary>
        /// Delete a product category and its node details
        /// </summary>
        /// <param name="categoryNode">Category node to delete.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool DeleteNode(CategoryNode categoryNode)
        {
            // Clear all cache memory.
            CacheManager.Clear();

            CategoryHelper categoryHelper = new CategoryHelper();
            return categoryHelper.DeleteCategoryNodeByID(categoryNode.CategoryNodeID);
        }

        /// <summary>
        /// Delete all the nodes available for the CategoryID
        /// </summary>
        /// <param name="categoryId">Category Id to list of category node to delete.</param>        
        public void DeleteNodesByCategoryID(int categoryId)
        {
            ZNode.Libraries.DataAccess.Service.CategoryNodeService service = new CategoryNodeService();
            TList<CategoryNode> list = service.GetByCategoryID(categoryId);
            service.Delete(list);
        }

        /// <summary>
        /// Returns a bread crumb path from a delimited string
        /// </summary>
        /// <param name="delimitedString">Delimitted string</param>
        /// <param name="separator">Path seperator.</param>
        /// <returns>Returns the parsed category path.</returns>
        public string ParsePath(string delimitedString, string separator)
        {
            StringBuilder categoryPath = new StringBuilder();

            string[] delim1 = { "||" };
            string[] pathItems = delimitedString.Split(delim1, StringSplitOptions.None);

            foreach (string pathItem in pathItems)
            {
                string[] delim2 = { "%%" };
                string[] items = pathItem.Split(delim2, StringSplitOptions.None);

                string categoryId = items.GetValue(0).ToString();
                string categoryName = items.GetValue(1).ToString();
                categoryPath.Append(categoryName);
                categoryPath.Append(string.Format(" {0} ", separator));
            }

            if (categoryPath.Length > 0)
            {
                if (categoryPath.ToString().LastIndexOf(string.Format(" {0} ", separator)) == categoryPath.ToString().Length - 3)
                {
                    categoryPath = categoryPath.Remove(categoryPath.ToString().Length - 3, 3);
                }
            }

            return categoryPath.ToString();
        }


        /// <summary>
        /// Get Category Root Items
        /// </summary>
        /// <param name="portalId">Portal Id to get category root item.</param>
        /// <param name="catalogId">Catalog Id to get category root item.</param>
        /// <returns>Returns the root category items for the portal catalog.</returns>
        public DataSet GetRootCategoryItems(int portalId, int catalogId)
        {
            CategoryHelper categoryHelper = new CategoryHelper();
            return categoryHelper.GetRootCategoryItems(portalId, catalogId);
        }



        #endregion

        #region Category Profile

        /// <summary>
        /// Get all the sku profile by skuid. 
        /// </summary>
        /// <returns>Returns the sku dataset.</returns>
        public DataSet GetCategoryProfileByCategoryID(int categoryId)
        {
            CategoryHelper helper = new CategoryHelper();
            DataSet skuDataSet = helper.GetCategoryProfileByCategoryID(categoryId);

            return skuDataSet;
        }


        #endregion
    }
}

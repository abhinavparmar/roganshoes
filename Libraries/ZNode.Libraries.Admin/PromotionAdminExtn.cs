﻿using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    public partial class PromotionAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Add Details in promotionextn table.
        /// </summary>
        /// <param name="promotion">Promotion object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddPromotionExtn(PromotionExtn promotionExt)
        {
            PromotionExtnService promotionExtnService = new PromotionExtnService();
            return promotionExtnService.Insert(promotionExt);
        }

        /// <summary>
        /// Update the promotionextn details.
        /// </summary>
        /// <param name="promotion">Promotion object to insert.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdatePromotionExtn(PromotionExtn promotionextn)
        {
            PromotionExtnService promotionExtnService = new PromotionExtnService();
            return promotionExtnService.Update(promotionextn);
        }

        /// <summary>
        /// Get the PromotionExtn by promotion Id
        /// </summary>
        /// <param name="promotionId">Promotion Id to get the promotion object.</param>
        /// <returns>Returns the Promotion object.</returns>
        public PromotionExtn GetPromotionExtnByPromotionId(int promotionId)
        {
            PromotionExtn promotionExtn = null;
            PromotionExtnService promotionService = new PromotionExtnService();
            TList<PromotionExtn> promotionExtnList = promotionService.GetByPromotionID(promotionId);
            if (promotionExtnList != null && promotionExtnList.Count > 0)
            {
                promotionExtn = new PromotionExtn();
                promotionExtn = promotionExtnList[0];
            }
            return promotionExtn;
        }


    }
}

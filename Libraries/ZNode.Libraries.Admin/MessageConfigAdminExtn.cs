﻿using System;
using System.Web;
using System.Web.Caching;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using System.Data.Linq;
using System.Linq;
using System.Configuration;
using ZNode.Libraries.ECommerce.Catalog;
namespace ZNode.Libraries.Admin
{
    public partial class MessageConfigAdmin : ZNodeBusinessBase
    {
        readonly string CacheKey_GetByKeyPortalIDLocaleIDMessageTypeID = "MessageConfigCache";

        /// <summary>
        /// Load Message Config Data In Cache
        /// </summary>
        /// <returns></returns>
        public TList<MessageConfig> LoadMessageConfigDataInCache()
        {
            TList<MessageConfig> messageConfigList = GetAllMessagesConfigDataByPortalIdLocaleId(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
            if (messageConfigList != null)
            {
                ZNodeCacheDependencyManager.Insert(CacheKey_GetByKeyPortalIDLocaleIDMessageTypeID + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId,
                                    messageConfigList, System.DateTime.UtcNow.AddHours(24), System.Web.Caching.Cache.NoSlidingExpiration, null);
            }
            return messageConfigList;
        }


        /// <summary>
        /// Get All From Cache
        /// </summary>
        /// <returns></returns>
        public TList<MessageConfig> GetAllFromCache()
        {
            var items = System.Web.HttpContext.Current.Cache[CacheKey_GetByKeyPortalIDLocaleIDMessageTypeID + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId] as TList<MessageConfig>;
            if (null == items)
            {
                items = this.LoadMessageConfigDataInCache();
            }
            return items;
        }

        /// <summary>
        /// Get By Key PortalID LocaleID MessageTypeID
        /// </summary>
        /// <param name="key"></param>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <param name="messageTypeId"></param>
        /// <returns></returns>
        public TList<MessageConfig> GetByKeyPortalIDLocaleIDMessageTypeID(string key, int portalId, int? localeId, int messageTypeId)
        {
            bool cacheMessageConfig = ConfigurationManager.AppSettings["EnableCacheMessageConfig"] != null ? bool.Parse(ConfigurationManager.AppSettings["EnableCacheMessageConfig"].ToString()) : false;
            if (cacheMessageConfig)
            {
                TList<MessageConfig> data = GetAllFromCache();
                var items = data.Where(x => x.Key == key && x.PortalID == portalId && x.LocaleID == localeId && x.MessageTypeID == messageTypeId).ToArray();
                TList<MessageConfig> msgConfig = new TList<MessageConfig>(items);
                if (msgConfig.Count > 0)
                {
                    return new TList<MessageConfig>(items);
                }
                else
                {
                    ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
                    TList<ZNode.Libraries.DataAccess.Entities.MessageConfig> messageConfigList = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID(key, portalId, localeId, messageTypeId);
                    return messageConfigList;
                }

            }
            else
            {
                ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
                TList<ZNode.Libraries.DataAccess.Entities.MessageConfig> messageConfigList = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID(key, portalId, localeId, messageTypeId);
                return messageConfigList;
            }
        }

        /// <summary>
        /// Get By Key PortalID LocaleID MessageTypeID for admin
        /// </summary>
        /// <param name="key"></param>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <param name="messageTypeId"></param>
        /// <returns></returns>
        public MessageConfig GetByKeyPortalIDLocaleIDMessageTypeIDByAdmin(string key, int portalId, int? localeId, int messageTypeId)
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            TList<ZNode.Libraries.DataAccess.Entities.MessageConfig> messageConfigList = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID(key, portalId, localeId, messageTypeId);

            if (messageConfigList != null && messageConfigList.Count > 0)
            {
                return messageConfigList[0];
            }
            else
            {
                return null;
            }           
        }

        /// <summary>
        /// Clear Message Config Cache
        /// </summary>
        public void ClearMessageConfigCache()
        {
            ZNodeCacheDependencyManager.Remove(CacheKey_GetByKeyPortalIDLocaleIDMessageTypeID);
        }


        /// <summary>
        /// Get all MessageConfig for a portal
        /// </summary>
        /// <param name="portalId">Portal Id to get the message config.</param>
        /// <param name="localeId">Locale Id to get the message config.</param>
        public TList<MessageConfig> GetAllMessagesConfigDataByPortalIdLocaleId(int portalId, int localeId)
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            MessageConfigQuery filters = new MessageConfigQuery();

            filters.Append(MessageConfigColumn.PortalID, portalId.ToString());
            if (localeId > 0)
            {
                filters.Append(MessageConfigColumn.LocaleID, localeId.ToString());
            }
            TList<ZNode.Libraries.DataAccess.Entities.MessageConfig> messageConfigList = messageConfigService.Find(filters.GetParameters());

            return messageConfigList;
        }

        /// <summary>
        /// Get all MessageConfig for a portal
        /// </summary>
        public TList<MessageConfig> GetAllMessageConfigData()
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            TList<ZNode.Libraries.DataAccess.Entities.MessageConfig> messageConfigList = messageConfigService.GetAll();
            return messageConfigList;
        }


        public bool CheckSiteMessageConfigCache()
        {
            var items = System.Web.HttpContext.Current.Cache[CacheKey_GetByKeyPortalIDLocaleIDMessageTypeID + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId] as TList<MessageConfig>;
            if (items != null && items.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage shipping rules types
    /// </summary>
    public class ShippingRuleTypeAdmin : ZNodeBusinessBase
    {
        #region Public Methods
        /// <summary>
        /// Get all shipping rule types
        /// </summary>
        /// <returns>Returns list of ShippingRuleType object</returns>
        public TList<ShippingRuleType> GetAllShippingRuleType()
        {
            ZNode.Libraries.DataAccess.Service.ShippingRuleTypeService shippingRuleTypeService = new ShippingRuleTypeService();
            TList<ZNode.Libraries.DataAccess.Entities.ShippingRuleType> shippingRuleTypeList = shippingRuleTypeService.GetAll();

            return shippingRuleTypeList;
        }

        /// <summary>
        /// Get a shipping rule type by Id.
        /// </summary>
        /// <param name="shippingRuleTypeId">Shipping rule type Id to get the shipping rule type object.</param>
        /// <returns>Returns the ShippingRuleType object.</returns>
        public ShippingRuleType GetByShippingRuleTypeID(int shippingRuleTypeId)
        {
            ZNode.Libraries.DataAccess.Service.ShippingRuleTypeService shippingRuleTypeService = new ShippingRuleTypeService();
            ZNode.Libraries.DataAccess.Entities.ShippingRuleType shippingRuleType = shippingRuleTypeService.GetByShippingRuleTypeID(shippingRuleTypeId);

            return shippingRuleType;
        }
        #endregion
    }
}

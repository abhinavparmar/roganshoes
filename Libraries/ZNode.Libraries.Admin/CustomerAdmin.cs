using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage Customers
    /// </summary>
    public class CustomerAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Get the Login Name for a User id
        /// </summary>
        /// <param name="userId">User Id to get the login name</param>
        /// <returns>Returns the login name for the user Id</returns>
        public string GetByUserID(int userId)
        {
            string loginName = string.Empty;

            ZNode.Libraries.DataAccess.Custom.CustomerHelper customerHelper = new CustomerHelper();
            loginName = customerHelper.GetByUserID(userId);

            return loginName;
        }

        /// <summary>
        /// Search the customer.
        /// </summary>
        /// <param name="firstName">Customer First name </param>
        /// <param name="lastName">Customer last name</param>
        /// <param name="companyName">Company Name</param>
        /// <param name="postalcode">Custome Postal code </param>
        /// <param name="loginName">Custome login name</param>
        /// <param name="orderId">Custome Order Id</param>
        /// <param name="portalId">Portal Id of the customer.</param>
        /// <param name="portalIds">Portal Ids of the customer.</param>
        /// <returns>Returns the search result.</returns>
        public DataSet CustomerSearch(string firstName, string lastName, string companyName, string postalcode, string loginName, int orderId, int portalId, string portalIds)
        {
            CustomerHelper customHelper = new CustomerHelper();

            return customHelper.CustomerSearch(firstName, lastName, companyName, postalcode, loginName, orderId, portalId, portalIds);
        }
    }
}

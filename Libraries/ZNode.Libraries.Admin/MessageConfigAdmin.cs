﻿using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// ZNodeMessageKey enumeration
    /// </summary>
    public enum ZNodeMessageKey
    {
        /// <summary>
        /// Call for Pricing Key
        /// </summary>
        ProductCallForPricing = 400,

        /// <summary>
        /// BreadCrumbsHomeLinkText Key
        /// </summary>
        BreadCrumbsHomeLinkText = 388
    }

    public enum ZNodeMessageType
    {
        /// <summary>
        /// Type is Message
        /// </summary>
        Message = 1,

        /// <summary>
        /// Type is Banner.
        /// </summary>
        Banner = 2
    }

    public partial class MessageConfigAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Get MessageConfig by Id.
        /// </summary>
        /// <param name="messageId">Message Id to get the messageconfig object.</param>
        /// <returns>Returns the MessageConfig object.</returns>
        public MessageConfig GetByMessageID(int messageId)
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            ZNode.Libraries.DataAccess.Entities.MessageConfig messageConfig = messageConfigService.GetByMessageID(messageId);
            return messageConfig;
        }

        /// <summary>
        /// Returns all Messages in MessageConfig
        /// </summary>
        /// <returns>Returns list of MessageConfig object.</returns>
        public TList<MessageConfig> GetAllMessagesConfig()
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            TList<ZNode.Libraries.DataAccess.Entities.MessageConfig> messageConfigList = messageConfigService.GetByMessageTypeID((int)ZNodeMessageType.Message);

            return messageConfigList;
        }

        /// <summary>
        /// Returns all banners.
        /// </summary>
        /// <returns>Return list of MessageConfig object</returns>
        public TList<MessageConfig> GetAllBanners()
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            TList<ZNode.Libraries.DataAccess.Entities.MessageConfig> messageConfigList = messageConfigService.GetByMessageTypeID((int)ZNodeMessageType.Banner);

            return messageConfigList;
        }

        /// <summary>
        /// Get all MessageConfig for a portal
        /// </summary>
        /// <param name="portalId">Portal Id to get the message config.</param>
        /// <param name="localeId">Locale Id to get the message config.</param>
        /// <returns>Returns list of MessageConfig object by portal and locale Id.</returns>
        public TList<MessageConfig> GetMessagesConfigByPortalIdLocaleId(int portalId, int localeId)
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            MessageConfigQuery filters = new MessageConfigQuery();

            filters.Append(MessageConfigColumn.PortalID, portalId.ToString());
            if (localeId > 0)
            {
                filters.Append(MessageConfigColumn.LocaleID, localeId.ToString());
            }

            filters.Append(MessageConfigColumn.MessageTypeID, ((int)ZNodeMessageType.Message).ToString());
            TList<ZNode.Libraries.DataAccess.Entities.MessageConfig> messageConfigList = messageConfigService.Find(filters.GetParameters());

            return messageConfigList;
        }

        /// <summary>
        ///  Get a MessageConfig by messge ket, portal Id and locale Id.
        /// </summary>
        /// <param name="messageKey">Message key to get the MessageConfig</param>
        /// <param name="portalId">Portal Id to get the MessageConfig</param>
        /// <param name="localeId">Locale Id to get the MessageConfig</param>
        /// <returns>Returns list of MessageConfig object based on message key, portal id and locale Id.</returns>
        public TList<MessageConfig> GetBannersByKeyPortalIDLocaleID(string messageKey, int portalId, int localeId)
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();

            //Zeon Customization - :: Get Message Config Data from Cache :: Start

            //TList<ZNode.Libraries.DataAccess.Entities.MessageConfig> messageConfigList = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID(messageKey, portalId, localeId, (int)ZNodeMessageType.Banner);

            TList<ZNode.Libraries.DataAccess.Entities.MessageConfig> messageConfigList = this.GetByKeyPortalIDLocaleIDMessageTypeID(messageKey, portalId, localeId, (int)ZNodeMessageType.Banner);

            //Zeon Customization - End

            return messageConfigList;
        }

        /// <summary>
        ///  Get a MessageConfig by messge ket, portal Id and locale Id.
        /// </summary>
        /// <param name="messageKey">Message key to get the MessageConfig</param>
        /// <param name="portalId">Portal Id to get the MessageConfig</param>
        /// <param name="localeId">Locale Id to get the MessageConfig</param>
        /// <returns>Returns MessageConfig object based on message key, portal id and locale Id.</returns>
        public MessageConfig GetByKeyPortalIDLocaleID(string messageKey, int portalId, int localeId)
        {
            //ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            //messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID(messageKey, portalId, localeId, (int)ZNodeMessageType.Message);

            //Zeon Customization - :: Get Message Config Data from Cache
            TList<MessageConfig> messageConfigList = this.GetByKeyPortalIDLocaleIDMessageTypeID(messageKey, portalId, localeId, (int)ZNodeMessageType.Message);
            //Zeon Customization

            if (messageConfigList != null && messageConfigList.Count > 0)
            {
                return messageConfigList[0];
            }
            else
            {
                return null;
            }

        }

        /// <summary>
        ///  Get a string  by messageKey, PortalId and LocaleId
        /// </summary>
        /// <param name="messageKey">Message key to get the message string.</param>
        /// <param name="portalId">Portal Id to get the the message string</param>
        /// <param name="localeId">Locale Id to get the the message string</param>
        /// <returns>Returns the message string.</returns>
        public string GetMessage(ZNodeMessageKey messageKey, int portalId, int localeId)
        {
            return this.GetMessage(messageKey.ToString(), portalId, localeId);
        }

        /// <summary>
        ///  Get a string  by messageKey, PortalId and LocaleId
        /// </summary>
        /// <param name="messageKey">Message key to get the message string.</param>
        /// <param name="portalId">Portal Id to get the the message string</param>
        /// <param name="localeId">Locale Id to get the the message string</param>
        /// <returns>Returns the message string.</returns>
        public string GetMessage(string messageKey, int portalId, int localeId)
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();

            //Zeon Customization - :: Get Message Config Data from Cache :: Start

            //TList<MessageConfig> messageConfig = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID(messageKey, portalId, localeId, (int)ZNodeMessageType.Message);

            TList<MessageConfig> messageConfig = this.GetByKeyPortalIDLocaleIDMessageTypeID(messageKey, portalId, localeId, (int)ZNodeMessageType.Message);

            //Zeon Customization - :: Get Message Config Data from Cache :: Start

            if (messageConfig != null && messageConfig.Count > 0)
            {
                return messageConfig[0].Value;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        ///  Add MessageConfig Data
        /// </summary>
        /// <param name="messageConfig">MessageConfig object to insert.</param>
        /// <param name="messageId">MessageConfig Id for the inserted object (out param)</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Insert(MessageConfig messageConfig, out int messageId)
        {
            messageId = 0;
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();

            // Check unique constraint for message type
            if (messageConfig.MessageTypeID == (int)ZNodeMessageType.Message)
            {
                TList<MessageConfig> existMessageConfig = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID(messageConfig.Key, messageConfig.PortalID, messageConfig.LocaleID, (int)ZNodeMessageType.Message);

                if (existMessageConfig != null && existMessageConfig.Count > 0)
                {
                    return false;
                }
            }

            bool isAdded = messageConfigService.Insert(messageConfig);
            if (!isAdded)
            {
                return false;
            }

            messageId = messageConfig.MessageID;

            return isAdded;
        }

        /// <summary>
        /// Update Message Config data
        /// </summary>
        /// <param name="messageConfig">MessageConfig object to update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(MessageConfig messageConfig)
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();

            bool isUpdated = messageConfigService.Update(messageConfig);
            return isUpdated;
        }

        /// <summary>
        ///  Delete  Message Config data based on messageId
        /// </summary>
        /// <param name="messageId">MessageConfig Id to delete from data source.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool Delete(int messageId)
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            return messageConfigService.Delete(messageId);
        }

        /// <summary>
        ///  Delete  Message Config data based on PortalID
        /// </summary>
        /// <param name="portalId">Portal Id to delete the message config for the portal.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool DeleteMessagesByPortalID(int portalId)
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            return messageConfigService.Delete(portalId);
        }

        /// <summary>
        ///  Delete  Message Config data based on PortalID and LocaleID
        /// </summary>
        /// <param name="portalId">Portal Id to delete the message config.</param>
        /// <param name="localeId">Locale Id to delete the message config.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool DeleteMessagesByPortalIDLocalID(int portalId, int localeId)
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            MessageConfig messageconfig = new MessageConfig();
            TList<MessageConfig> messageConfigList = this.GetMessagesConfigByPortalIdLocaleId(portalId, localeId);

            try
            {
                foreach (MessageConfig message in messageConfigList)
                {
                    messageConfigService.Delete(message.MessageID);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to verify database connection and permissions during Installation
    /// </summary>
    public class InstallAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Check the DataBase connection provided by the User
        /// </summary>
        /// <param name="serverName">Database server name.</param>
        /// <param name="dataBaseName">Database name</param>
        /// <param name="userId">Data user Id</param>
        /// <param name="password">Database password.</param>
        /// <returns>Returns true if database connected else false.</returns>
        public bool CheckDataBaseSettings(string serverName, string dataBaseName, string userId, string password)
        {
            SqlConnection connection = null;
            bool isConnected = false;

            try
            {
                string connectionString = "Data Source=" + serverName + "\\SQLEXPRESS;Initial Catalog=" + dataBaseName + ";user id=" + userId + ";password=" + password;
                connection = new SqlConnection(connectionString);
                connection.Open();
                isConnected = true;
            }
            catch 
            {
                throw;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return isConnected;
        }

        /// <summary>
        /// Validates the web.config for write permission
        /// </summary>
        /// <returns>Returns true if web.config file has write permission else false.</returns>
        public bool ValidatePermissions()
        {
            // Get Application Path
            string applicationPath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath;

            // Create a new FileInfo object.
            FileInfo fileInfo = new FileInfo(HttpContext.Current.Server.MapPath(applicationPath + "/web.config"));

            // Check for Read Only
            if (fileInfo.IsReadOnly)
            {
                // If it is readyonly then return false
                return false; 
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Validates the Public folder for Read/write permission
        /// </summary>
        /// <returns>Returns true if application data path has write permission else false.</returns>
        public bool ValidateDirectoryPermission()
        {
            // Get Application Path
            string applicationPath = ZNodeConfigManager.EnvironmentConfig.DataPath;
            bool hasPermission = false;

            try
            {
                string directoryName = "TestDir";
                Directory.CreateDirectory(applicationPath + "/" + directoryName);
                Directory.Delete(applicationPath + "/" + directoryName);
                hasPermission = true;
            }
            catch
            {
            }

            return hasPermission;
        }

        /// <summary>
        /// Creates and confiure the database 
        /// </summary>
        /// <param name="serverName">Database server name.</param>
        /// <param name="dataBaseName">Database name</param>
        /// <param name="userId">Data user Id</param>
        /// <param name="password">Database password.</param>
        /// <returns>Returns true if database conbfigured from install.sql file else false.</returns>
        public bool ConfigureDataBase(string serverName, string dataBaseName, string userId, string password)
        {
            SqlConnection connection = null;
            bool isDatabaseConfigured = false;

            try
            {
                string connectionString = "Data Source=" + serverName + "\\SQLEXPRESS;Initial Catalog=" + dataBaseName + ";user id=" + userId + ";password=" + password;
                connection = new SqlConnection(connectionString);

                // Get Application Path
                string applicationPath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath;

                string sqlFilePath = HttpContext.Current.Server.MapPath(applicationPath + "/admin/install/install.sql");
                StreamReader reader = new StreamReader(sqlFilePath);

                StringBuilder builder = new StringBuilder(reader.ReadToEnd());

                string[] sqlLine;
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^GO", RegexOptions.IgnoreCase | RegexOptions.Multiline);

                string txtSQL = builder.ToString();
                sqlLine = regex.Split(txtSQL);

                SqlCommand command = connection.CreateCommand();
                command.Connection = connection;
                connection.Open();

                foreach (string line in sqlLine)
                {
                    if (line.Length > 0)
                    {
                        command.CommandText = line;
                        command.CommandType = CommandType.Text;
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (SqlException) 
                        {
                            throw;
                        }
                    }
                }

                isDatabaseConfigured = true;
            }
            catch 
            {
                throw;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return isDatabaseConfigured;
        }

        /// <summary>
        /// Method replace the old connection string with the new connection string giben by the user
        /// </summary>
        /// <param name="serverName">Database server name.</param>
        /// <param name="dataBaseName">Database name</param>
        /// <param name="userId">Data user Id</param>
        /// <param name="password">Database password.</param>
        /// <returns>Returns true if web.config database connection string successfully updated else false.</returns>
        public bool SetupConfiugarationFile(string serverName, string dataBaseName, string userId, string password)
        {
            bool isDatabaseConfigured = true;
            try
            {
                // New connection String              
                string connectionString = "Data Source=" + serverName + "\\SQLEXPRESS;Initial Catalog=" + dataBaseName + ";user id=" + userId + ";password=" + password;

                // Get Application Path
                string webPath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath;

                // Get Configuaration object of the cureent web request using web path
                Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(webPath);

                // Remove the old connection string
                config.ConnectionStrings.ConnectionStrings.Remove("ZNodeECommerceDB");

                // Create ne connection string
                ConnectionStringSettings Settings = new ConnectionStringSettings("ZNodeECommerceDB", connectionString);

                // Add connection string to the collection
                config.ConnectionStrings.ConnectionStrings.Add(Settings);

                // Save the changes
                config.Save();                
            }
            catch
            {
                isDatabaseConfigured = false;
            }

            return isDatabaseConfigured;
        }
    }
}

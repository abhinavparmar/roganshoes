using System;
using System.ComponentModel;
using System.Reflection;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represents the Approval Status
    /// </summary>
    public class ZNodeApprovalStatus
    {
        /// <summary>
        /// Represents the Tracking Status
        /// </summary>
        public enum ApprovalStatus
        {
            /// <summary>
            /// Product review state is 'Pending Approval'
            /// </summary>
            [Description("Pending Approval")]
            N,

            /// <summary>
            /// Product review state is 'Approved'
            /// </summary>
            [Description("Approved")]
            A,

            /// <summary>
            /// Product review state is 'Declined'
            /// </summary>
            [Description("Declined")]
            I
        }

        /// <summary>
        /// Get Enum description value if it is a string.
        /// </summary>
        /// <param name="value">value object to get the value from enum.</param>
        /// <returns>Returns the enum string value.</returns>
        public static string GetEnumValue(object value)
        {
            string enumValue = string.Empty;
            try
            {
                FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
                enumValue = (attributes.Length > 0) ? attributes[0].Description : value.ToString();
            }
            catch (NullReferenceException)
            {
                enumValue = string.Empty;
            }

            return enumValue;
        }
    }
}

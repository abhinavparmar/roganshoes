﻿using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
     public partial class  ProductCrossSellAdmin
    {
        /// <summary>
        /// Add New related product item for a Product
        /// </summary>
        /// <param name="productId">Master Product Id</param>
        /// <param name="relatedProductId">Related product Id</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Insert(int productId, int RelatedProductId, int CrossSellTypeId)
        {
            // Instance for a ProductCrossSellerHelper class
            ProductCrossSellHelper productCrossHelper = new ProductCrossSellHelper();
            bool isAdded = productCrossHelper.Insert(productId, RelatedProductId, CrossSellTypeId);
            return isAdded;
        }

        /// <summary>
        /// Returns all related items for a product
        /// </summary>
        /// <param name="ProductID"></param>
        /// <returns></returns>
        public DataSet GetByProductID(int ProductID)
        {
            ProductCrossSellHelper productCrossHelper = new ProductCrossSellHelper();
            return productCrossHelper.GetCrossSellProductsByProductID(ProductID);
        }
    }
}

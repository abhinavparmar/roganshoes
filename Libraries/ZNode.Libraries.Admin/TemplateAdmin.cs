using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage templating system and style sheets
    /// </summary>
    public class TemplateAdmin : ZNodeBusinessBase
    {
        #region Public Methods

        /// <summary>
        /// Get the Style sheet content(CSS)
        /// </summary>
        /// <returns>Returns the style sheet template.</returns>
        public string GetTemplateStyle()
        {
            string templatePath = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/common/";
            DirectoryInfo directoryInfo = new DirectoryInfo(HttpContext.Current.Server.MapPath(templatePath));
            FileInfo fileInfo = new FileInfo(directoryInfo.FullName + "style.css");
            TextReader textReader = null;

            if (fileInfo.Exists)
            {
                textReader = new StreamReader(fileInfo.FullName);
            }

            try
            {
                string html = textReader.ReadToEnd();
                textReader.Close();

                return html;
            }
            catch
            {
                if (textReader != null)
                {
                    textReader.Close();
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Creates or updates an HTML Styles file 
        /// </summary>        
        /// <param name="css">CSS content to update.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdateTemplateStyleFile(string css)
        {
            string templatePath = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/common/";
            DirectoryInfo directoryInfo = new DirectoryInfo(HttpContext.Current.Server.MapPath(templatePath));
            FileInfo fileInfo = new FileInfo(directoryInfo.FullName + "style.css");
            TextWriter textWriter = new StreamWriter(fileInfo.FullName);

            try
            {
                textWriter.Write(css);
                textWriter.Close();

                return true;
            }
            catch
            {
                if (textWriter != null)
                {
                    textWriter.Close();
                }

                return false;
            }
        }

        #endregion

        #region Helper Methods

        #region Method to Replace Placeholders with UserControl
        /// <summary>
        /// Update the Placeholders with the User Control code
        /// </summary>
        /// <param name="templateContent">Template content string.</param>
        /// <returns>Returns the formatted master page content.</returns>
        public string FormatMasterPageContent(string templateContent)
        {
            StringBuilder build = new StringBuilder(templateContent);

            // Create a new XmlTextReader instance
            XmlTextReader xmlreader = null;

            try
            {
                // Load the file from the URL
                xmlreader = new XmlTextReader(HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "/templateconfig.xml"));
                xmlreader.WhitespaceHandling = WhitespaceHandling.None;

                string searchString = string.Empty;
                string replaceString = string.Empty;
                string outputString = string.Empty;

                // Read from XML File
                while (xmlreader.Read())
                {
                    switch (xmlreader.NodeType)
                    {
                        case XmlNodeType.CDATA:
                            // Get UserControl Code from CDATA
                            replaceString = xmlreader.Value;
                            if (searchString.Length > 0)
                            {
                                // Replace placeholders with the Usercontrol                                                    
                                outputString = string.Empty;
                                outputString = Regex.Replace(build.ToString(), searchString, replaceString, RegexOptions.IgnoreCase);
                                build.Remove(0, build.Length);
                                build.Append(outputString);
                            }

                            break;
                        case XmlNodeType.Element:
                            if (xmlreader.HasAttributes)
                            {
                                xmlreader.MoveToNextAttribute();

                                // Get PlaceHolders Key value
                                searchString = xmlreader.Value;
                            }
                            
                            break;

                        default:
                            break;
                    }
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
            finally
            {
                if (xmlreader != null)
                {
                    xmlreader.Close();
                }
            }

            string content = string.Empty;

            // Add Header Section with the Body content
            content = this.AddTemplateHeaders(build);
            build.Remove(0, build.Length);
            build.Append(content);

            // Add Footer Secton with the Body content
            content = this.AddTemplateFooters(build);
            build.Remove(0, build.Length);
            build.Append(content);

            // Return Template content
            return build.ToString();
        }
        #endregion       
        
        #endregion

        #region HTML Validation Helper Methods

        /// <summary>
        /// Validate HTML File - Using Regex Expressions
        /// </summary>
        /// <param name="builder">String builder object that contains data.</param>
        /// <returns>Returns true if validated otherwise false.</returns>
        public bool ValidateHTMLFile(StringBuilder builder)
        {
            MatchCollection matchCollection = null;
            MatchCollection MCollection = null;
            bool isExist = true;

            // Validate <html> tag
            if (!this.IsParsed("html", builder))
            {
                return false;
            }

            // Validate <head> tag
            if (!this.IsParsed("head", builder))
            {
                return false;
            }

            // Validate <title> tag
            if (!this.IsParsed("title", builder))
            {
                return false;
            }

            // Validate <body> tag
            if (!this.IsParsed("body", builder))
            {
                return false;
            }

            // Validate <form> tag
            if (!this.IsParsed("form", builder))
            {
                return false;
            }

            if (!this.ValidateHTMLTag("a", string.Empty, builder))
            {
                return false;
            }

            if (!this.ValidateHTMLTag("p", "param", builder))
            {
                return false;
            }

            if (!this.ValidateHTMLTag("div", string.Empty, builder))
            {
                return false;
            }

            if (!this.ValidateHTMLTag("span", string.Empty, builder))
            {
                return false;
            }

            if (!this.ValidateHTMLTag("table", string.Empty, builder))
            {
                return false;
            }

            if (!this.ValidateHTMLTag("tr", string.Empty, builder))
            {
                return false;
            }

            if (!this.ValidateHTMLTag("td", string.Empty, builder))
            {
                return false;
            }

            if (!this.ValidateHTMLTag("object", string.Empty, builder))
            {
                return false;
            }

            // Validate <strong> tag
            matchCollection = Regex.Matches(builder.ToString(), "<strong>", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            MCollection = Regex.Matches(builder.ToString(), "</strong>", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            if (MCollection.Count != matchCollection.Count)
            {
                isExist = false;
                return isExist;
            }

            // Validate bold <b> tag
            matchCollection = Regex.Matches(builder.ToString(), "<b>", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            MCollection = Regex.Matches(builder.ToString(), "</b>", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            if (MCollection.Count != matchCollection.Count)
            {
                isExist = false;
                return isExist;
            }

            // Validate </html> tag appears end of the file or not
            if (!Regex.IsMatch(builder.ToString(), "(</body(.*?)>)([.\\s\n]*</html(.*?)>$)", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace))
            {
                return false;
            }

            // Return Validate HTML boolean
            return isExist;
        }

        /// <summary>
        /// Validate other tags
        /// </summary>
        /// <param name="tag">Check the tags(Ex: p ,div,table,etc)</param>
        /// <param name="CheckTag">Check Related Tags like param tag related to tag p</param>
        /// <param name="builder">String builder object that contains data.</param>
        /// <returns>Returns true if validation success otherwise false.</returns>
        private bool ValidateHTMLTag(string tag, string CheckTag, StringBuilder builder)
        {
            MatchCollection matchCollection = null;
            MatchCollection MCollection = null;
            bool isExist = true;

            // Validate <html> tag
            matchCollection = Regex.Matches(builder.ToString(), "<" + tag + "\\s*[^><]*>", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            MCollection = Regex.Matches(builder.ToString(), "</" + tag + ">", RegexOptions.IgnoreCase);

            int OpeningTagCount = matchCollection.Count;
            int ClosingTagCount = MCollection.Count;

            if (CheckTag.Trim().Length > 0)
            {
                MatchCollection _Collection = Regex.Matches(builder.ToString(), "<" + CheckTag + "\\s*[^><]*>", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
                OpeningTagCount -= _Collection.Count;
            }

            if (OpeningTagCount != ClosingTagCount)
            {
                isExist = false;
            }

            return isExist;
        }

        /// <summary>
        /// Method to vlidate html,head,title,body tag count
        /// </summary>
        /// <param name="tag">Tag to parse.</param>
        /// <param name="builder">Builder object that contains data.</param>
        /// <returns>Returns true if parsed otherwise false.</returns>
        private bool IsParsed(string tag, StringBuilder builder)
        {
            MatchCollection matchCollection = null;
            MatchCollection MCollection = null;
            bool isExist = true;

            // Validate Tags
            matchCollection = Regex.Matches(builder.ToString(), "<" + tag + "\\s*[^><]*>", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            MCollection = Regex.Matches(builder.ToString(), "</" + tag + ">", RegexOptions.IgnoreCase);
            if (MCollection.Count != matchCollection.Count)
            {
                isExist = false;
            }
            else
            {
                if ((matchCollection.Count > 1) || (MCollection.Count > 1))
                {
                    isExist = false;
                }
            }

            return isExist;
        }
        
        #endregion

        #region Template Header Section
        /// <summary>
        /// Add template header section.
        /// </summary>
        /// <param name="build">String builder object to add template header.</param>
        /// <returns>Returns the formatted header string.</returns>
        private string AddTemplateHeaders(StringBuilder build)
        {
            StringBuilder builder = new StringBuilder();
            string headerContent = string.Empty;
            MatchCollection matchCollection = null;
            string originalTag = string.Empty;

            builder.Append("<%@ Master Language=\"C#\" AutoEventWireup=\"true\" Inherits=\"ZNode.Libraries.Framework.Business.ZNodeTemplate\" %>");
            builder.Append(Environment.NewLine);

            // Create a new XmlTextReader instance
            XmlTextReader xmlreader = null;

            // Load the file from the URL
            xmlreader = new XmlTextReader(HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "/templateconfig.xml"));
            xmlreader.WhitespaceHandling = WhitespaceHandling.None;

            string searchString = string.Empty;
            string registerTagString = string.Empty;

            // Read from XML File          
            while (xmlreader.Read())
            {
                switch (xmlreader.NodeType)
                {
                    case XmlNodeType.CDATA:
                        registerTagString = xmlreader.Value;

                        // Get UserControl Code from CDATA
                        if (searchString.Equals("#Register_UserControls#"))
                        {
                            // Replace placeholders with the Usercontrol                                                    
                            builder.Append(registerTagString);
                            builder.Append(Environment.NewLine);
                        }

                        break;
                    case XmlNodeType.Element:
                        if (xmlreader.HasAttributes)
                        {
                            xmlreader.MoveToNextAttribute();

                            // Get PlaceHolders Key value
                            searchString = xmlreader.Value;
                        }

                        break;

                    default:
                        break;
                }
            }

            if (xmlreader != null)
            {
                xmlreader.Close();
            }

            builder.Append(Environment.NewLine);

            // Check for <Html> tag
            matchCollection = Regex.Matches(build.ToString(), "<!DOCTYPE\\s*[^><]*>", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            if (matchCollection.Count == 0)
            {
                builder.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
                builder.Append(Environment.NewLine);
                builder.Append(Environment.NewLine);
            }
            else
            {
                build.Replace(matchCollection[0].Value, string.Empty);
                builder.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
                builder.Append(Environment.NewLine);
                builder.Append(Environment.NewLine);
            }

            // Check for <Html> tag
            matchCollection = Regex.Matches(build.ToString(), "<html(.*?)>", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            if (matchCollection.Count == 0)
            {
                // Not Exists ,Explicitly add into this section
                builder.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
                builder.Append(Environment.NewLine);
            }
            else
            {   // If Already exists ,add to the Header section
                build.Replace(matchCollection[0].Value, string.Empty);
                string htmlTag = matchCollection[0].Value;
                builder.Append(htmlTag);
                builder.Append(Environment.NewLine);
            }

            // Check for <Head> tag
            matchCollection = Regex.Matches(build.ToString(), "<head\\s*[^><]*>", RegexOptions.IgnoreCase);
            if (matchCollection.Count == 0)
            {
                // Not Exists ,Explicitly add into this section
                builder.Append("<head id=\"Head\" runat=\"server\">");
                builder.Append(Environment.NewLine);
            }
            else
            {
                build.Replace(matchCollection[0].Value, string.Empty);
                build.Replace("</head>", string.Empty);
                string headerTag = "<head id=\"Head\" runat=\"server\">";
                builder.Append(headerTag);
                builder.Append(Environment.NewLine);
            }

            // Check for <Title> tag
            matchCollection = Regex.Matches(build.ToString(), "<title>\\s*[^>]*</title>", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            if (matchCollection.Count > 0)
            {
                build.Replace(matchCollection[0].Value, string.Empty);
                build.Replace("</title>", string.Empty);
                string titleTag = matchCollection[0].Value;
                builder.Append(titleTag);
                builder.Append(Environment.NewLine);
            }

            // Check for <link> tag
            matchCollection = Regex.Matches(build.ToString(), "<link\\s*[^><]*>", RegexOptions.IgnoreCase);
            if (matchCollection.Count == 0)
            {
                builder.Append("<link id=\"stylesheet\" href=\"style.css\" type=\"text/css\" rel=\"stylesheet\" runat=\"server\" />");
            }
            else
            {
                foreach (Match match in matchCollection)
                {
                    Match _match = Regex.Match(match.Value, "runat=\"server\"", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
                    if (!_match.Success)
                    {
                        build.Replace(match.Value, string.Empty);
                        string tagValue = Regex.Replace(match.Value, "/>|>|</link>", string.Empty, RegexOptions.IgnoreCase);
                        string linkTag = Regex.Replace(match.Value, "<link (.*?)>", tagValue + " runat=\"server\" />", RegexOptions.IgnoreCase);
                        builder.Append(Environment.NewLine);
                        builder.Append(linkTag);
                    }
                    else
                    {
                        build.Replace(match.Value, string.Empty);
                        build.Replace("</link>", string.Empty);
                        string linkTag = match.Value;
                        builder.Append(Environment.NewLine);
                        builder.Append(linkTag);
                    }
                }
            }

            // Check for <meta> tag
            matchCollection = Regex.Matches(build.ToString(), "<meta\\s*[^><]*>", RegexOptions.IgnoreCase);
            foreach (Match match in matchCollection)
            {
                builder.Append(Environment.NewLine);
                builder.Append(match.Value);
                build.Replace(match.Value, string.Empty);
            }

            // Add closing head tag
            build.Replace("</head>", string.Empty);
            builder.Append(Environment.NewLine);
            builder.Append("</head>");

            // Check for <form> tag
            matchCollection = Regex.Matches(build.ToString(), "<form\\s*[^><]*>", RegexOptions.IgnoreCase);
            if (matchCollection.Count != 0)
            {
                build.Replace(matchCollection[0].Value, string.Empty);
                build.Replace("</form>", string.Empty);
            }

            // Check for <Body> tag
            matchCollection = Regex.Matches(build.ToString(), "<body(.*?)>", RegexOptions.IgnoreCase);
            if (matchCollection.Count == 0)
            {
                // There is no <body> tag. Add one.
                builder.Append("<body>");
            }
            else
            {
                // Save the original <body> tag so we can restore it properly in the next step.
                originalTag = matchCollection[0].Value;
            }

            // Merge Template Header Contents(Master,Register tags) with the Body content
            build.Insert(0, builder.ToString());

            // Add the <form> tag right after our <body> tag.
            headerContent = Regex.Replace(build.ToString(), originalTag, originalTag + Environment.NewLine + "<form id=\"form1\" runat=\"server\">", RegexOptions.IgnoreCase);

            build.Remove(0, build.Length);
            build.Append(headerContent.Trim());

            // Check for <a> anchor tag runat server attribute
            string content = this.CheckRunatServer("a", build);
            build.Remove(0, build.Length);
            build.Append(content);

            // Check for <a> anchor tag runat server attribute
            content = this.CheckRunatServer("img", build);
            build.Remove(0, build.Length);
            build.Append(content);

            // Return TemplateContent
            return build.ToString();
        }

        private string CheckRunatServer(string tagname, StringBuilder build)
        {
            MatchCollection matchCollection = null;

            matchCollection = Regex.Matches(build.ToString(), "<" + tagname + "\\s*[^><]*>", RegexOptions.IgnoreCase);

            foreach (Match match in matchCollection)
            {
                Match runAtmatch = Regex.Match(match.Value, "runat=\"server\"", RegexOptions.IgnoreCase);
                if (!runAtmatch.Success)
                {
                    if (tagname.Equals("a"))
                    {
                        string tagValue = Regex.Replace(match.Value, "/>|>", string.Empty, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
                        build.Replace(match.Value, tagValue + " runat=\"server\">");
                    }
                }
                else
                {
                    if (tagname.Equals("img"))
                    {
                        Match imgMatch = Regex.Match(match.Value, "(=|\")\\d{1,3}(px|%)(\"|)", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
                        if (imgMatch.Success)
                        {
                            string TagAttrib = Regex.Replace(match.Value, "runat=\"server\"", string.Empty, RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase);
                            build.Replace(match.Value, TagAttrib);
                        }
                    }
                }
            }

            return build.ToString();
        }

        #endregion

        #region Template Footer Section

        /// <summary>
        /// Add the template footer section
        /// </summary>
        /// <param name="build">String builder object that contains html text.</param>
        /// <returns>Returns the formatted footer string.</returns>
        private string AddTemplateFooters(StringBuilder build)
        {
            StringBuilder builder = null;
            MatchCollection matchCollection = null;
            string footerContent = string.Empty;

            // Adding Footer Section to New Template
            builder = new StringBuilder();

            // Add Form Closing Tag</form> before the body closing tag
            matchCollection = Regex.Matches(build.ToString(), "</body>", RegexOptions.IgnoreCase);
            if (matchCollection.Count == 0)
            {
                builder.Append("</body>" + Environment.NewLine);
            }

            // Check for <Html> tag
            matchCollection = Regex.Matches(build.ToString(), "</html>", RegexOptions.IgnoreCase);
            if (matchCollection.Count == 0)
            {
                builder.Append("</html>");
            }

            // Remove <html> tag
            build.Append(builder.ToString());
            footerContent = Regex.Replace(build.ToString(), "</html>", string.Empty, RegexOptions.IgnoreCase);
            build.Remove(0, build.Length);
            build.Append(footerContent.Trim());

            // Add Html Tag
            footerContent = Regex.Replace(build.ToString(), "</body>", "</form>" + Environment.NewLine + "</body>" + Environment.NewLine + "</html>", RegexOptions.IgnoreCase);
            build.Remove(0, build.Length);
            build.Append(footerContent.Trim());

            return build.ToString();
        }
        #endregion
    }
}

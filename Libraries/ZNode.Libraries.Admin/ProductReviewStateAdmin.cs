﻿using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage catalog products
    /// </summary>
    public class ProductReviewStateAdmin : ZNodeBusinessBase
    {
        #region Public Methods
        /// <summary>
        /// Get all product review state 
        /// </summary>
        /// <returns>Returns list of ProductReviewState object.</returns>
        public TList<ProductReviewState> GetAllProductReviewState()
        {
            ZNode.Libraries.DataAccess.Service.ProductReviewStateService productReviewStateService = new ZNode.Libraries.DataAccess.Service.ProductReviewStateService();
            TList<ZNode.Libraries.DataAccess.Entities.ProductReviewState> productReviewStateList = productReviewStateService.GetAll();

            return productReviewStateList;
        }

        /// <summary>
        /// Get product review state by review state name.
        /// </summary>        
        /// <param name="reviewStateName">Review state name to get the ProductReviewState object.</param>
        /// <returns>Returns the ProductReviewState object.</returns>
        public ProductReviewState GetProductReviewStateByName(string reviewStateName)
        {
            ZNode.Libraries.DataAccess.Service.ProductReviewStateService productReviewStateService = new ZNode.Libraries.DataAccess.Service.ProductReviewStateService();
            ZNode.Libraries.DataAccess.Entities.ProductReviewState productReviewState = productReviewStateService.GetByReviewStateName(reviewStateName);

            return productReviewState;
        }

        /// <summary>
        /// Get ProductReviewState name by review state Id.
        /// </summary>
        /// <param name="reviewStateId">Review State Id to get the ProductReviewState object.</param>
        /// <returns>Returns the product review state name.</returns>
        public string GetProductReviewStateName(int reviewStateId)
        {
            string reviewStateName = string.Empty;

            if (reviewStateId > 0)
            {
                ProductReviewStateService reviewStateService = new ProductReviewStateService();
                ProductReviewState reviewState = reviewStateService.GetByReviewStateID(reviewStateId);

                if (reviewState != null)
                {
                    reviewStateName = reviewState.ReviewStateName;
                }
            }

            return reviewStateName;
        }
        #endregion
    }
}

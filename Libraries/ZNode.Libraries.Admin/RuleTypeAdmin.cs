using System;
using System.Text;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represents the RuleTypeAdmin class.
    /// </summary>
    public class RuleTypeAdmin
    {
        #region Promotion Types

        /// <summary>
        /// Get all discount types.
        /// </summary>
        /// <returns>Returns list of DiscountType object</returns>
        public TList<DiscountType> GetDiscountTypes()
        {
            DiscountTypeService promotionTypeService = new DiscountTypeService();
            return promotionTypeService.GetAll();
        }

        /// <summary>        
        /// Get a dicount type by dicount type Id
        /// </summary>
        /// <param name="dicountTypeId">Discount type Id to the discount type object.</param>
        /// <returns>Returns the DiscountType object.</returns>
        public DiscountType GetDiscountTypeById(int dicountTypeId)
        {
            DiscountTypeService promotionTypeService = new DiscountTypeService();
            return promotionTypeService.GetByDiscountTypeID(dicountTypeId);
        }

        /// <summary>
        /// Add a new dicount type
        /// </summary>
        /// <param name="discountType">DiscountType object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddDicountType(DiscountType discountType)
        {
            DiscountTypeService promotionTypeService = new DiscountTypeService();          

            return promotionTypeService.Insert(discountType);
        }

        /// <summary>
        /// Update a discount type.
        /// </summary>
        /// <param name="discountType">DiscountType object to insert.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdateDicountType(DiscountType discountType)
        {
            DiscountTypeService promtionTypeService = new DiscountTypeService();
            return promtionTypeService.Update(discountType);
        }

        /// <summary>
        /// Delete a discount type by Id.
        /// </summary>
        /// <param name="discountTypeId">Discount type Id to get the discount type object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteDiscountType(int discountTypeId)
        {
            DiscountTypeService promtionTypeService = new DiscountTypeService();
            return promtionTypeService.Delete(discountTypeId);
        }
        #endregion

        #region Shipping Type

        /// <summary>
        /// Get all shipping types.
        /// </summary>
        /// <returns>Returns list of ShippingType object.</returns>
        public TList<ShippingType> GetShippingTypes()
        {
            ShippingTypeService shippingTypeService = new ShippingTypeService();
            return shippingTypeService.GetAll();
        }

        /// <summary>
        /// Get a shipping type by Id.
        /// </summary>
        /// <param name="shippingTypeId">Shipping type Id to get the shipping type object.</param>
        /// <returns>Returns the ShippingType object.</returns>
        public ShippingType GetShippingTypeById(int shippingTypeId)
        {
            ShippingTypeService shippingTypeService = new ShippingTypeService();
            return shippingTypeService.GetByShippingTypeID(shippingTypeId);
        }

        /// <summary>
        /// Add a new shipping type
        /// </summary>
        /// <param name="shippingType">ShippingType object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddShippingType(ShippingType shippingType)
        {
            ShippingTypeService shippingTypeService = new ShippingTypeService();

            return shippingTypeService.Insert(shippingType);
        }

        /// <summary>
        /// Update a shipping type
        /// </summary>
        /// <param name="shippingType">ShippingType object to udpate.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdateShippingType(ShippingType shippingType)
        {
            ShippingTypeService shippingTypeService = new ShippingTypeService();
            return shippingTypeService.Update(shippingType);
        }

        /// <summary>
        /// Delete a shipping type by Id.
        /// </summary>
        /// <param name="shippingTypeId">Shipping type Id to delete the shipping type object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteShippingType(int shippingTypeId)
        {
            ShippingTypeService shippingTypeService = new ShippingTypeService();
            return shippingTypeService.Delete(shippingTypeId);
        }
        #endregion

        #region TaxRuleType

        /// <summary>
        /// Get all tax tule types.
        /// </summary>
        /// <returns>Returns list of TaxRuleType object.</returns>
        public TList<TaxRuleType> GetTaxRuleTypes()
        {
            TaxRuleTypeService taxRuleTypeService = new TaxRuleTypeService();
            return taxRuleTypeService.GetAll();
        }

        /// <summary>        
        /// Get a tax rule type by Id.
        /// </summary>
        /// <param name="taxRuleTypeId">Tax rule type Id to get the tax rule object.</param>
        /// <returns>Returns the TaxRule object.</returns>
        public TaxRuleType GetTaxRuleTypeById(int taxRuleTypeId)
        {
            TaxRuleTypeService taxRuleTypeService = new TaxRuleTypeService();
            return taxRuleTypeService.GetByTaxRuleTypeID(taxRuleTypeId);
        }

        /// <summary>
        /// Add a new taxrule type
        /// </summary>
        /// <param name="taxRuleType">TaxRuleType object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddTaxRuleType(TaxRuleType taxRuleType)
        {
            TaxRuleTypeService taxRuleTypeService = new TaxRuleTypeService();

            return taxRuleTypeService.Insert(taxRuleType);
        }

        /// <summary>
        /// Update a taxrule type
        /// </summary>
        /// <param name="taxRuleType">TaxRuleType object to update.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdateTaxRuleType(TaxRuleType taxRuleType)
        {
            TaxRuleTypeService taxRuleTypeService = new TaxRuleTypeService();

            return taxRuleTypeService.Update(taxRuleType);
        }

        /// <summary>
        /// Delete a tax rule type by Id.
        /// </summary>
        /// <param name="taxRuleTypeId">Tax rule type Id to delete the Tax rule type object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteTaxRuleType(int taxRuleTypeId)
        {
            TaxRuleTypeService taxRuleTypeService = new TaxRuleTypeService();
            return taxRuleTypeService.Delete(taxRuleTypeId);
        }
        #endregion

        #region Supplier Type

        /// <summary>
        /// Get all supplier types.
        /// </summary>
        /// <returns>Returns list of SupplierType object.</returns>
        public TList<SupplierType> GetSupplierTypes()
        {
            SupplierTypeService supplierTypeService = new SupplierTypeService();
            return supplierTypeService.GetAll();
        }

        /// <summary>        
        /// Get a supplier type by supplier type Id.
        /// </summary>
        /// <param name="supplierTypeId">Supplier type Id to get the supplier type object.</param>
        /// <returns>Returns the SupplierType object.</returns>
        public SupplierType GetSupplierTypeById(int supplierTypeId)
        {
            SupplierTypeService supplierTypeService = new SupplierTypeService();
            return supplierTypeService.GetBySupplierTypeID(supplierTypeId);
        }

        /// <summary>
        /// Add a new supplier type
        /// </summary>
        /// <param name="supplierType">SupplierType object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddSupplierType(SupplierType supplierType)
        {
            SupplierTypeService supplierTypeService = new SupplierTypeService();

            return supplierTypeService.Insert(supplierType);
        }

        /// <summary>
        /// Update a supplier type
        /// </summary>
        /// <param name="supplierType">SupplierType object to udapte.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdateSupplierType(SupplierType supplierType)
        {
            SupplierTypeService supplierTypeService = new SupplierTypeService();

            return supplierTypeService.Update(supplierType);
        }

        /// <summary>
        /// Delete a supplier type by Id.
        /// </summary>
        /// <param name="supplierTypeId">Supplier type Id to delete the supplier type.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool DeleteSupplierType(int supplierTypeId)
        {
            SupplierTypeService supplierTypeService = new SupplierTypeService();
            return supplierTypeService.Delete(supplierTypeId);
        }
        #endregion

        #region Public Helper Method
        /// <summary>
        /// Check whether the class name already exist
        /// </summary>
        /// <param name="itemId">Item Id to check.</param>
        /// <param name="className">Class name to check</param>
        /// <param name="objectType">Type of object to check.</param>
        /// <returns>Returns true if class name already exist otherwise false.</returns>
        public bool IsClassNameExist(int itemId, string className, Type objectType)
        {
            bool isExist = false;

            if (objectType == typeof(DiscountType))
            {
                DiscountTypeService discountTypeService = new DiscountTypeService();
                DiscountTypeQuery discountTypeQuery = new DiscountTypeQuery();
                discountTypeQuery.AppendEquals(DiscountTypeColumn.ClassName, className.Trim());
                if (itemId > 0)
                {
                    // Id ItemID greater than 0 then include in query. To check if item other than this has the same class name.
                    discountTypeQuery.AppendNotEquals(DiscountTypeColumn.DiscountTypeID, itemId.ToString());
                }

                TList<DiscountType> discountTypeList = discountTypeService.Find(discountTypeQuery.GetParameters());
                if (discountTypeList.Count >= 1)
                {
                    isExist = true;
                }
            }

            if (objectType == typeof(ShippingType))
            {
                ShippingTypeService shippingTypeService = new ShippingTypeService();
                ShippingTypeQuery shippingTypeQuery = new ShippingTypeQuery();
                shippingTypeQuery.AppendEquals(ShippingTypeColumn.ClassName, className.Trim());
                if (itemId > 0)
                {
                    shippingTypeQuery.AppendNotEquals(ShippingTypeColumn.ShippingTypeID, itemId.ToString());
                }

                TList<ShippingType> shippingTypeList = shippingTypeService.Find(shippingTypeQuery.GetParameters());
                if (shippingTypeList.Count >= 1)
                {
                    isExist = true;
                }
            }

            if (objectType == typeof(TaxRuleType))
            {
                TaxRuleTypeService taxRuleTypeService = new TaxRuleTypeService();
                TaxRuleTypeQuery taxRuleTypeQuery = new TaxRuleTypeQuery();
                taxRuleTypeQuery.AppendEquals(TaxRuleTypeColumn.ClassName, className.Trim());
                if (itemId > 0)
                {
                    taxRuleTypeQuery.AppendNotEquals(TaxRuleTypeColumn.TaxRuleTypeID, itemId.ToString());
                }

                TList<TaxRuleType> taxRuleTypeList = taxRuleTypeService.Find(taxRuleTypeQuery.GetParameters());
                if (taxRuleTypeList.Count >= 1)
                {
                    isExist = true;
                }
            }

            if (objectType == typeof(SupplierType))
            {
                SupplierTypeService supplierTypeService = new SupplierTypeService();
                SupplierTypeQuery supplierTypeQuery = new SupplierTypeQuery();
                supplierTypeQuery.AppendEquals(SupplierTypeColumn.ClassName, className.Trim());
                if (itemId > 0)
                {
                    supplierTypeQuery.AppendNotEquals(SupplierTypeColumn.SupplierTypeID, itemId.ToString());
                }

                TList<SupplierType> supplierTypeList = supplierTypeService.Find(supplierTypeQuery.GetParameters());
                if (supplierTypeList.Count >= 1)
                {
                    isExist = true;
                }
            }

            return isExist;
        } 
        #endregion

        #region Private Methods
        /// <summary>
        /// Get the next unique discount type Id.
        /// </summary>
        /// <param name="promotionTypeService">PromotionTypeService object to get all promotion types.</param>
        /// <returns>Returns the next unique discount type Id.</returns>
        private int GetDiscountTypeId(DiscountTypeService promotionTypeService)
        {
            TList<DiscountType> discountTypeList = promotionTypeService.GetAll();

            if (discountTypeList.Count > 0)
            {
                discountTypeList.Sort("DiscountTypeID Desc");

                return discountTypeList[0].DiscountTypeID + 1;
            }

            return 1;
        }

        /// <summary>
        /// Get the next unique shipping type Id.
        /// </summary>
        /// <param name="shippingTypeService">ShippingTypeService object to get all shipping types.</param>
        /// <returns>Returns the next unique shipping type Id.</returns>
        private int GetShippingTypeId(ShippingTypeService shippingTypeService)
        {
            TList<ShippingType> shippingTypeList = shippingTypeService.GetAll();

            if (shippingTypeList.Count > 0)
            {
                shippingTypeList.Sort("ShippingTypeID Desc");

                return shippingTypeList[0].ShippingTypeID + 1;
            }

            return 1;
        }
        #endregion
    }
}

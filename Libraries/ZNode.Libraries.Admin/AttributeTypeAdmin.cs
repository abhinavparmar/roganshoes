using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// This Class Manages Product Attribute types,product Attributes , ProductTypeAttribute
    /// </summary>
    public class AttributeTypeAdmin : ZNodeBusinessBase
    {
        #region Public Attribute Type Methods

        /// <summary>
        /// Returns all product attribute types
        /// </summary>
        /// <returns>Returns list of AttributeType objects.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.AttributeType> GetAll()
        {
            ZNode.Libraries.DataAccess.Service.AttributeTypeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.AttributeTypeService();

            TList<ZNode.Libraries.DataAccess.Entities.AttributeType> attributeTypeList = attributeTypeService.GetAll();

            return attributeTypeList;
        }

        /// <summary>
        /// Get a Attribute type
        /// </summary>
        /// <param name="attributeTypeId">Attribute Type Id to get the AttributeType object</param>
        /// <returns>Returns the AttributeType object</returns>
        public ZNode.Libraries.DataAccess.Entities.AttributeType GetByAttributeTypeId(int attributeTypeId)
        {
            ZNode.Libraries.DataAccess.Service.AttributeTypeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.AttributeTypeService();
            ZNode.Libraries.DataAccess.Entities.AttributeType attributeType = attributeTypeService.GetByAttributeTypeId(attributeTypeId);

            return attributeType;
        }

        /// <summary>
        /// Get list of AttributeType by portal Ids
        /// </summary>
        /// <param name="PortalIds">Portal Ids to get the AttributeType. </param>
        /// <returns>Returns list of AttributeType objects</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.AttributeType> GetAttributeTypePortalId(string PortalIds)
        {
            ZNode.Libraries.DataAccess.Service.AttributeTypeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.AttributeTypeService();
            TList<ZNode.Libraries.DataAccess.Entities.AttributeType> attributeTypeList = new TList<AttributeType>();

            foreach (string sportalId in PortalIds.Split(','))
            {
                int PortalID = 0;
                int.TryParse(sportalId, out PortalID);
                if (PortalID > 0)
                {
                    attributeTypeList.AddRange(attributeTypeService.GetByPortalID(PortalID));
                }
            }

            return attributeTypeList;
        }

        /// <summary>
        /// Add Attribute Type
        /// </summary>
        /// <param name="attributeType">AttributeType object to insert into datasouce.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Add(AttributeType attributeType)
        {
            ZNode.Libraries.DataAccess.Service.AttributeTypeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.AttributeTypeService();
            return attributeTypeService.Insert(attributeType);
        }

        /// <summary>
        /// Update Attribute Type
        /// </summary>
        /// <param name="attributeType">AttributeType object to updated.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(AttributeType attributeType)
        {
            ZNode.Libraries.DataAccess.Service.AttributeTypeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.AttributeTypeService();
            return attributeTypeService.Update(attributeType);
        }

        /// <summary>
        /// Delete AttributeType by AttributeType object
        /// </summary>
        /// <param name="attributeType">AttributeType object to delete from data source.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool DeleteAttributeType(AttributeType attributeType)
        {
            ZNode.Libraries.DataAccess.Service.AttributeTypeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.AttributeTypeService();
            return attributeTypeService.Delete(attributeType);
        }

        /// <summary>
        /// Returns No of Product Types for this Attribute Type
        /// </summary>
        /// <param name="attributeTypeId">Attribute typd Id to get the product types.</param>
        /// <returns>Returns the number of product type count.</returns>
        public int GetCountByAttributeTypeID(int attributeTypeId)
        {
            ProductTypeHelper productTypeHelper = new ProductTypeHelper();
            return productTypeHelper.GetProductTypeCount(attributeTypeId);
        }

        /// <summary>
        /// Returns Attributes data by Search data
        /// </summary>
        /// <param name="name">Search the attribute by name.</param>
        /// <returns>Returns AttributeType dataset.</returns>
        public DataSet GetAttributeBySearchData(string name)
        {
            ProductTypeHelper productTypeHelper = new ProductTypeHelper();
            return productTypeHelper.GetAttributeBySearchData(name);
        }

        #endregion

        #region Public Product Attributes Methods

        /// <summary>
        /// Returns all product attribute
        /// </summary>
        /// <returns>Returns list of ProductAttribute object</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.ProductAttribute> GetAllProductAttribute()
        {
            ZNode.Libraries.DataAccess.Service.ProductAttributeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.ProductAttributeService();
            TList<ZNode.Libraries.DataAccess.Entities.ProductAttribute> attributeTypeList = attributeTypeService.GetAll();

            return attributeTypeList;
        }

        /// <summary>
        /// Returns a Product Attribute for this attribute type
        /// </summary>
        /// <param name="attributeTypeId">Attribute type Id to get the list of product attribute.</param>
        /// <returns>Returns list of ProductAttribute objects.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.ProductAttribute> GetByAttributeTypeID(int attributeTypeId)
        {
            ZNode.Libraries.DataAccess.Service.ProductAttributeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.ProductAttributeService();
            TList<ZNode.Libraries.DataAccess.Entities.ProductAttribute> AttributeList = attributeTypeService.GetByAttributeTypeId(attributeTypeId);

            return AttributeList;
        }

        /// <summary>
        /// Returns a Product Attribute for this attribute type
        /// </summary>
        /// <param name="attributeId">Attribute Id to get the ProductAttribute object.</param>
        /// <returns>Retruns ProductAttribute object.</returns>
        public ZNode.Libraries.DataAccess.Entities.ProductAttribute GetByAttributeID(int attributeId)
        {
            ZNode.Libraries.DataAccess.Service.ProductAttributeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.ProductAttributeService();
            ZNode.Libraries.DataAccess.Entities.ProductAttribute productAttribute = attributeTypeService.GetByAttributeId(attributeId);

            return productAttribute;
        }

        /// <summary>
        /// Add Product Attribute
        /// </summary>
        /// <param name="productAttribute">Product attribute type inset into data source.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool AddProductAttribute(ProductAttribute productAttribute)
        {
            ZNode.Libraries.DataAccess.Service.ProductAttributeService productAttributeService = new ZNode.Libraries.DataAccess.Service.ProductAttributeService();

            return productAttributeService.Insert(productAttribute);
        }

        /// <summary>
        /// Update Product Attribute
        /// </summary>
        /// <param name="productAttribute">Product attribute to update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool UpdateProductAttribute(ProductAttribute productAttribute)
        {
            ZNode.Libraries.DataAccess.Service.ProductAttributeService productAttributeService = new ZNode.Libraries.DataAccess.Service.ProductAttributeService();
            return productAttributeService.Update(productAttribute);
        }

        /// <summary>
        /// Delete Product Attribute
        /// </summary>
        /// <param name="productAttribute">Product attribute type to delete from data source.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool DeleteProductAttribute(ProductAttribute productAttribute)
        {
            ZNode.Libraries.DataAccess.Service.ProductAttributeService productAttributeService = new ZNode.Libraries.DataAccess.Service.ProductAttributeService();
            return productAttributeService.Delete(productAttribute);
        }

        #endregion

        #region Public ProductType Attribute Methods

        /// <summary>
        /// Returns Attribue type for a ProductType
        /// </summary>
        /// <param name="productTypeId">Product type Id to get AttributeType.</param>
        /// <returns>Returns AttributeType dataset.</returns>
        public DataSet GetAttributeTypeByProductTypeID(int productTypeId)
        {
            ZNode.Libraries.DataAccess.Service.ProductTypeAttributeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.ProductTypeAttributeService();

            TList<ProductTypeAttribute> productTypeAttribute = attributeTypeService.GetByProductTypeId(productTypeId);
            return productTypeAttribute.ToDataSet(true);
        }

        /// <summary>
        /// Add New Product type Attribute
        /// </summary>
        /// <param name="productTypeAttribute">Product type attribute object to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool AddProductTypeAttribute(ProductTypeAttribute productTypeAttribute)
        {
            ZNode.Libraries.DataAccess.Service.ProductTypeAttributeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.ProductTypeAttributeService();
            return attributeTypeService.Insert(productTypeAttribute);
        }

        /// <summary>
        /// Delete ProductTypeAttribute by ProductTypeAttribute
        /// </summary>
        /// <param name="productTypeAttribute">Product type attribute to delete from data source.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool DeleteProductTypeAttribute(ProductTypeAttribute productTypeAttribute)
        {
            ZNode.Libraries.DataAccess.Service.ProductTypeAttributeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.ProductTypeAttributeService();
            return attributeTypeService.Delete(productTypeAttribute);
        }

        /// <summary>
        /// Returns all product type attributes
        /// </summary>
        /// <returns>Returns list of ProductTypeAttribute object.</returns>
        public TList<ProductTypeAttribute> GetAllProductTypeAttribute()
        {
            ZNode.Libraries.DataAccess.Service.ProductTypeAttributeService attributeTypeService = new ZNode.Libraries.DataAccess.Service.ProductTypeAttributeService();
            TList<ProductTypeAttribute> productTypeAttribute = attributeTypeService.GetAll();
            return productTypeAttribute;
        }
        #endregion

        #region Locale

        /// <summary>
        /// Get all the LocaleId associated with Highlights.
        /// </summary>
        /// <returns>Returns the list of locale Id</returns>
        public TList<Locale> GetAllLocaleId()
        {
            // Get the Locale information with product id.
            LocaleService localeService = new LocaleService();
            LocaleQuery localeQuery = new LocaleQuery();
            localeQuery.AppendInQuery("LocaleId", "SELECT DISTINCT LOCALEID FROM ZNodeAttributeType");

            TList<Locale> localeList = localeService.Find(localeQuery.GetParameters());

            if (localeList.Count > 0)
            {
                localeList.Sort("LocaleDescription");
            }

            return localeList;
        }
        #endregion
    }
}


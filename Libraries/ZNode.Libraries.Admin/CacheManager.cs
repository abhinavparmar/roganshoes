using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represents the CacheManager class.
    /// </summary>
    public class CacheManager
    {
        /// <summary>
        /// Clears the cached navigation elements in all stores.
        /// </summary>
        public static void Clear()
        {
            // Get all portals
            PortalService portalService = new PortalService();
            TList<Portal> portalList = portalService.GetAll();

            // Remove Cache for each portals
            foreach (Portal portal in portalList)
            {
                HttpContext.Current.Cache.Remove("MenuNavigation" + portal.PortalID);
                HttpContext.Current.Cache.Remove("CategoryNavigation" + portal.PortalID);
                HttpContext.Current.Cache.Remove("SpecialsNavigation" + portal.PortalID);
                HttpContext.Current.Cache.Remove("BrandNavigation" + portal.PortalID);
                HttpContext.Current.Cache.Remove("BrandDropDownNavigation" + portal.PortalID);
                HttpContext.Current.Cache.Remove("PriceNavigation" + portal.PortalID);            
            }

            // Remove the Session
            HttpContext.Current.Session.Remove("ProductViewedList");
        }
    }
}

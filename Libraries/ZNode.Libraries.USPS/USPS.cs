﻿using System;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using System.Xml;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Shipping
{
    /// <summary>
    /// Represents the USPS Shipping class
    /// </summary>
    public partial class USPS : ZNodeBusinessBase
    {
        #region Protected Member Instance Variables

        private const string RateLookupQueryString = "API=RateV3&XML=";
        private const string AddressLookupQueryString = "API=Verify&XML=";

        private string _UspsShippingAPIUrl;
        private string _UspsWebToolsUserId;

        private Address _ShippingAddress;
        private string _OriginZipCode;
        private string _PDUZip5;
        private string _ServiceType = "Priority";
        private string _WeightInPounds = "0";
        private string _WeightInOunces = "0";

        // For Rate Lookup
        private string _SKU;
        private decimal _ShippingRate;

        private string _ErrorCode = "0";
        private string _ErrorDescription;
        #endregion

        #region Constructor
        public USPS()
        {
            // Set API URLS
            this._UspsShippingAPIUrl = System.Configuration.ConfigurationManager.AppSettings["USPSShippingAPIURL"].ToString();

            // Set API Credentials
            //this._UspsWebToolsUserId = System.Configuration.ConfigurationManager.AppSettings["USPSWebToolsUserID"].ToString();

            //Zeon Customization
            this._UspsWebToolsUserId = GetUspsWebToolsUserId();
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the USPS rate request (http) url.
        /// </summary>
        public string UspsShippingAPIUrl
        {
            get { return this._UspsShippingAPIUrl; }
            set { this._UspsShippingAPIUrl = value; }
        }

        /// <summary>
        /// Gets or sets the USPS web tools user Id.
        /// </summary>
        public string UspsWebToolsUserId
        {
            get { return this._UspsWebToolsUserId; }
            set { this._UspsWebToolsUserId = value; }
        }

        /// <summary>
        /// Gets or sets the shipping address
        /// </summary>
        public Address ShippingAddress
        {
            get
            {
                return this._ShippingAddress;
            }

            set
            {
                this._ShippingAddress = value;
            }
        }

        /// <summary>
        /// Gets or sets the origin 5-digit ZIP code.
        /// </summary>
        public string OriginZipCode
        {
            get { return this._OriginZipCode; }
            set { this._OriginZipCode = value; }
        }

        /// <summary>
        /// Gets or sets the Postage Delivery Unit 5-Digit Zip Code. The address to which the returned packages are posted.
        /// </summary>
        public string PDUZip5
        {
            get { return this._PDUZip5; }
            set { this._PDUZip5 = value; }
        }

        public string ServiceType
        {
            get { return this._ServiceType; }
            set { this._ServiceType = value; }
        }

        /// <summary>
        /// Gets or sets the Weight (in lbs) of the package
        /// </summary>
        public string WeightInPounds
        {
            get { return this._WeightInPounds; }
            set { this._WeightInPounds = value; }
        }

        /// <summary>
        /// Gets or sets the Weight (in ozs) of the package
        /// </summary>
        public string WeightInOunces
        {
            get { return this._WeightInOunces; }
            set { this._WeightInOunces = value; }
        }

        /// <summary>
        /// Gets or sets the SKU (for use with Rate Lookup)
        /// </summary>
        public string SKU
        {
            get { return this._SKU; }
            set { this._SKU = value; }
        }

        /// <summary>
        /// Gets or sets the ShippingRate
        /// </summary>
        public decimal ShippingRate
        {
            get { return this._ShippingRate; }
            set { this._ShippingRate = value; }
        }

        /// <summary>
        /// Gets or sets the API error response code 
        /// </summary>
        public string ErrorCode
        {
            get { return this._ErrorCode; }
            set { this._ErrorCode = value; }
        }

        /// <summary>
        /// Gets or sets the API response code description
        /// </summary>
        public string ErrorDescription
        {
            get { return this._ErrorDescription; }
            set { this._ErrorDescription = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method to retrieve shipping rate from USPS rate API
        /// Must retrieve from the ShippingRate property
        /// </summary>
        /// <returns>bool indicating if successful call to USPS was mad</returns>
        public bool CalculateShippingRate()
        {
            bool isSuccess = false;

            // Build the XML request string based on the set properties
            string xmlRequest = this.BuildRateRequestXmlString();

            // Build the API Request
            string apiRequest = this.BuildRequestUrl(this.UspsShippingAPIUrl, RateLookupQueryString, HttpUtility.UrlEncode(xmlRequest));

            try
            {
                // Perform an synchronous search
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(apiRequest);
                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();
                DataSet responseDataSet = new DataSet();
                responseDataSet.ReadXml(stream);

                isSuccess = this.ProcessRateResult(responseDataSet);                
            }
            catch (Exception e)
            {
                // Set error code and description
                this.ErrorCode = "-1";
                this.ErrorDescription = e.Message;
            }

            return isSuccess;
        }

        /// <summary>
        /// Check whether the address is valid or not. Address object should be assigned before calling this method.
        /// </summary>
        /// <returns>Returns true if address is valid otherwise false.</returns>
        public bool IsAddressValid()
        {
            bool isValid = this.IsAddressValid(this.ShippingAddress);
            return isValid;
        }

        /// <summary>
        /// Check whether the address is valid or not.
        /// </summary>
        /// <param name="address">Address object to validate.</param>
        /// <returns>Returns true if address is valid otherwise false.</returns>
        public bool IsAddressValid(Address address)
        {
            bool isValid = false;

            this.ErrorCode = "-1";
            if (address.Street.Length > 38)
            {
                this.ErrorDescription="Address1 is is limited to a maximum of 38 characters.";
                return false;
            }

            if (address.Street1.Length > 38)
            {
                this.ErrorDescription="Address2 is is limited to a maximum of 38 characters.";
                return false;
            }

            if (address.City.Length > 15)
            {
                this.ErrorDescription="City is is limited to a maximum of 15 characters.";
                return false;
            }

            if (address.StateCode.Length > 2)
            {
                this.ErrorDescription="State code is is limited to a maximum of 2 characters.";
                return false;
            }

            if (address.PostalCode.Length > 5)
            {
                this.ErrorDescription="Postal code is is limited to a maximum of 5 characters.";
                return false;
            }

            // Build the XML request string based on the set properties
            string xmlRequest = this.BuildAddressValidationRequest(address);

            // Build the API Request
            string apiRequest = this.BuildRequestUrl(this.UspsShippingAPIUrl, AddressLookupQueryString, HttpUtility.UrlEncode(xmlRequest));

            try
            {
                // Perform an synchronous search
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(apiRequest);
                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();
                DataSet responseDataSet = new DataSet();
                responseDataSet.ReadXml(stream);

                isValid = this.ProcessAddressResult(responseDataSet);
            }
            catch (Exception e)
            {
                // Set error code and description
                this.ErrorCode = "-1";
                this.ErrorDescription = e.Message;
            }

            return isValid;
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Builds the complete API Request
        /// </summary>
        /// <param name="apiUrl">USPS rate request Url</param>
        /// <param name="apiQueryString">USPS API query string.</param>
        /// <param name="xmlValue">Request XML string.</param>
        /// <returns>Returns the rate request Url to post.</returns>
        private string BuildRequestUrl(string apiUrl, string apiQueryString, string xmlValue)
        {
            string returnValue = string.Empty;
            returnValue = string.Format("{0}?{1}{2}", apiUrl, apiQueryString, xmlValue);
            return returnValue;
        }

        /// <summary>
        /// Builds the XML for the Rate Request
        /// </summary>
        /// <returns>Returns the USPS rate request string.</returns>
        private string BuildRateRequestXmlString()
        {
            string returnValue = string.Empty;

            XmlDocument document = new XmlDocument();
            XmlElement requestNode = document.CreateElement("RateV3Request");
            requestNode.SetAttribute("USERID", this._UspsWebToolsUserId);

            XmlElement packageNode = document.CreateElement("Package");
            packageNode.SetAttribute("ID", this.SKU);

            XmlElement serviceNode = document.CreateElement("Service");
            serviceNode.InnerText = "PRIORITY";

            XmlElement zipOriginationNode = document.CreateElement("ZipOrigination");
            zipOriginationNode.InnerText = this.OriginZipCode;

            XmlElement zipDestinationNode = document.CreateElement("ZipDestination");
            zipDestinationNode.InnerText = this.PDUZip5;

            XmlElement poundsNode = document.CreateElement("Pounds");
            poundsNode.InnerText = ((int)Decimal.Parse(this.WeightInPounds)).ToString();

            XmlElement ouncesNode = document.CreateElement("Ounces");
            ouncesNode.InnerText = ((int)Decimal.Parse(this.WeightInOunces)).ToString();

            XmlElement sizeNode = document.CreateElement("Size");
            sizeNode.InnerText = "REGULAR";

            // Append nodes
            packageNode.AppendChild(serviceNode);
            packageNode.AppendChild(zipOriginationNode);
            packageNode.AppendChild(zipDestinationNode);
            packageNode.AppendChild(poundsNode);
            packageNode.AppendChild(ouncesNode);
            packageNode.AppendChild(sizeNode);
            requestNode.AppendChild(packageNode);

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            requestNode.WriteTo(xw);
            returnValue = sw.ToString();

            return returnValue;
        }

        /// <summary>
        /// Builds the XML for the Rate Request
        /// </summary>
        /// <param name="address">Address object to build USPS address validation request.</param>
        /// <returns>Returns the USPS rate request string.</returns>
        private string BuildAddressValidationRequest(Address address)
        {
            string returnValue = string.Empty;           

            XmlDocument document = new XmlDocument();
            XmlElement requestNode = document.CreateElement("AddressValidateRequest");
            requestNode.SetAttribute("USERID", this._UspsWebToolsUserId);

            XmlElement addressNode = document.CreateElement("Address");
            addressNode.SetAttribute("ID", address.AddressID.ToString());

            XmlElement companyNode = document.CreateElement("FirmName");
            companyNode.InnerText = address.CompanyName;            

            // Swap the address line.
            XmlElement addressLine1Node = document.CreateElement("Address1");
            addressLine1Node.InnerText = address.Street1;

            XmlElement addressLine2Node = document.CreateElement("Address2");
            addressLine2Node.InnerText = address.Street;

            XmlElement cityNode = document.CreateElement("City");
            cityNode.InnerText = address.City;

            XmlElement stateNode = document.CreateElement("State");
            stateNode.InnerText = address.StateCode;

            XmlElement zip5Node = document.CreateElement("Zip5");
            zip5Node.InnerText = address.PostalCode;

            XmlElement zip4Node = document.CreateElement("Zip4");
            if (address.PostalCode.Length > 4)
            {
                zip4Node.InnerText = address.PostalCode.Substring(0, 4);
            }
            else
            {
                zip4Node.InnerText = address.PostalCode;
            }

            // Append nodes
            addressNode.AppendChild(companyNode);
            addressNode.AppendChild(addressLine1Node);
            addressNode.AppendChild(addressLine2Node);
            addressNode.AppendChild(cityNode);
            addressNode.AppendChild(stateNode);
            addressNode.AppendChild(zip5Node);
            addressNode.AppendChild(zip4Node);
            requestNode.AppendChild(addressNode);

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            requestNode.WriteTo(xw);
            returnValue = sw.ToString();

            return returnValue;
        }

        /// <summary>
        /// Process the response from the USPS from the Rate Request API
        /// </summary>
        /// <param name="responseDataSet">Response dataset that contains response from USPS</param>
        /// <returns>Returns true if rate successfully completed othwerwise.</returns>
        private bool ProcessRateResult(DataSet responseDataSet)
        {
            bool isSuccess = true;

            // Reset the erro code and message
            this.ErrorCode = "0";
            this.ErrorDescription = string.Empty;

            // Determine if Error response was returned
            if (responseDataSet.Tables["Error"] == null && responseDataSet.Tables["Package"] != null)
            {
                // Process Response
                DataTable tempTable = responseDataSet.Tables["Package"];
                DataRow tempPackage = tempTable.Rows[0];

                if (tempPackage != null)
                {
                    DataRow[] aryPostage = tempPackage.GetChildRows(responseDataSet.Relations["Package_Postage"]);
                    if (aryPostage != null)
                    {
                        DataRow postageRow = aryPostage[0];
                        this.ShippingRate = Decimal.Parse(postageRow["Rate"].ToString());
                    }
                }
            }
            else
            {
                isSuccess = false;
                this.ErrorCode = responseDataSet.Tables["Error"].Rows[0]["Number"].ToString();
                this.ErrorDescription = responseDataSet.Tables["Error"].Rows[0]["Description"].ToString();
            }

            return isSuccess;
        }

        /// <summary>
        /// Process the address resonse
        /// </summary>
        /// <param name="responseDataSet">Response dataset to parse.</param>
        /// <returns>Returns true if address exist otherwise false.</returns>
        private bool ProcessAddressResult(DataSet responseDataSet)
        {
            bool isSuccess = true;

            // Reset the erro code and message
            this.ErrorCode = "0";
            this.ErrorDescription = string.Empty;

            // Determine if Error response was returned
            if (responseDataSet.Tables["Error"] == null)
            {
                DataTable tempTable = responseDataSet.Tables["Address"];
            }
            else
            {
                isSuccess = false;
                this.ErrorCode = responseDataSet.Tables["Error"].Rows[0]["Number"].ToString();
                this.ErrorDescription = responseDataSet.Tables["Error"].Rows[0]["Description"].ToString();
            }

            return isSuccess;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
namespace ZNode.Libraries.Shipping
{
    /// <summary>
    /// Represents the USPS Shipping class
    /// </summary>
    public partial class USPS
    {

        #region[Varibales]

        private const string RateLookupQueryStringForInternational = "API=IntlRateV2&XML=";
        private const string RateLookupQueryStringV4 = "API=RateV4&XML=";
        private string _FilterValue = HttpUtility.HtmlEncode("Priority Mail International<sup>®</sup>");

        #endregion

        #region[Public Property]

        public string Country { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Length { get; set; }
        public string Girth { get; set; }
        public string ValueOfContent { get; set; }

        #endregion

        #region[USPS Domestic Shipping With V4 As it is upgraded version]

        /// <summary>
        /// Calclaute shipping rate 
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public List<zUSPSRate> CalculateShippingRate(string package, bool isSignatureRequired)
        {

            List<zUSPSRate> zUSPSRates = new List<zUSPSRate>();

            // Build the XML request string based on the set properties
            string xmlRequest = this.BuildRateRequestXmlStringV4(package, isSignatureRequired);

            // Build the API Request
            string apiRequest = this.BuildRequestUrlV4(this.UspsShippingAPIUrl, RateLookupQueryStringV4, HttpUtility.UrlEncode(xmlRequest));

            try
            {
                // Perform an synchronous search
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(apiRequest);
                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();

                //creating XML document
                var mySourceDoc = new XmlDocument();

                //load the file from the stream
                mySourceDoc.Load(stream);

                zUSPSRates = GetDomesticShippingRates(mySourceDoc);

                #region[Commented Code:: Old Code]

                //DataSet responseDataSet = new DataSet();
                //responseDataSet.ReadXml(stream);
                //isSuccess = this.ProcessV4RateResult(responseDataSet);

                #endregion
            }
            catch (Exception e)
            {
                // Set error code and description
                this.ErrorCode = "-1";
                this.ErrorDescription = e.Message;
                ZNodeLoggingBase.LogMessage("USPSExtns >> CalculateShippingRate(Package) >> " + e.ToString());

                Zeon.Libraries.Elmah.ElmahErrorManager.Log("USPSExtns >> CalculateShippingRate(Package) >> " + e.ToString());
            }

            return zUSPSRates;
        }

        /// <summary>
        /// Build Rate Request XmlString V4
        /// </summary>
        /// <param name="package">string</param>
        /// <param name="isSignatureRequired">bool</param>
        /// <returns>string</returns>
        private string BuildRateRequestXmlStringV4(string package, bool isSignatureRequired)
        {
            if (this.PDUZip5.Trim().Length > 5)
            {
                this.PDUZip5 = this.PDUZip5.Substring(0, 5);
            }
            StringBuilder requestNode = new StringBuilder();
            requestNode.AppendLine(string.Format("<RateV4Request USERID='{0}'>", this._UspsWebToolsUserId));
            if (isSignatureRequired) { requestNode.Append("<Revision>2</Revision>"); }
            requestNode.Append(string.Format(package, this.OriginZipCode, this.PDUZip5));
            requestNode.Append("</RateV4Request>");
            return requestNode.ToString().Trim();
        }

        /// <summary>
        /// Process the response from the USPS from the Rate Request API
        /// </summary>
        /// <param name="responseDataSet">Response dataset that contains response from USPS</param>
        /// <returns>Returns true if rate successfully completed othwerwise.</returns>
        private bool ProcessV4RateResult(DataSet responseDataSet)
        {
            bool isSuccess = true;

            // Reset the erro code and message
            this.ErrorCode = "0";
            this.ErrorDescription = string.Empty;

            // Determine if Error response was returned
            if (responseDataSet.Tables["Error"] == null && responseDataSet.Tables["Postage"] != null)
            {
                // Process Response
                DataTable tempTable = responseDataSet.Tables["Postage"];
                DataTable specialSevicesTempTable = responseDataSet.Tables["SpecialService"];
                //DataRow tempPackage = tempTable.Rows[0];

                if (tempTable != null && tempTable.Rows.Count > 0)
                {
                    decimal rate = 0;
                    foreach (DataRow row in tempTable.Rows)
                    {
                        rate += Decimal.Parse(row["Rate"].ToString());
                    }
                    this.ShippingRate = rate;
                }
                if (specialSevicesTempTable != null && specialSevicesTempTable.Rows.Count > 0)
                {
                    decimal rate = 0;
                    foreach (DataRow row in specialSevicesTempTable.Rows)
                    {
                        if (row["ServiceID"] != null && row["ServiceID"].ToString() == "15")
                        {
                            rate += Decimal.Parse(row["Price"].ToString());
                        }
                    }
                    this.ShippingRate = this.ShippingRate + rate;
                }
            }
            else
            {
                isSuccess = false;
                this.ErrorCode = responseDataSet.Tables["Error"].Rows[0]["Number"].ToString();
                this.ErrorDescription = responseDataSet.Tables["Error"].Rows[0]["Description"].ToString();
            }

            return isSuccess;
        }

        /// <summary>
        /// Get Usps WebTools UserId
        /// </summary>
        /// <returns>string</returns>
        private string GetUspsWebToolsUserId()
        {
            string uspsWebToolsUserId = string.Empty;
            try
            {
                TList<ZeonUSPSCredentials> zeonUSPSCredentialsList = new TList<ZeonUSPSCredentials>();
                ZNode.Libraries.DataAccess.Service.ZeonUSPSCredentialsService service = new DataAccess.Service.ZeonUSPSCredentialsService();
                zeonUSPSCredentialsList = service.GetByPortalID(ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID);
                if (zeonUSPSCredentialsList != null && zeonUSPSCredentialsList.Count > 0 && !string.IsNullOrEmpty(zeonUSPSCredentialsList[0].USPSUserName))
                {
                    ZNodeEncryption encrypt = new ZNodeEncryption();
                    uspsWebToolsUserId = encrypt.DecryptData(zeonUSPSCredentialsList[0].USPSUserName);
                }
                else
                {
                    uspsWebToolsUserId = System.Configuration.ConfigurationManager.AppSettings["USPSWebToolsUserID"].ToString();
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNode.Libraries.Shipping!!USPS!!Method>>GetUspsWebToolsUserId" + ex.ToString());
            }
            return uspsWebToolsUserId;
        }


        /// <summary>
        /// Builds the complete API Request
        /// </summary>
        /// <param name="apiUrl">USPS rate request Url</param>
        /// <param name="apiQueryString">USPS API query string.</param>
        /// <param name="xmlValue">Request XML string.</param>
        /// <returns>Returns the rate request Url to post.</returns>
        private string BuildRequestUrlV4(string apiUrl, string apiQueryString, string xmlValue)
        {
            string returnValue = string.Empty;
            returnValue = string.Format("{0}?{1}{2}", apiUrl, apiQueryString, xmlValue);
            return returnValue;
        }

        /// <summary>
        /// Get Domestic Shipping Rates
        /// </summary>
        /// <param name="_xmlRespone">System.Xml.XmlDocument</param>
        /// <returns>List<zUSPSRate></returns>
        private List<zUSPSRate> GetDomesticShippingRates(System.Xml.XmlDocument _xmlRespone)
        {
            List<zUSPSRate> zUSPSRates = new List<zUSPSRate>();
            try
            {
                zUSPSResponseEntity.RateV4Response rateV4Response = new zUSPSResponseEntity.RateV4Response();
                rateV4Response = ConvertXmlToObject(_xmlRespone, rateV4Response) as zUSPSResponseEntity.RateV4Response;


                if (rateV4Response != null && rateV4Response.Package.Count > 0)
                {
                    if (rateV4Response.Package != null && rateV4Response.Package.Count > 0)
                    {
                        foreach (zUSPSResponseEntity.Package package in rateV4Response.Package)
                        {
                            if (package.Postage != null && package.Postage.Count > 0)
                            {
                                foreach (zUSPSResponseEntity.Postage postage in package.Postage)
                                {
                                    zUSPSRate item = zUSPSRates != null && zUSPSRates.Count > 0 ? zUSPSRates.SingleOrDefault(o => o.ServiceName.Trim().Equals
                                                                                                            (
                                                                                                              HttpContext.Current.Server.HtmlDecode(postage.MailService.Trim())
                                                                                                              , StringComparison.OrdinalIgnoreCase
                                                                                                            )
                                                                                            && o.Code.Equals(postage.CLASSID)) : null;
                                    if (item != null && !string.IsNullOrEmpty(item.ServiceName))
                                    {
                                        item.MonetaryValue = item.MonetaryValue + decimal.Parse(postage.Rate);
                                        zUSPSRates.Remove(item);
                                        zUSPSRates.Add(item);
                                    }
                                    else
                                    {
                                        zUSPSRate zUSPSRate = new zUSPSRate();
                                        zUSPSRate.Code = postage.CLASSID;
                                        zUSPSRate.ServiceName = HttpContext.Current.Server.HtmlDecode(postage.MailService.Trim());
                                        zUSPSRate.MonetaryValue = decimal.Parse(postage.Rate);
                                        zUSPSRates.Add(zUSPSRate);
                                    }

                                }
                            }
                            else
                            {
                                string errorDesc = package.Error != null && !string.IsNullOrEmpty(package.Error.Description) ? package.Error.Description : "error while calculating usps rates";
                                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!! Error In ZNode.Libraries.Shipping!! USPS !!Method>>GetDomesticShippingRates !!" + errorDesc);
                            }

                        }
                    }
                    else
                    {

                        Zeon.Libraries.Elmah.ElmahErrorManager.Log("!! Error In ZNode.Libraries.Shipping!! USPS !!Method>>GetDomesticShippingRates !! Package Count = 0");
                    }
                }
                else
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!! Error In ZNode.Libraries.Shipping!! USPS !!Method>>GetDomesticShippingRates !!" + _xmlRespone.ToString());
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!! Error In ZNode.Libraries.Shipping!! USPS !!Method>>GetDomesticShippingRates !!" + ex.ToString());
            }


            return zUSPSRates;
        }


        #endregion

        #region[USPS International Shipping With V4 As it is upgaded version]


        /// <summary>
        /// Calculate USPS International Shipping Rates
        /// USPS Priority Mail International
        /// </summary>
        /// <returns></returns>
        public List<zUSPSRate> CalculateUSPSInternationalShippingRate(string package)
        {
            List<zUSPSRate> zUSPSRates = new List<zUSPSRate>();

            // Build the XML request string based on the set properties
            string xmlRequest = this.BuildRateRequestXmlStringForInternational(package);

            // Build the API Request
            string apiRequest = this.BuildRequestUrl(this.UspsShippingAPIUrl, RateLookupQueryStringForInternational, HttpUtility.UrlEncode(xmlRequest));

            try
            {
                // Perform an synchronous search
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(apiRequest);
                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();

                //creating XML document
                var mySourceDoc = new XmlDocument();

                //load the file from the stream
                mySourceDoc.Load(stream);

                //Retrive the List of USPS Rates
                zUSPSRates = this.GetUSPSInternationalShippingRates(mySourceDoc);
                //Log Error ON 'No Priority Shipping" Found:June 2015
                if (zUSPSRates.Count <= 0)
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!No Priotity Mail Shipping Found in  ZNode.Libraries.Shipping!! USPS !!Method>>CalculateUSPSInternationalShippingRate !!" + "Package:" + package + "Response:" + mySourceDoc.ToString());
                }
            }
            catch (Exception e)
            {
                // Set error code and description
                this.ErrorCode = "-1";
                this.ErrorDescription = e.Message;
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!! Error In ZNode.Libraries.Shipping!! USPS !!Method>>CalculateUSPSInternationalShippingRate !!" + e.ToString());

            }

            // return isSuccess;
            return zUSPSRates;
        }

        /// <summary>
        /// Build XML
        /// </summary>
        /// <returns>string</returns>
        private string BuildRateRequestXmlStringForInternational(string package)
        {
            StringBuilder requestNode = new StringBuilder();
            requestNode.AppendFormat("<IntlRateV2Request USERID='{0}'>", this._UspsWebToolsUserId);
            requestNode.AppendLine("<Revision>2</Revision>");
            requestNode.AppendLine(package);
            requestNode.AppendLine("</IntlRateV2Request>");
            return requestNode.ToString();
        }

        /// <summary>
        /// Process Rate Result
        /// USPS Priority Mail International
        /// </summary>
        /// <param name="responseDataSet">DataSet</param>
        /// <returns>string</returns>
        private bool ProcessInternationalRateResult(DataSet responseDataSet)
        {
            bool isSuccess = true;

            // Reset the erro code and message
            this.ErrorCode = "0";
            this.ErrorDescription = string.Empty;

            // Determine if Error response was returned
            if (responseDataSet.Tables["Error"] == null && responseDataSet.Tables["Service"] != null)
            {
                // Process Response
                DataTable tempTable = responseDataSet.Tables["Service"];
                tempTable.DefaultView.RowFilter = "[SvcDescription] = '" + _FilterValue + "'";
                DataTable dtPriorityMailRate = tempTable.DefaultView.ToTable();
                decimal rates = 0;
                if (dtPriorityMailRate != null && dtPriorityMailRate.Rows.Count > 0)
                {
                    foreach (DataRow row in dtPriorityMailRate.Rows)
                    {
                        rates += decimal.Parse(row["Postage"].ToString());
                    }
                }
                this.ShippingRate = rates;

            }
            else
            {
                isSuccess = false;
                this.ErrorCode = responseDataSet.Tables["Error"].Rows[0]["Number"].ToString();
                this.ErrorDescription = responseDataSet.Tables["Error"].Rows[0]["Description"].ToString();
            }

            return isSuccess;
        }

        /// <summary>
        /// Get USPS Shipping Rates for International Shipping
        /// </summary>
        /// <param name="_xmlRespone"></param>
        /// <returns></returns>
        private List<zUSPSRate> GetUSPSInternationalShippingRates(System.Xml.XmlDocument _xmlRespone)
        {
            List<zUSPSRate> zUSPSRates = new List<zUSPSRate>();
            try
            {
                zUSPSInternationalShippingResponse.IntlRateV2Response intlRateV2Response = new zUSPSInternationalShippingResponse.IntlRateV2Response();
                intlRateV2Response = ConvertXmlToObject(_xmlRespone, intlRateV2Response) as zUSPSInternationalShippingResponse.IntlRateV2Response;


                if (intlRateV2Response != null && intlRateV2Response.Package.Count > 0)
                {
                    if (intlRateV2Response.Package != null && intlRateV2Response.Package.Count > 0)
                    {
                        foreach (zUSPSInternationalShippingResponse.Package package in intlRateV2Response.Package)
                        {
                            if (package.Service != null && package.Error == null)
                            {
                                foreach (zUSPSInternationalShippingResponse.Service service in package.Service)
                                {
                                    if (!string.IsNullOrEmpty(service.SvcDescription) && service.SvcDescription.Equals(_FilterValue, StringComparison.OrdinalIgnoreCase))
                                    {
                                        zUSPSRate item = zUSPSRates != null && zUSPSRates.Count > 0 ? zUSPSRates.SingleOrDefault(o => o.ServiceName.Trim().Equals
                                                                                                                (
                                                                                                                  HttpContext.Current.Server.HtmlDecode(service.SvcDescription.Trim())
                                                                                                                  , StringComparison.OrdinalIgnoreCase
                                                                                                                )
                                                                                                && o.Code.Equals(service.ServiceID)) : null;
                                        if (item != null && !string.IsNullOrEmpty(item.ServiceName))
                                        {
                                            item.MonetaryValue = item.MonetaryValue + decimal.Parse(service.Postage);
                                            zUSPSRates.Remove(item);
                                            zUSPSRates.Add(item);
                                        }
                                        else
                                        {
                                            zUSPSRate zUSPSRate = new zUSPSRate();
                                            zUSPSRate.Code = service.ServiceID;
                                            zUSPSRate.ServiceName = HttpContext.Current.Server.HtmlDecode(service.SvcDescription.Trim());
                                            zUSPSRate.MonetaryValue = decimal.Parse(service.Postage);
                                            zUSPSRates.Add(zUSPSRate);
                                        }
                                    }

                                }
                            }
                            else
                            {
                                string errorDesc = package.Error != null && !string.IsNullOrEmpty(package.Error.Description) ? package.Error.Description : "error while calculating usps rates";
                                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!! Error In ZNode.Libraries.Shipping!! USPS !!Method>>GetUSPSInternationalShippingRates !!" + errorDesc);
                            }

                        }
                    }
                    else
                    {

                        Zeon.Libraries.Elmah.ElmahErrorManager.Log("!! Error In ZNode.Libraries.Shipping!! USPS !!Method>>GetUSPSInternationalShippingRates !! Package Count = 0");
                    }
                }
                else
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!! Error In ZNode.Libraries.Shipping!! USPS !!Method>>GetUSPSInternationalShippingRates !!" + _xmlRespone.ToString());
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!! Error In ZNode.Libraries.Shipping!! USPS !!Method>>GetUSPSInternationalShippingRates !!" + ex.ToString() + " Response:" + _xmlRespone.ToString());
            }

            

            return zUSPSRates;
        }


        #endregion

        #region[Helper Methods]

        /// <summary>
        /// Sets the public properties
        /// </summary>
        private object ConvertXmlToObject(XmlDocument xmlDoc, object classObjectName)
        {
            object obj = null;
            if (xmlDoc != null)
            {
                XmlNodeReader reader = new XmlNodeReader(xmlDoc.DocumentElement);
                XmlSerializer ser = new XmlSerializer(classObjectName.GetType());
                obj = ser.Deserialize(reader);
            }

            return obj;
        }
        #endregion
    }


}

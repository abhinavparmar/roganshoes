﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Shipping
{
    public class zUSPSTrackingResponse
    {
        public zUSPSTrackingResponse()
        {
        }
        public bool ResponseStatus { get; set; }
        public USPSTrackInfo TrackInfo { get; set; }
        public List<USPSTrackDetail> TrackDetailList { get; set; }

    }

    public class USPSTrackInfo
    {
        public USPSTrackInfo()
        {
        }
        public string TrackSummary { get; set; }
        public string TrackInfo_id { get; set; }
        public string ID { get; set; }
    }

    public class USPSTrackDetail
    {
        public USPSTrackDetail()
        {
        }
        public string TrackDetail_Text { get; set; }
        public string TrackInfo_id { get; set; }
    }
}

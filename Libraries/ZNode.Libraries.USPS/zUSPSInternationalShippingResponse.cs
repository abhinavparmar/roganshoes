﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ZNode.Libraries.Shipping
{
    public class zUSPSInternationalShippingResponse
    {
        [Serializable]
        [XmlRoot("IntlRateV2Response")]
        public class IntlRateV2Response
        {
            [XmlElement("Package")]
            public List<Package> Package { get; set; }
        }

        public class Package
        {
            [XmlAttribute("ID")]
            public string PackageID { get; set; }

            [XmlElement("Service")]
            public List<Service> Service { get; set; }

            [XmlElement("Error")]
            public Error Error { get; set; }
        }

        public class Service
        {
            [XmlAttribute("ID")]
            public string ServiceID { get; set; }
            public string Postage { get; set; }
            public string SvcDescription { get; set; }

        }

        public class Error
        {
            public string Number { get; set; }
            public string Source { get; set; }
            public string Description { get; set; }
        }

        
    }
}

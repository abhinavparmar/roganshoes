﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ZNode.Libraries.Shipping
{
    public class zUSPSRate
    {
        public string Code { get; set; }
        public string ServiceName { get; set; }
        public Decimal MonetaryValue { get; set; }
    }

    public class zUSPSResponseEntity
    {
        /// <summary>
        /// Domestic Shipping Response
        /// </summary>
        [Serializable]
        [XmlRoot("RateV4Response")]
        public class RateV4Response
        {
            [XmlElement("Package")]
            public List<Package> Package { get; set; }
        }

        public class Package
        {
            [XmlAttribute("ID")]
            public string PackageID { get; set; }

            [XmlElement("Postage")]
            public List<Postage> Postage { get; set; }

            [XmlElement("Error")]
            public Error Error { get; set; }

        }

        public class Postage
        {
            [XmlAttribute("CLASSID")]
            public string CLASSID { get; set; }
            public string MailService { get; set; }
            public string Rate { get; set; }
        }

        public class Error
        {
            public string Number { get; set; }
            public string Source { get; set; }
            public string Description { get; set; }
        }
    }
    
}

using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    public partial class ZNodePromotionAmountOffProduct : ZNodePromotionRule
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePromotionAmountOffProduct class.
        /// </summary>
        public ZNodePromotionAmountOffProduct()
            : base()
        {
        }

        /// <summary>
        /// Override the Calculate method to calculate amount of product discount for coupon and non-coupon based promtions.
        /// </summary>
        public override void Calculate()
        {
            //Zeon Custom Code : Start
            this.CalculateExtn();
            //Zeon Custom Code : End
            #region[Znode Default code]
            /*
            decimal basePrice = 0;

            var isCouponValid = ValidateCoupon();
            isCouponValid &= PromotionProperty.Coupon != null;

            var subTotal = ShoppingCart.SubTotal;

            // Loop through each cart Item
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                if (cartItem.Product.ProductID == PromotionProperty.ProductId)
                {
                    // Tiered Pricing calcaultion                
                    basePrice = cartItem.TieredPricing;
                    var qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

                    #region[Znode default code]
                    //if (PromotionProperty.Coupon == null && PromotionProperty.QuantityMinimum <= qtyOrdered && PromotionProperty.OrderMinimum <= subTotal)
                    #endregion

                    //Zeon Customization :: Promotion is not applied if IsMapPrice =  true  :: applicable only when cupon code is applied
                    if (PromotionProperty.Coupon == null && PromotionProperty.QuantityMinimum <= qtyOrdered && PromotionProperty.OrderMinimum <= subTotal && cartItem.Product.IsMapPrice == false)
                    {
                        // Product Discount amount
                        cartItem.Product.DiscountAmount += PromotionProperty.Discount;
                    }
                    else if (isCouponValid)
                    {
                        // apply discount
                        if (PromotionProperty.Coupon.QuantityMinimum <= qtyOrdered && PromotionProperty.Coupon.OrderMinimum <= subTotal)
                        {
                            // Product Discount amount
                            cartItem.Product.DiscountAmount += PromotionProperty.Coupon.Discount;

                            PromotionProperty.Coupon.CouponApplied = true;
                            ShoppingCart.CouponApplied = true;
                        }
                    }
                }
            }

            // Add Coupon display message
            if (PromotionProperty.Coupon != null)
            {
                AddPromotionMessage();
            }
            */
            #endregion
        }
    }
}

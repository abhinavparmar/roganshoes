﻿using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Property bag for settings used by Promotion Rules. 
    /// </summary>
	public partial class ZNodePromotionProperty : ZNodeBusinessBase
    {
        private decimal _Discount = 0;
        private decimal _OrderMinimum = 0;
        private int _QuantityMinimum = 0;
        private int _ProductId = 0;
        private ZNodeCoupon _Coupon = null;
        private int _PromotionProductId = 0;
        private int _PromotionalProductQuantity = 0;

        /// <summary>
        /// Gets or sets the coupon code for this promotion rule. If a coupon code is not required then leave blank.
        /// </summary>
        public ZNodeCoupon Coupon
        {
            get { return _Coupon; }
            set { _Coupon = value; }
        }

        /// <summary>
        /// Gets or sets the Product Id
        /// </summary>
        public int ProductId
        {
            get { return _ProductId; }
            set { _ProductId = value; }
        }

        /// <summary>
        /// Gets or sets the promotion product id
        /// </summary>
        public int PromotionProductId
        {
            get { return _PromotionProductId; }
            set { _PromotionProductId = value; }
        }

        /// <summary>
        /// Gets or sets the promotional product quantity
        /// </summary>
        public int PromotionalProductQuantity
        {
            get { return _PromotionalProductQuantity; }
            set { _PromotionalProductQuantity = value; }
        }

        /// <summary>
        /// Gets or sets the discount
        /// </summary>
        public decimal Discount
        {
            get { return _Discount; }
            set { _Discount = value; }
        }

        /// <summary>
        /// Gets or sets the order minimum
        /// </summary>
        public decimal OrderMinimum
        {
            get { return _OrderMinimum; }
            set { _OrderMinimum = value; }
        }

        /// <summary>
        /// Gets or sets the quantity minimum
        /// </summary>
        public int QuantityMinimum
        {
            get { return _QuantityMinimum; }
            set { _QuantityMinimum = value; }
        }

        /// <summary>
        /// Gets or sets the quantity minimum
        /// </summary>
        public int? ProfileId
        {
            get;
            set;
        }
    }
}

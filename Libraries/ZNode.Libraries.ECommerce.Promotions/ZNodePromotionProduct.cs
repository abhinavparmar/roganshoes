﻿using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
	public class ZNodePromotionProduct : ZNodePromotionPricingRule
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePromotionProduct class.
        /// </summary>
        public ZNodePromotionProduct() : base()
        {

        }

        /// <summary>
        /// Apply this promotion and return the promotion with updated product information.
        /// </summary>
        /// <param name="Product">The Product on which the Promotional price is to be calculated on.</param>
        /// <param name="CurrentPrice">The current promotional price that has been calculated.</param>
        /// <remarks> Normally promotions will be calculated on the Product's base price. 
        /// Here we are overriding All Possible ZNodeProductBaseEntity properties.</remarks>
        /// <returns>Returns the promotional price with updated product object</returns>
        public override decimal PromotionalPrice(ZNodeProductBaseEntity Product, decimal CurrentPrice)
        {
            // Check Promotion already applied.
            if (!Product.IsPromotionApplied)
            {
                // ***
                // Put in whatever logic you want here to override the product (ZNodeProductBaseEntity object) properties in this promotion.             
                // If you want override Product Name and Description for all product for whole sale, do like following code
                // ***
                //      Product.Name = Product.Name + "Whole Sale Product";
                //      Product.Description = "Whole Sale description";                

                // Use this property to ignore second time promotion apply.
                Product.IsPromotionApplied = true;
            }

            return CurrentPrice;
        }    
    }
}

using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Base class for handling pricing changes of products before they are put into the cart based on a promotion.
    /// </summary>
    public partial class ZNodePromotionPricingRule : IZNodePromotionPricingRule
    {
        private ZNodePromotionProperty _PromotionProperty;
        private int _Precedence = 0;

        /// <summary>
        /// Initializes a new instance of the ZNodePromotionPricingRule class.
        /// </summary>
        protected ZNodePromotionPricingRule()
        {
        }

        /// <summary>
        /// Gets or sets the order of precidence that this promotions should be applied.
        /// </summary>
        public int Precedence
        {
            get { return _Precedence; }
            set { _Precedence = value; }
        }

        /// <summary>
        /// Gets or sets the promotions
        /// </summary>
        protected ZNodePromotionProperty PromotionProperty
        {
            get { return _PromotionProperty; }
            set { _PromotionProperty = value; }
        }

        /// <summary>
        /// Binds the Promtion data to the Promotion Rule.
        /// </summary>
        /// <param name="PromotionProperty">The a property bag of promotion settings..</param>
        public void Bind(ZNodePromotionProperty PromotionProperty)
        {
            _PromotionProperty = PromotionProperty;
        }

        /// <summary>
        /// Calculate this promotion and return the price after the promotion.
        /// </summary>
        /// <param name="Product">The Product on which the Promotional price is to be calculated on.</param>
        /// <param name="CurrentPrice">The current promotional price that has been calculated.</param>
        /// <remarks> Normally promotions will be calculated on the Product's base price. 
        /// CurrentPrice is include just in case you need to calculate promotions based on other promotions that have already been applied.</remarks>
        /// <returns>Returns the promotional price</returns>
        public virtual decimal PromotionalPrice(ZNodeProductBaseEntity Product, decimal CurrentPrice)
        {
            return 0;
        }

        /// <summary>
        /// Validate the coupon codes.
        /// </summary>
        /// <returns>Returns if the coupon is valid or not</returns>
        protected bool ValidateCoupon()
        {
            var isValid = false;
            return isValid;
        }

    }
}

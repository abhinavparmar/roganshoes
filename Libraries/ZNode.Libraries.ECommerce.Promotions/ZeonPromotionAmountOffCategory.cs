﻿using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.DataAccess.Custom;
using System.Linq;
using System;

namespace ZNode.Libraries.ECommerce.Promotions
{
    public class ZeonPromotionAmountOffCategory : ZNodePromotionPricingRule
    {
        public ZeonPromotionAmountOffCategory()
            : base()
        {

        }

        /// <summary>
        /// Calculate category based promotion and return the price after the promotion.
        /// </summary>
        /// <param name="Product">The Product on which the Promotional price is to be calculated on.</param>
        /// <param name="CurrentPrice">The current promotional price that has been calculated.</param>
        /// <remarks> Normally promotions will be calculated on the Product's base price. 
        /// CurrentPrice is include just in case you need to calculate promotions based on other promotions that have already been applied.</remarks>
        /// <returns>Returns the promotional price</returns>
        public override decimal PromotionalPrice(ZNodeProductBaseEntity product, decimal CurrentPrice)
        {
            var discountedPrice = CurrentPrice;
            try
            {
                #region[update logic for promotion:: no need to check category hirachy:: check if product category belong to promotion category applied then only appy promotion]

                string productExcludeDiscount = product != null && !string.IsNullOrEmpty(product.Custom1) ? product.Custom1 : "0";
                if (discountedPrice > 0 && productExcludeDiscount.Equals("0") && product != null && product.IsMapPrice == false)
                {
                    ZNode.Libraries.Framework.Business.ZNodeGenericCollection<ZNodeProductCategoryList> productCategoryList = GetZNodeProductCategoryList(product);
                    if (productCategoryList != null && productCategoryList.Count > 0)
                    {
                        foreach (ZNodeProductCategoryList prodCategory in productCategoryList)
                        {
                            if (PromotionProperty != null && PromotionProperty.CategoryID.Equals(prodCategory.CategoryID) && product.IsCustomPromotionApplied == false)
                            {
                                discountedPrice -= PromotionProperty.Discount;
                                product.IsCustomPromotionApplied = true;
                                break;
                            }
                            else if (PromotionProperty == null || (PromotionProperty != null && PromotionProperty.CategoryID == 0))
                            {
                                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Success in PromotionalPrice!!CategoryID=0");
                            }
                        }
                    }
                    else
                    {
                        Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in PromotionalPrice!!productCategoryList is null either productCategoryList.Count=0");
                    }


                    if (product.IsCustomPromotionApplied == false)
                    {
                        ProductCategoryIds = GetProductCategoryIDsByProductID(product);
                        if (ProductCategoryIds != null && ProductCategoryIds.Length > 0)
                        {
                            foreach (var item in ProductCategoryIds)
                            {
                                // Get the catalogs for the category
                                if (PromotionProperty != null && PromotionProperty.CategoryID.Equals(int.Parse(item)))
                                {
                                    discountedPrice -= PromotionProperty.Discount;
                                    product.IsCustomPromotionApplied = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (productCategoryList != null && productCategoryList.Count > 0 && !product.IsCustomPromotionApplied)
                    {
                        Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in PromotionalPrice!!Promotion is executed but not applied!!ProductID=" + product.ProductID);
                    }

                }

                if (discountedPrice < 0)
                {
                    discountedPrice = 0;
                }

                #endregion

            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in  ZeonPromotionAmountOffCategory!!" + ex.ToString());
            }
            return discountedPrice;
        }
    }
}

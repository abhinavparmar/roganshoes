using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Represents the Promotion Amount Off X if Y Purchased
    /// </summary>
	public class ZNodePromotionAmountOffXifYPurchased : ZNodePromotionRule
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePromotionAmountOffXifYPurchased class.
        /// </summary>
        public ZNodePromotionAmountOffXifYPurchased() : base()
        {
        }

        /// <summary>
        /// Override the Calculate method to calculate amount of shipping discount for coupon and non-coupon based promtions 
        /// </summary>
        public override void Calculate()
        {
            var isCouponValid = ValidateCoupon();
            isCouponValid &= PromotionProperty.Coupon != null;

            // Loop through each cart Item
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                if (cartItem.Product.ProductID == PromotionProperty.PromotionProductId)
                {
                    // Tiered Pricing calcaultion                
                    var unitPrice = cartItem.TieredPricing;
                    var extendedPrice = unitPrice * PromotionProperty.PromotionalProductQuantity;

                    var isLastItem = IsLastItem(cartItem);
                    var productQty = GetQuantityOrdered(PromotionProperty.ProductId);
                    var qtyMinimum = PromotionProperty.QuantityMinimum;
                    var promotionalProductQty = cartItem.Quantity;

                    if (PromotionProperty.Coupon == null && isLastItem)
                    {
                        if (PromotionProperty.PromotionProductId == PromotionProperty.ProductId)
                        {
                            if (productQty == promotionalProductQty)
                            {
                                promotionalProductQty = cartItem.Quantity - qtyMinimum;
                            }
                            
                            qtyMinimum += PromotionProperty.PromotionalProductQuantity;
                        }

                        if (productQty >= qtyMinimum && PromotionProperty.PromotionalProductQuantity <= promotionalProductQty)
                        {
                            cartItem.ExtendedPriceDiscount += extendedPrice < PromotionProperty.Discount ? extendedPrice : PromotionProperty.Discount;
                            break;
                        }
                    }
                    else if (isLastItem && isCouponValid)
                    {
                        qtyMinimum = PromotionProperty.Coupon.QuantityMinimum;

                        if (PromotionProperty.PromotionProductId == PromotionProperty.ProductId)
                        {
                            if (productQty == promotionalProductQty)
                            {
                                promotionalProductQty = cartItem.Quantity - qtyMinimum;
                            }

                            qtyMinimum += PromotionProperty.PromotionalProductQuantity;
                        }

                        if (productQty >= qtyMinimum  && PromotionProperty.PromotionalProductQuantity <= promotionalProductQty)
                        {
                            cartItem.ExtendedPriceDiscount += extendedPrice < PromotionProperty.Coupon.Discount ? extendedPrice : PromotionProperty.Coupon.Discount;

                            PromotionProperty.Coupon.CouponApplied = true;
                            ShoppingCart.CouponApplied = true;
                            break;
                        }
                    }
                }
            }

            // Add Coupon display message
            AddPromotionMessage();
        }    
    }
}

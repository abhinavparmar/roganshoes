using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Promotion Rule for calculating Percentage off an item if other items are purchased.
    /// </summary>
	public class ZNodePromotionPercentXifYPurchased : ZNodePromotionRule
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePromotionPercentXifYPurchased class.
        /// </summary>
        public ZNodePromotionPercentXifYPurchased() : base()
        {
        }

        /// <summary>
        /// Do promotion calculation.
        /// </summary>
        public override void Calculate()
        {
            decimal extendedPrice = 0;

            var isCouponValid = ValidateCoupon();
            isCouponValid &= PromotionProperty.Coupon != null;
            
            // Loop through each cart Item
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                if (cartItem.Product.ProductID == PromotionProperty.PromotionProductId)
                {
                    // Tiered Pricing calcaultion                
                    var unitPrice = cartItem.TieredPricing;                    
                    var productQty = GetQuantityOrdered(PromotionProperty.ProductId);
                    var qtyMinimum = PromotionProperty.QuantityMinimum;
                    var promotionalProductQty = cartItem.Quantity;

                    extendedPrice = unitPrice * PromotionProperty.PromotionalProductQuantity;

                    if (PromotionProperty.Coupon == null)
                    {
                        if (PromotionProperty.PromotionProductId == PromotionProperty.ProductId)
                        {
                            if (productQty == promotionalProductQty)
                            {
                                promotionalProductQty = cartItem.Quantity - qtyMinimum;
                            }
                            
                            qtyMinimum += PromotionProperty.PromotionalProductQuantity;
                        }

                        if (productQty >= qtyMinimum && PromotionProperty.PromotionalProductQuantity <= promotionalProductQty)
                        {
                            cartItem.ExtendedPriceDiscount += extendedPrice * (PromotionProperty.Discount / 100);
                            break;
                        }
                    }
                    else if (isCouponValid)
                    {
                        qtyMinimum = PromotionProperty.Coupon.QuantityMinimum;

                        if (PromotionProperty.PromotionProductId == PromotionProperty.ProductId)
                        {
                            if (productQty == promotionalProductQty)
                            {
                                promotionalProductQty = cartItem.Quantity - qtyMinimum;
                            }

                            qtyMinimum += PromotionProperty.PromotionalProductQuantity;
                        }

                        if (productQty >= qtyMinimum  && PromotionProperty.PromotionalProductQuantity <= promotionalProductQty)
                        {
                            cartItem.ExtendedPriceDiscount += extendedPrice * (PromotionProperty.Coupon.Discount / 100);

                            PromotionProperty.Coupon.CouponApplied = true;
                            ShoppingCart.CouponApplied = true;
                            break;
                        }
                    }
                }
            }

            // Add Coupon display message
            if (PromotionProperty.Coupon != null)
            {
                AddPromotionMessage();
            }
        }
    }
}

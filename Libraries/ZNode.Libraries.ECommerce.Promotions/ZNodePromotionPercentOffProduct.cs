using System;
using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Promotion rule for doing percent off Product.
    /// </summary>
    public class ZNodePromotionPercentOffProduct : ZNodePromotionRule
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePromotionPercentOffProduct class.
        /// </summary>
        public ZNodePromotionPercentOffProduct()
            : base()
        {
        }

        /// <summary>
        /// Do promotion calculation.
        /// </summary>
        public override void Calculate()
        {
            try
            {
                decimal subTotal = ShoppingCart.SubTotal;
                decimal basePrice = 0;

                var isCouponValid = ValidateCoupon();
                isCouponValid &= PromotionProperty.Coupon != null;

                var pricingOption = new ZNodePromotionPricingOption();

                // Loop through each cart Item
                foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
                {
                    string productExcludeDiscount = !string.IsNullOrEmpty(cartItem.Product.Custom1) ? cartItem.Product.Custom1 : "0";
                    if (productExcludeDiscount.Equals("0") && cartItem.Product.ProductID == PromotionProperty.ProductId)
                    {
                        // Tiered Pricing calcaultion
                        basePrice = cartItem.TieredPricing;
                        basePrice = pricingOption.PromotionalPrice(cartItem.Product, basePrice);

                        var qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);
                        #region[Znode default code]
                        //if (PromotionProperty.QuantityMinimum <= cartItem.Quantity && PromotionProperty.Coupon == null && PromotionProperty.OrderMinimum <= subTotal)
                        #endregion

                        //Zeon Customization :: Promotion is not applied if IsMapPrice =  true  :: applicable only when cupon code is applied
                        if (PromotionProperty.QuantityMinimum <= cartItem.Quantity && PromotionProperty.Coupon == null && PromotionProperty.OrderMinimum <= subTotal && cartItem.Product.IsMapPrice == false)
                        {
                            // Product Discount amount
                            cartItem.Product.DiscountAmount += basePrice * (PromotionProperty.Discount / 100);
                            cartItem.Product.PromotionDiscountPercent += PromotionProperty.Discount;
                        }
                        else if (isCouponValid)
                        {
                            // apply discount
                            if (PromotionProperty.Coupon.QuantityMinimum <= qtyOrdered && PromotionProperty.Coupon.OrderMinimum <= subTotal)
                            {
                                var discount = basePrice * (PromotionProperty.Coupon.Discount / 100);

                                // Product Discount amount
                                cartItem.Product.DiscountAmount += discount;
                                //Zeon Custom Discount Percent Implementation:Starts
                                cartItem.Product.PromotionDiscountPercent += PromotionProperty.Coupon.Discount;
                                //Zeon Custom Discount Percent Implementation:Ends
                                PromotionProperty.Coupon.CouponApplied = true;
                                ShoppingCart.CouponApplied = true;
                            }
                        }
                    }
                }

                // Add Coupon display message            
                AddPromotionMessage();
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePromotionPercentOffProduct!!" + ex.ToString());
            }
        }
        
    }
}

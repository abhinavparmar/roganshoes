﻿using System;
using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    public partial class ZNodePromotionAmountOffOrder : ZNodePromotionRule
    {
        /// <summary>
        /// Override the Calculate method to calculate amount of order discount for coupon and non-coupon based promtions.
        /// </summary>
        public void CalculateExt()
        {
            try
            {
                //var subTotal = ShoppingCart.SubTotal;
                var subTotal = 0.0M;
                var itemCount = 0;

                // Count the CartItem and AddOn
                foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
                {
                    if (cartItem.PromotionalPrice != null && cartItem.PromotionalPrice > 0)
                    {
                        var unitPrice = cartItem.PromotionalPrice * cartItem.Quantity;
                        subTotal += unitPrice;
                    }

                    //discount can not be applied if Custom1 = ‘1’
                    string productExcludeDiscount = !string.IsNullOrEmpty(cartItem.Product.Custom1) ? cartItem.Product.Custom1 : "0";
                    if (productExcludeDiscount.Equals("0"))
                    {
                        var lineItemCount = 1;

                        foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                        {
                            foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                            {
                                if (addOnValue.Price > 0)
                                {
                                    lineItemCount++;
                                }
                            }
                        }

                        itemCount = itemCount + (lineItemCount * cartItem.Quantity);
                    }
                }

                if (subTotal <= 0)
                {
                    subTotal = ShoppingCart.SubTotal;
                }

                if (PromotionProperty.OrderMinimum <= subTotal && PromotionProperty.Coupon == null)
                {
                    if (itemCount > 0)
                    {
                        var lineItemDiscount = PromotionProperty.Discount / itemCount;
                        this.ApplyDiscountExtn(lineItemDiscount);
                    }
                }
                else if (PromotionProperty.Coupon != null)
                {
                    var isCouponValid = ValidateCoupon();

                    // apply discount
                    if (PromotionProperty.Coupon.OrderMinimum <= subTotal && isCouponValid)
                    {
                        if (itemCount > 0)
                        {
                            var lineItemDiscount = PromotionProperty.Coupon.Discount / itemCount;

                            // Helper Method to calculate discount
                            this.ApplyDiscountExtn(lineItemDiscount);

                            PromotionProperty.Coupon.CouponApplied = true;
                            ShoppingCart.CouponApplied = true;
                        }


                    }

                    AddPromotionMessage();
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePromotionAmountOffOrder!!Method!!CalculateExt!!" + ex.ToString());
            }
        }

        /// <summary>
        /// Apply the discount
        /// </summary>
        /// <param name="lineItemDiscount">Line Item Discount</param>
        private void ApplyDiscountExtn(decimal lineItemDiscount)
        {
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                //discount can not be applied if Custom1 = ‘1’
                string productExcludeDiscount = !string.IsNullOrEmpty(cartItem.Product.Custom1) ? cartItem.Product.Custom1 : "0";
                if (productExcludeDiscount.Equals("0"))
                {
                    cartItem.Product.DiscountAmount += lineItemDiscount;

                    foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                        {
                            if (addOnValue.Price > 0)
                            {
                                addOnValue.DiscountAmount += lineItemDiscount;
                            }
                        }
                    }
                }
            }
        }
    }
}

using System;
using System.Web;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Provides services for intantiating and managing promotion rules as well as applying Promotions to pricing.
    /// </summary>
    public partial class ZNodePromotionPricingOption
    {
        private ZNodeGenericCollection<IZNodePromotionPricingRule> _PromotionRules = null;

        //Zeon Custom Code : Start
        private ZeonPromotionTypeCollectionIds zeonPromotionTypeCollectionIds = null;
        //Zeon Custom Code : End

        /// <summary>
        /// Get the promotions from the cache.
        /// </summary>
        protected TList<Promotion> PromotionCache
        {
            get
            {

                var promotionList = new TList<Promotion>();

                // Application object holds the list of active promotions
                if (HttpRuntime.Cache["PromotionCache"] == null)
                {
                    CacheActivePromotions();
                }

                //
                if (HttpRuntime.Cache["PromotionCache"] != null)
                {
                    promotionList = HttpRuntime.Cache["PromotionCache"] as TList<Promotion>;
                }

                if (promotionList == null || (promotionList != null && promotionList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:- Error in PromotionExtnCache!!Reload Promotion Cache");
                    CacheActivePromotions();
                    if (HttpRuntime.Cache["PromotionCache"] != null)
                    {
                        promotionList = HttpRuntime.Cache["PromotionCache"] as TList<Promotion>;
                    }
                }

                //promotionList.Sort("DisplayOrder");

                return promotionList;
            }
        }

        protected TList<PromotionExtn> PromotionExtnCache
        {
            get
            {
                var promotionExtnList = new TList<PromotionExtn>();

                TList<Promotion> promotionList = PromotionCache;

                // Application object holds the list of active promotions
                if (HttpRuntime.Cache["PromotionExtnCache"] == null)
                {
                    CacheActivePromotionsExtn();
                }

                //
                if (HttpRuntime.Cache["PromotionExtnCache"] != null)
                {
                    promotionExtnList = HttpRuntime.Cache["PromotionExtnCache"] as TList<PromotionExtn>;
                }

                if (promotionExtnList == null && (promotionExtnList != null && promotionExtnList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:-Error in PromotionExtnCache!!Reload PromotionExtn Cache");
                    CacheActivePromotionsExtn();
                    if (HttpRuntime.Cache["PromotionExtnCache"] != null)
                    {
                        promotionExtnList = HttpRuntime.Cache["PromotionExtnCache"] as TList<PromotionExtn>;
                    }
                }

                if (promotionExtnList != null && promotionExtnList.Count > 0 && promotionList != null && promotionList.Count != promotionExtnList.Count)
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!ZNodePromotionPricingOption:- !Error in PromotionExtnCache!!Promotion="+promotionList.Count+" and PromotionExt="+ promotionExtnList.Count +"Data is Mismatch");                   
                }

                return promotionExtnList;
            }
        }

        /// <summary>
        /// This constructor will instantiate all the appropriate promotion rules that are required.
        /// </summary>
        //public ZNodePromotionPricingOption()
        //{
        //    _PromotionRules = new ZNodeGenericCollection<IZNodePromotionPricingRule>();

        //    System.Reflection.Assembly promoRuleAssebmly = System.Reflection.Assembly.GetExecutingAssembly();

        //    var assemblyName = promoRuleAssebmly.GetName().Name;

        //    var promotionService = new PromotionService();
        //    var promotionList = (TList<Promotion>)PromotionCache.Clone();

        //    Profile profile = null;

        //    if (HttpContext.Current.Session["ProfileCache"] != null)
        //    {
        //        // Retrieve profile
        //        profile = HttpContext.Current.Session["ProfileCache"] as Profile;
        //    }

        //    int? CurrentProfileID = null;
        //    if (profile != null)
        //    {
        //        CurrentProfileID = profile.ProfileID;
        //    }

        //    promotionList.ApplyFilter(delegate(Promotion promo) { return (promo.ProfileID == CurrentProfileID || promo.ProfileID == null) && promo.DiscountTypeIDSource.ClassType.Equals("PRICE", StringComparison.OrdinalIgnoreCase); });
        //    promotionList.Sort("DisplayOrder");

        //    // Build a list of rules that are appropriate for this instance of this class from the Promotion Cache.            
        //    // Promotion Rule Factory

        //    zeonPromotionTypeCollectionIds = new ZeonPromotionTypeCollectionIds();

        //    foreach (Promotion promo in promotionList)
        //    {
        //        var promotionProperty = new ZNodePromotionProperty();
        //        promotionProperty.Discount = promo.Discount;
        //        promotionProperty.OrderMinimum = promo.OrderMinimum.GetValueOrDefault(0);
        //        promotionProperty.Discount = promo.Discount;
        //        promotionProperty.ProductId = promo.ProductID.GetValueOrDefault(0);
        //        promotionProperty.QuantityMinimum = promo.QuantityMinimum.GetValueOrDefault(1);
        //        promotionProperty.PromotionProductId = promo.PromotionProductID.GetValueOrDefault(0);
        //        promotionProperty.PromotionalProductQuantity = promo.PromotionProductQty.GetValueOrDefault(1);

        //        promotionProperty.ProfileId = CurrentProfileID;

        //        if (promo.CouponInd)
        //        {
        //            promotionProperty.Coupon = GetCoupon(promo);
        //        }

        //        if (!string.IsNullOrEmpty(promo.PromotionMessage) && !promo.CouponInd)
        //        {
        //            if (promotionProperty.Coupon == null)
        //            {
        //                promotionProperty.Coupon = new ZNodeCoupon();
        //            }

        //            promotionProperty.Coupon.PromotionMessage = promo.PromotionMessage;
        //        }

        //        var className = promo.DiscountTypeIDSource.ClassName;

        //        // Instantiate the Promotion based on class name.
        //        if (!string.IsNullOrEmpty(className))
        //        {
        //            try
        //            {
        //                var promoRule = (IZNodePromotionPricingRule)promoRuleAssebmly.CreateInstance(assemblyName + "." + className);
        //                promoRule.Precedence = promo.DisplayOrder;

        //                promoRule.Bind(promotionProperty);
        //                _PromotionRules.Add(promoRule);
        //            }
        //            catch (Exception ex)
        //            {
        //                ZNodeLogging.LogMessage("Error while instantiating promotion: " + className);
        //                ZNodeLogging.LogObject(typeof(Exception), ex);
        //            }
        //        }

        //        #region[Zeon Custom Code]

        //        //Zeon Custom Code::Changes to Set Value for ProtalID ,CatalogID,CategoryID,ManufactureID,PromotionID :: Start
        //        PromotionExtn promoExt = null;
        //        if (promo != null && promo.PromotionExtnCollection != null && promo.PromotionExtnCollection.Count > 0)
        //        {
        //            // Assign Promotion Extension Details
        //            promoExt = new PromotionExtn();
        //            promoExt = promo.PromotionExtnCollection[0];
        //        }
        //        else if (promo != null)
        //        {
        //            var promotionExtnList = (TList<PromotionExtn>)PromotionExtnCache.Clone();
        //            promotionExtnList.ApplyFilter(delegate(PromotionExtn promoExtn)
        //            {
        //                return (promoExtn.PromotionID == promo.PromotionID);
        //            });

        //            if (promotionExtnList != null && promotionExtnList.Count > 0 && promotionExtnList[0].PromotionID == promo.PromotionID)
        //            {
        //                promoExt = promotionExtnList[0];
        //            }
        //            else
        //            {
        //                TList<PromotionExtn> promoextList = new TList<PromotionExtn>();
        //                PromotionExtnService promoextService = new PromotionExtnService();
        //                promoextList = promoextService.GetByPromotionID(promo.PromotionID);
        //                if (promoextList != null && promoextList.Count > 0)
        //                {
        //                    promoExt = new PromotionExtn();
        //                    promoExt = promoextList[0];
        //                }
        //            }

        //            if (promoExt == null && (promoExt != null && promoExt.CategoryID.HasValue == false))
        //            {
        //                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePromotionPricingOption()!!promoExt is null or promoExt.CategoryID=0");
        //            }

        //        }

        //        if (promoExt != null)
        //        {
        //            promotionProperty.CatalogID = promoExt.CatalogID.GetValueOrDefault(0);

        //            int promotionCategoryID = promoExt.CategoryID.GetValueOrDefault(0);
        //            if (promotionCategoryID > 0)
        //            {
        //                promotionProperty.CategoryID = promotionCategoryID;
        //            }
        //            else
        //            {
        //                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePromotionPricingOption()!!promotion categoryid is 0");
        //            }

        //            promotionProperty.ManufacturerID = promoExt.ManufacturerID.GetValueOrDefault(0);
        //            promotionProperty.PortalID = promoExt.PortalID.GetValueOrDefault(0);
        //            promotionProperty.Custom1 = promo.Custom1;
        //            promotionProperty.Custom2 = promo.Custom2;
        //            promotionProperty.PromotionID = promo.PromotionID;

        //            if (promoExt.ManufacturerID.GetValueOrDefault(0) > 0)
        //            {
        //                zeonPromotionTypeCollectionIds.BrandIDs.Add(promoExt.ManufacturerID.GetValueOrDefault(0));
        //            }
        //            if (promoExt.CategoryID.GetValueOrDefault(0) > 0)
        //            {
        //                zeonPromotionTypeCollectionIds.CategoryIDs.Add(promoExt.CategoryID.GetValueOrDefault(0));
        //            }
        //        }
        //        else
        //        {
        //            Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePromotionPricingOption()!!promoExt is null");
        //        }

        //        if (promo.ProductID.GetValueOrDefault(0) > 0)
        //        {
        //            zeonPromotionTypeCollectionIds.ProductIDs.Add(promo.ProductID.GetValueOrDefault(0));
        //        }

        //        //Zeon Custom Code::Changes to Set Value for ProtalID ,CatalogID,CategoryID,ManufactureID,PromotionID :: End

        //        #endregion
        //    }

        //    promotionList.Dispose();
        //}

        /// <summary>
        /// Calculate the final Promotional price for the given Product.
        /// </summary>
        /// <param name="Product">The Product on which the Promotional price is to be calculated on.</param>
        /// <returns></returns>
        /// <remarks>CurrentPrice is include just in case you need to calculate promotions based on other promotions that have already been applied.</remarks>
        public decimal PromotionalPrice(ZNodeProductBaseEntity Product)
        {
            var currentPrice = Product.ProductPrice;

            //Zeon Custom Code : Start
            string productExcludeDiscount = Product != null && !string.IsNullOrEmpty(Product.Custom1) ? Product.Custom1 : "0";
            if (productExcludeDiscount.Equals("0") && Product.IsMapPrice == false)
            {
                BindPromotionPricingOption(Product);

                Product.IsCustomPromotionApplied = false;
                Product.PricingDiscountPercent = 0;
                //Zeon Custom Code : Start              

                if (_PromotionRules != null && _PromotionRules.Count > 0)
                {
                    _PromotionRules.Sort("Precedence");

                    foreach (ZNodePromotionPricingRule rule in _PromotionRules)
                    {
                        //Zeon Custom Code : Start
                        rule.ZeonPromotionTypeCollectionIds = zeonPromotionTypeCollectionIds;
                        //Zeon Custom Code : End
                        currentPrice = rule.PromotionalPrice(Product, currentPrice);
                    }
                }
                else if (_PromotionRules == null || (_PromotionRules != null && _PromotionRules.Count == 0))
                {
                   // Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in _PromotionRules is null Or _PromotionRules.count==0: ProductiD=" + Product.ProductID);
                }
            }
            return currentPrice;
        }

        /// <summary>
        /// Calculate the final Promotional price for the given Product.
        /// </summary>
        /// <param name="Product">The Product on which the Promotional price is to be calculated on.</param>
        /// <param name="TieredPrice"></param>
        /// <returns></returns>
        /// <remarks>CurrentPrice is include just in case you need to calculate promotions based on other promotions that have already been applied.</remarks>
        public decimal PromotionalPrice(ZNodeProductBaseEntity Product, decimal TieredPrice)
        {
            var currentPrice = TieredPrice;

            //Zeon Custom Code : Start
            //Zeon Custom Code : Start
            string productExcludeDiscount = Product != null && !string.IsNullOrEmpty(Product.Custom1) ? Product.Custom1 : "0";
            if (productExcludeDiscount.Equals("0") && Product.IsMapPrice == false)
            {
                BindPromotionPricingOption(Product);

                Product.IsCustomPromotionApplied = false;
                Product.PricingDiscountPercent = 0;
                //Zeon Custom Code : Start              
                if (_PromotionRules != null && _PromotionRules.Count > 0)
                {
                    _PromotionRules.Sort("Precedence");
                    foreach (ZNodePromotionPricingRule rule in _PromotionRules)
                    {
                        //Zeon Custom Code : Start
                        rule.ZeonPromotionTypeCollectionIds = zeonPromotionTypeCollectionIds;
                        //Zeon Custom Code : End
                        currentPrice = rule.PromotionalPrice(Product, currentPrice);
                    }
                }
                else if (_PromotionRules == null || (_PromotionRules != null && _PromotionRules.Count == 0))
                {
                   // Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in _PromotionRules is null Or _PromotionRules.count==0: ProductiD=" + Product.ProductID);
                }
            }
            return currentPrice;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns></returns>
        private ZNodeCoupon GetCoupon(Promotion Entity)
        {
            var coupon = new ZNodeCoupon();

            coupon.PromotionProductId = Entity.PromotionProductID;
            coupon.QuantityMinimum = Entity.QuantityMinimum.GetValueOrDefault(1);
            coupon.PromotionProductQuantity = Entity.PromotionProductQty.GetValueOrDefault(1);
            coupon.CouponCode = Entity.CouponCode;
            coupon.CouponID = Entity.PromotionID;
            coupon.Discount = Entity.Discount;
            coupon.DiscountTypeID = Entity.DiscountTypeID;
            coupon.OrderMinimum = Entity.OrderMinimum.GetValueOrDefault(0);
            coupon.PromotionMessage = Entity.PromotionMessage;
            coupon.QuantityAvailable = Entity.CouponQuantityAvailable.GetValueOrDefault(0);
            coupon.ProductId = Entity.ProductID.GetValueOrDefault(0);
            coupon.ProfileId = Entity.ProfileID.GetValueOrDefault(0);

            return coupon;
        }

        /// <summary>
        /// Retrieves all active promotions and adds the list object to Application state object
        /// </summary>
        public static void CacheActivePromotions()
        {
            // Application object holds the list of active promotions
            if (HttpRuntime.Cache["PromotionCache"] == null)
            {
                var promotionService = new PromotionService();
                var promotionList = promotionService.GetAll();

                promotionList.ApplyFilter(delegate(Promotion promo) { return DateTime.Today.Date >= promo.StartDate.Date && DateTime.Today.Date <= promo.EndDate.Date && ((promo.CouponQuantityAvailable > 0 && promo.CouponInd) || promo.CouponInd == false); });
                promotionList.Sort("DisplayOrder");

                if (promotionList == null || (promotionList != null && promotionList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:-After Apply Error in promotionList is null!");
                }
                // Deepload discount type
                promotionService.DeepLoad(promotionList, true, DeepLoadType.IncludeChildren, typeof(DiscountType));

                if (promotionList == null || (promotionList != null && promotionList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:-After DeepLoad Error in promotionList is null!");
                }
                string[] tableNames = {"ZNodePromotion", "ZNodePromotionExtn" };
                ZNodeCacheDependencyManager.Insert("PromotionCache", promotionList, tableNames);
            }
        }

        /// <summary>
        /// Retrieves all active promotions and adds the list object to Application state object
        /// </summary>
        //public static void CacheActivePromotions()
        //{
        //    // Application object holds the list of active promotions
        //    if (HttpRuntime.Cache["PromotionCache"] == null)
        //    {
        //        var promotionService = new PromotionService();
        //        //Zeon Customization : Start
        //        //var promotionList = promotionService.GetAll();

        //        PromotionQuery promotionQuery = new PromotionQuery();
        //        promotionQuery.AppendInQuery(PromotionColumn.PromotionID, "SELECT PromotionID  FROM ZNodePromotionExtn WHERE PortalID=" + ZNodeConfigManager.SiteConfig.PortalID.ToString());

        //        var promotionList = promotionService.Find(promotionQuery.GetParameters());

        //        if (promotionList == null || (promotionList != null && promotionList.Count == 0))
        //        {
        //            Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:- Error in promotionList is null!");
        //        }

        //        //Zeon Customization : End

        //        promotionList.ApplyFilter(delegate(Promotion promo) { return DateTime.Today.Date >= promo.StartDate.Date && DateTime.Today.Date <= promo.EndDate.Date && ((promo.CouponQuantityAvailable > 0 && promo.CouponInd) || promo.CouponInd == false); });
        //        promotionList.Sort("DisplayOrder");

        //        if (promotionList == null || (promotionList != null && promotionList.Count == 0))
        //        {
        //            Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:- After Apply filter  Error in promotionList is null!");
        //        }

        //        // Deep load
        //        promotionService.DeepLoad(promotionList, true, DeepLoadType.IncludeChildren, typeof(DiscountType), typeof(TList<PromotionExtn>));

        //        if (promotionList == null || (promotionList != null && promotionList.Count == 0))
        //        {
        //            Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:- After DeepLoad Error in promotionList is null!");
        //        }

        //        ZNodeCacheDependencyManager.Insert("PromotionCache", promotionList, "ZNodePromotion");
        //    }
        //}

        /// <summary>
        /// Retrieves all active promotions extn data and adds the list object to Application state object
        /// </summary>
        public static void CacheActivePromotionsExtn()
        {
            // Application object holds the list of active promotions
            if (HttpRuntime.Cache["PromotionExtnCache"] == null)
            {
                var promotionExtnService = new PromotionExtnService();

                //PromotionExtnQuery promotionExtnQuery = new PromotionExtnQuery();
                //promotionExtnQuery.AppendInQuery(PromotionExtnColumn.PromotionExtnID, "SELECT PromotionExtnID  FROM ZNodePromotionExtn WHERE PortalID=" + ZNodeConfigManager.SiteConfig.PortalID.ToString());
                //var promotionExtnList = promotionExtnService.Find(promotionExtnQuery.GetParameters());

                var promotionExtnList = promotionExtnService.GetAll();
                if (promotionExtnList == null || (promotionExtnList != null && promotionExtnList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:- Error in promotionExtnList is null!");
                }

                promotionExtnList = FilterPromoExt(promotionExtnList);

                if (promotionExtnList == null || (promotionExtnList != null && promotionExtnList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:- FilterPromoExt Error in promotionExtnList is null!");
                }

                // Deep load
                promotionExtnService.DeepLoad(promotionExtnList);

                if (promotionExtnList == null || (promotionExtnList != null && promotionExtnList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:- After DeepLoad Error in promotionExtnList is null!");
                }
                string[] tableNames = { "ZNodePromotion", "ZNodePromotionExtn" };
                ZNodeCacheDependencyManager.Insert("PromotionExtnCache", promotionExtnList, tableNames);
            }
        }

        public static TList<PromotionExtn> FilterPromoExt(TList<PromotionExtn> promoExt)
        {
            TList<PromotionExtn> promoActiveExt = new TList<PromotionExtn>();
            if (HttpRuntime.Cache["PromotionCache"] != null)
            {
                TList<Promotion> promotionList = HttpRuntime.Cache["PromotionCache"] as TList<Promotion>;
                if (promotionList != null && promotionList.Count > 0)
                {
                    foreach (Promotion promo in promotionList)
                    {
                        promoActiveExt.Add(promoExt.Find((e) => { return (e.PromotionID == promo.PromotionID); }));
                    }
                }
            }
            if (promoActiveExt != null && promoActiveExt.Count > 0)
            {
                return promoActiveExt;
            }
            else
            {
                return promoExt;
            }
        }

    }
}

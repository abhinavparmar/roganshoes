﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.ECommerce.Promotions
{
    public class ZeonPromotionTypeCollectionIds
    {

        public ZeonPromotionTypeCollectionIds()
        {
            ProductIDs = new System.Collections.Generic.List<int>();
            BrandIDs = new System.Collections.Generic.List<int>();
            CategoryIDs = new System.Collections.Generic.List<int>();
        }

        public List<int> ProductIDs { get; set; }
        public List<int> BrandIDs { get; set; }
        public List<int> CategoryIDs { get; set; }
    }
}

﻿using System;
using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    public partial class ZNodePricingPercentOffProduct : ZNodePromotionPricingRule
    {
        /// <summary>
        /// Calculate this promotion and return the price after the promotion.
        /// </summary>
        /// <param name="Product">The Product on which the Promotional price is to be calculated on.</param>
        /// <param name="CurrentPrice">The current promotional price that has been calculated.</param>
        /// <remarks> Normally promotions will be calculated on the Product's base price. 
        /// CurrentPrice is include just in case you need to calculate promotions based on other promotions that have already been applied.</remarks>
        /// <returns>Returns the promotional price</returns>
        public decimal PromotionalPriceExtn(ZNodeProductBaseEntity Product, decimal CurrentPrice)
        {
            var discountedPrice = CurrentPrice;
            try
            {
                string productExcludeDiscount = !string.IsNullOrEmpty(Product.Custom1) ? Product.Custom1 : "0";
                if (discountedPrice > 0 && productExcludeDiscount.Equals("0") && Product.IsMapPrice == false && IsProductValidForPromotion(Product, (int)PromotionTypes.Product))
                {
                    if (Product.ProductID == PromotionProperty.ProductId)
                    {
                        discountedPrice -= CurrentPrice * (PromotionProperty.Discount / 100);
                        Product.IsCustomPromotionApplied = true;
                        Product.PricingDiscountPercent += PromotionProperty.Discount;
                    }
                }

                if (discountedPrice < 0)
                {
                    discountedPrice = 0;
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePricingPercentOffProduct!!" + ex.ToString());
            }
            return discountedPrice;
        }
    }
}

using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Represents the Promotion Amount off order
    /// </summary>
    public partial class ZNodePromotionAmountOffOrder : ZNodePromotionRule
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePromotionAmountOffOrder class.
        /// </summary>
        public ZNodePromotionAmountOffOrder()
            : base()
        {
        }

        /// <summary>
        /// Override the Calculate method to calculate amount of order discount for coupon and non-coupon based promtions.
        /// </summary>
        public override void Calculate()
        {
            ////Zeon Customization :: Start::product cannot be sold with any promotions if Custom1 = �1�
            this.CalculateExt();
            ////Zeon Customization :: End::product cannot be sold with any promotions if Custom1 = �1�

            #region[Znode Default Code]
            /*
            var subTotal = ShoppingCart.SubTotal;
            var itemCount = 0;

            // Count the CartItem and AddOn
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                var lineItemCount = 1;

                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        if (addOnValue.Price > 0)
                        {
                            lineItemCount++;
                        }
                    }
                }

                itemCount = itemCount + (lineItemCount * cartItem.Quantity);
            }

            if (PromotionProperty.OrderMinimum <= subTotal && PromotionProperty.Coupon == null)
            {
                if (itemCount > 0)
                {
                    var lineItemDiscount = PromotionProperty.Discount / itemCount;
                    this.ApplyDiscount(lineItemDiscount);
                }
            }
            else if (PromotionProperty.Coupon != null)
            {
                var isCouponValid = ValidateCoupon();

                // apply discount
                if (PromotionProperty.Coupon.OrderMinimum <= subTotal && isCouponValid)
                {
                    if (itemCount > 0)
                    {
                        var lineItemDiscount = PromotionProperty.Coupon.Discount / itemCount;

                        // Helper Method to calculate discount
                        this.ApplyDiscount(lineItemDiscount);
                    }

                    PromotionProperty.Coupon.CouponApplied = true;
                    ShoppingCart.CouponApplied = true;
                }

                AddPromotionMessage();
            }
            */
            #endregion
        }

        /// <summary>
        /// Apply the discount
        /// </summary>
        /// <param name="lineItemDiscount">Line Item Discount</param>
        private void ApplyDiscount(decimal lineItemDiscount)
        {
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                cartItem.Product.DiscountAmount += lineItemDiscount;

                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        if (addOnValue.Price > 0)
                        {
                            addOnValue.DiscountAmount += lineItemDiscount;
                        }
                    }
                }
            }
        }
    }
}

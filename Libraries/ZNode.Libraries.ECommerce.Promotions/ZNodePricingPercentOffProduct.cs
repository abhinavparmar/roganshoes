using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Represents the Pricing Percent off product
    /// </summary>
    public partial class ZNodePricingPercentOffProduct : ZNodePromotionPricingRule
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePricingPercentOffProduct class.
        /// </summary>
        public ZNodePricingPercentOffProduct()
            : base()
        {
        }

        /// <summary>
        /// Calculate this promotion and return the price after the promotion.
        /// </summary>
        /// <param name="Product">The Product on which the Promotional price is to be calculated on.</param>
        /// <param name="CurrentPrice">The current promotional price that has been calculated.</param>
        /// <remarks> Normally promotions will be calculated on the Product's base price. 
        /// CurrentPrice is include just in case you need to calculate promotions based on other promotions that have already been applied.</remarks>
        /// <returns>Returns the promotional price</returns>
        public override decimal PromotionalPrice(ZNodeProductBaseEntity Product, decimal CurrentPrice)
        {
            //Zeon Custom Code - Start
            return this.PromotionalPriceExtn(Product, CurrentPrice);
            //Zeon Custom Code - End

            #region[ZNode Default Code]
            /*
            var discountedPrice = CurrentPrice;

            if (Product.ProductID == PromotionProperty.ProductId)
            {
                discountedPrice -= CurrentPrice * (PromotionProperty.Discount / 100);
            }

            if (discountedPrice < 0)
            {
                discountedPrice = 0;
            }

            return discountedPrice;
             */
            #endregion
        }
    }
}

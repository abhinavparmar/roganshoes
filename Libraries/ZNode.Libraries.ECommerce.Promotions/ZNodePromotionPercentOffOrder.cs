using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Promotion rule for doing Percent off Order.
    /// </summary>
	public partial class ZNodePromotionPercentOffOrder : ZNodePromotionRule
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePromotionPercentOffOrder class.
        /// </summary>
        public ZNodePromotionPercentOffOrder() : base()
        {
        }

        /// <summary>
        /// Do promotion calculation.
        /// </summary>
        public override void Calculate()
        {
            ////Zeon Customization :: Start::product cannot be sold with any promotions if Custom1 = �1�
            this.CalculateExtn();
            ////Zeon Customization :: End::product cannot be sold with any promotions if Custom1 = �1�
            #region[Znode default code]
            /*
            var subTotal = ShoppingCart.SubTotal;
            
            if (PromotionProperty.OrderMinimum <= subTotal && PromotionProperty.Coupon == null)
            {
                var discount = PromotionProperty.Discount / 100;

                // Helper Method to calculate discount
                ApplyDiscount(discount);
            }
            else if (PromotionProperty.Coupon != null)
            {
                var isCouponValid = ValidateCoupon();

                // apply discount
                if (PromotionProperty.Coupon.OrderMinimum <= subTotal && isCouponValid)
                {
                    var discount = PromotionProperty.Coupon.Discount / 100;

                    // Helper Method to calculate discount
                    ApplyDiscount(discount);
                    
                    PromotionProperty.Coupon.CouponApplied = true;
                    ShoppingCart.CouponApplied = true;
                }

                AddPromotionMessage();
            }
            */
            #endregion
        }

        /// <summary>
        /// Apply the discount
        /// </summary>
        /// <param name="discount">discount amount</param>
        private void ApplyDiscount(decimal discount)
        {
            // Calculate the Percentage of discount for each order line item.            
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {   
                var finalPrice = cartItem.PromotionalPrice;
                var lineItemDiscount = finalPrice * discount;

                cartItem.Product.DiscountAmount += lineItemDiscount;

                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        lineItemDiscount = addOnValue.Price * discount;
                        addOnValue.DiscountAmount += lineItemDiscount;
                    }
                }
            }
        }
    }
}

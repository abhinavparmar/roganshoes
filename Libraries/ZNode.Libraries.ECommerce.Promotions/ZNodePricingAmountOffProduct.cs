using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Represent the pricing amount off product
    /// </summary>
	public partial class ZNodePricingAmountOffProduct : ZNodePromotionPricingRule
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePricingAmountOffProduct class.
        /// </summary>
        public ZNodePricingAmountOffProduct() : base()
        {
        }

        /// <summary>
        /// Calculate this promotion and return the price after the promotion.
        /// </summary>
        /// <param name="Product">The Product on which the Promotional price is to be calculated on.</param>
        /// <param name="CurrentPrice">The current promotional price that has been calculated.</param>
        /// <remarks> Normally promotions will be calculated on the Product's base price. 
        /// CurrentPrice is include just in case you need to calculate promotions based on other promotions that have already been applied.</remarks>
        /// <returns>Returns the promotional price</returns>
        public override decimal PromotionalPrice(ZNodeProductBaseEntity Product, decimal CurrentPrice)
        {
            //Zeon Customization :: Start
            return PromotionalPriceExtn(Product, CurrentPrice);
            //Zeon Customization :: End

            #region[Znode Default Code]
            /*
            var discountedPrice = CurrentPrice;

            if (Product.ProductID == PromotionProperty.ProductId)
            {
                discountedPrice -= PromotionProperty.Discount;
            }

            if (discountedPrice < 0)
            {
                discountedPrice = 0;
            }

            return discountedPrice;
             */
            #endregion
        }   
    }
}

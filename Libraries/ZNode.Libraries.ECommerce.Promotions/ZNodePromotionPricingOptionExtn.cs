﻿using System;
using System.Data;
using System.Web;
using System.Web.Security;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Ecommerce;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Promotions
{
    public partial class ZNodePromotionPricingOption
    {

        public void BindPromotionPricingOption(ZNodeProductBaseEntity product)
        {

            //Get Filtered Promotion Extn List
            TList<PromotionExtn> promotionExtnListForFiltedPromo = new TList<PromotionExtn>();
            promotionExtnListForFiltedPromo = GetFilteredPromotionExt(product);

            //if (promotionExtnListForFiltedPromo == null || (promotionExtnListForFiltedPromo != null && promotionExtnListForFiltedPromo.Count == 0))
            //{
            //    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in After promotionExtnListForFiltedPromo ()!!promotionExtnList is null Or promotionExtnList.count is 0. ProductID=" + product.ProductID);
            //}

            //Get Filtered Promotion List Based on Promotion Ext Categories
            TList<Promotion> promotionList = new TList<Promotion>();
            if (promotionExtnListForFiltedPromo != null && promotionExtnListForFiltedPromo.Count > 0)
            {
                promotionList = GetFilteredPromotionList(promotionExtnListForFiltedPromo);
            }
            else
            {
                return;
            }

            //if (promotionList == null || (promotionList != null && promotionList.Count == 0))
            //{
            //    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in After GetFilteredPromotionList ()!!promotionList is null Or promotionList.count is 0. ProductID=" + product.ProductID);
            //}

            Profile profile = null;
            if (HttpContext.Current.Session["ProfileCache"] != null)
            {
                // Retrieve profile
                profile = HttpContext.Current.Session["ProfileCache"] as Profile;
            }

            int? CurrentProfileID = null;
            if (profile != null)
            {
                CurrentProfileID = profile.ProfileID;
            }
            if (CurrentProfileID == null)
            {
                //PRFT Custom Code To Solve ProfileID null Issue:Starts
                CurrentProfileID = ZNodeConfigManager.SiteConfig.DefaultRegisteredProfileID.GetValueOrDefault();
                ProfileService profileService = new ProfileService();
                Profile ProfileEntity = profileService.GetByProfileID(CurrentProfileID.Value);

                if (ProfileEntity != null)
                {
                    // Hold this profile object in the session state
                    HttpContext.Current.Session["ProfileCache"] = ProfileEntity;

                }
                //PRFT Custom Code To Solve ProfileID null Issue:Ends
                if (CurrentProfileID == null)
                {

                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in BindPromotionPricingOption!!Is CurrentProfileID is null Or CurrentProfileID for all profile. ProductID=" + product.ProductID);
                }

            }

            string promoData = string.Empty;
            if (promotionList != null && promotionList.Count > 0)
            {
                string classType = promotionList[0].DiscountTypeIDSource != null ? promotionList[0].DiscountTypeIDSource.ClassType : string.Empty;
                promoData = "ProfileID:" + promotionList[0].ProfileID + "Count:" + promotionList.Count + "Discount type=" + classType + "PromotionID=" + promotionList[0].PromotionID;
            }

            // Build a list of rules that are appropriate for this instance of this class from the Promotion Cache.            
            // Promotion Rule Factory
            promotionList.ApplyFilter(delegate(Promotion promo) { return (promo.ProfileID == CurrentProfileID || promo.ProfileID == null) && promo.DiscountTypeIDSource.ClassType.Equals("PRICE", StringComparison.OrdinalIgnoreCase); });
            promotionList.Sort("DisplayOrder");


            //log if no promotion list exists
            if (promotionList == null || (promotionList != null && promotionList.Count == 0))
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in BindPromotionPricingOption!!Is promotionList is null Or promotionList.Count==0 ProductID=" + product.ProductID + " Current profileID=" + CurrentProfileID.GetValueOrDefault(0) + "Details:" + promoData);
            }

            _PromotionRules = new ZNodeGenericCollection<IZNodePromotionPricingRule>();
            System.Reflection.Assembly promoRuleAssebmly = System.Reflection.Assembly.GetExecutingAssembly();

            var assemblyName = promoRuleAssebmly.GetName().Name;

            zeonPromotionTypeCollectionIds = new ZeonPromotionTypeCollectionIds();

            if (promotionList != null && promotionList.Count > 0)
            {
                foreach (Promotion promo in promotionList)
                {
                    var promotionProperty = new ZNodePromotionProperty();
                    promotionProperty.Discount = promo.Discount;
                    promotionProperty.OrderMinimum = promo.OrderMinimum.GetValueOrDefault(0);
                    promotionProperty.Discount = promo.Discount;
                    promotionProperty.ProductId = promo.ProductID.GetValueOrDefault(0);
                    promotionProperty.QuantityMinimum = promo.QuantityMinimum.GetValueOrDefault(1);
                    promotionProperty.PromotionProductId = promo.PromotionProductID.GetValueOrDefault(0);
                    promotionProperty.PromotionalProductQuantity = promo.PromotionProductQty.GetValueOrDefault(1);

                    promotionProperty.ProfileId = CurrentProfileID;

                    if (promo.CouponInd)
                    {
                        promotionProperty.Coupon = GetCoupon(promo);
                    }

                    if (!string.IsNullOrEmpty(promo.PromotionMessage) && !promo.CouponInd)
                    {
                        if (promotionProperty.Coupon == null)
                        {
                            promotionProperty.Coupon = new ZNodeCoupon();
                        }

                        promotionProperty.Coupon.PromotionMessage = promo.PromotionMessage;
                    }

                    var className = promo.DiscountTypeIDSource.ClassName;

                    // Instantiate the Promotion based on class name.
                    if (!string.IsNullOrEmpty(className))
                    {
                        try
                        {
                            var promoRule = (IZNodePromotionPricingRule)promoRuleAssebmly.CreateInstance(assemblyName + "." + className);
                            promoRule.Precedence = promo.DisplayOrder;

                            promoRule.Bind(promotionProperty);
                            _PromotionRules.Add(promoRule);
                        }
                        catch (Exception ex)
                        {
                            ZNodeLogging.LogMessage("Error while instantiating promotion: " + className);
                            ZNodeLogging.LogObject(typeof(Exception), ex);
                        }
                    }

                    #region[Zeon Custom Code]

                    //Zeon Custom Code::Changes to Set Value for ProtalID ,CatalogID,CategoryID,ManufactureID,PromotionID :: Start
                    PromotionExtn promoExt = null;
                    if (promo != null && promo.PromotionExtnCollection != null && promo.PromotionExtnCollection.Count > 0)
                    {
                        // Assign Promotion Extension Details
                        promoExt = new PromotionExtn();
                        promoExt = promo.PromotionExtnCollection[0];
                    }
                    else if (promo != null)
                    {
                        var promotionExtnList = (TList<PromotionExtn>)PromotionExtnCache.Clone();
                        promotionExtnList.ApplyFilter(delegate(PromotionExtn promoExtn)
                        {
                            return (promoExtn.PromotionID == promo.PromotionID);
                        });

                        if (promotionExtnList != null && promotionExtnList.Count > 0 && promotionExtnList[0].PromotionID == promo.PromotionID)
                        {
                            promoExt = promotionExtnList[0];
                        }
                        else
                        {
                            TList<PromotionExtn> promoextList = new TList<PromotionExtn>();
                            PromotionExtnService promoextService = new PromotionExtnService();
                            promoextList = promoextService.GetByPromotionID(promo.PromotionID);
                            if (promoextList != null && promoextList.Count > 0)
                            {
                                promoExt = new PromotionExtn();
                                promoExt = promoextList[0];
                            }
                        }

                        if (promoExt == null && (promoExt != null && promoExt.CategoryID.HasValue == false))
                        {
                            Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePromotionPricingOption()!!promoExt is null or promoExt.CategoryID=0");
                        }

                    }

                    if (promoExt != null)
                    {
                        promotionProperty.CatalogID = promoExt.CatalogID.GetValueOrDefault(0);

                        int promotionCategoryID = promoExt.CategoryID.GetValueOrDefault(0);
                        if (promotionCategoryID > 0)
                        {
                            promotionProperty.CategoryID = promotionCategoryID;
                        }
                        else
                        {
                            Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePromotionPricingOption()!!promotion categoryid is 0");
                        }

                        promotionProperty.ManufacturerID = promoExt.ManufacturerID.GetValueOrDefault(0);
                        promotionProperty.PortalID = promoExt.PortalID.GetValueOrDefault(0);
                        promotionProperty.Custom1 = promo.Custom1;
                        promotionProperty.Custom2 = promo.Custom2;
                        promotionProperty.PromotionID = promo.PromotionID;

                        if (promoExt.ManufacturerID.GetValueOrDefault(0) > 0)
                        {
                            zeonPromotionTypeCollectionIds.BrandIDs.Add(promoExt.ManufacturerID.GetValueOrDefault(0));
                        }
                        if (promoExt.CategoryID.GetValueOrDefault(0) > 0)
                        {
                            zeonPromotionTypeCollectionIds.CategoryIDs.Add(promoExt.CategoryID.GetValueOrDefault(0));
                        }
                    }
                    else
                    {
                        Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePromotionPricingOption()!!promoExt is null");
                    }

                    if (promo.ProductID.GetValueOrDefault(0) > 0)
                    {
                        zeonPromotionTypeCollectionIds.ProductIDs.Add(promo.ProductID.GetValueOrDefault(0));
                    }
                    #endregion
                }
            }
            else
            {
                int promotionExtCount = promotionExtnListForFiltedPromo != null ? promotionExtnListForFiltedPromo.Count : 0;
                int categoryid = promotionExtnListForFiltedPromo != null && promotionExtnListForFiltedPromo[0].CategoryID.HasValue ? promotionExtnListForFiltedPromo[0].CategoryID.Value : 0;
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in BindPromotionPricingOption()!!promotionExtCount=" + promotionExtCount + "!!CategoryID=" + categoryid + " ProductID=" + product.ProductID);

            }

            promotionList.Dispose();
        }

        /// <summary>
        /// GetZNodeProductCategoryList
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        protected ZNodeGenericCollection<ZNodeProductCategoryList> GetZNodeProductCategoryList(ZNodeProductBaseEntity product)
        {
            if (product != null && product.ZNodeProductCategoryList != null && product.ZNodeProductCategoryList.Count > 0)
            {
                return product.ZNodeProductCategoryList;
            }
            else
            {
                ProductCategoryService productCategoryServices = new ProductCategoryService();
                var productCategories = productCategoryServices.GetByProductID(product.ProductID);

                product.ZNodeProductCategoryList = new ZNodeGenericCollection<ZNodeProductCategoryList>();
                foreach (ProductCategory item in productCategories)
                {
                    product.ZNodeProductCategoryList.Add(new ZNodeProductCategoryList() { CategoryID = item.CategoryID });
                }
                return product.ZNodeProductCategoryList;
            }
        }

        /// <summary>
        /// GetProductCategoryIDsByProductID
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        protected string[] GetProductCategoryIDsByProductID(ZNodeProductBaseEntity product)
        {
            string categoryIDs = string.Empty;
            string[] categoryArray = null;
            try
            {
                if (product != null && !string.IsNullOrEmpty(product.ProductCategoryHierarchy))
                {
                    categoryIDs = product.ProductCategoryHierarchy;
                }
                else
                {
                    categoryIDs = ZNode.Libraries.DataAccess.Custom.CategoryHelper.GetCategoryIDsByProductID(ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID, product.ProductID);
                }

                if (!string.IsNullOrEmpty(categoryIDs))
                {
                    categoryArray = categoryIDs.Split(',');

                    Array.Reverse(categoryArray);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }


            return categoryArray;
        }

        /// <summary>
        /// GetFilteredPromotionList 
        /// </summary>
        /// <param name="promotionExtnListForFiltedPromo"></param>
        /// <returns></returns>
        private TList<Promotion> GetFilteredPromotionList(TList<PromotionExtn> promotionExtnListForFiltedPromo)
        {
            TList<Promotion> newPromotionList = new TList<Promotion>();
            TList<Promotion> promoList = new TList<Promotion>();
            try
            {
                PromotionService promotionService = new PromotionService();
                promoList = (TList<Promotion>)PromotionCache;

                Promotion singlePromo = new Promotion();
                if (promotionExtnListForFiltedPromo != null && promotionExtnListForFiltedPromo.Count > 0 && promoList != null && promoList.Count > 0)
                {
                    int initalCount = promoList.Count;

                    //singlePromo = promotionList.Find((e) => { return (e.PromotionID == promotionExtnListForFiltedPromo[0].PromotionID); });
                    //if (singlePromo != null)
                    //{
                    //    newPromotionList.Add(singlePromo);
                    //}
                    //else
                    //{
                    //    Zeon.Libraries.Elmah.ElmahErrorManager.Log("Single Find!!Error in Method-GetFilteredPromotionList!!promotionList is null|| After FindAll||Before Find all count: Promotion List count=" + initalCount + "promotionId=" + promotionExtnListForFiltedPromo[0].PromotionID);
                    //}

                    System.Text.StringBuilder promotionids = new System.Text.StringBuilder();
                    foreach (Promotion promo in promoList)
                    {
                        promotionids.Append(promo.PromotionID);
                        promotionids.Append(",");
                        if (promo.PromotionID == promotionExtnListForFiltedPromo[0].PromotionID)
                        {
                            promotionids.Append("Macth Found PromotionID=" + promo.PromotionID);
                            newPromotionList.Add(promo);
                            break;
                        }
                    }

                    //promotionList = promotionList.FindAll((e) => { return (e.PromotionID == promotionExtnListForFiltedPromo[0].PromotionID); }); 

                    if (newPromotionList == null || (newPromotionList != null && newPromotionList.Count == 0))
                    {
                        Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!For each: Error in Method-GetFilteredPromotionList!!newPromotionList is null|| After FindAll||Before Find all count: Promotion List count=" + initalCount + "promotionId=" + promotionExtnListForFiltedPromo[0].PromotionID + " Details Product Id=" + promotionids.ToString());
                        newPromotionList = new TList<Promotion>();
                        newPromotionList.Add(promotionService.DeepLoadByPromotionID(promotionExtnListForFiltedPromo[0].PromotionID, true, DeepLoadType.IncludeChildren, typeof(DiscountType)));
                    }
                }
                else
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in Method-GetFilteredPromotionList!! promotionList is null");
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in GetFilteredPromotionList!!" + ex.ToString());
            }

            return newPromotionList;
        }

        /// <summary>
        /// Get FilteredPromotionExt
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        private TList<PromotionExtn> GetFilteredPromotionExt(ZNodeProductBaseEntity product)
        {
            bool isLeafCategoryApplied = false;
            TList<PromotionExtn> promotionExtnListForFiltedPromo = new TList<PromotionExtn>();
            try
            {
                TList<PromotionExtn> promotionExtnListFilter = (TList<PromotionExtn>)PromotionExtnCache.Clone();
                if (promotionExtnListFilter != null && promotionExtnListFilter.Count > 0)
                {
                    ZNode.Libraries.Framework.Business.ZNodeGenericCollection<ZNodeProductCategoryList> productCategoryList = GetZNodeProductCategoryList(product);
                    if (productCategoryList != null && productCategoryList.Count > 0)
                    {
                        foreach (ZNodeProductCategoryList prodCategory in productCategoryList)
                        {
                            TList<PromotionExtn> promotionExtnListForCategory = promotionExtnListFilter.FindAll((e) => { return (e.CategoryID == prodCategory.CategoryID); });
                            if (promotionExtnListForCategory != null && promotionExtnListForCategory.Count > 0)
                            {
                                promotionExtnListForFiltedPromo.Add(promotionExtnListForCategory[0]);
                                isLeafCategoryApplied = true;
                                break;
                            }
                        }
                    }
                    else if (productCategoryList == null || (productCategoryList != null && productCategoryList.Count == 0))
                    {
                        Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in BindPromotionPricingOption!!Is productCategoryList is null Or productCategoryList.Count==0");
                    }

                    if (isLeafCategoryApplied == false)
                    {
                        string[] ProductCategoryIds = GetProductCategoryIDsByProductID(product);
                        if (ProductCategoryIds != null && ProductCategoryIds.Length > 0)
                        {
                            foreach (var item in ProductCategoryIds)
                            {
                                int catID = 0;
                                int.TryParse(item, out catID);
                                TList<PromotionExtn> promotionExtnListForCategory = promotionExtnListFilter.FindAll((e) => { return (e.CategoryID == catID); });
                                if (promotionExtnListForCategory != null && promotionExtnListForCategory.Count > 0)
                                {
                                    promotionExtnListForFiltedPromo.Add(promotionExtnListForCategory[0]);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in BindPromotionPricingOption>>PromotionExtnCache.Clone()!!Is promotionExtnListFilter is null Or promotionExtnListFilter.Count==0");
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in GetFilteredPromotionExt!!" + ex.ToString());
            }

            return promotionExtnListForFiltedPromo;
        }
    }
}

﻿using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.DataAccess.Custom;
using System;

namespace ZNode.Libraries.ECommerce.Promotions
{
    public class ZeonPromotionAmountOffBrand : ZNodePromotionPricingRule
    {
        public ZeonPromotionAmountOffBrand()
            : base()
        {

        }


        /// <summary>
        /// Calculate brand based promotion and return the price after the promotion.
        /// </summary>
        /// <param name="Product">The Product on which the Promotional price is to be calculated on.</param>
        /// <param name="CurrentPrice">The current promotional price that has been calculated.</param>
        /// <remarks> Normally promotions will be calculated on the Product's base price. 
        /// CurrentPrice is include just in case you need to calculate promotions based on other promotions that have already been applied.</remarks>
        /// <returns>Returns the promotional price</returns>
        public override decimal PromotionalPrice(ZNodeProductBaseEntity product, decimal CurrentPrice)
        {
            var discountedPrice = CurrentPrice;
            try
            {
                string productExcludeDiscount = !string.IsNullOrEmpty(product.Custom1) ? product.Custom1 : "0";
                if (discountedPrice > 0 && productExcludeDiscount.Equals("0") && product.IsMapPrice == false && IsProductValidForPromotion(product, (int)PromotionTypes.Brand))
                {
                    if (PromotionProperty != null && PromotionProperty.ManufacturerID.Equals(product.ManufacturerID))
                    {
                        discountedPrice -= PromotionProperty.Discount;
                        product.IsCustomPromotionApplied = true;
                    }
                }

                if (discountedPrice < 0)
                {
                    discountedPrice = 0;
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZeonPromotionAmountOffBrand!!" + ex.ToString());
            }
            return discountedPrice;
        }
    }
}

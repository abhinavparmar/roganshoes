﻿using System;
using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    public partial class ZNodePromotionPercentOffOrder : ZNodePromotionRule
    {
        /// <summary>
        /// Do promotion calculation.
        /// </summary>
        public void CalculateExtn()
        {
            try
            {
                //var subTotal = ShoppingCart.SubTotal;
                var subTotal = 0.0M;
                foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
                {
                    if (cartItem.PromotionalPrice != null && cartItem.PromotionalPrice > 0)
                    {
                        var unitPrice = cartItem.PromotionalPrice * cartItem.Quantity;
                        subTotal += unitPrice;
                    }
                }

                if (subTotal <= 0)
                {
                    subTotal = ShoppingCart.SubTotal;
                }

                if (PromotionProperty.OrderMinimum <= subTotal && PromotionProperty.Coupon == null)
                {
                    var discount = PromotionProperty.Discount / 100;

                    // Helper Method to calculate discount
                    this.ApplyDiscountExtn(discount);
                }
                else if (PromotionProperty.Coupon != null)
                {
                    var isCouponValid = ValidateCoupon();

                    // apply discount
                    if (PromotionProperty.Coupon.OrderMinimum <= subTotal && isCouponValid)
                    {
                        var discount = PromotionProperty.Coupon.Discount / 100;

                        // Helper Method to calculate discount
                        this.ApplyDiscountExtn(discount);

                        PromotionProperty.Coupon.CouponApplied = true;
                        ShoppingCart.CouponApplied = true;
                    }

                    AddPromotionMessage();
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePromotionPercentOffOrder!!CalculateExtn!!" + ex.ToString());
            }
        }

        /// <summary>
        /// Apply the discount
        /// </summary>
        /// <param name="discount">discount amount</param>
        private void ApplyDiscountExtn(decimal discount)
        {
            // Calculate the Percentage of discount for each order line item.            
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                string productExcludeDiscount = !string.IsNullOrEmpty(cartItem.Product.Custom1) ? cartItem.Product.Custom1 : "0";
                if (productExcludeDiscount.Equals("0"))
                {
                    var finalPrice = cartItem.PromotionalPrice;
                    var lineItemDiscount = finalPrice * discount;

                    cartItem.Product.DiscountAmount += lineItemDiscount;

                    foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                        {
                            lineItemDiscount = addOnValue.Price * discount;
                            addOnValue.DiscountAmount += lineItemDiscount;
                        }
                    }
                }
            }
        }
    }

}

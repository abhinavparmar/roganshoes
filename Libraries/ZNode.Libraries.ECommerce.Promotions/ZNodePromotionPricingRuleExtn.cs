﻿using ZNode.Libraries.ECommerce.Entities;
using System.Linq;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.DataAccess.Entities;
using System;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Promotions
{
    public partial class ZNodePromotionPricingRule : IZNodePromotionPricingRule
    {
        #region[public property]
        public ZeonPromotionTypeCollectionIds ZeonPromotionTypeCollectionIds { get; set; }
        public enum PromotionTypes
        {
            Catalog = 1,
            Category = 2,
            Brand = 3,
            Product = 4
        }

        public string[] ProductCategoryIds { get; set; }

        #endregion

        #region[Methods]

        /// <summary>
        /// Check si product is appicable for promotion for selected promotion types
        /// </summary>
        /// <param name="Product"></param>
        /// <param name="promotionTypes"></param>
        /// <returns></returns>
        public bool IsProductValidForPromotion(ZNodeProductBaseEntity Product, int promotionTypes)
        {
            var returnValue = true;
            try
            {
                if (IsProductExistsInProductPromotion(Product))
                {
                    if (promotionTypes.Equals((int)PromotionTypes.Product))
                        returnValue = returnValue && true;
                    else
                        returnValue = returnValue && false;
                }
                else if (IsProductExistsInBrandsPromotion(Product))
                {
                    if (promotionTypes.Equals((int)PromotionTypes.Brand))
                        returnValue = returnValue && true;
                    else
                        returnValue = returnValue && false;

                }
                else if (IsProductExistsInCategoryPromotion(Product))
                {
                    if (promotionTypes.Equals((int)PromotionTypes.Category))
                        returnValue = returnValue && true;
                    else
                        returnValue = returnValue && false;
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePromotionPricingRule!!IsProductValidForPromotion!!" + ex.ToString());
            }
            return returnValue;
        }

        /// <summary>
        /// Check is product exists in category promotion
        /// if product exists in category promotion then this catalog and category promotion will not be applicable for that product
        /// </summary>
        /// <param name="productID"></param>
        /// <returns></returns>
        private bool IsProductExistsInCategoryPromotion(ZNodeProductBaseEntity product)
        {
            bool isExists = false;

            //ProductCategoryService prodCatService = new ProductCategoryService();
            //var productCategoryList = prodCatService.GetByProductID(productID);

            foreach (int catID in ZeonPromotionTypeCollectionIds.CategoryIDs)
            {
                string[] productCatIDs = ProductCategoryIds = GetProductCategoryIDsByProductID(product);

                if (productCatIDs != null && productCatIDs.Length > 0)
                {
                    int count = productCatIDs.Where(o => int.Parse(o) == catID).Count();
                    if (count > 0)
                    {
                        isExists = true;
                        break;
                    }
                }

            }
            return isExists;
        }

        /// <summary>
        /// Check is product exists in brand promotion
        /// if product exists in brand promotion then this catalog and category promotion will not be applicable for that product
        /// </summary>
        /// <param name="Product"></param>
        /// <returns></returns>
        private bool IsProductExistsInBrandsPromotion(ZNodeProductBaseEntity Product)
        {
            bool isExists = false;
            int count = ZeonPromotionTypeCollectionIds != null && ZeonPromotionTypeCollectionIds.BrandIDs != null
                && ZeonPromotionTypeCollectionIds.BrandIDs.Count > 0 ? ZeonPromotionTypeCollectionIds.BrandIDs.Where(n => n == Product.ManufacturerID).Count() : 0;
            if (count > 0)
            {
                isExists = true;
            }
            return isExists;
        }

        /// <summary>
        /// Check is product exists in product promotion
        /// if product exists in product promotion then brand,catalog and category promotion will not be applicable for that product
        /// </summary>
        /// <param name="Product"></param>
        /// <returns></returns>
        private bool IsProductExistsInProductPromotion(ZNodeProductBaseEntity Product)
        {
            bool isExists = false;
            int count = ZeonPromotionTypeCollectionIds != null && ZeonPromotionTypeCollectionIds.ProductIDs != null
                && ZeonPromotionTypeCollectionIds.ProductIDs.Count > 0 ? ZeonPromotionTypeCollectionIds.ProductIDs.Where(n => n == Product.ProductID).Count() : 0;
            if (count > 0)
            {
                isExists = true;
            }
            return isExists;
        }

        protected string[] GetProductCategoryIDsByProductID(ZNodeProductBaseEntity product)
        {
            string categoryIDs = string.Empty;
            string[] categoryArray = null;
            try
            {
                if (product != null && !string.IsNullOrEmpty(product.ProductCategoryHierarchy))
                {
                    categoryIDs = product.ProductCategoryHierarchy;
                }
                else
                {
                    categoryIDs = ZNode.Libraries.DataAccess.Custom.CategoryHelper.GetCategoryIDsByProductID(ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID, product.ProductID);
                }

                if (!string.IsNullOrEmpty(categoryIDs))
                {
                    categoryArray = categoryIDs.Split(',');

                    Array.Reverse(categoryArray);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }


            return categoryArray;
        }

        /// <summary>
        /// Check For Leaf Category Promotion of Product
        /// </summary>
        /// <param name="product">ZNodeProductBaseEntity</param>
        /// <param name="catID">int</param>
        /// <returns>bool</returns>
        public bool CheckPromotionIsAppliedOnLeafCategory(ZNodeProductBaseEntity product, out int catID)
        {
            bool isAppliedOnLeafCategory = false;

            catID = 0;
            try
            {
                ZNodeGenericCollection<ZNodeProductCategoryList> productCategoryList = GetZNodeProductCategoryList(product);

                if (productCategoryList != null && productCategoryList.Count > 0)
                {
                    foreach (ZNodeProductCategoryList prodCategory in productCategoryList)
                    {
                        if (ZeonPromotionTypeCollectionIds != null && ZeonPromotionTypeCollectionIds.CategoryIDs.Count > 0)
                        {
                            foreach (int categoryID in ZeonPromotionTypeCollectionIds.CategoryIDs)
                            {
                                if (prodCategory.CategoryID == categoryID)
                                {
                                    isAppliedOnLeafCategory = true;
                                    catID = categoryID;
                                    break;
                                }
                            }
                            if (isAppliedOnLeafCategory)
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in CheckPromotionIsAppliedOnLeafCategory!!" + ex.ToString());
            }

            return isAppliedOnLeafCategory;
        }


        /// <summary>
        /// Get All Category Associated to product
        /// </summary>
        /// <param name="product">ZNodeProductBaseEntity</param>
        /// <returns>ZNodeGenericCollection<ZNodeProductCategoryList></returns>
        protected ZNodeGenericCollection<ZNodeProductCategoryList> GetZNodeProductCategoryList(ZNodeProductBaseEntity product)
        {
            if (product != null && product.ZNodeProductCategoryList != null && product.ZNodeProductCategoryList.Count > 0)
            {
                return product.ZNodeProductCategoryList;
            } 
            else
            {
                ProductCategoryService productCategoryServices = new ProductCategoryService();
                var productCategories = productCategoryServices.GetByProductID(product.ProductID);

                product.ZNodeProductCategoryList = new ZNodeGenericCollection<ZNodeProductCategoryList>();
                foreach (ProductCategory item in productCategories)
                {
                    product.ZNodeProductCategoryList.Add(new ZNodeProductCategoryList() { CategoryID = item.CategoryID });
                }
                return product.ZNodeProductCategoryList;
            }
        }
        #endregion
    }
}

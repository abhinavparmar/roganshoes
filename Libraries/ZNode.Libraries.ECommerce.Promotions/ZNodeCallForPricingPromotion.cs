﻿using ZNode.Libraries.ECommerce.Entities;
namespace ZNode.Libraries.ECommerce.Promotions
{
    public class ZNodeCallForPricingPromotion: ZNodePromotionPricingRule
    {
        /// <summary>
		/// Initializes a new instance of the ZNodeCallForPricingPromotion class.
        /// </summary>
        public ZNodeCallForPricingPromotion() : base()
        {

        }

        /// <summary>
        /// Apply this promotion and return the promotion with updated product information.
        /// </summary>
        /// <param name="Product">The Product on which the Promotional price is to be calculated on.</param>
        /// <param name="CurrentPrice">The current promotional price that has been calculated.</param>
        /// <remarks> Normally promotions will be calculated on the Product's base price. 
        /// Here we are overriding All Possible ZNodeProductBaseEntity properties.</remarks>
        /// <returns>Returns the promotional price with updated product object</returns>
        public override decimal PromotionalPrice(ZNodeProductBaseEntity Product, decimal CurrentPrice)
        {
            // Check Promotion already applied.
            if (!Product.IsPromotionApplied && Product.ProductID == this.PromotionProperty.ProductId)
            {
                if (this.PromotionProperty.PromotionProductId > 0 && Product.SelectedSKUvalue.SKUID == this.PromotionProperty.PromotionProductId)
                {
                    // For anonymous profiles the should be a promotion created that will display the product but not allow it to be added to the cart. 
                    // Currently "Call to Purchase" is under this when CallForPricing is true. We want to have the option to enter custom text.
                    // The limit is calculated agains  qty of the  sku ordered in the customer’s account from current time back 48 hours.
                    // Example: Customer attempts to purchase now (1/18/2012  @ 3:30pm).  Check is against all of customer's orders since  1/16/2012 @ 3:30pm)

                    if (this.PromotionProperty.Coupon != null && !string.IsNullOrEmpty(this.PromotionProperty.Coupon.PromotionMessage))
                    {
                        Product.CallMessage = this.PromotionProperty.Coupon.PromotionMessage;
                    }

                    Product.CallForPricing = true;

                    // Use this property to ignore second time promotion apply.
                    Product.IsPromotionApplied = true;
                }
            }

            return CurrentPrice;
        }   
    }
}

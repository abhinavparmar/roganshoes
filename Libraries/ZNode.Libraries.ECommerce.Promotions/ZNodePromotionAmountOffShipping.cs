namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Represents the Promotion Amount off shipping
    /// </summary>
	public class ZNodePromotionAmountOffShipping : ZNodePromotionRule
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePromotionAmountOffShipping class.
        /// </summary>
        public ZNodePromotionAmountOffShipping() : base()
        { 
        }

        /// <summary>
        /// Override the Calculate method to calculate amount of shipping discount for coupon and non-coupon based promtions 
        /// </summary>
        public override void Calculate()
        {
            var subTotal = ShoppingCart.SubTotal;
            
            if (PromotionProperty.OrderMinimum <= subTotal && PromotionProperty.Coupon == null)
            {
                ShoppingCart.Shipping.ShippingDiscount += PromotionProperty.Discount;
            }
            else if (PromotionProperty.Coupon != null)
            {
                var isCouponValid = ValidateCoupon();

                // apply discount
                if (PromotionProperty.Coupon.OrderMinimum <= subTotal && isCouponValid)
                {
                    ShoppingCart.Shipping.ShippingDiscount += PromotionProperty.Coupon.Discount;

                    PromotionProperty.Coupon.CouponApplied = true;
                    ShoppingCart.CouponApplied = true;
                }

                AddPromotionMessage();
            }
        }
    }
}

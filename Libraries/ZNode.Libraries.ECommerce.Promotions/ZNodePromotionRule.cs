using System.Collections.Generic;
using System.Text;
using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Base class for doing applying Promotion Rules on the contents of a Shopping Cart.
    /// </summary>
	public class ZNodePromotionRule : IZNodePromotionRule
    {
        private const string _CouponCodeApplied = "Coupon code accepted";
        private const string _InvalidCouponCode = "Invalid coupon code";
        private const string _CouponCodeAccepted = "Coupon criteria not met";
        private string _ClassName = string.Empty;
        private string _Name = string.Empty;
        private string _Description = string.Empty;
        private ZNodeShoppingCart _ShoppingCart;
        private ZNodePromotionProperty _PromotionProperty;
        private int _Precedence = 0;

        /// <summary>
        /// Initializes a new instance of the ZNodePromotionRule class.
        /// </summary>
        protected ZNodePromotionRule()
        {
        }

        /// <summary>
        /// Gets the current class name.
        /// </summary>
        public string ClassName
        {
            get { return _ClassName; }
        }

        /// <summary>
        /// Gets the name of this promotion.
        /// </summary>
        public string Name
        {
            get { return _Name; }
        }

        /// <summary>
        /// Gets the description for this promotion.
        /// </summary>
        public string Description
        {
            get { return _Description; }
        }

        /// <summary>
        /// Gets or sets the order of precidence that this promotions should be applied.
        /// </summary>
        public int Precedence
        {
            get { return _Precedence; }
            set { _Precedence = value; }
        }

        /// <summary>
        /// Gets or sets the promotion property
        /// </summary>
        protected ZNodePromotionProperty PromotionProperty
        {
            get { return _PromotionProperty; }
            set { _PromotionProperty = value; }
        }

        /// <summary>
        /// Gets or sets the Shopping cart
        /// </summary>
        protected ZNodeShoppingCart ShoppingCart
        {
            get { return _ShoppingCart; }
            set { _ShoppingCart = value; }
        }

        /// <summary>
        /// Binds the Shopping Cart and Promtion data to the Promotion Rule.
        /// </summary>
        /// <param name="ShoppingCart">The current shopping cart.</param>
        /// <param name="PromotionProperty">The promotions properties.</param>
        public void Bind(ZNodeShoppingCart ShoppingCart, ZNodePromotionProperty PromotionProperty)
        {
            _ShoppingCart = ShoppingCart;
            _PromotionProperty = PromotionProperty;
        }

        /// <summary>
        /// Do any prepration needed before the order is submitted.        
        /// </summary>
        /// <returns>True if everything is good for submitting the order.</returns>
        /// <remarks>Declared virtual so it can be overridden.</remarks>
        public virtual bool PreSubmitOrderProcess()
        {
            // Most promotion rules don't need any special verification.
            return true;
        }

        /// <summary>
        /// Calculate this promotion and update the Shopping Cart with the value in the appropriate place.
        /// </summary>
        public virtual void Calculate()
        {
        }

        /// <summary>
        /// Does any post purchase cleanup such as decreasing coupon counts.
        /// </summary>
        public virtual void PostSubmitOrderProcess()
        {
            // Most promotion rules will not need any further processing after the order is submitted.
        }

        /// <summary>
        /// Validate the coupon codes.
        /// </summary>
        /// <returns>Validate the coupon</returns>
        protected bool ValidateCoupon()
        {
            var isValid = true;

            if (_PromotionProperty.Coupon != null)
            {
                isValid = _ShoppingCart.Coupon.ToLower() == _PromotionProperty.Coupon.CouponCode.ToLower();

                if (isValid)
                {
                    _PromotionProperty.Coupon.CouponValid = true;
                    _ShoppingCart.CouponValid = true;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Add coupon based promotion description
        /// </summary>
        protected void AddPromotionMessage()
        {
            if (_PromotionProperty.Coupon != null && _PromotionProperty.Coupon.CouponCode.ToLower() == _ShoppingCart.Coupon.ToLower())
            {
                if (_PromotionProperty.Coupon.CouponValid && _PromotionProperty.Coupon.CouponApplied)
                {
                    if (_PromotionProperty.Coupon.PromotionMessage.Length > 0)
                    {
                        _ShoppingCart.AddPromoDescription = _PromotionProperty.Coupon.PromotionMessage + " " + _PromotionProperty.Coupon.CouponCode;
                    }
                    else
                    {
                        _ShoppingCart.AddPromoDescription = _CouponCodeApplied + " " + _PromotionProperty.Coupon.CouponCode;
                    }
                }
                else if (_PromotionProperty.Coupon.CouponValid)
                {
                    _ShoppingCart.AddPromoDescription = _CouponCodeAccepted + " " + _PromotionProperty.Coupon.CouponCode;
                }
            }
        }

        /// <summary>
        /// Is this the last Item
        /// </summary>
        /// <param name="CurrentItem">Shopping Cart current Item </param>
        /// <returns>Returns true if it is lasts item else false</returns>
        protected bool IsLastItem(ZNodeShoppingCartItem CurrentItem)
        {
            var indexes = new List<int>();
            var isLastLineItem = false;
            var index = 0;

            var currentItemIndex = _ShoppingCart.ShoppingCartItems.IndexOf(CurrentItem);

            if (CurrentItem != null)
            {
                foreach (ZNodeShoppingCartItem Item in _ShoppingCart.ShoppingCartItems)
                {
                    if (CurrentItem.Product.ProductID == Item.Product.ProductID)
                    {
                        index = _ShoppingCart.ShoppingCartItems.IndexOf(Item);
                        indexes.Add(index);
                    }
                }

                if (indexes.Count == 1)
                {
                    isLastLineItem = true;
                }

                if (indexes.IndexOf(currentItemIndex) == indexes.LastIndexOf(index))
                {
                    isLastLineItem = true;
                }
            }

            return isLastLineItem;
        }

        /// <summary>
        /// Returns total quantity ordered for this product
        /// </summary>
        /// <param name="ProductId">product id</param>                
        /// <returns>Returns the quantity ordered</returns>
        protected int GetQuantityOrdered(int ProductId)
        {
            var qty = 0;

            foreach (ZNodeShoppingCartItem cartItem in _ShoppingCart.ShoppingCartItems)
            {
                if (cartItem.Product.ProductID == ProductId)
                {
                    qty += cartItem.Quantity;
                }
            }

            return qty;
        }
    }
}

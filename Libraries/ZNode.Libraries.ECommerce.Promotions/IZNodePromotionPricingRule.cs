﻿using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Interface for the ZNodePromotionPricingRule
    /// </summary>
    public interface IZNodePromotionPricingRule
    {
        int Precedence 
        { 
            get; 
            set; 
        }

        void Bind(ZNodePromotionProperty PromotionProperty);        

        decimal PromotionalPrice(ZNodeProductBaseEntity Product, decimal CurrentPrice);
    }
}

﻿using System;
using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    public partial class ZNodePromotionAmountOffProduct : ZNodePromotionRule
    {
        /// <summary>
        /// Extension for Method Calculate the Calculate method to calculate amount of product discount for coupon and non-coupon based promtions.
        /// </summary>
        public void CalculateExtn()
        {
            try
            {
                decimal basePrice = 0;

                var isCouponValid = ValidateCoupon();
                isCouponValid &= PromotionProperty.Coupon != null;

                var subTotal = ShoppingCart.SubTotal;

                // Loop through each cart Item
                foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
                {
                    string productExcludeDiscount = !string.IsNullOrEmpty(cartItem.Product.Custom1) ? cartItem.Product.Custom1 : "0";
                    if (cartItem.Product.ProductID == PromotionProperty.ProductId && productExcludeDiscount.Equals("0"))
                    {
                        // Tiered Pricing calcaultion                
                        basePrice = cartItem.TieredPricing;
                        var qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

                        if (PromotionProperty.Coupon == null && PromotionProperty.QuantityMinimum <= qtyOrdered && PromotionProperty.OrderMinimum <= subTotal)
                        {
                            // Product Discount amount
                            cartItem.Product.DiscountAmount += PromotionProperty.Discount;
                        }
                        else if (isCouponValid)
                        {
                            // apply discount
                            if (PromotionProperty.Coupon.QuantityMinimum <= qtyOrdered && PromotionProperty.Coupon.OrderMinimum <= subTotal)
                            {
                                // Product Discount amount
                                cartItem.Product.DiscountAmount += PromotionProperty.Coupon.Discount;

                                PromotionProperty.Coupon.CouponApplied = true;
                                ShoppingCart.CouponApplied = true;
                            }
                        }
                    }
                }

                // Add Coupon display message
                if (PromotionProperty.Coupon != null)
                {
                    AddPromotionMessage();
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNodePromotionAmountOffProduct!!Method>>CalculateExtn!!" + ex.ToString());
            }
        }
        
    }
}

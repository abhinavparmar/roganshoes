using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Provides methods to calculate Coupon discounts
    /// </summary>
    [Serializable]
    [XmlRoot("ZNodeCoupon")]
    public class ZNodeCoupon : ZNodeBusinessBase
    {
        private int _CouponID;
        private string _CouponCode;
        private decimal _Discount;
        private int _DiscountTypeID;
        private DateTime _StartDate;
        private DateTime _ExpDate;
        private int _QuantityAvailable;
        private int _ProductId;
        private int _profileID;
        private decimal _OrderMinimum;
        private int _QuantityMinimum;
        private int _PromotionProductQty;
        private int? _PromotionProductId;
        private string _PromotionMessage;
        private bool _CouponValid = false;
        private bool _CouponApplied = false;

        /// <summary>
        /// Initializes a new instance of the ZNodeCoupon class.
        /// </summary>
        public ZNodeCoupon()
        {
        }

        /// <summary>
        /// Gets or sets the Coupon id
        /// </summary>
        [XmlElement]
        public int CouponID
        {
            get { return _CouponID; }
            set { _CouponID = value; }
        }

        /// <summary>
        /// Gets or sets the coupon code
        /// </summary>
        [XmlElement]
        public string CouponCode
        {
            get { return _CouponCode; }
            set { _CouponCode = value; }
        }

        /// <summary>
        /// Gets or sets the coupon code
        /// </summary>
        [XmlElement]
        public string PromotionMessage
        {
            get
            {
                if (_PromotionMessage == null)
                {
                    return "Coupon Sucessfully Applied";
                }
                else if (_PromotionMessage.Trim().Length == 0)
                {
                    return "Coupon Sucessfully Applied";
                }

                return _PromotionMessage;
            }

            set
            {
                _PromotionMessage = value;
            }
        }

        /// <summary>
        /// Gets or sets the discount value for this coupon
        /// </summary>
        [XmlElement]
        public decimal Discount
        {
            get { return _Discount; }
            set { _Discount = value; }
        }

        /// <summary>
        /// Gets or sets the discount typeid for this coupon
        /// </summary>
        [XmlElement]
        public int DiscountTypeID
        {
            get { return _DiscountTypeID; }
            set { _DiscountTypeID = value; }
        }

        /// <summary>
        /// Gets or sets the begin date for this coupon
        /// </summary>
        [XmlElement]
        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }

        /// <summary>
        /// Gets or sets the end date for this coupon
        /// </summary>
        [XmlElement]
        public DateTime ExpDate
        {
            get { return _ExpDate; }
            set { _ExpDate = value; }
        }

        /// <summary>
        /// Gets or sets the available quantity for this coupon
        /// </summary>
        [XmlElement]
        public int QuantityAvailable
        {
            get { return _QuantityAvailable; }
            set { _QuantityAvailable = value; }
        }

        /// <summary>
        /// Gets or sets the product list for this coupon 
        /// </summary>
        [XmlElement]
        public int ProductId
        {
            get { return _ProductId; }
            set { _ProductId = value; }
        }

        /// <summary>
        /// Gets or sets the discounttargetid for this coupon
        /// </summary>
        [XmlElement]
        public int ProfileId
        {
            get { return _profileID; }
            set { _profileID = value; }
        }

        /// <summary>
        /// Gets or sets the order minimum value for this coupon
        /// </summary>
        [XmlElement]
        public decimal OrderMinimum
        {
            get { return _OrderMinimum; }
            set { _OrderMinimum = value; }
        }

        /// <summary>
        /// Gets or sets the quantity minimum value for this coupon
        /// </summary>
        [XmlElement]
        public int QuantityMinimum
        {
            get { return _QuantityMinimum; }
            set { _QuantityMinimum = value; }
        }

        /// <summary>
        /// Gets or sets the promotion product quantity value for this coupon
        /// </summary>
        [XmlElement]
        public int PromotionProductQuantity
        {
            get { return _PromotionProductQty; }
            set { _PromotionProductQty = value; }
        }

        /// <summary>
        /// Gets or sets the promotion product id for this coupon
        /// </summary>
        [XmlElement]
        public int? PromotionProductId
        {
            get { return _PromotionProductId; }
            set { _PromotionProductId = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether coupon applied or not.
        /// </summary>
        public bool CouponApplied
        {
            get { return _CouponApplied; }
            set { _CouponApplied = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether coupon is valid or not.
        /// </summary>
        public bool CouponValid
        {
            get { return _CouponValid; }
            set { _CouponValid = value; }
        }
    }
}
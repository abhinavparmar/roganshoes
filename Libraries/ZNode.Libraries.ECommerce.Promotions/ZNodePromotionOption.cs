using System;
using System.Web;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using System.Web.Security;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Provides services for instantiating and managing promotion rules as well as applying Promotions to a Shopping Cart.
    /// </summary>
    public class ZNodePromotionOption : ZNodeBusinessBase
    {
        private ZNodeShoppingCart _ShoppingCart;
        private ZNodeGenericCollection<IZNodePromotionRule> _PromotionRules = null;

        private const string _CouponCodeAccepted = "Coupon code accepted";
        private const string _InvalidCouponCode = "Invalid coupon code";

        /// <summary>
        /// Get the promotions from the cache.
        /// </summary>
        protected TList<Promotion> PromotionCache
        {
            get
            {
                var promotionList = new TList<Promotion>();

                // Application object holds the list of active promotions
                if (HttpRuntime.Cache["PromotionCache"] == null)
                {
                    CacheActivePromotions();
                }

                //
                if (HttpRuntime.Cache["PromotionCache"] != null)
                {
                    promotionList = HttpRuntime.Cache["PromotionCache"] as TList<Promotion>;
                }

                if (promotionList == null || (promotionList != null && promotionList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!! ZNodePromotionOption:- Error in PromotionExtnCache!!Reload Promotion Cache");
                    CacheActivePromotions();
                    if (HttpRuntime.Cache["PromotionCache"] != null)
                    {
                        promotionList = HttpRuntime.Cache["PromotionCache"] as TList<Promotion>;
                    }
                }

                //promotionList.Sort("DisplayOrder");

                return promotionList;
            }
        }

        /// <summary>
        /// PromotionExtnCache
        /// </summary>
        protected TList<PromotionExtn> PromotionExtnCache
        {
            get
            {
                var promotionExtnList = new TList<PromotionExtn>();

                TList<Promotion> promotionList = PromotionCache;

                // Application object holds the list of active promotions
                if (HttpRuntime.Cache["PromotionExtnCache"] == null)
                {
                    CacheActivePromotionsExtn();
                }

                //
                if (HttpRuntime.Cache["PromotionExtnCache"] != null)
                {
                    promotionExtnList = HttpRuntime.Cache["PromotionExtnCache"] as TList<PromotionExtn>;
                }

                if (promotionExtnList == null && (promotionExtnList != null && promotionExtnList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionOption:- Error in PromotionExtnCache!!Reload PromotionExtn Cache");
                    CacheActivePromotionsExtn();
                    if (HttpRuntime.Cache["PromotionExtnCache"] != null)
                    {
                        promotionExtnList = HttpRuntime.Cache["PromotionExtnCache"] as TList<PromotionExtn>;
                    }
                }

                if (promotionExtnList != null && promotionExtnList.Count > 0 && promotionList != null && promotionList.Count != promotionExtnList.Count)
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionOption:-Error in PromotionExtnCache!!Promotion and PromotionExt Data is Mismatch");
                }

                return promotionExtnList;
            }
        }

        /// <summary>
        /// The default constructor is hiddend because this class requires the ShoppingCart to work.
        /// </summary>
        protected ZNodePromotionOption()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This constructor will instantiate all the appropriate promotion rules that are required.
        /// </summary>
        /// <param name="ShoppingCart">The current instance of the Shopping Cart.</param>
        public ZNodePromotionOption(ZNodeShoppingCart ShoppingCart)
        {
            _ShoppingCart = ShoppingCart;

            _PromotionRules = new ZNodeGenericCollection<IZNodePromotionRule>();
            System.Reflection.Assembly promoRuleAssembly = System.Reflection.Assembly.GetExecutingAssembly();

            var assemblyName = promoRuleAssembly.GetName().Name;

            var promotionService = new PromotionService();
            var promotionList = (TList<Promotion>)PromotionCache.Clone();

            Profile profile = null;

            if (HttpContext.Current.Session["ProfileCache"] != null)
            {
                // Retrieve profile
                profile = HttpContext.Current.Session["ProfileCache"] as Profile;
            }

            int? CurrentProfileID = null;
            if (profile != null)
            {
                CurrentProfileID = profile.ProfileID;
            }
            else
            {
                //PRFT Custom Code To Solve ProfileID null Issue:Starts
                CurrentProfileID = ZNodeConfigManager.SiteConfig.DefaultRegisteredProfileID.GetValueOrDefault();
                if (CurrentProfileID == null)
                {
                    HttpContext.Current.Session.Remove(ZNode.Libraries.Framework.Business.ZNodeSessionKeyType.UserAccount.ToString());
                    HttpContext.Current.Session["ProfileCache"] = null; // Reset profile cache    
                    HttpContext.Current.Session.Abandon();
                    FormsAuthentication.SignOut();
                }
                //PRFT Custom Code To Solve ProfileID null Issue:Ends
            }

            // Find is default store, so we include all profiles.
            var categoryService = new CategoryService();
            int total;
            var categoryList = categoryService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID, 0, 1, out total);
            var isDefaultPortal = categoryList.Count == 0;

            // Filter based on User's Profile, whether or not we're interested in checking coupon related promotions, and only look at cart based Promotions.
            if (string.IsNullOrEmpty(_ShoppingCart.Coupon))
            {
                promotionList.ApplyFilter(delegate(Promotion promo)
                {
                    return (promo.ProfileID == CurrentProfileID || promo.ProfileID == null && isDefaultPortal) &&
                           (promo.CouponInd == false) &&
                           promo.DiscountTypeIDSource.ClassType.Equals("CART", StringComparison.OrdinalIgnoreCase);
                });
            }
            else
            {
                promotionList.ApplyFilter(delegate(Promotion promo)
                {
                    return (promo.CouponCode.ToLower() == _ShoppingCart.Coupon.ToLower() || promo.CouponInd == false) &&
                           (promo.ProfileID == CurrentProfileID || promo.ProfileID == null && isDefaultPortal) &&
                           promo.DiscountTypeIDSource.ClassType.Equals("CART", StringComparison.OrdinalIgnoreCase);
                });
            }

            promotionList.Sort("DisplayOrder");

            // Build a list of rules that are appropriate for this instance of this class from the Promotion Cache.            
            // Promotion Rule Factory
            foreach (var promo in promotionList)
            {
                var promotionProperty = new ZNodePromotionProperty();
                promotionProperty.Discount = promo.Discount;
                promotionProperty.OrderMinimum = promo.OrderMinimum.GetValueOrDefault(0);
                promotionProperty.Discount = promo.Discount;
                promotionProperty.ProductId = promo.ProductID.GetValueOrDefault(0);
                promotionProperty.QuantityMinimum = promo.QuantityMinimum.GetValueOrDefault(1);
                promotionProperty.PromotionProductId = promo.PromotionProductID.GetValueOrDefault(0);
                promotionProperty.PromotionalProductQuantity = promo.PromotionProductQty.GetValueOrDefault(1);

                if (promo.CouponInd)
                {
                    promotionProperty.Coupon = GetCoupon(promo);
                }

                var className = promo.DiscountTypeIDSource.ClassName;

                // Instantiate the Promotion based on class name.
                if (!string.IsNullOrEmpty(className))
                {
                    try
                    {
                        var promoRule = (IZNodePromotionRule)promoRuleAssembly.CreateInstance(assemblyName + "." + className);
                        promoRule.Precedence = promo.DisplayOrder;

                        promoRule.Bind(_ShoppingCart, promotionProperty);
                        _PromotionRules.Add(promoRule);
                    }
                    catch (Exception ex)
                    {
                        ZNodeLogging.LogMessage("Error while instantiating promotion: " + className);
                        ZNodeLogging.LogObject(typeof(Exception), ex);
                    }
                }
            }

            promotionList.Dispose();
        }

        /// <summary>
        /// Do any prepration needed before the order is submitted.
        /// </summary>
        /// <returns>True if everything is good for submitting the order.</returns>
        public bool PreSubmitOrderProcess()
        {
            // Make sure the coupons are in good order before letting the customer check out.
            var allPreConditionsOk = true;

            foreach (ZNodePromotionRule rule in _PromotionRules)
            {
                allPreConditionsOk &= rule.PreSubmitOrderProcess();
            }

            return allPreConditionsOk;
        }

        public void Calculate()
        {
            // Reset previous property values
            if (_ShoppingCart == null)
                return;

            _ShoppingCart.OrderLevelDiscount = 0;
            _ShoppingCart.Shipping.ShippingDiscount = 0;
            _ShoppingCart.CouponApplied = false;

            var pricingOption = new ZNodePromotionPricingOption();

            foreach (ZNodeShoppingCartItem cartItem in _ShoppingCart.ShoppingCartItems)
            {
                cartItem.PromotionalPrice = pricingOption.PromotionalPrice(cartItem.Product, cartItem.TieredPricing);

                cartItem.ExtendedPriceDiscount = 0;
                cartItem.Product.DiscountAmount = 0;
                //Zeon Custom Discount Percent Implementation:Starts
                cartItem.Product.PromotionDiscountPercent = 0;
                //Zeon Custom Discount Percent Implementation:Ends
                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        addOnValue.DiscountAmount = 0;
                    }
                }
            }

            _PromotionRules.Sort("Precedence");

            foreach (ZNodePromotionRule rule in _PromotionRules)
            {
                rule.Calculate();
            }

            if (!_ShoppingCart.CouponApplied && !_ShoppingCart.CouponValid && !string.IsNullOrEmpty(_ShoppingCart.Coupon))
            {
                _ShoppingCart.AddPromoDescription = _InvalidCouponCode + " " + _ShoppingCart.Coupon;

                _ShoppingCart.Coupon = "";
            }
        }

        /// <summary>
        /// Does any post purchase cleanup such as decreasing coupon counts.
        /// </summary>
        public void PostSubmitOrderProcess()
        {
            foreach (ZNodePromotionRule rule in _PromotionRules)
            {
                rule.PostSubmitOrderProcess();
            }

            var promotionService = new PromotionService();

            if (_ShoppingCart.CouponApplied)
            {
                var query = new PromotionQuery();
                query.Append(PromotionColumn.CouponCode, _ShoppingCart.Coupon);

                // Reduce the quantity of available coupons.
                var list = promotionService.Find(query.GetParameters());

                foreach (Promotion entity in list)
                {
                    if (entity.CouponQuantityAvailable.GetValueOrDefault(0) > 0)
                    {
                        entity.CouponQuantityAvailable -= 1;
                        promotionService.Update(entity);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns></returns>
        private ZNodeCoupon GetCoupon(Promotion Entity)
        {
            var coupon = new ZNodeCoupon();

            coupon.PromotionProductId = Entity.PromotionProductID;
            coupon.QuantityMinimum = Entity.QuantityMinimum.GetValueOrDefault(1);
            coupon.PromotionProductQuantity = Entity.PromotionProductQty.GetValueOrDefault(1);
            coupon.CouponCode = Entity.CouponCode;
            coupon.CouponID = Entity.PromotionID;
            coupon.Discount = Entity.Discount;
            coupon.DiscountTypeID = Entity.DiscountTypeID;
            coupon.OrderMinimum = Entity.OrderMinimum.GetValueOrDefault(0);
            coupon.PromotionMessage = Entity.PromotionMessage;
            coupon.QuantityAvailable = Entity.CouponQuantityAvailable.GetValueOrDefault(0);
            coupon.ProductId = Entity.ProductID.GetValueOrDefault(0);
            coupon.ProfileId = Entity.ProfileID.GetValueOrDefault(0);

            return coupon;
        }

        /// <summary>
        /// Retrieves all active promotions and adds the list object to Application state object
        /// </summary>
        public static void CacheActivePromotions()
        {
            // Application object holds the list of active promotions
            if (HttpRuntime.Cache["PromotionCache"] == null)
            {
                var promotionService = new PromotionService();
                var promotionList = promotionService.GetAll();

                promotionList.ApplyFilter(delegate(Promotion promo) { return DateTime.Today.Date >= promo.StartDate.Date && DateTime.Today.Date <= promo.EndDate.Date && ((promo.CouponQuantityAvailable > 0 && promo.CouponInd) || promo.CouponInd == false); });
                promotionList.Sort("DisplayOrder");

                if (promotionList == null || (promotionList != null && promotionList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:-After Apply Error in promotionList is null!");
                }

                // Deepload discount type
                promotionService.DeepLoad(promotionList, true, DeepLoadType.IncludeChildren, typeof(DiscountType));

                if (promotionList == null || (promotionList != null && promotionList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionPricingOption:-After DeepLoad Error in promotionList is null!");
                }
                string[] tableNames = { "ZNodePromotion", "ZNodePromotionExtn" };
                ZNodeCacheDependencyManager.Insert("PromotionCache", promotionList, tableNames);
            }
        }

        /// <summary>
        /// Retrieves all active promotions extn data and adds the list object to Application state object
        /// </summary>
        public static void CacheActivePromotionsExtn()
        {
            // Application object holds the list of active promotions
            if (HttpRuntime.Cache["PromotionExtnCache"] == null)
            {
                var promotionExtnService = new PromotionExtnService();
                //PromotionExtnQuery promotionExtnQuery = new PromotionExtnQuery();
                //promotionExtnQuery.AppendInQuery(PromotionExtnColumn.PromotionExtnID, "SELECT PromotionExtnID  FROM ZNodePromotionExtn WHERE PortalID=" + ZNodeConfigManager.SiteConfig.PortalID.ToString());
                //var promotionExtnList = promotionExtnService.Find(promotionExtnQuery.GetParameters());

                var promotionExtnList = promotionExtnService.GetAll();

                if (promotionExtnList == null || (promotionExtnList != null && promotionExtnList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionOption:- Error in promotionExtnList is null!");
                }

                promotionExtnList = FilterPromoExt(promotionExtnList);

                if (promotionExtnList == null || (promotionExtnList != null && promotionExtnList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionOption:- FilterPromoExt Error in promotionExtnList is null!");
                }

                // Deep load
                promotionExtnService.DeepLoad(promotionExtnList);

                if (promotionExtnList == null || (promotionExtnList != null && promotionExtnList.Count == 0))
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!ZNodePromotionOption:- After DeepLoad Error in promotionExtnList is null!");
                }
                string[] tableNames = { "ZNodePromotion", "ZNodePromotionExtn" };
                ZNodeCacheDependencyManager.Insert("PromotionExtnCache", promotionExtnList, tableNames);
            }
        }

        /// <summary>
        /// Filter Promo Ext
        /// </summary>
        /// <param name="promoExt"></param>
        /// <returns></returns>
        public static TList<PromotionExtn> FilterPromoExt(TList<PromotionExtn> promoExt)
        {
            TList<PromotionExtn> promoActiveExt = new TList<PromotionExtn>();
            if (HttpRuntime.Cache["PromotionCache"] != null)
            {
                TList<Promotion> promotionList = HttpRuntime.Cache["PromotionCache"] as TList<Promotion>;
                if (promotionList != null && promotionList.Count > 0)
                {
                    foreach (Promotion promo in promotionList)
                    {
                        promoActiveExt.Add(promoExt.Find((e) => { return (e.PromotionID == promo.PromotionID); }));
                    }
                }
            }
            if (promoActiveExt != null && promoActiveExt.Count > 0)
            {
                return promoActiveExt;
            }
            else
            {
                return promoExt;
            }
        }

    }
}
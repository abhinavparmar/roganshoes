﻿using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
	/// Interface for ZNodePromotionRule
    /// </summary>
	public interface IZNodePromotionRule
    {
        string ClassName { get; }

        string Description { get; }

        string Name { get; }

        int Precedence { get; set; }

        void Bind(ZNodeShoppingCart ShoppingCart, ZNodePromotionProperty PromotionProperty);

        void Calculate();

        void PostSubmitOrderProcess();
       
        bool PreSubmitOrderProcess();    
    }
}

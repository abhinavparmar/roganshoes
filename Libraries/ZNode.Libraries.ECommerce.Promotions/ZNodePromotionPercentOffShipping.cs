namespace ZNode.Libraries.ECommerce.Promotions
{
    /// <summary>
    /// Promotion Ruile for doing a percent off shipping.
    /// </summary>
	public class ZNodePromotionPercentOffShipping : ZNodePromotionRule
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePromotionPercentOffShipping class.
        /// </summary>
        public ZNodePromotionPercentOffShipping() : base()
        { 
        }

        /// <summary>
        /// Override the Calculate method to calculate percentage of shipping discount for coupon and non-coupon based promtions 
        /// </summary>
        public override void Calculate()
        {
            var shippingCost = ShoppingCart.ShippingCost;
            var subTotal = ShoppingCart.SubTotal;
            
            if (PromotionProperty.OrderMinimum <= subTotal && PromotionProperty.Coupon == null)
            {
                ShoppingCart.Shipping.ShippingDiscount += shippingCost * (PromotionProperty.Discount / 100);
            }
            else if (PromotionProperty.Coupon != null)
            {
                var isCouponValid = ValidateCoupon();

                if (PromotionProperty.Coupon.OrderMinimum <= subTotal && isCouponValid)
                {
                    var discount = shippingCost * (PromotionProperty.Coupon.Discount / 100);

                    PromotionProperty.Coupon.CouponApplied = true;
                    ShoppingCart.CouponApplied = true;

                    // apply shipping discount
                    ShoppingCart.Shipping.ShippingDiscount += discount;                    
                }

                AddPromotionMessage();
            }
        }
    }
}

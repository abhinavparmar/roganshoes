﻿using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Promotions
{
    public partial class ZNodePromotionProperty : ZNodeBusinessBase
    {
        #region[private varible]

        private int _promotionID = 0;
        private string _custom1 = string.Empty;
        private string _custom2 = string.Empty;

        #endregion

        #region[Public propety]

        public int PromotionID
        {
            get { return _promotionID; }
            set { _promotionID = value; }

        }

        public string Custom1
        {
            get { return _custom1; }
            set { _custom1 = value; }
        }

        public string Custom2
        {
            get { return _custom2; }
            set { _custom2 = value; }
        }

        #region[Promotion Extn Column]
        public int CategoryID { get; set; }
        public int ManufacturerID { get; set; }
        public int CatalogID { get; set; }
        public int PortalID { get; set; }
        #endregion

        #endregion
    }
}

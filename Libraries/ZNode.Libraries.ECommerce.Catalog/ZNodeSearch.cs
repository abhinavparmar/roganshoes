using System.Data;
using System.Text;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Provides method to search the product catalogs
    /// </summary>
    public class ZNodeSearch : ZNodeBusinessBase
    {
        /// <summary>
        /// Initializes a new instance of the ZNodeSearch class
        /// </summary>
        public ZNodeSearch()
        {
        }

        /// <summary>
        /// Get the paged products.
        /// </summary>
        /// <param name="productTable">Datatable that contains preloaded data.</param>
        /// <param name="orderBy">Sort the the product by field</param>
        /// <param name="orderDirection">Sort the product by direction (ASC or DESC)</param>
        /// <param name="pageIndex">Row number at which to start reading.</param>
        /// <param name="pageSize">Number of rows to return.</param>
        /// <param name="orderOption">Order by option.</param>
        /// <returns>Returns the sorted ZNodeProductList object.</returns>
        public ZNodeProductList GetProductList(DataTable productTable, string orderBy, string orderDirection, int pageIndex, int pageSize, int orderOption)
        {
            return ZNodePaging.GetProducts(productTable, orderBy, orderDirection, pageIndex, pageSize, orderOption);
        }

        /// <summary>
        /// Search products and return ZNodeProductList object
        /// </summary>
        /// <param name="portalId">The portal Id to search for.</param>        
        /// <param name="catalogId">The Catalog id to search for.</param>
        /// <param name="keywords">Keyword to search.</param>
        /// <param name="delimiter">Delimiter character</param>
        /// <param name="categoryId">Product category Id.</param>
        /// <param name="searchOption">Search option (0, 1, 2)</param>
        /// <param name="sku">The product sku to search for.</param>
        /// <param name="productNum">Product number to search.</param>     
        /// <param name="pageIndex">Product list page index.</param>
        /// <param name="pageSize">Product list page size</param>
        /// <param name="sortBy">Product list soft by field.</param>
        /// <param name="sortDirection">Product list sort direction.</param>
        /// <returns>Returns the ZNodeProductList object.</returns>
        public ZNodeProductList FindProducts(int portalId, int catalogId, string keywords, string delimiter, int categoryId, int searchOption, string sku, string productNum, int pageIndex, int pageSize, string sortBy, string sortDirection)
        {
            int totalPageCount = 0;
            int totalRecordCount = 0;

            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            StringBuilder sb = new StringBuilder();
            sb.Append("<ZNodeProductList>");
            sb.Append(productHelper.SearchProductsXML(portalId, catalogId, keywords, delimiter, categoryId, searchOption, sku, productNum, ZNodeProfile.CurrentUserProfileId));
            sb.Append("<TotalPageCount>");
            sb.Append(totalPageCount.ToString());
            sb.Append("</TotalPageCount>");

            sb.Append("<TotalRecordCount>");
            sb.Append(totalRecordCount.ToString());
            sb.Append("</TotalRecordCount>");
            sb.Append("</ZNodeProductList>");

            if (System.Web.HttpContext.Current.Session["ProductList"] != null)
            {
                return ZNodePaging.GetProducts((DataTable)System.Web.HttpContext.Current.Session["ProductList"], sortBy, sortDirection, pageIndex, pageSize, 0);
            }

            return null;
        }
    }
}
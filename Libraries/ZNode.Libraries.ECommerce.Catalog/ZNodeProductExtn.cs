﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.ECommerce.Catalog
{
    public partial class ZNodeProduct : ZNodeProductBase
    {
        #region Private Variables

        private DataTable _ProductAttributeCollection = new DataTable();
        #endregion

        #region Public Properties

        /// <summary>
        /// Get or Set _ProductAttributeCollection value
        /// </summary>
        public DataTable ProductAllAttributeCollection
        {
            get { return _ProductAttributeCollection; }
            set { _ProductAttributeCollection=value; }
        }

        #endregion

    }
}

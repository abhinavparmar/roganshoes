﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Entities;

namespace ZNode.Libraries.ECommerce.Catalog
{
    public class ZeonMessageConfig
    {

        /// <summary>
        /// Get the value of Message Key
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public string GetMessageKey(string messageKey)
        {
            ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigService = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
            TList<MessageConfig> messageConfig = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID(messageKey, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, 1);
            if (messageConfig != null && messageConfig.Count > 0)
            {
                return messageConfig[0].Value;
            }
            else
            {
                return string.Empty;
            }        
        }
    }
}

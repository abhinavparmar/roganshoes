using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the properties and methods of a product Categories.
    /// </summary>
    [Serializable()]
    [XmlRoot("ZNodeProductCategory")]
    public class ZnodeProductCategory : ZNodeBusinessBase
    {
        #region Private Member Variables
        private int _CategoryId;
        private int _ProductId;
        private int _DisplayOrder;
        private bool _ActiveInd;
        private string _MasterPage = string.Empty;
        private string _Theme = string.Empty;
        private string _Css = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the site portal Id
        /// </summary>
        [XmlElement()]
        public int ProductID
        {
            get { return this._ProductId; }
            set { this._ProductId = value; }
        }

        /// <summary>
        /// Gets or sets the category UniqueID
        /// </summary>
        [XmlElement()]
        public int CategoryID
        {
            get { return this._CategoryId; }
            set { this._CategoryId = value; }
        }

        /// <summary>
        /// Gets or sets the display order number
        /// </summary>
        [XmlElement()]
        public int DisplayOrder
        {
            get { return this._DisplayOrder; }
            set { this._DisplayOrder = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the category is active or not.
        /// </summary>
        [XmlElement()]
        public bool ActiveInd
        {
            get { return this._ActiveInd; }
            set { this._ActiveInd = value; }
        }

        /// <summary>
        /// Gets or sets the master page template for this product category
        /// </summary>
        [XmlElement()]
        public string MasterPage
        {
            get { return this._MasterPage; }
            set { this._MasterPage = value; }
        }

        /// <summary>
        ///  Gets or sets the Theme for this product category
        /// </summary>
        [XmlElement()]
        public string Theme
        {
            get { return this._Theme; }
            set { this._Theme = value; }
        }

        /// <summary>
        ///  Gets or sets the CSS for this product category
        /// </summary>
        [XmlElement()]
        public string CSS
        {
            get { return this._Css; }
            set { this._Css = value; }
        }

        #endregion
    }
}

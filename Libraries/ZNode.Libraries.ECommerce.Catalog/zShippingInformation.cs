﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{

    [Serializable()]
    [XmlRoot("ShippingInformation")]
    public class zShippingInformation : ZNodeBusinessBase
    {
        [XmlElement("ShippingInfo")]
        public ZNodeGenericCollection<ShippingInfo> ShippingInfo { get; set; }

        /// <summary>
        /// Return Shipping Information Object from XML
        /// </summary>
        /// <param name="shippingInformationXML"></param>
        /// <returns></returns>
        public zShippingInformation GetShippingInformation(string shippingInformationXML)
        {
            zShippingInformation shippingInformation = new zShippingInformation();
            try
            {
                // Serialize the object if XmlOut length is not equal to zero.
                if (shippingInformationXML.ToString().Trim().Length > 0)
                {
                    // Serialize the object
                    ZNodeSerializer serializer = new ZNodeSerializer();
                    shippingInformation = (zShippingInformation)serializer.GetContentFromString(shippingInformationXML.ToString(), typeof(zShippingInformation));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return shippingInformation;
        }
    }

    public class ShippingInfo
    {
        [XmlElement("TrackingNumber")]
        public string TrackingNumber { get; set; }

        [XmlElement("ShippingName")]
        public string Name { get; set; }

        [XmlElement("TransactionID")]
        public string TransactionID { get; set; }

    }


}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.SEO;
using System.Web;

namespace ZNode.Libraries.ECommerce.Catalog
{
    public partial class ZNodeCrossSellItem : ZNodeBusinessBase
    {
        #region Private Variables

        private int _CrossSellTypeID = 0;

        private bool _NewProductInd = false;
        private bool _FeaturedInd = false;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or Sets the CrossSellTypeId
        /// </summary>
        [XmlElement()]
        public int CrossSellTypeID
        {
            get
            {
                return _CrossSellTypeID;
            }
            set
            {
                _CrossSellTypeID = value;
            }
        }

        /// <summary>
        /// Gets or Sets the NewProductInd
        /// </summary>
        [XmlElement()]
        public bool NewProductInd
        {
            get
            {
                return _NewProductInd;
            }
            set
            {
                _NewProductInd = value;
            }
        }

        /// <summary>
        /// Gets or Sets the FeaturedInd
        /// </summary>
        [XmlElement()]
        public bool FeaturedInd
        {
            get
            {
                return _FeaturedInd;
            }
            set
            {
                _FeaturedInd = value;
            }
        }

        /// <summary>
        /// Gets or Sets the PreOrderFlag
        /// </summary>
        [XmlElement()]
        public bool PreOrderFlag
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or Sets the PreOrderFlag
        /// </summary>
        [XmlElement()]
        public bool IsMapPrice
        {
            get;
            set;
        }

        /// <summary>
        /// Get Or Set Comma Seperated Product Category IDs
        /// </summary>
        [XmlElement()]
        public string ProductCategoryHierarchy
        {
            get;
            set;
        }

        /// <summary>
        /// Custom 1
        /// </summary>
        [XmlElement()]
        public string Custom1
        {
            get;
            set;
        }

        /// <summary>
        /// Znode product category list
        /// </summary>
        [XmlElement("ZNodeProductCategoryList")]
        public ZNodeGenericCollection<ZNode.Libraries.ECommerce.Entities.ZNodeProductCategoryList> ZNodeProductCategoryList
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the product base price and returns it as string
        /// Check the user's Login status and display the price based on Showpricing Status.
        /// </summary>
        [XmlIgnore()]
        public string FormattedPriceExtn
        {
            get
            {
                decimal actualPrice = this.ProductPrice;
                decimal basePrice = this.PromotionalPrice;
                string returnPrice = string.Empty;

                if (actualPrice == basePrice)
                {
                    if (this.IsMapPrice)
                    {
                        returnPrice = ZNodePricingFormatter.ConfigureProductPricing(actualPrice);
                    }
                    else
                    {
                        returnPrice = ZNodePricingFormatter.ConfigureProductPricingRed(actualPrice);
                    }
                }
                else
                {
                    returnPrice = ZNodePricingFormatter.ConfigureProductPricing(actualPrice, basePrice);
                    //PRFT:Custom Code Starts PI Task 166
                    //returnPrice = returnPrice + FormatPriceDiscount(actualPrice, basePrice);
                    //PRFT:Custom Code Ends PI Task 166
                }

                return returnPrice.TrimEnd();
            }
        }


        /// <summary>
        /// Gets the final calculated price for a product without inclusive tax
        /// </summary>
        [XmlIgnore()]
        public decimal PromotionalPrice
        {
            get
            {
                ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption pricing = new ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption();
                ZNodeProductBase productBase = GetProduct(this);
                return pricing.PromotionalPrice(productBase);
            }
        }

        /// <summary>
        /// Gets the final calculated price for a product without inclusive tax
        /// </summary>
        [XmlIgnore()]
        public decimal ProductPrice
        {
            get
            {
                decimal basePrice = this._RetailPrice;
                // If sale price has value
                if (this.SalePrice.HasValue)
                {
                    // Then set sale price value
                    basePrice = this.SalePrice.Value;
                }

                return basePrice;
            }
        }

        //PRFT Custom Code : Start

        [XmlElement()]
        public string CallMessage
        {
            get;
            set;
        }
        //PRFT Custom Code : End
        #endregion

        #region[Zeon Custom Code::Custom Methods]

        /// <summary>
        /// Retrun Product Base Entity for Cross Sell Items
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ZNodeProductBase GetProduct(ZNodeCrossSellItem item)
        {
            ZNodeProductBase productBase = new ZNodeProductBase();
            productBase.ProductID = item.ProductId;
            productBase.RetailPrice = item.RetailPrice;
            productBase.IsMapPrice = item.IsMapPrice;
            productBase.SalePrice = item.SalePrice;
            productBase.Custom1 = item.Custom1;
            productBase.ProductCategoryHierarchy = item.ProductCategoryHierarchy;
            productBase.ZNodeProductCategoryList = item.ZNodeProductCategoryList;
            return productBase;
        }

        /// <summary>
        /// Calculate discount from origional price to discount Price
        /// </summary>
        /// <param name="descPrice"></param>
        /// <param name="oriPrice"></param>
        /// <returns></returns>
        private decimal GetFormattedDiscountValue(decimal oriPrice, decimal descPrice)
        {
            decimal discountVal = 0;
            if (oriPrice > 0 && descPrice > 0)
            {
                discountVal = (oriPrice - descPrice) * 100 / oriPrice;
                discountVal = decimal.Round(discountVal, 0);
            }
            return discountVal;
        }

        /// <summary>
        /// Format Discount Text
        /// </summary>
        /// <param name="retailprice"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        private string FormatPriceDiscount(decimal retailprice, decimal price)
        {
            string discPriceText = string.Empty;
            string discText = string.Empty;
            decimal discount = GetFormattedDiscountValue(retailprice, price);
            if (discount > 0)
            {
                if (ConfigurationManager.AppSettings["CheerAndPomPortalID"] != null && !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString()))
                {
                    if (ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString().Equals(ZNodeConfigManager.SiteConfig.PortalID.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        discText = HttpContext.GetGlobalResourceObject("CommonCaption", "CheerListDiscountText") == null ? "<span class='discount'>Save {0}%</span>" : HttpContext.GetGlobalResourceObject("CommonCaption", "CheerListDiscountText").ToString();
                    }
                    else
                    {
                        discText = HttpContext.GetGlobalResourceObject("CommonCaption", "RogansListDiscountText") == null ? "<span class='discount'>Save {0}%</span>" : HttpContext.GetGlobalResourceObject("CommonCaption", "RogansListDiscountText").ToString();

                    }

                    discPriceText = string.Format(discText, discount);
                }
            }
            return discPriceText;
        }

        #endregion
    }
}

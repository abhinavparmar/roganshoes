﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Xml.Serialization;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Provides methods for list of home specials products and collection of products for a category
    /// </summary>
    public partial class ZNodeProductList
    {
        /// <summary>
        /// Returns a product list of best seller items
        /// </summary>
        /// <param name="catalogID">Catalog Id of the Reqeusted Products</param>
        /// <param name="displayItem">Display Item of the Reqeusted Products</param>
        /// <param name="categoryID">Category Id of the Reqeusted Products</param>
        /// <param name="portalID">Portal Id of the Reqeusted Products</param>
        /// <returns>ZNodeProductList object</returns>
        public static ZNodeProductList GetNewArrivals(int displayItem, int portalID, int localeId)
        {
            StringBuilder xmlOut = new StringBuilder();
            xmlOut.AppendFormat("<ZNodeProductList>{0}</ZNodeProductList>", new ZNode.Libraries.DataAccess.Custom.ProductHelper().GetHomePageNewArrivalsXML(portalID, localeId, displayItem, ZNodeProfile.CurrentUserProfileId));

            return (ZNodeProductList)new ZNodeSerializer().GetContentFromString(xmlOut.ToString(), typeof(ZNodeProductList));
        }

        /// <summary>
        /// Get Compare products detail information
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static ZNodeProductList GetCompareProducts(string list, string upcSpecificationText, string alternatePartNumberSpecificationText)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            StringBuilder xmlOut = new StringBuilder();
            xmlOut.Append("<ZNodeProductList>");
            xmlOut.Append(productHelper.GetProductListByProductIDList(list, upcSpecificationText, alternatePartNumberSpecificationText));
            xmlOut.Append("</ZNodeProductList>");

            // serialize the object
            ZNodeSerializer ser = new ZNodeSerializer();
            ZNodeProductList productList = (ZNodeProductList)ser.GetContentFromString(xmlOut.ToString(), typeof(ZNodeProductList));

            //Zeon Customization :: productList.ApplyPromotion() as ApplyPromotion() promotion is when we are retriving product.price:: Start
            //productList.ApplyPromotion();
            //Zeon Customization :: productList.ApplyPromotion() as ApplyPromotion() promotion is when we are retriving product.price:: End

            return productList;
        }

        /// <summary>
        /// Returns a product list for a brand
        /// </summary>
        /// <param name="portalId">Portal Id of the request products</param>
        /// <param name="manufacturerId">Manufacturer (Brand) Id of the request products</param>
        /// <param name="localeId">Locale Id of the request products</param>
        /// <returns>ZNodeProductList object</returns>
        public static ZNodeProductList GetProductsListByBrand(int portalId, int manufacturerId, int localeId)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            StringBuilder xmlOut = new StringBuilder();
            xmlOut.Append("<ZNodeProductList>");
            xmlOut.Append(productHelper.GetProductsXMLByBrandID(portalId, manufacturerId, localeId));
            xmlOut.Append("</ZNodeProductList>");

            // serialize the object
            ZNodeSerializer ser = new ZNodeSerializer();
            ZNodeProductList productList = (ZNodeProductList)ser.GetContentFromString(xmlOut.ToString(), typeof(ZNodeProductList));

            //Zeon Customization :: productList.ApplyPromotion() as ApplyPromotion() promotion is when we are retriving product.price:: Start
            //productList.ApplyPromotion();
            //Zeon Customization :: productList.ApplyPromotion() as ApplyPromotion() promotion is when we are retriving product.price:: End

            return productList;
        }

        /// <summary>
        /// Get Latest Reviewed products detail information for Shop By Brands
        /// </summary>
        /// <param name="portalID">int</param>
        /// <param name="localID">int</param>
        /// <param name="manufactID">int</param>
        /// <param name="topRecordCount">int</param>
        /// <returns>ZNodeProductList</returns>
        public static ZNodeProductList GetBrandsTopReviewedProductList(int portalID, int localID, int manufactID, int topRecordCount)
        {
            ZNode.Libraries.DataAccess.Custom.ManufacturerHelper manufactHelper = new ZNode.Libraries.DataAccess.Custom.ManufacturerHelper();
            StringBuilder xmlOut = new StringBuilder();
            xmlOut.Append("<ZNodeProductList>");
            xmlOut.Append(manufactHelper.GetBrandsTopReviewedProductList(portalID, localID, manufactID, topRecordCount));
            xmlOut.Append("</ZNodeProductList>");

            // serialize the object
            ZNodeSerializer ser = new ZNodeSerializer();
            ZNodeProductList productList = (ZNodeProductList)ser.GetContentFromString(xmlOut.ToString(), typeof(ZNodeProductList));
            return productList;
        }

        /// <summary>
        /// Get Latest Reviewed products detail information for Shop By Brands
        /// </summary>
        /// <param name="portalID">int</param>
        /// <param name="localID">int</param>
        /// <param name="manufactID">int</param>
        /// <param name="topRecordCount">int</param>
        /// <returns>ZNodeProductList</returns>
        public static ZNodeProductList GetCategoryTopReviewedProductList(int portalID, int localID, int categoryID, int topRecordCount)
        {
            ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();
            StringBuilder xmlOut = new StringBuilder();
            xmlOut.Append("<ZNodeProductList>");
            xmlOut.Append(categoryHelper.GetTopReviewedProductList(portalID, localID, categoryID, topRecordCount));
            xmlOut.Append("</ZNodeProductList>");

            // serialize the object
            ZNodeSerializer ser = new ZNodeSerializer();
            ZNodeProductList productList = (ZNodeProductList)ser.GetContentFromString(xmlOut.ToString(), typeof(ZNodeProductList));
            return productList;
        }
    }
}

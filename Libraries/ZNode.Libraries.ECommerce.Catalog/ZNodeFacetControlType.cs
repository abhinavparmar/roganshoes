namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Specifies the control type of TagGroup
    /// </summary>
    public enum ZNodeFacetControlType
    { 
        /// <summary>
        /// Represents the label
        /// </summary>
        LABEL = 2 
        
    }
}

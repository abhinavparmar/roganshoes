using System;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;
using System.Data;

namespace ZNode.Libraries.ECommerce.Catalog
{
    #region ZNodeSKU
    /// <summary>
    /// Represents the properties and methods of a ZNodeSKU.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeSKU : ZNodeSKUEntity
    {

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ZNodeSKU class
        /// </summary>
        public ZNodeSKU()
        {
        }

        public ZNodeSKU(ZNodeSKUEntity skuEntity)
        {
            this.AttributesDescription = skuEntity.AttributesDescription;
            this.AttributesValue = skuEntity.AttributesValue;
            this.Custom1 = skuEntity.Custom1;
            this.Custom2 = skuEntity.Custom2;
            this.Custom3 = skuEntity.Custom3;
            this.DisplayOrder = skuEntity.DisplayOrder;
            this.ImageAltTag = skuEntity.ImageAltTag;
            this.IsActive = skuEntity.IsActive;
            this.Note = skuEntity.Note;
            this.ProductID = skuEntity.ProductID;
            this.QuantityBuffer = skuEntity.QuantityBuffer;
            this.QuantityOnHand = skuEntity.QuantityOnHand;
            this.RecurringBillingFrequency = skuEntity.RecurringBillingFrequency;
            this.RecurringBillingInitialAmount = skuEntity.RecurringBillingInitialAmount;
            this.RecurringBillingPeriod = skuEntity.RecurringBillingPeriod;
            this.RecurringBillingTotalCycles = skuEntity.RecurringBillingTotalCycles;
            this.ReorderLevel = skuEntity.ReorderLevel;            
            this.RetailPriceOverride = skuEntity.RetailPriceOverride;
            this.SalePriceOverride = skuEntity.SalePriceOverride;
            this.SKU = skuEntity.SKU;
            this.SKUID = skuEntity.SKUID;
            this.SKUPicturePath = skuEntity.SKUPicturePath;
            this.SupplierID = skuEntity.SupplierID;
            this.Surcharge = skuEntity.Surcharge;
            this.WarehouseNo = skuEntity.WarehouseNo;
            this.WeightAdditional = skuEntity.WeightAdditional;
            this.WholesalePriceOverride = skuEntity.WholesalePriceOverride;
            this.SkuProfileCollection = skuEntity.SkuProfileCollection;
        }
        #endregion

        #region Static Methods

        /// <summary>
        /// Gets sku by product attribute.
        /// </summary>
        /// <param name="productId">Product Id to get the Sku attribute</param>
        /// <param name="attributes">Attributes to get the product sku.</param>
        /// <returns>Returns the sku by product attributes.</returns>
        public static ZNodeSKU CreateByProductAndAttributes(int productId, string attributes)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            //string xmlOut = productHelper.GetSKUByAttributes(productId, attributes);//ZnodeOldCode
            string xmlOut = productHelper.GetSKUByAttributesExtn(productId, attributes);//Zeon Custom Code

            ZNodeSKU sku = new ZNodeSKU();

            // Serialize the object
            if (xmlOut.Trim().Length > 0)
            {
                ZNodeSerializer serializer = new ZNodeSerializer();
                sku = (ZNodeSKU)serializer.GetContentFromString(xmlOut, typeof(ZNodeSKU));
            }

            return sku;
        }

        /// <summary>
        /// Get a sku based on the SKUID
        /// </summary>
        /// <param name="productSku">Product SKU string</param>
        /// <returns>Returns the ZNodeSKU object</returns>
        public static ZNodeSKU CreateBySKU(string productSku)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            string xmlOut = productHelper.GetBySKU(productSku);

            ZNodeSKU sku = new ZNodeSKU();

            // Serialize the object
            if (xmlOut.Trim().Length > 0)
            {
                ZNodeSerializer serializer = new ZNodeSerializer();
                sku = (ZNodeSKU)serializer.GetContentFromString(xmlOut, typeof(ZNodeSKU));
            }
           
            return sku;
        }

       

        /// <summary>
        /// Returns the default sku for a product with NO attributes
        /// </summary>
        /// <param name="productId">Product Id to get the default sku.</param>
        /// <returns>Returns the default ZNodeSKU object.</returns>
        public static ZNodeSKU CreateByProductDefault(int productId)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            string xmlOut = productHelper.GetDefaultSKU(productId);

            ZNodeSKU sku = new ZNodeSKU();

            // Serialize the object
            if (xmlOut.Trim().Length > 0)
            {
                ZNodeSerializer serializer = new ZNodeSerializer();
                sku = (ZNodeSKU)serializer.GetContentFromString(xmlOut, typeof(ZNodeSKU));
            }
           
            return sku;
        }

      

        #endregion
    }
    #endregion
}

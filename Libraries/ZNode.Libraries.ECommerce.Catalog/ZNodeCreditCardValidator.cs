using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web.UI;

namespace ZNode.Libraries.ECommerce.Catalog
{
    [Flags, Serializable]
    public enum CardType
    {
        /// <summary>
        /// Master card type
        /// </summary>
        MasterCard = 0x0001,

        /// <summary>
        /// Visa card type
        /// </summary>
        VISA = 0x0002,

        /// <summary>
        /// Amex card type.
        /// </summary>
        Amex = 0x0004,

        /// <summary>
        /// DinersClud card type.
        /// </summary>
        DinersClub = 0x0008,

        /// <summary>
        /// enRoute card type.
        /// </summary>
        enRoute = 0x0010,

        /// <summary>
        /// Discover card type.
        /// </summary>
        Discover = 0x0020,

        /// <summary>
        /// JCB card type.
        /// </summary>
        JCB = 0x0040,

        /// <summary>
        /// Unknow card type.
        /// </summary>
        Unknown = 0x0080,

        /// <summary>
        /// All type of cards
        /// </summary>
        All = CardType.Amex | CardType.DinersClub | CardType.Discover | CardType.Discover |
            CardType.enRoute | CardType.JCB | CardType.MasterCard | CardType.VISA
    }

    /// <summary>
    /// Credit Card Validation web control
    /// </summary>
    public class ZNodeCreditCardValidator : System.Web.UI.WebControls.BaseValidator
    {
        private CardType _CardTypes;
        private System.Web.UI.WebControls.TextBox _CreditCardTextBox;
        private bool _ValidateCardType;

        /// <summary>
        /// Initializes a new instance of the ZNodeCreditCardValidator class
        /// </summary>
        public ZNodeCreditCardValidator()
        {
            this._ValidateCardType = true;

            // Accept everything
            this._CardTypes = CardType.All | CardType.Unknown;
        }

        #region Public Properties
        /// <summary>
        /// Gets or sets a value indicating whether the card type validated or not.
        /// </summary>
        public bool ValidateCardType
        {
            get
            {
                return this._ValidateCardType;
            }

            set
            {
                this._ValidateCardType = value;
            }
        }

        /// <summary>
        /// Gets or sets the card type
        /// </summary>
        public string AcceptedCardTypes
        {
            get
            {
                return this._CardTypes.ToString();
            }

            set
            {
                this._CardTypes = (CardType)Enum.Parse(typeof(CardType), value, false);
            }
        }

        #endregion     

        /// <summary>
        /// Will ensure that the card is a valid length for the card type. If the
        /// card type isn't recognised it will return true by default.        
        /// The CreditCardValidator control also includes a CardTypes property that determines
        /// what card types should be accepted. If the card isn't recognised, and the Unknown card
        /// type is in the AcceptedCardTypes property then the DefaultLengthTest value will be
        /// returned.
        /// </summary>
        /// <param name="cardNumber">Credit card number to validate.</param>
        /// <returns>Returns true if valid otherwise false.</returns>
        public bool IsValidCardType(string cardNumber)
        {
            // AMEX -- 34 or 37 -- 15 length
            if ((Regex.IsMatch(cardNumber, "^(34|37)")) && ((this._CardTypes & CardType.Amex) != 0))
            {
                return (15 == cardNumber.Length);
            }
            else if ((Regex.IsMatch(cardNumber, "^(51|52|53|54|55)")) && ((this._CardTypes & CardType.MasterCard) != 0))
            {
                // MasterCard -- 51 through 55 -- 16 length
                return (16 == cardNumber.Length);
            }
            else if ((Regex.IsMatch(cardNumber, "^(4)")) && ((this._CardTypes & CardType.VISA) != 0))
            {
                // VISA -- 4 -- 13 and 16 length
                return (13 == cardNumber.Length || 16 == cardNumber.Length);
            }
            else if ((Regex.IsMatch(cardNumber, "^(300|301|302|303|304|305|36|38)")) && ((this._CardTypes & CardType.DinersClub) != 0))
            {
                // Diners Club -- 300-305, 36 or 38 -- 14 length
                return (14 == cardNumber.Length);
            }
            else if ((Regex.IsMatch(cardNumber, "^(2014|2149)")) && ((this._CardTypes & CardType.DinersClub) != 0))
            {
                // enRoute -- 2014,2149 -- 15 length
                return (15 == cardNumber.Length);
            }
            else if ((Regex.IsMatch(cardNumber, "^(6011)")) && ((this._CardTypes & CardType.Discover) != 0))
            {
                // Discover -- 6011 -- 16 length
                return (16 == cardNumber.Length);
            }
            else if ((Regex.IsMatch(cardNumber, "^(3)")) && ((this._CardTypes & CardType.JCB) != 0))
            {
                // JCB -- 3 -- 16 length
                return (16 == cardNumber.Length);
            }
            else if ((Regex.IsMatch(cardNumber, "^(2131|1800)")) && ((this._CardTypes & CardType.JCB) != 0))
            {
                // JCB -- 2131, 1800 -- 15 length
                return (15 == cardNumber.Length);
            }
            else
            {
                // Card type wasn't recognised, provided Unknown is in the CardTypes property, then
                // return true, otherwise return false.
                if ((this._CardTypes & CardType.Unknown) != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Check whether the control properties are valid.
        /// </summary>
        /// <returns>Returns true if valid otherwise false.</returns>
        protected override bool ControlPropertiesValid()
        {
            // Should have a text box control to check
            Control control = FindControl(ControlToValidate);

            if (null != control)
            {
                // Ensure its a text box
                if (control is System.Web.UI.WebControls.TextBox)
                {
                    // Set the member variable
                    this._CreditCardTextBox = (System.Web.UI.WebControls.TextBox)control;

                    // Check that it's been set ok
                    return (null != this._CreditCardTextBox);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether the control value is valid.
        /// </summary>
        /// <returns>Returns true if valid otherwise false.</returns>
        protected override bool EvaluateIsValid()
        {
            // Should the length be validated also?
            if (this._ValidateCardType)
            {
                // Check the length, if the length is fine then validate the card number
                if (this.IsValidCardType(this._CreditCardTextBox.Text))
                {
                    return ValidateCardNumber(this._CreditCardTextBox.Text);
                }
                else
                {
                    // Invalid length
                    return false;
                }
            }
            else
            {
                // Check that the text box contains a valid number using the ValidateCardNumber method
                return ValidateCardNumber(this._CreditCardTextBox.Text);
            }
        }

        /// <summary>
        /// Performs a validation using Luhn's Formula.
        /// </summary>
        /// <param name="cardNumber">Credit card number to validate.</param>
        /// <returns>Returns true if valid otherwise false.</returns>
        private static bool ValidateCardNumber(string cardNumber)
        {
            try
            {
                // Array to contain individual numbers
                System.Collections.ArrayList checkNumbers = new ArrayList();

                // So, get length of card
                int cardLength = cardNumber.Length;

                // Double the value of alternate digits, starting with the second digit
                // from the right, i.e. back to front.

                // Loop through starting at the end
                for (int i = cardLength - 2; i >= 0; i = i - 2)
                {
                    // Now read the contents at each index, this
                    // can then be stored as an array of integers

                    // Double the number returned
                    checkNumbers.Add(Int32.Parse(cardNumber[i].ToString()) * 2);
                }

                // Will hold the total sum of all checksum digits
                int checkSum = 0;	

                // Second stage, add separate digits of all products
                for (int countIndex = 0; countIndex <= checkNumbers.Count - 1; countIndex++)
                {
                    // Will hold the sum of the digits
                    int count = 0;	

                    // Determine if current number has more than one digit
                    if ((int)checkNumbers[countIndex] > 9)
                    {
                        int numberLength = ((int)checkNumbers[countIndex]).ToString().Length;

                        // Add count to each digit
                        for (int x = 0; x < numberLength; x++)
                        {
                            count = count + Int32.Parse(((int)checkNumbers[countIndex]).ToString()[x].ToString());
                        }
                    }
                    else                        	
                    {
                        // Single digit, just add it by itself
                        count = (int)checkNumbers[countIndex];
                    }

                    // Add sum to the total sum
                    checkSum = checkSum + count;	
                }

                // Stage 3, add the unaffected digits
                // Add all the digits that we didn't double still starting from the right
                // but this time we'll start from the rightmost number with alternating digits
                int originalSum = 0;
                for (int y = cardLength - 1; y >= 0; y = y - 2)
                {
                    originalSum = originalSum + Int32.Parse(cardNumber[y].ToString());
                }

                // Perform the final calculation, if the sum Mod 10 results in 0 then
                // it's valid, otherwise its false.
                return (((originalSum + checkSum) % 10) == 0);
            }
            catch
            {
                return false;
            }
        }       
    }
}

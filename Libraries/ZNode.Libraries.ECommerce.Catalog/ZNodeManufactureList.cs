﻿using System;
using System.Text;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;



namespace ZNode.Libraries.ECommerce.Catalog
{
    [Serializable()]
    [XmlRoot("ZNodeManufactureList")]
    public class ZNodeManufactureList : ZNodeBusinessBase
    {
        [XmlElement("ZNodeMaufactureCollection")]
        public ZNodeGenericCollection<ZNodeMaufactureCollection> ZNodeMaufactureCollection { get; set; }


        /// <summary>
        /// This static method creates Manufacturer object using XMLSerializer
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <returns>List of Manufactures and Associated Product</returns>
        public static ZNodeManufactureList Create(int portalId, int localeId)
        {
            ZNode.Libraries.DataAccess.Custom.ManufacturerHelper manufacturerHelper = new ZNode.Libraries.DataAccess.Custom.ManufacturerHelper();

            StringBuilder xmlOut = new StringBuilder();
            xmlOut.Append("<ZNodeManufactureList>");
            xmlOut.Append(manufacturerHelper.GetBrandWiseProduct(portalId, localeId));
            xmlOut.Append("</ZNodeManufactureList>");


            // Serialize the object if XmlOut length is not equal to zero.
            if (xmlOut.ToString().Trim().Length > 0)
            {
                // Serialize the object
                ZNodeSerializer serializer = new ZNodeSerializer();
                return (ZNodeManufactureList)serializer.GetContentFromString(xmlOut.ToString(), typeof(ZNodeManufactureList));
            }

            return null;
        }
    }


    [Serializable()]
    public class ZNodeMaufactureCollection
    {
        [XmlElement()]
        public string ManufacturerID { get; set; }

        [XmlElement()]
        public string ManufacturerName { get; set; }

        [XmlElement("ZNodeMaufactureProductCollection")]
        public ZNodeGenericCollection<ZNodeMaufactureProductCollection> ZNodeMaufactureProductCollection { get; set; }

    }

    [Serializable()]
    public class ZNodeMaufactureProductCollection
    {
        [XmlElement()]
        public string ProductID { get; set; }
        [XmlElement()]
        public string ProductNum { get; set; }
        [XmlElement()]
        public string ProductName { get; set; }
        [XmlElement()]
        public string ProductSEOURL { get; set; }
    }
}

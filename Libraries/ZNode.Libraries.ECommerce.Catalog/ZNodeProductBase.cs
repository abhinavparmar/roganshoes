using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Taxes;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents a product in the catalog
    /// </summary>    
    [Serializable()]
    public partial class ZNodeProductBase : ZNodeProductBaseEntity
    {
        #region Private variables
        private int alternateProductImageCount = 0;
        private int? shoppingCartQuantity;
        private ZNodeGenericCollection<ZNodeDigitalAsset> digitalAssetCollection = new ZNodeGenericCollection<ZNodeDigitalAsset>();
        private ZNodeGenericCollection<ZNodeCrossSellItem> crossSellItemCollection = new ZNodeGenericCollection<ZNodeCrossSellItem>();
        private ZNodeAddOnList selectedAddOnItems = new ZNodeAddOnList();
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ZNodeProductBase class.
        /// </summary>
        public ZNodeProductBase()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeProductBase class. With ZNodeProduct parameter.
        /// </summary>
        /// <param name="product">ZNodeProduct object</param>
        public ZNodeProductBase(ZNodeProduct product)
        {
            // General Settings
            this._Name = product._Name;
            this._PortalID = product._PortalID;
            this._ProductID = product._ProductID;
            this._ProductNum = product._ProductNum;
            this._ProductTypeID = product._ProductTypeID;
            this._ExpirationPeriod = product.ExpirationPeriod;
            this._ExpirationFrequency = product.ExpirationFrequency;
            this._ImageFile = product._ImageFile;
            this._IsActive = product._IsActive;
            this._ManufacturerID = product._ManufacturerID;
            this._ManufacturerPartNum = product._ManufacturerPartNum;
            this._MaxQty = product._MaxQty;
            this._MinQty = product._MinQty;
            this._shortDescription = product._shortDescription;
            this._sku = product._sku;
            this._addOnDescription = product._addOnDescription;
            this._DisplayOrder = product._DisplayOrder;
            this._downloadLink = product._downloadLink;

            this._Guid = product._Guid;
            this._selectedSKU = product._selectedSKU;
            this._selectedAddOn = product._selectedAddOn;
            if (!string.IsNullOrEmpty(product._seoURL))
            {
                this._seoURL = product._seoURL.ToLower();
            }
            else
            {
                this._seoURL = product._seoURL;

            }
            // Collection Properties
            this.digitalAssetCollection = product.digitalAssetCollection;
            this._tieredPriceCollection = product._tieredPriceCollection;
            this._selectedAddOnCollection = product._selectedAddOnCollection;
            this._selectedAddOn = product._selectedAddOn;
            this.crossSellItemCollection = product.crossSellItemCollection;

            // Inventory Settings
            this._QuantityOnHand = product._QuantityOnHand;
            this._AllowBackOrder = product._AllowBackOrder;
            this._TrackInventoryInd = product._TrackInventoryInd;
            this._BackOrderMsg = product._BackOrderMsg;
            this._CallMessage = product._CallMessage;
            this._AffiliateUrl = product._AffiliateUrl;

            // Shipping & tax
            this._shippingCost = product._shippingCost;
            this._ShippingRuleTypeID = product._ShippingRuleTypeID;
            this._shipSeparately = product._shipSeparately;
            this.shoppingCartQuantity = product.shoppingCartQuantity;
            this._freeShippingInd = product._freeShippingInd;
            this._TaxClassID = product._TaxClassID;
            this._ShippingRate = product.ShippingRate;

            // Product pricing
            this._RetailPrice = product._RetailPrice;
            this._SalePrice = product._SalePrice;
            this._WholesalePrice = product._WholesalePrice;

            // Product Dimensions
            this._height = product._height;
            this._length = product._length;
            this._width = product._width;
            this._Weight = product._Weight;

            // Custom Properties
            this._Custom1 = product._Custom1;
            this._Custom2 = product._Custom2;
            this._Custom3 = product._Custom3;
            this._ImageAltTag = product._ImageAltTag;

            // Customer Review details
            this._reviewRating = product._reviewRating;
            this._totalReviews = product._totalReviews;

            // Recurring Billing
            this._RecurringBillingInd = product._RecurringBillingInd;
            this._RecurringBillingTotalCycles = product._RecurringBillingTotalCycles;
            this._RecurringBillingPeriod = product._RecurringBillingPeriod;
            this._RecurringBillingFrequency = product._RecurringBillingFrequency;
            this._RecurringBillingInstallmentInd = product._RecurringBillingInstallmentInd;
            this._RecurringBillingInitialAmount = product._RecurringBillingInitialAmount;

            // Supplier Detail
            this._SupplierID = product._SupplierID;

            // Bundle Product Collection
            this.ZNodeBundleProductCollection = product.ZNodeBundleProductCollection;

            this.isPromotionApplied = product.IsPromotionApplied;

            this._skuProfile = product._skuProfile;


            //Zeon Customization : Set Product Extn Property : Start
            this.PreOrderFlag = product.PreOrderFlag;
            this.IsMapPrice = product.IsMapPrice;
            this.ProductCategoryHierarchy = product.ProductCategoryHierarchy;
            this.ZNodeProductCategoryList = product.ZNodeProductCategoryList;
            this.IsIndexFollow = product.IsIndexFollow;
            //Zeon Customization : Set Product Extn Property : End
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the product Add-on selected by the user for this product
        /// </summary>
        [XmlIgnore()]
        public ZNodeAddOnList SelectedAddOnItems
        {
            get
            {
                return this.selectedAddOnItems;
            }

            set
            {
                this.selectedAddOnItems = value;

                this._selectedAddOnCollection.SelectedAddOnValues = value.SelectedAddOnValues;
                this._selectedAddOnCollection.AddOnCollection = new ZNodeGenericCollection<ZNodeAddOnEntity>();

                foreach (ZNodeAddOn entity in value.ZNodeAddOnCollection)
                {
                    this._selectedAddOnCollection.AddOnCollection.Add((ZNodeAddOnEntity)entity);
                }
            }
        }

        /// <summary>
        /// Gets or sets the product SKU selected by the user
        /// </summary>
        [XmlIgnore()]
        public ZNodeSKU SelectedSKU
        {
            get
            {
                return new ZNodeSKU(this._selectedSKU);
            }

            set
            {
                this._selectedSKU = value;
            }
        }

        /// <summary>
        /// Gets or sets the product Addon selected by the user
        /// </summary>
        [XmlIgnore()]
        public ZNodeAddOnValue SelectedAddon
        {
            get
            {
                return new ZNodeAddOnValue(this._selectedAddOn);
            }

            set
            {
                this._selectedAddOn = value;
            }
        }

        /// <summary>
        /// Gets or sets the retail price. Will return the SKU override retail price if one exists.        
        /// </summary>
        [XmlIgnore()]
        public new decimal RetailPrice
        {
            get
            {
                decimal retailPrice = this._RetailPrice;
                decimal? skuOverrideRetailPrice = this._selectedSKU.RetailPriceOverride;

                if (skuOverrideRetailPrice.HasValue)
                {
                    // if a sku retail price override exists then use that price
                    if (skuOverrideRetailPrice.Value >= 0)
                    {
                        retailPrice = skuOverrideRetailPrice.Value;
                    }
                }

                ZNodeInculsiveTax salesTax = new ZNodeInculsiveTax();
                retailPrice = salesTax.GetInclusivePrice(this._TaxClassID, retailPrice);

                return retailPrice;
            }

            set
            {
                this._RetailPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the wholesale price. Will return the SKU override wholesale price if one exists.
        /// </summary>
        [XmlIgnore()]
        public new decimal? WholesalePrice
        {
            get
            {
                decimal? wholesalePrice = this._WholesalePrice;
                decimal? skuWholeSalePrice = this._selectedSKU.WholesalePriceOverride;

                if (skuWholeSalePrice.HasValue)
                {
                    // if sku wholesale override exists then use that price                
                    if (skuWholeSalePrice.Value >= 0)
                    {
                        wholesalePrice = skuWholeSalePrice.Value;
                    }
                }

                if (wholesalePrice.HasValue)
                {
                    ZNodeInculsiveTax salesTax = new ZNodeInculsiveTax();
                    wholesalePrice = salesTax.GetInclusivePrice(this._TaxClassID, wholesalePrice.Value);

                    return wholesalePrice;
                }

                return wholesalePrice;
            }

            set
            {
                this._WholesalePrice = value;
            }
        }

        /// <summary>
        /// Gets or sets Collection of digital assets elated with this product
        /// </summary>
        [XmlIgnore()]
        public ZNodeGenericCollection<ZNodeDigitalAsset> ZNodeDigitalAssetCollection
        {
            get
            {
                return this.digitalAssetCollection;
            }

            set
            {
                this.digitalAssetCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets Collection of related products contained for this product
        /// </summary>
        [XmlElement("ZNodeCrossSellItem")]
        public ZNodeGenericCollection<ZNodeCrossSellItem> ZNodeCrossSellItemCollection
        {
            get
            {
                return this.crossSellItemCollection;
            }

            set
            {
                this.crossSellItemCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the product sale price as a decimal value
        /// </summary>
        [XmlIgnore()]
        public new decimal? SalePrice
        {
            get
            {
                decimal? salePrice = this._SalePrice;

                // Get Selected SKU override sale price
                decimal? skuSalePrice = this._selectedSKU.SalePriceOverride;

                if (skuSalePrice.HasValue)
                {
                    // if sku override exists then use that price with Sale price
                    salePrice = skuSalePrice.Value;
                }

                if (salePrice.HasValue)
                {
                    ZNodeInculsiveTax salesTax = new ZNodeInculsiveTax();
                    salePrice = salesTax.GetInclusivePrice(this._TaxClassID, salePrice.Value);

                    return salePrice;
                }

                return this._SalePrice;
            }

            set
            {
                this._SalePrice = value;
            }
        }

        /// <summary>
        /// Gets the product base price and returns it as string
        /// Check the user's Login status and display the price based on Showpricing Status.
        /// </summary>
        [XmlIgnore()]
        public string FormattedPrice
        {
            get
            {
                decimal actualPrice = base.ProductPrice;
                decimal basePrice = this.FinalPrice;
                string returnPrice = string.Empty;

                //if (this._tieredPriceCollection != null && this._tieredPriceCollection.Count == 0)
                //{
                //    actualPrice = this.RetailPrice;
                //}
                if (actualPrice == basePrice)
                {
                    if (this.IsMapPrice)
                    {
                        returnPrice = ZNodePricingFormatter.ConfigureProductPricing(actualPrice);
                    }
                    else
                    {
                        returnPrice = ZNodePricingFormatter.ConfigureProductPricingRed(actualPrice);
                    }
                }
                else
                {
                    returnPrice = ZNodePricingFormatter.ConfigureProductPricing(actualPrice, basePrice);
                    //PRFT:Custom Code Starts PI Task 166
                    //returnPrice = returnPrice + FormatPriceDiscount(actualPrice, basePrice);
                    //PRFT:Custom Code Ends PI Task 166
                }

                return returnPrice.TrimEnd();
            }
        }

        /// <summary>
        /// Gets the final calculated price for a product which includes inclusive tax
        /// </summary>
        [XmlIgnore()]
        public new decimal FinalPrice
        {
            get
            {
                decimal finalPrice = this.ProductPrice;

                ZNodeInculsiveTax inclusiveTax = new ZNodeInculsiveTax();
                finalPrice = inclusiveTax.GetInclusivePrice(this._TaxClassID, finalPrice);

                return finalPrice;
            }
        }

        /// <summary>
        /// Gets the final calculated price for a product without inclusive tax
        /// </summary>
        [XmlIgnore()]
        public new decimal ProductPrice
        {
            get
            {
                ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption pricing = new ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption();
                return pricing.PromotionalPrice(this);
            }
        }

        /// <summary>
        /// Gets or sets the alternate product image count
        /// </summary>        
        public int AlternateProductImageCount
        {
            get
            {
                return this.alternateProductImageCount;
            }

            set
            {
                this.alternateProductImageCount = value;
            }
        }

        /// <summary>
        /// Gets fully qualified link to product details page
        /// </summary>
        [XmlIgnore()]
        public string ViewProductLink
        {
            get
            {
                return ZNodeSEOUrl.MakeURL(ProductID.ToString(), SEOUrlType.Product, this._seoURL);
            }
        }
        #endregion

        #region Static Create

        /// <summary>
        /// This static method creates product object using XMLSerializer
        /// </summary>
        /// <param name="productId">Product Id of requested ZNodeProduct</param>
        /// <param name="portalId">Portal Id of requested ZNodeProduct</param>
        /// <param name="localeId">Locale Id of requested ZNodeProduct</param>
        /// <returns>ZNodeProduct object</returns>
        public static ZNodeProduct Create(int productId, int portalId, int localeId)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            string xmlOut = productHelper.GetProductXML(productId, portalId, localeId, ZNodeProfile.CurrentUserProfileId);

            // serialize the object if XmlOut length is not equal to zero.
            if (xmlOut.Trim().Length > 0)
            {
                ZNodeSerializer ser = new ZNodeSerializer();
                ZNodeProduct product = (ZNodeProduct)ser.GetContentFromString(xmlOut, typeof(ZNodeProduct));
                //Zeon Customization :: product.ApplyPromotion() as ApplyPromotion() promotion is called when we are retriving product.price:: Start
                //product.ApplyPromotion();
                //Zeon Customization :: product.ApplyPromotion() as ApplyPromotion() promotion is called when we are retriving product.price:: End
                return product;
            }

            return null;
        }

        /// <summary>
        /// This static method creates product object using XMLSerializer
        /// </summary>
        /// <param name="productId">Product Id of requested ZNodeProduct</param>
        /// <returns>ZNodeProduct object</returns>
        public static ZNodeProduct Create(int productId)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            string xmlOut = productHelper.GetProductByProductId(productId);

            // serialize the object if XmlOut length is not equal to zero.
            if (xmlOut.Trim().Length > 0)
            {
                ZNodeSerializer ser = new ZNodeSerializer();
                ZNodeProduct product = (ZNodeProduct)ser.GetContentFromString(xmlOut, typeof(ZNodeProduct));
                product.ApplyPromotion();
                return product;
            }

            return null;
        }

        /// <summary>
        /// This static method creates product object using XMLSerializer
        /// </summary>
        /// <param name="productId">Product Id of requested ZNodeProduct</param>
        /// <param name="isPromotion">If Promotion needs to be applied or not</param>
        /// <returns>ZNodeProduct object</returns>
        public static ZNodeProduct Create(int productId, bool isPromotion)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            string xmlOut = productHelper.GetProductByProductId(productId);

            // serialize the object if XmlOut length is not equal to zero.
            if (xmlOut.Trim().Length > 0)
            {
                ZNodeSerializer ser = new ZNodeSerializer();
                ZNodeProduct product = (ZNodeProduct)ser.GetContentFromString(xmlOut, typeof(ZNodeProduct));
                if (isPromotion)
                    product.ApplyPromotion();
                return product;
            }

            return null;
        }



        /// <summary>
        /// This static method gets the attributeId for the productId and it also gets the attributeId based on the selectorName passed
        /// </summary>
        /// <param name="productId">Product id of Requested Product</param>
        /// <param name="attributeId">Attribute id of Requested Product</param>
        /// <param name="productTypeId">Product type id of Requested Product</param>
        /// <param name="selectorName">Selector Name of Requested Product</param>
        /// <param name="selectorXaxis">SelectorXasis of Requested Product</param>
        /// <param name="selectorYaxis">SelectorYaxis of Requested Product</param>
        /// <returns>DataTable object</returns>
        public static DataTable GetByAttributeID(int productId, int attributeId, int productTypeId, string selectorName, string selectorXaxis, string selectorYaxis)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();

            return productHelper.GetByAttributeID(productId, attributeId, productTypeId, selectorName, selectorXaxis, selectorYaxis);
        }

        /// <summary>
        /// Apply Promotion Product if any.
        /// </summary>
        public void ApplyPromotion()
        {
            // Product Promotion
            ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption pricing = new ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption();
            this.CheckSKUProfile();
            pricing.PromotionalPrice(this);
        }

        /// <summary>
        /// Gets or sets the MaxQuantity       
        /// </summary>
        public ZNodeSKUProfile CheckSKUProfile()
        {
            foreach (ZNodeSKUProfile skuProfile in SelectedSKU.SkuProfileCollection)
            {
                if (skuProfile.ProfileID == ZNodeProfile.CurrentUserProfileId)
                {
                    this._skuProfile = skuProfile;
                }
            }

            return this._skuProfile;
        }

        /// <summary>
        /// This static method get product object using XMLSerializer
        /// </summary>
        /// <param name="productId">Product Id of requested ZNodeProduct</param>
        /// <param name="portalId">Portal Id of requested ZNodeProduct</param>
        /// <param name="localeId">Locale Id of requested ZNodeProduct</param>
        /// <returns>ZNodeProduct object</returns>
        public static ZNodeProduct GetProductDetails(int productId, int portalId, int localeId)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            string xmlOut = productHelper.GetProductDetailsXML(productId, portalId, localeId, ZNodeProfile.CurrentUserProfileId);

            // serialize the object if XmlOut length is not equal to zero.
            if (xmlOut.Trim().Length > 0)
            {
                ZNodeSerializer ser = new ZNodeSerializer();
                ZNodeProduct product = (ZNodeProduct)ser.GetContentFromString(xmlOut, typeof(ZNodeProduct));
                return product;
            }

            return null;
        }

        #endregion
    }
}
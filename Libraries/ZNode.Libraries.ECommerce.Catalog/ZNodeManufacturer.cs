using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents a product manufacturer in the catalog
    /// </summary>    
    [Serializable()]
    [XmlRoot("ZNodeManufacturer")]
    public class ZNodeManufacturer : ZNodeBusinessBase
    {
        #region Private Variables
        private int _ManufacturerID = 0;
        private int _PortalID = 0;
        private string _Name = string.Empty;
        private string _Description = string.Empty;
        private bool _ActiveInd;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeManufacturer class
        /// </summary>
        public ZNodeManufacturer()
        {
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the manufacturer Id
        /// </summary>
        [XmlElement()]
        public int ManufacturerID
        {
            get
            {
                return this._ManufacturerID;
            }

            set
            {
                this._ManufacturerID = value;
            }
        }

        /// <summary>
        /// Gets or sets the site portal id
        /// </summary>
        [XmlElement()]
        public int PortalID
        {
            get
            {
                return this._PortalID;
            }
            
            set
            {
                this._PortalID = value;
            }
        }

        /// <summary>
        /// Gets or sets the name for this Manufacturer
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get
            {
                return this._Name;
            }
            
            set
            {
                this._Name = value;
            }
        }

        /// <summary>
        /// Gets or sets the description for this Manufacturer
        /// </summary>
        [XmlElement()]
        public string Description
        {
            get
            {
                return this._Description;
            }
            
            set
            {
                this._Description = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether manufacturer is active or not.
        /// </summary>
        [XmlElement()]
        public bool ActiveInd
        {
            get
            {
                return this._ActiveInd;
            }

            set
            {
                this._ActiveInd = value;
            }
        }
        #endregion       

        #region Public Methods

        /// <summary>
        /// This static method creates Manufacturer object using XMLSerializer
        /// </summary>
        /// <param name="manufacturerId">Manufacturer Id to get the manufacturer details.</param>
        /// <param name="portalId">Portal Id to get the manufacturer details.</param>
        /// <returns>Returns the ZNodeManufacturer object.</returns>
        public static ZNodeManufacturer Create(int manufacturerId, int portalId)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            string xmlOut = productHelper.GetManufacturerXML(manufacturerId, portalId);

            // Serialize the object if XmlOut length is not equal to zero.
            if (xmlOut.Trim().Length > 0)
            {
                // Serialize the object
                ZNodeSerializer serializer = new ZNodeSerializer();
                return (ZNodeManufacturer)serializer.GetContentFromString(xmlOut, typeof(ZNodeManufacturer));
            }

            return null;
        }
        #endregion
    }
}

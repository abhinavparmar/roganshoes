﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.Search;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;
using ZNode.Libraries.Search.Interfaces;
using System.Data;
using ZNode.Libraries.Ecommerce.Entities;

namespace ZNode.Libraries.ECommerce.Catalog
{
    public class ZNodeSearchEngine
    {

        #region Public Methods

        public ZNodeSearchEngineResult Search(string category, string searchText, List<KeyValuePair<string, IEnumerable<string>>> refineBy, int sort)
        {
            IZNodeSearchRequest searchRequest = new LuceneSearchRequest();
            ZNodeSearchEngineResult result = new ZNodeSearchEngineResult();

            searchRequest.searchText = searchText;
            searchRequest.category = category;
            searchRequest.PortalCatalogID = ZNodeCatalogManager.CatalogConfig.PortalCatalogID;
            searchRequest.sortOrder = sort;
            searchRequest.Facets = refineBy;
            IZnodeSearchProvider searchProvider = new LuceneSearchProvider();

            IZNodeSearchResponse response = searchProvider.Search(searchRequest);

            if (response != null)
            {
                if (response.CategoryItems != null)
                {
                    Func<IEnumerable<IZNodeSearchCategoryItem>, IEnumerable<ZNodeCategoryNavigation>> SelectNavigation = null;
                    SelectNavigation = x => x.Select(y => new ZNodeCategoryNavigation() { Name = y.Name, Title = y.Title, NumberOfProducts = y.Count, CategoryHierarchy = SelectNavigation(y.Hierarchy) });

                    result.CategoryNavigation = SelectNavigation(response.CategoryItems);
                }
                if (response.Products != null)
                {
                    result.ProductIDs = response.Products.Select(x => x.ProductID).ToList();
                }
                if (response.Facets != null)
                {
                    //result.Facets = response.Facets.Select(x => new ZNodeFacet() { AttributeName = x.AttributeName, ControlTypeID = x.ControlTypeID, AttributeValues = x.AttributeValues.Select(y => new ZNodeFacetValue() { AttributeValue = y.AttributeValue, FacetCount = y.FacetCount }).ToList() }).ToList();//Old Code
                    result.Facets = response.Facets.Select(x => new ZNodeFacet() { AttributeName = x.AttributeName, ControlTypeID = x.ControlTypeID, FacetDisplayOrder = x.FacetDisplayOrder, AttributeValues = x.AttributeValues.Select(y => new ZNodeFacetValue() { AttributeValue = y.AttributeValue, FacetCount = y.FacetCount, ProductIDList = y.ProductIDList, FacetValueDisplayOrder = y.FacetValueDisplayOrder }).OrderBy(t =>t.AttributeValue).OrderBy(t =>t.FacetValueDisplayOrder).ToList() }).OrderBy(p => p.FacetDisplayOrder).ToList();//Zeon Custom Code
                }
                result.ProductIDs = result.ProductIDs.Distinct().ToList();
                return result;
            }
            return null;
        }


        public List<ZnodeTypeAheadAutoComplete> SuggestTermsFor(string term, string category)
        {
            IZnodeSearchProvider searchProvider = new LuceneSearchProvider();
            var response = searchProvider.SuggestTermsFor(term, category, ZNodeCatalogManager.CatalogConfig.PortalCatalogID);

            if (response != null && response.Count > 0)
            {
                return response.Select(x => new ZnodeTypeAheadAutoComplete()
                {
                    label = string.IsNullOrEmpty(x.Category) ? x.Name : string.Format("{0} in {1}", x.Name, x.Category),
                    name = x.Name,
                    value = x.Category
                }).ToList();

            }

            return new List<ZnodeTypeAheadAutoComplete>();
        }


        public void UpdateReader()
        {
            LuceneSearchProvider searchProvider = new LuceneSearchProvider();
            searchProvider.ReloadIndex();
        }


        #endregion
    }
}


using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the properties and methods of a product Highlights.
    /// </summary>
    [Serializable()]
    [XmlRoot("ZNodeHighlight")]
    public class ZNodeHighlight : ZNodeBusinessBase
    {
        #region Private Member Variables
        private int _HighlightId;
        private int _ProductId;
        private int _PortalId;
        private string _ImageFile = string.Empty;
        private string _Name = String.Empty;
        private string _Description = String.Empty;
        private bool _DisplayPopup;
        private string _Hyperlink = string.Empty;
        private bool _IsActive;
        private bool _HyperlinkNewWinInd;
        private int _DisplayOrder;
        private int _HighlightTypeId;
        private string _ImageAltTag = string.Empty;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the ZNodeHighlight class
        /// </summary>
        public ZNodeHighlight()
        {
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the product highlight id
        /// </summary>
        [XmlElement()]
        public int HighlightID
        {
            get { return this._HighlightId; }
            set { this._HighlightId = value; }
        }

        /// <summary>
        /// Gets or sets the product id
        /// </summary>
        [XmlElement()]
        public int ProductId
        {
            get { return this._ProductId; }
            set { this._ProductId = value; }
        }

        /// <summary>
        /// Gets or sets the site portal id
        /// </summary>
        [XmlElement()]
        public int PortalID
        {
            get { return this._PortalId; }
            set { this._PortalId = value; }
        }

        /// <summary>
        /// Gets or sets the image file name for this product highlight.
        /// </summary>
        [XmlElement()]
        public string ImageFile
        {
            get { return this._ImageFile; }
            set { this._ImageFile = value; }
        }
       
        /// <summary>
        /// Gets or sets the name for this product highlight
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        /// <summary>
        /// Gets or sets the description for this product highlight
        /// </summary>
        [XmlElement()]
        public string Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display popup.
        /// </summary>
        [XmlElement()]
        public bool DisplayPopup
        {
            get { return this._DisplayPopup; }
            set { this._DisplayPopup = value; }
        }

        /// <summary>
        /// Gets or sets the hyperlink value
        /// </summary>
        [XmlElement()]
        public string Hyperlink
        {
            get { return this._Hyperlink; }
            set { this._Hyperlink = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the HyperlinkNewWinInd is enabled or not.
        /// </summary>
        [XmlElement()]
        public bool HyperlinkNewWinInd
        {
            get { return this._HyperlinkNewWinInd; }
            set { this._HyperlinkNewWinInd = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the highlight is active or not.
        /// </summary>
        [XmlElement(ElementName = "ActiveInd")]
        public bool IsActive
        {
            get { return this._IsActive; }
            set { this._IsActive = value; }
        }

        /// <summary>
        /// Gets or sets the display order for this highlight
        /// </summary>
        [XmlElement()]
        public int DisplayOrder
        {
            get { return this._DisplayOrder; }
            set { this._DisplayOrder = value; }
        }

        /// <summary>
        /// Gets or sets the HighlightTypeId for this highlight
        /// </summary>
        [XmlElement()]
        public int HighlightTypeID
        {
            get { return this._HighlightTypeId; }
            set { this._HighlightTypeId = value; }
        }

        /// <summary>
        /// Gets or sets the image alternative tag text
        /// </summary>
        [XmlElement()]
        public string ImageAltTag
        {
            get
            {
                // If image Alt tag value not exists then use highlight name
                if (string.IsNullOrEmpty(this._ImageAltTag))
                {
                    return this._Name;
                }

                // Otherwise, return Alternative text
                return this._ImageAltTag;
            }
            
            set
            {
                this._ImageAltTag = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get a highlight object by Id.
        /// </summary>
        /// <param name="highlightId">Highlight Id to get the highlight object.</param>
        /// <returns>Returns the ZNodeHighlight object.</returns>
        public static ZNodeHighlight Create(int highlightId)
        {
            ZNode.Libraries.DataAccess.Service.HighlightService highlightservice = new ZNode.Libraries.DataAccess.Service.HighlightService();
            ZNode.Libraries.DataAccess.Entities.Highlight entity = highlightservice.GetByHighlightID(highlightId);

            ZNodeHighlight highlight = null;

            if (entity != null)
            {
                highlight = new ZNodeHighlight();

                highlight.Name = entity.Name;
                highlight.Description = entity.Description;
                highlight.DisplayOrder = entity.DisplayOrder.GetValueOrDefault(0);
                highlight.ImageFile = entity.ImageFile;
            }

            return highlight;
        }
        #endregion
    }
}

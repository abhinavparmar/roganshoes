﻿using System.Data;

namespace ZNode.Libraries.ECommerce.Catalog
{
    public partial class ZNodeStoreList
    {
        public static DataTable GetListOfStorewiseAvalabilityOfSku(int storeID,int skuID, int portalID)
        {
            ZNode.Libraries.DataAccess.Custom.StoreLocatorHelper storeLocatorHelper = new ZNode.Libraries.DataAccess.Custom.StoreLocatorHelper();
            return storeLocatorHelper.GetSKUAvailabilityPerZipCode(storeID, skuID, portalID);
        }
    }
}

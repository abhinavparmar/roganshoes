﻿using System.Web;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the ZNodeResourceManager class
    /// </summary>
    public class ZNodeResourceManager : ZNodeBusinessBase
    {
        /// <summary>
        /// Gets a page-level resource object based on the specified System.Web.Compilation.ExpressionBuilderContext.VirtualPath
        ///     and System.Web.Compilation.ResourceExpressionFields.ResourceKey properties.
        /// </summary>       
        /// <param name="controlName">Name of the control.</param>
        /// <param name="virtualPath">
        ///     The System.Web.Compilation.ExpressionBuilderContext.VirtualPath property
        ///     for the local resource object.</param>
        /// <param name="resourceKey">
        ///     A string that represents a System.Web.Compilation.ResourceExpressionFields.ResourceKey
        ///     property of the requested resource object
        /// </param>
        /// <returns>
        ///     An System.Object that represents the requested page-level resource object;
        ///     otherwise, null if a matching resource object is found but not a resourceKey
        ///     parameter.
        /// </returns>
        public string GetLocalResourceObject(string controlName, string virtualPath, string resourceKey)
        {
            return this.GetLocalResourceObject(string.Empty, controlName, virtualPath, resourceKey);
        }

        /// <summary>
        ///     Gets a page-level resource object based on the specified System.Web.Compilation.ExpressionBuilderContext.VirtualPath
        ///     and System.Web.Compilation.ResourceExpressionFields.ResourceKey properties.
        /// </summary>        
        /// <param name="virtualPath">
        ///     The System.Web.Compilation.ExpressionBuilderContext.VirtualPath property
        ///     for the local resource object.</param>
        /// <param name="resourceKey">
        ///     A string that represents a System.Web.Compilation.ResourceExpressionFields.ResourceKey
        ///     property of the requested resource object
        /// </param>
        /// <returns>
        ///     An System.Object that represents the requested page-level resource object;
        ///     otherwise, null if a matching resource object is found but not a resourceKey
        ///     parameter.
        /// </returns>
        public string GetLocalResourceObject(string virtualPath, string resourceKey)
        {
            return this.GetLocalResourceObject(string.Empty, string.Empty, virtualPath, resourceKey);
        }

        /// <summary>
        ///     Gets a page-level resource object based on the specified System.Web.Compilation.ExpressionBuilderContext.VirtualPath
        ///     and System.Web.Compilation.ResourceExpressionFields.ResourceKey properties.
        /// </summary>
        /// <param name="defaultValue">DefaultValue for the item.</param>
        /// <param name="controlName">Name of the control</param>
        /// <param name="virtualPath">
        ///     The System.Web.Compilation.ExpressionBuilderContext.VirtualPath property
        ///     for the local resource object.</param>
        /// <param name="resourceKey">
        ///     A string that represents a System.Web.Compilation.ResourceExpressionFields.ResourceKey
        ///     property of the requested resource object
        /// </param>
        /// <returns>
        ///     An System.Object that represents the requested page-level resource object;
        ///     otherwise, null if a matching resource object is found but not a resourceKey
        ///     parameter.
        /// </returns>
        public string GetLocalResourceObject(string defaultValue, string controlName, string virtualPath, string resourceKey)
        {
            try
            {
                object resourceObject = HttpContext.GetLocalResourceObject(virtualPath, resourceKey);

                if (resourceObject != null)
                {
                    defaultValue = resourceObject.ToString();
                }
            }
            catch (System.ArgumentException argumentException)
            {
                // The resource class for the page was not found.
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(controlName + "- Virtual path Exception : " + argumentException.Message);
            }
            catch (System.InvalidOperationException invalidOperationException)
            {
                // The specified virtualPath parameter is not in the current application's root directory
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(controlName + "- Invalid Operation Exception : " + invalidOperationException.Message);
            }
            catch (System.Resources.MissingManifestResourceException missingManifestResourceException)
            {
                // A resource object was not found for the specified virtualPath parameter.
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(controlName + "- Missing Manifest Resource Exception : " + missingManifestResourceException.Message);
            }
            catch (System.Resources.MissingSatelliteAssemblyException missingSatelliteAssemblyException)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(controlName + "- Missing Satellite Assembly Exception : " + missingSatelliteAssemblyException.Message);
            }

            // Get the local resource string.
            return defaultValue;
        }
       
        /// <summary>
        ///     Gets an application-level resource object based on the specified System.Web.Compilation.ResourceExpressionFields.ClassKey
        ///     and System.Web.Compilation.ResourceExpressionFields.ResourceKey properties.
        /// </summary>
        /// <param name="classKey">
        ///     A string that represents the System.Web.Compilation.ResourceExpressionFields.ClassKey
        ///     property of the requested resource object.
        /// </param>
        /// <param name="resourceKey">
        ///     A string that represents the System.Web.Compilation.ResourceExpressionFields.ResourceKey
        ///     property of the requested resource object.
        /// </param>
        /// <returns>
        ///     An System.Object that represents the requested application-level resource
        ///     object; otherwise, null if a matching resource object is found but not a
        ///     resourceKey parameter.
        /// </returns>
        /// <exception>
        ///     System.Resources.MissingManifestResourceException:
        ///     A resource object with the specified classKey parameter was not found.  -
        ///     or - The main assembly does not contain the resources for the neutral culture,
        ///     and these resources are required because the appropriate satellite assembly
        ///     is missing.
        /// </exception>
        public string GetGlobalResourceObject(string classKey, string resourceKey)
        {
            string globalresourcestring = string.Empty;
           
            try
            {
                object resourceObject = HttpContext.GetGlobalResourceObject(classKey, resourceKey);

                if (resourceObject != null)
                {
                    globalresourcestring = resourceObject.ToString();
                }
            }
            catch (System.Resources.MissingManifestResourceException missingManifestResourceException)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Missing Manifest Resource Exception : " + missingManifestResourceException.Message);
            }
            catch (System.Resources.MissingSatelliteAssemblyException missingSatelliteAssemblyException)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Missing Satellite Assembly Exception : " + missingSatelliteAssemblyException.Message);
            }

            // Get the global resource string.
            return globalresourcestring;
        }
    }
}

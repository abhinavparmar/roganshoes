using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents a digital asset for a product
    /// </summary>    
    [Serializable()]
    [XmlRoot("ZNodeDigitalAsset")]
    public class ZNodeDigitalAsset : ZNodeBusinessBase
    {
        #region Protected Member Variables
        private int _DigitalAssetId = 0;
        private string _DigitalAsset = string.Empty;
        private int? _OrderLineItemId = null;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets Digital Asset id
        /// </summary>
        [XmlElement()]
        public int DigitalAssetID
        {
            get 
            {
                return this._DigitalAssetId;
            }
            
            set 
            {
                this._DigitalAssetId = value;
            }
        }

        /// <summary>
        /// Gets or sets Digital Asset(It can include serial numbers or other digital information)
        /// </summary>
        [XmlElement()]
        public string DigitalAsset
        {
            get 
            {
                return this._DigitalAsset;
            }
            
            set 
            {
                this._DigitalAsset = value;
            }
        }

        /// <summary>
        /// Gets or sets order line item referred to this digital Asset
        /// </summary>
        [XmlElement()]
        public int? OrderLineItemID
        {
            get 
            {
                return this._OrderLineItemId;
            }
           
            set 
            {
                this._OrderLineItemId = value;            
            }
        }
        #endregion
    }
}

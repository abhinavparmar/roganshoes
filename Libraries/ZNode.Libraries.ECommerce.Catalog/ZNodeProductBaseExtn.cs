﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    public partial class ZNodeProductBase : ZNodeProductBaseEntity
    {
        #region Private variables
        private int productSKUCount = 0;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the  product sku  image count
        /// </summary>        
        public int ProductSKUCount
        {
            get
            {
                return this.productSKUCount;
            }

            set
            {
                this.productSKUCount = value;
            }
        }

        /// <summary>
        /// Gets or sets the list Distinct Color SkU Images
        /// </summary>
        [XmlElement("ZNodeSKUImage")]
        public ZNodeGenericCollection<ZNodeSKUImage> ZNodeSKUImageCollection
        {
            get;
            set;
        }

        [XmlElement("BrandTopReview")]
        public ZNodeReview ZNodeBrandTopReview { get; set; }

        #endregion

        #region Private Methods
        /// <summary>
        /// Calculate discount from origional price to discount Price
        /// </summary>
        /// <param name="descPrice"></param>
        /// <param name="oriPrice"></param>
        /// <returns></returns>
        private decimal GetFormattedDiscountValue(decimal oriPrice, decimal descPrice)
        {
            decimal discountVal = 0;
            if (oriPrice > 0 && descPrice > 0)
            {
                discountVal = (oriPrice - descPrice) * 100 / oriPrice;
                discountVal = decimal.Round(discountVal, 0);
            }
            return discountVal;
        }

        /// <summary>
        /// Format Discount Text
        /// </summary>
        /// <param name="retailprice"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        private string FormatPriceDiscount(decimal retailprice, decimal price)
        {
            string discPriceText = string.Empty;
            string discText = string.Empty;
            decimal discount = GetFormattedDiscountValue(retailprice, price);
            if (discount > 0)
            {
                if (ConfigurationManager.AppSettings["CheerAndPomPortalID"] != null && !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString()))
                {
                    if (ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString().Equals(ZNodeConfigManager.SiteConfig.PortalID.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        discText = HttpContext.GetGlobalResourceObject("CommonCaption", "CheerListDiscountText") == null ? "<span class='discount'>Save {0}%</span>" : HttpContext.GetGlobalResourceObject("CommonCaption", "CheerListDiscountText").ToString();
                    }
                    else
                    {
                        discText = HttpContext.GetGlobalResourceObject("CommonCaption", "RogansListDiscountText") == null ? "<span class='discount'>Save {0}%</span>" : HttpContext.GetGlobalResourceObject("CommonCaption", "RogansListDiscountText").ToString();

                    }

                    discPriceText = string.Format(discText, discount);
                }
            }
            return discPriceText;
        }
        #endregion
    }

    /// <summary>
    ///  Holds the Color Sku Picture Path
    /// </summary>
    [Serializable()]
    [XmlRoot("ZNodeSKUImage")]
    public class ZNodeSKUImage
    {
        [XmlElement()]
        public string SKUPicturePath { get; set; }

        [XmlElement()]
        public string ImageAltTag { get; set; }

        [XmlElement()]
        public bool ActiveInd { get; set; }
    }


}

using System;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents a pricing object and provides properties to return price of a product
    /// </summary>
    [Serializable()]
    public class ZNodePrice : ZNodeBusinessBase
    {
        #region Member Variables
        private decimal _ActualPrice = 0;
        private decimal _DiscountedPrice = 0;
        private decimal _DiscountAmount = 0;
        private string _DiscountName = string.Empty;
        private string _DiscountText = string.Empty;
        private bool _IsDiscountApplied = false;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the actual price of a product or service (without discounts and promotions)
        /// </summary>
        public decimal ActualPrice
        {
            get
            {
                return this._ActualPrice;
            }

            set
            {
                this._ActualPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the Price with discounts or promotions applied. If no discounts applied, it will return the actual price
        /// </summary>
        public decimal DiscountedPrice
        {
            get
            {
                if (this._DiscountedPrice < 0)
                {
                    return 0;
                }
                else
                {
                    return this._DiscountedPrice;
                }
            }

            set
            {
                this._DiscountedPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the amount of the discount for this item.
        /// </summary>
        public decimal DiscountAmount
        {
            get
            {
                return this._DiscountAmount;
            }

            set
            {
                this._DiscountAmount = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the discount is applied or not.
        /// </summary>
        public bool IsDiscountApplied
        {
            get
            {
                return this._IsDiscountApplied;
            }

            set
            {
                this._IsDiscountApplied = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the discount or promotion applied (ex: Frequent Buyer Discount)
        /// </summary>
        public string DiscountName
        {
            get
            {
                return this._DiscountName;
            }

            set
            {
                this._DiscountName = value;
            }
        }

        /// <summary>
        /// Gets or sets the details of the discount applied
        /// </summary>
        public string DiscountText
        {
            get
            {
                return this._DiscountText;
            }

            set
            {
                this._DiscountText = value;
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    public partial class ZNodePaging
    {
        /// <summary>
        /// Zeon Extn Method to apply all filters and get the paged products.
        /// </summary>
        /// <param name="productIds">List that contains preloaded data.</param>
        /// <param name="orderBy">Sort the the product by field</param>
        /// <param name="orderDirection">Sort the product by direction (ASC or DESC)</param>
        /// <param name="pageIndex">Row number at which to start reading.</param>
        /// <param name="pageSize">Number of rows to return.</param>
        /// <param name="orderOption">Order by option.</param>
        /// <returns>Returns the sorted ZNodeProductList object.</returns>
        public static ZNodeProductList GetProductsExtn(List<int> productIds, string orderBy, string orderDirection, int pageIndex, int pageSize, int orderOption, int categoryID = 0)
        {
            ZNodeProductList productList = null;
            var collection = new ZNodeGenericCollection<ZNodeProductBase>();

            if (pageIndex > 0 && productIds != null)
            {
                pageIndex = pageIndex - 1;

                if (pageSize == -1)
                {
                    pageSize = productIds.Count;
                }

                int pageLowerBound = pageSize * pageIndex;
                int pageUpperBound = (pageLowerBound + pageSize) - pageLowerBound;

                int totalPageCount = 0;
                int totalRecordCount = 0;

                StringBuilder xmlOut = new StringBuilder();
                xmlOut.Append("<ZNodeProductList>");

                if (productIds.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    if (orderBy == "FinalPrice" || orderBy == "Name" || orderBy == "Rating")
                    {
                        foreach (var item in productIds)
                        {
                            sb.Append(item.ToString());
                            sb.Append(",");
                        }

                        //Zeon Custom Code : Get the filter Product ID first only in case of sorting parameter and then retrive the actual Product list
                        if (pageSize != -1)
                        {
                            StringBuilder filteredProductIDs = new StringBuilder();
                            filteredProductIDs = GetProductIDSortBy(sb, orderBy, orderDirection, pageLowerBound, pageUpperBound, false, categoryID);
                            if (filteredProductIDs != null && filteredProductIDs.Length > 0)
                            {
                                sb = new StringBuilder();
                                sb = filteredProductIDs;
                            }
                        }
                    }
                    else
                    {
                        if (categoryID == 0)
                        {
                            foreach (var item in productIds.Skip(pageLowerBound).Take(pageUpperBound))
                            {
                                sb.Append(item.ToString());
                                sb.Append(",");
                            }
                        }
                        else
                        {
                            foreach (var item in productIds)
                            {
                                sb.Append(item.ToString());
                                sb.Append(",");
                            }

                            //Zeon Custom Code : Get the filter Product ID first only in case of sorting parameter and then retrive the actual Product list
                            if (pageSize != -1)
                            {
                                StringBuilder filteredProductIDs = new StringBuilder();
                                filteredProductIDs = GetProductIDSortBy(sb, orderBy, orderDirection, pageLowerBound, pageUpperBound, true, categoryID);
                                if (filteredProductIDs != null && filteredProductIDs.Length > 0)
                                {
                                    sb = new StringBuilder();
                                    sb = filteredProductIDs;
                                }
                            }
                        }
                    }

                    totalRecordCount = productIds.Count;
                    totalPageCount = totalRecordCount % pageSize == 0 ? totalRecordCount / pageSize : ((totalRecordCount / pageSize) + 1);

                    ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();

                    xmlOut.Append(productHelper.GetProductsByIds(sb.ToString(), categoryID));
                }

                xmlOut.Append("<TotalPageCount>");
                xmlOut.Append(totalPageCount);
                xmlOut.Append("</TotalPageCount>");

                xmlOut.Append("<TotalRecordCount>");
                xmlOut.Append(totalRecordCount);
                xmlOut.Append("</TotalRecordCount>");

                xmlOut.Append("</ZNodeProductList>");

                // Serialize the object
                ZNodeSerializer serializer = new ZNodeSerializer();

                productList = (ZNodeProductList)serializer.GetContentFromString(xmlOut.ToString(), typeof(ZNodeProductList));

                if (productList != null && productList.ZNodeProductCollection != null && productList.ZNodeProductCollection.Count > 0)
                {
                    if (categoryID > 0)
                    {
                        productList.ZNodeProductCollection = productList.ZNodeProductCollection.Sort("RowIndex").Sort("DisplayOrder");
                    }
                    else
                    {
                        productList.ZNodeProductCollection = productList.ZNodeProductCollection.Sort("RowIndex");
                    }
                }
                if (!string.IsNullOrEmpty(orderBy) && !string.IsNullOrEmpty(orderDirection))
                {
                    var productCollection = productList.ZNodeProductCollection.Cast<ZNodeProductBase>();
                    if (orderDirection == "ASC")
                    {
                        if (orderBy == "Name") { productCollection = productCollection.OrderBy(x => x.Name).ToList(); }
                        if (orderBy == "FinalPrice") { productCollection = productCollection.OrderBy(x => x.CallForPricing).ThenBy(x => x.FinalPrice).ToList(); }
                    }
                    else
                    {
                        if (orderBy == "Name") { productCollection = productCollection.OrderByDescending(x => x.Name); }
                        if (orderBy == "Rating") { productCollection = productCollection.OrderByDescending(x => x.ReviewRating); }
                        if (orderBy == "FinalPrice") { productCollection = productCollection.OrderBy(x => x.CallForPricing).ThenByDescending(x => x.FinalPrice); }
                    }
                    //productCollection.Skip(pageLowerBound)
                    //.Take(pageUpperBound).ToList()
                    //.ForEach(x => collection.Add(x));

                    productCollection.ToList()
                    .ForEach(x => collection.Add(x));

                    productList.ZNodeProductCollection = collection;
                }
            }

            return productList;
        }

        /// <summary>
        /// Get ProductIDs by Sort By
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDirection"></param>
        /// <param name="pageLowerBound"></param>
        /// <param name="pageUpperBound"></param>
        /// <param name="isOrderByDisplayOrder"></param>
        /// <param name="categoryID"></param>
        /// <returns></returns>
        public static StringBuilder GetProductIDSortBy(StringBuilder sb, string orderBy, string orderDirection, int pageLowerBound, int pageUpperBound, bool isOrderByDisplayOrder, int categoryID)
        {
            ZNodeProductList productList = null;
            var collection = new ZNodeGenericCollection<ZNodeProductBase>();

            if (sb != null && sb.Length > 0)
            {
                StringBuilder xmlOut = new StringBuilder();
                xmlOut.Append("<ZNodeProductList>");

                ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
                xmlOut.Append(productHelper.GetProductsByIdsExtnSortBy(sb.ToString(), categoryID));

                xmlOut.Append("</ZNodeProductList>");

                // Serialize the object
                ZNodeSerializer serializer = new ZNodeSerializer();

                productList = (ZNodeProductList)serializer.GetContentFromString(xmlOut.ToString(), typeof(ZNodeProductList));
                if (productList != null && productList.ZNodeProductCollection != null && productList.ZNodeProductCollection.Count > 0)
                {
                    if (categoryID > 0)
                    {
                        productList.ZNodeProductCollection = productList.ZNodeProductCollection.Sort("RowIndex").Sort("DisplayOrder");
                    }
                    else
                    {
                        productList.ZNodeProductCollection = productList.ZNodeProductCollection.Sort("RowIndex");
                    }
                }
                if (!string.IsNullOrEmpty(orderBy) && !string.IsNullOrEmpty(orderDirection))
                {

                    var productCollection = productList.ZNodeProductCollection.Cast<ZNodeProductBase>();
                    if (orderDirection == "ASC")
                    {
                        if (orderBy == "Name") { productCollection = productCollection.OrderBy(x => x.Name).ToList(); }
                        if (orderBy == "FinalPrice") { productCollection = productCollection.OrderBy(x => x.CallForPricing).ThenBy(x => x.FinalPrice).ToList(); }
                    }
                    else
                    {
                        if (orderBy == "Name") { productCollection = productCollection.OrderByDescending(x => x.Name); }
                        if (orderBy == "Rating") { productCollection = productCollection.OrderByDescending(x => x.ReviewRating).ToList(); }
                        if (orderBy == "FinalPrice") { productCollection = productCollection.OrderBy(x => x.CallForPricing).ThenByDescending(x => x.FinalPrice); }
                    }
                    productCollection.Skip(pageLowerBound)
                    .Take(pageUpperBound).ToList()
                    .ForEach(x => collection.Add(x));

                    productList.ZNodeProductCollection = collection;
                }
                else if (isOrderByDisplayOrder)
                {
                    var productCollection = productList.ZNodeProductCollection.Cast<ZNodeProductBase>();
                    productCollection.Skip(pageLowerBound)
                    .Take(pageUpperBound).ToList()
                    .ForEach(x => collection.Add(x));

                    productList.ZNodeProductCollection = collection;
                }


                if (productList != null && productList.ZNodeProductCollection != null && productList.ZNodeProductCollection.Count > 0)
                {
                    sb = new StringBuilder();
                    foreach (ZNodeProductBase item in productList.ZNodeProductCollection)
                    {
                        sb.Append(item.ProductID.ToString());
                        sb.Append(",");
                    }
                }
            }

            return sb;
        }
    }
}

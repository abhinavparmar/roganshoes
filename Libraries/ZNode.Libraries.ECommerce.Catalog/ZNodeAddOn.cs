using System;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the properties and methods of a product AddOn.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeAddOn : ZNodeAddOnEntity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeAddOn class
        /// </summary>
        public ZNodeAddOn()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeAddOn class
        /// </summary>
        /// <param name="addOnEntity">ZNodeAddOnEntity object.</param>
        public ZNodeAddOn(ZNodeAddOnEntity addOnEntity)
        {
            this.AddOnID = addOnEntity.AddOnID;
            this.AddOnValueCollection = addOnEntity.AddOnValueCollection;
            this.AllowBackOrder = addOnEntity.AllowBackOrder;
            this.BackOrderMsg = addOnEntity.BackOrderMsg;
            this.Description = addOnEntity.Description;
            this.DisplayOrder = addOnEntity.DisplayOrder;
            this.DisplayType = addOnEntity.DisplayType;
            this.InStockMsg = addOnEntity.InStockMsg;
            this.OptionalInd= addOnEntity.OptionalInd;
            this.Name = addOnEntity.Name;
            this.OutOfStockMsg = addOnEntity.OutOfStockMsg;
            this.ProductId = addOnEntity.ProductId;
            this.Title = addOnEntity.Title;
            this.TrackInventoryInd = addOnEntity.TrackInventoryInd;
        }
        #endregion

        /// <summary>
        /// Gets or sets the product addon value collection
        /// </summary>
        [XmlIgnore()]
        public ZNodeGenericCollection<ZNodeAddOnValue> ZNodeAddOnValueCollection
        {
            get
            {
                ZNodeGenericCollection<ZNodeAddOnValue> addOnValues = new ZNodeGenericCollection<ZNodeAddOnValue>();

                foreach (ZNodeAddOnValueEntity entity in this._AddonValueCollection)
                {
                    addOnValues.Add(new ZNodeAddOnValue(entity));
                }

                return addOnValues;
            }

            set
            {
                foreach (ZNodeAddOnValue entity in value)
                {
                    this._AddonValueCollection.Add((ZNodeAddOnValue)entity);
                }
            }
        }
    }
}

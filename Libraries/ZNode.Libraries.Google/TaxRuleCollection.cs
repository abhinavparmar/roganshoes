using System.Collections;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Google
{
    /// <summary>    
    /// Provides methods to handle google tax Rules
    /// </summary>
    internal class TaxRuleCollection : ZNodeBusinessBase
    {
        #region Private member variables
        /// <summary>
        /// Holds the collection of DefaultTaxRule object
        /// </summary>
        private ArrayList _TaxRules = new ArrayList();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the TaxRuleCollection class.
        /// </summary>
        public TaxRuleCollection() 
        {
            this._TaxRules = new ArrayList();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the tax rules
        /// </summary>
        public ArrayList TaxRules
        {
            get { return this._TaxRules; }
            set { this._TaxRules = value; }
        } 
        #endregion

        #region Public Methods
        /// <summary>        
        /// Create a new DefaultTaxRule
        /// This method adds a tax rule associated with a particular state
        /// </summary>
        /// <param name="defaultRule">Default Tax rule</param>
        public void AddRule(ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRule defaultRule)
        {
            this._TaxRules.Add(defaultRule);
        }

        /// <summary>        
        /// Create a new DefaultTaxRule
        /// This method adds a tax rule associated with a particular state
        /// </summary>
        /// <param name="StateAreaCode">State Area code</param>
        /// <param name="TaxRate">Tax rate for the rule</param>
        public void AddRule(ZNode.Libraries.Google.GoogleCheckoutServices.USStateArea StateAreaCode, double TaxRate)
        {
            ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRule _taxRule = new ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRule();
            _taxRule.rate = TaxRate;
            _taxRule.taxarea = new ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRuleTaxarea();            
            _taxRule.taxarea.Item = StateAreaCode;
                        
            this._TaxRules.Add(_taxRule);
        }

        /// <summary>
        /// This method adds a tax rule associated with a zip code pattern.
        /// </summary>
        /// <param name="PostalCode">Postal Code</param>
        /// <param name="TaxRate">Tax rate for the tule</param>
        /// <param name="IsShippingTaxed">Is the shipping taxed</param>
        public void AddRule(ZNode.Libraries.Google.GoogleCheckoutServices.USZipArea PostalCode, double TaxRate, bool IsShippingTaxed)
        {
            ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRule _taxRule = new ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRule();
            _taxRule.rate = TaxRate;
            _taxRule.taxarea = new ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRuleTaxarea();
            _taxRule.taxarea.Item = PostalCode;
            _taxRule.shippingtaxed = IsShippingTaxed;
            this._TaxRules.Add(_taxRule);
        }

        /// <summary>        
        /// This method adds a tax rule associated with a zip code pattern.        
        /// </summary>
        /// <param name="USAreaCode">US Area code</param>
        /// <param name="TaxRate">Tax rate of the rule</param>
        public void AddRule(ZNode.Libraries.Google.GoogleCheckoutServices.USAreas USAreaCode, double TaxRate)
        {
            ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRule _taxRule = new ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRule();
            _taxRule.rate = TaxRate;
            _taxRule.taxarea = new ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRuleTaxarea();
            _taxRule.taxarea.Item = USAreaCode.ToString();

            this._TaxRules.Add(_taxRule);
        }

        /// <summary>
        /// Returns the no of items in the list
        /// </summary>
        /// <returns>returns the count of tax rules</returns>
        public int GetItemsCount()
        {
            return this._TaxRules.Count;
        }

        /// <summary>
        /// Returns the Current Tax Rule for this Index
        /// </summary>
        /// <param name="i">index value i</param>
        /// <returns>Returns the default Tax rule</returns>
        public ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRule GetByIndex(int i)
        {
            return (ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRule)this._TaxRules[i] as ZNode.Libraries.Google.GoogleCheckoutServices.DefaultTaxRule;
        }

        #endregion
    }
}

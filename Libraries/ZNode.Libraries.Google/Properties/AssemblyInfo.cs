﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ZNode.Libraries.Google")]
[assembly: AssemblyDescription("Znode Multifront Google Payment Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Znode Inc.")]
[assembly: AssemblyProduct("ZNode.Libraries.Google")]
[assembly: AssemblyCopyright("Copyright ©  2010, Znode Inc. All Rights Reserved")]
[assembly: AssemblyTrademark("Znode Multifront")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7f364717-2052-4051-9aba-67d9e7ba4a55")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]

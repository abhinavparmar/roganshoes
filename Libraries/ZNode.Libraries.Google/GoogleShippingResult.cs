namespace ZNode.Libraries.Google.GoogleCallbackServices
{
    #region merchant shipping rate
    /// <summary>
    /// This class creates an object to indicate the cost of a particular 
    /// shipping method and the availability of that shipping method for the 
    /// designated shipping address.
    /// </summary>
    public class ShippingResult
    {
        #region Private Varibles
        private decimal _ShippingRate = 0;
        private bool _Shippable = false;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the shipping rate
        /// </summary>
        public decimal ShippingRate
        {
            get { return this._ShippingRate; }
            set { this._ShippingRate = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether it is shippable or not.
        /// </summary>
        public bool Shippable
        {
            get { return this._Shippable; }
            set { this._Shippable = value; }
        }
        #endregion
    }
    #endregion
}

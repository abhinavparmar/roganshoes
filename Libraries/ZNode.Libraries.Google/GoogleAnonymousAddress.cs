using System.Collections;
using System.Xml;
using ZNode.Libraries.Google.GoogleCheckoutServices;

namespace ZNode.Libraries.Google.GoogleCallbackServices
{
    #region Anonymous Address
    /// <summary>
    /// This class creates an object that identifies the customer's shipping 
    /// address. Your class that inherits from CallbackRules.cs will receive an 
    /// object of this type that identifies the customer's shipping address.
    /// </summary>
    public class AnonymousAddress
    {
        private string _City;
        private string _CountryCode;
        private string _Id;
        private string _PostalCode;
        private string _Region;

        /// <summary>
        /// Initializes a new instance of the AnonymousAddress class
        /// <see cref="AnonymousAddress"/>
        /// </summary>
        /// <param name="ThisAddress">The <see cref="AutoGen.AnonymousAddress"/>used to create the object.</param>
        public AnonymousAddress(ZNode.Libraries.Google.GoogleCheckoutServices.AnonymousAddress ThisAddress)
        {
            this._City = ThisAddress.city;
            this._CountryCode = ThisAddress.countrycode;
            this._Id = ThisAddress.id;
            this._PostalCode = ThisAddress.postalcode;
            this._Region = ThisAddress.region;
        }

        /// <summary>
        /// Gets the city to which an order is being shipped.
        /// </summary>
        public string City
        {
            get { return this._City; }
        }

        /// <summary>
        /// Gets the country code associate with an order's billing
        /// address or shipping address. The value of this tag must be a 
        /// two-letter
        /// <a href="http://www.iso.org/iso/en/prods-services/iso3166ma/02iso-3166-code-lists/list-en1.html">ISO 3166-1</a>
        /// country code.
        /// </summary>
        public string CountryCode
        {
            get { return this._CountryCode; }
        }

        /// <summary>
        /// Gets the id attribute specifies a unique that identifies a particular address.
        /// </summary>
        public string Id
        {
            get { return this._Id; }
        }

        /// <summary>
        /// Gets the zip code or postal code associated with either a billing address or a shipping address
        /// </summary>
        public string PostalCode
        {
            get { return this._PostalCode; }
        }

        /// <summary>
        /// Gets the state or province associated with a billing address or shipping address.
        /// </summary>
        public string Region
        {
            get { return this._Region; }
        }
    }
    #endregion
}

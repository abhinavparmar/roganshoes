using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Google.GoogleCallbackServices;
using ZNode.Libraries.Google.GoogleCheckoutServices;

namespace ZNode.Libraries.Google
{
    /// <summary>
    /// Provides methods to return merchant calculation results like shipping charge,tax,etc.
    /// </summary>
    public class GoogleMerchantCalculation : CallbackRules
    {
        #region Protected Member Variables 
        private ZNodeShoppingCart _shoppingCart = null;
        private ZNodeUserAccount _userAccount = null;
        private string CurrencyCode = ZNodeCurrencyManager.CurrencyCode();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the GoogleMerchantCalculation class
        /// </summary>
        public GoogleMerchantCalculation()
        {
            this._shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
            this._userAccount = ZNodeUserAccount.CurrentAccount();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the useraccount
        /// </summary>
        protected ZNodeUserAccount UserAccount
        {
            get { return this._userAccount; }
            set { this._userAccount = value; }
        }

        /// <summary>
        /// Gets or sets the shopping cart
        /// </summary>
        protected ZNodeShoppingCart ShoppingCart
        {
            get { return this._shoppingCart; }
            set { this._shoppingCart = value; }
        }

        /// <summary>
        /// Gets the Default Profile
        /// </summary>
        private static Profile DefaultRegisteredProfile
        {
            get
            {
                Profile profile = null;
                bool check = false;

                if (profile != null)
                {
                    if (profile.ProfileID == ZNodeConfigManager.SiteConfig.DefaultRegisteredProfileID.GetValueOrDefault())
                    {
                        check = true;
                    }
                }

                if (!check)
                {
                    // Get Portal by Domain Config
                    ProfileService ProfileService = new ProfileService();
                    Domain currentDomain = ZNodeConfigManager.DomainConfig;

                    // Get DefaultRegistered Profile
                    if (currentDomain != null)
                    {
                        PortalService portalService = new PortalService();
                        Portal currentPortal = portalService.GetByPortalID(currentDomain.PortalID);
                        profile = ProfileService.GetByProfileID(currentPortal.DefaultRegisteredProfileID.GetValueOrDefault());
                    }
                }

                return profile;
            }
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Returns tax cost
        /// </summary>
        /// <param name="ThisOrder">This order</param>
        /// <param name="Address">Anonymous address</param>
        /// <param name="ShippingRate">Shipping Rate</param>
        /// <returns>Returns the tax result</returns>
        public override decimal GetTaxResult(Google.GoogleCallbackServices.Order ThisOrder, ZNode.Libraries.Google.GoogleCallbackServices.AnonymousAddress Address, decimal ShippingRate)
        {
            if (this._userAccount == null)
            {
                this.BuildUserAccount(ThisOrder, Address);
            }
            else
            {
                this._userAccount.BillingAddress.City = Address.City;
                this._userAccount.BillingAddress.CountryCode = Address.CountryCode;
                this._userAccount.BillingAddress.StateCode = Address.Region;
                this._userAccount.BillingAddress.PostalCode = Address.PostalCode;
            }

            if (this._shoppingCart == null)
            {
                this.BuildShoppingCart(ThisOrder);
            }
            else
            {
                this._shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
            }

            this._shoppingCart.Payment.BillingAddress = this._userAccount.BillingAddress;
            this._shoppingCart.Calculate();

            return this._shoppingCart.TaxCost;
        }

        /// <summary>
        /// Returns shipping cost
        /// </summary>
        /// <param name="ShipMethodName">Shipping method</param>
        /// <param name="ThisOrder">This order</param>
        /// <param name="Address">Anonymous address</param>
        /// <returns>Returns the shipping result</returns>
        public override ShippingResult GetShippingResult(string ShipMethodName, Google.GoogleCallbackServices.Order ThisOrder, ZNode.Libraries.Google.GoogleCallbackServices.AnonymousAddress Address)
        {
            int index = ShipMethodName.LastIndexOf('_');
            int shippingID = int.Parse(ShipMethodName.Substring(index + 1));            

            ShippingResult result = new ShippingResult();

            if (this._userAccount == null)
            {
                this.BuildUserAccount(ThisOrder, Address);
            }
            else
            {
                this._userAccount.ShippingAddress.City = Address.City;
                this._userAccount.ShippingAddress.CountryCode = Address.CountryCode;
                this._userAccount.ShippingAddress.StateCode = Address.Region;
                this._userAccount.ShippingAddress.PostalCode = Address.PostalCode;
            }

            if (this._shoppingCart == null)
            {
                this.BuildShoppingCart(ThisOrder);
            }
            else
            {
                this._shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
            }

            this._shoppingCart.Shipping.ShippingID = shippingID;
            this._shoppingCart.Calculate();

            if (this._shoppingCart.Shipping.ResponseCode != "0")
            {                
                result.Shippable = false;
                result.ShippingRate = 0;
            }
            else
            {
                result.Shippable = true;
                result.ShippingRate = this._shoppingCart.ShippingCost;
            }

            return result;
        }
        #endregion

        #region Helper methods
        /// <summary>
        /// Creates an user account object and adds it into Session
        /// </summary>
        /// <param name="ThisOrder">this order</param>
        /// <param name="Address">Anonymous address</param>
        private void BuildUserAccount(Google.GoogleCallbackServices.Order ThisOrder, Google.GoogleCallbackServices.AnonymousAddress Address)
        {
            string strProfileID = ThisOrder.MerchantPrivateData;            
            Profile profile = new Profile();
            ProfileService profileService = new ProfileService();

            if (strProfileID.Length == 0)
            {
                profile = DefaultRegisteredProfile;                
            }
            else 
            {
                profile = profileService.GetByProfileID(int.Parse(strProfileID));
            }

            this._userAccount = new ZNodeUserAccount();
            this._userAccount.ProfileID = profile.ProfileID;
            this._userAccount.ShippingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
            this._userAccount.ShippingAddress.City = Address.City;
            this._userAccount.ShippingAddress.CountryCode = Address.CountryCode;
            this._userAccount.ShippingAddress.StateCode = Address.Region;
            this._userAccount.ShippingAddress.PostalCode = Address.PostalCode;

            this._userAccount.BillingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
            this._userAccount.BillingAddress.City = Address.City;
            this._userAccount.BillingAddress.CountryCode = Address.CountryCode;
            this._userAccount.BillingAddress.StateCode = Address.Region;
            this._userAccount.BillingAddress.PostalCode = Address.PostalCode;

            System.Web.HttpContext.Current.Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), this._userAccount);

            // Hold this profile object in the session state
            System.Web.HttpContext.Current.Session["ProfileCache"] = profile;
        }

        /// <summary>
        /// Creates an shopping cart object and adds it to Session
        /// </summary>
        /// <param name="ThisOrder">This order</param>
        private void BuildShoppingCart(Google.GoogleCallbackServices.Order ThisOrder)
        {
            this._shoppingCart = new ZNodeShoppingCart();

            System.Collections.IEnumerator enumerator = ThisOrder.GetEnumerator();
            int i = 0;
            System.Text.RegularExpressions.Match m;
            string privateItemData;
            int? shippingRuleTypeID = null;
            decimal weight = 0;
            decimal height = 0;
            decimal width = 0;
            decimal length = 0;
            bool taxExemptInd = false;

            while (enumerator.MoveNext())
            {
                OrderLine lineItem = (OrderLine)enumerator.Current;
                privateItemData = lineItem.MerchantPrivateItemData;                

                m = System.Text.RegularExpressions.Regex.Match(privateItemData, "(?<=<ShippingRuleTypeID[^>]*>).*?(?=</ShippingRuleTypeID>)", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
                if (m.Success) 
                { 
                    shippingRuleTypeID = int.Parse(m.Value); 
                }

                m = System.Text.RegularExpressions.Regex.Match(privateItemData, "(?<=<Weight[^>]*>).*?(?=</Weight>)", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
                if (m.Success) 
                { 
                    weight = decimal.Parse(m.Value); 
                }

                m = System.Text.RegularExpressions.Regex.Match(privateItemData, "(?<=<Length[^>]*>).*?(?=</Length>)", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
                if (m.Success) 
                { 
                    length = decimal.Parse(m.Value); 
                }

                m = System.Text.RegularExpressions.Regex.Match(privateItemData, "(?<=<Width[^>]*>).*?(?=</Width>)", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
                if (m.Success) 
                { 
                    width = decimal.Parse(m.Value); 
                }

                m = System.Text.RegularExpressions.Regex.Match(privateItemData, "(?<=<Height[^>]*>).*?(?=</Height>)", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
                if (m.Success) 
                { 
                    height = decimal.Parse(m.Value); 
                }

                m = System.Text.RegularExpressions.Regex.Match(privateItemData, "(?<=<TaxExempt[^>]*>).*?(?=</TaxExempt>)", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
                if (m.Success) 
                { 
                    taxExemptInd = bool.Parse(m.Value); 
                }

                ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                ZNodeProductBase product = new ZNodeProductBase();

                product.ProductID = i++;                
                product.RetailPrice = lineItem.UnitPrice;
                item.Product = product;
                item.Quantity = lineItem.Quantity;
                item.Product.ShippingRuleTypeID = shippingRuleTypeID;
                item.Product.TaxClassID = 0; 

                // add Item to cart
                this._shoppingCart.AddToCart(item);

                shippingRuleTypeID = null;                
                privateItemData = string.Empty;
                taxExemptInd = false;
            }

            // add to session
            this._shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
        }
        #endregion
    }
}

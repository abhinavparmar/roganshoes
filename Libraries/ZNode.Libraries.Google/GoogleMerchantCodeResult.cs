﻿using System.Collections;
using System.Xml;
using ZNode.Libraries.Google.GoogleCheckoutServices;

namespace ZNode.Libraries.Google.GoogleCallbackServices
{
    /// <summary>
    /// This class creates an object to indicate whether the coupon and gift 
    /// certificate codes that were supplied by the customer are valid. The 
    /// object also indicates whether the code is for a coupon, gift certificate 
    /// or other discount as well as the amount of the credit associated with 
    /// the code. Your class that inherits from CallbackRules.cs will access 
    /// these objects to calculate discounts associated with coupons and gift 
    /// certificates.
    /// </summary>
    public class MerchantCodeResult
    {
        #region Private Variables
        /// <summary>
        /// Define the Type of Merchant Code
        /// </summary>
        private MerchantCodeType _Type;

        /// <summary>
        /// Return if the Code is valid or not
        /// </summary>
        private bool _Valid = false;

        /// <summary>
        /// Return the Value of the Code
        /// </summary>
        private decimal _Amount = 0;

        /// <summary>
        /// Return a message relating to the callback.
        /// </summary>
        private string _Message = string.Empty; 
        #endregion

        #region Merchant Code Enumerator
        /// <summary>
        /// Merchant Code Types
        /// </summary>
        public enum MerchantCodeType
        {
            /// <summary>
            /// Represents the unknown
            /// </summary>
            Undefined = 0,

            /// <summary>
            /// Represents the gift certificate
            /// </summary>
            GiftCertificate = 1,

            /// <summary>
            /// Represents the coupon
            /// </summary>
            Coupon = 2
        } 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Merchant Code Type
        /// </summary>
        public MerchantCodeType Type
        {
            get { return this._Type; }
            set { this._Type = value; }
        }

        /// <summary>
        /// Gets or sets the message
        /// </summary>
        public string Message
        {
            get { return this._Message; }
            set { this._Message = value; }
        }

        /// <summary>
        /// Gets or sets the amount
        /// </summary>
        public decimal Amount
        {
            get { return this._Amount; }
            set { this._Amount = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether it is valid or not.
        /// </summary>
        public bool Valid
        {
            get { return this._Valid; }
            set { this._Valid = value; }
        } 
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;
using Zeon.Libraries.Elmah;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.ShoppingCart
{
    /// <summary>
    /// Order Checkout Class - Orchestrates the checkout process
    /// </summary>
    public partial class ZNodeCheckout : ZNodeBusinessBase
    {
        #region MemberVariable
        bool _ShoppingCartNotesEnabled = false;
        bool _IsTaxExemptedCustomer = false;
        #endregion

        #region Public Properties

        /// <summary>
        /// get or set value of _ShoppingCartNotesEnabled
        /// </summary>
        public bool ShoppingCartNotesEnabled
        {
            get { return _ShoppingCartNotesEnabled; }
            set { _ShoppingCartNotesEnabled = value; }
        }

        /// <summary>
        /// get or set value of _ShoppingCartNotesEnabled
        /// </summary>
        public bool TaxExemptedCustomer
        {
            get { return _IsTaxExemptedCustomer; }
            set { _IsTaxExemptedCustomer = value; }
        }

        /// <summary>
        /// get or set value of AdditionalCreditCardError
        /// </summary>
        public string AdditionalCreditCardError { get; set; }

        #endregion

        #region Private Methods

        /// <summary>
        /// add orderExtn Data into database
        /// </summary>
        /// <param name="orderID">int</param>
        private void AddOrderExtnToDatabase(int orderID, List<ZNode.Libraries.ECommerce.Entities.CreditCard> multiCreditCard, ZNode.Libraries.ECommerce.Entities.CreditCard defaultCard, ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse paymentRes)
        {
            if (orderID > 0)
            {
                try
                {
                    OrderExtnService orderExtnService = new OrderExtnService();
                    List<ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse> paymentResList = paymentRes.AdditionalTenderResponse;
                    OrderExtn orderExtn = new OrderExtn();
                    orderExtn.OrderID = orderID;
                    orderExtn.IsBillingPoBox = this.UserAccount.BillingAddressExtn.IsPOBox;
                    orderExtn.IsShippingPOBox = this.UserAccount.ShippingAddressExtn.IsPOBox;
                    if (_ShoppingCart.Payment.PaymentSetting.PaymentTypeID == 0 && _ShoppingCart.Payment.CreditCard != null && _ShoppingCart.Payment.CreditCard.CardNumber.Length > 4)
                    {
                        orderExtn.CardNumber = EncryptCreditCardNumber(this._ShoppingCart.Payment.CreditCard.CardNumber.ToString());
                        orderExtn.CardType = this._ShoppingCart.Payment.CreditCard.CreditCardNumberType.ToString();
                        orderExtn.Custom5 = _ShoppingCart.IsShipngChrgsExtended ? _ShoppingCart.ShippingCost.ToString() : null;//Save Origional Shipping charges in database
                        //Save Additional Data
                        orderExtn.CardTransactionID = paymentRes.TransactionId;
                        orderExtn.CardAmount = defaultCard.Amount;
                        orderExtn.CardExp = defaultCard.CreditCardExp;
                        orderExtn.CardAuthCode = defaultCard.CardSecurityCode;
                        orderExtn.CardPaymentStatusID = (int)paymentRes.PaymentStatus;
                        //Perficient Custom Code For Multiple Tenders #44:Starts
                        if (multiCreditCard != null && multiCreditCard.Count > 0 && paymentResList != null && paymentResList.Count > 0 && paymentResList.Count == multiCreditCard.Count)
                        {
                            int index = 0;
                            AddFirstCardData(multiCreditCard, paymentResList, orderExtn, index);
                            if (multiCreditCard.Count > 1)
                            {
                                AddSecondCardData(multiCreditCard, paymentResList, orderExtn, index);
                            }
                        }
                        //Perficient Custom Code For Multiple Tenders #44:Ends
                    }
                    orderExtn.ExemptInd = this.TaxExemptedCustomer;
                    orderExtn.TaxRate = ShoppingCart.AvaTaxRate;
                    orderExtn.ExemptReason = GetTaxExemptReason();

                    int currentProfile = this.UserAccount.ProfileID;

                    orderExtnService.Save(orderExtn);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                    ElmahErrorManager.Log(ex);
                    if (ex.InnerException != null)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.InnerException.ToString()); // log exception
                    }
                    throw;
                }

            }
        }


        /// <summary>
        /// Add First Additional Card Data
        /// </summary>
        /// <param name="multiCreditCard"></param>
        /// <param name="paymentResList"></param>
        /// <param name="orderExtn"></param>
        /// <param name="index"></param>
        private void AddFirstCardData(List<ZNode.Libraries.ECommerce.Entities.CreditCard> multiCreditCard, List<ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse> paymentResList, OrderExtn orderExtn, int index)
        {
            orderExtn.Card1Number = EncryptCreditCardNumber(multiCreditCard[index].CardNumber.ToString());
            orderExtn.Card1TransactionID = paymentResList[index].TransactionId;
            orderExtn.Card1AuthCode = multiCreditCard[index].CardSecurityCode;
            string cardType = GetCardTypeFromNumberExtn(multiCreditCard[index].CardNumber);
            orderExtn.Card1Type = multiCreditCard[index].CardType = !string.IsNullOrWhiteSpace(cardType) ? cardType : null;
            orderExtn.Card1Exp = multiCreditCard[index].CreditCardExp;
            orderExtn.Card1Amount = multiCreditCard[index].Amount;
            orderExtn.Card1PaymentStatusID = (int)paymentResList[index].PaymentStatus;
        }

        /// <summary>
        /// Add details of second credit card
        /// </summary>
        /// <param name="multiCreditCard"></param>
        /// <param name="paymentResList"></param>
        /// <param name="orderExtn"></param>
        /// <param name="index"></param>
        private void AddSecondCardData(List<ZNode.Libraries.ECommerce.Entities.CreditCard> multiCreditCard, List<ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse> paymentResList, OrderExtn orderExtn, int index)
        {
            orderExtn.Card2Number = EncryptCreditCardNumber(multiCreditCard[index + 1].CardNumber.ToString());
            orderExtn.Card2TransactionID = paymentResList[index + 1].TransactionId;
            orderExtn.Card2AuthCode = multiCreditCard[index + 1].CardSecurityCode;
            string card2Type = GetCardTypeFromNumberExtn(multiCreditCard[index].CardNumber);
            orderExtn.Card2Type = multiCreditCard[index + 1].CardType = !string.IsNullOrWhiteSpace(card2Type) ? card2Type : null; ;
            orderExtn.Card2Exp = multiCreditCard[index + 1].CreditCardExp;
            orderExtn.Card2Amount = multiCreditCard[index + 1].Amount;
            orderExtn.Card2PaymentStatusID = (int)paymentResList[index + 1].PaymentStatus;
        }


        /// <summary>
        ///  Encrypt Credit Card Number
        /// </summary>
        /// <param name="cardNumber">string cardNumber</param>
        /// <returns>String</returns>
        private string EncryptCreditCardNumber(string cardNumber)
        {
            StringBuilder modifiedCardNumber = new StringBuilder();
            cardNumber.Substring(cardNumber.Length - 4);
            foreach (char ch in cardNumber.Substring(0, cardNumber.Length - 4))
            {
                modifiedCardNumber.Append("X");
            }
            modifiedCardNumber.Append(cardNumber.Substring(cardNumber.Length - 4));
            return modifiedCardNumber.ToString();

        }

        /// <summary>
        ///  Save OrderLineItem in OrderLineItemExtn
        /// </summary>
        /// <param name="orderLineItemID">int</param>
        /// <param name="notes">string</param>
        private void SaveOrderLineItemExtn(ZNodeOrderFulfillment order)
        {
            OrderLineItemExtnService service = new OrderLineItemExtnService();
            try
            {
                if (order != null && order.OrderID > 0)
                {
                    for (int iOrd = 0; iOrd < order.OrderLineItems.Count; iOrd++)
                    {
                        OrderLineItem orderLineItem = order.OrderLineItems[iOrd] as OrderLineItem;
                        OrderLineItemExtn lineItemExtn = new OrderLineItemExtn();
                        ZNodeShoppingCartItem shoppingCartItem = _ShoppingCart.ShoppingCartItems[iOrd] as ZNodeShoppingCartItem;
                        if (shoppingCartItem != null)
                        {
                            lineItemExtn.OrderLineItemID = orderLineItem.OrderLineItemID;
                            lineItemExtn.Notes = shoppingCartItem.Notes;
                            lineItemExtn.Taxable = shoppingCartItem.Product.IsTaxable;
                            lineItemExtn.DiscountPercent = shoppingCartItem.Product.PricingDiscountPercent + shoppingCartItem.Product.PromotionDiscountPercent;
                            service.Save(lineItemExtn);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string orderID = string.Empty;
                if (order != null)
                {
                    orderID = order.OrderID.ToString();
                }
                ElmahErrorManager.Log(ex);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity(20000, "Error in ZNodeCheckoutExtn:AddOrderLineExtnItemToDatabase  Order ID=" + orderID, string.Empty, string.Empty, string.Empty, ex.StackTrace);
            }

        }


        /// <summary>
        /// Get Tax Exempt Reason
        /// </summary>
        /// <returns>int</returns>
        private int GetTaxExemptReason()
        {
            int exemptReasonIndex = 0;
            return exemptReasonIndex = this.TaxExemptedCustomer ? (int)TaxExemptReason.CustomerExempt : (int)TaxExemptReason.NotExempt;
        }

        /// <summary>
        /// Enum Tax Exempt Reason
        /// </summary>
        public enum TaxExemptReason
        {
            NotExempt = 0,
            StateExempt = 1,
            CustomerExempt = 2
        }

        /// <summary>
        /// Create addtional Payment response List
        /// </summary>
        /// <param name="addpaymentResList"></param>
        /// <returns></returns>
        private string GetMultiCardTransactionList(List<ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse> addpaymentResList)
        {
            StringBuilder strMultiCardPaymentList = new StringBuilder();
            try
            {
                if (addpaymentResList != null && addpaymentResList.Count > 0)
                {
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse currResponse in addpaymentResList)
                    {
                        if (!string.IsNullOrWhiteSpace(currResponse.TransactionId))
                        {
                            strMultiCardPaymentList.Append("," + currResponse.TransactionId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Creating trans List" + ex.Message);
                ElmahErrorManager.Log(ex);
            }
            return strMultiCardPaymentList.ToString();
        }

        /// <summary>
        /// Determines the credit card type using the card number
        /// </summary>
        /// <param name="cardNum">Card number to get the card type.</param>
        /// <returns>Returns the CreditCardType from card number.</returns>
        private string GetCardTypeFromNumberExtn(string cardNum)
        {
            // Validation based on article from Wikipedia http://en.wikipedia.org/wiki/Creditcardnumbers
            if (cardNum.StartsWith("4"))
            {
                return CreditCardType.Visa.ToString();
            }
            else if (cardNum.StartsWith("34") || cardNum.StartsWith("37"))
            {
                return CreditCardType.Amex.ToString();
            }
            else if (cardNum.StartsWith("51") || cardNum.StartsWith("52") || cardNum.StartsWith("53") || cardNum.StartsWith("54") || cardNum.StartsWith("55"))
            {
                return CreditCardType.MasterCard.ToString();
            }
            else if (cardNum.StartsWith("6011") || cardNum.StartsWith("62") || cardNum.StartsWith("64") || cardNum.StartsWith("65"))
            {
                return CreditCardType.Discover.ToString();
            }
            else if (cardNum.StartsWith("36"))
            {
                return CreditCardType.Diners.ToString();
            }
            else if (cardNum.StartsWith("5018") || cardNum.StartsWith("5020") || cardNum.StartsWith("5038") || cardNum.StartsWith("6304") || cardNum.StartsWith("6759") || cardNum.StartsWith("6761"))
            {
                return CreditCardType.Maestro.ToString();
            }
            else if (cardNum.StartsWith("6334") || cardNum.StartsWith("6767"))
            {
                return CreditCardType.Solo.ToString();
            }
            else if (cardNum.StartsWith("3528") || cardNum.StartsWith("3589"))
            {
                return CreditCardType.JCB.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion
    }


}

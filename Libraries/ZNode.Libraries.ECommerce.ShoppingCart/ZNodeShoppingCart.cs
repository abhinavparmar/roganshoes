using System;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Xml.Serialization;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Promotions;
using ZNode.Libraries.ECommerce.Shipping;
using ZNode.Libraries.ECommerce.Taxes;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.ShoppingCart
{
    /// <summary>
    /// Represents Shopping cart and shopping cart items
    /// </summary>
    [Serializable()]
    public partial class ZNodeShoppingCart : ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCart
    {
        #region Public Properties
        /// <summary>
        /// Gets or sets the Shoppingcart Items
        /// </summary>
        [XmlIgnore()]
        public new ZNodeGenericCollection<ZNodeShoppingCartItem> ShoppingCartItems
        {
            get
            {
                ZNodeGenericCollection<ZNodeShoppingCartItem> _cartItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();

                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCartItem item in _ShoppingCartItems)
                {
                    _cartItems.Add((ZNodeShoppingCartItem)item);
                }

                return _cartItems;
            }

            set
            {
                foreach (ZNodeShoppingCartItem item in value)
                {
                    _ShoppingCartItems.Add(item);
                }
            }
        }

        /// <summary>
        /// Gets total cost of items in the shopping cart before shipping and taxes
        /// </summary>
        public new decimal SubTotal
        {
            get
            {
                decimal subTotal = 0;

                foreach (ZNodeShoppingCartItem item in _ShoppingCartItems)
                {
                    decimal itemTotal = item.ExtendedPrice;
                    subTotal += itemTotal;
                }

                return Math.Round(subTotal, 2);
            }
        }

        /// <summary>
        /// Gets the total discount of applied to the items in the shopping cart.
        /// </summary>
        public new decimal Discount
        {
            get
            {
                decimal subTotal = this.SubTotal;

                decimal totalDiscount = OrderLevelDiscount;

                foreach (ZNodeShoppingCartItem item in _ShoppingCartItems)
                {
                    totalDiscount += item.DiscountAmount + item.ExtendedPriceDiscount;
                }

                if (totalDiscount > subTotal)
                {
                    return Math.Round(subTotal, 2);
                }

                return Math.Round(totalDiscount, 2);
            }
        }

        /// <summary>
        /// Gets the Total cost after shipping, taxes and promotions
        /// </summary>        
        public new decimal Total
        {
            get
            {
                decimal total = 0;

                total = (this.SubTotal - this.Discount) + ShippingCost + TaxCost + OrderLevelTaxes - GiftCardAmount;

                return total;
            }
        }
        #endregion

        #region static methods
        /// <summary>
        /// Returns the current shopping cart
        /// </summary>
        /// <returns>Returns the current shopping cart details</returns>
        public static new ZNodeShoppingCart CurrentShoppingCart()
        {
            ZNodeShoppingCart shoppingCart;

            // get the user account from session
            shoppingCart = (ZNodeShoppingCart)HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()];

            // not in session
            if (shoppingCart == null)
            {
                return null;
            }
            else
            {
                // return the value from session
                return shoppingCart;
            }
        }

        /// <summary>
        /// Returns the current shopping cart
        /// </summary>
        /// <param name="portalId">current Portal Id</param>
        /// <returns>Retuens the current shopping cart based on the portal id</returns>
        public static ZNodeShoppingCart CurrentShoppingCart(int portalId)
        {
            ZNodeShoppingCart shoppingCart;

            // get the user account from session
            shoppingCart = (ZNodeShoppingCart)HttpContext.Current.Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), portalId)];

            // not in session
            if (shoppingCart == null)
            {
                return null;
            }
            else
            {
                // return the value from session
                return shoppingCart;
            }
        }

        /// <summary>
        /// Returns the current shopping cart
        /// </summary>
        /// <param name="portalId">current Portal Id</param>
        /// <returns>Retuens the current shopping cart based on the portal id</returns>
        public ZNodeShoppingCart AddShoppingCartToSession(int portalId)
        {
            ZNodeShoppingCart shoppingCart = this;

            // get the user account from session
            HttpContext.Current.Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), portalId)] = shoppingCart;

            // not in session
            if (shoppingCart == null)
            {
                return null;
            }
            else
            {
                // return the value from session
                return shoppingCart;
            }
        }

        /// <summary>
        /// Represents the current session key count
        /// </summary>
        public void Test()
        {
            HttpContext.Current.Response.Write(HttpContext.Current.Session.Keys.Count.ToString());
        }

        #endregion

        #region Public and Private Methods

        /// <summary>
        /// Calculates final pricing, shipping and taxes in the cart.
        /// </summary>
        public void Calculate()
        {
            // Clear previous error message
            this.ClearPreviousErrorMessges();

            // ShippingRules
            ZNodeShippingOption shipping = new ZNodeShippingOption(this);
            shipping.Calculate();

            // Promotions
            ZNodePromotionOption promotionRules = new ZNodePromotionOption(this);
            promotionRules.Calculate();

            // TaxRules
            ZNodeTaxOption taxRules = new ZNodeTaxOption(this);
            taxRules.Calculate(this);

            this.GiftCardAmount = 0;

            if (this.GiftCardNumber != string.Empty)
            {
                this.AddGiftCard(this.GiftCardNumber);
            }
        }

        /// <summary>
        /// Process anything that must be done before the order is submitted.
        /// </summary>
        /// <returns>True if the order should be submitted. False if there is anything that will prevent the order from submitting correctly.</returns>
        public bool PreSubmitOrderProcess()
        {
            // Clear previous error message
            this.ClearPreviousErrorMessges();

            bool returnVal = true;

            // ShippingRules
            ZNodeShippingOption shipping = new ZNodeShippingOption(this);
            returnVal &= shipping.PreSubmitOrderProcess();

            // TaxRules
            ZNodeTaxOption taxRules = new ZNodeTaxOption(this);
            returnVal &= taxRules.PreSubmitOrderProcess(this);

            // Promotions
            ZNodePromotionOption promotions = new ZNodePromotionOption(this);
            returnVal &= promotions.PreSubmitOrderProcess();

            return returnVal;
        }

        /// <summary>
        /// Process anything that must be done after the order has been submitted.
        /// </summary>
        public void PostSubmitOrderProcess()
        {
            // ShippingRules
            ZNodeShippingOption shipping = new ZNodeShippingOption(this);
            shipping.PreSubmitOrderProcess();

            // TaxRules
            ZNodeTaxOption taxRules = new ZNodeTaxOption(this);
            taxRules.PreSubmitOrderProcess(this);

            // Promotions
            ZNodePromotionOption promotions = new ZNodePromotionOption(this);
            promotions.PostSubmitOrderProcess();

            // Reduce Inventory
            AddOnValueService AddOnValueServ = new AddOnValueService();
            ProductService productService = new ProductService();
            SKUService skuService = new SKUService();
            int quantity = 0;

            SKUInventoryService skuInventoryService = new SKUInventoryService();

            // Reduce the Product add-ons quantityAvailable by 1 ,if the order is placed successfully        
            // Loop through the Order Line Items
            foreach (ZNodeShoppingCartItem Item in _ShoppingCartItems)
            {
                quantity = Item.Quantity;

                if (Item.Product.TrackInventoryInd && Item.Product.ZNodeBundleProductCollection.Count == 0)
                {
                    SKUInventory skuInventory = skuInventoryService.GetBySKU(Item.Product.SKU);

                    if (skuInventory != null)
                    {
                        // Subtract Order quantity from the inventory quantity
                        skuInventory.QuantityOnHand = skuInventory.QuantityOnHand - quantity;

                        // update into SKU table
                        skuInventoryService.Update(skuInventory);
                    }
                }

                // Loop through the Selected addons for each Shopping cartItem
                foreach (ZNodeAddOnEntity AddOn in Item.Product.SelectedAddOns.AddOnCollection)
                {
                    // Add-On value collection
                    foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        if (AddOn.TrackInventoryInd)
                        {
                            // Update Quantity into Inventory Table.
                            SKUInventory addOnInventory = null;
                            addOnInventory = skuInventoryService.GetBySKU(AddOnValue.SKU);

                            if (addOnInventory != null)
                            {
                                addOnInventory.QuantityOnHand = addOnInventory.QuantityOnHand - quantity;

                                skuInventoryService.Update(addOnInventory);
                            }
                        }
                    }
                }

                // Bundle Product Inventory
                foreach (ZNodeProductBaseEntity bundleProductItem in Item.Product.ZNodeBundleProductCollection)
                {
                    if (bundleProductItem.TrackInventoryInd)
                    {
                        SKUInventory skuInventory = skuInventoryService.GetBySKU(bundleProductItem.SKU);

                        if (skuInventory != null)
                        {
                            // Subtract Order quantity from the inventory quantity
                            skuInventory.QuantityOnHand = skuInventory.QuantityOnHand - quantity;

                            // update into SKU table
                            skuInventoryService.Update(skuInventory);
                        }
                    }

                    // Loop through the Selected addons for each Shopping cartItem
                    foreach (ZNodeAddOnEntity AddOn in bundleProductItem.SelectedAddOns.AddOnCollection)
                    {
                        // Add-On value collection
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            if (AddOn.TrackInventoryInd)
                            {
                                // Update Quantity into Inventory Table.
                                SKUInventory addOnInventory = null;
                                addOnInventory = skuInventoryService.GetBySKU(AddOnValue.SKU);

                                if (addOnInventory != null)
                                {
                                    addOnInventory.QuantityOnHand = addOnInventory.QuantityOnHand - quantity;

                                    skuInventoryService.Update(addOnInventory);
                                }
                            }
                        }
                    }
                }
            }

            // Update the gift card
            if (this.GiftCardNumber != string.Empty)
            {
                GiftCardService giftCardService = new GiftCardService();
                GiftCard giftCard = giftCardService.GetByCardNumber(this.GiftCardNumber);

                // Deduct the balance.
                giftCard.Amount = giftCard.Amount - this.GiftCardAmount;

                // Update the Account Id.                 
                giftCard.AccountId = this.GetAccountId(true);

                giftCardService.Update(giftCard);
            }
        }

        /// <summary>
        /// Adds a coupon code to the shopping cart.
        /// </summary>
        /// <param name="CouponCode">Coupon Code</param>
        public void AddCouponCode(string CouponCode)
        {
            Coupon = CouponCode;
            CouponApplied = false;
            CouponValid = false;
        }

        /// <summary>
        /// Add Gift Card to the shopping cart.
        /// </summary>
        /// <param name="giftCardNumber">Unique gift card number.</param>        
        public void AddGiftCard(string giftCardNumber)
        {
            string response = string.Empty;
            string invalidGiftCard = string.Format("Invalid gift card: '{0}' ", giftCardNumber);
            string invalidAccountAsspciation = string.Format("Gift card '{0}' is not associated with this account.", giftCardNumber);
            string invalidStoreAssociation = string.Format("Gift card '{0}' is associated with this store.", giftCardNumber);

            if (giftCardNumber.Trim().Length == 0)
            {
                this.IsGiftCardApplied = false;
                response = string.Empty;
                this.GiftCardNumber = string.Empty;
                this.GiftCardAmount = 0;
            }
            else
            {
                GiftCardService giftCardService = new GiftCardService();
                GiftCard giftCard = giftCardService.GetByCardNumber(giftCardNumber);

                int? accountId = this.GetAccountId();

                // Reset previous value.
                this.GiftCardAmount = 0;

                if (giftCard != null)
                {
                    if (accountId != null)
                    {
                        // Check for specific account Gift Card.
                        if (giftCard.AccountId != null && giftCard.AccountId != accountId)
                        {
                            response = invalidAccountAsspciation;
                        }
                        else if (giftCard.PortalId != ZNodeConfigManager.SiteConfig.PortalID)
                        {
                            response = invalidStoreAssociation;
                        }
                        else
                        {
                            decimal availableBalance = Convert.ToDecimal(giftCard.Amount);
                            decimal remainingBalance = 0;
                            if (availableBalance > 0)
                            {
                                // Validate the giftcard expiration date.
                                if (giftCard.ExpirationDate == null || giftCard.ExpirationDate.Value < DateTime.Today.Date)
                                {
                                    IsGiftCardValid = false;
                                    this.IsGiftCardApplied = false;
                                    response = "Gift card expired.";
                                }
                                else
                                {
                                    if (this.Total > availableBalance)
                                    {
                                        // Set all available balance to Gift Card Amount.
                                        this.GiftCardAmount = availableBalance;
                                        remainingBalance = 0;
                                    }
                                    else if (this.Total <= availableBalance)
                                    {
                                        remainingBalance = Convert.ToDecimal(giftCard.Amount) - this.Total;
                                        this.GiftCardAmount = this.Total;
                                    }

                                    response = string.Format("You currently have a balance of {0}{1:0.00}", ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix(), remainingBalance.ToString("c"));

                                    this.GiftCardNumber = giftCardNumber;
                                    this.IsGiftCardValid = true;
                                    this.IsGiftCardApplied = true;
                                }
                            }
                            else
                            {
                                IsGiftCardValid = false;
                                this.IsGiftCardApplied = false;
                                response = "No balance amount in gift card";
                            }
                        }
                    }
                }
                else
                {
                    this.GiftCardNumber = string.Empty;
                    response = invalidGiftCard;
                }
            }

            this.GiftCardMessage = response;
        }

        /// <summary>
        /// Search an item in our shopping cart.
        /// Determines whether the specified item exists
        /// </summary>
        /// <param name="ShoppingCartItem">The Shopping CartI tem to find.</param>        
        /// <returns>The shopping cart item if BOTH the SKU and Addons are found in a the cart. Otherwise returns null</returns>        
        public ZNodeShoppingCartItem Exists(ZNodeShoppingCartItem ShoppingCartItem)
        {
            bool skuFound = false;
            bool addonsFound = false;
            ZNodeProductBaseEntity newProduct = ShoppingCartItem.Product;

            // loop through Shopping cart items
            foreach (ZNodeShoppingCartItem item in _ShoppingCartItems)
            {
                ZNodeProductBaseEntity _product = item.Product;

                skuFound = false;
                addonsFound = false;

                // Ensure that the product exists.
                if (_product.ProductID == newProduct.ProductID)
                {
                    // Check Product has any attributes
                    if (_product.SelectedSKUvalue.SKUID > 0)
                    {
                        // Ensure that the product skuId is same
                        if (_product.SelectedSKUvalue.SKUID == newProduct.SelectedSKUvalue.SKUID)
                        {
                            skuFound = true;
                        }
                    }
                    else
                    {
                        skuFound = true;
                    }

                    // Check Product has any add-ons
                    addonsFound = string.Compare(_product.SelectedAddOns.SelectedAddOnValues, newProduct.SelectedAddOns.SelectedAddOnValues, true) == 0;

                    // Ensure that the addons are matching.
                    addonsFound &= string.Compare(_product.SelectedAddOns.ShoppingCartAddOnsDescription, newProduct.SelectedAddOns.ShoppingCartAddOnsDescription, true) == 0;

                    // Ensure that bundle products are matching.
                    addonsFound &= string.Compare(_product.ShoppingCartDescription, newProduct.ShoppingCartDescription, true) == 0;

                    if (skuFound && addonsFound)
                    {
                        // The user clicked Add on an item that is already in the cart.
                        return item;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Returns Total Qunatity ordered for each Shopping cart Item
        /// </summary>
        /// <param name="ShoppingCartItem">Shopping Cart Item</param>
        /// <returns>Returns the Quantty ordered</returns>
        public int GetQuantityOrdered(ZNodeShoppingCartItem ShoppingCartItem)
        {
            int QuantityOrdered = 0;

            // Loop through Shopping cart items
            foreach (ZNodeShoppingCartItem item in _ShoppingCartItems)
            {
                if (item.Product.ProductID == ShoppingCartItem.Product.ProductID)
                {
                    // Check Product has any attributes
                    if (item.Product.SelectedSKU.SKUID > 0)
                    {
                        if (item.Product.SelectedSKU.SKUID == ShoppingCartItem.Product.SelectedSKU.SKUID)
                        {
                            if (item.GUID != ShoppingCartItem.GUID)
                            {
                                QuantityOrdered += item.Quantity;
                            }
                        }
                    }
                    else
                    {
                        if (item.GUID != ShoppingCartItem.GUID)
                        {
                            QuantityOrdered += item.Quantity;
                        }
                    }
                }
            }

            return QuantityOrdered;
        }

        /// <summary>
        /// Adds a gift card item to the shopping cart
        /// </summary>
        /// <param name="ShoppingCartItem">Shopping Cart Item</param>
        /// <param name="isGiftCard">If product type is gift card then add the item.</param>
        /// <returns>Returns true if inserted else return false</returns>
        public bool AddToCart(ZNodeShoppingCartItem ShoppingCartItem, bool isGiftCard)
        {
            bool isAdded = false;
            try
            {
                if (isGiftCard)
                {
                    // This is a new item added to the cart.
                    _ShoppingCartItems.Add(ShoppingCartItem);
                    isAdded = true;
                }
                else
                {
                    int quantityOrdered = this.GetQuantityOrdered(ShoppingCartItem) + ShoppingCartItem.Quantity;

                    //Zeon Customization : Start

                    /**********************************************************************************************************************
                     * Changes Done on Date : 05/Aug/2014
                     * Purpose :(Do not update quantity)
                     * Description : Add Product as a lineitem is Addtocart from Group Product Attribute grid (ZGroupProductAttributes.ascx)
                     * Znode Default Code : ZNodeShoppingCartItem cartItem = this.Exists(ShoppingCartItem);
                     * ********************************************************************************************************************/

                    ZNodeShoppingCartItem cartItem = ShoppingCartItem.AddProductAsLineItem == false ? this.Exists(ShoppingCartItem) : null;

                    //Zeon Customization : End

                    int maxQty = ShoppingCartItem.Product.MaxQty;
                    int newQuantity = ShoppingCartItem.Quantity;
                    //Perficient Customization to allow Role based order Qty:Starts
                    bool isPrdMaxQtyAllowToUser = Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP") ? true : false;
                    //Perficient Customization to allow Role based order Qty:Ends

                    if (maxQty == 0)
                    {
                        maxQty = 10;
                    }

                    if (cartItem != null)
                    {
                        newQuantity = ShoppingCartItem.Quantity + cartItem.Quantity;
                    }

                    if (isPrdMaxQtyAllowToUser)
                    {
                        if (cartItem != null)
                        {
                            // The user clicked Add on an item that is already in the cart. Just update the quantity                    
                            this.UpdateItemQuantity(cartItem.GUID, newQuantity);
                        }
                        else
                        {
                            // This is a new item added to the cart.
                            _ShoppingCartItems.Add(ShoppingCartItem);
                        }
                        isAdded = true;
                    }

                    if (!isPrdMaxQtyAllowToUser && quantityOrdered <= ShoppingCartItem.Product.QuantityOnHand && newQuantity <= maxQty)
                    {
                        if (cartItem != null)
                        {
                            // The user clicked Add on an item that is already in the cart. Just update the quantity                    
                            this.UpdateItemQuantity(cartItem.GUID, newQuantity);
                        }
                        else
                        {
                            // This is a new item added to the cart.
                            _ShoppingCartItems.Add(ShoppingCartItem);
                        }

                        isAdded = true;
                    }
                    else if (!isPrdMaxQtyAllowToUser && newQuantity <= maxQty && (ShoppingCartItem.Product.AllowBackOrder || !ShoppingCartItem.Product.TrackInventoryInd))
                    {
                        // If AllowBack Order is enabled ,then it will add this product into cart or 
                        // TrackInventory is disabled ,then it will add this product into cart
                        if (cartItem != null)
                        {
                            // The user clicked Add on an item that is already in the cart. Just update the quantity                       
                            this.UpdateItemQuantity(cartItem.GUID, newQuantity);
                        }
                        else
                        {
                            // This is a new item added to the cart.
                            _ShoppingCartItems.Add(ShoppingCartItem);
                        }

                        isAdded = true;
                    }
                }

                if (this.GiftCardNumber != string.Empty)
                {
                    this.AddGiftCard(this.GiftCardNumber);
                }
            }
            catch (Exception ex)
            {
                isAdded = false;
                string strlog = "\nAddToCart Lib Details. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
                strlog = string.Format(strlog, HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.Browser.Browser, HttpContext.Current.Request.Browser.Version, HttpContext.Current.Request.UserAgent, ShoppingCartItem.Product, ShoppingCartItem.Quantity);
                Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);
                Zeon.Libraries.Elmah.ElmahErrorManager.Log(ex);

            }
            return isAdded;
        }

        /// <summary>
        /// Adds an item to the shopping cart
        /// </summary>
        /// <param name="ShoppingCartItem">Shopping Cart Item</param>
        /// <returns>Returns true if inserted else return false</returns>
        public bool AddToCart(ZNodeShoppingCartItem ShoppingCartItem)
        {
            return this.AddToCart(ShoppingCartItem, false);
        }

        /// <summary>
        /// Removes an item from the shopping cart
        /// </summary>
        /// <param name="GUID">GUID of the cart</param>
        public void RemoveFromCart(string GUID)
        {
            // first locate the item in the collection
            ZNodeShoppingCartItem itemToRemove = this.GetItem(GUID);

            if (itemToRemove != null)
            {
                _ShoppingCartItems.Remove(itemToRemove);
            }

            if (this.GiftCardNumber != string.Empty)
            {
                this.AddGiftCard(this.GiftCardNumber);
            }
        }

        /// <summary>
        /// Empty the shopping cart
        /// </summary>
        public void EmptyCart()
        {
            this.AddGiftCard(String.Empty);
            _ShoppingCartItems.Clear();
        }

        /// <summary>
        /// Locates a shopping cart item by it's GUID
        /// </summary>
        /// <param name="GUID">GUID of the item</param>
        /// <returns>Returns the item</returns>
        public ZNodeShoppingCartItem GetItem(string GUID)
        {
            foreach (ZNodeShoppingCartItem item in _ShoppingCartItems)
            {
                if (item.GUID.Equals(GUID))
                {
                    return item;
                }
            }

            return null;
        }

        /// <summary>
        /// Updates a specific shopping cart item
        /// </summary>
        /// <param name="GUID">GUID of the item</param>
        /// <param name="UpdatedQuantity">Updated Quantity</param>
        public void UpdateItemQuantity(string GUID, int UpdatedQuantity)
        {
            // first locate the item in the collection
            ZNodeShoppingCartItem itemToUpdate = this.GetItem(GUID);
            itemToUpdate.Quantity = UpdatedQuantity;
        }


        /// <summary>
        /// Get the account Id.
        /// </summary>
        /// <returns>Returns the account Id</returns>
        private int? GetAccountId()
        {
            return this.GetAccountId(false);
        }

        /// <summary>
        /// Get the account Id.
        /// </summary>
        /// <param name="checkLogin">True to check the login for the current user else false.</param>
        /// <returns>Returns the account Id</returns>
        private int? GetAccountId(bool checkLogin)
        {
            int? accountId = null;

            if (HttpContext.Current.Session["AliasUserAccount"] != null)
            {
                // Order from Order Desk.
                Account account = (Account)HttpContext.Current.Session["AliasUserAccount"];
                if (account != null)
                {
                    // If accout has no login (checkout without registraion) then return the account Id is NULL.
                    if (account.UserID == null && checkLogin)
                    {
                        accountId = null;
                    }
                    else
                    {
                        accountId = Convert.ToInt32(account.AccountID);
                    }
                }
            }
            else
            {
                // Order from catalog side.
                ZNodeUserAccount account = ZNodeUserAccount.CurrentAccount();
                if (account != null)
                {
                    // If accout has no login (checkout without registraion) then return the account Id is NULL.
                    if (account.UserID == null && checkLogin)
                    {
                        accountId = null;
                    }
                    else
                    {
                        accountId = Convert.ToInt32(account.AccountID);
                    }
                }
            }

            return accountId;
        }
        #endregion
    }
}

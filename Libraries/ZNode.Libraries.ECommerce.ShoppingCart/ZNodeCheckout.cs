using System;
using System.Collections.Generic;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Catalog;
using System.Web;
using System.Configuration;

namespace ZNode.Libraries.ECommerce.ShoppingCart
{
    /// <summary>
    /// Order Checkout Class - Orchestrates the checkout process
    /// </summary>
    public partial class ZNodeCheckout : ZNodeBusinessBase
    {
        #region Member Variables
        private ZNodeUserAccount _UserAccount;
        private ZNodeShoppingCart _ShoppingCart;
        private bool _UpdateUserInfo = true;
        private int _PaymentSettingID = 0;
        private string _AdditionalInstructions = string.Empty;
        private string _PurchaseOrderNumber = string.Empty;
        private string _PaymentResponseText = string.Empty;
        private bool _IsSuccess = false;
        private int _PortalId = 0;

        // Worldpay Member Variables
        private string _EchoData = string.Empty;
        private string _IssuerPostData = string.Empty;
        private string _WorldPayHeaderCookie = string.Empty;
        private bool _IsWorldPaySecure = false;
        private string _ECRedirectURL = string.Empty;

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the ZNodeCheckout class.
        /// </summary>
        public ZNodeCheckout()
        {
            this._UserAccount = ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount) as ZNodeUserAccount;
            this._ShoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        }

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value indicating whether it is success or not
        /// </summary>
        public bool IsSuccess
        {
            get { return this._IsSuccess; }
            set { this._IsSuccess = value; }
        }

        /// <summary>
        /// Gets or sets the PaymentResponse Text
        /// </summary>
        public string PaymentResponseText
        {
            get { return this._PaymentResponseText; }
            set { this._PaymentResponseText = value; }
        }

        /// <summary>
        /// Gets or sets the Redirect URL
        /// </summary>
        public string ECRedirectURL
        {
            get { return this._ECRedirectURL; }
            set { this._ECRedirectURL = value; }
        }

        /// <summary>
        /// Gets or sets the user account 
        /// </summary>
        public ZNodeUserAccount UserAccount
        {
            get { return this._UserAccount; }
            set { this._UserAccount = value; }
        }

        /// <summary>
        /// Gets or sets the shopping cart 
        /// </summary>
        public ZNodeShoppingCart ShoppingCart
        {
            get { return this._ShoppingCart; }
            set { this._ShoppingCart = value; }
        }

        /// <summary>
        /// Gets or sets the Customer Additional instructions for this order
        /// </summary>
        public string AdditionalInstructions
        {
            get { return this._AdditionalInstructions; }
            set { this._AdditionalInstructions = value; }
        }

        /// <summary>
        /// Gets or sets the purchase order number applied by customer,
        /// if purchase order payment selected for this order
        /// </summary>
        public string PurchaseOrderNumber
        {
            get { return this._PurchaseOrderNumber; }
            set { this._PurchaseOrderNumber = value; }
        }

        /// <summary>
        /// Gets or sets the Payment setting id associated with the payment type
        /// </summary>
        public int PaymentSettingID
        {
            get { return this._PaymentSettingID; }
            set { this._PaymentSettingID = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to update user info
        /// </summary>
        public bool UpdateUserInfo
        {
            get { return this._UpdateUserInfo; }
            set { this._UpdateUserInfo = value; }
        }

        /// <summary>
        /// Gets or sets the portal id
        /// </summary>
        public int PortalID
        {
            get
            {
                if (this._PortalId != 0)
                {
                    return this._PortalId;
                }
                else
                {
                    return ZNodeConfigManager.SiteConfig.PortalID;
                }
            }

            set
            {
                this._PortalId = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is World pay secure
        /// </summary>
        public bool IsWorldpaySecure
        {
            get { return this._IsWorldPaySecure; }
            set { this._IsWorldPaySecure = value; }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Submits Order
        /// </summary>
        /// <returns>Order object that was submitted</returns>
        public ZNodeOrderFulfillment SubmitOrder()
        {
            List<ZNodeShoppingCartItem> giftCardItems = new List<ZNodeShoppingCartItem>();
            List<ZNodeShoppingCartItem> itemsToRemove = new List<ZNodeShoppingCartItem>();

            ZNode.Libraries.ECommerce.Catalog.ZNodeProduct product = new ZNode.Libraries.ECommerce.Catalog.ZNodeProduct();
            bool isGiftCardInCart = false;

            // loop through cart and add line items
            foreach (ZNodeShoppingCartItem shoppingCartItem in this._ShoppingCart.ShoppingCartItems)
            {
                if (this.IsGiftCard(shoppingCartItem))
                {
                    isGiftCardInCart = true;
                    int giftCardQuantity = shoppingCartItem.Quantity;
                    string giftCardNumber = string.Empty;

                    // Keep the item in removable list.
                    itemsToRemove.Add(shoppingCartItem);

                    for (int qtyIndex = 0; qtyIndex <= giftCardQuantity - 1; qtyIndex++)
                    {
                        // Generate new unique gift card number
                        giftCardNumber = product.GetNextGiftCardNumber();

                        ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                        item.Product = ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase.Create(shoppingCartItem.Product.ProductID);
                        item.Quantity = 1;
                        item.Product.GiftCardNumber = giftCardNumber;
                        item.Product.GiftCardDescription = string.Format("Gift Card Number : {0} <BR/>Expiration Date : {1}", giftCardNumber, product.GetExpirationDate(item.Product.ExpirationPeriod, item.Product.ExpirationFrequency).ToString("MM/dd/yyyy"));
                        giftCardItems.Add(item);
                    }
                }
            }

            // Add the manipulated gift card item.
            foreach (ZNodeShoppingCartItem giftCardItem in giftCardItems)
            {
                this._ShoppingCart.AddToCart(giftCardItem, true);
            }

            // Remove the source gift card item.
            foreach (ZNodeShoppingCartItem item in itemsToRemove)
            {
                this._ShoppingCart.RemoveFromCart(item.GUID);
            }

            // If gift card product added in shopping cart then, discount amount to be recalculated.
            if (isGiftCardInCart)
            {
                this._ShoppingCart.Calculate();
            }

            this._ShoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);

            ZNodeOrderFulfillment order = this.GetOrderFullfillment(this._UserAccount, this._ShoppingCart, this.PortalID);

            // get payment settings (merchant gateway, etc)
            PaymentSettingService pmtSetServ = new PaymentSettingService();
            PaymentSetting pmtSetting = pmtSetServ.GetByPaymentSettingID(this.PaymentSettingID);
            TransactionManager tranManager = null;
            ZNodeOrderState defaultOrderState = (ZNodeOrderState)Enum.Parse(typeof(ZNodeOrderState), ZNodeConfigManager.SiteConfig.DefaultOrderStateID.GetValueOrDefault(10).ToString());

            try
            {
                tranManager = ConnectionScope.CreateTransaction();

                if (this.UpdateUserInfo)
                {
                    // update account & customer
                    this._UserAccount.UpdateUserAccount();
                }

                // create order object
                order.ShippingID = this._ShoppingCart.Shipping.ShippingID;
                order.AdditionalInstructions = this.AdditionalInstructions;
                order.PurchaseOrderNumber = this.PurchaseOrderNumber;

                // Add the order and line items to database
                order.AddOrderToDatabase(defaultOrderState);



                // Add the Gift Card to ZNodeGiftCard if shopping cart has gift card product
                order.AddToGiftCard(this._UserAccount, this._ShoppingCart, order);

                // Submit Payment                
                if (order.Total > 0)
                {
                    // Submit payment to gateway
                    ZNode.Libraries.ECommerce.Payment.ZNodePayment payment = new ZNode.Libraries.ECommerce.Payment.ZNodePayment();
                    if (this._ShoppingCart.Payment != null)
                    {
                        this._ShoppingCart.Payment.BillingAddress = order.BillingAddress;
                        this._ShoppingCart.Payment.ShippingAddress = order.ShippingAddress;
                        payment = (ZNode.Libraries.ECommerce.Payment.ZNodePayment)this._ShoppingCart.Payment;
                    }

                    payment.ShoppingCart = this._ShoppingCart;
                    payment.Order = order;
                    payment.PaymentSetting = pmtSetting;

                    // WorldPay
                    payment.Is3DSecure = this._ShoppingCart.Payment.Is3DSecure;
                    payment.WorldPayEchodata = this._ShoppingCart.Payment.WorldPayEchodata;
                    payment.WorldPayPostData = this._ShoppingCart.Payment.WorldPayPostData;
                    payment.WorldPayHeaderCookie = this._ShoppingCart.Payment.WorldPayHeaderCookie;
                    ZNodePaymentResponse paymentResponse = new ZNodePaymentResponse();
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
                    if (payment.PaymentSetting.GatewayTypeID == (int)GatewayType.WORLDPAY && !payment.Is3DSecure)
                    {
                        log.LogActivityTimerStart();
                        string sessionId = System.DateTime.Now.Ticks.ToString();

                        if (sessionId.Length > 0)
                        {
                            sessionId = sessionId.Substring(sessionId.Length - 6);
                        }

                        this._ShoppingCart.Payment.SessionId = sessionId;

                        paymentResponse = payment.SubmitPayment();
                        if (paymentResponse.IsSuccess && !string.IsNullOrEmpty(paymentResponse.RedirectURL))
                        {
                            this.IsSuccess = paymentResponse.IsSuccess;
                            this._ECRedirectURL = paymentResponse.RedirectURL;
                            this._ShoppingCart.Payment.WorldPayEchodata = paymentResponse.EchoData;
                            this._ShoppingCart.Payment.WorldPayPostData = paymentResponse.IssuerPostData;
                            this._ShoppingCart.Payment.WorldPayHeaderCookie = paymentResponse.WorldPayHeaderCookie;

                            tranManager.Commit();
                        }
                        else if (paymentResponse.IsSuccess)
                        {
                            log.LogActivityTimerEnd(5000, payment.PaymentName);

                            // update transaction id and status
                            order.CardTransactionID = paymentResponse.TransactionId;
                            order.PaymentStatusID = (int)paymentResponse.PaymentStatus; // Payment status
                            order.PaymentTypeId = pmtSetting.PaymentTypeID;
                            order.PaymentSettingID = pmtSetting.PaymentSettingID;
                            // set exp date - required for voids & returns
                            order.CreditCardExp = payment.CreditCard.CreditCardExp;
                            order.Custom3 = "XXXX" + paymentResponse.CardNumber;//Last 4 card Number
                          
                            foreach (ZNodeSubscriptionResponse subscriptionResponse in paymentResponse.RecurringBillingSubscriptionResponse)
                            {
                                if (subscriptionResponse.IsSuccess)
                                {
                                    int index = order.OrderLineItems.FindIndex(delegate(OrderLineItem ordline) { return ordline.OrderLineItemID == subscriptionResponse.ReferenceId || ordline.OrderLineItemID == subscriptionResponse.ParentLineItemId; });

                                    if (subscriptionResponse.ParentLineItemId > 0)
                                    {
                                        int childItemIndex = order.OrderLineItems[index].OrderLineItemCollection.FindIndex(delegate(OrderLineItem ordline) { return ordline.OrderLineItemID == subscriptionResponse.ReferenceId; });

                                        order.OrderLineItems[index].OrderLineItemCollection[childItemIndex].TransactionNumber = subscriptionResponse.SubscriptionId;
                                        order.OrderLineItems[index].OrderLineItemCollection[childItemIndex].IsRecurringBilling = true;
                                    }
                                    else
                                    {
                                        order.OrderLineItems[index].TransactionNumber = subscriptionResponse.SubscriptionId;
                                        order.OrderLineItems[index].IsRecurringBilling = true;
                                    }

                                    // order.PaymentStatusID = null;
                                    order.PaymentStatusID = (int)ZNodePaymentStatus.CREDIT_PENDING;

                                }
                            }

                            order.UpdateOrderStatus(defaultOrderState);

                            order.AddToGiftCardHistory(order);

                            tranManager.Commit();
                        }
                        else
                        {
                            // payment submission failed so rollback transaction
                            log.LogActivityTimerEnd(5001, null, null, null, null, paymentResponse.ResponseText);
                            tranManager.Rollback();
                        }

                        this.PaymentResponseText = paymentResponse.ResponseText;
                        this.IsSuccess = paymentResponse.IsSuccess;
                        return order;
                    }

                    paymentResponse = payment.SubmitPayment();
                    if (paymentResponse.IsSuccess && !((string.IsNullOrEmpty(paymentResponse.TransactionId) && (payment.PaymentSetting.PaymentTypeID == 0 || payment.PaymentSetting.PaymentTypeID == 2))))//Added Additonal Transaction ID null check to solve paypal null transaction issue:2014-11-13
                    {
                        log.LogActivityTimerEnd(5000, payment.PaymentName);

                        // update transaction id and status
                        order.CardTransactionID = paymentResponse.TransactionId;
                        order.PaymentStatusID = (int)paymentResponse.PaymentStatus; // Payment status
                        order.PaymentTypeId = pmtSetting.PaymentTypeID;
                        order.PaymentSettingID = pmtSetting.PaymentSettingID;
                        order.Custom3 = "XXXX" + paymentResponse.CardNumber;
                        // set exp date - required for voids & returns
                        if (payment.CreditCard != null)
                        {
                            if (payment.CreditCard.CreditCardExp.Length == 4)
                            {
                                string MainString = payment.CreditCard.CreditCardExp;
                                string month = MainString.Remove(2, 2);
                                string year = MainString.Remove(0, 2);
                                payment.CreditCard.CreditCardExp = month + "/20" + year;
                            }
                            order.CreditCardExp = payment.CreditCard.CreditCardExp;
                            order.CardAuthCode = paymentResponse.CardAuthorizationCode;
                        }

                        foreach (ZNodeSubscriptionResponse subscriptionResponse in paymentResponse.RecurringBillingSubscriptionResponse)
                        {
                            if (subscriptionResponse.IsSuccess)
                            {
                                int index = order.OrderLineItems.FindIndex(delegate(OrderLineItem ordline) { return ordline.OrderLineItemID == subscriptionResponse.ReferenceId || ordline.OrderLineItemID == subscriptionResponse.ParentLineItemId; });

                                if (subscriptionResponse.ParentLineItemId > 0)
                                {
                                    int childItemIndex = order.OrderLineItems[index].OrderLineItemCollection.FindIndex(delegate(OrderLineItem ordline) { return ordline.OrderLineItemID == subscriptionResponse.ReferenceId; });

                                    order.OrderLineItems[index].OrderLineItemCollection[childItemIndex].TransactionNumber = subscriptionResponse.SubscriptionId;
                                    order.OrderLineItems[index].OrderLineItemCollection[childItemIndex].IsRecurringBilling = true;
                                }
                                else
                                {
                                    order.OrderLineItems[index].TransactionNumber = subscriptionResponse.SubscriptionId;
                                    order.OrderLineItems[index].IsRecurringBilling = true;
                                }

                                //  order.PaymentStatusID = null;
                                order.PaymentStatusID = (int)ZNodePaymentStatus.CREDIT_PENDING;
                            }
                        }

                        order.UpdateOrderStatus(defaultOrderState);

                        // Save the referral commission
                        this.SaveReferralCommission(order);

                        order.AddToGiftCardHistory(order);

                        tranManager.Commit();
                        //Zeon Custom Code:Starts
                        //Saving OrderExtn Details
                        AddOrderExtnToDatabase(order.OrderID, payment.CreditCardList,payment.CreditCard,paymentResponse);
                        SaveOrderLineItemExtn(order);
                        //Zeon Custom Code:Ends
                        //Perficient Custom Code For Multiple Tenders #44:Starts
                        string multiTendetTransactionID = GetMultiCardTransactionList(paymentResponse.AdditionalTenderResponse);
                        order.MultiTendetTransactionIDList = multiTendetTransactionID;
                        //Perficient Custom Code For Multiple Tenders #44:Ends
                    }
                    else
                    {
                        // payment submission failed so rollback transaction
                        string strlog = "\n" + paymentResponse.ResponseText + ". \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
                        strlog = string.Format(strlog, HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.Browser.Browser, HttpContext.Current.Request.Browser.Version, HttpContext.Current.Request.UserAgent, "", "");
                        Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);
                        log.LogActivityTimerEnd(5001, null, null, null, null, paymentResponse.ResponseText);
                        tranManager.Rollback();

                    }

                    this.PaymentResponseText = paymentResponse.ResponseText;
                    this.AdditionalCreditCardError = paymentResponse.MultipleTenderErrText;//Perft Custom Code for Multi Tender Error Message
                    this.IsSuccess = paymentResponse.IsSuccess; 
                    if (string.IsNullOrEmpty(paymentResponse.TransactionId) && (payment.PaymentSetting.PaymentTypeID == 0 || payment.PaymentSetting.PaymentTypeID == 2)) { this.IsSuccess = false; }//Zeon Custom Code:Set Is Successs is False 
                }
                else
                {
                    // Save the referal commission
                    this.SaveReferralCommission(order);

                    order.AddToGiftCardHistory(order);

                    // If order total is $0,then no need to take any further action
                    this._ShoppingCart.Payment.PaymentName = string.Empty; // Reset payment gateway name
                    this.IsSuccess = true;
                    tranManager.Commit();
                }
            }
            catch (ZNodePaymentException)
            {
                tranManager.Rollback();

                // rethrow
                throw;
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);

                if (ex.InnerException != null)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.InnerException.ToString()); // log exception
                }

                tranManager.Rollback();

                // rethrow
                throw;
            }

            return order;
        }

        /// <summary>
        /// This constructor initializes the order and OrderLineItem entity objects.
        /// </summary>
        /// <param name="userAccount">User Account</param>
        /// <param name="shoppingCart">Shopping Cart</param>
        /// <param name="portalId">Portal number</param>
        /// <returns>Get the order fulfillment</returns>
        public ZNodeOrderFulfillment GetOrderFullfillment(ZNodeUserAccount userAccount, ZNodeShoppingCart shoppingCart, int portalId)
        {
            //ZNodeOrderFulfillment order = new ZNodeOrderFulfillment();

            ZNodeOrderFulfillment order = new ZNodeOrderFulfillment(shoppingCart);

            order.PortalId = portalId;
            order.AccountID = userAccount.AccountID;
            order.TaxCost = shoppingCart.TaxCost;
            order.VAT = shoppingCart.VAT;
            order.SalesTax = shoppingCart.SalesTax;
            order.HST = shoppingCart.HST;
            order.PST = shoppingCart.PST;
            order.GST = shoppingCart.GST;
            order.Email = userAccount.EmailID;

            //Zeon Custom Code: Start
            if (HttpContext.Current.Session["BuyerUser"] != null)
            {
                ZNodeUserAccount buyerAccount = (ZNodeUserAccount)HttpContext.Current.Session["BuyerUser"];
                if (buyerAccount != null)
                {
                    order.AccountID = buyerAccount.AccountID;
                    order.Email = buyerAccount.EmailID;
                }
            }
            //Zeon Custom Code: End

            // Affiliate Settings
            ZNodeTracking tracker = new ZNodeTracking();
            string referralAffiliate = tracker.AffiliateId;
            int referralAccountId = 0;

            if (!string.IsNullOrEmpty(referralAffiliate))
            {
                if (int.TryParse(referralAffiliate, out referralAccountId))
                {
                    ZNode.Libraries.DataAccess.Service.AccountService accountServ = new ZNode.Libraries.DataAccess.Service.AccountService();
                    ZNode.Libraries.DataAccess.Entities.Account account = accountServ.GetByAccountID(referralAccountId);

                    // Affiliate account exists
                    if (account != null)
                    {
                        if (account.ReferralStatus == "A")
                        {
                            order.ReferralAccountId = account.AccountID;
                        }
                    }
                }
            }

            // This property will return the Actual ShippingCost - Discount Amount
            //order.ShippingCost = shoppingCart.ShippingCost; // Final Shipping cost//Old Code
            order.SubTotal = shoppingCart.SubTotal;
            //order.Total = shoppingCart.Total;//Old Code
            //Perficient Custom Code:OverrideShipping Cost for User
            order.ShippingCost = shoppingCart.IsShipngChrgsExtended ? shoppingCart.ExtendedShippingCost : shoppingCart.ShippingCost;
            order.Total = shoppingCart.IsShipngChrgsExtended ? (shoppingCart.Total - shoppingCart.ShippingCost) + shoppingCart.ExtendedShippingCost : shoppingCart.Total;
            //Perficient Custom Code:OverrideShipping Cost for User
            order.BillingAddress = userAccount.BillingAddress;
            order.ShippingAddress = userAccount.ShippingAddress;

            // Coupon discount
            order.DiscountAmount = shoppingCart.Discount;

            if (shoppingCart.CouponApplied && shoppingCart.CouponValid)
            {
                order.CouponCode = shoppingCart.Coupon;
            }

            // loop through cart and add line items
            foreach (ZNodeShoppingCartItem shoppingCartItem in shoppingCart.ShoppingCartItems)
            {
                decimal productPrice = shoppingCartItem.UnitPrice - shoppingCartItem.Product.AddOnPrice;

                for (int idx = 0; idx < shoppingCartItem.Product.ZNodeBundleProductCollection.Count; idx++)
                {
                    productPrice -= shoppingCartItem.Product.ZNodeBundleProductCollection[idx].AddOnPrice;
                }

                productPrice = productPrice < 0 ? 0 : productPrice;
                OrderLineItem orderLineItem = new OrderLineItem();
                orderLineItem.Description = shoppingCartItem.Product.ShoppingCartDescription;
                orderLineItem.Name = shoppingCartItem.Product.Name;
                orderLineItem.SKU = shoppingCartItem.Product.SKU;
                orderLineItem.Quantity = shoppingCartItem.Quantity;
                orderLineItem.ProductNum = shoppingCartItem.Product.ProductNum;
                orderLineItem.Price = productPrice;
                orderLineItem.DiscountAmount = shoppingCartItem.Product.DiscountAmount;
                orderLineItem.ShipSeparately = shoppingCartItem.Product.ShipSeparately;
                orderLineItem.ParentOrderLineItemID = null;
                orderLineItem.DownloadLink = shoppingCartItem.Product.DownloadLink;
                orderLineItem.HST = shoppingCartItem.Product.HST;
                orderLineItem.GST = shoppingCartItem.Product.GST;
                orderLineItem.PST = shoppingCartItem.Product.PST;
                orderLineItem.VAT = shoppingCartItem.Product.VAT;
                orderLineItem.SalesTax = shoppingCartItem.Product.SalesTax;
                //Zeon Custom Code To save Product Image:Starts
                orderLineItem.Custom2 = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + shoppingCartItem.Product.ThumbnailImageFilePath.Replace("~", string.Empty);
                //Zeon Custom Code To save Product Image:Ends

                // if product is marked as ship seperately, 
                // then make a shipping cost entry in orderlineItem table.
                if (shoppingCartItem.Product.ShipSeparately)
                {
                    orderLineItem.ShippingCost = shoppingCartItem.Product.ShippingCost;
                }
                // orderLineItem.IsRecurringBilling = shoppingCartItem.Product.RecurringBillingInd;

                if (shoppingCartItem.Product.RecurringBillingInd)
                {
                    orderLineItem.RecurringBillingAmount = shoppingCartItem.Product.RecurringBillingInitialAmount;
                    orderLineItem.RecurringBillingCycles = shoppingCartItem.Product.RecurringBillingTotalCycles;
                    orderLineItem.RecurringBillingFrequency = shoppingCartItem.Product.RecurringBillingFrequency;
                    orderLineItem.RecurringBillingPeriod = shoppingCartItem.Product.RecurringBillingPeriod;
                }
                orderLineItem.Created = DateTime.Now;
                orderLineItem.CreatedBy = HttpContext.Current.User.Identity.Name;

                // Loop through the Selected addons for each Shopping cartItem
                foreach (ZNodeAddOnEntity AddOn in shoppingCartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    // Add-On value collection
                    foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        OrderLineItem childOrderLineItem = new OrderLineItem();

                        childOrderLineItem.Description = AddOnValue.Name;
                        childOrderLineItem.Name = AddOn.Name;
                        childOrderLineItem.SKU = AddOnValue.SKU;
                        childOrderLineItem.Quantity = shoppingCartItem.Quantity;
                        childOrderLineItem.ProductNum = AddOn.Name;
                        childOrderLineItem.Price = AddOnValue.FinalPrice;
                        childOrderLineItem.DiscountAmount = AddOnValue.DiscountAmount;
                        childOrderLineItem.HST = AddOnValue.HST;
                        childOrderLineItem.GST = AddOnValue.GST;
                        childOrderLineItem.PST = AddOnValue.PST;
                        childOrderLineItem.VAT = AddOnValue.VAT;
                        childOrderLineItem.SalesTax = AddOnValue.SalesTax;
                        childOrderLineItem.OrderLineItemRelationshipTypeID = (int)ZNodeOrderLineItemRelationshipType.Addons;
                        //childOrderLineItem.IsRecurringBilling = AddOnValue.RecurringBillingInd;
                        if (AddOnValue.RecurringBillingInd)
                        {
                            childOrderLineItem.RecurringBillingAmount = AddOnValue.RecurringBillingInitialAmount;
                            childOrderLineItem.RecurringBillingCycles = AddOnValue.RecurringBillingTotalCycles;
                            childOrderLineItem.RecurringBillingFrequency = AddOnValue.RecurringBillingFrequency;
                            childOrderLineItem.RecurringBillingPeriod = AddOnValue.RecurringBillingPeriod;
                        }
                        childOrderLineItem.Created = DateTime.Now;
                        childOrderLineItem.CreatedBy = HttpContext.Current.User.Identity.Name;
                        // Set new ParentOrderLineItemId to this item
                        childOrderLineItem.ParentOrderLineItemID = orderLineItem.OrderLineItemID;

                        // Insert Product Addons to the OrderLineItem table
                        orderLineItem.OrderLineItemCollection.Add(childOrderLineItem);
                    }
                }

                foreach (ZNodeProductBaseEntity bundleProduct in shoppingCartItem.Product.ZNodeBundleProductCollection)
                {
                    OrderLineItem bundleOrderLineItem = new OrderLineItem();
                    bundleOrderLineItem.Description = bundleProduct.ShoppingCartDescription;
                    bundleOrderLineItem.Name = bundleProduct.Name;
                    bundleOrderLineItem.SKU = bundleProduct.SKU;
                    bundleOrderLineItem.Quantity = shoppingCartItem.Quantity;
                    bundleOrderLineItem.ProductNum = bundleProduct.ProductNum;

                    // Since child product price included in Bundle parent product price, we set zero for child product price.                    
                    bundleOrderLineItem.Price = 0;
                    bundleOrderLineItem.DiscountAmount = bundleProduct.DiscountAmount;
                    bundleOrderLineItem.ShipSeparately = bundleProduct.ShipSeparately;
                    bundleOrderLineItem.ParentOrderLineItemID = null;
                    bundleOrderLineItem.DownloadLink = bundleProduct.DownloadLink;
                    bundleOrderLineItem.HST = bundleProduct.HST;
                    bundleOrderLineItem.GST = bundleProduct.GST;
                    bundleOrderLineItem.PST = bundleProduct.PST;
                    bundleOrderLineItem.VAT = bundleProduct.VAT;
                    bundleOrderLineItem.SalesTax = bundleProduct.SalesTax;
                    bundleOrderLineItem.ParentOrderLineItemID = orderLineItem.OrderLineItemID;
                    //bundleOrderLineItem.IsRecurringBilling = bundleProduct.RecurringBillingInd;
                    if (bundleProduct.RecurringBillingInd)
                    {
                        bundleOrderLineItem.RecurringBillingAmount = bundleProduct.RecurringBillingInitialAmount;
                        bundleOrderLineItem.RecurringBillingCycles = bundleProduct.RecurringBillingTotalCycles;
                        bundleOrderLineItem.RecurringBillingFrequency = bundleProduct.RecurringBillingFrequency;
                        bundleOrderLineItem.RecurringBillingPeriod = bundleProduct.RecurringBillingPeriod;
                    }
                    bundleOrderLineItem.Created = DateTime.Now;
                    bundleOrderLineItem.CreatedBy = HttpContext.Current.User.Identity.Name;
                    // Set Relation Type, it decides the relation is bundle product
                    bundleOrderLineItem.OrderLineItemRelationshipTypeID = (int)ZNodeOrderLineItemRelationshipType.ProductBundles;

                    // if product is marked as ship seperately, 
                    // then make a shipping cost entry in orderlineItem table.
                    if (bundleProduct.ShipSeparately)
                    {
                        bundleOrderLineItem.ShippingCost = bundleProduct.ShippingCost;
                    }

                    // Loop through the Selected addons for each Shopping cartItem
                    foreach (ZNodeAddOnEntity bundleAddOn in bundleProduct.SelectedAddOns.AddOnCollection)
                    {
                        // Add-On value collection
                        foreach (ZNodeAddOnValueEntity AddOnValue in bundleAddOn.AddOnValueCollection)
                        {
                            OrderLineItem childOrderLineItem = new OrderLineItem();
                            childOrderLineItem.Description = AddOnValue.Name;
                            childOrderLineItem.Name = bundleAddOn.Name;
                            childOrderLineItem.SKU = AddOnValue.SKU;
                            childOrderLineItem.Quantity = shoppingCartItem.Quantity;
                            childOrderLineItem.ProductNum = bundleAddOn.Name;
                            childOrderLineItem.Price = AddOnValue.FinalPrice;
                            childOrderLineItem.DiscountAmount = AddOnValue.DiscountAmount;
                            childOrderLineItem.HST = AddOnValue.HST;
                            childOrderLineItem.GST = AddOnValue.GST;
                            childOrderLineItem.PST = AddOnValue.PST;
                            childOrderLineItem.VAT = AddOnValue.VAT;
                            childOrderLineItem.SalesTax = AddOnValue.SalesTax;
                            // childOrderLineItem.IsRecurringBilling = AddOnValue.RecurringBillingInd;
                            if (AddOnValue.RecurringBillingInd)
                            {
                                childOrderLineItem.RecurringBillingAmount = AddOnValue.RecurringBillingInitialAmount;
                                childOrderLineItem.RecurringBillingCycles = AddOnValue.RecurringBillingTotalCycles;
                                childOrderLineItem.RecurringBillingFrequency = AddOnValue.RecurringBillingFrequency;
                                childOrderLineItem.RecurringBillingPeriod = AddOnValue.RecurringBillingPeriod;
                            }
                            childOrderLineItem.Created = DateTime.Now;
                            childOrderLineItem.CreatedBy = HttpContext.Current.User.Identity.Name;
                            // Set Relation Type, it decides the relation is ADD-ONS
                            childOrderLineItem.OrderLineItemRelationshipTypeID = (int)ZNodeOrderLineItemRelationshipType.Addons;

                            // Set new ParentOrderLineItemId to this item
                            childOrderLineItem.ParentOrderLineItemID = bundleOrderLineItem.OrderLineItemID;

                            // Insert Product Addons to the OrderLineItem table
                            bundleOrderLineItem.OrderLineItemCollection.Add(childOrderLineItem);
                        }
                    }

                    orderLineItem.OrderLineItemCollection.Add(bundleOrderLineItem);
                }

                order.OrderLineItems.Add(orderLineItem);
            }

            return order;
        }


        //Web API
        public ZNodeOrderFulfillment GetOrderFullfillment(ZNodeShoppingCart shoppingCart, int portalId)
        {
            ZNodeOrderFulfillment order = new ZNodeOrderFulfillment(shoppingCart);

            order.PortalId = portalId;
            order.TaxCost = shoppingCart.TaxCost;
            order.VAT = shoppingCart.VAT;
            order.SalesTax = shoppingCart.SalesTax;
            order.HST = shoppingCart.HST;
            order.PST = shoppingCart.PST;
            order.GST = shoppingCart.GST;


            // This property will return the Actual ShippingCost - Discount Amount
            order.ShippingCost = shoppingCart.ShippingCost; // Final Shipping cost
            order.SubTotal = shoppingCart.SubTotal;
            order.Total = shoppingCart.Total;

            // Coupon discount
            order.DiscountAmount = shoppingCart.Discount;

            if (shoppingCart.CouponApplied && shoppingCart.CouponValid)
            {
                order.CouponCode = shoppingCart.Coupon;
            }

            // loop through cart and add line items
            foreach (ZNodeShoppingCartItem shoppingCartItem in shoppingCart.ShoppingCartItems)
            {
                decimal productPrice = shoppingCartItem.UnitPrice - shoppingCartItem.Product.AddOnPrice;

                for (int idx = 0; idx < shoppingCartItem.Product.ZNodeBundleProductCollection.Count; idx++)
                {
                    productPrice -= shoppingCartItem.Product.ZNodeBundleProductCollection[idx].AddOnPrice;
                }

                productPrice = productPrice < 0 ? 0 : productPrice;
                OrderLineItem orderLineItem = new OrderLineItem();
                orderLineItem.Description = shoppingCartItem.Product.ShoppingCartDescription;
                orderLineItem.Name = shoppingCartItem.Product.Name;
                orderLineItem.SKU = shoppingCartItem.Product.SKU;
                orderLineItem.Quantity = shoppingCartItem.Quantity;
                orderLineItem.ProductNum = shoppingCartItem.Product.ProductNum;
                orderLineItem.Price = productPrice;
                orderLineItem.DiscountAmount = shoppingCartItem.Product.DiscountAmount;
                orderLineItem.ShipSeparately = shoppingCartItem.Product.ShipSeparately;
                orderLineItem.ParentOrderLineItemID = null;
                orderLineItem.DownloadLink = shoppingCartItem.Product.DownloadLink;
                orderLineItem.HST = shoppingCartItem.Product.HST;
                orderLineItem.GST = shoppingCartItem.Product.GST;
                orderLineItem.PST = shoppingCartItem.Product.PST;
                orderLineItem.VAT = shoppingCartItem.Product.VAT;
                orderLineItem.SalesTax = shoppingCartItem.Product.SalesTax;

                // if product is marked as ship seperately, 
                // then make a shipping cost entry in orderlineItem table.
                if (shoppingCartItem.Product.ShipSeparately)
                {
                    orderLineItem.ShippingCost = shoppingCartItem.Product.ShippingCost;
                }
                // orderLineItem.IsRecurringBilling = shoppingCartItem.Product.RecurringBillingInd;

                if (shoppingCartItem.Product.RecurringBillingInd)
                {
                    orderLineItem.RecurringBillingAmount = shoppingCartItem.Product.RecurringBillingInitialAmount;
                    orderLineItem.RecurringBillingCycles = shoppingCartItem.Product.RecurringBillingTotalCycles;
                    orderLineItem.RecurringBillingFrequency = shoppingCartItem.Product.RecurringBillingFrequency;
                    orderLineItem.RecurringBillingPeriod = shoppingCartItem.Product.RecurringBillingPeriod;
                }
                orderLineItem.Created = DateTime.Now;
                orderLineItem.CreatedBy = HttpContext.Current.User.Identity.Name;

                // Loop through the Selected addons for each Shopping cartItem
                foreach (ZNodeAddOnEntity AddOn in shoppingCartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    // Add-On value collection
                    foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        OrderLineItem childOrderLineItem = new OrderLineItem();

                        childOrderLineItem.Description = AddOnValue.Name;
                        childOrderLineItem.Name = AddOn.Name;
                        childOrderLineItem.SKU = AddOnValue.SKU;
                        childOrderLineItem.Quantity = shoppingCartItem.Quantity;
                        childOrderLineItem.ProductNum = AddOn.Name;
                        childOrderLineItem.Price = AddOnValue.FinalPrice;
                        childOrderLineItem.DiscountAmount = AddOnValue.DiscountAmount;
                        childOrderLineItem.HST = AddOnValue.HST;
                        childOrderLineItem.GST = AddOnValue.GST;
                        childOrderLineItem.PST = AddOnValue.PST;
                        childOrderLineItem.VAT = AddOnValue.VAT;
                        childOrderLineItem.SalesTax = AddOnValue.SalesTax;
                        childOrderLineItem.OrderLineItemRelationshipTypeID = (int)ZNodeOrderLineItemRelationshipType.Addons;
                        //childOrderLineItem.IsRecurringBilling = AddOnValue.RecurringBillingInd;
                        if (AddOnValue.RecurringBillingInd)
                        {
                            childOrderLineItem.RecurringBillingAmount = AddOnValue.RecurringBillingInitialAmount;
                            childOrderLineItem.RecurringBillingCycles = AddOnValue.RecurringBillingTotalCycles;
                            childOrderLineItem.RecurringBillingFrequency = AddOnValue.RecurringBillingFrequency;
                            childOrderLineItem.RecurringBillingPeriod = AddOnValue.RecurringBillingPeriod;
                        }
                        childOrderLineItem.Created = DateTime.Now;
                        childOrderLineItem.CreatedBy = HttpContext.Current.User.Identity.Name;
                        // Set new ParentOrderLineItemId to this item
                        childOrderLineItem.ParentOrderLineItemID = orderLineItem.OrderLineItemID;

                        // Insert Product Addons to the OrderLineItem table
                        orderLineItem.OrderLineItemCollection.Add(childOrderLineItem);
                    }
                }

                foreach (ZNodeProductBaseEntity bundleProduct in shoppingCartItem.Product.ZNodeBundleProductCollection)
                {
                    OrderLineItem bundleOrderLineItem = new OrderLineItem();
                    bundleOrderLineItem.Description = bundleProduct.ShoppingCartDescription;
                    bundleOrderLineItem.Name = bundleProduct.Name;
                    bundleOrderLineItem.SKU = bundleProduct.SKU;
                    bundleOrderLineItem.Quantity = shoppingCartItem.Quantity;
                    bundleOrderLineItem.ProductNum = bundleProduct.ProductNum;

                    // Since child product price included in Bundle parent product price, we set zero for child product price.                    
                    bundleOrderLineItem.Price = 0;
                    bundleOrderLineItem.DiscountAmount = bundleProduct.DiscountAmount;
                    bundleOrderLineItem.ShipSeparately = bundleProduct.ShipSeparately;
                    bundleOrderLineItem.ParentOrderLineItemID = null;
                    bundleOrderLineItem.DownloadLink = bundleProduct.DownloadLink;
                    bundleOrderLineItem.HST = bundleProduct.HST;
                    bundleOrderLineItem.GST = bundleProduct.GST;
                    bundleOrderLineItem.PST = bundleProduct.PST;
                    bundleOrderLineItem.VAT = bundleProduct.VAT;
                    bundleOrderLineItem.SalesTax = bundleProduct.SalesTax;
                    bundleOrderLineItem.ParentOrderLineItemID = orderLineItem.OrderLineItemID;
                    //bundleOrderLineItem.IsRecurringBilling = bundleProduct.RecurringBillingInd;
                    if (bundleProduct.RecurringBillingInd)
                    {
                        bundleOrderLineItem.RecurringBillingAmount = bundleProduct.RecurringBillingInitialAmount;
                        bundleOrderLineItem.RecurringBillingCycles = bundleProduct.RecurringBillingTotalCycles;
                        bundleOrderLineItem.RecurringBillingFrequency = bundleProduct.RecurringBillingFrequency;
                        bundleOrderLineItem.RecurringBillingPeriod = bundleProduct.RecurringBillingPeriod;
                    }
                    bundleOrderLineItem.Created = DateTime.Now;
                    bundleOrderLineItem.CreatedBy = HttpContext.Current.User.Identity.Name;
                    // Set Relation Type, it decides the relation is bundle product
                    bundleOrderLineItem.OrderLineItemRelationshipTypeID = (int)ZNodeOrderLineItemRelationshipType.ProductBundles;

                    // if product is marked as ship seperately, 
                    // then make a shipping cost entry in orderlineItem table.
                    if (bundleProduct.ShipSeparately)
                    {
                        bundleOrderLineItem.ShippingCost = bundleProduct.ShippingCost;
                    }

                    // Loop through the Selected addons for each Shopping cartItem
                    foreach (ZNodeAddOnEntity bundleAddOn in bundleProduct.SelectedAddOns.AddOnCollection)
                    {
                        // Add-On value collection
                        foreach (ZNodeAddOnValueEntity AddOnValue in bundleAddOn.AddOnValueCollection)
                        {
                            OrderLineItem childOrderLineItem = new OrderLineItem();
                            childOrderLineItem.Description = AddOnValue.Name;
                            childOrderLineItem.Name = bundleAddOn.Name;
                            childOrderLineItem.SKU = AddOnValue.SKU;
                            childOrderLineItem.Quantity = shoppingCartItem.Quantity;
                            childOrderLineItem.ProductNum = bundleAddOn.Name;
                            childOrderLineItem.Price = AddOnValue.FinalPrice;
                            childOrderLineItem.DiscountAmount = AddOnValue.DiscountAmount;
                            childOrderLineItem.HST = AddOnValue.HST;
                            childOrderLineItem.GST = AddOnValue.GST;
                            childOrderLineItem.PST = AddOnValue.PST;
                            childOrderLineItem.VAT = AddOnValue.VAT;
                            childOrderLineItem.SalesTax = AddOnValue.SalesTax;
                            // childOrderLineItem.IsRecurringBilling = AddOnValue.RecurringBillingInd;
                            if (AddOnValue.RecurringBillingInd)
                            {
                                childOrderLineItem.RecurringBillingAmount = AddOnValue.RecurringBillingInitialAmount;
                                childOrderLineItem.RecurringBillingCycles = AddOnValue.RecurringBillingTotalCycles;
                                childOrderLineItem.RecurringBillingFrequency = AddOnValue.RecurringBillingFrequency;
                                childOrderLineItem.RecurringBillingPeriod = AddOnValue.RecurringBillingPeriod;
                            }
                            childOrderLineItem.Created = DateTime.Now;
                            childOrderLineItem.CreatedBy = HttpContext.Current.User.Identity.Name;
                            // Set Relation Type, it decides the relation is ADD-ONS
                            childOrderLineItem.OrderLineItemRelationshipTypeID = (int)ZNodeOrderLineItemRelationshipType.Addons;

                            // Set new ParentOrderLineItemId to this item
                            childOrderLineItem.ParentOrderLineItemID = bundleOrderLineItem.OrderLineItemID;

                            // Insert Product Addons to the OrderLineItem table
                            bundleOrderLineItem.OrderLineItemCollection.Add(childOrderLineItem);
                        }
                    }

                    orderLineItem.OrderLineItemCollection.Add(bundleOrderLineItem);
                }

                order.OrderLineItems.Add(orderLineItem);
            }

            return order;
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Save the refereal commission only if order is placed using affiliate account.
        /// </summary>
        /// <param name="order">ZnodeOrderFulfillment object</param>
        /// <returns>Returns true if success else false.</returns>
        private bool SaveReferralCommission(ZNodeOrderFulfillment order)
        {
            bool isSaved = true;
            if (order.ReferralAccountId != null && order.ReferralAccountId != 0)
            {
                AccountService accountService = new AccountService();
                Account referralAccount = accountService.GetByAccountID(Convert.ToInt32(order.ReferralAccountId));
                if (referralAccount != null)
                {
                    decimal commission = 0;
                    decimal commissionAmount = 0;

                    ReferralCommissionService referralCommissionService = new ReferralCommissionService();
                    ReferralCommission referralCommission = new ReferralCommission();
                    commission = referralAccount.ReferralCommission == null ? 0 : Convert.ToDecimal(referralAccount.ReferralCommission);

                    referralCommission.ReferralCommissionTypeID = referralAccount.ReferralCommissionTypeID == null ? 2 : Convert.ToInt32(referralAccount.ReferralCommissionTypeID);
                    referralCommission.ReferralCommission = commission;
                    referralCommission.ReferralAccountID = order.ReferralAccountId == null ? 0 : Convert.ToInt32(order.ReferralAccountId);
                    referralCommission.OrderID = order.OrderID;
                    referralCommission.TransactionID = order.CardTransactionID;
                    referralCommission.Description = string.Empty;

                    // If referal commission type is percent then covert to amount
                    if (referralCommission.ReferralCommissionTypeID == 1)
                    {
                        commissionAmount = order.Total * commission / 100;
                    }
                    else
                    {
                        commissionAmount = commission;
                    }

                    referralCommission.CommissionAmount = commissionAmount;
                    isSaved = referralCommissionService.Insert(referralCommission);
                }
            }

            return isSaved;
        }

        /// <summary>
        /// Check where the product type is gift card or not.
        /// </summary>
        /// <param name="cartItem">ZNodeShoppingCartItem object.</param>
        /// <returns>Returns true if product type is gift card else false.</returns>
        private bool IsGiftCard(ZNodeShoppingCartItem cartItem)
        {
            bool giftCard = false;
            ProductTypeService productTypeService = new ProductTypeService();
            ProductType productType = productTypeService.GetByProductTypeId(cartItem.Product.ProductTypeID);

            // This product type validation is not perfect.
            if (productType != null && productType.IsGiftCard != null)
            {
                giftCard = Convert.ToBoolean(productType.IsGiftCard);
            }

            return giftCard;
        }
        #endregion
    }
}

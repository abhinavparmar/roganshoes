﻿using System;
using System.Text;
using System.Web;
using System.Xml.Serialization;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Promotions;
using ZNode.Libraries.ECommerce.Shipping;
using ZNode.Libraries.ECommerce.Taxes;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.ShoppingCart
{
    public partial class ZNodeShoppingCart : ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCart
    {

        #region Public Properties

        /// <summary>
        /// Gets or sets the sales tax rate (%)
        /// </summary>
        public decimal AvaTaxRate
        {
            get
            {
                decimal taxedItemSubTotal = 0;
                decimal taxRate = 0;
                decimal salesTax = 0;
                foreach (ZNodeShoppingCartItem item in _ShoppingCartItems)
                {
                    taxedItemSubTotal = taxedItemSubTotal + item.ExtendedPrice;
                    salesTax += item.Product.SalesTax;
                }
                if (taxedItemSubTotal > 0 && salesTax > 0)
                {
                    taxRate = (salesTax * 100) / taxedItemSubTotal;
                }
                return Math.Round(taxRate, 2);
            }

        }

        #endregion
        /// <summary>
        /// Calculates final pricing, shipping in the cart.
        /// </summary>
        public void CalculateShoppingCart()
        {
            // Clear previous error message
            this.ClearPreviousErrorMessges();

            #region[Code Commented::Changes done on date>>24/06/2014 ::Shipping Charge can be calculated on checkout page]
            // ShippingRules
            //ZNodeShippingOption shipping = new ZNodeShippingOption(this);
            //shipping.Calculate();
            #endregion

            // Promotions
            ZNodePromotionOption promotionRules = new ZNodePromotionOption(this);
            promotionRules.Calculate();

            this.GiftCardAmount = 0;

            if (this.GiftCardNumber != string.Empty)
            {
                this.AddGiftCard(this.GiftCardNumber);
            }


        }

        /// <summary>
        /// Calculates final pricing, and taxes in the cart.
        /// </summary>
        public void CalculateExtn()
        {
            // Clear previous error message
            this.ClearPreviousErrorMessges();

            // Promotions
            ZNodePromotionOption promotionRules = new ZNodePromotionOption(this);
            promotionRules.Calculate();

            // TaxRules
            ZNodeTaxOption taxRules = new ZNodeTaxOption(this);
            taxRules.Calculate(this);
            this.GiftCardAmount = 0;

            if (this.GiftCardNumber != string.Empty)
            {
                this.AddGiftCard(this.GiftCardNumber);
            }
        }

        /// <summary>
        /// Clear Previously Calculated Shipping Details
        /// </summary>
        public void ClearExistingShippingDetails()
        {
            // Clear previous error message
            this.ClearPreviousErrorMessges();

            this.OrderLevelShipping = 0;
            this.Shipping.ShippingHandlingCharge = 0;
            this.Shipping.ResponseCode = "0";
            this.Shipping.ResponseMessage = string.Empty;
            this.Shipping.ShippingName = string.Empty;
            this.Shipping.ShippingID = 0;

            foreach (ZNodeShoppingCartItem cartItem in this.ShoppingCartItems)
            {
                // Reset each line item shipping cost
                cartItem.ShippingCost = 0;
                cartItem.Product.ShippingCost = 0;

                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        addOnValue.ShippingCost = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Get Flat Rate Shipping
        /// </summary>
        /// <param name="flatRateCost"></param>
        /// <returns></returns>
        public decimal GetFlatRateShippingCost()
        {
            decimal shippingCost = 0M;
            try
            {
                ZNodeShippingOption shipping = new ZNodeShippingOption(this);
                shipping.Calculate(out shippingCost);
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart!!Method>>GetFlatRateShippingCost!!" + ex.ToString());
            }
            return shippingCost;
        }

        
        /// <summary>
        /// Returns Total Qunatity ordered
        /// </summary>       
        /// <returns>Returns the Quantty ordered</returns>
        public int GetTotalQuantityInCart
        {
            get
            {
                int quantityOrdered = 0;

                // Loop through Shopping cart items
                if (_ShoppingCartItems != null && _ShoppingCartItems.Count > 0)
                {
                    foreach (ZNodeShoppingCartItem item in _ShoppingCartItems)
                    {
                        if (item.IsTierPrice)
                        {
                            quantityOrdered += item.Quantity;
                        }
                    }
                }
                return quantityOrdered;
            }
        }

        /// <summary>
        /// Get Team product is present in cart or not
        /// </summary>
        public bool IsTeamPriceProductCart
        {
            get
            {
                bool isTeamPrice = false;
                // Loop through Shopping cart items
                if (_ShoppingCartItems != null && _ShoppingCartItems.Count > 0)
                {
                    foreach (ZNodeShoppingCartItem item in _ShoppingCartItems)
                    {
                        if (item.IsTierPrice)
                        {
                            return true;
                        }
                    }
                }
                return isTeamPrice;
            }
        }

        /// <summary>
        /// Get or set  Extended shipping charges by admin
        /// </summary>
        public decimal ExtendedShippingCost { get; set; }

        /// <summary>
        /// Get or set  Extended shipping charges by admin
        /// </summary>
        public bool IsShipngChrgsExtended { get; set; }

       
    }
}

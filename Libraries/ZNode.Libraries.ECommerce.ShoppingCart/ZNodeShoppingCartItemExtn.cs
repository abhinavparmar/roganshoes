﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.ShoppingCart
{
    /// <summary>
    /// Represents a product items in the shopping cart
    /// </summary>
    public partial class ZNodeShoppingCartItem : ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCartItem
    {
        # region Private Member
        private string _Notes = string.Empty;
        private decimal _teamPrice = 0;
        private decimal _retailPrice = 0;
        #endregion

        #region Public Properties

        /// <summary>
        /// get or set _Notes values
        /// </summary>
        public string Notes
        {
            get { return _Notes; }
            set { _Notes = value; }
        }

        /// <summary>
        /// Get or set Product As Line Item
        /// </summary>
        public bool AddProductAsLineItem
        {
            get;
            set;
        }

        /// <summary>
        /// Get or set unit price override for admin or CSR
        /// </summary>
        public decimal UnitPriceOverride
        {
            get;
            set;
        }

        /// <summary>
        /// Get or set Actual Unit price
        /// </summary>
        public decimal ActualUnitPrice
        {
            get;
            set;
        }

        /// <summary>
        /// Get or set Show Actual Unit price
        /// </summary>
        public string ShowActualUnitPrice
        {
            get;
            set;
        }

        /// <summary>
        /// Get or set retail price
        /// </summary>
        public decimal ShowRetailPrice
        {
            get { return _retailPrice; }
            set { _retailPrice = value; }
        }

        /// <summary>
        /// Get Team Price
        /// </summary>
        public decimal TeamPrice
        {
            get
            {
                if (this.Product != null && this.Product.ZNodeTieredPriceCollection != null && this.Product.ZNodeTieredPriceCollection.Count > 0)
                {
                    if (this.Product.ZNodeTieredPriceCollection[0].TierStart == 10)
                    {
                        ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption pricing = new ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption();
                        decimal teamPrice = pricing.PromotionalPrice(this.Product, Math.Round(this.Product.ZNodeTieredPriceCollection[0].Price, 2));
                        return GetAdditionalPrice(teamPrice);
                    }
                }
                return 0;
            }
        }

        public decimal GetAdditionalPrice(decimal basePrice)
        {
            if (basePrice > 0)
            {
                // Calculate sales tax on discounted price.
                ZNodeInculsiveTax salesTax = new ZNodeInculsiveTax();
                basePrice = salesTax.GetInclusivePrice(Product.TaxClassID, basePrice);
            }

            basePrice = basePrice + Product.AddOnPrice;
            for (int idx = 0; idx < Product.ZNodeBundleProductCollection.Count; idx++)
            {
                basePrice += Product.ZNodeBundleProductCollection[idx].AddOnPrice;
            }
            return Math.Round(basePrice, 2);
        }

        /// <summary>
        /// Get product tier price
        /// </summary>
        public bool IsTierPrice
        {
            get
            {
                if (this.Product != null && this.Product.ZNodeTieredPriceCollection != null && this.Product.ZNodeTieredPriceCollection.Count > 0)
                {
                    if (this.Product.ZNodeTieredPriceCollection[0].TierStart == 10)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        #endregion
    }
}

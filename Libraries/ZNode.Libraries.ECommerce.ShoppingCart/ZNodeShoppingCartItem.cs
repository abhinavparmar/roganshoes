using System;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using System.Configuration;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.ShoppingCart
{
    /// <summary>
    /// Represents a product items in the shopping cart
    /// </summary>
    [Serializable()]
    public partial class ZNodeShoppingCartItem : ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCartItem
    {
        #region Public Properties
        /// <summary>
        /// Gets or sets the product object for this cart item
        /// </summary>        
        public new ZNodeProductBase Product
        {
            get { return (ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)_Product; }
            set { _Product = value; }
        }

        decimal _unitprice = 0;
        /// <summary>
        /// Gets the unit price of this line item.
        /// </summary>        
        public new decimal UnitPrice
        {
            get
            {
                ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption pricing = new ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption();

                decimal basePrice = pricing.PromotionalPrice(this.Product, TieredPricing);

                if (basePrice > 0)
                {
                    // Calculate sales tax on discounted price.
                    ZNodeInculsiveTax salesTax = new ZNodeInculsiveTax();
                    basePrice = salesTax.GetInclusivePrice(Product.TaxClassID, basePrice);
                }

                basePrice = basePrice + Product.AddOnPrice;
                for (int idx = 0; idx < Product.ZNodeBundleProductCollection.Count; idx++)
                {
                    basePrice += Product.ZNodeBundleProductCollection[idx].AddOnPrice;
                }
                //Zeon Custom code:start

                if (System.Web.Security.Roles.IsUserInRole("ADMIN") || System.Web.Security.Roles.IsUserInRole("CUSTOMER SERVICE REP"))
                {
                    ShowRetailPrice = GetAdditionalPrice(this.Product.FinalPrice);

                    ActualUnitPrice = Math.Round(basePrice, 2);
                    if (UnitPriceOverride != null && UnitPriceOverride > 0)
                    {
                        return Math.Round(UnitPriceOverride, 2);
                    }
                }
                //Zeon Custom Code:End
                return Math.Round(basePrice, 2);
            }
        }

        /// <summary>
        /// Gets or sets the product extended price
        /// </summary>       
        public new decimal ExtendedPrice
        {
            get
            {
                this._ExtendedPrice = this.UnitPrice * Quantity;

                return Math.Round(this._ExtendedPrice, 2);
            }

            set
            {
                this._ExtendedPrice = value;
            }
        }
        #endregion
    }
}


namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Gateway Action
    /// </summary>
    public enum ECGatewayAction
    {
        /// <summary>
        /// Initiate Express checkout transaction
        /// </summary>
        SetExpressCheckoutResponse = 1,

        /// <summary>
        /// Get information from Express checkout transaction
        /// </summary>
        GetExpressCheckoutDetailsResponse = 2,

        /// <summary>
        /// Complete an Express checkout transaction
        /// </summary>
        DoExpressCheckoutPaymentResponse = 3,

        /// <summary>
        /// Issue a refund for a PayPal transaction
        /// </summary>
        RefundTransaction = 4,
    }
}

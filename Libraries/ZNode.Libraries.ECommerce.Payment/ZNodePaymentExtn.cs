﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Zeon.Libraries.Elmah;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Processes different payment types
    /// </summary>
    public partial class ZNodePayment : ZNode.Libraries.ECommerce.Entities.ZNodePayment
    {

        #region Public Properties

        /// <summary>
        /// Gets or sets the multiple payment
        /// </summary>
        public bool IsMultipleCardPayment { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Submits Credit Card Payment 
        /// </summary>
        /// <returns>Returns the Payment response</returns>
        public ZNodePaymentResponse SubmitMultipleCreditCardPayment(CreditCard currCreditCard)
        {
            ZNodePaymentResponse PaymentResponse = new ZNodePaymentResponse();
            // Construct a response object using the gateway response received
            try
            {
                if (NonRecurringItemsTotalAmount > 0)
                {
                    // Transaction Info
                    currCreditCard.OrderID = this.Order.OrderID;
                    currCreditCard.ShippingCharge = Math.Round(this.Order.ShippingCost, 2);
                    currCreditCard.SubTotal = Math.Round(this.Order.SubTotal, 2);
                    currCreditCard.TaxCost = Math.Round(this.Order.TaxCost, 2);
                    // Description
                    foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
                    {
                        CreditCard.Description += cartItem.Product.ShoppingCartDescription;
                    }

                    // Get Gateway Info From Payment Settings
                    GatewayInfo gatewayInfo = new GatewayInfo();
                    ZNodeEncryption decrypt = new ZNodeEncryption();
                    gatewayInfo.GatewayLoginID = decrypt.DecryptData(PaymentSetting.GatewayUsername);
                    gatewayInfo.GatewayPassword = decrypt.DecryptData(PaymentSetting.GatewayPassword);

                    if (PaymentSetting.GatewayTypeID.Value == 6)
                    {
                        // paypal
                        gatewayInfo.TransactionKey = PaymentSetting.TransactionKey;
                    }
                    else
                    {
                        gatewayInfo.TransactionKey = decrypt.DecryptData(PaymentSetting.TransactionKey);
                    }

                    gatewayInfo.Vendor = PaymentSetting.Vendor; // for Verisign Payflow Pro
                    gatewayInfo.Partner = PaymentSetting.Partner;
                    gatewayInfo.TestMode = PaymentSetting.TestMode;
                    gatewayInfo.PreAuthorize = PaymentSetting.PreAuthorize;
                    gatewayInfo.CurrencyCode = this.CurrencyCode;

                    // set gateway type
                    if (PaymentSetting.GatewayTypeID.HasValue && Enum.IsDefined(typeof(GatewayType), PaymentSetting.GatewayTypeID))
                    {
                        // Retrieves the name of the gateway name
                        string gatewayName = Enum.GetName(typeof(GatewayType), PaymentSetting.GatewayTypeID.Value);
                        gatewayInfo.Gateway = (GatewayType)Enum.Parse(typeof(GatewayType), gatewayName);
                    }

                    // set billing address
                    ZNode.Libraries.ECommerce.Entities.Address gatewayBillingAddress = new ZNode.Libraries.ECommerce.Entities.Address();
                    gatewayBillingAddress.City = BillingAddress.City;
                    gatewayBillingAddress.CompanyName = BillingAddress.CompanyName;
                    gatewayBillingAddress.CountryCode = BillingAddress.CountryCode;
                    gatewayBillingAddress.EmailId = this._Order.Email;
                    gatewayBillingAddress.FirstName = BillingAddress.FirstName;
                    gatewayBillingAddress.LastName = BillingAddress.LastName;
                    gatewayBillingAddress.MiddleName = BillingAddress.MiddleName;
                    gatewayBillingAddress.PhoneNumber = BillingAddress.PhoneNumber;
                    gatewayBillingAddress.PostalCode = BillingAddress.PostalCode;
                    gatewayBillingAddress.StateCode = BillingAddress.StateCode;
                    gatewayBillingAddress.Street1 = BillingAddress.Street;
                    gatewayBillingAddress.Street2 = BillingAddress.Street1;

                    // set shipping address
                    ZNode.Libraries.ECommerce.Entities.Address gatewayShippingAddress = new ZNode.Libraries.ECommerce.Entities.Address();
                    gatewayShippingAddress.City = ShippingAddress.City;
                    gatewayShippingAddress.CompanyName = ShippingAddress.CompanyName;
                    gatewayShippingAddress.CountryCode = ShippingAddress.CountryCode;
                    gatewayShippingAddress.EmailId = this._Order.Email;
                    gatewayShippingAddress.FirstName = ShippingAddress.FirstName;
                    gatewayShippingAddress.LastName = ShippingAddress.LastName;
                    gatewayShippingAddress.MiddleName = ShippingAddress.MiddleName;
                    gatewayShippingAddress.PhoneNumber = ShippingAddress.PhoneNumber;
                    gatewayShippingAddress.PostalCode = ShippingAddress.PostalCode;
                    gatewayShippingAddress.StateCode = ShippingAddress.StateCode;
                    gatewayShippingAddress.Street1 = ShippingAddress.Street;
                    gatewayShippingAddress.Street2 = ShippingAddress.Street1;

                    GatewayResponse resp = new GatewayResponse();

                    // Check Selected GatewayType 
                    switch (gatewayInfo.Gateway)
                    {

                        case GatewayType.AUTHORIZE: // if Authorize.Net

                            GatewayAuthorize _SubmitToAuthorizeNet = new GatewayAuthorize();
                            if (UseToken)
                            {
                                throw new NotImplementedException("Authorize Tokens have not been implemented");
                            }
                            else
                            {
                                resp = _SubmitToAuthorizeNet.SubmitPayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, currCreditCard);
                            }

                            break;

                        default:

                            resp.ResponseCode = "Error :";
                            resp.ResponseText = "Unsupported gateway selected. Please select some other gateway option to process your payment.";
                            break;
                    }

                    PaymentResponse.IsSuccess = resp.IsSuccess;
                    PaymentResponse.ResponseCode = resp.ResponseCode;
                    PaymentResponse.ResponseText = resp.ResponseText;
                    PaymentResponse.TransactionId = resp.TransactionId;
                    PaymentResponse.CardAuthorizationCode = resp.CardAuthCode;
                    PaymentResponse.CardType = resp.CardTypeID;
                    PaymentResponse.CardNumber = resp.CardNumber;
                    PaymentResponse.ProfileID = resp.ProfileId;
                    PaymentResponse.PaymentProfileID = resp.PaymentProfileID;
                    PaymentResponse.ShippingAddressID = resp.ShippingAddressID;

                    if (resp.IsSuccess)
                    {
                        if (PaymentSetting.PreAuthorize)
                        {
                            PaymentResponse.PaymentStatus = ZNodePaymentStatus.CREDIT_AUTHORIZED;
                        }
                        else
                        {
                            PaymentResponse.PaymentStatus = ZNodePaymentStatus.CREDIT_CAPTURED;
                        }
                    }
                    else
                    {
                        PaymentResponse.PaymentStatus = ZNodePaymentStatus.CREDIT_DECLINED;
                    }
                }
                else
                {
                    PaymentResponse.IsSuccess = true;
                    PaymentResponse.ResponseCode = "0";
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "making Payment  Failed" + this.Order.OrderID, ex.Message);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("making Payment  Failed" + this.Order.OrderID + ex.Message);
                ElmahErrorManager.Log("making Payment  Failed" + this.Order.OrderID + ex.Message);
            }

            return PaymentResponse;
        }

        /// <summary>
        /// Cancel all Payment previousely made if any one transaction failes
        /// </summary>
        /// <param name="response"></param>
        public void VoidAllPrevSuccessfullPayment(ZNodePaymentResponse response, int failedIndex)
        {
            //Refund Main Card Payment

            ElmahErrorManager.Log("!!Processing Void Transaction if card failed!!OrderId=" + this.Order.OrderID);
            ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "!!Processing Void Transaction if card failed!!OrderId=" + this.Order.OrderID,"Card Count To Void:"+ failedIndex+1);

            bool isMainCardTransVoid = this.VoidPayment(response, CreditCard);
            try
            {
                if (response.AdditionalTenderResponse != null && response.AdditionalTenderResponse.Count > 0)
                {
                    try
                    {
                        for (int index = 0; index < failedIndex; index++)
                        {
                            try
                            {
                                ElmahErrorManager.Log("!!Processing Void Transaction if card failed For Additional Card !!OrderId=" + this.Order.OrderID + "!!Amount=" + this.CreditCardList[index].Amount);
                                bool isSuccess = this.VoidPayment(response.AdditionalTenderResponse[index], this.CreditCardList[index]);
                            }
                            catch (Exception ex)
                            {
                                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Error while making Payment Void" + this.Order.OrderID + ex.Message);
                                ElmahErrorManager.Log("Error while making Payment Void" + this.Order.OrderID + ex.Message);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Error while making Payment Void" + this.Order.OrderID, ex.Message);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Error while making Payment Void" + this.Order.OrderID + ex.Message);
                        ElmahErrorManager.Log("Error while making Payment Void" + this.Order.OrderID + ex.Message);
                    }
                }

            }
            catch (Exception ex)
            {
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Error while making Payment Void" + this.Order.OrderID, ex.Message);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Error while making Payment Void" + this.Order.OrderID + ex.Message);
                ElmahErrorManager.Log("Error while making Payment Void" + this.Order.OrderID + ex.Message);
            }
        }

        /// <summary>
        /// Submit mutiple tenders data
        /// </summary>
        /// <param name="response">ZNodePaymentResponse</param>
        private void ProcessPaymentForMultiTender(ZNodePaymentResponse response)
        {
            try
            {
                int paymentIndex = 0;
                foreach (CreditCard additionalTender in this.CreditCardList)
                {
                    ZNodePaymentResponse cardResponse = new ZNodePaymentResponse();
                    cardResponse = SubmitMultipleCreditCardPayment(additionalTender);
                    if (cardResponse.IsSuccess)
                    {
                        if (response.AdditionalTenderResponse == null)
                        {
                            response.AdditionalTenderResponse = new List<ZNodePaymentResponse>();
                        }

                        response.AdditionalTenderResponse.Add(cardResponse);
                        paymentIndex++;
                    }
                    else
                    {
                        //Refund main Payment
                        VoidAllPrevSuccessfullPayment(response, paymentIndex);
                        string strErrCardNum = new String('X', additionalTender.CardNumber.Length - 4) + additionalTender.CardNumber.Substring(additionalTender.CardNumber.Length-4,4);
                        response.IsSuccess = false;
                        response.ResponseText = cardResponse.ResponseText;
                        response.MultipleTenderErrText = strErrCardNum;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Error In Proceesing Additional Tenders" + this.Order.OrderID, ex.Message);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Error In Proceesing Additional Tenders" + this.Order.OrderID + ex.Message);
                ElmahErrorManager.Log("Error In Proceesing Additional Tenders" + this.Order.OrderID + ex.Message);

            }
        }


        #endregion

        #region Private Methods
        /// <summary>
        /// Make All previouse payment Void
        /// </summary>
        /// <param name="currResp"></param>
        /// <param name="currCard"></param>
        /// <returns>bool</returns>
        private bool VoidPayment(ZNodePaymentResponse currResp, CreditCard currCard)
        {
            bool isVoid = false;
            try
            {
                // Get Gateway Info From Payment Settings
                GatewayInfo gatewayInfo = new GatewayInfo();
                ZNodeEncryption decrypt = new ZNodeEncryption();
                gatewayInfo.GatewayLoginID = decrypt.DecryptData(PaymentSetting.GatewayUsername);
                gatewayInfo.GatewayPassword = decrypt.DecryptData(PaymentSetting.GatewayPassword);

                if (PaymentSetting.GatewayTypeID.Value == 6)
                {
                    // paypal
                    gatewayInfo.TransactionKey = PaymentSetting.TransactionKey;
                }
                else
                {
                    gatewayInfo.TransactionKey = decrypt.DecryptData(PaymentSetting.TransactionKey);
                }

                gatewayInfo.Vendor = PaymentSetting.Vendor; // for Verisign Payflow Pro
                gatewayInfo.Partner = PaymentSetting.Partner;
                gatewayInfo.TestMode = PaymentSetting.TestMode;
                gatewayInfo.PreAuthorize = PaymentSetting.PreAuthorize;
                gatewayInfo.CurrencyCode = this.CurrencyCode;
                // Set credit card
                CreditCard cc = new CreditCard();
                cc.Amount = currCard.Amount;
                cc.CardNumber = currCard.CardNumber;
                cc.CreditCardExp = currCard.CreditCardExp;
                cc.OrderID = currCard.OrderID;
                cc.TransactionID = currResp.TransactionId;
                cc.ProcTxnId = currResp.TransactionId;
                GatewayResponse resp = new GatewayResponse();
                GatewayAuthorize auth = new GatewayAuthorize();
                resp = auth.VoidPayment(gatewayInfo, cc);
                if (resp.IsSuccess)
                {
                    isVoid = true;
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "!!!Making Payment Void Success!!!" + resp.ResponseText.ToString() + "Order ID" + cc.OrderID + "Amount" + cc.Amount, cc.OrderID.ToString());
                    ElmahErrorManager.Log("making Payment Void Success" + resp.ResponseText.ToString() + "Order ID" + cc.OrderID + "amt" + cc.Amount);
                }
                else
                {
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "!!!Making Payment Void Failed!!!" + "Order ID" + cc.OrderID + "Amount" + cc.Amount, cc.OrderID.ToString());
                    ElmahErrorManager.Log("!!!making Payment Void Failed!!!" + resp.ResponseText.ToString() + "Order ID" + cc.OrderID + "Amount" + cc.Amount);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("!!!making Payment Void Failed!!!" + resp.ResponseText.ToString() + "Order ID" + cc.OrderID + "Amount" + cc.Amount);
                }

            }
            catch (Exception ex)
            {
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "!!!Error while making Payment Void!!!" + ex.Message.ToString() + "Order ID" + currCard.OrderID + "Amount" + currCard.Amount, currCard.OrderID.ToString());
                ElmahErrorManager.Log("!!!Error while making Payment Void!!!" + ex.ToString() + "Order ID" + currCard.OrderID + "Amount" + currCard.Amount);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("!!!Error while making Payment Void!!!" + ex.ToString() + "Order ID" + currCard.OrderID + "Amount" + currCard.Amount);
            }
            return isVoid;
        }
        #endregion
    }
}

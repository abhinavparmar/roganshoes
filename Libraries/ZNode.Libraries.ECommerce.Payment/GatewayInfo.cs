using System;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Gateway Information    
    /// </summary>    
    public class GatewayInfo : ZNodePaymentBase
    {
        #region Member Variables
        private string _PaRequestURL = string.Empty;               
        private GatewayType _Gateway = new GatewayType();
        private string _LoginID = string.Empty;
        private string _GatewayPwd = String.Empty;
        private string _GatewayURL = String.Empty;
        private bool _Testmode = false;
        private string _TransactionKey = String.Empty;        
        private string _TransactionType = string.Empty;
        private string _TenderType = string.Empty;
        private string _Vendor = string.Empty;
        private string _Partner = string.Empty;
        private string _PaymentActionCodeType = "Sale";
        private bool _PreAuthorize = false;
        private string _CustomerIPAddress = string.Empty;

        /// <summary>
        /// Private variable to hold currency code
        /// </summary>
        private string _CurrencyCode = "USD";

        // WorldPay Member Variables
        private string _BrowserUserAgentHeader = string.Empty;
        private string _BrowserAcceptHeader = string.Empty;
        private string _WorldPayEchoData = string.Empty;
        private string _WorldPayIssuerResponse = string.Empty;
        private string _HeaderCookies = string.Empty;
        private bool _is3DSecure = false;
        #endregion

        #region Public Instance properties
        /// <summary>
        /// Gets or sets the PaRequest URL
        /// </summary>
        public string PaRequestURL
        {
            get { return this._PaRequestURL; }
            set { this._PaRequestURL = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway type object.
        /// </summary>
        public GatewayType Gateway
        {
            get { return this._Gateway; }
            set { this._Gateway = value; }
        }

        /// <summary>
        /// Gets or sets the Merchant Gatewayid property
        /// </summary>
        public string GatewayLoginID
        {
            get { return this._LoginID; }
            set { this._LoginID = value; }
        }

        /// <summary>
        /// Gets or sets the Mercahnt Gateway password property
        /// </summary>
        public string GatewayPassword
        {
            get { return this._GatewayPwd; }
            set { this._GatewayPwd = value; }
        }

        /// <summary>
        /// Gets or sets the gateway specific URL. May not be needed for all gateways.
        /// </summary>
        public string GatewayURL
        {
            get { return this._GatewayURL; }
            set { this._GatewayURL = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the gateway into Test Mode. Not supported by all gateways.
        /// </summary>
        public bool TestMode
        {
            get { return this._Testmode; }
            set { this._Testmode = value; }
        }

        /// <summary>
        /// Gets or sets the gateway into TransactionType.
        /// </summary>
        public string TransactionType
        {
            get { return this._TransactionType; }
            set { this._TransactionType = value; }
        }

        /// <summary>
        /// Gets or sets the gateway into TransactionType.
        /// </summary>
        public string TenderType
        {
            get { return this._TenderType; }
            set { this._TenderType = value; }
        }

        /// <summary>
        /// Gets or sets the transaction key
        /// </summary>
        public string TransactionKey
        {
            get { return this._TransactionKey; }
            set { this._TransactionKey = value; }
        }       

        /// <summary>
        /// Gets or sets the Currency code type (like USD,USS,UZS,..,etc) 
        /// Default currency code type is 'USD'
        /// </summary>
        public string CurrencyCode
        {
            get { return this._CurrencyCode; }
            set { this._CurrencyCode = value; }
        }

        /// <summary>
        /// Gets or sets the Payment Action Code type
        /// </summary>
        public string PaymentActionCodeType
        {
            get { return this._PaymentActionCodeType; }
            set { this._PaymentActionCodeType = value; }
        }

        /// <summary>
        /// Gets or sets the verisign payflow pro vendor property
        /// </summary>
        public string Vendor
        {
            get { return this._Vendor; }
            set { this._Vendor = value; }
        }

        /// <summary>
        /// Gets or sets the verisign payflow pro partner property
        /// </summary>
        public string Partner
        {
            get { return this._Partner; }
            set { this._Partner = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to pre authorize or not
        /// </summary>
        public bool PreAuthorize
        {
            get { return this._PreAuthorize; }
            set { this._PreAuthorize = value; }
        }

        /// <summary>
        /// Gets the IP address of the remote client
        /// </summary>
        public string CustomerIPAddress
        {
            get 
            {
                if (this._CustomerIPAddress.Length == 0)
                {
                    // set customer's IP address of the remote client
                    this._CustomerIPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                }

                return this._CustomerIPAddress;
            }            
        }

        /// <summary>
        /// Gets or sets the worldpay request header property
        /// </summary>
        public string BrowserUserAgentHeader
        {
            get { return this._BrowserUserAgentHeader; }
            set { this._BrowserUserAgentHeader = value; }
        }

        /// <summary>
        /// Gets or sets the worldpay browser header property
        /// </summary>
        public string BrowserAcceptHeader
        {
            get { return this._BrowserAcceptHeader; }
            set { this._BrowserAcceptHeader = value; }
        }

        /// <summary>
        /// Gets or sets the IssuerUrl from worldpay response 
        /// </summary>
        public string WorldPayIssuerResponse
        {
            get { return this._WorldPayIssuerResponse; }
            set { this._WorldPayIssuerResponse = value; }
        }

        /// <summary>
        /// Gets or sets the Echo Data from worldpay response
        /// </summary>
        public string WorldPayEchoData
        {
            get { return this._WorldPayEchoData; }
            set { this._WorldPayEchoData = value; }
        }

        /// <summary>
        /// Gets or sets To hold the WorldPay Header Cookies
        /// </summary>
        public string HeaderCookies
        {
            get { return this._HeaderCookies; }
            set { this._HeaderCookies = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether To check the Secure Gateway 
        /// </summary>
        public bool Is3DSecure
        {
            get { return this._is3DSecure; }
            set { this._is3DSecure = value; }
        }
        #endregion
    }
}

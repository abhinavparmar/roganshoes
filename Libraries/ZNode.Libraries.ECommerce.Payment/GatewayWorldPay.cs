using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// WorldPay Gateway
    /// </summary>
    public class GatewayWorldPay : ZNodePaymentBase
    {
        private string payRequest = string.Empty;
        private string echoData = string.Empty;

        #region Public, Private and Protected Methods
        /// <summary>
        /// Submit payment to WorldPay Gateway
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse Submit3DSecurePayment(GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard)
        {
            return this.SubmitToWorldPay(true, Gateway, BillingAddress, ShippingAddress, CreditCard);
        }

        /// <summary>
        /// Submit payment to WorldPay Gateway
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitPayment(GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard)
        {
            return this.SubmitToWorldPay(false, Gateway, BillingAddress, ShippingAddress, CreditCard);
        }

        /// <summary>
        /// Creates the XML request
        /// </summary>
        /// <param name="Is3DSecure">3D secure or not</param>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        protected string CreateXMLRequestData(bool Is3DSecure, GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard)
        {
            StringBuilder xmlRequestData = new StringBuilder();
            string cardType = string.Empty;
            string UserName = Gateway.GatewayLoginID;
            string xmlPassword = Gateway.GatewayPassword;
            string transactionID = CreditCard.TransactionID;
            string sessionID = CreditCard.TransactionID;

            // Get Card Type for the PaymentDetails
            switch (CreditCard.CreditCardNumberType)
            {
                case CreditCardType.Amex:
                    cardType = "AMEX-SSL";
                    break;
                case CreditCardType.Diners:
                    cardType = "DINERS-SSL";
                    break;
                case CreditCardType.JCB:
                    cardType = "JCB-SSL";
                    break;
                case CreditCardType.MasterCard:
                    cardType = "ECMC-SSL";
                    break;
                case CreditCardType.Solo:
                    cardType = "SOLO_GB-SSL";
                    break;
                case CreditCardType.Maestro:
                    cardType = "MAESTRO-SSL";
                    break;
                case CreditCardType.Visa:
                    cardType = "VISA-SSL";
                    break;
                case CreditCardType.Discover:
                    cardType = "DISCOVER-SSL";
                    break;
            }

            // Xml version
            xmlRequestData.Append("<?xml version=\"1.0\"?>");
            xmlRequestData.Append(System.Environment.NewLine);

            // DTD Type Details
            xmlRequestData.Append("<!DOCTYPE paymentService PUBLIC \"-//WorldPay/DTD WorldPay PaymentService v1//EN\" \"http://dtd.wp3.rbsworldPay.com/paymentService_v1.dtd\">");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<paymentService version=\"1.4\" merchantCode=\"");
            xmlRequestData.Append(UserName);
            xmlRequestData.Append("\">");
            xmlRequestData.Append(System.Environment.NewLine);

            // CartId is nothing but Our OrderId and InstallionId - given by Worldpay for us
            xmlRequestData.Append("<submit>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<order orderCode=\"");
            xmlRequestData.Append(transactionID);
            xmlRequestData.Append("\" installationId=\"");
            xmlRequestData.Append(Gateway.TransactionKey);
            xmlRequestData.Append("\">");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<description>Your ShoppingCart Details</description>");

            // Amount and Currency Code details
            xmlRequestData.Append("<amount value=\"");
            xmlRequestData.Append(Convert.ToString(Math.Round((CreditCard.Amount * 100), 0)));
            xmlRequestData.Append("\" currencyCode=\"");
            xmlRequestData.Append(Gateway.CurrencyCode);
            xmlRequestData.Append("\" exponent=\"2\"/>");
            xmlRequestData.Append(System.Environment.NewLine);

            // Order Content - Optional
            xmlRequestData.Append("<orderContent>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<![CDATA[");
            xmlRequestData.Append("<center><table><tr><td bgcolor=\"#c0c0c0\">Your Internet Order:</td><td colspan=\"2\" bgcolor=\"#c0c0c0\" align=\"right\">" + transactionID + "</td></tr>");
            xmlRequestData.Append("<tr><td bgcolor=\"#c0c0c0\">Description:</td><td>Your Shopping Cart Details</td></tr>");
            xmlRequestData.Append("<tr><td colspan=\"2\">Subtotal:</td><td align=\"right\">" + CreditCard.SubTotal.ToString("N2") + "</td></tr>");
            xmlRequestData.Append("<tr><td colspan=\"2\">Tax Cost :</td><td align=\"right\">" + CreditCard.TaxCost.ToString("N2") + "</td></tr>");
            xmlRequestData.Append("<tr><td colspan=\"2\">Shipping Cost:</td><td align=\"right\">" + CreditCard.ShippingCharge.ToString("N2") + "</td></tr>");
            xmlRequestData.Append("<tr><td colspan=\"2\" bgcolor=\"#c0c0c0\">Total cost:</td><td bgcolor=\"#c0c0c0\" align=\"right\">" + Gateway.CurrencyCode + " " + CreditCard.Amount + "</td></tr>");
            xmlRequestData.Append("<tr><td colspan=\"3\">&nbsp;</td></tr>");
            xmlRequestData.Append("<tr><td bgcolor=\"#c0c0c0\" colspan=\"3\">Your billing address:</td></tr>"); // Billing Address
            xmlRequestData.Append("<tr><td colspan=\"3\">" + BillingAddress.FirstName.Substring(0, 1) + "." + BillingAddress.LastName + "<br>");
            xmlRequestData.Append(BillingAddress.Street1 + BillingAddress.Street2 + "<br>" + BillingAddress.City + "<br>" + BillingAddress.PostalCode + "<br>");
            xmlRequestData.Append(BillingAddress.CountryCode + "</td></tr>");
            xmlRequestData.Append("<tr><td colspan=\"3\">&nbsp;</td></tr>");
            xmlRequestData.Append("<tr><td bgcolor=\"#c0c0c0\" colspan=\"3\">Your Shipping address:</td></tr>"); // Shipping Address
            xmlRequestData.Append("<tr><td colspan=\"3\">" + ShippingAddress.FirstName.Substring(0, 1) + "." + ShippingAddress.LastName + "<br>");
            xmlRequestData.Append(ShippingAddress.Street1 + ShippingAddress.Street2 + "<br>" + ShippingAddress.City + "<br>" + ShippingAddress.PostalCode + "<br>");
            xmlRequestData.Append(ShippingAddress.CountryCode + "</td></tr>");
            xmlRequestData.Append("<tr><td colspan=\"3\">&nbsp;</td></tr>");
            xmlRequestData.Append("<tr><td bgcolor=\"#c0c0c0\" colspan=\"3\">Billing notice:</td></tr>");
            xmlRequestData.Append("<tr><td colspan=\"3\">Your payment will be handled by WorldPay<br>This name may ");
            xmlRequestData.Append("appear on your bank statement<br>http://www.worldpay.com</td></tr>");
            xmlRequestData.Append("</table></center>");
            xmlRequestData.Append("]]>");
            xmlRequestData.Append("</orderContent>");
            xmlRequestData.Append(System.Environment.NewLine);

            // Payment Details
            xmlRequestData.Append("<paymentDetails>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<" + cardType + ">");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<cardNumber>" + CreditCard.CardNumber + "</cardNumber>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<expiryDate>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<date month=\"");
            xmlRequestData.Append(CreditCard.CreditCardExp.Substring(0, 2));
            xmlRequestData.Append("\" year=\"");
            xmlRequestData.Append(CreditCard.CreditCardExp.Substring(3, 4));
            xmlRequestData.Append("\"/>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("</expiryDate>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<cardHolderName>" + CreditCard.CardHolderName + "</cardHolderName>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<cvc>" + CreditCard.CardSecurityCode + "</cvc>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("</" + cardType + ">");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<session shopperIPAddress=\"" + Gateway.CustomerIPAddress + "\" id=\"" + sessionID + "\"/>");

            if (Is3DSecure)
            {
                xmlRequestData.Append("<info3DSecure>");
                xmlRequestData.Append("<paResponse>" + Gateway.WorldPayIssuerResponse + "</paResponse>");
                xmlRequestData.Append("</info3DSecure>");
            }

            xmlRequestData.Append("</paymentDetails>");

            // Shopper EmailID
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<shopper>");

            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<browser>");
            xmlRequestData.Append("<acceptHeader>" + Gateway.BrowserAcceptHeader + "</acceptHeader>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<userAgentHeader>" + Gateway.BrowserUserAgentHeader + "</userAgentHeader>");
            xmlRequestData.Append("</browser>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("</shopper>");
            xmlRequestData.Append(System.Environment.NewLine);

            // Shipping Address
            xmlRequestData.Append("<shippingAddress>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<address>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<firstName>" + ShippingAddress.FirstName + "</firstName>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<lastName>" + ShippingAddress.LastName + "</lastName>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<street>" + ShippingAddress.Street1 + ShippingAddress.Street2 + "</street>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<postalCode>" + ShippingAddress.PostalCode + "</postalCode>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<countryCode>" + ShippingAddress.CountryCode + "</countryCode>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("<telephoneNumber>" + ShippingAddress.PhoneNumber + "</telephoneNumber>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("</address>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("</shippingAddress> ");

            if (Is3DSecure)
            {
                xmlRequestData.Append("<echoData>" + Gateway.WorldPayEchoData + "</echoData>");
            }

            xmlRequestData.Append("</order>");

            // root tags closed here.
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("</submit>");
            xmlRequestData.Append(System.Environment.NewLine);
            xmlRequestData.Append("</paymentService>");

            return xmlRequestData.ToString();
        }

        /// <summary>
        /// Submit to worrld pay
        /// </summary>
        /// <param name="Is3DSecure">3D secure or not</param>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        private GatewayResponse SubmitToWorldPay(bool Is3DSecure, GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard)
        {
            GatewayResponse paymentGatewayResponse = new GatewayResponse();
            StringBuilder xmlRequestData = new StringBuilder();

            // Local Variables
            string postURL = string.Empty;
            string IssuerpostURL = string.Empty;
            string UserName = Gateway.GatewayLoginID;
            string xmlPassword = Gateway.GatewayPassword;
            string inputXmlData = string.Empty;

            // To Check Test Mode.
            if (Gateway.TestMode)
            {
                postURL = "https://secure-test.wp3.rbsworldpay.com/jsp/merchant/xml/paymentService.jsp";
            }
            else
            {
                postURL = "https://secure.wp3.rbsworldpay.com/jsp/merchant/xml/paymentService.jsp";
            }

            // Creating XML Request Data
            inputXmlData = this.CreateXMLRequestData(Is3DSecure, Gateway, BillingAddress, ShippingAddress, CreditCard);

            try
            {
                // WebRequest and WebResponse
                // Credientials
                System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
                NetworkCredential netCred = new NetworkCredential(UserName, xmlPassword);
                myCredentials.Add(new Uri(postURL), "Basic", netCred);

                // WebRequest
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(postURL);
                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml";
                webRequest.ContentLength = inputXmlData.Length;
                webRequest.Credentials = myCredentials;

                if (Is3DSecure)
                {
                    // Set cookie
                    webRequest.Headers.Set("Cookie", Gateway.HeaderCookies);
                }

                // GetRequest
                StreamWriter myWriter = new StreamWriter(webRequest.GetRequestStream());
                myWriter.Write(inputXmlData);
                myWriter.Close();

                // WebResponse
                WebResponse webResponse = webRequest.GetResponse();

                paymentGatewayResponse.WorldPayHeaderCookie = webResponse.Headers["Set-Cookie"].ToString();

                // Read from XML
                // Get the stream associated with the response.
                Stream receiveStream = webResponse.GetResponseStream();

                // Stream reader with the required encoding format.
                StreamReader StreamInput = new StreamReader(receiveStream, Encoding.UTF8);
                XmlTextReader xmlReader = new XmlTextReader(StreamInput);
                string xmlResponse = StreamInput.ReadToEnd();

                // Read from XML
                while (xmlReader.Read())
                {
                    if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "reply")
                    {
                        while (xmlReader.Read())
                        {
                            if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "orderStatus")
                            {
                                paymentGatewayResponse.TransactionId = xmlReader.GetAttribute("orderCode");
                            }
                            else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "requestInfo")
                            {
                                // For 3D Secure
                                while (xmlReader.Read())
                                {
                                    if (xmlReader.Name == "paRequest")
                                    {
                                        this.payRequest = xmlReader.ReadString();
                                    }
                                    else if (xmlReader.Name == "issuerURL")
                                    {
                                        IssuerpostURL = xmlReader.ReadString();
                                    }
                                    else if (xmlReader.Name == "echoData")
                                    {
                                        this.echoData = xmlReader.ReadString();
                                    }
                                }

                                if (!string.IsNullOrEmpty(IssuerpostURL))
                                {
                                    paymentGatewayResponse.ECRedirectURL = IssuerpostURL + "?PaReq=" + this.payRequest + "&TermUrl=" + Gateway.PaRequestURL + "&MD=MD";
                                }
                                else
                                {
                                    paymentGatewayResponse.ECRedirectURL = null;
                                }

                                paymentGatewayResponse.IssuerPostData = this.payRequest;
                                paymentGatewayResponse.EchoData = this.echoData;
                                paymentGatewayResponse.ResponseCode = "0";
                            }
                            else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "payment")
                            {
                                while (xmlReader.Read())
                                {
                                    switch (xmlReader.NodeType)
                                    {
                                        case XmlNodeType.Element:
                                            switch (xmlReader.Name)
                                            {
                                                case "lastEvent":
                                                    goto case "Response";
                                                case "CVCResultCode":
                                                    goto case "Response";
                                                case "AVSResultCode":
                                                    goto case "Response";
                                                case "ISO8583ReturnCode":
                                                    goto case "Response";
                                                case "Response":
                                                    if (xmlReader.Name == "lastEvent")
                                                    {
                                                        paymentGatewayResponse.ResponseText = xmlReader.ReadString();
                                                        if (paymentGatewayResponse.ResponseText == "AUTHORISED")
                                                        {
                                                            paymentGatewayResponse.ResponseCode = "0";
                                                        }
                                                    }
                                                    else if (xmlReader.Name == "CVCResultCode")
                                                    {
                                                        paymentGatewayResponse.ApprovalCode = xmlReader.GetAttribute("description");
                                                    }
                                                    else if (xmlReader.Name == "AVSResultCode")
                                                    {
                                                        paymentGatewayResponse.AVSResponseCode = xmlReader.GetAttribute("description");
                                                    }
                                                    else if (xmlReader.Name == "ISO8583ReturnCode")
                                                    {
                                                        paymentGatewayResponse.ResponseCode = xmlReader.GetAttribute("code");
                                                        paymentGatewayResponse.ResponseText += xmlReader.GetAttribute("description");
                                                    }
                                                    else if (xmlReader.Name == "issuerURL")
                                                    {
                                                        paymentGatewayResponse.ECRedirectURL = string.Empty;
                                                    }

                                                    break;
                                            }

                                            break;
                                    }
                                }
                            }
                            else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "error")
                            {
                                paymentGatewayResponse.ResponseCode = xmlReader.GetAttribute("code");

                                while (xmlReader.Read())
                                {
                                    if (xmlReader.NodeType == XmlNodeType.CDATA)
                                    {
                                        paymentGatewayResponse.ResponseText += xmlReader.Value;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                // Response Code
                if (paymentGatewayResponse.ResponseCode == "0")
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.CardAuthCode = paymentGatewayResponse.TransactionId;
                }
                else
                {
                    paymentGatewayResponse.IsSuccess = false;
                }

                return paymentGatewayResponse;
            }
            catch (Exception Ex)
            {
                paymentGatewayResponse.ResponseText = Ex.Message.ToString(); // Response Error code Description
                return paymentGatewayResponse;
            }
        }


        /// <summary>
        /// Submit the recurring billing subscription
        /// </summary>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <param name="CreditCardInfo">Credit Card Info</param>
        /// <param name="RecurringBillingInfo">Recurring Billing info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitRecurringBillingSubscription(bool Is3DSecure, GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard, RecurringBillingInfo RecurringBillingInfo)
        {

            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
            PaymentGatewayResponse.IsSuccess = false;
            PaymentGatewayResponse.ResponseText = "";
            PaymentGatewayResponse.ResponseCode = "";
          
            // Return Payment GatewayResponse
            return PaymentGatewayResponse;
        }
        #endregion
    }
}

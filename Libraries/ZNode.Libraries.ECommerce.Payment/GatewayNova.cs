using System;
using System.IO;
using System.Net;
using System.Xml;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Nova Gateway
    /// </summary>    
    public class GatewayNova : ZNodePaymentBase
    {
        /// <summary>
        /// Submit payment to Nova Gateway
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitPayment(GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
            string _postURL = string.Empty;
            string EGCvalue = string.Empty;

            if (BillingAddress.Street1.Length > 20)
            {
                BillingAddress.Street1 = BillingAddress.Street1.Remove(20);
            }

            if (BillingAddress.Street2.Length > 30)
            {
                BillingAddress.Street2 = BillingAddress.Street2.Remove(30);
            }

            // Input data in Xml
            // Gateway Details
            EGCvalue += "<txn>";
            EGCvalue += "<ssl_merchant_ID>" + Gateway.GatewayLoginID + "</ssl_merchant_ID>";
            EGCvalue += "<ssl_user_id>" + Gateway.GatewayPassword + "</ssl_user_id>";
            EGCvalue += "<ssl_pin>" + Gateway.TransactionKey + "</ssl_pin>";
            EGCvalue += "<ssl_transaction_type>" + Gateway.TransactionType  + "</ssl_transaction_type>";            
            
            // Card Details 
            EGCvalue += "<ssl_card_number>" + CreditCard.CardNumber  + "</ssl_card_number>";
            
            // ExpiredDate should follow the following data format "0107" (which represents Jan,2007)
            string CardExpireDate = CreditCard.CreditCardExp.Remove(2, 3);
            EGCvalue += "<ssl_exp_date>" + CardExpireDate + "</ssl_exp_date>";
            EGCvalue += "<ssl_amount>" + CreditCard.Amount + "</ssl_amount>";
            EGCvalue += "<ssl_invoice_number>" + CreditCard.OrderID + "</ssl_invoice_number>";
            
            // CVV2 Data
            EGCvalue += "<ssl_cvv2cvc2_indicator>1</ssl_cvv2cvc2_indicator>"; // CVV2 Indicator 0= Bypassed 1=present, 2=Illegible, and 9=Not Present
            EGCvalue += "<ssl_cvv2cvc2>" + CreditCard.CardSecurityCode + "</ssl_cvv2cvc2>";
            
            // Customer BillingDetails
            EGCvalue += "<ssl_company>" + BillingAddress.CompanyName + "</ssl_company>";
            EGCvalue += "<ssl_first_name>" + BillingAddress.FirstName + "</ssl_first_name>";
            EGCvalue += "<ssl_last_name>" + BillingAddress.LastName + "</ssl_last_name>";
            EGCvalue += "<ssl_avs_address>" + BillingAddress.Street1 + "</ssl_avs_address>";
            EGCvalue += "<ssl_address2>" + BillingAddress.Street2 + "</ssl_address2>";
            EGCvalue += "<ssl_city>" + BillingAddress.City + "</ssl_city>";
            EGCvalue += "<ssl_state>" + BillingAddress.StateCode + "</ssl_state>";
            EGCvalue += "<ssl_avs_zip>" + BillingAddress.PostalCode + "</ssl_avs_zip>";
            EGCvalue += "<ssl_phone>" + BillingAddress.PhoneNumber + "</ssl_phone>";
            EGCvalue += "<ssl_email>" + BillingAddress.EmailId + "</ssl_email>";
            
            // Customer ShippingDetails
            EGCvalue += "<ssl_ship_to_company>" + ShippingAddress.CompanyName + "</ssl_ship_to_company>";
            EGCvalue += "<ssl_ship_to_first_name>" + ShippingAddress.FirstName + "</ssl_ship_to_first_name>";
            EGCvalue += "<ssl_ship_to_last_name>" + ShippingAddress.LastName + "</ssl_ship_to_last_name>";
            EGCvalue += "<ssl_ship_to_avs_address>" + ShippingAddress.Street1 + "</ssl_ship_to_avs_address>";
            EGCvalue += "<ssl_ship_to_address2>" + ShippingAddress.Street2 + "</ssl_ship_to_address2>";
            EGCvalue += "<ssl_ship_to_city>" + ShippingAddress.City + "</ssl_ship_to_city>";
            EGCvalue += "<ssl_ship_to_state>" + ShippingAddress.StateCode + "</ssl_ship_to_state>";
            EGCvalue += "<ssl_ship_to_avs_zip>" + ShippingAddress.PostalCode + "</ssl_ship_to_avs_zip>";
            EGCvalue += "<ssl_ship_to_phone>" + ShippingAddress.PhoneNumber + "</ssl_ship_to_phone>";          
            EGCvalue += "</txn>";

            // To Check Test Mode.
            if (Gateway.TestMode)
            {
                _postURL = "https://demo.myvirtualmerchant.com/VirtualMerchantDemo/processxml.do?xmldata=";
            }
            else
            {
                _postURL = "https://www.myvirtualmerchant.com/VirtualMerchant/processxml.do?xmldata=";
            }

            try
            {
                // Post the values into  Web Service   
                WebClient WebRequest = new WebClient();
                WebRequest.Headers.Add("Content-Type", "application/xml; charset=UTF-8");
                string objRespValues = WebRequest.UploadString(_postURL + EGCvalue, "POST", string.Empty);

                // Read XML Data
                // Read the Xml data.              
                StringReader StreamInput = new StringReader(objRespValues);
                XmlTextReader xmlReader = new XmlTextReader(StreamInput);

                while (xmlReader.Read())
                {
                    if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "txn")
                    {
                        while (xmlReader.Read())
                        {
                            if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "ssl_result")
                            {
                                PaymentGatewayResponse.ResponseCode = xmlReader.ReadElementString("ssl_result");
                                PaymentGatewayResponse.ResponseText = xmlReader.ReadElementString("ssl_result_message");
                                PaymentGatewayResponse.TransactionId = xmlReader.ReadElementString("ssl_txn_id");
                            }
                            else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "ssl_approval_code")
                            {
                                PaymentGatewayResponse.ApprovalCode = xmlReader.ReadElementString("ssl_approval_code");
                                PaymentGatewayResponse.CardAuthCode = xmlReader.ReadElementString("ssl_approval_code");
                            }
                            else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "errorCode")
                            {
                                PaymentGatewayResponse.ResponseCode = xmlReader.ReadElementString("errorCode");
                                PaymentGatewayResponse.ResponseText += xmlReader.ReadElementString("errorName") + ". ";
                                PaymentGatewayResponse.ResponseText += xmlReader.ReadElementString("errorMessage");
                            }
                        }
                    }
                }

                if (PaymentGatewayResponse.ResponseCode == "0")
                {
                    PaymentGatewayResponse.IsSuccess = true;
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }

                return PaymentGatewayResponse;
            }
            catch (Exception Ex)
            {                
                PaymentGatewayResponse.ResponseText = Ex.Message.ToString(); // Response Error code Description               
                return PaymentGatewayResponse;
            }
        }

        /// <summary>
        /// Refund payment
        /// </summary>
        /// <param name="Gateway">Gateway info</param>
        /// <param name="CreditCardInfo">Credit Card Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse RefundPayment(GatewayInfo Gateway, CreditCard CreditCardInfo)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
            string _postURL = string.Empty;
            string EGCvalue = string.Empty;

            // Input data in Xml
            // Gateway Details
            EGCvalue += "<txn>";
            EGCvalue += "<ssl_merchant_ID>" + Gateway.GatewayLoginID + "</ssl_merchant_ID>";
            EGCvalue += "<ssl_user_id>" + Gateway.GatewayPassword + "</ssl_user_id>";
            EGCvalue += "<ssl_pin>" + Gateway.TransactionKey + "</ssl_pin>";
            EGCvalue += "<ssl_transaction_type>" + Gateway.TransactionType + "</ssl_transaction_type>";            
            
            // Card Details 
            EGCvalue += "<ssl_card_number>" + CreditCardInfo.CardNumber + "</ssl_card_number>";
            
            // ExpiredDate should follow the following data format "0107" (which represents Jan,2007)
            string CardExpireDate = CreditCardInfo.CreditCardExp.Remove(2, 3);
            EGCvalue += "<ssl_exp_date>" + CardExpireDate + "</ssl_exp_date>";
            EGCvalue += "<ssl_amount>" + CreditCardInfo.Amount + "</ssl_amount>";

            // CVV2 Data
            EGCvalue += "<ssl_cvv2cvc2_indicator>1</ssl_cvv2cvc2_indicator>"; // CVV2 Indicator 0= Bypassed 1=present,2=Illegible, and 9=Not Present
            EGCvalue += "<ssl_cvv2cvc2>" + CreditCardInfo.CardSecurityCode + "</ssl_cvv2cvc2>";
            EGCvalue += "</txn>";

            // To Check Test Mode.
            if (Gateway.TestMode)
            {
                _postURL = "https://www.myvirtualmerchant.com/VirtualMerchantDemo/processxml.do?xmldata=";
            }
            else
            {
                _postURL = "https://www.myvirtualmerchant.com/VirtualMerchant/processxml.do?xmldata=";
            }

            try
            {
                // Post the values into  Web Service   
                WebClient WebRequest = new WebClient();
                WebRequest.Headers.Add("Content-Type", "application/xml; charset=UTF-8");
                string objRespValues = WebRequest.UploadString(_postURL + EGCvalue, "POST", string.Empty);

                // Read XML Data
                // Read the Xml data.              
                StringReader StreamInput = new StringReader(objRespValues);
                XmlTextReader xmlReader = new XmlTextReader(StreamInput);

                while (xmlReader.Read())
                {
                    if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "txn")
                    {
                        while (xmlReader.Read())
                        {
                            if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "ssl_result")
                            {
                                PaymentGatewayResponse.ResponseCode = xmlReader.ReadElementString("ssl_result");
                                PaymentGatewayResponse.ResponseText = xmlReader.ReadElementString("ssl_result_message");
                                PaymentGatewayResponse.TransactionId = xmlReader.ReadElementString("ssl_txn_id");
                                PaymentGatewayResponse.ApprovalCode = xmlReader.ReadElementString("ssl_approval_code");
                            }
                            else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.LocalName == "errorCode")
                            {
                                PaymentGatewayResponse.ResponseCode = xmlReader.ReadElementString("errorCode");
                                PaymentGatewayResponse.ResponseText += xmlReader.ReadElementString("errorName") + ". ";
                                PaymentGatewayResponse.ResponseText += xmlReader.ReadElementString("errorMessage");
                            }
                        }
                    }
                }

                if (PaymentGatewayResponse.ResponseCode == "0")
                {
                    PaymentGatewayResponse.IsSuccess = true;
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }
            }
            catch (Exception Ex)
            {
                PaymentGatewayResponse.ResponseText = Ex.Message.ToString(); // Response Error code Description
            }

            return PaymentGatewayResponse;
        }
    }
}


using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Override this class to extend Znode Multifront to connect with your own custom payment gateway
    /// </summary>
    public class CustomGateway : ZNodePaymentBase 
    {
        /// <summary>
        /// Override this method to submit a credit card transaction
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitPayment(GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();

            // Comment these lines if you have an nSoftware license
            PaymentGatewayResponse.ResponseCode = "0";
            PaymentGatewayResponse.ResponseText = "Error occurred while processing your payment in Custom gateway.";
            PaymentGatewayResponse.TransactionId = "1234";
            PaymentGatewayResponse.IsSuccess = true;
            // Add your custom payment gateway code here.....
            // Return gateway response
            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Override this method to void a transaction
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="CreditCard">Credit Card Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse VoidPayment(GatewayInfo Gateway, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
           
            // Add your custom payment gateway code here.....
            // Return gateway response
            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Override this method to refund a transaction
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="CreditCard">Credit Card Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse RefundPayment(GatewayInfo Gateway, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();

            // Add your custom payment gateway code here.....
            // Return gateway response
            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Override this method to submit a credit card transaction
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <param name="RecurringBillingInfo">RecurringBilling Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitRecurringBillingSubscription(GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard, RecurringBillingInfo RecurringBillingInfo)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();

            // Comment these lines if you have an nSoftware license
            PaymentGatewayResponse.ResponseCode = "-1";
            PaymentGatewayResponse.ResponseText = "Error occurred while processing your payment in Custom gateway.";

            // Add your custom payment gateway code here.....
            // Return gateway response
            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Cancel the recurring billing subscription
        /// </summary>
        /// <param name="SubscriptionId">Subscription Id</param>
        /// <param name="Gateway">Gateway Info</param>        
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse CancelRecurringBillingSubscription(string SubscriptionId, GatewayInfo Gateway)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();

            // Add your custom payment gateway code here.....
            // Return gateway response
            return PaymentGatewayResponse;
        }
    }
}

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Gateway Types
    /// </summary>
    public enum GatewayType
    {
        /// <summary>
        /// Represents the Authorize.NET gateway
        /// </summary>
        AUTHORIZE = 1,

        /// <summary>
        /// Represents the Verisign's PayFlow pro gateway
        /// </summary>
        VERISIGN = 2,

        /// <summary>
        /// Represents the Paymentech Orbital gateway
        /// </summary>
        PAYMENTECH = 3,

        /// <summary>
        /// Represents the NOVA payment gateway
        /// </summary>
        NOVA = 5,

        /// <summary>
        /// Represents the PayPal credit card payment
        /// </summary>
        PAYPAL = 6,

        /// <summary>
        /// Represents the CardStream payment gateway
        /// </summary>
        CARDSTREAM = 8,

        /// <summary>
        /// Represents the World Pay payment gateway
        /// </summary>
        WORLDPAY = 10,

        /// <summary>
        /// Represents the Cyber Source payment gateway
        /// </summary>
        CYBERSOURCE = 11,

        /// <summary>
        /// Represents the Custom payment Component
        /// </summary>
        CUSTOM = 100,
    }
}

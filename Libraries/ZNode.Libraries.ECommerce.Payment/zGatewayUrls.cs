﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
namespace ZNode.Libraries.ECommerce.Payment
{
    public class zGatewayUrls
    {
        /// <summary>
        /// Gateway Url for Submit Payment
        /// </summary>
        /// <param name="isTestMode"></param>
        /// <returns></returns>
        public static string GetAuthorizePaymentGatewayUrl(bool isTestMode)
        {
            string baseAddress = string.Empty;
            if (isTestMode)
            {
                baseAddress = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["AuthorizePaymentGatewayUrlTest"]) ?
                    ConfigurationManager.AppSettings["AuthorizePaymentGatewayUrlTest"].ToString() : "https://test.authorize.net/gateway/transact.dll";
            }
            else
            {
                baseAddress = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["AuthorizePaymentGatewayUrlProd"]) ?
                    ConfigurationManager.AppSettings["AuthorizePaymentGatewayUrlProd"].ToString() : "https://secure.authorize.net/gateway/transact.dll";
            }

            return baseAddress;
        }

        /// <summary>
        /// Gateway Url for Void and Capture Payment
        /// </summary>
        /// <param name="isTestMode"></param>
        /// <returns></returns>
        public static string GetAuthorizeVoidAndCapturePaymentGatwayUrl(bool isTestMode)
        {
            string baseAddress = string.Empty;
            if (isTestMode)
            {
                baseAddress = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["AuthorizeVoidAndCapturePaymentTest"]) ?
                    ConfigurationManager.AppSettings["AuthorizeVoidAndCapturePaymentTest"].ToString() : "https://certification.authorize.net/gateway/transact.dll";
            }
            else
            {
                baseAddress = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["AuthorizePaymentGatewayUrlProd"]) ?
                  ConfigurationManager.AppSettings["AuthorizePaymentGatewayUrlProd"].ToString() : "https://secure.authorize.net/gateway/transact.dll";
            }

            return baseAddress;
        }

        /// <summary>
        /// Gateway Url for Recurring Payment Option
        /// </summary>
        /// <param name="isTestMode"></param>
        /// <returns></returns>
        public static string GetRecurringBillingSubscriptionGatwayUrl(bool isTestMode)
        {
            string baseAddress = string.Empty;
            if (isTestMode)
            {
                baseAddress = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["RecurringBillingSubscriptionGatwayUrlTest"]) ?
                    ConfigurationManager.AppSettings["RecurringBillingSubscriptionGatwayUrlTest"].ToString() : "https://apitest.authorize.net/xml/v1/request.api";
            }
            else
            {
                baseAddress = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["RecurringBillingSubscriptionGatwayUrlProd"]) ?
                    ConfigurationManager.AppSettings["RecurringBillingSubscriptionGatwayUrlProd"].ToString() : "https://api.authorize.net/xml/v1/request.api";
            }

            return baseAddress;
        }
    }
}

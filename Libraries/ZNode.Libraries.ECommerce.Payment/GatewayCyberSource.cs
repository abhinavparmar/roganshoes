﻿using System;
using System.Collections;
using CyberSource.Clients;
using CyberSource.Clients.SoapWebReference;
using ZNode.Libraries.ECommerce.Entities;
using System.Net;

namespace ZNode.Libraries.ECommerce.Payment
{
    public class GatewayCyberSource
    {
        /// <summary>
        /// Submitting Initial Order and get the transactionID
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitPayment(ZNode.Libraries.ECommerce.Payment.GatewayInfo Gateway, ZNode.Libraries.ECommerce.Entities.Address BillingAddress, ZNode.Libraries.ECommerce.Entities.Address ShippingAddress, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
            bool useSavedCard = false;
            RequestMessage request = new RequestMessage();

            // Replace the generic value with your reference number for the current transaction.
            request.merchantReferenceCode = CreditCard.OrderID.ToString();

            // Information about application to trouble shoot.
            request.clientLibrary = "NVP Client";
            request.clientEnvironment = Environment.OSVersion.Platform + Environment.OSVersion.Version.ToString() + "-CLR" + Environment.Version.ToString();
            BillTo billTo = new BillTo();
            billTo.firstName = BillingAddress.FirstName;
            billTo.lastName = BillingAddress.LastName;
            billTo.street1 = BillingAddress.Street1;
            billTo.city = BillingAddress.City;
            billTo.state = BillingAddress.StateCode;
            billTo.postalCode = BillingAddress.PostalCode;
            billTo.country = BillingAddress.CountryCode;
            billTo.email = BillingAddress.EmailId;
            billTo.ipAddress = Gateway.CustomerIPAddress;
            request.billTo = billTo;

            ShipTo shipTo = new ShipTo();
            shipTo.firstName = ShippingAddress.FirstName;
            shipTo.lastName = ShippingAddress.LastName;
            shipTo.street1 = ShippingAddress.Street1;
            shipTo.city = ShippingAddress.City;
            shipTo.state = ShippingAddress.StateCode;
            shipTo.postalCode = ShippingAddress.PostalCode;
            shipTo.country = ShippingAddress.CountryCode;
            shipTo.email = ShippingAddress.EmailId;
            request.shipTo = shipTo;

            request.decisionManager = new DecisionManager();
            request.decisionManager.enabled = "false";

            request.ccAuthService = new CCAuthService();
            request.ccAuthService.run = "true";

            if (CreditCard.TransactionID != null)
            {
                useSavedCard = true;
            }

            // If useSavedCard is true we'll use the transaction ID from the customer's last purchase, otherwise we use the full credit card detail.
            if (useSavedCard)
            {
                request.recurringSubscriptionInfo = new RecurringSubscriptionInfo();
                request.recurringSubscriptionInfo.subscriptionID = CreditCard.TransactionID;
            }
            else 
            {
                // Process using credit card
                // If Authorize only
                if (Gateway.PreAuthorize) 
                {
                    request.ccAuthService = new CCAuthService();
                    request.ccAuthService.run = "true";
                }
                else 
                {
                    // If Authorize and Capture
                    // Authorize
                    request.ccAuthService = new CCAuthService();
                    request.ccAuthService.run = "true";

                    // Authorize and Capture
                    request.ccCaptureService = new CCCaptureService();
                    request.ccCaptureService.run = "true";
                }

                // Card Details
                Card card = new Card();
                card.fullName = billTo.firstName + " " + billTo.lastName;
                card.accountNumber = CreditCard.CardNumber;
                string month = CreditCard.CreditCardExp.Substring(0, 2);
                string year = CreditCard.CreditCardExp.Substring(3, 4);
                card.expirationMonth = month;
                card.expirationYear = year;
                card.cvNumber = CreditCard.CardSecurityCode;
                request.card = card;
            }

            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            // Getting Tax and shipping details
            PurchaseTotals purchaseTotals = new PurchaseTotals();
            purchaseTotals.currency = Gateway.CurrencyCode;
            purchaseTotals.taxAmount = shoppingCart.TaxCost.ToString();
            purchaseTotals.freightAmount = shoppingCart.ShippingCost.ToString();
            purchaseTotals.discountAmount = shoppingCart.Discount.ToString();
            purchaseTotals.grandTotalAmount = shoppingCart.Total.ToString();
            request.purchaseTotals = purchaseTotals;

            int i = 0;
            request.item = new Item[shoppingCart.ShoppingCartItems.Count];

            // Getting Cart item details
            foreach (ZNodeShoppingCartItem CartItem in shoppingCart.ShoppingCartItems)
            {
                Item item = new Item();
                item.id = Convert.ToString(i);
                item.unitPrice = CartItem.UnitPrice.ToString();
                item.quantity = CartItem.Quantity.ToString();
                item.productCode = CartItem.Product.ProductNum;
                item.productSKU = CartItem.Product.SKU;
                item.productName = CartItem.Product.Name;
                request.item[i] = item;

                i++;
            }

            try
            {
                ReplyMessage reply = SoapClient.RunTransaction(request);

                // To retrieve individual reply fields, follow these examples.
                PaymentGatewayResponse.GatewayResponseData = reply.ToString();
                PaymentGatewayResponse.ResponseCode = reply.reasonCode;
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(reply.reasonCode); 
                PaymentGatewayResponse.TransactionId = reply.requestID;
                PaymentGatewayResponse.CardAuthCode = reply.requestToken;

                if (reply.reasonCode == "100")
                {
                    if (useSavedCard)
                    {
                        // Create subscription profile for transaction
                        request = new RequestMessage();

                        request.decisionManager = new DecisionManager();
                        request.decisionManager.enabled = "false";
                        request.merchantReferenceCode = CreditCard.OrderID.ToString();

                        request.paySubscriptionCreateService = new PaySubscriptionCreateService();
                        request.paySubscriptionCreateService.run = "true";
                        request.merchantReferenceCode = CreditCard.OrderID.ToString();
                        request.paySubscriptionCreateService.paymentRequestID = reply.requestID;
                        request.paySubscriptionCreateService.paymentRequestToken = reply.requestToken;
                        request.orderRequestToken = reply.requestToken;
                        request.recurringSubscriptionInfo = new RecurringSubscriptionInfo();
                        request.recurringSubscriptionInfo.frequency = "on-demand";

                        reply = SoapClient.RunTransaction(request);

                        if (reply.reasonCode == "100")
                        {
                            PaymentGatewayResponse.ResponseText = reply.decision;
                            PaymentGatewayResponse.SubscriptionID = reply.paySubscriptionCreateReply.subscriptionID;
                            PaymentGatewayResponse.IsSuccess = true;
                        }
                        else
                        {
                            PaymentGatewayResponse.ResponseText = reply.decision;
                            PaymentGatewayResponse.IsSuccess = false;
                        }
                    }
                    else
                    {
                        PaymentGatewayResponse.SubscriptionID = CreditCard.TransactionID;
                        PaymentGatewayResponse.IsSuccess = true;
                    }
                }
            }
            catch (Exception e)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(e.Message);
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(string.Empty);
            }

            return PaymentGatewayResponse;
        }

 
        /// <summary>
        /// Submitting Recurring Payment Order and get the transactionID
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitRecurringBillingSubscription(ZNode.Libraries.ECommerce.Payment.GatewayInfo Gateway, ZNode.Libraries.ECommerce.Entities.Address BillingAddress, ZNode.Libraries.ECommerce.Entities.Address ShippingAddress, CreditCard CreditCard, RecurringBillingInfo RecurringBillingInfo)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();

            RequestMessage request = new RequestMessage();

            // Replace the generic value with your reference number for the current transaction.
            request.merchantReferenceCode = CreditCard.OrderID.ToString();

            // Information about application to trouble shoot.
            request.clientLibrary = "NVP Client";
            request.clientEnvironment = Environment.OSVersion.Platform + Environment.OSVersion.Version.ToString() + "-CLR" + Environment.Version.ToString();
            BillTo billTo = new BillTo();
            billTo.firstName = BillingAddress.FirstName;
            billTo.lastName = BillingAddress.LastName;
            billTo.street1 = BillingAddress.Street1;
            billTo.city = BillingAddress.City;
            billTo.state = BillingAddress.StateCode;
            billTo.postalCode = BillingAddress.PostalCode;
            billTo.country = BillingAddress.CountryCode;
            billTo.email = BillingAddress.EmailId;
            billTo.ipAddress = Gateway.CustomerIPAddress;
            request.billTo = billTo;

            ShipTo shipTo = new ShipTo();
            shipTo.firstName = ShippingAddress.FirstName;
            shipTo.lastName = ShippingAddress.LastName;
            shipTo.street1 = ShippingAddress.Street1;
            shipTo.city = ShippingAddress.City;
            shipTo.state = ShippingAddress.StateCode;
            shipTo.postalCode = ShippingAddress.PostalCode;
            shipTo.country = ShippingAddress.CountryCode;
            shipTo.email = ShippingAddress.EmailId;
            request.shipTo = shipTo;

            request.decisionManager = new DecisionManager();
            request.decisionManager.enabled = "false";

            request.ccAuthService = new CCAuthService();
            request.ccAuthService.run = "true";



            // If useSavedCard is true we'll use the transaction ID from the customer's last purchase, otherwise we use the full credit card detail.

            request.recurringSubscriptionInfo = new RecurringSubscriptionInfo();
            request.recurringSubscriptionInfo.amount = RecurringBillingInfo.InitialAmount.ToString();
            request.recurringSubscriptionInfo.numberOfPayments = RecurringBillingInfo.TotalCycles.ToString();
            request.recurringSubscriptionInfo.numberOfPaymentsToAdd = RecurringBillingInfo.Frequency.ToString();
            request.recurringSubscriptionInfo.frequency = RecurringBillingInfo.Period;
            request.recurringSubscriptionInfo.startDate = DateTime.Now.ToString("YYYYMMDD");
            if (RecurringBillingInfo.Period == "DAY")
                request.recurringSubscriptionInfo.endDate = DateTime.Now.AddDays(Convert.ToInt32(RecurringBillingInfo.Frequency) * RecurringBillingInfo.TotalCycles).ToString("YYYYMMDD");
            if (RecurringBillingInfo.Period == "WEEK")
                request.recurringSubscriptionInfo.endDate = DateTime.Now.AddDays(Convert.ToInt32(RecurringBillingInfo.Frequency) * 7 * RecurringBillingInfo.TotalCycles).ToString("YYYYMMDD");

            if (RecurringBillingInfo.Period == "MONTH")
                request.recurringSubscriptionInfo.endDate = DateTime.Now.AddMonths(Convert.ToInt32(RecurringBillingInfo.Frequency) * RecurringBillingInfo.TotalCycles).ToString("YYYYMMDD");

            if (RecurringBillingInfo.Period == "YEAR")
                request.recurringSubscriptionInfo.endDate = DateTime.Now.AddYears(Convert.ToInt32(RecurringBillingInfo.Frequency) * RecurringBillingInfo.TotalCycles).ToString("YYYYMMDD");

            // Card Details
            Card card = new Card();
            card.fullName = billTo.firstName + " " + billTo.lastName;
            card.accountNumber = CreditCard.CardNumber;
            string month = CreditCard.CreditCardExp.Substring(0, 2);
            string year = CreditCard.CreditCardExp.Substring(3, 4);
            card.expirationMonth = month;
            card.expirationYear = year;
            card.cvNumber = CreditCard.CardSecurityCode;
            request.card = card;
            PurchaseTotals purchaseTotals = new PurchaseTotals();
            purchaseTotals.currency = Gateway.CurrencyCode;
            purchaseTotals.grandTotalAmount = RecurringBillingInfo.InitialAmount.ToString();

            request.purchaseTotals = purchaseTotals;

            request.item = new Item[1];
            Item item = new Item();
            item.id = "1";
            item.unitPrice = RecurringBillingInfo.InitialAmount.ToString();
            item.productSKU = RecurringBillingInfo.ProfileName; ;

            request.item[0] = item;

            try
            {
                ReplyMessage reply = SoapClient.RunTransaction(request);

                // To retrieve individual reply fields, follow these examples.
                PaymentGatewayResponse.GatewayResponseData = reply.ToString();
                PaymentGatewayResponse.ResponseCode = reply.reasonCode;
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(reply.reasonCode);
                PaymentGatewayResponse.TransactionId = reply.requestID;
                PaymentGatewayResponse.CardAuthCode = reply.requestToken;

                if (reply.reasonCode == "100")
                {
                    PaymentGatewayResponse.IsSuccess = true;
                    return PaymentGatewayResponse;
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }
            }
            catch (Exception e)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(e.Message);
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(string.Empty);
            }

            return PaymentGatewayResponse;
        }


        /// <summary>
        /// Authorize the Payment
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse Authorize(GatewayInfo Gateway, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();

            RequestMessage request = new RequestMessage();

            // Replace the generic value with your reference number for the current transaction.
            request.merchantReferenceCode = CreditCard.OrderID.ToString();

            // Information about application to trouble shoot.
            request.clientLibrary = "NVP Client";
            request.clientEnvironment = Environment.OSVersion.Platform + Environment.OSVersion.Version.ToString() + "-CLR" + Environment.Version.ToString();

            // Authorize
            request.ccAuthService = new CCAuthService();
            request.ccAuthService.run = "true";

            request.recurringSubscriptionInfo = new RecurringSubscriptionInfo();
            request.recurringSubscriptionInfo.subscriptionID = CreditCard.TransactionID;

            PurchaseTotals purchaseTotals = new PurchaseTotals();
            purchaseTotals.currency = Gateway.CurrencyCode;
            purchaseTotals.grandTotalAmount = CreditCard.Amount.ToString();

            request.purchaseTotals = purchaseTotals;

            request.decisionManager = new DecisionManager();
            request.decisionManager.enabled = "false";

            try
            {
                ReplyMessage reply = SoapClient.RunTransaction(request);

                // To retrieve individual reply fields, follow these examples.
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(reply.reasonCode); 
                PaymentGatewayResponse.ResponseCode = reply.reasonCode;
                PaymentGatewayResponse.TransactionId = reply.requestID;
                PaymentGatewayResponse.CardAuthCode = reply.requestToken;
                PaymentGatewayResponse.TransactionType = Gateway.TransactionType;
                if (reply.reasonCode == "100")
                {
                    PaymentGatewayResponse.IsSuccess = true;
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }
            }
            catch (Exception e)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(e.Message);
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(string.Empty);
            }

            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Capture the payment
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse Capture(GatewayInfo Gateway, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();

            RequestMessage request = new RequestMessage();

            // Replace the generic value with your reference number for the current transaction.
            request.merchantReferenceCode = CreditCard.OrderID.ToString();

            // Information about application to trouble shoot.
            request.clientLibrary = "NVP Client";
            request.clientEnvironment = Environment.OSVersion.Platform + Environment.OSVersion.Version.ToString() + "-CLR" + Environment.Version.ToString();

            // Capture
            request.ccCaptureService = new CCCaptureService();
            request.ccCaptureService.run = "true";
            request.ccCaptureService.authRequestID = CreditCard.TransactionID;
            request.ccCaptureService.authRequestToken = CreditCard.CardDataToken;

            PurchaseTotals purchaseTotals = new PurchaseTotals();
            purchaseTotals.currency = Gateway.CurrencyCode;
            purchaseTotals.grandTotalAmount = CreditCard.Amount.ToString();

            request.purchaseTotals = purchaseTotals;

            request.decisionManager = new DecisionManager();
            request.decisionManager.enabled = "false";

            try
            {
                ReplyMessage reply = SoapClient.RunTransaction(request);

                // To retrieve individual reply fields, follow these examples.
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(reply.reasonCode); 
                PaymentGatewayResponse.ResponseCode = reply.reasonCode;
                PaymentGatewayResponse.TransactionId = reply.requestID;
                PaymentGatewayResponse.CardAuthCode = reply.requestToken;
                PaymentGatewayResponse.TransactionType = Gateway.TransactionType;
                if (reply.reasonCode == "100")
                {
                    PaymentGatewayResponse.IsSuccess = true;
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }
            }
            catch (Exception e)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(e.Message);
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(string.Empty);
            }

            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Refund the payment
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse RefundPayment(GatewayInfo Gateway, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();

            RequestMessage request = new RequestMessage();

            // Replace the generic value with your reference number for the current transaction.
            request.merchantReferenceCode = CreditCard.OrderID.ToString();

            // Information about application to trouble shoot.
            request.clientLibrary = "NVP Client";
            request.clientEnvironment = Environment.OSVersion.Platform + Environment.OSVersion.Version.ToString() + "-CLR" + Environment.Version.ToString();
           
            // This section contains a transaction request for the authorization
            // service with complete billing, payment card, and purchase information.
            if (Gateway.TransactionType == ZNode.Libraries.ECommerce.Entities.ZNodePaymentStatus.CREDIT_AUTHORIZED.ToString())
            {
                request.ccAuthReversalService = new CCAuthReversalService();
                request.ccAuthReversalService.run = "true";
                request.ccAuthReversalService.authRequestID = CreditCard.TransactionID;
                request.orderRequestToken = CreditCard.CardDataToken;
            }
            else
            {
                request.ccCreditService = new CCCreditService();
                request.ccCreditService.run = "true";
                request.ccCreditService.captureRequestID = CreditCard.TransactionID;
                request.ccCreditService.captureRequestToken = CreditCard.CardDataToken;
            }

            PurchaseTotals purchaseTotals = new PurchaseTotals();
            purchaseTotals.currency = Gateway.CurrencyCode;
            purchaseTotals.grandTotalAmount = CreditCard.Amount.ToString();

            request.purchaseTotals = purchaseTotals;

            request.decisionManager = new DecisionManager();
            request.decisionManager.enabled = "false";

            try
            {
                ReplyMessage reply = SoapClient.RunTransaction(request);

                // To retrieve individual reply fields, follow these examples.
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(reply.reasonCode); 
                PaymentGatewayResponse.ResponseCode = reply.reasonCode;
                PaymentGatewayResponse.TransactionId = reply.requestID;
                PaymentGatewayResponse.CCVResponsecode = reply.requestToken;

                if (reply.reasonCode == "100")
                {
                    PaymentGatewayResponse.IsSuccess = true;
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }
            }
            catch (Exception e)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(e.Message);
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(string.Empty);
            }

            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Void the payment
        /// </summary>
        /// <param name="Gateway">Gateway info</param>
        /// <param name="CreditCard">Credit card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse VoidPayment(GatewayInfo Gateway, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();

            RequestMessage request = new RequestMessage();

            // Replace the generic value with your reference number for the current transaction.
            request.merchantReferenceCode = CreditCard.OrderID.ToString();

            // Information about application to trouble shoot.
            request.clientLibrary = "NVP Client";
            request.clientEnvironment = Environment.OSVersion.Platform + Environment.OSVersion.Version.ToString() + "-CLR" + Environment.Version.ToString();

            // This section contains a transaction request for the authorization
            // service with complete billing, payment card, and purchase information.
            request.voidService = new VoidService();
            request.voidService.run = "true";
            request.voidService.voidRequestID = CreditCard.TransactionID;
            request.voidService.voidRequestToken = CreditCard.CardDataToken;

            try
            {
                ReplyMessage reply = SoapClient.RunTransaction(request);

                // To retrieve individual reply fields, follow these examples.
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(reply.reasonCode); 
                PaymentGatewayResponse.ResponseCode = reply.reasonCode;
                PaymentGatewayResponse.TransactionId = reply.requestID;
                PaymentGatewayResponse.CardAuthCode = reply.requestToken;
                PaymentGatewayResponse.TransactionType = Gateway.TransactionType;
                if (reply.reasonCode == "100")
                {
                    PaymentGatewayResponse.IsSuccess = true;
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }
            }
            catch (Exception e)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(e.Message);
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(string.Empty);
            }

            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Get the reason code description
        /// </summary>
        /// <param name="reasonCode">Reason code</param>
        /// <returns>Returns the reason description</returns>
        private string GetReasoncodeDescription(string reasonCode)
        {
            switch (reasonCode)
            {
                case "100":
                    return "Successful transaction.";
                case "203":
                    return "General decline of the card.";
                case "204":
                    return "Insufficient funds in the account.";
                case "208":
                    return "Inactive card or card not authorized for card-not-present transactions.";
                case "210":
                    return "The card has reached the credit limit.";
                case "211":
                    return "Invalid card verification number.";
                case "232":
                    return "The card type is not accepted by the payment processor.";
                case "234":
                    return "There is a problem with your CyberSource merchant configuration.";
                case "235":
                    return "The requested amount exceeds the originally authorized amount. Occurs, for example, if you try to capture an amount larger than the original authorization amount.";
                case "237":
                    return "The authorization has already been reversed.";
                case "238":
                    return "The authorization has already been captured.";
                case "239":
                    return "The requested transaction amount must match the previous transaction amount.";
                case "240":
                    return "The card type sent is invalid or does not correlate with the credit card number.";
                case "241":
                    return "The request ID is invalid.";
                case "243":
                    return "The transaction has already been settled or reversed.";
                case "247":
                    return "You requested a credit for a capture that was previously voided.";
                default:
                    break;
            }

            return "Unable to process, please contact customer support.";
        }
    }
}

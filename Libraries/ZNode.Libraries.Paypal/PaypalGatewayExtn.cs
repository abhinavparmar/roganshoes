﻿using System;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;
using System.Collections.Generic;
using com.paypal.sdk.profiles;
using com.paypal.sdk.services;
using com.paypal.soap.api;
using System.Web;

namespace ZNode.Libraries.Paypal
{
    public partial class PaypalGateway : ZNodeBusinessBase
    {
        #region[Public Property]


        public int ZNodeOrderID
        {
            get;
            set;
        }

        #endregion

        #region Paypal helper method
        /// <summary>
        /// Get Express Checkout Shipping Info
        /// </summary>
        /// <returns></returns>
        private AddressType GetAddressDetails(ZNode.Libraries.DataAccess.Entities.Address address)
        {
            AddressType adddressType = new AddressType();
            adddressType.Name = GetFullNameforPaypal(address);//_userAccount.ShippingAddress.FirstName;
            adddressType.Phone = address.PhoneNumber;
            adddressType.PostalCode = address.PostalCode;
            adddressType.StateOrProvince = address.StateCode;
            adddressType.Street1 = address.Street;
            adddressType.Street2 = address.Street1;
            adddressType.CityName = address.City;
            adddressType.CountryName = address.CountryCode;

            foreach (CountryCodeType codeType in Enum.GetValues(typeof(CountryCodeType)))
            {
                if (codeType.ToString().Equals(address.CountryCode))
                {
                    adddressType.Country = codeType;
                    adddressType.CountrySpecified = true;
                    break;
                }
            }

            return adddressType;
        }


        /// <summary>
        /// Get information about an Express Checkout transaction
        /// </summary>
        /// <param name="token">Token of the checkout</param>
        /// <returns>Paypal Response</returns>
        public PaypalResponse GetExpressCheckoutDetailsExtn(string token)
        {

            token = GetTokenInUpperCase(token);
            // Create the request object
            GetExpressCheckoutDetailsRequestType expCheckoutRequest = new GetExpressCheckoutDetailsRequestType();
            PaypalResponse paypalResponse = new PaypalResponse();

            expCheckoutRequest.Token = token;

            GetExpressCheckoutDetailsResponseType ECDetailsResponse = (GetExpressCheckoutDetailsResponseType)this.caller.Call("GetExpressCheckoutDetails", expCheckoutRequest);



            if (ECDetailsResponse.Ack == AckCodeType.SuccessWithWarning || ECDetailsResponse.Ack == AckCodeType.Success)
            {
                paypalResponse.PayerID = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.PayerID;

                // Set Shipping Address 
                ZNodeAddress _shippingAddress = new ZNodeAddress();
                _shippingAddress.City = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.CityName;

                //ZEON CUSTOMIZATION FOR SHIPPING NAME :Starts
                //ZNode Old Code
                //_shippingAddress.FirstName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Name;


                //Get user passed user name
                //_shippingAddress.FirstName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Name;
                //if (string.IsNullOrEmpty(_shippingAddress.FirstName))
                //{
                //    //if user passed name is empty then get paypal account user name
                //    _shippingAddress.FirstName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.PayerName.FirstName;
                //    _shippingAddress.LastName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.PayerName.LastName;
                //}
              
                string FirstName = FormatName(ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Name, 0);
                if (!string.IsNullOrEmpty(FirstName))
                {
                    _shippingAddress.FirstName = FirstName;
                }
                else
                {
                    _shippingAddress.FirstName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Name;
                }

                if (_shippingAddress.FirstName.Length == 0)
                {
                    _shippingAddress.FirstName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.PayerName.FirstName;
                }

                string LastName = FormatName(ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Name, 1);
                if (!string.IsNullOrEmpty(LastName))
                {
                    _shippingAddress.LastName = LastName;
                }
                else
                {
                    _shippingAddress.LastName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.PayerName.LastName;
                }

                //ZEON CUSTOMIZATION FOR SHIPPING NAME:Ends

                _shippingAddress.Street1 = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Street1;
                _shippingAddress.Street2 = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Street2;

                _shippingAddress.CountryCode = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Country.ToString();
                _shippingAddress.EmailId = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Payer;


                _shippingAddress.PhoneNumber = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.ContactPhone;

                if (_shippingAddress.PhoneNumber == null)
                {
                    _shippingAddress.PhoneNumber = string.Empty;
                }

                _shippingAddress.StateCode = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.StateOrProvince;
                _shippingAddress.PostalCode = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.PostalCode;

                paypalResponse.ShippingAddress = _shippingAddress;
                paypalResponse.AddressStatus = GetAddressStatus(ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.AddressStatus); //Zeon Custom Code
                paypalResponse.ResponseCode = "0";
            }
            else
            {
                paypalResponse.ResponseText = ECDetailsResponse.Errors[0].LongMessage;
                paypalResponse.ResponseCode = ECDetailsResponse.Errors[0].ErrorCode;
            }

            return paypalResponse;
        }

        #endregion

        #region[Helper Method]
        /// <summary>
        /// Get Formatted Full Name for Paypal Express Checkout 
        /// </summary>
        /// <param name="currentAddress"></param>
        /// <returns></returns>
        private string GetFullNameforPaypal(ZNode.Libraries.DataAccess.Entities.Address currentAddress)
        {
            string fullName = string.Empty;
            if (currentAddress != null)
            {
                if (!string.IsNullOrEmpty(currentAddress.FirstName))
                {
                    fullName = currentAddress.FirstName.Trim() + " ";
                }

                if (!string.IsNullOrEmpty(currentAddress.LastName))
                {
                    fullName = fullName + currentAddress.LastName.Trim();
                }
            }

            return fullName.Trim();
        }
        /// <summary>
        /// Retrun logo Path
        /// </summary>
        /// <returns></returns>
        private string GetLogoPath()
        {
            string domainPath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string storeLogoPath = string.Empty;

            string applicationPath = System.Web.HttpContext.Current.Request.ApplicationPath;
            if (!applicationPath.EndsWith("/"))
            {
                applicationPath = applicationPath + "/";
            }
            storeLogoPath = domainPath + applicationPath + ZNodeConfigManager.SiteConfig.LogoPath.Replace("~/", "");

            //make image url https else getting non secure page error on paypal
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                storeLogoPath = storeLogoPath.Replace("http://", "https://");
            }
            return storeLogoPath;
        }

        /// <summary>
        ///Zeon custom code start
        //Some time in production url and token value become is lowercase and paypal payment gets failed to handle it making token in upper case
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private string GetTokenInUpperCase(string token)
        {
            string returnToken = token;

            if (!string.IsNullOrEmpty(token))
            {
                returnToken = token.ToUpper();
            }

            return returnToken;
        }

        /// <summary>
        /// Retrun Paypal Address Status is Confirmed Or Not based on AddressStatusCodeType
        /// </summary>
        /// <param name="paypalAddressStatusCode"></param>
        /// <returns></returns>
        private bool GetAddressStatus(AddressStatusCodeType paypalAddressStatusCode)
        {
            switch (paypalAddressStatusCode)
            {
                case AddressStatusCodeType.Confirmed:
                    return true;
                case AddressStatusCodeType.None:
                case AddressStatusCodeType.Unconfirmed:
                    return false;
                default:
                    return false;
            }
        }

        private string GetBNCode()
        {
            string bnCode = string.Empty;
            if (System.Configuration.ConfigurationManager.AppSettings["PayPalBNCode"] != null)
            {
                bnCode = System.Configuration.ConfigurationManager.AppSettings["PayPalBNCode"];
            }

            return bnCode;
        }


        public decimal GetUnitPrice(ZNodeProductBaseEntity Product, decimal TieredPricing)
        {
            ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption pricing = new ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption();

            decimal basePrice = pricing.PromotionalPrice(Product, TieredPricing);

            if (basePrice > 0)
            {
                // Calculate sales tax on discounted price.
                ZNode.Libraries.ECommerce.Entities.ZNodeInculsiveTax salesTax = new ZNodeInculsiveTax();
                basePrice = salesTax.GetInclusivePrice(Product.TaxClassID, basePrice);
            }

            basePrice = basePrice + Product.AddOnPrice;

            for (int idx = 0; idx < Product.ZNodeBundleProductCollection.Count; idx++)
            {
                basePrice += Product.ZNodeBundleProductCollection[idx].AddOnPrice;
            }

            return Math.Round(basePrice, 2);
        }

        public decimal GetOrderTotal(decimal itemTotal, decimal shippingTotal, decimal taxTotal)
        {
            var total = itemTotal + shippingTotal + taxTotal;
            return total;
        }

        /// <summary>
        /// Format FirstName to get first name and last name 
        /// </summary>
        /// <param name="Name">string</param>
        /// <param name="Parameter">int</param>
        /// <returns></returns>
        public string FormatName(string Name, int Parameter)
        {
            string[] arrName = null;
            string strName = string.Empty;
            if (!string.IsNullOrEmpty(Name))
            {
                arrName = Name.Split(' ');
                if (arrName != null && arrName.Length == 2)
                {
                    strName = arrName[Parameter].ToString();
                }
            }
            return strName;
        }

        #endregion

    }
}

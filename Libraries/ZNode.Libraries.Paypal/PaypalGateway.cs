using System;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;
using System.Collections.Generic;
using com.paypal.sdk.profiles;
using com.paypal.sdk.services;
using com.paypal.soap.api;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Configuration;

namespace ZNode.Libraries.Paypal
{
    /// <summary>
    /// PayPal Gateway
    /// </summary>
    public partial class PaypalGateway : ZNodeBusinessBase
    {
        #region Private Member Variables
        /// <summary>
        /// Read only Instance object for Call Services 
        /// </summary>
        private readonly CallerServices caller;
        private ZNodeShoppingCart _shoppingCart = null;
        private string _ECRedirectUrl = string.Empty;
        private string _ECCancelUrl = string.Empty;
        private decimal _orderTotal = 0;
        private PaymentSetting _paymentSetting = new PaymentSetting();
        private string CurrencyCode = ZNodeCurrencyManager.CurrencyCode();
        private string _paymentActionTypeCode = "Sale";
        private TList<OrderLineItem> _orderLineItems = new TList<OrderLineItem>();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the PaypalGateway class
        /// </summary>
        /// <param name="PaymentSetting">Payment Setting</param>        
        public PaypalGateway(PaymentSetting PaymentSetting)
            : this(PaymentSetting, string.Empty, string.Empty, false, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the PaypalGateway class.
        /// </summary>
        /// <param name="PaymentSetting">Payment Setting</param>
        /// <param name="ECRedirectUrl">Redirect URL</param>
        /// <param name="ECReturnUrl">Return URL</param>
        public PaypalGateway(PaymentSetting PaymentSetting, string ECRedirectUrl, string ECReturnUrl, bool isShippingExtended, decimal extendedShipCharge)
        {
            this._shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            this._paymentSetting = PaymentSetting;
            this._shoppingCart.IsShipngChrgsExtended = isShippingExtended;
            this._shoppingCart.ExtendedShippingCost = extendedShipCharge;
            this.caller = new CallerServices();
            ZNodeEncryption encryption = new ZNodeEncryption();

            string gatewayLoginID = encryption.DecryptData(this._paymentSetting.GatewayUsername);
            string gatewayPassword = encryption.DecryptData(this._paymentSetting.GatewayPassword);
            string transactionKey = this._paymentSetting.TransactionKey;
            bool IsTestMode = this._paymentSetting.TestMode;
            this.caller.APIProfile = CreateAPIProfile(gatewayLoginID, gatewayPassword, transactionKey, IsTestMode);

            this._ECCancelUrl = ECReturnUrl;
            this._ECRedirectUrl = ECRedirectUrl;

        }

        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the Shipping Address 
        /// </summary>
        public decimal OrderTotal
        {
            get { return this._orderTotal; }
            set { this._orderTotal = value; }
        }

        /// <summary>
        /// Gets or sets the payment action type code which Specifies whether the transaction is a Sale, an Authorization, or an Order
        /// Use Sale (authorize and capture) or Authorize (authorize only) mode when processing payments.
        /// </summary>
        public string PaymentActionTypeCode
        {
            get { return this._paymentActionTypeCode; }
            set { this._paymentActionTypeCode = value; }
        }

        /// <summary>
        /// Gets or sets the order
        /// </summary>      
        public TList<OrderLineItem> OrderLineItems
        {
            get { return this._orderLineItems; }
            set { this._orderLineItems = value; }
        }
        #endregion

        #region Protected properties

        /// <summary>
        /// Gets or set the paypal server url to post request
        /// </summary>
        private string PaypalServerUrl
        {
            get
            {
                if (PaymentSetting.TestMode)
                {
                    return "www.sandbox.paypal.com"; // Test environment URL
                }
                else
                {
                    return "www.paypal.com"; // Production Site URL
                }
            }
        }

        /// <summary>
        /// Gets the payment settings
        /// </summary>
        private PaymentSetting PaymentSetting
        {
            get
            {
                return this._paymentSetting;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method initiate an Express Checkout transaction
        /// </summary>
        /// <returns>Returns GatewayResponse object</returns>
        public PaypalResponse DoPaypalExpressCheckout(bool hideChangeAddressLink = false)
        {
            PaypalResponse paypalResponse = new PaypalResponse();
            PaymentSetting PaymentSetting = new PaymentSetting();
            PaypalRecurringBillingInfo recurringBillingInfo = new PaypalRecurringBillingInfo();

            //Zeon Customtization
            ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount _userAccount = ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount();
            _shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();


            try
            {

                //PRFT Custom Code For Upgrading TLS Version:Starts
                bool isPaypalTLSEnabled = false;
                if (ConfigurationManager.AppSettings["isPayPalTLSEnabled"] != null)
                {
                    bool.TryParse(ConfigurationManager.AppSettings["isPayPalTLSEnabled"].ToString(), out isPaypalTLSEnabled);
                    if (isPaypalTLSEnabled)
                    {
                        System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                        System.Net.ServicePointManager.Expect100Continue = true;
                        System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;
                    }
                }
                //PRFT Custom Code  For Upgrading TLS Version:Ends

                // Create the request object
                SetExpressCheckoutRequestType expCheckoutRequest = new SetExpressCheckoutRequestType();

                // Create the request details object
                expCheckoutRequest.SetExpressCheckoutRequestDetails = new SetExpressCheckoutRequestDetailsType();

                if (this._paymentSetting != null && this._paymentSetting.PreAuthorize)
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.PaymentAction = (PaymentActionCodeType)Enum.Parse(typeof(PaymentActionCodeType), PaymentActionCodeType.Authorization.ToString());
                else
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.PaymentAction = (PaymentActionCodeType)Enum.Parse(typeof(PaymentActionCodeType), this._paymentActionTypeCode);

                expCheckoutRequest.SetExpressCheckoutRequestDetails.PaymentActionSpecified = true;

                // Set Number of Recurring Payments Profile to be created
                int count = 1;
                foreach (ZNodeShoppingCartItem Item in this._shoppingCart.ShoppingCartItems)
                {
                    if (Item.Product.RecurringBillingInd == true)
                    {
                        count = count + 1;
                    }
                }

                BillingAgreementDetailsType[] BADetailsArray = new BillingAgreementDetailsType[count];
                int index = 0;
                foreach (ZNodeShoppingCartItem Item in this._shoppingCart.ShoppingCartItems)
                {
                    if (Item.Product.RecurringBillingInd == true)
                    {
                        BillingAgreementDetailsType BADetailsType = new BillingAgreementDetailsType();
                        BADetailsType.BillingType = BillingCodeType.RecurringPayments;
                        BADetailsType.BillingAgreementDescription = Item.Product.Name;
                        BADetailsArray.SetValue(BADetailsType, index++);
                    }
                }

                PaymentDetailsItemType item = null;
                List<PaymentDetailsItemType> paypalItems = new List<PaymentDetailsItemType>();
                foreach (ZNodeShoppingCartItem shoppingCartItem in _shoppingCart.ShoppingCartItems)
                {
                    item = new PaymentDetailsItemType();
                    item.Name = shoppingCartItem.Product.Name;
                    item.Description = shoppingCartItem.Product.ShortDescription;
                    item.Number = shoppingCartItem.Product.SKU;
                    item.Quantity = shoppingCartItem.Quantity.ToString();
                    //Zeon Customization : Gets the unit price of this line item-Start.
                    //item.Amount = this.GetAmount(shoppingCartItem.UnitPrice);
                    item.Amount = this.GetAmount(this.GetUnitPrice(shoppingCartItem.Product, shoppingCartItem.TieredPricing));
                    //Zeon Customization : Gets the unit price of this line item-End.
                    paypalItems.Add(item);
                }

                if (this._shoppingCart.Discount > 0)
                {
                    // Add the discount field as shopping cart item with negative value.
                    item = new PaymentDetailsItemType();
                    item.Name = "Discount";
                    item.Description = string.Empty;
                    item.Number = string.Empty;
                    item.Quantity = "1";
                    item.Amount = this.GetAmount(-this._shoppingCart.Discount);
                    paypalItems.Add(item);
                }

                if (this._shoppingCart.GiftCardAmount > 0)
                {
                    // Add the gift card amount as shopping cart item with negative value.
                    item = new PaymentDetailsItemType();
                    item.Name = "Gift Card";
                    item.Description = string.Empty;
                    item.Number = string.Empty;
                    item.Quantity = "1";
                    item.Amount = this.GetAmount(-this._shoppingCart.GiftCardAmount);
                    paypalItems.Add(item);
                }


                PaymentDetailsType paymentDetails = new PaymentDetailsType();
                paymentDetails.ItemTotal = this.GetSubTotal(paypalItems.ToArray());
                paymentDetails.ShippingTotal = this.GetAmount(_shoppingCart.ShippingCost);
                //Perficient Custom Code
                //paymentDetails.ShippingTotal = this.GetAmount(_shoppingCart.ShippingCost);
                paymentDetails.ShippingTotal = (_shoppingCart != null && _shoppingCart.IsShipngChrgsExtended) ? this.GetAmount(_shoppingCart.ExtendedShippingCost) : this.GetAmount(_shoppingCart.ShippingCost);
                paymentDetails.TaxTotal = this.GetAmount(_shoppingCart.OrderLevelTaxes);

                //Zeon Custom Code - start
                //paymentDetails.OrderTotal = this.GetAmount(this._orderTotal);
                paymentDetails.OrderTotal = this.GetAmount(
                                                         decimal.Parse(paymentDetails.ItemTotal.Value)
                                                         + decimal.Parse(paymentDetails.ShippingTotal.Value)
                                                         + decimal.Parse(paymentDetails.TaxTotal.Value)
                                                       );
                //Zeon Custom Code - end
                paymentDetails.PaymentDetailsItem = paypalItems.ToArray();

                expCheckoutRequest.SetExpressCheckoutRequestDetails.PaymentDetails = new PaymentDetailsType[] { paymentDetails };

                //Zeon Customization :: Set Address in Paypal Request :: Start
                if (_userAccount != null && _userAccount.ShippingAddress != null)
                {
                    //Set Shipping Information
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address = new AddressType();
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.Address = GetAddressDetails(_userAccount.ShippingAddress);

                    //Set Billing Information
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress = new AddressType();
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAddress = GetAddressDetails(_userAccount.BillingAddress);

                    //**Buyer Email ID
                    expCheckoutRequest.SetExpressCheckoutRequestDetails.BuyerEmail = _userAccount.EmailID;

                    if (hideChangeAddressLink)
                    {
                        expCheckoutRequest.SetExpressCheckoutRequestDetails.AddressOverride = hideChangeAddressLink.ToString();
                    }

                }
                //Zeon Customization :: Set Address in Paypal Request :: End

                expCheckoutRequest.SetExpressCheckoutRequestDetails.BillingAgreementDetails = BADetailsArray;
                expCheckoutRequest.SetExpressCheckoutRequestDetails.CancelURL = this._ECCancelUrl;
                expCheckoutRequest.SetExpressCheckoutRequestDetails.ReturnURL = this._ECRedirectUrl;

                //ZEON CUSTOMIZATIOn : Added Version for we want to use the latest version of the API operation.
                expCheckoutRequest.SetExpressCheckoutRequestDetails.AllowNote = "0";
                expCheckoutRequest.Version = System.Configuration.ConfigurationManager.AppSettings["PayPalAPIVersion"];
                expCheckoutRequest.SetExpressCheckoutRequestDetails.cppheaderimage = GetLogoPath();

                SetExpressCheckoutResponseType ECResponsetype = (SetExpressCheckoutResponseType)this.caller.Call("SetExpressCheckout", expCheckoutRequest);

                if (ECResponsetype.Ack == AckCodeType.Success || ECResponsetype.Ack == AckCodeType.SuccessWithWarning)
                {
                    paypalResponse.PayalToken = ECResponsetype.Token;
                    string hostURL = paypalResponse.HostUrl;
                    paypalResponse.HostUrl = String.Format(hostURL, this.PaypalServerUrl, ECResponsetype.Token);
                    paypalResponse.ResponseCode = "0";
                }
                else
                {
                    paypalResponse.ResponseText = ECResponsetype.Errors[0].LongMessage;
                    paypalResponse.ResponseCode = ECResponsetype.Errors[0].ErrorCode;
                }
            }
            catch (Exception ex)
            {
                paypalResponse.ResponseText = ex.Message;
                paypalResponse.ResponseCode = "-1";
            }

            return paypalResponse;
        }

        /// <summary>
        /// Get the BasicAmountType for the amount value.
        /// </summary>
        /// <param name="amount">Amount value to format.</param>
        /// <returns></returns>
        private BasicAmountType GetAmount(decimal amount)
        {
            var moneyAmount = new BasicAmountType();
            moneyAmount.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), CurrencyCode);
            moneyAmount.Value = Math.Round(amount, 2).ToString();
            return moneyAmount;
        }

        /// <summary>
        /// Get the item sub-total value.
        /// </summary>
        /// <param name="paymentDetailsItemArray">PaymentDetailsItemType arracy object.</param>
        /// <returns>Returns the formatted BasicAmountType object.</returns>
        private BasicAmountType GetSubTotal(PaymentDetailsItemType[] paymentDetailsItemArray)
        {
            decimal itemSubtotal = 0;
            foreach (PaymentDetailsItemType lineItem in paymentDetailsItemArray)
            {
                itemSubtotal += decimal.Parse(lineItem.Amount.Value) * int.Parse(lineItem.Quantity);
            }

            return this.GetAmount(itemSubtotal);
        }


        /// <summary>
        /// Get information about an Express Checkout transaction
        /// </summary>
        /// <param name="token">Token of the checkout</param>
        /// <returns>Paypal Response</returns>
        public PaypalResponse GetExpressCheckoutDetails(string token)
        {
            // Create the request object
            GetExpressCheckoutDetailsRequestType expCheckoutRequest = new GetExpressCheckoutDetailsRequestType();
            PaypalResponse paypalResponse = new PaypalResponse();

            expCheckoutRequest.Token = token;

            GetExpressCheckoutDetailsResponseType ECDetailsResponse = (GetExpressCheckoutDetailsResponseType)this.caller.Call("GetExpressCheckoutDetails", expCheckoutRequest);

            if (ECDetailsResponse.Ack == AckCodeType.SuccessWithWarning || ECDetailsResponse.Ack == AckCodeType.Success)
            {
                paypalResponse.PayerID = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.PayerID;

                // Set Shipping Address 
                ZNodeAddress _shippingAddress = new ZNodeAddress();
                _shippingAddress.City = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.CityName;
                _shippingAddress.FirstName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Name;
                if (_shippingAddress.FirstName.Length == 0)
                {
                    _shippingAddress.FirstName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.PayerName.FirstName;
                }

                _shippingAddress.LastName = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.PayerName.LastName;
                _shippingAddress.CountryCode = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Country.ToString();
                _shippingAddress.EmailId = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Payer;
                _shippingAddress.PhoneNumber = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.Phone;
                if (_shippingAddress.PhoneNumber == null)
                {
                    _shippingAddress.PhoneNumber = string.Empty;
                }

                _shippingAddress.StateCode = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.StateOrProvince;
                _shippingAddress.PostalCode = ECDetailsResponse.GetExpressCheckoutDetailsResponseDetails.PayerInfo.Address.PostalCode;

                paypalResponse.ShippingAddress = _shippingAddress;
                paypalResponse.ResponseCode = "0";
            }
            else
            {
                paypalResponse.ResponseText = ECDetailsResponse.Errors[0].LongMessage;
                paypalResponse.ResponseCode = ECDetailsResponse.Errors[0].ErrorCode;
            }

            return paypalResponse;
        }

        /// <summary>
        /// Complete an Express Checkout transaction. Returns ZNodePaymentResponse object
        /// </summary>
        /// <param name="token">token of the express checkout</param>
        /// <param name="payerID">ID of the payer</param>
        /// <returns>returns the payment response</returns>
        public ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse DoExpressCheckoutPayment(string token, string payerID)
        {
            //Zeon Customization
            token = GetTokenInUpperCase(token);

            // Create the request object
            DoExpressCheckoutPaymentRequestType expCheckoutRequest = new DoExpressCheckoutPaymentRequestType();
            PaypalResponse response = new PaypalResponse();
            ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse payment_response = new ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse();

            CurrencyCodeType currencyCodeTypeId = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), this.CurrencyCode);

            //ZEON Customization
            // get the user account from session
            ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount _userAccount = ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount();

            // Create the request details object
            expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
            expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails.Token = token;
            expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails.PayerID = payerID;

            //Zeon Customization-Start
            if (this._paymentSetting != null && this._paymentSetting.PreAuthorize)
                expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails.PaymentAction = (PaymentActionCodeType)Enum.Parse(typeof(PaymentActionCodeType), PaymentActionCodeType.Authorization.ToString());
            else
                expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails.PaymentAction = (PaymentActionCodeType)Enum.Parse(typeof(PaymentActionCodeType), this._paymentActionTypeCode);
            expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails.PaymentActionSpecified = true;
            //Zeon Customization-End
            // Set Order details
            PaymentDetailsItemType item = null;
            List<PaymentDetailsItemType> paypalItems = new List<PaymentDetailsItemType>();
            foreach (ZNodeShoppingCartItem shoppingCartItem in _shoppingCart.ShoppingCartItems)
            {
                item = new PaymentDetailsItemType();
                item.Name = shoppingCartItem.Product.Name;
                item.Description = shoppingCartItem.Product.ShortDescription;
                item.Number = shoppingCartItem.Product.SKU;
                item.Quantity = shoppingCartItem.Quantity.ToString();
                //Zeon Customization :: Start
                //item.Amount =  this.GetAmount(shoppingCartItem.UnitPrice);
                item.Amount = this.GetAmount(this.GetUnitPrice(shoppingCartItem.Product, shoppingCartItem.TieredPricing));
                //Zeon Customization :: End
                paypalItems.Add(item);
            }

            if (this._shoppingCart.Discount > 0)
            {
                // Add the discount field as shopping cart item with negative value.
                item = new PaymentDetailsItemType();
                item.Name = "Discount";
                item.Description = string.Empty;
                item.Number = string.Empty;
                item.Quantity = "1";
                item.Amount = this.GetAmount(-this._shoppingCart.Discount);
                paypalItems.Add(item);
            }

            if (this._shoppingCart.GiftCardAmount > 0)
            {
                // Add the gift card amount as shopping cart item with negative value.
                item = new PaymentDetailsItemType();
                item.Name = "Gift Card";
                item.Description = string.Empty;
                item.Number = string.Empty;
                item.Quantity = "1";
                item.Amount = this.GetAmount(-this._shoppingCart.GiftCardAmount);
                paypalItems.Add(item);
            }

            PaymentDetailsType paymentDetails = new PaymentDetailsType();
            paymentDetails.ItemTotal = this.GetSubTotal(paypalItems.ToArray());
            paymentDetails.ShippingTotal = this.GetAmount(_shoppingCart.ShippingCost);
            paymentDetails.TaxTotal = this.GetAmount(_shoppingCart.SalesTax);
            //Zeon custom code-start
            //paymentDetails.OrderTotal = this.GetAmount(this._orderTotal);
            paymentDetails.OrderTotal = this.GetAmount(
                                                        decimal.Parse(paymentDetails.ItemTotal.Value)
                                                        + decimal.Parse(paymentDetails.ShippingTotal.Value)
                                                        + decimal.Parse(paymentDetails.TaxTotal.Value)
                                                      );
            //Zeon custom code-end
            paymentDetails.PaymentDetailsItem = paypalItems.ToArray();

            #region Zeon Customization
            //zeon custom code start //29/05/14//The Znode Order ID needs passed to PayPal as the InvoiceID field. 
            if (ZNodeOrderID != null && ZNodeOrderID > 0)
                paymentDetails.InvoiceID = ZNodeOrderID.ToString();

            string bnCode = GetBNCode();
            if (!string.IsNullOrEmpty(bnCode))
            {
                expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails.ButtonSource = bnCode;
            }

            if (_userAccount != null && _userAccount.ShippingAddress != null)
            {
                paymentDetails.ShipToAddress = new AddressType();
                paymentDetails.ShipToAddress = GetAddressDetails(_userAccount.ShippingAddress);
            }

            //Update : 17 Nov 2011 : Added Version for we want to use the latest version of the API operation.
            expCheckoutRequest.Version = System.Configuration.ConfigurationManager.AppSettings["PayPalAPIVersion"];

            #endregion

            expCheckoutRequest.DoExpressCheckoutPaymentRequestDetails.PaymentDetails = new PaymentDetailsType[] { paymentDetails };

            // Create the response object
            DoExpressCheckoutPaymentResponseType ECPaymentResponse = (DoExpressCheckoutPaymentResponseType)this.caller.Call("DoExpressCheckoutPayment", expCheckoutRequest);

            if (ECPaymentResponse.Ack == AckCodeType.Success || ECPaymentResponse.Ack == AckCodeType.SuccessWithWarning)
            {
                payment_response.ResponseText = ECPaymentResponse.Ack.ToString();
                response.PaymentStatus = ECPaymentResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].PaymentStatus.ToString();
                payment_response.PaymentStatus = response.GetPaymentStatus();
                payment_response.TransactionId = ECPaymentResponse.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].TransactionID.ToString();
                payment_response.IsSuccess = true;

                // Create Recurring Payments Profile
                if (payment_response.IsSuccess)
                {
                    // Code for Creating Recurring Payments Profile
                    PaymentDetailsItemType[] Ritems = new PaymentDetailsItemType[this._shoppingCart.Count];
                    int index = 0;
                    decimal RecurringTotal = 0.00M;
                    bool subscriptionResult = true;
                    string responseText = "This gateway does not support all of the billing periods that you have set on products. Please be sure to update the billing period on all your products.";
                    int counter = 0;

                    // Loop through the Order Line Items
                    foreach (OrderLineItem orderLineItem in this._orderLineItems)
                    {
                        ZNodeShoppingCartItem cartItem = this._shoppingCart.ShoppingCartItems[counter++];

                        decimal productPrice = (cartItem.TieredPricing + cartItem.ShippingCost + cartItem.TaxCost) * cartItem.Quantity;

                        if (cartItem.Product.RecurringBillingInd && subscriptionResult)
                        {
                            PaypalRecurringBillingInfo recurringBilling = new PaypalRecurringBillingInfo();
                            recurringBilling.InitialAmount = Math.Round(cartItem.Product.RecurringBillingInitialAmount, 2);
                            recurringBilling.InstallmentInd = cartItem.Product.RecurringBillingInstallmentInd;
                            recurringBilling.Period = cartItem.Product.RecurringBillingPeriod;
                            recurringBilling.TotalCycles = cartItem.Product.RecurringBillingTotalCycles;
                            recurringBilling.Frequency = cartItem.Product.RecurringBillingFrequency;
                            recurringBilling.Amount = Math.Round(productPrice, 2);
                            recurringBilling.ProfileName = cartItem.Product.SKU + " - " + orderLineItem.OrderLineItemID.ToString();
                            RecurringTotal = RecurringTotal + productPrice;

                            ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse val = this.CreateRecurringPaymentsProfile(token, payerID, recurringBilling, cartItem.Product.Name);

                            if (val.IsSuccess)
                            {
                                val.ReferenceId = orderLineItem.OrderLineItemID;
                                payment_response.RecurringBillingSubscriptionResponse.Add(val);
                            }
                            else
                            {
                                subscriptionResult &= false;
                                responseText = val.ResponseText + val.ResponseCode;
                                break;
                            }
                        }

                        foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                        {
                            index = 0;

                            foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                            {
                                if (addOnValue.RecurringBillingInd && subscriptionResult && addOnValue.FinalPrice > 0)
                                {
                                    productPrice = addOnValue.FinalPrice * cartItem.Quantity;

                                    PaypalRecurringBillingInfo recurringBilling = new PaypalRecurringBillingInfo();
                                    recurringBilling.InitialAmount = Math.Round(addOnValue.RecurringBillingInitialAmount, 2);
                                    recurringBilling.InstallmentInd = addOnValue.RecurringBillingInstallmentInd;
                                    recurringBilling.Period = addOnValue.RecurringBillingPeriod;
                                    recurringBilling.TotalCycles = addOnValue.RecurringBillingTotalCycles;
                                    recurringBilling.Frequency = addOnValue.RecurringBillingFrequency;
                                    recurringBilling.Amount = Math.Round(productPrice, 2);
                                    recurringBilling.ProfileName = addOnValue.SKU + " - " + orderLineItem.OrderLineItemID.ToString();

                                    ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse val = this.CreateRecurringPaymentsProfile(token, payerID, recurringBilling, cartItem.Product.Name);

                                    if (val.IsSuccess)
                                    {
                                        val.ParentLineItemId = orderLineItem.OrderLineItemID;
                                        val.ReferenceId = orderLineItem.OrderLineItemCollection[index].OrderLineItemID;
                                        payment_response.RecurringBillingSubscriptionResponse.Add(val);
                                    }
                                    else
                                    {
                                        subscriptionResult &= false;
                                        responseText = val.ResponseText + val.ResponseCode;
                                        break;
                                    }
                                }

                                if (!subscriptionResult)
                                {
                                    break;
                                }

                                index++;
                            }
                        }
                    }

                    // if any subscription fails, then revert all the transactions and Cancel the Subscription Created
                    if (!subscriptionResult)
                    {
                        foreach (ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse subscriptionResp in payment_response.RecurringBillingSubscriptionResponse)
                        {
                            // Cancel Subscription
                            ManageRecurringPaymentsProfileStatusRequestType recPaymentsRequest = new ManageRecurringPaymentsProfileStatusRequestType();
                            recPaymentsRequest.ManageRecurringPaymentsProfileStatusRequestDetails = new ManageRecurringPaymentsProfileStatusRequestDetailsType();
                            recPaymentsRequest.ManageRecurringPaymentsProfileStatusRequestDetails.ProfileID = subscriptionResp.SubscriptionId;
                            recPaymentsRequest.ManageRecurringPaymentsProfileStatusRequestDetails.Action = StatusChangeActionType.Cancel;

                            // Create the response object
                            ManageRecurringPaymentsProfileStatusResponseType recPaymaentProfileresponse = (ManageRecurringPaymentsProfileStatusResponseType)this.caller.Call("ManageRecurringPaymentsProfileStatus", recPaymentsRequest);
                            if (recPaymaentProfileresponse.Ack == AckCodeType.Success || recPaymaentProfileresponse.Ack == AckCodeType.SuccessWithWarning)
                            {
                                payment_response.ResponseText = recPaymaentProfileresponse.Ack.ToString();
                            }
                        }

                        if (!string.IsNullOrEmpty(payment_response.TransactionId))
                        {
                            // Refund Payment
                            RefundTransactionRequestType concreteRequest = new RefundTransactionRequestType();
                            string refundType = "Partial";
                            decimal RefundAmount = this.OrderTotal - RecurringTotal;
                            if ((RefundAmount > 0 && RefundAmount.ToString().Length > 0) && refundType.Equals("Partial"))
                            {
                                BasicAmountType amtType = new BasicAmountType();
                                amtType.Value = RefundAmount.ToString("N2");
                                amtType.currencyID = CurrencyCodeType.USD;
                                concreteRequest.Amount = amtType;
                                concreteRequest.RefundType = RefundType.Partial;
                            }
                            else
                            {
                                concreteRequest.RefundType = RefundType.Full;
                            }

                            concreteRequest.RefundTypeSpecified = true;
                            concreteRequest.TransactionID = payment_response.TransactionId;
                            concreteRequest.Memo = "Test note";

                            // Execute the API operation and obtain the response.
                            RefundTransactionResponseType refundTransResponse = new RefundTransactionResponseType();
                            refundTransResponse = (RefundTransactionResponseType)this.caller.Call("RefundTransaction", concreteRequest);

                            if (refundTransResponse.Ack == AckCodeType.Success || refundTransResponse.Ack == AckCodeType.SuccessWithWarning)
                            {
                                payment_response.ResponseText = refundTransResponse.Ack.ToString();
                            }
                        }

                        payment_response = new ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse();
                        payment_response.ResponseCode = "-1";
                        payment_response.ResponseText = responseText;
                    }
                }
            }
            else
            {
                payment_response.IsSuccess = true;
                payment_response.PaymentStatus = ZNode.Libraries.ECommerce.Entities.ZNodePaymentStatus.CREDIT_PENDING;
            }

            return payment_response;
        }

        /// <summary>
        /// Method creates Recurring Payments Profile
        /// </summary>
        /// <param name="token">token of the payment</param>
        /// <param name="payerID">ID of the payer</param>
        /// <param name="recurringBillingInfo">Recurring bill information</param>
        /// <param name="ProfileDesc">profile description</param>
        /// <returns>Returns ZNodeSubscriptionResponse object</returns>
        public ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse CreateRecurringPaymentsProfile(string token, string payerID, PaypalRecurringBillingInfo recurringBillingInfo, string ProfileDesc)
        {
            // string token,string payerID, DateTime date, string amount, int BF, BillingPeriodType BP, CurrencyCodeType currencyCodeType,PaymentSetting PaymentSetting, string ECRedirectUrl, string ECReturnUrl)
            PaypalResponse response = new PaypalResponse();
            ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse subscription_response = new ZNode.Libraries.ECommerce.Entities.ZNodeSubscriptionResponse();

            DateTime currentdate = System.DateTime.Now.Date;

            this._shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            this._paymentSetting = PaymentSetting;

            // create the request object
            CreateRecurringPaymentsProfileRequestType recPaymentRequest = new CreateRecurringPaymentsProfileRequestType();

            // Add request-specific fields to the request object.
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails = new CreateRecurringPaymentsProfileRequestDetailsType();
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.Token = token;
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.RecurringPaymentsProfileDetails = new RecurringPaymentsProfileDetailsType();
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.RecurringPaymentsProfileDetails.BillingStartDate = currentdate;
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails = new ScheduleDetailsType();
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod = new BillingPeriodDetailsType();
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.Amount = new BasicAmountType();
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.Amount.Value = recurringBillingInfo.Amount.ToString("N2");
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.Amount.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), this.CurrencyCode);
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.BillingFrequency = int.Parse(recurringBillingInfo.Frequency.ToString());
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.BillingPeriod = (BillingPeriodType)Enum.Parse(typeof(BillingPeriodType), response.GetPaypalBillingPeriod(recurringBillingInfo.Period));
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.TotalBillingCycles = recurringBillingInfo.TotalCycles;
            recPaymentRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.Description = ProfileDesc;

            // Execute the API operation and obtain the response.
            CreateRecurringPaymentsProfileResponseType refundTransResponse = new CreateRecurringPaymentsProfileResponseType();
            refundTransResponse = (CreateRecurringPaymentsProfileResponseType)this.caller.Call("CreateRecurringPaymentsProfile", recPaymentRequest);
            if (refundTransResponse.Ack == AckCodeType.SuccessWithWarning || refundTransResponse.Ack == AckCodeType.Success)
            {
                subscription_response.ResponseText = refundTransResponse.Ack.ToString();
                subscription_response.SubscriptionId = refundTransResponse.CreateRecurringPaymentsProfileResponseDetails.ProfileID.ToString();
                subscription_response.IsSuccess = true;
            }
            else
            {
                subscription_response.ResponseText = refundTransResponse.Errors[0].LongMessage;
                subscription_response.ResponseCode = refundTransResponse.Errors[0].ErrorCode;
            }

            return subscription_response;
        }
        #endregion

        #region private methods
        /// <summary>
        /// Initiate Merchant details for an API
        /// </summary>
        /// <param name="apiUsername">API User Name</param>
        /// <param name="apiPassword">API Password</param>
        /// <param name="signature">API Signature</param>
        /// <param name="IsTestMode">Test mode or not</param>
        /// <returns>Returns the Profile</returns>
        private static IAPIProfile CreateAPIProfile(string apiUsername, string apiPassword, string signature, bool IsTestMode)
        {
            IAPIProfile profile = ProfileFactory.createSignatureAPIProfile();
            profile.APIUsername = apiUsername;
            profile.APIPassword = apiPassword;
            profile.APISignature = signature;
            profile.Subject = string.Empty;

            if (!IsTestMode)
            {
                // Test mode is not enabled,then set Profile Environment as "live" - Production Server
                profile.Environment = "live";
            }

            return profile;
        }
        #endregion
    }
}

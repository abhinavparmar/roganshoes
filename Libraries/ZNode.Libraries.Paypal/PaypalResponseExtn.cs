﻿using System;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;


namespace ZNode.Libraries.Paypal
{
    /// <summary>
    /// Paypal response
    /// </summary>
    public partial class PaypalResponse : ZNodeBusinessBase
    {
        public bool AddressStatus { get; set; }
    }
}

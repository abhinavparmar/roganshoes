﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Zeon.Libraries.Elmah
{
    public class ElmahErrorManager
    {
        #region Public Property

        public static bool IsAddToCartLogEnable
        {
            get
            {
                bool isAddToCartLogEnable = false;
                if (ConfigurationManager.AppSettings["EnableAddToCartLogs"] != null)
                {
                    bool.TryParse(ConfigurationManager.AppSettings["EnableAddToCartLogs"].ToString(), out isAddToCartLogEnable);
                }
                return isAddToCartLogEnable;
            }
        }

        #endregion


        #region Methods


        /// <summary>
        /// Log exception in Elmah 
        /// </summary>
        /// <param name="ex"></param>
        public static void Log(Exception ex)
        {
            try
            {
                ErrorSignal.FromCurrentContext().Raise(new Exception("HANDLED", ex));
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }
        }

        /// <summary>
        /// Log custom  error message in elmah
        /// </summary>
        /// <param name="errorMessage"></param>
        public static void Log(string errorMessage)
        {
            Log(new Exception(errorMessage));
        }

        public static void CreateDebugLog(string strlog)
        {
            if (IsAddToCartLogEnable)
            {
                Log(strlog);
            }
        }
        #endregion
    }
}

using System;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.SEO
{
    /// <summary>
    /// Provides meta tag functions.
    /// </summary>
    public class ZNodeMetaTags : ZNodeBusinessBase
    {
        private readonly string NameTag = "<NAME>";
        private readonly string ProductNumTag = "<PRODUCT_NUM>";
        private readonly string SkuTag = "<SKU>";
        private readonly string BrandTag = "<BRAND>";

        #region Public Methods
        /// <summary>
        /// Sets the product Title Meta tags on pages.
        /// </summary>
        /// <param name="product">The current product Title.</param>
        /// <returns>Original Title.</returns>
        public string ProductTitle(ZNodeProductBaseEntity product)
        {
            string defaultProductTitle = ZNodeConfigManager.SiteConfig.SeoDefaultProductTitle;
            if (!string.IsNullOrEmpty(product.SEOTitle))
            {
                defaultProductTitle = product.SEOTitle;
            }
            else if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SeoDefaultProductTitle))
            {
                defaultProductTitle = ZNodeConfigManager.SiteConfig.SeoDefaultProductTitle;
            }
            else
            {
                defaultProductTitle = product.Name;
            }

            defaultProductTitle = defaultProductTitle.Replace(this.NameTag, product.Name);
            defaultProductTitle = defaultProductTitle.Replace(this.ProductNumTag, product.ProductNum);
            defaultProductTitle = defaultProductTitle.Replace(this.SkuTag, product.SKU);
            defaultProductTitle = defaultProductTitle.Replace(this.BrandTag, product.ManufacturerName);

            return defaultProductTitle;
        }

        /// <summary>
        /// Sets the product Description Meta tags on pages.
        /// </summary>
        /// <param name="product">The current product Description.</param>
        /// <returns>product Original Description.</returns>
        public string ProductDescription(ZNodeProductBaseEntity product)
        {
            string defaultProductDescription = ZNodeConfigManager.SiteConfig.SeoDefaultProductDescription;
            if (!string.IsNullOrEmpty(product.SEODescription))
            {
                defaultProductDescription = product.SEODescription;
            }
            else if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SeoDefaultProductDescription))
            {
                defaultProductDescription = ZNodeConfigManager.SiteConfig.SeoDefaultProductDescription;
            }
            else
            {
                defaultProductDescription = product.Description;
            }

            defaultProductDescription = defaultProductDescription.Replace(this.NameTag, product.Name);
            defaultProductDescription = defaultProductDescription.Replace(this.ProductNumTag, product.ProductNum);
            defaultProductDescription = defaultProductDescription.Replace(this.SkuTag, product.SKU);
            defaultProductDescription = defaultProductDescription.Replace(this.BrandTag, product.ManufacturerName);

            return defaultProductDescription;
        }

        /// <summary>
        /// Sets the product Keywords Meta tags on pages.
        /// </summary>
        /// <param name="product">Product Details</param>
        /// <returns>Product Original Keyword.</returns>
        public string ProductKeywords(ZNodeProductBaseEntity product)
        {
            string defaultProductKeyword = ZNodeConfigManager.SiteConfig.SeoDefaultProductKeyword;
            if (!string.IsNullOrEmpty(product.SEOKeywords))
            {
                defaultProductKeyword = product.SEOKeywords;
            }
            else if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SeoDefaultProductKeyword))
            {
                defaultProductKeyword = ZNodeConfigManager.SiteConfig.SeoDefaultProductKeyword;
            }
            else
            {
                defaultProductKeyword = product.Name;
            }

            defaultProductKeyword = defaultProductKeyword.Replace(this.NameTag, product.Name);
            defaultProductKeyword = defaultProductKeyword.Replace(this.ProductNumTag, product.ProductNum);
            defaultProductKeyword = defaultProductKeyword.Replace(this.SkuTag, product.SKU);
            defaultProductKeyword = defaultProductKeyword.Replace(this.BrandTag, product.ManufacturerName);

            return defaultProductKeyword;
        }

        /// <summary>
        /// Sets the Category Title Meta tags on pages.
        /// </summary>
        /// <param name="Category">The current Category Title.</param>
        /// <returns>Category Original Title.</returns>
        public string CategoryTitle(ZNodeCategoryBase Category)
        {
            string defaultCategoryTitle = ZNodeConfigManager.SiteConfig.SeoDefaultCategoryTitle;
            if (!string.IsNullOrEmpty(Category.SEOTitle))
            {
                defaultCategoryTitle = Category.SEOTitle;
            }
            else if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SeoDefaultCategoryTitle))
            {
                defaultCategoryTitle = ZNodeConfigManager.SiteConfig.SeoDefaultCategoryTitle;
            }
            else
            {
                //defaultCategoryTitle = Category.Name;//Znode old code
                defaultCategoryTitle = Category.Title;//Zeon Custom code
            }

            //defaultCategoryTitle = defaultCategoryTitle.Replace(this.NameTag, Category.Name);//Znode old code
            defaultCategoryTitle = defaultCategoryTitle.Replace(this.NameTag, Category.Title);//Zeon Custom code
            return defaultCategoryTitle;
        }

        /// <summary>
        /// Sets the Category Description Meta tags on pages.
        /// </summary>
        /// <param name="Category">The current Category Description.</param>
        /// <returns>Category Original Description.</returns>
        public string CategoryDescription(ZNodeCategoryBase Category)
        {
            string defaultCategoryDescription = ZNodeConfigManager.SiteConfig.SeoDefaultCategoryDescription;
            if (!string.IsNullOrEmpty(Category.SEODescription))
            {
                defaultCategoryDescription = Category.SEODescription;
            }
            else if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SeoDefaultCategoryDescription))
            {
                defaultCategoryDescription = ZNodeConfigManager.SiteConfig.SeoDefaultCategoryDescription;
            }
            else
            {
                defaultCategoryDescription = Category.Name;
            }

            defaultCategoryDescription = defaultCategoryDescription.Replace(this.NameTag, Category.Name);
            return defaultCategoryDescription;
        }

        /// <summary>
        /// Sets the Category Keyword Meta tags on pages.
        /// </summary>
        /// <param name="Category">The current Category Keyword .</param>
        /// <returns>Category Original Keyword.</returns>
        public string CategoryKeywords(ZNodeCategoryBase Category)
        {
            string defaultCategoryKeywords = ZNodeConfigManager.SiteConfig.SeoDefaultCategoryKeyword;
            if (!string.IsNullOrEmpty(Category.SEOKeywords))
            {
                defaultCategoryKeywords = Category.SEOKeywords;
            }
            else if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SeoDefaultCategoryKeyword))
            {
                defaultCategoryKeywords = ZNodeConfigManager.SiteConfig.SeoDefaultCategoryKeyword;
            }
            else
            {
                defaultCategoryKeywords = Category.Name;
            }

            defaultCategoryKeywords = defaultCategoryKeywords.Replace(this.NameTag, Category.Name);
            return defaultCategoryKeywords;
        }

        /// <summary>
        /// Sets the content Title Meta tags on pages.
        /// </summary>
        /// <param name="content">The current content Title.</param>
        /// <returns>content Original Title.</returns>
        public string ContentTitle(ContentPage content)
        {
            string defaultContentTitle = ZNodeConfigManager.SiteConfig.SeoDefaultContentTitle;
            if (!string.IsNullOrEmpty(content.SEOTitle))
            {
                defaultContentTitle = content.SEOTitle;
            }
            else if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SeoDefaultContentTitle))
            {
                defaultContentTitle = ZNodeConfigManager.SiteConfig.SeoDefaultContentTitle;
            }
            else
            {
                defaultContentTitle = content.Name;
            }

            defaultContentTitle = defaultContentTitle.Replace(this.NameTag, Convert.ToString(content.Name));
            return defaultContentTitle;
        }

        /// <summary>
        /// Sets the content Description Meta tags on pages.
        /// </summary>
        /// <param name="content">The current content Description.</param>
        /// <returns>content Original Description.</returns>
        public string ContentDescription(ContentPage content)
        {
            string defaultContentDescription = ZNodeConfigManager.SiteConfig.SeoDefaultContentDescription;

            if (!string.IsNullOrEmpty(content.SEOMetaDescription))
            {
                defaultContentDescription = content.SEOMetaDescription;
            }
            else if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SeoDefaultContentDescription))
            {
                defaultContentDescription = ZNodeConfigManager.SiteConfig.SeoDefaultContentDescription;
            }
            else
            {
                defaultContentDescription = content.Name;
            }

            defaultContentDescription = defaultContentDescription.Replace(this.NameTag, Convert.ToString(content.Name));
            return defaultContentDescription;
        }

        /// <summary>
        /// Sets the content Keyword Meta tags on pages.
        /// </summary>
        /// <param name="content">The current content Keyword.</param>
        /// <returns>content Original Keyword.</returns>
        public string ContentKeywords(ContentPage content)
        {
            string defaultContentKeywords = ZNodeConfigManager.SiteConfig.SeoDefaultContentKeyword;
            if (!string.IsNullOrEmpty(content.SEOMetaKeywords))
            {
                defaultContentKeywords = content.SEOMetaKeywords;
            }
            else if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SeoDefaultContentKeyword))
            {
                defaultContentKeywords = ZNodeConfigManager.SiteConfig.SeoDefaultContentKeyword;
            }
            else
            {
                defaultContentKeywords = content.Name;
            }

            defaultContentKeywords = defaultContentKeywords.Replace(this.NameTag, Convert.ToString(content.Name));

            return defaultContentKeywords;
        }

        /// <summary>
        /// Sets the content Additional Meta tags on pages.
        /// </summary>
        /// <param name="content">The current content Description.</param>
        /// <returns>content Original Description.</returns>
        public string ContentAdditionalMeta(ContentPage content)
        {
            if (!string.IsNullOrEmpty(content.MetaTagAdditional))
            {
                return content.MetaTagAdditional;
            }
            else
            {
                return String.Empty;
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.DataAccess.Entities;
using System.Web;

namespace ZNode.Libraries.TwoCheckout
{
    class TwoCOOption
    {
        TwoCOAPIInfo apiInfo = new TwoCOAPIInfo();

        public TwoCOResponse Update(int znodeAddOnId)
        {
            TwoCOCommon common = new TwoCOCommon();
            AddOnService addOnService = new AddOnService();
            AddOn addOn = addOnService.GetByAddOnID(znodeAddOnId);

            StringBuilder query = new StringBuilder();
            query.AppendFormat("&option_name={0}", HttpUtility.UrlEncode(addOn.Name));

            AddOnValueService addOnValueService = new AddOnValueService();
            foreach (AddOnValue addOnValue in addOnValueService.GetByAddOnID(znodeAddOnId))
            {
                if (addOnValue.ExternalProductAPIID != null)
                {
                    query.Append("&option_value_id=" + HttpUtility.UrlEncode(addOnValue.ExternalProductAPIID));
                }

                query.AppendFormat("&option_value_name={0}", HttpUtility.UrlEncode(addOnValue.Name));
                query.AppendFormat("&option_value_surcharge={0}", HttpUtility.UrlEncode(addOnValue.SalePrice.ToString()));
            }

            TwoCOResponse response;

            if (addOn.ExternalAPIID != null)
            {
                query.Append("&option_id=" + HttpUtility.UrlEncode(addOn.ExternalAPIID.ToString()));
                response = common.Submit(apiInfo.UpdateOption, query.ToString());
            }
            else
            {
                response = common.Submit(apiInfo.CreateOption, query.ToString());

                // Update API return IDs in our database.
                if (response.IsSuccess)
                {
                    addOn.ExternalAPIID = response.Response.Tables[0].Rows[0]["option_id"].ToString();

                    addOnService.Update(addOn);
                }
            }

            response = Select(addOn.ExternalAPIID);

            if (response.IsSuccess)
            {
                foreach (AddOnValue addOnValue in addOnValueService.GetByAddOnID(znodeAddOnId))
                {
                    addOnValue.ExternalProductAPIID = response.Response.Tables[1].Select(string.Format("[option_name]='{0}'", addOnValue.Name))[0]["option_value_id"].ToString();

                    addOnValueService.Update(addOnValue);
                }
            }

            return response;
        }

        public TwoCOResponse Select(string externalAPIID)
        {
            TwoCOCommon common = new TwoCOCommon();

            StringBuilder query = new StringBuilder();
            query.AppendFormat("&option_id={0}", HttpUtility.UrlEncode(externalAPIID));

            return common.Submit(apiInfo.DetailOption, query.ToString());
        }

        public TwoCOResponse Delete(string externalAPIID)
        {
            TwoCOCommon common = new TwoCOCommon();

            StringBuilder query = new StringBuilder();
            query.AppendFormat("&option_id={0}", HttpUtility.UrlEncode(externalAPIID));

            return common.Submit(apiInfo.DeleteOption, query.ToString());
        }
    }
}

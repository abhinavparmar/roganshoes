﻿using System;
using System.Collections.Generic;
using System.Text;
using ZNode.Libraries.DataAccess.Entities;
using System.Web;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using System.Web.Security;
using System.IO;
using System.Text.RegularExpressions;

namespace ZNode.Libraries.TwoCheckout
{
    public class ZNode2COPortal
    {
        TwoCOAPIInfo apiInfo = new TwoCOAPIInfo();

        public TwoCOResponse DetailCompanyInfo(string apiUsername, string apiPassword)
        {
            TwoCOCommon common = new TwoCOCommon();
            TwoCOResponse response = new TwoCOResponse();
            response = common.Submit(apiInfo.DetailCompanyInfo, string.Empty, apiUsername, apiPassword);
            return response;
        }

        public TwoCOResponse DetailContactInfo(string apiUsername, string apiPassword)
        {
            TwoCOCommon common = new TwoCOCommon();
            TwoCOResponse response = new TwoCOResponse();
            response = common.Submit(apiInfo.DetailContactInfo, string.Empty, apiUsername, apiPassword);           
            return response;
        }

        public TwoCOResponse CreateAccount(string VendorID, string apiUsername, string apiPassword)
        {
            TwoCOResponse detailCompanyInfo = DetailCompanyInfo(apiUsername, apiPassword);
            TwoCOResponse detailContactInfo = DetailContactInfo(apiUsername, apiPassword);
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();

            TwoCOResponse reply = new TwoCOResponse();
            reply.IsSuccess = false;
            reply.Response = null;

            try
            {
                if (detailCompanyInfo.Response != null &&
                    detailCompanyInfo.Response.Tables.Count > 0 &&
                    detailCompanyInfo.Response.Tables[0].Rows.Count > 0)
                {
                    ZNodeEncryption encrypt = new ZNodeEncryption();
                    string UserName = VendorID;
                    string Password = encrypt.EncryptData(VendorID);
                    string Email = detailContactInfo.Response.Tables[0].Rows[0]["office_email"].ToString();

                    ZNode.Libraries.DataAccess.Service.SupplierService supplierService = new ZNode.Libraries.DataAccess.Service.SupplierService();

                    // check if loginName already exists in DB
                    ZNodeUserAccount userAccount = new ZNodeUserAccount();
                    if (!userAccount.IsLoginNameAvailable(ZNodeConfigManager.SiteConfig.PortalID, UserName.Trim()))
                    {
                        log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName, null, null, "User name already exists", null);
                        return new TwoCOResponse();
                    }

                    // create user membership
                    MembershipCreateStatus memStatus;
                    MembershipUser user = Membership.CreateUser(UserName.Trim(), Password.Trim(), Email.Trim(), null, null, true, out memStatus);                    

                    if (memStatus != MembershipCreateStatus.Success)
                    {
                        log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName);
                        return new TwoCOResponse();
                    }

                    log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, UserName);
                    user.ResetPassword();

                    // create the user account
                    ZNodeUserAccount newUserAccount = new ZNodeUserAccount();
                    newUserAccount.UserID = (Guid)user.ProviderUserKey;
                    newUserAccount.EmailOptIn = false;

                    // copy info to billing
                    Address billingAddress = new Address();
                    billingAddress.FirstName = detailCompanyInfo.Response.Tables[0].Rows[0]["vendor_name"].ToString();
                    billingAddress.LastName = string.Empty;
                    billingAddress.CompanyName = detailCompanyInfo.Response.Tables[0].Rows[0]["site_title"].ToString();
                    billingAddress.Street = detailContactInfo.Response.Tables[0].Rows[0]["physical_address_1"].ToString();
                    billingAddress.Street1 = detailContactInfo.Response.Tables[0].Rows[0]["physical_address_2"].ToString();
                    billingAddress.City = detailContactInfo.Response.Tables[0].Rows[0]["physical_city"].ToString();
                    billingAddress.StateCode = detailContactInfo.Response.Tables[0].Rows[0]["physical_state"].ToString();
                    billingAddress.PostalCode = detailContactInfo.Response.Tables[0].Rows[0]["physical_postal_code"].ToString();
                    billingAddress.CountryCode = detailContactInfo.Response.Tables[0].Rows[0]["physical_country_code"].ToString();
                    billingAddress.PhoneNumber = detailContactInfo.Response.Tables[0].Rows[0]["office_phone"].ToString();
                    newUserAccount.EmailID = detailContactInfo.Response.Tables[0].Rows[0]["office_email"].ToString();
                    newUserAccount.BillingAddress = billingAddress;
                    newUserAccount.ExternalAccountNo = VendorID;

                    newUserAccount.ActiveInd = true;
                    newUserAccount.AccountTypeID = 0;

                    // copy info to shipping
                    Address shippingAddress = new Address();
                    newUserAccount.ShippingAddress = shippingAddress;
                    newUserAccount.CreateUser = HttpContext.Current.User.Identity.Name;

                    // register account
                    newUserAccount.AddUserAccount();

                    Roles.AddUserToRole(user.UserName, "VENDOR");                    

                    SendMail(user, newUserAccount);

                    reply.IsSuccess = true;
                    reply.ErrorCode = "0";

                    reply.Response = new System.Data.DataSet();
                    reply.Response.Tables.Add(new System.Data.DataTable());
                    reply.Response.Tables[0].Columns.Add(new System.Data.DataColumn("AccountID", typeof(string)));
                    reply.Response.Tables[0].Rows.Add(newUserAccount.AccountID.ToString().Split(','));
                    
                }
                else
                {
                    reply.IsSuccess = false;
                    reply.ErrorCode = "1";
                    reply.ErrorDescription = "Failed to get company & contact information";
                }
            }
            catch (Exception ex)
            {
                reply.ErrorCode = "1";
                reply.ErrorDescription = ex.ToString();
            }

            //Add New Contact            
            return reply;
        }


        /// <summary>
        /// Send mail to user with 
        /// </summary>
        protected void SendMail(MembershipUser user, ZNodeUserAccount account)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = "Account Information";

            string LoginPassword = string.Empty;           

            StreamReader rw;
            rw = new StreamReader(HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "VendorUserEmailTemplate.htm"));
            string body = rw.ReadToEnd();
            rw.Close();
            Regex rx1 = new Regex("#USERID#", RegexOptions.IgnoreCase);
            body = rx1.Replace(body, user.UserName);

            Regex rx2 = new Regex("#PASSWORD#", RegexOptions.IgnoreCase);
            body = rx2.Replace(body, LoginPassword);

            Regex rx3 = new Regex("#USERNAME#", RegexOptions.IgnoreCase);
            body = rx3.Replace(body, account.BillingAddress.FirstName);

            Regex rx4 = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            body = rx4.Replace(body, ZNodeConfigManager.SiteConfig.StoreName);

            Regex rx5 = new Regex("#CUSTOMERSERVICEPHONE#", RegexOptions.IgnoreCase);
            body = rx5.Replace(body, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

            Regex rx6 = new Regex("#CUSTOMERSERVICEMAIL#", RegexOptions.IgnoreCase);
            body = rx6.Replace(body, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

            try
            {
                ZNodeEmail.SendEmail(user.Email, senderEmail, String.Empty, subject, body, true);
            }
            catch
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format("Could not send the temporary password for user \"{0}\". Check that the email address is correct.", user.UserName));
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ZNode.Libraries.TwoCheckout
{
    /// <summary>
    /// Represents the 2Checoutout response class.
    /// </summary>
    public class TwoCOResponse
    {
        private bool _IsSuccess = false;
        private DataSet _Response = new DataSet();
        private string _ErrorCode = string.Empty;
        private string _ErrorDescription = string.Empty;

        /// <summary>
        /// Gets or Sets the 2Checkout call result status
        /// </summary>
        public bool IsSuccess
        {
            get
            {
                return _IsSuccess;
            }
            set
            {
                _IsSuccess = value;
            }
        }

        /// <summary>
        /// Gets or Sets the 2Checkout response dataset
        /// </summary>
        public DataSet Response
        {
            get
            {
                return _Response;
            }
            set
            {
                _Response = value;
            }
        }


        /// <summary>
        /// Gets or sets the 2Checkout error code if call fails.
        /// </summary>
        public string ErrorCode
        {
            get
            {
                return _ErrorCode;
            }

            set
            {
                _ErrorCode = value;
            }

        }

        /// <summary>
        /// Gets or sets 2Checkout error message if call fails.
        /// </summary>
        public string ErrorDescription
        {
            get
            {
                return _ErrorDescription;
            }
            set
            {
                _ErrorDescription = value;
            }
        }

    }
}

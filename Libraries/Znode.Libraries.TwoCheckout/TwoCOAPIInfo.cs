﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace ZNode.Libraries.TwoCheckout
{
    /// <summary>
    /// TwoCO Checkout API Information
    /// </summary>
    public class TwoCOAPIInfo
    {
        #region Private Variables
        XmlDocument document = null;
        string username;
        string password;
        string detailCompanyInfo;
        string detailContactInfo;
        string detailPendingPayment;
        string listPayments;
        string detailSale;
        string listSales;
        string refundInvoice;
        string refundLineItem;
        string stopLineItemRecurring;
        string markShipped;
        string createComment;
        string detailProduct;
        string listProducts;
        string createProduct;
        string updateProduct;
        string deleteProduct;
        string detailOption;
        string listOptions;
        string createOption;
        string updateOption;
        string deleteOption;
        string detailCoupon;
        string listCoupons;
        string createCoupon;
        string updateCoupon;
        string deleteCoupon;

        #endregion

        #region Properties

        public string Username { get { return username; } }
        public string Password { get { return password; } }
        public string DetailCompanyInfo { get { return detailCompanyInfo; } }
        public string DetailContactInfo { get { return detailContactInfo; } }
        public string DetailPendingPayment { get { return detailPendingPayment; } }
        public string ListPayments { get { return listPayments; } }
        public string DetailSale { get { return detailSale; } }
        public string ListSales { get { return listSales; } }
        public string RefundInvoice { get { return refundInvoice; } }
        public string RefundLineItem { get { return refundLineItem; } }
        public string StopLineItemRecurring { get { return stopLineItemRecurring; } }
        public string MarkShipped { get { return markShipped; } }
        public string CreateComment { get { return createComment; } }
        public string DetailProduct { get { return detailProduct; } }
        public string ListProducts { get { return listProducts; } }
        public string CreateProduct { get { return createProduct; } }
        public string UpdateProduct { get { return updateProduct; } }
        public string DeleteProduct { get { return deleteProduct; } }
        public string DetailOption { get { return detailOption; } }
        public string ListOptions { get { return listOptions; } }
        public string CreateOption { get { return createOption; } }
        public string UpdateOption { get { return updateOption; } }
        public string DeleteOption { get { return deleteOption; } }
        public string DetailCoupon { get { return detailCoupon; } }
        public string ListCoupons { get { return listCoupons; } }
        public string CreateCoupon { get { return createCoupon; } }
        public string UpdateCoupon { get { return updateCoupon; } }
        public string DeleteCoupon { get { return deleteCoupon; } }

        #endregion

        #region Constructors

        public TwoCOAPIInfo()
        {
            string xmlFilePath = System.Configuration.ConfigurationManager.AppSettings["TwoCOConfigXMLPath"];

            if (!string.IsNullOrEmpty(xmlFilePath))
                this.LoadXmlConfig(xmlFilePath);
        }

        public TwoCOAPIInfo(string xmlFilePath)
        {
            this.LoadXmlConfig(xmlFilePath);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Load the xml config file into XmlDocument.
        /// </summary>
        /// <param name="xmlFilePath">Xml Config file path.</param>
        private void LoadXmlConfig(string xmlFilePath)
        {
            if (System.Web.HttpContext.Current.Cache["TwoCOAPIInfo"] == null)
            {
                document = new XmlDocument();
                document.Load(xmlFilePath);
                System.Web.HttpContext.Current.Cache["TwoCOAPIInfo"] = document;
            }
            else
            {
                document = (XmlDocument)System.Web.HttpContext.Current.Cache["TwoCOAPIInfo"];
            }

            username = GetXmlValue("Username");
            password = GetXmlValue("Password");
            detailCompanyInfo = GetXmlValue("DetailCompanyInfo");
            detailContactInfo = GetXmlValue("DetailContactInfo");
            detailPendingPayment = GetXmlValue("DetailPendingPayment");
            listPayments = GetXmlValue("ListPayments");
            detailSale = GetXmlValue("DetailSale");
            listSales = GetXmlValue("ListSales");
            refundInvoice = GetXmlValue("RefundInvoice");
            refundLineItem = GetXmlValue("RefundLineItem");
            stopLineItemRecurring = GetXmlValue("StopLineItemRecurring");
            markShipped = GetXmlValue("MarkShipped");
            createComment = GetXmlValue("CreateComment");
            detailProduct = GetXmlValue("DetailProduct");
            listProducts = GetXmlValue("ListProducts");
            createProduct = GetXmlValue("CreateProduct");
            updateProduct = GetXmlValue("UpdateProduct");
            deleteProduct = GetXmlValue("DeleteProduct");
            detailOption = GetXmlValue("DetailOption");
            listOptions = GetXmlValue("ListOptions");
            createOption = GetXmlValue("CreateOption");
            updateOption = GetXmlValue("UpdateOption");
            deleteOption = GetXmlValue("DeleteOption");
            detailCoupon = GetXmlValue("DetailCoupon");
            listCoupons = GetXmlValue("ListCoupons");
            createCoupon = GetXmlValue("CreateCoupon");
            updateCoupon = GetXmlValue("UpdateCoupon");
            deleteCoupon = GetXmlValue("DeleteCoupon");
        }

        private string GetXmlValue(string xmlNodePath)
        {
            XmlNodeList nodeList = document.GetElementsByTagName(xmlNodePath);
            if (nodeList.Count > 0)
                return nodeList[0].InnerText;
            return string.Empty;
        }

        #endregion
    }
}

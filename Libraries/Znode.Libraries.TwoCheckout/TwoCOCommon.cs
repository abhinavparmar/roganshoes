﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Net;
using System.Web;

namespace ZNode.Libraries.TwoCheckout
{
    /// <summary>
    /// Represents the 2Checkout common utility function.
    /// </summary>
    public class TwoCOCommon
    {
        public TwoCOResponse Submit(string commandUrl, string requestParameters)
        {
            TwoCOAPIInfo apiInfo = new TwoCOAPIInfo();

            TwoCOResponse result = Submit(commandUrl, requestParameters, apiInfo.Username, apiInfo.Password);

            return result;

        }

        public TwoCOResponse Submit(string commandUrl, string requestParameters, string apiUsername, string apiPassword)
        {

            TwoCOResponse result = new TwoCOResponse();
            WebRequest request = WebRequest.Create(commandUrl);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            if (!string.IsNullOrEmpty(requestParameters))
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(requestParameters);

                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;

                // Get the request stream.
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }
            }

            NetworkCredential credential = new NetworkCredential(apiUsername, apiPassword);
            request.Credentials = credential;

            // Get the response.
            using (WebResponse response = request.GetResponse())
            {
                string status = ((HttpWebResponse)response).StatusDescription;
                if (string.Compare(status, "OK", true) != -1)
                {
                    // Get the stream containing content returned by the server.
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            string responseXml = reader.ReadToEnd();
                            result.IsSuccess = true;
                            result.Response = this.BuildDataSet(responseXml);
                        }
                    }
                }
                else
                {
                    // 2Checkout call failed.
                    result.IsSuccess = false;
                }
            }

            return result;

        }

        public DataSet BuildDataSet(string xmlString)
        {
            DataSet result = new DataSet();
            using (StringReader xmlStream = new StringReader(xmlString))
            {
                result.ReadXml(xmlStream);
            }
            return result;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ZNode.Libraries.TwoCheckout
{
    /// <summary>
    /// Represents the 2Checkout coupon class.
    /// </summary>
    public class TwoCOCoupon
    {
        TwoCOAPIInfo apiInfo = new TwoCOAPIInfo();

        /// <summary>
        /// Create a new coupon code in 2Checkout
        /// </summary>
        public TwoCOResponse Create()
        {
            TwoCOCommon common = new TwoCOCommon();
            TwoCOResponse response = new TwoCOResponse();
            StringBuilder requestQuery = new StringBuilder();
            requestQuery.Append("&coupon_code=" + HttpUtility.UrlEncode(""));

            // The date your coupon will expire in the following format: YYYY-MM-DD
            requestQuery.Append("&date_expire=" + HttpUtility.UrlEncode("2010-08-26"));

            // Denotes if coupon applies to product, shipping or sale.
            requestQuery.Append("&type=" + HttpUtility.UrlEncode("sale"));

            // percent_off must be null if value_off used.
            requestQuery.Append("&percent_off=" + HttpUtility.UrlEncode("10"));

            // value_off must be null if percent_off used.
            requestQuery.Append("&value_off=" + HttpUtility.UrlEncode("null"));
            requestQuery.Append("&minimum_purchase=" + HttpUtility.UrlEncode("10.00"));
            requestQuery.Append("&product_id=" + HttpUtility.UrlEncode("4297345540"));
            requestQuery.Append("&select_all=" + HttpUtility.UrlEncode("0"));

            response = common.Submit("https://www.2checkout.com/api/products/create_coupon", requestQuery.ToString());

            return response;

        }


        /// <summary>
        /// Update the existing 2Checkout coupon.
        /// </summary>
        public TwoCOResponse Update()
        {
            TwoCOCommon common = new TwoCOCommon();
            TwoCOResponse response = new TwoCOResponse();
            StringBuilder requestQuery = new StringBuilder();
            requestQuery.Append("&coupon_code=" + HttpUtility.UrlEncode(""));

            // The date your coupon will expire in the following format: YYYY-MM-DD
            requestQuery.Append("&date_expire=" + HttpUtility.UrlEncode(""));

            // Denotes if coupon applies to product, shipping or sale.
            requestQuery.Append("&type=" + HttpUtility.UrlEncode(""));

            // percent_off must be null if value_off used.
            requestQuery.Append("&percent_off=" + HttpUtility.UrlEncode(""));

            // percent_off must be null if value_off used.
            requestQuery.Append("&value_off=" + HttpUtility.UrlEncode(""));
            requestQuery.Append("&minimum_purchase=" + HttpUtility.UrlEncode(""));
            requestQuery.Append("&product_id=" + HttpUtility.UrlEncode(""));
            requestQuery.Append("&select_all=" + HttpUtility.UrlEncode(""));

            response = common.Submit("https://www.2checkout.com/api/products/update_coupon", requestQuery.ToString());

            return response;

        }

        /// <summary>
        /// Delete the existing 2Checkout coupon.
        /// </summary>
        public TwoCOResponse Delete()
        {
            TwoCOCommon common = new TwoCOCommon();
            TwoCOResponse response = new TwoCOResponse();
            StringBuilder requestQuery = new StringBuilder();
            requestQuery.Append("coupon_code=" + HttpUtility.UrlEncode(""));
            
            response = common.Submit("https://www.2checkout.com/api/products/delete_coupon", requestQuery.ToString());

            return response;

        }
    }
}

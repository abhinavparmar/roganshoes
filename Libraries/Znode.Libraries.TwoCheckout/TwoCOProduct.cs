﻿using System;
using System.Collections.Generic;
using System.Text;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Entities;
using System.Net;
using System.IO;
using System.Data;
using System.Web;
using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.TwoCheckout
{
    /// <summary>
    /// Represent the TwoCOProduct object.
    /// </summary>
    public class TwoCOProduct : ZNodeBusinessBase
    {
        #region Members & Properites

        private Product _ZNodeProduct;
        TwoCOAPIInfo apiInfo = new TwoCOAPIInfo();

        /// <summary>
        /// Gets or Sets the ZNodeProduct
        /// </summary>
        public Product ZnodeProduct
        {
            get { return _ZNodeProduct; }
            set { _ZNodeProduct = value; }
        }

        #endregion

        #region Constructors


        public TwoCOProduct()
        {

        }

        #endregion

        #region Public Methods - Products

        /// <summary>
        /// Create a 2Checkout product.
        /// </summary>
        public TwoCOResponse Update(int znodeProductId)
        {
            ProductService productService = new ProductService();
            SKUService skuService = new SKUService();
            ZnodeProduct = productService.GetByProductID(znodeProductId);
            productService.DeepLoad(ZnodeProduct, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(SKU), typeof(SKUAttribute), typeof(ProductAttribute));

            TwoCOCommon common = new TwoCOCommon();
            TwoCOResponse response = new TwoCOResponse();

            ZnodeProduct.SKUCollection = skuService.GetByProductID(znodeProductId);

            foreach (SKU sku in ZnodeProduct.SKUCollection)
            {

                string productName = ZnodeProduct.Name;

                if (ZnodeProduct.SKUCollection.Count > 1 && sku.SKUAttributeCollection.Count > 0) 
                    productName = string.Format("{0} - {1}", productName, sku.SKUAttributeCollection[0].AttributeIdSource.Name);

                StringBuilder requestQuery = new StringBuilder();
                requestQuery.Append("&name=" + HttpUtility.UrlEncode(ZnodeProduct.Name));
                requestQuery.Append("&price=" + HttpUtility.UrlEncode(ZnodeProduct.RetailPrice.GetValueOrDefault(0).ToString()));
                requestQuery.Append("&vendor_product_id=" + HttpUtility.UrlEncode(sku.SKU.ToString()));
                requestQuery.Append("&description=" + HttpUtility.UrlEncode(ZnodeProduct.ShortDescription));
                //requestQuery.Append("&long_description=" + HttpUtility.UrlEncode(ZnodeProduct.Description));
                requestQuery.Append("&tangible=" + HttpUtility.UrlEncode("1"));
                requestQuery.Append("&weight=" + HttpUtility.UrlEncode(ZnodeProduct.Weight.ToString()));
                requestQuery.Append("&handling=" + HttpUtility.UrlEncode("0"));
                requestQuery.Append("&recurring=" + HttpUtility.UrlEncode((ZnodeProduct.RecurringBillingInd == true ? "1" : "0")));
                if (ZnodeProduct.RecurringBillingInd == true)
                {
                    requestQuery.Append("&startup_fee=" +  HttpUtility.UrlEncode(ZnodeProduct.RecurringBillingInitialAmount.ToString()));
                    requestQuery.Append("&recurrence=" +  HttpUtility.UrlEncode(string.Format("{0} {1}", ZnodeProduct.RecurringBillingPeriod, ZnodeProduct.RecurringBillingFrequency)));
                    requestQuery.Append("&duration=" + HttpUtility.UrlEncode(string.Format("{0} {1}", ZnodeProduct.RecurringBillingTotalCycles, ZnodeProduct.RecurringBillingFrequency)));
                }

                if (sku.ExternalProductAPIID != null)
                {
                    requestQuery.Append("&product_id=" + HttpUtility.UrlEncode(sku.ExternalProductAPIID.ToString()));
                    response = common.Submit(apiInfo.UpdateProduct, requestQuery.ToString());
                }
                else
                {
                    response = common.Submit(apiInfo.CreateProduct, requestQuery.ToString());

                    // Update API return IDs in our database.
                    if (response.IsSuccess)
                    {
                        sku.ExternalProductAPIID = response.Response.Tables[0].Rows[0]["product_id"].ToString();

                        response = Select(sku.ExternalProductAPIID);

                        if (response.IsSuccess)
                        {
                            sku.ExternalProductID = Convert.ToInt32(response.Response.Tables[1].Rows[0]["assigned_product_id"]);

                            skuService.Update(sku);
                        }
                    }
                }
            }
            
            return response;

        }

        /// <summary>
        /// Delete the 2Checkout Product
        /// </summary>
        /// <param name="_2CheckoutProductId">2CO Assogned product Id to delete.</param>
        public TwoCOResponse Delete(int znodeProductId)
        {
            ProductService productService = new ProductService();
            SKUService skuService = new SKUService();
            ZnodeProduct = productService.GetByProductID(znodeProductId);
            productService.DeepLoad(_ZNodeProduct, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(SKU));
            ZnodeProduct.SKUCollection = skuService.GetByProductID(znodeProductId);
            TwoCOCommon common = new TwoCOCommon();
            TwoCOResponse response = new TwoCOResponse();

            foreach (SKU sku in ZnodeProduct.SKUCollection)
            {
                
                StringBuilder requestQuery = new StringBuilder();
                requestQuery.Append("&product_id=" + HttpUtility.UrlEncode(sku.ExternalProductAPIID.ToString()));
                response = common.Submit(apiInfo.DeleteProduct, requestQuery.ToString());

                if (response.IsSuccess)
                {
                    sku.ExternalProductID = null;
                    sku.ExternalProductAPIID = null;                    
                }

                skuService.Update(sku);
            }

            return response;
        }

        /// <summary>
        /// Delete the 2Checkout Product by particular sku id.
        /// </summary>
        /// <param name="_2CheckoutProductId">2CO Assogned product Id to delete.</param>
        /// <param name="twoCOProductAPIId">Delete particular sku id.</param>
        /// <returns></returns>
        public TwoCOResponse Delete(int znodeProductId, string twoCOProductAPIId)
        {
            ProductService productService = new ProductService();
            SKUService skuService = new SKUService();
            ZnodeProduct = productService.GetByProductID(znodeProductId);
            productService.DeepLoad(_ZNodeProduct, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(SKU));
            ZnodeProduct.SKUCollection = skuService.GetByProductID(znodeProductId);

            TwoCOCommon common = new TwoCOCommon();
            TwoCOResponse response = new TwoCOResponse();
            StringBuilder requestQuery = new StringBuilder();
            requestQuery.Append("&product_id=" + HttpUtility.UrlEncode(twoCOProductAPIId));
            response = common.Submit(apiInfo.DeleteProduct, requestQuery.ToString());

            if (response.IsSuccess)
            {
                SKU sku = ZnodeProduct.SKUCollection.Find("ExternalProductAPIID", twoCOProductAPIId);
                sku.ExternalProductID = null;
                sku.ExternalProductAPIID = null;
                skuService.Update(sku);
            }

            return response;
        }

        /// <summary>
        /// Get the 2Checkout Product detail
        /// </summary>
        /// <param name="_2CheckoutProductId">2CO Assogned product Id to delete.</param>
        public TwoCOResponse Select(string twoCOProductAPIId)
        {
            TwoCOCommon common = new TwoCOCommon();
            TwoCOResponse response = new TwoCOResponse();
            StringBuilder requestQuery = new StringBuilder();
            requestQuery.Append(string.Format("&product_id={0}", twoCOProductAPIId));
            response = common.Submit(apiInfo.DetailProduct, requestQuery.ToString());

            return response;
        }

        #endregion
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.ECommerce.Fulfillment
{
    /// <summary>
    /// Provides methods to manage Orders and OrderLine Items during Checkout
    /// </summary>
    public partial class ZNodeOrderFulfillment
    {
        public string MultiTendetTransactionIDList { get; set; }
    }
}

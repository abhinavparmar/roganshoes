﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Zeon.Libraries.Avatax
{
    public class AvataxHelper
    {
        #region Public Methods

        /// <summary>
        /// Get tge Avatax Settings
        /// </summary>
        public DataSet GetAvataxDetails(int portalID)
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_AvataxCredentials_GetByPortalID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalID);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Insert the Avatax Credentials
        /// </summary>
        public int InsertAvataxCredentials(int portalID, string accountID, string licence, string companyName, string url)
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                //Create instance of command object
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Zeon_AvataxCredentials_Insert";
                cmd.Connection = connection;

                cmd.Parameters.AddWithValue("@PortalID", portalID);
                cmd.Parameters.AddWithValue("@AccountID", accountID);
                cmd.Parameters.AddWithValue("@LicenceKey", licence);
                cmd.Parameters.AddWithValue("@ComapnyName", companyName);
                cmd.Parameters.AddWithValue("@Url", url);

                connection.Open();
                int iRowsAffected = cmd.ExecuteNonQuery();
                connection.Close();

                return iRowsAffected;
            }
        }

        /// <summary>
        /// Update the Avatax Credentials
        /// </summary>
        public int UpdateAvataxCredentials(int avataxID, int portalID, string accountID, string licence, string companyName, string url, string jointRule)
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                //Create instance of command object
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Zeon_AvataxCredentials_Update";
                cmd.Connection = connection;

                cmd.Parameters.AddWithValue("@AvataxID", avataxID);
                cmd.Parameters.AddWithValue("@PortalID", portalID);
                cmd.Parameters.AddWithValue("@AccountID", accountID);
                cmd.Parameters.AddWithValue("@LicenceKey", licence);
                cmd.Parameters.AddWithValue("@ComapnyName", companyName);
                cmd.Parameters.AddWithValue("@Url", url);
                cmd.Parameters.AddWithValue("@CustomField", jointRule);

                connection.Open();
                int iRowsAffected = cmd.ExecuteNonQuery();
                connection.Close();

                return iRowsAffected;
            }
        }

        #endregion
    }
}

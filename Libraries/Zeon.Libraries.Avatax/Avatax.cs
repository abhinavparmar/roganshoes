﻿using System;
using System.Configuration;
using System.Text;
using System.Web;
using Avalara.AvaTax.Adapter;
using Avalara.AvaTax.Adapter.TaxService;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using System.Data;
using ZNode.Libraries.ECommerce.Entities;

namespace Zeon.Libraries.Avatax
{
    public class Avatax
    {
        #region Constant

        private const string ZeonAvataxRuleTypeName = "zeontaxavatax";

        #endregion

        #region Public Methods

        /// <summary>
        /// Calculate Avalara Tax
        /// </summary>
        public void CalculateTax(ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCart shoppingCart, int accountId, string credentials, bool customerTaxExempt, string useCode)
        {
            GetTaxRequest getTaxRequestObject = new GetTaxRequest();
            Avalara.AvaTax.Adapter.AddressService.Address addressObject = new Avalara.AvaTax.Adapter.AddressService.Address();
            TaxSvc taxSvcObject = new TaxSvc();

            decimal taxSalesTax = 0;
            try
            {
                //Check for exemptions
                if (shoppingCart.Payment != null && shoppingCart.Payment.ShippingAddress != null && !string.IsNullOrEmpty(shoppingCart.Payment.ShippingAddress.PostalCode))
                {
                    //Set From Shipping Address
                    getTaxRequestObject.OriginAddress = GetFromShippingAddress();

                    //Set To Shipping Address
                    getTaxRequestObject.DestinationAddress = GetToShippingAddress(shoppingCart);

                    // Add the orderline items into the tax class.
                    int index = 1;
                    int itemCount = 0;
                    decimal discountedPrice = GetDiscountAmount(shoppingCart);

                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCartItem item in shoppingCart.ShoppingCartItems)
                    {
                        string taxCode = string.Empty;
                        TaxClassService service = new TaxClassService();
                        TaxClass taxCls = service.GetByTaxClassID(item.Product.TaxClassID);

                        if (taxCls != null && !string.IsNullOrEmpty(taxCls.Name))
                        {
                            taxCode = taxCls.Name;
                        }

                        Line lineObject = new Line();

                        lineObject.No = index.ToString();
                        lineObject.ItemCode = item.Product.ProductNum;
                        lineObject.Description = item.Product.Name;
                        lineObject.Qty = item.Quantity;
                        //Prft START: 309-Promotion Code Question
                        lineObject.Amount = 0;
                        decimal taxablePrice = item.ExtendedPrice - discountedPrice;
                        if (taxablePrice > 0)
                        {
                            lineObject.Amount = taxablePrice;
                        }
                        //lineObject.Amount = item.ExtendedPrice - discountedPrice;
                        //Prft END: 309-Promotion Code Question
                        lineObject.Discounted = false;
                        lineObject.TaxCode = customerTaxExempt ? ConfigurationManager.AppSettings["AvalaraProductLevelTaxExemptionCode"].ToString() : taxCode;  // Tax Code For Products
                        getTaxRequestObject.Lines.Add(lineObject);
                        index++;
                        //Set Custom Tax Properties 
                        shoppingCart.ShoppingCartItems[itemCount].Product.IsTaxable = true;
                    }
                    itemCount++;
                    //get shipping code
                    string shipTaxCode = ConfigurationManager.AppSettings["AvalaraShippingTaxCode"].ToString();
                    ShippingService shipService = new ShippingService();
                    Shipping shipDetails = shipService.GetByShippingID(shoppingCart.Shipping.ShippingID);
                    if (shipDetails != null && !string.IsNullOrEmpty(shipDetails.ShippingCode))
                    {
                        shipTaxCode = shipDetails.ShippingCode;
                    }

                    //Added the Shipping line Item for calculating avatax.
                    //This condition 'shoppingCart.ShippingCost > 0' will not add item for free shipping in AVATAX.
                    if (shoppingCart != null && shoppingCart.ShippingCost > 0 && shoppingCart.Shipping != null)
                    {
                        Line lineObject = new Line();
                        lineObject.No = index.ToString();
                        lineObject.ItemCode = string.IsNullOrEmpty(shoppingCart.Shipping.ShippingName) ? ConfigurationManager.AppSettings["AvalaraShippingDescription"].ToString() : shoppingCart.Shipping.ShippingName;
                        lineObject.Description = string.IsNullOrEmpty(shoppingCart.Shipping.ShippingName) ? ConfigurationManager.AppSettings["AvalaraShippingDescription"].ToString() : shoppingCart.Shipping.ShippingName;
                        lineObject.Qty = 1;
                        lineObject.Amount = shoppingCart.ShippingCost;
                        lineObject.Discounted = false;
                        lineObject.TaxCode = customerTaxExempt ? ConfigurationManager.AppSettings["AvalaraProductLevelTaxExemptionCode"].ToString() : shipTaxCode;
                        getTaxRequestObject.Lines.Add(lineObject);

                    }

                    if (getTaxRequestObject.Lines.Count > 0)
                    {
                        getTaxRequestObject.DocDate = DateTime.Today;

                        //check order status and make it committed and uncommited accordingly. 
                        string defaultOrderStatus = GetDefaultOrderStatusName();

                        if (defaultOrderStatus.Equals(ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.SUBMITTED) ||
                            defaultOrderStatus.Equals(ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.SHIPPED))
                        {
                            if (HttpContext.Current.Session["SetOrderIdForAvatax"] == null)
                            {
                                getTaxRequestObject.DocType = DocumentType.SalesOrder;
                            }
                            else
                            {
                                getTaxRequestObject.DocCode = HttpContext.Current.Session["SetOrderIdForAvatax"].ToString();
                                getTaxRequestObject.DocType = DocumentType.SalesInvoice;
                                getTaxRequestObject.Commit = true;
                                HttpContext.Current.Session["SetOrderIdForAvatax"] = null;
                            }
                        }
                        else
                        {
                            if (HttpContext.Current.Session["SetOrderIdForAvatax"] == null)
                            {
                                getTaxRequestObject.DocType = DocumentType.SalesOrder;
                            }
                            else
                            {
                                getTaxRequestObject.DocCode = HttpContext.Current.Session["SetOrderIdForAvatax"].ToString();
                                getTaxRequestObject.DocType = DocumentType.SalesInvoice;
                                HttpContext.Current.Session["SetOrderIdForAvatax"] = null;
                            }
                        }

                        getTaxRequestObject.TaxOverride.TaxDate = DateTime.Now;
                        getTaxRequestObject.CurrencyCode = GetDefaultCurrencyName();
                        getTaxRequestObject.PaymentDate = DateTime.Now;
                        getTaxRequestObject.Discount = 0m;
                        getTaxRequestObject.DetailLevel = DetailLevel.Tax;
                        getTaxRequestObject.PurchaseOrderNo = null;
                        getTaxRequestObject.SalespersonCode = null;
                        getTaxRequestObject.CustomerUsageType = useCode;
                        getTaxRequestObject.ExemptionNo = customerTaxExempt ? ConfigurationManager.AppSettings["AvalaraCustomerTaxExemptionCode"].ToString() : null;

                        if (!string.IsNullOrEmpty(credentials))
                        {
                            string[] credientails = credentials.Split('|');
                            if (credientails.Length > 0 && credientails.Length >= 4)
                            {
                                getTaxRequestObject.CompanyCode = credientails[2].Split('=')[1];
                                getTaxRequestObject.CustomerCode = Convert.ToString(accountId);
                                taxSvcObject.Configuration.Url = credientails[3].Split('=')[1];
                                taxSvcObject.Configuration.RequestTimeout = 100;
                                taxSvcObject.Configuration.Security.Account = credientails[0].Split('=')[1];
                                taxSvcObject.Configuration.Security.License = credientails[1].Split('=')[1];
                                taxSvcObject.Profile.Client = ConfigurationManager.AppSettings["AvalaraTaxSvcClient"].ToString();
                                taxSvcObject.Profile.Name = ConfigurationManager.AppSettings["AvalaraTaxSvcName"].ToString();
                            }
                        }

                        GetTaxResult getTaxResultObject = taxSvcObject.GetTax(getTaxRequestObject);

                        //// Get tax from the Webservice.                        
                        if (getTaxResultObject.ResultCode != Avalara.AvaTax.Adapter.SeverityLevel.Success)
                        {
                            int errorCount = getTaxResultObject.Messages.Count;

                            for (int errorIndex = 0; errorIndex <= errorCount - 1; errorIndex++)
                            {
                                StringBuilder errorText = new StringBuilder();
                                errorText.Append("Avatax Error: \n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Name + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Severity + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Summary + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Details + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Source + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].RefersTo + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].HelpLink + "\n");

                                // Log the error message received from Avatax.
                                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(errorText.ToString());
                                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Tax Error ", "CalculateTax", "In Library Zeon.Avatax", "Fail", errorText.ToString());
                                if (!shoppingCart.ErrorMessage.Contains("Unable to calculate tax rates at this time reason :  "))
                                {
                                    shoppingCart.AddErrorMessage = "Unable to calculate tax rates at this time reason :  " + errorText.ToString() + ". Please try again later.";
                                }
                            }
                        }
                        else
                        {
                            // If success                                
                            int taxCount = getTaxResultObject.TaxLines.Count;
                            shoppingCart.SalesTax = 0;

                            for (int taxIndex = 0; taxIndex < shoppingCart.ShoppingCartItems.Count; taxIndex++)
                            {
                                if (getTaxResultObject.TaxLines[taxIndex] != null)
                                {
                                    taxSalesTax = getTaxResultObject.TaxLines[taxIndex].Tax;
                                    int lineItemIndex = 0;
                                    int.TryParse(getTaxResultObject.TaxLines[taxIndex].No, out lineItemIndex);
                                    shoppingCart.ShoppingCartItems[lineItemIndex - 1].Product.SalesTax = taxSalesTax;
                                    shoppingCart.SalesTax += taxSalesTax;


                                }
                            }

                            if (taxCount > shoppingCart.ShoppingCartItems.Count)
                            {
                                for (int iTax = shoppingCart.ShoppingCartItems.Count; iTax < taxCount; iTax++)
                                {
                                    shoppingCart.SalesTax += getTaxResultObject.TaxLines[iTax].Tax;
                                }
                            }

                            //shoppingCart.IsTaxCalculated = true;
                        }
                    }
                    else
                    {
                        //shoppingCart.IsTaxCalculated = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.StackTrace.ToString());
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Tax Error ", "CalculateTax", "In Library Zeon.Avatax", "Fail", ex.StackTrace.ToString());
                Elmah.ElmahErrorManager.Log(ex);
                shoppingCart.AddErrorMessage = "Unable to calculate tax rates at this time. Please try again later.";
                //shoppingCart.IsTaxCalculated = false;
            }
        }

        /// <summary>
        /// Calculate Avalara Tax
        /// </summary>
        public void CalculateTax(ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCart shoppingCart, int accountId, int portalID)
        {
            if (HttpContext.Current.Session["SetOrderIdForAvatax"] != null)
            {
                DataView dvRules = GetTaxRules(shoppingCart);
                if (dvRules == null || dvRules.Count == 0)
                {
                    return;
                }
            }

            GetTaxRequest getTaxRequestObject = new GetTaxRequest();
            Avalara.AvaTax.Adapter.AddressService.Address addressObject = new Avalara.AvaTax.Adapter.AddressService.Address();
            TaxSvc taxSvcObject = new TaxSvc();

            decimal taxSalesTax = 0;
            try
            {
                //Check for exemptions
                if (shoppingCart.Payment != null && shoppingCart.Payment.ShippingAddress != null &&
                    !string.IsNullOrEmpty(shoppingCart.Payment.ShippingAddress.PostalCode))
                {
                    //Set From Shipping Address
                    getTaxRequestObject.OriginAddress = GetFromShippingAddress();

                    //Set To Shipping Address
                    getTaxRequestObject.DestinationAddress = GetToShippingAddress(shoppingCart);

                    // Add the orderline items into the tax class.
                    int index = 1;
                    int itemCount = 0;
                    decimal discountedPrice = GetDiscountAmount(shoppingCart);

                    foreach (
                        ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCartItem item in
                            shoppingCart.ShoppingCartItems)
                    {
                        ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem cartItem =
                            (ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem)item;
                        string taxCode = string.Empty;
                        TaxClassService service = new TaxClassService();
                        TaxClass taxCls = service.GetByTaxClassID(item.Product.TaxClassID);

                        if (taxCls != null && !string.IsNullOrEmpty(taxCls.Name))
                        {
                            TaxRuleService ruleService = new TaxRuleService();
                            TList<TaxRule> taxRule = ruleService.GetByTaxClassID(taxCls.TaxClassID);

                            if (taxRule != null && taxRule.Count > 0)
                            {
                                TaxRuleTypeService ruleTypService = new TaxRuleTypeService();
                                foreach (TaxRule rul in taxRule)
                                {
                                    int ruleID = 0;
                                    int.TryParse(rul.TaxRuleTypeID.ToString(), out ruleID);
                                    TaxRuleType ruleTyp = ruleTypService.GetByTaxRuleTypeID(ruleID);

                                    if (ruleTyp != null &&
                                        ruleTyp.ClassName.ToLower().Equals(ZeonAvataxRuleTypeName))
                                    {
                                        taxCode = taxCls.Name;
                                        break;
                                    }
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(taxCode))
                        {
                            Line lineObject = new Line();
                            lineObject.No = index.ToString();
                            lineObject.ItemCode = item.Product.ProductNum;
                            lineObject.Description = item.Product.Name;
                            //lineObject.Qty = item.Quantity;                            
                            //lineObject.Amount = item.ExtendedPrice - discountedPrice;

                            lineObject.Qty = cartItem.Quantity;
                            //Prft START: 309-Promotion Code Question
                            lineObject.Amount = 0;
                            decimal taxablePrice = cartItem.ExtendedPrice - discountedPrice;
                            if (taxablePrice > 0)
                            {
                                lineObject.Amount = taxablePrice;
                            }
                            //lineObject.Amount = cartItem.ExtendedPrice - discountedPrice;
                            //Prft END: 309-Promotion Code Question
                            lineObject.Discounted = false;

                            lineObject.TaxCode = string.IsNullOrEmpty(taxCode)
                                ? ConfigurationManager.AppSettings["AvalaraProductLevelTaxExemptionCode"].ToString()
                                : taxCode;
                            getTaxRequestObject.Lines.Add(lineObject);
                            index++;
                            //Setting Product Taxable Property
                            shoppingCart.ShoppingCartItems[itemCount].Product.IsTaxable = true;
                        }
                        itemCount++;
                    }

                    if (getTaxRequestObject.Lines.Count > 0)
                    {
                        getTaxRequestObject.DocDate = DateTime.Today;

                        if (HttpContext.Current.Session["SetOrderIdForAvatax"] == null)
                        {
                            getTaxRequestObject.DocType = DocumentType.SalesOrder;
                        }
                        else
                        {
                            getTaxRequestObject.DocCode =
                                HttpContext.Current.Session["SetOrderIdForAvatax"].ToString();
                            getTaxRequestObject.DocType = DocumentType.SalesInvoice;
                            getTaxRequestObject.Commit = true;
                            HttpContext.Current.Session["SetOrderIdForAvatax"] = null;
                        }

                        getTaxRequestObject.TaxOverride.TaxDate = DateTime.Now;
                        getTaxRequestObject.CurrencyCode = GetDefaultCurrencyName();
                        getTaxRequestObject.PaymentDate = DateTime.Now;
                        getTaxRequestObject.Discount = 0;
                        getTaxRequestObject.DetailLevel = DetailLevel.Tax;
                        getTaxRequestObject.PurchaseOrderNo = null;
                        getTaxRequestObject.SalespersonCode = null;

                        if (portalID > 0)
                        {
                            ZeonAvataxCredentialsService credentialsService = new ZeonAvataxCredentialsService();
                            TList<ZeonAvataxCredentials> credentials = credentialsService.GetByPortalID(portalID);

                            if (credentials != null && credentials.Count > 0)
                            {
                                getTaxRequestObject.CompanyCode = credentials[0].CompanyName;
                                getTaxRequestObject.CustomerCode = Convert.ToString(accountId);
                                taxSvcObject.Configuration.Url = credentials[0].Url;
                                taxSvcObject.Configuration.RequestTimeout = 100;
                                taxSvcObject.Configuration.Security.Account = credentials[0].AccountID;
                                taxSvcObject.Configuration.Security.License = credentials[0].LicenceKey;
                                taxSvcObject.Profile.Client =
                                    ConfigurationManager.AppSettings["AvalaraTaxSvcClient"].ToString();
                                taxSvcObject.Profile.Name =
                                    ConfigurationManager.AppSettings["AvalaraTaxSvcName"].ToString();
                            }
                        }

                        GetTaxResult getTaxResultObject = taxSvcObject.GetTax(getTaxRequestObject);

                        //// Get tax from the Webservice.                        
                        if (getTaxResultObject.ResultCode != Avalara.AvaTax.Adapter.SeverityLevel.Success)
                        {
                            int errorCount = getTaxResultObject.Messages.Count;

                            for (int errorIndex = 0; errorIndex <= errorCount - 1; errorIndex++)
                            {
                                StringBuilder errorText = new StringBuilder();
                                errorText.Append("Avatax Error: \n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Name + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Severity + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Summary + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Details + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Source + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].RefersTo + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].HelpLink + "\n");

                                // Log the error message received from Avatax.
                                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(errorText.ToString());
                                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed,
                                    "Avalara Tax Error ", "CalculateTax", "In Library Zeon.Avatax", "Fail",
                                    errorText.ToString());
                                if (
                                    !shoppingCart.ErrorMessage.Contains(
                                        "Unable to calculate tax rates at this time reason :  "))
                                {
                                    shoppingCart.AddErrorMessage =
                                        "Unable to calculate tax rates at this time reason :  " +
                                        errorText.ToString() + ". Please try again later.";
                                }
                            }
                        }
                        else
                        {
                            // If success                                
                            int taxCount = getTaxResultObject.TaxLines.Count;
                            shoppingCart.SalesTax = 0;

                            for (int taxIndex = 0; taxIndex < taxCount; taxIndex++)
                            {
                                if (getTaxResultObject.TaxLines[taxIndex] != null)
                                {
                                    taxSalesTax = getTaxResultObject.TaxLines[taxIndex].Tax;
                                    int lineItemIndex = 0;
                                    int.TryParse(getTaxResultObject.TaxLines[taxIndex].No, out lineItemIndex);
                                    shoppingCart.ShoppingCartItems[lineItemIndex - 1].Product.SalesTax = taxSalesTax;
                                    shoppingCart.SalesTax += taxSalesTax;
                                }
                            }

                            if (taxCount > shoppingCart.ShoppingCartItems.Count)
                            {
                                for (int iTax = shoppingCart.ShoppingCartItems.Count; iTax < taxCount; iTax++)
                                {
                                    shoppingCart.SalesTax += getTaxResultObject.TaxLines[iTax].Tax;

                                }
                            }
                        }
                    }
                    else
                    {
                        //shoppingCart.IsTaxCalculated = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.StackTrace.ToString());
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Tax Error ", "CalculateTax", "In Library Zeon.Avatax", "Fail", ex.StackTrace.ToString());
                shoppingCart.AddErrorMessage = "Unable to calculate tax rates at this time. Please try again later.";
                //shoppingCart.IsTaxCalculated = false;
            }
        }

        private DataView GetTaxRules(ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCart _ShoppingCart)
        {
            // get all tax rules
            DataView dataView = new DataView();
            string countryCode = string.Empty;
            string stateCode = string.Empty;
            StringBuilder taxClassIds = new StringBuilder();//Zeon custom code

            if (_ShoppingCart != null)
            {
                countryCode = _ShoppingCart.Payment.ShippingAddress.CountryCode;
                stateCode = _ShoppingCart.Payment.ShippingAddress.StateCode;
                foreach (ZNodeShoppingCartItem item in _ShoppingCart.ShoppingCartItems)
                {
                    taxClassIds.Append(item.Product.TaxClassID + ",");
                }
            }

            if (!string.IsNullOrEmpty(countryCode) && !string.IsNullOrEmpty(stateCode))
            {
                ZNode.Libraries.DataAccess.Custom.TaxHelper taxHelper = new ZNode.Libraries.DataAccess.Custom.TaxHelper();
                //DataTable dt = taxHelper.GetActiveTaxRulesByPortalId(ZNodeConfigManager.SiteConfig.PortalID, countryCode);
                DataTable dt = taxHelper.GetActiveTaxRulesByPortalId(ZNodeConfigManager.SiteConfig.PortalID, countryCode, stateCode, taxClassIds.ToString());

                dataView = new DataView(dt);

                if (ZNodeConfigManager.SiteConfig.InclusiveTax)
                {
                    dataView.RowFilter = "InclusiveInd = false";
                }
            }
            return dataView;
        }

        /// <summary>
        /// Return Avalara Tax
        /// </summary>
        public bool ReturnTax(ZNode.Libraries.DataAccess.Entities.Order order, string credentials, bool customerTaxExempt, string useCode)
        {
            try
            {
                if (order != null && order.OrderLineItemCollection != null)
                {
                    GetTaxRequest getTaxRequestObject = new GetTaxRequest();
                    TaxSvc taxSvcObject = new TaxSvc();

                    getTaxRequestObject.OriginAddress = GetFromShippingAddress();
                    getTaxRequestObject.DestinationAddress = GetToShippingAddress(order);

                    OrderLineItemService lineItemService = new OrderLineItemService();
                    TList<OrderLineItem> collection = lineItemService.GetByOrderID(order.OrderID);

                    decimal discountedPrice = GetDiscountAmount(order, collection.Count);

                    string taxClassCode = string.Empty;
                    int index = 1;
                    foreach (OrderLineItem item in collection)
                    {
                        SKUService ser = new SKUService();
                        TList<SKU> lst = ser.GetBySKU(item.SKU);

                        if (lst.Count > 0)
                        {
                            for (int iCnt = 0; iCnt < lst.Count; iCnt++)
                            {
                                ProductService productService = new ProductService();
                                Product prod = productService.GetByProductID(lst[iCnt].ProductID);

                                TaxClassService taxClsService = new TaxClassService();
                                TaxClass taxCls = taxClsService.GetByTaxClassID(Convert.ToInt32(prod.TaxClassID));

                                if (taxCls != null && !string.IsNullOrEmpty(taxCls.Name) && !taxCls.Name.ToLower().Equals("default"))
                                {
                                    taxClassCode = taxCls.Name;
                                }
                            }
                        }

                        Line lineObject = new Line();
                        lineObject.No = index.ToString();
                        lineObject.ItemCode = item.ProductNum;
                        lineObject.Description = item.Name;
                        lineObject.Qty = item.Quantity == null ? 0 : Convert.ToInt32(item.Quantity);
                        //Prft START: 309-Promotion Code Question
                        lineObject.Amount = 0;
                        decimal taxablePrice = Convert.ToDecimal((item.Price * item.Quantity) - discountedPrice);
                        if (taxablePrice > 0)
                        {
                            lineObject.Amount = taxablePrice;
                        }
                        lineObject.Amount = item.Price == null ? 0 : Convert.ToDecimal(Convert.ToDecimal(lineObject.Amount) * -1);
                        //lineObject.Amount = item.Price == null ? 0 : Convert.ToDecimal(Convert.ToDecimal(Convert.ToDecimal(item.Price) * item.Quantity) - discountedPrice) * -1;
                        //Prft END: 309-Promotion Code Question
                        lineObject.Discounted = false;
                        lineObject.TaxCode = customerTaxExempt ? ConfigurationManager.AppSettings["AvalaraProductLevelTaxExemptionCode"].ToString() : taxClassCode;
                        getTaxRequestObject.Lines.Add(lineObject);
                        index++;
                    }

                    if (order.ShippingCost > 0)
                    {
                        ShippingService service = new ShippingService();
                        Shipping shipInfo = service.GetByShippingID(int.Parse(order.ShippingID.ToString()));

                        string shipTaxCode = ConfigurationManager.AppSettings["AvalaraShippingTaxCode"].ToString();
                        if (shipInfo != null && !string.IsNullOrEmpty(shipInfo.ShippingCode))
                        {
                            shipTaxCode = shipInfo.ShippingCode;
                        }

                        Line lineObject = new Line();

                        lineObject.No = index.ToString();
                        lineObject.ItemCode = string.IsNullOrEmpty(shipInfo.Description) ? ConfigurationManager.AppSettings["AvalaraShippingDescription"].ToString() : shipInfo.Description;
                        lineObject.Description = string.IsNullOrEmpty(shipInfo.Description) ? ConfigurationManager.AppSettings["AvalaraShippingDescription"].ToString() : shipInfo.Description;
                        lineObject.Qty = 1;
                        lineObject.Amount = order.ShippingCost == null ? 0 : Convert.ToDecimal(Convert.ToDecimal(order.ShippingCost) * -1);
                        lineObject.Discounted = false;
                        lineObject.TaxCode = customerTaxExempt ? ConfigurationManager.AppSettings["AvalaraProductLevelTaxExemptionCode"].ToString() : shipTaxCode;
                        getTaxRequestObject.Lines.Add(lineObject);
                    }

                    if (getTaxRequestObject.Lines.Count > 0)
                    {
                        getTaxRequestObject.DocCode = order.OrderID.ToString();
                        getTaxRequestObject.DocDate = DateTime.Now;
                        getTaxRequestObject.DocType = DocumentType.ReturnInvoice;
                        getTaxRequestObject.Commit = true;
                        getTaxRequestObject.PaymentDate = Convert.ToDateTime(order.OrderDate);
                        getTaxRequestObject.TaxOverride.TaxDate = Convert.ToDateTime(order.OrderDate);
                        getTaxRequestObject.TaxOverride.TaxOverrideType = TaxOverrideType.TaxAmount;
                        getTaxRequestObject.TaxOverride.Reason = order.Custom1; //Return reason

                        getTaxRequestObject.CurrencyCode = GetDefaultCurrencyName();
                        getTaxRequestObject.Discount = 0m;
                        getTaxRequestObject.DetailLevel = DetailLevel.Tax;
                        getTaxRequestObject.PurchaseOrderNo = null;
                        getTaxRequestObject.SalespersonCode = null;
                        getTaxRequestObject.CustomerUsageType = useCode;
                        getTaxRequestObject.ExemptionNo = customerTaxExempt ? ConfigurationManager.AppSettings["AvalaraCustomerTaxExemptionCode"].ToString() : null;

                        if (!string.IsNullOrEmpty(credentials))
                        {
                            string[] credientails = credentials.Split('|');
                            if (credientails.Length > 0 && credientails.Length >= 4)
                            {
                                getTaxRequestObject.CompanyCode = credientails[2].Split('=')[1];
                                getTaxRequestObject.CustomerCode = order.AccountID == null ? string.Empty : Convert.ToString(order.AccountID);
                                taxSvcObject.Configuration.Url = credientails[3].Split('=')[1];
                                taxSvcObject.Configuration.RequestTimeout = 100;
                                taxSvcObject.Configuration.Security.Account = credientails[0].Split('=')[1];
                                taxSvcObject.Configuration.Security.License = credientails[1].Split('=')[1];
                                taxSvcObject.Profile.Client = ConfigurationManager.AppSettings["AvalaraTaxSvcClient"].ToString();
                                taxSvcObject.Profile.Name = ConfigurationManager.AppSettings["AvalaraTaxSvcName"].ToString();
                            }
                        }

                        GetTaxResult getTaxResultObject = taxSvcObject.GetTax(getTaxRequestObject);

                        if (getTaxResultObject.ResultCode != Avalara.AvaTax.Adapter.SeverityLevel.Success)
                        {
                            int errorCount = getTaxResultObject.Messages.Count;

                            for (int errorIndex = 0; errorIndex <= errorCount - 1; errorIndex++)
                            {
                                StringBuilder errorText = new StringBuilder();
                                errorText.Append("Avatax Error: \n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Name + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Severity + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Summary + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Details + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].Source + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].RefersTo + "\n");
                                errorText.Append(getTaxResultObject.Messages[errorIndex].HelpLink + "\n");

                                // Log the error message received from Avatax.
                                ZNodeLogging.LogMessage(errorText.ToString());
                                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Tax Error ", "CalculateTax", "In Library Zeon.Avatax", "Fail", errorText.ToString());
                            }
                            return false;
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.StackTrace.ToString());
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Tax Error ", "ReturnTax", "In Library Zeon.Avatax", "Fail", ex.StackTrace.ToString());
                Elmah.ElmahErrorManager.Log(ex);
                return false;
            }
        }

        /// <summary>
        /// Cancel Avalara Tax
        /// </summary>
        public bool CancelTax(ZNode.Libraries.DataAccess.Entities.Order order, string credentials)
        {
            CancelTaxRequest cancelTaxRequestObject = new CancelTaxRequest();
            Avalara.AvaTax.Adapter.AddressService.Address addressObject = new Avalara.AvaTax.Adapter.AddressService.Address();
            TaxSvc taxSvcObject = new TaxSvc();
            try
            {
                cancelTaxRequestObject.DocType = DocumentType.SalesInvoice;
                cancelTaxRequestObject.DocCode = order.OrderID.ToString();
                cancelTaxRequestObject.CancelCode = CancelCode.DocVoided;
                if (!string.IsNullOrEmpty(credentials))
                {
                    string[] credientails = credentials.Split('|');
                    if (credientails.Length > 0 && credientails.Length >= 4)
                    {
                        cancelTaxRequestObject.CompanyCode = credientails[2].Split('=')[1];
                        taxSvcObject.Configuration.Url = credientails[3].Split('=')[1];
                        taxSvcObject.Configuration.RequestTimeout = 100;
                        taxSvcObject.Configuration.Security.Account = credientails[0].Split('=')[1];
                        taxSvcObject.Configuration.Security.License = credientails[1].Split('=')[1];
                        taxSvcObject.Profile.Client = ConfigurationManager.AppSettings["AvalaraTaxSvcClient"].ToString();
                        taxSvcObject.Profile.Name = ConfigurationManager.AppSettings["AvalaraTaxSvcName"].ToString();
                    }
                }

                CancelTaxResult getTaxResultObject = taxSvcObject.CancelTax(cancelTaxRequestObject);

                //// Get tax from the Webservice.                        
                if (getTaxResultObject.ResultCode != Avalara.AvaTax.Adapter.SeverityLevel.Success)
                {
                    int errorCount = getTaxResultObject.Messages.Count;

                    for (int errorIndex = 0; errorIndex <= errorCount - 1; errorIndex++)
                    {
                        StringBuilder errorText = new StringBuilder();
                        errorText.Append("Avatax Error: \n");
                        errorText.Append(getTaxResultObject.Messages[errorIndex].Name + "\n");
                        errorText.Append(getTaxResultObject.Messages[errorIndex].Severity + "\n");
                        errorText.Append(getTaxResultObject.Messages[errorIndex].Summary + "\n");
                        errorText.Append(getTaxResultObject.Messages[errorIndex].Details + "\n");
                        errorText.Append(getTaxResultObject.Messages[errorIndex].Source + "\n");
                        errorText.Append(getTaxResultObject.Messages[errorIndex].RefersTo + "\n");
                        errorText.Append(getTaxResultObject.Messages[errorIndex].HelpLink + "\n");

                        // Log the error message received from Avatax.
                        ZNodeLogging.LogMessage(errorText.ToString());
                        ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Tax Error ", "CalculateTax", "In Library Zeon.Avatax", "Fail", errorText.ToString());
                    }
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.StackTrace.ToString());
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Tax Error ", "ReturnTax", "In Library Zeon.Avatax", "Fail", ex.StackTrace.ToString());
                Elmah.ElmahErrorManager.Log(ex);
                return false;
            }
        }

        /// <summary>
        /// Change the status of the Tax to Committed in AvaTax
        /// </summary>
        public bool CommitTax(ZNode.Libraries.DataAccess.Entities.Order order, string credentials)
        {
            try
            {
                if (order != null && order.OrderLineItemCollection != null)
                {
                    TaxSvc taxSvcObject = new TaxSvc();
                    if (!string.IsNullOrEmpty(credentials))
                    {
                        string[] credientails = credentials.Split('|');
                        if (credientails.Length > 0 && credientails.Length >= 4)
                        {
                            PostTaxRequest postTaxRequestObject = new PostTaxRequest();
                            postTaxRequestObject.Commit = true;
                            postTaxRequestObject.CompanyCode = credientails[2].Split('=')[1];
                            postTaxRequestObject.DocCode = order.OrderID.ToString();
                            postTaxRequestObject.DocDate = DateTime.Now;
                            postTaxRequestObject.DocType = DocumentType.SalesInvoice;
                            postTaxRequestObject.NewDocCode = order.OrderID.ToString();
                            postTaxRequestObject.TotalAmount = order.Total == null ? 0 : Convert.ToDecimal(order.Total) - Convert.ToDecimal(order.SalesTax);
                            postTaxRequestObject.TotalTax = order.SalesTax == null ? 0 : Convert.ToDecimal(order.SalesTax);

                            taxSvcObject.Configuration.Url = credientails[3].Split('=')[1];
                            taxSvcObject.Configuration.RequestTimeout = int.Parse(ConfigurationManager.AppSettings["AvalaraConnectionTimeOut"].ToString());
                            taxSvcObject.Configuration.Security.Account = credientails[0].Split('=')[1];
                            taxSvcObject.Configuration.Security.License = credientails[1].Split('=')[1];
                            taxSvcObject.Profile.Client = ConfigurationManager.AppSettings["AvalaraTaxSvcClient"].ToString();
                            taxSvcObject.Profile.Name = ConfigurationManager.AppSettings["AvalaraTaxSvcName"].ToString();

                            PostTaxResult postTaxResultObject = taxSvcObject.PostTax(postTaxRequestObject);

                            if (postTaxResultObject.ResultCode != Avalara.AvaTax.Adapter.SeverityLevel.Success)
                            {
                                int errorCount = postTaxResultObject.Messages.Count;

                                for (int errorIndex = 0; errorIndex <= errorCount - 1; errorIndex++)
                                {
                                    StringBuilder errorText = new StringBuilder();
                                    errorText.Append("Avatax Error: \n");
                                    errorText.Append(postTaxResultObject.Messages[errorIndex].Name + "\n");
                                    errorText.Append(postTaxResultObject.Messages[errorIndex].Severity + "\n");
                                    errorText.Append(postTaxResultObject.Messages[errorIndex].Summary + "\n");
                                    errorText.Append(postTaxResultObject.Messages[errorIndex].Details + "\n");
                                    errorText.Append(postTaxResultObject.Messages[errorIndex].Source + "\n");
                                    errorText.Append(postTaxResultObject.Messages[errorIndex].RefersTo + "\n");
                                    errorText.Append(postTaxResultObject.Messages[errorIndex].HelpLink + "\n");

                                    // Log the error message received from Avatax.
                                    ZNodeLogging.LogMessage(errorText.ToString());
                                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Tax Error ", "CommitTax", "In Library Zeon.Avatax", "Fail", errorText.ToString());
                                }

                                if (postTaxResultObject.ResultCode == SeverityLevel.Warning)
                                {
                                    return true;
                                }
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.StackTrace.ToString());
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Tax Error ", "SubmitTax", "In Library Zeon.Avatax", "Fail", ex.StackTrace.ToString());
                Elmah.ElmahErrorManager.Log(ex);
                return false;
            }
        }

        /// <summary>
        /// Change the status of the Tax to Committed in AvaTax
        /// </summary>
        public bool CommitTax(int orderID, decimal total, decimal salesTax, string credentials)
        {
            try
            {
                TaxSvc taxSvcObject = new TaxSvc();
                if (!string.IsNullOrEmpty(credentials))
                {
                    string[] credientails = credentials.Split('|');
                    if (credientails.Length > 0 && credientails.Length >= 4)
                    {
                        PostTaxRequest postTaxRequestObject = new PostTaxRequest();
                        postTaxRequestObject.Commit = true;
                        postTaxRequestObject.CompanyCode = credientails[2].Split('=')[1];
                        postTaxRequestObject.DocCode = Convert.ToString(orderID);
                        postTaxRequestObject.DocDate = DateTime.Now;
                        postTaxRequestObject.DocType = DocumentType.SalesInvoice;
                        postTaxRequestObject.NewDocCode = Convert.ToString(orderID);
                        postTaxRequestObject.TotalAmount = total - salesTax;
                        postTaxRequestObject.TotalTax = salesTax;

                        taxSvcObject.Configuration.Url = credientails[3].Split('=')[1];
                        taxSvcObject.Configuration.RequestTimeout = int.Parse(ConfigurationManager.AppSettings["AvalaraConnectionTimeOut"].ToString());
                        taxSvcObject.Configuration.Security.Account = credientails[0].Split('=')[1];
                        taxSvcObject.Configuration.Security.License = credientails[1].Split('=')[1];
                        taxSvcObject.Profile.Client = ConfigurationManager.AppSettings["AvalaraTaxSvcClient"].ToString();
                        taxSvcObject.Profile.Name = ConfigurationManager.AppSettings["AvalaraTaxSvcName"].ToString();

                        PostTaxResult postTaxResultObject = taxSvcObject.PostTax(postTaxRequestObject);

                        if (postTaxResultObject.ResultCode != Avalara.AvaTax.Adapter.SeverityLevel.Success)
                        {
                            int errorCount = postTaxResultObject.Messages.Count;

                            for (int errorIndex = 0; errorIndex <= errorCount - 1; errorIndex++)
                            {
                                StringBuilder errorText = new StringBuilder();
                                errorText.Append("Avatax Error: \n");
                                errorText.Append(postTaxResultObject.Messages[errorIndex].Name + "\n");
                                errorText.Append(postTaxResultObject.Messages[errorIndex].Severity + "\n");
                                errorText.Append(postTaxResultObject.Messages[errorIndex].Summary + "\n");
                                errorText.Append(postTaxResultObject.Messages[errorIndex].Details + "\n");
                                errorText.Append(postTaxResultObject.Messages[errorIndex].Source + "\n");
                                errorText.Append(postTaxResultObject.Messages[errorIndex].RefersTo + "\n");
                                errorText.Append(postTaxResultObject.Messages[errorIndex].HelpLink + "\n");

                                // Log the error message received from Avatax.
                                ZNodeLogging.LogMessage(errorText.ToString());
                                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Tax Error ", "CommitTax", "In Library Zeon.Avatax", "Fail", errorText.ToString());
                            }

                            if (postTaxResultObject.ResultCode == SeverityLevel.Warning)
                            {
                                return true;
                            }
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.StackTrace.ToString());
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Tax Error ", "SubmitTax", "In Library Zeon.Avatax", "Fail", ex.StackTrace.ToString());
                Elmah.ElmahErrorManager.Log(ex);
                return false;
            }
        }

        /// <summary>
        /// Test the connection with Avalara.
        /// </summary>
        /// <returns>Return the Valid till date.</returns>
        public DateTime? TestConnection(string url, string account, string license)
        {
            TaxSvc taxSvcObject = new TaxSvc();
            try
            {
                taxSvcObject.Configuration.Url = url;
                taxSvcObject.Configuration.RequestTimeout = int.Parse(ConfigurationManager.AppSettings["AvalaraConnectionTimeOut"].ToString());
                taxSvcObject.Configuration.Security.Account = account;
                taxSvcObject.Configuration.Security.License = license;
                taxSvcObject.Profile.Client = ConfigurationManager.AppSettings["AvalaraTaxSvcClient"].ToString();
                taxSvcObject.Profile.Name = ConfigurationManager.AppSettings["AvalaraTaxSvcName"].ToString();

                IsAuthorizedResult resAuth = taxSvcObject.IsAuthorized(string.Empty);
                if (resAuth.ResultCode.Equals(SeverityLevel.Success))
                {
                    return resAuth.Expires;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.StackTrace.ToString());
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.AvalaraTaxFailed, "Avalara Test Connection Error ", "TestConnection", "In Library Zeon.Avatax", "Fail", ex.StackTrace.ToString());
                Elmah.ElmahErrorManager.Log(ex);
                return null;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get where product is going to delivery (Ship To)
        /// </summary>
        /// <returns></returns>
        private Avalara.AvaTax.Adapter.AddressService.Address GetToShippingAddress(ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCart _ShoppingCart)
        {
            Avalara.AvaTax.Adapter.AddressService.Address shippingTo = new Avalara.AvaTax.Adapter.AddressService.Address();

            if (_ShoppingCart != null && _ShoppingCart.Payment != null && _ShoppingCart.Payment.ShippingAddress != null)
            {
                shippingTo.Line1 = _ShoppingCart.Payment.ShippingAddress.Street;
                shippingTo.Line2 = _ShoppingCart.Payment.ShippingAddress.Street1;
                shippingTo.Line3 = null;
                shippingTo.City = _ShoppingCart.Payment.ShippingAddress.City;
                shippingTo.Region = _ShoppingCart.Payment.ShippingAddress.StateCode;
                shippingTo.PostalCode = _ShoppingCart.Payment.ShippingAddress.PostalCode;
                shippingTo.Country = _ShoppingCart.Payment.ShippingAddress.CountryCode;
            }
            return shippingTo;
        }

        /// <summary>
        /// Get where product is going to delivery (Ship To)
        /// </summary>
        /// <returns></returns>
        private Avalara.AvaTax.Adapter.AddressService.Address GetToShippingAddress(ZNode.Libraries.DataAccess.Entities.Order _order)
        {
            Avalara.AvaTax.Adapter.AddressService.Address shippingTo = new Avalara.AvaTax.Adapter.AddressService.Address();

            if (_order != null && _order.ShippingID != null && _order.ShippingID > 0)
            {
                shippingTo.Line1 = _order.BillingStreet;
                shippingTo.Line2 = _order.BillingStreet1;
                shippingTo.Line3 = null;
                shippingTo.City = _order.BillingCity;
                shippingTo.Region = _order.BillingStateCode;
                shippingTo.PostalCode = _order.BillingPostalCode;
                shippingTo.Country = _order.BillingCountry;
            }
            return shippingTo;
        }

        /// <summary>
        /// Ship From Address (Ship From) 
        /// </summary>
        /// <returns></returns>
        private Avalara.AvaTax.Adapter.AddressService.Address GetFromShippingAddress()
        {
            Avalara.AvaTax.Adapter.AddressService.Address shippingFrom = new Avalara.AvaTax.Adapter.AddressService.Address();
            // Shipping from Address
            shippingFrom.Line1 = ZNodeConfigManager.SiteConfig.ShippingOriginAddress1;
            shippingFrom.Line2 = ZNodeConfigManager.SiteConfig.ShippingOriginAddress2;
            shippingFrom.Line3 = null;
            shippingFrom.City = ZNodeConfigManager.SiteConfig.ShippingOriginCity;
            shippingFrom.Region = ZNodeConfigManager.SiteConfig.ShippingOriginStateCode;
            shippingFrom.PostalCode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
            shippingFrom.Country = ZNodeConfigManager.SiteConfig.ShippingOriginCountryCode;
            return shippingFrom;
        }

        /// <summary>
        /// Gets the default currency
        /// </summary>
        /// <returns></returns>
        private string GetDefaultCurrencyName()
        {
            string defaultCurencyName = "USD";
            CurrencyTypeService currencyTypeService = new CurrencyTypeService();
            CurrencyType currencyInfo = currencyTypeService.GetByCurrencyTypeID(Convert.ToInt32(ZNodeConfigManager.SiteConfig.CurrencyTypeID));
            if (currencyInfo != null && !string.IsNullOrEmpty(currencyInfo.CurrencySuffix))
            {
                defaultCurencyName = currencyInfo.CurrencySuffix;
            }
            return defaultCurencyName;
        }

        /// <summary>
        /// Get the Default order status
        /// </summary>
        /// <returns></returns>
        private string GetDefaultOrderStatusName()
        {
            string defaultOrderStatusName = string.Empty;
            OrderStateService ordStateService = new OrderStateService();
            if (ZNodeConfigManager.SiteConfig.DefaultOrderStateID != null)
            {
                OrderState state = ordStateService.GetByOrderStateID(Convert.ToInt32(ZNodeConfigManager.SiteConfig.DefaultOrderStateID));
                if (state != null)
                {
                    defaultOrderStatusName = state.OrderStateName;
                }
            }

            return defaultOrderStatusName;
        }

        /// <summary>
        /// Get the discount applied on the product
        /// </summary>
        /// <returns></returns>
        private decimal GetDiscountAmount(ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCart cart)
        {
            int itemCount = cart.ShoppingCartItems.Count;

            if (cart.Discount.Equals(0))
            {
                return 0;
            }
            else
            {
                return (cart.Discount / itemCount);
            }
        }

        /// <summary>
        /// Get the discount applied on the product
        /// </summary>
        /// <returns></returns>
        private decimal GetDiscountAmount(Order order, int count)
        {
            if (order.DiscountAmount.Equals(0))
            {
                return 0;
            }
            else
            {
                return (Convert.ToDecimal(order.DiscountAmount) / Convert.ToDecimal(count));
            }
        }

        #endregion
    }
}

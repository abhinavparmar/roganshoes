using System;
using System.Collections.Generic;
using System.Web.Services.Protocols;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Shipping.FedExRateService;

namespace ZNode.Libraries.Shipping
{
    /// <summary>
    /// FedEx Shipping
    /// </summary>
    public partial class FedEx : ZNodeBusinessBase
    {
        #region Protected Member Instance Variables

        private string _FedExGatewayURL;
        private string _FedExAccountNumber;
        private string _FedExMeterNumber;
        private string _FedExAccessKey;
        private string _FedExSecurityCode;
        private string _CspAccessKey;
        private string _CspPassword;
        private string _ShipperAddress1;
        private string _ShipperAddress2;
        private string _ShipperCompany;
        private string _ShipperPhone;
        private string _ShipperCity;
        private string _ShipperZipCode;
        private string _ShipperStateCode;
        private string _ShipperCountryCode;
        private bool _ShipperAddressType = false;
        private string _ShipToFirstName;
        private string _ShipToLastName;
        private string _ShipToAddress1;
        private string _ShipToAddress2;
        private string _ShipToCity;
        private string _ShipToZipCode;
        private string _ShipToStateCode;
        private string _ShipToCountryCode;
        private string _ShipToCompany;
        private string _ShipToPhone;
        private bool _ShipToAddressType = false;
        private decimal _PackageWeight = 0;
        private string _DropOffType = "REGULAR_PICKUP";
        private string _FedExServiceType;
        private string _PackageTypeCode = "YOUR_PACKAGING";
        private string _WeightUnit = "LB";
        private string _DimensionUnit = "IN";
        private string _CurrencyCode = "USD";
        private string _PackageHeight = "1";
        private string _PackageWidth = "1";
        private string _PackageLength = "1";
        private string _ClientProductVersion = string.Empty;
        private string _VendorProductPlatform = string.Empty;
        private string _ClientProductId = string.Empty;
        private string _OriginLocationID = string.Empty;
        private bool _UseFedExDiscountRate = false;
        private decimal _TotalInsuredValue = 0;
        private decimal _TotalCustomsValue = 0;
        private string _ErrorCode;
        private string _ErrorDescription;
        private string _TrackingNumber;
        private byte[] _LabelImage;
        private decimal _ShipmentCharge;
        #endregion
         
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the FedEx class.
        /// </summary>
        public FedEx()
        {
            this._FedExGatewayURL = System.Configuration.ConfigurationManager.AppSettings["FedExGatewayURL"].ToString();
        }
        #endregion

        #region Public Instance Properties

        /// <summary>
        /// Gets or sets the transaction currency code
        /// </summary>
        public string CurrencyCode
        {
            get { return this._CurrencyCode; }
            set { this._CurrencyCode = value; }
        }

        /// <summary>
        /// Gets or sets the Merchant FedEx LoginId
        /// </summary>
        public string FedExAccountNumber
        {
            get { return this._FedExAccountNumber; }
            set { this._FedExAccountNumber = value; }
        }

        /// <summary>
        /// Gets or sets the Merchant FedEx LoginPassword
        /// </summary>
        public string FedExMeterNumber
        {
            get { return this._FedExMeterNumber; }
            set { this._FedExMeterNumber = value; }
        }

        /// <summary>
        /// Gets or sets the Merchant FedEx LoginPassword
        /// </summary>
        public string FedExAccessKey
        {
            get { return this._FedExAccessKey; }
            set { this._FedExAccessKey = value; }
        }

        /// <summary>
        /// Gets or sets the FedEx security code
        /// </summary>
        public string FedExSecurityCode
        {
            get { return this._FedExSecurityCode; }
            set { this._FedExSecurityCode = value; }
        }

        /// <summary>
        /// Gets or sets the CSP access key
        /// </summary>
        public string CSPAccessKey
        {
            get { return this._CspAccessKey; }
            set { this._CspAccessKey = value; }
        }

        /// <summary>
        /// Gets or sets the FedEx CSP password
        /// </summary>
        public string CSPPassword
        {
            get { return this._CspPassword; }
            set { this._CspPassword = value; }
        }

        /// <summary>
        /// Gets or sets the Label Image
        /// </summary>
        public byte[] LabelImage
        {
            get { return this._LabelImage; }
            set { this._LabelImage = value; }
        }

        /// <summary>
        /// Gets or sets the Shipper company
        /// </summary>
        public string ShipperCompany
        {
            get { return this._ShipperCompany; }
            set { this._ShipperCompany = value; }
        }

        /// <summary>
        /// Gets or sets  the Shipper city
        /// </summary>
        public string ShipperCity
        {
            get { return this._ShipperCity; }
            set { this._ShipperCity = value; }
        }

        /// <summary>
        /// Gets or sets the Shipper first address line
        /// </summary>
        public string ShipperAddress1
        {
            get { return this._ShipperAddress1; }
            set { this._ShipperAddress1 = value; }
        }

        /// <summary>
        /// Gets or sets the Shipper second address line
        /// </summary>
        public string ShipperAddress2
        {
            get { return this._ShipperAddress2; }
            set { this._ShipperAddress2 = value; }
        }

        /// <summary>
        /// Gets or sets the Shipper Phone
        /// </summary>
        public string ShipperPhone
        {
            get { return this._ShipperPhone; }
            set { this._ShipperPhone = value; }
        }

        /// <summary>
        /// Gets or sets the Shipper postal code
        /// </summary>
        public string ShipperZipCode
        {
            get { return this._ShipperZipCode; }
            set { this._ShipperZipCode = value; }
        }

        /// <summary>
        /// Gets or sets the Shipper postal code
        /// </summary>
        public string ShipperStateCode
        {
            get { return this._ShipperStateCode; }
            set { this._ShipperStateCode = value; }
        }

        /// <summary>
        /// Gets or sets the Shipper Country code
        /// </summary>
        public string ShipperCountryCode
        {
            get { return this._ShipperCountryCode; }
            set { this._ShipperCountryCode = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the shipper address type is resedential or some else
        /// Default address type is 'Residential'
        /// </summary>
        public bool ShippierAddressType
        {
            get { return this._ShipperAddressType; }
            set { this._ShipperAddressType = value; }
        }

        /// <summary>
        /// Gets or sets the ShipTo First Name
        /// </summary>
        public string ShipToFirstName
        {
            get { return this._ShipToFirstName; }
            set { this._ShipToFirstName = value; }
        }

        /// <summary>
        /// Gets or sets the ShipTo Last Name
        /// </summary>
        public string ShipToLastName
        {
            get { return this._ShipToLastName; }
            set { this._ShipToLastName = value; }
        }

        /// <summary>
        /// Gets or sets the ShipTo Address 1
        /// </summary>
        public string ShipToAddress1
        {
            get { return this._ShipToAddress1; }
            set { this._ShipToAddress1 = value; }
        }

        /// <summary>
        /// Gets or sets  the ShipTo Address 2
        /// </summary>
        public string ShipToAddress2
        {
            get { return this._ShipToAddress2; }
            set { this._ShipToAddress2 = value; }
        }

        /// <summary>
        /// Gets or sets the ShipTo city
        /// </summary>
        public string ShipToCity
        {
            get { return this._ShipToCity; }
            set { this._ShipToCity = value; }
        }

        /// <summary>
        /// Gets or sets  the Destination postal code
        /// </summary>
        public string ShipToZipCode
        {
            get { return this._ShipToZipCode; }
            set { this._ShipToZipCode = value; }
        }

        /// <summary>
        /// Gets or sets the Destination postal code
        /// </summary>
        public string ShipToStateCode
        {
            get { return this._ShipToStateCode; }
            set { this._ShipToStateCode = value; }
        }

        /// <summary>
        /// Gets or sets the Destination Company name
        /// </summary>
        public string ShipToCompany
        {
            get { return this._ShipToCompany; }
            set { this._ShipToCompany = value; }
        }

        /// <summary>
        /// Gets or sets the Destination Phone
        /// </summary>
        public string ShipToPhone
        {
            get { return this._ShipToPhone; }
            set { this._ShipToPhone = value; }
        }

        /// <summary>
        /// Gets or sets the Package Tracking Number
        /// </summary>
        public string TrackingNumber
        {
            get { return this._TrackingNumber; }
            set { this._TrackingNumber = value; }
        }

        /// <summary>
        /// Gets or sets the Destination Country code
        /// </summary>
        public string ShipToCountryCode
        {
            get { return this._ShipToCountryCode; }
            set { this._ShipToCountryCode = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ship to address type is resedential or some else.
        /// Default address type is 'Residential'
        /// </summary>
        public bool ShipToAddressType
        {
            get { return this._ShipToAddressType; }
            set { this._ShipToAddressType = value; }
        }

        /// <summary>
        /// Gets or sets the weight of the package
        /// </summary>
        public decimal PackageWeight
        {
            get { return this._PackageWeight; }
            set { this._PackageWeight = value; }
        }

        /// <summary>
        /// Gets or sets the Pickup type
        /// Bydefault pickup type is  'One Time Pickup'
        /// </summary>        
        public string DropOffType
        {
            get { return this._DropOffType; }
            set { this._DropOffType = value; }
        }

        /// <summary>
        /// Gets or sets the FedEx Service type
        /// </summary>
        public string FedExServiceType
        {
            get { return this._FedExServiceType; }
            set { this._FedExServiceType = value; }
        }

        /// <summary>
        /// Gets or sets the FedEx package type code
        /// </summary>
        public string PackageTypeCode
        {
            get { return this._PackageTypeCode; }
            set { this._PackageTypeCode = value; }
        }

        /// <summary>
        /// Gets or sets the Webservice response code 
        /// </summary>
        public string ErrorCode
        {
            get { return this._ErrorCode; }
            set { this._ErrorCode = value; }
        }

        /// <summary>
        /// Gets or sets the Webservice response code description
        /// </summary>
        public string ErrorDescription
        {
            get
            {
                // FedEx returns "Service type is missing or invalid". Replace this with a user friendly message.
                if (this._ErrorCode.Equals("540"))
                {
                    this._ErrorDescription = "FedEx does not support the selected shipping option to this zip code. Please select another shipping option.";
                }

                return this._ErrorDescription;
            }

            set
            {
                this._ErrorDescription = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the discount rate should be used or not.
        /// </summary>
        public bool UseDiscountRate
        {
            get { return this._UseFedExDiscountRate; }
            set { this._UseFedExDiscountRate = value; }
        }

        /// <summary>
        /// Gets or sets the Client product id
        /// Which used to identify the provider and the providerís software
        /// </summary>
        public string ClientProductId
        {
            get { return this._ClientProductId; }
            set { this._ClientProductId = value; }
        }

        /// <summary>
        /// Gets or sets the Client product version It changes every time the provider re-certifies
        /// </summary>
        public string ClientProductVersion
        {
            get { return this._ClientProductVersion; }
            set { this._ClientProductVersion = value; }
        }

        /// <summary>
        /// Gets or sets the FedEx approved software platform identifier
        /// </summary>
        public string VendorProductPlatform
        {
            get { return this._VendorProductPlatform; }
            set { this._VendorProductPlatform = value; }
        }

        /// <summary>
        /// Gets or sets the original location id
        /// </summary>
        public string OriginLocationID
        {
            get { return this._OriginLocationID; }
            set { this._OriginLocationID = value; }
        }

        /// <summary>
        /// Gets or sets weight unit (Lbs or Kgs)
        /// </summary>
        public string WeightUnit
        {
            get { return this._WeightUnit; }
            set { this._WeightUnit = value; }
        }

        /// <summary>
        /// Gets or sets package dimension unit (IN or CM)
        /// </summary>
        public string DimensionUnit
        {
            get { return this._DimensionUnit; }
            set { this._DimensionUnit = value; }
        }

        /// <summary>
        /// Gets or sets package height value         
        /// </summary>
        public string PackageHeight
        {
            get { return this._PackageHeight; }
            set { this._PackageHeight = value; }
        }

        /// <summary>
        /// Gets or sets package height
        /// </summary>
        public string PackageWidth
        {
            get { return this._PackageWidth; }
            set { this._PackageWidth = value; }
        }

        /// <summary>
        /// Gets or sets package length
        /// </summary>
        public string PackageLength
        {
            get { return this._PackageLength; }
            set { this._PackageLength = value; }
        }

        /// <summary>
        /// Gets or sets the total value of the products being shipped
        /// </summary>
        public decimal TotalCustomsValue
        {
            get { return this._TotalCustomsValue; }
            set { this._TotalCustomsValue = value; }
        }

        /// <summary>
        /// Gets or sets the total value of the products being shipped
        /// </summary>
        public decimal TotalInsuredValue
        {
            get { return this._TotalInsuredValue; }
            set { this._TotalInsuredValue = value; }
        }

        /// <summary>
        /// Gets or sets the shipment charge.
        /// </summary>
        public decimal ShipmentCharge
        {
            get { return this._ShipmentCharge; }
            set { this._ShipmentCharge = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Submits Shipping details to FedEx Web Service to get Rate in US dollars
        /// </summary>
        /// <returns>Returns the shipping rate</returns>
        public decimal GetShippingRate()
        {
            decimal totalShippingCost = 0;

            // Build a RateServices request object
            FedExRateService.RateRequest request = this.CreateRateRequest();

            // This is the call to the web service passing in a RateServicesRequest and returning a RateAvailableServicesReply
            FedExRateService.RateService rateService = new FedExRateService.RateService();

            // Production service Url
            rateService.Url = this._FedExGatewayURL;

            try
            {
                // This is the call to the web service passing in a RateRequest and returning a RateReply
                FedExRateService.RateReply reply = rateService.getRates(request);

                // For Most Fed Ex calls we can consider NOTE, Warning and above a succcess
                // check if the call was successful
                if (this.IsRateResponseValid(reply.HighestSeverity))
                {
                    this.ErrorDescription = reply.Notifications[0].Message;
                    this.ErrorCode = "0";

                    foreach (FedExRateService.RateReplyDetail rateDetail in reply.RateReplyDetails)
                    {
                        foreach (FedExRateService.RatedShipmentDetail shipmentDetail in rateDetail.RatedShipmentDetails)
                        {
                            if (!this.UseDiscountRate && shipmentDetail.EffectiveNetDiscount != null)
                            {
                                totalShippingCost += shipmentDetail.ShipmentRateDetail.TotalNetCharge.Amount - shipmentDetail.EffectiveNetDiscount.Amount;
                                break;
                            }
                            else
                            {
                                totalShippingCost += shipmentDetail.ShipmentRateDetail.TotalNetCharge.Amount;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    this.ErrorCode = reply.Notifications[0].Code;
                    this.ErrorDescription = reply.Notifications[0].Message;
                }
            }
            catch (SoapException e)
            {
                this.ErrorCode = "-1";
                this.ErrorDescription = e.Detail.InnerText;
            }
            catch (Exception e)
            {
                this.ErrorCode = "-1";
                this.ErrorDescription = e.Message;
            }

            return totalShippingCost;
        }
        #endregion
        
        #region Private Helper Methods
        /// <summary>
        /// Create the FedEx rate request.
        /// </summary>
        /// <returns>Returns the RateRequest object.</returns>
        private RateRequest CreateRateRequest()
        {
            // Build the RateRequest
            RateRequest request = new RateRequest();
            request.WebAuthenticationDetail = new WebAuthenticationDetail();
            request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.UserCredential.Key = this.FedExAccessKey;
            request.WebAuthenticationDetail.UserCredential.Password = this.FedExSecurityCode;

            request.ClientDetail = new ClientDetail();
            request.ClientDetail.AccountNumber = this.FedExAccountNumber;
            request.ClientDetail.MeterNumber = this.FedExMeterNumber;

            request.TransactionDetail = new TransactionDetail();

            // This is a reference field for the customer.  Any value can be used and will be provided in the response.
            request.TransactionDetail.CustomerTransactionId = "Rate Request";

            // WSDL version information, value is automatically set from wsdl
            request.Version = new VersionId();
            request.ReturnTransitAndCommit = true;
            request.ReturnTransitAndCommitSpecified = true;
            request.CarrierCodes = new CarrierCodeType[1];
            request.CarrierCodes[0] = CarrierCodeType.FDXE;

            this.SetShipmentDetails(request);

            this.SetOrigin(request);

            this.SetDestination(request);

            this.SetPayment(request);

            this.SetIndividualPackageLineItems(request);

            return request;
        }

        /// <summary>
        /// Set the shipment details.
        /// </summary>
        /// <param name="request">RateRequest object to set the shipment details.</param>
        private void SetShipmentDetails(RateRequest request)
        {
            request.RequestedShipment = new RequestedShipment();

            // Drop off types are BUSINESS_SERVICE_CENTER, DROP_BOX, REGULAR_PICKUP, REQUEST_COURIER, STATION
            request.RequestedShipment.DropoffType = (FedExRateService.DropoffType)Enum.Parse(typeof(FedExRateService.DropoffType), this.DropOffType);

            // Service types are STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND. etc.,
            request.RequestedShipment.ServiceType = (FedExRateService.ServiceType)Enum.Parse(typeof(FedExRateService.ServiceType), this.FedExServiceType);
            request.RequestedShipment.ServiceTypeSpecified = true;

            // Packaging type FEDEX_BOK, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, etc.,
            request.RequestedShipment.PackagingType = (FedExRateService.PackagingType)Enum.Parse(typeof(FedExRateService.PackagingType), this.PackageTypeCode);
            request.RequestedShipment.PackagingTypeSpecified = true;

            // If TotalInsured value is greater than 0, then set related properties
            if (this.TotalInsuredValue > 0)
            {
                request.RequestedShipment.TotalInsuredValue = new Money();
                request.RequestedShipment.TotalInsuredValue.Amount = this.TotalInsuredValue;
                request.RequestedShipment.TotalInsuredValue.Currency = this.CurrencyCode;
            }

            // Shipping date and time
            request.RequestedShipment.ShipTimestamp = DateTime.Now;
            request.RequestedShipment.ShipTimestampSpecified = true;
            if (this.UseDiscountRate)
            {
                // If it is enabled,then set "Account" as the element value, it will send discount rates in the reply.
                request.RequestedShipment.RateRequestTypes = new FedExRateService.RateRequestType[1] { FedExRateService.RateRequestType.ACCOUNT };
            }
            else
            {
                request.RequestedShipment.RateRequestTypes = new FedExRateService.RateRequestType[1] { FedExRateService.RateRequestType.LIST };
            }

            request.RequestedShipment.PackageDetail = RequestedPackageDetailType.INDIVIDUAL_PACKAGES;
            request.RequestedShipment.PackageDetailSpecified = true;
        }

        /// <summary>
        /// Set the reqest origin address.
        /// </summary>
        /// <param name="request">RateRequest object to set the origin address details.</param>
        private void SetOrigin(RateRequest request)
        {
            request.RequestedShipment.Shipper = new Party();
            request.RequestedShipment.Shipper.Address = new FedExRateService.Address();

            // Resdential or Standard
            request.RequestedShipment.Shipper.Address.Residential = this.ShippierAddressType;
            request.RequestedShipment.Shipper.Address.StreetLines = new string[2] { this.ShipperAddress1, this.ShipperAddress2 };
            request.RequestedShipment.Shipper.Address.City = this.ShipperCity;
            request.RequestedShipment.Shipper.Address.StateOrProvinceCode = this.ShipperStateCode;
            request.RequestedShipment.Shipper.Address.PostalCode = this.ShipperZipCode;
            request.RequestedShipment.Shipper.Address.CountryCode = this.ShipperCountryCode;
        }

        /// <summary>
        /// Set the reqest destination address.
        /// </summary>
        /// <param name="request">RateRequest object to set the destination address details.</param>
        private void SetDestination(RateRequest request)
        {
            request.RequestedShipment.Recipient = new Party();
            request.RequestedShipment.Recipient.Address = new Address();
            request.RequestedShipment.Recipient.Address.StreetLines = new string[2] { this.ShipToAddress1, this.ShipToAddress2 };
            request.RequestedShipment.Recipient.Address.City = this.ShipToCity;
            request.RequestedShipment.Recipient.Address.StateOrProvinceCode = this.ShipToStateCode;
            request.RequestedShipment.Recipient.Address.PostalCode = this.ShipToZipCode;
            request.RequestedShipment.Recipient.Address.CountryCode = this.ShipToCountryCode;
            request.RequestedShipment.Recipient.Address.Residential = this.ShipToAddressType;
        }

        /// <summary>
        /// Set the payment details.
        /// </summary>
        /// <param name="request">RateRequest object to set the payment details.</param>
        private void SetPayment(RateRequest request)
        {
            request.RequestedShipment.ShippingChargesPayment = new Payment();

            // Payment options are RECIPIENT, SENDER, THIRD_PARTY
            request.RequestedShipment.ShippingChargesPayment.PaymentType = PaymentType.SENDER;
            request.RequestedShipment.ShippingChargesPayment.PaymentTypeSpecified = true;
            request.RequestedShipment.ShippingChargesPayment.Payor = new Payor();
            request.RequestedShipment.ShippingChargesPayment.Payor.AccountNumber = this.FedExAccountNumber;
        }

        /// <summary>
        /// Set the individual package line item details.
        /// </summary>
        /// <param name="request">RateRequest object to set the package line item details.</param>
        private void SetIndividualPackageLineItems(RateRequest request)
        {
            // Passing individual pieces rate request            
            request.RequestedShipment.PackageCount = "1";

            request.RequestedShipment.RequestedPackageLineItems = new RequestedPackageLineItem[1];
            request.RequestedShipment.RequestedPackageLineItems[0] = new RequestedPackageLineItem();

            // Set the package sequence number
            request.RequestedShipment.RequestedPackageLineItems[0].SequenceNumber = "1";

            // Set the package weight
            request.RequestedShipment.RequestedPackageLineItems[0].Weight = new Weight();
            request.RequestedShipment.RequestedPackageLineItems[0].Weight.Value = this.PackageWeight;
            request.RequestedShipment.RequestedPackageLineItems[0].Weight.Units = (FedExRateService.WeightUnits)Enum.Parse(typeof(FedExRateService.WeightUnits), this.WeightUnit);

            if (this.PackageTypeCode == FedExRateService.PackagingType.YOUR_PACKAGING.ToString())
            {
                // Set the package dimensions
                request.RequestedShipment.RequestedPackageLineItems[0].Dimensions = new Dimensions();
                request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Length = this.PackageLength;
                request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Width = this.PackageWidth;
                request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Height = this.PackageHeight;
                request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Units = (FedExRateService.LinearUnits)Enum.Parse(typeof(FedExRateService.LinearUnits), this.DimensionUnit);
            }
        }

        /// <summary>
        /// Returns true if the RateReply is a SUCCESS or NOTE severity.
        /// </summary>
        /// <param name="highestSeverity">Severity type</param>
        /// <returns>Returns the true or false for the rate service</returns>
        private bool IsRateResponseValid(FedExRateService.NotificationSeverityType highestSeverity)
        {
            // Check if the call was successful
            if (highestSeverity == FedExRateService.NotificationSeverityType.SUCCESS ||
                highestSeverity == FedExRateService.NotificationSeverityType.NOTE ||
                highestSeverity == FedExRateService.NotificationSeverityType.WARNING)
            {
                return true;
            }
            else
            {
                return false;
            }
        } 


        #endregion       
    }
}

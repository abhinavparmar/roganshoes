﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Shipping
{
    public class PeftFedEXRates
    {
        public string Code { get; set; }
        public decimal MonetaryValue { get; set; }
        public string NumberOfDaysForDelivery { get; set; }
        public string DeliveryTime { get; set; }
        public bool IsSaturdayShipping { get; set; }
    }
}

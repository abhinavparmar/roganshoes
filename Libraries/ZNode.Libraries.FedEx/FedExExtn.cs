﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Shipping.FedExRateService;

namespace ZNode.Libraries.Shipping
{
    public partial class FedEx : ZNodeBusinessBase
    {
        #region Public Methods
        /// <summary>
        /// Submits Shipping details to FedEx Web Service to get Rate in US dollars
        /// </summary>
        /// <returns>Returns the shipping rates for all shipping options</returns>
        public List<PeftFedEXRates> GetAllShippingRates(Dictionary<int, decimal> packagecollection)
        {
            List<PeftFedEXRates> fedExRateList = new List<PeftFedEXRates>();
            string rateTypePackageName = WebConfigurationManager.AppSettings["FedExRateTypePackage"] != null && string.IsNullOrEmpty(WebConfigurationManager.AppSettings["FedExRateTypePackage"].ToString()) ? WebConfigurationManager.AppSettings["FedExRateTypePackage"].ToString() : "PAYOR_LIST_PACKAGE";

            // Build a RateServices request object
            FedExRateService.RateRequest request = this.CreateRateRequests(packagecollection);

            // This is the call to the web service passing in a RateServicesRequest and returning a RateAvailableServicesReply
            FedExRateService.RateService rateService = new FedExRateService.RateService();

            // Production service Url
            rateService.Url = this._FedExGatewayURL;

            try
            {
                // This is the call to the web service passing in a RateRequest and returning a RateReply
                FedExRateService.RateReply reply = rateService.getRates(request);

                // For Most Fed Ex calls we can consider NOTE, Warning and above a succcess
                // check if the call was successful
                if (this.IsRateResponseValid(reply.HighestSeverity))
                {
                    if (reply.RateReplyDetails != null)
                    {
                        fedExRateList = BindShipingRate(reply, rateTypePackageName);

                    }
                }
                else
                {
                    this.ErrorCode = reply.Notifications[0].Code;
                    this.ErrorDescription = reply.Notifications[0].Message;

                    //Logging Error
                    this.ErrorCode = "0";
                    ZNodeLoggingBase.LogMessage("PeftFedEx >> Rate Reply Error >> " + reply.Notifications[0].Message.ToString());
                }
            }
            catch (SoapException e)
            {
                ZNodeLoggingBase.LogMessage("PeftFedEx >>  Rate Reply Error >> " + e.Detail.InnerText.ToString());
            }
            catch (Exception e)
            {
                ZNodeLoggingBase.LogMessage("PeftFedEx >>  Rate Reply Error >> " + e.Message.ToString());
                this.ErrorCode = "Connection failed.";
                this.ErrorDescription = "Error while trying to connect with FedEx Host Server.Please try again."; // Response Error code Description
                ZNodeLoggingBase.LogMessage("PeftFedEx >> GetShippingRate >> " + e.ToString());
                return null;
            }

            return fedExRateList;
        }


        #endregion

        #region Private Helper Methods
        /// <summary>
        /// Create the FedEx rate request.
        /// </summary>
        /// <returns>Returns the RateRequest object.</returns>
        private RateRequest CreateRateRequests(Dictionary<int, decimal> packagecollection)
        {
            // Build the RateRequest
            RateRequest request = new RateRequest();

            request.WebAuthenticationDetail = new WebAuthenticationDetail();
            request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.UserCredential.Key = this.FedExAccessKey;
            request.WebAuthenticationDetail.UserCredential.Password = this.FedExSecurityCode;
            request.ClientDetail = new ClientDetail();
            request.ClientDetail.AccountNumber = this.FedExAccountNumber;
            request.ClientDetail.MeterNumber = this.FedExMeterNumber;

            request.TransactionDetail = new TransactionDetail();

            // This is a reference field for the customer.  Any value can be used and will be provided in the response.
            request.TransactionDetail.CustomerTransactionId = "Rate Request";

            // WSDL version information, value is automatically set from wsdl
            request.Version = new VersionId();
            request.ReturnTransitAndCommit = true;
            request.ReturnTransitAndCommitSpecified = true;
            //commenting for ground
            //request.CarrierCodes = new CarrierCodeType[1];
            //request.CarrierCodes[0] = CarrierCodeType.FDXE;

            this.SetShipmentDetail(request);

            this.SetOrigin(request);

            this.SetDestination(request);

            this.SetPayment(request);

            this.SetIndividualPackageLineItemsExtn(request, packagecollection);

            return request;
        }


        /// <summary>
        /// Set the shipment details.
        /// </summary>
        /// <param name="request">RateRequest object to set the shipment details.</param>
        private void SetShipmentDetail(RateRequest request)
        {
            request.RequestedShipment = new RequestedShipment();

            // Drop off types are BUSINESS_SERVICE_CENTER, DROP_BOX, REGULAR_PICKUP, REQUEST_COURIER, STATION
            request.RequestedShipment.DropoffType = (FedExRateService.DropoffType)Enum.Parse(typeof(FedExRateService.DropoffType), this.DropOffType);
            // Service types are STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND. etc.,

            // If ServiceType is omitted, all applicable ServiceTypes are returned.
            //request.RequestedShipment.ServiceType = (FedExRateService.ServiceType)Enum.Parse(typeof(FedExRateService.ServiceType), this.FedExServiceType);
            //request.RequestedShipment.ServiceTypeSpecified = true;

            // Packaging type FEDEX_BOK, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, etc.,
            request.RequestedShipment.PackagingType = (FedExRateService.PackagingType)Enum.Parse(typeof(FedExRateService.PackagingType), this.PackageTypeCode);
            request.RequestedShipment.PackagingTypeSpecified = true;

            //Code for saturday shipping start
            // request.VariableOptions = new ServiceOptionType[1];
            // request.VariableOptions[0] = ServiceOptionType.SATURDAY_DELIVERY;
            //Code for saturday shipping end.

            // If TotalInsured value is greater than 0, then set related properties
            if (this.TotalInsuredValue > 0)
            {
                request.RequestedShipment.TotalInsuredValue = new Money();
                request.RequestedShipment.TotalInsuredValue.Amount = this.TotalInsuredValue;
                request.RequestedShipment.TotalInsuredValue.Currency = this.CurrencyCode;
            }
            // Shipping date and time

            request.RequestedShipment.ShipTimestampSpecified = false;
            #region Znode Default Code
            //if (this.UseDiscountRate)
            //{
            //    // If it is enabled,then set "Account" as the element value, it will send discount rates in the reply.
            //    request.RequestedShipment.RateRequestTypes = new FedExRateService.RateRequestType[1] { FedExRateService.RateRequestType.LIST };
            //}
            //else
            //{
            // request.RequestedShipment.RateRequestTypes = new FedExRateService.RateRequestType[1] { FedExRateService.RateRequestType.LIST };
            //}
            #endregion
            request.RequestedShipment.RateRequestTypes = new FedExRateService.RateRequestType[1] { FedExRateService.RateRequestType.LIST };
            request.RequestedShipment.PackageDetail = RequestedPackageDetailType.INDIVIDUAL_PACKAGES;
            request.RequestedShipment.PackageDetailSpecified = true;
        }

        #endregion

        #region Bind Rate.

        /// <summary>
        /// Bind Shipping Rate List
        /// </summary>
        /// <param name="reply"></param>
        /// <returns></returns>
        private List<PeftFedEXRates> BindShipingRate(FedExRateService.RateReply reply, string packageRateType)
        {
            List<PeftFedEXRates> fedExRate = new List<PeftFedEXRates>();

            foreach (FedExRateService.RateReplyDetail rateDetail in reply.RateReplyDetails)
            {
                PeftFedEXRates fedRate = new PeftFedEXRates();

                //rateDetail.AppliedOptions exclude saturday delivery
                if (rateDetail.AppliedOptions == null)
                {
                    fedRate.Code = rateDetail.ServiceType.ToString();
                    fedRate.DeliveryTime = rateDetail.MaximumTransitTime.ToString();
                    List<RatedShipmentDetail> rateShipmentDetailList = rateDetail.RatedShipmentDetails.OfType<RatedShipmentDetail>().Where(o => o.ShipmentRateDetail.RateType.ToString().ToLower().Equals(packageRateType, StringComparison.OrdinalIgnoreCase)).ToList();
                    if (rateShipmentDetailList != null && rateShipmentDetailList.Count > 0)
                    {
                        foreach (RatedShipmentDetail rateShipmentDetail in rateShipmentDetailList)
                        {
                            fedRate.MonetaryValue = rateShipmentDetail.ShipmentRateDetail.TotalNetFedExCharge.Amount;
                        }
                    }
                    fedExRate.Add(fedRate);
                }
            }

            return fedExRate;
        }

        /// <summary>
        /// Set the individual package line item details.
        /// </summary>
        /// <param name="request">RateRequest object to set the package line item details.</param>
        private void SetIndividualPackageLineItemsExtn(RateRequest request, Dictionary<int, decimal> packageWeightCollection)
        {
            request.RequestedShipment.PackageCount = packageWeightCollection.Count.ToString();
            if (packageWeightCollection != null && packageWeightCollection.Count > 0)
            {
                request.RequestedShipment.RequestedPackageLineItems = new RequestedPackageLineItem[packageWeightCollection.Count];
                int count = 0;
                foreach (KeyValuePair<int, decimal> packageWight in packageWeightCollection)
                {
                    request.RequestedShipment.RequestedPackageLineItems[count] = new RequestedPackageLineItem();
                    // Set the package sequence number
                    request.RequestedShipment.RequestedPackageLineItems[count].SequenceNumber = (count + 1).ToString();

                    // Set the package weight
                    request.RequestedShipment.RequestedPackageLineItems[count].Weight = new Weight();
                    request.RequestedShipment.RequestedPackageLineItems[count].Weight.Value = packageWight.Value;
                    request.RequestedShipment.RequestedPackageLineItems[count].Weight.Units = (FedExRateService.WeightUnits)Enum.Parse(typeof(FedExRateService.WeightUnits), this.WeightUnit);

                    if (this.PackageTypeCode == FedExRateService.PackagingType.YOUR_PACKAGING.ToString())
                    {
                        // Set the package dimensions
                        request.RequestedShipment.RequestedPackageLineItems[count].Dimensions = new Dimensions();
                        request.RequestedShipment.RequestedPackageLineItems[count].Dimensions.Length = this.PackageLength;
                        request.RequestedShipment.RequestedPackageLineItems[count].Dimensions.Width = this.PackageWidth;
                        request.RequestedShipment.RequestedPackageLineItems[count].Dimensions.Height = this.PackageHeight;
                        request.RequestedShipment.RequestedPackageLineItems[count].Dimensions.Units = (FedExRateService.LinearUnits)Enum.Parse(typeof(FedExRateService.LinearUnits), this.DimensionUnit);
                    }
                    count++;
                }
            }
            else
            {
                request.RequestedShipment.RequestedPackageLineItems = new RequestedPackageLineItem[1];
                request.RequestedShipment.RequestedPackageLineItems[0] = new RequestedPackageLineItem();

                // Set the package sequence number
                request.RequestedShipment.RequestedPackageLineItems[0].SequenceNumber = "1";

                // Set the package weight
                request.RequestedShipment.RequestedPackageLineItems[0].Weight = new Weight();
                request.RequestedShipment.RequestedPackageLineItems[0].Weight.Value = this.PackageWeight;
                request.RequestedShipment.RequestedPackageLineItems[0].Weight.Units = (FedExRateService.WeightUnits)Enum.Parse(typeof(FedExRateService.WeightUnits), this.WeightUnit);

                if (this.PackageTypeCode == FedExRateService.PackagingType.YOUR_PACKAGING.ToString())
                {
                    // Set the package dimensions
                    request.RequestedShipment.RequestedPackageLineItems[0].Dimensions = new Dimensions();
                    request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Length = this.PackageLength;
                    request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Width = this.PackageWidth;
                    request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Height = this.PackageHeight;
                    request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Units = (FedExRateService.LinearUnits)Enum.Parse(typeof(FedExRateService.LinearUnits), this.DimensionUnit);
                }
            }
        }
        #endregion
    }


}

using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.ECommerce.Tagging
{
    public class ZNodeTag
    {
        /// <summary>
        /// Returns Products based on category
        /// </summary>
        /// <param name="TagIds">Tag Ids for the products</param>
        /// <param name="CategoryId">Category Id</param>
        /// <returns>Returns the products based on the tag ids</returns>
        public DataTable RetrieveProductsByTagIds(string TagIds, int CategoryId)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString);
            SqlCommand myCommand = new SqlCommand("ZNode_GetProductsByTagIDs", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandTimeout = 0;

            // Add Parameters to SPROC
            SqlParameter parameterTagId = new SqlParameter("@TagIds", SqlDbType.VarChar);
            parameterTagId.Value = TagIds;
            myCommand.Parameters.Add(parameterTagId);

            // Add Parameters to SPROC
            SqlParameter parameterCategoryId = new SqlParameter("@CategoryId", SqlDbType.Int);
            parameterCategoryId.Value = CategoryId;
            myCommand.Parameters.Add(parameterCategoryId);

            // Execute the command
            myConnection.Open();
            
            DataTable dt = new DataTable();

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = myCommand;

            dataAdapter.Fill(dt);
            
            myConnection.Close();

            return dt;
        }

        /// <summary>
        /// Returns Tags based on category
        /// </summary>
        /// <param name="CategoryId">Categgory ID</param>
        /// <param name="_tagIds">Tag Ids for the category</param>
        /// <returns>Returns the tags by category id</returns>
        public DataSet RetrieveTagsByCategoryID(int CategoryId, string _tagIds, int _catalogId)
        {
            SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString);

            SqlDataAdapter myCommand = new SqlDataAdapter("ZNode_GetTagsByCategoryId", myConnection);

            // Mark the Command as a SPROC
            myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
            myCommand.SelectCommand.CommandTimeout = 0;

            // Add Parameters to SPROC
            SqlParameter parameterCategoryId = new SqlParameter("@CategoryId", SqlDbType.Int, 4);
            parameterCategoryId.Value = CategoryId;
            myCommand.SelectCommand.Parameters.Add(parameterCategoryId);

            // Add Parameters to SPROC
            SqlParameter parameterTagId = new SqlParameter("@TagIds", SqlDbType.VarChar);
            parameterTagId.Value = _tagIds;
            myCommand.SelectCommand.Parameters.Add(parameterTagId);

            // Add Parameters to SPROC
            SqlParameter parameterCatalogId = new SqlParameter("@CatalogId", SqlDbType.Int, 4);
            parameterCatalogId.Value = _catalogId;
            myCommand.SelectCommand.Parameters.Add(parameterCatalogId);

            // Create and Fill the DataSet
            DataSet myDataSet = new DataSet();
            myCommand.Fill(myDataSet);

            // close connection
            myConnection.Close();

            // Return DataSet
            return myDataSet;
        }

        /// <summary>
        /// Returns Tags for BreadCrumbs
        /// </summary>
        /// <param name="TagIds">Tag Ids to get breadcrumbs</param>
        /// <returns>Returns the Tags for BreadCrumbs</returns>
        public DataSet GetBreadCrumbsByTagIds(string TagIds)
        {
            SqlConnection myConn = null;
            SqlDataAdapter MyAdapter = null;

            // Create Instance of Connection Object
            string ConnectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ToString();
            myConn = new SqlConnection(ConnectionStr);

            // CREATE INSTANCE OF SQLDATA ADAPTER
            MyAdapter = new SqlDataAdapter("ZNode_GetBreadCrumbsByTagIds", myConn);
            
            // Add Parameters to SPROC
            MyAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            MyAdapter.SelectCommand.CommandTimeout = 0;
            SqlParameter myparam1 = new SqlParameter("@TagIds", SqlDbType.VarChar);
            myparam1.Value = TagIds;

            MyAdapter.SelectCommand.Parameters.Add(myparam1);

            // Fill DataSet
            DataSet MyDataset = new DataSet();

            MyAdapter.Fill(MyDataset);

            // Release Resources
            MyAdapter.Dispose();

            myConn.Close();

            // return dataset
            return MyDataset;
        }
    }
}

﻿using System.Text;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Service;
using System.Data;
using ZNode.Libraries.ECommerce.Catalog;

namespace ZNode.Libraries.ECommerce.Suppliers
{
    /// <summary>
    /// Represents the Znode Receipt
    /// </summary>
    public partial class ZNodeReceipt : ZNodeBusinessBase
    {
        /// <summary>
        ///  To Get Address in Proper Format
        /// </summary>
        /// <param name="address">Address</param>
        /// <returns>string</returns>
        public static string GetAddressString(Address address)
        {
            StringBuilder sbHTMLAddress = new StringBuilder();
            if (address != null)
            {
                string addressName = string.Empty;
                string postalDetails = string.Empty;

                addressName = address.Name;
                sbHTMLAddress.Append(address.FirstName);
                sbHTMLAddress.Append(" ");
                sbHTMLAddress.Append(address.LastName);
                sbHTMLAddress.Append(GetFormattedText(address.CompanyName));
                sbHTMLAddress.Append(GetFormattedText(address.Street));
                sbHTMLAddress.Append(GetFormattedText(address.Street1));
                postalDetails = address.City + "," + address.StateCode + "," + address.PostalCode;
                postalDetails = postalDetails.Trim(',');
                sbHTMLAddress.Append(GetFormattedText(postalDetails));
                sbHTMLAddress.Append(GetFormattedText(address.CountryCode));
                sbHTMLAddress.Append(GetFormattedPhoneNumberText(address.PhoneNumber));
            }

            return sbHTMLAddress.ToString();
        }

        /// <summary>
        /// Get Formated Text
        /// </summary>
        /// <param name="strText">string</param>
        /// <returns>string</returns>
        public static string GetFormattedText(string strText)
        {
            if (!string.IsNullOrEmpty(strText))
            {
                return "<br>" + strText;
            }
            return string.Empty;
        }

        /// <summary>
        /// Get Formates Phone Number
        /// </summary>
        /// <param name="strPhone"></param>
        /// <returns></returns>
        public static string GetFormattedPhoneNumberText(string strPhone)
        {
            if (!string.IsNullOrEmpty(strPhone))
            {
                return "<br>" + "Tel:" + " " + strPhone;
            }
            return string.Empty;
        }

        /// <summary>
        /// Build the order amount information
        /// </summary>
        /// <param name="title">Title of the order</param>
        /// <param name="amount">Amount of the order</param>
        /// <param name="table">data table </param>
        private void BuildShippingOrderAmountInfo(string title, decimal amount, DataTable table, string shippingName)
        {
            // Amount Info
            if (amount == 0 && shippingName.Equals("Ground Shipping", System.StringComparison.OrdinalIgnoreCase))
            {
                DataRow orderDBRow = table.NewRow();
                orderDBRow["Title"] = title;
                orderDBRow["Amount"] = "Free";
                table.Rows.Add(orderDBRow);
            }
            else if (amount == 0 && shippingName.Equals("Ground", System.StringComparison.OrdinalIgnoreCase))
            {
                DataRow orderDBRow = table.NewRow();
                orderDBRow["Title"] = title;
                orderDBRow["Amount"] = "Free";
                table.Rows.Add(orderDBRow);
            }
            else if (amount != 0)
            {
                DataRow orderDBRow = table.NewRow();
                orderDBRow["Title"] = title;
                orderDBRow["Amount"] = amount.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();
                table.Rows.Add(orderDBRow);
            }
        }

    }
}

﻿
namespace ZNode.Libraries.ECommerce.Suppliers
{
    /// <summary>
    /// Provides an interface to allow the creation of different types of Supplier Types.
    /// </summary>
	public interface IZNodeSupplierType
    {
        bool SubmitOrder(ZNode.Libraries.ECommerce.Fulfillment.ZNodeOrderFulfillment Order, ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart ShoppingCart, ZNodeSupplierProperty Supplier);
    }
}

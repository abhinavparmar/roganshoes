﻿using System;

namespace ZNode.Libraries.ECommerce.Suppliers
{
    public partial class ZNodeProductSupplier
    {
        /// <summary>
        /// Get Set Image Path
        /// </summary>
        public string ImagePath
        {
            get;
            set;
        }

        /// <summary>
        ///  Get Set Image Atl Tag
        /// </summary>
        public string ImageAltTag
        {
            get;
            set;
        }
    }
}

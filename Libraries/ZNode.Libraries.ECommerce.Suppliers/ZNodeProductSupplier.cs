using System;

namespace ZNode.Libraries.ECommerce.Suppliers
{
    /// <summary>
    /// Property bag containing supplier settings from the database.
    /// </summary>    
    [Serializable()]
    public partial class ZNodeProductSupplier
    {
        #region Private Variables
        private string _ProductSupplierId = string.Empty;
        private int _Quantity = 0;
        private string _SKU = string.Empty;
        private string _Name = string.Empty;
        private string _Description = string.Empty;
        private decimal _UnitPrice = 0;       
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeProductSupplier class.
        /// </summary>
        public ZNodeProductSupplier()           
        {
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the product supplier id
        /// </summary>
        public string ProductSupplierId
        {
            get { return this._ProductSupplierId; }
            set { this._ProductSupplierId = value; }
        }

        /// <summary>
        /// Gets or sets the SKU
        /// </summary>
        public string SKU
        {
            get { return this._SKU; }
            set { this._SKU = value; }
        }

        /// <summary>
        /// Gets or sets the Name
        /// </summary>
        public string Name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }

        /// <summary>
        /// Gets or sets the Quantity
        /// </summary>
        public int Quantity
        {
            get { return this._Quantity; }
            set { this._Quantity = value; }
        }

        /// <summary>
        /// Gets or sets the unit price
        /// </summary>
        public decimal UnitPrice
        {
            get { return this._UnitPrice; }
            set { this._UnitPrice = value; }
        }

        /// <summary>
        /// Gets the extended price
        /// </summary>
        public decimal ExtendedPrice
        {
            get { return this._UnitPrice * this._Quantity; }           
        }
        #endregion        
    }
}

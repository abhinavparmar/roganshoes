using System;
using System.Text.RegularExpressions;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;

using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Suppliers
{
    /// <summary>
    /// Provides the facilities to send an email receipt to the customer and store owner.
    /// </summary>
    public class ZNodeOrderReceiptTypeDefault : ZNodeBusinessBase
    {
        /// <summary>
        /// Sends the order receipt email to the customer and site sales department.
        /// </summary>
        /// <param name="Order">Order Fulfilment</param>
        /// <param name="ShoppingCart">Shopping Cart</param>
        /// <param name="Supplier">This parameter is ignored for this class. All items in the cart will be included regardless of what the supplier is set to.</param>
        /// <returns>True if the receipt is successfully sent.</returns>
        public bool SubmitOrder(ZNodeOrderFulfillment Order, ZNodeShoppingCart ShoppingCart, ZNodeSupplierProperty Supplier)
        {
            bool orderStatus = false;

            try
            {
                ZNodeReceipt _EmailReceipt = new ZNodeReceipt(Order, ShoppingCart);

                // Cycle through the cart and build an order for the buyer and supplier.                
                string messageText = _EmailReceipt.GenerateEmailReceipt();
                messageText = ReplaceTheOrderReceiptFooter(messageText);
                string recepientEmail = Order.Email;
                string salesEmail = ZNodeConfigManager.SiteConfig.SalesEmail;
                string subject = ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptEmailSubject");
                ZNodeEmail.SendEmail(recepientEmail, salesEmail, salesEmail, subject, messageText, true);

                orderStatus = true;
            }
            catch (Exception ex)
            {
                orderStatus = false;

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Default Order Receipt : " + ex.Message);
            }

            return orderStatus;
        }

        /// <summary>
        /// Submit WEB API Order
        /// </summary>
        /// <param name="Order">ZNodeOrderFulfillment</param>
        /// <param name="ShoppingCart">ZNodeShoppingCart</param>
        /// <param name="Supplier">ZNodeSupplierProperty</param>
        /// <returns>string</returns>
        public string SubmitWebApiOrder(ZNodeOrderFulfillment Order, ZNodeShoppingCart ShoppingCart, ZNodeSupplierProperty Supplier)
        {
            string receipt = "Failure";

            try
            {
                ZNodeReceipt _EmailReceipt = new ZNodeReceipt(Order, ShoppingCart);

                // Cycle through the cart and build an order for the buyer and supplier.                
                string messageText = _EmailReceipt.GenerateEmailReceipt();
                string recepientEmail = Order.Email;
                string salesEmail = ZNodeConfigManager.SiteConfig.SalesEmail;
                string subject = ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptEmailSubject");

                ZNodeEmail.SendEmail(recepientEmail, salesEmail, salesEmail, subject, messageText, true);

                receipt = messageText;
            }
            catch (Exception ex)
            {

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Default Order Receipt : " + ex.Message);
            }

            return receipt;
        }

        #region Zeon Custom Code

        /// <summary>
        /// To replace the footer content
        /// </summary>
        private string ReplaceTheOrderReceiptFooter(string messageText)
        {
            Regex rxFooterContent = new Regex("{FooterText}", RegexOptions.IgnoreCase);
            Regex rxHeaderLogoContent = new Regex("{EmailReceiptLogo}", RegexOptions.IgnoreCase);
            Regex rxHeaderConfirmationContent = new Regex("{OrderConfirmationText}", RegexOptions.IgnoreCase);
            string logoPath = GetLogoHTML();
            messageText = rxFooterContent.Replace(messageText, ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptFooter", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID));
            messageText = rxHeaderLogoContent.Replace(messageText, logoPath);
            messageText = rxHeaderConfirmationContent.Replace(messageText, ZNodeCatalogManager.MessageConfig.GetMessage("OrderEmailConfirmationMessage", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID));
            return messageText;
        }

        /// <summary>
        /// Create HTML for LOgo on Email 
        /// </summary>
        /// <returns>string</returns>
        private string GetLogoHTML()
        {
            string logoHTML = string.Empty;
            string path = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            ZNode.Libraries.ECommerce.Utilities.ZNodeImage zImage = new Utilities.ZNodeImage();
            string format = "<a title='{0}' href='{1}'><img src={2}></a>";
            logoHTML = string.Format(format, ZNodeConfigManager.SiteConfig.StoreName, path, path + zImage.GetImageBySize(ZNodeConfigManager.SiteConfig.MaxCatalogItemSmallWidth, System.IO.Path.GetFileName(ZNodeConfigManager.SiteConfig.LogoPath)).Replace("~", string.Empty));
            return logoHTML;
        }

        #endregion
    }
}

using System;
using System.Xml.Serialization;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Suppliers
{
    /// <summary>
    /// Property bag for suppliers used by the Supplier Type classes.
    /// </summary>    
    [Serializable()]
    [XmlRoot("ZNodeSupplierProperty")]
    public class ZNodeSupplierProperty : ZNodeBusinessBase
    {
        #region Private Variables
        private int _SupplierID;
        private string _ExternalSupplierNo;
        private string _Name;
        private string _Description;
        private string _ContactFirstName;
        private string _ContactLastName;
        private string _ContactPhone;
        private string _ContactEmail;
        private string _NotificationEmailID;
        private string _EmailNotificationTemplate;
        private bool _EnableEmailNotification;
        private int _DisplayOrder;
        private bool _ActiveInd;
        private string _ClassName = string.Empty;
        private ZNodeGenericCollection<ZNodeProductSupplier> _AssociatedItems = new ZNodeGenericCollection<ZNodeProductSupplier>();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeSupplierProperty class.
        /// </summary>
        public ZNodeSupplierProperty()
        {
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the SupplierID
        /// </summary>
        [XmlElement()]
        public int SupplierID
        {
            get { return this._SupplierID; }
            set { this._SupplierID = value; }
        }

        /// <summary>
        /// Gets or sets the ExternalSupplierNo
        /// </summary>
        [XmlElement()]
        public string ExternalSupplierNo
        {
            get { return this._ExternalSupplierNo; }
            set { this._ExternalSupplierNo = value; }
        }

        /// <summary>
        /// Gets or sets the Name
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        /// <summary>
        /// Gets or sets the Description
        /// </summary>
        [XmlElement()]
        public string Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }

        /// <summary>
        /// Gets or sets the ContactFirstName
        /// </summary>
        [XmlElement()]
        public string ContactFirstName
        {
            get { return this._ContactFirstName; }
            set { this._ContactFirstName = value; }
        }

        /// <summary>
        /// Gets or sets the ContactLastName
        /// </summary>
        [XmlElement()]
        public string ContactLastName
        {
            get { return this._ContactLastName; }
            set { this._ContactLastName = value; }
        }

        /// <summary>
        /// Gets or sets the ContactPhone
        /// </summary>
        [XmlElement()]
        public string ContactPhone
        {
            get { return this._ContactPhone; }
            set { this._ContactPhone = value; }
        }

        /// <summary>
        /// Gets or sets the ContactEmail
        /// </summary>
        [XmlElement()]
        public string ContactEmail
        {
            get { return this._ContactEmail; }
            set { this._ContactEmail = value; }
        }

        /// <summary>
        /// Gets or sets the NotificationEmailId
        /// </summary>
        [XmlElement()]
        public string NotificationEmailID
        {
            get { return this._NotificationEmailID; }
            set { this._NotificationEmailID = value; }
        }

        /// <summary>
        /// Gets or sets the EmailNotificationTemplate
        /// </summary>
        [XmlElement()]
        public string EmailNotificationTemplate
        {
            get { return this._EmailNotificationTemplate; }
            set { this._EmailNotificationTemplate = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to EnableEmailNotification
        /// </summary>
        [XmlElement()]
        public bool EnableEmailNotification
        {
            get { return this._EnableEmailNotification; }
            set { this._EnableEmailNotification = value; }
        }

        /// <summary>
        /// Gets or sets the DisplayOrder
        /// </summary>
        [XmlElement()]
        public int DisplayOrder
        {
            get { return this._DisplayOrder; }
            set { this._DisplayOrder = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the activeind
        /// </summary>
        [XmlElement()]
        public bool ActiveInd
        {
            get { return this._ActiveInd; }
            set { this._ActiveInd = value; }
        }

        /// <summary>
        /// Gets or sets the class name
        /// </summary>
        [XmlElement()]
        public string ClassName
        {
            get { return this._ClassName; }
            set { this._ClassName = value; }
        }

        /// <summary>
        /// Gets or sets the activeind
        /// </summary>
        [XmlIgnore()]
        public ZNodeGenericCollection<ZNodeProductSupplier> AssociatedItems
        {
            get { return this._AssociatedItems; }
            set { this._AssociatedItems = value; }
        }
        #endregion

        #region Static Create
        /// <summary>
        /// This static method creates supplier object using XML Serializer
        /// </summary>
        /// <param name="SupplierId">Supplier ID</param>
        /// <returns>return the Supplier details in XML</returns>
        public static ZNodeSupplierProperty Create(int SupplierId)
        {
            SupplierHelper supplierHelper = new SupplierHelper();
            string xmlOut = supplierHelper.GetSupplierXMLById(SupplierId);

            // serialize the object
            ZNodeSerializer ser = new ZNodeSerializer();

            return (ZNodeSupplierProperty)ser.GetContentFromString(xmlOut, typeof(ZNodeSupplierProperty));
        }
        #endregion
    }
}

using System;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Suppliers
{
    /// <summary>
    /// Factory for Supplier Types. Manages what products in a cart should be sent to which supplier.
    /// </summary>
    public class ZNodeSupplierOption : ZNodeBusinessBase
    {
        #region Private Variables
        private ZNodeGenericCollection<ZNodeSupplierProperty> _SupplierCollection = new ZNodeGenericCollection<ZNodeSupplierProperty>();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the SupplierID
        /// </summary>        
        private ZNodeGenericCollection<ZNodeSupplierProperty> SupplierCollection
        {
            get { return this._SupplierCollection; }
            set { this._SupplierCollection = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Submit the order to the supplier.
        /// </summary>
        /// <param name="Order">Order details</param>
        /// <param name="ShoppingCart">Shopping Cart</param>
        /// <returns>Returns the order status</returns>
        public bool SubmitOrder(ZNodeOrderFulfillment Order, ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart ShoppingCart)
        {
            bool orderStatus = true;

            ZNodeProductSupplier productSupplier = null;

            // Loop through the Order Line Items
            foreach (ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                int supplierId = cartItem.Product.SupplierID;

                if (supplierId > 0)
                {
                    productSupplier = new ZNodeProductSupplier();
                    productSupplier.ProductSupplierId = "Product" + cartItem.Product.ProductID;
                    productSupplier.Name = cartItem.Product.Name;
                    productSupplier.SKU = cartItem.Product.SKU;
                    productSupplier.Description = cartItem.Product.ShoppingCartDescription;

                    if (cartItem.Product.SelectedSKU.SKUID > 0)
                    {
                        productSupplier.ProductSupplierId = "Sku" + cartItem.Product.SelectedSKU.SKUID;
                        productSupplier.SKU = cartItem.Product.SelectedSKU.SKU;
                    }

                    productSupplier.Quantity = cartItem.Quantity;
                    productSupplier.UnitPrice = cartItem.TieredPricing;

                    /*******
                     *Revision History : Zeon Cutomization 
                     *Changes done on date : 18/Sep/2014
                     *Purpose : Set Image file name and image alt tag
                     * ******/
                    productSupplier.ImagePath = cartItem.Product.ThumbnailImageFilePath;
                    productSupplier.ImageAltTag = cartItem.Product.ImageAltTag;

                    this.AddSupplier(supplierId, productSupplier);
                }

                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOnItems.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        supplierId = addOnValue.SupplierID;

                        if (supplierId > 0)
                        {
                            productSupplier = new ZNodeProductSupplier();

                            productSupplier.ProductSupplierId = "AddOnValue" + addOnValue.AddOnValueID;
                            productSupplier.Name = addOnValue.Name;
                            productSupplier.Description = addOn.Name + " - " + addOnValue.Name;
                            productSupplier.SKU = addOnValue.SKU;
                            productSupplier.Quantity = cartItem.Quantity;
                            productSupplier.UnitPrice = addOnValue.FinalPrice;

                            this.AddSupplier(supplierId, productSupplier);
                        }
                    }
                }
            }

            System.Reflection.Assembly supplierAssebmly = System.Reflection.Assembly.GetExecutingAssembly();
            string assemblyName = supplierAssebmly.GetName().Name;

            foreach (ZNodeSupplierProperty supplier in this._SupplierCollection)
            {
                string className = supplier.ClassName;

                // Instantiate the supplier type based on class name.
                if (!string.IsNullOrEmpty(className))
                {
                    try
                    {
                        IZNodeSupplierType supplierType = (IZNodeSupplierType)supplierAssebmly.CreateInstance(assemblyName + "." + className);
                        orderStatus &= supplierType.SubmitOrder(Order, ShoppingCart, supplier);
                    }
                    catch (Exception ex)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Error while instantiating supplier type: " + className);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                    }
                }
            }

            // default email receipt for buyer and store owner
            ZNodeOrderReceiptTypeDefault orderReceiptType = new ZNodeOrderReceiptTypeDefault();
            orderStatus &= orderReceiptType.SubmitOrder(Order, ShoppingCart, null);

            return orderStatus;
        }

        //Web API
        public string SubmitWebAPIOrder(ZNodeOrderFulfillment Order, ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart ShoppingCart)
        {
            bool orderStatus = true;

            ZNodeProductSupplier productSupplier = null;

            // Loop through the Order Line Items
            foreach (ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                int supplierId = cartItem.Product.SupplierID;

                if (supplierId > 0)
                {
                    productSupplier = new ZNodeProductSupplier();
                    productSupplier.ProductSupplierId = "Product" + cartItem.Product.ProductID;
                    productSupplier.Name = cartItem.Product.Name;
                    productSupplier.SKU = cartItem.Product.SKU;
                    productSupplier.Description = cartItem.Product.ShoppingCartDescription;

                    if (cartItem.Product.SelectedSKU.SKUID > 0)
                    {
                        productSupplier.ProductSupplierId = "Sku" + cartItem.Product.SelectedSKU.SKUID;
                        productSupplier.SKU = cartItem.Product.SelectedSKU.SKU;
                    }

                    productSupplier.Quantity = cartItem.Quantity;
                    productSupplier.UnitPrice = cartItem.TieredPricing;

                    this.AddSupplier(supplierId, productSupplier);
                }

                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOnItems.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        supplierId = addOnValue.SupplierID;

                        if (supplierId > 0)
                        {
                            productSupplier = new ZNodeProductSupplier();

                            productSupplier.ProductSupplierId = "AddOnValue" + addOnValue.AddOnValueID;
                            productSupplier.Name = addOnValue.Name;
                            productSupplier.Description = addOn.Name + " - " + addOnValue.Name;
                            productSupplier.SKU = addOnValue.SKU;
                            productSupplier.Quantity = cartItem.Quantity;
                            productSupplier.UnitPrice = addOnValue.FinalPrice;

                            this.AddSupplier(supplierId, productSupplier);
                        }
                    }
                }
            }

            System.Reflection.Assembly supplierAssebmly = System.Reflection.Assembly.GetExecutingAssembly();
            string assemblyName = supplierAssebmly.GetName().Name;

            foreach (ZNodeSupplierProperty supplier in this._SupplierCollection)
            {
                string className = supplier.ClassName;

                // Instantiate the supplier type based on class name.
                if (!string.IsNullOrEmpty(className))
                {
                    try
                    {
                        IZNodeSupplierType supplierType = (IZNodeSupplierType)supplierAssebmly.CreateInstance(assemblyName + "." + className);
                        orderStatus &= supplierType.SubmitOrder(Order, ShoppingCart, supplier);
                    }
                    catch (Exception ex)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Error while instantiating supplier type: " + className);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                    }
                }
            }

            // default email receipt for buyer and store owner
            ZNodeOrderReceiptTypeDefault orderReceiptType = new ZNodeOrderReceiptTypeDefault();
            return orderReceiptType.SubmitWebApiOrder(Order, ShoppingCart, null);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Gets a supplier from the local supplier collection.
        /// </summary>
        /// <param name="SupplierId">Supplier id</param>
        /// <returns>Returns the supplier details</returns>
        private ZNodeSupplierProperty GetItem(int SupplierId)
        {
            foreach (ZNodeSupplierProperty supplier in this._SupplierCollection)
            {
                if (supplier.SupplierID == SupplierId)
                {
                    return supplier;
                }
            }

            return null;
        }

        /// <summary>
        /// Adds a supplier to the local collection.
        /// </summary>
        /// <param name="SupplierId">Supplier id</param>
        /// <param name="SupplierItem">Supplier item</param>
        private void AddSupplier(int SupplierId, ZNodeProductSupplier SupplierItem)
        {
            ZNodeSupplierProperty supplier = null;

            if (SupplierId > 0)
            {
                supplier = this.GetItem(SupplierId);

                if (supplier == null)
                {
                    supplier = ZNodeSupplierProperty.Create(SupplierId);

                    this._SupplierCollection.Add(supplier);
                }

                this.AddSupplierItem(SupplierItem, supplier.AssociatedItems);
            }
        }

        /// <summary>
        /// Returns the index of a supplier in our collection.
        /// </summary>
        /// <param name="ProductSupplierId">Products of the supplier</param>
        /// <param name="ProductSupplierCollection">Product Supplier Collection</param>
        /// <returns>Returns the index</returns>
        private int IndexOf(string ProductSupplierId, ZNodeGenericCollection<ZNodeProductSupplier> ProductSupplierCollection)
        {
            int index = -1;

            if (ProductSupplierId.Length > 0)
            {
                int counter = 0;

                foreach (ZNodeProductSupplier prodSupplier in ProductSupplierCollection)
                {
                    if (prodSupplier.ProductSupplierId.Equals(ProductSupplierId, StringComparison.OrdinalIgnoreCase))
                    {
                        index = counter;
                        break;
                    }

                    counter++;
                }
            }

            return index;
        }

        /// <summary>
        /// Add a supplier item
        /// </summary>
        /// <param name="ProductSupplier">Products of the supplier</param>
        /// <param name="ProductSupplierCollection">Product supplier collection</param>
        private void AddSupplierItem(ZNodeProductSupplier ProductSupplier, ZNodeGenericCollection<ZNodeProductSupplier> ProductSupplierCollection)
        {
            int index = this.IndexOf(ProductSupplier.ProductSupplierId, ProductSupplierCollection);

            if (index >= 0)
            {
                ProductSupplierCollection[index].Quantity += ProductSupplier.Quantity;
            }
            else
            {
                ProductSupplierCollection.Add(ProductSupplier);
            }
        }
        #endregion
    }
}

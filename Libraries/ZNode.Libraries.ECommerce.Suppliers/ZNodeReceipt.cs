﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Xsl;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Suppliers
{
    /// <summary>
    /// Represents the Znode Receipt
    /// </summary>
    public partial class ZNodeReceipt : ZNodeBusinessBase
    {
        #region Member Variables

        private ZNodeOrderFulfillment _Order = new ZNodeOrderFulfillment();
        private PortalService _Portal = new PortalService();
        private string _FeedBackUrl = string.Empty;
        private ZNodeShoppingCart _ShoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeReceipt class.
        /// </summary>
        /// <param name="order">Order fulfilment</param>
        public ZNodeReceipt(ZNodeOrderFulfillment order)
        {
            this._Order = order;

            if (ZNodeShoppingCart.CurrentShoppingCart(order.PortalId) != null)
            {
                this._ShoppingCart = ZNodeShoppingCart.CurrentShoppingCart(order.PortalId);
            }
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeReceipt class.
        /// </summary>
        /// <param name="order">order fulfilment</param>
        /// <param name="feedbackUrl">Feed back URL</param>
        public ZNodeReceipt(ZNodeOrderFulfillment order, string feedbackUrl)
        {
            this._Order = order;
            this._FeedBackUrl = feedbackUrl;

            if (ZNodeShoppingCart.CurrentShoppingCart(order.PortalId) != null)
            {
                this._ShoppingCart = ZNodeShoppingCart.CurrentShoppingCart(order.PortalId);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <param name="shoppingCart"></param>
        public ZNodeReceipt(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart = null)
        {
            this._Order = order;
            if (this._ShoppingCart == null)
            {
                this._ShoppingCart = shoppingCart;

            }
            else
            {
                if (ZNodeShoppingCart.CurrentShoppingCart(order.PortalId) != null)
                {
                    this._ShoppingCart = ZNodeShoppingCart.CurrentShoppingCart(order.PortalId);
                }
            }
        }

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Order
        /// </summary>
        public ZNodeOrderFulfillment Order
        {
            get { return this._Order; }
            set { this._Order = value; }
        }

        /// <summary>
        /// Gets or sets the Portal
        /// </summary>
        public PortalService Portal
        {
            get { return this._Portal; }
            set { this._Portal = value; }
        }

        /// <summary>
        /// Gets or sets the FeedbackURL
        /// </summary>
        public string FeedbackUrl
        {
            get
            {
                // If feedback url is empty the build the url
                if (string.IsNullOrEmpty(this._FeedBackUrl))
                {
                    StringBuilder url = new StringBuilder();
                    url.Append(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority));
                    if (HttpContext.Current.Request.ApplicationPath.EndsWith("/"))
                    {
                        url.Append(HttpContext.Current.Request.ApplicationPath);
                    }
                    else
                    {
                        url.Append(HttpContext.Current.Request.ApplicationPath).Append("/");
                    }

                    url.Append("customerfeedback.aspx");
                    this._FeedBackUrl = url.ToString();
                }

                return this._FeedBackUrl;
            }

            set
            {
                this._FeedBackUrl = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get the Dynamic HTML Receipt
        /// </summary>
        /// <returns>Returns the HTML</returns>
        public string GetDynamicHtmlReceipt()
        {
            return this.GetDynamicHtmlReceipt(null);
        }

        /// <summary>
        /// Get the Dynamic HTML Receipt based on the supplier
        /// </summary>
        /// <param name="supplier">Supplier details</param>
        /// <returns>Returns the HTML</returns>
        public string GetDynamicHtmlReceipt(ZNodeSupplierProperty supplier)
        {
            string fullHtml = this.GenerateReceipt(supplier);

            HtmlParser html = new HtmlParser(fullHtml);

            html.CurIndex = fullHtml.IndexOf("<body", StringComparison.CurrentCultureIgnoreCase) + 2;

            html.ParseHtml();

            return html.HtmlBlockContents;
        }

        /// <summary>
        /// Generate the email receipt
        /// </summary>
        /// <returns>Returns the email receipt</returns>
        public string GenerateEmailReceipt()
        {
            return this.GenerateEmailReceipt(null);
        }

        /// <summary>
        /// Generate the email receipt
        /// </summary>
        /// <param name="supplier">Supplier Details</param>
        /// <returns>Returns the email receipt</returns>
        public string GenerateEmailReceipt(ZNodeSupplierProperty supplier)
        {
            string fullHtml = this.GenerateReceipt(supplier, true);

            HtmlParser html = new HtmlParser(fullHtml);

            html.CurIndex = fullHtml.IndexOf("<html", StringComparison.CurrentCultureIgnoreCase) + 2;

            html.ParseHtml();

            return html.HtmlBlockContents;
        }

        /// <summary>
        /// Generate the receipt using XSL Transformation
        /// </summary>
        /// <param name="_Order">Order details</param>
        /// <param name="ShoppingCart">Shopping cart</param>
        /// <param name="supplier">Supplier details</param>
        /// <returns>Returns the receipt text</returns>
        public string GetReceiptText(ZNodeOrderFulfillment _Order, ZNodeShoppingCart ShoppingCart, ZNodeSupplierProperty supplier)
        {
            string templatePath = System.Web.HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "Receipt.xsl");

            XmlDocument xmlDoc = new XmlDocument();
            XmlElement rootElement = this.GetElement(xmlDoc, "Order", string.Empty);

            rootElement.AppendChild(this.GetElement(xmlDoc, "SiteName", ZNodeConfigManager.SiteConfig.CompanyName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "AccountId", this._Order.AccountID.ToString()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ReceiptText", ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptConfirmationIntroText")));

            rootElement.AppendChild(this.GetElement(xmlDoc, "CustomerServiceEmail", ZNodeConfigManager.SiteConfig.CustomerServiceEmail));
            rootElement.AppendChild(this.GetElement(xmlDoc, "CustomerServicePhoneNumber", ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber));

            if (this._Order.CardTransactionID != null)
            {
                rootElement.AppendChild(this.GetElement(xmlDoc, "TransactionID", this._Order.CardTransactionID));
                rootElement.AppendChild(this.GetElement(xmlDoc, "CardTransactionLabel", "TransactionID:"));
            }

            rootElement.AppendChild(this.GetElement(xmlDoc, "PromotionCode", this._Order.CouponCode));
            if (this._Order.PurchaseOrderNumber != null)
            {
                rootElement.AppendChild(this.GetElement(xmlDoc, "PONumber", this._Order.PurchaseOrderNumber));
                rootElement.AppendChild(this.GetElement(xmlDoc, "PurchaseNumberLabel", "Purchase Order Number:"));
            }

            rootElement.AppendChild(this.GetElement(xmlDoc, "OrderId", this._Order.OrderID.ToString()));
            //rootElement.AppendChild(this.GetElement(xmlDoc, "OrderDate",this._Order.OrderDate.Date.ToString("MM/dd/yyyy")));

            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingName", ShoppingCart.Shipping.ShippingName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "PaymentName", ShoppingCart.Payment.PaymentName));

            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressCompanyName", this._Order.ShippingAddress.CompanyName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressFirstName", this._Order.ShippingAddress.FirstName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressLastName", this._Order.ShippingAddress.LastName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressStreet1", this._Order.ShippingAddress.Street));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressStreet2", this._Order.ShippingAddress.Street1));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressCity", this._Order.ShippingAddress.City));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressStateCode", this._Order.ShippingAddress.StateCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressPostalCode", this._Order.ShippingAddress.PostalCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressCountryCode", this._Order.ShippingAddress.CountryCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressPhoneNumber", this._Order.ShippingAddress.PhoneNumber));

            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressCompanyName", this._Order.BillingAddress.CompanyName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressFirstName", this._Order.BillingAddress.FirstName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressLastName", this._Order.BillingAddress.LastName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressStreet1", this._Order.BillingAddress.Street));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressStreet2", this._Order.BillingAddress.Street1));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressCity", this._Order.BillingAddress.City));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressStateCode", this._Order.BillingAddress.StateCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressPostalCode", this._Order.BillingAddress.PostalCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressCountryCode", this._Order.BillingAddress.CountryCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressPhoneNumber", this._Order.BillingAddress.PhoneNumber));

            XmlElement items = xmlDoc.CreateElement("Items");

            decimal totalAmount = 0;

            // loop through each coupon code list
            foreach (ZNodeProductSupplier SupplierItem in supplier.AssociatedItems)
            {
                totalAmount += SupplierItem.ExtendedPrice;

                // Add Supplier Line Item
                XmlElement item = xmlDoc.CreateElement("Item");

                item.AppendChild(this.GetElement(xmlDoc, "Quantity", SupplierItem.Quantity.ToString()));
                item.AppendChild(this.GetElement(xmlDoc, "Name", SupplierItem.Name));
                item.AppendChild(this.GetElement(xmlDoc, "SKU", SupplierItem.SKU));
                item.AppendChild(this.GetElement(xmlDoc, "Description", SupplierItem.Description));
                item.AppendChild(this.GetElement(xmlDoc, "Note", string.Empty));
                item.AppendChild(this.GetElement(xmlDoc, "Price", SupplierItem.UnitPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
                item.AppendChild(this.GetElement(xmlDoc, "Extended", SupplierItem.ExtendedPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));

                items.AppendChild(item);
            }

            rootElement.AppendChild(this.GetElement(xmlDoc, "SubTotal", totalAmount.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "DiscountAmount", "-" + 0.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingName", string.Empty));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingCost", 0.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "TotalCost", totalAmount.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));

            if (this._Order.AdditionalInstructions != null)
            {
                rootElement.AppendChild(this.GetElement(xmlDoc, "AdditionalInstructions", string.Empty));
                rootElement.AppendChild(this.GetElement(xmlDoc, "AdditionalInstructionsLabel", string.Empty));
            }

            rootElement.AppendChild(items);
            xmlDoc.AppendChild(rootElement);

            string cssFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/css/receipt.css");
            StreamReader streamReader = new StreamReader(cssFilePath);
            rootElement.AppendChild(this.GetElement(xmlDoc, "ReceiptStyleSheet", streamReader.ReadToEnd()));

            streamReader.Close();
            streamReader.Dispose();

            // Use a memorystream to store the result into the memory
            MemoryStream ms = new MemoryStream();

            XslCompiledTransform xsl = new XslCompiledTransform();
            if (supplier == null)
            {
                xsl.Load(templatePath);
            }
            else if (supplier.EmailNotificationTemplate.Length != 0)
            {
                xsl.Load(XmlReader.Create(new StringReader(supplier.EmailNotificationTemplate)));
            }
            else
            {
                xsl.Load(templatePath);
            }

            xsl.Transform(xmlDoc, null, ms);

            // Move to the begining 
            ms.Seek(0, SeekOrigin.Begin);

            // Pass the memorystream to a streamreader
            StreamReader sr = new StreamReader(ms);

            return sr.ReadToEnd();
        }

        /// <summary>
        /// Generate the receipt using XSL Transformation
        /// </summary>
        /// <param name="_Order">Order Fulfilment</param>
        /// <param name="ShoppingCart">Shopping cart</param>
        /// <returns>Returns the receipt text</returns>
        public string GetReceiptText(ZNodeOrderFulfillment _Order, ZNodeShoppingCart ShoppingCart)
        {
            string CurrentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;

            CurrentCulture = ZNodeCatalogManager.CultureInfo;

            defaultTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "Receipt.xsl");

            if (CurrentCulture != string.Empty)
            {
                templatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "Receipt_" + CurrentCulture + ".xsl");
            }
            else
            {
                templatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "Receipt.xsl");
            }

            // Check the receipt template exists, if not exists then set default receipt template
            FileInfo fileinfo = new FileInfo(templatePath);
            if (!fileinfo.Exists)
            {
                templatePath = defaultTemplatePath;
            }

            XmlDocument xmlDoc = new XmlDocument();
            XmlElement rootElement = this.GetElement(xmlDoc, "Order", string.Empty);

            rootElement.AppendChild(this.GetElement(xmlDoc, "SiteName", ZNodeConfigManager.SiteConfig.CompanyName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "AccountId", this._Order.AccountID.ToString()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ReceiptText", ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptConfirmationIntroText", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID)));
            rootElement.AppendChild(this.GetElement(xmlDoc, "CustomerServiceEmail", ZNodeConfigManager.SiteConfig.CustomerServiceEmail));
            rootElement.AppendChild(this.GetElement(xmlDoc, "CustomerServicePhoneNumber", ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber));
            rootElement.AppendChild(this.GetElement(xmlDoc, "FeedBack", this._FeedBackUrl));

            if (this._Order.CardTransactionID != null)
            {
                rootElement.AppendChild(this.GetElement(xmlDoc, "TransactionID", this._Order.CardTransactionID));
                rootElement.AppendChild(this.GetElement(xmlDoc, "CardTransactionLabel", "TransactionID:"));
            }

            rootElement.AppendChild(this.GetElement(xmlDoc, "PromotionCode", this._Order.CouponCode));

            if (this._Order.PurchaseOrderNumber != null)
            {
                rootElement.AppendChild(this.GetElement(xmlDoc, "PONumber", this._Order.PurchaseOrderNumber));
                rootElement.AppendChild(this.GetElement(xmlDoc, "PurchaseNumberLabel", "Purchase Order Number:"));
            }

            rootElement.AppendChild(this.GetElement(xmlDoc, "OrderId", this._Order.OrderID.ToString()));
            //rootElement.AppendChild(this.GetElement(xmlDoc, "OrderDate",this._Order.OrderDate.Date.ToString("MM/dd/yyyy")));

            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingName", ShoppingCart.Shipping.ShippingName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "PaymentName", ShoppingCart.Payment.PaymentName));

            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressCompanyName", this._Order.ShippingAddress.CompanyName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressFirstName", this._Order.ShippingAddress.FirstName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressLastName", this._Order.ShippingAddress.LastName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressStreet1", this._Order.ShippingAddress.Street));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressStreet2", this._Order.ShippingAddress.Street1));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressCity", this._Order.ShippingAddress.City));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressStateCode", this._Order.ShippingAddress.StateCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressPostalCode", this._Order.ShippingAddress.PostalCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressCountryCode", this._Order.ShippingAddress.CountryCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingAddressPhoneNumber", this._Order.ShippingAddress.PhoneNumber));

            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressCompanyName", this._Order.BillingAddress.CompanyName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressFirstName", this._Order.BillingAddress.FirstName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressLastName", this._Order.BillingAddress.LastName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressStreet1", this._Order.BillingAddress.Street));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressStreet2", this._Order.BillingAddress.Street1));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressCity", this._Order.BillingAddress.City));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressStateCode", this._Order.BillingAddress.StateCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressPostalCode", this._Order.BillingAddress.PostalCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressCountryCode", this._Order.BillingAddress.CountryCode));
            rootElement.AppendChild(this.GetElement(xmlDoc, "BillingAddressPhoneNumber", this._Order.BillingAddress.PhoneNumber));

            XmlElement items = xmlDoc.CreateElement("Items");

            foreach (ZNodeShoppingCartItem shoppingCartItem in ShoppingCart.ShoppingCartItems)
            {
                XmlElement item = xmlDoc.CreateElement("Item");
                item.AppendChild(this.GetElement(xmlDoc, "Quantity", shoppingCartItem.Quantity.ToString()));

                ZNodeGenericCollection<ZNodeDigitalAsset> assetCollection = shoppingCartItem.Product.ZNodeDigitalAssetCollection;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(shoppingCartItem.Product.Name + "<br />");

                if (assetCollection.Count > 0)
                {
                    //sb.Append("DownloadLink : <a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>" + shoppingCartItem.Product.DownloadLink + "</a><br />");

                    foreach (ZNodeDigitalAsset digitalAsset in assetCollection)
                    {
                        sb.Append(digitalAsset.DigitalAsset + "<br />");
                    }
                }

                item.AppendChild(this.GetElement(xmlDoc, "Name", sb.ToString()));
                item.AppendChild(this.GetElement(xmlDoc, "SKU", shoppingCartItem.Product.SKU));
                item.AppendChild(this.GetElement(xmlDoc, "Description", shoppingCartItem.Product.ShoppingCartDescription));
                item.AppendChild(this.GetElement(xmlDoc, "Note", string.Empty));
                item.AppendChild(this.GetElement(xmlDoc, "Price", shoppingCartItem.UnitPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
                item.AppendChild(this.GetElement(xmlDoc, "Extended", shoppingCartItem.ExtendedPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));

                items.AppendChild(item);
            }

            rootElement.AppendChild(this.GetElement(xmlDoc, "TaxCostValue", this._Order.TaxCost.ToString()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "SalesTaxValue", this._Order.SalesTax.ToString()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "VATCostValue", this._Order.VAT.ToString()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "HSTCostValue", this._Order.HST.ToString()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "PSTCostValue", this._Order.PST.ToString()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "GSTCostValue", this._Order.GST.ToString()));

            rootElement.AppendChild(this.GetElement(xmlDoc, "SubTotal", this._Order.SubTotal.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "TaxCost", this._Order.TaxCost.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "SalesTax", this._Order.SalesTax.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "VAT", this._Order.VAT.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "HST", this._Order.HST.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "PST", this._Order.PST.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "GST", this._Order.GST.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));

            rootElement.AppendChild(this.GetElement(xmlDoc, "DiscountAmount", "-" + this._Order.DiscountAmount.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingName", ShoppingCart.Shipping.ShippingName));
            rootElement.AppendChild(this.GetElement(xmlDoc, "ShippingCost", this._Order.ShippingCost.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));
            rootElement.AppendChild(this.GetElement(xmlDoc, "TotalCost", this._Order.Total.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix()));

            if (this._Order.AdditionalInstructions != null)
            {
                rootElement.AppendChild(this.GetElement(xmlDoc, "AdditionalInstructions", this._Order.AdditionalInstructions));
                rootElement.AppendChild(this.GetElement(xmlDoc, "AdditionalInstructionsLabel", "AdditionalInstructions"));
            }

            rootElement.AppendChild(items);

            xmlDoc.AppendChild(rootElement);

            string cssFilePath = System.Web.HttpContext.Current.Server.MapPath(ZNodeCatalogManager.GetReceiptCssPathByLocale(ZNodeCatalogManager.Theme, "Receipt"));
            StreamReader streamReader = new StreamReader(cssFilePath);
            rootElement.AppendChild(this.GetElement(xmlDoc, "ReceiptStyleSheet", streamReader.ReadToEnd()));

            streamReader.Close();
            streamReader.Dispose();

            // Use a memorystream to store the result into the memory
            MemoryStream ms = new MemoryStream();

            XslCompiledTransform xsl = new XslCompiledTransform();
            xsl.Load(templatePath);

            xsl.Transform(xmlDoc, null, ms);

            // Move to the begining 
            ms.Seek(0, SeekOrigin.Begin);

            // Pass the memorystream to a streamreader
            StreamReader sr = new StreamReader(ms);

            return sr.ReadToEnd();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Generate the html receipt using HTML Parser
        /// </summary>
        /// <param name="supplier">supplier details</param>
        /// <returns>Returns the generated receipt</returns>
        private string GenerateReceipt(ZNodeSupplierProperty supplier)
        {
            return this.GenerateReceipt(supplier, false);
        }

        /// <summary>
        /// Generate the html receipt using HTML Parser
        /// </summary>
        /// <param name="supplier">Supplier details</param>
        /// <param name="includeReceiptStyleSheet">to include the style sheet or not</param>
        /// <returns>returns the generated receipt</returns>
        private string GenerateReceipt(ZNodeSupplierProperty supplier, bool includeReceiptStyleSheet)
        {
            string htmlTemplatePath = string.Empty;
            string spacerImagePath = string.Empty;
            string CurrentCulture = string.Empty;
            string defaultHtmlTemplatePath = string.Empty;

            // TemplatePath
            CurrentCulture = ZNodeCatalogManager.CultureInfo;

            defaultHtmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "Receipt.htm");

            // modified receipt file selection as we are using in-line styles for displaying template on mail content.
            if (CurrentCulture != string.Empty)
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath("~/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Receipt/" + "Receipt_" + CurrentCulture + ".htm");
            }
            else
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath("~/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Receipt/" + "Receipt.htm");
            }

            // For Mobile Browser
            if (ZNodeCatalogManager.Theme == "Mobile" || ZNodeCatalogManager.Theme == "MobileApp" || (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null))
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "Receipt_Mobile.htm");
            }

            // Check the receipt template exists, if not exists then set default receipt template
            FileInfo fileinfo = new FileInfo(htmlTemplatePath);

            if (!fileinfo.Exists)
            {
                htmlTemplatePath = defaultHtmlTemplatePath;
            }

            // Order DataTable
            // Create Order Table
            DataTable OrderTable = new DataTable();

            // Add a columns to the ordertable
            // Additional Info
            OrderTable.Columns.Add("SiteName");
            OrderTable.Columns.Add("ReceiptText");
            OrderTable.Columns.Add("CustomerServiceEmail");
            OrderTable.Columns.Add("CustomerServicePhoneNumber");
            OrderTable.Columns.Add("FeedBack");
            OrderTable.Columns.Add("AdditionalInstructions");
            OrderTable.Columns.Add("AdditionalInstructLabel");

            // Payment Info
            OrderTable.Columns.Add("CardTransactionID");
            OrderTable.Columns.Add("CardTransactionLabel");
            OrderTable.Columns.Add("PaymentName");
            OrderTable.Columns.Add("ShippingName");
            OrderTable.Columns.Add("PONumber");
            OrderTable.Columns.Add("PurchaseNumberLabel");

            // Customer Info
            OrderTable.Columns.Add("OrderId");
            //OrderTable.Columns.Add("OrderDate");
            OrderTable.Columns.Add("AccountId");
            OrderTable.Columns.Add("BillingAddress");
            OrderTable.Columns.Add("ShippingAddress");
            OrderTable.Columns.Add("PromotionCode");
            OrderTable.Columns.Add("TotalCost");
            OrderTable.Columns.Add("StyleSheetPath");

            //Spacer Image
            OrderTable.Columns.Add("SpacerImage");


            // Create Order Amount Table
            DataTable OrderAmountTable = new DataTable();
            OrderAmountTable.Columns.Add("Title");
            OrderAmountTable.Columns.Add("Amount");

            // Create a NewRow.
            DataRow orderDBRow = OrderTable.NewRow();

            if (includeReceiptStyleSheet)
            {
                try
                {
                    string cssFilePath = string.Empty;

                    // If request comes from mobile then load the theme from ZNodePortal.MobileTheme
                    if (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
                    {
                        cssFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Themes/" + ZNodeConfigManager.SiteConfig.MobileTheme + "/Receipt/receipt.css");
                    }
                    else
                    {
                        cssFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Receipt/receipt.css");
                    }

                    // Create an instance of StreamReader to read from a file.
                    // The using statement also closes the StreamReader.
                    using (StreamReader streamReader = new StreamReader(cssFilePath))
                    {
                        orderDBRow["StyleSheetPath"] = streamReader.ReadToEnd();
                    }
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Email Receipt Stylesheet issue." + ex.Message);
                }
            }

            //Spacer Image
            spacerImagePath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
            if (!string.IsNullOrEmpty(spacerImagePath))
            {
                orderDBRow["SpacerImage"] = spacerImagePath;
            }
            else
            {
                orderDBRow["SpacerImage"] = spacerImagePath;
            }

            // Additional Info
            orderDBRow["SiteName"] = this._Portal.GetByPortalID(this._Order.PortalId) == null ? String.Empty : this._Portal.GetByPortalID(this._Order.PortalId).StoreName;
            orderDBRow["ReceiptText"] = ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptConfirmationIntroText", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID);
            orderDBRow["CustomerServiceEmail"] = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            orderDBRow["CustomerServicePhoneNumber"] = ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
            orderDBRow["FeedBack"] = this.FeedbackUrl;

            // Payment Info
            if (!String.IsNullOrEmpty(this._Order.CardTransactionID))
            {
                orderDBRow["CardTransactionID"] = this._Order.CardTransactionID + this._Order.MultiTendetTransactionIDList;
                orderDBRow["CardTransactionLabel"] = "TransactionID:";
            }

            orderDBRow["PaymentName"] = this._ShoppingCart.Payment.PaymentName;
            orderDBRow["ShippingName"] = this._ShoppingCart.Shipping.ShippingName;

            if (!String.IsNullOrEmpty(this._Order.PurchaseOrderNumber))
            {
                orderDBRow["PONumber"] = this._Order.PurchaseOrderNumber;
                orderDBRow["PurchaseNumberLabel"] = "Purchase Order Number:";
            }

            // Customer Info
            orderDBRow["OrderId"] = this._Order.OrderID.ToString();
            //orderDBRow["OrderDate"] = this._Order.OrderDate.Date.ToString("MM/dd/yyyy") ;
            orderDBRow["AccountId"] = this._Order.AccountID.ToString();

            orderDBRow["BillingAddress"] = GetAddressString(this._Order.BillingAddress);//Zeon Custom Code
            orderDBRow["ShippingAddress"] = GetAddressString(this._Order.ShippingAddress);
            orderDBRow["PromotionCode"] = this._Order.CouponCode;

            // Create OrderlineItem Table
            DataTable OrderlineItemTable = new DataTable();

            //Zeon Custom Code change done on date :9/18/2014 : Merge SKU and Name in same row;
            //OrderlineItemTable.Columns.Add("SKU");

            OrderlineItemTable.Columns.Add("Image");
            OrderlineItemTable.Columns.Add("Quantity");
            OrderlineItemTable.Columns.Add("Description");
            OrderlineItemTable.Columns.Add("Price");
            OrderlineItemTable.Columns.Add("ExtendedPrice");
            //Changes done on date : 18/Sep/2014 : Purpose : Add new row to show the image
            OrderlineItemTable.Columns.Add("Name");


            if (supplier == null)
            {
                // Amount Info
                this.BuildOrderAmountInfo(HttpContext.GetGlobalResourceObject("CommonCaption", "SubTotal") == null ? "SubTotal" : HttpContext.GetGlobalResourceObject("CommonCaption", "SubTotal").ToString(), this._Order.SubTotal, OrderAmountTable);
                this.BuildOrderAmountInfo(HttpContext.GetGlobalResourceObject("CommonCaption", "TaxCost") == null ? "TaxCost" : HttpContext.GetGlobalResourceObject("CommonCaption", "TaxCost").ToString(), this._Order.TaxCost, OrderAmountTable);
                this.BuildOrderAmountInfo(HttpContext.GetGlobalResourceObject("CommonCaption", "SalesTax") == null ? "SalesTax" : HttpContext.GetGlobalResourceObject("CommonCaption", "SalesTax").ToString(), this._Order.SalesTax, OrderAmountTable);
                this.BuildOrderAmountInfo(HttpContext.GetGlobalResourceObject("CommonCaption", "VAT") == null ? "VAT" : HttpContext.GetGlobalResourceObject("CommonCaption", "VAT").ToString(), this._Order.VAT, OrderAmountTable);
                this.BuildOrderAmountInfo(HttpContext.GetGlobalResourceObject("CommonCaption", "HST") == null ? "HST" : HttpContext.GetGlobalResourceObject("CommonCaption", "HST").ToString(), this._Order.HST, OrderAmountTable);
                this.BuildOrderAmountInfo(HttpContext.GetGlobalResourceObject("CommonCaption", "PST") == null ? "PST" : HttpContext.GetGlobalResourceObject("CommonCaption", "PST").ToString(), this._Order.PST, OrderAmountTable);
                this.BuildOrderAmountInfo(HttpContext.GetGlobalResourceObject("CommonCaption", "GST") == null ? "GST" : HttpContext.GetGlobalResourceObject("CommonCaption", "GST").ToString(), this._Order.GST, OrderAmountTable);
                this.BuildOrderAmountInfo(HttpContext.GetGlobalResourceObject("CommonCaption", "DiscountAmount") == null ? "DiscountAmount" : HttpContext.GetGlobalResourceObject("CommonCaption", "DiscountAmount").ToString(), -this._Order.DiscountAmount, OrderAmountTable);
                //Zeon Customization :: Start
                //this.BuildOrderAmountInfo(string.Format(HttpContext.GetGlobalResourceObject("CommonCaption", "ShippingCost") == null ? "ShippingCost" : HttpContext.GetGlobalResourceObject("CommonCaption", "ShippingCost").ToString(), this._ShoppingCart.Shipping.ShippingName), this._Order.ShippingCost, OrderAmountTable);

                //Show Ground Shipping - Free  in Order Recipt.
                this.BuildShippingOrderAmountInfo(string.Format(HttpContext.GetGlobalResourceObject("CommonCaption", "ShippingCost") == null ? "ShippingCost" : HttpContext.GetGlobalResourceObject("CommonCaption", "ShippingCost").ToString(), this._ShoppingCart.Shipping.ShippingName), this._Order.ShippingCost, OrderAmountTable, this._ShoppingCart.Shipping.ShippingName);
                //Zeon Customization :: End

                if (HttpContext.GetGlobalResourceObject("CommonCaption", "GiftCardAmount") == null)
                {
                    this.BuildOrderAmountInfo("GiftCardAmount", 0, OrderAmountTable);
                }
                else
                {
                    this.BuildOrderAmountInfo(HttpContext.GetGlobalResourceObject("CommonCaption", "GiftCardAmount").ToString(), -this._Order.GiftCardAmount, OrderAmountTable);
                }
                orderDBRow["TotalCost"] = this._Order.Total.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();

                if (this._Order.AdditionalInstructions != null)
                {
                    orderDBRow["AdditionalInstructions"] = this._Order.AdditionalInstructions;
                    orderDBRow["AdditionalInstructLabel"] = "Additional Instructions";
                }

                this.BuildOrderLineItem(OrderlineItemTable);
            }
            else
            {
                decimal orderTotal = 0;

                this.BuildOrderLineItem(OrderlineItemTable, supplier, out orderTotal);

                // Amount Info
                this.BuildOrderAmountInfo(HttpContext.GetGlobalResourceObject("CommonCaption", "SubTotal") == null ? "SubTotal" : HttpContext.GetGlobalResourceObject("CommonCaption", "SubTotal").ToString(), orderTotal, OrderAmountTable);
                orderDBRow["TotalCost"] = orderTotal.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();

                if (this._Order.AdditionalInstructions != null)
                {
                    orderDBRow["AdditionalInstructions"] = string.Empty;
                    orderDBRow["AdditionalInstructLabel"] = "Additional Instructions";
                }
            }

            // Add rows to Order datatable.
            OrderTable.Rows.Add(orderDBRow);

            // Html Parser
            // Parse the template
            ZNodeHtmlTemplate template = new ZNodeHtmlTemplate();

            // set the html template path
            template.Path = htmlTemplatePath;

            // Parse Order
            template.Parse(OrderTable.CreateDataReader());

            // Parse OrderLineItem
            template.Parse("LineItems", OrderlineItemTable.CreateDataReader());

            // Parse OrderLineItem
            template.Parse("AmountLineItems", OrderAmountTable.CreateDataReader());

            // template html output
            return template.Output;
        }

        /// <summary>
        /// Build the order amount information
        /// </summary>
        /// <param name="title">Title of the order</param>
        /// <param name="amount">Amount of the order</param>
        /// <param name="table">data table </param>
        private void BuildOrderAmountInfo(string title, decimal amount, DataTable table)
        {
            // Amount Info
            if (amount != 0)
            {
                DataRow orderDBRow = table.NewRow();
                orderDBRow["Title"] = title;
                orderDBRow["Amount"] = amount.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();
                table.Rows.Add(orderDBRow);
            }

        }

        /// <summary>
        /// OrderLineItem DataTable
        /// </summary>
        /// <param name="OrderlineItemTable">Order line item</param>
        private void BuildOrderLineItem(DataTable OrderlineItemTable)
        {
            foreach (ZNodeShoppingCartItem shoppingCartItem in this._ShoppingCart.ShoppingCartItems)
            {
                ZNodeGenericCollection<ZNodeDigitalAsset> assetCollection = shoppingCartItem.Product.ZNodeDigitalAssetCollection;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(shoppingCartItem.Product.Name + "<br />");
                //Zeon Custom Code:Download link is not needed
                //if (!string.IsNullOrEmpty(shoppingCartItem.Product.DownloadLink.Trim()))
                //{
                //    sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>Download</a><br />");
                //}

                if (assetCollection.Count > 0)
                {
                    foreach (ZNodeDigitalAsset digitalAsset in assetCollection)
                    {
                        sb.Append(digitalAsset.DigitalAsset + "<br />");
                    }
                }
                //Zeon  Custom Code:Starts
                string notes = string.Empty;
                if (!string.IsNullOrEmpty(shoppingCartItem.Notes)) { notes = "Name: " + shoppingCartItem.Notes; }
                //Zeon Custom Code:Ends

                DataRow orderlineItemDBRow = OrderlineItemTable.NewRow();

                //Changes done on date : 18/Sep/2014 : Purpose : Add new row to show the image
                string imagePath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + shoppingCartItem.Product.ThumbnailImageFilePath.Replace("~/", string.Empty);
                orderlineItemDBRow["Image"] = "<img alt='" + shoppingCartItem.Product.ImageAltTag + "' title='" + shoppingCartItem.Product.ImageAltTag + "' src='" + imagePath + "'/>";

                //Zeon Custom Code change done on date :9/18/2014 : Merge SKU and Name in same row;
                //orderlineItemDBRow["Name"] = sb.ToString();
                //orderlineItemDBRow["SKU"] = shoppingCartItem.Product.SKU; 

                orderlineItemDBRow["Name"] = sb.ToString() + "<span><strong>Item #</strong>" + shoppingCartItem.Product.SKU + "</span>";

                //orderlineItemDBRow["Description"] = shoppingCartItem.Product.ShoppingCartDescription;//Znode Old Code

                orderlineItemDBRow["Description"] = shoppingCartItem.Product.ShoppingCartDescription + notes;//Zeon Custom Code For showing notes
                orderlineItemDBRow["Quantity"] = shoppingCartItem.Quantity.ToString();
                orderlineItemDBRow["Price"] = shoppingCartItem.UnitPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();
                orderlineItemDBRow["ExtendedPrice"] = shoppingCartItem.ExtendedPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();

                OrderlineItemTable.Rows.Add(orderlineItemDBRow);
            }
        }

        /// <summary>
        /// OrderLineItem DataTable
        /// </summary>
        /// <param name="OrderlineItemTable">Order line item</param>
        /// <param name="supplier">Supplier details</param>
        /// <param name="orderTotal">Order total</param>
        private void BuildOrderLineItem(DataTable OrderlineItemTable, ZNodeSupplierProperty supplier, out decimal orderTotal)
        {
            orderTotal = 0;

            if (supplier != null && supplier.AssociatedItems != null)
            {
                // loop through each coupon code list
                foreach (ZNodeProductSupplier supplierItem in supplier.AssociatedItems)
                {
                    orderTotal += supplierItem.ExtendedPrice;

                    // Add Supplier Line Item
                    DataRow orderlineItemDBRow = OrderlineItemTable.NewRow();

                    //Changes done on date : 18/Sep/2014 : Purpose : Add new row to show the image
                    string imagePath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + supplierItem.ImagePath.Replace("~/", string.Empty);
                    orderlineItemDBRow["Image"] = "<img alt='" + supplierItem.ImageAltTag + "' src='" + imagePath + "'/>";

                    //Zeon Custom Code change done on date :9/18/2014 : Merge SKU and Name in same row;
                    //orderlineItemDBRow["Name"] = supplierItem.Name;
                    //orderlineItemDBRow["SKU"] = supplierItem.SKU;

                    orderlineItemDBRow["Name"] = supplierItem.Name + "<span><strong>Item #</strong>" + supplierItem.SKU + "</span>";
                    orderlineItemDBRow["Description"] = supplierItem.Description;
                    orderlineItemDBRow["Quantity"] = supplierItem.Quantity.ToString();
                    orderlineItemDBRow["Price"] = supplierItem.UnitPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();
                    orderlineItemDBRow["ExtendedPrice"] = supplierItem.ExtendedPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();

                    OrderlineItemTable.Rows.Add(orderlineItemDBRow);
                }
            }
        }

        /// <summary>
        /// Creates an XML Element
        /// </summary>
        /// <param name="xmlDoc">XML Document</param>
        /// <param name="elementName">Element Name</param>
        /// <param name="elementValue">Element Value</param>
        /// <returns>Returns the element</returns>
        private XmlElement GetElement(XmlDocument xmlDoc, string elementName, string elementValue)
        {
            XmlElement elmt = xmlDoc.CreateElement(elementName);

            if (elementValue.Length > 0)
            {
                elmt.InnerText = elementValue;
            }

            return elmt;
        }
        #endregion
    }
}

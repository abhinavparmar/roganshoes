using System;
using System.Text.RegularExpressions;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Suppliers
{
    /// <summary>
    /// Provides the ability to email a supplier an order.
    /// </summary>
	public class ZNodeSupplierTypeEmail : ZNodeBusinessBase, IZNodeSupplierType
    {
        /// <summary>
        /// Email the order to this supplier.
        /// </summary>
        /// <param name="Order">Order details</param>
        /// <param name="ShoppingCart">Shopping cart</param>
        /// <param name="Supplier">Supplier details</param>
        /// <returns>Returns the order status</returns>
        public bool SubmitOrder(ZNodeOrderFulfillment Order, ZNodeShoppingCart ShoppingCart, ZNodeSupplierProperty Supplier)
        {
            bool orderStatus = false;
            string salesEmail = ZNodeConfigManager.SiteConfig.SalesEmail;
            string subject = ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptEmailSubject");
            string messageText = string.Empty; 

            try
            {
                // Cycle through the cart and build an order for the supplier.
                if (Supplier.EnableEmailNotification)
                {
                    ZNodeReceipt _EmailReceipt = new ZNodeReceipt(Order);

                    messageText = _EmailReceipt.GenerateEmailReceipt(Supplier);
                    messageText = ReplaceTheOrderReceiptFooter(messageText); // Zeon Custom code

                    string recepientEmail = Supplier.NotificationEmailID;

                    ZNodeEmail.SendEmail(recepientEmail, salesEmail, salesEmail, subject, messageText, true);

                    orderStatus = true;
                }
            }
            catch (Exception ex)
            {
                   // log exception
                if (ex.InnerException != null)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.InnerException.ToString());
                }
                else
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                }

                // do not rethrow as this is non-critical
                ZNodeEmail.SendEmail(salesEmail, salesEmail, string.Empty, subject, messageText, true);
            }

            return orderStatus;
        }

        #region Zeon Custom Code
        /// <summary>
        /// To replace the footer content
        /// </summary>
        private string ReplaceTheOrderReceiptFooter(string messageText)
        {
            Regex rxFooterContent = new Regex("{FooterText}", RegexOptions.IgnoreCase);
            Regex rxHeaderLogoContent = new Regex("{EmailReceiptLogo}", RegexOptions.IgnoreCase);
            Regex rxHeaderConfirmationContent = new Regex("{OrderConfirmationText}", RegexOptions.IgnoreCase);
            string logoPath = GetLogoHTML();
            messageText = rxFooterContent.Replace(messageText, ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptFooter", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID));
            messageText = rxHeaderLogoContent.Replace(messageText, logoPath);
            messageText = rxHeaderConfirmationContent.Replace(messageText, ZNodeCatalogManager.MessageConfig.GetMessage("OrderEmailConfirmationMessage", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID));
            return messageText;
        }

        /// <summary>
        /// Create HTML for LOgo on Email 
        /// </summary>
        /// <returns>string</returns>
        private string GetLogoHTML()
        {
            string logoHTML = string.Empty;
            string path = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            ZNode.Libraries.ECommerce.Utilities.ZNodeImage zImage = new Utilities.ZNodeImage();
            string format = "<a title='{0}' href='{1}'><img src={2}></a>";
            logoHTML = string.Format(format, ZNodeConfigManager.SiteConfig.StoreName, path, path + zImage.GetImageBySize(ZNodeConfigManager.SiteConfig.MaxCatalogItemSmallWidth, System.IO.Path.GetFileName(ZNodeConfigManager.SiteConfig.LogoPath)).Replace("~", string.Empty));
            return logoHTML;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZNode.Libraries.Search.Lucene.UnitTests.Config;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;
using ZNode.Libraries.Search.LuceneSearchProvider.Search.Constants;
using ZNode.Libraries.Search.Lucene.UnitTests.Config;
using Directory = System.IO.Directory;
using ZLSLS = ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace ZNode.Libraries.Search.Lucene.UnitTests
{
    [TestClass]
    public class SearchCreateIndexTest
    {
        private readonly string ZnodeIndexDirectory = System.Configuration.ConfigurationSettings.AppSettings[LuceneLibraryConstants.indexDirectory];

        [TestMethod]
        public void CreateIndex()
        {
            LuceneSearchIndexer indexer = new LuceneSearchIndexer(new LuceneProductService(),
                                                                  new LuceneDocMappingService(),
                                                                  new LuceneCategoryService(), System.Configuration.ConfigurationSettings.AppSettings[LuceneLibraryConstants.indexDirectory], 1000);

            var CreateIndexWriter = ZLSLS.LuceneSearchManager.GetIndexWriter(ZnodeIndexDirectory, true);

            indexer.CreateIndex(CreateIndexWriter);

            CreateIndexWriter.Optimize();
            CreateIndexWriter.Dispose();
         
        }

        [TestMethod]
        public void GetAllProductsToXML()
        {
            LuceneProductService productService = new LuceneProductService();

            int start = 0;
            int pageLength = 1000;
            int totalCount = 0;
      
            do
            {
                var products = productService.GetAllProducts(start, pageLength, out totalCount);
                TestUtility.CreateXmlFile(products,"../../Config/Products.xml");
                start++;
                Assert.IsTrue(products != null && products.Count > 0);
            } while (start * pageLength < totalCount);

             start = 0;
            pageLength = 1000;
            totalCount = 0;
            do
            {
                LuceneCategoryService categoryService = new LuceneCategoryService();

                var categoryFacets = categoryService.GetAllCategoryFacets(start, pageLength, out totalCount);
                TestUtility.CreateXmlFile(categoryFacets, "../../Config/Categories.xml");

                start++;
            } while (start * pageLength < totalCount); 

           
        }


        [TestMethod]
        public void CreatIndexFromXmlTest()
        {
            LuceneSearchIndexer indexer = new LuceneSearchIndexer(new LuceneProductService(),
                                                                   new LuceneDocMappingService(),
                                                                   new LuceneCategoryService(), System.Configuration.ConfigurationSettings.AppSettings[LuceneLibraryConstants.indexDirectory], 1000);

            var productList = (List<LuceneProduct>)TestUtility.CreateObjectFromXmlFile<List<LuceneProduct>>(@"Config/Products.xml");
            var categories = TestUtility.CreateObjectFromXmlFile<List<LuceneCategoryFacet>>(@"Config/Categories.xml");
            var indexWriter = LuceneSearchManager.GetIndexWriter(System.Configuration.ConfigurationSettings.AppSettings[LuceneLibraryConstants.indexDirectory], true);
            indexer.AddProductDocuments(productList, indexWriter);
           

            indexer.AddCategoryFacetDocuments(categories,indexWriter);
            
            indexWriter.Dispose();
          
        }

       
    }
}

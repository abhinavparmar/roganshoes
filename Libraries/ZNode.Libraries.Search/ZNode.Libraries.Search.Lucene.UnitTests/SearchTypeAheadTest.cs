﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.UI.WebControls;
using ZNode.Libraries.Search.Interfaces;
using ZLSLS = ZNode.Libraries.Search.LuceneSearchProvider.Search;
using ZNode.Libraries.DataAccess.Custom;
namespace ZNode.Libraries.Search.Lucene.UnitTests
{
    [TestClass]
    public class SearchTypeAheadTest
    {
        private int portalCatalogID = 3;
        
        // with category and partial product
        [TestMethod]
        public void DefaultTypeAheadTest()
        {
            var searchprovider = new ZLSLS.LuceneSearchProvider();
            var result = searchprovider.SuggestTermsFor("app", "Fruit", portalCatalogID);
            
            Assert.AreEqual(true, result.Any(x=>x.Name=="Apple"));
        }

        // with only partial product
        [TestMethod]
        public void TypeAheadWithNoCategoryTest()
        {
            IZnodeSearchProvider searchprovider;
            searchprovider = new ZLSLS.LuceneSearchProvider();
            var result = searchprovider.SuggestTermsFor("roa", "", portalCatalogID);
            Assert.AreEqual(true, result.Count == 3);
            Assert.AreEqual(true ,result.Any(x=>x.Category.Equals("Nuts")));
        }

        // with Full product Name Search
        [TestMethod]
        public void TypeAheadWithCategoryTest()
        {
            IZnodeSearchProvider searchprovider;
            searchprovider = new ZLSLS.LuceneSearchProvider();
            var result = searchprovider.SuggestTermsFor("roa", "Nuts", portalCatalogID);
            Assert.AreEqual(true, result.Count == 3);
            Assert.AreEqual(true, result.Any(x => x.Category.Equals(string.Empty)));
        }

        // Ruturns multiple Product of different category with same name
        [TestMethod]
        public void TextSuggestionsMultiple()
        {
            IZnodeSearchProvider searchprovider;
            searchprovider = new ZLSLS.LuceneSearchProvider();
            var result = searchprovider.SuggestTermsFor("rose", "", portalCatalogID);
            Assert.AreEqual(true, result.Count > 0);
        }
        
    }
}

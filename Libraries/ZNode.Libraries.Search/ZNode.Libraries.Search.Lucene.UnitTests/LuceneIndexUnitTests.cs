﻿using System.Collections.Generic;
using System.Configuration;
using Lucene.Net.Index;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services;
using ZNode.Libraries.Search.LuceneSearchProvider.Search.Constants;
using ZLSLS = ZNode.Libraries.Search.LuceneSearchProvider.Search;

//using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace ZNode.Libraries.Search.Lucene.UnitTests
{
    [TestClass]
    public class LuceneIndexUnitTests
    {
        private readonly string ZnodeIndexDirectory =
            ConfigurationSettings.AppSettings[LuceneLibraryConstants.indexDirectory];


        [TestMethod]
        public void CreateIndex()
        {
            var indexer = new LuceneSearchIndexer(new LuceneProductService(),
                                                  new LuceneDocMappingService(),
                                                  new LuceneCategoryService(),
                                                  ConfigurationSettings.AppSettings[
                                                      LuceneLibraryConstants.indexDirectory], 1000);

            IndexWriter CreateIndexWriter = ZLSLS.LuceneSearchManager.GetIndexWriter(ZnodeIndexDirectory, true);

            indexer.CreateIndex(CreateIndexWriter);
        }

        [TestMethod]
        public void GetAllProducts()
        {
            var productService = new LuceneProductService();

            int start = 0;
            int pageLength = 1000;
            int totalCount = 0;

            do
            {
                List<LuceneProduct> products = productService.GetAllProducts(start, pageLength, out totalCount);
                start++;
                Assert.IsTrue(products != null && products.Count > 0);
            } while (start*pageLength < totalCount);
        }
    }
}

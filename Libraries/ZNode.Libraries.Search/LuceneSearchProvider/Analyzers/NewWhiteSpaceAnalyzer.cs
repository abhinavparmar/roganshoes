﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Analyzers
{
    public sealed class NewWhitespaceAnalyzer : Analyzer
    {
        public override TokenStream TokenStream(String fieldName, System.IO.TextReader reader)
        {
            StandardTokenizer baseTokenizer = new StandardTokenizer(Lucene.Net.Util.Version.LUCENE_29, reader);
            StandardFilter standardFilter = new StandardFilter(baseTokenizer);
            LowerCaseFilter lcFilter = new LowerCaseFilter(standardFilter);
            return lcFilter;
        }


    }
}

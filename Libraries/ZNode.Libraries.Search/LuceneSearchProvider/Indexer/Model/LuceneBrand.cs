﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneBrand
    {
        public int? BrandId { get; set; }
        public double Boost { get; set; }
        public string BrandName { get; set; }
    }
}

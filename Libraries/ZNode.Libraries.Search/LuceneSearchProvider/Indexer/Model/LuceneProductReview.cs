﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneProductReview
    {
        public int ReviewId { get; set; }
        public double ReviewRating { get; set; }
    }
}

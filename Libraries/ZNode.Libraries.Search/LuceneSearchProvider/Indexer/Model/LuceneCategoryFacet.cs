﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneCategoryFacet
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int PortalCatalogID { get; set; }
        public List<string> FacetNames { get; set; }
        public List<LuceneCategory> ParentCategory { get; set; }
        public List<LuceneCategory> SubCategory { get; set; }
    }
}

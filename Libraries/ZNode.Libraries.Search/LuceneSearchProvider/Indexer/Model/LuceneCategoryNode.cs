﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneCategoryNode
    {
        public int CategoryNodeId { get; set; }
        public int? ParentCategoryNodeId { get; set; }
        public LuceneCatalog Catalog { get; set; }
        public LuceneCategory Category { get; set; }
        public LuceneCategoryNode ParentCategory { get; set; }
    }
}

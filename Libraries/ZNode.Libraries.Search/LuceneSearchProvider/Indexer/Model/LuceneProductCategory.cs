﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneProductCategory
    {
        public int ProductCategoryId { get; set; }
        public LuceneCategory Category { get; set;}
        public bool ActiveInd { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneProductFacet
    {
        public string FacetName { get; set; }
        public string FacetValue { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LucenePortalCatalog
    {
        public int PortalCatalogId { get; set; }
        public LucenePortal Portal { get; set; }
    }
}

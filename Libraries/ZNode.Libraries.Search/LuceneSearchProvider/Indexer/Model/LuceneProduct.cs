﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MS.Internal.Xml.XPath;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneProduct
    {
        public int ProductId { get; set; }
        public double Boost { get; set; }
        public LuceneBrand Brand { get; set; }
        public string Description { get; set; }
        public List<LuceneProductFacet> Facets { get; set; } 
        public string Features { get; set; }
        public string Name { get; set; }
        public List<LuceneProductCategory> ProductCategories { get; set; }
        public List<LuceneCategory> ProductSubCategories { get; set; }
        public string ProductNum { get; set; }
        public List<LuceneProductReview> Reviews { get; set; }
        public string ShortDescription { get; set; }
        public List<LuceneSKU> SKUs { get; set; }
        public string Specifications { get; set; }
        public LuceneSupplier Supplier { get; set; }
        public string ProductName { get; set; }
        public string IndexOperationType { get; set; }
        public int? ProductDisplayOrder { get; set; } //zeon
        public List<LuceneCategory> CategoryHierarchy
        {
            get
            {
                List<LuceneCategory> hierarchy = new List<LuceneCategory>();
                foreach (var category in ProductCategories)
                {
                    if (category.Category != null && category.Category.Hierarchy != null)
                        hierarchy.AddRange(category.Category.Hierarchy.ToArray());
                }

                return hierarchy;
            }
        }
        public bool IsActive { set; get; }
        public int? ReviewStateID { get; set; }

        public LuceneProduct()
        {
            Boost = 1.0;
            IsActive = true;
            Reviews = new List<LuceneProductReview>();
            ProductCategories = new List<LuceneProductCategory>();
            SKUs = new List<LuceneSKU>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneCatalog
    {
        public int CatalogId { get; set; }
        public double Boost { get; set; }
        public string CatalogName { get; set; }
        public List<LucenePortalCatalog> PortalCatalogs { get; set; }
    }
}

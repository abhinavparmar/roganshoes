﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Mapping
{
    public static class NetTiersToLuceneMapper
    {
        public static LuceneBrand LuceneBrand(Manufacturer ntManufacturer)
        {
            return new LuceneBrand
                {
                    BrandId = ntManufacturer.ManufacturerID,
                    BrandName = ntManufacturer.Name
                };
        }

        public static LuceneCatalog LuceneCatalog(Catalog ntCatalog)
        {
            if (ntCatalog == null || !ntCatalog.IsActive) return null;

            var portalCatalogs = ntCatalog.PortalCatalogCollection.Select(ntPortalCatalog => LucenePortalCatalog(ntPortalCatalog)).ToList();

            return new LuceneCatalog
                {
                    CatalogId = ntCatalog.CatalogID,
                    CatalogName = ntCatalog.Name,
                    PortalCatalogs = portalCatalogs
                };
        }

        public static LuceneCategory LuceneCategory(Category ntCategory)
        {
            if (ntCategory == null || ntCategory.VisibleInd == false) return null;

            var categoryNodes = ntCategory.CategoryNodeCollection.Select(ntCategoryNode => LuceneCategoryNode(ntCategoryNode)).Where(x => x != null).ToList();

            return new LuceneCategory
                {
                    CategoryId = ntCategory.CategoryID,
                    CategoryName = System.Web.HttpUtility.HtmlDecode(ntCategory.Name),
                    Title = System.Web.HttpUtility.HtmlDecode(ntCategory.Title),
                    CategoryNodes = categoryNodes
                };
        }

        public static LuceneCategoryFacet LuceneCategoryFacet(Category ntCategory, int portalCatalogID)
        {
            //Issue findinfg code
            //try
            //{
            var category = LuceneCategory(ntCategory);
            return new LuceneCategoryFacet
                {
                    CategoryId = ntCategory.CategoryID,
                    PortalCatalogID = portalCatalogID,
                    CategoryName = System.Web.HttpUtility.HtmlDecode(ntCategory.Name),
                    ParentCategory = category.Hierarchy,
                    SubCategory = ntCategory.CategoryNodeCollection.SelectMany(x => x.CategoryNodeCollection).Select(x => LuceneCategory(x.CategoryIDSource)).Where(x => x != null).ToList()
                };
            //}
            //catch (Exception ex)
            //{
            //    string logFilePath = @"C:/LuceneError.txt";
            //    using (System.IO.StreamWriter sw = System.IO.File.AppendText(logFilePath))
            //    {
            //        sw.WriteLine("2==================================================");
            //        sw.WriteLine("Method name: LuceneCategoryFacet");
            //        sw.WriteLine("CategoryID=" + ntCategory.CategoryID);
            //        sw.WriteLine("CategoryNodeCollection Count=" + ntCategory.CategoryNodeCollection.Count);                   
            //        sw.WriteLine("portalCatalogID=" + portalCatalogID);
            //        sw.WriteLine(ex.ToString());
            //        sw.WriteLine(ex.StackTrace);
            //        sw.WriteLine("==================================================");
            //    }
            //}
            //return null;
        }

        public static LuceneCategoryNode LuceneCategoryNode(CategoryNode ntCategoryNode)
        {
            if (ntCategoryNode == null || ntCategoryNode.ActiveInd == false) return null;

            return new LuceneCategoryNode
                {
                    CategoryNodeId = ntCategoryNode.CategoryNodeID,
                    Catalog = LuceneCatalog(ntCategoryNode.CatalogIDSource),
                    ParentCategoryNodeId = ntCategoryNode.ParentCategoryNodeID,
                    Category = LuceneCategory(ntCategoryNode.CategoryIDSource),
                    ParentCategory = LuceneCategoryNode(ntCategoryNode.ParentCategoryNodeIDSource),
                };
        }

        public static LuceneDocMapping LuceneDocMapping(LuceneDocumentMapping ntLuceneDocumentMapping)
        {
            return new LuceneDocMapping
                {
                    SearchDocumentMappingId = ntLuceneDocumentMapping.LuceneDocumentMappingID,
                    Boost = ntLuceneDocumentMapping.Boost ?? 1.0,
                    DocumentName = ntLuceneDocumentMapping.DocumentName,
                    IsBoosted = ntLuceneDocumentMapping.IsBoosted,
                    IsFaceted = ntLuceneDocumentMapping.IsFaceted,
                    IsIndexed = ntLuceneDocumentMapping.IsIndexed,
                    IsStored = ntLuceneDocumentMapping.IsStored,
                    PropertyName = ntLuceneDocumentMapping.PropertyName
                };
        }

        public static LucenePortal LucenePortal(Portal ntPortal)
        {
            return new LucenePortal
                {
                    PortalId = ntPortal.PortalID,
                    PortalStoreName = ntPortal.StoreName
                };
        }

        public static LucenePortalCatalog LucenePortalCatalog(PortalCatalog ntPortalCatalog)
        {
            return new LucenePortalCatalog
                {
                    PortalCatalogId = ntPortalCatalog.PortalCatalogID,
                    Portal = LucenePortal(ntPortalCatalog.PortalIDSource)
                };
        }

        public static LuceneProduct LuceneProduct(Product ntProduct)
        {
            return new LuceneProduct
                {
                    ProductId = ntProduct.ProductID,
                    Brand = ntProduct.ManufacturerID.HasValue ? LuceneBrand(ntProduct.ManufacturerIDSource) : null,
                    Description = ntProduct.Description,
                    Features = ntProduct.FeaturesDesc,
                    Name = System.Web.HttpUtility.HtmlDecode(ntProduct.Name),
                    ProductNum = ntProduct.ProductNum,
                    ShortDescription = ntProduct.ShortDescription,
                    Specifications = ntProduct.Specifications,
                    Supplier = ntProduct.SupplierID.HasValue ? LuceneSupplier(ntProduct.SupplierIDSource) : null,
                    IsActive = ntProduct.ActiveInd,
                    ReviewStateID = ntProduct.ReviewStateID,
                    ProductDisplayOrder = ntProduct.DisplayOrder //zeon
                };
        }

        public static LuceneProductCategory LuceneProductCategory(ProductCategory ntProductCategory)
        {
            return new LuceneProductCategory
                {
                    ProductCategoryId = ntProductCategory.ProductCategoryID,
                    Category = LuceneCategory(ntProductCategory.CategoryIDSource),
                    ActiveInd = ntProductCategory.ActiveInd
                };
        }

        public static LuceneProductFacet LuceneProductFacet(TagProductSKU ntProductFacet)
        {
            //Zeon Custom Code:start
            string disp = string.Empty;
            if (ntProductFacet.TagIDSource.TagDisplayOrder != null)
            {
                disp = "♦" + ntProductFacet.TagIDSource.TagDisplayOrder.ToString(); //ALT+4
            }
            return new LuceneProductFacet
                {
                    FacetName = ntProductFacet.TagIDSource.TagGroupIDSource.TagGroupLabel,
                    FacetValue = ntProductFacet.TagIDSource.TagName + disp
                };
            //Zeon Custom Code:end
        }

        public static LuceneProductReview LuceneProductReview(Review ntReview)
        {
            return new LuceneProductReview
                {
                    ReviewId = ntReview.ReviewID,
                    ReviewRating = ntReview.Rating
                };
        }

        public static LuceneSKU LuceneSku(SKU ntSku)
        {
            return new LuceneSKU
                {
                    SKUId = ntSku.SKUID,
                    SKUName = ntSku.SKU
                };
        }

        public static LuceneSupplier LuceneSupplier(Supplier ntSupplier)
        {
            return new LuceneSupplier
                {
                    SupplierId = ntSupplier.SupplierID,
                    SupplierName = ntSupplier.Name
                };
        }
    }
}

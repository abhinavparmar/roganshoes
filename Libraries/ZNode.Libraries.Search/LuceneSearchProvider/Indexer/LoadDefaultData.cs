﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;
using ZNode.Libraries.Search.LuceneSearchProvider.Search.Constants;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer
{
    public class LoadDefaultData
    {

        private readonly string ZnodeIndexDirectory = System.Configuration.ConfigurationManager.AppSettings[LuceneLibraryConstants.indexDirectory];
        //private readonly string ZnodeIndexDirectoryForService = @"c:\_LuceneData";

        #region Public Methods
        /// <summary>
        /// Populate the default data 
        /// </summary>
        /// <returns></returns>
        public bool IndexingDefaultData()
        {
            try
            {
                LuceneSearchIndexer indexer = new LuceneSearchIndexer(new LuceneProductService(), new LuceneDocMappingService(), new LuceneCategoryService(), ZnodeIndexDirectory);
                var CreateIndexWriter = LuceneSearchManager.GetIndexWriter(ZnodeIndexDirectory, true);

                indexer.CreateIndex(CreateIndexWriter);
                CreateIndexWriter.Optimize();
                CreateIndexWriter.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool IndexingDefaultDataForService()
        {
            LuceneSearchIndexer indexer = new LuceneSearchIndexer(new LuceneProductService(), new LuceneDocMappingService(), new LuceneCategoryService(), ZnodeIndexDirectory);

           // indexer.CreateIndexForService();
            return true;
        }

        #endregion
    }
}

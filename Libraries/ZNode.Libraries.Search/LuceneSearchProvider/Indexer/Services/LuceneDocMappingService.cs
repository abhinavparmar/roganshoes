﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Mapping;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services
{
    public class LuceneDocMappingService : ILuceneDocMappingService
    {
        public List<LuceneDocMapping> GetDocumentMappings()
        {
            LuceneDocumentMappingService luceneDocumentMappingService = new LuceneDocumentMappingService();

            return luceneDocumentMappingService.GetAll().Select(ldm => NetTiersToLuceneMapper.LuceneDocMapping(ldm)).ToList();
        }
    }
}

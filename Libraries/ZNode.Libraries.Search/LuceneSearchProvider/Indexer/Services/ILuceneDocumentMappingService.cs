﻿using System.Collections.Generic;
using System.Linq;


using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services
{
    public interface ILuceneDocumentMappingService
    {
        List<LuceneDocMapping> GetDocumentMappings();
    }
}
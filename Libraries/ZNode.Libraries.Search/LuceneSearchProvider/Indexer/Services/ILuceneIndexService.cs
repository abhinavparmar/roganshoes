﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;


namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services
{
    public interface ILuceneIndexService
    {
        List<LuceneIndexMonitor> GetIndexChanges();

        List<LuceneIndexMonitor> GetWinServiceIndexChanges();

        int IsLuceneTriggersDisabled();

        void SwitchLuceneTrigger(int intFlag);

        void SwitchWindowsService(int intFlag);

        List<LuceneIndexServerStatus> GetIndexServerStatus(int luceneIndexMonitorID);
    }
}

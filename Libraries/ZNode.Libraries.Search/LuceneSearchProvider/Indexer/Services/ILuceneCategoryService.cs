﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services
{
    public interface ILuceneCategoryService
    {
        LuceneCategoryFacet GetCategoryFacet(int categoryId);

        List<LuceneCategoryFacet> GetAllCategoryFacets(int start, int pageLength, out int totalCount);

        List<string> GetCategoryFacetNames(int categoryId);
    }
}

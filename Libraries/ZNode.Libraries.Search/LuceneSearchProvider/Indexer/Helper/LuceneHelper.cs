﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lucene.Net.Documents;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Helper
{
    public static class LuceneHelper
    {
        public static Lucene.Net.Documents.Field.Index GetMappingIndex(bool isIndexed)
        {
            return (isIndexed) ? Field.Index.ANALYZED : Field.Index.NOT_ANALYZED;
        }


        public static Field.Store GetMappingStore(bool isStored)
        {
            return (isStored) ? Field.Store.YES : Field.Store.NO;
        }
    }
}

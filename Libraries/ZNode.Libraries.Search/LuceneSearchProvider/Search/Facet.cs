﻿using System.Collections.Generic;

namespace ZNode.Libraries.Search
{
    public class Facet
    {
        public string AttributeName { set; get; }
        public List<FacetValue> AttributeValues { set; get; }        

        public Facet()
        {
            AttributeValues = new List<FacetValue>();
        }
    }

    public class FacetValue
    {
        public string AttributeValue { get; set; }
        public int FacetCount { set; get; }
        public List<string> ProductID { set; get; }
    }    
}

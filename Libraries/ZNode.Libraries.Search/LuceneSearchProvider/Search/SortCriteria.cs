﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Search
{
    public class SortCriteria
    {
        public enum SortNameEnum
        {
            Relevance =0,
            PopularItem,
            PriceLowToHigh,
            PriceHighToLow,
            ProductAtoZ,
            ProductZtoA,
            HighestRated
        }

        public enum SortDirectionEnum
        {
            ASC = 0,
            DESC = 1,
        }

        public SortNameEnum SortName { set; get; }
        public SortDirectionEnum SortDirection { set; get; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Search.Constants
{
    public class LuceneLibraryConstants
    {
        public const string indexDirectory = "LuceneIndexLocation";
        public static string[] defaultFields = new string[] { "Name", "Category", "SubCategory", "CategoryFacets", "CategoryID", "Tags", "ProductNum", "SKU", "Brand", "Supplier", "Description", "ShortDescription", "Features", "Specifications", "ReviewRating", "PortalCatalogID" };
        public static string[] facetDefaultFields = new string[] { "Name", "Category", "SubCategory", "CategoryFacets", "CategoryID", "PortalCatalogID" };
        public static string[] Category = new string[] { "Category", "SubCategory" };
        public const string ID = "ID";
    }
}

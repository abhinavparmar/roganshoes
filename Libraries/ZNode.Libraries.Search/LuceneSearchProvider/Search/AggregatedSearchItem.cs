﻿using System.Collections.Generic;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace ZNode.Libraries.Search
{
    public class AggregatedSearchItem
    {
        public List<SearchItems> SearchItems { get; set; }
        public string ProductNum { get; set; }
        public int MerchantID { get; set; }
        public int ProductType { get; set; }
        public double MinDistance { get; set; }
        public decimal LowPrice { get; set; }
        public decimal HighPrice { get; set; }

        public AggregatedSearchItem()
        {
            SearchItems = new List<SearchItems>();
            ProductNum = string.Empty;
            MerchantID = 0;
            ProductType = 0;
            MinDistance = 0;
            HighPrice = 0;
            LowPrice = 0;
        }
    }
}

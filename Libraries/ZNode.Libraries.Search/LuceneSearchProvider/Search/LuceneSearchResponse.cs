﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZNode.Libraries.Search.Interfaces;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;


namespace ZNode.Libraries.Search
{
    public class LuceneSearchResponse : IZNodeSearchResponse
    {
        public List<IZNodeSearchCategoryItem> CategoryItems { get; set; }
        public List<IZNodeSearchFacet> Facets { get; set; }
        public List<IZNodeSearchItem> Products { get; set; }
    }

    public class LuceneSearchCategoryItem : IZNodeSearchCategoryItem
    {
        public int CategoryID { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public int Count { get; set; }
        public List<IZNodeSearchCategoryItem> Hierarchy { get; set; }
    }

    public class LuceneSearchItem : IZNodeSearchItem
    {
        public int ProductID { get; set; }
        public int? DisplayOrder { get; set; } //zeon
    }

    public class LuceneSearchFacet : IZNodeSearchFacet
    {
        public string AttributeName { set; get; }
        public List<IZNodeSearchFacetValue> AttributeValues { set; get; }
        public int ControlTypeID { get; set; }
        public int FacetDisplayOrder { set; get; } //zeon 
    }

    public class LuceneSearchFacetValue : IZNodeSearchFacetValue
    {
        public string AttributeValue { get; set; }
       
        public long FacetCount { set; get; }
        public bool Selected { get; set; }
        //Zeon Custom Code :Starts
        public List<int> ProductIDList { set; get; }
        public int FacetValueDisplayOrder { set; get; }
        //Zeon Custom Code :Ends
    }
}
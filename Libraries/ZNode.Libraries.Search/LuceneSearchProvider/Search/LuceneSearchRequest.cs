﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.Search.Interfaces;


namespace ZNode.Libraries.Search.LuceneSearchProvider.Search
{
    public class LuceneSearchRequest : IZNodeSearchRequest
    {

       public string searchText { get; set; }
       public string category { get; set; }
       public string searchType { get; set; }
       public int PortalCatalogID { get; set; }
       public string FacetRefineBy { get; set; }
       public List<KeyValuePair<string, IEnumerable<string>>> Facets { get; set; } 
       public string ProductAtoZ { get; set; }
       public string ProductZtoA { get; set; }
       public string Relevance { get; set; }
       public int sortOrder { get; set; }
       public List<SortCriteria> SortCriteria { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using ZNode.Libraries.Search.Interfaces;
using ZNode.Libraries.Search.LuceneSearchProvider;
using ZNode.Libraries.Search.LuceneSearchProvider.Analyzers;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;
using Version = Lucene.Net.Util.Version;
using ZNode.Libraries.Search.LuceneSearchProvider.LuceneExtensionMethods;
using System.IO;
using Lucene.Net.Store;
using Directory = Lucene.Net.Store.Directory;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Search
{
    /// <summary>
    /// Class resonsible for doing text Search 
    /// </summary>
    public class LuceneSearchBase
    {
        protected Analyzer LuceneAnalyzer { set; get; }
        protected QueryParser QueryParser { get; set; }
        protected QueryParser PartialQueryParser { get; set; }

        public LuceneSearchBase()
        {
            LuceneAnalyzer = LuceneSearchManager.GetAnalyzer();
            QueryParser = new MultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_29, LuceneSearchManager.SearchFields, LuceneAnalyzer);
            QueryParser.DefaultOperator = QueryParser.AND_OPERATOR;
            PartialQueryParser = new MultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_29, LuceneSearchManager.PartialDefaultFields, LuceneAnalyzer);
            PartialQueryParser.DefaultOperator = QueryParser.AND_OPERATOR;
        }

        /// <summary>
        /// Constructor to use different type of Analyzer and parser
        /// </summary>
        /// <param name="analyzer"></param>
        /// <param name="parser"></param>
        public LuceneSearchBase(QueryParser parser)
        {
            QueryParser = parser;
            LuceneAnalyzer = parser.Analyzer;
        }


        /// <summary>
        /// Parameterized constructor
        /// </summary>
        /// <param name="analyzer">Analyzer</param>
        public LuceneSearchBase(Analyzer analyzer)
        {
            LuceneAnalyzer = analyzer;
        }

        /// <summary>
        /// Resonsible for doing Search with given query string 
        /// </summary>
        /// <returns></returns>
        public IZNodeSearchResponse SearchText(IZNodeSearchRequest request)
        {
            var luceneresponse = new LuceneSearchResponse();
            var indexSearcher = LuceneSearchManager.GetIndexSearcher;
            var indexReader = indexSearcher.IndexReader;
            var sortFields = LuceneUtility.GetSortFields(request.SortCriteria, request.sortOrder);
            var parserFields = LuceneSearchManager.SearchFields.ToList();

         
            // update query parser with facet fields
            if (request.Facets != null && request.Facets.Count > 0)
            {                
                parserFields.AddRange(request.Facets.Select(x => x.Key.ToString()));                             
            }

            QueryParser = new MultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_29, parserFields.ToArray(), LuceneAnalyzer);
            QueryParser.DefaultOperator = QueryParser.AND_OPERATOR;
            //Occur[] flags = { Occur.MUST, Occur.MUST, Occur.MUST, Occur.MUST, Occur.MUST, Occur.MUST };
            //Query q = MultiFieldQueryParser.Parse(Lucene.Net.Util.Version.LUCENE_29, request.searchText, parserFields.ToArray(), flags, LuceneAnalyzer);

            BooleanQuery searchQuery = GetQuery(request.searchText, request.category, request.PortalCatalogID);         
            UpdateFacetQuery(request, searchQuery);
            var hits = indexSearcher.Search(searchQuery, null, LuceneSearchManager.MaxSearchLimit,Sort.RELEVANCE).ScoreDocs;
            var mapResult = MapDocumentToDataList(hits, indexSearcher);
            luceneresponse.Products = mapResult.Where(x => x.ProductID > 0).ToList();         
            return luceneresponse;
        }

      

        private List<IZNodeSearchCategoryItem> GetCategoryHierarachy(ScoreDoc[] sDocs, IZNodeSearchRequest request, IndexSearcher indexSearcher)
        {
            List<IZNodeSearchCategoryItem> searchResults = new List<IZNodeSearchCategoryItem>();
            List<SearchResultDocument> hits = new List<SearchResultDocument>();
            List<ScoreDoc> listdocs = new List<ScoreDoc>();
            List<string> categories = new List<string>();

            foreach (ScoreDoc doc in sDocs)
            {
                SearchResultDocument resultDoc = new SearchResultDocument();
                resultDoc.Doc = indexSearcher.Doc(doc.Doc);
                hits.Add(resultDoc);
                categories.AddRange(resultDoc.Doc.GetValues("CategoryHierarchy"));
            }

            string parentCategoryName = string.Empty;
            categories.Where(x => x.Split('/')[0] == request.PortalCatalogID.ToString())
                .Distinct()
                .ToList()
                .ForEach(x =>
                {
                    parentCategoryName = string.Empty;                    
                    x.Split('/').Skip(2).Select((categoryName, idx) => new { categoryName, idx }).ToList().ForEach(y =>
                    {
                        var z = y.categoryName.Split('|');
                        var existing = searchResults.FirstOrDefaultFromMany(sr => sr.Hierarchy, sr => sr.Name == z[0]);
                        if (existing == null)
                        {
                            var path = x.Split('/').Take(y.idx + 3).ToArray();
                            path[1] = y.idx.ToString();
                            var category = new LuceneSearchCategoryItem()
                            {
                                Title = z[1],
                                Name = z[0],
                                Count =  GetCategoryCount(string.Join("/", path), request, indexSearcher), 
                                Hierarchy = new List<IZNodeSearchCategoryItem>()
                            };

                            var parent = searchResults.FirstOrDefaultFromMany(sr => sr.Hierarchy, sr => sr.Name == parentCategoryName);
                            if (parent != null)
                                parent.Hierarchy.Add(category);
                            else
                                searchResults.Add(category);
                        }

                        parentCategoryName = z[0];
                    });
                });

            return searchResults;
        }

        public int GetCategoryCount(string categoryHeirarchy, IZNodeSearchRequest request, IndexSearcher indexSearcher)
        {
            if (!string.IsNullOrEmpty(categoryHeirarchy))
            {
                var booleanQuery = new BooleanQuery();

                TermQuery term = new TermQuery(new Term("CategoryHierarchy", categoryHeirarchy));
                booleanQuery.Add(new BooleanClause(term, Occur.MUST));

                booleanQuery.Add(new BooleanClause(GetQuery(string.Empty, string.Empty, request.PortalCatalogID), Occur.MUST));

                var result = indexSearcher.Search(booleanQuery, LuceneSearchManager.MaxSearchLimit);

                int count = result.TotalHits;

                return count;
            }
            else
            {
                return 0;
            }
        }

        private SearchItems GetSearchItems(SearchResultDocument hit)
        {
            var objSearchItems = new SearchItems
            {
                ProductID = Convert.ToInt32(hit.Doc.Get("ProductID"))
            };

            return objSearchItems;
        }

        /// <summary>
        /// Creates Query for basic Search 
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="category"></param>
        /// <param name="PortalCatalogID"></param>
        /// <returns></returns>
        protected virtual BooleanQuery GetQuery(string searchText, string category, int PortalCatalogID)
        {
            var booleanQuery = new BooleanQuery();

            // Search Text Query 
            if (!String.IsNullOrEmpty(searchText))
            {
                // Normal Search
                var searchQuery = QueryParser.Parse(QueryParser.Escape(searchText));

                if (PartialQueryParser != null)
                {
                    // Partial Search
                    var orQuery = new BooleanQuery();
                    orQuery.Add(new BooleanClause(searchQuery, Occur.SHOULD));
                    var partialSearchQuery = PartialQueryParser.Parse(LuceneUtility.BuildPrefixQuery(QueryParser.Escape(searchText)));
                    PartialQueryParser.DefaultOperator = QueryParser.AND_OPERATOR;
                    orQuery.Add(new BooleanClause(partialSearchQuery, Occur.SHOULD));
                    booleanQuery.Add(new BooleanClause(orQuery, Occur.MUST));
                }
                else
                {
                    booleanQuery.Add(new BooleanClause(searchQuery, Occur.MUST));
                }
            }
        
            // Category Text Search 
            if (!string.IsNullOrEmpty(category))
            {
                booleanQuery.Add(GetCategoryClasue(category));
            }

            // Catalog Id 
            if (PortalCatalogID != 0)
            {
                booleanQuery.Add(GetMustClause("PortalCatalogID", PortalCatalogID.ToString()));
            }
            return booleanQuery;
        }


        protected BooleanClause GetCategoryClasue(string category)
        {
            var temp = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(category.ToLower());
            return GetMustClause("Category_" + temp.ToLuceneCategoryString(), category);
        }

        protected BooleanQuery GetTypeAheadQuery(string searchText, string category, int PortalCatalogID)
        {          
            var booleanQuery = new BooleanQuery();

            //Search Text Query 
            if (!String.IsNullOrEmpty(searchText))
            {
                //Normal Search
                var queryBuilder = new StringBuilder();
                queryBuilder.AppendFormat("({0}*)", searchText.Trim());

                var orQuery = new BooleanQuery();
                var searchQuery = QueryParser.Parse(queryBuilder.ToString());
                orQuery.Add(new BooleanClause(searchQuery, Occur.SHOULD));

                booleanQuery.Add(new BooleanClause(orQuery, Occur.MUST));
            }

            // Category Text Search 
            if (!string.IsNullOrEmpty(category))
            {
                booleanQuery.Add(GetMustClause("Category_" + category.ToLuceneCategoryString(), category));
            }

            // Catalog Id 
            if (PortalCatalogID != 0)
            {
                booleanQuery.Add(GetMustClause("PortalCatalogID", PortalCatalogID.ToString()));
            }

            return booleanQuery;

        }

        protected void UpdateFacetQuery(IZNodeSearchRequest request, BooleanQuery query)
        {
            if (request.Facets != null && request.Facets.Count > 0)
            {
                request.Facets
                       .ForEach(
                           x =>
                           {
                               // create OR query if multiple value are comming from same facetname.
                               BooleanQuery orQuery = new BooleanQuery();
                               x.Value.ToList().ForEach(y =>
                               {
                                   orQuery.Add(new TermQuery(new Term(x.Key, y)), Occur.MUST);
                               });
                               query.Add(new BooleanClause(orQuery, Occur.MUST));
                           });
            }

        }

        protected BooleanClause GetMustClause(string name, string value)
        {
            Query query2 = QueryParser.Parse(LuceneUtility.GetQuery(name, value));
            return new BooleanClause(query2, Occur.MUST);
        }

        protected TermQuery GetTerm(string name, string value)
        {
            Term term = new Term(name, value);
            return new TermQuery(term);
        }

        /// <summary>
        /// Map document to datalist with hits and searcher
        /// </summary>
        /// <param name="hits"></param>
        /// <param name="searcher"></param>
        /// <returns>Returns the Product list</returns>
        protected IEnumerable<IZNodeSearchItem> MapDocumentToDataList(IEnumerable<ScoreDoc> hits, IndexSearcher searcher)
        {          
            return hits.Select(hit => MapDocumentToData(searcher.Doc(hit.Doc))).ToList();
        }       

        /// <summary>
        /// Map Document To Data with document
        /// </summary>
        /// <param name="doc"></param>
        /// <returns>Returns the Search Product</returns>
        private IZNodeSearchItem MapDocumentToData(Document doc)
        {
            //zeon
            LuceneSearchItem items = new LuceneSearchItem();
            if (doc.Get("DisplayOrder") != null)
            {
                items.DisplayOrder = Convert.ToInt32(doc.Get("DisplayOrder"));
            }
            items.ProductID = Convert.ToInt32(doc.Get("ID"));
            return items;

            //Znode
            //return new LuceneSearchItem
            //{
            //    ProductID = Convert.ToInt32(doc.Get("ID")),
            //};
        }

        /// <summary>
        /// Map document to datalist with hits and searcher
        /// </summary>
        /// <param name="hits"></param>
        /// <param name="searcher"></param>
        /// <returns>Returns the Product list</returns>
        protected IEnumerable<string> MapDocumentToFacetList(IEnumerable<ScoreDoc> hits, IndexSearcher searcher)
        {
            return hits.Select(hit => MapDocumentToFacet(searcher.Doc(hit.Doc))).ToList();
        }

        /// <summary>
        /// Map Document To Data with document
        /// </summary>
        /// <param name="doc"></param>
        /// <returns>Returns the Search Product</returns>
        private string MapDocumentToFacet(Document doc)
        {
            return doc.Get("CategoryFacets");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace ZNode.Libraries.Search.Interfaces
{
    public interface IZNodeSearchRequest
    {
        string searchText { get; set; }
        string category { get; set; }
        string searchType { get; set; }
        int PortalCatalogID { get; set; }
        string FacetRefineBy { get; set; }
        string Relevance { get; set; }
        string ProductAtoZ { get; set; }
        string ProductZtoA { get; set; }
        int sortOrder { get; set; }

        List<KeyValuePair<string, IEnumerable<string>>> Facets { get; set; }

        List<SortCriteria> SortCriteria { get; set; }
    }
}

﻿using System.Collections.Generic;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace ZNode.Libraries.Search.Interfaces
{
    public interface IZNodeSearchResponse
    {
        List<IZNodeSearchCategoryItem> CategoryItems { get; set; }
        List<IZNodeSearchFacet> Facets { get; set; }
        List<IZNodeSearchItem> Products { get; set; }
    }

    public interface IZNodeSearchCategoryItem
    {
        int CategoryID { get; set; }
        string Name { get; set; }
        string Title { get; set; }
        int Count { get; set; }
        List<IZNodeSearchCategoryItem> Hierarchy { get; set; }
    }

    public interface IZNodeSearchItem
    {
        int ProductID { get; set; }
        int? DisplayOrder { get; set; } //zeon       
    }

    public interface IZNodeSearchFacet
    {
        string AttributeName { set; get; }
        List<IZNodeSearchFacetValue> AttributeValues { set; get; }
        int ControlTypeID { get; set; }
        int FacetDisplayOrder { set; get; } //zeon 
    }

    public interface IZNodeSearchFacetValue
    {
        string AttributeValue { get; set; }
        long FacetCount { set; get; }
        bool Selected { get; set; }
        //Zeon Custom Code :Starts
        List<int> ProductIDList { set; get; }
        int FacetValueDisplayOrder { set; get; }
        //Zeon Custom Code :Ends
    }    
    
}

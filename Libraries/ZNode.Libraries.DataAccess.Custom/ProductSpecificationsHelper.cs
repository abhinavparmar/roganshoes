﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ZNode.Libraries.DataAccess.Entities;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class ProductSpecificationsHelper
    {
        /// <summary>
        /// Get the Product Specification by Product and SKU id.
        /// </summary>
        /// <param name="ProductId">int Product ID</param>
        /// <param name="SkuId">int SKU ID</param>
        /// <returns>DataSet :Complete details of Specifications</returns>
        public DataSet GetProductSpecifications_ByProductIdSkuId(int ProductId, int SkuId)
        {
            // Create Instance of Connection     
            using (SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create Instance of Command Object
                SqlDataAdapter mySqlAdapter = new SqlDataAdapter("Zeon_GetProductSpecifications_ByProductIdSkuId", myConnection);

                // Mark the Command as a SPROC
                mySqlAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySqlAdapter.SelectCommand.CommandTimeout = 0;

                // Add Parameters to SPROC
                SqlParameter parameterCatalogId = new SqlParameter("@ProductId", SqlDbType.Int);
                parameterCatalogId.Value = ProductId;
                mySqlAdapter.SelectCommand.Parameters.Add(parameterCatalogId);

                SqlParameter parameterCategoryId = new SqlParameter("@SkuId", SqlDbType.Int);
                parameterCategoryId.Value = SkuId;
                mySqlAdapter.SelectCommand.Parameters.Add(parameterCategoryId);

                // Create and Fill the DataSet
                DataSet myDataSet = new DataSet();
                myConnection.Open();
                mySqlAdapter.Fill(myDataSet);

                //Release Resources
                mySqlAdapter.Dispose();
                myConnection.Close();

                //Return DataSet
                return myDataSet;
            }
        }

        /// <summary>
        /// Insert Product specification to FSZeon product specification table
        /// </summary>
        /// <param name="_prodSpec"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool InsertProductSpecification(ZeonProductSpecifications _prodSpec, out int message)
        {
            message = 1; // record insert successfully
            SqlConnection MyConnection = null;
            SqlCommand MyCommand = null;

            System.Data.SqlClient.SqlTransaction transaction = null;
            bool status = false;

            try
            {
                string ConnectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString;

                MyConnection = new SqlConnection(ConnectionStr);

                //Open Connection
                MyConnection.Open();

                // BeginTransaction() Requires Open Connection
                transaction = MyConnection.BeginTransaction();

                //Create instance of Command Object
                MyCommand = new SqlCommand("Zeon_ZeonProductSpecifications_Insert", MyConnection);

                // Mark the Command as a Stored Procedure
                MyCommand.CommandType = CommandType.StoredProcedure;
                MyCommand.CommandTimeout = 0;

                // Assign Transaction to Command
                MyCommand.Transaction = transaction;

                //Declare Parameters and Add to Command Object
                SqlParameter paramProductSpecId = new SqlParameter("@ProductSpecID", SqlDbType.Int);
                paramProductSpecId.Direction = ParameterDirection.Output;
                MyCommand.Parameters.Add(paramProductSpecId);

                SqlParameter paramProductId = new SqlParameter("@ProductID", SqlDbType.Int);
                paramProductId.Value = _prodSpec.ProductID;
                MyCommand.Parameters.Add(paramProductId);

                SqlParameter paramSKUID = new SqlParameter("@SKUID", SqlDbType.Int);
                paramSKUID.Value = _prodSpec.SKUID;
                MyCommand.Parameters.Add(paramSKUID);

                SqlParameter paramSpecName = new SqlParameter("@SpecificationName", SqlDbType.VarChar);
                paramSpecName.Value = _prodSpec.SpecificationName;
                MyCommand.Parameters.Add(paramSpecName);

                SqlParameter paramSpecValue = new SqlParameter("@SpecificationValue", SqlDbType.VarChar);
                paramSpecValue.Value = _prodSpec.SpecificationValue;
                MyCommand.Parameters.Add(paramSpecValue);

                SqlParameter paramDisplayOrder = new SqlParameter("@DisplayOrder", SqlDbType.Int);
                paramDisplayOrder.Value = _prodSpec.DisplayOrder;
                MyCommand.Parameters.Add(paramDisplayOrder);

                SqlParameter paramCustom1 = new SqlParameter("@Custom1", SqlDbType.VarChar);
                paramCustom1.Value = _prodSpec.Custom1;
                MyCommand.Parameters.Add(paramCustom1);

                SqlParameter paramCustom2 = new SqlParameter("@Custom2", SqlDbType.VarChar);
                paramCustom2.Value = _prodSpec.Custom2;
                MyCommand.Parameters.Add(paramCustom2);

                SqlParameter paramCustom3 = new SqlParameter("@Custom3", SqlDbType.VarChar);
                paramCustom3.Value = _prodSpec.Custom3;
                MyCommand.Parameters.Add(paramCustom3);

                SqlParameter paramCustom4 = new SqlParameter("@Custom4", SqlDbType.VarChar);
                paramCustom4.Value = _prodSpec.Custom4;
                MyCommand.Parameters.Add(paramCustom4);

                SqlParameter paramCustom5 = new SqlParameter("@Custom5", SqlDbType.VarChar);
                paramCustom5.Value = _prodSpec.Custom5;
                MyCommand.Parameters.Add(paramCustom5);

                SqlParameter paramMessage = new SqlParameter("@Message", SqlDbType.VarChar);
                paramMessage.Direction = ParameterDirection.Output;
                paramMessage.Size = 1;
                MyCommand.Parameters.Add(paramMessage);

                //Execute Query
                MyCommand.ExecuteNonQuery();

                // Set Return values 
                status = true;

                //If we reach here, all command succeeded, so commit the transaction
                transaction.Commit();

                if (paramProductSpecId.Value != DBNull.Value)
                {
                    _prodSpec.ProductSpecID = Convert.ToInt32(paramProductSpecId.Value);
                }
                if (paramMessage.Value != DBNull.Value)
                {
                    message = Convert.ToInt32(paramMessage.Value);
                    if (message.Equals(0)) // duplicate specification
                    {
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Insert Product specification failed- " + ex.Message);

                // Failed to insert, so rollback the transaction
                transaction.Rollback();

                status = false;
            }
            finally
            {
                if (transaction != null)
                    transaction.Dispose();

                if (MyConnection != null)
                {
                    // Finally, close the connection
                    MyConnection.Close();
                    // Release Resources
                    MyConnection.Dispose();
                }
            }
            // Return boolean value
            return status;
        }
    }
}

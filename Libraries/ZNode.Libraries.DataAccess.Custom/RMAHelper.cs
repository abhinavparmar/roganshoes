﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace ZNode.Libraries.DataAccess.Custom
{
    public class RMAHelper
    {
        /// <summary>
        /// Search the RMA Request for first name,last name,  rmaRequestID or Request status Id
        /// </summary>
        /// <param name="rmaRequestID">RMA Request ID to search.</param>
        ///  <param name="orderId">Order Id to search.</param>
        /// <param name="billingFirstName">Billing first name to search.</param>
        /// <param name="billingLastName">Billing last name to search.</param>
        /// <param name="startDate">RMA request created from this date.</param>
        /// <param name="endDate">RMA request created to this date.</param>
        /// <param name="requestStatusId">REquest status Id to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <param name="portalIds">Comma seperated portal Ids to search.</param>
        /// <returns>Returns the RMA request search result dataset.</returns>
        public DataSet SearchRMARequest(int? rmaRequestID, int? orderid, string billingFirstName, string billingLastName, DateTime? startDate, DateTime? endDate, int? requestStatusId, int? portalId, string portalIds)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_SEARCHRMA", connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@RMARequestID", rmaRequestID);
                adapter.SelectCommand.Parameters.AddWithValue("@OrderID", orderid);
                adapter.SelectCommand.Parameters.AddWithValue("@BillingFirstName", billingFirstName);
                adapter.SelectCommand.Parameters.AddWithValue("@BillingLastName", billingLastName);
                adapter.SelectCommand.Parameters.AddWithValue("@StartDate", startDate);
                adapter.SelectCommand.Parameters.AddWithValue("@EndDate", endDate);
                adapter.SelectCommand.Parameters.AddWithValue("@RequestStatusId", requestStatusId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalIds", portalIds);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get all RMA Request by Reason For Return ID
        /// </summary>
        /// <param name="ReasonForReturnID">Reason For Return ID to search.</param>
        /// <returns>Returns the RMA request search result dataset.</returns>
        public DataSet GetAllRMARequestByReasonID(int ReasonForReturnID)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetAllRMARequestByReasonID", connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@ReasonForReturnID", ReasonForReturnID);
               

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get all RMA Reason For Return 
        /// </summary>
        /// <returns>Returns thall RMA Reason For Return .</returns>
        public DataSet GetReasonForReturn()
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAllReasonForReturn", connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
         /// <summary>
        /// Get all RMA Request Items by RMA Request Item IDs
        /// </summary>
        /// <param name="RMARequestItemIDs">RMA RequestItem IDs ID to search.</param>
        /// <returns>Returns the RMA request Items search result dataset.</returns>
        public DataSet GetRMARequestItem(string RMARequestItemIDs)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetRMARequestItem", connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@RMARequestItemIDs", RMARequestItemIDs);
               

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get all RMA Request Report items by RMA Request  ID
        /// </summary>
        /// <param name="RMARequestID">RMA Request ID ID to search.</param>
        /// <returns>Returns the RMA request Items search result dataset.</returns>
        public DataSet GetRMARequestReport(int RMARequestID)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetRMARequestReport", connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@RMARequestID", RMARequestID);
               

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        /// <summary>
        /// Get GiftCard details for RMA Request Items by RMA Request IDs
        /// </summary>
        /// <param name="RMARequestID">RMA Request ID ID to search.</param>
        /// <returns>Returns the RMA request Items search result dataset.</returns>
        public DataSet GetGiftCardByRMARequest(int RMARequestID)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetGiftCardByRMARequest", connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@RMARequestID", RMARequestID);
               

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        /// <summary>
        /// Get all RMA Request Items by OrderID
        /// </summary>
        /// <param name="OrderID">OrderID to search.</param>
        /// <returns>Returns the RMA request Items search result dataset.</returns>
        public DataSet GetAllRMARequestItemByOrderID(int OrderID)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetAllRMARequestItemByOrderID", connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@OrderID", OrderID);


                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        /// <summary>
        /// Get Orderline items for RMA request based on Order ID
        /// </summary>
        /// <param name="OrderID"></param>
        /// <param name="RMARequestID"></param>
        /// <returns></returns>
        public DataSet GetRMAOrderLineItemsByOrderID(int OrderID,int RMARequestID,int IsReturnable,string flag)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetRMAOrderLineItemByOrderID", connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@OrderID", OrderID);
                adapter.SelectCommand.Parameters.AddWithValue("@RMAID", RMARequestID);
                adapter.SelectCommand.Parameters.AddWithValue("@IsReturnable", IsReturnable);
                adapter.SelectCommand.Parameters.AddWithValue("@Flag", flag);
                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        /// <summary>
        /// Delete the RMA request Items by RMA request Id.
        /// </summary>
        /// <param name="rmaRequestId">rma RequestId to delete the product object.</param>        
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteItemsByRMARequestID(int rmaRequestId)
        {
            bool isDeleted = false;

            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    command = new SqlCommand("ZNode_DeleteItemsByRMARequestID", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;

                    // Assign Transaction to Command
                    command.Transaction = transaction;

                    // Add parameters
                    command.Parameters.AddWithValue("@rmaRequestId", rmaRequestId);

                    // Execute Query
                    command.ExecuteNonQuery();

                    // Set Return values 
                    isDeleted = true;

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception)
                {
                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isDeleted = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }

            // Return boolean value
            return isDeleted;
        }
        /// <summary>
        /// Get AppendRMA Flag by RMA request Id.
        /// </summary>
        /// <param name="rmaRequestId">rma RequestId to Get Append RMAFlag.</param>        
        /// <returns>Returns true if APPEND RMA is 1 else returns false.</returns>
        public bool GetAppendRMAFlag(int rmaRequestId)
        {
            bool isAppend = false;

            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNODE_GetAppendRMAFlag", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;
                    command.Parameters.AddWithValue("@RMARequestID", rmaRequestId);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);


                    if (reader.Read())
                    {
                        if (reader[0].ToString() == "1")
                            isAppend = true;

                    }


                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }
                return isAppend;
            }
        }
        /// <summary>
        /// Get Order RMA Display Flag by Order Id.
        /// </summary>
        /// <param name="OrderId">Order Id to Get RMA display Flag.</param>        
        /// <returns>Returns true if Order RMA  is 1 else returns false.</returns>
        public bool GetOrderRMAFlag(int OrderId)
        {
            bool isAppend = false;

            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNode_GetOrderRMAFlag", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;

                    command.Parameters.AddWithValue("@OrderId", OrderId);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);


                    if (reader.Read())
                    {
                        if (reader[0].ToString() == "1")
                            isAppend = true;

                    }


                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }
                return isAppend;
            }
        }
           
       
    }
}

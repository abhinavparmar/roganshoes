﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public partial class PromotionHelper
    {
        /// <summary>
        /// Get all the promotions.
        /// </summary>        
        /// <returns>Returns promotion dataset.</returns>
        public DataTable GetCatalogDetailByProductsID(int productID)
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetCatlogDetailByProductID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.Parameters.AddWithValue("@ProductID", productID);
                adapter.SelectCommand.Parameters.AddWithValue("@LocalID", ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.LocaleID);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                // Create and fill the dataset
                DataTable dataTable = new DataTable();
                connection.Open();
                adapter.Fill(dataTable);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataTable;
            }
        }
    }
}

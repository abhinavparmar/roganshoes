using System;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public partial class DataManagerHelper
    {
        /// <summary>
        /// Gets the download tracking Data
        /// </summary>
        /// <param name="startDate">Get the tracking date from this date.</param>
        /// <param name="endDate">Get the tracking date to this date.</param>
        /// <returns>Returns the tracking data.</returns>
        public DataSet GetDownloadTrackingData(string startDate, string endDate)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchTrackingData", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@StartDate", startDate);
                adapter.SelectCommand.Parameters.AddWithValue("@EndDate", endDate);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get tags by catalogId and/or categoryId.
        /// </summary>
        /// <param name="categoryId">Category Id to get the tag dataset.</param>        
        /// <param name="catalogId">Catalog Id to get the tag dataset.</param>
        /// <returns>Returns the tags datase.</returns>
        public DataSet GetDownloadTags(int categoryId, int catalogId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetTagsByCategories", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@CategoryID", categoryId);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogID", catalogId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Upload the product data.
        /// </summary>
        /// <param name="dataTable">Datatable that contains data to upload.</param>
        /// <param name="sqlErrorCount">Sql error count while uploading the data.</param>
        /// <returns>Returns true if uploaded otherwise false.</returns>
        public bool UploadProducts(ZNodeProductDataset.ZNodeProductDataTable dataTable, out int sqlErrorCount)
        {
            return this.UploadProducts(dataTable, 0, out sqlErrorCount);
        }

        /// <summary>
        /// Upload the product data.
        /// </summary>
        /// <param name="dataTable">Datatable that contains data to upload.</param>
        /// <param name="accountId">Account Id </param>
        /// <param name="sqlErrorCount">Sql error count while uploading the data.</param>
        /// <returns>Returns true if uploaded otherwise false.</returns>
        public bool UploadProducts(ZNodeProductDataset.ZNodeProductDataTable dataTable, int accountId, out int sqlErrorCount)
        {
            // Declare the local variable
            int count = 0;
            sqlErrorCount = 0;
            SqlConnection sqlConnection = new SqlConnection();
            SqlTransaction transaction = null;

            try
            {
                // Create an Instance for Connection Object
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString;

                // Open the Connection
                sqlConnection.Open();

                foreach (ZNodeProductDataset.ZNodeProductRow dr in dataTable.Rows)
                {
                    try
                    {
                        transaction = sqlConnection.BeginTransaction();
                        SqlCommand sqlCommand = new SqlCommand("ZNode_UpsertProduct", sqlConnection);

                        // Set transaction for SqlCommand 
                        sqlCommand.Transaction = transaction;

                        // Set Command Type
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.CommandTimeout = 0;

                        // Add parameters and set values.
                        sqlCommand.Parameters.AddWithValue("@ProductID", dr.ProductID);
                        sqlCommand.Parameters.AddWithValue("@Name", dr.Name);
                        if (!dr.IsSKUNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@SKU", dr.SKU);
                        }

                        if (!dr.IsQuantityOnHandNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@QuantityOnHand", dr.QuantityOnHand);
                        }

                        if (!dr.IsShortDescriptionNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@ShortDescription", dr.ShortDescription);
                        }

                        if (!dr.IsFeaturesDescNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@FeaturesDesc", dr.FeaturesDesc);
                        }

                        sqlCommand.Parameters.AddWithValue("@Description", dr.Description);
                        sqlCommand.Parameters.AddWithValue("@ProductNum", dr.ProductNum);
                        sqlCommand.Parameters.AddWithValue("@ProductTypeID", dr.ProductTypeID);
                        if (!dr.IsExpirationPeriodNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@ExpirationPeriod", dr.ExpirationPeriod);
                        }

                        if (!dr.IsExpirationFrequencyNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@ExpirationFrequency", dr.ExpirationFrequency);
                        }
                        
                        if (!dr.IsRetailPriceNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@RetailPrice", dr.RetailPrice);
                        }

                        if (!dr.IsSalePriceNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@SalePrice", dr.SalePrice);
                        }

                        if (!dr.IsWholesalePriceNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@WholesalePrice", dr.WholesalePrice);
                        }

                        if (!dr.IsImageFileNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@ImageFile", dr.ImageFile);
                        }

                        if (!dr.IsWeightNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@Weight", dr.Weight);
                        }

                        if (!dr.IsLengthNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@Length", dr.Length);
                        }

                        if (!dr.IsWidthNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@Width", dr.Width);
                        }

                        if (!dr.IsHeightNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@Height", dr.Height);
                        }

                        if (!dr.IsDisplayOrderNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@DisplayOrder", dr.DisplayOrder);
                        }

                        sqlCommand.Parameters.AddWithValue("@ActiveInd", dr.ActiveInd);
                        sqlCommand.Parameters.AddWithValue("@CallForPricing", dr.CallForPricing);
                        sqlCommand.Parameters.AddWithValue("@HomepageSpecial", dr.HomepageSpecial);
                        sqlCommand.Parameters.AddWithValue("@InventoryDisplay", dr.InventoryDisplay);
                        sqlCommand.Parameters.AddWithValue("@Franchisable", dr.Franchisable);
                        if (!dr.IsKeywordsNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@Keywords", dr.Keywords);
                        }

                        if (!dr.IsManufacturerIDNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@ManufacturerID", dr.ManufacturerID);
                        }

                        if (!dr.IsShippingRuleTypeIDNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@ShippingRuleTypeID", dr.ShippingRuleTypeID);
                        }

                        if (!dr.IsSEOTitleNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@SEOTitle", dr.SEOTitle);
                        }

                        if (!dr.IsSEOKeywordsNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@SEOKeywords", dr.SEOKeywords);
                        }

                        if (!dr.IsSEODescriptionNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@SEODescription", dr.SEODescription);
                        }

                        if (!dr.IsInStockMsgNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@InStockMsg", dr.InStockMsg);
                        }

                        if (!dr.IsOutOfStockMsgNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@OutOfStockMsg", dr.OutOfStockMsg);
                        }

                        if (!dr.IsTrackInventoryIndNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@TrackInventoryInd", dr.TrackInventoryInd);
                        }

                        if (!dr.IsFreeShippingIndNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@FreeShippingInd", dr.FreeShippingInd);
                        }

                        if (!dr.IsNewProductIndNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@NewProductInd", dr.NewProductInd);
                        }

                        if (!dr.IsSEOURLNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@SEOURL", dr.SEOURL);
                        }

                        sqlCommand.Parameters.AddWithValue("@ShipSeparately", dr.ShipSeparately);
                        sqlCommand.Parameters.AddWithValue("@FeaturedInd", dr.FeaturedInd);
                        if (!dr.IsMaxQtyNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@MaxQty", dr.MaxQty);
                        }

                        sqlCommand.Parameters.AddWithValue("@RecurringBillingInd", dr.RecurringBillingInd);
                        sqlCommand.Parameters.AddWithValue("@RecurringBillingInstallmentInd", dr.RecurringBillingInstallmentInd);

                        if (!dr.IsPortalIDNull() && (dr.PortalID > 0))
                        {
                            sqlCommand.Parameters.AddWithValue("@PortalID", dr.PortalID);
                        }
                        else
                        {
                            sqlCommand.Parameters.AddWithValue("@PortalID", null);
                        }

                        if (accountId > 0)
                        {
                            sqlCommand.Parameters.AddWithValue("@AccountID", accountId);
                        }

                        if (!dr.IsReviewStateIDNull())
                        {
                            sqlCommand.Parameters.AddWithValue("@ReviewStateID", dr.ReviewStateID);
                        }

                        // Execute the Query.
                        count += sqlCommand.ExecuteNonQuery();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                        sqlErrorCount++;
                        transaction.Rollback();
                    }
                }
            }
            catch (Exception exception)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                sqlErrorCount++;
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Upload inventory data
        /// </summary>
        /// <param name="dataTable">Datatable that contains data to upload.</param>
        /// <param name="sqlErrorCount">Sql error count while uploading the data.</param>
        /// <returns>Returns true if uploaded otherwise false.</returns>
        public bool UploadInventory(ZNodeSKUInventoryDataset.ZNodeSKUInventoryDataTable dataTable, out int sqlErrorCount)
        {
            // declare the local variable
            int count = 0;
            sqlErrorCount = 0;

            // Create an Instance for Connection Object
            using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlTransaction transaction = null;
                try
                {
                    // Open the Connection
                    sqlConnection.Open();

                    // Transaction Begins
                    transaction = sqlConnection.BeginTransaction();

                    foreach (ZNodeSKUInventoryDataset.ZNodeSKUInventoryRow dr in dataTable.Rows)
                    {
                        try
                        {
                            SqlCommand sqlCommand = new SqlCommand("ZNode_UpsertSKUInventory", sqlConnection);

                            // Set transaction for SqlCommand 
                            sqlCommand.Transaction = transaction;

                            // Set Command Type
                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.CommandTimeout = 0;

                            // Add parameters and set values.
                            sqlCommand.Parameters.Add("@SKU ", SqlDbType.NVarChar).Value = dr.SKU;
                            if (!dr.IsQuantityOnHandNull())
                            {
                                sqlCommand.Parameters.Add("@QuantityOnHand", SqlDbType.Int).Value = dr.QuantityOnHand;
                            }

                            if (!dr.IsReOrderLevelNull())
                            {
                                sqlCommand.Parameters.Add("@ReOrderLevel", SqlDbType.Int).Value = dr.ReOrderLevel;
                            }

                            // Execute the Query.
                            count += sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                            sqlErrorCount++;
                        }
                    }
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    sqlErrorCount++;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Commit();
                    }

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
                
                if (count > 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Upload Tags data
        /// </summary>
        /// <param name="dataTable">Datatable that contains data to upload.</param>
        /// <param name="sqlErrorCount">Sql error count while uploading the data.</param>
        /// <returns>Returns true if uploaded otherwise false.</returns>
        public bool UploadTags(ZNodeTagsDataSet.ZNodeTagDataTable dataTable, out int sqlErrorCount)
        {
            // declare the local variable
            int count = 0;
            sqlErrorCount = 0;

            // Create an Instance for Connection Object
            using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlTransaction transaction = null;
                try
                {
                    // Open the Connection
                    sqlConnection.Open();

                    // Transaction Begins
                    transaction = sqlConnection.BeginTransaction();

                    foreach (ZNodeTagsDataSet.ZNodeTagRow dr in dataTable.Rows)
                    {
                        try
                        {
                            SqlCommand sqlCommand = new SqlCommand("ZNode_UpsertTags", sqlConnection);

                            // Set transaction for SqlCommand 
                            sqlCommand.Transaction = transaction;

                            // Set Command Type
                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.CommandTimeout = 0;

                            // Add parameters and set values.                    
                            sqlCommand.Parameters.Add("@TagGroupLabel", SqlDbType.NVarChar).Value = dr.TagGroupLabel;
                            sqlCommand.Parameters.Add("@ControlType", SqlDbType.NVarChar).Value = dr.ControlType;
                            if (!dr.IsCatalogIDNull())
                            {
                                sqlCommand.Parameters.Add("@CatalogID", SqlDbType.Int).Value = dr.CatalogID;
                            }

                            if (!dr.IsDisplayOrderNull())
                            {
                                sqlCommand.Parameters.Add("@DisplayOrder", SqlDbType.Int).Value = dr.DisplayOrder;
                            }

                            if (!dr.IsTagNameNull())
                            {
                                sqlCommand.Parameters.Add("@TagName", SqlDbType.NVarChar).Value = dr.TagName;
                            }

                            if (!dr.IsTagDisplayOrderNull())
                            {
                                sqlCommand.Parameters.Add("@TagDisplayOrder", SqlDbType.Int).Value = dr.TagDisplayOrder;
                            }
                            
                            if (!dr.IsIconPathNull())
                            {
                                sqlCommand.Parameters.Add("@IconPath", SqlDbType.NVarChar).Value = dr.IconPath;
                            }
                            
                            if (!dr.IsProductIDNull())
                            {
                                sqlCommand.Parameters.Add("@ProductID", SqlDbType.Int).Value = dr.ProductID;
                            }

                            sqlCommand.Parameters.Add("@CategoryID", SqlDbType.Int).Value = dr.CategoryID;
                            if (!dr.IsCategoryDisplayOrderNull())
                            {
                                sqlCommand.Parameters.Add("@CategoryDisplayOrder", SqlDbType.Int).Value = dr.CategoryDisplayOrder;
                            }

                            // Execute the Query.
                            count += sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                            sqlErrorCount++;
                        }
                    }
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    sqlErrorCount++;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Commit();
                    }

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                if (count > 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Upload SKU data
        /// </summary>
        /// <param name="dataTable">Datatable that contains data to upload.</param>
        /// <param name="sqlErrorCount">Sql error count while uploading the data.</param>
        /// <returns>Returns true if uploaded otherwise false.</returns>
        public bool UpdateSKU(ZNodeSKUDataSet.ZNodeSKUDataTable dataTable, out int sqlErrorCount)
        {
            // declare the local variable
            int count = 0;
            sqlErrorCount = 0;

            // Create an Instance for Connection Object
            using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlTransaction transaction = null;
                try
                {
                    // Open the Connection
                    sqlConnection.Open();

                    // Transaction Begins
                    transaction = sqlConnection.BeginTransaction();

                    foreach (ZNodeSKUDataSet.ZNodeSKURow dr in dataTable)
                    {
                        try
                        {
                            SqlCommand sqlCommand = new SqlCommand("ZNode_UpsertSKU", sqlConnection);

                            // Set transaction for SqlCommand 
                            sqlCommand.Transaction = transaction;

                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.CommandTimeout = 0;

                            SqlParameter p1 = new SqlParameter("@SKUID", SqlDbType.Int);
                            p1.Value = dr.SKUID;
                            sqlCommand.Parameters.Add(p1);

                            SqlParameter p2 = new SqlParameter("@ProductID", SqlDbType.Int);
                            p2.Value = dr.ProductID;
                            sqlCommand.Parameters.Add(p2);

                            SqlParameter p3 = new SqlParameter("@SKU", SqlDbType.NVarChar);
                            p3.Value = dr.SKU;
                            sqlCommand.Parameters.Add(p3);

                            if (!dr.IsNoteNull())
                            {
                                SqlParameter p4 = new SqlParameter("@Note", SqlDbType.NVarChar);
                                p4.Value = dr.Note;
                                sqlCommand.Parameters.Add(p4);
                            }

                            if (!dr.IsWeightAdditionalNull())
                            {
                                SqlParameter p5 = new SqlParameter("@WeightAdditional", SqlDbType.Decimal);
                                p5.Value = dr.WeightAdditional;
                                sqlCommand.Parameters.Add(p5);
                            }
                              if (!dr.IsSKUPicturePathNull())
                            {
                                SqlParameter p12 = new SqlParameter("@SKUPicturePath", SqlDbType.NVarChar);
                                p12.Value = dr.SKUPicturePath;
                                sqlCommand.Parameters.Add(p12);
                            }
                            if (!dr.IsImageAltTagNull())
                            {
                                SqlParameter p6 = new SqlParameter("@ImageAltTag", SqlDbType.NVarChar);
                                p6.Value = dr.ImageAltTag;
                                sqlCommand.Parameters.Add(p6);
                            }

                            if (!dr.IsDisplayOrderNull())
                            {
                                SqlParameter p7 = new SqlParameter("@DisplayOrder", SqlDbType.Int);
                                p7.Value = dr.DisplayOrder;
                                sqlCommand.Parameters.Add(p7);
                            }

                            if (!dr.IsRetailPriceOverrideNull())
                            {
                                SqlParameter p8 = new SqlParameter("@RetailPriceOverride", SqlDbType.Decimal);
                                p8.Value = dr.RetailPriceOverride;
                                sqlCommand.Parameters.Add(p8);
                            }

                            if (!dr.IsSalePriceOverrideNull())
                            {
                                SqlParameter p9 = new SqlParameter("@SalePriceOverride", SqlDbType.Decimal);
                                p9.Value = dr.SalePriceOverride;
                                sqlCommand.Parameters.Add(p9);
                            }

                            if (!dr.IsWholesalePriceOverrideNull())
                            {
                                SqlParameter p10 = new SqlParameter("@WholesalePriceOverride", SqlDbType.Decimal);
                                p10.Value = dr.WholesalePriceOverride;
                                sqlCommand.Parameters.Add(p10);
                            }

                            SqlParameter p11 = new SqlParameter("@ActiveInd", SqlDbType.Bit);
                            p11.Value = dr.ActiveInd;
                            sqlCommand.Parameters.Add(p11);
                            
                            // Execute the Query.
                            count += sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                            sqlErrorCount++;
                        }
                    }
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    sqlErrorCount++;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Commit();
                    }

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                if (count > 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Upload Product Attribute data
        /// </summary>
        /// <param name="dataTable">Datatable that contains data to upload.</param>
        /// <param name="sqlErrorCount">Sql error count while uploading the data.</param>
        /// <returns>Returns true if uploaded otherwise false.</returns>
        public bool UpdateAttribute(ZNodeAttributeDataSet.ZNodeProductAttributeDataTable dataTable, out int sqlErrorCount)
        {
            // declare the local variable
            int count = 0;
            sqlErrorCount = 0;

            // Create an Instance for Connection Object
            using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlTransaction transaction = null;
                try
                {
                    // Open the Connection
                    sqlConnection.Open();

                    // Transaction Begins
                    transaction = sqlConnection.BeginTransaction();

                    foreach (ZNodeAttributeDataSet.ZNodeProductAttributeRow dr in dataTable)
                    {
                        try
                        {
                            SqlCommand sqlCommand = new SqlCommand("ZNode_UpsertProductAttribute", sqlConnection);

                            // Set transaction for SqlCommand 
                            sqlCommand.Transaction = transaction;

                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.CommandTimeout = 0;

                            SqlParameter p1 = new SqlParameter("@AttributeId", SqlDbType.Int);
                            p1.Value = dr.AttributeId;
                            sqlCommand.Parameters.Add(p1);

                            SqlParameter p2 = new SqlParameter("@AttributeTypeId", SqlDbType.Int);
                            p2.Value = dr.AttributeTypeId;
                            sqlCommand.Parameters.Add(p2);

                            SqlParameter p3 = new SqlParameter("@Name", SqlDbType.NVarChar);
                            p3.Value = dr.Name;
                            sqlCommand.Parameters.Add(p3);

                            if (!dr.IsExternalIdNull())
                            {
                                SqlParameter p4 = new SqlParameter("@ExternalId", SqlDbType.NVarChar);
                                p4.Value = dr.ExternalId;
                                sqlCommand.Parameters.Add(p4);
                            }

                            if (!dr.IsOldAttributeIdNull())
                            {
                                SqlParameter p5 = new SqlParameter("@OldAttributeId", SqlDbType.Decimal);
                                p5.Value = dr.OldAttributeId;
                                sqlCommand.Parameters.Add(p5);
                            }

                            if (!dr.IsDisplayOrderNull())
                            {
                                SqlParameter p6 = new SqlParameter("@DisplayOrder", SqlDbType.Int);
                                p6.Value = dr.DisplayOrder;
                                sqlCommand.Parameters.Add(p6);
                            }

                            SqlParameter p7 = new SqlParameter("@IsActive", SqlDbType.Bit);
                            p7.Value = dr.IsActive;
                            sqlCommand.Parameters.Add(p7);

                            // Execute the Query.
                            count += sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                            sqlErrorCount++;
                        }
                    }
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    sqlErrorCount++;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Commit();
                    }

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                if (count > 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Upload Product Pricing data
        /// </summary>
        /// <param name="dataTable">Datatable that contains data to upload.</param>
        /// <param name="sqlErrorCount">Sql error count while uploading the data.</param>
        /// <returns>Returns true if uploaded otherwise false.</returns>
        public bool UpdatePricing(ZNodePricingDataSet.ZNodeProductDataTable dataTable, out int sqlErrorCount)
        {
            // declare the local variable
            sqlErrorCount = 0;

            // Create an Instance for Connection Object
            using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlTransaction transaction = null;
                try
                {
                    // Open the Connection
                    sqlConnection.Open();

                    // Transaction Begins
                    transaction = sqlConnection.BeginTransaction();
                    foreach (ZNodePricingDataSet.ZNodeProductRow dr in dataTable)
                    {
                        try
                        {
                            SqlCommand sqlCommand = new SqlCommand("ZNODE_UpdateSkuPrice", sqlConnection);

                            // Set transaction for SqlCommand 
                            sqlCommand.Transaction = transaction;

                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.CommandTimeout = 0;

                            SqlParameter p1 = new SqlParameter("@Sku", SqlDbType.VarChar);
                            p1.Value = dr.Sku;
                            sqlCommand.Parameters.Add(p1);
                            if (!dr.IsProductIDNull())
                            {
                                SqlParameter p2 = new SqlParameter("@ProductId", SqlDbType.Int);
                                p2.Value = dr.ProductID;
                                sqlCommand.Parameters.Add(p2);
                            }

                            if (!dr.IsSKUIdNull())
                            {
                                SqlParameter p3 = new SqlParameter("@SkuId", SqlDbType.Int);
                                p3.Value = dr.SKUId;
                                sqlCommand.Parameters.Add(p3);
                            }

                            if (!dr.IsAddOnvalueIdNull())
                            {
                                SqlParameter p4 = new SqlParameter("@AddOnValueId", SqlDbType.Int);
                                p4.Value = dr.AddOnvalueId;
                                sqlCommand.Parameters.Add(p4);
                            }

                            if (!dr.IsRetailPriceNull())
                            {
                                SqlParameter p5 = new SqlParameter("@RetailPrice", SqlDbType.Money);
                                p5.Value = dr.RetailPrice;
                                sqlCommand.Parameters.Add(p5);
                            }

                            if (!dr.IsSalePriceNull())
                            {
                                SqlParameter p6 = new SqlParameter("@SalePrice", SqlDbType.Money);
                                p6.Value = dr.SalePrice;
                                sqlCommand.Parameters.Add(p6);
                            }

                            if (!dr.IsWholesalePriceNull())
                            {
                                SqlParameter p7 = new SqlParameter("@WholeSalePrice", SqlDbType.Money);
                                p7.Value = dr.WholesalePrice;
                                sqlCommand.Parameters.Add(p7);
                            }
                            if (!dr.IsIsfirstSkuNull())
                            {
                                SqlParameter p8 = new SqlParameter("@IsfirstSku", SqlDbType.Bit);
                                p8.Value = dr.IsfirstSku;
                                sqlCommand.Parameters.Add(p8);
                            }
                            // Execute the Query.
                            sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                            sqlErrorCount++;
                        }
                    }
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    sqlErrorCount++;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Commit();
                    }

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                if (sqlErrorCount == 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Upload Order Shiping status data
        /// </summary>
        /// <param name="dataTable">Datatable that contains data to upload.</param>
        /// <param name="sqlErrorCount">Sql error count while uploading the data.</param>
        /// <returns>Returns true if uploaded otherwise false.</returns>
        public bool UpdateShippingStatus(ZNodeShippingStatusDataSet.ZNodeShippingStatusDataTable dataTable, out int sqlErrorCount)
        {
            // declare the local variable
            int count = 0;
            sqlErrorCount = 0;

            // Create an Instance for Connection Object
            using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlTransaction transaction = null;
                try
                {
                    // Open the Connection
                    sqlConnection.Open();

                    // Transaction Begins
                    transaction = sqlConnection.BeginTransaction();
                    foreach (ZNodeShippingStatusDataSet.ZNodeShippingStatusRow dr in dataTable)
                    {
                        try
                        {
                            SqlCommand sqlCommand = new SqlCommand("ZNode_UpdateShippingStatus", sqlConnection);

                            // Set transaction for SqlCommand 
                            sqlCommand.Transaction = transaction;

                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.CommandTimeout = 0;

                            SqlParameter p1 = new SqlParameter("@OrderId", SqlDbType.Int);
                            p1.Value = dr.OrderId;
                            sqlCommand.Parameters.Add(p1);
                            if (!dr.IsShipDateNull())
                            {
                                SqlParameter p2 = new SqlParameter("@ShipDate", SqlDbType.DateTime);
                                p2.Value = dr.ShipDate;
                                sqlCommand.Parameters.Add(p2);
                            }

                            if (!dr.IsTrackingNumberNull())
                            {
                                SqlParameter p3 = new SqlParameter("@TrackingNumber", SqlDbType.NVarChar);
                                p3.Value = dr.TrackingNumber;
                                sqlCommand.Parameters.Add(p3);
                            }

                            // Execute the Query.
                            count += sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                            sqlErrorCount++;
                        }
                    }
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    sqlErrorCount++;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Commit();
                    }

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                if (count > 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Upload Order Shiping status data
        /// </summary>
        /// <param name="dataTable">Datatable that contains data to upload.</param>
        /// <param name="sqlErrorCount">Sql error count while uploading the data.</param>
        /// <returns>Returns true if uploaded otherwise false.</returns>
        public bool UpdateZipCode(ZNodeZipCodeDataSet.ZNodeZipCodeDataTable dataTable, out int sqlErrorCount)
        {
            // declare the local variable
            int count = 0;
            sqlErrorCount = 0;

            // Create an Instance for Connection Object
            using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlTransaction transaction = null;
                try
                {
                    // Open the Connection
                    sqlConnection.Open();

                    // Transaction Begins
                    transaction = sqlConnection.BeginTransaction();
                    foreach (ZNodeZipCodeDataSet.ZNodeZipCodeRow dr in dataTable)
                    {
                        try
                        {
                            SqlCommand sqlCommand = new SqlCommand("ZNode_UpsertZipCode", sqlConnection);

                            // Set transaction for SqlCommand 
                            sqlCommand.Transaction = transaction;

                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.CommandTimeout = 0;

                            SqlParameter p = new SqlParameter("@ZIPCodeId", SqlDbType.Int);
                            p.Value = dr.ZipCodeID;
                            sqlCommand.Parameters.Add(p);

                            if (!dr.IsZIPNull())
                            {
                                SqlParameter p1 = new SqlParameter("@ZIP", SqlDbType.NVarChar);
                                p1.Value = dr.ZIP;
                                sqlCommand.Parameters.Add(p1);
                            }

                            if (!dr.IsZIPTypeNull())
                            {
                                SqlParameter p2 = new SqlParameter("@ZIPType", SqlDbType.NVarChar);
                                p2.Value = dr.ZIPType;
                                sqlCommand.Parameters.Add(p2);
                            }

                            if (!dr.IsCityNameNull())
                            {
                                SqlParameter p3 = new SqlParameter("@CityName", SqlDbType.NVarChar);
                                p3.Value = dr.CityName;
                                sqlCommand.Parameters.Add(p3);
                            }

                            if (!dr.IsCityTypeNull())
                            {
                                SqlParameter p4 = new SqlParameter("@CityType", SqlDbType.NVarChar);
                                p4.Value = dr.CityType;
                                sqlCommand.Parameters.Add(p4);
                            }

                            if (!dr.IsStateNameNull())
                            {
                                SqlParameter p5 = new SqlParameter("@StateName", SqlDbType.NVarChar);
                                p5.Value = dr.StateName;
                                sqlCommand.Parameters.Add(p5);
                            }

                            if (!dr.IsStateAbbrNull())
                            {
                                SqlParameter p6 = new SqlParameter("@StateAbbr", SqlDbType.NVarChar);
                                p6.Value = dr.StateAbbr;
                                sqlCommand.Parameters.Add(p6);
                            }

                            if (!dr.IsAreaCodeNull())
                            {
                                SqlParameter p7 = new SqlParameter("@AreaCode", SqlDbType.NVarChar);
                                p7.Value = dr.AreaCode;
                                sqlCommand.Parameters.Add(p7);
                            }

                            if (!dr.IsLatitudeNull())
                            {
                                SqlParameter p8 = new SqlParameter("@Latitude", SqlDbType.Decimal);
                                p8.Value = dr.Latitude;
                                sqlCommand.Parameters.Add(p8);
                            }

                            if (!dr.IsLongitudeNull())
                            {
                                SqlParameter p9 = new SqlParameter("@Longitude", SqlDbType.Decimal);
                                p9.Value = dr.Longitude;
                                sqlCommand.Parameters.Add(p9);
                            }

                            if (!dr.IsCountyNameNull())
                            {
                                SqlParameter p10 = new SqlParameter("@CountyName", SqlDbType.VarChar);
                                p10.Value = dr.CountyName;
                                sqlCommand.Parameters.Add(p10);
                            }

                            if (!dr.IsCountyFIPSNull())
                            {
                                SqlParameter p11 = new SqlParameter("@CountyFIPS", SqlDbType.VarChar);
                                p11.Value = dr.CountyFIPS;
                                sqlCommand.Parameters.Add(p11);
                            }

                            if (!dr.IsStateFIPSNull())
                            {
                                SqlParameter p12 = new SqlParameter("@StateFIPS", SqlDbType.VarChar);
                                p12.Value = dr.StateFIPS;
                                sqlCommand.Parameters.Add(p12);
                            }

                            if (!dr.IsMSACodeNull())
                            {
                                SqlParameter p13 = new SqlParameter("@MSACode", SqlDbType.VarChar);
                                p13.Value = dr.MSACode;
                                sqlCommand.Parameters.Add(p13);
                            }

                            if (!dr.IsTimeZoneNull())
                            {
                                SqlParameter p14 = new SqlParameter("@TimeZone", SqlDbType.VarChar);
                                p14.Value = dr.TimeZone;
                                sqlCommand.Parameters.Add(p14);
                            }

                            if (!dr.IsUTCNull())
                            {
                                SqlParameter p15 = new SqlParameter("@UTC", SqlDbType.Decimal);
                                p15.Value = dr.UTC;
                                sqlCommand.Parameters.Add(p15);
                            }

                            if (!dr.IsDSTNull())
                            {
                                SqlParameter p16 = new SqlParameter("@DST", SqlDbType.Char);
                                p16.Value = dr.DST;
                                sqlCommand.Parameters.Add(p16);
                            }

                            // Execute the Query.
                            count += sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                            sqlErrorCount++;
                        }
                    }
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    sqlErrorCount++;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Commit();
                    }

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                if (count > 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
}

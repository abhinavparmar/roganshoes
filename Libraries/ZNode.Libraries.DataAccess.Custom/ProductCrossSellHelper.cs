using System;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Product CrossSell related Helper methods
    /// </summary>
    public partial class ProductCrossSellHelper
    {
        #region Public Methods
        /// <summary>
        /// Get related product items for a product
        /// </summary>
        /// <param name="productId">Product Id to get the product details.</param>
        /// <returns>Returns the product details dataset.</returns>
        public DataSet GetByProductID(int productId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetRelatedProductByProductID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@Product_Id", productId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Add a new product crossSell item
        /// </summary>
        /// <param name="productId">Parent product Id.</param>
        /// <param name="relatedProductId">Relate product item to add.</param>
        /// <returns>Returns true if inserted otherwise false.</returns> 
        public bool Insert(int productId, int relatedProductId, int CrossSellTypeId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand mySqlCommand = null;

                bool isAdded = false;
                try
                {
                    // Create Instance of  Command Object
                    mySqlCommand = new SqlCommand("Zeon_InsertCrossSellProduct", connection);

                    //Mark the Command as Stored Procedure
                    mySqlCommand.CommandType = CommandType.StoredProcedure;
                    mySqlCommand.CommandTimeout = 0;

                    SqlParameter MyParam1 = new SqlParameter("@ProductId", SqlDbType.Int);
                    MyParam1.Value = productId;

                    SqlParameter MyParam2 = new SqlParameter("@RelatedProductId", SqlDbType.Int);
                    MyParam2.Value = relatedProductId;

                    SqlParameter MyParam3 = new SqlParameter("@CrossSellTypeId", SqlDbType.Int);
                    MyParam3.Value = CrossSellTypeId;

                    mySqlCommand.Parameters.Add(MyParam1);
                    mySqlCommand.Parameters.Add(MyParam2);
                    mySqlCommand.Parameters.Add(MyParam3);
                   
                    // Execute the Query 
                    connection.Open();
                    mySqlCommand.ExecuteNonQuery();

                    connection.Close();

                    isAdded = true;
                }
                catch 
                {
                }

                return isAdded;
            }
        }

        /// <summary>
        /// Delete a product cross sell item   
        /// </summary>
        /// <param name="relatedProductId">Related product Id.</param>
        /// <param name="productId">Parent product Id.</param>        
        /// <returns>Returns true if removed otherwise false.</returns>
        public bool RemoveProduct(int relatedProductId, int productId)
        {
            bool isRemoved = false;

            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;

                try
                {
                    // Create Instance of  Command Object
                    command = new SqlCommand("ZNODE_DELETERELATEDPRODUCT", connection);

                    // Mark the Command as Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;

                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@RelatedProductId", relatedProductId);

                    // Execute the Query 
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();

                    isRemoved = true;
                }
                catch (Exception ex)
                {
                    string error = ex.ToString();
                }

                // return Boolean
                return isRemoved;
            }
        }
        #endregion
    }
}

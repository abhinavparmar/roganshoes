﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ZNode.Libraries.DataAccess.Entities;
using System.Configuration;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class ZeonSKUImageHelper
    {
        /// <summary>
        /// Get all Manufacturer with its initial alphabets
        /// </summary>
        /// <returns></returns>
        public List<ZeonSKUImage> GetSkuImagesBySkuID(int skuID)
        {
            List<ZeonSKUImage> zeonSkuImageList = new List<ZeonSKUImage>();

            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter command = new SqlDataAdapter("Zeon_GetSKUImagesBySKUID", connection);

                // Mark the command as store procedure
                command.SelectCommand.CommandType = CommandType.StoredProcedure;
                command.SelectCommand.CommandTimeout = 0;

                SqlParameter parameter = new SqlParameter("@SKUId", SqlDbType.NVarChar, 100);
                parameter.Value = skuID;
                command.SelectCommand.Parameters.Add(parameter);

                // Create and Fill the datatable
                DataTable myDatatable = new DataTable();
                command.Fill(myDatatable);

                command.Dispose();

                // close connection
                connection.Close();

                if (myDatatable != null && myDatatable.Rows.Count > 0)
                {
                    zeonSkuImageList = zConvertDataTableToList.ConvertTo<ZeonSKUImage>(myDatatable);
                }
                return zeonSkuImageList;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ZNode.Libraries.DataAccess.Entities;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Manages the methods related to seo friendly urls of product, cateory and content pages
    /// </summary>
    public partial class SEOUrlHelper
    {
        #region Methods To Enable Same SEO URL on Diffrent Portal

        /// <summary>
        /// Returns Category list by SEO and portalID
        /// </summary>
        /// <param name="seoUrl">string</param>
        /// <param name="portalId">int</param>
        /// <param name="localeID">int</param>
        /// <returns>List<Category></returns>
        public List<Category> GetCategoryBySEOURL(string seoUrl, int portalId,int localeID)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_NT_ZNodeCategory_GetBySEOURL_Extn", connection);
                List<Category> categoryList = new List<Category>();
                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@SEOURL", seoUrl);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", localeID);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    categoryList = zConvertDataTableToList.ConvertTo<Category>(dataSet.Tables[0] as DataTable);
                }
                return categoryList;
            }
        }

        /// <summary>
        /// Returns Product list by SEO and portalID
        /// </summary>
        /// <param name="seoUrl">string</param>
        /// <param name="portalId">int</param>
        /// <param name="localeID">int</param>
        /// <returns>List<Category></returns>
        public List<Product> GetProductBySEOURL(string seoUrl, int portalId, int localeID)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_NT_ZNodeProduct_GetBySEOURL_Extn", connection);
                List<Product> productList = new List<Product>();
                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@SEOURL", seoUrl);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", localeID);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    productList = zConvertDataTableToList.ConvertTo<Product>(dataSet.Tables[0] as DataTable);
                }
                return productList;
            }
        }

        /// <summary>
        /// Returns Content Page list by SEO and portalID
        /// </summary>
        /// <param name="seoUrl">string</param>
        /// <param name="portalId">int</param>
        /// <param name="localeID">int</param>
        /// <returns>List<Category></returns>
        public List<ContentPage> GetContentPageBySEOURL(string seoUrl, int portalId, int localeID)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_NT_ZNodeContentPage_GetBySEOURL_Extn", connection);
                List<ContentPage> contentPageList = new List<ContentPage>();
                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@SEOURL", seoUrl);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", localeID);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    contentPageList = zConvertDataTableToList.ConvertTo<ContentPage>(dataSet.Tables[0] as DataTable);
                }
                return contentPageList;
            }
        }

        /// <summary>
        /// Returns ManufacturerExtn Page list by SEO and portalID
        /// </summary>
        /// <param name="seoUrl">string</param>
        /// <param name="portalId">int</param>
        /// <param name="localeID">int</param>
        /// <returns>List<Category></returns>
        public List<ManufacturerExtn> GetManufacturerExtnBySEOURL(string seoUrl, int portalId, int localeID)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_NT_ZNodeManufacturerExtn_GetBySEOURL_Extn", connection);
                List<ManufacturerExtn> manufacturerExtnList = new List<ManufacturerExtn>();
                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@SEOURL", seoUrl);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", localeID);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    manufacturerExtnList = zConvertDataTableToList.ConvertTo<ManufacturerExtn>(dataSet.Tables[0] as DataTable);
                }
                return manufacturerExtnList;
            }
        }

        #endregion

    }
}

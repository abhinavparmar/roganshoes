using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace ZNode.Libraries.DataAccess.Custom
{

    /// <summary>
    /// Product Category related helper functions
    /// </summary>
    public class ProductCategoryHelper
    {

        #region Public Methods   
        /// <summary>
        /// Returns category items for a productid
        /// </summary>
        /// <param name="ProductID"></param>
        /// <returns>DataSet</returns>
        public DataSet GetByProductID(int productID, int? portalId = null)
        {
                // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetProductCategoryByProductID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@PRODUCTID",productID);
                adapter.SelectCommand.Parameters.AddWithValue("@PORTALID", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }

        }

        /// <summary>
        /// Returns category items for a productid
        /// </summary>
        /// <param name="ProductID"></param>
        /// <returns>DataSet</returns>
        public DataSet GetByCategoryID(int CategoryID)
        {
                // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetProductCategoryByCategoryID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                // Add Parameters to SPROC
                SqlParameter myParam = new SqlParameter("@CATEGORYID", SqlDbType.Int);
                myParam.Value = CategoryID;
                adapter.SelectCommand.Parameters.Add(myParam);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }

        }

        /// <summary>
        /// Add a new product to Product Category
        /// </summary>
        /// <param name="PortalID"></param>
        /// <param name="ProductID"></param>
        /// <param name="RelatedProductID"></param>
        public bool Insert(int ProductID, int CategoryID)
        {
            SqlConnection MySqlConnection = null;
            SqlCommand MySqlCommand = null;

            bool returnBool = false;
            try
            {
                // Create instance of connection Object
                string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ToString();
                MySqlConnection = new SqlConnection(ConnectionString);

                // Create Instance of  Command Object
                MySqlCommand = new SqlCommand("ZNode_InsertRelatedProductToCategory", MySqlConnection);

                // Mark the Command as Stored Procedure
                MySqlCommand.CommandType = CommandType.StoredProcedure;
                MySqlCommand.CommandTimeout = 0;

                // Declare parameters and Pass Values
                SqlParameter MyParam1 = new SqlParameter("@PRODUCTID", SqlDbType.Int);
                MyParam1.Value = ProductID;

                SqlParameter MyParam2 = new SqlParameter("@CATEGORYID", SqlDbType.Int);
                MyParam2.Value = CategoryID;

                // Add parameters to Command Object
                MySqlCommand.Parameters.Add(MyParam1);
                MySqlCommand.Parameters.Add(MyParam2);
                
                // Execute the Query 
                MySqlConnection.Open();
                MySqlCommand.ExecuteNonQuery();
                MySqlConnection.Close();

                returnBool = true;
            }
            catch { }

            return returnBool;
        }

        /// <summary>
        /// Delete a product from Product Category   
        /// </summary>
        /// <param name="RelatedProductID"></param>
        /// <param name="productID"></param>
        /// <param name="PortalID"></param>
        /// <returns>Boolean</returns>
        public bool RemoveProduct(int ProductID, int CategoryID)
        {
            bool ReturnBool = false;
            SqlConnection MySqlConnection = null;
            SqlCommand MySqlCommand = null;

            try
            {
                // Create instance of connection Object
                string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ToString();
                MySqlConnection = new SqlConnection(ConnectionString);

                // Create Instance of  Command Object
                MySqlCommand = new SqlCommand("ZNode_DeleteRelatedProductCategory", MySqlConnection);

                // Mark the Command as Stored Procedure
                MySqlCommand.CommandType = CommandType.StoredProcedure;
                MySqlCommand.CommandTimeout = 0;

                // Declare parameters 
                SqlParameter MyParam1 = new SqlParameter("@PRODUCTID", SqlDbType.Int);
                MyParam1.Value = ProductID;

                SqlParameter MyParam2 = new SqlParameter("@CATEGORYID", SqlDbType.Int);
                MyParam2.Value = CategoryID;

                // Add parameters to Command Object
                MySqlCommand.Parameters.Add(MyParam1);
                MySqlCommand.Parameters.Add(MyParam2);
                

                // Execute the Query 
                MySqlConnection.Open();
                MySqlCommand.ExecuteNonQuery();
                MySqlConnection.Close();

                ReturnBool = true;
            }
            catch { }

            // return Boolean
            return ReturnBool;
        }
        

        public bool RemoveProductCategory(int ProductID)
        {
            bool ReturnBool = false;
            SqlConnection MySqlConnection = null;
            SqlCommand MySqlCommand = null;

            try
            {
                // Create instance of connection Object
                string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ToString();
                MySqlConnection = new SqlConnection(ConnectionString);

                // Create Instance of  Command Object
                MySqlCommand = new SqlCommand("ZNode_DeleteProductCategoryByProductID", MySqlConnection);

                // Mark the Command as Stored Procedure
                MySqlCommand.CommandType = CommandType.StoredProcedure;
                MySqlCommand.CommandTimeout = 0;

                // Declare parameters 
                SqlParameter MyParam1 = new SqlParameter("@ProductID", SqlDbType.Int);
                MyParam1.Value = ProductID;                

                // Add parameters to Command Object
                MySqlCommand.Parameters.Add(MyParam1);                

                // Execute the Query 
                MySqlConnection.Open();
                MySqlCommand.ExecuteNonQuery();
                MySqlConnection.Close();

                ReturnBool = true;
            }
            catch { }

            // return Boolean
            return ReturnBool;
        }
        #endregion

    }
}

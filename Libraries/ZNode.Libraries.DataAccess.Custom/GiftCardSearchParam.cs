﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Represents the Gift Card Search Parameter.
    /// </summary>
    public class GiftCardSearchParam
    {
        private string _Name = string.Empty;
        private decimal _Balance = 0;
        private int _AccountID = 0;
        private string _CardNumber = string.Empty;
        private DateTime? _ExpiryStartDate = null;
        private DateTime? _ExpiryEndDate = null;
        private bool _DontShowExpired = false;

        /// <summary>
        /// Gets or sets the card dispaly name.
        /// </summary>
        public string Name
        {
            get
            {
                return this._Name;
            }
            
            set
            {
                this._Name = value;
            }
        }

        /// <summary>
        /// Gets or sets the card Balance value.
        /// </summary>
        public decimal Balance
        {
            get
            {
                return this._Balance;
            }
            
            set
            {
                this._Balance = value;
            }
        }

        /// <summary>
        /// Gets or sets the Account ID
        /// </summary>
        public int AccountID
        {
            get { return this._AccountID; }
            set { this._AccountID = value; }
        }

        /// <summary>
        /// Gets or sets the card number
        /// </summary>
        public string CardNumber
        {
            get { return this._CardNumber; }
            set { this._CardNumber = value; }
        }

        /// <summary>
        /// Gets or sets the expiry start date.
        /// </summary>
        public DateTime? ExpiryStartDate
        {
            get { return this._ExpiryStartDate; }
            set { this._ExpiryStartDate = value; }
        }

        /// <summary>
        /// Gets or sets the expiry end date.
        /// </summary>
        public DateTime? ExpiryEndDate
        {
            get { return this._ExpiryEndDate; }
            set { this._ExpiryEndDate = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show or hide the expired gift card.
        /// </summary>
        public bool DontShowExpired
        {
            get { return this._DontShowExpired; }
            set { this._DontShowExpired = value; }
        }
    }
}

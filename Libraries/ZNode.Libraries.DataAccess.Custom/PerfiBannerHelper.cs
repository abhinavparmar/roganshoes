﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Resources;


namespace ZNode.Libraries.DataAccess.Custom
{
    public class PerfiBannerHelper
    {
        #region Private Methods

        /// <summary>
        /// Get Home Page Banner By ID
        /// </summary>
        /// <param name="startDate">DateTime</param>
        /// <param name="enddate">DateTime</param>
        /// <param name="bannerID">int</param>
        /// <returns>datatable</returns>
        private DataTable GetHomePageBannerByID(DateTime startDate, DateTime enddate, int bannerID,int portalID)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Get_PerfiHomePageBanner_ByDateAndID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@BannerID", bannerID);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalID);
                adapter.SelectCommand.Parameters.AddWithValue("@StartDate", startDate);
                adapter.SelectCommand.Parameters.AddWithValue("@EndDate", enddate);


                // Create and fill the dataset
                DataTable dtBanner = new DataTable();
                connection.Open();
                adapter.Fill(dtBanner);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datatable.
                return dtBanner;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// validate Banner if Banner is already Exist with dates
        /// </summary>
        /// <param name="startdate">DateTime</param>
        /// <param name="enddate">DateTime</param>
        /// <param name="bannerID">int</param>
        /// <returns>bool</returns>
        public bool ValidateBanner(DateTime startdate, DateTime enddate, int bannerID,int portalID)
        {
            bool isBannerValid = true;
            DataTable dtBannerList = GetHomePageBannerByID(startdate, enddate, bannerID, portalID);
            if (dtBannerList!=null && dtBannerList.Rows != null && dtBannerList.Rows.Count > 0)
            {
                isBannerValid = false;
            }
            return isBannerValid;
        }


        /// <summary>
        /// Get Home Page Banner By Date and Portal id
        /// </summary>
        /// <param name="portalId"></param>
        /// <returns>datatable</returns>
        public DataTable GetHomePageBannerByDate(int portalId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Get_PerfiHomePageBanner_ByDate", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@StartDate", System.DateTime.Today.Date);


                // Create and fill the dataset
                DataTable dtBanner = new DataTable();
                connection.Open();
                adapter.Fill(dtBanner);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datatable.
                return dtBanner;
            }
        }

        #endregion

    }
}

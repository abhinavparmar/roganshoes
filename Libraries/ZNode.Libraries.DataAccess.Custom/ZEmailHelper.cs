﻿using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;


namespace ZNode.Libraries.DataAccess.Custom
{
    public class ZEmailHelper
    {
        #region Methods
        /// <summary>
        /// Email Header Content
        /// </summary>
        /// <param name="domainPath"></param>
        /// <param name="portalId"></param>
        /// <param name="spacerImage"></param>
        /// <returns></returns>
        public string EmailHeaderContent(string domainPath, int portalId, string spacerImage)
        {
            string headerContent = GenerateHeaderContent(domainPath, portalId , spacerImage);
            return headerContent;
        }

        /// <summary>
        /// To generate header of Email Template
        /// </summary>
        /// <param name="domainPath"></param>
        /// <param name="portalId"></param>
        /// <param name="spacerImage"></param>
        /// <returns></returns>
        private string GenerateHeaderContent(string domainPath, int portalId,string spacerImage)
        {
            string defaultHtmlTemplatePath = string.Empty;
            string htmlTemplatePath = string.Empty;
            string rogansShoesImageLog = domainPath + ZNodeConfigManager.SiteConfig.LogoPath.TrimStart('~');

            int logoWidth = 278;
            int logoHeight = 32;
          
            //if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogoWidth"]))
            //{
            //    logoWidth = Convert.ToInt32(ConfigurationManager.AppSettings["LogoWidth"]);
            //}

            //if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogoHeight"]))
            //{
            //    logoHeight = Convert.ToInt32(ConfigurationManager.AppSettings["LogoHeight"]);
            //}

            // TemplatePath

            defaultHtmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZHeaderContent.html");

            // modified receipt file selection as we are using in-line styles for displaying template on mail content.
            if (portalId != 0)
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZHeaderContent_" + portalId + ".html");
            }
            else
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZHeaderContent.html");
            }
            // Check the receipt template exists, if not exists then set default receipt template
            FileInfo fileinfo = new FileInfo(htmlTemplatePath);

            if (!fileinfo.Exists)
            {
                htmlTemplatePath = defaultHtmlTemplatePath;
            }

            string customerCareEmail = !string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.CustomerServiceEmail) ? ZNodeConfigManager.SiteConfig.CustomerServiceEmail : string.Empty;
            string customerCarePhneNo = !string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber) ? ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber : string.Empty;
            StreamReader streamReader = new StreamReader(htmlTemplatePath);
            string messageText = streamReader.ReadToEnd();

            //Header Section
            Regex rxRoganShoesLogo = new Regex("<#RoganShoesLogo#>", RegexOptions.IgnoreCase);
            messageText = rxRoganShoesLogo.Replace(messageText, rogansShoesImageLog);

            Regex rxRoganShoesSpacerImage = new Regex("<#SpacerImage#>", RegexOptions.IgnoreCase);
            messageText = rxRoganShoesSpacerImage.Replace(messageText, spacerImage);

            Regex rxCustomerCarePhoneNo = new Regex("#CustomerCarePhoneNo#", RegexOptions.IgnoreCase);
            messageText = rxCustomerCarePhoneNo.Replace(messageText, customerCarePhneNo);

            Regex rxMailToCustomerCareEmailId = new Regex("#MailToCustomerCareEmailId#", RegexOptions.IgnoreCase);
            messageText = rxMailToCustomerCareEmailId.Replace(messageText, "mailto:" + customerCareEmail);

            Regex rxCustomerCareEmailId = new Regex("#CustomerCareEmailId#", RegexOptions.IgnoreCase);
            messageText = rxCustomerCareEmailId.Replace(messageText, customerCareEmail);

            Regex rxLogoWidth = new Regex("#LogoWidth#", RegexOptions.IgnoreCase);
            messageText = rxLogoWidth.Replace(messageText, logoWidth.ToString());

            Regex rxLogoHeight = new Regex("#LogoHeight#", RegexOptions.IgnoreCase);
            messageText = rxLogoHeight.Replace(messageText, logoHeight.ToString());

            return messageText;

        }

        /// <summary>
        /// Footer Email Template
        /// </summary>
        /// <param name="domainPath"></param>
        /// <param name="portalId"></param>
        /// <param name="copyRightText"></param>
        /// <param name="spacerImage"></param>
        /// <returns></returns>
        public string EmailFooterContent(string domainPath, int portalId, string copyRightText, string spacerImage)
        {
            string footerContent = GenerateFooterTemplate(domainPath, portalId, copyRightText , spacerImage);
            return footerContent;
        }

        /// <summary>
        /// To Generate footer of Email Template
        /// </summary>
        /// <param name="domainPath"></param>
        /// <param name="portalId"></param>
        /// <param name="copyRightText"></param>
        /// <param name="spacerImage"></param>
        /// <returns></returns>
        private string GenerateFooterTemplate(string domainPath, int portalId, string copyRightText, string spacerImage)
        {
            string defaultHtmlTemplatePath = string.Empty;
            string htmlTemplatePath = string.Empty;

            // TemplatePath
            defaultHtmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZFooterContent.html");

            // modified receipt file selection as we are using in-line styles for displaying template on mail content.
            if (portalId != 0)
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZFooterContent_" + portalId + ".html");
            }
            else
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZFooterContent.html");
            }
            // Check the receipt template exists, if not exists then set default receipt template
            FileInfo fileinfo = new FileInfo(htmlTemplatePath);

            if (!fileinfo.Exists)
            {
                htmlTemplatePath = defaultHtmlTemplatePath;
            }

            StreamReader streamReader = new StreamReader(htmlTemplatePath);
            string messageText = streamReader.ReadToEnd();

            Regex rxCopyRightText = new Regex("#CopyRightText#", RegexOptions.IgnoreCase);
            messageText = rxCopyRightText.Replace(messageText, copyRightText);

            Regex rxRoganShoesSpacerImage = new Regex("<#SpacerImage#>", RegexOptions.IgnoreCase);
            messageText = rxRoganShoesSpacerImage.Replace(messageText, spacerImage);

            return messageText;
        }
        #endregion
    }
}

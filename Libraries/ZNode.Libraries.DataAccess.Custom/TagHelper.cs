using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class TagHelper
    {
        /// <summary>
        /// Search the tag group.
        /// </summary>
        /// <param name="name">Tag group name to search.</param>
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <returns>Returns the tag group search result dataset.</returns>
        public DataSet GetTagGroupBySearchData(string name, int catalogId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchTagGroup", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogID", catalogId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();

                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the categories data by tag group Id.
        /// </summary>
        /// <param name="tagGroupId">Tag group Id to get the categories.</param>
        /// <param name="catalogId">Catalog id to get the categories.</param>
        /// <param name="isTagGroup">Indicates whether this is tag group or not.</param>
        /// <returns>Returns the categories dataset.</returns>
        public DataSet GetCategoriesByTagGroupID(int tagGroupId, int catalogId, bool isTagGroup)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCategoryByTagGroupID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@TagGroupId", tagGroupId);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogId", catalogId);
                adapter.SelectCommand.Parameters.AddWithValue("@IsTagGroup", isTagGroup);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Delete the categories by tag group Id.
        /// </summary>
        /// <param name="tagGroupId">Tag group Id to delete the categories.</param>
        public void DeleteCategoriesByTagGroupID(int tagGroupId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_DeleteTagGroupCategoryByTagGroupID", connection);

                // Add Parameters to SPROC
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 0;

                command.Parameters.AddWithValue("@TagGroupId", tagGroupId);

                connection.Open();

                int rowsAffected = command.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Get the locale by associated tags.
        /// </summary>
        /// <returns>Returns the locale dataset</returns>
        public DataSet GetLocaleByAssociatedTags()
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetLocaleByAssociatedTags", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;


                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }
}

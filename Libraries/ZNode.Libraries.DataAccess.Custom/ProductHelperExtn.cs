﻿using System.Data;
using System.Data.SqlClient;
namespace ZNode.Libraries.DataAccess.Custom
{
    public partial class ProductHelper
    {
        #region Public Enum
        /// <summary>
        /// Mapping ids in DB
        /// </summary>
        public enum ProductCrossSellTypeEnum
        {
            CrossSells = 1,
            UpSells = 2,
            RelatedProducts = 3
        }
        #endregion

        /// <summary>
        /// Get the Home Page Specials as XML
        /// </summary>
        /// <param name="portalId">Portal Id to get the list of home page special product</param>
        /// <param name="localeId">Locale Id of the product.</param>
        /// <param name="displayItem">Select N number of item.</param>
        /// <returns>Returns the home page special products XML.</returns>
        public string GetHomePageNewArrivalsXML(int portalId, int localeId, int displayItem, int profileId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("Zeon_GetNewArrivals_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@DisplayItem", displayItem);
                    command.Parameters.AddWithValue("@ProfileId", profileId);
                    command.Parameters.AddWithValue("@Categories", System.Configuration.ConfigurationManager.AppSettings["NewArrivalCategories"].ToString());
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get product list
        /// </summary>
        /// <param name="ProductIDList"></param>
        /// <returns></returns>
        public string GetProductListByProductIDList(string ProductIDList, string upcSpecificationText, string alternatePartNumberSpecificationText)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("Zeon_GetProductComparisonList_ByProductIds_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductIdList", ProductIDList);
                    command.Parameters.AddWithValue("@UpcSpecificationText", upcSpecificationText);
                    command.Parameters.AddWithValue("@AlternatePartNumberSpecificationText", alternatePartNumberSpecificationText);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }


        /// <summary>
        /// Get products by products Ids Sort By.
        /// </summary>
        /// <param name="productIds">Comma seperated product Ids.</param>        
        /// <returns>Returns the product details in XML format.</returns>
        public string GetProductsByIdsExtnSortBy(string productIds, int categoryID = 0)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                //SqlCommand command = new SqlCommand("ZNode_GetProductsByIds", connection);

                SqlCommand command = new SqlCommand("ZNode_GetProductsByIds_Extn_SortBy", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;

                // Add Parameters to SPROC
                SqlParameter parameterProductList = new SqlParameter("@ProductIds", productIds);
                command.Parameters.Add(parameterProductList);

                SqlParameter parameterPortalID = new SqlParameter("@PortalID", ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID);
                command.Parameters.Add(parameterPortalID);

                SqlParameter parameterCategoryID = new SqlParameter("@CategoryID", categoryID);
                command.Parameters.Add(parameterCategoryID);

                // Execute the command
                connection.Open();

                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                while (reader.Read())
                {
                    xmlOut.Append(reader[0].ToString());
                }

                reader.Close();
                connection.Close();

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get all product attribute details for a product Id.
        /// </summary>
        /// <param name="productId">Get product attribute details for the product.</param>
        /// <returns>Returns the product attribute  details dataset.</returns>
        public DataTable GetProductAttributesByID(int productId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetProductAttributesByID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductID", productId);

                // Create and fill the dataset
                DataTable datatab = new DataTable();
                connection.Open();
                adapter.Fill(datatab);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return datatab;
            }
        }

        #region Public Extension Methods - Related to Get Product & Best Seller List

        /// <summary>
        /// Get products by products Ids.
        /// </summary>
        /// <param name="productIds">Comma seperated product Ids.</param>        
        /// <returns>Returns the product details in XML format.</returns>
        public string GetProductsByIdsExtn(string productIds, int categoryID = 0)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                //SqlCommand command = new SqlCommand("ZNode_GetProductsByIds", connection);

                SqlCommand command = new SqlCommand("ZNode_GetProductsByIds_Extn", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;

                // Add Parameters to SPROC
                SqlParameter parameterProductList = new SqlParameter("@ProductIds", productIds);
                command.Parameters.Add(parameterProductList);

                SqlParameter parameterPortalID = new SqlParameter("@PortalID", ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID);
                command.Parameters.Add(parameterPortalID);


                SqlParameter parameterCategoryID = new SqlParameter("@CategoryID", categoryID);
                command.Parameters.Add(parameterCategoryID);

                // Execute the command
                connection.Open();

                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                while (reader.Read())
                {
                    xmlOut.Append(reader[0].ToString());
                }

                reader.Close();
                connection.Close();

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get the most purchased product as XML.
        /// </summary>
        /// <param name="catalogId">Catalog Id to check.</param>
        /// <param name="displayItem">Number of items to retrieve.</param>
        /// <param name="categoryId">Category Id to check.</param>
        /// <param name="portalId">Portal Id to check.</param>
        /// <returns>Returns the bestseller items XML.</returns>
        public string GetBestSellerItemsExtn(int catalogId, int displayItem, int categoryId, int portalId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_SearchBestSellerListExtn", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 0;

                command.Parameters.AddWithValue("@CatalogID", catalogId);
                command.Parameters.AddWithValue("@DisplayItem", displayItem);
                command.Parameters.AddWithValue("@CategoryId", categoryId);
                command.Parameters.AddWithValue("@PortalId", portalId);

                // Execute the command
                connection.Open();
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                while (reader.Read())
                {
                    xmlOut.Append(reader[0].ToString());
                }

                connection.Close();

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get product data as XML for serialization
        /// </summary>
        /// <param name="productId">Product Id to get the product dataset.</param>
        /// <param name="portalId">Portal Id to get the product dataset.</param>
        /// <param name="localeId">Locale Id of the product.</param>
        /// <returns>Returns the product details dataset.</returns>
        public string GetProductXMLExtn(int productId, int portalId, int localeId, int profileId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNode_GetProductByProductID_XML_Extn", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@profileId", profileId);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                // return XML string
                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get the Products for a brand
        /// </summary>
        /// <param name="portalId">Portal Id to get the products by brand.</param>
        /// <param name="manufacturerId">Manufacturer Id of the products.</param>
        /// <param name="localeId">Locale Id of the product.</param>        
        /// <returns>Returns the product details by product XML.</returns>
        public string GetProductsXMLByBrandID(int portalId, int manufacturerId, int localeId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNode_GetProductsByBrand_XML_Extn", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@ManufacturerID", manufacturerId);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        #endregion

        /// <summary>
        /// Gets sku by product attribute.
        /// </summary>
        /// <param name="productId">Product Id to get the Sku attribute</param>
        /// <param name="attributes">Attributes to get the product sku.</param>
        /// <returns>Returns the sku by product attributes.</returns>
        public string GetSKUByAttributesExtn(int productId, string attributes)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNode_GetSKUByAttributes_XMLExtn", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@Attributes", attributes);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get SKU Images By ProductID
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public DataTable GetSKUImagesByProductID(int productId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetSKUImagesByProductID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductID", productId);

                // Create and fill the dataset
                DataTable datatab = new DataTable();
                connection.Open();
                adapter.Fill(datatab);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return datatab;
            }
        }

        /// <summary>
        /// Get Product of Compare Product List
        /// </summary>
        /// <param name="CommaSeperatedProductID"></param>
        /// <returns></returns>
        public DataTable GetCompareProductListByProductID(string CommaSeperatedProductID)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetCompareProductListByProductID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductIdList", CommaSeperatedProductID);

                // Create and fill the dataset
                DataTable datatab = new DataTable();
                connection.Open();
                adapter.Fill(datatab);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return datatab;
            }
        }


        /// <summary>
        /// Get product data as XML for serialization
        /// </summary>
        /// <param name="productId">Product Id to get the product dataset.</param>
        /// <param name="portalId">Portal Id to get the product dataset.</param>
        /// <param name="localeId">Locale Id of the product.</param>
        /// <returns>Returns the product details dataset.</returns>
        public string GetProductDetailsXML(int productId, int portalId, int localeId, int profileId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("Perfi_GetOnlyProductDetailsByID_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@profileId", profileId);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                // return XML string
                return xmlOut.ToString();
            }
        }

    }
}

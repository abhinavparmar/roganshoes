﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ZNode.Libraries.DataAccess.Entities;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class ZStateHelper
    {
       /// <summary>
       /// Get list of States by Country Code
       /// </summary>
       /// <param name="countryCode">string</param>
        /// <returns>TList<State></returns>
        public DataTable GetStateListByCountryCode(string countryCode)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetAllStateByCountryCode", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@CountryCode", countryCode);

                // Create and fill the dataset
                DataTable dtStates = new DataTable();
                connection.Open();
                adapter.Fill(dtStates);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dtStates;
            }
        }
    }
}

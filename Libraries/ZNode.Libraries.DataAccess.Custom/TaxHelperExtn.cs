﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.DataAccess.Custom
{
    public partial class TaxHelper
    {

        /// <summary>
        /// Get the active tax rules by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the active tax rules.</param>
        /// <param name="countryCode">Country code to get the active tax rules.</param>
        /// <returns>Returns the active tax rules.</returns>
        public DataTable GetActiveTaxRulesByPortalId(int portalId, string countryCode, string stateCode, string taxClassIds)
        {
            // Create Instance for Connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create Instance for Adaptor Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetActiveTaxRulesExtn", connection);

                // Mark the Select command as Stored procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@CountryCode", countryCode);
                adapter.SelectCommand.Parameters.AddWithValue("@StateCode", stateCode);
                adapter.SelectCommand.Parameters.AddWithValue("@TaxClassIDs", taxClassIds);

                // Execute the Command
                connection.Open();

                // Create Datatable
                DataTable dt = new DataTable();

                // Fill the datatable
                adapter.Fill(dt);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                return dt;
            }
        }


    }
}

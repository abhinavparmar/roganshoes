﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace ZNode.Libraries.DataAccess.Custom
{
    public class AddOnHelper
    {
        public string GetAddOnIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetAddOnByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);
                adapter.SelectCommand.CommandTimeout = 0;

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string addOnId = null;

                if (reader.HasRows)
                {
                    addOnId = reader[0].ToString();
                }

                connection.Close();

                return addOnId;
            }
        }

        public string GetAddOnValueIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetAddOnValueByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);
                adapter.SelectCommand.CommandTimeout = 0;

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string addOnValueId = null;

                if (reader.HasRows)
                {
                    addOnValueId = reader[0].ToString();
                }

                connection.Close();

                return addOnValueId;
            }
        }

        public List<string> GetAddOnIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var addOnIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }                    
                }
                adapter.SelectCommand.CommandTimeout = 0;
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    addOnIds.Add(reader[0].ToString());
                }

                connection.Close();

                return addOnIds;
            }
        }

        public List<string> GetAddOnValueIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var addOnValueIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                adapter.SelectCommand.CommandTimeout = 0;
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    addOnValueIds.Add(reader[0].ToString());
                }

                connection.Close();

                return addOnValueIds;
            }
        }
    }
}

using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Manages the methods related to seo friendly urls of product, cateory and content pages
    /// </summary>
    public partial class SEOUrlHelper
    {
        #region Public Methods
        /// <summary>
        /// Returns SEO friendly url list by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the SEO url.</param>
        /// <returns>Returns the SEO friendly url dataset for the store.</returns>
        public DataSet GetSEOUrlListByPortalID(int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetSEOUrlList", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        #endregion
    }
}
 
﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace ZNode.Libraries.DataAccess.Custom
{
    public partial class DataManagerHelper
    {
        /// <summary>
        /// Gets the download tracking Data
        /// </summary>
        /// <param name="startDate">Get the tracking date from this date.</param>
        /// <param name="endDate">Get the tracking date to this date.</param>
        /// <returns>Returns the tracking data.</returns>
        public void ExecuteSQLAgentJob(string jobName)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                //SqlDataAdapter adapter = new SqlDataAdapter("Zeon_ExecuteSQLJOB", connection);
                SqlCommand cmd = new SqlCommand("Zeon_ExecuteSQLJOB", connection);

                // Mark the command as store procedure
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@JobName", jobName);

                // Create and fill the dataset
                connection.Open();
                cmd.ExecuteNonQuery();
                // Release the resources
                connection.Close();

            }
        }
    }
}

﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace ZNode.Libraries.DataAccess.Custom
{
   public partial class ProductCrossSellHelper
    {
        /// <summary>
        /// Get  the Cross Sell Product by ProductID
        /// </summary>
        /// <param name="ProductID">ProductID</param>
        /// <returns></returns>
        public DataSet GetCrossSellProductsByProductID(int ProductID)
        {
            // Create Instance of Connection  Object   
            using (SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create Instance of Command Object
                SqlDataAdapter mySqlAdapter = new SqlDataAdapter("Zeon_GetCrossSellProductsByProductID", myConnection);

                // Mark the Command as a SPROC
                mySqlAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySqlAdapter.SelectCommand.CommandTimeout = 0;

                // Add Parameters to SPROC
                SqlParameter MyParam = new SqlParameter("@ProductId", SqlDbType.Int);
                MyParam.Value = ProductID;
                mySqlAdapter.SelectCommand.Parameters.Add(MyParam);

                // Create and Fill the DataSet
                DataSet myDataSet = new DataSet();
                myConnection.Open();
                mySqlAdapter.Fill(myDataSet);

                //Release Resources
                mySqlAdapter.Dispose();
                myConnection.Close();

                //Return DataSet
                return myDataSet;
            }
        }
        
    }
}

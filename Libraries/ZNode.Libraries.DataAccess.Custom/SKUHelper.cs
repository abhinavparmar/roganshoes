using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Product SKU helper related functions
    /// </summary>
    public class SKUHelper
    {
        #region Public Methods

        /// <summary>
        /// Check the Sku Id match.
        /// </summary>
        /// <param name="productId">Product Id to check.</param>
        /// <param name="skuId">Sku Id to check.</param>
        /// <param name="selectAttributes">Selected attributes to check.</param>
        /// <returns>Returns the sku attribute dataset.</returns>
        public bool GetSKUAttributes(int productId, int skuId, string selectAttributes)
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_Getskuattributecombo_ByProductid", connection);

                DataSet dataSet = new DataSet();

                // Mark  Command as Stored Procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@Attributes", selectAttributes);

                // Fills the data.
                adapter.Fill(dataSet);

                connection.Close();

                // Row count
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    if (skuId > 0)
                    {
                        int value = (int)dataSet.Tables[0].Rows[0]["SKUID"];
                        if (skuId == value)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }

                    return true;
                }

                return false;
            }
        }
     
        /// <summary>
        /// Get the sku for the collection of attributes
        /// </summary>
        /// <param name="productId">Product Id to get the sku attribute.</param>
        /// <param name="selectAttributes">Selected attributes.</param>
        /// <returns>Returns the sku attribute dataset.</returns>
        public DataSet GetSkubyAttributes(int productId, string selectAttributes)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetSKUAttributeCombo_ByProductID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@Attributes", selectAttributes);
                
                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get all skus dataset.
        /// </summary>
        /// <returns>Returns the sku dataset.</returns>
        public DataSet GetAllSKU()
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAllSku", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get all sku inventory.
        /// </summary>
        /// <returns>Returns the sku inventory dataset.</returns>
        public DataSet GetAllSKUInventory()
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAllSkuInventory", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get all sku inventory details by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the sku inventory dataset.</param>
        /// <returns>Returns the sku inventory dataset.</returns>
        public DataSet GetSKUInventoryByPortalID(string portalId)
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetSkuInventoryByPortalID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the sku details by catalog Id.
        /// </summary>
        /// <param name="catalogId">Catalog Id to get the sku details.</param>        
        /// <returns>Returns the sku details catalog dataset.</returns>
        public DataSet GetSkuByCatalogID(int catalogId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetSkuByCatalogID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogId", catalogId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the sku details by catalog Id.
        /// </summary>
        /// <param name="catalogId">Catalog Id to get the sku details.</param>        
        /// <returns>Returns the sku details catalog dataset.</returns>
        public DataSet GetSkuProfileBySkuID(int skuId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetSKUProfileBySKUID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@SKUID", skuId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the sku details by catalog Id.
        /// </summary>
        /// <param name="catalogId">Catalog Id to get the sku details.</param>        
        /// <returns>Returns the sku details catalog dataset.</returns>
        public DataSet GetSkuProfileEffectiveBySkuID(int skuId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetSKUProfileEffectiveBySKUID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@SKUID", skuId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
       
        #endregion

        public string GetSkuIdBySkuProductNum(string sku, string productNum)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetSKUIDBySKUProductNum", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@SKU", sku);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductNum", productNum);
                adapter.SelectCommand.CommandTimeout = 0;

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();

                if (!reader.HasRows)
                {
                    return null;
                }

                var skuId = reader[0].ToString();

                connection.Close();

                return skuId;
            }
        }

        public List<string> GetInventoryByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var skus = new List<string>();

                var adapter = new SqlDataAdapter(query, connection);

                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                adapter.SelectCommand.CommandTimeout = 0;

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    skus.Add(reader[0].ToString());
                }

                connection.Close();

                return skus;
            }
        }

        #region Public Methods related to Locale
        /// <summary>
        /// Get Locale information by sku id, if it has different language.
        /// </summary>
        /// <param name="skuId">Sku Id to get the locale Ids.</param>
        /// <param name="portalId">Portal Id to get the locale Ids.</param>
        /// <returns>Returns the locale Ids dataset.</returns>
        public DataSet GetLocaleIdsBySkuId(int skuId, int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetLocaleListBySkuId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@SkuId", skuId);                
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        #endregion
    }
}

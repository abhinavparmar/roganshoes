﻿using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public partial class StoreLocatorHelper
    {
        /// <summary>
        /// Get the list of SKU availability as per the zipcode and radius specified
        /// </summary>
        /// <param name="zipcode"></param>
        /// <param name="radius"></param>
        /// <param name="skuID"></param>
        /// <param name="portalID"></param>
        /// <returns>Details of the SKU store and availability</returns>
        public DataTable GetSKUAvailabilityPerZipCode(int storeID, int skuID, int portalID)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetInStoreAvailability_Update", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@StoreID", storeID);
                adapter.SelectCommand.Parameters.AddWithValue("@SkuID", skuID);
                adapter.SelectCommand.Parameters.AddWithValue("@PORTALID", portalID);

                // Create and fill the dataset
                DataTable dataTable = new DataTable();
                connection.Open();
                adapter.Fill(dataTable);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datatable.
                return dataTable;
            }
        }
    }
}

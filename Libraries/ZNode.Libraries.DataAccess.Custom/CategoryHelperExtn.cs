﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.DataAccess.Custom
{
    public partial class CategoryHelper
    {
        /// <summary>
        /// Gets the Navigation Menus
        /// </summary>
        /// <param name="portalId">int Portal ID</param>
        /// <param name="localeId">int Locale ID</param>
        /// <param name="profileId">int Profile ID</param>
        /// <param name="removeCategoryList">List of categories not needed</param>
        /// <returns>Completed details of menu items</returns>
        public DataSet GetNavigationItemsExtn(int portalId, int localeId, int profileId, string removeCategoryList)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetShoppingCartNavigationItems_Extn", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", localeId);
                adapter.SelectCommand.Parameters.AddWithValue("@ProfileId", profileId);
                adapter.SelectCommand.Parameters.AddWithValue("@RemoveCategories", removeCategoryList);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Gets the Seo URL for the New Arrival Product Categories
        /// </summary>
        /// <param name="portalId">int PortalID</param>
        /// <param name="localeId">int LocalID</param>
        /// <param name="categoryList">string Category List</param>
        /// <returns>returns the SEO Url</returns>
        public string GetCategorySEOUrl(int portalId, int localeId, string categoryList)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetSEOUrlForNewArrivalsCategory", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleID", localeId);
                adapter.SelectCommand.Parameters.AddWithValue("@Categories", categoryList);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                if (dataSet != null && dataSet.Tables != null && dataSet.Tables[0] != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    string seoUrl = string.Empty;
                    if (dataSet.Tables[0].Rows[0]["SEOUrl"] != null && !string.IsNullOrEmpty(dataSet.Tables[0].Rows[0]["SEOUrl"].ToString()))
                    {
                        seoUrl = "~/" + dataSet.Tables[0].Rows[0]["SEOUrl"].ToString();
                    }
                    else if (dataSet.Tables[0].Rows[0]["CatID"] != null && !string.IsNullOrEmpty(dataSet.Tables[0].Rows[0]["CatID"].ToString()))
                    {
                        seoUrl = "~/category.aspx?zcid=" + dataSet.Tables[0].Rows[0]["CatID"].ToString();
                    }
                    else
                    {
                        seoUrl = string.Empty;
                    }

                    return seoUrl;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        ///  Gets the CategoryIDs By ProductID
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <param name="categoryList"></param>
        /// <returns></returns>
        public static string GetCategoryIDsByProductID(int portalID, int productID)
        {
            string categoryIDs = string.Empty;
            try
            {
                // Create instance of connection     
                using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
                {
                    // Create instance of command object
                    SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetCategoryIDsByProductID", connection);

                    // Mark the command as store procedure
                    adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                    adapter.SelectCommand.CommandTimeout = 0;

                    adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalID);

                    adapter.SelectCommand.Parameters.AddWithValue("@ProductID", productID);

                    // Create and fill the dataset
                    DataSet dataSet = new DataSet();
                    connection.Open();
                    adapter.Fill(dataSet);

                    // Release the resources
                    adapter.Dispose();
                    connection.Close();

                    if (dataSet != null && dataSet.Tables != null && dataSet.Tables[0] != null && dataSet.Tables[0].Rows.Count > 0)
                    {
                        string seoUrl = string.Empty;
                        if (dataSet.Tables[0].Rows[0]["CategoryIdList"] != null && !string.IsNullOrEmpty(dataSet.Tables[0].Rows[0]["CategoryIdList"].ToString()))
                        {
                            categoryIDs = dataSet.Tables[0].Rows[0]["CategoryIdList"].ToString();
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return categoryIDs;

        }

        /// <summary>
        /// Get all Manufacturer with its initial alphabets
        /// </summary>
        /// <returns></returns>
        public DataTable GetBrandCategoryListByPortal(int portalID, int localeID, string categoryName)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter command = new SqlDataAdapter("Zeon_GetAllShopByBrandCategory", connection);

                // Mark the command as store procedure
                command.SelectCommand.CommandType = CommandType.StoredProcedure;
                command.SelectCommand.CommandTimeout = 0;

                SqlParameter parameter = new SqlParameter("@portalId", SqlDbType.Int);
                parameter.Value = portalID;
                command.SelectCommand.Parameters.Add(parameter);

                SqlParameter parameterLocaleId = new SqlParameter("@LocaleID", SqlDbType.Int);
                parameterLocaleId.Value = localeID;
                command.SelectCommand.Parameters.Add(parameterLocaleId);

                SqlParameter parameterMaxCount = new SqlParameter("@CategoryName", SqlDbType.NVarChar);
                parameterMaxCount.Value = categoryName;
                command.SelectCommand.Parameters.Add(parameterMaxCount);

                // Create and Fill the datatable
                DataTable myDatatable = new DataTable();
                command.Fill(myDatatable);

                command.Dispose();

                // close connection
                connection.Close();

                // Return the datadet.
                return myDatatable;
            }
        }

        /// <summary>
        ///  Get Products for All Shop By Brand Category in XML formart
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <param name="manufacturerID"></param>
        /// <param name="recordMaxCount"></param>
        /// <returns></returns>
        public string GetTopReviewedProductList(int portalId, int localeId, int categoryID, int recordMaxCount)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("Zeon_GetTopReviewedProductByCategory", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalId", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@CategoryID", categoryID);
                    command.Parameters.AddWithValue("@TopCount", recordMaxCount);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }


        /// <summary>
        /// Gets Category List For Sitemap
        /// </summary>
        /// <param name="portalId">int Portal ID</param>
        /// <param name="localeId">int Locale ID</param>
        /// <param name="profileId">int Profile ID</param>
        /// <param name="removeCategoryList">List of categories not needed</param>
        /// <returns>Completed details of menu items</returns>
        public DataSet GetSitemapCategoryList(int portalId, int localeId, int profileId, string removeCategoryList)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Perficient_Get_Sitemap_CategoryList ", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", localeId);
                adapter.SelectCommand.Parameters.AddWithValue("@ProfileId", profileId);
                adapter.SelectCommand.Parameters.AddWithValue("@RemoveCategories", removeCategoryList);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }
}

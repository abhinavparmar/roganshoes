﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ZNode.Libraries.DataAccess.Entities;
using System.Configuration;

namespace ZNode.Libraries.DataAccess.Custom
{
    public partial class ManufacturerHelper
    {
        /// <summary>
        /// Get all Manufacturer with its initial alphabets
        /// </summary>
        /// <returns></returns>
        public DataTable GetActiveBrandsByPortal(int portalID,int localeID, int topCount)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter command = new SqlDataAdapter("Zeon_GetAllManufacturer", connection);
                string categoryName = ConfigurationManager.AppSettings["ShopByBrandCategory"] != null ? ConfigurationManager.AppSettings["ShopByBrandCategory"].ToString() : "Shop by Brand";
                // Mark the command as store procedure
                command.SelectCommand.CommandType = CommandType.StoredProcedure;
                command.SelectCommand.CommandTimeout = 0;

                SqlParameter parameter = new SqlParameter("@portalId", SqlDbType.Int);
                parameter.Value = portalID;
                command.SelectCommand.Parameters.Add(parameter);

                SqlParameter parameterLocaleId = new SqlParameter("@LocaleID", SqlDbType.Int);
                parameterLocaleId.Value = localeID;
                command.SelectCommand.Parameters.Add(parameterLocaleId);

                SqlParameter parameterMaxCount = new SqlParameter("@MaxBrandsCount", SqlDbType.Int);
                parameterMaxCount.Value = topCount;
                command.SelectCommand.Parameters.Add(parameterMaxCount);

                SqlParameter parameterCategoryName = new SqlParameter("@CategoryName", SqlDbType.NVarChar);
                parameterCategoryName.Value = categoryName;
                command.SelectCommand.Parameters.Add(parameterCategoryName);

                // Create and Fill the datatale
                DataTable myDatatable = new DataTable();
                command.Fill(myDatatable);

                command.Dispose();

                // close connection
                connection.Close();

                // Return the datadet.
                return myDatatable;
            }
        }

        /// <summary>
        /// Get all Manufacturer with its initial alphabets
        /// </summary>
        /// <returns></returns>
        public List<ManufacturerExtn> GetBySEOURL(string formattedSeoUrl)
        {
            List<ManufacturerExtn> manufacturerExtn = new List<ManufacturerExtn>();
      
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter command = new SqlDataAdapter("Zeon_NT_ZNodeManufacturerExtn_GetBySEOURL", connection);

                // Mark the command as store procedure
                command.SelectCommand.CommandType = CommandType.StoredProcedure;
                command.SelectCommand.CommandTimeout = 0;

                SqlParameter parameter = new SqlParameter("@SEOURL", SqlDbType.NVarChar, 100);
                parameter.Value = formattedSeoUrl;
                command.SelectCommand.Parameters.Add(parameter);

                // Create and Fill the datatable
                DataTable myDatatable = new DataTable();
                command.Fill(myDatatable);

                command.Dispose();

                // close connection
                connection.Close();

                if (myDatatable != null && myDatatable.Rows.Count > 0)
                {
                    //Get ManufacturerExtn Data Table into ManufacturerExtn List Object
                    manufacturerExtn = zConvertDataTableToList.ConvertTo<ManufacturerExtn>(myDatatable);
                }
                return manufacturerExtn;
            }
        }

        /// <summary>
        /// Get Products for All Brands in XML formart
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <returns></returns>
        public string GetBrandWiseProduct(int portalId, int localeId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("Zeon_GetBrandWiseProduct_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }


        /// <summary>
        /// Get Products for All Brands in XML formart
        /// </summary>
        /// <param name="portalId">int</param>
        /// <param name="localeId">int</param>
        /// <returns>DataTable</returns>
        public string GetBrandsTopReviewedProductList(int portalId, int localeId, int manufacturerID,int recordMaxCount)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("Zeon_GetTopReviewedProductByBrand", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalId", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@ManufacturerId", manufacturerID);
                    command.Parameters.AddWithValue("@TopCount", recordMaxCount);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

    }
}

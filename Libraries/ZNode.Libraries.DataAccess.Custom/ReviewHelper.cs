﻿using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Manage the customer review.
    /// </summary>
    public class ReviewHelper
    {
        /// <summary>
        /// Search the customer review based on the search criteria.
        /// </summary>
        /// <param name="reviewTitle">Review title text.</param>
        /// <param name="nickName">Customer name</param>
        /// <param name="productName">Product name.</param>
        /// <param name="status">Status of the customer review.</param>
        /// <returns>Returns the customer review DataSet.</returns>
        public DataSet SearchReview(string reviewTitle, string nickName, string productName, string status)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("Znode_SearchReview", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                // Set the parameter value
                adapter.SelectCommand.Parameters.AddWithValue("@ReviewTitle", reviewTitle);
                adapter.SelectCommand.Parameters.AddWithValue("@Nickname", nickName);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductName", productName);
                adapter.SelectCommand.Parameters.AddWithValue("@Status", status);

                // Create and fill the dataset
                DataSet ds = new DataSet();
                connection.Open();
                
                adapter.Fill(ds, "ZnodeReview");

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return ds;
            }
        }
    }
}

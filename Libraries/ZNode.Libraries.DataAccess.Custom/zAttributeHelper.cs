﻿using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class zAttributeHelper
    {
        /// <summary>
        /// Get the attribute for this catalog Id.
        /// </summary>
        /// <param name="CatalogId">Catalog Id to get the attribute type dataset.</param>
        /// <returns>Returns the attribute dataset.</returns>
        public DataSet GetNextLevelAttriutes(int productID, string attributeIDs, int nextLevelAttributeTypeId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetNextLevelAttriutes", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.CommandTimeout = 0;

                adapter.SelectCommand.Parameters.AddWithValue("@ProductID", productID);
                adapter.SelectCommand.Parameters.AddWithValue("@AttributeIDs", attributeIDs);
                adapter.SelectCommand.Parameters.AddWithValue("@AttributeTypeID", nextLevelAttributeTypeId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }


    }
}

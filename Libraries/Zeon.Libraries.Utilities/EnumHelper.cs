﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeon.Libraries.Utilities
{
    public class EnumHelper
    {

        public enum TriggerEmailFunnelStage
        {
            ViewCart = 1,
            Welcome = 2, //Login Page
            ShippingBilling = 3,
            Payment = 4,
            OrderComplete = 5,
        }

    }
}

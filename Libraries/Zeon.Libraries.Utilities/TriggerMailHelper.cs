﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Admin;
using System.Web;
using ZNode.Libraries.ECommerce.Utilities;

namespace Zeon.Libraries.Utilities
{
    public class TriggerMailHelper
    {
        #region[Global Variable]

        StringBuilder sbPixcelImageSrc = new StringBuilder();

        #endregion

        #region[Public Methods]

        /// <summary>
        /// Method to render Pixel tag from Checkout Page
        /// </summary>
        /// <param name="userAccount"></param>
        /// <param name="shoppingCart"></param>
        /// <returns></returns>
        public string AddShoppingCartPagePixcelTag(ZNodeUserAccount userAccount, ZNodeShoppingCart shoppingCart)
        {
            return GenerateCartPixcelTagImages(userAccount, shoppingCart, EnumHelper.TriggerEmailFunnelStage.ViewCart);
        }

        /// <summary>
        /// Method to render Pixel tag from Login Page
        /// </summary>
        /// <param name="userAccount"></param>
        /// <param name="shoppingCart"></param>
        /// <returns></returns>
        public string AddLoginPagePixcelTag(ZNodeUserAccount userAccount, ZNodeShoppingCart shoppingCart)
        {
            return GenerateCartPixcelTagImages(userAccount, shoppingCart, EnumHelper.TriggerEmailFunnelStage.Welcome);
        }

        /// <summary>
        /// Method to render Pixel tag from Checkout Address Page
        /// </summary>
        /// <param name="userAccount"></param>
        /// <param name="shoppingCart"></param>
        /// <returns></returns>
        public string AddCheckoutShippingBillingPagePixcelTag(ZNodeUserAccount userAccount, ZNodeShoppingCart shoppingCart)
        {
            return GenerateCartPixcelTagImages(userAccount, shoppingCart, EnumHelper.TriggerEmailFunnelStage.ShippingBilling);
        }

        /// <summary>
        /// Method to render Pixel tag from Checkout Order Summary 
        /// </summary>
        /// <param name="userAccount"></param>
        /// <param name="shoppingCart"></param>
        /// <returns></returns>
        public string AddCheckoutPaymentPagePixcelTag(ZNodeUserAccount userAccount, ZNodeShoppingCart shoppingCart)
        {
            return GenerateCartPixcelTagImages(userAccount, shoppingCart, EnumHelper.TriggerEmailFunnelStage.Payment);
        }


        /// <summary>
        ///  Method to render Pixel tag from Checkout Order Receipt 
        /// </summary>
        /// <param name="userAccount"></param>
        /// <param name="shoppingCart"></param>
        /// <returns></returns>
        public string AddOrderReceiptPagePixcelTag(ZNodeUserAccount userAccount, ZNodeShoppingCart shoppingCart)
        {
            return GenerateCartPixcelTagImages(userAccount, shoppingCart, EnumHelper.TriggerEmailFunnelStage.OrderComplete);
        }


        #endregion

        #region[Private Methods]

        /// <summary>
        /// Helper Method to Create Trigger Email Pixel Tag and retrun as tring
        /// </summary>
        /// <param name="userAccount"></param>
        /// <param name="shoppingCart"></param>
        /// <param name="funnelStage"></param>
        /// <returns></returns>
        private string GenerateCartPixcelTagImages(ZNodeUserAccount userAccount, ZNodeShoppingCart shoppingCart, EnumHelper.TriggerEmailFunnelStage funnelStage)
        {
            string triggerEmailPixelTag = string.Empty;
            try
            {
                StringBuilder sbImages = new StringBuilder();

                string initialPixelTag = GetMessageConfigValueByKey("TriggerEmailInitialPixelTag");

                if (!string.IsNullOrEmpty(initialPixelTag))
                {
                    if (shoppingCart != null)
                    {
                        triggerEmailPixelTag = string.Empty;


                        //crete funnel stage API call
                        string funnelPixelTagSrc = string.Empty;
                        funnelPixelTagSrc = CreateFunnelStagePixelTagImageSrc(userAccount, shoppingCart, funnelStage, initialPixelTag);
                        sbImages.Append("<img src='" + funnelPixelTagSrc + "' height='0' width='0' border='0'>");

                        //now create product item API calls
                        //15March2011 trigger email suggestion (There is no need to send item data on that OrderComplete funnel stage)
                        if (funnelStage != EnumHelper.TriggerEmailFunnelStage.OrderComplete)
                        {
                            foreach (ZNodeShoppingCartItem shoppingCartItem in shoppingCart.ShoppingCartItems)
                            {
                                string productPixelTagSrc = string.Empty;
                                productPixelTagSrc = CreateProductItemPixelTagImageSrc(userAccount, shoppingCartItem, shoppingCart, funnelStage, initialPixelTag);
                                sbImages.Append("<img src='" + productPixelTagSrc + "' height='0' width='0' border='0'>");
                            }
                        }

                        triggerEmailPixelTag = sbImages.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNumExtn.TriggerEmailFailed, "TriggerEmailFailed Error ", "TriggerEmailFailed", "In Library Zeon.Libraries.Utilities.TriggerMailHelper", "Fail", ex.ToString());
            }
            return triggerEmailPixelTag;
        }

        /// <summary>
        /// Retrun Pixel Tag Base Url From ZNode Message Config 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetMessageConfigValueByKey(string key)
        {
            MessageConfigAdmin messageadmin = new MessageConfigAdmin();
            MessageConfig messageconfig = new MessageConfig();
            messageconfig = messageadmin.GetByKeyPortalIDLocaleID(key, ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID);
            if (messageconfig != null && !string.IsNullOrEmpty(messageconfig.Value))
            {
                return messageconfig.Value;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get the Domain Url
        /// </summary>
        /// <returns></returns>
        private string GetViewCartUrl()
        {
            String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
            String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");

            strUrl = strUrl + "shoppingcart.aspx";

            return strUrl;
        }

        #endregion

        #region[Method :Genrate Trigger Email Pixel Tag:]

        /// <summary>
        /// Genrate Trigger Email Src Tag 
        /// </summary>
        /// <param name="userAccount"></param>
        /// <param name="shoppingCart"></param>
        /// <param name="funnelStage"></param>
        /// <param name="initialPixelTag"></param>
        /// <returns></returns>
        private string CreateFunnelStagePixelTagImageSrc(ZNodeUserAccount userAccount, ZNodeShoppingCart shoppingCart, EnumHelper.TriggerEmailFunnelStage funnelStage, string initialPixelTag)
        {
            sbPixcelImageSrc = new StringBuilder();

            if (!string.IsNullOrEmpty(initialPixelTag))
            {
                //initial src url with base key tags
                sbPixcelImageSrc.Append(initialPixelTag.Trim().TrimEnd('&'));

                //customer email
                string sUserEmail = string.Empty;
                if (userAccount != null)
                {
                    sUserEmail = userAccount.EmailID;
                }
                sbPixcelImageSrc.Append("&e=");
                sbPixcelImageSrc.Append(sUserEmail);

                //empty cart flag
                if (funnelStage == EnumHelper.TriggerEmailFunnelStage.ViewCart ||
                    funnelStage == EnumHelper.TriggerEmailFunnelStage.ShippingBilling ||
                    funnelStage == EnumHelper.TriggerEmailFunnelStage.Payment)
                {
                    sbPixcelImageSrc.Append("&a_empty=true");
                }

                //Abandoned Cart Funnel Stage
                sbPixcelImageSrc.Append("&n_eboac=");
                sbPixcelImageSrc.Append(funnelStage.ToString());


                //Set User Account Info
                this.SetUserAccountInfo(sbPixcelImageSrc, userAccount);

                //Set Cart Total Info
                this.SetCartTotalInfo(sbPixcelImageSrc, shoppingCart, funnelStage);


            }

            return sbPixcelImageSrc.ToString();
        }

        /// <summary>
        /// Genrate Trigger Email Src Tag  for Shopping Cart Item
        /// </summary>
        /// <param name="userAccount"></param>
        /// <param name="currShoppingCartItem"></param>
        /// <param name="shoppingCart"></param>
        /// <param name="funnelStage"></param>
        /// <param name="initialPixelTag"></param>
        /// <returns></returns>
        private string CreateProductItemPixelTagImageSrc(ZNodeUserAccount userAccount, ZNodeShoppingCartItem currShoppingCartItem, ZNodeShoppingCart shoppingCart, EnumHelper.TriggerEmailFunnelStage funnelStage, string initialPixelTag)
        {
            StringBuilder sbPixcelImageSrc = new StringBuilder();

            if (currShoppingCartItem != null)
            {
                if (!string.IsNullOrEmpty(initialPixelTag))
                {
                    //initial src url with base key tags
                    sbPixcelImageSrc.Append(initialPixelTag.Trim().TrimEnd('&'));

                    //customer email
                    string sUserEmail = string.Empty;
                    if (userAccount != null)
                    {
                        sUserEmail = userAccount.EmailID;
                    }
                    sbPixcelImageSrc.Append("&e=");
                    sbPixcelImageSrc.Append(sUserEmail);


                    //i_sku :Item SKU Number
                    sbPixcelImageSrc.Append("&i_sku=");
                    sbPixcelImageSrc.Append(currShoppingCartItem.Product.SKU);

                    //i_qty :Item quantity
                    sbPixcelImageSrc.Append("&i_qty=");
                    sbPixcelImageSrc.Append(currShoppingCartItem.Quantity);


                    //i_name :Item name
                    sbPixcelImageSrc.Append("&i_name=");
                    sbPixcelImageSrc.Append(currShoppingCartItem.Product.Name);

                    //i_price :Item Unit Price
                    sbPixcelImageSrc.Append("&i_price=");
                    sbPixcelImageSrc.Append(currShoppingCartItem.UnitPrice.ToString("N2").Replace(",", ""));

                    //i_img_url :Url of Item thumbnail image
                    sbPixcelImageSrc.Append("&i_img_url=");
                    string imageFullPath = string.Empty;
                    string domainPath = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.HttpContext.Current.Request.ApplicationPath;
                    if (domainPath.EndsWith("/") == false)
                    {
                        domainPath = domainPath + "/";
                    }

                    //imageFullPath = domainPath + ZNode.Libraries.DataAccess.Custom.GlobalHelper.GetDefaultImagePath(currShoppingCartItem.Product.ThumbnailImageFilePath).Trim('~').Trim('/');
                    imageFullPath = domainPath + currShoppingCartItem.Product.ThumbnailImageFilePath.Replace("~/", string.Empty);
                    sbPixcelImageSrc.Append(imageFullPath);

                    //i_for_id :Unique "giftee" identifier
                    sbPixcelImageSrc.Append("&i_for_id=");
                    sbPixcelImageSrc.Append("");

                    //i_for_name :"Giftee" Name
                    sbPixcelImageSrc.Append("&i_for_name=");
                    sbPixcelImageSrc.Append("");

                }
            }

            return sbPixcelImageSrc.ToString();
        }

        /// <summary>
        /// Set User Account Information in Pixel Tag
        /// </summary>
        /// <param name="sbPixcelImageSrc"></param>
        /// <param name="userAccount"></param>
        private void SetUserAccountInfo(StringBuilder sbPixcelImageSrc, ZNodeUserAccount userAccount)
        {
            //fisrt name                    
            sbPixcelImageSrc.Append("&f_first_name=");
            string sUserFirstName = string.Empty;
            if (userAccount != null)
            {
                sUserFirstName = userAccount.FirstName;
            }
            sbPixcelImageSrc.Append(sUserFirstName);

            //last name                    
            sbPixcelImageSrc.Append("&f_last_name=");
            string sUserLasttName = string.Empty;
            if (userAccount != null)
            {
                sUserLasttName = userAccount.LastName;
            }
            sbPixcelImageSrc.Append(sUserLasttName);

            //Phone Number                   
            sbPixcelImageSrc.Append("&f_phone=");
            string sUserPhoneNumber = string.Empty;
            if (userAccount != null)
            {
                sUserPhoneNumber = userAccount.BillingAddress.PhoneNumber;
            }
            sbPixcelImageSrc.Append(sUserPhoneNumber);
        }

        /// <summary>
        /// Set Cart Information in Pixel Tag
        /// </summary>
        /// <param name="sbPixcelImageSrc"></param>
        /// <param name="shoppingCart"></param>
        /// <param name="funnelStage"></param>
        private void SetCartTotalInfo(StringBuilder sbPixcelImageSrc, ZNodeShoppingCart shoppingCart, EnumHelper.TriggerEmailFunnelStage funnelStage)
        {
            //f_cart_amount :ordersubtotal
            sbPixcelImageSrc.Append("&f_cart_amount=");
            sbPixcelImageSrc.Append(shoppingCart.SubTotal.ToString("N2").Replace(",", ""));

            //f_view_cart_url :Url that return customer to their cart
            sbPixcelImageSrc.Append("&f_view_cart_url=");
            string viewCartUrl = GetViewCartUrl();//GetMessageConfigValueByKey("AbandonedShoppingReturnToCartUrl");
            sbPixcelImageSrc.Append(viewCartUrl);


            //conversion signal
            if (funnelStage == EnumHelper.TriggerEmailFunnelStage.OrderComplete)
            {
                //c_web=<Final&Conversion Subtotal (without Tax & Shipping)> 
                sbPixcelImageSrc.Append("&c_web=");
                sbPixcelImageSrc.Append(shoppingCart.SubTotal.ToString("N2").Replace(",", ""));

                //c_codes=<Comma Separated List of All Coupon Codes Applied>
                sbPixcelImageSrc.Append("&c_codes=");
                sbPixcelImageSrc.Append(shoppingCart.Coupon);
            }
        }

        #endregion

    }


}

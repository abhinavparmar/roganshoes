﻿using System;
using System.Web.Script.Serialization;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Net;

namespace Zeon.Libraries.Utilities
{
    public class GetResponseEmailSubscriber
    {
        // your API key
        // available at http://www.getresponse.com/my_api_key.html
        //String api_key = "8fbc4194dafbba2ea141e4c45e440d98";

        // API 2.x URL
        //String api_url = "http://api2.getresponse.com";

        /// <summary>
        /// Add Contact in GetResponse Email marketing
        /// </summary>
        /// <param name="api_key"></param>
        /// <param name="api_url"></param>
        /// <param name="campaignName"></param>
        /// <param name="contactName"></param>
        /// <param name="subscriberEmail"></param>
        /// <returns></returns>
        public string AddContactEmailSubcriber(string api_key, string api_url, string campaignName, string contactName, string subscriberEmail)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            // get CAMPAIGN_ID of 'sample_marketing' campaign
            // new request object
            Hashtable _request = new Hashtable();
            _request["jsonrpc"] = "2.0";
            _request["id"] = 1;
            // set method name
            _request["method"] = "get_campaigns";
            // set conditions
            Hashtable operator_obj = new Hashtable();
            operator_obj["EQUALS"] = campaignName;
            Hashtable name_obj = new Hashtable();
            name_obj["name"] = operator_obj;
            // set params request object
            object[] params_array = { api_key, name_obj };
            _request["params"] = params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            String response_string = null;

            try
            {
                // call method 'get_messages' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                //check for communication and response errors
                //implement handling if needed
                Elmah.ElmahErrorManager.Log(e.StackTrace);
                Environment.Exit(0);
            }

            // decode response to Json object
            Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;

            // get result
            Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;

            string campaign_id = null;

            // get campaign id
            foreach (object key in result.Keys)
            {
                campaign_id = key.ToString();
            }

            // add contact to 'sample_marketing' campaign
            // new request object
            _request = new Hashtable();

            _request["jsonrpc"] = "2.0";
            _request["id"] = 2;

            // set method name
            _request["method"] = "add_contact";

            Hashtable contact_params = new Hashtable();
            contact_params["campaign"] = campaign_id;
            contact_params["name"] = contactName;
            contact_params["email"] = subscriberEmail;

            //Hashtable custom = new Hashtable();
            //custom["name"] = "last_purchased_product";
            //custom["content"] = "netbook";

            // contact customs array
            //object[] customs_array = { custom };

            // add customs to contact params
            //contact_params["customs"] = customs_array;

            // set params request object
            object[] add_contact_params_array = { api_key, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            response_string = null;

            try
            {
                // call method 'add_contact' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                //check for communication and response errors
                //implement handling if needed
                Elmah.ElmahErrorManager.Log(e.StackTrace);
                Environment.Exit(0);
            }
            return campaign_id;           
            //console.Write("Contact added\n");
        } 

        /// <summary>
        /// Get Contact detail from GetResponse
        /// </summary>
        /// <param name="api_key"></param>
        /// <param name="api_url"></param>
        /// <param name="campaign_id"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public string GetContactEmailSubcriber(string api_key, string api_url, string campaign_id, string email)
        {            
            JavaScriptSerializer jss = new JavaScriptSerializer();
            // get CAMPAIGN_ID of 'sample_marketing' campaign
            // new request object
            Hashtable _request = new Hashtable();
            _request["jsonrpc"] = "2.0";
            _request["id"] = 3;

            // set method name
            _request["method"] = "get_contacts";

            Hashtable contact_params = new Hashtable();
            contact_params["campaigns"] = campaign_id;

            Hashtable contact_params1 = new Hashtable();
            contact_params1["EQUALS"] = email;

            Hashtable name_obj1 = new Hashtable();
            name_obj1["email"] = contact_params1;

            // set params request object
            object[] add_contact_params_array = { api_key, name_obj1, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            string response_string = null;

            try
            {
                // call method 'get_contacts' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                //check for communication and response errors
                //implement handling if needed
                Elmah.ElmahErrorManager.Log(e.StackTrace);
                Environment.Exit(0);
            }

            // decode response to Json object
            Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;

            // get result
            Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;
            string contactID = null;

            // get campaign id
            foreach (object key in result.Keys)
            {
                contactID = key.ToString();
            }
            return contactID;
        }

        /// <summary>
        /// Delete (Unsubscribe) contact from GetResponse
        /// </summary>
        /// <param name="contactID"></param>
        public void UnsubscribeContactEmail(string api_key, string api_url,string contactID)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String response_string = null;
            // new request object
            Hashtable _request = new Hashtable();

            _request["jsonrpc"] = "2.0";
            _request["id"] = 4;

            // set method name
            _request["method"] = "delete_contact";

            Hashtable contact_params = new Hashtable();
            contact_params["contact"] = contactID;

            // set params request object
            object[] add_contact_params_array = { api_key, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            response_string = null;

            try
            {
                // call method 'delete_contact' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                //check for communication and response errors
                //implement handling if needed
                Elmah.ElmahErrorManager.Log(e.StackTrace);
                Environment.Exit(0);
            }
        }

    }
}


﻿using System.Runtime.Serialization;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Represents product pricing information.
    /// </summary>    
    [DataContract(Namespace = "http://mobile.znode.com")]
    public class Pricing 
    {
        #region Private Member Variables
        private string _Sku = string.Empty;        
        private decimal? _RetailPrice;
        private decimal? _WholesalePrice;
        private decimal? _SalePrice;
        private int? _TierStart;
        private int? _TierEnd;
        private bool _ActiveInd = false;
        private int? _PortalId = null;
        private int? _LocaleId = null;
        private string _StoreName = string.Empty;
        private string _LocaleCode = string.Empty;
        #endregion

        #region Public Member Properties

        /// <summary>
        /// Gets or sets the product SKU.
        /// </summary>
        [DataMember]
        public string StoreName
        {
            get
            {
                return this._StoreName;
            }
           
            set
            {
                this._StoreName = value;
            }
        }

        /// <summary>
        /// Gets or sets the product SKU.
        /// </summary>
        [DataMember]
        public string LocaleCode
        {
            get
            {
                return this._LocaleCode;
            }
            
            set
            {
                this._LocaleCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the product SKU.
        /// </summary>
        [DataMember]
        public string Sku
        {
            get
            {
                return this._Sku;
            }
           
            set
            {
                this._Sku = value;
            }
        }

        /// <summary>
        /// Gets or sets the retail price.
        /// </summary>    
        [DataMember]
        public decimal? RetailPrice
        {
            get
            {
                return this._RetailPrice;
            }
            
            set            
            {
                this._RetailPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the wholesale price.
        /// </summary>
        [DataMember]
        public decimal? WholesalePrice
        {
            get
            {
                return this._WholesalePrice;
            }
           
            set
            {
                this._WholesalePrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the product sale price.
        /// </summary>
        [DataMember]
        public decimal? SalePrice
        {
            get
            {
                return this._SalePrice;
            }
           
            set
            {
                this._SalePrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimum quantity for this pricing tier.
        /// </summary>
        [DataMember]
        public int? TierStart
        {
            get
            {
                return this._TierStart;
            }
           
            set
            {
                this._TierStart = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum quantity for this pricing tier.
        /// </summary>
        [DataMember]
        public int? TierEnd
        {
            get
            {
                return this._TierEnd;
            }
           
            set
            {
                this._TierEnd = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether if this product is active.
        /// </summary>
        [DataMember]
        public bool ActiveInd
        {
            get
            {
                return this._ActiveInd;
            }
           
            set
            {
                this._ActiveInd = value;
            }
        }

        /// <summary>
        /// Gets or sets the Portal Id of the Product/SKU.
        /// </summary>
        [DataMember]
        public int? PortalId
        {
            get
            {
                return this._PortalId;
            }
           
            set
            {
                this._PortalId = value;
            }
        }

        /// <summary>
        /// Gets or sets the LocaleId the Product/SKU.
        /// </summary>
        [DataMember]
        public int? LocaleId
        {
            get
            {
                return this._LocaleId;
            }
            
            set
            {
                this._LocaleId = value;
            }
        }
        #endregion
    }
}

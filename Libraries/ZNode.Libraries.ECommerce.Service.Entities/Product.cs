﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Represents a product in the catalog.
    /// </summary>    
    [DataContract(Namespace = "http://mobile.znode.com")]
    [XmlSerializerFormat]
    public class Product
    {
        #region Private Variables
        private int _ProductID = 0;
        private string _Name = string.Empty;
        private string _Description = string.Empty;
        private string _FeatureDesc = string.Empty;
        private string _ProductNum = string.Empty;
        private string _Sku = string.Empty;
        private string _ImageFile = string.Empty;
        private int _QuantityOnHand = 0;
        private decimal _DisplayPrice = 0;
        private decimal _RetailPrice = 0;
        private decimal? _SalePrice = 0;
        private int _DisplayOrder = 500;
        private string _ShortDescription = string.Empty;
        private string _ImageLinkLarge = string.Empty;
        private string _ImageLinkMedium = string.Empty;
        private string _ImageLinkSmall = string.Empty;
        private string _ImageLinkCrossSell = string.Empty;
        private string _ImageLinkThumbnail = string.Empty;
        private string _ImageLinkSmallThumbNail = string.Empty;
        private int _TotalReviews = 0;
        private decimal _ReviewRating = 0;
        private string _ReviewRatingImageUrl = string.Empty;
        private string _BuyButtonUrl = string.Empty;
        private int? _SplashCategoryID;
        private string _SplashCategoryName = string.Empty;

        // Splash Image
        private string _SplashImageUrlLarge = string.Empty;
        private string _SplashImageUrlSmall = string.Empty;

        /// <summary>
        /// Entiry helper object
        /// </summary>
        private EntityHelper helper = new EntityHelper();

        #endregion

        #region Members

        /// <summary>
        /// Gets or sets the product ID for this product. This value is assigned by the storefront when a new product is added.
        /// </summary>
        [DataMember]
        public int ProductID
        {
            get
            {
                return this._ProductID;
            }
            
            set
            {
                this._ProductID = value;
            }
        }

        /// <summary>
        /// Gets or sets the display price.
        /// </summary>
        [DataMember]
        public decimal DisplayPrice
        {
            get
            {
                return this._DisplayPrice;
            }
           
            set
            {
                this._DisplayPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the product retail as defined in store front.
        /// </summary>        
        public decimal RetailPrice
        {
            get
            {
                return this._RetailPrice;
            }
            
            set
            {
                this._RetailPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the sale price as defined in the storefront.
        /// </summary>        
        public decimal? SalePrice
        {
            get
            {
                return this._SalePrice;
            }
           
            set
            {
                this._SalePrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the product name.
        /// </summary>        
        [DataMember]
        public string Name
        {
            get
            {
                return this._Name;
            }
            
            set
            {
                this._Name = value;
            }
        }

        /// <summary>
        /// Gets or sets the product description.
        /// </summary>
        [DataMember]
        public string Description
        {
            get
            {
                return this.helper.EncloseCData(this._Description);
            }
            
            set
            {
                this._Description = value;
            }
        }

        /// <summary>
        /// Gets or sets the product short description.
        /// </summary>
        [DataMember]
        public string ShortDescription
        {
            get
            {
                return this._ShortDescription;
            }
            
            set
            {
                this._ShortDescription = value;
            }
        }

        /// <summary>
        /// Gets or sets the product feature description.
        /// </summary>
        [DataMember]
        public string FeaturesDesc
        {
            get
            {
                return this.helper.EncloseCData(this._FeatureDesc);
            }
           
            set
            {
                this._FeatureDesc = value;
            }
        }

        /// <summary>
        /// Gets or sets the product number.
        /// </summary>
        [DataMember]
        public string ProductNum
        {
            get
            {
                return this._ProductNum;
            }
            
            set
            {
                this._ProductNum = value;
            }
        }

        /// <summary>
        /// Gets or sets the product image file name.
        /// </summary>
        [DataMember]
        public string ImageFile
        {
            get
            {
                return this._ImageFile;
            }
           
            set
            {
                this._ImageFile = value;
            }
        }

        /// <summary>
        /// Gets or sets the product SKU.
        /// </summary>
        [DataMember]
        public string Sku
        {
            get
            {
                return this._Sku;
            }
            
            set
            {
                this._Sku = value;
            }
        }

        /// <summary>
        /// Gets or sets the Quantity on hand value.
        /// </summary>
        [DataMember]
        public int QuantityOnHand
        {
            get { return this._QuantityOnHand; }
            set { this._QuantityOnHand = value; }
        }

        /// <summary>
        /// Gets or sets the display order. Display order is used for sorting products when being displayed. Products are sorted from lowest to highest.
        /// </summary>
        [DataMember]
        public int DisplayOrder
        {
            get
            {
                return this._DisplayOrder;
            }
            
            set
            {
                this._DisplayOrder = value;
            }
        }

        /// <summary>
        /// Gets or sets the large image link
        /// </summary>
        [DataMember]
        public string ImageLinkLarge
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.Large, this._ImageFile);
            }
           
            set
            {
                this._ImageLinkLarge = value;
            }
        }

        /// <summary>
        /// Gets or sets the medium image link
        /// </summary>
        [DataMember]
        public string ImageLinkMedium
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.Medium, this._ImageFile);
            }
           
            set
            {
                this._ImageLinkMedium = value;
            }
        }

        /// <summary>
        /// Gets or sets the large image link
        /// </summary>
        [DataMember]
        public string ImageLinkSmall
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.Small, this._ImageFile);
            }
           
            set
            {
                this._ImageLinkSmall = value;
            }
        }

        /// <summary>
        /// Gets or sets the cross sell image link
        /// </summary>
        [DataMember]
        public string ImageLinkCrossSell
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.CrossSell, this._ImageFile);
            }
            
            set
            {
                this._ImageLinkCrossSell = value;
            }
        }

        /// <summary>
        /// Gets or sets the thumbnail image link
        /// </summary>
        [DataMember]
        public string ImageLinkThumbnail
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.Thumbnail, this._ImageFile);
            }
            
            set
            {
                this._ImageLinkThumbnail = value;
            }
        }

        /// <summary>
        /// Gets or sets the small thumbnail image link
        /// </summary>
        [DataMember]
        public string ImageLinkSmallThumbnail
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.Medium, this._ImageFile);
            }
            
            set
            {
                this._ImageLinkSmallThumbNail = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of reviews.
        /// </summary>
        [DataMember]
        public int TotalReviews
        {
            get
            {
                return this._TotalReviews;
            }
            
            set
            {
                this._TotalReviews = value;
            }
        }

        /// <summary>
        /// Gets or sets the review rating.
        /// </summary>        
        public decimal ReviewRating
        {
            get
            {
                return this._ReviewRating;
            }
            
            set
            {
                this._ReviewRating = value;
            }
        }

        /// <summary>
        /// Gets or sets the review rating image Url
        /// </summary>
        [DataMember]
        public string ReviewRatingImageUrl
        {
            get
            {                
                this._ReviewRatingImageUrl = this.helper.GetStarRatingUrl(this.ReviewRating);

                return this._ReviewRatingImageUrl;
            }
            
            set
            {
                this._ReviewRatingImageUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the buy button Url.
        /// </summary>
        [DataMember]
        public string BuyButtonUrl
        {
            get
            {
                return this._BuyButtonUrl;
            }
           
            set
            {
                this._BuyButtonUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the spalsh category Id.
        /// </summary>
        [DataMember]
        public int? SplashCategoryID
        {
            get
            {
                return this._SplashCategoryID;
            }
           
            set
            {
                this._SplashCategoryID = value;
            }
        }

        /// <summary>
        /// Gets or sets the splash category name.
        /// </summary>
        [DataMember]
        public string SplashCategoryName
        {
            get
            {
                return this._SplashCategoryName;
            }
           
            set 
            {
                this._SplashCategoryName = value;
            }
        }

        /// <summary>
        /// Gets or sets large splash image url. 
        /// </summary>
        [DataMember]
        public string SplashImageUrlLarge
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.SplashImageLarge, this._SplashImageUrlLarge);
            }
            
            set
            {
                this._SplashImageUrlLarge = value;
            }
        }

        /// <summary>
        /// Gets the splash large image height. (Read Only)
        /// </summary>
        [DataMember]
        public int SplashImageHeightLarge
        {
            get
            {
                return this.helper.GetImageDimension(this.SplashImageUrlLarge).Height;
            }
            
            private set
            {
                // No Operation
            }
        }

        /// <summary>
        /// Gets the splash large image width. (Read Only)
        /// </summary>
        [DataMember]
        public int SplashImageWidthLarge
        {
            get
            {
                return this.helper.GetImageDimension(this.SplashImageUrlLarge).Width;
            }
            
            private set
            {
                // No Operation
            }
        }

        /// <summary>
        /// Gets or sets the small splash image url.
        /// </summary>
        [DataMember]
        public string SplashImageUrlSmall
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.SplashImageSmall, this._SplashImageUrlSmall);
            }
           
            set
            {
                this._SplashImageUrlSmall = value;
            }
        }

        /// <summary>
        /// Gets the splash small image width. (Read Only)
        /// </summary>
        [DataMember]
        public int SplashImageWidthSmall
        {
            get
            {
                return this.helper.GetImageDimension(this.SplashImageUrlSmall).Width;
            }
            
            private set
            {
                // No Operation
            }
        }

        /// <summary>
        /// Gets the splash large image height. (Read Only)
        /// </summary>
        [DataMember]
        public int SplashImageHeightSmall
        {
            get
            {
                return this.helper.GetImageDimension(this.SplashImageUrlSmall).Height;
            }
            
            private set
            {
                // No Operation
            }
        }
        #endregion
    }
}
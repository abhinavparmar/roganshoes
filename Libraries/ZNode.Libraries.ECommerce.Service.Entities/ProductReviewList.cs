﻿using System.Runtime.Serialization;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Represents the product review list.
    /// </summary>
    [DataContract(Namespace = "http://mobile.znode.com")]
    public class ProductReviewList
    {
        #region Private Member Variables
        private EntityCollection<ProductReview> _ProductReviewList = new EntityCollection<ProductReview>();
        #endregion

        #region Public Member Properties

        /// <summary>
        /// Gets or sets the product review entity collection.
        /// </summary>
        [DataMember()]
        [System.Xml.Serialization.XmlElement("ProductReview")]
        public EntityCollection<ProductReview> ProductReviewCollection
        {
            get
            {
                return this._ProductReviewList;
            }
            
            set
            {
                this._ProductReviewList = value;
            }
        }
        #endregion
    }
}

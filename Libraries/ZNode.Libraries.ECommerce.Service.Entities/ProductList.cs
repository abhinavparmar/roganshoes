﻿using System.Runtime.Serialization;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Represents a Storefront Product collection.
    /// </summary>    
    [DataContract(Namespace = "http://mobile.znode.com")]
    public class ProductList
    {
        #region Private Member Variables
        private EntityCollection<Product> _ProductList = new EntityCollection<Product>();        
        #endregion

        #region Public Member Properties
        /// <summary>
        /// Gets or sets the product entity collection.
        /// </summary>
        [DataMember()]
        [System.Xml.Serialization.XmlElement("Product")]
        public EntityCollection<Product> ProductCollection
        {
            get { return this._ProductList; }
            set { this._ProductList = value; }
        }
        
        #endregion
    }
}

﻿using System.Runtime.Serialization;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Represents a response of this service containing stauts, error messages, and transaction codes.
    /// </summary>    
    [DataContract(Namespace = "http://mobile.znode.com")]
    public class Response
    {
        #region Private Member Variables
        private string _Id = "0";
        private string _Name = string.Empty;
        private string _Message = string.Empty;
        private string _TransactionCode = "0";
        private string _TransactionMessage = string.Empty;
        private bool _IsSuccess = false;

        #endregion

        #region Public Member Properties
        /// <summary>
        /// Gets or sets the response message code.
        /// </summary>
        [DataMember]
        public string ID
        {
            get { return this._Id; }
            set { this._Id = value; }
        }

        /// <summary>
        /// Gets or sets the response message name.
        /// </summary>
        [DataMember]
        public string Name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        /// <summary>
        /// Gets or sets any error message encountered during an opperation.
        /// </summary>
        [DataMember]
        public string Message
        {
            get { return this._Message; }
            set { this._Message = value; }
        }

        /// <summary>
        /// Gets or sets the transaction code returned from the opperation performed. For Adds it will be the ID of the object added, 
        /// for Updates it will be the number of rows updated. Other opperations may have differnent values.
        /// </summary>
        [DataMember]
        public string TransactionCode
        {
            get { return this._TransactionCode; }
            set { this._TransactionCode = value; }
        }

        /// <summary>
        /// Gets or sets the transaction message returned from the operation.
        /// </summary>
        [DataMember]
        public string TransactionMessage
        {
            get { return this._TransactionMessage; }
            set { this._TransactionMessage = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the transaction is suuccess or failure.
        /// </summary>
        [DataMember]
        public bool IsSuccess
        {
            get { return this._IsSuccess; }
            set { this._IsSuccess = value; }
        }
        #endregion
    }
}

﻿using System;
using System.Runtime.Serialization;
using System.Web;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Represents product pricing information.
    /// </summary>    
    [DataContract(Namespace = "http://mobile.znode.com")]
    public class Portal
    {
        #region Private Member Variables
        private int _PortalId;
        private string _CompanyName;
        private string _StoreName;
        private string _Logo;
        #endregion

        /// <summary>
        /// Gets or sets the PortalID property. 
        /// </summary>        
        [DataMember]
        public int PortalID
        {
            get
            {
                return this._PortalId;
            }

            set
            {
                this._PortalId = value;
            }
        }

        /// <summary>
        /// Gets or sets the CompanyName property. 
        /// </summary>
        /// <value>This type is nvarchar.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>        
        [DataMember]
        public string CompanyName
        {
            get
            {
                return this._CompanyName;
            }

            set
            {
                this._CompanyName = value;
            }
        }

        /// <summary>
        /// Gets or sets the StoreName property. 
        /// </summary>
        /// <value>This type is nvarchar.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>
        [DataMember]
        public string StoreName
        {
            get
            {
                return this._StoreName;
            }

            set
            {
                this._StoreName = value;
            }
        }

        /// <summary>
        /// Gets or sets the LogoPath property. 
        /// </summary>
        /// <value>This type is nvarchar.</value>
        /// <remarks>
        /// This property can be set to null. 
        /// </remarks>
        [DataMember]
        public string Logo
        {
            get
            {
                return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath + this._Logo.Replace("~", string.Empty);                
            }

            set
            {
                this._Logo = value;
            }
        }
    }
}

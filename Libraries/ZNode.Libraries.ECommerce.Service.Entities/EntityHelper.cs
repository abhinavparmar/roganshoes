﻿using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Utilities;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Image size enum object
    /// </summary>
    public enum ZNodeImageSize
    {
        /// <summary>
        /// Large size image.
        /// </summary>
        Large = 450,

        /// <summary>
        /// Medium Size Image.
        /// </summary>
        Medium = 250,

        /// <summary>
        /// Small size image
        /// </summary>
        Small = 150,

        /// <summary>
        /// Cross Sell Image
        /// </summary>
        CrossSell = 97,

        /// <summary>
        /// Thumbnail Image
        /// </summary>
        Thumbnail = 50,

        /// <summary>
        /// Small Thumbnail Image
        /// </summary>
        SmallThumbnail = 37,

        /// <summary>
        /// Splash image large size (640x316)
        /// </summary>
        SplashImageLarge = 640,

        /// <summary>
        /// Splash image small size (320x158)
        /// </summary>
        SplashImageSmall = 320
    }

    /// <summary>
    /// Represent the Entiry Helper class.
    /// </summary>
    public class EntityHelper
    {
        /// <summary>
        /// Get the product images full url based on image size. Ex.http://www.examplestore.com/data/default/images/catalog/250/apple.jpg
        /// </summary>
        /// <param name="imageSize">ImageSize enum object</param>
        /// <param name="fileName">Image file name</param>
        /// <returns>Returns the product images full url.</returns>
        public string GetImageFullPath(ZNodeImageSize imageSize, string fileName)
        {
            string fullPath = string.Empty;
            string requestUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

            // Remove trailing slash (/)
            requestUrl = requestUrl.Remove(requestUrl.Length - 1, 1);
            string parsedConfigPath = string.Empty;

            if (fileName.Trim().Length == 0)
            {
                return string.Empty;
            }
            else
            {
                string configPath = string.Empty;
                ZNodeImage znodeImage = new ZNodeImage();

                switch (imageSize)
                {
                    case ZNodeImageSize.Large:
                        znodeImage.GetImageHttpPathLarge(fileName);
                        configPath = ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.LargeImagePath;
                        break;
                    case ZNodeImageSize.Medium:
                        znodeImage.GetImageHttpPathMedium(fileName);
                        configPath = ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.MediumImagePath;
                        break;
                    case ZNodeImageSize.Small:
                        znodeImage.GetImageHttpPathSmall(fileName);
                        configPath = ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.SmallImagePath;
                        break;
                    case ZNodeImageSize.CrossSell:
                        znodeImage.GetImageHttpPathCrossSell(fileName);
                        configPath = ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.CrossSellImagePath;
                        break;
                    case ZNodeImageSize.Thumbnail:
                        znodeImage.GetImageHttpPathThumbnail(fileName);
                        configPath = ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.ThumbnailImagePath;
                        break;
                    case ZNodeImageSize.SmallThumbnail:
                        znodeImage.GetImageHttpPathSmallThumbnail(fileName);
                        configPath = ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.ThumbnailImagePath;
                        break;
                    case ZNodeImageSize.SplashImageSmall:
                        configPath = Path.Combine(ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.DataPath, "images/splash/320/");
                        break;
                    case ZNodeImageSize.SplashImageLarge:
                        configPath = Path.Combine(ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.DataPath, "images/splash/640/");
                        break;
                }

                parsedConfigPath = this.ParseImageConfigPath(configPath);

                fullPath = requestUrl + parsedConfigPath + fileName;
            }

            return fullPath;
        }       

        /// <summary>
        /// Get the image dimension.
        /// </summary>
        /// <param name="imageVirtualPath">Image file virtual path.</param>
        /// <returns>Returns the image height and width.</returns>
        internal System.Drawing.Size GetImageDimension(string imageVirtualPath)
        {
            string requestUrl = string.Format("{0}{1}", HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority).ToLower(), HttpContext.Current.Request.ApplicationPath.ToLower());
            Size imageDimension = new Size();

            // Make virtual path as relative path.
            string relativePath = string.Format("~/{0}", imageVirtualPath.ToLower().Replace(requestUrl, string.Empty));

            string physicalPath = HttpContext.Current.Server.MapPath(relativePath);
            if (System.IO.File.Exists(physicalPath))
            {
                Image image = Image.FromFile(physicalPath);
                imageDimension.Width = image.Width;
                imageDimension.Height = image.Height;
            }

            return imageDimension;
        }

        /// <summary>
        /// Enclose value with CDATA tag.
        /// </summary>
        /// <param name="value">String to enclose in CDATA</param>
        /// <returns>Returns CDATA enclosed value.</returns>
        internal string EncloseCData(string value)
        {
            return string.Format("<![CDATA[ {0} ]]>", value);
        }

        /// <summary>
        /// Get the star rating image url based on star rating value.
        /// </summary>
        /// <param name="reviewRating">ReviewRating value.</param>
        /// <returns>Returns the review rating star image url.</returns>
        internal string GetStarRatingUrl(decimal reviewRating)
        {
            StringBuilder ratingImageUrl = new StringBuilder();
            string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

            if (baseUrl.EndsWith("/"))
            {
                int lastIndex = baseUrl.LastIndexOf("/");
                baseUrl = baseUrl.Remove(lastIndex, 1);
            }

            ratingImageUrl.Append(baseUrl);
            ratingImageUrl.Append("/themes/Default/Images/");

            switch (Convert.ToInt32(reviewRating))
            {
                case 0:
                    ratingImageUrl.Append("836-Stars-00.gif");
                    break;
                case 1:
                    ratingImageUrl.Append("836-Stars-01.gif");
                    break;
                case 2:
                    ratingImageUrl.Append("836-Stars-02.gif");
                    break;
                case 3:
                    ratingImageUrl.Append("836-Stars-03.gif");
                    break;
                case 4:
                    ratingImageUrl.Append("836-Stars-04.gif");
                    break;
                case 5:
                    ratingImageUrl.Append("836-Stars-05.gif");
                    break;
                default:
                    ratingImageUrl.Append("837-Stars-no");
                    break;
            }

            return ratingImageUrl.ToString();
        }

        /// <summary>
        /// Replace the ~ symbol from image configuration path.
        /// </summary>
        /// <param name="configPath">Image Configuration path.</param>
        /// <returns>Returns the ~  removed config path.</returns>
        private string ParseImageConfigPath(string configPath)
        {
            return configPath.Replace("~", string.Empty);
        }
    }
}

﻿using System.Runtime.Serialization;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Represents the multifront product review entity class.
    /// </summary>
    [DataContract(Namespace = "http://mobile.znode.com")]
    public class ProductReview
    {
        #region Private Member Variables
        private int _ProductID = 0;
        private string _ProductName = string.Empty;
        private decimal _ReviewRating = 0;
        private string _ReviewRatingImageUrl = string.Empty;
        private string _ReviewTitle = string.Empty;
        private string _ReviewText = string.Empty;
        private string _ReviewUser = string.Empty;
        private string _ReviewUserLocation = string.Empty;
        private EntityHelper helper = new EntityHelper();
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the product Id.
        /// </summary>
        [DataMember]
        public int ProductID
        {
            get
            {
                return this._ProductID;
            }
            
            set
            {
                this._ProductID = value;
            }
        }

        /// <summary>
        /// Gets or sets the product name.
        /// </summary>
        [DataMember]
        public string ProductName
        {
            get
            {
                return this._ProductName;
            }
           
            set
            {
                this._ProductName = value;
            }
        }

        /// <summary>
        /// Gets or sets the review rating.
        /// </summary>        
        public decimal ReviewRating
        {
            get
            {
                return this._ReviewRating;
            }
           
            set
            {
                this._ReviewRating = value;
            }
        }

        /// <summary>
        /// Gets or sets the review rating image url.
        /// </summary>
        [DataMember]
        public string ReviewRatingImageUrl
        {
            get
            {
                this._ReviewRatingImageUrl = this.helper.GetStarRatingUrl(this.ReviewRating);
                return this._ReviewRatingImageUrl;
            }
           
            set
            {
                this._ReviewRatingImageUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the review title.
        /// </summary>
        [DataMember]
        public string ReviewTitle
        {
            get
            {
                return this._ReviewTitle;
            }
            
            set
            {
                this._ReviewTitle = value;
            }
        }

        /// <summary>
        /// Gets or sets the review text.
        /// </summary>
        [DataMember]
        public string ReviewText
        {
            get
            {
                return this._ReviewText;
            }
            
            set
            {
                this._ReviewText = value;
            }
        }

        /// <summary>
        /// Gets or sets the reviewed user name.
        /// </summary>
        [DataMember]
        public string ReviewUser
        {
            get
            {
                return this._ReviewUser;
            }
            
            set
            {
                this._ReviewUser = value;
            }
        }

        /// <summary>
        /// Gets or sets the reviewed user location.
        /// </summary>
        [DataMember]
        public string ReviewUserLocation
        {
            get
            {
                return this._ReviewUserLocation;
            }
            
            set
            {
                this._ReviewUserLocation = value;
            }
        }
        #endregion
    }
}

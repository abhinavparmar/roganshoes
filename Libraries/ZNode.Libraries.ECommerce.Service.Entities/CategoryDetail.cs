﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Represents a category with it's associated products.
    /// </summary>    
    [DataContract(Namespace = "http://mobile.znode.com")]    
    public class CategoryDetail
    {
        #region Private Member Variables
        private EntityCollection<Product> _ProductCollection = new EntityCollection<Product>();
        private EntityCollection<Category> _CategoryCollection = new EntityCollection<Category>();
        #endregion
        
        #region Public Member Properties

        /// <summary>
        /// Gets or sets the product entity collection object.
        /// </summary>
        [DataMember()]
        [XmlElement("Product")]
        public EntityCollection<Product> ProductCollection
        {
            get { return this._ProductCollection; }
            set { this._ProductCollection = value; }
        }

        /// <summary>
        /// Gets or sets the category entity collection object.
        /// </summary>
        [DataMember()]
        [XmlElement("Category")]
        public EntityCollection<Category> CategoryCollection
        {
            get { return this._CategoryCollection; }
            set { this._CategoryCollection = value; }
        }

        #endregion
    }
}

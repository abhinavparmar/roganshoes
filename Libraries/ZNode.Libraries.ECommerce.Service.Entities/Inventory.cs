﻿using System.Runtime.Serialization;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Represents a product inventory item. Use to set inventory levels for products in the catalog.
    /// </summary>    
    [DataContract(Namespace = "http://mobile.znode.com")]
    public class Inventory 
    {
        #region Private Member Variables
        private string _Sku = string.Empty;
        private int _QuantityOnHand = 0;
        private bool _ActiveInd = false;
        private int? _ReorderLevel;
        #endregion

        #region Public Member Properties
        /// <summary>
        /// Gets or sets the product SKU.
        /// </summary>
        [DataMember]
        public string Sku
        {
            get
            {
                return this._Sku;
            }
           
            set
            {
                this._Sku = value;
            }
        }

        /// <summary>
        /// Gets or sets the quantity on hand for this product.
        /// </summary>    
        [DataMember]
        public int QuantityOnHand
        {
            get
            {
                return this._QuantityOnHand;
            }
            
            set
            {
                this._QuantityOnHand = value;
            }
        }

        /// <summary>
        /// Gets or sets the reorder level. Use this value to determine at what point new inventory must be replenished.
        /// </summary>
        [DataMember]
        public int? ReorderLevel
        {
            get 
            {
                return this._ReorderLevel; 
            }
            
            set 
            {
                this._ReorderLevel = value; 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the inventory item is enabled or not.
        /// </summary>
        [DataMember]
        public bool ActiveInd
        {
            get
            {
                return this._ActiveInd;
            }
            
            set
            {
                this._ActiveInd = value;
            }
        }
        #endregion
    }
}

﻿using System.Runtime.Serialization;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Represents a storefront product category class.
    /// </summary>    
    [DataContract(Namespace = "http://mobile.znode.com")]
    public class Category
    {
        #region Private Member Variables
        private int _CategoryID;

        private string _Name = string.Empty;
        private string _Description = string.Empty;
        private string _ShortDescription = string.Empty;
        private int _DisplayOrder;
        private string _ImageFile = string.Empty;
        private string _StoreName = string.Empty;
        private string _LocaleCode = string.Empty;
        private string _ImageLinkLarge = string.Empty;
        private string _ImageLinkMedium = string.Empty;
        private string _ImageLinkSmall = string.Empty;
        private string _ImageLinkCrossSell = string.Empty;
        private string _ImageLinkThumbnail = string.Empty;
        private string _ImageLinkSmallThumbNail = string.Empty;
        private string _SplashImageUrlLarge = string.Empty;
        private string _SplashImageUrlSmall = string.Empty;

        /// <summary>
        /// Entiry helper object
        /// </summary>
        private EntityHelper helper = new EntityHelper();

        #endregion

        #region Public Member Properties
        /// <summary>
        /// Gets or sets the category ID. The category ID is automatically set by the setorefront when a category is added.
        /// </summary>
        [DataMember]
        public int CategoryId
        {
            get { return this._CategoryID; }
            set { this._CategoryID = value; }
        }
        
        /// <summary>
        /// Gets or sets the category Name.
        /// </summary>
        [DataMember]
        public string Name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        /// <summary>
        /// Gets or sets the category description.
        /// </summary>
        [DataMember]
        public string Description
        {
            get { return this.helper.EncloseCData(this._Description); }
            set { this._Description = value; }
        }

        /// <summary>
        /// Gets or sets the category short description.
        /// </summary>
        [DataMember]
        public string ShortDescription
        {
            get { return this._ShortDescription; }
            set { this._ShortDescription = value; }
        }
        
        /// <summary>
        /// Gets or sets the display order. Display order is used for sorting categories when being displayed. Categories are sorted from lowest to highest.
        /// </summary>
        [DataMember()]
        public int DisplayOrder
        {
            get { return this._DisplayOrder; }
            set { this._DisplayOrder = value; }
        }

        /// <summary>
        /// Gets or sets the image file name.
        /// </summary>
        [DataMember()]
        public string ImageFile
        {
            get { return this._ImageFile; }
            set { this._ImageFile = value; }
        }

        /// <summary>
        /// Gets or sets the store name
        /// </summary>
        [DataMember]
        public string StoreName
        {
            get
            {
                return this._StoreName;
            }
            
            set
            {
                this._StoreName = value;
            }
        }

        /// <summary>
        /// Gets or sets the locale code.
        /// </summary>
        [DataMember]
        public string LocaleCode
        {
            get
            {
                return this._LocaleCode;
            }
            
            set
            {
                this._LocaleCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the large image link
        /// </summary>
        [DataMember]
        public string ImageLinkLarge
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.Large, this._ImageFile);
            }
            
            set
            {
                this._ImageLinkLarge = value;
            }
        }

        /// <summary>
        /// Gets or sets the medium image link
        /// </summary>
        [DataMember]
        public string ImageLinkMedium
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.Medium, this._ImageFile);
            }
            
            set
            {
                this._ImageLinkMedium = value;
            }
        }

        /// <summary>
        /// Gets or sets the large image link
        /// </summary>
        [DataMember]
        public string ImageLinkSmall
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.Small, this._ImageFile);
            }
           
            set
            {
                this._ImageLinkSmall = value;
            }
        }

        /// <summary>
        /// Gets or sets the cross sell image link
        /// </summary>
        [DataMember]
        public string ImageLinkCrossSell
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.CrossSell, this._ImageFile);
            }
           
            set
            {
                this._ImageLinkCrossSell = value;
            }
        }

        /// <summary>
        /// Gets or sets the thumbnail image link
        /// </summary>
        [DataMember]
        public string ImageLinkThumbnail
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.Thumbnail, this._ImageFile);
            }
           
            set
            {
                this._ImageLinkThumbnail = value;
            }
        }

        /// <summary>
        /// Gets or sets the small thumbnail image link
        /// </summary>
        [DataMember]
        public string ImageLinkSmallThumbnail
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.Medium, this._ImageFile);
            }
           
            set
            {
                this._ImageLinkSmallThumbNail = value;
            }
        }

        /// <summary>
        /// Gets or sets large splash image url. 
        /// </summary>
        [DataMember]
        public string SplashImageUrlLarge
        {
            get
            {
                return this.helper.GetImageFullPath(ZNodeImageSize.SplashImageLarge, this._SplashImageUrlLarge);
            }
            
            set
            {
                this._SplashImageUrlLarge = value;
            }
        }

        /// <summary>
        /// Gets the splash large image height. (Read Only)
        /// </summary>
        [DataMember]
        public int SplashImageHeightLarge
        {
            get
            {
                return this.helper.GetImageDimension(this.SplashImageUrlLarge).Height;
            }
            
            private set
            {
                // No Operation
            }
        }

        /// <summary>
        /// Gets the splash large image width. (Read Only)
        /// </summary>
        [DataMember]
        public int SplashImageWidthLarge
        {
            get
            {
                return this.helper.GetImageDimension(this.SplashImageUrlLarge).Width;
            }
            
            private set
            {
                // No Operation
            }
        }

        /// <summary>
        /// Gets or sets the small splash image url.
        /// </summary>
        [DataMember]
        public string SplashImageUrlSmall
        {
            get
            {                
                return this.helper.GetImageFullPath(ZNodeImageSize.SplashImageSmall, this._SplashImageUrlSmall);
            }
            
            set
            {
                this._SplashImageUrlSmall = value;
            }
        }

        /// <summary>
        /// Gets the splash small image width. (Read Only)
        /// </summary>
        [DataMember]
        public int SplashImageWidthSmall
        {
            get
            {
                return this.helper.GetImageDimension(this.SplashImageUrlSmall).Width;
            }
            
            private set
            {
                // No Operation
            }
        }

        /// <summary>
        /// Gets the splash large image height. (Read Only)
        /// </summary>
        [DataMember]
        public int SplashImageHeightSmall
        {
            get
            {
                return this.helper.GetImageDimension(this.SplashImageUrlSmall).Height;
            }
            
            private set
            {
                // No Operation
            }
        }

        #endregion
    }
}

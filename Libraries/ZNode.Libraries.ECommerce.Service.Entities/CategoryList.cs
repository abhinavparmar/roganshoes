﻿using System.Runtime.Serialization;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Represents a Storefront Product collection.
    /// </summary>    
    [DataContract(Namespace = "http://mobile.znode.com")]
    public class CategoryList 
    {
        #region Private Member Variables
        private EntityCollection<Category> _CategoryList = new EntityCollection<Category>();
        #endregion

        #region Public Member Properties
        /// <summary>
        /// Gets or sets the product entity collection.
        /// </summary>
        [DataMember()]
        [System.Xml.Serialization.XmlElement("Category")]
        public EntityCollection<Category> CategoryCollection
        {
            get { return this._CategoryList; }
            set { this._CategoryList = value; }
        }
        #endregion
    }
}
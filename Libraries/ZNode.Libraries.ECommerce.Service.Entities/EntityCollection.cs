﻿using System.Runtime.Serialization;

namespace ZNode.Libraries.ECommerce.Service.Entities
{
    /// <summary>
    /// Implements a generic collection class. Use this class to implement strongly typed collections
    /// </summary>
    /// <typeparam name="T">Type of entity.</typeparam>        
    [CollectionDataContract(Namespace = "http://mobile.znode.com", Name = "{0}Collection")]    
    public class EntityCollection<T> : System.Collections.CollectionBase
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the element at the specified index
        /// </summary>
        /// <param name="index">The zero-based index of the element to get or set.</param>
        /// <returns>Returns the item from the specified index position.</returns>
        [DataMember]
        public T this[int index]
        {
            get
            {
                return (T)List[index];
            }

            set
            {
                List[index] = value;
            }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Add a new element to the end of the collection
        /// </summary>
        /// <param name="val">The object to be added to the end of the collection.</param>
        /// <returns>Returns the added index position.</returns>
        public int Add(T val)
        {
            return List.Add(val);
        }

        /// <summary>
        /// Returns the index of a specific item in the collection
        /// </summary>
        /// <param name="val">The object to locate in the collection or list</param>
        /// <returns>The index of item if found in the list; otherwise, -1.</returns>
        public int IndexOf(T val)
        {
            return List.IndexOf(val);
        }

        /// <summary>
        /// Insert a new element in the middle of the collection at the specified index
        /// </summary>
        /// <param name="index">The zero-based index at which item should be inserted.</param>
        /// <param name="val">The(T value type) object to insert into the collection</param>
        public void Insert(int index, T val)
        {
            List.Insert(index, val);
        }

        /// <summary>
        /// Removes the specified element from the collection
        /// </summary>
        /// <param name="val">The object to remove from the collection</param>
        public void Remove(T val)
        {
            List.Remove(val);
        }

        /// <summary>
        /// Determines whether an element is in the collection      
        /// </summary>
        /// <param name="val">The object to locate in the collection</param>
        /// <returns>true if element is found in the collection; otherwise, false.</returns>
        public bool Contains(T val)
        {
            return List.Contains(val);
        }

        #endregion        
    }
}

﻿using System;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.ECommerce.Analytics
{
    /// <summary>
    /// Represents the ZNodeOutboundTracking class.
    /// </summary>
    public class ZNodeOutboundTracking
    {
        /// <summary>
        /// Log the tracking event.
        /// </summary>
        /// <param name="eventName">Tracking event name.</param>
        /// <param name="fromUrl">Redirect from this url. Usually current page.</param>
        /// <param name="toUrl">Redirected to this url.</param>
        /// <param name="productId">Selected product Id.</param>
        /// <param name="productPrice">Product display price.</param>
        public void LogTrackingEvent(string eventName, string fromUrl, string toUrl, int productId, decimal productPrice)
        {
            try
            {
                TrackingOutbound trackingOutbound = new TrackingOutbound();
                trackingOutbound.ProductID = productId;
                trackingOutbound.Date = DateTime.Now;
                trackingOutbound.FromUrl = fromUrl;
                trackingOutbound.ToUrl = toUrl;
                trackingOutbound.Price = productPrice;
                trackingOutbound.EventName = eventName;
                TrackingOutboundService trackingOutboundService = new TrackingOutboundService();
                trackingOutboundService.Insert(trackingOutbound);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            } 
        }
    }
}

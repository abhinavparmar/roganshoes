﻿using System.Text;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Analytics
{
    /// <summary>
    /// Provides Analytics code specific to Listrak that will put javascript on selected pages.
    /// </summary>
    public class ZNodeListrakAnalyticsService : ZNodeAnalyticsService
    {
        #region Public Properties
        /// <summary>
        /// Gets the Javascript that will be placed on the top of each page.
        /// </summary>
        public override string SiteWideTopJavascript
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the Javascript that will be placed on the bottom of each page.
        /// </summary>
        public override string SiteWideBottomJavascript
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the Javascript that will be placed on the bottom of each page EXCEPT the receipt.
        /// </summary>
        public override string SiteWideAnalyticsJavascript
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the Javascript for the shopping cart page (Step - 1).
        /// </summary>
        public override string ShoppingCartJavaScript
        {
            get
            {
                return this.GetShoppingCartJavaScript();
            }
        }

        /// <summary>
        /// Gets the Javascript for the address page of the checkout (Step - 2).
        /// </summary>
        public override string AddressJavaScript
        {
            get
            {
                return this.GetAddressJavaScript();
            }
        }

        /// <summary>
        /// Gets the Javascript for the checkout page (Step - 3).
        /// </summary>
        public override string CheckoutJavaScript
        {
            get
            {
                return this.GetCheckoutJavaScript();
            }
        }

        /// <summary>
        /// Gets the Javascript for the order receipt page (Step - 4).
        /// </summary>
        public override string OrderReceiptJavascript
        {
            get
            {
                return this.GetOrderReceiptJavascript();
            }
        }

        /// <summary>
        /// Gets the browser finderprint image url for order receipt page.
        /// </summary>
        public override string EmailCaptureScript
        {
            get
            {
                return this.GetEmailCaptureScript(this.EmailCaptureContorlId);
            }
        }

        public override string BrowserFingerprintImageUrl
        {
            get
            {
                return this.GetBrowserFingerprintImageUrl();
            }
        }
        #endregion

        #region Private Helper Methods
        /// <summary>
        /// Get the listrak browser fingerprint image url.
        /// </summary>
        /// <returns>Returns the listrak browser fingerprint image url.</returns>
        private string GetBrowserFingerprintImageUrl()
        {
            StringBuilder script = new StringBuilder(string.Empty);
            if (this.IsListrakEnabled())
            {
                script.Append("https://fp.listrakbi.com/fp/");
                script.Append(this.ExtractTrackingId());
                script.Append(".jpg");
            }

            return script.ToString();
        }

        /// <summary>
        /// Get the email capture listrack javascript.
        /// </summary>
        /// <param name="emailAddress">Name of the email address text box control.</param>
        /// <returns>Returns the email capture listrak script.</returns>
        private string GetEmailCaptureScript(string emailAddress)
        {
            StringBuilder script = new StringBuilder(string.Empty);

            // Listrak Shopping Cart Abandonment (Email Capture): Step 2
            if (this.IsListrakEnabled())
            {
                // Build the Listrak script in new user registraion page.
                script.Append("<!-- Listrak Shopping Cart Abandonment (Email Capture)-->");

                script.Append("<script type='text/javascript'>");

                // Add this line only to capture email
                script.Append(" _ltk.SCA.CaptureEmail('");
                script.Append(emailAddress);
                script.Append("'); ");

                script.Append(" _ltk.SCA.Stage = 2;");

                script.Append(" _ltk.SCA.SetCustomer(");

                script.Append("\"\",");
                script.Append("\"\",");
                script.Append("\"\"); ");

                script.Append(" _ltk.SCA.Submit();");
                script.Append("</script>");
                script.Append("<!-- Listrak Shopping Cart Abandonment (Email Capture)-->");
            }

            return script.ToString();
        }

        /// <summary>
        /// Injects the ecommerce code in the shopping cart page
        /// </summary>
        /// <returns>Returns the shopping cart listrak analytics script.</returns>
        private string GetShoppingCartJavaScript()
        {
            StringBuilder script = new StringBuilder(string.Empty);
            ZNodeAnalyticsData analytics = new ZNodeAnalyticsData();

            // Listrak Shopping Cart Abandonment : Step 1
            if (this.IsListrakEnabled())
            {
                // Build the additional Google Asynchronous Javascript for the order receipt page.                    
                script.Append("<!-- Listrak Shopping Cart Abandonment -->");
                script.Append("<script type='text/javascript'>");
                script.Append("     _ltk.SCA.Stage = 1; ");

                // Append order line items
                int itemIndex = 1;
                foreach (ZNodeShoppingCartItem shoppingCartItem in ShoppingCart.ShoppingCartItems)
                {
                    string scaProductVar = "scaProduct" + itemIndex;

                    script.Append("var ").Append(scaProductVar).Append(" = new SCAItem(); ");

                    script.Append(scaProductVar).Append(".Sku = ");
                    script.Append(CreateQuotedString(shoppingCartItem.Product.SKU));
                    script.Append("; ");

                    script.Append(scaProductVar).Append(".Name = ");
                    script.Append(CreateQuotedString(shoppingCartItem.Product.Name));
                    script.Append("; ");

                    script.Append(scaProductVar).Append(".Quantity = ");
                    script.Append(shoppingCartItem.Quantity.ToString());
                    script.Append("; ");

                    script.Append(scaProductVar).Append(".Price = \"");
                    script.Append(shoppingCartItem.UnitPrice.ToString("N2"));
                    script.Append("\"; ");

                    script.Append(scaProductVar).Append(".LinkUrl = ");
                    script.Append(CreateQuotedString("/product.aspx?zpid=" + shoppingCartItem.Product.ProductID.ToString()));
                    script.Append("; ");

                    script.Append(scaProductVar).Append(".ImageUrl = \"");
                    script.Append(shoppingCartItem.Product.ThumbnailImageFilePath.Replace("~", string.Empty));
                    script.Append("\"; ");

                    // Set the line total (unit price * quantity) in Meta1 field.
                    script.Append(scaProductVar).Append(".Meta1 = \"");
                    script.Append(shoppingCartItem.ExtendedPrice.ToString("N2"));
                    script.Append("\"; ");

                    script.Append("_ltk.SCA.AddItemEx(").Append(scaProductVar).Append("); ");

                    itemIndex++;
                }

                // Set the subtotal
                script.Append(" _ltk.SCA.Total = \"");
                script.Append(ShoppingCart.SubTotal.ToString("N2"));
                script.Append("\"; ");

                // Submits transaction to the analytics servers
                script.Append("_ltk.SCA.Submit();");
                script.Append("</script>");

                script.Append("<!-- Listrak Shopping Cart Abandonment -->");
            }
            
            return script.ToString();
        }

        /// <summary>
        /// Get the address page javascript.
        /// </summary>
        /// <returns>Returns the address page listrak script.</returns>
        private string GetAddressJavaScript()
        {
            StringBuilder script = new StringBuilder(string.Empty);
            ZNodeAnalyticsData analytics = new ZNodeAnalyticsData();

            // Listrak Shopping Cart Abandonment : Step 2
            if (this.IsListrakEnabled())
            {
                // Build the additional Listrak script for user account address edit page.
                script.Append("<!-- Listrak Shopping Cart Abandonment -->");

                script.Append("<script type='text/javascript'>");
                script.Append(" _ltk.SCA.Stage = 2;");

                script.Append(" _ltk.SCA.SetCustomer(");

                script.Append(CreateQuotedString(UserAccount.EmailID));
                script.Append(", ");

                string firstName = string.Empty;
                if (!string.IsNullOrEmpty(UserAccount.FirstName))
                {
                    firstName = UserAccount.FirstName;
                }

                script.Append(CreateQuotedString(firstName));
                script.Append(", ");

                string lastName = string.Empty;
                if (!string.IsNullOrEmpty(UserAccount.LastName))
                {
                    lastName = UserAccount.LastName;
                }

                script.Append(CreateQuotedString(lastName));
                script.Append("); ");

                // Submits transaction to the Analytics servers
                script.Append(" _ltk.SCA.Submit();");
                script.Append("</script>");

                script.Append("<!-- Listrak Shopping Cart Abandonment -->");
            }
            
            return script.ToString();
        }

        /// <summary>
        /// Get the checkout page listrak script.
        /// </summary>
        /// <returns>Returns the checkout page listrak javascript.</returns>
        private string GetCheckoutJavaScript()
        {
            StringBuilder script = new StringBuilder(string.Empty);
            ZNodeAnalyticsData analytics = new ZNodeAnalyticsData();

            // Listrak Shopping Cart Abandonment : Step 3
            if (this.IsListrakEnabled())
            {
                // Build the additional Listrak script for user account address edit page.
                script.Append("<!-- Listrak Shopping Cart Abandonment -->");

                script.Append("<script type='text/javascript'>");
                script.Append(" _ltk.SCA.Stage = 3;");

                script.Append(" _ltk.SCA.SetCustomer(");

                script.Append(CreateQuotedString(UserAccount.EmailID));
                script.Append(", ");

                script.Append(CreateQuotedString(UserAccount.FirstName));
                script.Append(", ");

                script.Append(CreateQuotedString(UserAccount.LastName));
                script.Append("); ");

                // Submits transaction to the Analytics servers
                script.Append("_ltk.SCA.Submit();");
                script.Append("</script>");

                script.Append("<!-- Listrak Shopping Cart Abandonment -->");
            }
            
            return script.ToString();
        }

        /// <summary>
        /// Get the order receipt page listrak script.
        /// </summary>
        /// <returns>Returns the order receipt page listrak analytics script.</returns>
        private string GetOrderReceiptJavascript()
        {
            StringBuilder script = new StringBuilder(string.Empty);
            ZNodeAnalyticsData analytics = new ZNodeAnalyticsData();

            // Listrak Shopping Cart Abandonment : Step 4
            if (this.IsListrakEnabled())
            {
                // Build the Listrak script for the order receipt page.
                script.Append("<!-- Listrak Shopping Cart Abandonment -->");
                script.Append("<script type='text/javascript'>");
                script.Append("     _ltk.SCA.Stage = 4;");

                script.Append(" _ltk.SCA.OrderNumber = ");
                script.Append(this.CreateQuotedString(Order.OrderID.ToString()));
                script.Append("; ");

                // Submits transaction to the analytics servers
                script.Append("_ltk.SCA.Submit();");
                script.Append("</script>");
                script.Append("<!-- Listrak Shopping Cart Abandonment -->");

                script.Append("<!-- Listrak Conversion Tracking -->");
                script.Append("<script type='text/javascript'>");
                script.Append(" _ltk.Order.SetCustomer(");
                script.Append(this.CreateQuotedString(Order.Email.ToString()));
                script.Append(", ");

                script.Append(this.CreateQuotedString(Order.BillingAddress.FirstName));
                script.Append(", ");

                script.Append(this.CreateQuotedString(Order.BillingAddress.LastName));
                script.Append("); ");

                script.Append(" _ltk.Order.OrderNumber = ");
                script.Append(this.CreateQuotedString(Order.OrderID.ToString()));
                script.Append("; ");

                script.Append(" _ltk.Order.ItemTotal = ");
                script.Append(this.CreateQuotedString(Order.SubTotal.ToString("N2")));
                script.Append("; ");

                script.Append(" _ltk.Order.ShippingTotal = ");
                script.Append(this.CreateQuotedString(Order.ShippingCost.ToString("N2")));
                script.Append("; ");

                script.Append(" _ltk.Order.TaxTotal = ");
                script.Append(this.CreateQuotedString(Order.SalesTax.ToString("N2")));
                script.Append("; ");

                script.Append(" _ltk.Order.HandlingTotal = ");
                script.Append(this.CreateQuotedString("0"));
                script.Append("; ");

                script.Append(" _ltk.Order.OrderTotal = ");
                script.Append(this.CreateQuotedString(Order.Total.ToString("N2")));
                script.Append("; ");

                foreach (OrderLineItem orderLineItem in Order.OrderLineItems)
                {
                    script.Append(" _ltk.Order.AddItem(");
                    script.Append(this.CreateQuotedString(orderLineItem.SKU));
                    script.Append(", ");

                    script.Append(orderLineItem.Quantity);
                    script.Append(", ");

                    script.Append(this.CreateQuotedString(((decimal)orderLineItem.Price).ToString("N2")));
                    script.Append("); ");
                }

                // Submits conversion tracking.
                script.Append("_ltk.Order.Submit();");
                script.Append("</script>");

                script.Append("<!-- Listrak Conversion Tracking -->");
            }
            
            return script.ToString();
        }

        /// <summary>
        /// Check whether the listrak tracking script added in site admin.
        /// </summary>
        /// <returns>Returns true if listrak script enabled else false.</returns>
        private bool IsListrakEnabled()
        {
            ZNodeAnalyticsData analytics = new ZNodeAnalyticsData();
            if (analytics.SiteWideBottomJavascript.ToLower().Contains("listrakbi") ||
                analytics.SiteWideTopJavascript.ToLower().Contains("listrakbi"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Extrach the Tracking ID from source URL for browser fingerprint.
        /// </summary>
        /// <returns>Returns the extracted tracking Id.</returns>
        private string ExtractTrackingId()
        {
            ZNodeAnalyticsData analytics = new ZNodeAnalyticsData();
            string trackingId = string.Empty;
            string script = string.Empty;
            if (analytics.SiteWideTopJavascript.ToLower().Contains("listrakbi"))
            {
                script = analytics.SiteWideTopJavascript;
            }

            if (analytics.SiteWideBottomJavascript.ToLower().Contains("listrakbi"))
            {
                script = analytics.SiteWideBottomJavascript;
            }

            if (!string.IsNullOrEmpty(script))
            {
                int startIndex = script.IndexOf("m=");
                int endIndex = script.IndexOf("&v=");
                trackingId = script.Substring(startIndex + 2, (endIndex - startIndex) - 2);
            }

            return trackingId;
        }

        #endregion
    }
}

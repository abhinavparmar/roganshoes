﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Analytics
{
    /// <summary>
    /// Provides a central place to manage how Analytics code is placed on a page.
    /// </summary>
    /// <remarks>
    /// The analytics capability requires that you derive your pages from ZNodeTemplate. 
    /// Because of the way that the ZNodeTemplate adds analytics code to the page (RegisterClientScriptBlock and RegisterStartupScript)
    /// you need to instantiate this class during the Page_Init event.    
    /// YOU ONLY NEED TO INSTANTIATE THIS CLASS IF YOU WANT TO OVERRIDE THE DEFAULT MESSAGES SET IN THE DATABASE.
    /// </remarks>
    /// 
    public partial class ZNodeAnalytics
    {
        /// <summary>
        /// Adds the Ecommerce tracking code on the Order Reciept Page
        /// </summary>
        /// <returns>String</returns>
        public string AddEcommerceTrackingCode()
        {
            int count = 0;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type='text/javascript'>");
            sb.Append("dataLayer = [{");
            sb.Append("'transactionId':");
            sb.Append("'" + AnalyticsData.Order.OrderID + "',");
            sb.Append("'transactionAffiliation':");
            sb.Append("'" + ZNodeConfigManager.SiteConfig.StoreName + "',");
            sb.Append("'transactionTotal':");
            sb.Append(AnalyticsData.Order.Total.ToString("N2") + ",");
            sb.Append("'transactionTax':");
            sb.Append(AnalyticsData.Order.SalesTax.ToString("N2") + ",");
            sb.Append("'transactionShipping':");
            sb.Append(AnalyticsData.Order.ShippingCost.ToString("N2") + ",");
            sb.Append("'transactionPromoCode':");
            sb.Append("'" + AnalyticsData.Order.CouponCode + "',");
            sb.Append("'transactionProducts': [");

            int orderLineItemsCount = AnalyticsData.Order.OrderLineItems.Count;

            if (AnalyticsData.ShoppingCart != null)
            {
                foreach (ZNodeShoppingCartItem item in AnalyticsData.ShoppingCart.ShoppingCartItems)
                {
                    sb.Append("{");
                    sb.Append("'sku':");
                    sb.Append("'" + item.Product.SKU + "',");
                    sb.Append("'name':");
                    sb.Append("'" + item.Product.Name.Replace("'", "&#39;") + "',");
                    //sb.Append("'category':");
                    //sb.Append("'" + item.CategoryName + "',");
                    sb.Append("'price':");
                    decimal orderLineItemPrice = 0;
                    if (item.UnitPrice > 0)
                    {
                        orderLineItemPrice = (decimal)item.UnitPrice;
                    }
                    sb.Append(orderLineItemPrice.ToString("N2") + ",");
                    sb.Append("'quantity':");
                    sb.Append(item.Quantity);
                    count++;
                    if (count == orderLineItemsCount)
                    {
                        sb.Append("}");
                    }
                    else
                    {
                        sb.Append("},");
                    }

                }
            }
            sb.Append("]");
            sb.Append("}];");
            sb.AppendLine("</script>");
            return sb.ToString();
        }

        /// <summary>
        /// Adds the SideCar tracking code on the Order Reciept Page
        /// </summary>
        /// <returns>String</returns>
        public string AddSideCarTrackingCode()
        {
            int count = 0;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type='text/javascript'>");
            sb.Append("var sidecar = sidecar || {};sidecar.transactions = {add: true,data: {");
            sb.Append("order_id:");
            sb.Append("'" + AnalyticsData.Order.OrderID + "',");
            sb.Append("subtotal:");
            sb.Append("'" + AnalyticsData.Order.SubTotal.ToString("N2") + "',");
            sb.Append("tax:");
            sb.Append("'" + AnalyticsData.Order.SalesTax.ToString("N2") + "',");
            sb.Append("shipping:");
            sb.Append("'" + AnalyticsData.Order.ShippingCost.ToString("N2") + "',");
            //Discount
            sb.Append("discounts:");
            sb.Append("'" + AnalyticsData.Order.Discount.ToString("N2") + "',");
            sb.Append("total:");
            sb.Append("'" + AnalyticsData.Order.Total.ToString("N2") + "',");
            sb.Append("zipcode:");
            sb.Append("'" + AnalyticsData.Order.ShippingAddress.PostalCode.ToString() + "'");
            sb.Append("},items: [");

            int orderLineItemsCount = AnalyticsData.Order.OrderLineItems.Count;

            if (AnalyticsData.ShoppingCart != null)
            {
                foreach (ZNodeShoppingCartItem item in AnalyticsData.ShoppingCart.ShoppingCartItems)
                {
                    sb.Append("{");
                    sb.Append("product_id:");
                    sb.Append("'" + item.Product.SKU + "',");

                    sb.Append("unit_price:");
                    decimal orderLineItemPrice = 0;
                    if (item.PromotionalPrice != null && item.PromotionalPrice > 0)
                    {
                        orderLineItemPrice = (decimal)item.PromotionalPrice;
                    }
                    else if (item.UnitPrice > 0)
                    {
                        orderLineItemPrice = (decimal)item.UnitPrice;
                    }
                    sb.Append("'" + orderLineItemPrice.ToString("N2") + "',");
                    sb.Append("quantity:");
                    sb.Append("'" + item.Quantity + "'");
                    count++;
                    if (count == orderLineItemsCount)
                    {
                        sb.Append("}");
                    }
                    else
                    {
                        sb.Append("},");
                    }

                }
            }
            sb.Append("],");
            sb.Append("discounts: [{");
            sb.Append("name:");
            if (AnalyticsData.Order.CouponCode != null)
            {
                sb.Append("'" + AnalyticsData.Order.CouponCode.ToString() + "',");
            }
            else
            {
                sb.Append("'',");
            }
            sb.Append("amount:");
            sb.Append("'" + AnalyticsData.Order.Discount.ToString("N2") + "'");
            sb.Append("}]");
            sb.Append("};");
            sb.AppendLine("</script>");
            return sb.ToString();
        }
    }
}

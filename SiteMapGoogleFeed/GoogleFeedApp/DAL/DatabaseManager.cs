#region Import Directives
using System;
using System.Data;
#endregion

namespace Zeon.Data
{
    public sealed class DatabaseManager : IDatabaseManager
    {
        #region ctor

        /// <summary>
        /// Initalize the Database Manager with Specific Provider.
        /// </summary>
        /// <param name="providerType">SQL / Access / ODBC / Oracle</param>
        public DatabaseManager(DataProvider providerType)
        {
            this.providerType = providerType;
        }

        /// <summary>
        /// Initalize the Database Manager with Specific Provider and Connection
        /// String.
        /// </summary>
        /// <param name="providerType">SQL / Access / ODBC / Oracle</param>
        /// <param name="connectionString">Database Connection String</param>
        public DatabaseManager(DataProvider providerType, string connectionString)
        {
            this.providerType = providerType;
            strConnection = connectionString;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Used to attached parameters to the Stored Procedure
        /// </summary>
        /// <param name="command">Stored Procedure / SQL Query</param>
        /// <param name="commandParameters">Parameter List</param>
        private void AttachParameters(IDbCommand command, IDbDataParameter[] commandParameters)
        {
            foreach (IDbDataParameter idbParameter in commandParameters)
            {
                if ((idbParameter.Direction == ParameterDirection.InputOutput)
                    &&
                    (idbParameter.Value == null))
                {
                    idbParameter.Value = DBNull.Value;
                }
                command.Parameters.Add(idbParameter);
            }
        }

        /// <summary>
        /// Prepare the Database Command as per given parameters
        /// </summary>
        /// <param name="command">Generic Command Instance</param>
        /// <param name="connection">SQL Database Connection</param>
        /// <param name="transaction">Valid Transaction</param>
        /// <param name="commandType">Stored Procdure / Table / Query</param>
        /// <param name="commandText">SQL Query / Stored Procedure</param>
        /// <param name="commandParameters">Stoted Procedure Parameter</param>
        private void PrepareCommand(IDbCommand command, IDbConnection connection, IDbTransaction transaction, CommandType commandType, string commandText, IDbDataParameter[] commandParameters)
        {
            command.Connection = connection;
            command.CommandText = commandText;
            command.CommandType = commandType;

            if (transaction != null)
            {
                command.Transaction = transaction;
            }

            if (commandParameters != null)
            {
                AttachParameters(command, commandParameters);
            }
        }

        #endregion

        #region IDatabaseManager Members

        #region Private Fields
        private IDbConnection idbConnection;
        private IDataReader idataReader;
        private IDbCommand idbCommand;
        private DataProvider providerType;
        private IDbTransaction idbTransaction;
        private IDbDataParameter[] idbParameters;
        private string strConnection;
        #endregion

        #region Properties

        /// <summary>
        /// Holds the Database Provider Type
        /// I.e Datasource is SQL Server or MS Access or Oracle
        /// </summary>
        public DataProvider ProviderType
        {
            get
            {
                return providerType;
            }
            set
            {
                providerType = value;
            }
        }

        /// <summary>
        /// Holds the Database Connection String
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return strConnection;
            }
            set
            {
                strConnection = value;
            }
        }

        /// <summary>
        /// Holds the Generic Database Connection
        /// </summary>
        public IDbConnection Connection
        {
            get
            {
                return idbConnection;
            }
        }

        /// <summary>
        /// Holds the Database Transaction Instance
        /// </summary>
        public IDbTransaction Transaction
        {
            get
            {
                return idbTransaction;
            }
        }

        /// <summary>
        /// Holds the Data Reader
        /// </summary>
        public IDataReader DataReader
        {
            get
            {
                return idataReader;
            }
            set
            {
                idataReader = value;
            }
        }

        /// <summary>
        /// Hold the Database Command
        /// </summary>
        public IDbCommand Command
        {
            get
            {
                return idbCommand;
            }
        }

        /// <summary>
        /// Holds the Store Procedure Parameter
        /// </summary>
        public IDbDataParameter[] Parameters
        {
            get
            {
                return idbParameters;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Opens The Database
        /// </summary>
        public void Open()
        {
            idbConnection = DatabaseManagerFactory.GetConnection(providerType);
            idbConnection.ConnectionString = ConnectionString;
            if (idbConnection.State != ConnectionState.Open)
                idbConnection.Open();
            idbCommand = DatabaseManagerFactory.GetCommand(ProviderType);
        }

        /// <summary>
        /// Starts a New Database Transaction
        /// </summary>
        public void BeginTransaction()
        {
            if (idbTransaction == null)
                idbTransaction =
                    DatabaseManagerFactory.GetTransaction(ProviderType);
            idbCommand.Transaction = idbTransaction;
        }


        /// <summary>
        /// Save the Current Transaction
        /// </summary>
        public void CommitTransaction()
        {
            if (idbTransaction != null)
                idbTransaction.Commit();
            idbTransaction = null;
        }

        /// <summary>
        /// Create Parameter for a Stored Procedure
        /// </summary>
        /// <param name="paramsCount">Total Number of Parameters to be Created</param>
        public void CreateParameters(int paramsCount)
        {
            idbParameters = new IDbDataParameter[paramsCount];
            idbParameters = DatabaseManagerFactory.GetParameters(ProviderType,
                                                                 paramsCount);
        }

        /// <summary>
        /// Adds the Parameters and its values 
        /// </summary>
        /// <param name="index">Index of Stored Procedure Parameter</param>
        /// <param name="paramName">Stored Procedure Paramter Name</param>
        /// <param name="objValue">Value of the Stored Procedure Parameter</param>
        public void AddParameters(int index, string paramName, object objValue)
        {
            if (index < idbParameters.Length)
            {
                idbParameters[index].ParameterName = paramName;
                idbParameters[index].Value = objValue;
            }
        }

        /// <summary>
        /// Executes the Stored Procedure or SQL Statement
        /// </summary>
        /// <param name="commandType">Stored Procedure / SQL Statement</param>
        /// <param name="commandText">SQL Query</param>
        /// <returns>Returns a Generic Datareader for given Command Type and Command Text</returns>
        public IDataReader ExecuteReader(CommandType commandType, string commandText)
        {
            idbCommand = DatabaseManagerFactory.GetCommand(ProviderType);
            idbCommand.Connection = Connection;
            PrepareCommand(idbCommand, Connection, Transaction,
                           commandType,
                           commandText, Parameters);
            DataReader = idbCommand.ExecuteReader();
            idbCommand.Parameters.Clear();
            return DataReader;
        }

        /// <summary>
        /// Executes the Stored Procedure or SQL Statement
        /// </summary>
        /// <param name="commandType">Stored Procedure / SQL Statement</param>
        /// <param name="commandText">SQL Query</param>
        /// <returns>Returns a Dataset for given Command Type and Command Text</returns>
        public DataSet ExecuteDataSet(CommandType commandType, string commandText)
        {
            idbCommand = DatabaseManagerFactory.GetCommand(ProviderType);
            PrepareCommand(idbCommand, Connection, Transaction,
                           commandType,
                           commandText, Parameters);
            IDbDataAdapter dataAdapter = DatabaseManagerFactory.GetDataAdapter
                (ProviderType);
            dataAdapter.SelectCommand = idbCommand;
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);
            idbCommand.Parameters.Clear();
            return dataSet;
        }

        /// <summary>
        /// Executes the Stored Procedure or SQL Statement
        /// </summary>
        /// <param name="commandType">Stored Procedure / SQL Statement</param>
        /// <param name="commandText">SQL Query</param>
        /// <returns>Returns a Object Value for given Command Type and Command Text</returns>
        public object ExecuteScalar(CommandType commandType, string commandText)
        {
            idbCommand = DatabaseManagerFactory.GetCommand(ProviderType);
            PrepareCommand(idbCommand, Connection, Transaction,
                           commandType,
                           commandText, Parameters);
            object returnValue = idbCommand.ExecuteScalar();
            idbCommand.Parameters.Clear();
            return returnValue;
        }

        /// <summary>
        /// Executes the Stored Procedure / SQL Statement
        /// </summary>
        /// <param name="commandType">Stored Procedure / SQL Statement</param>
        /// <param name="commandText">SQL Query</param>
        /// <returns>Returns a Integer Value as the result</returns>
        public int ExecuteNonQuery(CommandType commandType, string commandText)
        {
            idbCommand = DatabaseManagerFactory.GetCommand(ProviderType);
            PrepareCommand(idbCommand, Connection, Transaction,
                           commandType, commandText, Parameters);
            int returnValue = idbCommand.ExecuteNonQuery();
            idbCommand.Parameters.Clear();
            return returnValue;
        }

        /// <summary>
        /// Closes the Open Data Reader
        /// </summary>
        public void CloseReader()
        {
            if (DataReader != null)
                DataReader.Close();
        }

        /// <summary>
        /// Closes The Open Database Connection
        /// </summary>
        public void Close()
        {
            if (idbConnection.State != ConnectionState.Closed)
                idbConnection.Close();
        }
        #endregion

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Dispose Object
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Close();
            idbCommand = null;
            idbTransaction = null;
            idbConnection = null;
        }

        #endregion
    }
}
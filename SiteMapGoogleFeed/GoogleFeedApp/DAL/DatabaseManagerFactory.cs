#region Import Directives
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Data.OleDb;
#endregion

namespace Zeon.Data
{
    public static class DatabaseManagerFactory
    {
        #region ctor

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Used to Set the Database Connection as per Provider
        /// </summary>
        /// <param name="providerType">Database Provider Type can be SQL Server / Access / Oracle / ODBC</param>
        /// <returns>Generic Database Connection</returns>
        public static IDbConnection GetConnection(DataProvider providerType)
        {
            IDbConnection iDbConnection = null;
            switch (providerType)
            {
                case DataProvider.SqlServer:
                    iDbConnection = new SqlConnection();
                    break;
                case DataProvider.OleDb:
                    iDbConnection = new OleDbConnection();
                    break;
                case DataProvider.Odbc:
                    iDbConnection = new OdbcConnection();
                    break;
                case DataProvider.Oracle:
                    //iDbConnection = new OracleConnection();
                    break;
                default:
                    return null;
            }
            return iDbConnection;
        }

        /// <summary>
        /// Used to Set the Database Command to Execute
        /// </summary>
        /// <param name="providerType">Database Provider Type can be SQL Server / Access / Oracle / ODBC</param>
        /// <returns>Generic Command prepared using given Provider</returns>
        public static IDbCommand GetCommand(DataProvider providerType)
        {
            switch (providerType)
            {
                case DataProvider.SqlServer:
                    return new SqlCommand();
                case DataProvider.OleDb:
                    return new OleDbCommand();
                case DataProvider.Odbc:
                    return new OdbcCommand();
                //case DataProvider.Oracle:
                //return new OracleCommand();
                default:
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="providerType">Database Provider Type can be SQL Server / Access / Oracle / ODBC</param>
        /// <returns>Generic Data Adaptor using given Provider</returns>
        public static IDbDataAdapter GetDataAdapter(DataProvider providerType)
        {
            switch (providerType)
            {
                case DataProvider.SqlServer:
                    return new SqlDataAdapter();
                case DataProvider.OleDb:
                    return new OleDbDataAdapter();
                case DataProvider.Odbc:
                    return new OdbcDataAdapter();
                //case DataProvider.Oracle:
                //return new OracleDataAdapter();
                default:
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="providerType">Database Provider Type can be SQL Server / Access / Oracle / ODBC</param>
        /// <returns></returns>
        public static IDbTransaction GetTransaction(DataProvider providerType)
        {
            IDbConnection iDbConnection = GetConnection(providerType);
            IDbTransaction iDbTransaction = iDbConnection.BeginTransaction();
            return iDbTransaction;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="providerType">Database Provider Type can be SQL Server / Access / Oracle / ODBC</param>
        /// <returns></returns>
        public static IDataParameter GetParameter(DataProvider providerType)
        {
            IDataParameter iDataParameter = null;
            switch (providerType)
            {
                case DataProvider.SqlServer:
                    iDataParameter = new SqlParameter();
                    break;
                case DataProvider.OleDb:
                    iDataParameter = new OleDbParameter();
                    break;
                case DataProvider.Odbc:
                    iDataParameter = new OdbcParameter();
                    break;
                case DataProvider.Oracle:
                    //iDataParameter = new OracleParameter();
                    break;

            }
            return iDataParameter;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="providerType">Database Provider Type can be SQL Server / Access / Oracle / ODBC</param>
        /// <param name="paramsCount">Total Number of Parameters</param>
        /// <returns></returns>
        public static IDbDataParameter[] GetParameters(DataProvider providerType, int paramsCount)
        {
            IDbDataParameter[] idbParams = new IDbDataParameter[paramsCount];

            switch (providerType)
            {
                case DataProvider.SqlServer:
                    for (int i = 0; i < paramsCount; ++i)
                    {
                        idbParams[i] = new SqlParameter();
                    }
                    break;
                case DataProvider.OleDb:
                    for (int i = 0; i < paramsCount; ++i)
                    {
                        idbParams[i] = new OleDbParameter();
                    }
                    break;
                case DataProvider.Odbc:
                    for (int i = 0; i < paramsCount; ++i)
                    {
                        idbParams[i] = new OdbcParameter();
                    }
                    break;
                //case DataProvider.Oracle:
                //    for (int i = 0; i <  ParamsLength; ++i)
                //    {
                //        //idbParams[i] = newOracleParameter();
                //    }
                //    break;
                default:
                    idbParams = null;
                    break;
            }
            return idbParams;
        }

        #endregion
    }
}


#region Import Directives
using System;
using System.Data;
using Zeon.Data;
#endregion

namespace Zeon.Data
{
    public interface IDatabaseManager : IDisposable
    {

        #region Properties

        /// <summary>
        /// Holds the Database Provider Type
        /// I.e Datasource is SQL Server or MS Access or Oracle
        /// </summary>
        DataProvider ProviderType
        {
            get;
            set;
        }

        /// <summary>
        /// Holds the Database Connection String
        /// </summary>
        string ConnectionString
        {
            get;
            set;
        }


        /// <summary>
        /// Holds the Generic Database Connection
        /// </summary>
        IDbConnection Connection
        {
            get;
        }

        /// <summary>
        /// Holds the Database Transaction Instance
        /// </summary>
        IDbTransaction Transaction
        {
            get;
        }

        /// <summary>
        /// Holds the Data Reader
        /// </summary>
        IDataReader DataReader
        {
            get;
        }

        /// <summary>
        /// Hold the Database Command
        /// </summary>
        IDbCommand Command
        {
            get;
        }

        /// <summary>
        /// Holds the Store Procedure Parameter
        /// </summary>
        IDbDataParameter[] Parameters
        {
            get;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Opens The Database
        /// </summary>
        void Open();

        /// <summary>
        /// Starts a New Database Transaction
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// Save the Current Transaction
        /// </summary>
        void CommitTransaction();

        /// <summary>
        /// Create Parameter for a Stored Procedure
        /// </summary>
        /// <param name="paramsCount">Total Number of Parameters to be Created</param>
        void CreateParameters(int paramsCount);

        /// <summary>
        /// Adds the Parameters and its values 
        /// </summary>
        /// <param name="index">Index of Stored Procedure Parameter</param>
        /// <param name="paramName">Stored Procedure Paramter Name</param>
        /// <param name="objValue">Value of the Stored Procedure Parameter</param>
        void AddParameters(int index, string paramName, object objValue);

        /// <summary>
        /// Executes the Stored Procedure or SQL Statement
        /// </summary>
        /// <param name="commandType">Stored Procedure / SQL Statement</param>
        /// <param name="commandText">SQL Query</param>
        /// <returns>Returns a Generic Datareader for given Command Type and Command Text</returns>
        IDataReader ExecuteReader(CommandType commandType, string commandText);

        /// <summary>
        /// Executes the Stored Procedure or SQL Statement
        /// </summary>
        /// <param name="commandType">Stored Procedure / SQL Statement</param>
        /// <param name="commandText">SQL Query</param>
        /// <returns>Returns a Dataset for given Command Type and Command Text</returns>
        DataSet ExecuteDataSet(CommandType commandType, string commandText);

        /// <summary>
        /// Executes the Stored Procedure or SQL Statement
        /// </summary>
        /// <param name="commandType">Stored Procedure / SQL Statement</param>
        /// <param name="commandText">SQL Query</param>
        /// <returns>Returns a Object Value for given Command Type and Command Text</returns>
        object ExecuteScalar(CommandType commandType, string commandText);

        /// <summary>
        /// Executes the Stored Procedure / SQL Statement
        /// </summary>
        /// <param name="commandType">Stored Procedure / SQL Statement</param>
        /// <param name="commandText">SQL Query</param>
        /// <returns>Returns a Integer Value as the result</returns>
        int ExecuteNonQuery(CommandType commandType, string commandText);

        /// <summary>
        /// Closes the Open Data Reader
        /// </summary>
        void CloseReader();

        /// <summary>
        /// Closes The Open Database Connection
        /// </summary>
        void Close();

        /// <summary>
        /// Dispose Object
        /// </summary>
        new void Dispose();
        #endregion
    }

}

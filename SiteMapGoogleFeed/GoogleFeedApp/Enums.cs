
namespace Zeon.Data
{
    /// <summary>
    /// Used to represent the different types of Data Providers
    /// for example Oracle, SQL Server, OleDB, Odbc etc
    /// </summary>
    public enum DataProvider
    {
        /// <summary>
        /// Used to Indicate Data Provider is Oracle
        /// </summary>
        Oracle,

        /// <summary>
        /// Used to Indicate Data Provider is SQL Server
        /// </summary>
        SqlServer,

        /// <summary>
        /// Used to Indicate Data Provider is OleDb
        /// </summary>
        OleDb,

        /// <summary>
        /// Used to Indicate Data Provider is Odbc
        /// </summary>
        Odbc
    }
}

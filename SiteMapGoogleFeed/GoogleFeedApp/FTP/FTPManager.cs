﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace SearchFeedApp.FTP
{
    public class FTPManager
    {
        #region Methods

        /// <summary>
        /// Uploaded the File to FTP Location
        /// </summary>
        /// <returns>True when upload is successful, else false</returns>
        public bool UploadFile()
        {
            try
            {
                //Create FTP request
                FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(FTPAddress + "/" + Path.GetFileName(FilePath));

                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(UserName, Password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Load the file
                using (FileStream stream = File.OpenRead(FilePath))
                {
                        byte[] buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, buffer.Length);
                        stream.Close();

                        //Upload file
                        Stream reqStream = request.GetRequestStream();
                        reqStream.Write(buffer, 0, buffer.Length);
                        reqStream.Close();
                        return true;                    
                }                
            }
            catch (Exception ex)
            {
                throw ex;
                //Write to Log
            }
        }
        #endregion 
        #region Properties


        #region private Members
        private string _ftpAddress;
        
        private string _filePath;
        
        private string _userName;
        
        private string _password;

        private string _uploadLocation;
        #endregion

        public string FTPAddress
        {
            get
            {
                return _ftpAddress;
            }
            set
            {
                _ftpAddress = value;
            }
        }

        public string FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }

               
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SearchFeedApp.FTP
{
    interface IFTPManager
    {
        
        string FTPAddress {get; set;}
        
        string FilePath {get; set;}
        
        string UserName {get; set;}
        
        string Password {get; set;}

        bool UploadFile();
        
    }
}

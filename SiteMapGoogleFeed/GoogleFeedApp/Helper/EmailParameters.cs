﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SearchFeedApp.Helper
{
    public class EmailParameters
    {

        public string Host { get; set; }

        public int Port { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string To { get; set; }

        public string CC { get; set; }

        public string From { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }


    }
}

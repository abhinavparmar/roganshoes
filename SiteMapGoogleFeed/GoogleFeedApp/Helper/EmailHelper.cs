﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace SearchFeedApp.Helper
{
    public class EmailHelper
    {

        /// <summary>
        /// Send Email for Status Update
        /// </summary>
        /// <param name="emailOptions"></param>
        /// <returns></returns>
        public static bool SendEmail(EmailParameters emailOptions)
        {            
            
            MailMessage mailMessage = new MailMessage();

            
            MailAddress fromMailAddress = new MailAddress(emailOptions.From);
            mailMessage.From = fromMailAddress;

            string[] emailAddress = emailOptions.To.Split(';');
            
            for (int i = 0; i < emailAddress.Length; i++)
            {
                MailAddress toMailAddress = new MailAddress(emailAddress[i]);
                mailMessage.To.Add(toMailAddress);
            }
            
            mailMessage.Subject = emailOptions.Subject;
            mailMessage.Body = emailOptions.Body;
            mailMessage.BodyEncoding = Encoding.UTF8;
            mailMessage.Priority = MailPriority.High;
            mailMessage.IsBodyHtml = false;

            SmtpClient smtpClient = new SmtpClient(emailOptions.Host, emailOptions.Port);
            //smtpClient.EnableSsl = true;
            smtpClient.EnableSsl = Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["EnableSSL"]);

            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new NetworkCredential(emailOptions.Username, emailOptions.Password);
            try
            {
                smtpClient.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeon.Data;
using System.Configuration;
using System.Data;
using System.Xml;
using SearchFeedApp.FTP;
using System.IO;
using System.Reflection;
using SearchFeedApp.Helper;
namespace GoogleFeedApp
{
    class Program
    {
        private static StringBuilder logMessage;

        static void Main(string[] args)
        {
            string appPath = AppDomain.CurrentDomain.BaseDirectory;

            logMessage = new StringBuilder();
            using (IDatabaseManager dbManager = new DatabaseManager(DataProvider.SqlServer))
            {
                // Get DB Connection String
                dbManager.ConnectionString = ConfigurationSettings.AppSettings["ConnectionString"];
                try
                {
                    // Open Database
                    logMessage.Append(DateTime.Now.ToString() + ":Connecting to database..." + Environment.NewLine);
                    dbManager.Open();

                    // Create Parameter for Stored Procedure
                    logMessage.Append(DateTime.Now.ToString() + ":Initializing Stored Procedure Parameters" + Environment.NewLine);
                    dbManager.CreateParameters(2);

                    // Add Stored Procedure Parameter Value
                    dbManager.AddParameters(0, "@url", ConfigurationSettings.AppSettings["StoreURL"]);
                    dbManager.AddParameters(1, "@PortalID", ConfigurationSettings.AppSettings["PortalID"]);

                    // Execute Stored Procedure
                    string siteMapNamespace = string.Empty;
                    logMessage.Append(DateTime.Now.ToString() + ":Executing Stored Procedure" + Environment.NewLine);
                    //IDataReader reader = dbManager.ExecuteReader(CommandType.StoredProcedure, "Zeon_SiteMapFeed");
                    DataSet dsUrlList = dbManager.ExecuteDataSet(CommandType.StoredProcedure, "Zeon_SiteMapFeed");
                    logMessage.Append(DateTime.Now.ToString() + ":Current PortalID :" + ConfigurationSettings.AppSettings["PortalID"] + Environment.NewLine);

                    logMessage.Append(DateTime.Now.ToString() + ":Reading XML Data" + Environment.NewLine);

                    if (dsUrlList != null && dsUrlList.Tables.Count > 0 && dsUrlList.Tables[0].Rows.Count > 0)
                    {
                        if (ConfigurationSettings.AppSettings["SiteMapNameSpace"] != null)
                        {
                            siteMapNamespace = ConfigurationSettings.AppSettings["SiteMapNameSpace"];
                        }
                        CreateXmlSiteMap(dsUrlList, "urlset", siteMapNamespace, "sitemap");
                    }
                    else
                    {
                        Console.WriteLine(DateTime.Now + ":Stored Procedure return 0 record count.");
                    }


                    //if (reader.Read()) // Check if reader has data
                    //{

                    //    string filename =ConfigurationSettings.AppSettings["ResultFileName"];

                    //    // Save Response as XML on specified location
                    //    SaveResponseAsXML(reader, filename);
                    //}
                    //else
                    //{
                    //    throw new Exception("Invalid or Empty Response");
                    //}
                    dbManager.Close();
                    logMessage.Append(DateTime.Now.ToString() + ":Performing Cleanup...Done" + Environment.NewLine);
                    LogWritter.Write(appPath + "ApplicationLog.txt", logMessage);
                    string body = "*****SiteMap Feed Submission Log Begin On*****" + DateTime.Now.ToShortDateString() + Environment.NewLine + logMessage.ToString();
                    SetEmailOptions(ConfigurationSettings.AppSettings["FeedGenerationSuccessEmailSubject"] + DateTime.Now.ToShortDateString(), body);
                }
                catch (Exception ex)
                {
                    logMessage.Append(DateTime.Now.ToString() + ":" + Environment.NewLine + "Error: " + ex.Message);
                    logMessage.Append(Environment.NewLine + "*****End of Log File*****");

                    LogWritter.Write(appPath + "ApplicationLog.txt", logMessage);
                    string body = "*****Site MapSubmission Log Begin On*****" + DateTime.Now.ToShortDateString() + Environment.NewLine + logMessage.ToString();
                    SetEmailOptions(ConfigurationSettings.AppSettings["FeedGenerationFailedEmailSubject"] + DateTime.Now.ToShortDateString(), body);

                }
            }
        }

        #region Private Methods

        /// <summary>
        /// Save Response from Reader into XML file
        /// </summary>
        /// <param name="reader">SQL Data Reader</param>
        /// <param name="appPath">App Executable Path</param>
        /// <param name="filename">XML Filename</param>
        private static void SaveResponseAsXML(IDataReader reader, string filename)
        {

            string productString = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
            "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">" +
            "@" +
            "</urlset>";

            logMessage.Append(DateTime.Now.ToString() + ":Processing XML Response" + Environment.NewLine);
            productString = productString.Replace("@", Convert.ToString(reader.GetValue(0)));
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(productString);
            logMessage.Append(DateTime.Now.ToString() + ":Saving XML Response" + Environment.NewLine);
            xmlDocument.Save(filename);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appPath"></param>
        /// <param name="uploadFileName"></param>
        /// <returns></returns>
        private static bool UploadFiletoFTP(string appPath, string uploadFileName)
        {
            FTPManager ftpManager = new FTPManager();
            ftpManager.UserName = ConfigurationSettings.AppSettings["Username"];
            ftpManager.Password = ConfigurationSettings.AppSettings["Password"];
            ftpManager.FTPAddress = ConfigurationSettings.AppSettings["FTPAddress"];
            ftpManager.FilePath = appPath + uploadFileName;
            logMessage.Append(DateTime.Now.ToString() + ":Uploading File to FTP" + Environment.NewLine);
            bool result = ftpManager.UploadFile();
            logMessage.Append(DateTime.Now.ToString() + ":File Uploaded to FTP" + Environment.NewLine);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        private static void SetEmailOptions(string subject, string body)
        {
            EmailParameters emailOptions = new EmailParameters();
            emailOptions.Host = ConfigurationSettings.AppSettings["Host"]; ;
            emailOptions.Port = Convert.ToUInt16(ConfigurationSettings.AppSettings["Port"]);
            emailOptions.To = ConfigurationSettings.AppSettings["To"];
            emailOptions.From = ConfigurationSettings.AppSettings["From"];

            emailOptions.Username = ConfigurationSettings.AppSettings["EmailUsername"];
            emailOptions.Password = ConfigurationSettings.AppSettings["EmailPassword"];
            emailOptions.Subject = subject;
            emailOptions.Body = body;
            EmailHelper.SendEmail(emailOptions);
        }

        private static void CreateXmlSiteMap(DataSet datasetValues, string rootTag, string rootTagValue, string xmlFileName)
        {
            try
            {
                int recordsCount = datasetValues.Tables[0].Rows.Count;
                int loopCnt = 0;
                int recCnt = 0;
                int fileCount = 0;
                string filePath = string.Empty;
                string sitemapPath = string.Empty;
                // Number of records to be generated in the file.
                if (ConfigurationSettings.AppSettings["XMLSiteMapRecordCount"] != null)
                {
                    recCnt = Convert.ToInt32(ConfigurationSettings.AppSettings["XMLSiteMapRecordCount"]);
                }

                if (ConfigurationSettings.AppSettings["ResultFileName"] != null)
                {
                    filePath = ConfigurationSettings.AppSettings["ResultFileName"];
                }

                if (ConfigurationSettings.AppSettings["SiteMapURL"] != null)
                {
                    sitemapPath = ConfigurationSettings.AppSettings["SiteMapURL"].ToString();
                }
                //Create Main file
                var requestMainXmlDoc = new XmlDocument();

                requestMainXmlDoc.AppendChild(requestMainXmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null));

                XmlElement urlsetMainElement = null;

                urlsetMainElement = requestMainXmlDoc.CreateElement(rootTag);
                var rootTagValues = rootTagValue.Split(',');
                for (var i = 0; i < rootTagValues.Count(); i++)
                {
                    var values = rootTagValues[i].Split('=');

                    urlsetMainElement.SetAttribute(values[0], values[1]);
                    break;
                }
                requestMainXmlDoc.AppendChild(urlsetMainElement);

                for (; loopCnt < recordsCount; )
                {
                    var fileName = string.Format("{0}{1}_{2}{3}", filePath, xmlFileName.Trim(), fileCount, ".xml");
                    //var fileName = string.Format("{0}{1}{2}", filePath, xmlFileName.Trim(), ".xml");

                    // Construct the XML for the Site Map creation
                    var requestXmlDoc = new XmlDocument();

                    requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null));

                    XmlElement urlsetElement = null;

                    urlsetElement = requestXmlDoc.CreateElement(rootTag);

                    for (var i = 0; i < rootTagValues.Count(); i++)
                    {
                        var values = rootTagValues[i].Split('=');

                        urlsetElement.SetAttribute(values[0], values[1]);
                    }

                    requestXmlDoc.AppendChild(urlsetElement);

                    // Loop thro the dataset values.
                    do
                    {
                        var dr = datasetValues.Tables[0].Rows[loopCnt];

                        var urlElement = requestXmlDoc.CreateElement("url");
                        urlsetElement.AppendChild(urlElement);
                        foreach (DataColumn dc in datasetValues.Tables[0].Columns.Cast<DataColumn>().Where(dc => !string.IsNullOrEmpty(dr[dc.ColumnName].ToString())))
                        {
                            urlElement.AppendChild(MakeElement(requestXmlDoc, dc.ColumnName, dr[dc.ColumnName].ToString()));
                        }

                        loopCnt++;
                    }
                    while (loopCnt < recordsCount && (loopCnt + 1) % recCnt != 0);

                    //Writes xml file to server
                    WriteXml(requestXmlDoc.OuterXml, fileName);
                    string childFileName = string.Format("{0}{1}_{2}{3}", sitemapPath, xmlFileName.Trim(), fileCount, ".xml");
                    var urlMainElement = requestMainXmlDoc.CreateElement("url");
                    urlsetMainElement.AppendChild(urlMainElement);
                    urlMainElement.AppendChild(MakeElement(requestMainXmlDoc, "loc", childFileName));
                    urlMainElement.AppendChild(MakeElement(requestMainXmlDoc, "lastmod", DateTime.Now.ToString()));
                    //requestMainXmlDoc.AppendChild(urlMainElement);
                    // Increment the file count if the file has to be splitted.
                    fileCount++;
                }
                WriteXml(requestMainXmlDoc.OuterXml, string.Format("{0}{1}", filePath, "sitemapindex.xml"));
            }
            catch (Exception)
            {
            }
        }

        private static XmlElement MakeElement(XmlDocument doc, string tagName, string tagValue)
        {
            XmlElement elem;

            if (tagName.Contains("g:"))
            {
                elem = doc.CreateElement("g", tagName.Split(':')[1], "http://base.google.com/ns/1.0");
            }
            else
            {
                elem = doc.CreateElement(tagName);
            }

            elem.InnerText = tagValue;

            return elem;
        }

        private static void WriteXml(string fileData, string filePath)
        {
            //Delete file if exist
            var fileInfo = new System.IO.FileInfo(filePath);

            if (fileInfo.Exists)
            {
                fileInfo.Delete();
                Console.WriteLine(filePath + " deleted successfully....." + Environment.NewLine);
            }

            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(fileData);
            xmlDocument.Save(filePath);
            Console.WriteLine(filePath + " created successfully....." + Environment.NewLine);
        }
        #endregion
    }
}

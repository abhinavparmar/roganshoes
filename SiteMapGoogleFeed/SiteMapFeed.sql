CREATE PROCEDURE [dbo].[Zeon_SiteMapFeed]                     
(@url  varchar(max))
AS            
BEGIN                    
	SET NOCOUNT OFF                    

	--Create Temp Table to Hold Query results in XML Feed Format
	CREATE TABLE #tblSiteMapFeed                    
		(                      
		  loc		  NVARCHAR(MAX),        
		  lastmod     NVARCHAR(MAX),                        
		  changefreq  NVARCHAR(MAX),        
		  priority    NVARCHAR(MAX),        
		)                    
                     
    --Extract Products and Category Data and Insert in temp table                 
	INSERT INTO #tblSiteMapFeed                    
		(                    
		  loc,                    
		  lastmod,                    
		  changefreq,                     
		  priority
		)
	--Home Page
	Select @url + '/' + 'default.aspx' , REPLACE(CONVERT(VARCHAR(10), GETDATE(), 111) ,'/','-'), 'weekly' AS changefreq, '1.0' AS priority  
	Union All
	--Category
	Select @url + '/' + SEOURL + '.aspx' , REPLACE(CONVERT(VARCHAR(10), GETDATE(), 111) ,'/','-'), 'weekly' AS changefreq, '0.8' AS priority  From ZNodeCategory Where VisibleInd=1 And SEOURL is not null
    Union All
	--Products
	Select @url + '/' + SEOURL + '.aspx' , REPLACE(CONVERT(VARCHAR(10), UpdateDte, 111) ,'/','-'), 'weekly' AS changefreq, '0.7' AS priority  From ZNodeProduct Where ActiveInd = 1 And SEOURL is not null               
	Union ALL
	--Misc Pages
	Select @url + '/' + SEOURL + '.aspx' , REPLACE(CONVERT(VARCHAR(10), GETDATE(), 111) ,'/','-'), 'weekly' AS changefreq, '0.6' AS priority  
	From ZNodeContentPage Where ActiveInd = 1 And SEOURL is not null                   
   	--Contact us
	Union All
	Select @url + '/' + 'contact-us.aspx' , REPLACE(CONVERT(VARCHAR(10), GETDATE(), 111) ,'/','-'), 'weekly' AS changefreq, '0.5' AS priority  
	
            
	--Return XML Output From Temp Table
	SELECT  *  FROM   #tblSiteMapFeed as [url]  
	FOR XML AUTO, TYPE, ELEMENTS                    
	
	
	--Delete the temp table
	DROP TABLE #tblSiteMapFeed                     
END

	
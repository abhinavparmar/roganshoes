%serverString% = %2


if "%serverString%" EQU "" goto nonserverLoop
if "%serverString%" NEQ "" goto serverLoop

:serverLoop
for /f "delims=;" %%a in ("!serverString!") do set substring=%%a
	
	if "%1" == "1" (
		sc !substring! start "Znode Background Service" 
		
	) else (
		sc !substring! stop "Znode Background Service"
	
	}


:nonserverloop
if "%1" == "1" (
	sc start "Znode Background Service"
	
) else (
	sc stop "Znode Background Service"
	
)


	


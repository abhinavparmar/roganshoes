﻿namespace Znode.Lucene.Tasks
{
    public class Enums
    {
        public enum ProcessedStatus
        {
            InProcess = 1,
            Completed = 2,
            Failed = 3,
            Ignore = 4
        }
    }
}
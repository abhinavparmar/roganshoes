using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// The Monitor.aspx page is used to monitor the availability of the website. 
/// This page is a very lightweight page to ensure low server resources
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the Monitor Page class.
    /// </summary>
    public partial class Monitor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("TEST OK - " + System.DateTime.Now.ToString());
        }
    }
}
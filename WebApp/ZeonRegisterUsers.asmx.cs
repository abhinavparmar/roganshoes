﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
namespace WebApp
{
    /// <summary>
    /// Summary description for ZeonRegisterUsers
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ZeonRegisterUsers : System.Web.Services.WebService
    {
        #region Public Method

        [WebMethod(EnableSession = true)]
        public string RegisterUsers()
        {
            StringBuilder stringResponse = new StringBuilder();
            if (System.Configuration.ConfigurationManager.AppSettings["EnableOneTimeRegistration"] != null && System.Configuration.ConfigurationManager.AppSettings["EnableOneTimeRegistration"].ToString().Equals("1"))
            {
                string Status = string.Empty;
                int currentSourceSiteID = GetSourceSiteIDByPortal();
                if (currentSourceSiteID > 0)
                {
                    List<AsynchUser> dtAllUser = GetAllUser(currentSourceSiteID);
                    if (dtAllUser != null && dtAllUser.Count > 0)
                    {
                        //Get PO Profile
                        int poProfile = GetPOProfileID();
                        foreach (AsynchUser userRow in dtAllUser)
                        {
                            if (userRow != null)
                            {
                                ZeonCustomerImportStatus zeonCustomerImportStatus = new ZeonCustomerImportStatus();
                                zeonCustomerImportStatus = RegisterUser(userRow, poProfile);
                                if (zeonCustomerImportStatus != null && !string.IsNullOrEmpty(zeonCustomerImportStatus.ErrorReason)) { WriteRegisterLogFile(zeonCustomerImportStatus.ErrorReason); stringResponse.Append(string.Format("{0}:{1}</br>", "Customer ID: " + userRow.Customer_Id, zeonCustomerImportStatus.ErrorReason)).AppendLine().AppendLine(); }
                                //Insert values Zeon_CustomerImportStatus table
                                InsertCustomerImportStatus(zeonCustomerImportStatus);
                            }
                        }
                    }
                }
            }
            return stringResponse.ToString(); ;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// To Account confirmation Email
        /// </summary>
        /// <param name="Email">Email Id of the register user</param>
        ///
        private void SendAccountConfirmationEmail(string email, string password, bool emailOptIn)
        {
            try
            {
                string accountConfirmationEmailTemplate = GetAccountConfirmationEmailTemplate(email, password, emailOptIn);
                ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();

                if (!string.IsNullOrEmpty(accountConfirmationEmailTemplate))
                {
                    ZNodeEmail.SendEmail(email, ZNodeConfigManager.SiteConfig.CustomerServiceEmail, string.Empty, zeonMessageConfig.GetMessageKey("EmailConfirmationSubject"), accountConfirmationEmailTemplate, true);
                }
            }
            catch (Exception ex)
            {
                WriteRegisterLogFile("Register user web service email template sending problem(Mehtod name: SendAccountConfirmationEmail()):" + ex.StackTrace);
            }
        }

        /// <summary>
        /// To Get Account Confriumation email template with body content
        /// </summary>
        /// <returns></returns>
        private string GetAccountConfirmationEmailTemplate(string username, string password, bool emailOptIn)
        {
            string defaultTemplatePath = string.Empty;
            bool isChecked = false;

            username = username.Trim().Replace("_" + ZNodeConfigManager.SiteConfig.PortalID.ToString(), "");
            isChecked = emailOptIn;

            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ZAccountConfirmationEmail.html"));

            StreamReader streamReader = new StreamReader(defaultTemplatePath);
            string messageText = streamReader.ReadToEnd();

            Regex rxUserName = new Regex("#UserName#", RegexOptions.IgnoreCase);
            messageText = rxUserName.Replace(messageText, username);

            Regex rxPassword = new Regex("#Password#", RegexOptions.IgnoreCase);
            messageText = rxPassword.Replace(messageText, password);

            Regex rxisChecked = new Regex("#not#", RegexOptions.IgnoreCase);

            messageText = isChecked ? rxisChecked.Replace(messageText, " ") : rxisChecked.Replace(messageText, "not ");
            return messageText;
        }

        /// <summary>
        /// Get all users from table
        /// </summary>
        /// <returns>List<AsynchUser> </returns>
        private List<AsynchUser> GetAllUser(int sourceSiteId)
        {
            List<AsynchUser> AllUserList = new List<AsynchUser>();
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter command = new SqlDataAdapter("Zeon_GetAllUser", connection);

                // Mark the command as store procedure
                command.SelectCommand.CommandType = CommandType.StoredProcedure;
                command.SelectCommand.CommandTimeout = 0;

                // Add parameters to command object                
                command.SelectCommand.Parameters.AddWithValue("@SourceSiteID", sourceSiteId);

                // Create and Fill the datatable
                DataTable dtAllUser = new DataTable();

                command.Fill(dtAllUser);

                command.Dispose();

                // close connection
                connection.Close();
                if (dtAllUser != null && dtAllUser.Rows.Count > 0)
                {
                    //Get ManufacturerExtn Data Table into ManufacturerExtn List Object
                    AllUserList = zConvertDataTableToList.ConvertTo<AsynchUser>(dtAllUser);
                }

            }
            return AllUserList;
        }

        /// <summary>
        /// Register single user 
        /// </summary>
        /// <param name="currentUser">DataRow</param>
        /// <returns>string</returns>
        private ZeonCustomerImportStatus RegisterUser(AsynchUser currentUser, int poProfile)
        {
            string responseString = string.Empty;
            ZeonCustomerImportStatus customerImportStatus = new ZeonCustomerImportStatus();
            try
            {
                if (!string.IsNullOrEmpty(currentUser.login) && !string.IsNullOrEmpty(currentUser.Password))
                {
                    string newUsername = currentUser.login.ToString().Trim() + "_" + ZNodeConfigManager.SiteConfig.PortalID;
                    int profileId = ZNodeProfile.DefaultRegisteredProfileId;
                    //Assign Property TO Import Status
                    customerImportStatus.PortalId = ZNodeConfigManager.SiteConfig.PortalID;
                    customerImportStatus.Username = currentUser.login.ToString().Trim();
                    customerImportStatus.CreateDate = System.DateTime.Now;
                    customerImportStatus.EporiaID = int.Parse(currentUser.Customer_Id.ToString());
                    if (profileId == 0)
                    {
                        responseString = Resources.CommonCaption.CreateFailed;
                        customerImportStatus.ErrorReason = Resources.CommonCaption.CreateFailed;
                        customerImportStatus.LoggedIn = customerImportStatus.Status = false;

                        return customerImportStatus;
                    }

                    // Check if loginName already exists in DB
                    ZNodeUserAccount userAccount = new ZNodeUserAccount();
                    //Check if username is valid Email Address
                    if (!System.Text.RegularExpressions.Regex.IsMatch(currentUser.login, Resources.CommonCaption.EmailValidationExpression))
                    {

                        responseString = currentUser.login + ":EporiaID: " + currentUser.Customer_Id + ": " + Resources.CommonCaption.InvalidEmailAddress;
                        customerImportStatus.ErrorReason = currentUser.login + ":EporiaID: " + currentUser.Customer_Id + ": " + Resources.CommonCaption.InvalidEmailAddress;
                        customerImportStatus.LoggedIn = customerImportStatus.Status = false;
                        return customerImportStatus;
                    }
                    if (!userAccount.IsLoginNameAvailable(ZNodeConfigManager.SiteConfig.PortalID, newUsername))
                    {
                        responseString = currentUser.login + ":EporiaID: " + currentUser.Customer_Id + ": " + Resources.CommonCaption.AlreadyExistLogin;
                        customerImportStatus.ErrorReason = currentUser.login + ":EporiaID: " + currentUser.Customer_Id + ": " + Resources.CommonCaption.AlreadyExistLogin;
                        customerImportStatus.LoggedIn = customerImportStatus.Status = false;
                        return customerImportStatus;
                    }

                    // Create user membership
                    MembershipUser user = Membership.CreateUser(newUsername, currentUser.Password.Trim(), currentUser.Email);

                    if (!user.IsApproved)
                    {
                        responseString = currentUser.login + ":EporiaID: " + currentUser.Customer_Id + ": " + Resources.CommonCaption.CreateFailed;
                        customerImportStatus.ErrorReason = currentUser.login + ":EporiaID: " + currentUser.Customer_Id + ": " + Resources.CommonCaption.CreateFailed;
                        customerImportStatus.LoggedIn = customerImportStatus.Status = false;
                        return customerImportStatus;
                    }

                    // Create the user account
                    ZNodeUserAccount newUserAccount = new ZNodeUserAccount();
                    newUserAccount.UserID = (Guid)user.ProviderUserKey;
                    newUserAccount.EmailOptIn = currentUser.IsEmailOptIn != null && currentUser.IsEmailOptIn.ToString().Equals("1") ? true : false;
                    newUserAccount.EmailID = currentUser.Email;

                    // Copy info to billing
                    Address billingAddress = new Address();
                    newUserAccount.BillingAddress = billingAddress;

                    // Copy info to shipping
                    Address shippingAddress = new Address();
                    newUserAccount.ShippingAddress = shippingAddress;

                    //Adding EporiaID
                    newUserAccount.Custom3 = currentUser.Customer_Id.ToString();

                    // Affiliate Settings
                    ZNodeTracking tracker = new ZNodeTracking();
                    string referralAffiliate = tracker.AffiliateId;
                    int referralAccountId = 0;

                    if (!string.IsNullOrEmpty(referralAffiliate))
                    {
                        if (int.TryParse(referralAffiliate, out referralAccountId))
                        {
                            ZNode.Libraries.DataAccess.Service.AccountService accountServ = new ZNode.Libraries.DataAccess.Service.AccountService();
                            ZNode.Libraries.DataAccess.Entities.Account account = accountServ.GetByAccountID(referralAccountId);

                            // Affiliate account exists
                            if (account != null)
                            {
                                if (account.ReferralStatus == "A")
                                {
                                    newUserAccount.ReferralAccountId = account.AccountID;
                                }
                            }
                        }
                    }

                    newUserAccount.AddUserAccount();

                    AddressService addressService = new AddressService();
                    billingAddress.Name = "Default Billing";
                    billingAddress.FirstName = currentUser.First_Name;
                    billingAddress.MiddleName = currentUser.Middle_Initial;
                    billingAddress.LastName = currentUser.Last_Name;
                    billingAddress.CompanyName = currentUser.Company;
                    billingAddress.Street = currentUser.Street1;
                    billingAddress.Street1 = currentUser.Street2 + currentUser.Street3;
                    billingAddress.City = currentUser.City;
                    billingAddress.StateCode = currentUser.State;
                    billingAddress.PostalCode = currentUser.Zip.ToString();
                    billingAddress.CountryCode = currentUser.Country;
                    billingAddress.PhoneNumber = currentUser.Phone != null && !string.IsNullOrEmpty(currentUser.Phone.ToString()) ? currentUser.Phone.ToString() : string.Empty;
                    billingAddress.IsDefaultBilling = true;
                    billingAddress.IsDefaultShipping = false;
                    billingAddress.AccountID = newUserAccount.AccountID;
                    addressService.Insert(billingAddress);
                    newUserAccount.SetBillingAddress(billingAddress.AddressID);

                    shippingAddress.Name = "Default Shipping";
                    shippingAddress.FirstName = currentUser.First_Name;
                    shippingAddress.MiddleName = currentUser.Middle_Initial;
                    shippingAddress.LastName = currentUser.Last_Name;
                    shippingAddress.CompanyName = currentUser.Company;
                    shippingAddress.Street = currentUser.Street1;
                    shippingAddress.Street1 = currentUser.Street2 + currentUser.Street3;
                    shippingAddress.City = currentUser.City;
                    shippingAddress.StateCode = currentUser.State.ToString();
                    shippingAddress.PostalCode = currentUser.Zip.ToString();
                    shippingAddress.CountryCode = currentUser.Country;
                    shippingAddress.PhoneNumber = currentUser.Phone2 != null && !string.IsNullOrEmpty(currentUser.Phone2.ToString()) ? currentUser.Phone2.ToString() : string.Empty; ;
                    shippingAddress.AccountID = newUserAccount.AccountID;
                    shippingAddress.IsDefaultBilling = false;
                    shippingAddress.IsDefaultShipping = true;
                    addressService.Insert(shippingAddress);
                    newUserAccount.SetShippingAddress(shippingAddress.AddressID);

                    // Add Account Profile
                    AccountProfile accountProfile = new AccountProfile();
                    accountProfile.AccountID = newUserAccount.AccountID;
                    accountProfile.ProfileID = profileId;

                    AccountProfileService accountProfileServ = new AccountProfileService();
                    accountProfileServ.Insert(accountProfile);

                    //Assign PO Profile
                    if (currentUser.IsPOAffiliated != null && currentUser.IsPOAffiliated.ToString().Equals("1") && poProfile > 0)
                    {
                        AccountProfile accountPOProfile = new AccountProfile();
                        accountPOProfile.AccountID = newUserAccount.AccountID;
                        accountPOProfile.ProfileID = poProfile;
                        accountProfileServ.Insert(accountPOProfile);
                    }
                    // Login the user
                    ZNodeUserAccount userAcct = new ZNodeUserAccount();
                    bool loginSuccess = userAcct.Login(ZNodeConfigManager.SiteConfig.PortalID, newUsername, currentUser.Password.Trim());
                    if (loginSuccess)
                    {
                        // Set current user Profile.
                        userAcct.ProfileID = accountProfile.ProfileID.Value;

                        //// Log password for further debugging
                        ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, currentUser.Password);

                        // Get Profile entity for logged-in user profileId
                        ZNode.Libraries.DataAccess.Service.ProfileService _ProfileService = new ZNode.Libraries.DataAccess.Service.ProfileService();
                        ZNode.Libraries.DataAccess.Entities.Profile _Profile = _ProfileService.GetByProfileID(userAcct.ProfileID);
                        customerImportStatus.ErrorReason = responseString;
                        customerImportStatus.Status = loginSuccess;
                        customerImportStatus.LoggedIn = loginSuccess;
                    }
                    else
                    {
                        responseString = currentUser.login + ":EporiaID: " + currentUser.Customer_Id + "Invalid UserID or Password,Login Failed!";
                    }
                }
                else
                {
                    //Insert Values in ZeonCustomerImportStatus table explcitely on Exception
                    customerImportStatus.ErrorReason = "Register user web service registration User name or Password is blank";
                    customerImportStatus.Status = customerImportStatus.LoggedIn = false;
                    InsertCustomerImportStatus(customerImportStatus);
                }
            }
            catch (Exception ex)
            {
                WriteRegisterLogFile("Register user web service registration problem(Mehtod name: RegisterUser()):" + ex.StackTrace);
                //Insert Values in ZeonCustomerImportStatus table explcitely on Exception
                customerImportStatus.ErrorReason = "Register user web service registration problem(Mehtod name: RegisterUser()):" + ex.StackTrace;
                customerImportStatus.Status = customerImportStatus.LoggedIn = false;
                InsertCustomerImportStatus(customerImportStatus);
            }
            return customerImportStatus;
        }


        /// <summary>
        /// Get PO Profile 
        /// </summary>
        /// <returns>int</returns>
        private int GetPOProfileID()
        {
            int poProfile = 0;
            if (System.Configuration.ConfigurationManager.AppSettings["POProfileName"] != null)
            {
                ProfileService profileService = new ProfileService();
                TList<Profile> prolileList = profileService.GetByName(System.Configuration.ConfigurationManager.AppSettings["POProfileName"].ToString().Trim());
                if (prolileList != null && prolileList.Count > 0)
                {
                    poProfile = prolileList[0].ProfileID;
                }
            }
            return poProfile;
        }

        /// <summary>
        /// Get Site Source ID by current portal
        /// </summary>
        /// <returns>int</returns>
        private int GetSourceSiteIDByPortal()
        {
            int portalSourceSiteId = 0;
            PortalService portalSer = new PortalService();
            Portal currentPortal = portalSer.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID);
            if (currentPortal != null && System.Configuration.ConfigurationManager.AppSettings["PortalSourceSiteID"] != null)
            {
                string[] allPortalSourceID = System.Configuration.ConfigurationManager.AppSettings["PortalSourceSiteID"].ToString().Split(',');
                foreach (string portal in allPortalSourceID)
                {
                    if (portal.Split('=')[0].Equals(currentPortal.StoreName) || currentPortal.StoreName.Contains(portal.Split('=')[0]) || portal.Split('=')[0].Contains(currentPortal.StoreName))
                    {
                        int.TryParse(portal.Split('=')[1], out portalSourceSiteId);
                        break;
                    }
                }
            }
            return portalSourceSiteId;
        }

        /// <summary>
        /// Insert Customer Import Status into database
        /// </summary>
        /// <param name="zeonCustomerImportStatus">ZeonCustomerImportStatus</param>
        private void InsertCustomerImportStatus(ZeonCustomerImportStatus zeonCustomerImportStatus)
        {
            try
            {
                ZeonCustomerImportStatusService importService = new ZeonCustomerImportStatusService();
                importService.Insert(zeonCustomerImportStatus);
            }
            catch (Exception ex)
            {
                WriteRegisterLogFile("Inserting data into Zeon_CustomerImportStatus problem(Mehtod name: InsertCustomerImportStatus()):" + ex.StackTrace);
            }

        }

        /// <summary>
        /// Create a Custom log file and write text to it
        /// </summary>
        /// <param name="text">string</param>
        private void WriteRegisterLogFile(string text)
        {
            StreamWriter log;
            string filepath = ZNodeConfigManager.EnvironmentConfig.DataPath + "/Logs/RegisterUserLog.log";
            if (!File.Exists(Server.MapPath(filepath)))
            {
                log = new StreamWriter(Server.MapPath(filepath));
            }
            else
            {
                log = File.AppendText(Server.MapPath(filepath));
            }
            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now);
            log.WriteLine("Portal ID:" + ZNodeConfigManager.SiteConfig.PortalID);
            log.WriteLine("Registration Status:" + text);
            log.WriteLine("===========================================");
            // Close the stream:
            log.Close();
        }
        #endregion

        #region Helper Classes

        private class AsynchUser
        {
            public object RowNumber { get; set; }
            public object Customer_Id { get; set; }
            public object Source_Site_Id { get; set; }
            public string login { get; set; }
            public string Password { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string First_Name { get; set; }
            public string Middle_Initial { get; set; }
            public string Last_Name { get; set; }
            public string Company { get; set; }
            public string Street1 { get; set; }
            public string Street2 { get; set; }
            public string Street3 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public double Zip { get; set; }
            public string Country { get; set; }
            public object Phone { get; set; }
            public object Phone2 { get; set; }
            public string Fax { get; set; }
            public object IsPOAffiliated { get; set; }
            public object IsEmailOptIn { get; set; }
            public DateTime Date_Created { get; set; }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace WebApp.CustomClasses.WebControls
{
    public class LinkButton : System.Web.UI.WebControls.LinkButton
    {
        public ButtonType ButtonType { get; set; }

        public ButtonPriority ButtonPriority { get; set; }

        protected override void AddAttributesToRender(System.Web.UI.HtmlTextWriter writer)
        {
            switch (ButtonType)
            {
                case ButtonType.Link:
                    CssClass += " link";
                    break;
                case ButtonType.LoginButton:
                    CssClass += " buttonlogin";
                    break;
                default:
                    CssClass += " button";
                    break;
            }

            switch (ButtonPriority)
            {
                case ButtonPriority.Primary:
                    CssClass += "";
                    break;
                default:
                    break;
            }

            base.AddAttributesToRender(writer);
        }

        protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
        {
            writer.Write("<span class='btn-wrap-out'><span class='btn-wrap-in'><span class='text'><span>{0}</span></span></span></span>", base.Text);
        }


        // To Fire Click event when Default Button configured on Panel.
        protected override void OnLoad(System.EventArgs e)
        {
            Page.ClientScript.RegisterStartupScript(GetType(), "addClickFunctionScript",
                _addClickFunctionScript, true);

            string script = String.Format(_addClickScript, ClientID);
            Page.ClientScript.RegisterStartupScript(GetType(), "click_" + ClientID,
                script, true);
            base.OnLoad(e);
        }

        private const string _addClickScript = "addClickFunction('{0}');";

        private const string _addClickFunctionScript =
            @"  function addClickFunction(id) {{
            var b = document.getElementById(id);
            if (b && typeof(b.click) == 'undefined') b.click = function() {{
                var result = true; if (b.onclick) result = b.onclick();
                if (typeof(result) == 'undefined' || result) {{ eval(b.getAttribute('href')); }}
            }}}};";
    }

    public enum ButtonType
    {
        Button,
        Link,
        LoginButton
    }

    public enum ButtonPriority
    {
        Normal,
        Primary,
        Secondary
    }
}
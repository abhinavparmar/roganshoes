﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using System.Configuration;

namespace WebApp 
{
    //zeon custom helper class
    
    public class ZCommonHelper
    {
        /// <summary>
        /// To Get Address in Proper Formate
        /// </summary>
        public static string GetAddressString(ZNode.Libraries.DataAccess.Entities.Address address)
        {
            StringBuilder sbHTMLAddress = new StringBuilder();
            if (address != null)
            {
                string addressName = string.Empty;
                //addressName = address.FirstName + " "+ address.LastName;
                addressName = address.Name;
                if (!string.IsNullOrEmpty(addressName))
                {
                    sbHTMLAddress.Append(addressName);
                    sbHTMLAddress.Append("<br>");
                }

                string street = address.Street + " " + address.Street1;
                street = street.Trim();
                if (!string.IsNullOrEmpty(street))
                {
                    sbHTMLAddress.Append(street);
                }

                sbHTMLAddress.Append(GetFormattedText(address.City));

                string postalDetails = string.Empty;
                postalDetails = address.StateCode + "," + address.PostalCode;
                postalDetails = postalDetails.Trim(',');
                sbHTMLAddress.Append(GetFormattedText(postalDetails));

                sbHTMLAddress.Append(GetFormattedText(address.CountryCode));
                sbHTMLAddress.Append(GetFormattedPhoneNumberText(address.PhoneNumber));
            }

            return sbHTMLAddress.ToString();
        }

        /// <summary>
        /// Get Formated Text
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public static string GetFormattedText(string strText)
        {
            if (!string.IsNullOrEmpty(strText))
            {
                return "<br>" + strText;
            }
            return string.Empty;
        }

        /// <summary>
        /// Get Formates Phone Number
        /// </summary>
        /// <param name="strPhone"></param>
        /// <returns></returns>
        public static string GetFormattedPhoneNumberText(string strPhone)
        {
            if (!string.IsNullOrEmpty(strPhone))
            {
                return "<br><strong>" + "Phone:</strong>" + " <div class='phn'>" + strPhone+"</div>";
            }
            return string.Empty;
        }

        /// <summary>
        /// Get formatted product name to avoid wired UI by setting max character limit
        /// </summary>
        /// <param name="strProductName"></param>
        /// <param name="productNameMaxLimit"></param>
        /// <returns></returns>
        public static string GetFormattedProductName(string strProductName, string productNameMaxLimit)
        {
            string strFormattedProductName = string.Empty;
            int productNameMaxLimitVal = Convert.ToInt32(productNameMaxLimit);
            if (strProductName.Length > productNameMaxLimitVal) 
            {
                strFormattedProductName = strProductName.Substring(0, productNameMaxLimitVal) + "...";
            }
            else
            {
                strFormattedProductName = strProductName;
            }
            return strFormattedProductName;
        }

        /// <summary>
        /// Create HTML for LOgo on Email 
        /// </summary>
        /// <returns>string</returns>
        public static string GetLogoHTML() 
        {
            string logoHTML = string.Empty;
            string path = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            ZNode.Libraries.ECommerce.Utilities.ZNodeImage zImage = new ZNode.Libraries.ECommerce.Utilities.ZNodeImage();
            string format = "<a title='{0}' href='{1}'><img src={2}></a>";
            logoHTML = string.Format(format, ZNodeConfigManager.SiteConfig.StoreName, path, path + zImage.GetImageBySize(ZNodeConfigManager.SiteConfig.MaxCatalogItemSmallWidth, System.IO.Path.GetFileName( ZNodeConfigManager.SiteConfig.LogoPath)).Replace("~", string.Empty));
            return logoHTML;
        }

        /// <summary>
        /// Find if current portal is Mulitiple attribute customize portal
        /// </summary>
        /// <returns>bool</returns>
        public static bool IsCustomMulitipleAttributePortal()
        {
            bool result = false;
            if (ConfigurationManager.AppSettings["CheerAndPomPortalID"] != null && !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString()))
            {
                string[] portalIdArray = ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString().Split(',');

                if (portalIdArray.Length > 0)
                {
                    foreach (string portalIdStr in portalIdArray)
                    {
                        if (!string.IsNullOrWhiteSpace(portalIdStr) && ZNodeConfigManager.SiteConfig.PortalID.ToString().Equals(portalIdStr))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }
    }
}
<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Admin_Secure_Default" Title="Untitled Page" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Dashboard">
        <div>
            <img id="Img1" src="~/SiteAdmin/Themes/images/clear.gif" runat="server" width="10" height="10"
                alt="" />
        </div>

        <!-- Welcome Message -->
        <div class="Caption">
            Welcome
            <asp:LoginName ID="uxLoginName" runat="server" />
        </div>
        <p class="h2-sub">
            Manage your stores, products, inventory, promotions, and orders.
        </p>
        <ul class="horizontal-nav callouts">
            <li class="setting">
                <a href="/SiteAdmin/Secure/Setup/Default.aspx">
                    <span class="wrap-out">
                        <span class="wrap-in">
                            <span class="text"><span id="dashheading">Setup</span><br />
                                <div id="dashtext">Create new stores, and configure shipping, payments and taxes.</div>
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="catalog">
                <a href="/SiteAdmin/Secure/Inventory/Default.aspx">
                    <span class="wrap-out">
                        <span class="wrap-in">
                            <span class="text"><span id="dashheading">Inventory</span><br />
                                <div id="dashtext">Manage your products and inventory.</div>
                            </span>
                        </span>
                    </span>
                </a>
            </li>

            <li class="market">
                <a href="/SiteAdmin/Secure/Marketing/default.aspx">
                    <span class="wrap-out">
                        <span class="wrap-in">
                            <span class="text"><span id="dashheading">Marketing</span><br />
                                <div id="dashtext">Manage promotions and optimize your store's search engine.</div>
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="service">
                <a href="/SiteAdmin/Secure/Orders/Default.aspx">
                    <span class="wrap-out">
                        <span class="wrap-in">
                            <span class="text"><span id="dashheading">Orders</span><br />
                                <div id="dashtext">Manage your orders, RMA, service requests, and reviews.</div>
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="optimize">
                <a href="/SiteAdmin/Secure/reports/report.aspx">
                    <span class="wrap-out">
                        <span class="wrap-in">
                            <span class="text"><span id="dashheading">Reports</span><br />
                                <div id="dashtext">Generate reports on sales, customers, and other metrics.</div>
                            </span>
                        </span>
                    </span>
                </a>
            </li>
        </ul>

        <div class="DashBoardRow">
            <div class="clearwrap">
                <div class="tips-alerts">
                    <h2>Alerts </h2>
                    <ul class="vertical-nav alerts">
                        <li class="alert" id="inventoryalert" runat="server">
                            <strong>
                                <asp:Label ID="inventoryText" runat="server"></asp:Label></strong>
                            <p>
                                <a id="lnkInventory" runat="server" href="/SiteAdmin/Secure/Reports/Report.aspx?mode=Inventory Re-Order">Manage Inventory</a>
                            </p>
                        </li>
                        <li class="alert" id="activityalert" runat="server">
                            <strong>
                                <asp:Label ID="loginText" runat="server"></asp:Label></strong>
                            <p>
                                <a id="lnkActivitylog" runat="server" href="/SiteAdmin/Secure/Reports/Report.aspx?mode=Activity Log">View Activity Log</a>
                            </p>
                        </li>
                    </ul>
                    <div class="Error">
                        <asp:Label ID="lblAlertError" Visible="false" runat="server"></asp:Label>
                    </div>
                    <ul class="vertical-nav tips">
                        <li class="linktext">
                            <p>
                                Create and manage multiple storefronts for your business. 
                              <a id="A7" href="/SiteAdmin/Secure/Setup/Storefront/Stores/Default.aspx" runat="server">Click here</a>
                            </p>
                        </li>
                        <li class="linktext">
                            <p>
                                Download and upload data to your store including inventory. 
                                <a id="A8" href="/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx" runat="server">Click here</a>
                            </p>
                        </li>
                    </ul>
                </div>

                <div class="orders">
                    <h2></h2>
                    <ul class="vertical-nav orders">
                        <li>
                            <strong>
                                <asp:Label ID="YTDSales" runat="server"></asp:Label></strong>
                            <p>Total Sales YTD</p>
                        </li>
                        <li>
                            <strong>
                                <asp:Label ID="YTDOrders" runat="server"></asp:Label></strong>
                            <p>Orders YTD</p>
                        </li>
                        <li>
                            <strong>
                                <asp:Label ID="YTDAccountsCreated" runat="server"></asp:Label></strong>
                            <p>Accounts Created YTD</p>
                        </li>
                        <li>
                            <p><a id="A4" href="~/SiteAdmin/Secure/reports/report.aspx" runat="server">Run Reports</a></p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

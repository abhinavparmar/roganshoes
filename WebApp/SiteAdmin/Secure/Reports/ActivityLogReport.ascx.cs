﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Reporting.WebForms;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reports_CActivityLogReport class
    /// </summary>
    public partial class Admin_Secure_Reports_CActivityLogReport : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private string Year = string.Empty;
        private string _ReportTitle;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the IsPostBack property
        /// </summary>
        public string IsPostback
        {
            get 
            { 
                return hdnIsPostBack.Value; 
            }

            set 
            { 
                hdnIsPostBack.Value = value; 
            }
        }

        /// <summary>
        /// Gets or sets the Mode
        /// </summary>
        public int Mode
        {
            get 
            { 
                return Convert.ToInt32(hdnMode.Value); 
            }

            set 
            { 
                hdnMode.Value = value.ToString(); 
            }
        }

        /// <summary>
        /// Gets or sets the ReportTitle
        /// </summary>
        public string ReportTitle
        {
            get 
            { 
                return this._ReportTitle; 
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }        
          
        }

        /// <summary>
        /// Page Pre Render Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(hdnIsPostBack.Value))
            {
                this.objReportViewer.Visible = false;
                this.IsPostback = "true";
                this.BindStores();

                if (this.Mode == 22)
                {
                    ddlPortal.SelectedIndex = -1;
                    txtStartDate.Text = System.DateTime.Today.ToString("MM/dd/yyyy");
                    this.ShowReport();
                }
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Reports/default.aspx");
        }

        /// <summary>
        /// Order Filter Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            this.ShowReport();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            hdnIsPostBack.Value = "false";
            ddlLogCategory.SelectedIndex = -1;
            ddlPortal.SelectedIndex = -1;
            txtStartDate.Text = string.Empty;
            this.objReportViewer.Visible = false;
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Bind store names
        /// </summary>
        private void BindStores()
        {
            // Load StoresList
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = "ALL STORES";
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();

            foreach (ListItem listItem in ddlPortal.Items)
            {
                listItem.Text = Server.HtmlDecode(listItem.Text);
            }
        }

        /// <summary>
        /// Display the activity log report based on parameter.
        /// </summary>
        private void ShowReport()
        {
            objReportViewer.Visible = true;
            ReportAdmin reportAdmin = new ReportAdmin();
            DateTime fromDate = DateTime.Parse(txtStartDate.Text);
            string category = ddlLogCategory.SelectedIndex == 0 ? string.Empty : ddlLogCategory.SelectedItem.Text;
            string portalId = ddlPortal.SelectedValue;
            DataSet ds = reportAdmin.ReportList(ZnodeReport.ActivityLog, fromDate, DateTime.Today, string.Empty, category, portalId);
            if (ds.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = "No records found";
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivityLogReport, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Activity Log - Report", "ActivityLog");

                objReportViewer.LocalReport.DataSources.Clear();
                ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
                objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 });
                objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZnodeActivityLog_ZnodeActivityLog", ds.Tables[0]));
                this.objReportViewer.PageCountMode = PageCountMode.Actual; 
            }
        }
        #endregion
    }
}
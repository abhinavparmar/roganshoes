﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Admin_Secure_Reports_CActivityLogReport"
    CodeBehind="ActivityLogReport.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<div class="ReportLayout">
    <h4 class="SubTitle">
        <asp:Label ID="lblTitle" runat="server" Text="Activity Log Report"></asp:Label>
    </h4>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:Panel ID="pnlCustom" runat="server">
        <div class="SearchForm">
            <div class="RowStyle">
                <div class="ItemStyle" style="width: 190px;">
                    <span class="FieldStyle">Store Name</span><br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlPortal" runat="server">
                        </asp:DropDownList>
                    </span>
                </div>
                <div class="ItemStyle" style="width: 140px;">
                    <span class="FieldStyle">Begin Date </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtStartDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="imgbtnStartDt"
                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<br />Enter Begin date"
            ControlToValidate="txtStartDate" ValidationGroup="grpReports" CssClass="Error"
            Text="" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
            CssClass="Error" Display="Dynamic" ErrorMessage="<br />Enter Valid Date in <br />MM/DD/YYYY format"
            Text="" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
            ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                    </span>
                </div>
                <div class="ItemStyle" style="width: 140px;">
                    <span class="FieldStyle">
                        <asp:Label ID="Label1" Text="Category" runat="server"></asp:Label>
                    </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlLogCategory" runat="server">
                            <asp:ListItem Value="0" Text="ALL"></asp:ListItem>
                            <asp:ListItem Value="1" Text="ERROR"></asp:ListItem>
                            <asp:ListItem Value="2" Text="NOTICE"></asp:ListItem>
                            <asp:ListItem Value="3" Text="SEARCH"></asp:ListItem>
                            <asp:ListItem Value="4" Text="WARNING"></asp:ListItem>
                        </asp:DropDownList>
                    </span>
                </div>
            </div>
        </div>
        <div align="left" class="ClearBoth">
            <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                PopupPosition="TopLeft" runat="server" TargetControlID="txtStartDate">
            </ajaxToolKit:CalendarExtender>
        </div>
        <asp:PlaceHolder ID="pnlSearch" runat="server" Visible="true">
            <div align="right" style="padding-right: 10px;">
                <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../Themes/Images/buttons/button_clear_highlight.gif';"
                    onmouseout="this.src='../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                    runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                <asp:ImageButton ID="btnOrderFilter" onmouseover="this.src='../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnOrderFilter_Click" CausesValidation="true"
                    ValidationGroup="grpReports" />
            </div>
        </asp:PlaceHolder>
    </asp:Panel>
    <div>
       
       
    </div>
    <div>
        <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server" EnableViewState="false"></asp:Label></div>
    <br />
    <div>
        <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%" AsyncRendering="false"
            Font-Names="Verdana" Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true"
            ShowCredentialPrompts="false" ShowDocumentMapButton="false" ShowFindControls="false"
            ShowPrintButton="true" ShowRefreshButton="false" ShowZoomControl="false">
            <LocalReport DisplayName="Report" ReportPath="SiteAdmin/Secure/Reports/ActivityLog.rdlc">
            </LocalReport>
        </rsweb:ReportViewer>
    </div>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:HiddenField runat="server" ID="hdnIsPostBack" Value="false" />
    <asp:HiddenField runat="server" ID="hdnMode" Value="12" />
</div>

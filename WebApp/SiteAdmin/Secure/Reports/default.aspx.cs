using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace SiteAdmin
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reports_default class
    /// </summary>
    public partial class Admin_Secure_Reports_default : System.Web.UI.Page
    {
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["SelectedMenuItem"] = null;
            Response.Redirect("Report.aspx");
        }
    }
}
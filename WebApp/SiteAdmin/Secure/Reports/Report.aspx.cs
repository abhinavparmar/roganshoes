﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reports_Report class
    /// </summary>
    public partial class Admin_Secure_Reports_Report : System.Web.UI.Page
    {
        #region Member Variables
        public const string RptOrder = "Orders";
        public const string RptRecurringBilling = "Recurring Billing";
        public const string RptAccounts = "Accounts";
        public const string RptBestSellers = "Best Sellers";
        public const string RptServiceRequest = "Service Requests";
        public const string RptEmailOptn = "Email Opt-In Customers";
        public const string RptInventory = "Inventory Re-Order";
        public const string RptTopSpending = "Top Spending Customers";
        public const string RptMostFrequent = "Most Frequent Customers";
        public const string RptTopEarning = "Top Earning Products";
        public const string RptOrderPickList = "Order Pick List";
        public const string RptCouponUsage = "Coupon Usage";
        public const string RptAffiliateOrders = "Affiliate Orders";
        public const string RptActivityLog = "Activity Log";
        public const string RptSalesTax = "Sales Tax";
        public const string RptSupplierList = "Supplier List";
        public const string RptVendorRevenue = "Franchise Orders";
        public const string RptVendorProductRevenue = "Franchise Sale By Product";
        public const string RptProductsSoldOnVendorSites = "Products Sold on Vendors Sites";
        public const string RptPopularSearch = "Popular Search";
        #endregion

        #region Public Methods
        /// <summary>
        /// Get SubMenu Css Method
        /// </summary>
        /// <param name="url">The value of url</param>
        /// <returns>Returns the css value </returns>
        public string GetSubmenuCss(object url)
        {
            if (hdnSelectedReport.Value == url.ToString())
            {
                return "ReportSelectedStyle";
            }
            else
            {
                return "ReportMenuItemStyle";
            }
        }

        /// <summary>
        /// Get SubMenu Name Method
        /// </summary>
        /// <param name="url">The value of url</param>
        /// <returns>Returns the Selected SubMenu name</returns>
        public string GetSubmenuName(object url)
        {
            if (hdnSelectedReport.Value == url.ToString())
            {
                return url.ToString() + " &raquo";
            }
            else
            {
                return url.ToString();
            }
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Mode"] != null)
            {
                Session["SelectedMenuItem"] = Request.QueryString["Mode"].ToString();
                Response.Redirect("Report.aspx");
            } 

            if (Session["SelectedMenuItem"] != null)
            {
                this.BindReports(Session["SelectedMenuItem"].ToString());
            }

            if (!IsPostBack)
            {
                hdnSelectedReport.Value = "Orders";
                if (Session["SelectedMenuItem"] != null)
                {
                    hdnSelectedReport.Value = Session["SelectedMenuItem"].ToString();
                    Session["SelectedMenuItem"] = null; 
                }
                uxOrderReport.ReportTitle = hdnSelectedReport.Value;
            }
        }

        /// <summary>
        /// Page Pre Render Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            ctrlSubMenu.DataBind();

            GC.Collect();
        }

        #endregion

        #region Protected Methods and Events
        
        /// <summary>
        /// Sub Menu Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void SubMenu_OnClick(object sender, EventArgs e)
        {
            this.ClearReportSession();

            LinkButton obj = sender as LinkButton;

            this.BindReports(obj.CommandArgument);
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Reports Method
        /// </summary>
        /// <param name="reportName">The value of reportName</param>
        private void BindReports(string reportName)
        {
            uxSupplierReport.Visible = false;
            uxTaxReport.Visible = false;
            uxActivityLogReport.Visible = false;
            uxInventoryReport.Visible = false;
            uxOrderReport.Visible = false;         
            uxRecurringBillingReport.Visible = false;
            uxPopularSearchReport.Visible = false;
            switch (reportName)
            {
                case RptOrder:
                    uxOrderReport.Visible = true;
                    uxOrderReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.Orders;
                    uxOrderReport.IsPostback = "false";
                    uxOrderReport.OrderStatusSelector = true;
                    uxOrderReport.DataBind();
                    hdnSelectedReport.Value = "Orders";
                    uxOrderReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptRecurringBilling:
                    uxRecurringBillingReport.Visible = true;
                    uxRecurringBillingReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.RecurringBilling;
                    uxRecurringBillingReport.IsPostback = "false";
                    uxRecurringBillingReport.OrderStatusSelector = true;
                    uxRecurringBillingReport.DataBind();
                    hdnSelectedReport.Value = "Recurring Billing";
                    uxRecurringBillingReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptAccounts:
                    uxOrderReport.Visible = true;
                    uxOrderReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.Accounts;
                    uxOrderReport.IsPostback = "false";
                    uxOrderReport.OrderStatusSelector = false;
                    uxOrderReport.DataBind();
                    hdnSelectedReport.Value = "Accounts";
                    uxOrderReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptBestSellers:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.BestSeller;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    hdnSelectedReport.Value = "Best Sellers";
                    uxInventoryReport.IntervalSelector = true;
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptServiceRequest:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.ServiceRequest;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = false;
                    hdnSelectedReport.Value = "Service Requests";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptEmailOptn:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.EmailOptInCustomer;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = false;
                    hdnSelectedReport.Value = "Email Opt-In Customers";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptInventory:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.ReOrder;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = false;
                    hdnSelectedReport.Value = "Inventory Re-Order";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptTopSpending:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.TopSpendingCustomer;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = true;
                    hdnSelectedReport.Value = "Top Spending Customers";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptMostFrequent:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.FrequentCustomer;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = true;
                    hdnSelectedReport.Value = "Most Frequent Customers";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptTopEarning:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.TopEarningProduct;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = true;
                    hdnSelectedReport.Value = "Top Earning Products";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptOrderPickList:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.Picklist;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = false;
                    hdnSelectedReport.Value = "Order Pick List";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptCouponUsage:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.CouponUsage;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    hdnSelectedReport.Value = "Coupon Usage";
                    uxInventoryReport.IntervalSelector = true;
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptAffiliateOrders:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.AffiliateOrder;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = true;
                    hdnSelectedReport.Value = "Affiliate Orders";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptActivityLog:
                    uxActivityLogReport.Visible = true;
                    uxActivityLogReport.Mode = 22;
                    uxActivityLogReport.IsPostback = "false";
                    uxActivityLogReport.DataBind();
                    hdnSelectedReport.Value = "Activity Log";
                    uxActivityLogReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptSalesTax:
                    uxTaxReport.Visible = true;
                    uxTaxReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.SalesTax;
                    uxTaxReport.IsPostback = "false";
                    uxTaxReport.DataBind();
                    hdnSelectedReport.Value = "Sales Tax";
                    uxTaxReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case RptSupplierList:
                    uxSupplierReport.Visible = true;
                    uxSupplierReport.IsPostback = "false";
                    uxSupplierReport.DataBind();
                    hdnSelectedReport.Value = "Supplier List";
                    uxSupplierReport.ReportTitle = hdnSelectedReport.Value;
                    break;               
                case RptPopularSearch:
                    uxPopularSearchReport.Visible = true;
                    uxPopularSearchReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.PopularSearch;
                    uxPopularSearchReport.IsPostback = "false";
                    uxPopularSearchReport.DataBind();
                    hdnSelectedReport.Value = "Popular Search";
                    uxPopularSearchReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                default:
                    uxOrderReport.Visible = true;
                    uxOrderReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.Orders;
                    uxOrderReport.IsPostback = "false";
                    uxOrderReport.OrderStatusSelector = true;
                    uxOrderReport.DataBind();
                    hdnSelectedReport.Value = "Orders";
                    uxOrderReport.ReportTitle = hdnSelectedReport.Value;
                    break;
            }
        }

        /// <summary>
        /// Clear Report Session Method
        /// </summary>
        private void ClearReportSession()
        {
            if (Session.Count > 0 && Session != null)
            {
                for (int reportSessionIndex = 0; reportSessionIndex < Session.Count; reportSessionIndex++)
                {
                    if (Session[reportSessionIndex] != null && Session[reportSessionIndex].GetType().ToString().Contains("Microsoft.Reporting.WebForms.ReportHierarchy"))
                    {
                        Session.RemoveAt(reportSessionIndex);
                    }
                }
            }

            GC.Collect();
        }
        #endregion
    }
}
<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Admin_Secure_Reports_default" Codebehind="default.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h1>Znode Reports</h1>
    <p>Use the links on this page to view various store reports. You can also write your own custom reports using the templates provided in your web project.</p>
    
	<div class="LandingPage">
		<hr />
		<div style="margin-right:10px;">
			<div class="Shortcut"><a id="A3" href="~/SiteAdmin/Secure/reports/ReportList.aspx?filter=12" runat="server">Orders</a></div>
			<div class="Shortcut"><a id="A12" href="~/SiteAdmin/Secure/reports/ReportList.aspx?filter=21" runat="server">Accounts</a></div> 
			<div class="Shortcut"><a id="A21" href="~/SiteAdmin/Secure/reports/InventoryReports.aspx?filter=20" runat="server">Best Sellers</a></div>  
			<div class="Shortcut"><a id="A20" href="~/SiteAdmin/Secure/reports/InventoryReports.aspx?filter=19" runat="server">Service Requests</a></div>  
			<div class="Shortcut"><a id="A15" href="~/SiteAdmin/Secure/reports/InventoryReports.aspx?filter=14" runat="server">Email Opt-In Customers</a></div>
			<div class="Shortcut"><a id="A19" href="~/SiteAdmin/Secure/reports/InventoryReports.aspx?filter=18" runat="server">Inventory Re-Order</a></div> 
			<div class="Shortcut"><a id="A16" href="~/SiteAdmin/Secure/reports/InventoryReports.aspx?filter=15" runat="server">Most Frequent Customers</a></div>
			<div class="Shortcut"><a id="A17" href="~/SiteAdmin/Secure/reports/InventoryReports.aspx?filter=16" runat="server">Top Spending Customers</a></div> 
		</div>
	    
		<div>
			<div class="Shortcut"><a id="A18" href="~/SiteAdmin/Secure/reports/InventoryReports.aspx?filter=17" runat="server">Top Earning Products</a></div> 
			<div class="Shortcut"><a id="A14" href="~/SiteAdmin/Secure/reports/InventoryReports.aspx?filter=13" runat="server">Order Pick List</a></div>   
			<div class="Shortcut"><a id="A1" href="~/SiteAdmin/Secure/reports/ActivityLogReport.aspx?filter=22" runat="server">Activity Log</a></div> 
			<div class="Shortcut"><a id="A2" href="~/SiteAdmin/Secure/reports/InventoryReports.aspx?filter=24" runat="server">Coupon Usage</a></div> 
			<div class="Shortcut"><a id="A4" href="~/SiteAdmin/Secure/reports/TaxReport.aspx?filter=25" runat="server">Sales Tax</a></div> 
			<div class="Shortcut"><a id="A5" href="~/SiteAdmin/Secure/reports/InventoryReports.aspx?filter=26" runat="server">Affiliate Orders</a></div> 
			<div class="Shortcut"><a id="A6" href="~/SiteAdmin/Secure/reports/SupplierReport.aspx?filter=27" runat="server">Supplier List</a></div> 
            <div class="Shortcut"><a id="A7" href="~/SiteAdmin/Secure/reports/PopularSearchReport.aspx?filter=23" runat="server">Popular Search</a></div> 
		</div>            
    </div>
</asp:Content>

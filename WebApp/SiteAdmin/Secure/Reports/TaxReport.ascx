﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Admin_Secure_Reports_TaxReport" Codebehind="TaxReport.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<div class="ReportLayout">
    <h4 class="SubTitle">
        <asp:Label ID="lblTitle" runat="server"></asp:Label>
    </h4>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:Panel ID="pnlCustom" runat="server">
        <div class="SearchForm">
            <div class="RowStyle">
                <div class="ItemStyle" style="width: 190px;">
                    <span class="FieldStyle">Store Name</span><br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlPortal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlPortal_SelectedIndexChanged">
                        </asp:DropDownList>
                    </span>
                </div>
                <div class="ItemStyle" style="width: 160px;">
                    <span class="FieldStyle">GROUP BY </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlReportGroupBy" runat="server" Width="130px" AutoPostBack="true" OnSelectedIndexChanged="DdlReportGroupBy_SelectedIndexChanged">
                            <asp:ListItem Selected="True">None</asp:ListItem>
                            <asp:ListItem>Month</asp:ListItem>
                            <asp:ListItem>Quarter</asp:ListItem>
                        </asp:DropDownList>
                    </span> 
                </div>
               
            </div>
        </div>
        <div align="left" class="ClearBoth">
          
        </div>
        <asp:PlaceHolder ID="pnlSearch" runat="server" Visible="true">
            <div align="right" style="padding-right: 10px;">
                <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../Themes/Images/buttons/button_clear_highlight.gif';"
                    onmouseout="this.src='../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                    runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                <asp:ImageButton ID="btnOrderFilter" onmouseover="this.src='../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnOrderFilter_Click" CausesValidation="true"
                    ValidationGroup="grpReports" />
            </div>
        </asp:PlaceHolder>
    </asp:Panel>
    <div>
       
        <uc1:Spacer ID="Spacer3" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server" EnableViewState="false"></asp:Label></div>
    <br />
    <div>
    <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%"  AsyncRendering="false" Font-Names="Verdana"
        Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true" ShowCredentialPrompts="false"
        ShowDocumentMapButton="false" ShowFindControls="false" ShowPrintButton="true"
        ShowRefreshButton="false" ShowZoomControl="false">
        <LocalReport DisplayName="Report" ReportPath="SiteAdmin/Secure/Reports/ActivityLog.rdlc">
        </LocalReport>
    </rsweb:ReportViewer>
    </div>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:HiddenField runat="server" ID="hdnIsPostBack" Value="false" />
    <asp:HiddenField runat="server" ID="hdnMode" Value="12" />
</div>

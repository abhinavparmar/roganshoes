﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master"
    AutoEventWireup="True" EnableEventValidation="false" Inherits="SiteAdmin.Admin_Secure_Reports_Report" Codebehind="Report.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="OrderReport" Src="~/SiteAdmin/Secure/Reports/OrderReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="RecurringBillingReport" Src="~/SiteAdmin/Secure/Reports/RecurringBillingReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="InventoryReport" Src="~/SiteAdmin/Secure/Reports/InventoryReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActivityLogReport" Src="~/SiteAdmin/Secure/Reports/ActivityLogReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TaxReport" Src="~/SiteAdmin/Secure/Reports/TaxReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SupplierReport" Src="~/SiteAdmin/Secure/Reports/SupplierReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PopularSearchReport" Src="~/SiteAdmin/Secure/Reports/PopularSearchReport.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h1>
        Reports</h1>
  
   
    <div style="display: block;">
        <div class="ReportLeft">
            <asp:SiteMapDataSource ID="SiteMapDataSource2" runat="server" ShowStartingNode="False"
                SiteMapProvider="ZNodeAdminSiteMap" StartingNodeUrl="~/SiteAdmin/Secure/Reports/report.aspx" />
            <asp:DataList runat="server" ID="ctrlSubMenu" RepeatDirection="Horizontal" RepeatLayout="Flow"
                CssClass="ReportMenuStyle" RepeatColumns="1" DataSourceID="SiteMapDataSource2"
                ItemStyle-Wrap="true">
                <ItemStyle Width="200px" />
                <ItemTemplate>
                    <span class='<%# GetSubmenuCss(Eval("Title")) %>'>
                        <asp:LinkButton runat="server" ID="SubMenu" onmouseover="this.innerText += ' &raquo;';"
                            onmouseout="this.innerText = this.innerText.replace(' &raquo;','');" Text='<%# GetSubmenuName(Eval("Title")) %>'
                            CommandArgument='<%# Eval("Title") %>' OnClick="SubMenu_OnClick"></asp:LinkButton>
                    </span>
                </ItemTemplate>
            </asp:DataList></div>
        <div class="ReportRight">
            <div >
                <uc1:OrderReport runat="server" ID="uxOrderReport" Mode="12" />
                <uc1:RecurringBillingReport runat="server" ID="uxRecurringBillingReport" Mode="206" Visible="false"/>
                <uc1:InventoryReport runat="server" ID="uxInventoryReport" Mode="13" Visible="false" />
                <uc1:ActivityLogReport runat="server" ID="uxActivityLogReport" Mode="22" Visible="false" />
                <uc1:TaxReport runat="server" ID="uxTaxReport" Mode="25" Visible="false" />
                <uc1:SupplierReport runat="server" ID="uxSupplierReport" Mode="27" Visible="false" />
                <uc1:PopularSearchReport runat="server" ID="uxPopularSearchReport" Mode="23" Visible="false" />
            </div>   
        </div>           
        <div style="clear:both"></div>
        <div class="ReportLeft">&nbsp;</div>
        <div class="ReportRight">  
            <div class="FormView">
                <div class="FieldStyle" style="width: 100%;padding-top:25px;">
                    <small style="width: 100%;">Hint: If you are having trouble viewing the report or would like to print the
                        report, use the 'Export' option</small></div>
            </div>
        </div>
        </div>
    <asp:HiddenField runat="server" ID="hdnSelectedReport" Value="Orders" />
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.Default" Title="Manage Suppliers" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <div class="LeftMargin">
        <h1>Order Management</h1>
        <hr />
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/approve orders.png" /></div>
            <div class="Shortcut"><a id="A2" href="~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/Default.aspx" runat="server">View Orders</a></div>
            <div class="LeftAlign">
                <p>Search and download orders and update order status.</p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/create-an-order.png" /></div>
            <div class="Shortcut"><a id="A3" href="~/SiteAdmin/Secure/Orders/OrderManagement/OrderDesk/Default.aspx" runat="server">Create an Order</a></div>
            <div class="LeftAlign">
                <p>Customer service representatives can use this feature to manually create orders.</p>
            </div>
        </div>

        <h1>Customer Management</h1>
        <hr />

             <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/profiles.png" />
            </div>
            <div class="Shortcut"><a id="A11" href="~/SiteAdmin/Secure/Orders/CustomerManagement/Profiles/default.aspx" runat="server">Profiles</a></div>
            <div class="LeftAlign">
                <p>Create customer groups using profiles. You can then apply special promotions, pricing, taxes, shipping, and display options to these profiles.</p>
            </div>
        </div>

           <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/CustomerAcctManager.png" /></div>
            <div class="Shortcut"><a id="A6" href="~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx" runat="server">Customers</a></div>
            <div class="LeftAlign">
                <p>Search customer, partner, and vendor accounts, view order history, and access service notes.</p>
            </div>
        </div>


          <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ServiceRequests.png" /></div>
            <div class="Shortcut"><a id="A1" href="~/SiteAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx" runat="server">Service Requests</a></div>
            <div class="LeftAlign">
                <p>
                    Respond to service requests submitted by your customers using the Contact-Us form on your website.
                </p>
            </div>
        </div> 

        <h1>Returns Management</h1>
        <hr />
         <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/RMAconfiguration.png" /></div>
           <div class="Shortcut"><a id="A4" href="~/SiteAdmin/Secure/Orders/ReturnsManagement/RMAConfiguration/Default.aspx" runat="server">RMA Configuration</a></div>
            <div class="LeftAlign">
                <p>Configure RMA rules that determine how return requests are managed in your store.</p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/RMAmanager.png" /></div>

          <div class="Shortcut"><a id="A7" href="~/SiteAdmin/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx" runat="server">RMA Manager</a></div>
            <div class="LeftAlign">
                <p>Manage pending return requests in your store.</p>
            </div>

        </div>

        <h1>Supplier Management</h1>
        <hr />
        
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/suppliers.png" /></div>
            <div class="Shortcut"><a id="A8" href="~/SiteAdmin/Secure/Orders/SupplierManagement/Suppliers/Default.aspx" runat="server">Suppliers</a></div>
            <div class="LeftAlign">
                <p>Manage vendors that will fulfill products in your store.</p>
            </div>
        </div>
    </div>
</asp:Content>


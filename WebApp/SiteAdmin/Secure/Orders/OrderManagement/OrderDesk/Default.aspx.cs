using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Taxes;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using WebApp;

namespace SiteAdmin.Secure.Orders.OrderManagement.OrderDesk
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Orders.Sales.OrderDesk - Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Variables
        private ZNodeShoppingCart shoppingCart;
        private ZNodeCheckout checkout = null;
        private string PortalIds = string.Empty;
        private int portalId = 0;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Account Object from/to Cache
        /// </summary>
        public Account UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the cache using Get Method and
                    // It is converted to a Entity Account object
                    return (Account)ViewState["AccountObject"];
                }

                return new Account();
            }

            set
            {
                //  object is placed in the session and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the billing address
        /// </summary>
        public int BillingAddressID
        {
            get
            {
                if (ViewState["BillingAddressID"] != null)
                {
                    return (int)ViewState["BillingAddressID"];
                }

                return 0;
            }

            set
            {
                ViewState["BillingAddressID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the shipping address
        /// </summary>
        public int ShippingAddressID
        {
            get
            {
                if (ViewState["ShippingAddressID"] != null)
                {
                    return (int)ViewState["ShippingAddressID"];
                }

                return 0;
            }

            set
            {
                ViewState["ShippingAddressID"] = value;
            }
        }

        /// <summary>
        /// Gets the Customer Billing address
        /// </summary>
        public Address BillingAddress
        {
            get
            {
                Address _billingAddress = new Address();
                if (this.UserAccount.AccountID > 0)
                {
                    AddressService addressService = new AddressService();
                    Address billingAddress = null;

                    if (this.BillingAddressID == 0)
                    {
                        // If first time user created then billing address Id will be 0, but address will created on database.
                        AccountAdmin accountAdmin = new AccountAdmin();
                        billingAddress = accountAdmin.GetDefaultBillingAddress(this.UserAccount.AccountID);
                        this.BillingAddressID = billingAddress.AddressID;
                    }

                    billingAddress = addressService.GetByAddressID(this.BillingAddressID);
                    if (billingAddress == null)
                    {
                        billingAddress = new Address();
                    }

                    _billingAddress.FirstName = billingAddress.FirstName;
                    _billingAddress.LastName = billingAddress.LastName;
                    _billingAddress.PhoneNumber = billingAddress.PhoneNumber;
                    _billingAddress.Street = billingAddress.Street;
                    _billingAddress.Street1 = billingAddress.Street1;
                    _billingAddress.StateCode = billingAddress.StateCode;
                    _billingAddress.PostalCode = billingAddress.PostalCode;
                    _billingAddress.CompanyName = billingAddress.CompanyName;
                    _billingAddress.City = billingAddress.City;
                    _billingAddress.CountryCode = billingAddress.CountryCode;
                }

                return _billingAddress;
            }
        }

        /// <summary>
        ///  Gets the Customer shipping address
        /// </summary>
        public Address ShippingAddress
        {
            get
            {
                Account _account = this.UserAccount;
                Address _shippingAddress = new Address();

                if (_account.AccountID > 0)
                {
                    AddressService addressService = new AddressService();
                    Address shippingAddress = null;
                    if (this.ShippingAddressID == 0)
                    {
                        // If first time user created then shipping address Id will be 0, but address will created on database.
                        AccountAdmin accountAdmin = new AccountAdmin();
                        shippingAddress = accountAdmin.GetDefaultShippingAddress(this.UserAccount.AccountID);
                        this.ShippingAddressID = shippingAddress.AddressID;
                    }

                    shippingAddress = addressService.GetByAddressID(this.ShippingAddressID);
                    if (shippingAddress == null)
                    {
                        shippingAddress = new Address();
                    }

                    _shippingAddress.FirstName = shippingAddress.FirstName;
                    _shippingAddress.LastName = shippingAddress.LastName;
                    _shippingAddress.CompanyName = shippingAddress.CompanyName;
                    _shippingAddress.PhoneNumber = shippingAddress.PhoneNumber;
                    _shippingAddress.Street = shippingAddress.Street;
                    _shippingAddress.Street1 = shippingAddress.Street1;
                    _shippingAddress.City = shippingAddress.City;
                    _shippingAddress.PostalCode = shippingAddress.PostalCode;
                    _shippingAddress.StateCode = shippingAddress.StateCode;
                    _shippingAddress.CountryCode = shippingAddress.CountryCode;
                }

                return _shippingAddress;
            }
        }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Gets the Login Name
        /// </summary>
        protected string LoginName
        {
            get
            {
                string _loginName = string.Empty;
                Account _account = this.UserAccount;

                if (_account.UserID.HasValue)
                {
                    MembershipUser user = Membership.GetUser(_account.UserID.Value);

                    if (user != null)
                    {
                        _loginName = user.UserName;
                    }
                }

                return _loginName;
            }
        }

        #endregion

        #region Private Properties
        /// <summary>
        /// Gets the Profile Name
        /// </summary>
        private string ProfileName
        {
            get
            {
                string _profileName = string.Empty;

                _profileName = ZNodeProfile.ProfileName;

                return _profileName;
            }
        }

        #endregion

        #region Page Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set the selected Portal Id
            SetSelectedPortalId();

            // Get Portals 
            this.PortalIds = UserStoreAccess.GetAvailablePortals;

            // Registers the event for the customer search (child) control
            this.uxCustomerSearch.SelectedIndexChanged += new EventHandler(this.FoundUsers_SelectedIndexChanged);

            // Registers the event for the create user (child) control
            this.uxCreateUser.ButtonClick += new EventHandler(this.BtnCancel_Click);

            // Registers the event for the create user (child) control
            this.uxCustomerSearch.SearchButtonClick += new EventHandler(this.BtnSearchClose_Click);

            // Registers the event for the quick Order (child) control - Addto cart button
            this.uxQuickOrder.SubmitButtonClicked += new EventHandler(this.AddToCart_Click);

            // Registers the event for the quick Order (child) control - Addto cart button
            this.uxQuickOrder.CancelButtonClicked += new EventHandler(this.AddToCart_Click);

            // Registers the event for the quick Order (child) control - Add another product button
            this.uxQuickOrder.AddProductButtonClicked += new EventHandler(this.AddAnotherProduct_Click);

            // Registers the event for the payment (child) control - Shipping Type selected change list
            this.uxPayment.ShippingSelectedIndexChanged += new EventHandler(this.Shipping_SelectedIndexChanged);

            if (!Page.IsPostBack)
            {
                // Remove the Session
                Session.Remove("currentStoreID");

                this.shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

                if (this.shoppingCart != null)
                {
                    this.shoppingCart.EmptyCart();
                }

                // Bind Portals
                this.BindPortal();

                // Bind fields
                //this.Bind();//Znode old code

                pnlSelectStore.Visible = true;
               // pnlSelectCatalog.Visible = false;
                pnlSelectCustomer.Visible = false;
                pnlCart.Visible = false;
            }
        }

        /// <summary>
        /// Page Pre Render Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

            if (this.shoppingCart != null)
            {
                this.shoppingCart.Payment = uxPayment.Payment;

                if (this.UserAccount.AccountID > 0)
                {
                    uxPayment.UserAccount = this.UserAccount;
                    this.shoppingCart.Payment.BillingAddress = this.BillingAddress;
                    this.shoppingCart.Payment.ShippingAddress = this.ShippingAddress;
                }

                if (this.shoppingCart.ShoppingCartItems.Count > 0 && this.UserAccount.AccountID > 0)
                {
                    pnlPaymentShipping.Visible = true;

                    // Calculate tax if Customer selected
                    this.CalculateTaxes(this.shoppingCart);
                }

                if (this.shoppingCart.ShoppingCartItems.Count > 0)
                {
                    // Calculate shipping  
                    uxPayment.CalculateShipping();
                    uxShoppingCart.Visible = true;

                    uxShoppingCart.ShoppingCartObject = this.shoppingCart;

                    // Bind Shopping Cart items
                    uxShoppingCart.Bind();
                }
                else
                {
                    uxShoppingCart.Visible = false;
                }

                if (this.shoppingCart.Total == 0)
                {
                    uxPayment.ShowPaymentSection = false;
                }
                else
                {
                    uxPayment.ShowPaymentSection = true;
                }
            }
            else
            {
                uxShoppingCart.Visible = false;
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Fires when Cancel Button Click triggered by child control (Create User)
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            this.UserAccount = uxCreateUser.UserAccount;
            this.BillingAddressID = uxCreateUser.BillingAddressID;
            this.ShippingAddressID = uxCreateUser.ShippingAddressID;
            this.Bind();

            UpdatePanelOrderDesk.Update();
            mdlCreateUserPopup.Hide();
        }

        /// <summary>
        /// Search Close button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearchClose_Click(object sender, EventArgs e)
        {
            this.Bind();

            mdlPopup.Hide();
            UpdatePanelOrderDesk.Update();
        }

        /// <summary>
        /// Cancel Order Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancelOrder_Click(object sender, EventArgs e)
        {
            this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (this.shoppingCart != null)
            {
                this.shoppingCart.EmptyCart();
                this.shoppingCart = null;

                Session.Remove(ZNodeSessionKeyType.ShoppingCart.ToString());
            }

            this.ResetProfileCache();

            Response.Redirect("~/SiteAdmin/Secure/Orders/Default.aspx");
        }

        /// <summary>
        /// Add To cart Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddToCart_Click(object sender, EventArgs e)
        {
            this.Bind();
            UpdatePanelOrderDesk.Update();
        }

        /// <summary>
        /// Search Customer Link Button click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LinkSearchCustomer_Click(object sender, EventArgs e)
        {
            uxCustomerSearch.ClearUI();
            mdlPopup.Show();
        }

        /// <summary>
        /// Create new User link button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LinkNewCustomer_Click(object sender, EventArgs e)
        {
            // Reset Account object
            this.UserAccount = new Account();
            uxCreateUser.UserAccount = new Account();
            uxPayment.ClearUI();

            // Clear previous values bounded with the text box fields
            uxCreateUser.Bind();
            uxCreateUser.BindCountry();
            mdlCreateUserPopup.Show();
        }

        /// <summary>
        /// Quick Order Popup Link button
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LinkbtnQuickOrder_Click(object sender, EventArgs e)
        {
            uxQuickOrder.Product = new ZNodeProduct();
            uxQuickOrder.Bind();
            UpdatePanelQuickOrder.Update();
            mdlQuickOrderPopup.Show();
        }


        /// <summary>
        /// Quick Order Popup Link button
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddAnotherProduct_Click(object sender, EventArgs e)
        {
            uxQuickOrder.Product = new ZNodeProduct();
            uxQuickOrder.Bind();
            UpdatePanelQuickOrder.Update();
            mdlQuickOrderPopup.Show();
        }

        /// <summary>
        /// Portal Dropdown Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlPortal_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlSelectCatalog.Visible = true;

            CatalogHelper catalogHelper = new CatalogHelper();
            DataSet ds = catalogHelper.GetCatalogsByPortalId(portalId);

            DataRow dr = ds.Tables[0].NewRow();
            dr["Name"] = "Select Catalog";
            ds.Tables[0].Rows.InsertAt(dr, 0);
            ddlCatalog.DataSource = ds.Tables[0];
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogId";
            ddlCatalog.DataBind();

            foreach (ListItem licatalog in ddlCatalog.Items)
            {
                licatalog.Text = Server.HtmlDecode(licatalog.Text);
            }


            ddlCatalog.Visible = true;
        }

        // Set the selected Portal Id
        private void SetSelectedPortalId()
        {
            if (int.TryParse(ddlPortal.SelectedValue.ToString(), out portalId))
            {
                if (ZNodeConfigManager.SiteConfig.PortalID != portalId)
                {
                    ZNode.Libraries.Framework.Business.ZNodeConfigManager.AliasSiteConfig(portalId);
                }
            }
        }



        /// <summary>
        /// Catalog Dropdown Selected Index Changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {

            pnlSelectCustomer.Visible = true;
            //pnlCart.Visible = true;//Znode old code
            pnltitle.Visible = true;
            int catalogId = 0;

            if (int.TryParse(ddlCatalog.SelectedValue.ToString(), out catalogId))
            {
                uxQuickOrder.CatalogId = catalogId;
            }

        }

        /// <summary>
        /// Found Users Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void FoundUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            uxPayment.ClearUI();
            this.UserAccount = uxCustomerSearch.UserAccount;
            this.BillingAddressID = uxCustomerSearch.BillingAddressID;
            this.ShippingAddressID = uxCustomerSearch.ShippingAddressID;
            this.Bind();
            mdlPopup.Hide();
            UpdatePanelOrderDesk.Update();
        }

        /// <summary>
        /// Shipping Type list selected index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Shipping_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Bind();
            UpdatePanelOrderDesk.Update();
        }

        /// <summary>
        /// Edit Customer
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Ui_Edit_Click(object sender, EventArgs e)
        {
            if (this.UserAccount.AccountID > 0)
            {
                uxCreateUser.UserAccount = this.UserAccount;
                uxCreateUser.BindAddressList();
                uxCreateUser.BillingAddressID = this.BillingAddressID;
                uxCreateUser.ShippingAddressID = this.ShippingAddressID;
                uxCreateUser.Bind();
                UpdatePanelOrderDesk.Update();
                mdlCreateUserPopup.Show();
            }
        }

        /// <summary>
        /// Submit Button Click Event 
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Submit_Click(object sender, EventArgs e)
        {
            // Get ShoppingCart object from current session
            this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
            ZNodeOrderFulfillment order = new ZNodeOrderFulfillment();
            ZNodeUserAccount _userAccount = new ZNodeUserAccount();

            // Validate the customer shipping address if address validation enabled.
            if (ZNodeConfigManager.SiteConfig.EnableAddressValidation != null ||
                ZNodeConfigManager.SiteConfig.EnableAddressValidation == true)
            {
                AccountAdmin _accountAdmin = new AccountAdmin();
                bool isAddressValid = _accountAdmin.IsAddressValid(this.ShippingAddress);

                // Do not allow the customer to go to next page if valid shipping address required is enabled. 
                if (!isAddressValid && ZNodeConfigManager.SiteConfig.RequireValidatedAddress != null &&
                    ZNodeConfigManager.SiteConfig.RequireValidatedAddress == true)
                {
                    lblError.Text = "Customer shipping address is not a valid address. Please enter valid address to continue.";
                    return;
                }
            }

            if (this.shoppingCart != null)
            {
                // Check no.of items in the shopping cart object
                if (this.shoppingCart.ShoppingCartItems.Count == 0)
                {
                    lblError.Text = "There are no items in the shopping cart. Please add items to your shopping cart before checking out.";
                    return;
                }

                if (!uxPayment.IsPaymentOptionExists)
                {
                    lblError.Text = "No payment options have been setup for this profile in the database. Please use the admin to setup valid payment options first.";
                    return;
                }

                try
                {
                    // Retrieve Account object from Cache object
                    Account _account = this.UserAccount;

                    if (_account.AccountID == 0)
                    {
                        btnSubmit.Enabled = true;
                        lblError.Text = "Customer information is incomplete.";
                        return;
                    }

                    // User Settings
                    _userAccount.AccountID = _account.AccountID;

                    if (_account.ProfileID.HasValue)
                    {
                        _userAccount.ProfileID = ZNodeProfile.CurrentUserProfileId;
                    }

                    if (_account.ParentAccountID.HasValue)
                    {
                        _userAccount.ParentAccountID = _account.ParentAccountID.Value;
                    }

                    _userAccount.UpdateUser = _account.UpdateUser;
                    _userAccount.UserID = _account.UserID;
                    _userAccount.CreateUser = _account.CreateUser;
                    _userAccount.EmailID = _account.Email;

                    // Set address
                    _userAccount.BillingAddress = this.BillingAddress;
                    _userAccount.ShippingAddress = this.ShippingAddress;

                    if ((this.BillingAddress.Street.Length == 0) || (this.BillingAddress.City.Length == 0) || (this.BillingAddress.PostalCode.Length == 0) || (this.BillingAddress.StateCode.Length == 0) || (this.BillingAddress.FirstName == null) || (this.BillingAddress.LastName == null))
                    {
                        lblError.Text = "Customer address fields are incomplete.";
                        return;
                    }

                    // Get data from user controls
                    this.GetControlData(_userAccount);

                    // Set the current store portalID 
                    // To make a note in Order table.
                    this.checkout.PortalID = ZNodeConfigManager.SiteConfig.PortalID;
                    this.checkout.UserAccount = _userAccount;

                    if (this.checkout.ShoppingCart.PreSubmitOrderProcess())
                    {
                        order = (ZNodeOrderFulfillment)this.checkout.SubmitOrder();
                    }
                    else
                    {
                        lblError.Text = this.checkout.ShoppingCart.ErrorMessage;
                        return;
                    }
                }
                catch (ZNode.Libraries.ECommerce.Entities.ZNodePaymentException ex)
                {
                    // Display payment error message
                    lblError.Text = ex.Message;
                    return;
                }
                catch
                {
                    // Display error page
                    lblError.Text = "Could not submit your order. Please contact customer support.";
                    return;
                }

                if (this.checkout.IsSuccess)
                {
                    // Receipt 
                    Session["OrderDetail"] = order;

                    // Update product inventory and coupon
                    this.OnSubmitOrder(order, this.shoppingCart);

                    Account _account = this.UserAccount;

                    // Log Activity
                    string AssociateName = "Creation of Order " + order.OrderID + " - " + order.BillingAddress.FirstName + " Account ";
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, "OrderDesk");

                    this.ResetProfileCache();

                    Response.Redirect("~/SiteAdmin/Secure/Orders/OrderManagement/OrderDesk/OrderDeskReceipt.aspx?itemid=" + uxQuickOrder.CatalogId);
                }
                else
                {
                    // Display error page
                    lblError.Text = this.checkout.PaymentResponseText;
                    return;
                }
            }
            else
            {
                lblError.Text = "There are no items in the shopping cart. Please add items to your shopping cart before checking out.";
                return;
            }
        }

        /// <summary>
        /// This method get data from user controls(payment and shippping info)
        /// </summary>
        /// <param name="userAccount">The User Account instance</param>
        protected void GetControlData(ZNodeUserAccount userAccount)
        {
            // Get objects from session
            this.checkout = new ZNodeCheckout();

            // Set payment data in checkout object
            this.checkout.ShoppingCart.Payment = uxPayment.Payment;
            this.checkout.ShoppingCart.Payment.BillingAddress = this.BillingAddress;
            this.checkout.ShoppingCart.Payment.ShippingAddress = this.ShippingAddress;
            this.checkout.UserAccount = new ZNodeUserAccount();
            this.checkout.UserAccount.BillingAddress = this.BillingAddress;
            this.checkout.UserAccount.ShippingAddress = this.ShippingAddress;
            this.checkout.PaymentSettingID = uxPayment.PaymentSettingID;
            this.checkout.UpdateUserInfo = false;
            this.checkout.PurchaseOrderNumber = uxPayment.PurchaseOrderNumber;
            this.checkout.AdditionalInstructions = uxPayment.AdditionalInstructions;
            uxPayment.CalculateShipping();
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();
            DataSet ds = portals.ToDataSet(true);
            DataView dv = new DataView(ds.Tables[0]);

            if (this.PortalIds == "0")
            {
                // Portal Drop Down List
                dv.RowFilter = "PortalID in (" + ZNodeConfigManager.SiteConfig.PortalID + ")";
                ddlPortal.DataSource = dv;
                ddlPortal.DataTextField = "StoreName";
                ddlPortal.DataValueField = "PortalID";
                ddlPortal.DataBind();
            }
            else if (this.PortalIds.Length > 0)
            {
                dv.RowFilter = "PortalID in (" + this.PortalIds + ")";
                dv.RowFilter = "PortalID in (" + ZNodeConfigManager.SiteConfig.PortalID + ")";
                // Portal Drop Down List
                ddlPortal.DataSource = dv;
                ddlPortal.DataTextField = "StoreName";
                ddlPortal.DataValueField = "PortalID";
                ddlPortal.DataBind();
            }
            else
            {
                lblmsg.Text = "You do not have permission to manage this Order Desk. Please contact your administrator for permission";
                ddlPortal.Visible = false;
            }

            foreach (ListItem liportal in ddlPortal.Items)
            {
                liportal.Text = Server.HtmlDecode(liportal.Text);
            }
            BindCatalogonOnLoad();
        }

        /// <summary>
        /// Bind Customer section and Shopping cart 
        /// </summary>
        private void Bind()
        {
            if (this.UserAccount.AccountID > 0)
            {
                ui_lblProfile.Text = this.ProfileName;
                ui_lblUserID.Text = this.LoginName;
                ui_BillingAddress.Text = string.Empty;
                ui_ShippingAddress.Text = string.Empty;

                try
                {
                    uxPayment.UserAccount = this.UserAccount;
                    uxPayment.BindShipping();
                    uxPayment.BindPaymentTypeData();
                    ui_pnlAcctInfo.Visible = true;

                    ui_BillingAddress.Text = ZCommonHelper.GetAddressString(this.BillingAddress);//Zeon Custom Code
                    ui_ShippingAddress.Text = ZCommonHelper.GetAddressString(this.ShippingAddress);

                    // Hide the edit button if the seleted Customer is Admin user
                    Account _account = this.UserAccount;
                    MembershipUser _user = Membership.GetUser(_account.UserID.Value);

                    string roleList = string.Empty;

                    // Get roles for this User account
                    string[] roles = Roles.GetRolesForUser(_user.UserName);

                    foreach (string Role in roles)
                    {
                        roleList += Role + "<br>";
                    }

                    string rolename = roleList;

                    // Hide the Delete button if a NonAdmin user has entered this page
                    if (!Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
                    {
                        if (Roles.IsUserInRole(_user.UserName, "ADMIN"))
                        {
                            ui_Edit.Visible = false;
                        }
                        else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                        {
                            if (rolename == Convert.ToString("USER<br>") || rolename == Convert.ToString(string.Empty))
                            {
                                ui_Edit.Visible = true;
                            }
                            else
                            {
                                ui_Edit.Visible = false;
                            }
                        }
                    }
                }
                catch
                {
                }
            }
            else
            {
                ui_pnlAcctInfo.Visible = false;
                ui_lblProfile.Text = string.Empty;
                ui_lblUserID.Text = string.Empty;
                ui_BillingAddress.Text = string.Empty;
                ui_ShippingAddress.Text = string.Empty;
            }
        }

        /// <summary>
        /// Calculate taxes
        /// </summary>
        /// <param name="shoppingCart">The Shopping cart object</param>
        private void CalculateTaxes(ZNodeShoppingCart shoppingCart)
        {
            Account userAccount = this.UserAccount;

            if (userAccount.AccountID > 0)
            {
                shoppingCart.Payment.BillingAddress = this.BillingAddress;
                shoppingCart.Calculate();
            }
        }

        /// <summary>
        /// Submit Order Method
        /// </summary>
        /// <param name="order">The Instance of Order</param>
        /// <param name="shoppingCart">The Instance of Shopping Cart</param>
        private void OnSubmitOrder(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart)
        {
            ZNode.Libraries.ECommerce.Suppliers.ZNodeSupplierOption supplierOption = new ZNode.Libraries.ECommerce.Suppliers.ZNodeSupplierOption();

            supplierOption.SubmitOrder(order, shoppingCart);

            // Set Digital Asset
            int Counter = 0;
            DigitalAssetService digitalAssetService = new DigitalAssetService();

            // Loop through the Order Line Items
            foreach (OrderLineItem orderLineItem in order.OrderLineItems)
            {
                ZNodeShoppingCartItem shoppingCartItem = shoppingCart.ShoppingCartItems[Counter++];

                // Set quantity ordered
                int qty = shoppingCartItem.Quantity;

                // Set product id
                int productId = shoppingCartItem.Product.ProductID;

                ZNodeGenericCollection<ZNodeDigitalAsset> AssignedDigitalAssets = new ZNodeGenericCollection<ZNodeDigitalAsset>();

                // Get Digital assets for productid and quantity
                AssignedDigitalAssets = ZNodeDigitalAssetList.CreateByProductIdAndQuantity(productId, qty).DigitalAssetCollection;

                // Loop through the digital asset retrieved for this product
                foreach (ZNodeDigitalAsset digitalAsset in AssignedDigitalAssets)
                {
                    DigitalAsset entity = digitalAssetService.GetByDigitalAssetID(digitalAsset.DigitalAssetID);

                    // Set OrderLineitemId property
                    entity.OrderLineItemID = orderLineItem.OrderLineItemID;

                    // Update digital asset to the database
                    digitalAssetService.Update(entity);
                }

                // Set retrieved digital asset collection to shopping product object
                // If product has digital assets, it will display it on the receipt page along with the product name
                shoppingCartItem.Product.ZNodeDigitalAssetCollection = AssignedDigitalAssets;
            }

            shoppingCart.PostSubmitOrderProcess();
        }

        /// <summary>
        /// Reset Profile Cache
        /// </summary>
        private void ResetProfileCache()
        {
            // Reset current cached profile cache object with logged in user profile.
            if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
            {
                ZNode.Libraries.DataAccess.Entities.Profile _profile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];

                ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();

                _userAccount.ProfileID = ZNodeProfile.CurrentUserProfileId;

                if (_profile.ProfileID != _userAccount.ProfileID)
                {
                    ZNode.Libraries.DataAccess.Service.ProfileService profileService = new ZNode.Libraries.DataAccess.Service.ProfileService();
                    ZNode.Libraries.DataAccess.Entities.Profile profile = profileService.GetByProfileID(_userAccount.ProfileID);

                    System.Web.HttpContext.Current.Session["ProfileCache"] = profile;
                }
            }
        }

        /// <summary>
        /// Bind Catalog on load based on current Portal
        /// </summary>
        private void BindCatalogonOnLoad()
        {
            pnlSelectCatalog.Visible = true;
            SetSelectedPortalId();
            CatalogHelper catalogHelper = new CatalogHelper();
            DataSet ds = catalogHelper.GetCatalogsByPortalId(portalId);

            DataRow dr = ds.Tables[0].NewRow();
            dr["Name"] = "Select Catalog";
            ds.Tables[0].Rows.InsertAt(dr, 0);
            ddlCatalog.DataSource = ds.Tables[0];
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogId";
            ddlCatalog.DataBind();

            foreach (ListItem licatalog in ddlCatalog.Items)
            {
                licatalog.Text = Server.HtmlDecode(licatalog.Text);
            }


            ddlCatalog.Visible = true;
        }

        #endregion
    }
}
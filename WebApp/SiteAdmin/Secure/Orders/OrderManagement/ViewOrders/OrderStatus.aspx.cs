using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.OrderManagement.ViewOrders
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Orders.Sales.Orders - OrderStatus class
    /// </summary>
    public partial class OrderStatus : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int OrderId = 0;
        private string ListPage = "Default.aspx";
        #endregion

        #region General Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["itemid"] != null) || (Request.Params["itemid"].Length != 0))
            {
                this.OrderId = int.Parse(Request.Params["itemid"].ToString());
                lblOrderID.Text = this.OrderId.ToString();
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
               
            }
        }

        /// <summary>
        /// Update Order Status Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UpdateOrderStatus_Click(object sender, EventArgs e)
        {
            OrderAdmin orderAdmin = new OrderAdmin();
            Order order = orderAdmin.GetOrderByOrderID(this.OrderId);

            if (order != null)
            {
                order.OrderStateID = int.Parse(ListOrderStatus.SelectedValue);
                order.OrderID = this.OrderId;
                //order.TrackingNumber = TrackingNumber.Text.Trim();
                
               
                if (order.OrderStateID == 20)
                {
                    order.ShipDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                }

                if (order.OrderStateID == 30)
                {
                    order.ReturnDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                }

                // Delete referral commission entry of order if order status is "RETURNED" (30) or "CANCELLED" (40)
                bool Check = orderAdmin.Update(order);
                bool isReferralCommissionDeleted = true;

                if (order.OrderStateID == 30 || order.OrderStateID == 40)
                {
                    ReferralCommissionAdmin referralCommissionAdmin = new ReferralCommissionAdmin();
                    if (referralCommissionAdmin.GetReferralCommissionByOrderID(this.OrderId))
                    {
                        referralCommissionAdmin.GetReferralCommissionByOrderID(this.OrderId);
                        isReferralCommissionDeleted = referralCommissionAdmin.DeleteByOrderId(this.OrderId);
                    }
                }

                if (Check && isReferralCommissionDeleted)
                {
                    //If Selected index changed is selected as Shipped then Send the mail
                    if (order.OrderStateID == 20)
                    {
                        Order _OrderList = orderAdmin.DeepLoadByOrderID(this.OrderId);
                        this.SendEmailReceipt(_OrderList);
                    }
                    Response.Redirect(this.ListPage);
                }
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CancelStatus_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListPage);
        }

        /// <summary>
        /// Dropdown list selected index change
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void OrderStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListOrderStatus.SelectedValue.Equals("20"))
            {
                this.BindData();
                ListOrderStatus.SelectedValue = "20";
              
            }
           
        }

        /// <summary>
        /// Email Status Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        //protected void EmailStatus_Click(object sender, EventArgs e)
        //{
        //    OrderAdmin _OrderAdmin = new OrderAdmin();
        //    Order _OrderList = _OrderAdmin.DeepLoadByOrderID(this.OrderId);

        //    this.SendEmailReceipt(_OrderList);
        //}

        private void SendEmailReceipt(Order order)
        {
            string recepientEmail = string.Empty;

            try
            {
                recepientEmail = order.BillingEmailId;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;

                if (string.IsNullOrEmpty(senderEmail))
                {
                    trackmessage.CssClass = "Error";
                    trackmessage.Text = "Sender email address (Customer service email) is empty.";
                    return;
                }
                
                if (string.IsNullOrEmpty(recepientEmail))
                {
                    trackmessage.CssClass = "Error";
                    trackmessage.Text = "Recepient email address is empty.";
                    return;
                }

                string subject = "Track your package";

                // Get message text.
                StreamReader rw = new StreamReader(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "TrackingNumber.htm"));
                string messageText = rw.ReadToEnd();
                Regex rx1 = new Regex("#BillingFirstName#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, order.BillingFirstName);

                Regex rx2 = new Regex("#BillingLastName#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, order.BillingLastName);

                Regex rx3 = new Regex("#Custom1#", RegexOptions.IgnoreCase);
                //messageText = rx3.Replace(messageText, TrackingNumber.Text);

                //Zeon Custom Code:Added Logo
                string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
                string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
                int portalId = ZNodeConfigManager.SiteConfig.PortalID;

                ZEmailHelper zEmailHelper = new ZEmailHelper();
                ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
                string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

                string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
                string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

                //To add header to the template
                Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
                messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

                //To add footer to the template
                Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
                messageText = rxFooterContent.Replace(messageText, messageTextFooter);

                Regex rx4 = new Regex("#TrackingMessage#", RegexOptions.IgnoreCase);
                if (order.ShippingIDSource.ShippingTypeID == 3)
                { 
                    messageText = rx4.Replace(messageText, "Your FedEx Tracking Number :  "); 
                }
                else if (order.ShippingIDSource.ShippingTypeID == 2)
                { 
                    messageText = rx4.Replace(messageText, "Your UPS Tracking Number :  "); 
                }
                else
                { 
                    messageText = rx4.Replace(messageText, string.Empty); 
                }

                Regex rx5 = new Regex("#Message#", RegexOptions.IgnoreCase);
                if (order.ShippingIDSource.ShippingTypeID == 3)
                { 
                    messageText = rx5.Replace(messageText, "We would like to inform you that your order has shipped. You can track your package using the tracking number given below:"); 
                }
                else if (order.ShippingIDSource.ShippingTypeID == 2)
                { 
                    messageText = rx5.Replace(messageText, "We would like to inform you that your order has shipped. You can track your package using the tracking number given below:"); 
                }
                else
                { 
                    messageText = rx5.Replace(messageText, "We would like to inform you that your order has shipped."); 
                }

                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, messageText, true);

                trackmessage.Text = "Tracking Number Email Sent to " + "<a href=\"mailto:" + recepientEmail.ToString() + "\">" + recepientEmail + "</a>";

                //EmailStatus.Enabled = false;
            }
            catch (Exception ex)
            {
                trackmessage.CssClass = "Error";
                trackmessage.Text = "There was a problem sending the email to " + "<a href=\"mailto:" + recepientEmail.ToString() + "\">" + recepientEmail + "</a>";

                // Log exception
                ExceptionPolicy.HandleException(ex, "ZNODE_GLOBAL_EXCEPTION_POLICY");
            }
        }

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Data Method\
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.OrderAdmin _OrderAdminAccess = new ZNode.Libraries.Admin.OrderAdmin();

            // Load Order State Item 
            ListOrderStatus.DataSource = _OrderAdminAccess.GetAllOrderStates();
            ListOrderStatus.DataTextField = "orderstatename";
            ListOrderStatus.DataValueField = "Orderstateid";
            ListOrderStatus.DataBind();

            // Remove Pending Approval Status from the dropdown
            ListItem li = ListOrderStatus.Items.FindByValue(((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL).ToString());
           
            if (li != null)
            {
                ListOrderStatus.Items.Remove(li);
            }

            // Remove Shipped Status from the dropdown
            ListItem liShipped = ListOrderStatus.Items.FindByValue(((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.SHIPPED).ToString());

            if (liShipped != null)
            {
                ListOrderStatus.Items.Remove(liShipped);
            }

            ZNode.Libraries.DataAccess.Entities.Order order = _OrderAdminAccess.DeepLoadByOrderID(OrderId);     //GetOrderByOrderID(OrderId);

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                string[] stores = profiles.StoreAccess.Split(',');
                Array Stores = (Array)stores;
                int found = Array.IndexOf(Stores, order.PortalId.ToString());
                if (found == -1)
                {
                    Response.Redirect("list.aspx", true);
                }
            }
            
            int ShippingId = Convert.ToInt32(order.ShippingID);

            ZNode.Libraries.Admin.ShippingAdmin _shipping = new ZNode.Libraries.Admin.ShippingAdmin();

            int ShipId = _shipping.GetShippingtypeid(ShippingId);

            // Set the Tracking number title based on the ShippingTypeId
            if (ShipId == 1)
            {
                pnlTrack.Visible = true;
                //TrackTitle.Visible = false;
                //TrackingNumber.Visible = false;
            }
            else if (ShipId == 2)
            {
                //pnlTrack.Visible = true;
                //TrackTitle.Text = "UPS Tracking Number";
            }
            else
            {
                //pnlTrack.Visible = true;
                //TrackTitle.Text = "FedEx Tracking Number";
            }

            if (order != null)
            {
                ListItem item = ListOrderStatus.Items.FindByValue(order.OrderStateID.ToString());
                if (item != null)
                {
                    item.Selected = true;
                }
                
                //TrackingNumber.Text = order.TrackingNumber;
                if (order.PaymentStatusIDSource != null)
                {
                    lblPaymentStatus.Text = order.PaymentStatusIDSource.Description;
                }
                
            }

            lblCustomerName.Text = order.BillingFirstName + " " + order.BillingLastName;
            lblTotal.Text = order.Total.Value.ToString("c");
           
        }

        #endregion
    }
}
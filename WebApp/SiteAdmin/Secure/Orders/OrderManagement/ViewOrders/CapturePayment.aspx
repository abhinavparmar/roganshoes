<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Orders.OrderManagement.ViewOrders.CapturePayment" Title="Manage Orders - Capture" CodeBehind="CapturePayment.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <h1>Capture Credit Card Payment</h1>
        <asp:Panel ID="pnlEdit" runat="server" Visible="true">
            <p>
                Please confirm if you want to capture this previously authorized payment.
            </p>
            <p>
                Note that this function works only with <b>Authorize.Net and PayFlow Pro Gateways.</b>
            </p>
            <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            <div style="margin-bottom: 20px; margin-top: 20px;">
                <div>
                    <b>Order ID: </b>
                    <asp:Label ID="lblOrderID" runat="server" />
                </div>
                <div>
                    <b>Transaction ID: </b>
                    <asp:Label ID="lblTransactionID" runat="server" />
                </div>
                <div>
                    <b>Customer Name: </b>
                    <asp:Label ID="lblCustomerName" runat="server" />
                </div>
                <div>
                    <b>Order Total: </b>
                    <asp:Label ID="lblTotal" runat="server" />
                </div>
            </div>
            <div class="ImageButtons">
                <asp:Button ID="Capture" runat="Server" CssClass="Size100" Text="CAPTURE" OnClick="Capture_Click" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="Cancel_Click" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
            <div>
                The transaction was successfully captured.
            </div>
            <div style="margin-top: 20px;">
                <a href="~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/Default.aspx" runat="server">Back to order list &raquo</a>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

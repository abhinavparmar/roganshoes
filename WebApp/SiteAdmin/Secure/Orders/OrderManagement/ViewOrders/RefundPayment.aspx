<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.OrderManagement.ViewOrders.RefundPayment" Title="Untitled Page" CodeBehind="RefundPayment.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <h1>Refund Customer's Credit Card</h1>
        <asp:Panel ID="pnlEdit" runat="server" Visible="true">
            <p>
                Use this page to void this transaction or apply a refund to your customer's credit
                card.
            </p>
            <p>
                Note that this function works only with <b>IP Commerce, Authorize.Net, PayFlow Pro and Cyber Source
                    Gateways.</b>
            </p>
            <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            <div style="margin-bottom: 20px; margin-top: 20px;">
                <div>
                    <b>Order ID: </b>
                    <asp:Label ID="lblOrderID" runat="server" />
                </div>
                <div>
                    <b>Transaction ID: </b>
                    <asp:Label ID="lblTransactionID" runat="server" />
                </div>
                <div>
                    <b>Customer Name: </b>
                    <asp:Label ID="lblCustomerName" runat="server" />
                </div>
                <div>
                    <b>Order Total: </b>
                    <asp:Label ID="lblTotal" runat="server" />
                </div>
            </div>
            <div>
                <div class="FieldStyle">
                    <asp:Label ID="lblCardNumber" runat="server">Credit Card Number </asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtCardNumber" runat="server" Width="130" Columns="30" MaxLength="100" />
                    <br />
                    <asp:RequiredFieldValidator ID="reqCardNumber" runat="server" ControlToValidate="txtCardNumber" ValidationGroup="refund"
                        ErrorMessage="Enter Credit Card Number" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>

                </div>
                <asp:Panel ID="pnlCreditCardInfo" runat="server" Visible="false">
                    <div class="FieldStyle">
                        Expiration Date
                    </div>
                    <div class="ValueStyle">
                        <div>
                            Select Month<span style="margin-left: 15px;">Select year</span>
                        </div>
                        <asp:DropDownList ID="lstMonth" runat="server" Width="80">
                            <asp:ListItem Value="01">Jan</asp:ListItem>
                            <asp:ListItem Value="02">Feb</asp:ListItem>
                            <asp:ListItem Value="03">Mar</asp:ListItem>
                            <asp:ListItem Value="04">Apr</asp:ListItem>
                            <asp:ListItem Value="05">May</asp:ListItem>
                            <asp:ListItem Value="06">Jun</asp:ListItem>
                            <asp:ListItem Value="07">Jul</asp:ListItem>
                            <asp:ListItem Value="08">Aug</asp:ListItem>
                            <asp:ListItem Value="09">Sep</asp:ListItem>
                            <asp:ListItem Value="10">Oct</asp:ListItem>
                            <asp:ListItem Value="11">Nov</asp:ListItem>
                            <asp:ListItem Value="12">Dec</asp:ListItem>
                        </asp:DropDownList>
                        <span style="margin-left: 5px;">
                            <asp:DropDownList ID="lstYear" runat="server" Width="80">
                            </asp:DropDownList>
                        </span>
                    </div>
                    <div class="FieldStyle">
                        Security Code
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtSecurityCode" runat="server" Columns="3" MaxLength="4" />
                        <asp:RequiredFieldValidator ControlToValidate="txtSecurityCode" CssClass="Error"
                            Display="Dynamic" ID="SecurityCodeValidator" runat="server" ErrorMessage="Enter Security Code"></asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <div class="FieldStyle">
                    Amount to Refund<span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtAmount" runat="server" Width="130" Columns="30" MaxLength="100" /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtAmount" ValidationGroup="refund"
                        runat="server" ErrorMessage="Amount is Required" CssClass="Error"></asp:RequiredFieldValidator><br />
                    <asp:RangeValidator
                        ID="RangeValidator1" runat="server" ControlToValidate="txtAmount" ErrorMessage="You must enter a valid amount" ValidationGroup="refund"
                        MaximumValue="99999999" Type="Currency" MinimumValue="0.01" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
                </div>
            </div>
            <div class="ClearBoth"></div>
            <div class="ImageButtons">
                <asp:Button ID="Void" runat="Server" CssClass="Size100" Text="VOID" OnClick="Void_Click" />
                <asp:Button ID="Refund" runat="Server" CssClass="Size100" Text="REFUND" OnClick="Refund_Click" ValidationGroup="refund" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="Cancel_Click" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
            <div id="OrderReceipt" runat="server">
                <%=ReceiptTemplate%>
            </div>
            <div style="margin-top: 20px;">
                <a id="A1" href="~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/Default.aspx" runat="server">Back to order
                    list &raquo</a>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

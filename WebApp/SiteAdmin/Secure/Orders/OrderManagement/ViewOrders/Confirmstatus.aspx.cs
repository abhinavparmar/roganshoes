using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;
using AdminZNodeLogging = ZNode.Libraries.ECommerce.Utilities.ZNodeLogging;

namespace SiteAdmin.Secure.Orders.OrderManagement.ViewOrders
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Orders.Sales.Orders Confirmstatus class
    /// </summary>
    public partial class Confirmstatus : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string _OrderStatus = string.Empty;
        private string _OderId = string.Empty;
        private string CancelLink = "Default.aspx";
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the order status.
        /// </summary>
        public string OrderStatus
        {
            get { return this._OrderStatus; }
            set { this._OrderStatus = value; }
        }

        /// <summary>
        /// Gets or sets the order Id
        /// </summary>
        public string OrderId
        {
            get { return this._OderId; }
            set { this._OderId = value; }
        }
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind the data for a Order id
        /// </summary>
        public void BindData()
        {
            this.OrderId = Convert.ToString(Request["itemid"]);
            OrderAdmin orderAdmin = new OrderAdmin();
            Order order = orderAdmin.GetOrderByOrderID(int.Parse(this.OrderId));

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                string[] stores = profiles.StoreAccess.Split(',');
                Array Stores = (Array)stores;
                int found = Array.IndexOf(Stores, order.PortalId.ToString());
                if (found == -1)
                {
                    Response.Redirect("list.aspx", true);
                }
            }

            this.OrderStatus = Convert.ToString((ZNodeOrderState)int.Parse(Request["OrderStatus"]));
        }

        #endregion

        #region Page Load

        /// <summary>
        /// Page Pre Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreInit(object sender, EventArgs e)
        {
            // Change the master file, if the user is Order Approver.
            if (HttpContext.Current.User.IsInRole("ORDER APPROVER"))
            {
                this.MasterPageFile = "~/SiteAdmin/Themes/Standard/order.master";
            }
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring   
            if (Request.Params["Itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["Itemid"].ToString());
                this.BindData();
            }
            else
            {
                this.ItemId = 0;
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink);
        }

        /// <summary>
        /// Confirm Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnConfirm_Click(object sender, EventArgs e)
        {
            OrderAdmin _OrderAdmin = new OrderAdmin();
            Order _order = _OrderAdmin.GetOrderByOrderID(Convert.ToInt32(Request["itemid"]));

            if (_order != null)
            {
                _order.OrderStateID = int.Parse(Request["OrderStatus"].ToString());
                bool isUpdated = _OrderAdmin.Update(_order);
                if (isUpdated)
                {
                    // Notify by email to customer and admin
                    if (this.SendEmailReceipt(_order))
                    {
                        Response.Redirect(this.CancelLink);
                    }
                }
            }
            else
            {
                Response.Redirect(this.CancelLink);
            }
        }
        #endregion

        #region Email Notification

        /// <summary>
        /// Send Mail Receipt
        /// </summary>
        /// <param name="order">Order object</param>
        /// <returns>Returns true if email receipt sent otherwise false</returns>
        private bool SendEmailReceipt(Order order)
        {
            string recepientEmail = order.BillingEmailId;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string myAccountPage = "http://" + ZNodeConfigManager.DomainConfig.DomainName + "/Account.aspx";
            if (string.IsNullOrEmpty(senderEmail))
            {
                errormessage.Text = "Sender email address (Customer service email) is empty.";
                return false;
            }
            
            if (string.IsNullOrEmpty(recepientEmail))
            {
                errormessage.Text = "Recepient email address is empty.";
                return false;
            }
            
            string subject;
            string bodytext;

            try
            {
                if (order.OrderStateID == (int)ZNodeOrderState.SUBMITTED)
                {
                    subject = "Your Order Approved : Order No:" + order.OrderID;
                    bodytext = "We would like to inform you that your order (Number: " + order.OrderID + ") has been APPROVED by the site admin. <br /><br />Please check your order in <a href='" + myAccountPage + "'>My Account Page</a>.";
                }
                else if (order.OrderStateID == (int)ZNodeOrderState.CANCELLED)
                {
                    subject = "Your Order Cancelled : Order No:" + order.OrderID;
                    bodytext = "We would like to inform you that your order (Number: " + order.OrderID + ") has been CANCELLED by the site admin. <br /><br />Please check your order in <a href='" + myAccountPage + "'>My Account Page</a>.";
                }
                else
                {
                    return true;
                }

                // Get message text.
                StreamReader rw = new StreamReader(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "OrderStatusNotification.htm"));
                string messageText = rw.ReadToEnd();

                Regex rx1 = new Regex("#BillingFirstName#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, order.BillingFirstName);

                string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
                string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
                int portalId = ZNodeConfigManager.SiteConfig.PortalID;

                ZEmailHelper zEmailHelper = new ZEmailHelper();
                ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
                string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

                string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
                string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

                //To add header to the template
                Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
                messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

                //To add footer to the template
                Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
                messageText = rxFooterContent.Replace(messageText, messageTextFooter);

                Regex rx2 = new Regex("#BillingLastName#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, order.BillingLastName);

                Regex rx5 = new Regex("#Message#", RegexOptions.IgnoreCase);
                messageText = rx5.Replace(messageText, bodytext);

                ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, messageText, true);

                return true;
            }
            catch (Exception ex)
            {
                errormessage.Text = "There was a problem sending the email to " + "<a href=\"mailto:" + recepientEmail + "\">" + recepientEmail + "</a>";

                // Log the message
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message + "\n\n" + ex.StackTrace + "\n\n");

                // Log exception, do not rethrow as this is non-critical
                ExceptionPolicy.HandleException(ex, "ZNODE_GLOBAL_EXCEPTION_POLICY");

                return false;
            }
        }
        #endregion
    }
}
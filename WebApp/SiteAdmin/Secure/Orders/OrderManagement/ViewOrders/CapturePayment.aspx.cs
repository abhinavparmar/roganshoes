using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Payment;

namespace SiteAdmin.Secure.Orders.OrderManagement.ViewOrders
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Orders.Sales.Orders Capture class
    /// </summary>
    public partial class CapturePayment : System.Web.UI.Page
    {
        #region Private Member Variables
        private int OrderID = 0;
        #endregion

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["itemid"] != null) || (Request.Params["itemid"].Length != 0))
            {
                this.OrderID = int.Parse(Request.Params["itemid"].ToString());
                lblOrderID.Text = this.OrderID.ToString();
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        /// <summary>
        /// Refund button clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Capture_Click(object sender, EventArgs e)
        {
            // Retrieve order
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            Order order = orderAdmin.GetOrderByOrderID(this.OrderID);
            ZNode.Libraries.Framework.Business.ZNodeEncryption enc = new ZNode.Libraries.Framework.Business.ZNodeEncryption();

            // Get payment settings
            int paymentSettingID = (int)order.PaymentSettingID;
            PaymentSettingService pss = new PaymentSettingService();
            PaymentSetting ps = pss.GetByPaymentSettingID(paymentSettingID);

            // Set gateway info
            GatewayInfo gi = new GatewayInfo();
            gi.GatewayLoginID = enc.DecryptData(ps.GatewayUsername);
            gi.GatewayPassword = enc.DecryptData(ps.GatewayPassword);
            gi.TransactionKey = enc.DecryptData(ps.TransactionKey);
            gi.Vendor = ps.Vendor;
            gi.Partner = ps.Partner;
            gi.TestMode = ps.TestMode;
            gi.Gateway = (GatewayType)ps.GatewayTypeID;

            string creditCardExp = Convert.ToString(order.CardExp);
            if (creditCardExp == null)
            {
                creditCardExp = string.Empty;
            }

            // Set credit card
            CreditCard cc = new CreditCard();
            cc.CreditCardExp = creditCardExp;
            cc.OrderID = order.OrderID;
            cc.TransactionID = order.CardTransactionID;

            GatewayResponse resp = new GatewayResponse();

            if ((GatewayType)ps.GatewayTypeID == GatewayType.AUTHORIZE)
            {
                GatewayAuthorize auth = new GatewayAuthorize();
                resp = auth.CapturePayment(gi, cc);
            }
            else if ((GatewayType)ps.GatewayTypeID == GatewayType.VERISIGN)
            {
                GatewayPayFlowPro pp = new GatewayPayFlowPro();
                resp = pp.CapturePayment(gi, cc);
            }
            else if ((GatewayType)ps.GatewayTypeID == GatewayType.PAYMENTECH)
            {
                GatewayOrbital pmt = new GatewayOrbital();
                resp = pmt.CapturePayment(gi, cc);
            }
            else if ((GatewayType)ps.GatewayTypeID == GatewayType.CYBERSOURCE)
            {
                GatewayCyberSource cyberSource = new GatewayCyberSource();
                cc.TransactionID = lblTransactionID.Text;
                cc.CardDataToken = order.CardAuthCode;
                cc.Amount = Decimal.Parse(order.Total.GetValueOrDefault(0).ToString());
                resp = cyberSource.Capture(gi, cc);
            }
            else
            {
                lblError.Text = "Error: Credit card payment capture is not supported for your gateway.";
            }

            if (resp.IsSuccess)
            {
                // Update order status
                // Refund status
                order.PaymentStatusID = 1;

                OrderService os = new OrderService();
                os.Update(order);

                pnlEdit.Visible = false;
                pnlConfirm.Visible = true;
            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("Could not complete request. The following response was returned by the gateway: ");
                sb.Append(resp.ResponseText);
                lblError.Text = sb.ToString();
            }
        }

        /// <summary>
        /// Cancel button clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("View.aspx?itemid=" + this.OrderID.ToString());
        }

        /// <summary>
        /// Bind fields
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.OrderAdmin _OrderAdminAccess = new ZNode.Libraries.Admin.OrderAdmin();
            ZNode.Libraries.DataAccess.Entities.Order order = _OrderAdminAccess.GetOrderByOrderID(this.OrderID);

            lblCustomerName.Text = order.BillingFirstName + " " + order.BillingLastName;
            lblTotal.Text = order.Total.Value.ToString("c");
            lblTransactionID.Text = order.CardTransactionID;
        }
    }
}
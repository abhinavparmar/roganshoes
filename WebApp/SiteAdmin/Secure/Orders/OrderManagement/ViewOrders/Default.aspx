<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.OrderManagement.ViewOrders.Default" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>View Orders</h1>
                <p>
                    Search and download orders and update order status.
                </p>
            </div>
            <div align="left">
                <h4 class="SubTitle">Search Orders</h4>
                <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
                    <div class="SearchForm">
                        <div class="RowStyle">
                            <div class="ItemStyle">
                                <span class="FieldStyle">Store Name</span><br />
                                <span class="ValueStyle">
                                    <asp:DropDownList ID="ddlPortal" runat="server">
                                    </asp:DropDownList>
                                </span>
                            </div>
                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle">Order ID</span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtorderid" runat="server" MaxLength="9"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtorderid"
                                            Display="Dynamic" ErrorMessage="<br />Enter valid Order ID" SetFocusOnError="true"
                                            ValidationExpression="(\d)+" ValidationGroup="grpReports" CssClass="Error"></asp:RegularExpressionValidator>
                                    </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">First Name</span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtfirstname" runat="server"></asp:TextBox></span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">Last Name</span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtlastname" runat="server"></asp:TextBox></span>
                                </div>
                            </div>
                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle">Company Name</span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtcompanyname" runat="server"></asp:TextBox></span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">Account ID</span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtaccountnumber" runat="server" MaxLength="9"></asp:TextBox><br />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtaccountnumber"
                                            Display="Dynamic" ErrorMessage="Enter a valid Number" SetFocusOnError="true"
                                            ValidationExpression="(\d)+" ValidationGroup="grpReports" CssClass="Error"></asp:RegularExpressionValidator></span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">Order Status</span><br />
                                    <span class="ValueStyle">
                                        <asp:DropDownList ID="ListOrderStatus" runat="server">
                                        </asp:DropDownList>
                                    </span>
                                </div>
                            </div>
                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle">Begin Date</span>
                                    <br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtStartDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="imgbtnStartDt"
                                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" /><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Begin date"
                                            ControlToValidate="txtStartDate" ValidationGroup="grpReports" CssClass="Error"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                                            CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format"
                                            ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                                            ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                                            runat="server" TargetControlID="txtStartDate">
                                        </ajaxToolKit:CalendarExtender>
                                    </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">End Date</span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtEndDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="ImgbtnEndDt"
                                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" /><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndDate"
                                            ErrorMessage="Enter End date" ValidationGroup="grpReports" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                                            CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format"
                                            ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                                            ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
                                            runat="server" TargetControlID="txtEndDate">
                                        </ajaxToolKit:CalendarExtender>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
                                            ValidationGroup="grpReports"
                                            ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage=" End Date must be greater than Begin date"
                                            Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                                    </span>
                                </div>
                            </div>
                            <div class="ClearBoth">
                                <asp:ImageButton ID="btnClear" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                                    onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                                    runat="server" CausesValidation="false" OnClick="BtnClearSearch_Click" AlternateText="Clear"/>
                                <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                                    onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                                    runat="server" OnClick="BtnSearch_Click" ValidationGroup="grpReports" AlternateText="Search"/>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <br />

            </div>
            <div>
                <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
            </div>
            <div>
                <h4 class="SubTitle" style="margin-bottom: -8px;">Order LisT</h4>
                <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="No Orders exist in the database."
                    EnableSortingAndPagingCallbacks="False" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                    OnRowCommand="UxGrid_RowCommand" PageSize="25" Width="100%" OnRowDataBound="uxGrid_RowDataBound">
                    <Columns>
                        <asp:HyperLinkField DataNavigateUrlFields="orderid" DataNavigateUrlFormatString="view.aspx?itemid={0}"
                            DataTextField="orderid" HeaderText="ID" SortExpression="orderid" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Store Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="OrderStateID" HeaderText="Order Status" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="OrderStatus" Text='<%# Eval("OrderStatus") %>'
                                    runat="server" Font-Bold="true" Font-Size="Smaller"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payment Status" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="PaymentStatus" Text='<%# Eval("PaymentStatusName") %>'
                                    runat="server" Font-Bold="true" Font-Size="Smaller"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="BillingFirstName" HeaderStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                                <asp:Label ID="headerTotal" Text="Name" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="CustomerName" Text='<%# ReturnName(Eval("BillingFirstName"),Eval("BillingLastName")) %>'
                                    runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Orderdate" HeaderText="Date" SortExpression="OrderDate" ItemStyle-Width="120"
                            DataFormatString="{0:MM/dd/yyyy hh:mm tt}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField SortExpression="total" HeaderText="Amount" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "total","{0:c}").ToString()%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="PaymentTypeID" HeaderText="Payment Type" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="PaymentType" Text='<%# Eval("PaymentTypeName") %>'
                                    runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ACTIONS" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="220px">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CausesValidation="False" ID="ViewOrder" Text="MANAGE ORDER &raquo"
                                    CommandArgument='<%# Eval("orderid") %>' CommandName="ViewOrder" />
                                <asp:LinkButton runat="server" Visible='<%# EnableCapture(Eval("PaymentTypeID"), Eval("PaymentStatusID")) %>'
                                    CausesValidation="false" ID="Capture" Text="Capture &raquo" CommandArgument='<%# Eval("orderid") %>'
                                    CommandName="Capture" />
                                <%-- <asp:LinkButton runat="server" CausesValidation="false" ID="ChangeStatus" Text="Status &raquo"
                                    CommandArgument='<%# Eval("orderid") %>' CommandName="Status" />--%>
                                <asp:LinkButton runat="server" CausesValidation="false" ID="RMA" Text="RMA" Visible='<%# EnableRMA(Eval("orderid"),Eval("OrderStatus"),Eval("Orderdate"),Eval("PaymentSettingId")) %>'
                                    CommandArgument='<%# Eval("orderid") %>' CommandName="RMA" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle"  />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </div>
            <div align="left">
                <h4 class="SubTitle">Download Orders
                </h4>
                <div align="left">
                    <div>
                        <div class="FieldStyle">
                            Starting Order ID
                        </div>
                        <small>Orders higher than the Starting Order ID will be downloaded.</small>
                    </div>
                    <div>
                        <asp:TextBox ID="OrderNumber" runat="server" MaxLength="9"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="OrderNumber"
                            ValidationGroup="Download" CssClass="Error" Display="Dynamic" ErrorMessage="You must specify a starting Order ID"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="OrderNumber"
                            ValidationGroup="Download" CssClass="Error" Display="Dynamic" ErrorMessage="Enter Valid starting Order ID"
                            ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    </div>
                    <div>
                        <uc1:Spacer ID="Spacer2" runat="server" SpacerHeight="10" />
                    </div>
                    <div class="ImageButtons">
                        <asp:Label ID="lblError" runat="server" Text="" CssClass="Error"></asp:Label>
                        <div class="Error">
                            <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
                        </div>
                        <asp:Button ID="ButDownload" runat="server" OnClick="ButDownload_Click" Text="Download Orders to CSV"
                            CssClass="Size175" ValidationGroup="Download" />&nbsp;&nbsp;<asp:Button ID="ButOrderLineItems"
                                runat="server" OnClick="ButOrderLineItemsDownload_Click" Text="Download Order LineItems"
                                CssClass="Size175" ValidationGroup="Download" /><br />
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.ServiceRequests.Add"  ValidateRequest="false" Codebehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <uc2:demomode id="DemoMode1" runat="server"></uc2:demomode>
            </h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                OnClick="BtnSubmit_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';" AlternateText="Submit"/>
            <asp:ImageButton ID="btnCancelTop" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                OnClick="BtnCancel_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';" AlternateText="Cancel"/>
        </div>
        <div class="ClearBoth">
            <uc1:spacer id="Spacer8" spacerheight="5" spacerwidth="3" runat="server"></uc1:spacer>
        </div>
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label></div>
        <div>
            <uc1:spacer id="Spacer1" spacerheight="5" spacerwidth="3" runat="server"></uc1:spacer>
        </div>
        
        <div class="FormView">
            <h4 class="SubTitle">
                Contact Information</h4>
            <div class="FieldStyle">
                First Name<span class="Asterix">*</span></div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                    ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                    Display="Dynamic" ErrorMessage="Enter First Name" SetFocusOnError="True" CssClass="Error"></asp:RequiredFieldValidator></div>
            <div class="FieldStyle">
                Last Name<span class="Asterix">*</span></div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                    ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName" Display="Dynamic"
                    ErrorMessage="Enter Last Name" SetFocusOnError="True" CssClass="Error"></asp:RequiredFieldValidator></div>
            <div class="FieldStyle">
                Company Name (Optional)</div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox></div>
            <div class="FieldStyle">
                Email ID<span class="Asterix">*</span></div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtEmailID" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEmailID"
                    Display="Dynamic" ErrorMessage="Enter Email ID" SetFocusOnError="True" CssClass="Error"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ID="regemailID" runat="server" ControlToValidate="txtEmailID" ErrorMessage="*Please use a valid email address."
                    Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    CssClass="Error" SetFocusOnError="True"></asp:RegularExpressionValidator></div>
            <div class="FieldStyle">
                Phone Number</div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtPhoneNo" runat="server" MaxLength="20"></asp:TextBox>   
                <asp:RegularExpressionValidator id="PhonenumberValidator"  runat="server" ControlToValidate="txtPhoneNo" ErrorMessage="Enter valid phone number" Display="Dynamic" 
                   ValidationExpression ="^([0-9\(\)\/\+ \-]*)$"   CssClass="Error"  SetFocusOnError="True" ></asp:RegularExpressionValidator>
            </div>
            <h4 class="SubTitle">
            Service Request Information</h4>
            <div class="FieldStyle">Create Date</div>
            <div class="ValueStyle">
                <b><asp:Label ID="lblCaseDate" runat="server" /></b>
            </div>
            
            <div class="FieldStyle">
                Origin</div>
            <div class="ValueStyle">
                <b><asp:Label ID="lblCaseOrigin" runat="server" /></b></div>
            
            <div><znode:storename id="uxStoreName" VisibleLocaleDropdown="false" runat="server" /></div>            
    
            <div class="FieldStyle">
                Status</div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstCaseStatus" runat="server">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                Priority</div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstCasePriority" runat="server">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                Title<span class="Asterix">*</span></div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCaseTitle" runat="server" MaxLength="50" Columns="20"></asp:TextBox> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtCaseTitle"
                    Display="Dynamic" ErrorMessage="Enter Case Title" SetFocusOnError="True" CssClass="Error"></asp:RequiredFieldValidator></div>            
            <div class="FieldStyle">
                Message<span class="Asterix">*</span></div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCaseDescription" runat="server" TextMode="MultiLine" Height="190px"
                    MaxLength="10000" Width="600px"></asp:TextBox><br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCaseDescription"
                    Display="Dynamic" ErrorMessage="Enter  Case Description" SetFocusOnError="True"
                    CssClass="Error"></asp:RequiredFieldValidator>
            </div>
            
        </div>
        <div class="ClearBoth">
            <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                OnClick="BtnSubmit_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';" AlternateText="Submit" />
            <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="false" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                OnClick="BtnCancel_Click" onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';"
                onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';" AlternateText="Cancel"/>
        </div>
        <asp:HiddenField ID="HiddenOldStatus" runat="server" />
    </div>
</asp:Content>

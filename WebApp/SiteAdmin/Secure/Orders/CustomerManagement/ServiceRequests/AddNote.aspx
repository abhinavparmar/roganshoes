<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.ServiceRequests.AddNote" ValidateRequest="false" Title="Untitled Page"
    CodeBehind="AddNote.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div style="float: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="LeftFloat">
            <h1><asp:Label ID="lblHeading" runat="server" Text="Label"></asp:Label><uc2:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        <div class="ClearBoth">
        </div>
        <div class="FormView">
            <div class="FieldStyle">
                Title<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='txtNoteTitle' runat='server' MaxLength="50" Columns="20"></asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="txtNoteTitle" ID="RequiredFieldValidator1"
                    runat="server" ErrorMessage="* Enter Note Title" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                Note<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="ClearBoth">
                <asp:Label ID="lblError" CssClass="Error" ForeColor="DarkRed" runat="server" Text="Label"
                    Visible="False"></asp:Label>
            </div>
        </div>
        <div>
            <asp:ImageButton ID="btnCancel" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnSubmit" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.CustomerManagement.Profiles
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_settings_Profile_add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Private Member Variables
        private int ItemId = 0;
        private string ListPageLink = "~/SiteAdmin/Secure/Orders/CustomerManagement/Profiles/Default.aspx";
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["ItemID"] != null)
            {
                this.ItemId = int.Parse(Request.Params["ItemID"].ToString());
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit Profile � ";
                    this.BindData();
                }
                else
                {
                    lblTitle.Text = "Add a New Profile";
                    chkShowPrice.Checked = true;
                }
            }
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind fields for this Profile
        /// </summary>
        protected void BindData()
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            ZNode.Libraries.DataAccess.Entities.Profile profile = profileAdmin.GetByProfileID(this.ItemId);

            if (profile != null)
            {
                ProfileName.Text = Server.HtmlDecode(profile.Name);
                chkShowPrice.Checked = profile.ShowPricing;
                chkShowOnPartner.Checked = profile.ShowOnPartnerSignup;

                if (profile.UseWholesalePricing.HasValue)
                {
                    chkUseWholesalePrice.Checked = profile.UseWholesalePricing.Value;
                }

                chkTaxExempt.Checked = profile.TaxExempt;
                ExternalAccountNum.Text = profile.DefaultExternalAccountNo;

                if (profile.Weighting.HasValue)
                {
                    txtWeighting.Text = profile.Weighting.ToString();
                }

                // Set Page Title
                lblTitle.Text += profile.Name;
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            ZNode.Libraries.DataAccess.Entities.Profile profile = new ZNode.Libraries.DataAccess.Entities.Profile();

            if (this.ItemId > 0) 
            {
                // Edit Mode
                // Get profile by Id
                profile = profileAdmin.GetByProfileID(this.ItemId);
            }

            if (ExternalAccountNum.Text.Trim().Length > 0)
            {
                profile.DefaultExternalAccountNo = ExternalAccountNum.Text.Trim();
            }
            else if (this.ItemId > 0)
            {
                profile.DefaultExternalAccountNo = ExternalAccountNum.Text.Trim();
            }

            profile.Name = Server.HtmlEncode(ProfileName.Text.Trim());
            profile.ShowPricing = chkShowPrice.Checked;
            profile.ShowOnPartnerSignup = chkShowOnPartner.Checked;
            profile.UseWholesalePricing = chkUseWholesalePrice.Checked;
            profile.TaxExempt = chkTaxExempt.Checked;
            profile.Weighting = Convert.ToDecimal(txtWeighting.Text);

            bool Status = false;

            if (this.ItemId > 0) 
            {
                // Edit Mode
                // If exists then update the store.
                Status = profileAdmin.Update(profile);
                
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit Profile - " + ProfileName.Text.Trim(), ProfileName.Text.Trim());
            }
            else
            {
                Status = profileAdmin.Add(profile);
               
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Profile - " + ProfileName.Text.Trim(), ProfileName.Text.Trim());
            }

            if (Status)
            {
                Response.Redirect(this.ListPageLink);
            }
            else
            {
                lblErrorMsg.Text = "Unable to update profile. Please try again.";
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListPageLink);
        }
        #endregion
    }
}
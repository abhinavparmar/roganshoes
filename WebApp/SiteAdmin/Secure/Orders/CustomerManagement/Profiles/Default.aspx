<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.Profiles.Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                Profiles
                 <div class="tooltip" >
                        <a href="javascript:void(0);" class="learn-more" ><span>
                            <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                        <div class="content">
                            <h6>
                                Help</h6>
                            <p>
                                Examples of profiles include "Wholesale Customer", "Loyal Customer", "Affiliate", "B2B", etc.
                            </p>
                        </div>
              </div>
            </h1>
        </div>
        <div class="ButtonStyle">
               <zn:LinkButton ID="btnAddProfile" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAddProfile_Click" Text="Add New Profile" 
                ButtonPriority="Primary"/>
        </div>
        <div class="ClearBoth">
            <p>
                Create customer groups using profiles. You can then apply special promotions, pricing, taxes, shipping, and display options to these profiles.</p>
            
        </div>
        <h4 class="GridTitle">
            Profile List</h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" AllowSorting="True"
            EmptyDataText="No profiles available to display.">
            <Columns>
                <asp:BoundField DataField="ProfileID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Profile Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='add.aspx?ItemID=<%# DataBinder.Eval(Container.DataItem, "ProfileID").ToString()%>'>
                            <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Show Price" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img11" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowPricing").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Wholesale Pricing" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img51" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "UseWholesalePricing").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tax Exempt" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img31" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "TaxExempt").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Affiliate Sign-up" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img id="Img15" runat="server" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowOnPartnerSignup").ToString()))%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ACTIONS" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div class="LeftFloat" style="width: 40%; text-align: left">
                            <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("ProfileID") %>'
                                CommandName="Edit" Text="EDIT &raquo" CssClass="LinkButton" />
                        </div>
                        <div class="LeftFloat" style="width: 50%; text-align: left">
                            <asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("ProfileID") %>'
                                CommandName="Delete" Text="DELETE &raquo " CssClass="LinkButton" />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </div>
</asp:Content>

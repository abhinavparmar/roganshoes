<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.CustomerManagement.Customers.AddNote" ValidateRequest="false"
    Title="Untitled Page"  Codebehind="AddNote.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/AddNote.ascx" TagName="NotesAdd" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
     <ZNode:NotesAdd ID="uxNotesAdd" runat="server" RoleName="" />
</asp:Content>

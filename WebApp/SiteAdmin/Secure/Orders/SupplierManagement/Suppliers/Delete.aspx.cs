using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.SupplierManagement.Suppliers
{
    /// <summary>
    /// Represents the Site Admin - SiteAdmin.Secure.Orders.SupplierManagement.Suppliers.Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private AccountAdmin accountAdmin = new AccountAdmin();
        private int SupplierId = 0;
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            
            if (Request.Params["supplier"] != null)
            {
                this.SupplierId = int.Parse(Request.Params["supplier"]);
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Delete button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            Account _account = this.accountAdmin.GetByAccountID(this.ItemId);

            Guid UserKey = new Guid();

            try
            {
                if (_account != null)
                {
                    if (_account.UserID.HasValue)
                    {
                        // Get UserId for this account
                        UserKey = _account.UserID.Value;
                    }

                    bool status = this.accountAdmin.Delete(_account);

                    // Check 
                    if (status) 
                    {
                        // Get user by Unique GUID - User Id
                        MembershipUser user = Membership.GetUser(UserKey);
                        string AssociateName = "Delete Account - " + user.UserName;
                        string AccountName = user.UserName;
                        if (user != null)
                        {
                            // Delete online account
                            Membership.DeleteUser(user.UserName);
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, AccountName);

                            SupplierAdmin supplieradmin = new SupplierAdmin();
                        }
                        
                        // Redirect to list page
                        Response.Redirect("~/SiteAdmin/Secure/Orders/SupplierManagement/Suppliers/Add.aspx?itemid=" + this.SupplierId);
                    }
                }
                else
                {
                    lblMsg.Text = "Unable to delete this account. Please try again.";
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);

                lblMsg.Text = "Unable to delete this account. Please remove any child relationship with this account.";
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Orders/SupplierManagement/Suppliers/Add.aspx?itemid=" + this.SupplierId);
        }
        #endregion
    }
}
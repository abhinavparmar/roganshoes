using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.SupplierManagement.Suppliers
{
    /// <summary>
    /// Represents the SiteAdmin -  SiteAdmin.Secure.Orders.SupplierManagement.Suppliers.AddAccount class
    /// </summary>
    public partial class AddAccount : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int AccountId;
        private int SupplierId;
        private ZNodeAddress _billingAddress = new ZNodeAddress();
        private ZNodeAddress _shippingAddress = new ZNodeAddress();        
        private int Mode;
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.Params["itemid"] == null)
            {
                this.AccountId = 0;
            }
            else
            {
                this.AccountId = int.Parse(Request.Params["itemid"].ToString());
            }
            
            if (HttpContext.Current.Request.Params["supplier"] == null)
            {
                this.SupplierId = 0;
            }
            else
            {
                this.SupplierId = int.Parse(Request.Params["supplier"].ToString());
            }

            if (HttpContext.Current.Request.Params["mode"] == null)
            {
                this.Mode = 0;
            }
            else
            {
                this.Mode = int.Parse(Request.Params["mode"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindCountry();
                this.BindData();

                if (this.AccountId > 0)
                {
                    lblTitle.Text = "Edit Account Information";
                }
                else
                {
                    lblTitle.Text = "Add Account Information";
                }
            }
        }

        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if ((lstBillingCountryCode.SelectedValue == "US") && (!string.IsNullOrEmpty(txtBillingState.Text.Trim())))
            {
                if (txtBillingState.Text.Trim().Length > 2 || txtBillingState.Text.Trim().Length < 2)
                {
                    lblErrorMsg.Text = Resources.CommonCaption.ValidateStateCode;
                    return;
                }
            }
            
            ZNode.Libraries.Admin.AccountAdmin userAccountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account _UserAccount = new Account();
            AddressService addressService = new AddressService();

            if (this.AccountId > 0)
            {
                _UserAccount = userAccountAdmin.GetByAccountID(this.AccountId);
            }

            if (this.AccountId == 0)
            {
                _UserAccount.CreateDte = System.DateTime.Now;
            }

            _UserAccount.AccountProfileCode = DBNull.Value.ToString();
            _UserAccount.UpdateUser = System.Web.HttpContext.Current.User.Identity.Name;
            _UserAccount.UpdateDte = System.DateTime.Now;
            _UserAccount.Email = txtBillingEmail.Text;
            _UserAccount.UserID = _UserAccount.UserID;
            _UserAccount.AccountID = this.AccountId;

            Address address = userAccountAdmin.GetDefaultBillingAddress(this.AccountId);
            if (address == null)
            {
                address = new Address();
                address.IsDefaultBilling = true;
                address.AccountID = this.AccountId;
            }
            
            // Set user's address
            address.FirstName = Server.HtmlEncode(txtBillingFirstName.Text);
            address.LastName = Server.HtmlEncode(txtBillingLastName.Text);
            address.CompanyName = Server.HtmlEncode(txtBillingCompanyName.Text);
            address.Street = Server.HtmlEncode(txtBillingStreet1.Text);
            address.Street1 = Server.HtmlEncode(txtBillingStreet2.Text);
            address.City = Server.HtmlEncode(txtBillingCity.Text);
            address.StateCode = Server.HtmlEncode(txtBillingState.Text);
            address.PostalCode = txtBillingPostalCode.Text;
            address.PhoneNumber = txtBillingPhoneNumber.Text;
            
            if (lstBillingCountryCode.SelectedIndex != -1)
            {
                address.CountryCode = lstBillingCountryCode.SelectedValue;
            }
            
            if (address.AddressID > 0)
            {
                addressService.Update(address);
            }
            else
            {
                addressService.Insert(address);
            }

            try
            {
                if (_UserAccount.UserID.HasValue)
                {
                    // Get the User associated with the specified Unique identifier
                    MembershipUser user = Membership.Providers["ZNodeAdminMembershipProvider"].GetUser(_UserAccount.UserID.Value, false);

                    user.Email = txtBillingEmail.Text.Trim();
                    
                    // Update the database with the Email Id for the specified user
                    Membership.UpdateUser(user);

                    if (Password.Text.Length > 0)
                    {
                        string passwordAnswer = Answer.Text.Trim();

                        if (passwordAnswer.Length == 0)
                        {
                            // Display error message
                            lblAnswerErrorMsg.Text = " * Enter security answer";
                            lblErrorMsg.Text = " * Enter security answer";
                            return;
                        }

                        // Update new password
                        user.ChangePassword(user.ResetPassword(), Password.Text.Trim());

                        // Log password for further debugging
                        ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, Password.Text.Trim());

                        // Change password question and answer
                        user.ChangePasswordQuestionAndAnswer(Password.Text.Trim(), ddlSecretQuestions.SelectedItem.Text, Answer.Text.Trim());
                    }
                }
                else
                {
                    if (UserID.Text.Trim().Length > 0)
                    {
                        string errorMessage = string.Empty;

                        // Check password field
                        if (Password.Text.Trim().Length == 0)
                        {
                            errorMessage = " * Enter password <br />";
                            lblPwdErrorMsg.Text = " * Enter password";
                        }

                        // Check security answer field
                        if (Answer.Text.Trim().Length == 0)
                        {
                            errorMessage += " * Enter security answer";
                            lblAnswerErrorMsg.Text = " * Enter security answer";
                        }

                        // Show error message
                        if (errorMessage.Length > 0)
                        {
                            lblErrorMsg.Text = errorMessage;
                            return;
                        }

                        MembershipUser user = Membership.GetUser(UserID.Text.Trim());

                        // Online account DOES NOT exist 
                        if (user == null) 
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
                            MembershipCreateStatus status = MembershipCreateStatus.ProviderError;
                            log.LogActivityTimerStart();
                            MembershipUser newUser = Membership.CreateUser(UserID.Text.Trim(), Password.Text.Trim(), txtBillingEmail.Text.Trim(), ddlSecretQuestions.SelectedItem.Text, Answer.Text.Trim(), true, out status);

                            if (status == MembershipCreateStatus.Success)
                            {
                                log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, UserID.Text.Trim());

                                Roles.AddUserToRole(newUser.UserName, "VENDOR");
                                
                                // Log password for further debugging
                                ZNodeUserAccount.LogPassword((Guid)newUser.ProviderUserKey, Password.Text.Trim());

                                // Update provider user key
                                _UserAccount.UserID = new Guid(newUser.ProviderUserKey.ToString());
                            }
                            else
                            {
                                log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserID.Text.Trim());
                                lblErrorMsg.Text = "Unable to create online account. Please try again.";
                                return;
                            }
                        }
                        else
                        {
                            lblErrorMsg.Text = "User Id already exists. Please use differnet user id.";
                            return;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                lblErrorMsg.Text = exception.Message;
                return;
            }

            bool Check = false;

            if (this.AccountId > 0)
            {
                // Set update date
                _UserAccount.UpdateDte = System.DateTime.Now;

                Check = userAccountAdmin.Update(_UserAccount);

                // Log Activity
                string AssociateName = "Edit of Account - " + UserID.Text.Trim();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, "Account");
            }
            else
            {
                Check = userAccountAdmin.Insert(_UserAccount);

                // Log Activity
                string AssociateName = "Creation of Account - " + UserID.Text.Trim();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, "Account");
            }
            
            // Check Boolean Value
            if (Check)
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/SupplierManagement/Suppliers/Add.aspx?mode=" + this.Mode + "&itemid=" + this.SupplierId);
            }
            else
            {
                lblErrorMsg.Text = string.Empty;
            }
        }

        /// <summary>
        ///  Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.SupplierId > 0)
            {
                Response.Redirect("~~/SiteAdmin/Secure/Orders/SupplierManagement/Suppliers/Add.aspx?itemid=" + this.SupplierId);
            }
            else
            {
                Response.Redirect("~~/SiteAdmin/Secure/Orders/SupplierManagement/Suppliers/Default.aspx");
            }
        }
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Account Details
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.AccountAdmin _UserAccountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account _UserAccount = _UserAccountAdmin.GetByAccountID(this.AccountId);

            if (_UserAccount != null)
            {
                // Login Info
                if (_UserAccount.UserID.HasValue)
                {
                    UserID.Enabled = false;

                    MembershipUser _user = Membership.GetUser(_UserAccount.UserID.Value);
                    UserID.Text = _user.UserName;
                    ddlSecretQuestions.Text = _user.PasswordQuestion;
                }

                txtBillingEmail.Text = _UserAccount.Email;

                AccountAdmin accountAdmin = new AccountAdmin();
                Address defaultBillingAddress = accountAdmin.GetDefaultBillingAddress(_UserAccount.AccountID);
                if (defaultBillingAddress != null)
                {
                    // Set field values
                    txtBillingFirstName.Text = Server.HtmlDecode(defaultBillingAddress.FirstName);
                    txtBillingLastName.Text = Server.HtmlDecode(defaultBillingAddress.LastName);
                    txtBillingCompanyName.Text = Server.HtmlDecode(defaultBillingAddress.CompanyName);
                    txtBillingStreet1.Text = Server.HtmlDecode(defaultBillingAddress.Street);
                    txtBillingStreet2.Text = Server.HtmlDecode(defaultBillingAddress.Street1);
                    txtBillingCity.Text = Server.HtmlDecode(defaultBillingAddress.City);
                    txtBillingState.Text = Server.HtmlDecode(defaultBillingAddress.StateCode);
                    txtBillingPostalCode.Text = Server.HtmlDecode(defaultBillingAddress.PostalCode);
                    ListItem blistItem = lstBillingCountryCode.Items.FindByValue(defaultBillingAddress.CountryCode);
                    if (blistItem != null)
                    {
                        lstBillingCountryCode.SelectedIndex = lstBillingCountryCode.Items.IndexOf(blistItem);
                    }

                    txtBillingPhoneNumber.Text = defaultBillingAddress.PhoneNumber;
                }
            }
        }

        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            // PortalCountry
            PortalCountryHelper portalCountryHelper = new PortalCountryHelper();
            DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            DataView BillingView = new DataView(countryDs.Tables[0]);
            BillingView.RowFilter = "BillingActive = 1";

            // Billing Drop Down List
            lstBillingCountryCode.DataSource = BillingView;
            lstBillingCountryCode.DataTextField = "Name";
            lstBillingCountryCode.DataValueField = "Code";
            lstBillingCountryCode.DataBind();

            ListItem blistItem = lstBillingCountryCode.Items.FindByValue("US");
            if (blistItem != null)
            {
                lstBillingCountryCode.SelectedIndex = lstBillingCountryCode.Items.IndexOf(blistItem);
            }
        }

        #endregion
    }
}
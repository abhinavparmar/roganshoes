<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Orders.SupplierManagement.Suppliers.Default" ValidateRequest="false" CodeBehind="Default.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div class="LeftFloat" style="width: 80%; text-align: left">
            <h1>Suppliers</h1>
            <p>
                Manage vendors that will fulfill products in your store.
            </p>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAddSupplier_Click" Text="Add New Supplier"
                ButtonPriority="Primary" />

        </div>
    </div>
    <div align="left">
        <h4 class="SubTitle">Search Suppliers
        </h4>
        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtName" runat="server" ValidationGroup="grpSearch"></asp:TextBox>
                        </span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Status</span>
                        <br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlSupplierStatus" runat="server">
                                <asp:ListItem Selected="True" Text="All" Value=""></asp:ListItem>
                                <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Inactive" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <asp:ImageButton ID="btnClearSearch" CausesValidation="false" runat="server" OnClick="BtnClearSearch_Click"
                        onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif" AlternateText="Clear"/>
                    <asp:ImageButton ID="btnSearch" runat="server" OnClick="BtnSearch_Click" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif" AlternateText="Search"/>
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">Suppliers List
        </h4>
        <div>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
                OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText="No vendor exist in the database.">
                <Columns>
                    <asp:BoundField DataField="supplierid" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <a href='add.aspx?itemid=<%# DataBinder.Eval(Container.DataItem, "Supplierid").ToString()%>'>
                                <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ACTIONS" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <div class="LeftFloat" style="width: 20%; text-align: left">
                                <asp:LinkButton ID="btnEdit" CommandName="Edit" Text="Edit &raquo" CommandArgument='<%# Eval("supplierid") %>'
                                    runat="server" CssClass="LinkButton" />
                            </div>
                            <div class="LeftFloat" style="width: 20%; text-align: left">
                                <asp:LinkButton ID="btnDelete" CommandName="Delete" Text="Delete &raquo" runat="server"
                                    CommandArgument='<%# Eval("supplierid") %>' CssClass="LinkButton" Width="50px" />
                                <asp:HiddenField ID="hdnSupplierName" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>' />
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
        <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
    </div>
</asp:Content>

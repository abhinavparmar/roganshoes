﻿<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Orders.ReturnsManagement.RMAConfiguration.Default" ValidateRequest="false"
    CodeBehind="Default.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="MobileSettingsList" Src="~/SiteAdmin/Controls/Default/MobileSettingsList.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <script type="text/javascript">
            function textboxMultilineMaxNumber(txt, maxLen) {
                try {
                    if (txt.value.length > (maxLen - 1))
                        return false;
                } catch (e) {
                }
            }
        </script>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>RMA Configuration</h1>
            <p style="width: 650px;">
                Configure RMA rules that determine how return requests are managed in your store.
            </p>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer10" SpacerHeight="50" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div style="clear: both;">
            <ajaxToolKit:TabContainer ID="tabStoreSettings" runat="server">
                <ajaxToolKit:TabPanel ID="pnlGeneral" runat="server">
                    <HeaderTemplate>
                        General 
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="FormView">
                            <div>
                                <ZNode:Spacer ID="Spacer18" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>

                            <div class="FieldStyle1">
                                Maximum number of days RMA can be<br />
                                requested after order completion
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtMaxNoOfDays" MaxLength="5" runat="server" Width="100px"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="reqMaxNoOfDays" runat="server" ControlToValidate="txtMaxNoOfDays"
                                    ErrorMessage="Enter Maximum number of days" CssClass="Error" Display="Dynamic" ValidationGroup="General"></asp:RequiredFieldValidator>

                                <asp:RegularExpressionValidator ID="regMaxNoofDays" runat="server" ControlToValidate="txtMaxNoOfDays"
                                    ErrorMessage="Enter valid number of days" Display="Dynamic" ValidationExpression="[0-9]+" ValidationGroup="General"
                                    CssClass="Error"></asp:RegularExpressionValidator>
                            </div>
                              <div class="FieldStyle1">
                                Enable Email Notifications
                            </div>
                            <div class="ValueStyle">
                                <asp:DropDownList ID="ddlEnable" runat="server">
                                    <asp:ListItem Selected="True" Text="Yes" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>

                            </div>

                            <div class="FieldStyle1">
                                Returns Department Title
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtDisplayName" MaxLength="100" runat="server" Width="250px"></asp:TextBox>
                                   <div class="tooltip">
                                  <a href="javascript:void(0);" class="learn-more"><span>
                                      <asp:Localize ID="Localize1" runat="server"></asp:Localize></span></a>
                                      <div class="content">
                                          <h6>Help</h6>
                                          <p>
                                             This title will be displayed on the RMA form. Example: "Acme Inc. Returns Department"
                                             </p>
                      
                                      </div>
                                  </div>
                            </div>
                            <div class="FieldStyle1">
                                Customer Service Email ID
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtdepartmentemail" MaxLength="100" runat="server" Width="250px"></asp:TextBox>

                                <asp:RegularExpressionValidator ID="regdepartmentemail" runat="server" ControlToValidate="txtdepartmentemail"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Enter a valid email address."
                                    CssClass="Error" Display="Dynamic" ValidationGroup="General"></asp:RegularExpressionValidator>

                            </div>

                            <div class="FieldStyle1">
                               Mailing Address for Returns
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtAddress" runat="server" Width="250px" MaxLength="250"></asp:TextBox>
                                  <div class="tooltip">
                                  <a href="javascript:void(0);" class="learn-more"><span>
                                      <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                                      <div class="content">
                                          <h6>Help</h6>
                                          <p>
                                             Enter the mailing address where the customer must ship their product with the RMA form.
                                             </p>                      
                                      </div>
                                  </div>
                            </div>
                            <div class="FieldStyle1">
                                Shipping Instructions
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtShippingDirection" runat="server" Width="250px" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                 <div class="tooltip">
                                  <a href="javascript:void(0);" class="learn-more"><span>
                                      <asp:Localize ID="Localize3" runat="server"></asp:Localize></span></a>
                                      <div class="content">
                                          <h6>Help</h6>
                                          <p>
                                            Enter instructions for your customer such as packing, shipping, etc.
                                            </p>                      
                                      </div>
                                  </div>
                            </div>
                          
                            <div class="ClearBoth">
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer1110" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>

                            </div>
                            <div>

                                <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                                    runat="server" AlternateText="Cancel" OnClick="btnCancel_Click" />

                                <asp:ImageButton ID="btnSave" onmouseover="this.src='../../../../Themes/Images/buttons/button_save_highlight.gif';" ValidationGroup="General"
                                    onmouseout="this.src='../../../../Themes/Images/buttons/button_Save.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_save.gif"
                                    runat="server" AlternateText="Save" OnClick="btnSave_Click" />

                            </div>

                        </div>
                    </ContentTemplate>

                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="TabPanel2" runat="server">
                    <HeaderTemplate>
                        Reasons for Return
                    </HeaderTemplate>
                    <ContentTemplate>

                        <div class="ViewForm200">
                            <asp:Panel ID="pnlDiplayReasonGrid" runat="server" Visible="true">
                                <div>
                                    <ZNode:Spacer ID="Spacer31" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                </div>
                                <div class="ImageButtons">
                                    <div class="ButtonStyle">
                                        <zn:LinkButton ID="lbCreateReason" runat="server" ButtonType="Button" OnClick="lbCreateReason_Click"
                                            Text="Create Return Reason" CausesValidation="false" ButtonPriority="Primary" />
                                    </div>
                                    <div>
                                        <ZNode:Spacer ID="Spacer8" runat="server" SpacerHeight="15" SpacerWidth="3" />
                                    </div>
                                </div>
                                <div colspan="2" valign="middle">

                                    <asp:GridView ID="gvReason" ShowHeader="true" CaptionAlign="Left" runat="server"
                                        ForeColor="Black" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="100%" GridLines="None" EmptyDataText="No Reason for Return exist in the database."
                                        AllowPaging="True" PageSize="20" OnRowCommand="gvReason_RowCommand" OnRowCreated="gvReason_RowCreated"
                                        OnPageIndexChanging="gvReason_PageIndexChanging">
                                        <Columns>
                                            <asp:BoundField DataField="ReasonForReturnID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Return Reason" HeaderStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Is Enabled" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                                <ItemTemplate>
                                                    <img alt="" id="Img11" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsEnabled").ToString()))%>'
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ACTION" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate> 
                                                        <asp:LinkButton CausesValidation="false" ID="EditProductView" Text="Edit &raquo"
                                                            CommandArgument='<%# Eval("ReasonForReturnID") %>'
                                                            CommandName="EditReason" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="Delete" Text="Delete &raquo" CommandArgument='<%# Eval("ReasonForReturnID") %>'
                                                            CommandName="RemoveItem" Visible='<%# HideDeleteButton(Eval("ReasonForReturnID").ToString()) %>' CausesValidation="false"
                                                            runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                        <FooterStyle CssClass="FooterStyle" />
                                    </asp:GridView>


                                </div>
                            </asp:Panel>
 
                            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />

                            <ajaxToolKit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="btnShowPopup"
                                PopupControlID="pnlCreateReason" BackgroundCssClass="modalBackground" />


                            <asp:Panel ID="pnlCreateReason" runat="server" Style="display: none;" CssClass="PopupStyle" Width="400" Visible="true" >


                                <h4 class="SubTitle">
                                    <asp:Label ID="lblTitle" runat="server" Text="Create a Return Reason"></asp:Label></h4>
                               

                                    <div class="FieldStyle" style="width: 40%">
                                        Name
                                    </div>
                                    <div class="ValueStyle" style="width: 55%">
                                        <asp:TextBox ID="txtName" runat="server" MaxLength="250" Width="200px"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtName" ValidationGroup="grpReasonForReturn"
                                            ErrorMessage="Enter Name" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>

                                    </div>
                                    <div>
                                        <ZNode:Spacer ID="Spacer2" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>

                                    </div>
                                    <div class="FieldStyle" style="width: 40%">
                                        Enabled
                                    </div>
                                    <div class="ValueStyle" style="width: 55%">
                                        <asp:DropDownList ID="ddlReasonenabled" runat="server">
                                            <asp:ListItem Selected="True" Text="Yes" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="hdnReasonID" runat="server" Value="0" />

                                    </div>

                                    <div class="ClearBoth">
                                    </div>
                                    <div>
                                        <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>

                                    </div>
                                    <div>
                                        <div class="ImageButtons">
                                            <asp:Button CssClass="Size100" ID="btnReasonOk" CausesValidation="False" Text="Ok" Visible="false"
                                                runat="server" OnClick="btnReasonOk_Click"></asp:Button>
                                        </div>
                                        <asp:ImageButton ID="btnSaveReason" onmouseover="this.src='../../../../Themes/Images/buttons/button_save_highlight.gif';"
                                            onmouseout="this.src='../../../../Themes/Images/buttons/button_Save.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_save.gif"
                                            runat="server" AlternateText="Submit" OnClick="btnSaveReason_Click" ValidationGroup="grpReasonForReturn" />
                                        <asp:ImageButton ID="btnCancelReason" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                                            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                                            runat="server" AlternateText="Cancel" OnClick="btnCancelReason_Click" />

                                    </div> 
                            </asp:Panel>
                             </div>

                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlrequestStatuses" runat="server">
                    <HeaderTemplate>
                        Request Statuses 
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="ViewForm200" id="5">

                            <asp:Panel ID="pnlRequeststatus" runat="server">
                                <div>
                                    <ZNode:Spacer ID="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                </div>

                                <div colspan="2" valign="middle">

                                    <asp:GridView ID="gvRequeststatus" ShowHeader="true" CaptionAlign="Left" runat="server"
                                        ForeColor="Black" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="100%" GridLines="None" EmptyDataText="No REquest status exist in the database."
                                        AllowPaging="True" PageSize="20" OnRowCommand="gvRequeststatus_RowCommand" OnRowCreated="gvRequeststatus_RowCreated"
                                        OnPageIndexChanging="gvRequeststatus_PageIndexChanging">
                                        <Columns>
                                            <asp:BoundField DataField="RequestStatusID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Request Status" HeaderStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:BoundField>

                                            <asp:TemplateField HeaderText="ACTION" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate> 
                                                             <asp:LinkButton ID="EditProductView" Text="Edit &raquo" CausesValidation="false"
                                                            CommandArgument='<%# Eval("RequestStatusID") %>' CommandName="EditStatus" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                       <%-- <asp:LinkButton ID="Delete" Text="Delete &raquo" CommandArgument='<%# Eval("RequestStatusID") %>' CausesValidation="false"
                                                            CommandName="DeleteStatus" runat="server" />--%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                        <FooterStyle CssClass="FooterStyle" />
                                    </asp:GridView>


                                </div>
                            </asp:Panel>


                                <asp:Button ID="btnShowPopupReqStatus" runat="server" Style="display: none" />

                            <ajaxToolKit:ModalPopupExtender ID="mdlPopupReqStatus" runat="server" TargetControlID="btnShowPopupReqStatus"
                                PopupControlID="pnlEditRequestStatus" BackgroundCssClass="modalBackground" />
                              
                            <asp:Panel ID="pnlEditRequestStatus" runat="server" Visible="true"  Style="display: none;" CssClass="PopupStyle" Width="620">

                                <div class="FormView">
                                    <div> 
                                        <asp:HiddenField ID="hdnRequestID" runat="server" Value="0" /> 
                                    </div>
                                     <h4 class="SubTitle">
                                    <asp:Label ID="lblRequest" runat="server" Text="Edit Status"></asp:Label></h4>
                                    <div class="FieldStyle" style="width: 40%">
                                        Name
                                    </div>
                                     <div class="ValueStyle" style="width: 55%">
                                        <asp:TextBox ID="txtStatusName" runat="server" Width="180px" Enabled="false"></asp:TextBox>
                                    </div>
                                      <div class="FieldStyle" style="width: 40%">
                                       Notification Message to 
                                        <br />
                                        Customer 
                                    </div>
                                   <div class="ValueStyle" style="width: 55%">
                                        <asp:TextBox ID="txtCustomerNotification" runat="server" Width="350px" TextMode="MultiLine" Height="100px"></asp:TextBox>
                                    </div>
                                    <div class="FieldStyle" style="width: 40%">
                                      Notification Message to 
                                        <br />
                                        Administrator 
                                    </div>
                                   <div class="ValueStyle" style="width: 55%">
                                        <asp:TextBox ID="txtAdminNotification" runat="server" Width="350px" TextMode="MultiLine" Height="100px"></asp:TextBox>
                                    </div>
                                    <div class="ClearBoth">
                                    </div>
                                    <div>
                                        <div class="ImageButtons">
                                            <asp:Button CssClass="Size100" ID="btnRequestOK" CausesValidation="False" Text="Ok" Visible="false"
                                                runat="server" OnClick="btnRequestOK_Click"></asp:Button>
                                        </div>
                                        <asp:ImageButton ID="btnCancelStatus" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                                            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                                            runat="server" AlternateText="Cancel" OnClick="btnCancelStatus_Click" />

                                        <asp:ImageButton ID="btnSaveStatus" onmouseover="this.src='../../../../Themes/Images/buttons/button_save_highlight.gif';"
                                            onmouseout="this.src='../../../../Themes/Images/buttons/button_Save.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_save.gif"
                                            runat="server" AlternateText="Save" OnClick="btnSaveStatus_Click" />
                                         

                                    </div>

                                </div>
                            </asp:Panel>
                        </div>

                    </ContentTemplate>
                </ajaxToolKit:TabPanel>

                <ajaxToolKit:TabPanel ID="pnlIssueGC" runat="server">
                    <HeaderTemplate>
                        Issue Gift Card 
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="FormView">
                            <div>
                                <ZNode:Spacer ID="Spacer28" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>

                            <div class="FieldStyle1">
                                Number of days until expiration
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtGCExpiration" MaxLength="5" runat="server" Width="100px"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="ReqGCExpiration" runat="server" ControlToValidate="txtGCExpiration"
                                    ErrorMessage="Enter Number of days" CssClass="Error" Display="Dynamic" ValidationGroup="GC"></asp:RequiredFieldValidator>

                                <asp:RegularExpressionValidator ID="RegGCExpiration" runat="server" ControlToValidate="txtGCExpiration"
                                    ErrorMessage="Enter valid number of days" Display="Dynamic" ValidationExpression="[0-9]+" ValidationGroup="GC"
                                    CssClass="Error"></asp:RegularExpressionValidator>
                            </div>

                            <div class="FieldStyle1">
                                Notification sent to customer
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtGCNotification" runat="server" Width="250px" Rows="5" TextMode="MultiLine"></asp:TextBox>
                            </div>

                            <div class="ClearBoth">
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer210" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>

                            </div>
                            <div>

                                <asp:ImageButton ID="btnGCCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                                    runat="server" AlternateText="Cancel" OnClick="btnGCCancel_Click" />

                                <asp:ImageButton ID="btnGCSubmit" onmouseover="this.src='../../../../Themes/Images/buttons/button_save_highlight.gif';" ValidationGroup="GC"
                                    onmouseout="this.src='../../../../Themes/Images/buttons/button_Save.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_save.gif"
                                    runat="server" AlternateText="Save" OnClick="btnGCSubmit_Click" />

                            </div>

                        </div>
                    </ContentTemplate>

                </ajaxToolKit:TabPanel>
            </ajaxToolKit:TabContainer>
        </div>
    </div>

</asp:Content>

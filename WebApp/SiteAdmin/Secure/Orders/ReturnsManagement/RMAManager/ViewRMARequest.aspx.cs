﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Admin;
using System.Data;

namespace SiteAdmin.Secure.Orders.ReturnsManagement.RMAManager
{
    public partial class ViewRMARequest : System.Web.UI.Page
    {
        private int OrderID = 0;
        private int ItemId = 0;

        /// <summary>
        /// Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["orderid"] != null) && (Request.Params["orderid"].Length != 0))
            {
                this.OrderID = int.Parse(Request.Params["orderid"].ToString());
                lblOrderID.Text = this.OrderID.ToString();
            }

            if ((Request.Params["itemid"] != null) && (Request.Params["itemid"].Length != 0))
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());

            }

          

            if (!IsPostBack)
            {
                BindGrid();
                BindEditData();


                btnPrint.Attributes.Add("onclick", " window.open('" + Page.ResolveUrl("~/SiteAdmin/Secure/Orders/ReturnsManagement/RMAManager/RMARequestReport.aspx?itemid=" + ItemId + "&orderid=" + OrderID + "&Email=false") + "','popup','height=700, width=750,status=no,resizable=no,scrollbars=no,toolbar=no,location=no,menubar=no'); return false;");
            }

        }

        #region Private & Public Methods

        /// <summary>
        /// Bind Grid
        /// </summary>
        private void BindGrid()
        {
            
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            DataSet _OrderLineItems = rmaRequestAdmin.GetRMAOrderLineItemsByOrderID(this.OrderID, ItemId,0,"view");
            uxGrid.DataSource = _OrderLineItems.Tables[0];
            uxGrid.DataBind();

            if (_OrderLineItems.Tables[0].Rows.Count > 0)
            {
                lblTotal.Text = string.Format("{0:c}", _OrderLineItems.Tables[0].Rows[0]["Total"]);
                lblSubTotal.Text = string.Format("{0:c}", _OrderLineItems.Tables[0].Rows[0]["SubTotal"]);
                lblTax.Text = string.Format("{0:c}", _OrderLineItems.Tables[0].Rows[0]["TaxCost"]);    
            }
         
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public string GetPrice(object price, object discount)
        {
            return String.Format("{0:c}", Convert.ToDecimal(price.ToString()) - decimal.Parse(discount.ToString()));

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="price"></param>
        /// <param name="discount"></param>
        /// <param name="qty"></param>
        /// <returns></returns>
        public string GetTotal(object price, object discount, object qty)
        {
            if (int.Parse(qty.ToString()) > 0)
                return String.Format("{0:c}", (Convert.ToDecimal(price.ToString()) - Convert.ToDecimal(discount.ToString())) * int.Parse(qty.ToString()));
            else
                return string.Empty;
        }
        /// <summary>
        /// Bind Data
        /// </summary>
        private void BindEditData()
        {
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            RMARequest rmaRequest = rmaRequestAdmin.GetByRMARequestID(this.ItemId);

            if (rmaRequest != null)
            {
                //lblRMARequestHeader.Text = rmaRequest.RequestNumber;
                //ddlReason.SelectedValue = rmaRequest.ReasonForReturnID.ToString();
                lblRequestDate.Text = rmaRequest.RequestDate.ToString("MM/dd/yyyy");
                txtComments.Text = Server.HtmlDecode(rmaRequest.Comments);
                txtComments.Enabled = false;
              
                lblRMARequestHeader.Text = rmaRequest.RequestNumber;

                if (rmaRequest.RequestStatusID == Convert.ToInt32(ZNodeRequestState.ReturnedOrRefunded))
                {
                    DataSet giftDS = rmaRequestAdmin.GetGiftCardByRMARequest(ItemId);
                    if (giftDS.Tables[0].Rows.Count > 0)
                    {
                        pnlGiftcard.Visible = true;
                        rptGiftCard.DataSource = giftDS.Tables[0];
                        rptGiftCard.DataBind();
                    }
                }

                if( rmaRequest.RequestStatusID == Convert.ToInt32(ZNodeRequestState.ReturnedOrRefunded) ||rmaRequest.RequestStatusID == Convert.ToInt32(ZNodeRequestState.Authorized))
                    btnPrint.Visible=true;
                else
                    btnPrint.Visible=false;
            }
          
        }
        /// <summary>
        /// Format Gift card data
        /// </summary>
        /// <param name="cardnumber"></param>
        /// <param name="amount"></param>
        /// <param name="expirydate"></param>
        /// <returns></returns>
        public string FormatGiftCard(object cardnumber, object amount, object expirydate)
        {
            return "Gift&nbsp;card&nbsp;issued:&nbsp;" + cardnumber.ToString() + "&nbsp;for&nbsp;" + String.Format("{0:c}", amount) + "<br />Expiration&nbsp;Date&nbsp;:&nbsp;" + Convert.ToDateTime(expirydate).ToString("MM/dd/yyyy");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetReasonName(object id)
        {
            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            ReasonForReturn reasonForReturn= rmaConfigAdmin.GetByReasonFoReturnID(int.Parse(id.ToString()));

            if (reasonForReturn != null)
                return reasonForReturn.Name;
            else
                return string.Empty;

        }
       
        #endregion

        /// <summary>
        /// Order Items Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }
        #region Button click Events
        /// <summary>
        ///  Print Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx");
        }

        /// <summary>
        ///  Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["page"] != null && Request.QueryString["page"].ToString() == "order")
                Response.Redirect("~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/View.aspx?itemid=" + this.OrderID);
            Response.Redirect("~/SiteAdmin/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx");
        }


        #endregion


      
    }
}
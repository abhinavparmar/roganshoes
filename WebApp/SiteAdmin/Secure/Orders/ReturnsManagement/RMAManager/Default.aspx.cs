﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;

namespace SiteAdmin.Secure.Orders.ReturnsManagement.RMAManager
{
    public partial class Default : System.Web.UI.Page
    {
        
       
        private DataSet MyDataSet = null;       
        private string PortalIds = string.Empty;
        private string FirstName = null;
        private string LastName = null;
        private DateTime? StartDate = null;
        private DateTime? EndDate = null;
        private int? RequestStatusID = null;
        private int? PortalID = null;
        private int? OrderId = null;
        private int? RMARequestId = null;
        private string ViewLink = "ViewRMARequest.aspx?mode=view&itemid=";
        private string EditLink = "RMARequests.aspx?mode=edit&itemid=";
        private string DeleteLink = "Delete.aspx?itemid=";
        int rid = 0;
        int ORDERID = 0;
        bool email = false;
        

        protected void Page_Load(object sender, EventArgs e)
        {

            lblmsg.Text = string.Empty;

            // Get Portals
            this.PortalIds = UserStoreAccess.GetAvailablePortals;

            if (!Page.IsPostBack)
            {
                this.BindPortal();
                this.BindRequestStatus();
                this.BindSearchData();


                if ((Request.Params["itemid"] != null) && (Request.Params["itemid"].Length != 0))
                {
                    this.rid = int.Parse(Request.Params["itemid"].ToString());
                    if ((Request.Params["orderid"] != null) && (Request.Params["orderid"].Length != 0))
                    {
                        this.ORDERID = int.Parse(Request.Params["orderid"].ToString());
                    }

                    if ((Request.Params["Email"] != null) && (Request.Params["Email"].Length != 0))
                    {
                        email = bool.Parse(Request.Params["Email"].ToString());


                    }
                    string st = "window.open('" + Page.ResolveUrl("~/SiteAdmin/Secure/Orders/ReturnsManagement/RMAManager/RMARequestReport.aspx?itemid=" + rid + "&orderid=" + ORDERID + "&Email=" + email.ToString()) + "','popup','height=700, width=750,status=no,resizable=no,scrollbars=no,toolbar=no,location=no,menubar=no');";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "PopUpwindowOpen", string.Format(st), true);

                }

            }
        }

        #region Public & Private Methods


        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                if (profiles.StoreAccess != string.Empty)
                {
                    portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
                }
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = "ALL STORES";
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
        }


        /// <summary>
        /// Bind Search Data Method
        /// </summary>
        public void BindSearchData()
        {
            if (this.PortalIds.Length > 0)
            {
                RMAHelper _rmaHelper = new RMAHelper();
                this.MyDataSet = _rmaHelper.SearchRMARequest(this.RMARequestId, this.OrderId, this.FirstName, this.LastName,  this.StartDate, this.EndDate, this.RequestStatusID, this.PortalID, this.PortalIds);

                DataView dv = new DataView(this.MyDataSet.Tables[0]);
                dv.Sort = "RMARequestId Desc";
                uxGrid.DataSource = dv;
                uxGrid.DataBind();
            }
            else
            {
                lblmsg.Text = "You do not have permission to view this order. Please contact your administrator for permission";               
            }
        }

        /// <summary>
        /// Bind Data Method
        /// </summary>
        public void BindRequestStatus()
        {
            ZNode.Libraries.Admin.RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();

            // Add New Item
            ListItem Li = new ListItem();
            Li.Text = "All";
            Li.Value = "0";

            // Load Order State Item 
            listRequestStatus.DataSource = rmaConfigAdmin.GetAllRequestStatus();
            listRequestStatus.DataTextField = "Name";
            listRequestStatus.DataValueField = "RequestStatusID";
            listRequestStatus.DataBind();
            listRequestStatus.Items.Insert(0, Li);

           //this.RequestStatusID= Convert.ToInt32(ZNodeRequestState.PendingAuthorization);
           //listRequestStatus.Items[1].Selected = true;
        }

        /// <summary>
        /// Contact first name and last name
        /// </summary>
        /// <param name="firstname">The value of firstname</param>
        /// <param name="lastName">The value of lastname</param>
        /// <returns>Returns the Name</returns>
        public string ReturnName(object firstname, object lastName)
        {
            return firstname.ToString() + " " + lastName.ToString();
        }

     
        /// <summary>
        /// Display the Order State name for a Order state
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>Returns the order status</returns>
        protected string DisplayRequestStatus(object fieldValue)
        {
            ZNode.Libraries.Admin.RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            RequestStatus _RequestStatus = rmaConfigAdmin.GetByRequestStatusID(int.Parse(fieldValue.ToString()));
            return _RequestStatus.Name;
        }

        /// <summary>
        /// Get Store Name by PortalId
        /// </summary>
        /// <param name="portalID">The value of portalID</param>
        /// <returns>Returns the Store Name</returns>
        protected string GetStoreName(string portalID)
        {
            PortalAdmin portalAdmin = new PortalAdmin();

            return portalAdmin.GetStoreNameByPortalID(portalID);
        }

        /// <summary>
        /// Get View URL
        /// </summary>
        /// <param name="RMARequestID">The value of RMARequestID</param>
        /// <param name="OrderID">The value of OrderID</param>
        /// <returns>Returns the View URL</returns>
        protected string GetViewURL(object RMARequestID, object OrderID)
        {
            return "<a href='" + ViewLink + RMARequestID.ToString() + "&orderid=" + OrderID.ToString() + "'>View  &raquo;</a>";

        }

        /// <summary>
        /// Get Delete URL
        /// </summary>
        /// <param name="RMARequestID">The value of RMARequestID</param>
        /// <param name="OrderID">The value of OrderID</param>
        /// <returns>Returns the Delete URL</returns>
        protected string GetDeleteURL(object RMARequestID, object RequestNo)
        {
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            RMARequest rmaRequest = rmaRequestAdmin.GetByRMARequestID(Convert.ToInt32(RMARequestID));

            //if request status is not equal to "Pending Approval" then hide Edit Button
            if (rmaRequest != null && rmaRequest.RequestStatusID != 1)
                return string.Empty;
            else
            return "<a href='" + DeleteLink + RMARequestID.ToString() + "&RequestNo=" + RequestNo.ToString() + "'>Delete  &raquo;</a>";

        }

        /// <summary>
        /// Get Edit URL
        /// </summary>
        /// <param name="RMARequestID">The value of RMARequestID</param>
        /// <param name="OrderID">The value of OrderID</param>
        /// <returns>Returns the Edit URL</returns>
        protected string GetEditURL(object RMARequestID, object OrderID,object Status)
        {          
             RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            RMARequest rmaRequest = rmaRequestAdmin.GetByRMARequestID(Convert.ToInt32(RMARequestID));

            //if request status is not equal to "Pending Approval" then hide Edit Button
            if (rmaRequest != null && (rmaRequest.RequestStatusID == Convert.ToInt32(ZNodeRequestState.Void) || rmaRequest.RequestStatusID == Convert.ToInt32(ZNodeRequestState.ReturnedOrRefunded)))
                return string.Empty;
            else
            return "<a href='" + EditLink + RMARequestID.ToString() + "&orderid=" + OrderID.ToString() + "'>Edit  &raquo;</a>";
           
        }
        /// <summary>
        /// Get Append RMA URL
        /// </summary>
        /// <param name="RMARequestID">The value of RMARequestID</param>
        /// <param name="OrderID">The value of OrderID</param>
        /// <returns>Returns the Edit URL</returns>
        protected string GetAppendURL(object RMARequestID, object OrderID, object Status)
        {
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            bool flag = rmaRequestAdmin.GetAppendRMAFlag(Convert.ToInt32(RMARequestID));

            //if request status is not equal to "Pending Approval" then hide Edit Button
            if (flag && Status.ToString()== "Returned/Refunded")
                return "<a href='" + EditLink + RMARequestID.ToString() + "&flag=append&orderid=" + OrderID.ToString() + "'>Append RMA  &raquo;</a>";
            else
                return string.Empty;

        }

        #endregion

        #region Button click Events
        /// <summary>
        /// Search Button Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            short shortOrderId;

            if (txtRMAID.Text.Length > 0 && Int16.TryParse(txtRMAID.Text.Trim(), out shortOrderId))
            {
                this.RMARequestId = Int16.Parse(txtRMAID.Text.Trim());
            }

            if (txtorderid.Text.Length > 0 && Int16.TryParse(txtorderid.Text.Trim(), out shortOrderId))
            {
                this.OrderId = Int16.Parse(txtorderid.Text.Trim());
            }

            if (txtfirstname.Text.Length > 0)
            {
                this.FirstName = txtfirstname.Text.Trim();
            }

            if (txtlastname.Text.Length > 0)
            {
                this.LastName = txtlastname.Text.Trim();
            }

          
            if (txtStartDate.Text.Length > 0)
            {
                this.StartDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (txtEndDate.Text.Length > 0)
            {
                this.EndDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            if (listRequestStatus.SelectedValue != "0")
            {
                this.RequestStatusID = Convert.ToInt32(listRequestStatus.SelectedValue);
            }

            if (ddlPortal.SelectedValue != "0")
            {
                this.PortalID = Convert.ToInt32(ddlPortal.SelectedValue);
            }

            this.BindSearchData();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
           
            txtStartDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;
            txtorderid.Text = string.Empty;
            txtfirstname.Text = string.Empty;
            txtlastname.Text = string.Empty;
            txtRMAID.Text = string.Empty;
            ddlPortal.SelectedIndex = 0;
            this.BindRequestStatus();
            this.BindSearchData();
        }

        #endregion

        #region Gridview Events
        /// <summary>
        /// Grid Paging Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        /// <summary>
        ///  Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
                
            }
            else
            {
                if (e.CommandName == "View")
                {
                    Response.Redirect(this.ViewLink + e.CommandArgument.ToString());
                }               
                else if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.EditLink + e.CommandArgument.ToString());
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + e.CommandArgument.ToString());
                }
            }
        }
        #endregion

    }
}
﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Orders.ReturnsManagement.RMAManager
{
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables
        private string _RequestNo = string.Empty;
        private int ItemId;
        private string RedirectLink = "~/SiteAdmin/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx";
        #endregion

        /// <summary>
        /// Gets or sets the gift card name.
        /// </summary>
        public string RequestNo
        {
            get { return _RequestNo; }
            set { _RequestNo = value; }
        }

        #region General Events

        /// <summary>
        /// Delete Button click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            bool check = false;
            ZNode.Libraries.Admin.RMARequestAdmin rmaRequestAdmin = new ZNode.Libraries.Admin.RMARequestAdmin();

            check = rmaRequestAdmin.DeleteRequestItemByRMARequestID(this.ItemId);
            check = rmaRequestAdmin.Delete(this.ItemId);

            if (check)
            {
                
                Response.Redirect(this.RedirectLink);
            }
            else
            {
                lblErrorMsg.Text = "Delete action could not be completed. Please try again.";
                lblErrorMsg.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RedirectLink);
        }

        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemID from querystring        
            if (Request.Params["ItemID"] != null)
            {
                this.ItemId = int.Parse(Request.Params["ItemID"]);
            }
            else
            {
                this.ItemId = 0;
            }
            if (Request.Params["RequestNo"] != null)
            {
                this.RequestNo = Request.Params["RequestNo"].ToString();
            }
                    

        }

        #endregion

       
    }
}
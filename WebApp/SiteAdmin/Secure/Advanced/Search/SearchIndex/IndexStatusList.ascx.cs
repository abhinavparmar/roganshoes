﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using System.Web.Services;

namespace SiteAdmin.Secure.Advanced.Search.SearchIndex
{
    public partial class IndexStatusList : System.Web.UI.UserControl
    {

        #region Private variables
        private int currentpage = 0;
        private int totalpages = 1;
        private int totalCount = 0;

        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (hdnpage.Value.Length>0)
                currentpage = Convert.ToInt32(hdnpage.Value.ToString());
            if (!IsPostBack)
            {
                BindData(0);
            }
        }

        #endregion
        
        #region Public Methods

        /// <summary>
        /// Bindgrid data based on the page index
        /// </summary>
        /// <param name="start"></param>
        public void BindData(int start)
        {
             LuceneIndexMonitorService luceneIndexerStatusService = new LuceneIndexMonitorService();        
         
            hdnpage.Value = start.ToString();
            gvIndexStatusList.DataSource = luceneIndexerStatusService.GetPaged("", "LuceneIndexMonitorID DESC", start, 10, out totalCount);
            gvIndexStatusList.DataBind();
            gvIndexStatusList.Visible = true;
            hdnTotal.Value = totalCount.ToString();
            totalpages = totalCount / 10;

            if (totalCount % 10 > 0)
                totalpages++;

            if (start == 0)
            {
                lbfirst.Enabled = false;
                lbprev.Enabled = false;
            }
            else
            {
                lbfirst.Enabled = true;
                lbprev.Enabled = true;
            }

            if (start < totalpages - 1)
            {
                lbnext.Enabled = true;
                lblast.Enabled = true;
            }
            else
            {
                lbnext.Enabled = false;
                lblast.Enabled = false;
            }

            if (totalpages == 1)
            {
                lbfirst.Enabled = false;
                lbprev.Enabled = false;
                lbnext.Enabled = false;
                lblast.Enabled = false;
            }
            pnl.Update();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvIndexStatusList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                long indexMonitorId = ((LuceneIndexMonitor)e.Row.DataItem).LuceneIndexMonitorID;
                GridView gvServer = e.Row.FindControl("gvServerStatus") as GridView;
                LuceneIndexService luceneIndexService = new LuceneIndexService();
                gvServer.DataSource = luceneIndexService.GetIndexServerStatus(Convert.ToInt32(indexMonitorId));
                gvServer.DataBind();
            }

        }
        #endregion

        #region Paging Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbfirst_Click(object sender, EventArgs e)
        {           
            BindData(0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbprev_Click(object sender, EventArgs e)
        {
            if (currentpage > 0)
                currentpage = currentpage - 1;
           
            BindData(currentpage);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbnext_Click(object sender, EventArgs e)
        {
            totalCount = Convert.ToInt32(hdnTotal.Value);

            totalpages = (totalCount / 10) - 1;

            if (totalCount % 10 > 0)
                totalpages++;
           
            if(currentpage<totalpages)
                currentpage = currentpage + 1;

            BindData(currentpage);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lblast_Click(object sender, EventArgs e)
        {        
            totalCount = Convert.ToInt32(hdnTotal.Value);

            totalpages = (totalCount / 10) -1;
            
            if (totalCount % 10 > 0)
                totalpages++;
            BindData(totalpages);
        }

        #endregion



    }
}
<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" Inherits="SiteAdmin.Secure.Advanced.Search.SearchIndex.Default" Title="Manage Index" CodeBehind="Default.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Secure/Advanced/Search/SearchIndex/IndexStatusList.ascx" TagPrefix="znode" TagName="IndexStatusList" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<asp:Content ID="IndexManagement" ContentPlaceHolderID="uxMainContent" runat="Server">
    <script type="text/javascript">
        function activeTabChanged(sender, e) {
            var activeTabIndex = document.getElementById('<%=hdnActiveTabIndex.ClientID%>');
		    activeTabIndex.value = sender.get_activeTabIndex();
		    document.getElementById('<%= statusMessage.ClientID %>').innerHTML = '';
        }
    </script>

    <div id="ManageIndex">
        <div>
        <div class="LeftFloat">
              <h1>Manage Search Index</h1>
        </div>
        <div class="RightFloat">
        <div class="ImageButtons">
             <asp:Button ID="List" CssClass="ImageButtonsText" runat="server" Text="Back" OnClick="List_Click" />
        </div>
        </div>
     </div>
    <div class="ClearBoth" align="left">
        <hr />
    </div>
        <p>Please note these functions are for advanced administrators only.</p>
    
        <div>
            <asp:HiddenField ID="hdnActiveTabIndex" runat="server" Value="0" />
                        <asp:UpdatePanel runat="server" ID="upIndexes">
                            <ContentTemplate>
                                <div class="CustomMessage">
                                    <label runat="server" id="statusMessage" />
                                </div>
                               
                                <div class="ImageAlign">
                                    <h2>Create Search Index</h2>
                                    <p>Delete the current index file and rebuild a new index file.</p>
                                    <div class="ImageButtons">
                                        <asp:Button ID="btnCreate" CssClass="ImageButtonsText" runat="server" Text="Create Index" OnCommand="BtnCreate_Click" />
                                    </div>
                                </div>
                                <div class="ImageAlign">
                                    <h2>Enable/Disable Triggers</h2>
                                    <p>Enable or disable search index triggers which will update the index file via the service. Examples are changes to a category, product or facet field.</p>
                                    <div class="ImageButtons">
                                        <asp:Button ID="btnDisableTriggers" CssClass="ImageButtonsText" runat="server" Text="Disable Triggers" OnCommand="BtnDisableTriggers_Click" />
                                    </div>
                                </div>
                                <div class="ImageAlign">
                                    <h2>Enable/Disable Winservice</h2>
                                    <p>Enable or disable the service that picks up trigger updates and then sends the updates to the index file.</p>
                                    <div class="ImageButtons">
                                        <asp:Button ID="btnDisableServices" CssClass="ImageButtonsText" runat="server" Text="Disable Winservice" OnCommand="BtnDisableWinservice_Click" />
                                    </div>
                                </div>
                             
                         
                               
                                <br />
                                <br />
                                
                                <h4 class="GridTitle">
                                <div style="float:left;width:70%">Service Monitor Table</div>  
                                <div style="float:right">
                                     <div class="ImageButtons">
                                     <asp:Button ID="btnRefresh" CssClass="ImageButtonsText"  runat="server" Text="Refresh" OnCommand="btnRefresh_Click" />  
                                     </div>   
                                </div>
                                </h4>
                                <div class="ClearBoth"></div>
                                <znode:IndexStatusList runat="server" ID="ucIndexStatusList" />
                            </ContentTemplate>
                             <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ucIndexStatusList" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <asp:UpdateProgress ID="uxIndexUpdateProgress" runat="server" AssociatedUpdatePanelID="upIndexes" DisplayAfter="10">
                            <ProgressTemplate>
                                <div id="ajaxProgressBg"></div>
                                <div id="ajaxProgress"></div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
             
        </div>
    </div> 
    <script language="javascript" type="text/javascript">
        var is_ie = (navigator.userAgent.indexOf('MSIE') >= 0) ? 1 : 0;
        var is_ie5 = (navigator.appVersion.indexOf("MSIE 5.5") != -1) ? 1 : 0;
        var xmlHttp;

        /* This function requests the HTTPRequest, will be used to render the Dynamic content html markup 
        * and it will call HandleResponse to handle the response
        */
        function GetChildGrid(Id) {

            // Get the Div
            var childGridDiv = document.getElementById("div" + Id);
            var img = document.getElementById('img' + Id);

            if (childGridDiv) {
                // Is already ChildGrid shown. If not shown
                if (childGridDiv.style.display == "none") {

                    // If the Child Grid is not fetched, then go and fetch it.
                    if (document.getElementById('hid' + Id).value == '0') {
                        // Calling with dummy parameter Time for calling server
                        var startDateTime = new Date();

                        var url = 'StatusGridBuilder.aspx?ID=' + Id + '&Time=' + startDateTime.getTime();
                        xmlHttp = createAjaxObject();
                        if (xmlHttp) {
                            xmlHttp.open('get', url, true);
                            xmlHttp.onreadystatechange = function () {
                                HandleResponse(Id);
                            }
                            xmlHttp.send(null);
                        }
                    }
                    else {
                        childGridDiv.style.display = "block";
                        img.src = "../../../../../SiteAdmin/Themes/Images/minus.png";
                    }
                }
                else { // Already Child Grid Shown
                    childGridDiv.style.display = "none";
                    img.src = "../../../../../SiteAdmin/Themes/Images/plus.png";
                }
            }
        }
        /* This function is used to handler the http response */
        function HandleResponse(Id) {

            var childGridDiv = document.getElementById("div" + Id);
            var img = document.getElementById('img' + Id);

            // If Response completed
            if (xmlHttp.readyState == 4) {

                if (xmlHttp.status == 200) {

                    // Here is the response
                    var str = xmlHttp.responseText;

                    childGridDiv.innerHTML = str;

                    xmlHttp.abort();

                    // Mark the flag, Child Grid is fetched from server
                    document.getElementById('hid' + Id).value = '1';
                }
                else {
                    childRow.style.display = "none";
                    img.src = "../../../../../SiteAdmin/Themes/Images/plus.png";
                }
            }
            else {
                //                alert("sdf");
                //                // Show the Progress status
                //                while (childRow.childNodes.length > 0)
                //                    childRow.removeChild(childRow.childNodes[0]);

                //                var td = document.createElement('td');
                //                td.innerHTML = "<div style='padding:4px;' id='div" + Id + "'><img src='Images/ajax-loader.gif' alt='' /><span style='color:blue;font-size:17px;font-weight:bold;'>Loading... Please wait</span></div>";
                //                //td.appendChild(document.createTextNode("hey"));
                //                td.id = "td" + Id;
                //                td.bgColor = "red";
                //                td.colSpan = "6";
                //                childRow.appendChild(td);

                //                childRow.style.display = "block";
                //                alert("sdf1");
                //document.getElementById("GridViewHierachical").rows[2].cells[0].colSpan = "6";
                //childGridDiv.innerHTML = "<div style='padding:4px;'><img src='Images/ajax-loader.gif' alt='' /><span style='color:blue;font-size:17px;font-weight:bold;'>Loading... Please wait</span></div>";
                childGridDiv.style.display = "block";
                img.src = "../../../../../SiteAdmin/Themes/Images/minus.png";
            }
        }
        /* function to create Ajax object */
        function createAjaxObject() {
            var ro;
            var browser = navigator.appName;
            if (browser == "Microsoft Internet Explorer") {
                if (xmlHttp != null) {
                    xmlHttp.abort();
                }
                ro = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else {
                if (xmlHttp != null) {
                    xmlHttp.abort();
                }
                ro = new XMLHttpRequest();
            }
            return ro;
        }

        /* Get the XML Http Object */
        function GetXmlHttpObject(handler) {
            var objXmlHttp = null;
            if (is_ie) {
                var strObjName = (is_ie5) ? 'Microsoft.XMLHTTP' : 'Msxml2.XMLHTTP';

                try {
                    objXmlHttp = new ActiveXObject(strObjName);
                    objXmlHttp.onreadystatechange = handler;
                }
                catch (e) {
                    alert('Object could not be created');
                    return;
                }
            }
            return objXmlHttp;
        }

        function xmlHttp_Get(xmlhttp, url) {
            xmlhttp.open('GET', url, true);
            xmlhttp.send(null);
        }
    </script>
</asp:Content>

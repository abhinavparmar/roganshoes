using System;
using System.Web.UI;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services;

namespace SiteAdmin.Secure.Advanced.Search.SearchIndex
{
    /// <summary>
    /// Represents the SiteAdmin - SiteAdmin.Secure.Advanced.Search.Default class
    /// </summary>
	public partial class Default : System.Web.UI.Page
	{
		private const string IndexStarted = "Started";
		private const string IndexCompleted = "Completed";
		private const string IndexFailed = "Failed";
        private string ListPage = "~/Siteadmin/Secure/Advanced/Default.aspx";

		protected void Page_Load(object sender, EventArgs e)
		{
			
            LuceneIndexService luceneIndexService = new LuceneIndexService();
            btnCreate.Enabled = true;

			if (!IsPostBack)
			{
				BindButtons();
			}

			ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Reset", "javascript:_isset=0;", true);
		}
        protected void List_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListPage);
        }

       

		public void BtnCreate_Click(object sender, EventArgs e)
		{
			var account = Session["AccountObject"] as Account;
			LuceneIndexService luceneIndexService = new LuceneIndexService();
			luceneIndexService.CreateLuceneIndexButton(0);
			statusMessage.InnerText = "You have set the windows service to create a new index, this will take up to 5 minutes.";
            ucIndexStatusList.BindData(0);
           
		}

		public void BtnDisableTriggers_Click(object sender, EventArgs e)
		{
			int intFlag = 0;
			LuceneIndexService luceneIndexService = new LuceneIndexService();

			if (btnDisableTriggers.Text == "Enable Triggers")
			{
				btnDisableTriggers.Text = "Disable Triggers";
				intFlag = 1;
				statusMessage.InnerText = "Your triggers have been enabled successfully.";
			}
			else
			{
				btnDisableTriggers.Text = "Enable Triggers";
				statusMessage.InnerText = "Your triggers have been disabled successfully.";
			}

			luceneIndexService.SwitchLuceneTrigger(intFlag);
		}

		public void BtnDisableWinservice_Click(object sender, EventArgs e)
		{
			var intFlag = 0;
			var luceneIndexService = new LuceneIndexService();

			intFlag = luceneIndexService.IsLucceneWinserviceDisable();
			luceneIndexService.SwitchWindowsService(intFlag);

			if (intFlag == 1)
			{
				statusMessage.InnerText = "You have run the batch file to enable the windows service";
				btnDisableServices.Text = "Disable Winservice";
			}
			else
			{
				statusMessage.InnerText = "You have run the batch file to diable the windows service";
				btnDisableServices.Text = "Enable Winservice";
			}
		}

		public void BindButtons()
		{
			LuceneIndexService luceneIndexService = new LuceneIndexService();
			int triggerFlag = 1;

			triggerFlag = luceneIndexService.IsLuceneTriggersDisabled();

			if (triggerFlag == 0)
				btnDisableTriggers.Text = "Disable Triggers";
			else
				btnDisableTriggers.Text = "Enable Triggers";

			int serviceFlag = 1;
			serviceFlag = luceneIndexService.IsLucceneWinserviceDisable();

			if (serviceFlag == 1)
				btnDisableServices.Text = "Enable Winservice";
			else if (serviceFlag == 0)
				btnDisableServices.Text = "Disable Winservice";
			else
			{
				btnDisableServices.Text = "Enable Winservice";
				btnDisableServices.Enabled = false;
				statusMessage.InnerText = "You do not have the windows service running.";
			}
		}

		public void ClearStatusMessage(object sender, EventArgs e)
		{
			statusMessage.InnerText = "";
		}
        public void btnRefresh_Click(object sender, EventArgs e)
        {
            ucIndexStatusList.BindData(0);
        }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services;

namespace WebApp.SiteAdmin.Secure.Advanced.Search.SearchIndex
{
    public partial class StatusGridBuilder : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Clear();
            Response.ContentType = "text/xml";
            BindGrid();

            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            gvServerStatus.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString()); 

            Response.End();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
        }
        private void BindGrid()
        {
            long indexMonitorId = Convert.ToInt32(Request.QueryString["Id"].ToString());

            LuceneIndexService luceneIndexService = new LuceneIndexService();
            gvServerStatus.DataSource = luceneIndexService.GetIndexServerStatus(Convert.ToInt32(indexMonitorId));
            gvServerStatus.DataBind();
        }
    }
}
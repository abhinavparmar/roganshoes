﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="AddProfile.aspx.cs" Inherits="SiteAdmin.Secure.Advanced.Accounts.StoreAdministrators.AddProfile" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/Profile.ascx" TagName="Profile" TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
 <ZNode:Profile ID="uxProfile" runat="server" RoleName = "admin" />
</asp:Content>

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Advanced.Accounts.StoreAdministrators
{
    /// <summary>
    /// Represents the Admin_Secure_sales_customers_list class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Add Contact Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddContact_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/Edit.aspx?pagefrom=" + e.CommandArgument.ToString());
        }
        #endregion
    }
}
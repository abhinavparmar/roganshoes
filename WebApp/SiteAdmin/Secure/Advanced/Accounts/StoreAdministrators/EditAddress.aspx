﻿<%@ Page Language="C#" AutoEventWireup="True"  MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" CodeBehind="EditAddress.aspx.cs" Inherits="SiteAdmin.Secure.Advanced.Accounts.StoreAdministrators.EditAddress" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/Address.ascx" TagName="Address" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
   <ZNode:Address ID="uxSiteAddress" runat="server" RoleName="admin" />
</asp:Content>
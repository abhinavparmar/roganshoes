<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Advanced.Accounts.StoreAdministrators.Default" Title="Untitled Page" CodeBehind="Default.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/Accounts/Customer.ascx" TagName="CustomerList" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div align="center">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Store Administrators</h1>
        </div>
        <div class="ClearBoth">
        </div>
        <div class="LeftFloat">
            <p>
                Manage store administrator accounts, roles and permissions.
            </p>
        </div>

        <div style="float: right;">
            <zn:LinkButton ID="AddContact" runat="server" CausesValidation="false" Text="Add Store Admin"
                CommandArgument="ADMIN" OnCommand="AddContact_Command"
                ButtonType="Button" ButtonPriority="Primary" />
        </div>
    </div>
    <ZNode:CustomerList ID="uxCustomer" runat="server" RoleName="admin" />
</asp:Content>


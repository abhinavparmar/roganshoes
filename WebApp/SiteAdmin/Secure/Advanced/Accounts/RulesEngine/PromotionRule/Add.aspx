<%@ Page Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Advanced.Accounts.RulesEngine.PromotionRule.Add"
    MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" ValidateRequest="false" Codebehind="Add.aspx.cs" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/RuleEngine/Rule.ascx" TagName="AddRule" TagPrefix="Znode"%>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <Znode:AddRule ID="AddRule" runat="server"></Znode:AddRule>
</asp:Content>

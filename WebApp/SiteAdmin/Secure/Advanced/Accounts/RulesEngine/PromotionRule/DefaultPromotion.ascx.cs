﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Advanced.Accounts.RuleEngine.PromotionRule
{
    public partial class DefaultPromotion : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private RuleTypeAdmin ruleTypeAdmin = new RuleTypeAdmin();
        private string AddPageLink = "~/SiteAdmin/Secure/Advanced/Accounts/RulesEngine/PromotionRule/Add.aspx";
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindPromotionTypes();
            }
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Edit")
                {
                    this.AddPageLink = this.AddPageLink + "?ItemID=" + Id + "&ruletype=0";
                    Response.Redirect(this.AddPageLink);
                }
                else if (e.CommandName == "Delete")
                {
                    try
                    {
                        bool status = this.ruleTypeAdmin.DeleteDiscountType(int.Parse(Id));

                        string AssociateName = "Delete Promotion Rule - " + selectedRow.Cells[2].Text;
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, selectedRow.Cells[2].Text);

                        if (!status)
                        {
                            lblErrorMsg.Text = "The promotion type '" + selectedRow.Cells[2].Text + "' can not be deleted until all associated items are removed. Please ensure that this promotion type does not contain promotions. If it does, then delete promotions first.";
                        }
                        else
                        {
                            this.BindPromotionTypes();
                        }
                    }
                    catch (Exception ex)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                        lblErrorMsg.Text = "The promotion type '" + selectedRow.Cells[2].Text + "' can not be deleted until all associated items are removed. Please ensure that this promotion type does not contain promotions. If it does, then delete promotions first.";
                    }
                }
            }
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindPromotionTypes();
        }

        /// <summary>
        /// Grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindPromotionTypes();
        }
        #endregion

        #region Events
        /// <summary>
        /// Add Rule Type Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddRuleType_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddPageLink + "?ruletype=0");
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/RulesEngine/Default.aspx?mode=promotion");
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Promotion Types Method
        /// </summary>
        private void BindPromotionTypes()
        {
            uxGrid.DataSource = this.ruleTypeAdmin.GetDiscountTypes();
            uxGrid.DataBind();
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SiteAdmin.Secure.Advanced.Accounts.RulesEngine
{
    public partial class Default : System.Web.UI.Page
    {
        private string Mode = string.Empty;

        #region Page_Load

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get mode value from querystring        
            if (Request.Params["mode"] != null)
            {
                this.Mode = Request.Params["mode"];
            }


            if (!Page.IsPostBack)
            {
                this.ResetTab();
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get Active Tab Name Method
        /// </summary>
        /// <param name="tabIndex">The value of tabIndex</param>
        /// <returns>Returns the Active Tab Name</returns>
        public string GetActiveTabName(int tabIndex)
        {
            string tabName = string.Empty;
            switch (tabIndex)
            {
                case 1:
                    tabName = "tax";
                    break;
                case 2:
                    tabName = "shipping";
                    break;
                case 3:
                    tabName = "promotion";
                    break;
                case 4:
                    tabName = "supplier";
                    break;
            }

            return tabName;
        }
        #endregion

        private void ResetTab()
        {
            if (this.Mode.Equals("tax"))
            {
                // Set Tax Rule as active tab 
                
                tabRuleEngine.ActiveTab = pnlTaxRule;
            }
            else if (this.Mode.Equals("shipping"))
            {
                // Set Shipping Rule as active tab 

                tabRuleEngine.ActiveTab = pnlShippingRule;
            }
            else if (this.Mode.Equals("promotion"))
            {
                // Set Promotion Rule as active tab 

                tabRuleEngine.ActiveTab = pnlPromotionRule;
            }
            else if (this.Mode.Equals("supplier"))
            {
                // Set Supplier Rule as active tab 

                tabRuleEngine.ActiveTab = pnlSupplierRule;
            }
            else
            {
                //  Product Settings 
                tabRuleEngine.ActiveTabIndex = 0;
            }
        }
    }
}
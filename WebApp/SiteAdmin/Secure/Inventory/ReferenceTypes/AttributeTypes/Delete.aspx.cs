using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ReferenceTypes.AttributeTypes
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Setup.ReferenceTypes.AttributeTypes.Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private string RedirectLink = "~/SiteAdmin/Secure/Inventory/ReferenceTypes/AttributeTypes/Default.aspx";
        private string _ProductAttributeTypeName = string.Empty;        
        #endregion

        /// <summary>
        /// Gets or sets the product attribute type name
        /// </summary>
        public string ProductAttributeTypeName
        {
            get { return _ProductAttributeTypeName; }
            set { _ProductAttributeTypeName = value; }
        }

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// Delete Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            AttributeTypeAdmin adminAccess = new AttributeTypeAdmin();

            int ReturnValue = adminAccess.GetCountByAttributeTypeID(this.ItemId);

            AttributeType attributeTypeList = adminAccess.GetByAttributeTypeId(this.ItemId);

            if (ReturnValue == 0)
            {
                bool Check = false;

                if (attributeTypeList != null)
                {
                    this.ProductAttributeTypeName = attributeTypeList.Name;
                    Check = adminAccess.DeleteAttributeType(attributeTypeList);

                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Attribute Types - " + this.ProductAttributeTypeName, this.ProductAttributeTypeName);
                }

                if (Check)
                {
                    Response.Redirect(this.RedirectLink);
                }
                else
                {
                    this.ProductAttributeTypeName = attributeTypeList.Name;
                    lblErrorMsg.Text = "* Delete action could not be completed because the Product Attribute Type is in use.";
                    lblErrorMsg.Visible = true;
                }
            }
            else
            {
                this.ProductAttributeTypeName = attributeTypeList.Name;
                lblErrorMsg.Text = "* Delete action could not be completed because the Product Attribute Type is in use.";
                lblErrorMsg.Visible = true;
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RedirectLink);
        }

        #endregion

        #region Bind Data

        private void BindData()
        {
            AttributeTypeAdmin attributeTypeAccess = new AttributeTypeAdmin();
            AttributeType attributeTypeList = attributeTypeAccess.GetByAttributeTypeId(this.ItemId);

            if (attributeTypeList != null)
            {
                this.ProductAttributeTypeName = attributeTypeList.Name;
            }
        }

        #endregion
    }
}
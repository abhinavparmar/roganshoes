using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ReferenceTypes.ProductTypes
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Setup.ReferenceTypes.ProductTypes.Add  class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (Page.IsPostBack == false)
            {
                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = "Add Product Type";
                }
            }
        }
        #endregion

        #region Bind Edit Data
        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        protected void BindEditData()
        {
            ProductTypeAdmin ProdTypeAdmin = new ProductTypeAdmin();
            ProductType productType = ProdTypeAdmin.GetByProdTypeId(this.ItemId);

            if (productType != null)
            {
                Name.Text = Server.HtmlDecode(productType.Name);
                lblTitle.Text = string.Format("Edit Product Type - {0}", Server.HtmlEncode(Name.Text));
                chkFranchisble.Checked = productType.Franchisable;
                Description.Text = Server.HtmlDecode(productType.Description);
                DisplayOrder.Text = productType.DisplayOrder.ToString();
                hdnProductName.Value = Server.HtmlDecode(productType.Name);

                // Put Default Product Type Franchisble always to True.
                if (productType.Name.Equals("Default"))
                {
                    chkFranchisble.Checked = true;
                    chkFranchisble.Enabled = false;
                }

                // Disable the submit button if gift cart accessed by typing url in address bar.
                if (productType.IsGiftCard != null && productType.IsGiftCard==true)
                {
                    btnSubmitBottom.Enabled = btnSubmitTop.Enabled = false;
                }
            }
            else
            {
                throw new ApplicationException("Product Type Requested could not be found.");
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductTypeAdmin prodTypeAdmin = new ProductTypeAdmin();
            ProductType prodType = new ProductType();
            prodType.ProductTypeId = this.ItemId;
            prodType.Name = Server.HtmlEncode(Name.Text);

            // Gift card type added in standard database. Do not allow to create gift card type.
            prodType.IsGiftCard = null;
            prodType.Franchisable = chkFranchisble.Checked;
            prodType.Description = Server.HtmlEncode(Description.Text);
            prodType.DisplayOrder = Convert.ToInt32(DisplayOrder.Text);

            bool check = false;

            // Checking for duplication
            ProductTypeService productTypeService = new ProductTypeService();
            ProductTypeQuery query = new ProductTypeQuery();
            if ((this.ItemId == 0) || ((this.ItemId > 0) && (hdnProductName.Value != prodType.Name)))
            {
                query.AppendEquals(ProductTypeColumn.Name, prodType.Name);

                TList<ProductType> list = productTypeService.Find(query);
                if (list.Count > 0)
                {
                    // Display error message
                    lblError.Text = "Product type name already exists. Please enter some other name.";
                    Name.Focus();
                    return;
                }
            }

            if (this.ItemId > 0)
            {
                check = prodTypeAdmin.Update(prodType);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Product Types - " + Name.Text, Name.Text);
            }
            else
            {
                check = prodTypeAdmin.Add(prodType);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Product Types - " + Name.Text, Name.Text);
            }

            if (check)
            {
                // Redirect to main page
                Response.Redirect("default.aspx");
            }
            else
            {
                // Display error message
                lblError.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // Redirect to main page
            Response.Redirect("default.aspx");
        }

        #endregion
    }
}
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ReferenceTypes.AddOnTypes
{
    /// <summary>
    /// Represents the Site Admin - Admin_Secure_catalog_product_addons_delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables
        private string _ProductAddOnName = string.Empty;
        private int ItemId;
        private string AddonName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Product Addon Name
        /// </summary>
        public string ProductAddOnName
        {
            get 
            { 
                return this._ProductAddOnName; 
            }

            set 
            { 
                this._ProductAddOnName = value; 
            }
        }
        #endregion

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            this.BindData();
        }

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the screen
        /// </summary>
        protected void BindData()
        {
            ProductAddOnAdmin AddOnAdmin = new ProductAddOnAdmin();
            AddOn Entity = AddOnAdmin.GetByAddOnId(this.ItemId);

            if (Entity != null)
            {
                this.ProductAddOnName = Entity.Name;
            }
            else
            {
                throw new ApplicationException("Product Add-On Requested could not be found.");
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Inventory/ReferenceTypes/AddOnTypes/default.aspx");
        }

        /// <summary>
        /// Delete Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            bool retval = false;

            try
            {
                ProductAddOnAdmin AddOnAdmin = new ProductAddOnAdmin();
                AddOn AddOn = new AddOn();
                AddOn.AddOnID = this.ItemId;
                this.AddonName = AddOn.Name;

                retval = AddOnAdmin.DeleteAddOn(AddOn);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            if (retval)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Addon Values - " + this.AddonName, this.AddonName);
                Response.Redirect("~/SiteAdmin/Secure/Inventory/ReferenceTypes/AddOnTypes/Default.aspx");
            }
            else
            {
                lblMsg.Text = "An error occurred and the product Add-On could not be deleted. Please ensure that this Add-On does not contain Add-On Values or products. If it does, then delete the Add-On values and products first.";
            }
        }
        #endregion
    }
}
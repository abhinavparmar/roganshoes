using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ReferenceTypes.Brands
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Setup.ReferenceTypes.Brands -  Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Edit Datas
        /// </summary>
        public void BindEditDatas()
        {
            ManufacturerAdmin ManuAdmin = new ManufacturerAdmin();
            Manufacturer _manufacturer = ManuAdmin.GetByManufactureId(this.ItemId);
            if (_manufacturer != null)
            {
                Name.Text = Server.HtmlDecode(_manufacturer.Name);
                ctrlHtmlDescText.Html = Server.HtmlDecode(_manufacturer.Description);
                CheckActiveInd.Checked = _manufacturer.ActiveInd;
                if (_manufacturer.IsDropShipper.HasValue)
                {
                    DropShipInd.Checked = _manufacturer.IsDropShipper.Value;
                }

                EmailId.Text = _manufacturer.EmailID;
                NotificationTemplate.Text = _manufacturer.EmailNotificationTemplate;
                Custom1.Text = Server.HtmlDecode(_manufacturer.Custom1);
                Custom2.Text = Server.HtmlDecode(_manufacturer.Custom2);
                Custom3.Text = Server.HtmlDecode(_manufacturer.Custom3);
                //Zeon Custom Code:Starts
                //if (_manufacturer.PortalID != null && _manufacturer.PortalID > 0)
                //{
                //    ddlPortal.SelectedValue = _manufacturer.PortalID.ToString();
                //}
                if (_manufacturer.DisplayOrder > 0)
                {
                    txtDisplayOrder.Text = _manufacturer.DisplayOrder.ToString();
                }

                this.BindManufacturerExtnData(_manufacturer.ManufacturerID);
                //Zeon Custom Code:Ends
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                //Zeon Custom Code:Starts
                //BindPortal();
                //Zeon Custom Code:Ends
                if (this.ItemId > 0)
                {
                    this.BindEditDatas();
                    lblTitle.Text = "Edit Brand - " + Server.HtmlEncode(Name.Text.Trim());
                }
                else
                {
                    lblTitle.Text = "Add New Brand";
                    //Zeon Custom Code:Start
                    tblProductDescription.Visible = true;
                    pnlImage.Visible = false;
                    pnlShowImage.Visible = false;
                    //Zeon Custom Code:Ends
                }
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ManufacturerAdmin ManuAdmin = new ManufacturerAdmin();
            Manufacturer _manufacturer = new Manufacturer();
            if (this.ItemId > 0)
            {
                _manufacturer = ManuAdmin.GetByManufactureId(this.ItemId);
            }

            _manufacturer.Name = Server.HtmlEncode(Name.Text);
            _manufacturer.Description = Server.HtmlEncode(ctrlHtmlDescText.Html);
            _manufacturer.ActiveInd = CheckActiveInd.Checked;
            _manufacturer.IsDropShipper = DropShipInd.Checked;
            _manufacturer.EmailID = EmailId.Text.Trim();
            _manufacturer.EmailNotificationTemplate = NotificationTemplate.Text.Trim();
            _manufacturer.Custom1 = Server.HtmlEncode(Custom1.Text.Trim());
            _manufacturer.Custom2 = Server.HtmlEncode(Custom2.Text.Trim());
            _manufacturer.Custom3 = Server.HtmlEncode(Custom3.Text.Trim());
            //Zeon Custom Code:Starts
            //int portalId = 0;
            //int.TryParse(ddlPortal.SelectedValue, out portalId);
            //_manufacturer.PortalID = portalId;
            if (!string.IsNullOrEmpty(txtDisplayOrder.Text))
            {
                _manufacturer.DisplayOrder = int.Parse(txtDisplayOrder.Text);
            }

            //Zeon Custom Code:Ends
            bool check = false;

            if (this.ItemId > 0)
            {
                check = ManuAdmin.Update(_manufacturer);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Brand - " + Name.Text, Name.Text);
            }
            else
            {
                check = ManuAdmin.Insert(_manufacturer);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Brand - " + Name.Text, Name.Text);
            }

            if (check)
            {
                //Zeon Custom Code:Starts 
                //Save data in ManufacturerExtn
                if (SaveManufacturerExtn(_manufacturer.ManufacturerID)) { Response.Redirect("Default.aspx"); }
                //Zeon Custom Code:Ends
                // Redirect to main page
                //Response.Redirect("Default.aspx");
            }
            else
            {
                // Display error message
                lblError.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
        #endregion

        #region Zeon Custom Code
        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                if (profiles.StoreAccess != string.Empty)
                {
                    portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
                }
            }
            // Portal Drop Down List
            //ddlPortal.DataSource = portals;
            //ddlPortal.DataTextField = "StoreName";
            //ddlPortal.DataValueField = "PortalID";
            //ddlPortal.SelectedIndex = 0;
            //ddlPortal.DataBind();
        }

        /// <summary>
        /// Upload Brands Image
        /// </summary>
        /// <returns></returns>
        private string UploadBrandImage()
        {
            string imgFullName = string.Empty;
            System.IO.FileInfo fileInfo = null;
            string bImagePath = string.Empty;
            bool isSuccess = false;
            if (uxBrandImageUploader.PostedFile != null && !string.IsNullOrEmpty(uxBrandImageUploader.PostedFile.FileName))
            {
                // Validate image
                uxBrandImageUploader.AppendToFileName = "-" + this.ItemId.ToString();
                fileInfo = uxBrandImageUploader.FileInformation;
                isSuccess = uxBrandImageUploader.SaveImage("sadmin", ZNodeConfigManager.SiteConfig.PortalID.ToString(), "0");
                bImagePath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + uxBrandImageUploader.FileInformation.Name;
                if (!isSuccess)
                {
                    if (uxBrandImageUploader.FileInformation.Name != string.Empty)
                    {
                        imgFullName = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + uxBrandImageUploader.FileInformation.Name;
                    }
                }
                else
                {
                    imgFullName = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + uxBrandImageUploader.FileInformation.Name;

                }
            }

            return imgFullName;
        }

        /// <summary>
        /// set value of custom1 field in brands
        /// </summary>
        /// <param name="prevValue"></param>
        /// <returns></returns>
        private string GetImageFile(string prevValue)
        {
            string currentvalue = string.Empty;
            if (((!string.IsNullOrEmpty(prevValue) && RadioProductNewImage.Checked)) || (string.IsNullOrEmpty(prevValue) && uxBrandImageUploader.PostedFile != null))
            {
                currentvalue = UploadBrandImage();
            }
            else
            {
                currentvalue = prevValue;
            }
            return currentvalue;
        }

        /// <summary>
        /// saves manufacturerextn data
        /// </summary>
        private bool SaveManufacturerExtn(int manufacturerID)
        {
            ManufacturerExtn manufacturer = GetManufacturerExtnEntity(manufacturerID);
            ManufacturerExtnService mfrSer = new ManufacturerExtnService();
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            manufacturer.ManufacturerID = manufacturerID;
            string mappedSEOUrl = string.Empty;
            if (manufacturer != null && manufacturer.ManufacturerExtnID > 0 && manufacturer.SEOURL != null)
            {
                mappedSEOUrl = manufacturer.SEOURL;
            }
            if (txtSeoURL.Text.Trim().Length > 0)
            {
                manufacturer.SEOURL = txtSeoURL.Text.Trim().Replace(" ", "-");
            }

            // Set other SEO properties
            manufacturer.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text.Trim());
            manufacturer.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text.Trim());
            manufacturer.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text.Trim());

            if (string.Compare(manufacturer.SEOURL, mappedSEOUrl, true) != 0)
            {
                if (urlRedirectAdmin.SeoUrlExists(manufacturer.SEOURL, manufacturerID))
                {
                    lblError.Text = "The SEO Friendly URL you entered is already in use on another page. Please select another name for your URL";
                    return false;
                }
            }
            if (manufacturer != null && manufacturer.ManufacturerExtnID > 0)
            {
                manufacturer.Image = GetImageFile(manufacturer.Image);
                mfrSer.Update(manufacturer);
            }
            else if (manufacturer.ManufacturerExtnID <= 0)
            {
                manufacturer.Image = GetImageFile(string.Empty);
                mfrSer.Insert(manufacturer);
            }

            return true;
        }

        /// <summary>
        /// get manufacturer Entity by Manufacturer Id
        /// </summary>
        /// <returns></returns>
        private ManufacturerExtn GetManufacturerExtnEntity(int manufacturerID)
        {
            ManufacturerExtn manufacturerExtn = new ManufacturerExtn();
            ManufacturerExtnService mfrSer = new ManufacturerExtnService();
            TList<ManufacturerExtn> manufacturerList = mfrSer.GetByManufacturerID(manufacturerID);
            if (manufacturerList != null && manufacturerList.Count > 0)
            {
                manufacturerExtn = manufacturerList[0];
            }
            return manufacturerExtn;

        }

        /// <summary>
        /// Bind Manufacturer Extn Detail
        /// </summary>
        /// <param name="manufactID"></param>
        private void BindManufacturerExtnData(int manufactID)
        {
            ManufacturerExtn manufacturerExt = GetManufacturerExtnEntity(manufactID);
            bool hasImage = false;
            if (manufacturerExt != null)
            {
                txtSEOTitle.Text = Server.HtmlDecode(manufacturerExt.SEOTitle);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(manufacturerExt.SEOKeywords);
                txtSEOMetaDescription.Text = Server.HtmlDecode(manufacturerExt.SEODescription);
                txtSeoURL.Text = !string.IsNullOrEmpty(manufacturerExt.SEOURL) ? manufacturerExt.SEOURL : string.Empty;
                if (!string.IsNullOrEmpty(manufacturerExt.Image))
                {
                    hasImage = true;
                    ZNodeImage znodeImage = new ZNodeImage();
                    imgBrandImage.ImageUrl = znodeImage.GetImageHttpPathLarge(manufacturerExt.Image);
                }
            }
            //Set Panel Visibility
            pnlShowImage.Visible = hasImage;
            pnlImage.Visible = hasImage;
            tblProductDescription.Visible = !hasImage;

        }

        #region Zeon Custom Events
        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = false;

        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = true;

        }

        #endregion
        #endregion
    }
}
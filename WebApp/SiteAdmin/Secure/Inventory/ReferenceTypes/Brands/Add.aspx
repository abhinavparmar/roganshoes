<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.Brands.Add" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ImageUploader" Src="~/SiteAdmin/Controls/Default/ImageUploader.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <uc2:DemoMode ID="DemoMode1" runat="server"></uc2:DemoMode>
            </h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FieldStyle">
            Brand Name<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID='Name' runat='server' MaxLength="50" Columns="50"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator1" runat="server" ControlToValidate="Name" Display="Dynamic"
                ErrorMessage="* Enter a Brand Name" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Enter a Description
        </div>
        <div class="ValueStyle">
            <ZNode:HtmlTextBox ID="ctrlHtmlDescText" runat="server"></ZNode:HtmlTextBox>
        </div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        <div class="FieldStyle">
            Email Address
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID='EmailId' runat='server' MaxLength="50" Columns="50"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="EmailId"
                CssClass="Error" Display="Dynamic" ErrorMessage="Enter Valid EmailId" SetFocusOnError="True"
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </div>
        <asp:Panel ID="TempPanel" runat="server" Style="display: none">
            <div class="FieldStyle">
                Drop Shipper
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="DropShipInd" runat="server" Text="" />
            </div>
            <uc1:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="10" runat="server"></uc1:Spacer>
            <div class="FieldStyle">
                Email Notification Template
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="NotificationTemplate" runat="server" Rows="15" Columns="50" TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Custom1
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Custom1" runat="server" Rows="10" MaxLength="4000" Columns="50"
                    TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Custom2
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Custom2" runat="server" Rows="10" MaxLength="4000" Columns="50"
                    TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Custom3
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Custom3" runat="server" Rows="10" MaxLength="4000" Columns="50"
                    TextMode="MultiLine"></asp:TextBox>
            </div>
        </asp:Panel>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="10" runat="server"></uc1:Spacer>
            <div class="FieldStyle">
                Is Active?
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="CheckActiveInd" runat="server" Text="" CssClass="FieldStyle" Checked="true" />
            </div>
            
        </div>
        <p>
        </p>
        <%--Zeon Custom Image Implementation For Brands--%>
        <div>
            <uc1:Spacer ID="Spacer4" SpacerHeight="5" SpacerWidth="10" runat="server"></uc1:Spacer>
            <%--<div class="FieldStyle">
                Store Name
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlPortal" runat="server"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvStoreValidator" runat="server" ControlToValidate="ddlPortal" InitialValue="Select" ErrorMessage="Select Store" CssClass="Error" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>--%>
            <div class="FieldStyle">
                Display Order<br />
                <small>(Display brands on the home page)</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="2"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revDisplayOrder" runat="server" ControlToValidate="txtDisplayOrder" ErrorMessage="Only numeric values allowed" SetFocusOnError="true" ValidationExpression="^[0-9]*$" CssClass="Error" Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
             <h4 class="SubTitle">SEO Settings</h4>
            <div class="FormView">
                 <div class="FieldStyle">
                SEO Title
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Keywords
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Description

            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
                <div class="FieldStyle">
                     SEO Friendly Page Name<br />
                     <small>Use only characters a-z and numbers 0-9. Use "-" instead of spaces. Do not use file extensions.</small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSeoURL" runat="server"  MaxLength="500" Columns="50"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="rgvtxtSeoUR" runat="server" ControlToValidate="txtSeoURL"
                        CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid SEO friendly name"
                        SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="ClearBoth"></div>
             <h4 class="SubTitle">Brand Image</h4>
            <br />
            <div id="tblShowImage" style="margin: 10px 0px 10px 0px;">
                <div class="LeftFloat" style="border-right: solid 1px #cccccc; height: 150px;"
                    id="pnlImage" runat="server">
                    <div class="FieldStyle">
                        Brand Image
                    </div>
                    <div class="ValueStyle">
                        <asp:Image ID="imgBrandImage" runat="server" Height="150" Width="150" />
                    </div>
                </div>
                <div class="LeftFloat" style="padding-left: 15px;" runat="server">
                    <asp:Panel runat="server" ID="pnlShowImage" Visible="false">

                        <div class="FieldStyle" style="margin-bottom: 4px;">
                            Select an Option
                        </div>
                        <div class="ClearBoth"></div>
                        <div>
                            <asp:RadioButton ID="RadioProductCurrentImage" Text="Keep Current Image" runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioProductNewImage" Text="Upload New Image" runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductNewImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <br />
            <div class="LeftFloat" id="tblProductDescription" runat="server" visible="true">
                <div class="FieldStyle">
                    Select an Image
                </div>
                <div class="ValueStyle">
                    <uc2:ImageUploader ID="uxBrandImageUploader" runat="server" />
                </div>
            </div>
        </div>
        <%--Zeon Custom Image Implementation For Brands--%>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Inventory.ReferenceTypes.Highlights.Default" Title="Manage Highlights" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Highlight</h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddHighlight" runat="server"
                ButtonType="Button" OnClick="BtnAddHighlight_Click" CausesValidation="False" Text="Add New Highlight"
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
            <p>Create highlights that apply to multiple products. For example: "Certified Organic", etc.</p>
            <br />
        </div>
        <h4 class="SubTitle">Search Product Highlights</h4>
        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtName" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Type</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlHighlightType" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle" runat="server" id="lblLocale">Locale</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlLocales" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Value="">All</asp:ListItem>
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <asp:ImageButton ID="btnClearSearch" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClearSearch_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">Product Highlight List</h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:GridView ID="uxGrid" Width="100%" CssClass="Grid" CellPadding="4" CaptionAlign="Left"
            GridLines="None" runat="server" AutoGenerateColumns="False" AllowPaging="True"
            ForeColor="Black" OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowCommand="UxGrid_RowCommand"
            PageSize="25" EmptyDataText="No highlights found for this Product.">
            <FooterStyle CssClass="FooterStyle" />
            <Columns>
                <asp:BoundField DataField="HighlightID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left"
                    Visible="true" />
                <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='<%# "add.aspx?itemid=" + DataBinder.Eval(Container.DataItem,"Highlightid")%>'
                            id="LinkView">
                            <img id="Img1" alt="" src='<%# GetImagePath(Eval("ImageFile").ToString()) %>' runat="server"
                                style="border: none" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                    <HeaderTemplate>
                        Enable Hyperlink
                    </HeaderTemplate>
                    <ItemTemplate>
                        <img alt="" id="Img2" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark((bool)DataBinder.Eval(Container.DataItem, "DisplayPopup"))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetHighlightTypeName(DataBinder.Eval(Container.DataItem, "HighlightId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Display Order" DataField="DisplayOrder" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img id="Img3" alt='' src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark((bool)DataBinder.Eval(Container.DataItem, "ActiveInd"))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:LinkButton ID="Button1" CommandName="Edit" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"HighlightID")%>'
                            Text="Edit &raquo" runat="server" CssClass="actionlink" />
                        &nbsp;<asp:LinkButton ID="Button5" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"HighlightID")%>'
                            Text="Delete &raquo" runat="server" CssClass="actionlink" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="RowStyle" />
            <EditRowStyle CssClass="EditRowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>

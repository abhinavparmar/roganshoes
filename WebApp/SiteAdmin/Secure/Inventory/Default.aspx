<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Inventory.Default" Title="Inventory - Default" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">


     <div class="LeftMargin">
        <h1>Inventory</h1>

        <hr />

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/NewProducts.png" />
            </div>
            <div class="Shortcut"><a id="A3" href="~/SiteAdmin/Secure/Inventory/Products/Default.aspx" runat="server">Products</a></div>
            <div class="LeftAlign">
                <p>Manage products and inventory in your store.</p>
            </div>
        </div>

      
         <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ImportExport.png" /></div>
            <div class="Shortcut">
                <a id="A1" href="~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx" runat="server">Import / Export Data</a>
            </div>
            <p>
                Download and upload bulk data including inventory, products, tags, and more.
            </p>
        </div>

          <h1>Reference Types</h1>
    <hr />

    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/Brands.png" />
        </div>
        <div class="Shortcut"><a id="A12" href="~/SiteAdmin/Secure/Inventory/ReferenceTypes/Brands/Default.aspx" runat="server">Brands</a></div>
        <div class="LeftAlign">
            <p>Manage a reference list of manufacturers or brands for products in your catalog.</p>
        </div>
    </div>

    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/ProductTypes.png" />
        </div>

        <div class="Shortcut"><a id="A5" href="~/SiteAdmin/Secure/Inventory/ReferenceTypes/ProductTypes/default.aspx" runat="server">Product Types</a></div>
        <div class="LeftAlign">
            <p>Product types are used to group products with similar characteristics and assign special behavior for those products. For example, you could automatically assign "color" and "size" attributes for product type of "apparel".</p>
        </div>
    </div>


    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/attributes.png" />
        </div>
        <div class="Shortcut"><a id="A2" href="~/SiteAdmin/Secure/Inventory/ReferenceTypes/AttributeTypes/Default.aspx" runat="server">Attribute Types</a></div>
        <div class="LeftAlign">
            <p>
                Manage product attribute types such as "size", "color", etc.
            </p>
        </div>
    </div>


    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/add-ons.png" />
        </div>
        <div class="Shortcut"><a id="A4" href="~/SiteAdmin/Secure/Inventory/ReferenceTypes/AddOnTypes/Default.aspx" runat="server">Add-On Types</a></div>
        <div class="LeftAlign">
            <p>Add-Ons are product options that the user can select during checkout. For example, "Gift Wrap", etc.</p>
        </div>
    </div>


    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/highlights.png" />
        </div>
        <div class="Shortcut"><a id="A9" href="~/SiteAdmin/Secure/Inventory/ReferenceTypes/Highlights/Default.aspx" runat="server">Highlights</a></div>
        <div class="LeftAlign">
            <p>Create highlights that apply to multiple products. For Example: "Certified Organic", etc.</p>
        </div>
    </div>

    </div>
</asp:Content>


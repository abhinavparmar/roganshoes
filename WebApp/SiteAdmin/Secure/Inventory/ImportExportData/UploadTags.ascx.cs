using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin - PlugIns_DataManager_UploadTags class
    /// </summary>
    public partial class UploadTags : System.Web.UI.UserControl
    {
        #region Protected Methods and Events
        /// <summary>
        /// Event fired when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Stream fileStream = Request.Files[0].InputStream;
            StreamReader fileReader = new StreamReader(fileStream);

            ZNodeTagsDataSet impTags = new ZNodeTagsDataSet();
            string dataRecord = fileReader.ReadLine();

            if (dataRecord == null)
            {
                ltrlError.Text = "No Records found";
                return;
            }

            dataRecord = fileReader.ReadLine();

            int ErrorCount = 0;
            int SqlErrorCount = 0;
            bool status = false;
            int parsedInt;
            int indexRowCount = 0;
            int MaxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());

            while (dataRecord != null)
            {
                indexRowCount++;

                if (ErrorCount >= MaxErrorCount)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Upload Products: Maximum error count reached. Please see the log details for additional information.");
                    return;
                }

                string[] data = dataRecord.Split('|');

                if (data.Length != 10)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format("Upload Products: There are input fields missing in row:{0}", indexRowCount));
                    ErrorCount++;

                    dataRecord = fileReader.ReadLine();
                    continue;
                }

                try
                {
                    ZNodeTagsDataSet.ZNodeTagRow dataRow = impTags.ZNodeTag.NewZNodeTagRow();

                    dataRow.TagGroupLabel = data[0];
                    dataRow.ControlType = data[1];
                    if (int.TryParse(data[2], out parsedInt)) 
                    { 
                        dataRow.CatalogID = parsedInt; 
                    }

                    if (int.TryParse(data[3], out parsedInt)) 
                    { 
                        dataRow.DisplayOrder = parsedInt; 
                    }

                    dataRow.TagName = data[4];

                    if (int.TryParse(data[5], out parsedInt)) 
                    { 
                        dataRow.TagDisplayOrder = parsedInt; 
                    }

                    dataRow.IconPath = data[6];

                    if (int.TryParse(data[7], out parsedInt)) 
                    { 
                        dataRow.ProductID = parsedInt; 
                    }

                    if (int.TryParse(data[8], out parsedInt)) 
                    { 
                        dataRow.CategoryID = parsedInt; 
                    }

                    if (int.TryParse(data[9], out parsedInt)) 
                    {
                        dataRow.CategoryDisplayOrder = parsedInt;
                    }

                    impTags.ZNodeTag.AddZNodeTagRow(dataRow);
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    ErrorCount++;
                }

                dataRecord = fileReader.ReadLine();
            }

            if (impTags.Tables[0].Rows.Count > 0)
            {
                DataManagerAdmin managerAdmin = new DataManagerAdmin();
                status = managerAdmin.UploadTags(impTags.ZNodeTag, out SqlErrorCount);
            }

            if ((ErrorCount > 0 || SqlErrorCount > 0) && status)
            {
                ltrlError.Text = "Some records could not be updated. Please see the activity log for more information";
                return;
            }

            if (!status)
            {
                ltrlError.Text = "Upload failed, Please see the activity log for more information.";                
            }
            else if (status)
            {
                Response.Redirect("~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx");
            }
        }

        /// <summary>
        /// Event fired when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx");
        }
        #endregion
    }
}
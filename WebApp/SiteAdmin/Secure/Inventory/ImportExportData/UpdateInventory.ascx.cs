using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin -  PlugIns_DataManager_UpdateInventory class
    /// </summary>
    public partial class UpdateInventory : System.Web.UI.UserControl
    {
        #region Events
        /// <summary>
        /// Event fierd when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Stream fileStream = Request.Files[0].InputStream;
            StreamReader fileReader = new StreamReader(fileStream);

            ZNodeSKUInventoryDataset impSKUInventory = new ZNodeSKUInventoryDataset();
            string dataRecord = fileReader.ReadLine();

            if (dataRecord == null)
            {
                ltrlError.Text = "No Records found";
                return;
            }

            dataRecord = fileReader.ReadLine();

            int ErrorCount = 0;
            int SqlErrorCount = 0;
            bool status = false;
            int parsedInt;
            int indexRowCount = 0;
            int MaxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());

            while (dataRecord != null)
            {
                indexRowCount++;

                if (ErrorCount >= MaxErrorCount)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Upload Products: Maximum error count reached. Please see the log details for additional information.");
                    ltrlError.Text = "Upload failed, Please see the activity log for more information.";
                    return;
                }

                try
                {
                    string[] data = dataRecord.Split('|');

                    if (data.Length != 3)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format("Upload Products: There are input fields missing in row:{0}", indexRowCount));
                        ErrorCount++;

                        dataRecord = fileReader.ReadLine();
                        continue;
                    }

                    ZNodeSKUInventoryDataset.ZNodeSKUInventoryRow dataRow = impSKUInventory.ZNodeSKUInventory.NewZNodeSKUInventoryRow();
                    dataRow.SKU = data[0];

                    if (int.TryParse(data[1], out parsedInt))
                    {
                        dataRow.QuantityOnHand = parsedInt;
                    }

                    if (int.TryParse(data[2], out parsedInt))
                    {
                        dataRow.ReOrderLevel = parsedInt;
                    }

                    impSKUInventory.ZNodeSKUInventory.AddZNodeSKUInventoryRow(dataRow);
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    ErrorCount++;
                }

                dataRecord = fileReader.ReadLine();
            }

            if (impSKUInventory.ZNodeSKUInventory.Rows.Count > 0)
            {
                DataManagerAdmin managerAdmin = new DataManagerAdmin();
                status = managerAdmin.UploadInventory(impSKUInventory.ZNodeSKUInventory, out SqlErrorCount);
            }

            if ((ErrorCount > 0 || SqlErrorCount > 0) && status)
            {
                ltrlError.Text = "Some of the records could not be updated. Please see the activity log for more information.";
                return;
            }

            if (!status)
            {
                ltrlError.Text = "Upload failed, Please see the activity log for more information.";
            }
            else if (status)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Exported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Upload Inventory", "Inventory");
                Response.Redirect("~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx");
            }
        }

        /// <summary>
        /// Event fierd when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx");
        }

        #endregion
    }
}
<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="true" %>
<%@ Register Src="~/SiteAdmin/Secure/Inventory/ImportExportData/UpdateInventory.ascx" TagName="UpdateInventory" TagPrefix="ZNode" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">    
    <ZNode:UpdateInventory ID="UpdateInventory" runat="server" />
</asp:Content>


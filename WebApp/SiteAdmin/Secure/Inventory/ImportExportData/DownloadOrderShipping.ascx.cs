using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin - PlugIns_DataManager_DownloadOrderShipping class
    /// </summary>
    public partial class DownloadOrderShipping : System.Web.UI.UserControl
    {
        #region Private member Variables
        private string DefaultPageLink = "~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx";
        private Helper adminHelper = new Helper();
        #endregion

        #region Helper Methods
        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="strFileName">The value of filename</param>
        /// <param name="gridViewControl">Grid View Control Method</param>
        public void ExportDataToExcel(string strFileName, GridView gridViewControl)
        {
            Response.Clear();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);

            // Set as Excel as the primary format
            Response.AddHeader("Content-Type", "application/Excel");
            Response.ContentType = "application/Excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gridViewControl.RenderControl(htw);
            Response.Write(sw.ToString());

            gridViewControl.Dispose();

            Response.End();
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="fileName">The value of File Name</param>
        /// <param name="Data">The value of Data</param>
        public void ExportDataToCSV(string fileName, byte[] Data)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.AddHeader("Content-Type", "application/Excel");

            // Set as text as the primary format
            Response.ContentType = "text/csv";
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Pragma", "public");

            Response.BinaryWrite(Data);
            Response.End();
        }
        #endregion

        #region Page_Load

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(ltrlError.Text.Trim()))
                {
                    lnkLog.Visible = true;
                }
                else
                {
                    lnkLog.Visible = false;
                }
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Event fired when Download Inventory Button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDownloadProdInventory_Click(object sender, EventArgs e)
        {
            DataDownloadAdmin adminAccess = new DataDownloadAdmin();

            try
            {
                DataSet ds = this.BindSearchData();

                if (ds.Tables[0].Rows.Count != 0)
                {
                    DataTable order = ds.Tables[0].DefaultView.ToTable(true, "OrderId", "ShipDate", "TrackingNumber");

                    // Save as Excel Sheet
                    if (ddlFileSaveType.SelectedValue == ".xls")
                    {
                        // Temp Grid control
                        GridView gridView = new GridView();
                        gridView.DataSource = order;
                        gridView.DataBind();
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Shipping Status", "Shipping Status");
                        this.ExportDataToExcel("OrderShipping.xls", gridView);
                    }
                    else
                    {
                        // Save as CSV Format
                        DataSet orderDs = new DataSet();
                        orderDs.Tables.Add(order);

                        // Set Formatted Data from dataset object
                        string strData = adminAccess.Export(orderDs, true);

                        byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);

                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Shipping Status", "Shipping Status");
                        this.ExportDataToCSV("OrderShipping.csv", data);
                    }
                }
                else
                {
                    ltrlError.Text = "No Orders Found";
                    return;
                }
            }
            catch (Exception ex)
            {
                lnkLog.Visible = true;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                ltrlError.Text = "Failed to process your request. Please check the activity log for more information.";
            }
        }

        /// <summary>
        /// Event fired when Cancel Button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.DefaultPageLink);
        }

        /// <summary>
        /// Bind Search Data Method
        /// </summary>
        /// <returns>Returns the DataSet</returns>
        protected DataSet BindSearchData()
        {
            string Attributes = String.Empty;
            string AttributeList = string.Empty;
            DataSet MyDatas = new DataSet();
            SKUAdmin _SkuAdmin = new SKUAdmin();
            ProductAdmin _adminAccess = new ProductAdmin();
            string FirstName = null;
            string LastName = null;
            string CompanyName = null;
            string AccountNumber = null;
            DateTime? StartDate = null;
            DateTime? EndDate = null;
            int? OrderStateID = null;
            int? PortalID = null;
            int? OrderId = null;
            DataSet MyDataSet = null;
            string portalIds = string.Empty;

            // Get Portals
            portalIds = UserStoreAccess.GetAvailablePortals;

            if (txtStartDate.Text.Length > 0)
            {
                StartDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (txtEndDate.Text.Length > 0)
            {
                EndDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            OrderAdmin _OrderAdmin = new OrderAdmin();
            MyDataSet = _OrderAdmin.FindOrders(OrderId, FirstName, LastName, CompanyName, AccountNumber, StartDate, EndDate, OrderStateID, PortalID, portalIds);

            return MyDataSet;
        }
        #endregion
    }
}
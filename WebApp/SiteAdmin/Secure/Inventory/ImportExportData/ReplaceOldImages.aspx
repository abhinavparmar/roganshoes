﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="ReplaceOldImages.aspx.cs" Inherits="WebApp.SiteAdmin.Secure.Inventory.ImportExportData.ReplaceOldImages" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div>
        <div class="Form">
            <h1>Start Replacing Old Conflicting Images</h1>
            <p>
                Click on continue button to start replacing old images
            </p>
            <div>
                <ZNode:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="Error ClearBoth">
                <asp:Label ID="lblStatusMessage" runat="server"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <asp:ImageButton ID="btnCancel" CausesValidation="False" PostBackUrl="~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx" 
                    ImageUrl="~/SiteAdmin/Themes/images/buttons/back.gif"
                    runat="server" AlternateText="Submit" />
                <asp:ImageButton ID="btnReplaceImage" ImageUrl="~/SiteAdmin/Themes/images/buttons/continue.gif"
                    runat="server" AlternateText="Submit" OnClick="btnReplaceImage_Click" />
            </div>
        </div>
    </div>
</asp:Content>

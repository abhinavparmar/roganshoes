<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Inventory.ImportExportData.UploadZipcode" CodeBehind="UploadZipcode.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div class="Form">
    <h1>Upload Zip Code Data</h1>
    <p>
        Use this function to load Zip Code reference data into your store. This data
        is used for calculating county based taxes and the Store Locator.
    </p>
    <p>
        You can learn more about obtaining Zip Code data in our documentation.
    </p>
    <p>
        <strong>WARNING: </strong>This action can not be undone!
    </p>
    <div>
        <ZNode:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <asp:Panel ID="uploadPanel" runat="server" CssClass="SearchForm">
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    Enter the Path of delimited File
                </div>
                <div class="HintStyle">
                    Upload your Zip Code data file to your server using FTP. Enter the full path to
                    your file here.
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtInputFile" runat="server" Width="300px"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="dynamic" ControlToValidate="txtInputFile" CssClass="Error"
                        ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a delimited file to upload."></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtInputFile"
                        CssClass="Error" Display="dynamic" ValidationExpression=".*(\.[cC][sS][vV])"
                        ErrorMessage="Please enter a valid delimited file."></asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
        <div class="Error ClearBoth">
            <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="ClearBoth">
            <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </asp:Panel>
    <div>
        <div class="SuccessMark">
            <asp:Literal ID="ltrlmsg" runat="server" Visible="false"></asp:Literal>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer4" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div>
            <asp:Button class="Button" ID="btnGoback" CausesValidation="True" Text="< Back" runat="server"
                OnClick="BtnCancel_Click" Visible="false"></asp:Button>
        </div>
    </div>
</div>

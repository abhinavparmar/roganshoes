using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin - PlugIns_DataManager_UploadAttributes class
    /// </summary>
    public partial class UploadAttributes : System.Web.UI.UserControl
    {
        #region Events
        /// <summary>
        /// Event fired when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Stream fileStream = Request.Files[0].InputStream;
            StreamReader fileReader = new StreamReader(fileStream);

            int ErrorCount = 0;
            int SqlErrorCount = 0;
            bool status = false;
            int indexRowCount = 0;
            int parsedInt;
            int MaxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());
            ZNodeAttributeDataSet impattribute = new ZNodeAttributeDataSet();
            string dataRecord = fileReader.ReadLine();
            if (dataRecord == null)
            {
                ltrlError.Text = "No Records found";
                return;
            }

            dataRecord = fileReader.ReadLine();

            while (dataRecord != null)
            {
                indexRowCount++;
                if (ErrorCount >= MaxErrorCount)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format("Attribute Upload Failed. The maximum error count of {0} has been reached.", MaxErrorCount.ToString()));
                    ltrlError.Text = "Attribute Upload Failed. Please see the activity log for further information.";
                    return;
                }

                string[] data = dataRecord.Split('|');

                if (data.Length != 7)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format("Attribute Upload exception: There are missing columns in row {0}", indexRowCount));
                    ErrorCount++;

                    dataRecord = fileReader.ReadLine();
                    continue;
                }

                try
                {
                    ZNodeAttributeDataSet.ZNodeProductAttributeRow dr = impattribute.ZNodeProductAttribute.NewZNodeProductAttributeRow();
                    dr.AttributeId = 0;
                    if (int.TryParse(data[0], out parsedInt))
                    {
                        dr.AttributeId = parsedInt;
                    }

                    if (int.TryParse(data[1], out parsedInt))
                    {
                        dr.AttributeTypeId = parsedInt;
                    }

                    dr.Name = data[2].ToString();
                    dr.ExternalId = data[3];
                    if (int.TryParse(data[4], out parsedInt))
                    {
                        dr.DisplayOrder = parsedInt;
                    }

                    dr.IsActive = bool.Parse(data[5].ToString());
                    if (int.TryParse(data[6], out parsedInt))
                    {
                        dr.OldAttributeId = parsedInt;
                    }

                    impattribute.ZNodeProductAttribute.AddZNodeProductAttributeRow(dr);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                    ErrorCount++;
                }

                dataRecord = fileReader.ReadLine();
            }

            if (impattribute.ZNodeProductAttribute != null && impattribute.ZNodeProductAttribute.Rows.Count == 0)
            {
                ltrlError.Text = "No Records found";
                return;
            }

            if (impattribute.Tables[0].Rows.Count > 0)
            {
                DataManagerAdmin managerAdmin = new DataManagerAdmin();
                status = managerAdmin.UploadAttribute(impattribute.ZNodeProductAttribute, out SqlErrorCount);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Exported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Upload Attributes", "Attributes");
            }

            if ((ErrorCount > 0 || SqlErrorCount > 0) && status)
            {
                ltrlError.Text = "Some of the rows are not updated, Please see the log details for the detail information.";
                return;
            }

            if (!status)
            {
                ltrlError.Text = string.Empty;
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx");
            }
        }

        /// <summary>
        /// Event fired when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Inventory/ImportExportData/Default.aspx");
        }
        #endregion
    }
}
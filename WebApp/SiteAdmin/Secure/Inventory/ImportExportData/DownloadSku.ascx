<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Inventory.ImportExportData.DownloadSku" CodeBehind="DownloadSku.ascx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="ProductAutoComplete" Src="~/SiteAdmin/Controls/Default/ProductAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div class="SearchForm">
    <h1>Download SKUs</h1>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    Catalog
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlCatalog" AutoPostBack="true" OnSelectedIndexChanged="DdlCatalog_SelectedIndexChanged"
                        runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer6" SpacerHeight="30" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    Product Name
                </div>
                <div class="ValueStyle">
                    <ZNode:ProductAutoComplete ID="lstProduct" runat="server" />
                </div>
            </div>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <asp:Panel ID="pnlAttribteslist" Visible="false" runat="server">
                    <div class="FieldStyle">
                        Select Product Attributes
                    </div>
                    <div class="ValueStyle">
                        <asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder>
                    </div>
                    <div><small>Leave at the default values to download all SKUs for the product</small></div>
                </asp:Panel>
            </div>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    File Type
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlFileSaveType" runat="server">
                        <asp:ListItem Text="Microsoft Excel (.xls)" Value=".xls"></asp:ListItem>
                        <asp:ListItem Text="Delimited File Format" Value=".csv" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div>
                    <ZNode:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
                </div>
                <div class="ClearBoth">
                    <div class="Error">
                        <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
                    </div>
                    <div>
                        <a href="../../../Data/Default/Config/ActivityLog.txt" id="lnkLog" runat="server"
                            visible="false" target="_blank">Click here to view log >></a>
                    </div>
                    <div>
                        <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                    </div>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="ImageButtons">
                <asp:Button ID="btnDownloadInventory" runat="server" Text="Download SKU Data" OnClick="BtnDownloadProdInventory_Click" />
                <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </div>
</div>

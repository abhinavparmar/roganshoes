<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Inventory.ImportExportData.UpdateInventory" CodeBehind="UpdateInventory.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div class="Form SearchForm">
    <h1>Upload Inventory</h1>
    <div>
        <ZNode:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="RowStyle">
        <div class="ItemStyle">
            <div class="FieldStyle">Select Delimited File</div>
            <div class="ValueStyle">
                <asp:FileUpload ID="UploadFile" runat="server" Width="350px" />
                <asp:RequiredFieldValidator Display="dynamic" ControlToValidate="UploadFile" CssClass="Error" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select a delimited file to upload."></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="UploadFile" CssClass="Error" Display="dynamic" ValidationExpression=".*(\.[cC][sS][vV])" ErrorMessage="Please select a valid CSV."></asp:RegularExpressionValidator>
            </div>
        </div>
    </div>

    <div class="ClearBoth"></div>

    <div class="Error">
        <asp:Literal ID="ltrlError" runat="server"></asp:Literal></div>

    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>

    <div class="ClearBoth">
        <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
            onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
        <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
</div>

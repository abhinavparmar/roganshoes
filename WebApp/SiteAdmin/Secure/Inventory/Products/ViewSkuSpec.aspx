﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="ViewSkuSpec.aspx.cs" Inherits="WebApp.SiteAdmin.Secure.Inventory.Products.ViewSkuSpec" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/spacer.ascx" TagName="Spacer" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1><asp:Label ID="lblProductSKU" runat="server" /></h1>
            <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
            <small>Use this page to view specification(s).</small>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div align="right">
            <asp:LinkButton CssClass="AddButton" ID="AddSpec" Text="Add Specification to this Product"
                runat="server" OnClick="AddSpecItems_Click" />&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Submit" OnClick="btnCancel_Click" />
        </div>
        <asp:GridView ID="uxSpecGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
            runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
            CssClass="Grid" Width="100%" GridLines="None" OnRowCommand="uxSpecGrid_RowCommand"
            AllowPaging="True" OnPageIndexChanging="uxSpecGrid_PageIndexChanging" PageSize="5">
            <Columns>
                <asp:BoundField DataField="ProductSpecId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="SpecificationName" HeaderText="Specification Name" HeaderStyle-HorizontalAlign="Left" HtmlEncode="false" />
                <asp:BoundField DataField="SpecificationValue" HeaderText="Specification Value" HeaderStyle-HorizontalAlign="Left" HtmlEncode="false" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="Delete" Text="Remove &raquo" CommandArgument='<%# Eval("ProductSpecId") %>'
                            CommandName="RemoveItem" CssClass="Button" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No specification(s) found
            </EmptyDataTemplate>
            <RowStyle CssClass="RowStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <FooterStyle CssClass="FooterStyle" />
        </asp:GridView>
    </div>
</asp:Content>

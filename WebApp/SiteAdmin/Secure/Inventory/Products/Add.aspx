<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" EnableEventValidation="false"
    ValidateRequest="false" Title="Manage Products - Add" Inherits="SiteAdmin.Secure.Inventory.Products.Add" CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="uc2" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/ProductTypeAutoComplete.ascx" TagName="ProductTypeAutoComplete"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/SupplierAutoComplete.ascx" TagName="SupplierAutoComplete"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/ManufacturerAutoComplete.ascx" TagName="ManufacturerAutoComplete"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/ImageUploader.ascx" TagName="UploadImage"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <script language="javascript" type="text/javascript">


        function onCheckboxChanged() {

            var chkfree = document.getElementById('ctl00_ctl00_uxMainContent_uxMainContent_chkFreeShippingInd');
            var chkseparate = document.getElementById('ctl00_ctl00_uxMainContent_uxMainContent_chkShipSeparately');
            var separateHide = document.getElementById('ctl00_ctl00_uxMainContent_uxMainContent_dvShipSeparate');

            if (chkfree.checked) { 
                chkseparate.checked = false;
                separateHide.style.display = "none"; 
            }
            else {
                chkseparate.disabled = false;
                if (chkseparate.parentElement.tagName == 'SPAN')
                    chkseparate.parentElement.disabled = false;

                separateHide.style.display = "block";

            }

        }

    </script>
    <asp:HiddenField ID="existSKUvalue" runat="server" />
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>

        <div style="clear: both">
            <asp:Label ID="Label1" CssClass="Error" runat="server"></asp:Label>
        </div>
        <%--<div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                ShowMessageBox="True" ShowSummary="False" />
        </div>--%>
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <uc2:DemoMode ID="DemoMode1" runat="server"></uc2:DemoMode>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FormView">
            <h4 class="SubTitle">General Information</h4>
            <div class="FieldStyle">
                Product Name<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductName" runat="server" Width="152px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProductName"
                    ErrorMessage="Enter Product Name" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regProductName" runat="server" ControlToValidate="txtProductName"
                    ErrorMessage="Enter valid Product Name" Display="Dynamic" ValidationExpression="[0-9A-Za-z()\s\',.:&%#$@_-]+"
                    CssClass="Error"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                SKU or Part#<span class="Asterix">*</span><br />
                <small>The SKU or stock keeping unit is the code assigned to your product inventory.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductSKU" MaxLength="100" runat="server"></asp:TextBox>
                <asp:Label ID="lblProductSKU" runat="server" Visible="false"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtProductSKU"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a Valid SKU or Part#"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                Product Code<span class="Asterix">*</span><br />
                <small>Enter your internal (ERP) product code. If not applicable, then enter the SKU or Part# instead.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductNum" runat="server" Width="152px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ProductName" runat="server" ControlToValidate="txtProductNum"
                    ErrorMessage="Enter Product Number" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="FieldStyle">
                        Product Type<br />
                        <small>Product type will be used to determine if this product has special attributes (example:color, size, etc).</small>
                    </div>
                    <div class="ValueStyle">
                        <ZNode:ProductTypeAutoComplete ID="ProductTypeList" runat="server" Width="152px"
                            IsRequired="true" AutoPostBack="true" />
                    </div>
                    <div runat="server" id="DivAttributes" visible="false">
                        <div class="FieldStyle">
                            Product Attributes<span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>
                    <div id="divExpirationDays" runat="Server" visible="false">
                        <div class="FieldStyle" id="div1" runat="Server">
                            Expiration Period<span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtExporationPeriod" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtExporationPeriod"
                                CssClass="Error" Display="Dynamic" ErrorMessage="Enter the gift card expiration period."></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtExporationPeriod"
                                Display="Dynamic" CssClass="Error" ErrorMessage="Enter a valid expiration period."
                                MinimumValue="1" MaximumValue="9999" Type="Integer"></asp:RangeValidator>
                        </div>
                        <div class="FieldStyle">
                            Expiration Frequency<span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="ddlExpirationFrequency" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ProductTypeList" />
                </Triggers>
            </asp:UpdatePanel>

            <div class="FieldStyle">
                Brand<br />
                <small>This is the product's brand as identified by the manufacturer (example:"Apple").</small>
            </div>
            <div class="ValueStyle">
                <ZNode:ManufacturerAutoComplete ID="ManufacturerList" runat="server" Width="152px" />
            </div>
            <asp:Panel ID="pnlSupplier" runat="server">
                <div class="FieldStyle">
                    Supplier<br />
                    <small>Select the supplier who will fulfill your order.</small>
                </div>
                <div class="ValueStyle">
                    <ZNode:SupplierAutoComplete ID="SupplierList" runat="server" />
                </div>
            </asp:Panel>
            <div class="FieldStyle">
                Size Chart Link<br />
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDownloadLink" Columns="50" runat="server"></asp:TextBox>
                <div class="FieldStyle">
                    <small>Add document name (Ex:Shoes.pdf)<br /><b>File must exist in "Data/Default/ProductPDF" folder.</b>
                     <br />Or<br /> Add document url. (Ex:http://www.mentalhealth.org.nz/file/downloads/pdf/file_293.pdf) </small>
                </div>
            </div>
            <div class="FieldStyle">
                YouTube Video URL<br />
                <small>View product specs on YouTube.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtVideoUrl" Columns="50" runat="server"></asp:TextBox>
                <div class="FieldStyle">
                    <small>Example: http://www.youtube.com/embed/BENolyyDcUU </small>
                </div>
            </div>
            <h4 class="SubTitle">Pricing</h4>
            <div class="FieldStyle">
                Retail Price<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                    ID="txtproductRetailPrice" runat="server" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter Retail Price"
                    ControlToValidate="txtproductRetailPrice" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtproductRetailPrice"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid Retail Price (ex: 123.45)"
                    CssClass="Error" Display="Dynamic" />
                <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtproductRetailPrice"
                    ErrorMessage="You must enter a Retail Price value between $0 and $999,999.99"
                    MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
            </div>
            <div class="FieldStyle">
                Sale Price
            </div>
            <div class="ValueStyle">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix()%>&nbsp;<asp:TextBox
                    ID="txtproductSalePrice" runat="server" MaxLength="10"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtproductSalePrice"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid Sale price(ex: 123.45)"
                    CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                Wholesale Price<br />
                <small>Wholesale price will be applied to profiles that have the wholesale setting enabled.</small>
            </div>
            <div class="ValueStyle">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                    ID="txtProductWholeSalePrice" runat="server" MaxLength="10"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtProductWholeSalePrice"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid Wholesale price(ex: 123.45)"
                    CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                Tax Class<br />
                <small>The tax class determines the sales tax applied to this product during checkout.</small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlTaxClass" runat="server" />
            </div>
            <h4 class="SubTitle">Inventory</h4>
            <asp:Panel ID="pnlquantity" runat="server">
                <div class="FieldStyle">
                    Quantity On Hand<span class="Asterix">*</span><br />
                    <small>Enter the number of items in stock.</small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtProductQuantity" runat="server" Rows="3">999</asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtProductQuantity"
                        CssClass="Error" Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999"
                        MinimumValue="-999999" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtProductQuantity"
                        CssClass="Error" Display="Dynamic" ErrorMessage="You Must Enter a Valid Product Quantity"></asp:RequiredFieldValidator>
                </div>
                <div class="FieldStyle">
                    Re-Order Level<br />
                    <small>Enter the minimum stock level below which you would need to re-order the product.</small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtReOrder" runat="server"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtReOrder"
                        CssClass="Error" Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999"
                        MinimumValue="-999999" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
                </div>
            </asp:Panel>
            <div class="FieldStyle">
                Min Selectable Quantity<br />
                <small>Enter the minimum quantity that can be selected when adding an item to the cart.
                </small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtMinQuantity" runat="server" Rows="3">1</asp:TextBox>
                <asp:RangeValidator ID="rvMinQuantity" runat="server" ControlToValidate="txtMinQuantity"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999"
                    MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
            </div>
            <div class="FieldStyle">
                Max Selectable Quantity<span class="Asterix">*</span><br />
                <small>Enter the maximum quantity that can be selected when adding an item to the cart.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtMaxQuantity" runat="server" Rows="3">10</asp:TextBox>
                <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtMaxQuantity"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999"
                    MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
                <asp:CompareValidator ID="cvQuantityRange" runat="server" ControlToValidate="txtMaxQuantity"
                    ControlToCompare="txtMinQuantity" CssClass="Error" Display="Dynamic" ErrorMessage="Max quantity should be greater than Min quantity."
                    Operator="GreaterThanEqual" SetFocusOnError="True" Type="Integer"></asp:CompareValidator>
            </div>
            <h4 class="SubTitle">Display Settings</h4>
            <div class="FieldStyle">
                Display Order<span class="Asterix">*</span><br />
                <small>Enter the display order for the product in search results. A product with lower display order will be displayed first.</small>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDisplayOrder"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a Display Order"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtDisplayOrder"
                    Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999999"
                    MinimumValue="1" Type="Integer"></asp:RangeValidator>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5">500</asp:TextBox>
            </div>
            <h4 class="SubTitle">Shipping Settings</h4>
            <div class="FieldStyle">
                Free Shipping
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkFreeShippingInd" onclick="onCheckboxChanged();" Text="Enable free shipping for this product. All other shipping rules will be ignored."
                    runat="server" />
            </div>

            <div id="dvShipSeparate" runat="server">

                <div class="FieldStyle">
                    Ship Separately
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkShipSeparately" Text="Calculate shipping costs on this item separately from other items in the cart."
                        runat="server" />
                </div>
                <asp:UpdatePanel ID="upnlShippingType" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="FieldStyle">
                            Shipping Cost<span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList AutoPostBack="true" ID="ShippingTypeList" runat="server" OnSelectedIndexChanged="ShippingTypeList_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <asp:Panel ID="pnlShippingRate" runat="server" Visible="false">
                            <div class="FieldStyle">
                                Shipping Rate<br />
                                <small>This applies only for "Fixed Rate Per Item" custom shipping option.</small>
                            </div>
                            <div class="ValueStyle">
                                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;
                            <asp:TextBox ID="txtShippingRate" runat="server" Width="46px" MaxLength="7" Text="0"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter a shipping rate for this Product"
                                    ControlToValidate="txtShippingRate" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CompareValidator runat="server" ControlToValidate="txtShippingRate" Type="Currency"
                                    Operator="DataTypeCheck" ErrorMessage="You must enter a valid shipping rate (ex: 123.45)"
                                    CssClass="Error" Display="Dynamic" />
                                <asp:RangeValidator runat="server" ControlToValidate="txtShippingRate" ErrorMessage="You must enter a shipping rate between $0 and $999,999.99"
                                    MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>

            <h4 class="SubTitle">Product Attributes</h4>
            <div class="FieldStyle">
                Weight
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductWeight" runat="server" Width="46px" MaxLength="7"> </asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.WeightUnit %>
                <asp:RangeValidator Enabled="false" ID="weightBasedRangeValidator" runat="server"
                    ControlToValidate="txtProductWeight" CssClass="Error" Display="Dynamic" ErrorMessage="This Shipping Type requires that Weight be greater than 0."
                    MaximumValue="999999" MinimumValue="0.1" CultureInvariantValues="true" Type="Double"></asp:RangeValidator>
                <asp:RequiredFieldValidator Enabled="false" ID="RequiredForWeightBasedoption" runat="server"
                    ErrorMessage="You must enter weight for this Shipping Type." ControlToValidate="txtProductWeight"
                    CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                <div>
                    <asp:CompareValidator ID="WeightFieldCompareValidator" runat="server" ControlToValidate="txtProductWeight"
                        Type="Currency" Operator="DataTypeCheck" ErrorMessage='' CssClass="Error" Display="Dynamic" />
                </div>
            </div>
            <div class="FieldStyle">
                Height
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductHeight" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="txtProductHeight"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid product Weight(ex: 2.5)"
                    CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                Width
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductWidth" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtProductWidth"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid product Weight(ex: 2.5)"
                    CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                Length
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductLength" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtProductLength"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid product Weight(ex: 2.5)"
                    CssClass="Error" Display="Dynamic" />
            </div>
            <h4 class="SubTitle">Product Image</h4>
            <small>Upload a suitable image for your product. Only JPG, GIF and PNG images are supported.
                The file size should be less than 1.5 Meg. Your image will automatically be scaled
                so it displays correctly in the catalog.</small>
            <div id="tblShowImage" runat="server" visible="false" style="margin: 10px 0px 10px 0px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;" runat="server"
                    id="pnlImage">
                    <asp:Image ID="Image1" runat="server" />
                </div>
                <div class="LeftFloat" style="margin-left: -1px;" runat="server" id="pnlUploadSection">
                    <asp:Panel runat="server" ID="pnlShowImage">
                        <div class="FieldStyle" style="margin-bottom:4px;">
                            Select an Option
                        </div>
                        <div class="ClearBoth"></div>
                        <div>
                            <asp:RadioButton ID="RadioProductCurrentImage" Text="Keep Current Image" runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioProductNewImage" Text="Upload New Image" runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductNewImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblProductDescription" runat="server" visible="false">
                        <div class="FieldStyle">
                            Select an Image
                        </div>
                        <div class="ValueStyle">
                            <ZNode:UploadImage ID="uxProductImage" runat="server"></ZNode:UploadImage>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Product Image ALT Text<br />
                        <small>The image ALT Text is used for SEO and Accessibility.</small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="lblProductImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                    <asp:Panel ID="pnlImageName" runat="server" Visible="false">
                        <div class="FieldStyle">
                            Image File Name
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtimagename" runat="server"></asp:TextBox>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <h4 class="SubTitle">Description</h4>
            <div class="FieldStyle">
                Short Description<br />
                <small>The short description is displayed in product search results. Enter 100 characters or less.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtshortdescription" runat="server" Width="300px" TextMode="MultiLine"
                    Height="75px" MaxLength="100"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Long Description
            </div>
            <div class="HtmlValueStyle">
                <ZNode:HtmlTextBox ID="ctrlHtmlText" runat="server"></ZNode:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                Product Features<br />
                <small>Enter a list of product features to display. Leave blank to disable this tab.</small>
            </div>
            <div class="HtmlValueStyle">
                <ZNode:HtmlTextBox ID="ctrlHtmlPrdFeatures" runat="server"></ZNode:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                Product Specifications<br />
                <small>Enter additional product specifications and characteristics. Leave blank to disable this tab.</small>
            </div>
            <div class="HtmlValueStyle">
                <ZNode:HtmlTextBox ID="ctrlHtmlPrdSpec" runat="server"></ZNode:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                Shipping Information<br />
                <small>Enter information about shipping methods and shipping time for this product. Leave blank to disable this tab.</small>
            </div>
            <div class="HtmlValueStyle">
                <ZNode:HtmlTextBox ID="ctrlHtmlProdInfo" runat="server"></ZNode:HtmlTextBox>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>

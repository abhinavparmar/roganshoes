using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin  Admin_Secure_catalog_product_addrelateditems class
    /// </summary>
    public partial class AddRelatedItems : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int _ItemId = 0;
        TList<ZeonProductCrossSellType> _ZeonCrossSellTypes = null;
        private string ViewPage = "view.aspx?itemid=";
        private string selectedItemsKey = "SelectedItemsKey";
        private ProductAdmin productAdmin = new ProductAdmin();
        private Product product = new Product();
        #endregion

        /// <summary>
        /// Gets or sets the item Id
        /// </summary>
        public int ItemId
        {
            get { return this._ItemId; }
            set { this._ItemId = value; }
        }

        #region Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
                product = productAdmin.GetByProductId(this.ItemId);
                lblHeading.Text = "Add Related Items to - " + product.Name;
            }
            if (!Page.IsPostBack)
            {
                Session[this.selectedItemsKey] = null;
                this.BindData();
            }
            //Zeon Custom Code: Start
             _ZeonCrossSellTypes = GetZeonCrossSellTypes();
            //Zeon Custom Code: End
        }

        /// <summary>
        /// Update Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Update_Click(object sender, EventArgs e)
        {
            this.RememberOldValues();
            ProductCrossSellAdmin _ProdCrossAdmin = new ProductCrossSellAdmin();
            //Dictionary<int, string> productIdList = (Dictionary<int, string>)Session[this.selectedItemsKey];
            Dictionary<int, ProductCrossSellMapping> productIdList = (Dictionary<int, ProductCrossSellMapping>)Session["CHECKEDITEMS"];
            bool status = true;

            if (productIdList != null)
            {
                StringBuilder sb = new StringBuilder();
                StringBuilder productName = new StringBuilder();
                foreach (KeyValuePair<int, ProductCrossSellMapping> pair in productIdList)
                {
                    // Get ProductId
                    int productId = pair.Key;
                    ProductCrossSellMapping prodCrossSellMapping = (ProductCrossSellMapping)(pair.Value);
                    status = _ProdCrossAdmin.Insert(this.ItemId, productId,prodCrossSellMapping.CrossSellTypeId);
                    productName.Append(pair.Value + ",");
                    if (!status)
                    {
                        sb.Append(prodCrossSellMapping.ProductName + ",");
                        lblError.Visible = true;
                    }
                }

                //Session.Remove(this.selectedItemsKey);
                Session.Remove("CHECKEDITEMS");
                if (sb.ToString().Length > 0)
                {
                    sb.Remove(sb.ToString().Length - 1, 1);

                    // Display Error message
                    lblError.Text = "You are trying to add same product(s) as Related Item.<br/>" + sb.ToString();

                    foreach (GridViewRow row in uxGrid.Rows)
                    {
                        CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

                        if (check != null)
                        {
                            check.Checked = false;
                        }
                    }
                }
                else
                {
                    ProductAdmin prodAdmin = new ProductAdmin();
                    Product entity = prodAdmin.GetByProductId(this.ItemId);
                    productName.Remove(productName.Length - 1, 1);

                    string AssociationName = "Associated  Related Items " + productName + " to Product " + entity.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociationName, entity.Name);
                    Response.Redirect(this.ViewPage + this.ItemId + "&mode=crosssell");
                }
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ViewPage + this.ItemId + "&mode=crosssell");
        }

        /// <summary>
        /// Search Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchProduct();
        }

        /// <summary>
        /// Clear Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("addrelateditems.aspx?itemid=" + this.ItemId);
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.RememberOldValues();
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchProduct();
        }

        /// <summary>
        /// Grid Row Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Dictionary<int, string> productIdList = (Dictionary<int, string>)Session[this.selectedItemsKey];

                if (productIdList != null)
                {
                    CheckBox check = (CheckBox)e.Row.Cells[0].FindControl("chkProduct") as CheckBox;
                    int id = int.Parse(e.Row.Cells[1].Text);

                    if (productIdList.ContainsKey(id) && check != null)
                    {
                        check.Checked = true;
                    }
                }
                DropDownList ddlZeonCrossSellProduct = (DropDownList)e.Row.FindControl("drpZeonCrossSellType");
                if (ddlZeonCrossSellProduct != null)
                {
                    BindZeonCrossSellTypeCombo(ddlZeonCrossSellProduct);
                }
            }
        }

        
        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ViewPage + this.ItemId + "&mode=crosssell");
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Get ImagePath
        /// </summary>
        /// <param name="Imagefile">The value of Image File</param>
        /// <returns>Returns the Image Path</returns>
        protected string GetImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(Imagefile);
        }

        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            TList<Catalog> catalogList = catalogAdmin.GetAllCatalogs();
            DataSet ds = catalogList.ToDataSet(false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            ddlCatalog.DataSource = ds;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();
            ListItem item3 = new ListItem("ALL", "0");
            ddlCatalog.Items.Insert(0, item3);
            ddlCatalog.SelectedIndex = 0;
        }

        /// <summary>
        /// Search a Product by name,productnumber,sku,manufacturer,category,producttype
        /// </summary>
        private void BindSearchProduct()
        {
            ProductAdmin prodadmin = new ProductAdmin();
            DataSet ds = prodadmin.SearchProduct(Server.HtmlEncode(txtproductname.Text.Trim()), txtproductnumber.Text.Trim(), txtsku.Text.Trim(), dmanufacturer.Value, dproducttype.Value, dproductcategory.Value, ddlCatalog.SelectedValue);
             pnlProductList.Visible = true;
                uxGrid.DataSource = ds;
                uxGrid.DataBind();
           
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Remember Old Values Method
        /// </summary>
        //private void RememberOldValues()
        //{
        //    Dictionary<int, string> productIdList = new Dictionary<int, string>();

        //    // Loop through the grid values
        //    foreach (GridViewRow row in uxGrid.Rows)
        //    {
        //        CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

        //        // Check in the Session
        //        if (Session[this.selectedItemsKey] != null)
        //        {
        //            productIdList = (Dictionary<int, string>)Session[this.selectedItemsKey];
        //        }

        //        int id = int.Parse(row.Cells[1].Text);

        //        if (check.Checked)
        //        {
        //            if (!productIdList.ContainsKey(id))
        //            {
        //                productIdList.Add(id, row.Cells[3].Text);
        //            }
        //        }
        //        else
        //        {
        //            productIdList.Remove(id);
        //        }
        //    }

        //    if (productIdList.Count > 0)
        //    {
        //        Session[this.selectedItemsKey] = productIdList;
        //    }
        //    if (productIdList.Count == 0 && uxGrid.Rows.Count >0)
        //    {
        //        lblError.Text = "At least one Product should be selected.";
        //        lblError.Visible = true;
        //    }
        //}

        #endregion

        #region Zeon Custom Code
        /// <summary>
        /// Remember Old Values Method
        /// </summary>
        private void RememberOldValues()
        {
            Dictionary<int, ProductCrossSellMapping> productIdList = new Dictionary<int, ProductCrossSellMapping>();

            // Loop through the grid values
            foreach (GridViewRow row in uxGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;
                DropDownList ddlCrossSellTypes = (DropDownList)row.Cells[0].FindControl("drpZeonCrossSellType") as DropDownList;

                // Check in the Session
                if (Session["CHECKEDITEMS"] != null)
                    productIdList = (Dictionary<int, ProductCrossSellMapping>)Session["CHECKEDITEMS"];

                int id = int.Parse(row.Cells[1].Text);
                string productName = row.Cells[3].Text;
                int crossSellTypeId = int.Parse(ddlCrossSellTypes.SelectedItem.Value);

                if (check.Checked)
                {
                    if (!productIdList.ContainsKey(id))
                    {
                        ProductCrossSellMapping prodCrossSellMapping = new ProductCrossSellMapping(id, productName, crossSellTypeId);
                        productIdList.Add(id, prodCrossSellMapping);
                    }
                }
                else
                    productIdList.Remove(id);
            }

            if (productIdList.Count > 0)
                Session["CHECKEDITEMS"] = productIdList;
        }

        /// <summary>
        /// Get the Cross Sell Product
        /// </summary>
        /// <returns></returns>
        private TList<ZeonProductCrossSellType> GetZeonCrossSellTypes()
        {
            ZNode.Libraries.DataAccess.Service.ZeonProductCrossSellTypeService zeonProdCrossSellTypeService = new ZNode.Libraries.DataAccess.Service.ZeonProductCrossSellTypeService();
            return zeonProdCrossSellTypeService.GetAll();
        }

        /// <summary>
        /// Bind the Cross Sell product to drop down list
        /// </summary>
        /// <param name="ddlZeonCrossSellProduct"></param>
        private void BindZeonCrossSellTypeCombo(DropDownList ddlZeonCrossSellProduct)
        {
            ddlZeonCrossSellProduct.DataSource = _ZeonCrossSellTypes;
            ddlZeonCrossSellProduct.DataBind();
        }

        public class ProductCrossSellMapping
        {
            public ProductCrossSellMapping()
            {
            }

            public ProductCrossSellMapping(int productId, string productName, int crossSellTypeId)
            {
                ProductId = productId;
                ProductName = productName;
                CrossSellTypeId = crossSellTypeId;
            }

            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public int CrossSellTypeId { get; set; }
        }
        #endregion
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="AddSpecItems.aspx.cs" Inherits="WebApp.SiteAdmin.Secure.Inventory.Products.AddSpecItems" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/spacer.ascx" TagName="Spacer" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1><asp:Label ID="lblHeading" runat="server" /></h1>
            <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
            <small>Use this page to add specification.</small>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="btnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Submit" OnClick="btnCancel_Click" />
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
        <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        <div class="FormView">
            <div class="FieldStyle">
                Specification Name <span class="Asterix">*</span></div>
            <div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSpecKey"
                    ErrorMessage="* Enter Specification Name" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSpecKey"
                    ErrorMessage="* Enter valid Specification Name" ValidationExpression="^(?:[A-Za-z0-9-'`~%\^\*_=\\{\}\[\];\<\>\|+,@:?!()$#/\\]+|&(?!#))+$"
                    Display="dynamic" CssClass="Error"></asp:RegularExpressionValidator>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSpecKey" runat="server" Width="150px"></asp:TextBox></div>
            <div class="FieldStyle">
                Specification Value <span class="Asterix">*</span></div>
            <div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSpecValue"
                    ErrorMessage="* Enter Specification Value" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSpecValue" runat="server" Width="150px"></asp:TextBox></div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="btnSubmit_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Submit" OnClick="btnCancel_Click" />
            </div>
        </div>
    </div>
</asp:Content>

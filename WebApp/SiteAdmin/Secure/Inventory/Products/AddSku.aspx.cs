using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_catalog_product_add_sku class
    /// </summary>
    public partial class AddSku : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private int SkuId = 0;
        private int ProductTypeId = 0;
        private string ViewLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?itemid=";
        //Zeon Custom Members
        private string AddViewlink = "AddView.aspx?itemid=";
        private string AssociateName = string.Empty;


        public bool ShowSubmitButton
        {
            set
            {
                skuTopButtons.Visible = value;
                skuBottomButtons.Visible = value;
                //upSku.Update();
            }
        }

        #endregion

        #region Private Property
        private string ProductName
        {
            get
            {
                ProductAdmin AdminAccess = new ProductAdmin();
                Product entity = AdminAccess.GetByProductId(this.ItemId);
                UserStoreAccess.CheckStoreAccess(entity.PortalID.GetValueOrDefault(0), true);
                if (entity != null)
                {
                    return entity.Name;
                }

                return string.Empty;
            }
        }

        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (Request.Params["skuid"] != null)
            {
                this.SkuId = int.Parse(Request.Params["skuid"].ToString());
            }

            if (Request.Params["typeid"] != null)
            {
                this.ProductTypeId = int.Parse(Request.Params["typeid"].ToString());
            }

            if (this.SkuId <= 0)
            {
                ProductTags1.Visible = false;
            }

            Page.Form.Attributes.Add("enctype", "multipart/form-data");

            this.Bind();

            if (!Page.IsPostBack)
            {
                this.BindBillingFrequency();

                // Bind Supplier 
                this.BindSuppliersTypes();

                this.ShowSubscriptionPanel();

                if ((this.ItemId > 0) && (this.SkuId > 0))
                {
                    lblHeading.Text = "Edit SKU for Product : " + this.ProductName;

                    // Bind Sku Details
                    this.BindData();

                    // Bind Attributes
                    this.BindAttributes();
                    tblShowImage.Visible = true;
                }
                else
                {
                    tblSKUDescription.Visible = true;
                    lblHeading.Text = "Add SKU for Product : " + this.ProductName;
                }

                // Bind the Locale Dropdown based on skuid.
                this.BindLocaleDropdown();
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            bool Check = this.UpdateSKU();

            if (Check)
            {
                ProductAdmin prodAdmin = new ProductAdmin();
                Product entity = prodAdmin.GetByProductId(this.ItemId);

                this.AssociateName = "Edit Product SKU - " + entity.Name;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, entity.Name);

                // Redirect to View Page
                Response.Redirect(this.ViewLink + this.ItemId + "&mode=inventory");
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // Redirect to View Page
            Response.Redirect(this.ViewLink + this.ItemId + "&mode=inventory");
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioSKUCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblSKUDescription.Visible = false;
            SKUImage.Visible = true;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioSKUNoImage_CheckedChanged(object sender, EventArgs e)
        {
            tblSKUDescription.Visible = false;
            SKUImage.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioSKUNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblSKUDescription.Visible = true;
            SKUImage.Visible = true;
        }

        /// <summary>
        /// Billing Periods Dropdown list Selected Index Changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBillingPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindBillingFrequency();
        }

        /// <summary>
        /// Redirect to Locale based AddOnValue edit page.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlLocales_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool status = this.UpdateSKU();

            if (status)
            {
                int id = 0;
                int.TryParse(ddlLocales.SelectedValue, out id);

                // Get the AddOnId for the selected AddOnValueId.
                ZNode.Libraries.DataAccess.Service.SKUService skuService = new ZNode.Libraries.DataAccess.Service.SKUService();
                SKU sku = skuService.GetBySKUID(id);

                skuService.DeepLoad(sku);

                if (sku != null)
                {
                    // Redirect, if user select other language of same add on.
                    Response.Redirect("AddSku.aspx?itemid=" + sku.ProductID.ToString() + "&skuid=" + id.ToString() + "&typeid=" + sku.ProductIDSource.ProductTypeID.ToString());
                }
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Update SKU Method
        /// </summary>
        /// <returns>Returns a bool value true or false</returns>
        private bool UpdateSKU()
        {
            System.IO.FileInfo fileInfo = null;
            SKUAdmin adminAccess = new SKUAdmin();
            SKUQuery skufilters = new SKUQuery();
            SKU skuEntity = new SKU();
            SKUAttribute skuAttribute = new SKUAttribute();
            ProductAdmin prodAdminAccess = new ProductAdmin();

            if (this.SkuId > 0)
            {
                skuEntity = adminAccess.GetBySKUID(this.SkuId);
            }

            // Check SKU exists, If already exists return false
            bool isSkuExist = adminAccess.IsSkuExist(Server.HtmlEncode(SKU.Text.Trim()), this.ItemId);

            if (isSkuExist)
            {
                lblError.Text = "* SKU or Part# already exists. Please try with different SKU or Part#";
                return false;
            }

            // Set Values to SKU
            skuEntity.SKU = Server.HtmlEncode(SKU.Text.Trim());

            // Update inventory record.
            int reorder = 0;
            int.TryParse(ReOrder.Text.Trim(), out reorder);
            SKUAdmin.UpdateQuantity(skuEntity, int.Parse(Quantity.Text.Trim()), reorder);

            if (WeightAdditional.Text.Trim().Length > 0)
            {
                skuEntity.WeightAdditional = Convert.ToDecimal(WeightAdditional.Text);
            }

            if (RetailPrice.Text.Trim().Length > 0)
            {
                skuEntity.RetailPriceOverride = Convert.ToDecimal(RetailPrice.Text.Trim());
            }
            else
            {
                skuEntity.RetailPriceOverride = null;
            }

            // Set Sale price override property
            if (SalePrice.Text.Trim().Length > 0)
            {
                skuEntity.SalePriceOverride = Convert.ToDecimal(SalePrice.Text.Trim());
            }
            else
            {
                skuEntity.SalePriceOverride = null;
            }

            // Set wholesale price override property
            if (WholeSalePrice.Text.Trim().Length > 0)
            {
                skuEntity.WholesalePriceOverride = decimal.Parse(WholeSalePrice.Text.Trim());
            }
            else
            {
                skuEntity.WholesalePriceOverride = null;
            }

            skuEntity.ProductID = this.ItemId;
            skuEntity.ActiveInd = VisibleInd.Checked;
            skuEntity.ImageAltTag = txtImageAltTag.Text.Trim();

            int twoCOID = 0;
            skuEntity.ExternalProductID = null;
            if (int.TryParse(TwoCOProductID.Text, out twoCOID))
            {
                skuEntity.ExternalProductID = twoCOID;
            }

            if (RadioSKUNoImage.Checked == false)
            {
                // Validate image    
                if ((this.SkuId == 0) || (RadioSKUNewImage.Checked == true))
                {
                    if (UploadSKUImage.PostedFile != null && !string.IsNullOrEmpty(UploadSKUImage.PostedFile.FileName))
                    {
                        if (!UploadSKUImage.IsFileNameValid())
                        {
                            return false;
                        }

                        if (UploadSKUImage.PostedFile.FileName != string.Empty)
                        {
                            // Check for Store Image
                            fileInfo = UploadSKUImage.FileInformation;

                            if (fileInfo != null)
                            {
                                skuEntity.SKUPicturePath = fileInfo.Name;
                            }
                        }
                    }
                }
                else
                {
                    skuEntity.SKUPicturePath = skuEntity.SKUPicturePath;
                }
            }
            else
            {
                skuEntity.SKUPicturePath = null;
            }

            // Upload File if this is a new sku or the New Image option was selected for an existing sku
            if (RadioSKUNewImage.Checked || this.SkuId == 0)
            {
                if (fileInfo != null)
                {
                    UploadSKUImage.SaveImage();
                }
            }

            // Supplier
            if (ddlSupplier.SelectedIndex != -1)
            {
                if (ddlSupplier.SelectedItem.Text.Equals("Not Applicable"))
                {
                    skuEntity.SupplierID = null;
                }
                else
                {
                    skuEntity.SupplierID = Convert.ToInt32(ddlSupplier.SelectedValue);
                }
            }

            // Recurring Billing
            skuEntity.RecurringBillingPeriod = ddlBillingPeriods.SelectedItem.Value;
            skuEntity.RecurringBillingFrequency = ddlBillingFrequency.SelectedItem.Text;
            skuEntity.RecurringBillingTotalCycles = int.Parse(txtSubscrptionCycles.Text.Trim());

            // For Attribute value.
            string Attributes = String.Empty;

            DataSet MyAttributeTypeDataSet = prodAdminAccess.GetAttributeTypeByProductTypeID(this.ProductTypeId);

            foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
            {
                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                int selValue = int.Parse(lstControl.SelectedValue);

                if (selValue > 0)
                {
                    Attributes += selValue.ToString() + ",";
                }
            }

            // Split the string
            string Attribute;
            bool RetValue = false;
            if (Attributes.Length > 0)
            {
                Attribute = Attributes.Substring(0, Attributes.Length - 1);

                // To check SKU combination is already exists.
                RetValue = adminAccess.CheckSKUAttributes(this.ItemId, this.SkuId, Attribute);
            }

            if (RetValue)
            {
                lblError.Text = "* This Attribute combination already exists for this product. Please select different combination.";
                return false;
            }

            bool Check = false;

            // Check For Edit SKu
            if (this.SkuId > 0)
            {
                // Set update date for web service download
                skuEntity.UpdateDte = System.DateTime.Now;

                // Update Product SKU
                Check = adminAccess.Update(skuEntity);

                // Delete SKUAttributes
                adminAccess.DeleteBySKUId(this.SkuId);
            }
            else
            {
                // Add New Product SKU and SKUAttribute
                Check = adminAccess.Add(skuEntity, out this.SkuId);
            }

            foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
            {
                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                int selValue = int.Parse(lstControl.SelectedValue);

                if (selValue > 0)
                {
                    skuAttribute.AttributeId = selValue;

                    skuAttribute.SKUID = this.SkuId;

                    adminAccess.AddSKUAttribute(skuAttribute);
                }
            }

            return Check;
        }

        /// <summary>
        /// Show Subscription Panel Method
        /// </summary>
        private void ShowSubscriptionPanel()
        {
            ProductAdmin prodAdmin = new ProductAdmin();
            Product entity = prodAdmin.GetByProductId(this.ItemId);

            if (entity != null)
            {
                pnlRecurringBilling.Visible = entity.RecurringBillingInd;
            }
        }

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        private void Bind()
        {
            ProductAdmin _adminAccess = new ProductAdmin();

            DataSet MyDataSet = _adminAccess.GetAttributeTypeByProductTypeID(this.ProductTypeId);

            // Repeats until Number of AttributeType for this Product
            foreach (DataRow dr in MyDataSet.Tables[0].Rows)
            {
                // Bind Attributes
                DataSet _AttributeDataSet = _adminAccess.GetByAttributeTypeID(int.Parse(dr["attributetypeid"].ToString()));

                System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                lstControl.ID = "lstAttribute" + dr["AttributeTypeId"].ToString();

                ListItem li = new ListItem(dr["Name"].ToString(), "0");
                li.Selected = true;

                lstControl.DataSource = _AttributeDataSet;
                lstControl.DataTextField = "Name";
                lstControl.DataValueField = "AttributeId";
                lstControl.DataBind();
                lstControl.Items.Insert(0, li);

                if (!Convert.ToBoolean(dr["IsPrivate"]))
                {
                    // Add Dynamic Attribute DropDownlist in the Placeholder
                    ControlPlaceHolder.Controls.Add(lstControl);

                    // Required Field validator to check SKU Attribute
                    RequiredFieldValidator FieldValidator = new RequiredFieldValidator();
                    FieldValidator.ID = "Validator" + dr["AttributeTypeId"].ToString();
                    FieldValidator.ControlToValidate = "lstAttribute" + dr["AttributeTypeId"].ToString();
                    FieldValidator.ErrorMessage = "Select " + dr["Name"].ToString();
                    FieldValidator.Display = ValidatorDisplay.Dynamic;
                    FieldValidator.CssClass = "Error";
                    FieldValidator.InitialValue = "0";

                    ControlPlaceHolder.Controls.Add(FieldValidator);
                    Literal lit1 = new Literal();
                    lit1.Text = "&nbsp;&nbsp;";
                    ControlPlaceHolder.Controls.Add(lit1);
                }
            }

            // Hide the Product Attribute
            if (MyDataSet.Tables[0].Rows.Count == 0)
            {
                DivAttributes.Visible = false;
                pnlProductAttributes.Visible = false;
            }
        }

        /// <summary>
        /// Bind Edit Attributes
        /// </summary>
        private void BindAttributes()
        {
            SKUAdmin _adminSKU = new SKUAdmin();
            ProductAdmin _adminAccess = new ProductAdmin();
            SKUQuery skufilters = new SKUQuery();
            DataSet SkuDataSet = _adminSKU.GetBySKUId(this.SkuId);
            DataSet MyDataSet = _adminAccess.GetAttributeTypeByProductTypeID(this.ProductTypeId);

            foreach (DataRow dr in MyDataSet.Tables[0].Rows)
            {
                foreach (DataRow Dr in SkuDataSet.Tables[0].Rows)
                {
                    System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + dr["AttributeTypeId"].ToString());

                    if (lstControl != null)
                    {
                        lstControl.SelectedValue = Dr["Attributeid"].ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Binds Edit SKU Datas
        /// </summary>
        private void BindData()
        {
            // Create Instance for SKU Admin and SKU entity
            SKUAdmin skuAdmin = new SKUAdmin();
            SKU skuEntity = skuAdmin.DeepLoadBySKUID(this.SkuId);

            if (skuEntity != null)
            {
                SKU.Text = Server.HtmlDecode(skuEntity.SKU);

                existSKUvalue.Value = skuEntity.SKU;
                TwoCOProductID.Text = skuEntity.ExternalProductID.HasValue ? skuEntity.ExternalProductID.GetValueOrDefault(0).ToString() : string.Empty;

                // Load the inventory record and assign to text boxes.
                SKUInventory si = SKUAdmin.GetInventory(skuEntity);
                if (si != null)
                {
                    Quantity.Text = si.QuantityOnHand.ToString();
                    ReOrder.Text = si.ReOrderLevel.ToString();
                }

                WeightAdditional.Text = skuEntity.WeightAdditional.ToString();

                if (skuEntity.RetailPriceOverride.HasValue)
                {
                    RetailPrice.Text = skuEntity.RetailPriceOverride.Value.ToString("N");
                }

                if (skuEntity.SalePriceOverride.HasValue)
                {
                    SalePrice.Text = skuEntity.SalePriceOverride.Value.ToString("N");
                }

                if (skuEntity.WholesalePriceOverride.HasValue)
                {
                    WholeSalePrice.Text = skuEntity.WholesalePriceOverride.Value.ToString("N");
                }

                VisibleInd.Checked = skuEntity.ActiveInd;

                if (ddlSupplier.SelectedIndex != -1)
                {
                    ddlSupplier.SelectedValue = skuEntity.SupplierID.ToString();
                }

                if (skuEntity.SKUPicturePath != null)
                {
                    ZNodeImage znodeImage = new ZNodeImage();
                    SKUImage.ImageUrl = znodeImage.GetImageHttpPathMedium(skuEntity.SKUPicturePath);
                }
                else
                {
                    RadioSKUNoImage.Checked = true;
                    RadioSKUCurrentImage.Visible = false;
                }

                txtImageAltTag.Text = skuEntity.ImageAltTag;

                pnlRecurringBilling.Visible = skuEntity.ProductIDSource.RecurringBillingInd;

                if (skuEntity.ProductIDSource.RecurringBillingInd)
                {
                    ddlBillingPeriods.SelectedValue = skuEntity.RecurringBillingPeriod;
                    ddlBillingFrequency.SelectedValue = skuEntity.RecurringBillingFrequency;
                    txtSubscrptionCycles.Text = skuEntity.RecurringBillingTotalCycles.GetValueOrDefault(0).ToString();
                }
                //Zeon Custom Code:Starts
                BindSKUImageDatas();
                //Zeon Custom Code:Ends
            }
        }

        /// <summary>
        /// Bind supplier option list
        /// </summary>
        private void BindSuppliersTypes()
        {
            // Bind Supplier
            ZNode.Libraries.DataAccess.Service.SupplierService serv = new ZNode.Libraries.DataAccess.Service.SupplierService();
            TList<ZNode.Libraries.DataAccess.Entities.Supplier> list = serv.GetAll();
            list.Sort("DisplayOrder Asc");

            list.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Supplier supplier)
            {
                return supplier.ActiveInd == true;
            });

            DataSet ds = list.ToDataSet(false);
            DataView dv = new DataView(ds.Tables[0]);
            ddlSupplier.DataSource = dv;
            ddlSupplier.DataTextField = "name";
            ddlSupplier.DataValueField = "supplierid";
            ddlSupplier.DataBind();
            ListItem li1 = new ListItem("Not Applicable", "0");
            ddlSupplier.Items.Insert(0, li1);
        }

        /// <summary>
        /// Bind the locale dropdown, if this sku have different locale.
        /// </summary>
        private void BindLocaleDropdown()
        {
            // Get the Locale information with product id.
            DataSet Localeds = SKUAdmin.GetLocaleIdsBySkuId(this.SkuId);

            // Bind the dropdown.
            ddlLocales.DataSource = Localeds.Tables[0];
            ddlLocales.DataTextField = "LocaleDescription";
            ddlLocales.DataValueField = "SkuId";
            ddlLocales.DataBind();

            if (ddlLocales.Items.Count == 0)
            {
                pnlSKULocale.Visible = false;
            }
            else
            {
                pnlSKULocale.Visible = true;

                // Select the current language.
                ddlLocales.SelectedValue = this.SkuId.ToString();
            }
        }

        /// <summary>
        /// Bind Billing Frequency Method
        /// </summary>
        private void BindBillingFrequency()
        {
            ddlBillingFrequency.Items.Clear();

            int upperBoundValue = 30;

            if (ddlBillingPeriods.SelectedValue == "WEEK")
            {
                // Week
                upperBoundValue = 4;
            }
            else if (ddlBillingPeriods.SelectedValue == "MONTH")
            {
                // Month
                upperBoundValue = 12;
            }
            else if (ddlBillingPeriods.SelectedValue == "YEAR")
            {
                // Year
                upperBoundValue = 1;
            }

            for (int i = 1; i <= upperBoundValue; i++)
            {
                ddlBillingFrequency.Items.Add(i.ToString());
            }
        }
        #endregion

        #region Zeon Custom Code

        #region Protected Events and Methods

        /// <summary>
        /// handles button clicj=k event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddSKUView_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddViewlink + this.ItemId + "&skuid=" + this.SkuId + "&typeid=" + this.ProductTypeId);
        }

        /// <summary>
        /// Get http image path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the image file path with name.</returns>
        public string GetImagePath(string imageFileName)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(imageFileName);
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.AddViewlink + this.ItemId + "&productimageid=" + e.CommandArgument.ToString() + "&skuid=" + this.SkuId + "&typeid=" + this.ProductTypeId);
                }

                if (e.CommandName == "RemoveItem")
                {
                    ZeonSKUImageService zeonSkuImgSer = new ZeonSKUImageService();
                    bool Status = zeonSkuImgSer.Delete(int.Parse(e.CommandArgument.ToString()));
                    if (Status)
                    {
                        this.BindSKUImageDatas();
                    }
                    updPnlGridThumb.Update();
                }
            }
        }

        /// <summary>
        /// Related Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridThumb.PageIndex = e.NewPageIndex;
            this.BindSKUImageDatas();
        }

        /// <summary>
        /// Grid Row Deleting  Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindSKUImageDatas();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// bind zeonsku images by skuid
        /// </summary>
        private void BindSKUImageDatas()
        {
            ZeonSKUImageService zeonSKUImgSer = new ZeonSKUImageService();
            try
            {
                TList<ZeonSKUImage> zeonSKUImageList = zeonSKUImgSer.GetBySKUID(this.SkuId);
                zeonSKUImageList = zeonSKUImageList.FindAll(x => x.ProductImageTypeID == 2);
                GridThumb.DataSource = zeonSKUImageList;
                GridThumb.DataBind();
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in BindSKUImageDatas!!" + this.Request.Url + this.SkuId + ex.ToString());
            }
        }

        #endregion

        #endregion
    }
}
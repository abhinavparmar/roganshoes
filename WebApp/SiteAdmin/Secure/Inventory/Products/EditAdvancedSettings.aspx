<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    ValidateRequest="false" Inherits="SiteAdmin.Secure.Inventory.Products.EditAdvancedSettings"
    Title="Manage Products - Edit Advanced Settings" CodeBehind="EditAdvancedSettings.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server" Text="Edit Settings for"></asp:Label></h1>
            <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
        </div>
        <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
        <div class="FormView">
            <h4 class="SubTitle">Display Settings</h4>
            <div class="FieldStyle">z
                Display Product
            </div>
            <div class="ValueStyle">
                <asp:CheckBox Checked="true" ID="CheckEnabled" Text="Display this product in the store."
                    runat="server" />
            </div>
            <div class="FieldStyle">
                Home Page Special
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="CheckHomeSpecial" Text="Display product as a featured item on the home page."
                    runat="server" />
            </div>
            <div class="FieldStyle">
                New Item
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="CheckNewItem" Text='Display the "new" icon for this product.'
                    runat="server" />
            </div>
            <div class="FieldStyle">
                Featured Item
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="ChkFeaturedProduct" Text='Display the "featured" icon for this product.'
                    runat="server" />
            </div>
            <div class="FieldStyle">
                Call For Pricing
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="CheckCallPricing" Text="Prompt customer to call for pricing. Disable the add-to-cart button"
                    runat="server" />
            </div>
            <div class="FieldStyle" style="display: none;">
                Display Inventory
            </div>
            <div class="ValueStyle" style="display: none;">
                <asp:CheckBox ID="CheckDisplayInventory" Text="Product inventory to be displayed to the user."
                    runat="server" />
            </div>
            <asp:Panel ID="pnlFranchise" runat="server">
                <div class="FieldStyle">
                    Franchisable
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="CheckFranchisable" Text="Enable your franchisees to sell this product"
                        runat="server" />
                </div>
            </asp:Panel>
          <%--  Zeon Product Extn Setting--%>
            <div class="FieldStyle">
                Is PreOrder
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkPreOrder" Text="Enable customer to place preorder"
                    runat="server" />
            </div>
            <div class="FieldStyle">
                IsMapPrice
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkMapPrice" Text="Enable customer for mapping price"
                    runat="server" />
            </div>
             <div class="FieldStyle">
               Disable Follow Index
                 <br />
                 <small> Disabling this will add "NOINDEX, FOLLOW" in robot meta tag inspite of "INDEX, FOLLOW"  in page source.<br />Will stop page indexing.</small>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkFollInd" Text="Follow Index"
                    runat="server" />
            </div>
           <%-- Zeon Product Extn Setting--%>
            <h4 class="SubTitle">Inventory Settings</h4>
            <div class="FieldStyle">
                Out-of-Stock Options
            </div>
            <div class="ValueStyle">
                <asp:RadioButtonList ID="InvSettingRadiobtnList" runat="server">
                    <asp:ListItem Selected="True" Value="1">Disable purchasing for out-of-stock products.</asp:ListItem>
                    <asp:ListItem Value="2">Allow back-ordering of products.</asp:ListItem>
                    <asp:ListItem Value="3">Don't track inventory. Enable product purchasing regardless of stock.</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="FieldStyle">
                In-Stock Message
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtInStockMsg" runat="server"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Out-of-Stock Message
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtOutofStock" runat="server">Out of Stock</asp:TextBox>
            </div>
            <div class="FieldStyle">
                Back-Order Message
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBackOrderMsg" runat="server"></asp:TextBox>
            </div>
            <div style="display: none;">
                <small>Check this box to indicate that this product is a drop ship item.</small>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkDropShip" runat="server" Visible="false" Text="Drop Ship" />
            </div>
            <h4 class="SubTitle">Recurring Billing Settings</h4>
            <div class="FieldStyle">
                Recurring Billing
            </div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkRecurringBillingInd" AutoPostBack="true" OnCheckedChanged="ChkRecurringBillingInd_CheckedChanged"
                    runat="server" Text="Enable recurring billing for this product" />
            </div>
            <asp:Panel ID="pnlRecurringBilling" runat="server" Visible="false">
                <div class="FieldStyle">
                    Billing Amount<span class="Asterix">*</span><br />
                    <small>Enter the amount billed for each billing period.</small>
                </div>
                <div class="ValueStyle">
                    <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;
                    <asp:TextBox ID="txtRecurringBillingInitialAmount" MaxLength="10" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter a recurring billing amount"
                        ControlToValidate="txtRecurringBillingInitialAmount" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtRecurringBillingInitialAmount"
                        Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid billing amount (ex: 123.45)"
                        CssClass="Error" Display="Dynamic" /><br />
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtRecurringBillingInitialAmount"
                        ErrorMessage="You must enter a Billing Amount value between $0 and $999,999.99"
                        MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
                </div>
                <div class="FieldStyle">
                    Billing Period<span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlBillingPeriods" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DdlBillingPeriods_SelectedIndexChanged">
                        <asp:ListItem Text="Weekly" Value="WEEK"></asp:ListItem>
                        <asp:ListItem Text="Monthly" Value="MONTH"></asp:ListItem>
                        <asp:ListItem Text="Yearly" Value="YEAR"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </asp:Panel>
        </div>
        <h4 class="SubTitle">SEO Settings</h4>

        <div class="FormView">
            <div class="FieldStyle">
                SEO Title
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Keywords
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Description
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                SEO Friendly Page Name<br />
                <small>Use only characters a-z and numbers 0-9. Use "-" instead of spaces. Do not use file extensions.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOUrl" runat="server" MaxLength="100" Columns="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSEOUrl"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid SEO friendly name"
                    SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle" style="display: none">
            </div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkAddURLRedirect" runat="server" Text=' Add 301 redirect on URL changes.' />
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>

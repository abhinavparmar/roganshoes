<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Inventory.Products.View" ValidateRequest="false" Title="Manage Products - View" CodeBehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ProductTags" Src="producttags.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">

    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            var Tab = document.getElementById('<%=tabProductDetails.ClientID%>');
            var activeTabIndex = document.getElementById('<%=hdnActiveTabIndex.ClientID%>');
            activeTabIndex.value = sender.get_activeTabIndex();
        }
    </script>

    <div>
        <div class="LeftFloat" style="width: 70%;">
            <h1>Product Details - <%= lblProdName.Text.Trim() %></h1>
        </div>
        <div class="LeftFloat ImageButtons" style="width: 29%" align="right">
            <asp:Button ID="btnBack" CausesValidation="False" Text="<< PRODUCT LIST"
                runat="server" OnClick="BtnBack_Click"></asp:Button>
        </div>
    </div>
    <div class="ClearBoth" align="left">
        <hr />
    </div>
    <asp:HiddenField ID="hdnActiveTabIndex" runat="server" Value="0" />
    <div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <ajaxToolKit:TabContainer ID="tabProductDetails" OnClientActiveTabChanged="ActiveTabChanged"
        TabStripPlacement="Top" runat="server" ActiveTabIndex="11">
        <ajaxToolKit:TabPanel ID="pnlGeneral" runat="server">
            <HeaderTemplate>
                Info
            </HeaderTemplate>
            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer7" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div style="width: 100%; margin-bottom: 20px;">
                    <div class="ImageButtons" style="float: left;">
                        <asp:Button ID="EditProduct" runat="server" OnClick="EditProduct_Click" Text="Edit Information" />
                    </div>
                    <script type="text/javascript" language="javascript">
                        var $j = jQuery.noConflict();

                        var isAuthenticated = function () {
                            var retval = false;

                            $j.ajax({
                                url: "/plugins/x-commerce/admin/Account/IsAuthenticated",
                                type: "POST",
                                success: function (data) {
                                    retval = data.IsAuthenticated;
                                },
                                async: false
                            });

                            return retval;
                        };

                        var refreshProductDetailsPage = function () {
                            if (window.opener && !window.opener.closed) {
                                window.opener.location.reload(true);
                            }
                        };

                        var stateChanged = false;

                        $j(document).ready(function () {

                            var $marketplaceModal = $find("marketplaceModal");
                            var $marketplaceIFrame = $j("#marketplaceAccountFrame");

                            $j("div.btn-group ul.dropdown-menu li a").live("click", function (evt) {
                                evt.preventDefault();
                                if (isAuthenticated()) {
                                    var src;
                                    src = $j(evt.target).attr("href");
                                    $marketplaceIFrame.attr("src", src);
                                    $marketplaceModal.show();
                                }
                                else {
                                    refreshProductDetailsPage();
                                }
                            });

                            $j("button#closeMarketplaceAccountModal").live("click", function (evt) {
                                evt.preventDefault();
                                if (stateChanged) {
                                    refreshProductDetailsPage();
                                }
                                else {
                                    $marketplaceModal.hide();
                                    $marketplaceIFrame.attr("src", "");
                                }
                            });
                        });
                    </script>
                    <div id="btnXCommerceAdmin" runat="server" clientidmode="Static">
                    </div>
                </div>
                <asp:Button ID="btnShowMarketplaceModal" runat="server" Style="display: none" ClientIDMode="Static" />
                <ajaxToolKit:ModalPopupExtender ID="marketplaceModal" runat="server" TargetControlID="btnShowMarketplaceModal" PopupControlID="pnlMarketplaceActions" BackgroundCssClass="modalBackground" ClientIDMode="Static" />
                <asp:Panel ID="pnlMarketplaceActions" runat="server" Style="display: none;" CssClass="PopupStyle" Width="60%" Height="65%">
                    <div style="width: 100%; height: 98%;">
                        <button class="close" id="closeMarketplaceAccountModal" style="float: right;">x</button>
                        <iframe src="" clientidmode="Static" runat="server" id="marketplaceAccountFrame" style="border: 0px; border-top: 1px solid #EEE; width: 100%; height: 95%; margin-top: 1%;" marginheight="0" marginwidth="0" frameborder="0"></iframe>
                    </div>
                </asp:Panel>
                <br />
                <h4 class="SubTitle">General Information</h4>
                <div class="Display">
                    <div class="FieldStyle">
                        Product Name
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblProdName" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        Product Code
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblProdNum" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        Min Selectable Quantity
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblMinQuantity" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        Max Selectable Quantity
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblMaxQuantity" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        Product Type
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblProdType" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div runat="server" id="divExpirationPeriod" visible="false">
                        <div class="FieldStyle">
                            Expiration Period
                        </div>
                        <div class="ValueStyle">
                            <asp:Label ID="lblExpirationPeriod" runat="server"></asp:Label>&nbsp;
                        </div>
                    </div>
                    <div class="FieldStyleA">
                        Supplier Name
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblSupplierName" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        Retail Price
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblRetailPrice" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        Sale Price
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblSalePrice" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        WholeSale Price
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblWholeSalePrice" runat="server" CssClass="Price"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        Tax Class
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblTaxClass" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        Size Chart Link
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblDownloadLink" runat="server" Text="Label"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        YouTube Video URL
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblVideoUrl" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="ClearBoth">
                        <br />
                    </div>
                    <h4 class="SubTitle">Display Settings</h4>
                    <div class="FieldStyle">
                        Display Order
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblProdDisplayOrder" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="ClearBoth">
                        <br />
                    </div>
                    <h4 class="SubTitle">Shipping Settings</h4>
                    <div class="FieldStyle">
                        Free Shipping
                    </div>
                    <div class="ValueStyle">
                        <img id="freeShippingInd" runat="server" />
                    </div>
                    <div class="FieldStyleA">
                        Ship Separately
                    </div>
                    <div class="ValueStyleA">
                        <img id="shipSeparatelyInd" runat="server" />
                    </div>
                    <div class="FieldStyle">
                        Shipping Rule
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblShippingRuleTypeName" runat="server" />&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        Shipping Rate
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblShippingRate" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        Weight
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblWeight" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        Height
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblHeight" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        Width
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblWidth" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        Length
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblLength" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="ClearBoth">
                        <br />
                    </div>
                    <h4 class="SubTitle">Product Image</h4>
                    <div class="Image">
                        <asp:Image ID="ItemImage" runat="server" />
                    </div>
                    <h4 class="SubTitle">Short Description</h4>
                    <div class="ShortDescription">
                        <asp:Label ID="lblShortDescription" runat="server"></asp:Label>
                    </div>
                    <br />
                    <h4 class="SubTitle">Product Description</h4>
                    <div class="Description">
                        <asp:Label ID="lblProductDescription" runat="server"></asp:Label>
                    </div>
                    <div>
                        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                </div>
                <h4 class="SubTitle">Product Features</h4>
                <div class="Features">
                    <asp:Label ID="lblProductFeatures" runat="server"></asp:Label>
                </div>
                <br />
                <h4 class="SubTitle">Product Specification</h4>
                <div class="Features">
                    <asp:Label ID="lblproductspecification" runat="server"></asp:Label>
                </div>
                <br />
                <h4 class="SubTitle">Shipping Information</h4>
                <div class="Features">
                    <asp:Label ID="lbladditionalinfo" runat="server"></asp:Label>
                </div>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlAdvancedSettings" runat="server">
            <HeaderTemplate>
                Settings
            </HeaderTemplate>
            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer9" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div align="left" class="ImageButtons">
                    <asp:Button CssClass="Size175" ID="Button2" runat="server" OnClick="EditAdvancedSettings_Click"
                        Text="Edit Settings" />
                </div>
                <br />
                <h4 class="SubTitle">Display Settings</h4>
                <div class="Display">
                    <tr class="RowStyle">
                        <div class="FieldStyleImg">
                            Enabled
                        </div>
                        <div class="ValueStyleImg">
                            <img id="chkProductEnabled" runat="server" alt="" src="" />
                        </div>
                        <div class="FieldStyleImgA">
                            Home Page Special
                        </div>
                        <div class="ValueStyleImgA">
                            <img id="chkIsSpecialProduct" runat="server" alt="" src="" />
                        </div>
                        <div class="FieldStyleImg">
                            New Item
                        </div>
                        <div class="ValueStyleImg">
                            <img id="chkIsNewItem" runat="server" alt="" src="" />
                        </div>
                        <div class="FieldStyleImgA">
                            Featured Item
                        </div>
                        <div class="ValueStyleImgA">
                            <img id="FeaturedProduct" runat="server" alt="" src="" />
                        </div>
                        <div class="FieldStyleImg">
                            Call For Pricing
                        </div>
                        <div class="ValueStyleImg">
                            <img id="chkProductPricing" runat="server" alt="" src="" />
                        </div>
                        <div class="DisplayNone">
                            Display Inventory
                        </div>
                        <div class="DisplayNone">
                            <img id="chkproductInventory" runat="server" alt="" src="" />
                        </div>
                        <asp:Panel ID="pnlFranchise" runat="server">
                            <div class="FieldStyleImgA">
                                Franchisable
                            </div>
                            <div class="ValueStyleImgA">
                                <img id="chkFranchisable" runat="server" alt="" src="" />
                            </div>
                        </asp:Panel>
                         <%--  Zeon Custom Product Extn Setting--%>
                        <div class="FieldStyleImg">
                           Is PreOrder
                        </div>
                        <div class="ValueStyleImg">
                            <img id="imgPreOrder" runat="server" alt="" src='' />
                        </div>
                        <div class="FieldStyleImg">
                           Is MapPrice
                        </div>
                        <div class="ValueStyleImg">
                            <img id="imgMapPrice" runat="server" alt="" src='' />
                        </div>
                        <div class="FieldStyleImg">
                             Follow Index
                        </div>
                        <div class="ValueStyleImg">
                           <img id="imgIndexFollow" runat="server" />
                        </div>
                        <%-- Zeon Custom Product Extn Setting--%>
                        <div class="ClearBoth">
                            <br />
                        </div>
                        <h4 class="SubTitle">Inventory Settings</h4>
                        <div class="FieldStyleImg">
                            <img id="chkCartInventoryEnabled" runat="server" alt="" src='' />
                        </div>
                        <div class="ValueStyleImg">
                            Disable purchasing for out-of-stock products.
                        </div>
                        <div class="FieldStyleImgA">
                            <img id="chkIsBackOrderEnabled" runat="server" alt="" src='' />
                        </div>
                        <div class="ValueStyleImgA">
                            Allow back-ordering of products.
                        </div>
                        <div class="FieldStyleImg">
                            <img id="chkIstrackInvEnabled" runat="server" alt="" src="" />
                        </div>
                        <div class="ValueStyleImg">
                            Don't track inventory. Enable product purchasing regardless of stock.
                        </div>
                        <div class="FieldStyleA">
                            In-Stock Message
                        </div>
                        <div class="ValueStyleA">
                            <asp:Label ID="lblInStockMsg" runat="server"></asp:Label>&nbsp;
                        </div>
                        <div class="FieldStyle">
                            Out-of-Stock Message
                        </div>
                        <div class="ValueStyle">
                            <asp:Label ID="lblOutofStock" runat="server"></asp:Label>&nbsp;
                        </div>
                        <div class="FieldStyleA">
                            Back-Order Message
                        </div>
                        <div class="ValueStyleA">
                            <asp:Label ID="lblBackOrderMsg" runat="server"></asp:Label>&nbsp;
                        </div>
                        <div class="FieldStyleImg" style="display: none;">
                            Drop Ship
                        </div>
                        <div class="ValueStyleImg" style="display: none;">
                            <img id="IsDropShipEnabled" runat="server" alt="" src='' />
                        </div>

                        <div class="ClearBoth">
                            <br />
                        </div>
                        <h4 class="SubTitle">Recurring Billing Settings</h4>
                        <div class="FieldStyleImg">
                            Recurring Billing 
                        </div>
                        <div class="ValueStyleImg">
                            <img id="imgRecurringBillingInd" runat="server" alt="" src='' />
                        </div>
                        <div class="FieldStyleA">
                            Amount
                        </div>
                        <div class="ValueStyleA">
                            <asp:Label ID="lblRecurringBillingInitialAmount" runat="server"></asp:Label>&nbsp;
                        </div>
                        <div class="FieldStyle">
                            Period
                        </div>
                        <div class="ValueStyle">
                            <asp:Label ID="lblBillingPeriod" runat="server"></asp:Label>&nbsp;
                        </div>
                        <div class="ClearBoth">
                            <br />
                        </div>
                </div>
                <h4 class="SubTitle">SEO Settings</h4>
                <div class="Display">
                    <div class="FieldStyle">
                        SEO Title
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblSEOTitle" runat="server"></asp:Label>
                    </div>
                    <div class="FieldStyleA">
                        SEO Keywords
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblSEOKeywords" runat="server" CssClass="Price"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        SEO Description
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblSEODescription" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        SEO URL
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblSEOURL" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="ClearBoth">
                    </div>
                </div>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="TabDepartments" runat="server">
            <HeaderTemplate>
                Categories
            </HeaderTemplate>
            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div class="ButtonStyle">
                    <zn:LinkButton ID="btnAssociateDepartment" runat="server"
                        ButtonType="Button" OnClick="BtnAssociateDepartment_Click" Text="Associate Categories"
                        ButtonPriority="Primary" />
                </div>
                <div>
                    <uc1:Spacer ID="Spacer10" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="gvProductDepartment" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                            runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                            CssClass="Grid" Width="100%" GridLines="None" AllowPaging="True" PageSize="5"
                            OnPageIndexChanging="GvProductDepartment_PageIndexChanging" OnRowCommand="GvProductDepartment_RowCommand">
                            <Columns>
                                <asp:BoundField DataField="ProductCategoryId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                <asp:BoundField DataField="Title" HeaderText="Title" HeaderStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="Delete" Text="Remove &raquo" CommandArgument='<%# Eval("ProductCategoryId") %>'
                                            CommandName="RemoveItem" CssClass="actionlink" runat="server" OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No category items found.
                            </EmptyDataTemplate>
                            <RowStyle CssClass="RowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <FooterStyle CssClass="FooterStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlManageinventory" runat="server">
            <HeaderTemplate>
                SKUs
            </HeaderTemplate>
            <ContentTemplate>
                <div class="Form">
                    <div>
                        <uc1:Spacer ID="Spacer15" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <asp:Panel ID="pnlSKUAttributes" runat="server">
                        <div class="ButtonStyle">
                            <zn:LinkButton ID="butAddNewSKU" runat="server"
                                ButtonType="Button" OnClick="BtnAddSKU_Click" Text="Add SKU or Part#"
                                ButtonPriority="Primary" />
                        </div>

                        <div class="SearchForm">
                            <div class="SubTitle">
                                SEARCH SKUs
                            </div>
                            <div class="ValueStyle">
                                <asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder>
                            </div>
                            <div>
                                <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_clear_highlight.gif';"
                                    onmouseout="this.src='../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                                    runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                                <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../Themes/Images/buttons/button_search_highlight.gif';"
                                    onmouseout="this.src='../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                                    runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <h4 class="GridTitle">Current Inventory</h4>
                <asp:UpdatePanel ID="updPnlInventoryDisplayGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="uxGridInventoryDisplay" Width="100%" CssClass="Grid" CellPadding="4"
                            CaptionAlign="Left" GridLines="None" runat="server" AutoGenerateColumns="False"
                            AllowPaging="True" ForeColor="Black" OnPageIndexChanging="UxGridInventoryDisplay_PageIndexChanging"
                            OnRowCommand="UxGridInventoryDisplay_RowCommand" OnRowDeleting="UxGridInventoryDisplay_RowDeleting"
                            PageSize="25">
                            <FooterStyle CssClass="FooterStyle" />
                            <Columns>
                                <asp:BoundField DataField="sku" HeaderText="SKU or Part#" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="skuid" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Quantity On Hand">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="quantity" Text='<%# GetSkuQuantity((int)Eval("skuid")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Re-Order Level">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="reorderlevel" Text='<%# GetSkuReorderLevel((int)Eval("skuid")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <img alt="" id="Img3" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div id="Test" runat="server">
                                            <asp:LinkButton CssClass="actionlink" ID="EditSKU" Text="Edit &raquo" CommandArgument='<%# Eval("skuid") %>'
                                                CommandName="Edit" runat="Server" />
                                            <asp:LinkButton ID="RemoveSKU" CssClass="actionlink" Text="Remove &raquo" CommandArgument='<%# Eval("skuid") %>'
                                                CommandName="Delete" runat="Server" Visible='<%# HasAttributes  %>' />
                                            <asp:LinkButton ID="ManageSpec" CssClass="Button" Text="Manage Spec &raquo" CommandArgument='<%# Eval("skuid") %>'
                                                CommandName="ManageSpec" runat="Server" />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        </asp:GridView>
                        <uc1:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                        <asp:Label ID="lblSkuErrorMsg" runat="server" CssClass="Error"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlBundleProduct" runat="server">
            <HeaderTemplate>
                Bundles
            </HeaderTemplate>
            <ContentTemplate>
                <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                <div class="ButtonStyle">
                    <zn:LinkButton ID="AddBundleProducts" runat="server"
                        ButtonType="Button" OnClick="AddBundleProducts_Click" CausesValidation="False" Text="Associate Product"
                        ButtonPriority="Primary" />
                </div>

                <br />
                <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                <%--  <asp:UpdatePanel ID="updPnlBundleProductGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>--%>
                <asp:GridView ID="uxBundleProductGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                    runat="server" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid" Width="100%"
                    OnPageIndexChanging="UxBundleProductGrid_PageIndexChanging" OnRowCommand="UxBundleProductGrid_RowCommand"
                    OnRowDataBound="UxBundleProductGrid_RowDataBound" OnRowDeleting="UxBundleProductGrid_RowDeleting"
                    GridLines="None" AllowPaging="True" PageSize="10">
                    <Columns>
                        <asp:BoundField DataField="ChildProductId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Product Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblProductName" runat="server" Text='<%# GetProductName(Eval("ChildProductId")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(GetProductIsActive(Eval("ChildProductId")))%>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="Delete" Text="Remove &raquo" CommandArgument='<%# Eval("ParentChildProductId") %>'
                                    CommandName="RemoveItem" CssClass="actionlink" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No products found
                    </EmptyDataTemplate>
                    <RowStyle CssClass="RowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <FooterStyle CssClass="FooterStyle" />
                </asp:GridView>
                <%--   </ContentTemplate>
                </asp:UpdatePanel>--%>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlTags" runat="server">
            <HeaderTemplate>
                Facets
            </HeaderTemplate>
            <ContentTemplate>
                <uc1:ProductTags runat="server" />
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlRelatedItems" runat="server">
            <HeaderTemplate>
                Related Items
            </HeaderTemplate>
            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div>
                    <div class="TabDescription" style="width: 75%;">
                        <p>
                            Cross-Sell / Up Sell / Related  products to the customer.
                        </p>
                    </div>
                    <div class="NewButtonStyle">
                        <zn:LinkButton ID="AddRelatedItems" runat="server"
                            ButtonType="Button" OnClick="AddRelatedItems_Click" Text="Add Related Items"
                            ButtonPriority="Primary" />
                    </div>
                </div>
                <div class="ClearBoth"></div>
                <div>
                    <uc1:Spacer ID="Spacer6" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>

                <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                <%-- <asp:UpdatePanel ID="updPnlRealtedGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>--%>
                <asp:GridView ID="uxGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                    runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                    CssClass="Grid" Width="100%" GridLines="None" OnRowCommand="UxGrid_RowCommand"
                    AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging"
                    OnRowDataBound="UxGrid_RowDataBound" OnRowDeleting="UxGrid_RowDeleting"
                    PageSize="5">
                    <Columns>
                        <asp:BoundField DataField="RelatedProductId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="Product Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblProductName" runat="server" Text='<%# GetProductName(Eval("RelatedProductId")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cross Sell Type" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblProductType" runat="server" Text='<%# Eval("CrossSellType") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="Delete" Text="Remove &raquo" CommandArgument='<%# Eval("RelatedProductId") %>'
                                    CommandName="RemoveItem" CssClass="actionlink" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No related items found
                    </EmptyDataTemplate>
                    <RowStyle CssClass="RowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <FooterStyle CssClass="FooterStyle" />
                </asp:GridView>
                <%-- </ContentTemplate>
                </asp:UpdatePanel>--%>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlAlternateImages" runat="server">
            <HeaderTemplate>
                Images
            </HeaderTemplate>
            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div class="ButtonStyle">
                    <zn:LinkButton ID="Button1" runat="server"
                        ButtonType="Button" OnClick="AddProductView_Click" Text="Add Alternate Product Image"
                        ButtonPriority="Primary" />
                </div>
                <div>
                    <uc1:Spacer ID="Spacer23" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                <asp:UpdatePanel ID="updPnlGridThumb" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="GridThumb" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                            runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                            CssClass="Grid" Width="100%" GridLines="None" OnRowCommand="GridThumb_RowCommand"
                            AllowPaging="True" OnPageIndexChanging="GridThumb_PageIndexChanging" OnRowDeleting="GridThumb_RowDeleting"
                            PageSize="5">
                            <Columns>
                                <asp:BoundField DataField="productimageid" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <img id="SwapImage" alt="" src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ImageTypeName" HeaderText="Image Type" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Product Page" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Category Page" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <img id="Img2" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage").ToString()))%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton CssClass="actionlink" ID="EditProductView" Text="Edit &raquo" CommandArgument='<%# Eval("productimageid") %>'
                                            CommandName="Edit" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:LinkButton ID="Delete" Text="Delete &raquo" CommandArgument='<%# Eval("productimageid") %>'
                                            CommandName="RemoveItem" CssClass="actionlink" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No alternate product image found
                            </EmptyDataTemplate>
                            <RowStyle CssClass="RowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <FooterStyle CssClass="FooterStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlProductOptions" runat="server">
            <HeaderTemplate>
                Add-Ons
            </HeaderTemplate>
            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer14" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div>
                    <div class="TabDescription" style="width: 80%;">
                        <p>
                            Add-Ons are additional product options that a customer can configure during checkout. For example: "Gift Wrapping", etc
                        </p>
                    </div>
                    <div class="ButtonStyle" style="clear: right; float: right; padding-top: 10px; padding-right: 10px;">
                        <zn:LinkButton ID="btnAddNewAddOn" runat="server"
                            ButtonType="Button" OnClick="BtnAddNewAddOn_Click" Text="Associate Add-On"
                            ButtonPriority="Primary" />
                    </div>
                </div>
                <div class="ClearBoth"></div>
                <div>
                    <uc1:Spacer ID="Spacer19" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                <asp:UpdatePanel ID="updPnlProductAddOnsGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView OnRowDataBound="UxGridProductAddOns_RowDataBound" ID="uxGridProductAddOns"
                            runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGridProductAddOns_PageIndexChanging"
                            CaptionAlign="Left" OnRowCommand="UxGridProductAddOns_RowCommand" Width="100%"
                            EnableSortingAndPagingCallbacks="False" PageSize="15" AllowSorting="True" EmptyDataText="No Add-Ons associated with this Product.">
                            <Columns>
                                <asp:BoundField DataField="ProductAddOnId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# GetAddOnName(Eval("AddonId")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Title" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# GetAddOnTitle(Eval("AddonId")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton CommandName="Remove" CausesValidation="false" ID="btnDelete" runat="server"
                                            Text="Remove &raquo" CssClass="actionlink" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle CssClass="FooterStyle" />
                            <RowStyle CssClass="RowStyle" />
                            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlTieredPricing" runat="server">
            <HeaderTemplate>
                Tiered Pricing
            </HeaderTemplate>
            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer16" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div>
                    <div class="TabDescription" style="width: 80%;">
                        <p>
                            Tiered pricing enables you to specify a product price based on the number of items ordered. For example: you could offer discounted pricing if the customer buys more than 10 items.
                        </p>
                    </div>
                    <div class="ButtonStyle" style="float: right; padding-top: 10px; padding-right: 10px;">
                        <zn:LinkButton ID="AddTieredPricing" runat="server"
                            ButtonType="Button" OnClick="AddTieredPricing_Click" Text="Add Pricing Tier"
                            ButtonPriority="Primary" />
                    </div>
                </div>
                <div class="ClearBoth"></div>
                <div>
                    <uc1:Spacer ID="Spacer20" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                <asp:UpdatePanel ID="updPnlTieredPricingGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="uxGridTieredPricing" Width="100%" CssClass="Grid" CellPadding="4"
                            CaptionAlign="Left" GridLines="None" runat="server" AutoGenerateColumns="False"
                            AllowPaging="True" ForeColor="Black" OnPageIndexChanging="UxGridTieredPricing_PageIndexChanging"
                            OnRowDataBound="UxGridTieredPricing_RowDataBound" OnRowCommand="UxGridTieredPricing_RowCommand"
                            OnRowDeleting="UxGridTieredPricing_RowDeleting" PageSize="25" EmptyDataText="No Tiered pricing found for this Product.">
                            <FooterStyle CssClass="FooterStyle" />
                            <Columns>
                                <asp:BoundField DataField="ProductTierID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Profile" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# GetProfileName(DataBinder.Eval(Container.DataItem, "ProfileID"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="TierStart" HeaderText="Tier Start" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="TierEnd" HeaderText="Tier End" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Price" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# DataBinder.Eval(Container.DataItem,"Price","{0:c}") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div id="Div1" runat="server">
                                            <asp:LinkButton CssClass="actionlink" ID="EditTieredPricing" Text="Edit &raquo" CommandArgument='<%# Eval("ProductTierID") %>'
                                                CommandName="Edit" runat="Server" />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnRemoveTieredPricing" CssClass="actionlink" Text="Delete &raquo"
                                            CommandArgument='<%# Eval("ProductTierID") %>' CommandName="Delete" runat="Server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlHighlights" runat="server">
            <HeaderTemplate>
                Highlights
            </HeaderTemplate>
            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer17" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>

                <div>
                    <div class="TabDescription" style="width: 75%;">
                        <p>
                            Highlights enable you to showcase common product features. For example, you could display<br />
                            the "Certified Organic" label for specific products.
                        </p>
                    </div>
                    <div class="NewButtonStyle">
                        <zn:LinkButton ID="btnAddNewHighlight" runat="server"
                            ButtonType="Button" OnClick="BtnAddHighlight_Click" Text="Associate a Highlight"
                            ButtonPriority="Primary" />
                    </div>
                </div>
                <div class="ClearBoth"></div>
                <div>
                    <uc1:Spacer ID="Spacer22" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>

                <uc1:Spacer ID="Spacer12" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                <div>
                    <asp:GridView ID="uxGridHighlights" Width="100%" CssClass="Grid" CellPadding="4"
                        CaptionAlign="Left" GridLines="None" runat="server" AutoGenerateColumns="False"
                        AllowPaging="True" ForeColor="Black" OnPageIndexChanging="UxGridHighlights_PageIndexChanging"
                        OnRowDataBound="UxGridHighlights_RowDataBound" OnRowDeleting="UxGridHighlights_RowDeleting"
                        OnRowCommand="UxGridHighlights_RowCommand" PageSize="25" EmptyDataText="No highlights found for this Product.">
                        <FooterStyle CssClass="FooterStyle" />
                        <Columns>
                            <asp:TemplateField Visible="true" HeaderStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    ID
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem,"HighlightId")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    Name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem,"Name") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ImageTypeName" HeaderText="Type" HeaderStyle-HorizontalAlign="Left" />
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    Display Order
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem,"DisplayOrder")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="DeleteHighlight" CssClass="actionlink" Text="Remove &raquo" CommandArgument='<%# Eval("ProductHighlightID") %>'
                                        CommandName="Delete" runat="Server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="RowStyle" />
                        <EditRowStyle CssClass="EditRowStyle" />
                        <PagerStyle CssClass="PagerStyle" />
                        <HeaderStyle CssClass="HeaderStyle" />
                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlDigitalAsset" runat="server">
            <HeaderTemplate>
                Digital Assets
            </HeaderTemplate>
            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer18" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div>
                    <div class="TabDescription" style="width: 75%;">
                        <p>
                            Digital assets could be serial numbers or other information that are used for downloadable or
                            <br />
                            software products.
                        </p>
                    </div>
                    <div class="NewButtonStyle">
                        <zn:LinkButton ID="btnAddDigitalAsset" runat="server"
                            ButtonType="Button" OnClick="BtnAddDigitalAsset_Click" Text="Add Digital Asset"
                            ButtonPriority="Primary" />
                    </div>
                </div>
                <div class="ClearBoth"></div>
                <uc1:Spacer ID="Spacer11" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                <asp:UpdatePanel ID="updPnlDigitalAsset" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="uxGridDigitalAsset" Width="100%" CssClass="Grid" CellPadding="4"
                            CaptionAlign="Left" GridLines="None" runat="server" AutoGenerateColumns="False"
                            AllowPaging="True" ForeColor="Black" OnPageIndexChanging="UxGridDigitalAsset_PageIndexChanging"
                            OnRowCommand="UxGridDigitalAsset_RowCommand"
                            OnRowDataBound="UxGridDigitalAsset_RowDataBound"
                            OnRowDeleting="UxGridDigitalAsset_RowDeleting" PageSize="25" EmptyDataText="No digital assets found for this Product.">
                            <FooterStyle CssClass="FooterStyle" />
                            <Columns>
                                <asp:BoundField DataField="DigitalAssetID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="DigitalAsset" HtmlEncode="false" HeaderText="Digital Asset"
                                    HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="70%" />
                                <asp:TemplateField HeaderText="Assigned" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <img alt="" id="Img2" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(IsDigitalAssetAssigned(DataBinder.Eval(Container.DataItem, "OrderLineItemId")))%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnRemoveDigitalAsset" CssClass="actionlink" Text="Remove &raquo"
                                            CommandArgument='<%# Eval("DigitalAssetID") %>' CommandName="Delete" runat="Server"
                                            Visible='<%# !(IsDigitalAssetAssigned(DataBinder.Eval(Container.DataItem, "OrderLineItemId")))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlProductSpecifications" runat="server">
            <HeaderTemplate>Specs</HeaderTemplate>
            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="spacerProdSpec" runat="server" />
                </div>
                <div align="right">
                    <asp:LinkButton CssClass="AddButton" ID="AddSpecItems" Text="Add Specification to this Product"
                        runat="server" OnClick="AddSpecItems_Click" />
                </div>
                <asp:UpdatePanel ID="uplnProdSpecs" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="uxSpecGrid" runat="server" ShowHeader="true" ShowFooter="true"
                            CaptionAlign="Left" ForeColor="Black" CellPadding="5" AutoGenerateColumns="false"
                            OnRowCommand="uxSpecGrid_RowCommand" OnPageIndexChanging="uxSpecGrid_PageIndexChanging"
                            CssClass="Grid" Width="100%" GridLines="None" AllowPaging="true" PageSize="5">
                            <Columns>
                                <asp:BoundField DataField="ProductSpecId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="SpecificationName" HeaderText="Specification Name"
                                    HeaderStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                <asp:BoundField DataField="SpecificationValue" HeaderText="Specification Value"
                                    HeaderStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="Delete" Text="Remove &raquo" CommandArgument='<%# Eval("ProductSpecId") %>'
                                            CommandName="RemoveItem" CssClass="Button" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No specification(s) found
                            </EmptyDataTemplate>
                            <RowStyle CssClass="RowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <FooterStyle CssClass="FooterStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
    </ajaxToolKit:TabContainer>
</asp:Content>

<%@ Page Language="C#" Title="Manage Products - Add Images" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Inventory.Products.AddView" CodeBehind="AddView.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc1" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/ImageUploader.ascx" TagName="UploadImage" TagPrefix="ZNode" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblHeading" runat="server" /><uc1:DemoMode ID="DemoMode1" runat="server"></uc1:DemoMode>
            </h1>
        </div>

        <div class="ClearBoth">
        </div>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <asp:Label ID="lblError" runat="server"></asp:Label>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="FormView">
            <div class="FieldStyle">
                Title<br />
                <small>Enter the title to be displayed for this product image. Leave blank for no title.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txttitle" runat="server" Columns="30" MaxLength="30"></asp:TextBox>
            </div>
            <%-- Zeon Custom UI--%>
            <div id="divImageType" runat="server">
                <div class="FieldStyle">
                    Image Type<br />
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ImageType" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <%-- Zeon Custom UI--%>
            <div class="ClearBoth" style="width: 100%">
                <span style="font-size: 10.5pt; font-weight: bold; color: #404040; font-family: Calibri, arial;">
                    <asp:Label ID="lblImage" runat="server">Product Image </asp:Label></span><span class="Asterix">*</span><br />
                <div id="ProductHint" runat="server" visible="false" style="width: 100%">
                    <small>Upload a suitable image for your product. Only JPG, GIF and PNG images are supported.
                        The file size should be less than 1.5 Meg. Your image will automatically be scaled
                        so it displays correctly in the catalog.</small>
                </div>
                <div id="SwatchHint" runat="server" visible="false" style="width: 100%">
                    <small>Upload a suitable image for your product swatch. Only JPG, GIF and PNG images
                        are supported. The file size should be less than 1.5 Meg. Your image will automatically
                        be cropped so it displays correctly in the catalog.</small>
                </div>
            </div>
            <div class="ClearBoth"></div>
            <div id="tblShowImage" runat="server" visible="false" style="margin: 10px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;">
                    <asp:Image ID="Image1" runat="server" />
                </div>
                <div class="LeftFloat" style="padding-left: 10px; margin-left: -1px; border-left: solid 1px #cccccc;">
                    <asp:Panel runat="server" ID="pnlShowOption">
                        <div>
                            Select an Option
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioProductCurrentImage" Text="Keep Current Image" runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioProductNewImage" Text="Upload New Image" runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductNewImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblProductDescription" runat="server" visible="false">
                        <div class="FieldStyle">
                            Select an Image
                        </div>
                        <div class="ValueStyle">
                            <ZNode:UploadImage ID="UploadProductImage" runat="server"></ZNode:UploadImage>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Image ALT Text<br />
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="lblProductImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                    <div id="AlternateThumbnail" runat="server" visible="false">
                        <%-- <div class="FieldStyle">
                        Product Image File Name</div>
                        <div class="ValueStyle">--%>
                        <asp:TextBox ID="txtAlternateThumbnail" Visible="false" runat="server" Columns="30" MaxLength="30"></asp:TextBox>
                        <%--   </div>
                   </div>--%>
                        <asp:Panel ID="ImagefileName" runat="server" Visible="false">
                            <div class="FieldStyle">
                                <asp:Label ID="lblImageName" runat="server"></asp:Label>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtimagename" runat="server"></asp:TextBox>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>

                <div class="ClearBoth">
                    <br />
                </div>
                <div class="FieldStyle">
                    Product Page
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID='VisibleInd' runat='server' Checked="true" Text="Display thumbnail on product page"></asp:CheckBox>
                </div>
                <div class="FieldStyle">
                    Category Page
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID='VisibleCategoryInd' runat='server' Text="Display thumbnail on category page" />
                </div>
                <div class="FieldStyle">
                    Display Order<span class="Asterix">*</span><br />
                    <small>An image with lower display order will be displayed first.</small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9">500</asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="DisplayOrder"
                        CssClass="Error" Display="Dynamic" ErrorMessage="Enter a Display Order"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="DisplayOrder"
                        CssClass="Error" Display="Dynamic" ErrorMessage="Enter a whole number." ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </div>
</asp:Content>

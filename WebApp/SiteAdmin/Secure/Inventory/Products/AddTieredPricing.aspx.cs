using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_catalog_product_add_TieredPricing class
    /// </summary>
    public partial class AddTieredPricing : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private int ProductTierId = 0;
        private string AssociateName = string.Empty;
        private string ViewLink = "~/SiteAdmin/Secure/Inventory/Products/View.aspx?itemid=";
        #endregion

        #region Protected Properties
        /// <summary>
        /// Gets the product name
        /// </summary>
        protected string GetProductName
        {
            get
            {
                ProductAdmin productAdmin = new ProductAdmin();
                Product product = productAdmin.GetByProductId(this.ItemId);

                if (product != null)
                {
                    return product.Name;
                }

                return string.Empty;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve Product Id from Query string
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (Request.Params["tierid"] != null)
            {
                this.ProductTierId = int.Parse(Request.Params["tierid"].ToString());
            }

            if (!IsPostBack)
            {
                this.Bind();

                if (this.ProductTierId > 0)
                {
                    // Edit Mode
                    lblHeading.Text = "Edit Pricing Tier - " + this.GetProductName;

                    this.BindData();
                }
                else
                {
                    lblHeading.Text = "Add Pricing Tier - " + this.GetProductName;
                }
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// Submit Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin productTierAdmin = new ProductAdmin();
            ProductTier _productTier = new ProductTier();

            if (this.ProductTierId > 0)
            {
                _productTier = productTierAdmin.GetByProductTierId(this.ProductTierId);
            }

            if (ddlProfiles.SelectedValue == "0")
            {
                _productTier.ProfileID = null;
            }
            else
            {
                _productTier.ProfileID = int.Parse(ddlProfiles.SelectedValue);
            }

            _productTier.TierStart = int.Parse(txtTierStart.Text.Trim());
            _productTier.TierEnd = int.Parse(txtTierEnd.Text.Trim());
            _productTier.Price = decimal.Parse(txtPrice.Text.Trim());

            // Set ProductId field
            _productTier.ProductID = this.ItemId;

            if (this.IsTieredPricingExists(_productTier))
            {
                lblError.Text = "Could not update the product Tiered pricing. Please try again.";
                return;
            }

            bool status = false;

            // If Edit mode, then update fields
            if (this.ProductTierId > 0)
            {
                this.AssociateName = "Edit Tiered Pricing - " + this.GetProductName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, this.GetProductName);

                status = productTierAdmin.UpdateProductTier(_productTier);
            }
            else
            {
                this.AssociateName = "Added Tiered Pricing - " + this.GetProductName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, this.GetProductName);

                status = productTierAdmin.AddProductTier(_productTier);
            }

            if (status)
            {
                Response.Redirect(this.ViewLink + this.ItemId + "&mode=tieredPricing");
            }
            else
            {
                lblError.Text = "Could not update the product Tiered pricing. Please try again.";
            }
        }

        /// <summary>
        /// Cancel Button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ViewLink + this.ItemId + "&mode=tieredPricing");
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Binds Profile dropdown list
        /// </summary>
        private void Bind()
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            ddlProfiles.DataSource = profileAdmin.GetAll();
            ddlProfiles.DataTextField = "Name";
            ddlProfiles.DataValueField = "ProfileId";
            ddlProfiles.DataBind();

            ListItem li = new ListItem("Apply to All Profiles", "0");
            ddlProfiles.Items.Insert(0, li);
        }

        /// <summary>
        /// Bind edit data fields
        /// </summary>
        private void BindData()
        {
            ProductAdmin productTierAdmin = new ProductAdmin();
            ProductTier productTier = productTierAdmin.GetByProductTierId(this.ProductTierId);

            if (productTier != null)
            {
                txtPrice.Text = productTier.Price.ToString("N");
                txtTierStart.Text = productTier.TierStart.ToString();
                txtTierEnd.Text = productTier.TierEnd.ToString();

                if (productTier.ProfileID.HasValue)
                {
                    ddlProfiles.SelectedValue = productTier.ProfileID.Value.ToString();
                }
            }
        }
        #endregion

        #region Helper Method
        /// <summary>
        /// To check whether the tiered pricing already exists for this product
        /// </summary>
        /// <param name="productTier">ProductTier instance</param>
        /// <returns>Returns a bool value Tiered Pricing exists or not</returns>
        private bool IsTieredPricingExists(ProductTier productTier)
        {
            TList<ProductTier> list;
            ProductTierService productTierService = new ProductTierService();
            ProductTierQuery query = new ProductTierQuery();
            query.Append(ProductTierColumn.ProductID, productTier.ProductID.ToString());
            if (this.ProductTierId > 0)
            {
                query.AppendNotEquals(ProductTierColumn.ProductTierID, productTier.ProductTierID.ToString());
            }

            TList<ProductTier> Mainlist = productTierService.Find(query.GetParameters());

            if (productTier.ProfileID.HasValue)
            {
                list = Mainlist.FindAll(delegate(ProductTier p) { return p.ProfileID == null || p.ProfileID == productTier.ProfileID; });
            }
            else
            {
                list = Mainlist;
            }

            if (list.Count > 0)
            {
                foreach (ProductTier listitem in list)
                {
                    if ((productTier.TierStart >= listitem.TierStart) && (productTier.TierStart <= listitem.TierEnd))
                    {
                        return true;
                    }

                    if ((productTier.TierEnd >= listitem.TierStart) && (productTier.TierEnd <= listitem.TierEnd))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        #endregion
    }
}
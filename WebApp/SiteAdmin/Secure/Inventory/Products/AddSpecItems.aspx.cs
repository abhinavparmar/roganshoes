﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace WebApp.SiteAdmin.Secure.Inventory.Products
{
    public partial class AddSpecItems : System.Web.UI.Page
    {
        # region Protected Member Variables
        
        protected int ItemID = 0; 
        protected int productSKUID = 0;
        
        protected string Associatename = string.Empty;
        protected string viewLink = "~/SiteAdmin/Secure/Inventory/Products/view.aspx?itemid=";
        protected string viewSKULink = "~/SiteAdmin/Secure/Inventory/Products/ViewSkuSpec.aspx?itemid=";

        # endregion

        # region Protected Properties
        
        /// <summary>
        /// Retrieves the product name
        /// </summary>
        protected string GetProductName
        {
            get
            {
                ProductAdmin productAdmin = new ProductAdmin();
                Product product = productAdmin.GetByProductId(ItemID);

                if (product != null)
                {
                    return product.Name;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Retrieves the SKU name
        /// </summary>
        protected string GetSKUName
        {
            get
            {
                SKUAdmin SkuAdmin = new SKUAdmin();
                SKU SpecName = new SKU();
                if (productSKUID > 0)
                {
                    SpecName = SkuAdmin.GetBySKUID(productSKUID);

                    if (!string.IsNullOrEmpty(SpecName.SKU))
                    {
                        return SpecName.SKU;
                    }
                }
                return string.Empty;
            }
        }

        #endregion

        #region Page Load
        
        /// <summary>
        /// Page Laod Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Retrieve Product Id from Query string
            if (Request.Params["itemid"] != null)
            {
                ItemID = int.Parse(Request.Params["itemid"].ToString());
            }
            
            //Retrieve SKU Id from Query string
            if (Request.Params["skuid"] != null)
            {
                productSKUID = int.Parse(Request.Params["skuid"].ToString());
            }
            
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(GetSKUName))
                {
                    lblHeading.Text = "Add Specification - " + GetProductName + " - " + GetSKUName;
                }
                else
                {
                    lblHeading.Text = "Add Specification - " + GetProductName;
                }
            }
        }

        #endregion

        # region General Events

        /// <summary>
        /// Submit Button Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ZeonProductSpecifications _prodSpec = new ZeonProductSpecifications();
            ZeonProductSpecificationsService SpecService = new ZeonProductSpecificationsService();

            _prodSpec.ProductID = ItemID;

            _prodSpec.SKUID = null;
            if (productSKUID > 0)
            {
                _prodSpec.SKUID = productSKUID;
            }
            _prodSpec.SpecificationName = txtSpecKey.Text.Trim();
            _prodSpec.SpecificationValue = txtSpecValue.Text.Trim();
            _prodSpec.DisplayOrder = 1;
            _prodSpec.Custom1 = string.Empty;
            _prodSpec.Custom2 = string.Empty;
            _prodSpec.Custom3 = string.Empty;
            _prodSpec.Custom4 = string.Empty;
            _prodSpec.Custom5 = string.Empty;

            bool status = false;

            Associatename = "Added product specification - " + GetProductName;
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, Associatename, GetProductName);

            int returnValue;
            ProductSpecificationsHelper specHelper = new ProductSpecificationsHelper();
            status = specHelper.InsertProductSpecification(_prodSpec, out returnValue);

            if (status)
            {
                if (productSKUID > 0)
                {
                    Response.Redirect(viewSKULink + ItemID + "&skuid=" + productSKUID);
                }
                else
                {
                    Response.Redirect(viewLink + ItemID + "&mode=Spec");
                }
            }
            else
            { 
                if (returnValue.Equals(0))
                { 
                    lblError.Text = "Specification name is already exist.";
                }
                else
                {
                    lblError.Text = "Could not add the product specification. Please try again.";
                }
            }
        }

        /// <summary>
        /// Cancel Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (productSKUID > 0)
            {
                Response.Redirect(viewSKULink + ItemID + "&skuid=" + productSKUID);
            }
            else
            {
                Response.Redirect(viewLink + ItemID + "&mode=Spec");
            }
        }
        
        #endregion
    }
}
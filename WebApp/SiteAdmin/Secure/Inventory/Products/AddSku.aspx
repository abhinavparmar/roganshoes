<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Inventory.Products.AddSku" ValidateRequest="false" Title="Manage Products - Add Sku"
    CodeBehind="AddSku.aspx.cs" %>

<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ProductTags" Src="ProductTags.ascx" %>
<%@ Register TagPrefix="znode" TagName="SkuProfileEffectiveDate" Src="ManageSKUEffectiveDate.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/ImageUploader.ascx" TagName="UploadImage"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <asp:UpdatePanel runat="server" ID="upSku">
        <ContentTemplate>
            <asp:HiddenField ID="existSKUvalue" runat="server" />
            <div class="FormView">
                <div class="LeftFloat" style="text-align: left">
                    <h1>
                        <asp:Label ID="lblHeading" runat="server" />
                        <uc1:DemoMode ID="DemoMode1" runat="server"></uc1:DemoMode>
                    </h1>
                </div>
                <div style="text-align: right; display: none;" runat="server" id="skuTopButtons">
                    <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                        runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
                    <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
                </div>
                <div class="ClearBoth" align="left">
                    <div>
                        <asp:Panel ID="pnlSKULocale" runat="server">
                            <div class="SubTitle1" style="display: none;">
                                Associated Locales &nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList runat="server" ID="ddlLocales"
                                    AutoPostBack="true" OnSelectedIndexChanged="DdlLocales_SelectedIndexChanged">
                                    <asp:ListItem>Select Language</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <hr />
                        </asp:Panel>
                    </div>
                </div>
                <br />
                <br />
                <div>
                    <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
                </div>
                <div class="FieldStyle">
                    SKU or Part#<span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="SKU" runat="server" Columns="30" MaxLength="100"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="SKU"
                        Display="Dynamic" ErrorMessage="* Enter a valid SKU" SetFocusOnError="True" CssClass="Error"></asp:RequiredFieldValidator>

                </div>
                <div class="FieldStyle">
                    Quantity On Hand<span class="Asterix">*</span><br />
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="Quantity" runat="server" Columns="5" MaxLength="4">1</asp:TextBox>&nbsp;
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="Quantity"
                        Display="Dynamic" ErrorMessage="* Enter a valid Quantity" CssClass="Error"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="Quantity"
                        Display="Dynamic" ErrorMessage="* Enter a number between 1-9999" MaximumValue="9999"
                        MinimumValue="0" Type="Integer" CssClass="Error"></asp:RangeValidator>
                </div>
                <div class="FieldStyle">
                    Re-Order Level
                </div>

                <div class="ValueStyle">
                    <asp:TextBox ID="ReOrder" runat="server" Columns="5" MaxLength="3"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="ReOrder"
                        Display="Dynamic" ErrorMessage="* Enter a valid ReOrder level. Only numbers are allowed"
                        SetFocusOnError="true" ValidationExpression="(^N/A$)|(^[-]?(\d+)(\.\d{0,3})?$)|(^[-]?(\d{1,3},(\d{3},)*\d{3}(\.\d{1,3})?|\d{1,3}(\.\d{1,3})?)$)"
                        CssClass="Error"></asp:RegularExpressionValidator>
                </div>
                <asp:Panel ID="pnlProductAttributes" runat="server">
                    <div class="FieldStyle">
                        Additional Weight
                    </div>

                    <div class="ValueStyle">
                        <asp:TextBox ID="WeightAdditional" runat="server" Columns="10" MaxLength="7"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.WeightUnit %>
                        <asp:CompareValidator ID="CompareValidator4" SetFocusOnError="true" runat="server"
                            ControlToValidate="WeightAdditional" Type="Double" Operator="DataTypeCheck" ErrorMessage="* Enter a valid additional weight. Only numbers are allowed"
                            CssClass="Error" Display="Dynamic" />
                    </div>
                    <div class="FieldStyle">
                        Retail Price<br />
                        <small>If you provide a retail price, this will override the product's retail price.</small>
                    </div>
                    <div class="ValueStyle" style="margin-left: -10px;">
                        <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                            ID="RetailPrice" runat="server" Columns="10" MaxLength="7"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="RetailPrice"
                            Type="Currency" Operator="DataTypeCheck" ErrorMessage="* Enter a valid override retail Price. Only numbers are allowed"
                            CssClass="Error" Display="Dynamic" />
                    </div>
                    <div class="FieldStyle">
                        Sale Price<br />
                        <small>If you provide a sale price, this will override the product's sale price.</small>
                    </div>
                    <div class="ValueStyle" style="margin-left: -10px;">
                        <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox ID="SalePrice" runat="server" Columns="10" MaxLength="7"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="SalePrice"
                            Type="Currency" Operator="DataTypeCheck" ErrorMessage="* Enter a valid override sale Price. Only numbers are allowed"
                            CssClass="Error" Display="Dynamic" />
                    </div>
                    <div class="FieldStyle">
                        Wholesale Price
                        <br />
                        <small>If you provide a wholesale price, this will override the product's wholesale
                            price.</small>
                    </div>
                    <div class="ValueStyle" style="margin-left: -10px;">
                        <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox ID="WholeSalePrice" runat="server" Columns="10" MaxLength="7"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="WholeSalePrice"
                            Type="Currency" Operator="DataTypeCheck" ErrorMessage="* Enter a valid override wholesale Price. Only numbers are allowed"
                            CssClass="Error" Display="Dynamic" />
                    </div>
                    <div class="FieldStyle">
                        Enable Inventory
                    </div>
                    <div class="ValueStyle">
                        <asp:CheckBox ID='VisibleInd' runat='server' Text="Enable this SKU in your store"
                            Checked="true"></asp:CheckBox>
                    </div>
                    <div class="FieldStyle" id="DivAttributes" runat="Server">
                        Product Attributes<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder>
                    </div>
                    <div class="FieldStyle">
                        Supplier
                        <br />
                        <small>Select the supplier who will ship this SKU.</small>
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlSupplier" runat="server" />
                    </div>
                    <div class="ClearBoth">
                        <ZNode:Spacer ID="Spacer3" SpacerHeight="7" SpacerWidth="3" runat="server"></ZNode:Spacer>
                    </div>
                    <div>
                        <uc1:ProductTags ID="ProductTags1" runat="server" ItemType="SkuId" />
                    </div>
                    <div class="ClearBoth">
                        <ZNode:Spacer ID="Spacer4" SpacerHeight="7" SpacerWidth="3" runat="server"></ZNode:Spacer>
                    </div>
                    <div>
                        <ZNode:SkuProfileEffectiveDate ID="uxSkuProfileEffectiveDate" runat="server" />
                    </div>
                    <div class="ClearBoth">
                        <ZNode:Spacer ID="Spacer5" SpacerHeight="7" SpacerWidth="3" runat="server"></ZNode:Spacer>
                    </div>
                    <h4 class="SubTitle">SKU Image</h4>
                    <small>Upload a suitable image for this SKU. Only JPG, GIF and PNG images are supported.
                        The file size should be less than 1.5 Meg. Your image will automatically be scaled
                        so it displays correctly.</small>
                    <div>
                        <ZNode:Spacer ID="Spacer1" SpacerHeight="7" SpacerWidth="3" runat="server"></ZNode:Spacer>
                    </div>
                    <div id="tblShowImage" visible="false" runat="server">
                        <div>
                            <asp:Image ID="SKUImage" runat="server" />
                        </div>
                        <div>
                            <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div>
                            <div class="FieldStyle">
                                Select an Option
                            </div>
                            <div class="ValueStyle">
                                <asp:RadioButton ID="RadioSKUCurrentImage" Text="Keep Current Image" runat="server"
                                    GroupName="SKU Image" AutoPostBack="True" OnCheckedChanged="RadioSKUCurrentImage_CheckedChanged"
                                    Checked="True" />
                                <asp:RadioButton ID="RadioSKUNewImage" Text="Upload New Image" runat="server" GroupName="SKU Image"
                                    AutoPostBack="True" OnCheckedChanged="RadioSKUNewImage_CheckedChanged" />
                                <asp:RadioButton ID="RadioSKUNoImage" Text="No Image" runat="server" GroupName="SKU Image"
                                    AutoPostBack="True" OnCheckedChanged="RadioSKUNoImage_CheckedChanged" />
                            </div>
                        </div>
                    </div>
                    <div class="ClearBoth">
                    </div>
                    <div id="tblSKUDescription" runat="server" visible="false">
                        <div>
                            <div>
                                <div>
                                    <asp:Label ID="lblSKUImageError" runat="server" CssClass="Error" ForeColor="Red"
                                        Text="" Visible="False"></asp:Label>
                                </div>
                                <div class="FieldStyle">
                                    Select Image:
                                </div>
                                <div class="ValueStyle">
                                    <ZNode:UploadImage ID="UploadSKUImage" runat="server"></ZNode:UploadImage>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        Image ALT Text
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox>
                    </div>
                    <%-- Zeon Custom UI For SKU Alternate Images:Start--%>
                    <div>
                       <h4 class="SubTitle">Alternate SKU  Image</h4>
                      <small>Upload a suitable alternate image for this SKU. Only JPG, GIF and PNG images are supported.</small>
                       <div>
                            <ZNode:Spacer ID="ucSpacerTop" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div class="ButtonStyle">
                            <zn:LinkButton ID="lbtnAddSKUImages" runat="server"
                                ButtonType="Button" OnClick="AddSKUView_Click" Text="Add Alternate SKU Image"
                                ButtonPriority="Primary" />
                        </div>
                        <div>
                            <ZNode:Spacer ID="ucSpacerTopGrid" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                        <asp:UpdatePanel ID="updPnlGridThumb" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="GridThumb" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                                    runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                                    CssClass="Grid" Width="100%" GridLines="None" OnRowCommand="GridThumb_RowCommand"
                                    AllowPaging="True" OnPageIndexChanging="GridThumb_PageIndexChanging" OnRowDeleting="GridThumb_RowDeleting"
                                    PageSize="5">
                                    <Columns>
                                        <asp:BoundField DataField="ZeonSKUImageID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                        <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <img id="SwapImage" alt="" src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                                    runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />
                                        <asp:TemplateField HeaderText="Product Page" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                                    runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Category Page" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <img id="Img2" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage").ToString()))%>'
                                                    runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton CssClass="actionlink" ID="EditProductView" Text="Edit &raquo" CommandArgument='<%# Eval("ZeonSKUImageID") %>'
                                                    CommandName="Edit" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:LinkButton ID="Delete" Text="Delete &raquo" CommandArgument='<%# Eval("ZeonSKUImageID") %>'
                                            CommandName="RemoveItem" CssClass="actionlink" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No alternate SKU  image found
                                    </EmptyDataTemplate>
                                    <RowStyle CssClass="RowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" />
                                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                    <FooterStyle CssClass="FooterStyle" />
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div><br />
                    <%-- Zeon Custom UI For SKU Alternate Images:Ends--%>
                    <asp:Panel ID="pnlRecurringBilling" runat="server" Visible="false">
                        <h4 class="SubTitle">Recurring Billing</h4>
                        <div class="FieldStyle">
                            Billing Period<span class="Asterix">*</span><br />
                            <small>Select a value for billing during this subscription period.</small>
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="ddlBillingPeriods" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DdlBillingPeriods_SelectedIndexChanged">
                                <asp:ListItem Text="Day(s)" Value="DAY"></asp:ListItem>
                                <asp:ListItem Text="Weekly" Value="WEEK"></asp:ListItem>
                                <asp:ListItem Text="Monthly" Value="MONTH"></asp:ListItem>
                                <asp:ListItem Text="Yearly" Value="YEAR"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="FieldStyle">
                            Billing Frequency<span class="Asterix">*</span><br />
                            <small>Select number of billing periods that make up one billing cycle.</small>
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="ddlBillingFrequency" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="FieldStyle">
                            Billing Cycles<span class="Asterix">*</span><br />
                            <small>Enter a number of billing cycles for payment period.</small>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtSubscrptionCycles" runat="server">1</asp:TextBox>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="txtSubscrptionCycles"
                                Display="Dynamic" ErrorMessage="* Enter a valid number" CssClass="Error"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtSubscrptionCycles"
                                Display="Dynamic" ErrorMessage="* Enter a number between 1-9999" MaximumValue="9999"
                                MinimumValue="0" Type="Integer" CssClass="Error"></asp:RangeValidator>
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <div class="ClearBoth">
                </div>
                <div class="ClearBoth">
                </div>
                <div runat="server" id="skuBottomButtons">
                    <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                        runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
                    <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                        onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
                </div>
            </div>
            <!-- hided this section as we did 2CO PTP method -->
            <div id="Div1" runat="server" visible="false">
                <div class="FieldStyle">
                    2CO Product ID<br />
                    <small>Leave blank if this store not using 2CO Payment Gateway.</small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="TwoCOProductID" Columns="50" runat="server"></asp:TextBox>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="uxSkuProfileProgress" runat="server" AssociatedUpdatePanelID="upSku"
        DisplayAfter="1">
        <ProgressTemplate>
            <div id="ajaxProgressBg">
            </div>
            <div id="ajaxProgress">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>

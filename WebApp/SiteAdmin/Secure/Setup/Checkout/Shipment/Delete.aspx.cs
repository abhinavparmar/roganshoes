using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Checkout.Shipment
{
    /// <summary>
    /// Represents the SiteAdmin -Admin_Secure_settings_ship_Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Private Variables
        private int ItemId;
        private string _ShippingOptionName = string.Empty;
        #endregion

        /// <summary>
        /// Gets or sets the Shipping Option Name
        /// </summary>
        public string ShippingOptionName
        {
            get
            {
                return this._ShippingOptionName;
            }

            set
            {
                this._ShippingOptionName = value;
            }
        }
        
        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the screen
        /// </summary>
        protected void BindData()
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            this.ShippingOptionName = Server.HtmlEncode(shipAdmin.GetShippingOptionById(this.ItemId).Description);
        }
        #endregion

        #region Events
        /// <summary>
        /// Page Load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            this.BindData();
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Shipment/default.aspx");
        }

        /// <summary>
        /// Delete button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            Shipping shipping = new Shipping();
            shipping.ShippingID = this.ItemId;

            bool retval = false;

            try
            {
                if (!this.IsShippingDeletable())
                {
                    lblMsg.Text = "Delete action could not be completed. At least one shipping option should be associated with 'All Profiles' or every profile should be associated with shipping option.";
                    return;
                }
               
                // Get Shipping Option Description
                ShippingAdmin shippingAdmin = new ShippingAdmin();
                Shipping shippingOption = shippingAdmin.GetShippingOptionById(this.ItemId);
                string ShipOptionName = shippingOption.Description;
                string AssociateName = "Delete Shipping Option - " + ShipOptionName;

                retval = shipAdmin.DeleteShippingOption(shipping);

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, ShipOptionName);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }
           
            if (!retval)
            {
                lblMsg.Text = "Error: Delete action could not be completed. You must delete all shipping rules on this option first. ";
                lblMsg.Text = lblMsg.Text + "You should also ensure that this shipping option is not currently referenced by an order or a product. ";
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Shipment/Default.aspx");
            }
        }

        /// <summary>
        /// Check whether the shipping option is deletable.
        /// </summary>
        /// <returns>Returns true </returns>
        protected bool IsShippingDeletable()
        {
            bool isDeletable = false;
            ShippingAdmin shipAdmin = new ShippingAdmin();
            TList<Shipping> shippingList = shipAdmin.GetAll();

            TList<Shipping> tempShippingList = null;

            // Check whether the some other shipping option has the current shipping option profile.
            if (this.IsSameProfileExist(this.ItemId))
            {
                isDeletable = true;
            }
            else
            {
                // Find all shiping list associated with "All Profiles" except current item
                tempShippingList = shippingList.FindAll(delegate(Shipping shipping) { return shipping.ProfileID == null && shipping.ShippingID != this.ItemId; });
                if (tempShippingList.Count > 0)
                {
                    // If "All Profile" shipping option found then deletable
                    isDeletable = true;
                }
                else
                {
                    // Get the "All Profile" shipping option
                    tempShippingList = shippingList.FindAll(delegate(Shipping shipping) { return shipping.ProfileID == null; });
                    int allProfilesCount = tempShippingList.Count;

                    // Get all profiles.
                    ProfileAdmin profileAdmin = new ProfileAdmin();
                    TList<Profile> profileList = profileAdmin.GetAll();

                    // Get the shipping option list except associated with All Profiles.
                    tempShippingList = shippingList.FindAll(delegate(Shipping shipping) { return shipping.ProfileID != null; });
                    TList<Shipping> profileShippingList = tempShippingList.FindAllDistinct("ProfileID");

                    if (profileShippingList.Count != profileList.Count)
                    {
                        isDeletable = false;
                    }
                    else
                    {
                        isDeletable = allProfilesCount > 0 ? true : false;
                    }
                }
            }

            return isDeletable;
        }

        /// <summary>
        /// Check whether the same profile Id associated with some other shipping option. 
        /// </summary>
        /// <param name="shippingId">Shipping Id to check. </param>
        /// <returns>Returns True if associated, else False.</returns>
        private bool IsSameProfileExist(int shippingId)
        {
            // Get the shipping object
            ShippingAdmin shipAdmin = new ShippingAdmin();
            Shipping shipping = shipAdmin.GetShippingOptionById(shippingId);

            // Get the profile Id
            TList<Shipping> shippingList = shipAdmin.GetAll();
            TList<Shipping> resultShippingList = shippingList.FindAll(delegate(Shipping s) { return s.ProfileID == shipping.ProfileID; });

            // If more than one shipping object with same profile Id then return true else false.
            return resultShippingList.Count > 1 ? true : false;
        }

        #endregion
    }
}
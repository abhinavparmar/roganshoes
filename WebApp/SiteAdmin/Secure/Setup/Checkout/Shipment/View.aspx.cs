using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Checkout.Shipment
{
    /// <summary>
    /// Represents the SiteAdmin Admin_Secure_settings_shipping_View class
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Private Variables
        private int ItemId;
        private string ListLink = "~/SiteAdmin/Secure/Setup/Checkout/Shipment/default.aspx";
        private string AddRuleLink = "~/SiteAdmin/Secure/Setup/Checkout/Shipment/addrule.aspx";
        private string EditOptionLink = "~/SiteAdmin/Secure/Setup/Checkout/Shipment/add.aspx";
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields 
        /// </summary>
        protected void BindData()
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();

            if (this.ItemId > 0)
            {
                Shipping shippingOption = shipAdmin.GetShippingOptionById(this.ItemId);

                lblShippingType.Text = Server.HtmlEncode(shipAdmin.GetShippingTypeName(shippingOption.ShippingTypeID));
                if (shippingOption.ProfileID.HasValue)
                {
                    lblProfileName.Text = shipAdmin.GetProfileName((int)shippingOption.ProfileID);
                }
                else
                {
                    lblProfileName.Text = "All Profiles";
                }

                lblDescription.Text = Server.HtmlEncode(shippingOption.Description);
                lblShippingCode.Text = Server.HtmlEncode(shippingOption.ShippingCode);

                if (shippingOption.HandlingCharge > 0)
                {
                    lblHandlingCharge.Text = shippingOption.HandlingCharge.ToString("N2");
                }
                else
                {
                    lblHandlingCharge.Text = "0.00";
                }

                if (shippingOption.DestinationCountryCode != null)
                {
                    if (shippingOption.DestinationCountryCode.Length > 0)
                    {
                        lblDestinationCountry.Text = shippingOption.DestinationCountryCode;
                    }
                }
                else
                {
                    lblDestinationCountry.Text = "All Countries";
                }

                imgActive.Src = ZNode.Libraries.Admin.Helper.GetCheckMark((bool)shippingOption.ActiveInd);
                lblDisplayOrder.Text = shippingOption.DisplayOrder.ToString();

                if (shippingOption.ShippingTypeID == 1)
                {
                    pnlShippingRuletypes.Visible = true;
                }
            }
        }
      
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (Page.IsPostBack == false)
            {
                // Bind data to the fields on the page
                this.BindData();
                this.BindGridData();
            }
        }

        /// <summary>
        /// Edit Shipping Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditShipping_Click(object sender, EventArgs e)
        {
            this.EditOptionLink = this.EditOptionLink + "?itemid=" + this.ItemId.ToString();
            Response.Redirect(this.EditOptionLink);
        }

        /// <summary>
        /// Add Rule Button CLick Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddRule_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddRuleLink + "?sid=" + this.ItemId.ToString());
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    this.AddRuleLink = this.AddRuleLink + "?itemid=" + Id + "&sid=" + this.ItemId.ToString();
                    Response.Redirect(this.AddRuleLink);
                }
                else if (e.CommandName == "Delete")
                {
                    ShippingAdmin shipAdmin = new ShippingAdmin();
                    ShippingRule shipRule = new ShippingRule();
                    shipRule.ShippingRuleID = int.Parse(Id);

                    // Get Shipping Rule Name
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                    string ShipRuleName = row.Cells[1].Text;

                    // Get Shipping Option Description
                    ShippingAdmin shippingAdmin = new ShippingAdmin();
                    Shipping shippingOption = shippingAdmin.GetShippingOptionById(this.ItemId);
                    string ShipOptionName = shippingOption.Description;
                    string AssociateName = "Delete Shipping Rule  " + ShipRuleName + " - " + ShipOptionName;

                    shipAdmin.DeleteShippingRule(shipRule);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, ShipOptionName);
                }
            }
        }

        #endregion
        #region Private Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();

            uxGrid.DataSource = shipAdmin.GetShippingRules(this.ItemId);
            uxGrid.DataBind();
        }

        #endregion
    }
}
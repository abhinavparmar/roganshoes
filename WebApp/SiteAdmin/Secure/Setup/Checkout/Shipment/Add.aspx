<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Setup.Checkout.Shipment.Add" Title="Manage Shipping - Add" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="LeftFloat" style="width: 70%; text-align: left">
        <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
            <uc2:DemoMode ID="DemoMode1" runat="server" />
        </h1>
    </div>
    <div style="text-align: right">
        <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
        <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div class="ClearBoth">
        <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
    </div>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div class="FormView Size350">
        <h4 class="SubTitle">General Settings</h4>
        <div class="FieldStyle">
            Select Profile<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="lstProfile" runat="server">
            </asp:DropDownList>
        </div>
        <div class="FieldStyle">
            Select Shipping Type<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="lstShippingType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="LstShippingType_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <asp:Panel ID="pnlShippingServiceCodes" runat="server" Visible="false">
            <div class="FieldStyle">
                Select a service<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstShippingServiceCodes" runat="server" AutoPostBack="false">
                </asp:DropDownList>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlShippingOptions" runat="server">
            <div class="FieldStyle">
                Display Name (ex: FedEx Overnight)<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDescription" runat="server" MaxLength="50" Columns="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDescription"
                    Display="Dynamic" CssClass="Error" ErrorMessage="* Enter Shipping option name" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
              Internal Code (ex: "FDX")<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtShippingCode" runat="server" MaxLength="10" Columns="23"></asp:TextBox><asp:RequiredFieldValidator
                    ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtShippingCode"
                    Display="Dynamic" CssClass="Error" ErrorMessage="* Enter Shipping code" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </div>
        </asp:Panel>
        <div class="FieldStyle">
            Handling Charge (Optional)
        </div>
        <div class="ValueStyle">
            <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;
            <asp:TextBox ID="txtHandlingCharge" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHandlingCharge"
                Display="Dynamic" CssClass="Error" ErrorMessage="* Enter handling charge" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:CompareValidator
                    ID="CompareValidator1" runat="server" ControlToValidate="txtHandlingCharge" Type="Currency"
                    Operator="DataTypeCheck" />
        </div>
        <asp:Panel ID="pnlDestinationCountry" runat="server">
            <div class="FieldStyle">
                Destination Country (Optional)<br />
                <small>Select a country if you want to restrict this shipping option to a specific country</small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstCountries" runat="server">
                </asp:DropDownList>
            </div>
        </asp:Panel>
        <div>
            <uc1:Spacer ID="Spacer8" runat="server" SpacerHeight="15" SpacerWidth="3" />
        </div>
        <h4 class="SubTitle">Display Settings</h4>
        <div class="FieldStyle">
            Display Setting
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkActiveInd" runat="server" Checked="true" Text="Enable this shipping option during checkout" />
        </div>
        <div class="FieldStyle">
            Display Order<span class="Asterix">*</span><br />
            <small>Enter a number. Items with a lower number are displayed first on the page.</small>
        </div>
        <div class="HintStyle">
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" CssClass="Error" runat="server" Display="Dynamic"
                ErrorMessage="* Enter Display Order" ControlToValidate="txtDisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtDisplayOrder"
                Display="Dynamic" CssClass="Error" ErrorMessage="Enter Whole number." MaximumValue="999999999"
                MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="ClearBoth">
            <uc1:Spacer ID="Spacer2" runat="server" SpacerHeight="15" SpacerWidth="3" />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>

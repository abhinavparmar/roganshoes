using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Checkout.Shipment
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_settings_ship_Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Private Variables
        private int ItemId;
        private string AddRuleLink = "~/SiteAdmin/Secure/Setup/Checkout/Shipment/addrule.aspx";
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (Page.IsPostBack == false)
            {
                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit Shipping Option";
                }
                else
                {
                    lblTitle.Text = "Add a Shipping Option";
                }

                txtHandlingCharge.Text = 0.0.ToString("N");
                CompareValidator1.Text = "You must enter a valid handling charge (ex: " + 123.45.ToString("N") + " )";

                // Bind data to the fields on the page
                this.BindData();
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields 
        /// </summary>
        protected void BindData()
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();

            // GET LIST DATA
            // Get shipping types
            TList<ShippingType> shippingTypeList = shipAdmin.GetShippingTypes();
            lstShippingType.DataSource = shippingTypeList.FindAll(ShippingTypeColumn.IsActive, true);
            lstShippingType.DataTextField = "Name";
            lstShippingType.DataValueField = "ShippingTypeID";
            lstShippingType.DataBind();
            lstShippingType.SelectedIndex = 0;

            // Bind countries
            this.BindCountry();

            // Get profiles
            lstProfile.DataSource = UserStoreAccess.CheckProfileAccess(settingsAdmin.GetProfiles());
            lstProfile.DataTextField = "Name";
            lstProfile.DataValueField = "ProfileID";
            lstProfile.DataBind();
            lstProfile.Items.Insert(0, new ListItem("All Profiles", "-1"));
            lstProfile.SelectedItem.Value = "-1";

            if (this.ItemId > 0)
            {
                Shipping shippingOption = shipAdmin.GetShippingOptionById(this.ItemId);

                if (shippingOption.ProfileID.HasValue)
                {
                    lstProfile.SelectedValue = shippingOption.ProfileID.Value.ToString();
                }

                lstShippingType.SelectedValue = shippingOption.ShippingTypeID.ToString();

                lstShippingType.Enabled = false;

                // If UPS Shipping Option is Selected
                if (lstShippingType.SelectedValue == "2")
                {
                    // Bind UPS Service codes
                    this.BindShippingServiceCodes();
                    lstShippingServiceCodes.SelectedValue = shippingOption.ShippingCode;

                    pnlShippingServiceCodes.Visible = true;
                    pnlShippingOptions.Visible = false;
                    pnlDestinationCountry.Visible = false;
                }
                else if (lstShippingType.SelectedValue == "3")
                {
                    // If FedEx Shipping Option is Selected
                    // Bind FedEx Service codes
                    this.BindShippingServiceCodes();
                    lstShippingServiceCodes.SelectedValue = shippingOption.ShippingCode;

                    pnlShippingServiceCodes.Visible = true;
                    pnlShippingOptions.Visible = false;
                    //pnlDestinationCountry.Visible = false;
                    pnlDestinationCountry.Visible = true;
                }
                else if (lstShippingType.SelectedValue == "4") //Zeon Customization :: Set USPS Option On Drop Down :: Start
                {
                    // If USPS Shipping Option is Selected
                    // Bind USPS Service codes
                    this.BindShippingServiceCodes();
                    lstShippingServiceCodes.SelectedValue = shippingOption.ShippingCode;

                    pnlShippingServiceCodes.Visible = true;
                    pnlShippingOptions.Visible = false;
                    //pnlDestinationCountry.Visible = false;
                    pnlDestinationCountry.Visible = true;
                }//Zeon Customization :: Set USPS Option On Drop Down :: End
                else
                {
                    txtDescription.Text = Server.HtmlDecode(shippingOption.Description);
                    txtShippingCode.Text = Server.HtmlDecode(shippingOption.ShippingCode);
                }

                if (shippingOption.HandlingCharge > 0)
                {
                    txtHandlingCharge.Text = shippingOption.HandlingCharge.ToString("N2");
                }
                else
                {
                    txtHandlingCharge.Text = "0.00";
                }

                if (shippingOption.DestinationCountryCode != null)
                {
                    ListItem blistItem = lstCountries.Items.FindByValue(shippingOption.DestinationCountryCode);
                    if (blistItem != null)
                    {
                        lstCountries.SelectedIndex = lstCountries.Items.IndexOf(blistItem);
                    }
                }

                chkActiveInd.Checked = (bool)shippingOption.ActiveInd;
                txtDisplayOrder.Text = shippingOption.DisplayOrder.ToString();
            }
        }

        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
            ShippingAdmin shipAdmin = new ShippingAdmin();
            Shipping shipOption = new Shipping();

            if (!this.IsShippingSetupValid())
            {
                lblMsg.Text = "At least one shipping option should be enabled.";
                return;
            }

            // If edit mode then retrieve data first
            if (this.ItemId > 0)
            {
                shipOption = shipAdmin.GetShippingOptionById(this.ItemId);
            }

            // Set values
            shipOption.ActiveInd = chkActiveInd.Checked;

            // If UPS Shipping type is selected
            if (lstShippingType.SelectedValue == "2")
            {
                shipOption.ShippingCode = lstShippingServiceCodes.SelectedItem.Value;
                shipOption.Description = lstShippingServiceCodes.SelectedItem.Text;
            }
            else if (lstShippingType.SelectedValue == "3")
            {
                // If FedEx Shipping type is selected
                shipOption.ShippingCode = lstShippingServiceCodes.SelectedItem.Value;
                shipOption.Description = lstShippingServiceCodes.SelectedItem.Text;
            }
            else if (lstShippingType.SelectedValue == "4")//Zeon Customization :: Set USPS Option On Drop Down :: Start
            {
                // If USPS Shipping type is selected
                shipOption.ShippingCode = lstShippingServiceCodes.SelectedItem.Value;
                shipOption.Description = lstShippingServiceCodes.SelectedItem.Text;
            }//Zeon Customization :: Set USPS Option On Drop Down :: END
            else
            {
                shipOption.ShippingCode = Server.HtmlDecode(txtShippingCode.Text);
                shipOption.Description = Server.HtmlDecode(txtDescription.Text);
            }

            if (lstCountries.SelectedValue.Equals("*"))
            {
                shipOption.DestinationCountryCode = null;
            }
            else
            {
                shipOption.DestinationCountryCode = lstCountries.SelectedValue;
            }

            shipOption.DisplayOrder = int.Parse(txtDisplayOrder.Text);

            // Profile settings
            if (lstProfile.SelectedValue != "-1")
            {
                shipOption.ProfileID = int.Parse(lstProfile.SelectedValue);
            }
            else
            {
                shipOption.ProfileID = null;
            }

            shipOption.ShippingTypeID = int.Parse(lstShippingType.SelectedValue);

            if (txtHandlingCharge.Text.Length > 0)
            {
                shipOption.HandlingCharge = decimal.Parse(txtHandlingCharge.Text);
            }

            bool retval = false;
            bool isUpdate = false;
            if (this.ItemId > 0)
            {
                retval = shipAdmin.UpdateShippingOption(shipOption);
                isUpdate = true;
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Shipping Option - " + txtDescription.Text, txtDescription.Text);
            }
            else
            {
                retval = shipAdmin.AddShippingOption(shipOption);
                this.ItemId = shipOption.ShippingID;

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Shipping Option - " + txtDescription.Text, txtDescription.Text);
            }

            if (retval)
            {

                // Redirect to main page
                if (lstShippingType.SelectedValue == "1")
                {
                    if (isUpdate)
                    {
                        Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Shipment/view.aspx?itemid=" + this.ItemId);
                    }
                    else
                    {
                        Response.Redirect(this.AddRuleLink + "?sid=" + this.ItemId.ToString());
                    }
                }
                else
                {
                    Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Shipment/view.aspx?itemid=" + this.ItemId);
                }
            }

            else
            {
                // Display error message
                lblMsg.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Check whether the shipping option setup is valid. Check at least one shipping option enabled.
        /// </summary>
        /// <returns>Returns True if at least one shipping option enabled otherwise False.</returns>
        protected bool IsShippingSetupValid()
        {
            bool isShippingOptionValid = true;
            ShippingAdmin shipAdmin = new ShippingAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList = shipAdmin.GetAll();
            TList<ZNode.Libraries.DataAccess.Entities.Shipping> tempShippingList = null;

            tempShippingList = shippingList.FindAll(delegate(ZNode.Libraries.DataAccess.Entities.Shipping shipping) { return shipping.ActiveInd == false; });
            if (tempShippingList.Count == (shippingList.Count - 1) && !chkActiveInd.Checked)
            {
                isShippingOptionValid = false;
            }

            return isShippingOptionValid;
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.ItemId > 0)
            {
                Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Shipment/view.aspx?itemid=" + this.ItemId);
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Shipment/default.aspx");
            }
        }

        /// <summary>
        /// Shipping Type Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstShippingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstShippingType.SelectedValue == "2")
            {
                this.BindShippingServiceCodes();

                pnlShippingServiceCodes.Visible = true;
                pnlShippingOptions.Visible = false;
                pnlDestinationCountry.Visible = false;
            }
            else if (lstShippingType.SelectedValue == "3")
            {
                this.BindShippingServiceCodes();

                pnlShippingServiceCodes.Visible = true;
                pnlShippingOptions.Visible = false;
                //pnlDestinationCountry.Visible = false;
                pnlDestinationCountry.Visible = true;
            }
            else if (lstShippingType.SelectedValue == "4")//Zeon Customization :: Set USPS Option On Drop Down :: Start
            {
                this.BindShippingServiceCodes();
                pnlShippingServiceCodes.Visible = true;
                pnlShippingOptions.Visible = false;
                pnlDestinationCountry.Visible = true;
            }//Zeon Customization :: Set USPS Option On Drop Down :: End
            else
            {
                pnlDestinationCountry.Visible = true;
                pnlShippingServiceCodes.Visible = false;
                pnlShippingOptions.Visible = true;
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Country> countries = shipAdmin.GetDestinationCountries();

            lstCountries.DataSource = countries;
            lstCountries.DataTextField = "Name";
            lstCountries.DataValueField = "Code";
            lstCountries.DataBind();
            lstCountries.SelectedValue = "*";
        }

        /// <summary>
        /// Binds Shipping Servic code list
        /// </summary>
        private void BindShippingServiceCodes()
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            DataSet ds = shipAdmin.GetShippingServiceCodes(int.Parse(lstShippingType.SelectedValue));

            // If FedEx shipping type is selected
            if (lstShippingType.SelectedValue == "3")
            {
                // Reset dropdownlist items
                lstShippingServiceCodes.Items.Clear();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string description = dr["Description"].ToString();

                    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("&reg;");
                    description = regex.Replace(description, "�");
                    ListItem li = new ListItem(description, dr["Code"].ToString());
                    lstShippingServiceCodes.Items.Add(li);
                }
            }
            else
            {
                lstShippingServiceCodes.DataSource = ds;
                lstShippingServiceCodes.DataTextField = "Description";
                lstShippingServiceCodes.DataValueField = "code";
                lstShippingServiceCodes.DataBind();
            }
        }

        /// <summary>
        /// Check whether the same profile Id associated with some other shipping option. 
        /// </summary>
        /// <param name="shippingId">Shipping Id to check. </param>
        /// <returns>Returns True if associated, else False.</returns>
        private bool IsSameProfileExist(int shippingId)
        {
            // Get the shipping object
            ShippingAdmin shipAdmin = new ShippingAdmin();
            ZNode.Libraries.DataAccess.Entities.Shipping shipping = shipAdmin.GetShippingOptionById(shippingId);

            // Get the profile Id
            TList<Shipping> shippingList = shipAdmin.GetAll();
            TList<Shipping> resultShippingList = shippingList.FindAll(delegate(Shipping s) { return s.ProfileID == shipping.ProfileID; });

            // If more than one shipping object with same profile Id then return true else false.
            return resultShippingList.Count > 1 ? true : false;
        }

        #endregion
    }
}
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Checkout.Payments
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Setup.Storefront.Payments - Add  class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (Page.IsPostBack == false)
            {
                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit Payment Option";
                }
                else
                {
                    lblTitle.Text = "Add a Payment Option";
                }

                // Bind data to the fields on the page
                this.BindData();
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields 
        /// </summary>
        protected void BindData()
        {
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();

            txtDisplayOrder.Text = "1";

            // GET LIST DATA
            // Get payment types
            lstPaymentType.DataSource = settingsAdmin.GetPaymentTypes();
            lstPaymentType.DataTextField = "Name";
            lstPaymentType.DataValueField = "PaymentTypeID";
            lstPaymentType.DataBind();
            lstPaymentType.SelectedIndex = 0;

            // Get gateways
            lstGateway.DataSource = settingsAdmin.GetGateways();
            lstGateway.DataTextField = "GatewayName";
            lstGateway.DataValueField = "GatewayTypeID";
            lstGateway.DataBind();
            lstGateway.SelectedIndex = 0;

            // Get profiles
            lstProfile.DataSource = UserStoreAccess.CheckProfileAccess(settingsAdmin.GetProfiles());
            lstProfile.DataTextField = "Name";
            lstProfile.DataValueField = "ProfileID";
            lstProfile.DataBind();
            lstProfile.Items.Insert(0, new ListItem("All Profiles", "-1"));
            lstProfile.SelectedValue = "-1";

            OrderAdmin orderadmin = new OrderAdmin();
            int ordersCount = orderadmin.GetTotalOrderItemsByPaymentId(ItemId);
            if (ordersCount > 0)
            {
                lstGateway.Enabled = false;
                lstPaymentType.Enabled = false;
                lstProfile.Enabled = false;
            }

            try
            {
                if (this.ItemId > 0)
                {
                    // Get payment setting
                    PaymentSetting paymentSetting = settingsAdmin.GetPaymentSettingByID(this.ItemId);

                    txtDisplayOrder.Text = paymentSetting.DisplayOrder.ToString();

                    if (paymentSetting.ProfileID.HasValue)
                    {
                        lstProfile.SelectedValue = paymentSetting.ProfileID.Value.ToString();
                    }

                    lstPaymentType.SelectedValue = paymentSetting.PaymentTypeID.ToString();
                    chkActiveInd.Checked = paymentSetting.ActiveInd;

                    if (paymentSetting.PaymentTypeID == 0)
                    {
                        pnlCreditCard.Visible = true;

                        ZNodeEncryption encrypt = new ZNodeEncryption();

                        txtGatewayUserName.Text = encrypt.DecryptData(paymentSetting.GatewayUsername);
                        if (paymentSetting.IsRMACompatible.HasValue)
                            chkRMA.Checked = paymentSetting.IsRMACompatible.Value;
                        // If authorize.net
                        if (paymentSetting.GatewayTypeID == 1 || paymentSetting.GatewayTypeID == 12)
                        {
                            txtTransactionKey.Text = encrypt.DecryptData(paymentSetting.TransactionKey);
                            lblTransactionKey.Text = "Transaction Key (Authorize.Net only)";
                            pnlAuthorizeNet.Visible = true;
                            pnlPassword.Visible = false;
                            chkTestMode.Checked = paymentSetting.TestMode;
                            chkPreAuthorize.Checked = paymentSetting.PreAuthorize;
                            pnlGatewayOptions.Visible = true;
                        }
                        else if (paymentSetting.GatewayTypeID == 2)
                        {
                            // If verisign payflow pro
                            // Vendor
                            txtVendor.Text = paymentSetting.Vendor;
                            txtGatewayPassword.Text = encrypt.DecryptData(paymentSetting.GatewayPassword);
                            txtPartner.Text = paymentSetting.Partner;
                            pnlPassword.Visible = true;
                            pnlVerisignGateway.Visible = true;
                            chkTestMode.Checked = paymentSetting.TestMode;
                            pnlGatewayOptions.Visible = true;
                        }
                        else if (paymentSetting.GatewayTypeID == 5)
                        {
                            // If Nova Gateway
                            txtTransactionKey.Text = encrypt.DecryptData(paymentSetting.TransactionKey);
                            lblTransactionKey.Text = "Pin Number (Nova gateway Only)";
                            pnlAuthorizeNet.Visible = true;
                            pnlPassword.Visible = true;
                            txtGatewayPassword.Text = encrypt.DecryptData(paymentSetting.GatewayPassword);
                            chkTestMode.Checked = paymentSetting.TestMode;
                        }
                        else if (paymentSetting.GatewayTypeID == 6)
                        {
                            // If NSoftware Gateway
                            txtTransactionKey.Text = paymentSetting.TransactionKey;
                            txtGatewayPassword.Text = encrypt.DecryptData(paymentSetting.GatewayPassword);
                            lblTransactionKey.Text = "Transaction key";
                            pnlAuthorizeNet.Visible = true;
                            pnlPassword.Visible = true;
                            chkTestMode.Checked = paymentSetting.TestMode;
                        }
                        else if (paymentSetting.GatewayTypeID == 9)
                        {
                            pnlLogin.Visible = false;
                            pnlPassword.Visible = false;
                            chkTestMode.Checked = paymentSetting.TestMode;
                        }
                        else if (paymentSetting.GatewayTypeID == 10)
                        {
                            // If World Pay Gateway
                            txtGatewayPassword.Text = encrypt.DecryptData(paymentSetting.GatewayPassword);
                            txtTransactionKey.Text = encrypt.DecryptData(paymentSetting.TransactionKey);
                            lblGatewayUserName.Text = "Merchant Code";
                            lblGatewaypassword.Text = "Authorization Password";
                            lblTransactionKey.Text = "World Pay Installation Id";
                            pnlAuthorizeNet.Visible = true;
                            pnlPassword.Visible = true;
                            chkTestMode.Checked = paymentSetting.TestMode;
                        }
                        else if (paymentSetting.GatewayTypeID == 11)
                        {
                            // If CyberSource
                            pnlLogin.Visible = false;
                            pnlAuthorizeNet.Visible = false;
                            pnlPassword.Visible = false;
                            pnlVerisignGateway.Visible = false;
                            pnlGatewayOptions.Visible = true;
                            pnlTestMode.Visible = true;
                            chkTestMode.Checked = paymentSetting.TestMode;
                            chkPreAuthorize.Checked = paymentSetting.PreAuthorize;
                        }
                        else
                        {
                            pnlAuthorizeNet.Visible = false;
                            pnlPassword.Visible = true;
                            txtGatewayPassword.Text = encrypt.DecryptData(paymentSetting.GatewayPassword);
                            txtTransactionKey.Text = string.Empty;
                            chkTestMode.Checked = paymentSetting.TestMode;
                        }

                        lstGateway.SelectedValue = paymentSetting.GatewayTypeID.ToString();
                        chkEnableAmex.Checked = (bool)paymentSetting.EnableAmex;
                        chkEnableDiscover.Checked = (bool)paymentSetting.EnableDiscover;
                        chkEnableMasterCard.Checked = (bool)paymentSetting.EnableMasterCard;
                        chkEnableVisa.Checked = (bool)paymentSetting.EnableVisa;

                        // Hide 2CO panel
                        pnl2COGateway.Visible = false;
                    }
                    else if (paymentSetting.PaymentTypeID == 2)
                    {
                        pnlCreditCard.Visible = true;

                        ZNodeEncryption encrypt = new ZNodeEncryption();

                        txtGatewayUserName.Text = encrypt.DecryptData(paymentSetting.GatewayUsername);
                        txtTransactionKey.Text = paymentSetting.TransactionKey;
                        txtGatewayPassword.Text = encrypt.DecryptData(paymentSetting.GatewayPassword);
                        lblTransactionKey.Text = "API Signature (PayPal only)";
                        pnlAuthorizeNet.Visible = true;
                        pnlPassword.Visible = true;
                        pnlGatewayList.Visible = false;
                        pnlCreditCardOptions.Visible = false;
                        chkTestMode.Checked = paymentSetting.TestMode;

                        // Hide 2CO panel
                        pnl2COGateway.Visible = false;
                    }
                    else if (paymentSetting.PaymentTypeID == 3)
                    {
                        // If Google Checkout is Selected
                        ZNodeEncryption encrypt = new ZNodeEncryption();

                        txtGatewayUserName.Text = encrypt.DecryptData(paymentSetting.GatewayUsername);
                        txtGatewayPassword.Text = encrypt.DecryptData(paymentSetting.GatewayPassword);
                        chkTestMode.Checked = paymentSetting.TestMode;

                        // Hide CreditCard Options
                        pnlCreditCardOptions.Visible = false;

                        // Hide Gateway dropdownlist
                        pnlGatewayList.Visible = false;

                        // Enable settings
                        pnlCreditCard.Visible = true;

                        // Disable TransactionKey field
                        pnlAuthorizeNet.Visible = false;

                        // Enable PassWord field
                        pnlPassword.Visible = true;

                        // Hide 2CO panel
                        pnl2COGateway.Visible = false;
                    }
                    else if (paymentSetting.PaymentTypeID == 5)
                    {
                        // If 2CO is Selected
                        ZNodeEncryption encrypt = new ZNodeEncryption();

                        // Vendor ID.
                        txtGatewayUserName.Text = encrypt.DecryptData(paymentSetting.GatewayUsername);

                        // Secret word.
                        txtGatewayPassword.Attributes.Add("value", encrypt.DecryptData(paymentSetting.GatewayPassword));

                        // Sandbox.
                        chkTestMode.Checked = paymentSetting.TestMode;

                        // Additional Fee
                        txtAdditionalFee.Text = encrypt.DecryptData(paymentSetting.Vendor);

                        googleNote.Visible = false;

                        // Hide CreditCard Options
                        pnlCreditCardOptions.Visible = false;

                        // Hide Gateway dropdownlist
                        pnlGatewayList.Visible = false;

                        // Enable settings
                        pnlCreditCard.Visible = true;

                        // Disable TransactionKey field
                        pnlAuthorizeNet.Visible = false;

                        // Enable PassWord field
                        pnlPassword.Visible = true;

                        // Enable Additional Fee.
                        pnl2COGateway.Visible = true;

                        lblGatewayUserName.Text = "2CO Vendor ID";
                        lblGatewaypassword.Text = "Secret Word to use MD5 Verification";
                    }
                    else
                    {
                        pnlCreditCard.Visible = false;
                    }
                }
                else
                {
                    // Enable credit card option by default
                    pnlCreditCard.Visible = true;

                    lstGateway.SelectedValue = "1";
                    this.BindPanels();
                }
            }
            catch
            {
                // Ignore
            }
        }

        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
            PaymentSetting paymentSetting = new PaymentSetting();

            // If edit mode then retrieve data first
            if (this.ItemId > 0)
            {
                paymentSetting = settingsAdmin.GetPaymentSettingByID(this.ItemId);

                bool settingExists = this.PaymentSettingExists();

                if (settingExists)
                {
                    lblMsg.Text = "This Payment Option already exists for this Profile. Please select a different Payment Option.";
                    return;
                }
            }

            // Set values based on user input
            paymentSetting.ActiveInd = chkActiveInd.Checked;
            paymentSetting.PaymentTypeID = int.Parse(lstPaymentType.SelectedValue);
            if (lstProfile.SelectedValue == "-1")
            {
                // If All profiles is selected
                paymentSetting.ProfileID = null;
            }
            else
            {
                paymentSetting.ProfileID = int.Parse(lstProfile.SelectedValue);
            }

            paymentSetting.DisplayOrder = Convert.ToInt32(txtDisplayOrder.Text);

            // If credit card
            if (paymentSetting.PaymentTypeID == 0)
            {
                paymentSetting.GatewayTypeID = int.Parse(lstGateway.SelectedValue);

                paymentSetting.EnableAmex = chkEnableAmex.Checked;
                paymentSetting.EnableDiscover = chkEnableDiscover.Checked;
                paymentSetting.EnableMasterCard = chkEnableMasterCard.Checked;
                paymentSetting.EnableVisa = chkEnableVisa.Checked;
                paymentSetting.TestMode = chkTestMode.Checked;
                paymentSetting.PreAuthorize = chkPreAuthorize.Checked;
                paymentSetting.GatewayPassword = string.Empty;
                paymentSetting.TransactionKey = string.Empty;
                paymentSetting.IsRMACompatible = chkRMA.Checked;
                ZNodeEncryption encrypt = new ZNodeEncryption();

                paymentSetting.GatewayUsername = encrypt.EncryptData(txtGatewayUserName.Text);

                // If authorize.net
                if (paymentSetting.GatewayTypeID == 1)
                {
                    paymentSetting.TransactionKey = encrypt.EncryptData(txtTransactionKey.Text);
                }
                else if (paymentSetting.GatewayTypeID == 12)
                {
                    paymentSetting.TransactionKey = encrypt.EncryptData(txtTransactionKey.Text);
                }
                else if (paymentSetting.GatewayTypeID == 2)
                {
                    // If Verisign PayFlow pro gateway is selected
                    paymentSetting.GatewayPassword = encrypt.EncryptData(txtGatewayPassword.Text);
                    paymentSetting.Partner = txtPartner.Text.Trim();
                    paymentSetting.Vendor = txtVendor.Text.Trim();
                }
                else if (paymentSetting.GatewayTypeID == 5)
                {
                    // If Nova gateway is selected
                    paymentSetting.GatewayPassword = encrypt.EncryptData(txtGatewayPassword.Text);
                    paymentSetting.TransactionKey = encrypt.EncryptData(txtTransactionKey.Text);
                }
                else if (paymentSetting.GatewayTypeID == 6)
                {
                    // If Paypal direct payment gateway is selected
                    paymentSetting.GatewayPassword = encrypt.EncryptData(txtGatewayPassword.Text);
                    paymentSetting.TransactionKey = txtTransactionKey.Text;
                }
                else if (paymentSetting.GatewayTypeID == 10)
                {
                    // If World Pay gateway is selected
                    // Authorization password
                    paymentSetting.GatewayPassword = encrypt.EncryptData(txtGatewayPassword.Text);

                    // Installation Id
                    paymentSetting.TransactionKey = encrypt.EncryptData(txtTransactionKey.Text);
                }
                else
                {
                    paymentSetting.TransactionKey = string.Empty;
                    paymentSetting.GatewayPassword = encrypt.EncryptData(txtGatewayPassword.Text);
                }
            }
            else if (paymentSetting.PaymentTypeID == 2)
            {
                // If Paypal
                paymentSetting.GatewayTypeID = null;
                ZNodeEncryption encrypt = new ZNodeEncryption();

                paymentSetting.GatewayUsername = encrypt.EncryptData(txtGatewayUserName.Text);
                paymentSetting.TransactionKey = txtTransactionKey.Text;
                paymentSetting.GatewayPassword = encrypt.EncryptData(txtGatewayPassword.Text);
                paymentSetting.TestMode = chkTestMode.Checked;
                paymentSetting.PreAuthorize = chkPreAuthorize.Checked;
            }
            else if (paymentSetting.PaymentTypeID == 3)
            {
                // If Google Checkout
                // Set null value to Google
                paymentSetting.GatewayTypeID = null;
                ZNodeEncryption encrypt = new ZNodeEncryption();

                paymentSetting.GatewayUsername = encrypt.EncryptData(txtGatewayUserName.Text);
                paymentSetting.GatewayPassword = encrypt.EncryptData(txtGatewayPassword.Text);
                paymentSetting.TestMode = chkTestMode.Checked;
            }
            else if (paymentSetting.PaymentTypeID == 5)
            {
                // If 2CO
                // Set null value to 2CO
                paymentSetting.GatewayTypeID = null;
                ZNodeEncryption encrypt = new ZNodeEncryption();

                // Vendor ID 
                paymentSetting.GatewayUsername = encrypt.EncryptData(txtGatewayUserName.Text);

                // Secret word to use MD5
                paymentSetting.GatewayPassword = encrypt.EncryptData(txtGatewayPassword.Text);

                // Sandbox
                paymentSetting.TestMode = chkTestMode.Checked;

                // Additional Fee, if any to add in all transaction, like service charge like that.
                paymentSetting.Vendor = encrypt.EncryptData(txtAdditionalFee.Text);
            }
            else
            {
                // Purchase Order
                paymentSetting.GatewayTypeID = null;
            }

            bool retval = false;

            // Update Payment setting into database
            if (this.ItemId > 0)
            {
                retval = settingsAdmin.UpdatePaymentSetting(paymentSetting);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit Payment Option - " + lstPaymentType.SelectedItem.Text, lstPaymentType.SelectedItem.Text);
            }
            else
            {
                bool settingExists;
                if (int.Parse(lstPaymentType.SelectedValue) == 0)
                    settingExists = settingsAdmin.PaymentSettingExists(int.Parse(lstProfile.SelectedValue), int.Parse(lstPaymentType.SelectedValue), int.Parse(lstGateway.SelectedValue), 0);
                else
                    settingExists = settingsAdmin.PaymentSettingExists(int.Parse(lstProfile.SelectedValue), int.Parse(lstPaymentType.SelectedValue));

                if (settingExists)
                {
                    lblMsg.Text = "This Payment Option already exists for this Profile. Please select a different Payment Option.";
                    return;
                }

                retval = settingsAdmin.AddPaymentSetting(paymentSetting);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Added Payment Option - " + lstPaymentType.SelectedItem.Text, lstPaymentType.SelectedItem.Text);
            }

            if (retval)
            {
                // Redirect to main page
                Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Payments/Default.aspx");
            }
            else
            {
                // Display error message
                lblMsg.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Setup/Checkout/Payments/Default.aspx");
        }

        /// <summary>
        /// Payment type change event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            googleNote.Visible = false;

            if (lstPaymentType.SelectedValue.Equals("0"))
            {
                pnlCreditCard.Visible = true;
                pnlCreditCardOptions.Visible = true;
                pnlGatewayList.Visible = true;

                // Hide Additional Fee
                pnl2COGateway.Visible = false;
                this.BindPanels();
            }
            else if (lstPaymentType.SelectedValue.Equals("2"))
            {
                // If Paypal gateway
                lblTransactionKey.Text = "API Signature (PayPal only)";
                pnlAuthorizeNet.Visible = true;
                pnlPassword.Visible = true;
                pnlCreditCard.Visible = true;
                pnlCreditCardOptions.Visible = false;
                pnlGatewayList.Visible = false;

                // Hide Additional Fee
                pnl2COGateway.Visible = false;
            }
            else if (lstPaymentType.SelectedValue.Equals("3"))
            {
                // if Google EC gateway
                googleNote.Visible = true;

                // Hide CreditCard Options
                pnlCreditCardOptions.Visible = false;

                // Hide Gateway dropdownlist
                pnlGatewayList.Visible = false;

                // Enable settings
                pnlCreditCard.Visible = true;

                // Disable TransactionKey field
                pnlAuthorizeNet.Visible = false;

                // Enable PassWord field
                pnlPassword.Visible = true;

                // Hide Additional Fee
                pnl2COGateway.Visible = false;
            }
            else if (lstPaymentType.SelectedValue.Equals("5"))
            {
                // If 2CO gateway
                googleNote.Visible = false;

                // Hide CreditCard Options
                pnlCreditCardOptions.Visible = false;

                // Hide Gateway dropdownlist
                pnlGatewayList.Visible = false;

                // Enable settings
                pnlCreditCard.Visible = true;

                // Disable TransactionKey field
                pnlAuthorizeNet.Visible = false;

                // Enable PassWord field
                pnlPassword.Visible = true;

                // Enable Additional Fee
                pnl2COGateway.Visible = true;

                lblGatewayUserName.Text = "2CO Vendor ID";

                lblGatewaypassword.Text = "Secret Word to use MD5 Verification";
            }
            else
            {
                pnlCreditCard.Visible = false;
            }
        }

        /// <summary>
        /// Gateway changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstGateway_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindPanels();
        }

        /// <summary>
        /// Bind Panels Method
        /// </summary>
        protected void BindPanels()
        {
            // Initialize panels
            pnlLogin.Visible = true;
            pnlPassword.Visible = true;
            pnlVerisignGateway.Visible = false;
            pnlAuthorizeNet.Visible = false;
            pnlGatewayOptions.Visible = false;

            if (lstGateway.SelectedValue.Equals("1") || lstGateway.SelectedValue.Equals("12"))
            {
                // If authorize.net
                lblTransactionKey.Text = "Transaction Key (Authorize.Net only)";
                pnlAuthorizeNet.Visible = true;
                pnlLogin.Visible = true;
                pnlPassword.Visible = false;
                pnlGatewayOptions.Visible = true;
                lblGatewayUserName.Text = "Merchant Login";
                lblGatewaypassword.Text = "Merchant Account Password";
            }
            else if (lstGateway.SelectedValue.Equals("2"))
            {
                // If Verisign payflow pro
                pnlVerisignGateway.Visible = true;
                pnlAuthorizeNet.Visible = false;
                pnlLogin.Visible = true;
                pnlPassword.Visible = true;
                pnlGatewayOptions.Visible = true;
            }
            else if (lstGateway.SelectedValue.Equals("5"))
            {
                // If Nova Payment  Gateway
                lblTransactionKey.Text = "NOVA PIN Number";
                pnlAuthorizeNet.Visible = true;
                pnlLogin.Visible = true;
                pnlPassword.Visible = true;
            }
            else if (lstGateway.SelectedValue.Equals("6"))
            {
                // If paypal direct Payment
                lblTransactionKey.Text = "PayPal Transaction Key";
                pnlAuthorizeNet.Visible = true;
                pnlLogin.Visible = true;
                pnlPassword.Visible = true;
            }
            else if (lstGateway.SelectedValue.Equals("9"))
            {
                // If IP Commerce
                pnlLogin.Visible = false;
                pnlAuthorizeNet.Visible = false;
                pnlPassword.Visible = false;
                pnlVerisignGateway.Visible = false;
            }
            else if (lstGateway.SelectedValue.Equals("11"))
            {
                // If CyberSource
                pnlLogin.Visible = false;
                pnlAuthorizeNet.Visible = false;
                pnlPassword.Visible = false;
                pnlVerisignGateway.Visible = false;
                pnlGatewayOptions.Visible = true;
                pnlTestMode.Visible = true;
            }
            else if (lstGateway.SelectedValue.Equals("10"))
            {
                lblGatewayUserName.Text = "Merchant Code";
                lblGatewaypassword.Text = "Authorization Password";
                lblTransactionKey.Text = "World Pay Installation Id";
                pnlAuthorizeNet.Visible = true;
                pnlPassword.Visible = true;
            }
            else if (lstGateway.SelectedValue.Equals("100"))
            {
                pnlTestMode.Visible = false;
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Check if this setting already exists for this profile
        /// </summary>
        /// <returns>Returns bool value true or false</returns>
        private bool PaymentSettingExists()
        {
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
            return settingsAdmin.PaymentSettingExists(int.Parse(lstProfile.SelectedValue), int.Parse(lstPaymentType.SelectedValue), int.Parse(lstGateway.SelectedValue), ItemId);

        }

        #endregion
    }
}
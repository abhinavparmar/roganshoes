using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Checkout.Payments
{
    /// <summary>
    /// Represents the  SiteAdmin.Secure.Setup.Checkout.Payments - Default
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string CatalogImagePath = string.Empty;
        private string AddLink = "~/SiteAdmin/Secure/Setup/Checkout/Payments/Add.aspx";
        private string EditLink = "~/SiteAdmin/Secure/Setup/Checkout/Payments/Add.aspx";
        private string DeleteLink = "~/SiteAdmin/Secure/Setup/Checkout/Payments/Delete.aspx";
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    this.EditLink = this.EditLink + "?itemid=" + Id;
                    Response.Redirect(this.EditLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + "?itemid=" + Id);
                }
            }
        }

        /// <summary>
        /// Add Payment Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddLink);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
            uxGrid.DataSource = UserStoreAccess.CheckProfileAccess(settingsAdmin.GetAllPaymentSettings().Tables[0], true);
            uxGrid.DataBind();
        }
        #endregion
    }
}
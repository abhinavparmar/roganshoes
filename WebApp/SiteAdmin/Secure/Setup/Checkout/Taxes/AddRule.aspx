<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Checkout.Taxes.AddRule" ValidateRequest="false"
    Title="Manage Taxes - Add Rule" CodeBehind="AddRule.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <div class="tooltip">
                    <a href="javascript:void(0);" class="learn-more"><span>
                        <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                    <div class="content">
                        <h6>Help</h6>
                        <p>
                            Tax Rules are applied in the order of precedence. For Example, to implement a tax rule to apply 5% tax to residents of Alaska and 6.5% for all other US States do the following:
                        </p>
                        <p>
                            (a) Add a rule with the Country=US, State=AK, Tax=5%, Precedence=1<br />
                            (b) Add a second rule with Country=US, State=ALL States, Tax=6.5%, Precedence=2<br />
                        </p>
                    </div>
                </div>
                <uc2:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
            <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
         <h4 class="SubTitle">Store Setting</h4>
           <span class="FieldStyle">Store Name</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlPortal" runat="server">
                            </asp:DropDownList>
                        </span>
        <h4 class="SubTitle">General</h4>
        <div class="FieldStyle">
            Rule Type
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlRuleTypes" runat="server" AutoPostBack="true">
            </asp:DropDownList>
        </div>
        <asp:Panel runat="server" ID="pnlTaxRegion">
            <h4 class="SubTitle">Tax Region</h4>
            <div>
                <p>
                    Apply this rule based on where this item is being shipped to.
                </p>
            </div>
            <asp:UpdatePanel ID="UpdatePanelTaxRegion" runat="server" UpdateMode="Conditional"
                ChildrenAsTriggers="true" RenderMode="Inline">
                <ContentTemplate>
                    <div class="FieldStyle">
                        Destination Country
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="lstCountries" runat="server" AutoPostBack="true" OnSelectedIndexChanged="LstCountries_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="FieldStyle">
                        Destination State
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="lstStateOption" runat="server" AutoPostBack="true" OnSelectedIndexChanged="LstStateOption_SelectedIndexChanged"
                            Width="160px">
                            <asp:ListItem Text="Apply to ALL States" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="FieldStyle">
                        Destination County
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="lstCounty" runat="server" Width="160px">
                            <asp:ListItem Text="Apply to ALL Counties" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlTaxRate">
            <h4 class="SubTitle">Tax Rate</h4>
            <p>
                Specify one or more tax rates below based on your regional requirements.
            </p>
            <div class="FieldStyle">
                Sales Tax
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSalesTax" runat="server" Columns="6" MaxLength="5">0.00</asp:TextBox>%&nbsp;&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtSalesTax"
                    Display="Dynamic" ErrorMessage="Enter a Sales Tax" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtSalesTax"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid Sales Tax" MaximumValue="100"
                    MinimumValue="0" SetFocusOnError="True" Type="Double"></asp:RangeValidator>
            </div>
            <div class="FieldStyle">
                VAT Tax
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtVAT" runat="server" Columns="6" MaxLength="5">0.00</asp:TextBox>%&nbsp;&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtVAT"
                    Display="Dynamic" ErrorMessage="Enter a Vat Tax" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtVAT"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid VAT Tax" MaximumValue="100"
                    MinimumValue="0" SetFocusOnError="True" Type="Double"></asp:RangeValidator>
            </div>
            <div class="FieldStyle">
                GST Tax
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtGST" runat="server" Columns="6" MaxLength="5">0.00</asp:TextBox>%&nbsp;&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtGST"
                    Display="Dynamic" ErrorMessage="Enter a GST Tax" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtGST"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid GST Tax" MaximumValue="100"
                    MinimumValue="0" SetFocusOnError="True" Type="Double"></asp:RangeValidator>
            </div>
            <div class="FieldStyle">
                PST Tax
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtPST" runat="server" Columns="6" MaxLength="5">0.00</asp:TextBox>%&nbsp;&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPST"
                    Display="Dynamic" ErrorMessage="Enter a PST Tax" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtPST"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid PST Tax" MaximumValue="100"
                    MinimumValue="0" SetFocusOnError="True" Type="Double"></asp:RangeValidator>
            </div>
            <div class="FieldStyle">
                HST Tax
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtHST" runat="server" Columns="6" MaxLength="5">0.00</asp:TextBox>%&nbsp;&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtHST"
                    Display="Dynamic" ErrorMessage="Enter a HST Tax" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtHST"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a valid HST Tax" MaximumValue="100"
                    MinimumValue="0" SetFocusOnError="True" Type="Double"></asp:RangeValidator>
            </div>
            <div class="FieldStyle">
                Precedence<span class="Asterix">*</span><br />
                <small>This is the order in which this tax rule will be processed.</small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtPrecedence" runat="server" MaxLength="9" Columns="5">1</asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPrecedence"
                    Display="Dynamic" ErrorMessage="* Enter precedence" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPrecedence"
                    Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999999"
                    MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
            </div>
            <div class="FieldStyle">
            </div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkTaxInclusiveInd" Text="Include Taxes in Product Pricing" runat="server" />
            </div>
            <div class="ValueStyle">
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlAvalaraCredientials" Visible="false">
            <h4 class="SubTitle">
               Avalara Security Credientials</h4>
            <div class="FieldStyle">
                Account ID</div>
            <div class="ValueStyle">
                <asp:Label ID="lblAccount" runat="server" class="FieldStyle"></asp:Label>
            </div>
            <div class="FieldStyle">
                License</div>
            <div class="ValueStyle">
                <asp:Label ID="lblLicence" runat="server" class="FieldStyle"></asp:Label>
            </div>
            <div class="FieldStyle">
                Company</div>
            <div class="ValueStyle">
                <asp:Label ID="lblCompany" runat="server" class="FieldStyle"></asp:Label>
            </div>
            <div class="FieldStyle">
                Webservice URL<br />
                <small>Specify corresponding url based on Test Mode or Production Mode.</small></div>
            <div class="ValueStyle">
                <asp:Label ID="lblURL" runat="server" class="FieldStyle"></asp:Label>
            </div>
        </asp:Panel>        
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>

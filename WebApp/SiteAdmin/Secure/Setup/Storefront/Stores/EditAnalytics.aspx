<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    ValidateRequest="false" Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.EditAnalytics" Title="Manage Stores - Edit Analytics"
    CodeBehind="EditAnalytics.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView Size350">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label><ZNode:DemoMode ID="DemoMode1"
                    runat="server" />
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cacnel" OnClick="BtnCancel_Click" />
        </div>
        <div align="left">
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">JAVASCRIPT</h4>
        <div class="FieldStyle">
            Site Wide Javascript (Top)<br />
            <small>The contents of this field will be added to the top of every page on the site.<br />
                If you are using <strong>Google Analytics</strong>, placing your tracking code here
                will automatically enable the e-commerce tracking on the receipt page.
                <br />
                If you are using <strong>Listrak Shopping Cart Abandonment</strong>, placing your tracking code here
                will track customers who have added items to their shopping cart but did not complete their purchase.<br />
                You should
                only place your code in the Site Wide Javascript (top) or the Site Wide Javascript (bottom)
                but not both fields.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSiteWideTopJavaScript" runat="server" Height="150px" TextMode="MultiLine"
                Width="450px"></asp:TextBox><br />
        </div>
        <div class="FieldStyle">
            Site Wide Javascript (Bottom)
            <br />
            <small>The contents of this field will be added to the bottom of every page on the site.
                <br />
                If you are using <strong>Google Analytics</strong>, placing your tracking code here
                will automatically enable the e-commerce tracking on the receipt page.
                <br />
                If you are using <strong>Listrak Shopping Cart Abandonment</strong>, placing your tracking code here
                will track customers who have added items to their shopping cart but did not complete their purchase.<br />
                You should
                only place your code in the Site Wide Javascript (top) or the Site Wide Javascript (bottom)
                but not both fields.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSiteWideBottomJavaScript" runat="server" Height="150px" TextMode="MultiLine"
                Width="450px"></asp:TextBox><br />
        </div>
        <div class="FieldStyle">
            Site Wide Javascript (except Order Receipt page)<br />
            <small>The contents of this field will be placed at the bottom of every page on the
                site <strong>except</strong> the receipt page.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSiteWideAnalyticsJavascript" runat="server" Height="150px" TextMode="MultiLine"
                Width="450px"></asp:TextBox><br />
        </div>
        <div class="FieldStyle">
            Order Receipt Javascript<br />
            <small>The contents of this field will be added to the order receipt page. In your Javascript you can refer to hidden input fields on the Order Receipt page which have the following IDs: OrderID, OrderTotal, ShippingTotal and TaxTotal.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtOrderReceiptJavaScript" runat="server" Height="150px" TextMode="MultiLine"
                Width="450px"></asp:TextBox><br />
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>

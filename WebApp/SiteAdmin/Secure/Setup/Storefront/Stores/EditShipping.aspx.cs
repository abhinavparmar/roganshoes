using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Admin_Secure_settings_Stores_EditShipping class.
    /// </summary>
    public partial class EditShipping : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int itemId = 0;
        private bool showThreshold = false;//Zeon Custom Code
        private bool ShowUSPSDetails = false;
        private string redirectUrl = "view.aspx?ItemId={0}&mode=ship";
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get portalId value from querystring
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"].ToString());
            }

            //Zeon Custom Code : Start
            if (Request.Params["showthreshold"] != null)
            {
                this.showThreshold = bool.Parse(Request.Params["showthreshold"].ToString());
            }
            if (Request.Params["ShowUSPSDetails"] != null)
            {
                this.ShowUSPSDetails = bool.Parse(Request.Params["ShowUSPSDetails"].ToString());
            }


            //Zeon Custom Code : End

            if (!Page.IsPostBack)
            {
                this.ShowHidePanels();
                this.BindCountry();
                this.Bind();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = null;
            bool isUpdated = false;

            if (this.itemId > 0)
            {
                portal = storeAdmin.GetByPortalId(this.itemId);
            }
            if (ShowUSPSDetails) //Zeon Custom Code to set USPS Shipping Details:: End
            {
                ZNodeEncryption encryption = new ZNodeEncryption();
                ZeonUSPSCredentialsService service = new ZeonUSPSCredentialsService();
                TList<ZeonUSPSCredentials> zeonUSPSCredentialsList = service.GetByPortalID(portal.PortalID);
                if (zeonUSPSCredentialsList != null && zeonUSPSCredentialsList.Count > 0)
                {
                    zeonUSPSCredentialsList[0].USPSUserName = encryption.EncryptData(txtUSPSUserName.Text);
                    zeonUSPSCredentialsList[0].USPSPassword = encryption.EncryptData(txtUSPSPassword.Text);
                    zeonUSPSCredentialsList[0].PortalID = portal.PortalID;
                    isUpdated = service.Update(zeonUSPSCredentialsList[0]);
                }
                else
                {
                    ZeonUSPSCredentials uspsCredentials = new ZeonUSPSCredentials()
                    {
                        USPSUserName = encryption.EncryptData(txtUSPSUserName.Text),
                        USPSPassword = encryption.EncryptData(txtUSPSPassword.Text),
                        PortalID = portal.PortalID
                    };
                    isUpdated = service.Insert(uspsCredentials);
                }
            } //Zeon Custom Code to set USPS Shipping Details :: End
            else if (!showThreshold) ////Zeon Custom Code : Start
            {
                if (portal != null)
                {
                    ZNodeEncryption encryption = new ZNodeEncryption();

                    // Shipping Settings        
                    portal.ShippingOriginAddress1 = Server.HtmlEncode(txtShippingAddress1.Text.Trim());
                    portal.ShippingOriginAddress2 = Server.HtmlEncode(txtShippingAddress2.Text.Trim());
                    portal.ShippingOriginCity = Server.HtmlEncode(txtShippingCity.Text.Trim());
                    portal.ShippingOriginPhone = Server.HtmlEncode(txtShippingPhone.Text.Trim());

                    portal.ShippingOriginZipCode = Server.HtmlEncode(txtShippingZipCode.Text.Trim());
                    portal.ShippingOriginStateCode = Server.HtmlEncode(txtShippingStateCode.Text.Trim());
                    portal.ShippingOriginCountryCode = lstCountries.SelectedValue;

                    portal.FedExDropoffType = ddldropOffTypes.SelectedItem.Value;
                    portal.FedExPackagingType = ddlPackageTypeCodes.SelectedItem.Value;
                    portal.FedExUseDiscountRate = chkFedExDiscountRate.Checked;
                    portal.FedExAddInsurance = chkAddInsurance.Checked;

                    // UPS Shipping Settings
                    portal.UPSUserName = encryption.EncryptData(txtUPSUserName.Text.Trim());
                    portal.UPSPassword = encryption.EncryptData(txtUPSPassword.Text.Trim());
                    portal.UPSKey = encryption.EncryptData(txtUPSKey.Text.Trim());

                    // FedEx Shipping Settings
                    portal.FedExAccountNumber = encryption.EncryptData(txtAccountNum.Text.Trim());
                    portal.FedExMeterNumber = encryption.EncryptData(txtMeterNum.Text.Trim());
                    portal.FedExProductionKey = encryption.EncryptData(txtProductionAccessKey.Text.Trim());
                    portal.FedExSecurityCode = encryption.EncryptData(txtSecurityCode.Text.Trim());

                    isUpdated = storeAdmin.UpdateStore(portal);
                }
            }

            else
            {
                //Zeon Custom Code : Start
                PortalExtnService portalExtService = new PortalExtnService();
                TList<PortalExtn> portalList = portalExtService.GetByPortalID(portal.PortalID);
                if (portalList != null && portalList.Count > 0)
                {
                    portalList[0].ShippingThreshholdOrderAmount = Convert.ToDecimal(txtShippingThresholdAmount.Text);
                    isUpdated = portalExtService.Update(portalList[0]);
                }
                else
                {
                    PortalExtn portalExt = new PortalExtn();
                    portalExt.PortalID = portal.PortalID;
                    portalExt.ShippingThreshholdOrderAmount = Convert.ToDecimal(txtShippingThresholdAmount.Text);
                    isUpdated = portalExtService.Insert(portalExt);
                }
                //Zeon Custom Code : End
            }

            if (isUpdated)
            {
                // Log Activity
                string associateName = "Edit Shipping settings - " + portal.StoreName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, portal.StoreName);
                Response.Redirect(string.Format(this.redirectUrl, this.itemId));
            }
            else
            {
                lblMsg.Text = "An error ocurred while updating the store shipping settings. Please try again.";

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format(this.redirectUrl, this.itemId));
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind the store shipping details.
        /// </summary>
        private void Bind()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();

            // Set default Value from Store1 For Add page
            Portal portal = storeAdmin.GetByPortalId(this.itemId);

            if (portal != null)
            {
                try
                {
                    lblTitle.Text = "Edit Shipping Settings for \"" + portal.StoreName + "\"";
                    ZNodeEncryption encryption = new ZNodeEncryption();

                    // Set UPS Account details
                    if (portal.UPSUserName != null)
                    {
                        txtUPSUserName.Text = encryption.DecryptData(portal.UPSUserName);
                    }

                    if (portal.UPSPassword != null)
                    {
                        txtUPSPassword.Text = encryption.DecryptData(portal.UPSPassword);
                    }

                    if (portal.UPSKey != null)
                    {
                        txtUPSKey.Text = encryption.DecryptData(portal.UPSKey);
                    }

                    // Bind FedEx Account details
                    if (portal.FedExAccountNumber != null)
                    {
                        txtAccountNum.Text = encryption.DecryptData(portal.FedExAccountNumber);
                    }

                    if (portal.FedExMeterNumber != null)
                    {
                        txtMeterNum.Text = encryption.DecryptData(portal.FedExMeterNumber);
                    }

                    if (portal.FedExProductionKey != null)
                    {
                        txtProductionAccessKey.Text = encryption.DecryptData(portal.FedExProductionKey);
                    }

                    if (portal.FedExSecurityCode != null)
                    {
                        txtSecurityCode.Text = encryption.DecryptData(portal.FedExSecurityCode);
                    }

                    if (portal.FedExUseDiscountRate.HasValue)
                    {
                        chkFedExDiscountRate.Checked = portal.FedExUseDiscountRate.Value;
                    }

                    if (portal.FedExAddInsurance.HasValue)
                    {
                        chkAddInsurance.Checked = portal.FedExAddInsurance.Value;
                    }

                    if (portal.FedExPackagingType != null)
                    {
                        ddlPackageTypeCodes.SelectedValue = portal.FedExPackagingType;
                    }

                    if (portal.FedExDropoffType != null)
                    {
                        ddldropOffTypes.SelectedValue = portal.FedExDropoffType;
                    }
                }
                catch
                {
                    // Ignore decryption errors
                }

                txtShippingAddress1.Text = Server.HtmlDecode(portal.ShippingOriginAddress1);
                txtShippingAddress2.Text = Server.HtmlDecode(portal.ShippingOriginAddress2);
                txtShippingCity.Text = Server.HtmlDecode(portal.ShippingOriginCity);
                txtShippingPhone.Text = Server.HtmlDecode(portal.ShippingOriginPhone);
                txtShippingZipCode.Text = Server.HtmlDecode(portal.ShippingOriginZipCode);
                txtShippingStateCode.Text = Server.HtmlDecode(portal.ShippingOriginStateCode);
                ListItem listItem = lstCountries.Items.FindByValue(portal.ShippingOriginCountryCode);
                if (listItem != null)
                {
                    lstCountries.SelectedIndex = lstCountries.Items.IndexOf(listItem);
                }

                //Zeon Custom Code : Start
                PortalExtnService portalExtService = new PortalExtnService();
                TList<PortalExtn> portalList = portalExtService.GetByPortalID(portal.PortalID);
                if (portalList != null && portalList.Count > 0)
                {
                    txtShippingThresholdAmount.Text = portalList[0].ShippingThreshholdOrderAmount == null ? "0.00" : Convert.ToDecimal(portalList[0].ShippingThreshholdOrderAmount).ToString("c").Substring(1);
                }

                if (ShowUSPSDetails)
                {
                    ZeonUSPSCredentialsService service = new ZeonUSPSCredentialsService();
                    TList<ZeonUSPSCredentials> zeonUSPSCredentialsList = service.GetByPortalID(portal.PortalID);
                    if (zeonUSPSCredentialsList != null && zeonUSPSCredentialsList.Count > 0)
                    {
                        ZNodeEncryption encrypt = new ZNodeEncryption();
                        txtUSPSUserName.Text = encrypt.DecryptData(zeonUSPSCredentialsList[0].USPSUserName);
                    }
                }
                //Zeon Custom Code : End
            }
        }

        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Country> countries = shipAdmin.GetDestinationCountries();

            lstCountries.DataSource = countries;
            lstCountries.DataTextField = "Code";
            lstCountries.DataValueField = "Code";
            lstCountries.DataBind();
            lstCountries.Items.RemoveAt(0);
        }

        /// <summary>
        /// Show and Hide the panels
        /// </summary>
        private void ShowHidePanels()
        {
            if (showThreshold)
            {
                pnlThreshold.Visible = true;
                pnlShippingInfo.Visible = false;
                pnlSetUSPSCredentials.Visible = false;
            }
            else if (ShowUSPSDetails)
            {
                pnlThreshold.Visible = false;
                pnlShippingInfo.Visible = false;
                pnlSetUSPSCredentials.Visible = true;
            }
            else
            {
                pnlThreshold.Visible = false;
                pnlShippingInfo.Visible = true;
                pnlSetUSPSCredentials.Visible = false;
            }
        }
        #endregion
    }
}
<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" ValidateRequest="false" Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.AddProfile" CodeBehind="AddProfile.aspx.cs" Title="Manage Stores - Add Profile" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Add Profiles to Store<asp:Label ID="lblTitle" runat="server" /></h1>
        </div>
        <div style="text-align: right" class="ImageButtons">
            <asp:Button ID="btnBack" CausesValidation="False" CssClass="Size100" Text="<< Back"
                runat="server" OnClick="BtnBack_Click"></asp:Button>
        </div>
        <div class="ClearBoth">
        </div>
        <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
        <!-- Profile List -->
        <asp:Panel ID="pnlProfileList" runat="server">
            <div class="FieldStyle">
                Select Profile
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlProfileList" runat="server" />
            </div>
            <div class="FieldStyle"></div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkIsDefaultAnonymous" runat="server" Checked="false" Text="This is the default profile for anonymous users"></asp:CheckBox></div>

            <div class="FieldStyle"></div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkIsDefaultRegistered" runat="server" Checked="false" Text="This is the default profile for registered users"></asp:CheckBox></div>

            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="0" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="Submit_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="Cancel_Click" />
            </div>
        </asp:Panel>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </div>
</asp:Content>

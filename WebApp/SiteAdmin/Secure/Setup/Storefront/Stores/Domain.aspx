<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" Title="Manage Stores - Add Domain" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.AddDomain" ValidateRequest="false"
    CodeBehind="Domain.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <div>
        <asp:Label ID="lblMessage" runat="server" CssClass="Error"></asp:Label>
    </div>

    <div class="FormView" id="DomainContent" runat="server">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div align="left" class="ClearBoth">
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <ZNode:Spacer ID="Sapcer1" SpacerHeight="10" SpacerWidth="3" runat="server" />
        </div>
        <div class="ClearBoth">
        </div>
        <div class="FieldStyle">
            URL Name<span class="Asterix">*</span><br />
            <small>Enter a fully qualified URL (example: http://mysite.com). Note that the "www" and any port number will be ignored.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDomainName" runat="server" Width="300px"></asp:TextBox>&nbsp;
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDomainName"
                ErrorMessage="* Enter Domain Name" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RecularExpressionValidator" runat="server" ControlToValidate="txtDomainName"
                ErrorMessage="* Enter Valid URL" CssClass="Error" Display="dynamic" ValidationExpression="^(http\:\/\/[a-zA-Z0-9_\-/]+(?:\.[a-zA-Z0-9_\-/]+)*)$"></asp:RegularExpressionValidator>
        </div>
        <div class="FieldStyle">
        </div>
        <div class="ValueStyleText">
            <asp:CheckBox ID="CheckIsDomainInd" runat="server" Checked="true" Text="Enable this URL"></asp:CheckBox>
        </div>
        <div class="ClearBoth"></div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" Title="Manage Stores - Edit Shipping" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.EditShipping" CodeBehind="EditShipping.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label><ZNode:DemoMode ID="DemoMode1"
                    runat="server" />
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cacnel" OnClick="BtnCancel_Click" />
        </div>
        <div align="left">
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div class="ClearBoth">
        </div>
        <asp:Panel ID="pnlThreshold" runat="server">
            <h4 class="SubTitle">Shipping Threshold</h4>
            <div class="FieldStyle">
                Shipping Threshold Amount
            </div>
            <div class="ValueStyle">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;
                <asp:TextBox ID="txtShippingThresholdAmount" runat="server" Width="142px"></asp:TextBox>
                <br />
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtShippingThresholdAmount"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage="You must enter a valid Shipping Threshold Amount(ex: 123.45)."
                    CssClass="Error" Display="Dynamic" />
                <br />
                <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtShippingThresholdAmount"
                    ErrorMessage="You must enter a Shipping Threshold Amount between $0 and $9,999.99."
                    MaximumValue="999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlSetUSPSCredentials" runat="server" Visible="false">
            <h4 class="SubTitle">USPS</h4>
              <div class="FieldStyle">
                USPS User Name
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtUSPSUserName" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                USPS Password
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtUSPSPassword" TextMode="Password" runat="server" Width="152px"></asp:TextBox>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlShippingInfo" runat="server">
            <h4 class="SubTitle">Shipping Origin</h4>
            <div class="FieldStyle">
                Shipping Origin Address 1
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtShippingAddress1" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Shipping Origin Address 2
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtShippingAddress2" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Shipping Origin City
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtShippingCity" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Shipping Origin State Code
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtShippingStateCode" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Shipping Origin Zip Code
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtShippingZipCode" runat="server" Width="152px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ControlToValidate="txtShippingZipCode" CssClass="Error"
                    ValidationExpression="^\d{5}$|^\d{5}-\d{4}$" ErrorMessage="Enter valid Zip code!" Display="static" runat="server" />
            </div>
            <div class="FieldStyle">
                Shipping Origin Country Code
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstCountries" runat="server">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                Shipping Origin Phone
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtShippingPhone" runat="server" Width="152px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator14"
                    ControlToValidate="txtShippingPhone" CssClass="Error"
                    ValidationExpression="^[ 0-9a-zA-Z()-]{7,15}$"
                    ErrorMessage="Enter valid Phone Number!" Display="static" runat="server" />

            </div>
            <h4 class="SubTitle">FedEx</h4>
            <p>
                Required to retrieve FedEx shipping rates. You would need to signup for an API account
                with FedEx&reg;.<br />
                <br />
            </p>
            <div class="FieldStyle">
                FedEx&reg; Account Number
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtAccountNum" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                FedEx&reg; Meter Number
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtMeterNum" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                FedEx Production key
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductionAccessKey" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                FedEx Security Code
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSecurityCode" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                Select FedEx Drop-Off Type
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddldropOffTypes" runat="server">
                    <asp:ListItem Value="BUSINESS_SERVICE_CENTER">BUSINESS SERVICE CENTER</asp:ListItem>
                    <asp:ListItem Value="DROP_BOX">DROP BOX</asp:ListItem>
                    <asp:ListItem Selected="true" Value="REGULAR_PICKUP">REGULAR PICKUP</asp:ListItem>
                    <asp:ListItem Value="REQUEST_COURIER">REQUEST COURIER</asp:ListItem>
                    <asp:ListItem Value="STATION">STATION</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                Select FedEx Packaging Type
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlPackageTypeCodes" runat="server">
                    <asp:ListItem Selected="true" Value="YOUR_PACKAGING">Your Packaging</asp:ListItem>
                    <asp:ListItem Value="FEDEX_10KG_BOX" Enabled="false">FedEx&reg; 10KG Box</asp:ListItem>
                    <asp:ListItem Value="FEDEX_25KG_BOX" Enabled="false">FedEx&reg; 25KG Box</asp:ListItem>
                    <asp:ListItem Value="FEDEX_BOX">FedEx&reg; Box</asp:ListItem>
                    <asp:ListItem Value="FEDEX_ENVELOPE">FedEx&reg; Envelope</asp:ListItem>
                    <asp:ListItem Value="FEDEX_TUBE">FedEx&reg; Tube</asp:ListItem>
                    <asp:ListItem Value="FEDEX_PAK">FedEx&reg; Pak</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="FieldStyle"></div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkFedExDiscountRate" runat="server" Text="Use FedEx Discount Rate" /></div>
            <div class="FieldStyle"></div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkAddInsurance" runat="server" Text="Add Insurance" /></div>
            <div class="ClearBoth">
                <br />
            </div>
            <h4 class="SubTitle">UPS</h4>
            <p>
                Required to retrieve UPS shipping rates. You would need to signup for an API account
                with UPS.<br />
                <br />
            </p>
            <div class="FieldStyle">
                UPS User Name
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtUPSUserName" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                UPS Password
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtUPSPassword" TextMode="Password" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                UPS Access key
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtUPSKey" runat="server" Width="152px"></asp:TextBox>
            </div>
        </asp:Panel>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cacnel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_settings_Stores_List class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Member Variables
        private string addPageLink = "~/SiteAdmin/Secure/Setup/Storefront/Stores/Add.aspx";
        private string editPageLink = "~/SiteAdmin/Secure/Setup/Storefront/Stores/view.aspx";
        private string deletePageLink = "~/SiteAdmin/Secure/Setup/Storefront/Stores/delete.aspx";
        private int portalCount;
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindSearchStore();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchStore();
        }

        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindSearchStore();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    this.editPageLink = this.editPageLink + "?itemid=" + Id;
                    Response.Redirect(this.editPageLink);
                }
                else if (e.CommandName == "Copy")
                {
                    if (!UserStoreAccess.IsMultiStoreAdminEnabled() && uxGrid.Rows.Count >= 1)
                    {
                        lblmsg.Text = UserStoreAccess.UpgradeMessage;
                        return;
                    }
                    else
                    {
                        StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();

                        // Copy the Selected Store.                
                        bool isSuccess = storeAdmin.CopyStoreByPortalID(Convert.ToInt32(Id));

                        this.BindSearchStore();
                    }
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.deletePageLink + "?itemid=" + Id);
                }
            }
        }

        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton previewButton = (LinkButton)e.Row.Cells[3].FindControl("btnPreview");
                int id = int.Parse(e.Row.Cells[0].Text);

                DomainAdmin domainAdmin = new DomainAdmin();
                TList<Domain> domainList = domainAdmin.DeepLoadDomainsByPortalID(id);

                foreach (Domain domain in domainList)
                {
                    if (domain.IsActive)
                    {
                        previewButton.Attributes.Add("onclick", "window.open('http://" + domain.DomainName + "','',''); return false;");
                        break;
                    }
                }

                // Disable the Delete Button if this is the only portal
                if (this.portalCount <= 1)
                {
                    LinkButton deleteButton = (LinkButton)e.Row.Cells[3].FindControl("btnDelete");
                    deleteButton.Visible = false;
                }
            }
        }
        #endregion

        #region Other Events

        /// <summary>
        /// Search Button Click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchStore();
        }

        /// <summary>
        /// Clear Button Click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtkeyword.Text = string.Empty;
            this.BindSearchStore();
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            // Display upgrade warning message if singlestore edition.
            if (!UserStoreAccess.IsMultiStoreAdminEnabled() && uxGrid.Rows.Count >= 1)
            {
                lblmsg.Text = UserStoreAccess.UpgradeMessage;
            }
            else
            {
                Response.Redirect(this.addPageLink);
            }
        }
        #endregion

        #region Helper Method
        /// <summary>
        /// To hide the delete option in the grid for current store
        /// </summary>
        /// <param name="portalId">Portal Id to check</param>
        /// <returns>Return true if portal is not current portal</returns>
        protected bool HideDeleteButton(string portalId)
        {
            return ZNodeConfigManager.SiteConfig.PortalID != int.Parse(portalId);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindSearchStore()
        {
            StoreSettingsHelper storeSettingsHelper = new StoreSettingsHelper();

            // Count the total number of portals 
            this.portalCount = storeSettingsHelper.GetStoreCount();

            DataSet ds = storeSettingsHelper.GetStoreByKeyword(txtkeyword.Text.Trim().Replace(" ", "%"));

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["CompanyName"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["CompanyName"].ToString());
                    ds.Tables[0].Rows[index]["StoreName"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["StoreName"].ToString());
                }
            }

            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(ds.Tables[0]);
            uxGrid.DataBind();
        }
        #endregion
    }
}
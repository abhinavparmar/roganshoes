<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" Title="Manage Stores - Edit SMTP" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.EditSmtp" CodeBehind="EditSmtp.aspx.cs" ValidateRequest="false" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label><ZNode:DemoMode ID="DemoMode1"
                    runat="server" />
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
        </div>
        <div align="left">
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div class="FieldStyle">
            SMTP Port
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSMTPPort" runat="server" Width="152px" Text="25"></asp:TextBox>
            <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" ControlToValidate="txtSMTPPort"
                ErrorMessage="*Please use a number."
                Display="Dynamic" ValidationExpression="[\d]+"
                ValidationGroup="smtp" CssClass="Error"></asp:RegularExpressionValidator>
        </div>
        <div class="FieldStyle">
            SMTP Server
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSMTPServer" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            SMTP Server User Name
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSMTPUserName" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            SMTP Server Password
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSMTPPassword" TextMode="Password" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click"
                CausesValidation="true" ValidationGroup="smtp" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the SiteAdmin  Admin_PortalCatalog class
    /// </summary>
    public partial class AddPortalCatalog : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int PortalId = 0;
        private int PortalCatalogId = 0;
        private string ViewPage = "View.aspx?itemid=";
        private string AddCatalogPage = "~/SiteAdmin/Secure/Setup/Storefront/Catalogs/add.aspx";
        private string SelectedTheme = string.Empty;
        private string AssociateName = string.Empty;
        private int Localeid = 43;
        private CatalogAdmin catalogAdmin = new CatalogAdmin();
        #endregion

        #region Page Load Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemId"] != null)
            {
                this.PortalId = int.Parse(Request.Params["itemId"].ToString());
            }

            if (Request.Params["PortalCatalogId"] != null)
            {
                this.PortalCatalogId = int.Parse(Request.Params["PortalCatalogId"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindCatalog();
                this.BindTheme();
                this.BindLocale();
                this.BindPortalCatalog(); 
            }
        }
        #endregion

        #region Form Events
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancelBottom_Click(object sender, ImageClickEventArgs e)
        {
            this.RedirectToViewPage();
        }

        /// <summary>
        /// Theme Dropdown list Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlTheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCSS.Items.Clear();
            pnlCSS.Visible = true;
            this.SelectedTheme = ddlTheme.SelectedItem.Text;
            this.BindCss();
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            this.RedirectToViewPage();
        }

        /// <summary>
        /// Catalog Dropdown Control Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCatalog.SelectedValue == "0")
            {
                Response.Redirect(this.AddCatalogPage);
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmitBottom_Click(object sender, ImageClickEventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();

            lblError.Visible = true;

            if (ddlCatalog.SelectedValue == string.Empty)
            {
                // Display Error message
                lblError.Text = "Please select valid catalog";
                return;
            }

            if (ddlCSS.Items.Count == 0)
            {
                lblError.Text = "CSS file list is empty.";
                return;
            }

            if (ddlLocale.Items.Count == 0)
            {
                lblError.Text = "Locale list is empty.";
                return;
            }

            if (this.IsCatalogExist())
            {
                lblError.Text = "You can only associate a catalog to a store once per locale. Please select a different catalog or locale when associating a catalog.";
                return;
            }

            if (this.IsCatalogLocaleExist())
            {
                lblError.Text = "Portal catalog locale already exist.";
                return;
            }
              
            PortalCatalogService portalService = new PortalCatalogService();
            PortalCatalog portalCatalog = new PortalCatalog();
            portalCatalog.PortalCatalogID = this.PortalCatalogId;
            portalCatalog.PortalID = this.PortalId;
            portalCatalog.CatalogID = int.Parse(ddlCatalog.SelectedValue);


            portalCatalog.LocaleID = Localeid;
            portalCatalog.Theme = ddlTheme.SelectedItem.Text;
            portalCatalog.CSS = ddlCSS.SelectedItem.Text;

            if (this.PortalCatalogId == 0)
            {
                portalService.Save(portalCatalog);

                // Add Default Content Pages
                ContentPageService contentPageService = new ContentPageService();
                ContentPage contentPage = new ContentPage();
                ContentPageAdmin contentPageAdmin = new ContentPageAdmin();
                ContentPageQuery filter = new ContentPageQuery();

                // Get Portal By portalID
                Portal portal = storeAdmin.GetByPortalId(this.PortalId);

                portal.LocaleID = Localeid;


                // Get ContentPages list using portalID and LocaleId
                filter.Append(ContentPageColumn.LocaleId, Localeid.ToString());
                filter.Append(ContentPageColumn.PortalID, this.PortalId.ToString());
                TList<ContentPage> contentPageList = contentPageService.Find(filter.GetParameters());

                // Get ContentPages list using portalID and LocaleId
                filter.Clear();
                filter.Append(ContentPageColumn.PortalID, this.PortalId.ToString());
                filter.Append(ContentPageColumn.LocaleId, Localeid.ToString());
                TList<ContentPage> contentPageAllList = contentPageService.Find(filter.GetParameters());

                if (contentPageList.Count > 0)
                {
                    foreach (ContentPage page in contentPageList)
                    {
                        ContentPage cp = contentPageAllList.Find("Name", page.Name);

                        if (cp == null)
                        {
                            contentPage = page.Clone() as ContentPage;
                            contentPage.ContentPageID = -1;
                            contentPage.PortalID = this.PortalId;
                            contentPage.LocaleId = Localeid;
                            contentPage.SEOURL = null;
                            contentPage.Theme = ddlTheme.SelectedItem.Text;
                            contentPageAdmin.AddPage(contentPage, contentPageAdmin.GetPageHTMLByName(page.Name, page.PortalID, page.LocaleId.ToString()), this.PortalId, ddlLocale.SelectedValue, HttpContext.Current.User.Identity.Name, null, false);
                        }
                    }
                }
                else
                {
                    // Add Default Home Page
                    ContentPage defaultContentPage = new ContentPage();

                    defaultContentPage.Name = "Home";
                    defaultContentPage.PortalID = this.PortalId;
                    defaultContentPage.AllowDelete = false;
                    defaultContentPage.ActiveInd = true;
                    defaultContentPage.Title = "Home";
                    defaultContentPage.SEOMetaDescription = "Home";
                    defaultContentPage.SEOMetaKeywords = "Home";
                    defaultContentPage.SEOTitle = "Home";
                    defaultContentPage.TemplateName = "home.master";
                    defaultContentPage.LocaleId = Localeid;
                    defaultContentPage.SEOURL = null;
                    defaultContentPage.Theme = ddlTheme.SelectedItem.Text;

                    // Add code here
                    contentPageAdmin.AddPage(defaultContentPage, string.Empty, this.PortalId, ddlLocale.SelectedValue, HttpContext.Current.User.Identity.Name, null, false);

                    portal.LocaleID = Localeid;

                    bool check = storeAdmin.UpdateStore(portal);
                }

                // Add Custom Messages
                // Create Custom Messages
                storeAdmin.CreateMessage(this.PortalId.ToString(), ddlLocale.SelectedValue);

                // Log Activity
                this.AssociateName = "Associate Catalog " + ddlCatalog.SelectedItem.Text + " to Store " + portal.StoreName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, portal.StoreName);
            }
            else
            {
                Portal portal = storeAdmin.GetByPortalId(this.PortalId);
                portalService.Update(portalCatalog);
                string AssociateName = "Edit Association between " + ddlCatalog.SelectedItem.Text + " Catalog and '" + portal.StoreName + "' Store";
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, portal.StoreName);
            }

            this.RedirectToViewPage();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Redirect to Product catalog view page
        /// </summary>
        private void RedirectToViewPage()
        {
            Response.Redirect(this.ViewPage + this.PortalId + "&Mode=catalog");
        }

        /// <summary>
        /// Check whether the selected catalog already associated with the store.
        /// </summary>
        /// <returns>Returns bool value true or false</returns>
        private bool IsCatalogExist()
        {
            PortalCatalogService portalCatalogService = new PortalCatalogService();

            int selectedCatalogId = Convert.ToInt32(ddlCatalog.SelectedValue);

            PortalCatalog portalCatalog = portalCatalogService.GetByPortalIDCatalogID(this.PortalId, selectedCatalogId);

            if (portalCatalog == null)
            {
                return false;
            }

            return !(portalCatalog.PortalCatalogID == this.PortalCatalogId);
        }

        /// <summary>
        /// Check whether the portal catalog-locale theme already exist.
        /// </summary>
        /// <returns>Returns True if exist, else False</returns>
        private bool IsCatalogLocaleExist()
        {
            PortalCatalogHelper portalCatalogHelper = new PortalCatalogHelper();
            DataSet ds = portalCatalogHelper.GetPortalCatalog(this.PortalId);
            int catalogId = int.Parse(ddlCatalog.SelectedValue);
            int localeId = Localeid;
            string expression = "PortalCatalogId<>" + this.PortalCatalogId.ToString() + " AND CatalogId=" + catalogId + "  AND LocaleId=" + localeId.ToString();
            ds.Tables[0].DefaultView.RowFilter = expression;

            if (ds.Tables[0].DefaultView.Count > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Bind catalog theme
        /// </summary>
        private void BindPortalCatalog()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = storeAdmin.GetByPortalId(this.PortalId);
            lblTitle.Text = "Associate Catalog to Store - \"" + portal.StoreName + "\"";

            if (this.PortalCatalogId > 0)
            {
                ddlLocale.Enabled = false;
                PortalCatalogService portalCatalogService = new PortalCatalogService();
                PortalCatalog portalCatalog = portalCatalogService.GetByPortalCatalogID(this.PortalCatalogId);

                ddlCatalog.SelectedValue = portalCatalog.CatalogID.ToString();
                ddlTheme.SelectedValue = portalCatalog.Theme;
                this.SelectedTheme = ddlTheme.SelectedItem.Text;
                ddlCSS.Items.Clear();
                pnlCSS.Visible = true;
                this.BindCss();

                ddlCSS.SelectedIndex = ddlCSS.Items.IndexOf(ddlCSS.Items.FindByValue(portalCatalog.CSS));
                ddlLocale.SelectedIndex = ddlLocale.Items.IndexOf(ddlLocale.Items.FindByValue(portalCatalog.LocaleID.ToString()));
            }
        }

        /// <summary>
        /// Bind the catalog by selected portal
        /// </summary>
        private void BindCatalog()
        {
            ddlCatalog.DataSource = this.catalogAdmin.GetAllCatalogs();
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();

            foreach (ListItem licatalog in ddlCatalog.Items)
            {
                licatalog.Text = Server.HtmlDecode(licatalog.Text);
            }

            ListItem separator = new ListItem("------------------------------", "");
            ddlCatalog.Items.Insert(ddlCatalog.Items.Count, separator);

            ListItem item = new ListItem("Add New Catalog", "0");
            ddlCatalog.Items.Insert(ddlCatalog.Items.Count, item);
            ddlCatalog.SelectedIndex = 0;

        }

        /// <summary>
        /// Bind locale by selected portal Id
        /// </summary>
        private void BindLocale()
        {
            // Load all locales
            LocaleService localeService = new LocaleService();
            TList<Locale> localeList = localeService.GetAll();
            localeList.Sort("LocaleDescription");
            ddlLocale.DataValueField = "LocaleId";
            ddlLocale.DataTextField = "LocaleDescription";
            ddlLocale.DataSource = localeList;
            ddlLocale.DataBind(); 
            // Remove the existing locale from DropDownList
            PortalCatalogService portalCatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByPortalID(this.PortalId);
            if (this.PortalCatalogId == 0)
            {
                foreach (PortalCatalog pc in portalCatalogList)
                {
                    ListItem li = ddlLocale.Items.FindByValue(pc.LocaleID.ToString());
                    if (li != null)
                    {
                        ddlLocale.Items.Remove(li);
                    }
                }
            }
            else
            {
                PortalCatalog portalCatalog = portalCatalogService.GetByPortalCatalogID(this.PortalCatalogId);
                int localeId = portalCatalog.LocaleID;
                foreach (PortalCatalog pc in portalCatalogList)
                {
                    ListItem li = ddlLocale.Items.FindByValue(pc.LocaleID.ToString());
                    if (li != null && localeId != pc.LocaleID)
                    {
                        ddlLocale.Items.Remove(li);
                    }
                }
            }
            if (ddlLocale.SelectedValue != null)
            {  
                this.ddlLocale.SelectedValue = 43.ToString();
            }
        }


        /// <summary>
        /// Bind theme file list
        /// </summary>
        private void BindTheme()
        {
            System.IO.DirectoryInfo path = new System.IO.DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + ConfigurationManager.AppSettings["WebThemePath"].ToString()));
            ddlTheme.DataSource = path.GetDirectories();
            ddlTheme.DataBind();
            ddlTheme.SelectedIndex = 0;

            // Remove the svn hidden path folder.
            ddlTheme.Items.Remove(".svn");
            this.SelectedTheme = ddlTheme.SelectedItem.Text;
            this.BindCss();
        }

        /// <summary>
        /// Bind the CSS to the DropDownList
        /// </summary>
        private void BindCss()
        {
            System.IO.DirectoryInfo path = new System.IO.DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + ConfigurationManager.AppSettings["WebThemePath"].ToString() + this.SelectedTheme + "/"));
            DirectoryInfo[] directoryInfoList = path.GetDirectories("Css");

            foreach (DirectoryInfo directory in directoryInfoList)
            {
                // Returns a master file list from the current directory.
                FileInfo[] masterFiles = directory.GetFiles("*.css");

                foreach (FileInfo masterPage in masterFiles)
                {
                    string fileName = masterPage.Name;
                    fileName = fileName.Replace(".css", string.Empty);
                    ddlCSS.Items.Add(fileName);
                }
            }
        }
        #endregion


    }
}
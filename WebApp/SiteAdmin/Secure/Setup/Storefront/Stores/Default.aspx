<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" Title="Manage Stores - List" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.Default" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <div>
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>Manage Stores
                      <div class="tooltip">
                          <a href="javascript:void(0);" class="learn-more"><span>
                              <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                          <div class="content">
                              <h6>Help</h6>
                              <p>
                                  You can create a new storefront or manage an existing one. To create a
                                new storefront follow these steps:
                              </p>
                              <p>
                                  (1) Click the "Add New Store" button<br />
                                  (2) Add one or more URLs to the store<br />
                                  (3) Add at least one profile to use as the default anonymous profile<br />
                                  (4) Add at least one profile to use as the default registered profile<br />
                                  (5) Configure other store settings including SMTP, Currencies, Shipping and Analytics
                              </p>
                          </div>
                      </div>
                </h1>

            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAdd" runat="server" CommandName="Login"
                    ButtonType="Button" OnClick="BtnAdd_Click" Text="Add New Store"
                    ButtonPriority="Primary" />

            </div>
            <div class="ClearBoth">
                <p>
                    Set up stores, associate a catalog, and create URLs to access the store.
                </p>
            </div>

            <!-- Search product panel -->
            <div>
                <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                    <h4 class="SubTitle">Search Stores</h4>
                    <div class="SearchForm">
                        <div class="RowStyle">
                            <div class="ItemStyle">
                                <span class="FieldStyle">Keyword</span><br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtkeyword" runat="server"></asp:TextBox></span>
                            </div>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                            runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                        <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                            runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                    </div>
                </asp:Panel>
            </div>
            <br />

            <div>
                <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label></div>
            <br />

            <h4 class="GridTitle">Store List</h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
                OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText="No records exist in the database."
                OnRowDataBound="UxGrid_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="PortalID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="StoreName" HeaderText="Store Name" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="CompanyName" HeaderText="Brand" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="ACTION" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <div class="LeftFloat" style="width: 25%; text-align: left">
                                <asp:LinkButton ID="btnPreview" CommandName="Preview" Text="PREVIEW &raquo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PortalID")%>'
                                    runat="server"></asp:LinkButton>
                            </div>
                            <div class="LeftFloat" style="width: 25%; text-align: left">
                                <asp:LinkButton ID="btnView" CommandName="Edit" Text="MANAGE &raquo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PortalID")%>'
                                    runat="server"></asp:LinkButton>
                            </div>
                            <div class="LeftFloat" style="width: 25%; text-align: left">
                                <asp:LinkButton ID="btnEdit" CommandName="Copy" Text="COPY &raquo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PortalID")%>'
                                    runat="server"></asp:LinkButton>
                            </div>
                            <div class="LeftFloat" style="width: 25%; text-align: left">
                                <asp:LinkButton ID="btnDelete" CommandName="Delete" Text="DELETE &raquo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PortalID")%>'
                                    runat="server" Visible='<%# HideDeleteButton(Eval("PortalID").ToString()) %>'></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <div>
                <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
            </div>
        </div>
    </div>
</asp:Content>

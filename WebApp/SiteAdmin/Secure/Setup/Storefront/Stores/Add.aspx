<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.Add" ValidateRequest="false" Title="Manage Stores - Add"
    CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <script type="text/javascript">
        function SetAddressValidationSectionEnabled() {

            var chkEnableAddressValidation = document.getElementById("<%=chkEnableAddressValidation.ClientID %>");
            var chkRequireValidatedAddress = document.getElementById("<%=chkRequireValidatedAddress.ClientID %>");

            if (!chkEnableAddressValidation.checked) {
                chkRequireValidatedAddress.checked = false;
                chkRequireValidatedAddress.disabled = true;
            }
            else {
                chkRequireValidatedAddress.disabled = false;
            }

        }
    </script>

    <div>
        <asp:Label ID="lblMessage" runat="server" CssClass="Error"></asp:Label>
    </div>
    <div class="FormView" id="StoreContent" runat="server">
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label><uc1:DemoMode ID="DemoMode1"
                        runat="server" />
                </h1>
            </div>
            <div style="text-align: right; padding-right: 10px;">
                <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
                <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
            <div align="left">
                <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
            </div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                ShowMessageBox="False" ShowSummary="False" />
        </div>
        <div class="ClearBoth">
            <h4 class="SubTitle">Store Identity</h4>
            <div class="FieldStyle">
                Brand Name<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCompanyName" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCompanyName"
                    ErrorMessage="* Enter Brand Name" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                Store Name<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtStoreName" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtStoreName"
                    ErrorMessage="* Enter Store Name" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle" runat="server">
                Catalog
            </div>
            <div class="ValueStyle" runat="server">
                <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <asp:Panel ID="pnlThemes" runat="server">
                <div class="FieldStyle" runat="server">
                    Theme
                </div>
                <div class="ValueStyle" runat="server">
                    <asp:DropDownList ID="ddlThemeslist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlThemeslist_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlCssList" runat="server" Visible="false">
                <div class="FieldStyle" runat="server">
                    CSS
                </div>
                <div class="ValueStyle" runat="server">
                    <asp:DropDownList ID="ddlCSSList" runat="server">
                    </asp:DropDownList>
                </div>
            </asp:Panel>
            <div class="FieldStyle"> 
                <asp:Label ID="lblLocale" runat="server" Text="Locale" Visible="false" ></asp:Label>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlLocale" runat="server" Visible="false" />
            </div>
            <asp:Panel ID="tblShowImage" runat="server" Visible="true" Width="100%">
                <div>
                    <asp:Image ID="imgLogo" runat="server" />
                </div>
                <div class="FieldStyle">
                    Select an Option
                </div>
                &nbsp;
                <asp:RadioButton ID="radCurrentImage" Text="Keep Current Image" runat="server" GroupName="LogoImage"
                    AutoPostBack="True" OnCheckedChanged="RadCurrentImage_CheckedChanged" Checked="True" />
                <asp:RadioButton ID="radNewImage" Text="Upload New Image" runat="server" GroupName="LogoImage"
                    AutoPostBack="True" OnCheckedChanged="RadNewImage_CheckedChanged" />
            </asp:Panel>
            <asp:Panel ID="tblLogoUpload" Width="100%" runat="server" Visible="false">
                <div class="FieldStyle">
                    Select a Logo<span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:FileUpload ID="UploadImage" runat="server" />
                    <asp:RequiredFieldValidator CssClass="Error" ID="RequiredFieldValidator14" runat="server"
                        ControlToValidate="UploadImage" ErrorMessage="* Select an Image to Upload" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblImageError" runat="server" CssClass="Error" ForeColor="Red" Text="Select Valid Product Image (JPEG or PNG or GIF)"
                        Visible="False"></asp:Label>
                </div>
            </asp:Panel>
            <h4 class="SubTitle">Security</h4>

            <div class="ValueStyleText">
                <asp:CheckBox ID="chkEnableSSL" runat="server" Text="Enable Secure Socket Layer (SSL) for this store" />
            </div>
            <div class="ClearBoth">
            </div>
            <br />
            <h4 class="SubTitle">Store Contact Information</h4>
            <p>
                Use a comma to separate multiple email addresses.
            </p>
            <div class="FieldStyle">
                Administrator's Email<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtAdminEmail" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtAdminEmail"
                    ErrorMessage="* Enter Admin Email" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="valRegEx" runat="server" ControlToValidate="txtAdminEmail"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="* Enter a valid e-mail address."
                    Display="dynamic" CssClass="Error"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                Sales Department Email<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSalesEmail" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtSalesEmail"
                    ErrorMessage="* Enter Sales Email" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSalesEmail"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="* Enter a valid e-mail address."
                    CssClass="Error" Display="dynamic"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                Customer Service Email<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCustomerServiceEmail" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCustomerServiceEmail"
                    ErrorMessage="* Enter Customer Service Email" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCustomerServiceEmail"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="* Enter a valid e-mail address."
                    Display="dynamic" CssClass="Error"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                Sales Department Phone Number<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSalesPhoneNumber" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtSalesPhoneNumber"
                    ErrorMessage="* Enter Sales Phone Number" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                Customer Service Phone Number<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCustomerServicePhoneNumber" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtCustomerServicePhoneNumber"
                    ErrorMessage="* Enter Customer Service Phone Number" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <h4 class="SubTitle">Default Settings</h4>
            <div class="FieldStyle">
                Default Customer Review Status
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ListReviewStatus" runat="server">
                    <asp:ListItem Text="Publish Immediately" Value="A"></asp:ListItem>
                    <asp:ListItem Text="Do Not Publish. Require Moderator Approval" Value="N" Selected="true"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="FieldStyle">
                        Default Order Status<br />
                        <small>When a new order is placed,
                        <br />
                            it will be automatically set to<br />
                            this status.</small>
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlOrderStateList" runat="server" Width="160px" />
                        <asp:CheckBox ID="chkPendingApproval" runat="server" Text="Require manual approval of every order"
                            AutoPostBack="True" OnCheckedChanged="ChkPendingApproval_CheckedChanged" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="FieldStyle">
                Include Taxes In Product Price
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkInclusiveTax" Enabled="false" runat="server" Text="" />
            </div>
            <div class="FieldStyle">
                Enable Persistent Cart
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkPersistentCart" runat="server" Text="" />
            </div>
            <div class="FieldStyle">
                Enable Address Validation
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkEnableAddressValidation" runat="server" onclick="SetAddressValidationSectionEnabled()" />
            </div>
            <div class="FieldStyle">
                Require Validated Address
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkRequireValidatedAddress" runat="server" />
            </div>
            <div class="FieldStyle">
                Enable PIMS
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkEnablePIMS" runat="server" />
            </div>
            <div class="FieldStyle">
                Default Product Review Status
                <br />
                <small style="width: 153px;">The status selected will appear when a product is added by a franchise or mall admin.</small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlProductReviewStateID" runat="server" Width="160px" />
            </div>
            <div class="ClearBoth">
                <br />
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </div>
</asp:Content>

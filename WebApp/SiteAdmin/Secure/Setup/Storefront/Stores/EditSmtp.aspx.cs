using System;
using System.Web;
using System.Web.UI;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Admin_Secure_settings_Stores_EditSmtp class.
    /// </summary>
    public partial class EditSmtp : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string redirectUrl = "view.aspx?ItemId={0}&mode=smtp";
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get portalId value from querystring
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.Bind();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = null;
            bool isUpdated = false;

            if (this.itemId > 0)
            {
                portal = storeAdmin.GetByPortalId(this.itemId);
            }

            if (portal != null)
            {
                ZNodeEncryption encryption = new ZNodeEncryption();

                // SMTP Server Settings
                portal.SMTPServer = Server.HtmlEncode(txtSMTPServer.Text);
                portal.SMTPPort = txtSMTPPort.Text.Trim().Length > 0 ? int.Parse(txtSMTPPort.Text.Trim()) : 0;
                portal.SMTPUserName = encryption.EncryptData(txtSMTPUserName.Text);

                string currentPassword = encryption.DecryptData(portal.SMTPPassword);
                portal.SMTPPassword = encryption.EncryptData(txtSMTPPassword.Text);

                isUpdated = storeAdmin.UpdateStore(portal);
            }

            if (isUpdated)
            {
                // Log Activity
                string associateName = "Edit SMTP Settings - " + portal.StoreName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, portal.StoreName);
                Response.Redirect(string.Format(this.redirectUrl, this.itemId));
            }
            else
            {
                lblMsg.Text = "An error ocurred while updating the store smtp settings. Please try again.";

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format(this.redirectUrl, this.itemId));
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind the store SMTP settings.
        /// </summary>
        private void Bind()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();

            // Set default Value from Store1 For Add page
            Portal portal = storeAdmin.GetByPortalId(this.itemId);

            try
            {
                lblTitle.Text = "Edit SMTP Settings for \"" + portal.StoreName + "\"";

                ZNodeEncryption encryption = new ZNodeEncryption();
                txtSMTPPort.Text = portal.SMTPPort.GetValueOrDefault(25).ToString();

                if (portal.SMTPServer != null)
                {
                    txtSMTPServer.Text = Server.HtmlDecode(portal.SMTPServer);
                }

                if (portal.SMTPUserName != null)
                {
                    txtSMTPUserName.Text = encryption.DecryptData(portal.SMTPUserName);
                }

                if (portal.SMTPPassword != null)
                {
                    string password = encryption.DecryptData(portal.SMTPPassword);

                    txtSMTPPassword.Attributes.Add("value", password);
                }
            }
            catch
            {
                // Ignore decryption errors
            }
        }
        #endregion
    }
}
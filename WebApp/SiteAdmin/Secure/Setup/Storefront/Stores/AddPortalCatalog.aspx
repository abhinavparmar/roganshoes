<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Stores.AddPortalCatalog" ValidateRequest="false"
    Title="Manage Stores - Add PortalCatalog" CodeBehind="AddPortalCatalog.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server" /></h1>
        </div>
        <div style="text-align: right" class="ImageButtons">
            <asp:Button ID="btnBack" CausesValidation="False" CssClass="Size100" Text="<< Back"
                runat="server" OnClick="BtnBack_Click"></asp:Button>
        </div>
        <div class="ClearBoth">
        </div>
        <ZNode:DemoMode ID="DemoMode1" runat="server"></ZNode:DemoMode>
        <!-- Profile List -->
        <asp:Panel ID="pnlProfileList" runat="server">
            <asp:UpdatePanel ID="updCatalog" runat="server">
                <ContentTemplate>
                    <div class="FieldStyle">
                        Catalog
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlCatalog" runat="server"   OnSelectedIndexChanged="DdlCatalog_SelectedIndexChanged" AutoPostBack="true" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="FieldStyle">
                Theme
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlTheme" runat="server" OnSelectedIndexChanged="DdlTheme_SelectedIndexChanged"
                    AutoPostBack="True" />
            </div>
            <asp:Panel ID="pnlCSS" runat="server">
                <div class="FieldStyle">
                    Stylesheet (CSS)
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlCSS" runat="server" />
                </div>
            </asp:Panel>
            <div class="FieldStyle" style="display:none;">
                Locale
            </div>
            <div class="ValueStyle" style="display:none;">
                <asp:DropDownList ID="ddlLocale" runat="server" />
            </div>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" CausesValidation="true"
                    OnClick="BtnSubmitBottom_Click" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancelBottom_Click" />
            </div>
        </asp:Panel>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </div>
</asp:Content>

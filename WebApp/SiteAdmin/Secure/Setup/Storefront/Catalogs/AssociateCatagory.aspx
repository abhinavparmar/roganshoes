<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Setup.Storefront.Catalogs.AssociateCatagory"
    Title="Manage Categories - Associate Category" CodeBehind="AssociateCatagory.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/SiteAdmin/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ManageCategoryProfile" Src="~/SiteAdmin/Secure/Setup/Storefront/Categories/ManageCategoryProfile.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <script type="text/javascript">
        var hiddenTextValue; //alias to the hidden field: hideValue      

        function AutoComplete_ParentCategorySelected(source, eventArgs) {
            hiddenTextValue = $get("<%=ParentCategoryId.ClientID %>");
            hiddenTextValue.value = eventArgs.get_value();
        }

        function AutoComplete_ParentCategoryShowing(source, eventArgs) {
            hiddenTextValue = $get("<%=ParentCategoryId.ClientID %>");
            hiddenTextValue.value = 0;

        }
    </script>



    <asp:HiddenField ID="ParentCategoryId" runat="server"></asp:HiddenField>
    <div class="FormView DesignPageAdd">
        <div>
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>
        <div>

            <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <asp:Panel ID="pnlSearchDepartment" runat="server">
            <div class="FieldStyle" runat="server" id="titleSearch">
                Search & Select Category <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <div class="TextValueStyle">
                    <ZNode:CategoryAutoComplete runat="server" ID="txtCategory" Width="180px" ForceAutoCompleteTextBox="true" IsRequired="true" />
                </div>
                <div>
                    <asp:Image CssClass="SearchIcon" ImageUrl="~/SiteAdmin/Themes/images/enlarge.gif" ToolTip="Search Category" AlternateText="Search" runat="server" ImageAlign="AbsMiddle" ID="btnShowPopup" />
                </div>
            </div>

        </asp:Panel>
        <h4 class="SubTitle">Category Settings</h4>
        <div class="FieldStyle">
            Parent Category
        </div>
        <div class="ValueStyle"> 
      <asp:DropDownList ID="ddlParentCategory" runat="server" Width="160px" ></asp:DropDownList> 

        </div>
        <div class="FieldStyle">
            Enable this Category
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkDepartment" Checked="true" runat="server" Text="Display this category on the navigation menu"></asp:CheckBox>
        </div>
        <div class="FieldStyle">
            Display Order <span class="Asterix">*</span><br />
            <small>Categories with a lower display order are displayed first on the navigation menu.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDisplayOrder" runat="server" Width="60px" MaxLength="5">99</asp:TextBox>
            <div>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Display="Dynamic"
                    ErrorMessage="* Enter a Display Order" ControlToValidate="txtDisplayOrder"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtDisplayOrder"
                    Display="Dynamic" ErrorMessage="Enter a valid numeric display order." MaximumValue="999999999"
                    MinimumValue="1" Type="Integer"></asp:RangeValidator>
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePnlSelecCustomer" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlAdvanced" runat="server" Visible="true">
                    <h4 class="SubTitle">Advanced Settings</h4>

                    <div class="FieldStyle">
                        Category Page Theme<br />
                        <small>You can optionally select a different theme and style for this category page.</small>
                    </div>


                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlThemeslist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlThemeslist_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <asp:Panel ID="pnlTemplateList" runat="server" Visible="false">
                        <div class="FieldStyle">
                            Master Page Template
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="ddlPageTemplateList" runat="server">
                            </asp:DropDownList>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlCssList" runat="server" Visible="false">
                        <div class="FieldStyle">
                            Stylesheet (CSS)
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="ddlCSSList" runat="server">
                            </asp:DropDownList>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="ClearBoth"></div>
        <ZNode:ManageCategoryProfile ID="Managecategoryprofile1" runat="server"></ZNode:ManageCategoryProfile>
        <div>
            <uc1:Spacer ID="Spacer9" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <asp:UpdatePanel ID="UpdatePnldivButton" runat="server">
            <ContentTemplate>
                <div>
                    <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
                </div>
                <div>

                    <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div runat="server" id="divButton">
                    <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                        runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
                    <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace SiteAdmin.Secure.Setup.Storefront.Catalogs
{
    /// <summary>
    /// Represents the Catalogs Copy class
    /// </summary>
    public partial class Copy : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string Link = "~/SiteAdmin/Secure/Setup/Storefront/Catalogs/Default.aspx";
        private int ItemId = 0;
        #endregion

        #region Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["ItemId"] != null)
            {
                this.ItemId = int.Parse(Request.Params["ItemId"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindEditData();               
            }
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            bool status = false;

            try
            {
                CatalogHelper helper = new CatalogHelper();
                status = helper.CopyCatalog(this.ItemId, Server.HtmlEncode(txtcatalogName.Text.Trim()), 43);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            if (!status)
            {
                lblMsg.Text = "Error: Copying catalog could not be done.";
            }
            else
            {
                Response.Redirect(this.Link);
            }
        }

        /// <summary>
        /// Cancel Button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.Link);
        }
        #endregion

        #region Bind Methods
        protected void BindEditData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            Catalog catalog = new Catalog();

            if (this.ItemId > 0)
            {
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess).Split(',');
                    int found = Array.IndexOf(stores, this.ItemId.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }

                catalog = catalogAdmin.GetCatalogByCatalogId(this.ItemId);

                lblTitle.Text = "Copy " + catalog.Name;
                txtcatalogName.Text = "Copy of " + Server.HtmlDecode(catalog.Name);
            }
        }

      

        #endregion
    }
}
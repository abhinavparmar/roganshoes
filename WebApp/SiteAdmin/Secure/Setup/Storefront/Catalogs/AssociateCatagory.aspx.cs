using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Storefront.Catalogs
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_catalog_product_category_AssociateCatagory class
    /// </summary>
    public partial class AssociateCatagory : System.Web.UI.Page
    {
        #region Private Variables
        private string AssociateName = string.Empty;
        private int CatalogId = 0;
        private int NodeId = 0;

        #endregion

        #region Properties

        public bool ShowSubmitButton
        {
            set
            {
                divButton.Visible = value;
            }
        }

        #endregion


        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        { 
            if (Request.Params["nodeId"] != null)
            {
                this.NodeId = int.Parse(Request.Params["nodeId"].ToString());
            }

            if (Request.Params["itemid"] != null)
            {
                this.CatalogId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (Request.Params["name"] != null)
            {
                lblTitle.Text = "Associate Category with Catalog - \"" + Server.UrlDecode(Request.Params["name"].ToString()) + "\"";
            }
            else
            {
                lblTitle.Text = "Associate Category with Catalog";
            } 
            if (IsPostBack)
            {
                if (ParentCategoryId.Value.Length == 0 && ddlParentCategory.SelectedValue == "0")
                { 
                    BindParentCategory();
                }
            }
            if (!IsPostBack)
            {
                this.BindParentCategory();
                this.BindThemeList();

                if (this.NodeId > 0)
                {
                    this.BindEditData();
                    btnShowPopup.Visible = false;
                    titleSearch.InnerText = "Category";
                }
                else
                {
                    pnlSearchDepartment.Visible = true;
                    btnShowPopup.Visible = true;
                }
            }

            btnShowPopup.Attributes.Add("onclick", "var newwindow = window.open('../Catalogs/DepartmentSearch.aspx?sourceId=" + txtCategory.ClientId + "&source=" + txtCategory.txtClientId + "','RequiredDepartment','left=400, top=100, height=540, width= 650, status=n o, resizable= no, scrollbars=no, toolbar= no,location= no, menubar= no'); newwindow.focus(); return false;");
        }

        #region General Events

        /// <summary>
        /// Dropdown Control Themes list - Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlThemeslist_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlTemplateList.Visible = true;
            pnlCssList.Visible = true;
            ddlPageTemplateList.Items.Clear();
            ddlCSSList.Items.Clear();
            this.BindMasterPageTemplates();
            this.BindCssList();
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                CategoryAdmin categoryAdmin = new CategoryAdmin();
                CategoryNode categoryInfo = new CategoryNode();

                bool retval = false;
                int? ParentNodeId = null;

                if (this.NodeId > 0)
                {
                    categoryInfo = categoryAdmin.GetByCategoryNodeId(this.NodeId);
                }

                if (ddlParentCategory.SelectedValue == "0")
                {
                    ParentCategoryId.Value = "0";
                }
                else
                {
                    ParentCategoryId.Value = ddlParentCategory.SelectedValue;
                }

                ParentNodeId = int.Parse(ParentCategoryId.Value); 

                categoryInfo.CategoryID = int.Parse(txtCategory.Value);

                if (categoryInfo.CategoryID == 0)
                {
                    lblError.Text = "Please select valid Category.";
                    return;
                }

                // Set ParentCategoryNodeId is Null.
                // If the user selects the Root Node.
                if (ParentNodeId.Value == 0 && ddlParentCategory.SelectedValue !="0")
                {
                    lblError.Text = "Please select valid parent Category.";
                    return;
                }
                else if (ParentNodeId.Value == 0 && ddlParentCategory.SelectedValue == "0")
                {
                    ParentNodeId = null;
                }

                categoryInfo.ParentCategoryNodeID = ParentNodeId;
                categoryInfo.CatalogID = this.CatalogId;
                categoryInfo.DisplayOrder = int.Parse(txtDisplayOrder.Text);

                if (ddlThemeslist.SelectedValue != "0")
                {
                    categoryInfo.Theme = ddlThemeslist.SelectedValue;
                }
                else
                {
                    categoryInfo.Theme = null;
                    categoryInfo.MasterPage = null;
                    categoryInfo.CSS = null;
                }

                if (ddlThemeslist.SelectedValue != "0")
                {
                    if (ddlPageTemplateList.SelectedValue != "0")
                    {
                        categoryInfo.MasterPage = ddlPageTemplateList.SelectedValue;
                    }
                    else
                    {
                        categoryInfo.MasterPage = null;
                    }

                    if (ddlCSSList.SelectedValue != "0")
                    {
                        categoryInfo.CSS = ddlCSSList.SelectedValue;
                    }
                    else
                    {
                        categoryInfo.CSS = null;
                    }
                }

                categoryInfo.ActiveInd = chkDepartment.Checked;

                if (this.NodeId > 0)
                {
                    retval = categoryAdmin.UpdateNode(categoryInfo);
                }
                else
                {
                    retval = categoryAdmin.AddNode(categoryInfo);
                }

                if (retval)
                {
                    this.AssociateName = "Associated Category " + txtCategory.Text + "  to  " + Request.Params["name"].ToString();
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, Request.Params["name"].ToString());

                    Response.Redirect("~/SiteAdmin/Secure/Setup/Storefront/Catalogs/add.aspx?Mode=Dept&itemid=" + this.CatalogId.ToString(), false);
                }
                else
                {
                    lblError.Text = "This Category already exists in this catalog.";
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "An error occured:" + ex.Message;
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Setup/Storefront/Catalogs/add.aspx?Mode=Dept&itemid=" + this.CatalogId);
        }

        #endregion

        #region Bind Methods

        private void BindEditData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            CategoryNode categoryNode = categoryAdmin.GetByCategoryNodeId(this.NodeId);

            Category category = categoryAdmin.GetByCategoryId(categoryNode.CategoryID);

            if (categoryNode.ParentCategoryNodeID.HasValue)
            {
                ParentCategoryId.Value = categoryNode.ParentCategoryNodeID.Value.ToString();
            }
            else
            {
                ParentCategoryId.Value = "0";
            }

            txtDisplayOrder.Text = categoryNode.DisplayOrder.ToString();
            txtCategory.Text = Server.HtmlDecode(category.Name);
            txtCategory.Value = category.CategoryID.ToString();
            if (categoryNode.Theme != null)
            {
                ddlThemeslist.SelectedValue = categoryNode.Theme;
                pnlTemplateList.Visible = true;
                pnlCssList.Visible = true;
                this.BindMasterPageTemplates();
                this.BindCssList();
            }

            chkDepartment.Checked = categoryNode.ActiveInd;

            if (categoryNode.MasterPage != null)
            {
                ddlPageTemplateList.SelectedValue = categoryNode.MasterPage;
            }

            if (categoryNode.CSS != null)
            {
                ddlCSSList.SelectedValue = categoryNode.CSS;
            }
        }

        /// <summary>
        /// Binds the themes to the DropDownList
        /// </summary>
        private void BindThemeList()
        {
            System.IO.DirectoryInfo path = new System.IO.DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/"));
            ddlThemeslist.DataSource = path.GetDirectories();
            ddlThemeslist.DataBind();
            ListItem li = new ListItem("Same as Store", "0");
            ddlThemeslist.Items.Insert(0, li);

            // Remove the svn hidden path folder.
            ddlThemeslist.Items.Remove(".svn");
            try
            {
                ddlThemeslist.SelectedIndex = 0;
                pnlTemplateList.Visible = true;
                pnlCssList.Visible = true;
                this.BindMasterPageTemplates();
                this.BindCssList();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Bind the MasterPage templates
        /// </summary>
        private void BindMasterPageTemplates()
        {
            if (ddlThemeslist.SelectedIndex >= 0)
            {
                // Create instance for direcoryInfo and specify the directory 'MasterPages/Product'
                DirectoryInfo directoryInfo = new DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + ddlThemeslist.SelectedItem.Text + "/MasterPages/Category/"));

                // Determine whether the directory 'MasterPages/Category' exists.
                if (directoryInfo.Exists)
                {
                    // Returns a master file list from the current directory.
                    FileInfo[] masterFiles = directoryInfo.GetFiles("*.master");
                    ddlPageTemplateList.Items.Clear();
                    foreach (FileInfo masterPage in masterFiles)
                    {
                        string fileName = masterPage.Name;

                        // Name only
                        fileName = fileName.Replace(".master", string.Empty);

                        ddlPageTemplateList.Items.Add(fileName);
                    }
                }

                // Master template
                ListItem li = new ListItem("Select Template", "0");
                ddlPageTemplateList.Items.Insert(0, li);
            }
        }

        /// <summary>
        /// Bind the CSS to the DropDownList
        /// </summary>
        private void BindCssList()
        {
            if (ddlThemeslist.SelectedIndex >= 0)
            {
                System.IO.DirectoryInfo path = new System.IO.DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + ddlThemeslist.SelectedItem.Text + "/"));

                if (path.Exists)
                {
                    DirectoryInfo[] directoryInfoList = path.GetDirectories("Css");
                    foreach (DirectoryInfo directory in directoryInfoList)
                    {
                        // Returns a master file list from the current directory.
                        FileInfo[] masterFiles = directory.GetFiles("*.css");
                        ddlCSSList.Items.Clear();
                        foreach (FileInfo masterPage in masterFiles)
                        {
                            string fileName = masterPage.Name;

                            // Name only
                            fileName = fileName.Replace(".css", string.Empty);

                            ddlCSSList.Items.Add(fileName);
                        }
                    }
                }

                ListItem li = new ListItem("Same as Store", "0");
                ddlCSSList.Items.Insert(0, li);
            }
        }

        /// <summary>
        /// Bind the Parent Category list
        /// </summary>
        private void BindParentCategory()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            CategoryHelper categoryHelper = new CategoryHelper();

            CategoryNode categoryNode = categoryAdmin.GetByCategoryNodeId(this.NodeId);
            string parentCategory = string.Empty;
            string parentCategoryName = string.Empty;
            int categoryId = 0;
            int parentCategoryId = 0;

            if (this.NodeId > 0)
            {
                categoryId = categoryNode.CategoryID;
                if (categoryNode.ParentCategoryNodeID.HasValue)
                {
                    parentCategoryId = categoryNode.ParentCategoryNodeID.Value;
                }
            }
            else
            {
                categoryId = 0;
            }

            DataSet datasetCategory = categoryHelper.GetCategoryNodes(this.CatalogId, categoryId);

            foreach (System.Data.DataRow dr in datasetCategory.Tables[0].Select())
            {
                dr["Name"] = categoryAdmin.ParsePath(dr["Name"].ToString(), ">");                
            }
            ddlParentCategory.DataSource = datasetCategory.Tables[0];
            ddlParentCategory.DataTextField = "Name";
            ddlParentCategory.DataValueField = "CategoryNodeid";
            ddlParentCategory.DataBind();
            
            if (Request.Params["nodeId"] != null)
            { 
                foreach (System.Data.DataRow dr in datasetCategory.Tables[0].Select("CategoryNodeid=" + parentCategoryId.ToString()))
                {
                    parentCategoryName = dr["Name"].ToString();
                    parentCategory = parentCategoryName + " >";
                } 
                if (parentCategoryId != 0)
                { 
                    ListItem item = ddlParentCategory.Items.FindByText(parentCategoryName);
                    if (item != null)
                    {
                        item.Selected = true;
                    } 
                }
                else
                {
                    ddlParentCategory.Items.Insert(0, new ListItem("No Parent - Root Level", "0"));
                }
            }
            else
            {
                ddlParentCategory.Items.Insert(0, new ListItem("No Parent - Root Level", "0"));

            } 
        }

        #endregion

         
    }
}
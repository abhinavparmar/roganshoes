<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Catalogs.SearchDepartment" CodeBehind="SearchDepartment.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:UpdatePanel ID="UpdatePnlSearch" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>Search Categories</h1>
            <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                <div class="SearchForm">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtCategoryName" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Catalog</span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="ddlCatalog" runat="server"></asp:DropDownList></span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                    </div>
                    <div>
                        <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                            runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                        <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                            runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
            <h4 class="GridTitle">Category List</h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                CellPadding="2" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                CaptionAlign="Left" Width="100%" EnableSortingAndPagingCallbacks="False" PageSize="10"
                EmptyDataText="No categories exist in the database.">
                <Columns>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <a href="javascript:Close('<%# Eval("CategoryID") %>','<%# GetName(Eval("Name")) %>')">
                                <%# DataBinder.Eval(Container.DataItem, "Name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "VisibleInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DisplayOrder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div>
    <ZNode:Spacer ID="uxSpacer" runat="server" SpacerWidth="5" SpacerHeight="5" />
</div>
<div align="right">
    <input type="image" id="btnClose" onclick="javascript: window.close();" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
        onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" value="Close"
        src="../../../../Themes/Images/buttons/button_cancel.gif" style="border: none;" />
</div>

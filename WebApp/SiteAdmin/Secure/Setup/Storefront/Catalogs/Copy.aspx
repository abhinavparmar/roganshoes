﻿<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master"
    Inherits="SiteAdmin.Secure.Setup.Storefront.Catalogs.Copy" ValidateRequest="false" Title="Manage Catalogs - Copy" CodeBehind="Copy.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <asp:Panel ID="pnlCatalog" runat="server" DefaultButton="btnSubmit">
        <div>
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <ajaxToolKit:TabContainer ID="tabCatalogSettings" runat="server">
            <ajaxToolKit:TabPanel ID="pnlSettings" runat="server">
                <HeaderTemplate>
                    Settings
                </HeaderTemplate>
                <ContentTemplate>
                    <div class="FieldStyle">
                        Catalog Name<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtcatalogName" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcatalogName"
                            ErrorMessage="* Enter Catalog Name" CssClass="Error" Display="dynamic" ValidationGroup="grpTitle"></asp:RequiredFieldValidator>
                    </div>
                                     
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>
        <div class="ClearBoth">
            <br />
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <asp:ImageButton ID="btnSubmit" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Submit" OnClick="BtnCancel_Click" />
        </div>
    </asp:Panel>
</asp:Content>

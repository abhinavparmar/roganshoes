﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageCategoryProfile.ascx.cs" Inherits="SiteAdmin.Secure.Setup.Storefront.Categories.ManageCategoryProfile" %>
<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<script type="text/javascript">
    $(".deleteLink").click(function () {
        return confirm('Are you sure you wish to delete this record?');
    });
</script>

<asp:Panel runat="server" ID="pnlCategoryProfile">
    <asp:UpdatePanel runat="server" ID="upSkuProfile">
        <ContentTemplate>
            <div style="float: left; width: 50%;">
                <h4 class="GridTitle">Profile Category Available Date</h4>
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="AddProfile" runat="server" CausesValidation="false"
                    ButtonType="Button" OnClick="Add_Click" Text="Profile Category Available Date"
                    ButtonPriority="Primary" />
            </div>
            <div class="ClearBoth"></div>
            <div class="LeftContent">
                <asp:GridView ID="uxGrid" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                    CaptionAlign="Left" CellPadding="4" CssClass="Grid" GridLines="None" Width="50%"
                    DataKeyNames="CategoryProfileID" OnRowEditing="uxGrid_EditCommand" OnRowDeleting="uxGrid_DeleteCommand" onrowdatabound="uxGrid_RowDataBound" >
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Profile Name" />
                        <asp:BoundField DataField="EffectiveDate" HeaderText="Effective Date" DataFormatString="{0:MM/dd/yyyy}" />
                        <asp:ButtonField ButtonType="Link" CommandName="Edit" Text="Edit" />
                        <asp:ButtonField ButtonType="Link" CommandName="Delete" Text="Delete"/>
                        
                    </Columns>
                    <RowStyle CssClass="RowStyle" />
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <EmptyDataTemplate>
                        No profiles associated.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
            <znode:Spacer ID="Spacer1" runat="server" SpacerHeight="10" />
            <asp:Label runat="server" ID="lblError" CssClass="Error"></asp:Label>
            <asp:Panel runat="server" ID="pnlEditProfile" Visible="false" CssClass="FormView">
                <h4 class="GridTitle">
                    <asp:Label runat="server" ID="lblTitle"></asp:Label></h4>
                <div class="FieldStyle">
                    Profile :<br />
                    <small>Select the profile you want to associate.</small>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlProfile" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle">
                    Effective Date :<br />
                    <small>Enter the effective date for this profile to show the category.</small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox runat="server" ID="txtEffectiveDate"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="imgbtnStartDt" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter valid date"
                        ControlToValidate="txtEffectiveDate" ValidationGroup="skuprofile" CssClass="Error"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEffectiveDate"
                        CssClass="Error" Display="Dynamic" ErrorMessage="* Enter valid date in MM/DD/YYYY format"
                        ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                        ValidationGroup="skuprofile"></asp:RegularExpressionValidator>
                    <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                        runat="server" TargetControlID="txtEffectiveDate">
                    </ajaxToolKit:CalendarExtender>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div>
                    <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                        runat="server" AlternateText="Submit" OnClick="Update_Click" CausesValidation="true" ValidationGroup="skuprofile" />
                    <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                        runat="server" AlternateText="Cacnel" OnClick="Cancel_Click" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>

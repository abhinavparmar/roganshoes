using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace SiteAdmin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the Site Admin - Admin_Secure_catalog_product_category_Addrelatedproducts class
    /// </summary>
    public partial class Addrelatedproducts : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int _ItemId = 0;
        private string ViewPage = "View.aspx?itemid=";
        private string PortalIds = string.Empty;
        #endregion

        /// <summary>
        /// Gets or sets the item Id.
        /// </summary>
        public int ItemId
        {
            get { return _ItemId; }
            set { _ItemId = value; }
        }

        #region Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PortalIds = UserStoreAccess.GetAvailablePortals;

            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        /// <summary>
        /// Submit button click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Submit_Click(object sender, EventArgs e)
        {
            this.RememberOldValues();
            ProductCategoryAdmin prodCategoryAdmin = new ProductCategoryAdmin();
            Dictionary<int, string> productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];

            if (productIdList != null)
            {
                StringBuilder sb = new StringBuilder();
                StringBuilder productName = new StringBuilder();

                foreach (KeyValuePair<int, string> pair in productIdList)
                {
                    // Get ProductId
                    int productId = pair.Key;
                    if (!prodCategoryAdmin.ProductExists(productId, this.ItemId))
                    {
                        prodCategoryAdmin.Insert(productId, this.ItemId);
                        productName.Append(pair.Value + ",");
                    }
                    else
                    {
                        sb.Append(pair.Value + ",");
                        lblError.Visible = true;
                    }
                }

                Session.Remove("CHECKEDITEMS");

                if (sb.ToString().Length > 0)
                {
                    sb.Remove(sb.ToString().Length - 1, 1);

                    // Display Error message
                    lblError.Text = "The following product(s) are already associated with this category.<br/>" + sb.ToString();

                    foreach (GridViewRow row in uxGrid.Rows)
                    {
                        CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

                        if (check != null)
                        {
                            check.Checked = false;
                        }
                    }
                }
                else
                {
                    CategoryAdmin categoryAdmin = new CategoryAdmin();
                    Category category = categoryAdmin.GetByCategoryId(this.ItemId);

                    string AssociationName = "Associated Product " + productName + "  to Category " + category.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociationName, category.Name);

                    Response.Redirect(this.ViewPage + this.ItemId + "&mode=1");
                }
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ViewPage + this.ItemId + "&mode=1");
        }

        /// <summary>
        /// Search Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchProduct();
        }

        /// <summary>
        /// Clear Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddrelatedProducts.aspx?itemid=" + this.ItemId);
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.RememberOldValues();
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchProduct();
        }

        /// <summary>
        /// Grid Row Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Dictionary<int, string> productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];

                if (productIdList != null)
                {
                    CheckBox check = (CheckBox)e.Row.Cells[0].FindControl("chkProduct") as CheckBox;
                    int id = int.Parse(e.Row.Cells[1].Text);

                    if (productIdList.ContainsKey(id) && check != null)
                    {
                        check.Checked = true;
                    }
                }
            }
        }

        /// <summary>
        /// Back Button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ViewPage + this.ItemId + "&Mode=1");
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Get ImagePath
        /// </summary>
        /// <param name="Imagefile">The value of Image File</param>
        /// <returns>Returns the Image path</returns>
        protected string GetImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(Imagefile);
        }

        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = categoryAdmin.GetByCategoryId(this.ItemId);

            CatalogAdmin catalogAdmin = new CatalogAdmin();
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                string[] stores = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess).Split(',');
                bool isFound = false;
                CategoryNodeService service = new CategoryNodeService();
                foreach (CategoryNode categoryNode in service.GetByCategoryID(this.ItemId))
                {
                    int found = Array.IndexOf(stores, categoryNode.CatalogID.ToString());
                    if (found != -1)
                    {
                        isFound = true;
                    }
                }

                if (!isFound)
                {
                    Response.Redirect("Default.aspx", true);
                }
            }

            lblTitle.Text = category.Name;
        }

        /// <summary>
        /// Search a Product by name,productnumber,sku,manufacturer,category,producttype
        /// </summary>
        private void BindSearchProduct()
        {
            string ProductType = string.Empty;
            if (!string.IsNullOrEmpty(ProductTypeId.Value))
            {
                ProductType = ProductTypeId.Value;
            }

            ProductAdmin prodadmin = new ProductAdmin();
            DataSet ds = prodadmin.SearchProductByNames(Server.HtmlEncode(txtproductname.Text.Trim()), txtproductnumber.Text.Trim(), txtsku.Text.Trim(), txtManufacturer.Text.Trim(), ProductType, txtCategory.Text.Trim(), this.PortalIds);

            if (ds.Tables.Count > 0)
            {
                pnlProductList.Visible = true;
                uxGrid.DataSource = ds;
                uxGrid.DataBind();
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Remember Old values method
        /// </summary>
        private void RememberOldValues()
        {
            Dictionary<int, string> productIdList = new Dictionary<int, string>();

            // Loop through the grid values
            foreach (GridViewRow row in uxGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

                // Check in the Session
                if (Session["CHECKEDITEMS"] != null)
                {
                    productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];
                }

                int id = int.Parse(row.Cells[1].Text);

                if (check.Checked)
                {
                    if (!productIdList.ContainsKey(id))
                    {
                        productIdList.Add(id, row.Cells[3].Text);
                    }
                }
                else
                {
                    productIdList.Remove(id);
                }
            }

            if (productIdList.Count > 0)
            {
                Session["CHECKEDITEMS"] = productIdList;
            }
            else
            {
                Session.Clear();
            }
        }
        #endregion
    }
}
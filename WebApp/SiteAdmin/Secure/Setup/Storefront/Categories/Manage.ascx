<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Categories.Manage" CodeBehind="Manage.ascx.cs" %>
<asp:UpdatePanel ID="updatepanelManage" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="FormView">
            <h4 class="SubTitle">
                <asp:Label ID="lblTitle" runat="server" Text="Edit Product Settings"></asp:Label></h4>
            <div>
                <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                Select Theme
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlThemeslist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlThemeslist_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <asp:Panel ID="pnlTemplateList" runat="server" Visible="false">
                <div class="FieldStyle">
                    Master Page Template
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlPageTemplateList" runat="server">
                    </asp:DropDownList>
                    <div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="grpTitle" ControlToValidate="ddlPageTemplateList"
                            CssClass="Error" Display="Dynamic" ErrorMessage="* Select Master Page Template" InitialValue="0">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlCssList" runat="server" Visible="false">
                <div class="FieldStyle">
                    CSS
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlCSSList" runat="server">
                    </asp:DropDownList>
                </div>
            </asp:Panel>
            <div class="RowStyle">
                <div class="FieldStyle">
                    Display Order
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtDisplayOrder" Text="" runat="server" />
                    <div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="grpTitle" ControlToValidate="txtDisplayOrder"
                            CssClass="Error" Display="Dynamic" ErrorMessage="* Enter a Display Order"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtDisplayOrder"
                            CssClass="Error" Display="Dynamic" ErrorMessage="* Enter a whole number." MaximumValue="999999999" ValidationGroup="grpTitle"
                            MinimumValue="1" Type="Integer"></asp:RangeValidator>
                    </div>
                </div>
            </div>
            <div class="RowStyle">
                <div class="FieldStyle">
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox Checked="true" ID="CheckPrdtCategoryEnabled" Text="Enable this product" runat="server" />
                </div>
            </div>
            <!-- Ends Product Category Part -->
            <div class="ClearBoth">
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" ValidationGroup="grpTitle" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

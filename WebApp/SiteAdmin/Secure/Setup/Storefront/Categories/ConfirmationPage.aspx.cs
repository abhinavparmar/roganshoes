using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the Site Admin - Admin_Secure_catalog_product_category_ConfirmationPage class
    /// </summary>
    public partial class ConfirmationPage : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private string _ProductCategoryName = string.Empty;
        private string CategoryName = string.Empty;
        private CategoryNodeService categoryNodeService = new CategoryNodeService();
        #endregion

        /// <summary>
        /// Gets or sets the product category name.
        /// </summary>
        public string ProductCategoryName
        {
            get { return _ProductCategoryName; }
            set { _ProductCategoryName = value; }
        }

        #region Page load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            this.BindData();
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the screen
        /// </summary>
        protected void BindData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = categoryAdmin.GetByCategoryId(this.ItemId);

            if (category != null)
            {
                CatalogAdmin catalogAdmin = new CatalogAdmin();
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess).Split(',');
                    bool isFound = false;
                    CategoryNodeService service = new CategoryNodeService();
                    TList<CategoryNode> categoryNodes = service.GetByCategoryID(this.ItemId);
                    if (categoryNodes.Count == 0)
                    {
                        isFound = true;
                    }

                    foreach (CategoryNode categoryNode in categoryNodes)
                    {
                        int found = Array.IndexOf(stores, categoryNode.CatalogID.ToString());
                        if (found != -1)
                        {
                            isFound = true;
                        }
                    }

                    if (!isFound)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }

                this.ProductCategoryName = category.Name;
            }
            else
            {
                throw new ApplicationException("Category could not be found.");
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Setup/Storefront/Categories/Default.aspx");
        }

        /// <summary>
        /// Delete Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            bool retval = false;

            try
            {
                Category category = new Category();
                CategoryNode categoryNode = new CategoryNode();
                CategoryAdmin categoryAdmin = new CategoryAdmin();

                category.CategoryID = this.ItemId;

                TList<CategoryNode> categoryNodes = categoryAdmin.GetCategoryNodeId(this.ItemId);

                foreach (CategoryNode node in categoryNodes)
                {
                    categoryNode.CategoryID = this.ItemId;
                    categoryNode.CategoryNodeID = node.CategoryNodeID;
                    retval = categoryAdmin.DeleteNode(categoryNode);
                }

                category = categoryAdmin.GetByCategoryId(this.ItemId);
                this.CategoryName = category.Name;

                retval = categoryAdmin.Delete(category);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            if (retval)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Category - " + this.CategoryName, this.CategoryName);

                Response.Redirect("~/SiteAdmin/Secure/Setup/Storefront/Categories/Default.aspx");
            }
            else
            {
                lblMsg.Text = "An error occurred and the category could not be deleted. Please ensure that this category does not contain child categories or products. If it does, then delete the child categories and products first.";
            }
        }
        #endregion
    }
}
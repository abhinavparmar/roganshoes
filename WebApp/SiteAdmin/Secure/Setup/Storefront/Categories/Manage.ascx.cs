using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the Site admin - Admin_Secure_catalog_product_category_Manage user control class
    /// </summary>
    public partial class Manage : System.Web.UI.UserControl
    {
        #region Public Events
        public System.EventHandler SubmitButtonClicked;
        public System.EventHandler CancelButtonClicked;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the ItemId
        /// </summary>
        public int ItemId
        {
            get
            {
                if (ViewState["ItemId"] != null)
                {
                    return int.Parse(ViewState["ItemId"].ToString());
                }
                
                return 0;
            }

            set
            {
                ViewState["ItemId"] = value;
            }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Bind Data Method
        /// </summary>
        public void BindData()
        {
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            ProductCategory productCategory = productCategoryAdmin.GetByProductCategoryID(this.ItemId);

            if (productCategory != null)
            {
                ddlPageTemplateList.SelectedValue = productCategory.MasterPage;
                if (!string.IsNullOrEmpty(productCategory.DisplayOrder.ToString()))
                {
                    txtDisplayOrder.Text = productCategory.DisplayOrder.GetValueOrDefault().ToString();
                }
                else
                {
                    txtDisplayOrder.Text = "99";
                }
                CheckPrdtCategoryEnabled.Checked = productCategory.ActiveInd;

                if (productCategory.Theme != null)
                {
                    ddlThemeslist.SelectedValue = productCategory.Theme;
                }

                if (ddlThemeslist.SelectedValue != "0")
                {
                    pnlTemplateList.Visible = true;
                    pnlCssList.Visible = true;
                    ddlPageTemplateList.Items.Clear();
                    ddlCSSList.Items.Clear();
                    this.BindMasterPageTemplates();
                    this.BindCssList();

                    if (productCategory.CSS != null)
                    {
                        ddlCSSList.SelectedValue = productCategory.CSS;
                    }

                    if (productCategory.MasterPage != null)
                    {
                        ddlPageTemplateList.SelectedValue = productCategory.MasterPage;
                    }
                }
                else
                {
                    pnlTemplateList.Visible = false;
                    pnlCssList.Visible = false;
                }
            }

            updatepanelManage.Update();
        }

        /// <summary>
        /// Binds the themes to the DropDownList
        /// </summary>
        public void BindThemeList()
        {
            System.IO.DirectoryInfo path = new System.IO.DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/"));
            ddlThemeslist.DataSource = path.GetDirectories();
            ddlThemeslist.DataBind();
            ListItem li = new ListItem("Same as Store", "0");
            ddlThemeslist.Items.Insert(0, li);

            // Remove the svn hidden path folder.
            ddlThemeslist.Items.Remove(".svn");
        }

        /// <summary>
        /// Bind MasterPages
        /// </summary>
        public void BindMasterPageTemplates()
        {
            if (ddlThemeslist.SelectedIndex != 0)
            {
                pnlTemplateList.Visible = true;

                // Create instance for direcoryInfo and specify the directory 'MasterPages/Product'
                DirectoryInfo directoryInfo = new DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + ddlThemeslist.SelectedItem.Text + "/MasterPages/Product/"));

                // Determine whether the directory 'MasterPages/Product' exists.
                if (directoryInfo.Exists)
                {
                    // Returns a master file list from the current directory.
                    FileInfo[] masterFiles = directoryInfo.GetFiles("*.master");

                    foreach (FileInfo masterPage in masterFiles)
                    {
                        string fileName = masterPage.Name;

                        // Name only
                        fileName = fileName.Replace(".master", string.Empty); 

                        ddlPageTemplateList.Items.Add(fileName);
                    }
                }

                // Master template
                ListItem li = new ListItem("Select Template", "0");
                ddlPageTemplateList.Items.Insert(0, li);
            }
        }

        #endregion
        
        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindThemeList();
                this.BindData();
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            bool status = false;
            if (this.ItemId > 0)
            {
                ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
                ProductCategory productCategory = productCategoryAdmin.GetByProductCategoryID(this.ItemId);
                
                // Theme
                if (ddlThemeslist.Items.Count > 0 && ddlThemeslist.SelectedIndex != 0)
                {
                    productCategory.Theme = ddlThemeslist.SelectedItem.Text;
                }
                else
                {
                    productCategory.Theme = null;
                }

                // MasterPage
                if (ddlPageTemplateList.Items.Count > 0 && ddlPageTemplateList.SelectedIndex != 0)
                {
                    productCategory.MasterPage = ddlPageTemplateList.SelectedItem.Text;
                }
                else
                {
                    productCategory.MasterPage = null;
                }

                // CSS
                if (ddlCSSList.Items.Count > 0 && ddlCSSList.SelectedIndex != 0)
                {
                    productCategory.CSS = ddlCSSList.SelectedItem.Text;
                }
                else
                {
                    productCategory.CSS = null;
                }

                productCategory.DisplayOrder = int.Parse(txtDisplayOrder.Text.Trim());
                productCategory.ActiveInd = CheckPrdtCategoryEnabled.Checked;

                status = productCategoryAdmin.Update(productCategory);
            }

            if (status)
            {
                if (this.SubmitButtonClicked != null)
                {
                    this.SubmitButtonClicked(sender, e);
                }
            }
            else
            {
                lblMsg.Text = "Could not update the product template. Please try again.";
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.CancelButtonClicked != null)
            {
                this.CancelButtonClicked(sender, e);
            }
        }
        
        /// <summary>
        /// Country option changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlThemeslist_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlThemeslist.SelectedIndex > 0)
            {
                pnlTemplateList.Visible = true;
                pnlCssList.Visible = true;
                ddlPageTemplateList.Items.Clear();
                ddlCSSList.Items.Clear();
                this.BindMasterPageTemplates();
                this.BindCssList();
            }
            else
            {
                pnlTemplateList.Visible = false;
                pnlCssList.Visible = false;
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind the CSS to the DropDownList
        /// </summary>
        private void BindCssList()
        {
            if (ddlThemeslist.SelectedIndex != 0)
            {
                pnlCssList.Visible = true;
                System.IO.DirectoryInfo path = new System.IO.DirectoryInfo(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + ddlThemeslist.SelectedItem.Text + "/"));
                DirectoryInfo[] directoryInfoList = path.GetDirectories("Css"); 

                foreach (DirectoryInfo directory in directoryInfoList)
                {
                    // Returns a master file list from the current directory.
                    FileInfo[] masterFiles = directory.GetFiles("*.css");

                    foreach (FileInfo masterPage in masterFiles)
                    {
                        string fileName = masterPage.Name;
                        
                        // Name only
                        fileName = fileName.Replace(".css", string.Empty); 

                        ddlCSSList.Items.Add(fileName);
                    }
                }

                ListItem li = new ListItem("Site CSS", "0");
                ddlCSSList.Items.Insert(0, li);
            }
        }

        #endregion
    }
}
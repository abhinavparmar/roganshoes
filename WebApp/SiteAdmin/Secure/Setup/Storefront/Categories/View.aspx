<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Storefront.Categories.View" ValidateRequest="false"
    Title="Manage Categories - View" CodeBehind="View.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<%@ Register Src="Manage.ascx" TagName="Manage" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <asp:UpdatePanel ID="UpdatePanelView" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true"
        RenderMode="Inline">
        <ContentTemplate>
            <div>
                <div class="LeftFloat" style="width: 80%">
                    <h1>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
                    <uc2:DemoMode ID="DemoMode1" runat="server" />
                </div>
                <div class="LeftFloat ImageButtons" style="width: 19%" align="right">
                    <asp:Button CssClass="Size175" ID="btnCancelTop" CausesValidation="False" Text="<< Back to List Page"
                        runat="server" OnClick="BtnCancel_Click"></asp:Button>
                </div>
            </div>
            <div class="ClearBoth" align="left">
                <br />
            </div>
            <div>
                <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
            </div>
            <div>
                <uc1:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <div>
                <ajaxToolKit:TabContainer ID="tabCategory" runat="server">
                    <ajaxToolKit:TabPanel ID="pnlCategory" runat="server">
                        <HeaderTemplate>
                            Category Information
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="ViewForm200">
                                <uc1:Spacer ID="Spacer15" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                                <div class="ImageButtons">
                                    <asp:Button ID="btnEditCategory" runat="server" Text="Edit Information"
                                        OnClick="BtnEditCategory_Click" />
                                </div>
                                <br />
                                <h4 class="SubTitle">General Settings</h4>
                                <div class="FieldStyle">
                                    Category Name
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID='lblName' runat='server'></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    Category Title
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID='lblCategoryTitle' runat='server'></asp:Label>&nbsp;
                                </div>
                                <div class="ClearBoth">
                                </div>
                                <br />
                                <h4 class="SubTitle">Display Settings</h4>
                                <div class="FieldStyle">
                                    Display Order
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblDisplayOrder" runat="server"></asp:Label>&nbsp;
                                </div>
                                <div style="display: none">
                                    <div class="FieldStyleImgA">
                                        Enable Category
                                    </div>
                                    <div class="ValueStyleImgA">
                                        <img id="chkCategoryEnabled" runat="server" />
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="FieldStyleA">
                                    Child Categories
                                </div>
                                <div class="ValueStyleA">
                                    <img id="chkDisplaySubCategory" runat="server" />
                                </div>
                                <br />
                                <%-- Perficient Custom Code:Starts--%>
                                <div class="FieldStyle">
                                    Show in Navigation
                                </div>
                                <div class="ValueStyle">
                                    <img id="imgHideCategory" runat="server" />&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    Follow Index
                                </div>
                                <div class="ValueStyle">
                                    <img id="imgIndexFollow" runat="server" />&nbsp;
                                </div>
                                <div id="dvShowSubCategory" runat="server">
                                    <div class="FieldStyle">
                                        Show Sub Category Listing
                                    </div>
                                    <div class="ValueStyle">
                                        <img id="imgSubCatgory" runat="server" />&nbsp;
                                    </div>
                                </div>
                                <%-- Perficient Custom Code:Starts--%>
                                <br />
                                <h4 class="SubTitle">Category Image</h4>
                                <div class="Image">
                                    <asp:Image ID="CategoryImage" runat="server" />
                                </div>
                                <br />
                                <h4 class="SubTitle">SEO Settings</h4>
                                <div class="FieldStyle">
                                    SEO Title
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblSEOTitle" runat="server"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    SEO Keywords
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblSEOMetaKeywords" runat="server"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    SEO Description
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblSEOMetaDescription" runat="server"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    SEO URL
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblSEOURL" runat="server"></asp:Label>&nbsp;
                                </div>
                                <br />
                                <h4 class="SubTitle">Descriptions</h4>
                                <div class="FieldStyle">
                                    Short Description
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblshortdescription" runat="server"></asp:Label>&nbsp;
                                </div>
                                <div class="MainStyleA">
                                    <div class="FieldStyleA">
                                        Long Description
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblLongDescription" runat="server"></asp:Label>&#160
                                    </div>
                                </div>
                                <div class="FieldStyle">
                                    Additional Description
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblAlternativeDesc" runat="server"></asp:Label>&nbsp;
                                </div>
                                <h4 class="SubTitle">Banner</h4>
                                <div class="MainStyle">
                                    <div class="FieldStyle">
                                        Category Banner
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblCategoryBanner" runat="server"></asp:Label>&#160
                                    </div>
                                </div>
                            </div>
                            <div class="ClearBoth">
                            </div>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="pnlProduct" runat="server">
                        <HeaderTemplate>
                            Associated Products
                        </HeaderTemplate>
                        <ContentTemplate>
                            <uc1:Spacer ID="Spacer1" SpacerHeight="7" SpacerWidth="3" runat="server"></uc1:Spacer>
                            <div>
                                <div class="TabDescription" style="width: 80%;">
                                    <p>The products displayed below are linked to this category. Click on "SETTINGS" to change the display order of a product on a category page.</p>
                                </div>
                                <div class="ButtonStyle" style="clear: right; float: right; padding-top: 10px; padding-right: 10px;">
                                    <zn:LinkButton ID="Addrelatedproducts" runat="server" ButtonType="Button" OnClick="Addrelatedproducts_Click" CausesValidation="False" Text="Add Product" ButtonPriority="Primary" />
                                </div>

                            </div>
                            <div class="ClearBoth"></div>
                            <br />
                            <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                            <asp:UpdatePanel ID="updPnlRelatedProductGrid" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="uxGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                                        runat="server" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid" Width="100%"
                                        OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowCommand="UxGrid_RowCommand"
                                        GridLines="None" AllowPaging="True" PageSize="10">
                                        <Columns>
                                            <asp:BoundField DataField="productid" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="ProductCategoryID" Visible="false" />
                                            <asp:BoundField DataField="name" HtmlEncode="false" HeaderText="Product Name" HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="displayorder" HeaderText="Display Order" HeaderStyle-HorizontalAlign="Left" />
                                            <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Edit" Text="Settings &raquo" ValidationGroup="grpProduct" CommandArgument='<%# Eval("ProductCategoryID") %>'
                                                        CommandName="Manage" CssClass="actionlink" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Delete" Text="Remove &raquo" ValidationGroup="grpProduct" CommandArgument='<%# Eval("productid") %>'
                                                        CommandName="RemoveItem" CssClass="actionlink" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No products found
                                        </EmptyDataTemplate>
                                        <RowStyle CssClass="RowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                        <FooterStyle CssClass="FooterStyle" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                </ajaxToolKit:TabContainer>
            </div>
            <div>
                <uc1:Spacer ID="Spacer9" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <ajaxToolKit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="btnShowPopup"
                PopupControlID="pnlManage" BackgroundCssClass="modalBackground" />
            <asp:Panel runat="server" ID="pnlManage" Style="display: none;" CssClass="PopupStyle"
                Width="400">
                <div>
                    <ZNode:Manage ID="uxManage" runat="server" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Default" Title="Untitled Page" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">


    <div class="LeftMargin">

        <h1>Storefront</h1>
        <hr />
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/stores.png" />
            </div>
            <div class="Shortcut"><a id="A10" href="~/SiteAdmin/Secure/Setup/Storefront/Stores/Default.aspx" runat="server">Stores</a></div>
            <div class="LeftAlign">
                <p>Set up stores, associate a catalog, and create URLs to access the store.</p>
            </div>
        </div>
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/catalog.png" />
            </div>
            <div class="Shortcut"><a id="A1" href="~/SiteAdmin/Secure/Setup/Storefront/Catalogs/Default.aspx" runat="server">Catalogs</a></div>
            <div class="LeftAlign">
                <p>Catalogs are groupings of selected categories that you want to display in your store (example: Summer Catalog).</p>
            </div>

        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/departments.png" />
            </div>
            <div class="Shortcut"><a id="A8" href="~/SiteAdmin/Secure/Setup/Storefront/Categories/Default.aspx" runat="server">Categories</a></div>
            <div class="LeftAlign">
                <p>Categories are hierarchical groupings of products in your catalog (example: Apparel, Electronics, etc).</p>
            </div>
        </div>

          <h1>Checkout</h1>
        <hr />





        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/payments.png" />
            </div>
            <div class="Shortcut"><span class="Icon"><a id="A4" href="~/SiteAdmin/Secure/Setup/Checkout/Payments/Default.aspx" runat="server">Payments</a></div>
            <div class="LeftAlign">
                <p>Set up payment options such as credit cards, purchase orders, and PayPal. </p>
            </div>
        </div>


        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/Shipping.png" />
            </div>

            <div class="Shortcut"><span class="Icon"><a id="A6" href="~/SiteAdmin/Secure/Setup/Checkout/Shipment/default.aspx" runat="server">Shipping</a></div>
            <div class="LeftAlign">
                <p>Set up shipping options such as Custom Rates, UPS, and FedEx.</p>
            </div>
        </div>


        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/taxclasses.png" />
            </div>
            <div class="Shortcut"><span class="Icon"><a id="A7" href="~/SiteAdmin/Secure/Setup/Checkout/Taxes/default.aspx" runat="server">Taxes</a></div>
            <div class="LeftAlign">
                <p>Set up rules to compute the correct sales taxes during checkout.</p>
            </div>
        </div>

        <h1>Content</h1>
        <hr />

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ManageMessages.png" />
            </div>
            <div class="Shortcut">
                <a id="A11" href="~/SiteAdmin/Secure/Setup/Content/Messages/Default.aspx" runat="server">Manage
                Messages</a>
            </div>
            <div class="LeftAlign">
                <p>
                    Edit messages displayed in different areas of your store. Messages can be plain text or rich text and can include images.
                </p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ManageBanners.png" />
            </div>
            <div class="Shortcut">
                <a id="A13" href="~/SiteAdmin/Secure/Setup/Content/Banners/Default.aspx" runat="server">Manage
                Banners</a>
            </div>
            <div class="LeftAlign">
                <p>
                    You can use this option to create or edit banner messages on your site.
                </p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ManageContent.png" />
            </div>
            <div class="Shortcut">
                <a id="A14" href="~/SiteAdmin/Secure/Setup/Content/Pages/Default.aspx" runat="server">Manage Content
                Pages</a>
            </div>
            <div class="LeftAlign">
                <p>
                    You can create or edit static content pages (ex: "About Us") in your store.
            You can add rich text, images, flash and other media to these pages using a WYSIWYG
            editor.
                </p>
            </div>
        </div>
    </div>

   
</asp:Content>


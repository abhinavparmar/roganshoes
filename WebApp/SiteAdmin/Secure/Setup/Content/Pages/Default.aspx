<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Setup.Content.Pages.Default" Title="Manage Pages" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 50%">
            <h1>Manage Pages</h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAdd_Click" Text="Add New Page"
                ButtonPriority="Primary" />
        </div>
        <div>
            <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
    </div>
    <div class="SearchForm">
        <div class="RowStyle">
            <div class="ItemStyle">
                <span class="FieldStyle">Store Name</span><br />
                <span class="ValueStyle">
                    <asp:DropDownList ID="ddlPortal" runat="server"></asp:DropDownList></span>
            </div>
           
            <div class="ItemStyle">
                <span class="FieldStyle">Page Name</span><br />
                <span class="ValueStyle">
                    <asp:TextBox ID="txtPageName" runat="server"></asp:TextBox></span>
                <div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                        ErrorMessage="The page name can only contain letters, numbers, '_' and '-'."
                        ControlToValidate="txtPageName" ValidationExpression="^[a-zA-Z0-9_-]+$"></asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
    </div>

    <div class="ClearBoth" align="left"></div>

    <div>
        <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
            runat="server" AlternateText="Search" OnClick="BtnSearch_Click" CausesValidation="true" />
        <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnClear_Click" />
    </div>

    <div>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label></div>
    <br />

    <h4 class="GridTitle">Page List</h4>
    <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
    <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
        CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
        CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
        OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText="No content pages exist in the database.">
        <Columns>
            <asp:BoundField DataField="ContentPageID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="Name" HeaderText="Page Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="25%" />

            <asp:BoundField DataField="MasterPage" HeaderText="Page Template" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="20%" />
            <asp:TemplateField HeaderText="Store Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
                <ItemTemplate>
                    <%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>
                </ItemTemplate>
            </asp:TemplateField>
        

            <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                <ItemTemplate>
                    <asp:LinkButton ID="Button1" CommandName="Edit" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ContentPageID")%>'
                        Text="Edit &raquo" runat="server" CssClass="actionlink" />
                    &nbsp;<asp:LinkButton ID="LinkButton1" CommandName="Revert" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ContentPageID")%>'
                        Text="View Revisions &raquo" runat="server" CssClass="actionlink" />
                    &nbsp;<asp:LinkButton ID="Button5" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ContentPageID")%>'
                        Text="Delete &raquo" runat="server" CssClass="actionlink" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle CssClass="FooterStyle" />
        <RowStyle CssClass="RowStyle" />
        <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
    </asp:GridView>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
    </div>
</asp:Content>

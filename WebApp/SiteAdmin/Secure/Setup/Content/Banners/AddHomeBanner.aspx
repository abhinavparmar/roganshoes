﻿<%@ Page Title="Add Home Banner" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="AddHomeBanner.aspx.cs" Inherits="WebApp.SiteAdmin.Secure.Setup.Content.Banners.AddHomeBanner" ValidateRequest="false" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="Form">
        <asp:XmlDataSource ID="XmlDataSource1" runat="server"></asp:XmlDataSource>
        <br />
        <div style="width: 870px; display: table-cell;">
            <div class="LeftFloat">
                <h1>
                    <asp:Label ID="lbltitle" runat="server"></asp:Label></h1>
            </div>
            <div class="RightFloat">
                <asp:ImageButton ID="btnSaveTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit"
                    CausesValidation="true" OnClick="BtnSubmit_Click" />
                <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
        <div>
            <asp:Label ID="lblmsg" runat="server" Font-Bold="True" CssClass="Error"></asp:Label>
        </div>
        <br />
        <div class="FormView">
            <div class="ValueStyle">
                <ZNode:StoreName ID="uxStoreName" runat="server" VisibleLocaleDropdown="false" />
            </div>
            <div class="FieldStyle">
                Banner Name<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBannerName" runat="server" Width="350"> </asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="txtBannerName" CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Banner Name" ID="reqDescription" runat="server"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                Begin date (MM/DD/YYYY)<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtStartDate" Text="04/14/2015" runat="server" />
                <asp:ImageButton CausesValidation="false" ID="imgbtnStartDt" runat="server" ImageAlign="AbsMiddle"
                    ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtStartDate"
                    CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Start Date" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator CssClass="Error" ID="RequiredFieldValidator4" runat="server"
                    ControlToValidate="txtStartDate" Display="Dynamic" ErrorMessage="* Enter a valid start Date"
                    SetFocusOnError="True">
                </asp:RequiredFieldValidator>
            </div>
            <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                runat="server" TargetControlID="txtStartDate" PopupPosition="TopRight">
            </ajaxToolKit:CalendarExtender>
            <div class="FieldStyle">
                End date (MM/DD/YYYY)<span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtEndDate" Text="04/14/2015" runat="server" />
                <asp:ImageButton CausesValidation="false" ID="ImgbtnEndDt" runat="server" ImageAlign="AbsMiddle"
                    ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />
                <div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEndDate"
                        CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format"
                        ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
                    </asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator CssClass="Error" ID="RequiredFieldValidator5" runat="server"
                        ControlToValidate="txtEndDate" Display="Dynamic" ErrorMessage="* Enter a valid Exp Date"
                        SetFocusOnError="True">
                    </asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareEndDate" runat="server" ControlToCompare="txtStartDate"
                        ControlToValidate="txtEndDate" Display="Dynamic" CssClass="Error" ErrorMessage="The Expiration date must be later than the Start Date"
                        Operator="GreaterThanEqual" Type="Date">
                    </asp:CompareValidator>
                </div>
            </div>
            <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
                runat="server" TargetControlID="txtEndDate" PopupPosition="TopRight">
            </ajaxToolKit:CalendarExtender>
             <div class="FieldStyle">
                Display Order <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDisplayOrder" runat="server" Width="350"> </asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="txtDisplayOrder" CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Dispaly Order" ID="RequiredFieldValidator1" runat="server"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="rvMinQuantity" runat="server" ControlToValidate="txtDisplayOrder"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter a number only." MaximumValue="999999"
                    MinimumValue="0" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
            </div>
             <div class="FieldStyle" style="display:none">
               Is Banner Active
            </div>
            <div class="ValueStyle" style="display:none">
                <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" />
            </div>
            <div class="FieldStyle">
                Banner Content
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
            </div>
        </div>
    </div>
    <br />

    <asp:HiddenField ID="hdnHomePageBannerID" runat="server" />
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="true" CodeBehind="HomePageBanners.aspx.cs" Inherits="WebApp.SiteAdmin.Secure.Setup.Content.Banners.HomePageBanners" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName"
    TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="Form">
        <h1>Manage Home Banners</h1>
        <div>
            <p>
                You can create more than one banner per page in which case the banners will alternately rotate.  If you choose not to create any banners for a page then nothing will be displayed.
            </p>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False"
                    ButtonType="Button" OnClick="BtnAdd_Click" Text="Add Home Banner"
                    ButtonPriority="Primary" />
            </div>
            <div>
                <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <h4 class="SubTitle">Search Banners</h4>
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Store Name</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlPortal" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Banner Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtBannerName" runat="server"></asp:TextBox>
                            <div>
                                <asp:RegularExpressionValidator ID="MessageValidator" runat="server" ErrorMessage="Please enter a search string which does not contain special characters."
                                    ControlToValidate="txtBannerName" meta:resourceKey="MessageValidator" Display="Dynamic"
                                    ValidationExpression='[A-Za-z0-9\s]*' CssClass="Error"></asp:RegularExpressionValidator>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
            <div class="ClearBoth" align="left">
            </div>
            <div>
                <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                    runat="server" AlternateText="Search" OnClick="BtnSearch_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnClear_Click" />
            </div>
            <div>
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
            </div>
            <br />
            <h4 class="GridTitle">Available Home Page Banners</h4>
            <asp:GridView ID="uxGrid" runat="server" PageSize="50" CssClass="Grid" AllowPaging="True"
                AutoGenerateColumns="False" CellPadding="4" OnRowCommand="UxGrid_RowCommand"
                GridLines="None" CaptionAlign="Left" Width="100%" AllowSorting="True" EmptyDataText="No home banner available."
                OnRowCreated="UxGrid_RowCreated" OnRowDataBound="UxGrid_DataBound" OnPageIndexChanging="UxGrid_PageIndexChanging"
                OnRowDeleting="UxGrid_RowDeleting">
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                <Columns>
                    <asp:BoundField DataField="BannerName" HtmlEncode="false" HeaderText="Banner Name" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Store Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblStoreName" runat="server" Text='<%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="StartDate" HeaderText="Start Date" DataFormatString="{0:MM/dd/yyyy}"  HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM/dd/yyyy}"  HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Button" runat="server" Text="Manage &raquo" CommandName="Edit"
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem,"BannerID") %>' CssClass="actionlink" />
                            <asp:LinkButton ID="lbDelete" runat="server" Text="Delete Banner &raquo" CommandName="Delete"
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem,"BannerID") %>' CssClass="actionlink" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PortalID" HeaderStyle-HorizontalAlign="Left" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

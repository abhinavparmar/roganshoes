﻿<%@ Page Title="Edit Banner" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="AddBanner.aspx.cs"
    Inherits="SiteAdmin.Secure.Setup.Content.Banners.AddBanner" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName"
    TagPrefix="ZNode" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="Form">
        <asp:XmlDataSource ID="XmlDataSource1" runat="server"></asp:XmlDataSource>
        <br />
        <div style="width: 870px; display: table-cell;">
            <div class="LeftFloat">
                <h1>
                    <asp:Label ID="lbltitle" runat="server"></asp:Label></h1>
            </div>
            <div class="RightFloat">
                <asp:ImageButton ID="ImageButton1" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" ValidationGroup="Messages"
                    CausesValidation="true" />
                <asp:ImageButton ID="ImageButton2" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
        <br />
        <div class="FormView">
            <div class="ValueStyle">
                <ZNode:StoreName ID="uxStoreName" runat="server" VisibleLocaleDropdown="false"/>
            </div>
            <div class="FieldStyle" style="margin-top:-13px;">
                Location<br />
                <small>Use only characters a-z and 0-9. Use "-" instead of spaces. </small>
            </div>
            <asp:UpdatePanel runat="server" ID="upBanner">
                <ContentTemplate>
                    <div class="ValueStyle" style="margin-top:-10px;">
                        <asp:DropDownList ID="ddlMessageKey" runat="server" Width="350">
                        </asp:DropDownList>
                        <asp:Panel runat="server" ID="pnlMessageKey">
                            <asp:TextBox ID="txtMessageKey" runat="server" Width="350"> </asp:TextBox>
                            <asp:ImageButton ID="btnMessageKey" ImageAlign="Bottom" CausesValidation="False"
                                onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                                runat="server" AlternateText="Cancel" />
                            <asp:RequiredFieldValidator ID="rfvMessageKey" runat="server" ErrorMessage="* Enter message key"
                                ControlToValidate="txtMessageKey" CssClass="Error" Display="Dynamic" SetFocusOnError="True"
                                ValidationGroup="Messages"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMessageKey"
                                CssClass="Error" Display="Dynamic" ValidationGroup="Messages" ErrorMessage="Enter a valid message key."
                                SetFocusOnError="True" ValidationExpression="([A-Za-z0-9]+)"></asp:RegularExpressionValidator>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="ClearBoth" align="left">
            </div>
            <div class="FieldStyle">
                SEO Page URL<br />
                <small>Use only characters a-z and 0-9. Use "-" instead of spaces. Specify a valid category
                    / product / content page SEO URL (Ex: Nuts, Flowers)</small>

            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOName" runat="server" Width="350"> </asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtSEOName"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter valid SEO firendly URL"
                    SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)" ValidationGroup="Messages"></asp:RegularExpressionValidator>
            </div>
            <div class="ValueStyle">
                <small>NOTE: If you do not specify any URL, the banner will be displayed to the location mentioned.</small>
            </div>
            <div class="ClearBoth" align="left">
            </div>
            <div class="FieldStyle">
                Description
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDescription" runat="server" Width="350"> </asp:TextBox>
            </div>
            <div class="FieldStyle">
                Banner Content
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="ClearBoth" align="left">
            </div>
        </div>
        <br />
        <div>
            <asp:ImageButton ID="SubmitButton" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" ValidationGroup="Messages"
                CausesValidation="true" />
            <asp:ImageButton ID="CancelButton" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Submit" OnClick="BtnCancel_Click" />
        </div>
        <br />
        <div>
            <asp:Label ID="lblmsg" runat="server" Font-Bold="True" CssClass="Error"></asp:Label>
        </div>
        <asp:HiddenField ID="hdnMessageID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnMessageKey" runat="server" Value="" />
        <asp:HiddenField ID="hdnDescription" runat="server" />
    </div>
</asp:Content>

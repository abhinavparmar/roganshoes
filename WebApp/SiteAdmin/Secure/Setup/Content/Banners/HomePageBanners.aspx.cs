﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace WebApp.SiteAdmin.Secure.Setup.Content.Banners
{
    public partial class HomePageBanners : System.Web.UI.Page
    {
        #region Private Variables
        private string EditLink = "AddHomebanner.aspx?itemid=";
        #endregion

        #region Helper Methods

        /// <summary>
        /// Get Store Name by PortalId
        /// </summary>
        /// <param name="portalID">The value of portalID</param>
        /// <returns>Returns the Store name</returns>
        public string GetStoreName(string portalID)
        {
            PortalAdmin portalAdmin = new PortalAdmin();

            return portalAdmin.GetStoreNameByPortalID(portalID);
        }

        /// <summary>
        /// Get Store Name by PortalId
        /// </summary>
        /// <param name="localeID">The value of localeID</param>
        /// <returns>Returns the locale name</returns>
        public string GetLocaleName(string localeID)
        {
            LocaleAdmin localeAdmin = new LocaleAdmin();

            return localeAdmin.GetLocaleNameByLocaleID(localeID);
        }

        /// <summary>
        /// Return the Searchdata
        /// </summary>
        public void BindSearchData()
        {
            PerfiHomePageBannerService perfiBannerService = new PerfiHomePageBannerService();
            TList<PerfiHomePageBanner> perfiBannerList = perfiBannerService.GetAll();

            DataSet ds = perfiBannerList.ToDataSet(false);
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "BannerName ASC";

            // Filter the Search data.
            if (ddlPortal.SelectedValue == "0")
            {
                dv.RowFilter = "BannerName like '%" + txtBannerName.Text + "%'";
            }
            else
            {
                dv.RowFilter = "BannerName like '%" + txtBannerName.Text + "%' and PortalID <> '0' and PortalID = '" + ddlPortal.SelectedValue + "'";
            }


            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindPortal();
                this.BindSearchData();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                int idx = row.DataItemIndex;

                int bannerID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.EditLink + bannerID );
                }

                if (e.CommandName == "Delete")
                {
                    PerfiHomePageBannerService perfiBannerService = new PerfiHomePageBannerService();
                    bool isDeleted = perfiBannerService.Delete(bannerID);
                    if (isDeleted)
                    {
                        this.BindSearchData();
                    }
                    else
                    {
                        lblMsg.Text = "Unable to delete the message. Please try again.";
                    }
                }
            }
        }

        /// <summary>
        /// Grid Row Deleted Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[5].Visible = false;
            }
        }

        /// <summary>
        /// Grid Row Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_DataBound(object sender, GridViewRowEventArgs e)
        {
            Label lblstore = (Label)e.Row.FindControl("lblStoreName");
            if (lblstore != null)
            {
                if (lblstore.Text == string.Empty)
                {
                    e.Row.Visible = false;
                }
            }
        }

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        /// <summary>
        /// Grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindSearchData();
        }
        #endregion

        #region Events
        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Add Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddHomeBanner.aspx");
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            ddlPortal.SelectedValue = "0";
            txtBannerName.Text = string.Empty;
            this.BindSearchData();
        }

        #endregion

        #region Private methods
        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = "ALL STORES";
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();

            foreach (ListItem listItem in ddlPortal.Items)
            {
                listItem.Text = Server.HtmlDecode(listItem.Text);
            }
        }



        #endregion
    }
}
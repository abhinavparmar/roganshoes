using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons

{
    /// <summary>
    /// Represents the Site Admin SiteAdmin.Secure.Marketing.Promotion.PromotionsAndCoupons.Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private string RedirectLink = "~/SiteAdmin/Secure/Marketing/Promotions/PromotionsAndCoupons/Default.aspx";
        private string _PromotionName = string.Empty;
        #endregion

        /// <summary>
        /// Gets or sets the Product Review Title
        /// </summary>
        public string PromotionName
        {
            get
            {
                return this._PromotionName;
            }

            set
            {
                this._PromotionName = value;
            }
        }

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemID from querystring        
            if (Request.Params["ItemID"] != null)
            {
                this.ItemId = int.Parse(Request.Params["ItemID"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// Delete Button click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ZNode.Libraries.Admin.PromotionAdmin PromotionAdmin = new ZNode.Libraries.Admin.PromotionAdmin();
            ZNode.Libraries.DataAccess.Entities.Promotion _promotion = new ZNode.Libraries.DataAccess.Entities.Promotion();
            bool check = false;

            _promotion = PromotionAdmin.GetByPromotionId(this.ItemId);

            check = PromotionAdmin.DeletePromotion(this.ItemId);

            if (check)
            {
                // Replace the Promtion Cache application object with new active promotions
                HttpContext.Current.Application["PromotionCache"] = PromotionAdmin.GetActivePromotions();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Promotion - " + _promotion.Name, _promotion.Name);
                Response.Redirect(this.RedirectLink);
            }
            else
            {
                lblErrorMsg.Text = "Delete action could not be completed. Please try again.";
                lblErrorMsg.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RedirectLink);
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind Promotion name
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.PromotionAdmin PromotionAdmin = new ZNode.Libraries.Admin.PromotionAdmin();
            ZNode.Libraries.DataAccess.Entities.Promotion _promotion = PromotionAdmin.GetByPromotionId(this.ItemId);

            if (_promotion != null)
            {
                this.PromotionName = _promotion.Name;
            }
        }

        #endregion
    }
}
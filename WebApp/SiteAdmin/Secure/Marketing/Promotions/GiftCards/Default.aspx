﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master"
    AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Promotions.GiftCards.Default" CodeBehind="Default.aspx.cs"  %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>Gift Cards               

            </h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddCoupon" runat="server"
                ButtonType="Button" OnClick="BtnAddCoupon_Click" Text="Add New Gift Card"
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth" align="left">
            <p>
                Create and Manage stored value gift cards for your customers.
            </p>
            <br />
        </div>
        <h4 class="SubTitle">Search Gift Cards</h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtName" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Balance</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtBalance" runat="server" ValidationGroup="grpSearch"></asp:TextBox>
                            <div>
                                <asp:RangeValidator ID="rvBalance" runat="server" ControlToValidate="txtBalance"
                                    CssClass="Error" Display="Dynamic" MaximumValue="99999" ErrorMessage="Enter a valid balance in between 0.01 to 99999.00"
                                    MinimumValue="0.01" CultureInvariantValues="true" Type="Currency"></asp:RangeValidator>
                            </div>
                        </span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Card Number</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtCardNumber" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Account ID</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAccountId" runat="server" ValidationGroup="grpSearch"></asp:TextBox>
                            <div>
                                <asp:RangeValidator ID="rvAccountID" runat="server" ControlToValidate="txtAccountId"
                                    Display="Dynamic" ErrorMessage="Enter a valid account number." CssClass="Error"
                                    MaximumValue="999999999" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                            </div>
                        </span></span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:CheckBox ID="chkDontShowExpired" runat="server" Text="Exclude Expired Cards"
                                Checked="true" ValidationGroup="grpSearch"></asp:CheckBox>
                        </span>
                        <br />
                        <span class="ValueStyle"></span>
                    </div>
                    <div class="ItemStyle">
                    </div>
                </div>
                <div class="ClearBoth">
                    <asp:ImageButton ID="btnClearSearch" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClearSearch_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
 

                </div>
            </div>
        </asp:Panel>
        <br />
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <h4 class="GridTitle">Gift Card List
        </h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
            AllowSorting="True" EmptyDataText="No gift cards available to display.">
            <Columns>
                <asp:BoundField DataField="GiftCardId" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='add.aspx?ItemID=<%# DataBinder.Eval(Container.DataItem, "GiftCardId").ToString()%>'>
                            <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CardNumber" HeaderText="Card Number" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="CreateDate" HeaderText="Create Date" DataFormatString="{0:MM/dd/yyyy}"
                    HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="ExpirationDate" HeaderText="Expiration Date" DataFormatString="{0:MM/dd/yyyy}"
                    HeaderStyle-HorizontalAlign="Left" />
                <%--<asp:BoundField DataField="Amount" HeaderText="Amount" DataFormatString="{0:0.00}"
                    ItemStyle-HorizontalAlign="Right" />--%>
                <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div align="left">
                            <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix()%>&nbsp;<%#String.Format("{0, 0:N2}",(DataBinder.Eval(Container.DataItem, "Amount")))%>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:ButtonField CommandName="Edit" Text="Edit &raquo" ButtonType="Link">
                    <ControlStyle CssClass="actionlink" />
                </asp:ButtonField>
                <asp:ButtonField CommandName="Delete" Text="Delete &raquo" ButtonType="Link">
                    <ControlStyle CssClass="actionlink" />
                </asp:ButtonField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master"
    AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Promotions.GiftCards.Add" ValidateRequest="false" 
    CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode"
    TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <asp:HiddenField ID="hdnReturnItem" runat="server" Value="" />
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <uc2:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth">
          
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <h4 class="SubTitle">
            General Information</h4>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
              <div class="FieldStyle">
                    Card Number</div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtCardNumber" Enabled="false" runat="server"></asp:TextBox></div>
                <div class="FieldStyle">
                    Gift Card Name<span class="Asterix">*</span></div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtGiftCardName" runat="server" Columns="25" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                        CssClass="Error" ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtGiftCardName"
                        Display="Dynamic" ErrorMessage=" * Enter Gift Card Name." SetFocusOnError="True"></asp:RequiredFieldValidator></div>
                <div class="FieldStyle">
                    Store Name</div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlStoreName" runat="server">
                    </asp:DropDownList>
                </div>
                  <div class="FieldStyle">
                    Gift Card Amount<span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator CssClass="Error" ID="rfvBalance" runat="server" ControlToValidate="txtAmount"
                        Display="Dynamic" ErrorMessage="* Enter Gift Card Amount." SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvBalance" runat="server" ControlToValidate="txtAmount" CssClass="Error"
                        Display="Dynamic" MaximumValue="99999" ErrorMessage="Enter a valid amount in between 0.01 to 99999.00"
                        MinimumValue="0.01" CultureInvariantValues="true" Type="Currency"></asp:RangeValidator>
                </div>              
                <div class="FieldStyle">
                    Expiration Date(MM/DD/YYYY)<span class="Asterix">*</span></div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtExpiryDate" runat="server" />
                    <asp:ImageButton ID="imgbtnStartDt" runat="server" CausesValidation="false" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" />
                    <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                        runat="server" TargetControlID="txtExpiryDate">
                    </ajaxToolKit:CalendarExtender>
                    <asp:RequiredFieldValidator
                        CssClass="Error" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtExpiryDate"
                        Display="Dynamic" ErrorMessage=" * Enter Expiration Date." SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtExpiryDate"
                        CssClass="Error" Display="Dynamic" ErrorMessage="<BR>* Enter Valid Expiration Date in MM/DD/YYYY format."
                        ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"></asp:RegularExpressionValidator>
                    <asp:RangeValidator ID="rvExpirationDate" 
                    runat="server"                     
                    ErrorMessage="<BR>Expiration date must be greater than current date." 
                    ControlToValidate="txtExpiryDate" 
                    Display="Dynamic"
                    CssClass="Error"
                    Type="Date"
                    ></asp:RangeValidator>
                </div>
                <div class="FieldStyle">
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkUseSingleAccount" AutoPostBack="true" Text="Enable this gift card for an existing customer account."
                        runat="server" OnCheckedChanged="ChkUseSingleAccount_CheckedChanged" />
                </div>
                <div runat="server" id="divAccountID" visible="false">
                    <div class="FieldStyle">
                        Account ID<span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtAccountID" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="Error" ID="rfvAccountID" Enabled="false" runat="server"
                            ControlToValidate="txtAccountID" Display="Dynamic" ErrorMessage="* Enter valid Account ID."
                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rvAmount" runat="server" ControlToValidate="txtAccountID"
                            Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999999"
                            MinimumValue="1" Type="Integer"></asp:RangeValidator>
                    </div>
                </div>
              
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ClearBoth">
        </div>
        <div class="Error">
            <asp:Label ID="lblError" runat="server" Visible="true"></asp:Label></div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
    <!--During Update Process -->
    <asp:UpdateProgress ID="UpdateProgressMozilla" runat="server" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <asp:Panel ID="Panel1" CssClass="overlay" runat="server">
                <asp:Panel ID="Panel2" CssClass="loader" runat="server">
                    Loading...<img id="Img1" align="absmiddle" src="~/SiteAdmin/Themes/images/loading.gif"
                        runat="server" />
                </asp:Panel>
            </asp:Panel>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Promotions.GiftCards.Delete"  Codebehind="Delete.aspx.cs" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" Runat="Server">

<uc1:DemoMode ID="DemoMode1" runat="server" />
    <h5>
        <asp:Label ID="deletecoupon" runat="server">Delete Gift Card - <b><%=GiftCardName%></b></asp:Label></h5>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <p>
        Please confirm if you want to delete this gift card.</p>
    <p>
        <asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error"></asp:Label></p>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
     <div>
            <asp:ImageButton ID="btnDelete" onmouseover="this.src='../../../../Themes/Images/buttons/button_delete_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_delete.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_delete.gif"
                runat="server" AlternateText="Delete" OnClick="BtnDelete_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    <div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    
</asp:Content>


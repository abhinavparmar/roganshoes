﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.Promotions.GiftCards
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Marketing.Promotion.GiftCards Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private int rmaItemId = 0;
        private string accountid = string.Empty;
        private string expirydate = string.Empty;
        private decimal amount = 0.0m;
        private string mode = string.Empty;
        private string returnitemid = string.Empty;
        private string qtylist = string.Empty;
        private string orderid = string.Empty;
        #endregion

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            rfvAccountID.Enabled = chkUseSingleAccount.Checked;
            rvExpirationDate.MinimumValue = DateTime.Today.Date.ToString("MM/dd/yyyy");
            rvExpirationDate.MaximumValue = DateTime.MaxValue.Date.ToString("MM/dd/yyyy");

            // Get promotionid from query string
            if (Request.Params["ItemID"] != null)
            {
                this.ItemId = int.Parse(Request.Params["ItemID"].ToString());
            }
            if (Request.Params["mode"] != null && Request.Params["mode"].ToString() == "RMA")
            {
                mode = "RMA";
                if (Request.Params["item"] != null)
                    this.rmaItemId = int.Parse(Request.Params["item"].ToString());
                if (Request.Params["items"] != null)
                    this.returnitemid = Request.Params["items"].ToString();
                if (Request.Params["qty"] != null)
                    this.qtylist = Request.Params["qty"].ToString();
                if (Request.Params["orderid"] != null)
                    this.orderid = Request.Params["orderid"].ToString();
                if (Request.Params["amount"] != null)
                    this.amount = Convert.ToDecimal(Request.Params["amount"].ToString());

            }


            if (!this.IsPostBack)
            {
                txtExpiryDate.Text = string.Empty;

                this.BindStores();

                if (this.ItemId > 0)
                {
                    this.BindEditData();
                }

                if (this.ItemId == 0)
                {
                    if (Request.Params["mode"] != null && Request.Params["mode"].ToString() == "RMA")
                    {
                        BindRMAData();
                    }


                    lblTitle.Text = "Add a New Gift Card";
                    ZNode.Libraries.ECommerce.Catalog.ZNodeProduct product = new ZNode.Libraries.ECommerce.Catalog.ZNodeProduct();
                    txtCardNumber.Text = product.GetNextGiftCardNumber();
                }
            }
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, ImageClickEventArgs e)
        {
            this.SubmitGiftCard();
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, ImageClickEventArgs e)
        {
            if (mode == "RMA" && Request.QueryString["flag"] != null)
                Response.Redirect("~/SiteAdmin/Secure/Orders/ReturnsManagement/RMAManager/RMARequests.aspx?mode=edit&itemid=" + rmaItemId + "&orderid=" + orderid + "&flag=append");
            if (mode == "RMA")
                Response.Redirect("~/SiteAdmin/Secure/Orders/ReturnsManagement/RMAManager/RMARequests.aspx?mode=edit&itemid=" + rmaItemId + "&orderid=" + orderid);
            Response.Redirect("Default.aspx");
        }

        /// <summary>
        /// Use Single Account Checked Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkUseSingleAccount_CheckedChanged(object sender, EventArgs e)
        {
            divAccountID.Visible = chkUseSingleAccount.Checked;
        }
        #region Bind Methods

        /// <summary>
        /// Bind RMA Data
        /// </summary>
        private void BindRMAData()
        {
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            DataSet dsItems = rmaRequestAdmin.GetRMARequestItem(returnitemid);

            foreach (DataRow dr in dsItems.Tables[0].Rows)
            {
                accountid = dr["AccountID"].ToString();
                int GCExpirationPeriod = Convert.ToInt32(dr["GCExpirationPeriod"]);
                expirydate = DateTime.Now.AddDays(GCExpirationPeriod).ToString("MM/dd/yyyy");
                break;
            }


            chkUseSingleAccount.Checked = true;
            divAccountID.Visible = chkUseSingleAccount.Checked;

            ddlStoreName.SelectedIndex = ddlStoreName.Items.IndexOf(ddlStoreName.Items.FindByValue(ZNodeConfigManager.SiteConfig.PortalID.ToString()));
            ddlStoreName.Enabled = false;
            txtAccountID.Text = accountid;
            txtAmount.Text = String.Format("{0:0.00}", amount);
            txtExpiryDate.Text = expirydate;
            txtAccountID.Enabled = false;
            txtAmount.Enabled = false;
            // txtExpiryDate.Enabled = false;
            //imgbtnStartDt.Enabled = false;

        }
        /// <summary>
        /// Bind Edit mode fields
        /// </summary>
        private void BindEditData()
        {
            ZNode.Libraries.Admin.GiftCardAdmin giftCardAdmin = new ZNode.Libraries.Admin.GiftCardAdmin();
            ZNode.Libraries.DataAccess.Entities.GiftCard giftCard = giftCardAdmin.GetByGiftCardId(this.ItemId);

            if (giftCard != null)
            {
                // General Section
                ddlStoreName.SelectedIndex = ddlStoreName.Items.IndexOf(ddlStoreName.Items.FindByValue(giftCard.PortalId.ToString()));
                txtGiftCardName.Text = Server.HtmlDecode(giftCard.Name);
                lblTitle.Text = "Edit Gift Card - " + txtGiftCardName.Text;
                txtCardNumber.Text = giftCard.CardNumber;
                if (giftCard.AccountId.HasValue)
                {
                    txtAccountID.Text = giftCard.AccountId.Value.ToString();
                    chkUseSingleAccount.Checked = true;
                    divAccountID.Visible = true;
                }

                if (giftCard.ExpirationDate.HasValue)
                {
                    txtExpiryDate.Text = giftCard.ExpirationDate.Value.ToString("MM/dd/yyyy");
                }

                txtAmount.Text = string.Format("{0:0.00}", giftCard.Amount);
                lblTitle.Text = "Edit Gift Card – " + giftCard.Name;

                // If Gift card is expired then disable the balance range validator. 
                if (giftCard.ExpirationDate == null || giftCard.ExpirationDate < DateTime.Today.Date)
                {
                    rvBalance.Enabled = false;
                    lblTitle.Text += " (Card Expired)";
                }
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Bind Stores.
        /// </summary>
        private void BindStores()
        {
            string portalIds = string.Empty;

            // Get Portals 
            portalIds = UserStoreAccess.GetAvailablePortals;
            string filterExpression = string.Empty;
            if (portalIds != "0")
            {
                filterExpression = "PortalID IN (" + portalIds + ")";
            }

            PortalService portalServ = new PortalService();
            ddlStoreName.Visible = true;
            TList<Portal> portalList = portalServ.GetAll();
            DataSet ds = portalList.ToDataSet(false);
            ds.Tables[0].DefaultView.RowFilter = filterExpression;

            if (ds.Tables[0].DefaultView.Count > 0)
            {
                ddlStoreName.DataSource = ds.Tables[0].Rows.Cast<DataRow>().Select(x => new { PortalID = x["PortalID"], StoreName = Server.HtmlDecode(x["StoreName"].ToString()) });
                ddlStoreName.DataTextField = "StoreName";
                ddlStoreName.DataValueField = "PortalID";
                ddlStoreName.DataBind();
            }
            else
            {
                lblError.Text = "You do not have permission to manage this gift cards. Please contact your administrator for permission.";
                btnSubmitBottom.Enabled = btnSubmitTop.Enabled = false;
            }
        }
        #endregion

        #region Submit Gift Card Data
        /// <summary>
        /// Submit Gift Card method
        /// </summary>
        private void SubmitGiftCard()
        {
            GiftCardAdmin giftCardAdmin = new GiftCardAdmin();
            ZNode.Libraries.DataAccess.Entities.GiftCard giftCardEntity = null;

            if (!this.IsAccountExist())
            {
                lblError.Text = "Account ID doesn't exist.";
                return;
            }

            if (this.ItemId > 0)
            {
                giftCardEntity = giftCardAdmin.GetByGiftCardId(this.ItemId);
            }
            else
            {
                giftCardEntity = new ZNode.Libraries.DataAccess.Entities.GiftCard();
            }

            // Set properties
            giftCardEntity.PortalId = Convert.ToInt32(ddlStoreName.SelectedValue);
            giftCardEntity.Name = Server.HtmlEncode(txtGiftCardName.Text);
            giftCardEntity.CardNumber = txtCardNumber.Text;

            if (Page.User.Identity.IsAuthenticated)
            {
                ZNodeUserAccount account = ZNodeUserAccount.CurrentAccount();
                if (account != null)
                {
                    giftCardEntity.CreatedBy = account.AccountID;
                }
            }

            if (this.ItemId == 0)
            {
                // Set created date for new item only.
                giftCardEntity.CreateDate = DateTime.Today;
            }

            if (chkUseSingleAccount.Checked && txtAccountID.Text.Trim().Length > 0)
            {
                giftCardEntity.AccountId = Convert.ToInt32(txtAccountID.Text);
            }

            giftCardEntity.Amount = Convert.ToDecimal(txtAmount.Text);
            giftCardEntity.ExpirationDate = Convert.ToDateTime(txtExpiryDate.Text.Trim());

            bool isSuccess = false;

            if (this.ItemId > 0)
            {
                isSuccess = giftCardAdmin.UpdateGiftCard(giftCardEntity);
            }
            else
            {

                isSuccess = giftCardAdmin.AddGiftCard(giftCardEntity, out ItemId);
            }

            if (isSuccess)
            {
                #region RMA Block
                if (mode == "RMA")
                {
                    //Update RMA request status to "ReturnedOrRefunded"
                    RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
                    RMARequest rmaRequest = rmaRequestAdmin.GetByRMARequestID(this.rmaItemId);

                    if (rmaRequest != null)
                    {
                        rmaRequest.RequestStatusID = Convert.ToInt32(ZNodeRequestState.ReturnedOrRefunded); ;

                    }

                    rmaRequestAdmin.Update(rmaRequest);
                    string[] lineitems = returnitemid.Split(',');
                    string[] qtyItems = qtylist.Split(',');
                    //update RMAREquestId with GiftCardID
                    for (int i = 0; i < lineitems.Length; i++)
                    {
                        RMARequestItem item = new RMARequestItem();
                        int orderlineitemid = Convert.ToInt32(lineitems[i]);
                        item.RMARequestID = this.rmaItemId;
                        item.Quantity = Convert.ToInt32(qtyItems[i]);
                        item.OrderLineItemID = orderlineitemid;
                        item.GiftCardId = ItemId;
                        item.IsReturnable = true;
                        item.IsReceived = true;
                        rmaRequestAdmin.Add(item);

                    }
                    SendStatusMail(rmaItemId);
                    Response.Redirect("~/SiteAdmin/Secure/Orders/ReturnsManagement/RMAManager/RMARequests.aspx?mode=edit&itemid=" + rmaItemId + "&orderid=" + orderid + "&giftcardid=" + ItemId.ToString());
                    
                }
                #endregion
                Response.Redirect("Default.aspx");
            }
            else
            {
                lblError.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Send mail to user with 
        /// </summary>
        protected void SendStatusMail(int RMAItemId)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = ZNodeConfigManager.SiteConfig.StoreName;
            string ReceiverMailId = "";
            RMARequestAdmin rmaAdmin = new RMARequestAdmin();
            DataSet reportds = rmaAdmin.GetRMARequestReport(RMAItemId);
            if (reportds.Tables[0].Rows.Count > 0)
            {
                string sign = string.Empty;
                if (reportds.Tables[0].Rows[0]["EnableEmailNotification"].ToString() == "True" && reportds.Tables[0].Rows[0]["CustomerNotification"].ToString().Length > 0)
                {
                    sign += "Regards," + "<br />";
                    sign += ZNodeConfigManager.SiteConfig.StoreName + "<br />";
                    sign += reportds.Tables[0].Rows[0]["DepartmentName"].ToString() + "<br />";
                    sign += reportds.Tables[0].Rows[0]["DepartmentAddress"].ToString() + "<br />";
                    sign += reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString() + "<br />";

                    if (reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString().Length > 0)
                        senderEmail = reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString();
                    ReceiverMailId = reportds.Tables[0].Rows[0]["BillingEmailId"].ToString();

                    string messageText = "";


                    messageText = "Dear " + reportds.Tables[0].Rows[0]["CustomerName"].ToString() + "<br />";



                    messageText += "<br />";

                    messageText += reportds.Tables[0].Rows[0]["CustomerNotification"].ToString() + "<br />";

                    messageText += "<br />";
                    messageText += sign;


                    try
                    {
                        // Send mail
                        ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(ReceiverMailId, senderEmail, String.Empty, subject, messageText, true);
                    }
                    catch (Exception ex)
                    {
                        //lblErrorMsg.Text = "Unable to send mail";
                    }
                }
            }
        }


        /// <summary>
        /// Check the Account ID is exist.
        /// </summary>
        /// <returns>Returns true if Account ID exist else false.</returns>
        private bool IsAccountExist()
        {
            bool isExist = true;
            if (chkUseSingleAccount.Checked && txtAccountID.Text.Trim().Length > 0)
            {
                AccountAdmin accountAdmin = new AccountAdmin();
                Account account = accountAdmin.GetByAccountID(Convert.ToInt32(txtAccountID.Text));
                if (account == null)
                {
                    isExist = false;
                }
            }

            return isExist;
        }
        #endregion
    }
}
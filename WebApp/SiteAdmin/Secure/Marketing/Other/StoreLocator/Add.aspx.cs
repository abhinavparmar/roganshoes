using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace SiteAdmin.Secure.Marketing.Other.StoreLocator
{
    /// <summary>
    /// Represents the SiteAdmin.Secure.Marketing.Other.StoreLocator.Add class.
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int itemId = 0;
        private string associateName = string.Empty;
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["ItemID"] != null)
            {
                this.itemId = int.Parse(Request.Params["ItemID"].ToString());
            }

            if (!Page.IsPostBack)
            {
                if (this.itemId > 0)
                {
                    this.BindAccount();
                    this.BindEditData();
                    tblShowImage.Visible = true;
                    lblTitle.Text = "Edit Store Location";
                }
                else
                {
                    this.BindAccount();
                    StoreImage.Visible = false;
                    tblShowImage.Visible = true; 
                    RadioStoreCurrentImage.Visible = false;
                    RadioStoreNewImage.Checked = true;
                    tblStoreDescription.Visible = true;
                    lblTitle.Text = "Add Store Location";
                }
            }
        }

        #endregion        

        #region General Events

        /// <summary>
        /// Submit button click event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            string fileName = string.Empty;
            ZNode.Libraries.Admin.StoreLocatorAdmin storeLocatorAdmin = new StoreLocatorAdmin();
            ZNode.Libraries.DataAccess.Entities.Store store = new Store();
            if (this.itemId > 0)
            {
                store = storeLocatorAdmin.GetByStoreID(this.itemId);
            }

            store.Name = Server.HtmlEncode(txtstorename.Text);
            store.Address1 = Server.HtmlEncode(txtaddress1.Text);
            store.Address2 = Server.HtmlEncode(txtaddress2.Text);
            store.Address3 = Server.HtmlEncode(txtaddress3.Text);
            store.City = Server.HtmlEncode(txtcity.Text);
            store.State = Server.HtmlEncode(txtstate.Text);
            store.Zip = txtzip.Text;
            store.Phone = txtphone.Text;
            store.Fax = txtfax.Text;
            store.ContactName = Server.HtmlEncode(txtcname.Text);
            store.PortalID = uxStoreName.SelectedValue;
            store.Custom1 = Server.HtmlEncode(txtStoreUrl.Text);
            if (ListAccounts.SelectedValue == "0")
            {
                store.AccountID = null;
            }
            else
            {
                store.AccountID = Convert.ToInt32(ListAccounts.SelectedValue);  
            }

            if (txtdisplayorder.Text != string.Empty)
            {
                store.DisplayOrder = Convert.ToInt32(txtdisplayorder.Text);
            }
            else
            {
                store.DisplayOrder = null;
            }

            store.ActiveInd = chkActiveInd.Checked;

          if (RadioStoreNoImage.Checked == false)
            {
                // Validate image    
                if ((this.itemId == 0) || (RadioStoreNewImage.Checked == true))
                {
                    if (UploadStoreImage.PostedFile.FileName != string.Empty)
                    {
                        // Check for Store Image
                        fileName = System.IO.Path.GetFileNameWithoutExtension(UploadStoreImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(UploadStoreImage.PostedFile.FileName);


                        // Check for Store Image
                        if (fileName != string.Empty)
                        {
                            store.ImageFile = fileName;
                        }
                    }
                }
                else
                {
                    store.ImageFile = store.ImageFile;
                }
            }
            else
            {
                store.ImageFile = null;
            }

            // Upload File if this is a new Store or the New Image option was selected for an existing Store
            if (RadioStoreNewImage.Checked || this.itemId == 0)
            {
                if (fileName != string.Empty)
                {
                    byte[] imageData = new byte[UploadStoreImage.PostedFile.InputStream.Length];
                    UploadStoreImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadStoreImage.PostedFile.InputStream.Length);
                    ZNodeStorageManager.WriteBinaryStorage(imageData, ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName);
                }
            }

            bool isSuccess = false;

            if (this.itemId > 0)
            {
                isSuccess = storeLocatorAdmin.UpdateStore(store);
                this.associateName = "Edit of Store Location - " + txtstorename.Text;

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, txtstorename.Text);
            }
            else
            {
                isSuccess = storeLocatorAdmin.InsertStore(store);
                this.associateName = "Creation of Store Location - " + txtstorename.Text;

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, txtstorename.Text);
            }
            
            if (isSuccess)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                // Display error message
                lblError.Text = "An error occurred while updating. Please try again.";
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioStoreCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblStoreDescription.Visible = false;

            if (this.itemId > 0)
            {
                StoreImage.Visible = true;
            }
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioStoreNoImage_CheckedChanged(object sender, EventArgs e)
        {
            tblStoreDescription.Visible = false;
            StoreImage.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioStoreNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblStoreDescription.Visible = true;
            if (this.itemId > 0)
            {
                StoreImage.Visible = true;
            }
        }

        #endregion

        #region Bind Method

        /// <summary>
        /// Bind the edit data.
        /// </summary>
        private void BindEditData()
        {
            ZNode.Libraries.Admin.StoreLocatorAdmin storeLocatorAdmin = new ZNode.Libraries.Admin.StoreLocatorAdmin();
            ZNode.Libraries.DataAccess.Entities.Store store = storeLocatorAdmin.GetByStoreID(this.itemId);
            ListAccounts.SelectedValue = store.AccountID.ToString();
            uxStoreName.PreSelectValue = store.PortalID.ToString();
            txtstorename.Text = Server.HtmlDecode(store.Name);
            txtaddress1.Text = Server.HtmlDecode(store.Address1);
            txtaddress2.Text = Server.HtmlDecode(store.Address2);
            txtaddress3.Text = Server.HtmlDecode(store.Address3);
            txtcity.Text = Server.HtmlDecode(store.City);
            txtstate.Text = Server.HtmlDecode(store.State);
            txtzip.Text = store.Zip;
            txtphone.Text = store.Phone;
            txtfax.Text = store.Fax;
            txtcname.Text = Server.HtmlDecode(store.ContactName);
            txtdisplayorder.Text = store.DisplayOrder.ToString();
            chkActiveInd.Checked = (bool)store.ActiveInd;
            txtStoreUrl.Text = Server.HtmlDecode(store.Custom1);
            ZNodeImage znodeImage = new ZNodeImage();
            if (store.ImageFile != null)
            {
                StoreImage.ImageUrl = znodeImage.GetImageHttpPathSmall(Path.GetFileName(store.ImageFile));
            }
            else
            {
                StoreImage.ImageUrl = znodeImage.GetImageHttpPathSmall(string.Empty);
                RadioStoreNoImage.Checked = true;
                RadioStoreCurrentImage.Visible = false;
            }
        }

        /// <summary>
        /// Bind the accounts
        /// </summary>
        private void BindAccount()
        {
            DataSet ds = new DataSet();
            AccountAdmin accountAdmin = new AccountAdmin();
            PortalAdmin portalAdmin = new PortalAdmin();
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                ds = accountAdmin.GetAccountByPortal(profiles.StoreAccess);
            }
            else
            {
                ds = accountAdmin.GetAllCustomers();
            }

            // Load AccountID
            ListAccounts.DataSource = ds;
            ListAccounts.DataTextField = "AccountID";
            ListAccounts.DataValueField = "AccountID";
            ListAccounts.DataBind();
            ListItem li = new ListItem("Select","0");
            ListAccounts.Items.Insert(0,li);
        }

        #endregion
    }
}
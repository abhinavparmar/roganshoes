<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="SiteAdmin.Secure.Marketing.Other.StoreLocator.Delete" CodeBehind="Delete.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>
        <asp:Label ID="deletestore" runat="server">Delete Store</asp:Label>
        <uc1:DemoMode ID="DemoMode1" runat="server" />
    </h5>
    <p>
        Please confirm if you want to delete the Store "<b><%=StoreName%></b>".
    </p>
    <p>
        <asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error"></asp:Label>
    </p>
    <asp:ImageButton ID="btnDelete" onmouseover="this.src='../../../../Themes/Images/buttons/button_delete_highlight.gif';"
        onmouseout="this.src='../../../../Themes/Images/buttons/button_delete.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_delete.gif"
        runat="server" AlternateText="Delete" OnClick="BtnDelete_Click" CausesValidation="true" />
    <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
        onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    <br />
    <br />
    <br />
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="SiteAdmin.Secure.Marketing.Other.StoreLocator.Add" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                <uc2:DemoMode ID="DemoMode1" runat="server" />
            </h1>
        </div>
        <div style="text-align: right">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
            <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">General Information
        </h4>
        <div class="FieldStyle"  style="display:none">
            Select Account ID<span class="Asterix">*</span>
        </div>
        <div class="ValueStyle" style="display:none">
            <asp:DropDownList ID="ListAccounts" runat="server" Width="141px">
            </asp:DropDownList>
        </div>
        <div>
            <ZNode:StoreName ID="uxStoreName" runat="server" VisibleLocaleDropdown="false"/>
        </div>
        <div class="FieldStyle">
            Store Name<span class="Asterix">*</span><br />
            <small>Will be displayed in search results. For example: "Maxwell Foods - NYC"</small>

        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtstorename" runat="server" MaxLength="30" Columns="25"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtstorename"
                CssClass="Error" Display="Dynamic" ErrorMessage="* Add Store Name"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            Address Line 1
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtaddress1" runat="server" MaxLength="35" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            Address Line 2
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtaddress2" runat="server" MaxLength="35" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            Address Line 3
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtaddress3" runat="server" MaxLength="35" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            City
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtcity" runat="server" MaxLength="15" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            State / Province
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtstate" runat="server" MaxLength="15" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            Zip / Postal Code
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtzip" runat="server" MaxLength="15" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            Phone Number
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtphone" runat="server" MaxLength="25" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            Fax Number
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtfax" runat="server" MaxLength="25" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            Contact Name
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtcname" runat="server" MaxLength="30" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            Display Order<span class="Asterix">*</span><br />
            <small>Enter the order that this store should be displayed in the search results. Stores
                are sorted from lowest to highest.</small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtdisplayorder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                ErrorMessage="* Enter a Display Order" ControlToValidate="txtdisplayorder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtdisplayorder"
                Display="Dynamic" ErrorMessage="Enter a whole number." MaximumValue="999999999"
                MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            Display Store<br />
            <small>Check this box to display this store in the store locator search results.</small>
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkActiveInd" runat="server" Checked="true" Text="Display this store in search results" />
        </div>
         <div class="FieldStyle">
           Enter Store Url<br />
              <small>Enter store url for ex : http://www.roganshoes.com/scheduleatruck.aspx </small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtStoreUrl" runat="server"></asp:TextBox>
        </div>
        <h4 class="SubTitle">Store Image</h4>
        <small>Upload a suitable image for your Store. Only JPG, GIF and PNG images are supported.
            The file size should be less than 1.5 Meg. Your image will automatically be scaled
            so it displays correctly.</small>
        <asp:Panel ID="ImagePanel" runat="server">
            <div id="tblShowImage" runat="server" visible="false" style="margin: 10px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;">
                    <asp:Image ID="StoreImage" runat="server" />
                </div>
                <div class="LeftFloat" style="padding-left: 10px; margin-left: -1px; border-left: solid 1px #cccccc;">
                    <asp:Panel runat="server" ID="pnlShowOption">
                        <div>
                            Select an Option
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioStoreCurrentImage" Text="Keep Current Image" runat="server"
                                GroupName="Store Image" AutoPostBack="True" OnCheckedChanged="RadioStoreCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioStoreNewImage" Text="Upload New Image" runat="server" GroupName="Store Image"
                                AutoPostBack="True" OnCheckedChanged="RadioStoreNewImage_CheckedChanged" /><br />
                            <asp:RadioButton ID="RadioStoreNoImage" Text="No Image" runat="server" GroupName="Store Image"
                                AutoPostBack="True" OnCheckedChanged="RadioStoreNoImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblStoreDescription" runat="server" visible="false">
                        <div class="FieldStyle">
                            Select an Image:
                        </div>
                        <div class="ValueStyle">
                            <asp:FileUpload ID="UploadStoreImage" runat="server" BorderStyle="Inset" EnableViewState="true" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="UploadStoreImage"
                                CssClass="Error" Display="dynamic" ValidationExpression=".*(\.[Jj][Pp][Gg]|\.[Gg][Ii][Ff]|\.[Jj][Pp][Ee][Gg]|\.[Pp][Nn][Gg])"
                                ErrorMessage="Please select a valid JPEG, JPG, PNG or GIF image"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <asp:Label ID="lblStoreImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div>
            <asp:Label ID="lblError" runat="server" Visible="true"></asp:Label>
        </div>
        <div class="ClearBoth">
        </div>
        <br />
        <div>
            <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
            <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="30" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>

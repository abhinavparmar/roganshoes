<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Marketing.Default" Title="Marketing - Default"
    CodeBehind="Default.aspx.cs" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="LeftMargin">

        <h1>Promotions</h1>        
        <hr /> 
      
                <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/promotions.png" />
            </div>
            <div class="Shortcut"><a id="A2" href="~/SiteAdmin/Secure/Marketing/Promotions/PromotionsAndCoupons/Default.aspx" runat="server">Promotions <span style="text-transform:lowercase;"> and </span> Coupons</a></div>
            <div class="LeftAlign">
                <p>Manage promotional campaigns and discount coupons.</p>
            </div>
        </div>


       <%-- <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/GiftCards.png" />
            </div>
            <div class="Shortcut"><a id="A11" href="~/SiteAdmin/Secure/Marketing/Promotions/GiftCards/Default.aspx" runat="server">Gift Cards</a></div>
            <div class="LeftAlign">
                <p>Create and Manage stored value gift cards for your customers.</p>
            </div>
        </div>--%>

          <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/customer-reviews.png" /></div>
            <div class="Shortcut"><a id="A1" href="~/SiteAdmin/Secure/Marketing/Promotions/CustomerReviews/Default.aspx" runat="server">Customer Reviews</a></div>
            <div class="LeftAlign">
                <p>Approve or disable reviews submitted by customers.</p>
            </div>
        </div> 

        <h1>Search & Personalization</h1>        
        <hr />
           <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/tags.png" />
            </div>
            <div class="Shortcut"><a id="A10" href="~/SiteAdmin/Secure/Marketing/Search/Facets/Default.aspx" runat="server">Facets</a></div>
            <div class="LeftAlign">
                <p>Facets enable customers to quickly filter product search results based on familiar criteria.</p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/GlobalBoost.png" />
            </div>
            <div class="Shortcut"><a id="A3" href="~/SiteAdmin/Secure/Marketing/Search/ProductSearchSettings/Default.aspx" runat="server">Product Search Settings</a></div>
            <div class="LeftAlign">
                <p>Control the ranking of products displayed to your users on category pages and search results.</p>
            </div>
        </div>


        <h1>SEO</h1>        
        <hr />

         <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/DefaultSEOSettings.png" />
            </div>
            <div class="Shortcut"><a id="A4" href="~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/Default.aspx" runat="server">SEO Settings</a></div>
            <div class="LeftAlign">
                <p>Manage your store's SEO settings including default storewide settings, product settings, category settings, content page settings and 301 re-direction.</p>
            </div>
        </div>
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/DownloadTrackingData.png" />
            </div>
            <div class="Shortcut"><a id="A14" href="~/SiteAdmin/Secure/Marketing/SEO/AffiliateTracking/Default.aspx" runat="server">Affiliate Tracking</a></div>
            <div class="LeftAlign">
                <p>Download affiliate tracking data from your site.</p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/GenerateSiteMap.png" />
            </div>
            <div class="Shortcut"><a id="A15" href="~/SiteAdmin/Secure/Marketing/SEO/GoogleSiteMap/Default.aspx" runat="server">Google Site Map</a></div>
            <div class="LeftAlign">
                <p>The XML site map is used by Google to index your website.</p>
            </div>
        </div>

        
        <h1>Other</h1>        
        <hr />

            <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/store-locator.png" />
            </div>
            <div class="Shortcut"><a id="A16" href="~/SiteAdmin/Secure/Marketing/Other/StoreLocator/Default.aspx" runat="server">Store Locator</a></div>
            <div class="LeftAlign">
                <p>Manage locations of your physical stores that can then be searched via a store locator.</p>
            </div>
        </div>
    

    </div>
</asp:Content>

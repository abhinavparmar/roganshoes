<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="SiteAdmin.Secure.Marketing.SEO.AffiliateTracking.DownloadTracking" Codebehind="DownloadTracking.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<div class="Form">
    <h1>
        Affiliate Tracking</h1>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
     <div class="ClearBoth">
                <p>
                    Set up stores, associate a catalog, and create URLs to access the store.
                </p>
            </div>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="SearchForm">
        <div class="RowStyle">
            <div class="ItemStyle">
                <span class="FieldStyle">Begin Date</span><br />
                <span class="ValueStyle">
                    <asp:TextBox ID="txtStartDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="imgbtnStartDt"
                        runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" /><br /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Begin date"
                        ControlToValidate="txtStartDate" ValidationGroup="GroupTracking" CssClass="Error"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                    CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format"
                    ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                    ValidationGroup="GroupTracking"></asp:RegularExpressionValidator>
                <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                    runat="server" TargetControlID="txtStartDate">
                </ajaxToolKit:CalendarExtender></span>
            </div>
            <div class="ItemStyle">
                <span class="FieldStyle">End Date</span><br />
                <span class="ValueStyle">
                    <asp:TextBox ID="txtEndDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="ImgbtnEndDt"
                        runat="server" ImageAlign="AbsMiddle" ImageUrl="~/SiteAdmin/Themes/images/SmallCalendar.gif" /><br /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndDate"
                        ErrorMessage="Enter End date" ValidationGroup="GroupTracking" CssClass="Error"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                    CssClass="Error" Display="Dynamic" ErrorMessage="* Enter Valid Date in MM/DD/YYYY format"
                    ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                    ValidationGroup="GroupTracking"></asp:RegularExpressionValidator>
                <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
                    runat="server" TargetControlID="txtEndDate">
                </ajaxToolKit:CalendarExtender>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
                    ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage=" End Date must be greater than Begin date"
                    Operator="GreaterThanEqual" Type="Date" ValidationGroup="GroupTracking">
                </asp:CompareValidator></span>
            </div>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <span class="FieldStyle">File Type</span><br />
                <span class="ValueStyle">
                    <asp:DropDownList ID="ddlFileSaveType" runat="server">
                        <asp:ListItem Text="Microsoft Excel (.xls)" Value=".xls"></asp:ListItem>
                        <asp:ListItem Text="Delimited File Format" Value=".csv" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </span>
            </div>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="Error">
            <asp:Literal ID="ltrlError" runat="server"></asp:Literal></div>
        <div>
            <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="ImageButtons">
            <asp:Button ID="btnSubmit" runat="server" CausesValidation="True" CssClass="Size175"
                ValidationGroup="GroupTracking" OnClick="BtnSubmit_Click" Text="Download Tracking Data" />
            <asp:ImageButton ID="btnCancel" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
        </div>
    </div>
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategorySeo.ascx.cs" Inherits="WebApp.SiteAdmin.Secure.Marketing.SEO.SEOSettings.Categories.CategorySeo" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<asp:UpdatePanel ID="updatepanelProductList" runat="server">
    <ContentTemplate>
    <div class="SEO">
        <div class="SearchForm">
            <p> Update Category level SEO settings such as title, description, meta tags and page URL.
             </p>
            <h4 class="SubTitle">
                Search Categories</h4>
            <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">Name</span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtCategoryName" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">Catalog</span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlCatalog" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div>
                    <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                        runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                    <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                        runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                </div>
            </asp:Panel>
            <div class="ClearBoth">
                <br />
            </div>
            <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
            <h4 class="GridTitle">
                Category List</h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" OnSorting="UxGrid_Sorting"
                AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                OnPageIndexChanging="UxGrid_PageIndexChanging" CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand"
                Width="100%" EnableSortingAndPagingCallbacks="False" PageSize="25" AllowSorting="True"
                EmptyDataText="No categories exist in the database.">
                <Columns>
                    <asp:BoundField DataField="CategoryID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="60%">
                        <ItemTemplate>
                            <a href='Categories/Edit.aspx?itemid=<%# DataBinder.Eval(Container.DataItem,"CategoryId").ToString()%>'>
                                <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Enabled" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "VisibleInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="btnEdit" CssClass="actionlink" runat="server" CommandName="Edit"
                                Text="Manage SEO &raquo" CommandArgument='<%# Eval("CategoryID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <div>
                <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
            </div>
        </div>
    </div>
    </ContentTemplate>
</asp:UpdatePanel>

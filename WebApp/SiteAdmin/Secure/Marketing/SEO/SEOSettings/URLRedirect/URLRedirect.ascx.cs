﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;

namespace WebApp.SiteAdmin.Secure.Marketing.SEO.SEOSettings.URLRedirect
{
    public partial class URLRedirect : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private string EditPageLink = "~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/URLRedirect/Edit.aspx?ItemId=";
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Bind();
            }
        }

        /// <summary>
        /// Add Url Redirect Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddUrlRedirect_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/URLRedirect/Edit.aspx");
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.Bind();
        }

        /// <summary>
        /// Clear Search Button
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            txtNewUrl.Text = string.Empty;
            txtOldUrl.Text = string.Empty;
            ddlURLStatus.SelectedValue = "0";

            this.Bind();
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind grid Method
        /// </summary>
        protected void Bind()
        {
            UrlRedirectAdmin adminAccess = new UrlRedirectAdmin();

            uxGrid.DataSource = adminAccess.Search(txtOldUrl.Text.Trim(), txtNewUrl.Text.Trim(), ddlURLStatus.SelectedValue);
            uxGrid.DataBind();
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.Bind();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // Convert the row index stored in the CommandArgument
            // property to an Integer.
            int index = Convert.ToInt32(e.CommandArgument);

            // Get the values from the appropriate
            // cell in the GridView control.
            GridViewRow selectedRow = uxGrid.Rows[index];

            TableCell Idcell = selectedRow.Cells[0];
            string Id = Idcell.Text;

            if (e.CommandName == "Edit")
            {
                Response.Redirect(this.EditPageLink + Id);
            }
            else if (e.CommandName == "Delete")
            {
                UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
                UrlRedirect urlRedirect = urlRedirectAdmin.GetById(int.Parse(Id));
                string URLName = urlRedirect.NewUrl;
                string AssociateName = "Delete " + URLName + " 301 URL Redirect";
                bool status = urlRedirectAdmin.Delete(int.Parse(Id));

                if (status)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, URLName);

                    if (urlRedirect != null)
                    {
                        ZNodeSEOUrl seoUrl = new ZNodeSEOUrl();
                        seoUrl.RemoveRedirectURL(urlRedirect.OldUrl);
                    }

                    this.Bind();
                }
            }
        }

        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton DeleteButton = (LinkButton)e.Row.Cells[5].FindControl("btnDelete");

                // Set the Button's CommandArgument property with the row's index.
                DeleteButton.CommandArgument = e.Row.RowIndex.ToString();

                // Add Client Side confirmation
                DeleteButton.OnClientClick = "return confirm('Are you sure you want to delete this item?');";
            }
        }

        /// <summary>
        /// Grid Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }
        #endregion
    }
}
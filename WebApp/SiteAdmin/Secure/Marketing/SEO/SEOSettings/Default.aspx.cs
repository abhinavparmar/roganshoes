﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Secure.Marketing.SEO.SEOSettings
{
    public partial class Default : System.Web.UI.Page
    {
        private string Mode = string.Empty;

        #region Page_Load

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get mode value from querystring        
            if (Request.Params["mode"] != null)
            {
                this.Mode = Request.Params["mode"];
            }


            if (!Page.IsPostBack)
            {
                this.ResetTab();
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get Active Tab Name Method
        /// </summary>
        /// <param name="tabIndex">The value of tabIndex</param>
        /// <returns>Returns the Active Tab Name</returns>
        public string GetActiveTabName(int tabIndex)
        {
            string tabName = string.Empty;
            switch (tabIndex)
            {
                case 1:
                    tabName = "default";
                    break;
                case 2:
                    tabName = "product";
                    break;
                case 3:
                    tabName = "category";
                    break;
                case 4:
                    tabName = "content";
                    break;
                case 5:
                    tabName = "301redirect";
                    break;
            }

            return tabName;
        }
        #endregion

        private void ResetTab()
        {
            if (this.Mode.Equals("default"))
            {
                // Set Category Settings as active tab 

                tabSeoSettings.ActiveTab = pnlDefaultSeo;
            }
            else if (this.Mode.Equals("product"))
            {
                // Set Category Settings as active tab 

                tabSeoSettings.ActiveTab = pnlProductSeo;
            }
            else if (this.Mode.Equals("category"))
            {
                // Set Category Settings as active tab 

                tabSeoSettings.ActiveTab = pnlCategorySeo;
            }
            else if (this.Mode.Equals("content"))
            {
                // Content the Content Page Settings as active tab
                tabSeoSettings.ActiveTab = pnlContentSeo;
            }
            else if (this.Mode.Equals("urlredirect"))
            {
                // Content the Content Page Settings as active tab
                tabSeoSettings.ActiveTab = pnl301Redirect;
            }
            else
            {
                //  Product Settings 
                tabSeoSettings.ActiveTabIndex = 0;
            }
        }
    }
}
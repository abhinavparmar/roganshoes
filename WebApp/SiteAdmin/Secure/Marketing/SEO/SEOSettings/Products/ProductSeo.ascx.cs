﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp.SiteAdmin.Secure.Marketing.SEO.SEOSettings.Products
{
    public partial class ProductSeo : System.Web.UI.UserControl
    {
        #region Protected Variables
        private string EditLink = "~/SiteAdmin/Secure/Marketing/SEO/SEOSettings/Products/Edit.aspx?itemid=";
        #endregion

        #region Public Methods
        /// <summary>
        /// Get ImagePath
        /// </summary>
        /// <param name="Imagefile">The value of ImageFile</param>
        /// <returns>Returns the Image path</returns>
        public string GetImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(Imagefile);
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        public void BindGridData()
        {
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ZNode.Libraries.Admin.ProductAdmin();
            DataSet ds = ProdAdmin.SearchProduct(txtproductname.Text.Trim(), txtproductnumber.Text.Trim(), txtsku.Text.Trim(), dmanufacturer.Value, dproducttype.Value, dproductcategory.Value, ddlCatalog.SelectedValue);
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "DisplayOrder";

            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind data to the drop down list
        /// </summary>
        public void BindDropDownData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            TList<Catalog> catalogList = catalogAdmin.GetAllCatalogs();
            DataSet ds = catalogList.ToDataSet(false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            DataView dataView = ds.Tables[0].DefaultView;
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);

            // Filter the data based on user role of store access
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                dataView.RowFilter = string.Concat("CatalogID IN (", catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess), ")");
            }

            ddlCatalog.DataSource = dataView;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();

            if (string.Compare(profiles.StoreAccess, "AllStores") == 0)
            {
                ListItem li = new ListItem("ALL", "0");
                ddlCatalog.Items.Insert(0, li);
            }
        }
        #endregion

        #region Page_Load

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindDropDownData();
                this.BindGridData();
            }

            dproductcategory.ContextKey = ddlCatalog.SelectedValue;
        }

        #endregion

        #region Grid Events

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            if ((txtproductname.Text == string.Empty) && (txtproductnumber.Text == string.Empty) && (txtsku.Text == string.Empty) && (dmanufacturer.Value == Convert.ToString(0)) && (dproductcategory.Value == Convert.ToString(0)) && (dproducttype.Value == Convert.ToString(0)))
            {
                this.BindGridData();
            }
            else
            {
                this.BindSearchProduct();
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Manage")
            {
                Response.Redirect(this.EditLink + e.CommandArgument);
            }
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchProduct();
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtproductname.Text = string.Empty;
            txtproductnumber.Text = string.Empty;
            txtsku.Text = string.Empty;
            ddlCatalog.SelectedIndex = 0;
            dmanufacturer.Text = string.Empty;
            dmanufacturer.Value = "0";
            dproducttype.Text = string.Empty;
            dproducttype.Value = "0";
            dproductcategory.Text = string.Empty;
            dproductcategory.Value = "0";
            this.BindGridData();

        }

        /// <summary>
        /// Returns formatted price to the grid
        /// </summary>
        /// <param name="productPrice">The value of productprice</param>
        /// <returns>Returns the formatted price</returns>
        protected string FormatPrice(object productPrice)
        {
            if (productPrice != null)
            {
                if (productPrice.ToString().Length > 0)
                {
                    return decimal.Parse(productPrice.ToString()).ToString("c");
                }
            }

            return string.Empty;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Search a Product by name,productnumber,sku,manufacturer,category,producttype
        /// </summary>
        private void BindSearchProduct()
        {
            ProductAdmin prodadmin = new ProductAdmin();
            uxGrid.DataSource = prodadmin.SearchProduct(txtproductname.Text.Trim(), txtproductnumber.Text.Trim(), txtsku.Text.Trim(), dmanufacturer.Value, dproducttype.Value, dproductcategory.Value, ddlCatalog.SelectedValue);
            uxGrid.DataBind();
        }
        #endregion
    }
}
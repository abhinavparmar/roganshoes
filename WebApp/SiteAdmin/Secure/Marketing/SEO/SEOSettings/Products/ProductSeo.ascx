﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSeo.ascx.cs" Inherits="WebApp.SiteAdmin.Secure.Marketing.SEO.SEOSettings.Products.ProductSeo" %>
<%@ Register TagPrefix="ZNode" TagName="ManufacturerAutoComplete" Src="~/SiteAdmin/Controls/Default/ManufacturerAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/SiteAdmin/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductTypeAutoComplete" Src="~/SiteAdmin/Controls/Default/ProductTypeAutoComplete.ascx" %>

<asp:UpdatePanel ID="updatepanelProductList" runat="server">
    <ContentTemplate>
        <div class="SEO">
            <div class="SearchForm">
                <p>
                   Update Product level SEO settings such as title, description, meta tags and page URL.
                </p>
                <h4 class="SubTitle">Search Products</h4>
                <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">Product Name</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Product Number</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtproductnumber" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">SKU</span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtsku" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Catalog</span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true"></asp:DropDownList></span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">Brand</span><br />
                            <span class="ValueStyle">
                                <znode:manufacturerautocomplete id="dmanufacturer" runat="server" />
                            </span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Product Type</span><br />
                            <span class="ValueStyle">
                                <znode:producttypeautocomplete id="dproducttype" runat="server" />
                            </span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">Product Category</span><br />
                            <span class="ValueStyle">
                                <znode:categoryautocomplete id="dproductcategory" runat="server" />
                            </span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <br />
                    </div>
                    <div>
                        <asp:ImageButton ID="btnClear" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_clear_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_clear.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_clear.gif"
                            runat="server" AlternateText="Clear" OnClick="BtnClear_Click" />
                        <asp:ImageButton ID="btnSearch" onmouseover="this.src='../../../../Themes/Images/buttons/button_search_highlight.gif';"
                            onmouseout="this.src='../../../../Themes/Images/buttons/button_search.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_search.gif"
                            runat="server" AlternateText="Search" OnClick="BtnSearch_Click" />
                    </div>
                </asp:Panel>
                <br />
            </div>
            <h4 class="GridTitle">Product List</h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                OnRowCommand="UxGrid_RowCommand" EmptyDataText="No Products exist in the database."
                Width="100%" CellPadding="4">
                <Columns>
                    <asp:BoundField DataField="productid" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <a href='<%# "Products/Edit.aspx?itemid=" + DataBinder.Eval(Container.DataItem,"productid")%>'
                                id="LinkView">
                                <img id="Img1" alt=" " src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>' runat="server" style="border: none" />
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField DataNavigateUrlFields="productid" DataNavigateUrlFormatString="Edit.aspx?itemid={0}"
                        DataTextField="name" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="50%" />
                    <asp:TemplateField HeaderText="Is Active" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="Img2" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:LinkButton ID="Button1" CommandName="Manage" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                                Text="Manage SEO &raquo" runat="server" CssClass="actionlink" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

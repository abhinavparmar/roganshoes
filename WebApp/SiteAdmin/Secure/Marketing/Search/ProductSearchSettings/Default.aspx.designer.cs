﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace SiteAdmin.Secure.Marketing.Search.ProductSearchSettings {
    
    
    public partial class Default {
        
        /// <summary>
        /// hdnActiveTabIndex control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdnActiveTabIndex;
        
        /// <summary>
        /// tabBoostSettings control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabContainer tabBoostSettings;
        
        /// <summary>
        /// pnlBoostProduct control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel pnlBoostProduct;
        
        /// <summary>
        /// ucProductSelect_ProductBoost control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SiteAdmin.Controls.Default.Search.ProductSelect ucProductSelect_ProductBoost;
        
        /// <summary>
        /// pnlBoostCategory control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel pnlBoostCategory;
        
        /// <summary>
        /// ucProductCategorySelect_ProductCategoryBoost control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SiteAdmin.Controls.Default.Search.ProductCategorySelect ucProductCategorySelect_ProductCategoryBoost;
        
        /// <summary>
        /// pnlBoostField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.TabPanel pnlBoostField;
        
        /// <summary>
        /// FieldBoostSelect control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SiteAdmin.Controls.Default.Search.FieldBoostSelect FieldBoostSelect;
    }
}

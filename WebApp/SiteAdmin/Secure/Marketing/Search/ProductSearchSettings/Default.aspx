<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/SiteAdmin/Themes/Standard/content.master"
    Inherits="SiteAdmin.Secure.Marketing.Search.ProductSearchSettings.Default" Title="Manage Global Boost Settings"
    CodeBehind="Default.aspx.cs" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/Search/ProductSelect.ascx" TagPrefix="uc1" TagName="ProductSelect" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/Search/ProductCategorySelect.ascx" TagPrefix="uc3" TagName="ProductCategorySelect" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/Search/FieldBoostSelect.ascx" TagPrefix="uc4" TagName="FieldBoostSelect" %>


<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            var activeTabIndex = document.getElementById('<%=hdnActiveTabIndex.ClientID%>');
            activeTabIndex.value = sender.get_activeTabIndex();
        }
    </script>
    <div>
        <h1>Product Search Settings
        </h1>
        <p>
            Control the ranking of products displayed to your users on category pages and search results.
        </p>

        <div style="clear: both;">

            <asp:HiddenField ID="hdnActiveTabIndex" runat="server" Value="0" />
            <ajaxToolKit:TabContainer ID="tabBoostSettings" runat="server">
                <ajaxToolKit:TabPanel ID="pnlBoostProduct" runat="server">
                    <HeaderTemplate>Product Level</HeaderTemplate>
                    <ContentTemplate>
                        <h4 class="SubTitle">Product Level Settings
                        </h4>
                        <p>
                            Search for the product to set their boost value.  Products with a higher boost value are ranked better in search results.  
                        </p>
                        <h4 class="SubTitle">Search Products
                        </h4>
                        <uc1:ProductSelect runat="server" ID="ucProductSelect_ProductBoost" />
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlBoostCategory" runat="server">
                    <HeaderTemplate>Category Level</HeaderTemplate>
                    <ContentTemplate>
                        <h4 class="SubTitle">Category Level Settings
                        </h4>
                        <p>
                            Search for the products to set their boost value at a category level.  Products with a higher boost value within that cateogry are ranked better in search results.
                        </p>
                        <h4 class="SubTitle">Search Products
                        </h4>
                        <uc3:ProductCategorySelect runat="server" ID="ucProductCategorySelect_ProductCategoryBoost" />
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlBoostField" runat="server">
                    <HeaderTemplate>Field Level</HeaderTemplate>
                    <ContentTemplate>
                        <h4 class="SubTitle">Field Level Settings
                        </h4>
                        <p>
                            Change boost settings based on individual product fields.
                        </p>

                        <uc4:FieldBoostSelect runat="server" ID="FieldBoostSelect" />
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
            </ajaxToolKit:TabContainer>
        </div>
    </div>
</asp:Content>

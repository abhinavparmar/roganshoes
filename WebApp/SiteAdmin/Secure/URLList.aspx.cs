using System;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace SiteAdmin
{
    /// <summary>
    /// Represents the URL List
    /// </summary>
    public partial class Admin_Secure_URLList : System.Web.UI.Page
    {
        #region Page Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindGrid();
        }
        #endregion

        #region Grid Events
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }
        #endregion

        #region Bind Method

        protected void BindGrid()
        {
            DomainAdmin domainAdmin = new DomainAdmin();
            TList<Domain> domainList = domainAdmin.GetAllDomain();
            domainList.Sort("DomainName");
            uxGrid.DataSource = domainList;
            uxGrid.DataBind();
        }
        #endregion
    }
}
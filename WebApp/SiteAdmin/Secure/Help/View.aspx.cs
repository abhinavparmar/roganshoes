﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace WebApp.SiteAdmin.Secure.Help
{
    public partial class View : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["file"] != null)
                {
                    string filename=Request.QueryString["file"].ToString();
                    string filepath = Server.MapPath(ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.DataPath) +"help/"+filename;
                    if (File.Exists(filepath))
                    {
                        FileStream MyFileStream = new FileStream(filepath, FileMode.Open);

                        long Filesize = MyFileStream.Length;

                        byte[] Buffer = new byte[(int)Filesize];

                        MyFileStream.Read(Buffer, 0, (int)MyFileStream.Length);

                        MyFileStream.Close();

                        Response.ContentType = "application/pdf";

                        Response.AppendHeader("content-disposition", "inline; filename=" + filename);

                        Response.BinaryWrite(Buffer);

                        Response.End();
                    }
                    else
                    {
                        lblerror.Text = "File not found";
                    }
                }
            }
        }
    }
}
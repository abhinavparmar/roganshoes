<%@ Page Language="C#" MasterPageFile="~/SiteAdmin/Themes/Standard/edit.master"  AutoEventWireup="True" Inherits="SiteAdmin.Secure.ChangePassword" Title="Change Password" Codebehind="ChangePassword.aspx.cs" ValidateRequest="false" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" Runat="Server">
<div class="Form Search">
   <%-- <h1>Change Password<uc2:DemoMode ID="DemoMode1" runat="server" />
    </h1>--%>

     <div align="center">
        <div class="LeftFloat" style="width: 70%; text-align: left">
          <h1>Change Password<uc2:DemoMode ID="DemoMode1" runat="server" />
    </h1>
        </div>
        <div class="ClearBoth">
        </div>
        <div class="LeftFloat">
            <p>
               Change your administrator password.
            </p>
        </div> 
    </div> 
    <div class="ClearBoth" ></div>
    <div><uc1:spacer id="Spacer1" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer></div>
    <asp:ChangePassword ID="AdminChangePassword" runat="server" CancelDestinationPageUrl="~/account.aspx"
        ContinueDestinationPageUrl="~/account.aspx" DisplayUserName="False" ChangePasswordTitleText=""
        OnChangePasswordError="ChangePassword1_ChangePasswordError" OnChangingPassword="AdminChangePassword_ChangingPassword">
        <SuccessTemplate>
            <div class="Row">
                <div align="left">
                    Confirmation</div>
                <div class="Success">
                    Your password has been changed!</div>
                <div align="right" colspan="2">
                    <asp:Button ID="ContinuePushButton" OnClick="ContinuePushButton_Click" runat="server"
                        CausesValidation="False" CommandName="Continue" CssClass="Button" Text="Continue" />
                </div>
            </div>
        </SuccessTemplate>
        <ChangePasswordTemplate>
            <div>
                <div align="left" style="color: red" class="Error">
                    <asp:Literal ID="PasswordFailureText" runat="server" EnableViewState="False"></asp:Literal>
                </div>
            </div>
            <div class="CPassword">
                <span align="right" class="FieldStyle">
                    <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Current Password</asp:Label></span>
                <span class="ValueStyle">
                    <asp:TextBox ID="CurrentPassword" autocomplete="off" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                        ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword1"
                        CssClass="Error" Display="Dynamic">Password is required.</asp:RequiredFieldValidator>
                </span>
            </div>
            <div class="CPassword">
                <span align="right" class="FieldStyle">
                    <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password</asp:Label></span>
                <span class="ValueStyle">
                    <asp:TextBox ID="NewPassword" autocomplete="off" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                        ErrorMessage="New Password is required." ToolTip="New Password is required."
                        ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic">New Password is required.</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="NewPassword"
                        Display="Dynamic" ErrorMessage="New Password length should be minimum 8" SetFocusOnError="True"
                        ToolTip="New Password length should be minimum 8" ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})"
                        ValidationGroup="ChangePassword1" CssClass="Error">Password must be at least 8 alphanumeric characters and contain at least one number.</asp:RegularExpressionValidator>
                </span>
            </div>
            <div class="CPassword">
                <span align="right" class="FieldStyle">
                    <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm New Password</asp:Label></span>
                <span>
                    <asp:TextBox ID="ConfirmNewPassword" autocomplete="off" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                        ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                        ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic">Confirmation password is required.</asp:RequiredFieldValidator>
                </span></span>
                <div class="Row">
                    <span align="center" colspan="2">
                        <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                            ControlToValidate="ConfirmNewPassword" CssClass="Error" Display="Dynamic" ErrorMessage="Confirmation password does not match your new password."
                            ValidationGroup="ChangePassword1"></asp:CompareValidator>
                    </span>
                </div>
                <div class="CPassword">
                    <div class="Buttons">

            <asp:ImageButton ID="ChangePasswordPushButton"  CommandName="ChangePassword" onmouseover="this.src='../Themes/Images/buttons/button_submit_highlight.gif';"
                onmouseout="this.src='../Themes/Images/buttons/button_submit.gif';" ValidationGroup="ChangePassword1"  ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
                runat="server" AlternateText="Submit" CausesValidation="true" />
            <asp:ImageButton ID="CancelPushButton" CausesValidation="False" onmouseover="this.src='../Themes/Images/buttons/button_cancel_highlight.gif';"
                onmouseout="this.src='../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
                runat="server" AlternateText="Submit"  CommandName="Cancel" PostBackUrl="~/SiteAdmin/Secure/Default.aspx" />
 
                     </div>
                </div>
        </ChangePasswordTemplate>
    </asp:ChangePassword>
</div> 
</asp:Content>


using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;

namespace SiteAdmin
{
    public partial class Admin_Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Session.Abandon();

                FormsAuthentication.SignOut();
            }

            //instantiated just to trigger licensing > Do not remove!
            ZNodeHelper hlp = new ZNodeHelper();

            //Set input focus on the page load
            UserName.Focus();

            string inURL = Request.Url.ToString();

            //check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {                    
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }

            //Forgot password link 
            string link = "~/SiteAdmin/ForgotPassword.aspx";
            forgotPasswordLink.HRef = link.ToLower().Replace("https://", "http://");

            if (Page.User.Identity.IsAuthenticated)
            {
                lblaccess.Visible = true;
            }            
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            ZNodeUserAccount userAcct = new ZNodeUserAccount();
            bool loginSuccess = userAcct.Login(ZNodeConfigManager.SiteConfig.PortalID, UserName.Text.Trim(), Password.Text.Trim());

            if (loginSuccess)
            {
                string retValue = "";

                bool status = ZNodeUserAccount.CheckLastPasswordChangeDate((Guid)userAcct.UserID, out retValue);

                if (!status)
                {
                    ZNode.Libraries.DataAccess.Service.AccountService acctService = new ZNode.Libraries.DataAccess.Service.AccountService();
                    ZNode.Libraries.DataAccess.Entities.Account account = acctService.GetByAccountID(userAcct.AccountID);

                    // Set Error Code to session Object
                    Session.Add("ErrorCode", retValue);

                    // Get account and set to session
                    Session.Add("AccountObject", account);

                    Response.Redirect("~/SiteAdmin/ResetPassword.aspx");
                }

                // Set CurrentUserProfile ProfileID
                userAcct.ProfileID = ZNodeProfile.CurrentUserProfileId;

                //get account and set to session
                Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), userAcct);

                FormsAuthentication.SetAuthCookie(UserName.Text.Trim(), false);

                if (Roles.IsUserInRole(UserName.Text, "ORDER APPROVER") && !Roles.IsUserInRole(UserName.Text, "ADMIN"))
                    Response.Redirect("~/SiteAdmin/Secure/Orders/OrderManagement/ViewOrders/Default.aspx", true);

                if ((Roles.IsUserInRole(UserName.Text.Trim(), "REVIEWER") || Roles.IsUserInRole(UserName.Text.Trim(), "REVIEWER MANAGER")) && !Roles.IsUserInRole(UserName.Text, "ADMIN"))
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorProducts/Default.aspx", true);
                }

                // if user is an admin, then redirect to the admin dashboard
                if (Roles.IsUserInRole(UserName.Text.Trim(), "ADMIN") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
                {
                    Response.Redirect("~/siteadmin/secure/default.aspx", true);
                }
                else if (Roles.IsUserInRole(UserName.Text.Trim(), "FRANCHISE") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
                {
                    Response.Redirect("~/franchiseAdmin/secure/default.aspx", true);
                }
                else if (Roles.IsUserInRole(UserName.Text.Trim(), "CUSTOMER SERVICE REP") || Roles.IsUserInRole(UserName.Text.Trim(), "CATALOG EDITOR") || Roles.IsUserInRole(UserName.Text.Trim(), "EXECUTIVE") || Roles.IsUserInRole(UserName.Text.Trim(), "ORDER ONLY") || Roles.IsUserInRole(UserName.Text.Trim(), "SEO") || Roles.IsUserInRole(UserName.Text.Trim(), "CONTENT EDITOR") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
                {  
                    Response.Redirect("~/siteadmin/secure/default.aspx", true);
                }
                FormsAuthentication.RedirectFromLoginPage(UserName.Text.Trim(), false);
            }
            else
            {
                FailureText.Text = "Login unsuccessful. Please try again.";
            }
        }
    }
}
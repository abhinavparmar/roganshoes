﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Cross sell user conrtol class
    /// </summary>
    public partial class CrosssSell : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private string AddCrossSellLink = "~/SiteAdmin/Secure/Vendors/VendorProducts/AddRelatedItems.aspx?itemid=";
        private int ItemId = 0;
        #endregion

        #region Bind method
        /// <summary>
        /// Bind Related Products 
        /// </summary>
        public void BindRelatedItems()
        {
            ProductCrossSellAdmin ProdCrossSellAccess = new ProductCrossSellAdmin();
            TList<ProductCrossSell> productCrossSellList = ProdCrossSellAccess.GetRelatedItemsByProductId(this.ItemId);
            uxGrid.DataSource = productCrossSellList;
            uxGrid.DataBind();

            // Vendors will be limited to a total of 5 cross sell items."
            btnAddRelatedItems.Enabled = productCrossSellList.Count >= 5 ? false : true;
        }
        #endregion

        #region Page load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId (Product Id) from QueryString        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!this.IsPostBack)
            {
                this.BindRelatedItems();
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Get the product name by product Id
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <returns>Return the name of the product</returns>
        protected string GetProductName(object productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            return productAdmin.GetByProductId(Convert.ToInt32(productId)).Name;
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "RemoveItem")
                {
                    ProductCrossSellAdmin _prodCrossSellAdmin = new ProductCrossSellAdmin();
                    bool Check = _prodCrossSellAdmin.Delete(int.Parse(e.CommandArgument.ToString()), this.ItemId);
                    if (Check)
                    {
                        this.BindRelatedItems();
                    }
                }
            }
        }

        /// <summary>
        /// Related Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindRelatedItems();
        }

        /// <summary>
        /// Event is raised when Add Related Items button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddRelatedItems_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddCrossSellLink + this.ItemId);
        }

        #endregion
    }
}
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Html TextBox user control class
    /// </summary>
    public partial class HtmlTextBox : System.Web.UI.UserControl
    {
        private int _Mode;

        /// <summary>
        /// Gets or sets the html text
        /// </summary>
        public string Html
        {
            get
            {
                // Remove the <p> tag
                string htmlContent = ZnodeEditBox.Text;

                // Check for first occurence of opening paragraph tag
                if (htmlContent.IndexOf("<p>") == 0)
                {
                    htmlContent = htmlContent.Remove(0, 3); // If exists, then remove the first occurence
                }

                // Check for last occurence of closing paragraph tag
                if (htmlContent.LastIndexOf("</p>") == (htmlContent.Length - 4))
                {
                    htmlContent = htmlContent.Remove(htmlContent.Length - 4, 4); // If exists, then remove it
                }

                return htmlContent;
            }

            set
            {
                ZnodeEditBox.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the mode for this control
        /// </summary>
        public int Mode
        {
            get
            {
                return this._Mode;
            }

            set
            {
                this._Mode = value;
            }
        }
    }
}
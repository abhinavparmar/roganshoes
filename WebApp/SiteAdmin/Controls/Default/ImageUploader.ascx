﻿<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="SiteAdmin.Controls.Default.ImageUploader" CodeBehind="ImageUploader.ascx.cs" %>
<div class="Form">
    <script type="text/javascript">
        function setEnabled() {
            document.getElementById('<%=txtFileName.ClientID %>').disabled = !document.getElementById('<%=rdoNewFileName.ClientID %>').checked;
    }
    </script>
    <div>
        <asp:FileUpload ID="UploadImage" runat="server" BorderStyle="Inset" EnableViewState="true" /><br />
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UploadImage"
            CssClass="Error" Display="dynamic" ErrorMessage="Please select a valid JPEG, JPG, PNG or GIF image"
            ValidationExpression=".*(\.[Jj][Pp][Gg]|\.[Gg][Ii][Ff]|\.[Jj][Pp][Ee][Gg]|\.[Pp][Nn][Gg])"></asp:RegularExpressionValidator>
    </div>
</div>
<asp:Panel ID="pnlSaveOption" runat="server" Visible="false" Style="width: 300px; border: solid 0px gray; padding: 5px;">
    <div class="Error">
        <asp:Label ID="lblMsg" runat="server"></asp:Label>
    </div>
    Select an Option
        <br />
    <asp:RadioButton ID="rdoOverwrite" Text="Overwrite the existing file" runat="server"
        GroupName="SaveOption" />
    <br />
    <asp:RadioButton ID="rdoNewFileName" Checked="true" runat="server" GroupName="SaveOption"
        Text="Use the file name" />
    <asp:TextBox ID="txtFileName" runat="server"></asp:TextBox><br />
    <asp:RegularExpressionValidator ID="revFilename" runat="server" ControlToValidate="txtFileName"
        CssClass="Error" Display="dynamic" ErrorMessage="Enter a valid file name. Do not enter file extension."
        ValidationExpression=".*([a-zA-Z_0-9 ])"></asp:RegularExpressionValidator>
</asp:Panel>


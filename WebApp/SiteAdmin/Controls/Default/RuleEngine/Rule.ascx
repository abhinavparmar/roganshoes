﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="SiteAdmin.Controls.Default.RuleEngine.Rule" CodeBehind="Rule.ascx.cs" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>

<div class="LeftFloat" style="width: 80%">
	<h1><asp:Label ID="lblTitle" runat="server" /></h1>
	<ZNode:DemoMode ID="DemoMode1" runat="server" />
</div>

<div class="LeftFloat" align="right" style="width: 20%">
	<asp:ImageButton ID="btnSubmitTop" runat="server"
					 onmouseover="this.src='../../../../../Themes/Images/buttons/button_submit_highlight.gif';"
		             onmouseout="this.src='../../../../../Themes/Images/buttons/button_submit.gif';"
		             ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
		             AlternateText="Submit"
		             OnClick="BtnSubmit_Click" />

	<asp:ImageButton ID="btnCancelTop" runat="server"
		             CausesValidation="False"
		             onmouseover="this.src='../../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
		             onmouseout="this.src='../../../../../Themes/Images/buttons/button_cancel.gif';"
		             ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
		             AlternateText="Cancel"
		             OnClick="BtnCancel_Click" />
</div>

<div class="ClearBoth" align="left"></div>

<div class="FormView">
	<div>
		<ZNode:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server" />
	</div>
	<div>
		<asp:Label ID="lblErrorMsg" EnableViewState="false" runat="server" CssClass="Error" />
	</div>
	<div>
		<ZNode:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server" />
	</div>

	<asp:Panel ID="pnlRuleType" runat="server" Visible="false">
		<div class="FieldStyle">Rule Type</div>
		<div class="ValueStyle">
			<asp:DropDownList ID="ddlRuleTypes" runat="server">
				<asp:ListItem Selected="True" Value="0" Text="Promotion" />
				<asp:ListItem Text="Shipping" Value="1" />
				<asp:ListItem Text="Tax" Value="2" />
				<asp:ListItem Text="Supplier" Value="3" />
			</asp:DropDownList>
		</div>
	</asp:Panel>

	<asp:Panel ID="pnlClassType" runat="server" Visible="false">
		<div class="FieldStyle">Class Type</div>
		<div class="ValueStyle">
			<asp:DropDownList ID="ddlClassTypes" runat="server">
				<asp:ListItem Selected="True" Value="CART" Text="CART" />
				<asp:ListItem Text="PRICE" Value="PRICE" />
			</asp:DropDownList>
		</div>
	</asp:Panel>

	<div class="FieldStyle">
		Class Name<span class="Asterix">*</span>
	</div>
	<div class="ValueStyle">
		<asp:TextBox ID="txtRuleClassName" runat="server" Width="350px" />
		<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtRuleClassName" ErrorMessage=" Enter  rule class name" CssClass="Error" Display="dynamic" />
	</div>

	<div class="FieldStyle">
		Rule Name<span class="Asterix">*</span>
	</div>
	<div class="ValueStyle">
		<asp:TextBox ID="txtRuleName" runat="server" Width="350px" />
		<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtRuleName" ErrorMessage=" Enter rule name" CssClass="Error" Display="dynamic" />
	</div>

	<div class="FieldStyle">
		Rule Description
	</div>
	<div class="ValueStyle">
		<asp:TextBox ID="txtRuleDesc" runat="server" Width="350px" Rows="4" TextMode="MultiLine" />
	</div>

	<div class="FieldStyle"></div>
	<div class="ValueStyleText">
		<asp:CheckBox ID="chkEnable" runat="server" Text="Enable Rule" />
	</div>
	<div>
		<ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server" />
	</div>

	<div class="ClearBoth" align="left">
		<br />
	</div>

	<div>
		<asp:ImageButton ID="btnSubmit" runat="server"
			             onmouseover="this.src='../../../../../Themes/Images/buttons/button_submit_highlight.gif';"
			             onmouseout="this.src='../../../../../Themes/Images/buttons/button_submit.gif';"
			             ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
			             AlternateText="Submit"
			             OnClick="BtnSubmit_Click"
			             CausesValidation="true" />

		<asp:ImageButton ID="btnCancel" runat="server"
			             onmouseover="this.src='../../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
			             onmouseout="this.src='../../../../../Themes/Images/buttons/button_cancel.gif';"
			             ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
			             AlternateText="Cancel"
			             OnClick="BtnCancel_Click"
			             CausesValidation="False" />
	</div>

	<div>
		<ZNode:Spacer ID="Spacer3" SpacerHeight="15" SpacerWidth="3" runat="server" />
	</div>
</div>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default.Accounts
{
    public partial class AffiliatePayment : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private int AccountId = 0;
        private string _RoleName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName
        {
            get
            {
                return this._RoleName;
            }

            set
            {
                this._RoleName = value;
            }
        }
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSubmitTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel.gif") + "'");

            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }
            else
            {
                this.ItemId = 0;
            }

            if (Request.Params["accId"] != null)
            {
                this.AccountId = int.Parse(Request.Params["accId"].ToString());
            }

            if (Request.Params["pagefrom"] != null)
            {
                this.RoleName = Request.Params["pagefrom"].ToString();
            }

            if (this.ItemId > 0)
            {
                lblTitle.Text = "Edit Payment";
            }

            AccountHelper helperAccess = new AccountHelper();
            DataSet MyDataSet = helperAccess.GetCommisionAmount(ZNodeConfigManager.SiteConfig.PortalID, this.AccountId.ToString());

            lblAmountOwed.Text = "$" + MyDataSet.Tables[0].Rows[0]["CommissionOwed"].ToString();
        }

        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Parse(txtDate.Text);

            if (dt.CompareTo(System.DateTime.Today) > 0)
            {
                ErrorMessage.Text = "Received date should not be greater than today.";
                return;
            }

            AccountAdmin accountAdmin = new AccountAdmin();
            AccountPayment entity = new AccountPayment();

            if (this.ItemId > 0)
            {
                entity = accountAdmin.GetByAccountPaymentId(this.ItemId);
            }

            entity.AccountID = this.AccountId;
            entity.Amount = decimal.Parse(txtAmount.Text.Trim());
            entity.Description = Server.HtmlEncode(txtDescription.Text.Trim());
            entity.ReceivedDate = dt;

            bool status = accountAdmin.AddAffiliatePayment(entity);
            string AssociateName = "Added Affiliate Payment - " + txtDescription.Text.Trim();

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, txtDescription.Text.Trim());

            if (status)
            {
                if (RoleName.ToLower() == "admin")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=admin");
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=franchise");
                }
                else if (RoleName.ToLower() == "vendor")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=vendor");
                }
                else
                {
                    Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=customer");
                }
            }
            else
            {
                ErrorMessage.Text = string.Empty;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (RoleName.ToLower() == "admin")
            {
                Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=admin");
            }
            else if (RoleName.ToLower() == "franchise")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=franchise");
            }
            else if (RoleName.ToLower() == "vendor")
            {
                Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=customer");
            }

        }
        #endregion
    }
}
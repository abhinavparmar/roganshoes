﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNote.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.AddNote" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/SiteAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/SiteAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>


<div class="FormView">
    <div class="LeftFloat" style="width: 70%; text-align: left">
        <h1>Add Note<uc2:DemoMode ID="DemoMode1" runat="server" />
        </h1>
    </div>
    <div style="text-align: right; padding-right: 10px;">
        <asp:ImageButton ID="btnSubmitTop" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
        <asp:ImageButton ID="btnCancelTop" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>
    <div class="ClearBoth">
    </div>
    <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    <div class="FieldStyle">
        Note Title<span class="Asterix">*</span>
    </div>
    <div class="ValueStyle">
        <asp:TextBox ID='txtNoteTitle' runat='server' MaxLength="50" Columns="50"></asp:TextBox>
        <asp:Label ID="lblErrorTitle" runat="server" CssClass="Error" Text="Label" Visible="False"></asp:Label>
    </div>
    <div class="FieldStyle">
        Note Body<span class="Asterix">*</span>
    </div>
    <div class="ValueStyle">
        <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
    </div>
     <div class="FieldStyle"></div>
    <div class="ValueStyle"><asp:Label ID="lblError" CssClass="Error" runat="server" Text="Label"
            Visible="False"></asp:Label>
    </div>
    <div class="ClearBoth">
        <br />
    </div>
    <div>
        <asp:ImageButton ID="btnSubmitBottom" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
        <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
    </div>

</div>

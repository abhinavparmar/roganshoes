﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using WebApp;
using Zeon.Libraries.Elmah;
using Zeon.Libraries.Utilities;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default.Accounts
{
    public partial class EditAccount : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int AccountId;
        private ZNodeAddress _billingAddress = new ZNodeAddress();
        private ZNodeAddress _shippingAddress = new ZNodeAddress();
        private int Mode;
        private string _RoleName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName
        {
            get
            {
                return this._RoleName;
            }

            set
            {
                this._RoleName = value;
            }
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSubmitTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel.gif") + "'");

            btnSubmitBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/SiteAdmin/Themes/Images/buttons/button_cancel.gif") + "'");

            if (HttpContext.Current.Request.Params["itemid"] == null)
            {
                this.AccountId = 0;
            }
            else
            {
                this.AccountId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (HttpContext.Current.Request.Params["mode"] == null)
            {
                this.Mode = 0;
            }
            else
            {
                this.Mode = int.Parse(Request.Params["mode"].ToString());
            }
            if (Request.Params["pagefrom"] != null)
            {
                this.RoleName = Request.Params["pagefrom"].ToString();

                // To validate siteadmin account
                if (RoleName == "ADMIN" || RoleName == "admin")
                {
                    lbluseridastric.Visible = true;
                    lblpwdastric.Visible = true;
                    lblEMailastric.Visible = true;
                    UseridRequired.Enabled = true;
                    PasswordRequired.Enabled = true;
                    EmailAddressRequired.Enabled = true;
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    CmpnyNameRequired.Enabled = true;
                }

            }
            if (!Page.IsPostBack)
            {
                this.BindData();
                this.RoleValidation();
                //Zeon Custom Code:Starts
                if ((RoleName.ToLower() == "customer" || RoleName.ToLower() == "order") && this.AccountId <= 0)
                {
                    ddlPortal.Visible = true;
                    ltlStores.Visible = true;
                    this.BindStore();
                }
                else
                {
                    ltlStores.Visible = false;
                    ddlPortal.Visible = false;
                }
                chkWelcomeEmail.Visible = this.AccountId <= 0 ? true : false;
                //Zeon Custom Code:Ends
            }


        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            // Zeon Custom Implementation:Replacing UserId with Email:Task Sprint 2
            bool isAccountCreated = false;//Zeon Email Subcription Implementation
            // Validate Account number should not duplicate
            if (txtExternalAccNumber.Text.Trim() != string.Empty)
            {
                AccountService accountService = new AccountService();
                TList<Account> accountList = accountService.Find(string.Format("ExternalAccountNo='{0}'", txtExternalAccNumber.Text.Trim()));

                if (this.AccountId > 0)
                {
                    accountList.Filter = string.Format("AccountID != {0}", this.AccountId);
                }

                if (accountList != null && accountList.Count > 0)
                {
                    lblErrorMsg.Text = string.Format("This Account Number \"{0}\"  already has an account associated with it. Please contact our support staff at {1} for assistance", txtExternalAccNumber.Text, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);
                    return;
                }
            }

            ZNode.Libraries.Admin.AccountAdmin _UserAccountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account _UserAccount = new Account();

            if (this.AccountId > 0)
            {
                _UserAccount = _UserAccountAdmin.GetByAccountID(this.AccountId);
            }

            if (this.AccountId == 0)
            {
                _UserAccount.CreateDte = System.DateTime.Now;
                _UserAccount.CreateUser = System.Web.HttpContext.Current.User.Identity.Name;
            }

            _UserAccount.Email = txtEmail.Text;
            _UserAccount.EmailOptIn = chkOptIn.Checked;

            // Set Account Details
            if (string.IsNullOrEmpty(txtExternalAccNumber.Text.Trim()))
            {
                _UserAccount.ExternalAccountNo = null;
            }
            else
            {
                _UserAccount.ExternalAccountNo = Server.HtmlEncode(txtExternalAccNumber.Text.Trim());
            }

            _UserAccount.Description = Server.HtmlEncode(txtDescription.Text);
            _UserAccount.CompanyName = Server.HtmlEncode(txtCompanyName.Text);
            _UserAccount.Website = Server.HtmlEncode(txtWebSite.Text.Trim());
            _UserAccount.Source = Server.HtmlEncode(txtSource.Text.Trim());

            _UserAccount.AccountProfileCode = DBNull.Value.ToString();
            _UserAccount.UpdateUser = System.Web.HttpContext.Current.User.Identity.Name;
            _UserAccount.UpdateDte = System.DateTime.Now;

            _UserAccount.UserID = _UserAccount.UserID;
            _UserAccount.AccountID = this.AccountId;

            // Custom Information Section
            _UserAccount.Custom1 = Server.HtmlEncode(txtCustom1.Text.Trim());
            _UserAccount.Custom2 = Server.HtmlEncode(txtCustom2.Text.Trim());
            _UserAccount.Custom3 = Server.HtmlEncode(txtCustom3.Text.Trim());

            try
            {
                if (_UserAccount.UserID.HasValue)
                {
                    // Get the User associated with the specified Unique identifier
                    MembershipUser user = Membership.Providers["ZNodeAdminMembershipProvider"].GetUser(_UserAccount.UserID.Value, false);

                    user.Email = txtEmail.Text.Trim();

                    // Update the database with the Email Id for the specified user
                    Membership.UpdateUser(user);

                    if (Password.Text.Length > 0)
                    {
                        // Send the password reset notification email.
                        this.SendPasswordResetNotificationMail(_UserAccount.AccountID);

                        // Update new password
                        user.ChangePassword(user.ResetPassword(), Password.Text.Trim());

                        // Update the LastPasswordChanged Date
                        _UserAccountAdmin.UpdateLastPasswordChangedDate(_UserAccount.UserID.ToString());

                        // Log password for further debugging
                        ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, Password.Text.Trim());
                    }
                }
                else
                {
                    if (UserID.Text.Trim().Length > 0)
                    {
                        //Zeon Custom Code:Starts
                        string newUsername = string.Empty;
                        if (RoleName.ToLower() == "customer" || RoleName.ToLower() == "order") { newUsername = UserID.Text.Trim() + "_" + ddlPortal.SelectedValue; } else { newUsername = UserID.Text.Trim(); }
                        //Zeon Custom Code:Ends
                        string errorMessage = string.Empty;

                        // Check password field
                        if (Password.Text.Trim().Length == 0)
                        {
                            errorMessage = " * Enter password <br />";
                            lblPwdErrorMsg.Text = " * Enter password";
                        }

                        // Show error message
                        if (errorMessage.Length > 0)
                        {
                            lblErrorMsg.Text = errorMessage;
                            return;
                        }

                        MembershipUser user = Membership.GetUser(newUsername);

                        if (user == null)
                        {

                            // Online account DOES NOT exist 
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
                            MembershipCreateStatus status = MembershipCreateStatus.ProviderError;
                            log.LogActivityTimerStart();
                            //Zeon Custom Code:Commented as part of removing security Question and answer:Starts
                            //MembershipUser newUser = Membership.CreateUser(Server.HtmlEncode(newUsername), Password.Text.Trim(), txtEmail.Text.Trim(), ddlSecretQuestions.SelectedItem.Text, Answer.Text.Trim(), true, out status);//Znode old Code
                            MembershipUser newUser = Membership.CreateUser(Server.HtmlEncode(newUsername), Password.Text.Trim(), txtEmail.Text.Trim());
                            //Zeon Custom Code:Commented as part of removing security Question and answer:Ends
                            if (newUser.IsApproved)
                            {
                                log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, newUsername);

                                // Log password for further debugging
                                ZNodeUserAccount.LogPassword((Guid)newUser.ProviderUserKey, Password.Text.Trim());

                                // Update provider user key
                                _UserAccount.UserID = new Guid(newUser.ProviderUserKey.ToString());

                                if (RoleName.ToLower() == "admin")
                                {
                                    // Associate the User with the selected role
                                    Roles.AddUserToRole(newUser.UserName, "ADMIN");

                                    // Create an Profile for the selected user
                                    ProfileCommon newProfile = (ProfileCommon)ProfileCommon.Create(newUser.UserName, true);

                                    // Properties Value
                                    newProfile.StoreAccess = "AllStores";

                                    // Save profile - must be done since we explicitly created it 
                                    newProfile.Save();
                                }

                                //Zeon Custom Code to Send Mail to User:Starts
                                if (chkWelcomeEmail.Checked)
                                {
                                    SendAccountConfirmationEmail(UserID.Text.Trim());
                                }
                                //Zeon Custom Code to Send Mail to User:Ends
                            }
                            else
                            {
                                log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, newUsername);
                                lblErrorMsg.Text = "Unable to create online account. Please try again.";
                                return;
                            }
                        }
                        else
                        {
                            lblErrorMsg.Text = "User Id already exists. Please use differnet user id.";
                            return;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                lblErrorMsg.Text = exception.Message;
                return;
            }

            bool Check = false;

            if (this.AccountId > 0)
            {
                // Set update date
                _UserAccount.UpdateDte = System.DateTime.Now;

                Check = _UserAccountAdmin.Update(_UserAccount);

                // Log Activity
                string AssociateName = "Edit of Account - " + UserID.Text.Trim();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, "Account");
                isAccountCreated = true;
            }
            else
            {

                //Zeon Custom Code:Starts for assigning PortalID to ZnodeAccount
                DataSet dsPortalProfile = new DataSet();
                PortalAdmin portalAdmin = new PortalAdmin();
                Portal currentPortal = new Portal();
                if (RoleName.ToLower() == "customer" || RoleName.ToLower() == "order")
                {
                    currentPortal = portalAdmin.GetByPortalId(int.Parse(ddlPortal.SelectedValue));
                    if (currentPortal != null && currentPortal.DefaultRegisteredProfileID > 0)
                    {
                        _UserAccount.ProfileID = currentPortal.DefaultRegisteredProfileID;
                    }
                }
                //Zeon Custom Code:Ends for assigning PortalID to ZnodeAccount
                Check = _UserAccountAdmin.Insert(_UserAccount);
                //Zeon Custom Code:Starts
                if (RoleName.ToLower() == "customer" || RoleName.ToLower() == "order")
                {
                    if (currentPortal != null && currentPortal.DefaultRegisteredProfileID > 0)
                    {
                        AccountProfileAdmin accountProfileAdmin = new AccountProfileAdmin();
                        AccountProfile accountProfile = new AccountProfile();
                        accountProfile.ProfileID = currentPortal.DefaultRegisteredProfileID;
                        accountProfile.AccountID = _UserAccount.AccountID;
                        accountProfileAdmin.Insert(accountProfile);
                    }
                }
                isAccountCreated = true;
                ZNode.Libraries.DataAccess.Entities.Address billingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
                ZNode.Libraries.DataAccess.Entities.Address shippingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
                AddressService addressService = new AddressService();
                billingAddress.Name = "Default Billing";
                billingAddress.FirstName = string.Empty;
                billingAddress.MiddleName = string.Empty;
                billingAddress.LastName = string.Empty;
                billingAddress.CompanyName = string.Empty;
                billingAddress.Street = string.Empty;
                billingAddress.Street1 = string.Empty;
                billingAddress.City = string.Empty;
                billingAddress.StateCode = string.Empty;
                billingAddress.PostalCode = string.Empty;
                billingAddress.CountryCode = string.Empty;
                billingAddress.PhoneNumber = string.Empty;
                billingAddress.IsDefaultBilling = true;
                billingAddress.IsDefaultShipping = false;
                billingAddress.AccountID = _UserAccount.AccountID;
                addressService.Insert(billingAddress);
                _UserAccount.AddressCollection.Add(billingAddress);

                shippingAddress.Name = "Default Shipping";
                shippingAddress.FirstName = string.Empty;
                shippingAddress.MiddleName = string.Empty;
                shippingAddress.LastName = string.Empty;
                shippingAddress.CompanyName = string.Empty;
                shippingAddress.Street = string.Empty;
                shippingAddress.Street1 = string.Empty;
                shippingAddress.City = string.Empty;
                shippingAddress.StateCode = string.Empty;
                shippingAddress.PostalCode = string.Empty;
                shippingAddress.CountryCode = string.Empty;
                shippingAddress.PhoneNumber = string.Empty;
                shippingAddress.AccountID = _UserAccount.AccountID;
                shippingAddress.IsDefaultBilling = false;
                shippingAddress.IsDefaultShipping = true;
                addressService.Insert(shippingAddress);
                _UserAccount.AddressCollection.Add(shippingAddress);
                //Zeon Custom Code:Ends
                // Log Activity
                string AssociateName = "Creation of Account - " + UserID.Text.Trim();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, "Account");
            }
            //Zeon Email Subscription Code :Start

            if (isAccountCreated)
            {
                if (chkOptIn.Checked)
                {
                    SubscribeEmailUser(UserID.Text.Trim());
                }
                else
                {
                    if (!string.IsNullOrEmpty(_UserAccount.Custom2))
                    {
                        UnSubscribeEmailUser(_UserAccount.Custom2);
                    }
                }
            }

            if (Request.Params["pagefrom"] != null && Request.Params["pagefrom"] == "ORDER" && _UserAccount.AccountID > 0)
            {
                SetCustomerForCreateOrderAndRedirectToWebSite(_UserAccount.AccountID);
            }

            //Zeon Email Subscription Code :Start

            // Check Boolean Value
            if (Check)
            {
                if (RoleName.ToLower() == "admin")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?mode=" + this.Mode + "&itemid=" + _UserAccount.AccountID + "&pagefrom=admin");
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/View.aspx?mode=" + this.Mode + "&itemid=" + _UserAccount.AccountID + "&pagefrom=franchise");
                }
                else if (RoleName.ToLower() == "vendor")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/View.aspx?mode=" + this.Mode + "&itemid=" + _UserAccount.AccountID + "&pagefrom=vendor");
                }
                else
                {
                    Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + this.Mode + "&itemid=" + _UserAccount.AccountID + "&pagefrom=customer");
                }
            }
            else
            {
                lblErrorMsg.Text = string.Empty;
            }
        }

        /// <summary>
        /// Send the password notification mail to the user.
        /// </summary>
        /// <param name="accountId"> The value of Account Id </param>
        protected void SendPasswordResetNotificationMail(int accountId)
        {
            AccountAdmin accountAdmin = new AccountAdmin();
            Account account = accountAdmin.GetByAccountID(accountId);

            string customerName = string.Empty;
            string portalLogo = ZCommonHelper.GetLogoHTML();
            ZNode.Libraries.DataAccess.Entities.Address address = accountAdmin.GetDefaultBillingAddress(accountId);
            if (address != null)
            {
                customerName = address.FirstName;
            }

            MembershipUser user = Membership.GetUser(account.UserID.Value);
            System.Web.Profile.ProfileBase profile = System.Web.Profile.ProfileBase.Create(user.UserName, true);
            string stores = (string)profile.GetPropertyValue("StoreAccess");

            // Get the accounts portal Id
            int portalId = 1;
            if (stores == "AllStores" || stores == string.Empty)
            {
                portalId = 1;
            }
            else
            {
                portalId = Convert.ToInt32(stores.Split(',')[0]);
            }

            string storeName = string.Empty;
            string customerServicePhoneNo = string.Empty;
            PortalAdmin portalAdmin = new PortalAdmin();
            Portal portal = portalAdmin.GetByPortalId(portalId);

            if (portal != null)
            {
                storeName = portal.StoreName;
                customerServicePhoneNo = portal.CustomerServicePhoneNumber;
            }

            if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SMTPServer) &&
                !string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.CustomerServiceEmail) &&
                !string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SMTPPassword))
            {
                string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
                string subject = string.Format("{0} - Password Reset", storeName);
                string toAddress = account.Email;
                string body = string.Empty;
                string templateFile = Path.Combine(HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath), "ResetPasswordNotification.htm");

                // Check whether the template file exist or not.
                if (!File.Exists(templateFile))
                {
                    lblErrorMsg.Text = "Template file doesn't exist.";
                    return;
                }

                using (StreamReader rw = new StreamReader(templateFile))
                {
                    body = rw.ReadToEnd();
                    rw.Close();
                }

                Regex regularExpName = new Regex("#BillingFirstName#", RegexOptions.IgnoreCase);
                body = regularExpName.Replace(body, customerName);

                Regex regularExpCustomerServiceNo = new Regex("#CUSTOMERSUPPORT#", RegexOptions.IgnoreCase);
                body = regularExpCustomerServiceNo.Replace(body, customerServicePhoneNo);

                string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
                string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
                int portalIdTemp = ZNodeConfigManager.SiteConfig.PortalID;

                ZEmailHelper zEmailHelper = new ZEmailHelper();
                ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
                string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

                string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalIdTemp, spacerImage);
                string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalIdTemp, copyRightText, spacerImage);

                //To add header to the template
                Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
                body = rxHeaderContent.Replace(body, messageTextHeader);

                //To add footer to the template
                Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
                body = rxFooterContent.Replace(body, messageTextFooter);

                try
                {
                    ZNodeEmail.SendEmail(toAddress, senderEmail, String.Empty, subject, body, true);
                }
                catch (Exception ex)
                {
                    string msg = string.Format("Could not send the password reset notification message. " + ex.Message);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(msg);
                }
            }
            else
            {
                string msg = string.Format("Could not send the password reset notification message. SMTP setting is incomplete for the store " + storeName);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(msg);
            }
        }

        /// <summary>
        ///  Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.AccountId > 0)
            {
                if (RoleName.ToLower() == "admin")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?mode=" + this.Mode + "&itemid=" + this.AccountId + "&pagefrom=admin");
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/View.aspx?mode=" + this.Mode + "&itemid=" + this.AccountId + "&pagefrom=franchise");
                }
                else if (RoleName.ToLower() == "vendor")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/View.aspx?mode=" + this.Mode + "&itemid=" + this.AccountId + "&pagefrom=vendor");
                }
                else
                {
                    Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + this.Mode + "&itemid=" + this.AccountId + "&pagefrom=customer");
                }

            }
            else
            {
                if (RoleName.ToLower() == "admin")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/FranchiseAdministrators/Default.aspx");
                }
                else if (RoleName.ToLower() == "vendor")
                {
                    Response.Redirect("~/SiteAdmin/Secure/Vendors/VendorAccounts/Default.aspx");
                }
                else
                {
                    Response.Redirect("~/SiteAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx");
                }
            }
        }

        #endregion

        #region Private Methods

        private void RoleValidation()
        {
            if (RoleName.ToLower() == "admin")
            {
                pnlAccount.Visible = false;
                pnlSubTitle.Visible = false;
                pnlCustomerInformation.Visible = false;
                if (this.AccountId > 0)
                {
                    lblTitle.Text = "Edit Store Administrator";
                }
                else
                {
                    lblTitle.Text = "Add Store Administrator";
                }
            }
            else if (RoleName.ToLower() == "franchise")
            {
                if (this.AccountId > 0)
                {
                    lblTitle.Text = "Edit Customer Information";
                }
                else
                {
                    lblTitle.Text = "Add Customer Information";
                }
            }
            else if (RoleName.ToLower() == "vendor")
            {

                if (this.AccountId > 0)
                {
                    lblTitle.Text = "Edit Customer Information";
                }
                else
                {
                    lblTitle.Text = "Add Customer Information";
                }
            }
            else
            {
                if (this.AccountId > 0)
                {
                    lblTitle.Text = "Edit Customer Information";
                }
                else
                {
                    lblTitle.Text = "Add Customer Information";
                }
            }
        }

        /// <summary>
        /// Bind Account Details
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.AccountAdmin _UserAccountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account _UserAccount = _UserAccountAdmin.GetByAccountID(this.AccountId);
            if (_UserAccount != null)
            {
                // General Information
                if (string.IsNullOrEmpty(_UserAccount.ExternalAccountNo))
                {
                    txtExternalAccNumber.Text = string.Empty;
                }
                else
                {
                    txtExternalAccNumber.Text = _UserAccount.ExternalAccountNo;
                }

                txtCompanyName.Text = Server.HtmlDecode(_UserAccount.CompanyName);
                txtWebSite.Text = _UserAccount.Website;
                txtSource.Text = Server.HtmlDecode(_UserAccount.Source);

                // Description
                txtDescription.Text = Server.HtmlDecode(_UserAccount.Description);

                // Login Info
                if (_UserAccount.UserID.HasValue)
                {
                    UserID.Enabled = false;
                    MembershipUser _user = Membership.GetUser(_UserAccount.UserID.Value);
                    //Zeon Custom Code :Starts
                    string username = _user.UserName.Trim();
                    if (username.IndexOf('_') > 0) { ddlPortal.SelectedValue = username.Substring(username.LastIndexOf('_') + 1); }
                    if (username.IndexOf('_') > 0) { username = username.Substring(0, username.LastIndexOf('_')); }
                    UserID.Text = username;
                    //ddlSecretQuestions.Text = _user.PasswordQuestion;
                    //Zeon Custom Code :Ends

                }

                txtEmail.Text = _UserAccount.Email;
                chkOptIn.Checked = _UserAccount.EmailOptIn;

                // Custom properties
                txtCustom1.Text = Server.HtmlDecode(_UserAccount.Custom1);
                txtCustom2.Text = Server.HtmlDecode(_UserAccount.Custom2);
                txtCustom3.Text = Server.HtmlDecode(_UserAccount.Custom3);
            }
        }
        #endregion

        #region Zeon Custom Methods

        /// <summary>
        /// Bind All Store
        /// </summary>
        private void BindStore()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            ddlPortal.DataSource = (this.RoleName.ToLower().Equals("order")) ? portals.FindAll(X => X.PortalID == ZNodeConfigManager.SiteConfig.PortalID) : portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
            ddlPortal.SelectedValue = ZNodeConfigManager.SiteConfig.PortalID.ToString();
        }

        #region  Email Subscription Mail
        /// <summary>
        /// Subscribe User For Email Service
        /// </summary>
        /// <param name="userEmail">string</param>
        private void SubscribeEmailUser(string userEmail)
        {
            string responseContactID = string.Empty;
            try
            {
                if (ConfigurationManager.AppSettings["GetResponseAPIKey"] != null && ConfigurationManager.AppSettings["GetResponseAPIURL"] != null)
                {
                    string newsLetterCampaign = GetCompaignNameByPortal();
                    if (!string.IsNullOrEmpty(newsLetterCampaign))
                    {
                        GetResponseEmailSubscriber emailSubscriber = new GetResponseEmailSubscriber();
                        emailSubscriber.AddContactEmailSubcriber(ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString(), ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString(), newsLetterCampaign, userEmail, userEmail);

                    }
                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }
        }

        /// <summary>
        /// Get Newsletter Compaign Name by Portal
        /// </summary>
        /// <returns></returns>
        private string GetCompaignNameByPortal()
        {
            string compaignName = string.Empty;
            PortalExtnService portalExtnService = new PortalExtnService();
            TList<PortalExtn> currentPortal = portalExtnService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID);
            if (currentPortal != null && currentPortal.Count > 0)
            {
                compaignName = currentPortal[0].NewsLetterCampaignName;
            }

            return compaignName;
        }


        /// <summary>
        /// Subscribe User For Email Service
        /// </summary>
        /// <param name="contactId">string</param>
        private void UnSubscribeEmailUser(string contactId)
        {
            string responseContactID = string.Empty;
            try
            {
                if (ConfigurationManager.AppSettings["GetResponseAPIKey"] != null && ConfigurationManager.AppSettings["GetResponseAPIURL"] != null)
                {
                    string newsLetterCampaign = GetCompaignNameByPortal();
                    if (!string.IsNullOrEmpty(newsLetterCampaign))
                    {
                        GetResponseEmailSubscriber emailSubscriber = new GetResponseEmailSubscriber();
                        emailSubscriber.UnsubscribeContactEmail(ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString(), ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString(), contactId);

                    }
                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }
        }
        #endregion

        #region Account Creation Success Mail
        /// <summary>
        /// To Account confirmation Email
        /// </summary>
        /// <param name="Email">Email Id of the register user</param>
        ///
        private void SendAccountConfirmationEmail(string email)
        {
            try
            {
                string accountConfirmationEmailTemplate = GetAccountConfirmationEmailTemplate();
                ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();

                if (!string.IsNullOrEmpty(accountConfirmationEmailTemplate))
                {
                    ZNodeEmail.SendEmail(email, ZNodeConfigManager.SiteConfig.CustomerServiceEmail, string.Empty, zeonMessageConfig.GetMessageKey("EmailConfirmationSubject"), accountConfirmationEmailTemplate, true);
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Register user email template sending problem(Mehtod name: SendAccountConfirmationEmail()):" + ex.StackTrace);
            }
        }

        /// <summary>
        /// To Get Account Confriumation email template with body content
        /// </summary>
        /// <returns></returns>
        private string GetAccountConfirmationEmailTemplate()
        {
            string userName = string.Empty;
            string password = string.Empty;
            string defaultTemplatePath = string.Empty;
            bool isChecked = false;

            userName = UserID.Text.Trim().Replace("_" + ZNodeConfigManager.SiteConfig.PortalID.ToString(), "");
            password = Password.Text.Trim();
            isChecked = chkOptIn.Checked;

            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ZAccountConfirmationEmail.html"));

            StreamReader streamReader = new StreamReader(defaultTemplatePath);
            string messageText = streamReader.ReadToEnd();

            Regex rxUserName = new Regex("#UserName#", RegexOptions.IgnoreCase);
            messageText = rxUserName.Replace(messageText, userName);

            Regex rxPassword = new Regex("#Password#", RegexOptions.IgnoreCase);
            messageText = rxPassword.Replace(messageText, password);

            Regex rxisChecked = new Regex("#not#", RegexOptions.IgnoreCase);

            messageText = isChecked ? rxisChecked.Replace(messageText, " ") : rxisChecked.Replace(messageText, "not ");
            return messageText;
        }
        #endregion

        private void SetCustomerForCreateOrderAndRedirectToWebSite(int accountID)
        {
            AccountService accountService = new AccountService();
            Account selectedUserAccount = accountService.GetByAccountID(accountID);
            if (selectedUserAccount != null)
            {
                ZNodeUserAccount userAccountFromAdmin = new ZNodeUserAccount(selectedUserAccount);
                // Set Page session object
                // Get inserted Address Id
                AccountAdmin accountAdmin = new AccountAdmin();
                ZNode.Libraries.DataAccess.Entities.Address address = accountAdmin.GetDefaultBillingAddress(accountID);
                if (address != null)
                {
                    userAccountFromAdmin.BillingAddress = address;
                }

                address = accountAdmin.GetDefaultShippingAddress(accountID);
                if (address != null)
                {
                    userAccountFromAdmin.ShippingAddress = address;
                }

                ZNode.Libraries.DataAccess.Custom.AccountHelper accountHelper = new ZNode.Libraries.DataAccess.Custom.AccountHelper();
                int _ProfileID = accountHelper.GetCustomerProfile(selectedUserAccount.AccountID, ZNodeConfigManager.SiteConfig.PortalID);

                if (_ProfileID != 0)
                {
                    ZNode.Libraries.Admin.ProfileAdmin profileAdmin = new ZNode.Libraries.Admin.ProfileAdmin();
                    HttpContext.Current.Session["ProfileCache"] = profileAdmin.GetByProfileID(_ProfileID);
                }
                Session["BuyerUser"] = userAccountFromAdmin;
                RedirectToSiteUrl("~/default.aspx", Resources.CommonCaption.AdminCreateOrderTarget, string.Empty);
            }
        }

        /// <summary>
        /// Redirecting to site
        /// </summary>
        /// <param name="url">url for site</param>
        /// <param name="target"></param>
        /// <param name="windowFeatures"></param>
        private void RedirectToSiteUrl(string url, string target, string windowFeatures)
        {
            HttpContext context = HttpContext.Current;
            if ((String.IsNullOrEmpty(target) || target.Equals(Resources.CommonCaption.AdminCreateOrderTarget, StringComparison.OrdinalIgnoreCase)) && String.IsNullOrEmpty(windowFeatures))
            {
                context.Response.Redirect(url);
            }
            else
            {
                Page page = (Page)context.Handler;
                if (page == null)
                {
                    throw new InvalidOperationException("Cannot redirect to new window outside Page context.");
                }
                url = page.ResolveClientUrl(url);
                string script;
                if (!String.IsNullOrEmpty(windowFeatures))
                {
                    script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
                }
                else
                {
                    script = @"window.open(""{0}"", ""{1}"");";
                }
                script = String.Format(script, url, target, windowFeatures);
                ScriptManager.RegisterStartupScript(page, typeof(Page), "Redirect", script, true);
            }
        }

        #endregion
    }
}
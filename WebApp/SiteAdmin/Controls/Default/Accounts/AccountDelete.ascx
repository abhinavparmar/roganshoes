﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountDelete.ascx.cs" Inherits="SiteAdmin.Controls.Default.Accounts.AccountDelete" %>

<%@ Register Src="~/SiteAdmin/Controls/Default/DemoMode.ascx" TagName="DemoMode" TagPrefix="ZNode" %>


<h5>Please Confirm<ZNode:DemoMode ID="DemoMode1" runat="server" />
</h5>
<p>
    Please confirm if you want to delete this account. Note that this change cannot
        be undone.
</p>
 <div class="LeftFloat"><asp:Label ID="lblMsg" runat="server"  CssClass="Error" ></asp:Label></div><br />
<br />
<div>
    <asp:ImageButton ID="btnDelete" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_delete.gif"
        runat="server" AlternateText="Delete" OnClick="BtnDelete_Click" CausesValidation="true" />
    <asp:ImageButton ID="btnCancel" CausesValidation="False" ImageUrl="~/SiteAdmin/Themes/images/buttons/button_cancel.gif"
        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
</div>
<br />

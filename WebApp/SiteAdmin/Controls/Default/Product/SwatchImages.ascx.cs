using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace SiteAdmin.Controls.Default.Product
{
    /// <summary>
    /// Represents the SwatchImages user control class
    /// </summary>
    public partial class SwatchImages : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private bool _IsProductPage = false;
        private int _ProductId;
        private ZNodeProduct product = new ZNodeProduct();
        private bool _HasImages = false;
        private int _MaxDisplayColumns = ZNodeConfigManager.SiteConfig.MaxCatalogCategoryDisplayThumbnails;
        private bool _IncludeProductImage = true;
        private string _ImageSizePath = "thumbnail";
        private string _ControlType = string.Empty;
        private ZNodeImage znodeImage = new ZNodeImage();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets a value indicating whether there are related images in this control.
        /// </summary>
        public bool HasImages
        {
            get
            {
                return this._HasImages;
            }
        }

        /// <summary>
        /// Gets or sets the Maximum number of columns used to display the images. Defaults to Max Catalog Display Columns in General Settings.
        /// </summary>
        public int MaxDisplayColumns
        {
            get
            {
                return this._MaxDisplayColumns;
            }

            set
            {
                this._MaxDisplayColumns = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to include the main product picture with the images displayed.
        /// </summary>
        public bool IncludeProductImage
        {
            get
            {
                return this._IncludeProductImage;
            }

            set
            {
                this._IncludeProductImage = value;
            }
        }

        /// <summary>
        /// Gets or sets the directory the images should be pulled from (thumbnail, small, medium or large). You do not need to specify the whole path.
        /// </summary>
        public string ImageSizePath
        {
            get
            {
                return this._ImageSizePath;
            }

            set
            {
                this._ImageSizePath = value;
            }
        }

        /// <summary>
        /// Gets or sets the ControlType for the AdditionalImages
        /// </summary>
        public string ControlType
        {
            get
            {
                return this._ControlType;
            }

            set
            {
                this._ControlType = value;
            }
        }

        /// <summary>
        /// Gets or sets the Product Id
        /// </summary>
        public int ProductId
        {
            get
            {
                return this._ProductId;
            }

            set
            {
                this._ProductId = value;
                this.BindImage();
            }
        }

        #endregion

        #region Bind Method
        /// <summary>
        /// Bind Image Method
        /// </summary>
        public void BindImage()
        {
            if (Request.Params["itemId"] != null)
            {
                this._ProductId = int.Parse(Request.Params["itemId"]);

                this._IsProductPage = true;

                // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
                this.product = (ZNodeProduct)HttpContext.Current.Items["Product"];
            }

            if (this._ProductId > 0)
            {
                ZNode.Libraries.DataAccess.Service.ProductImageService service = new ZNode.Libraries.DataAccess.Service.ProductImageService();

                // Set up our grid.
                DataListSwatches.RepeatColumns = this._MaxDisplayColumns;

                // Set up our grid.
                DataListAlternateImages.RepeatColumns = this._MaxDisplayColumns;

                if (this.ControlType == "AlternateImage")
                {
                    TList<ProductImage> alternateImages = service.GetByProductIDActiveIndProductImageTypeID(this._ProductId, true, 1);
                    alternateImages.Sort("DisplayOrder");

                    DataListAlternateImages.DataSource = alternateImages;
                    DataListAlternateImages.DataBind();
                    foreach (DataListItem item in DataListAlternateImages.Items)
                    {
                        item.EnableViewState = false;
                    }

                    this._HasImages = alternateImages.Count > 0;
                }
                else if (this.ControlType == "Swatches")
                {
                    TList<ProductImage> swatchesList = new TList<ProductImage>();

                    // Pick category page swatches.
                    if (!this._IsProductPage)
                    {
                        swatchesList = service.Find("ProductId = " + this._ProductId.ToString() + " AND ProductImageTypeID = 2 AND ShowOnCategoryPage = true");
                    }
                    else
                    {
                        // Pick Product Page swatches.
                        swatchesList = service.GetByProductIDActiveIndProductImageTypeID(this._ProductId, true, 2);
                    }

                    swatchesList.Sort("DisplayOrder");
                    swatchesList.Filter = "ReviewStateId = 20";

                    if (this._IsProductPage && this.product != null && this.product.ProductID > 0 && swatchesList != null && swatchesList.Count > 0)
                    {
                        ProductImage defaultProductImage = new ProductImage();
                        defaultProductImage.ActiveInd = true;
                        defaultProductImage.ImageAltTag = this.product.ImageAltTag;
                        defaultProductImage.ImageFile = this.product.ImageFile;
                        defaultProductImage.AlternateThumbnailImageFile = this.product.ImageFile;
                        defaultProductImage.Name = this.product.Name;
                        defaultProductImage.ProductID = this.product.ProductID;

                        swatchesList.Add(defaultProductImage);
                    }

                    DataListSwatches.DataSource = swatchesList;
                    DataListSwatches.DataBind();

                    foreach (DataListItem item in DataListSwatches.Items)
                    {
                        item.EnableViewState = false;
                    }

                    this._HasImages = swatchesList.Count > 0;
                }
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Get catalog product image Name
        /// </summary>
        /// <param name="title">The value of title</param>
        /// <param name="imageFileName">Image File Name</param>
        /// <returns>Returns the Image Name</returns>
        protected string GetImageName(string title, string imageFileName)
        {
            if (title.Trim().Length > 0)
            {
                return title;
            }
            else if (imageFileName.Length > 0)
            {
                return imageFileName.Remove(imageFileName.IndexOf('.'));
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Show Image Method
        /// </summary>
        /// <param name="ShowOnCategoryPage">Show On Category Page</param>
        /// <returns>Returns a bool value to show Image</returns>
        protected bool ShowImage(object ShowOnCategoryPage)
        {
            return this._IsProductPage || bool.Parse(ShowOnCategoryPage.ToString());
        }

        /// <summary>
        /// Get product image alternative text
        /// </summary>
        /// <param name="altTagText">Alternate Tag Text</param>
        /// <param name="productName">Product Name</param>
        /// <returns>Returns the alternative text</returns>
        protected string GetImageAltTagText(object altTagText, string productName)
        {
            if (altTagText == null)
            {
                return productName;
            }
            else if (altTagText.ToString().Length == 0)
            {
                return productName;
            }
            else
            {
                return altTagText.ToString();
            }
        }

        #endregion

        #region DataList Events
        /// <summary>
        /// DataList Item Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DataListSwatches_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            string clickImg = string.Empty;
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    ProductImage img = (ProductImage)e.Item.DataItem;
                    clickImg = img.AlternateThumbnailImageFile;
                    if (string.IsNullOrEmpty(clickImg))
                    {
                        clickImg = img.ImageFile;
                    }

                    if (this._IsProductPage)
                    {
                        clickImg = this.znodeImage.GetImageHttpPathMedium(clickImg);
                    }
                    else
                    {
                        clickImg = this.znodeImage.GetImageHttpPathSmall(clickImg);
                    }

                    HtmlImage imgSwatch = (HtmlImage)e.Item.FindControl("imgSwatch");

                    if (this._IsProductPage)
                    {
                        imgSwatch.Attributes.Add("onclick", "document.getElementById('CatalogItemReviewImage').src='" + ResolveUrl(clickImg) + "';");
                    }
                    else
                    {
                        imgSwatch.Attributes.Add("onclick", "document.getElementById('Img" + this._ProductId + "').src='" + ResolveUrl(clickImg) + "';");
                    }
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }
        }

        protected string GetImagePath(string fileName, bool useSwatch)
        {
            string sourceFileName = string.Empty;
            if (!useSwatch)
            {
                sourceFileName = this.znodeImage.GetImageHttpPathSmallThumbnail(fileName);
            }
            else
            {
                sourceFileName = this.znodeImage.GetImageHttpPathSmallThumbnail(fileName.ToLower().Replace(".", "-swatch."));
            }

            return sourceFileName;
        }

        /// <summary>
        /// DataList Alternate Images Item Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DataListAlternateImages_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            string clickImg = string.Empty;
            try
            {
                ProductImage img = (ProductImage)e.Item.DataItem;
                clickImg = img.AlternateThumbnailImageFile;
                if (string.IsNullOrEmpty(clickImg))
                {
                    clickImg = img.ImageFile;
                }

                if (this._IsProductPage)
                {
                    clickImg = this.znodeImage.GetImageHttpPathMedium(clickImg);
                }
                else
                {
                    clickImg = this.znodeImage.GetImageHttpPathSmall(clickImg);
                }

                HtmlImage imgSwatch = (HtmlImage)e.Item.FindControl("imgSwatch");

                if (this._IsProductPage)
                {
                    imgSwatch.Attributes.Add("onclick", "document.getElementById('CatalogItemImage').src='" + ResolveUrl(clickImg) + "';");
                }
                else
                {
                    imgSwatch.Attributes.Add("onclick", "document.getElementsByName('img" + this._ProductId + "')[0].src='" + ResolveUrl(clickImg) + "';");
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }
        }
        #endregion
    }
}

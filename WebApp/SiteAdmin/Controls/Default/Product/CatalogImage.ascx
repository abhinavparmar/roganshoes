﻿<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="SiteAdmin.Controls.Default.Product.CatalogImage" CodeBehind="CatalogImage.ascx.cs" %>

<script type="text/javascript" language="javascript">
    var catalogImageItems;
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(PageLoadedEventHandler);

    //
    function PageLoadedEventHandler() {
        window.addEvents({
            'domready': function () {
                /* thumbnails example , div containers */
                catalogImageItems = new SlideItMoo({
                    overallContainer: 'CatalogImageReview_outer',
                    elementScrolled: 'CatalogImageReview_inner',
                    thumbsContainer: 'CatalogImageReview_items',
                    itemWidth: 260,
                    itemsVisible: 1,
                    elemsSlide: 1,
                    duration: 500,
                    itemsSelector: '.CatalogImageReview_element',
                    showControls: 1
                });
            }
        });
    }
</script>

<!--thumbnails slideshow begin-->
<div id="CatalogImageReview_outer">
    <div id="CatalogImageReview_inner">
        <div id="CatalogImageReview_items">
            <span>
                <asp:PlaceHolder ID="uxPlaceHolder" runat="server"></asp:PlaceHolder>
            </span>
        </div>
    </div>
</div>

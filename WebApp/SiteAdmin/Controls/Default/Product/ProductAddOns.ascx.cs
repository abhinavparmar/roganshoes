using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace SiteAdmin.Controls.Default.Product
{
    /// <summary>
    /// Represents the Product Addons user control class
    /// </summary>
    public partial class ProductAddOns : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct product;
        private int ProductId = 0;
        private bool _ShowRadioButtonsVerically = false;
        private bool _ShowCheckBoxesVertically = false;
        #endregion

        // Public Event Handler
        public event System.EventHandler SelectedIndexChanged;

        #region Public properties

        /// <summary>
        /// Gets or sets a value indicating whether to show radio buttons vertically
        /// </summary>
        public bool ShowRadioButtonsVerically
        {
            get
            {
                return this._ShowRadioButtonsVerically;
            }

            set
            {
                this._ShowRadioButtonsVerically = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show check boxes vertically
        /// </summary>
        public bool ShowCheckBoxesVertically
        {
            get
            {
                return this._ShowCheckBoxesVertically;
            }

            set
            {
                this._ShowCheckBoxesVertically = value;
            }
        }

        #endregion

        #region Helper  methods
        /// <summary>
        /// Returns current selected addons from the list
        /// </summary>
        /// <param name="status">The value of status</param>
        /// <param name="message"> The value of Message</param>
        /// <returns>Returns the selected addons</returns>
        public ZNodeAddOnList GetSelectedAddOns(out bool status, out string message)
        {
            bool retValue = false;
            string statusMessage = string.Empty;

            System.Text.StringBuilder addOnValues = new System.Text.StringBuilder();

            foreach (ZNodeAddOn AddOn in this.product.ZNodeAddOnCollection)
            {
                // Bind RadioButton Control
                if (AddOn.DisplayType == "RadioButton")
                {
                    System.Web.UI.WebControls.RadioButtonList rbtnlistControl = new RadioButtonList();
                    rbtnlistControl = (System.Web.UI.WebControls.RadioButtonList)ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        if (rbtnlistControl.SelectedValue != string.Empty)
                        {
                            int selvalue = int.Parse(rbtnlistControl.SelectedValue);

                            if (selvalue > 0 && AddOnValue.AddOnValueID.ToString() == rbtnlistControl.SelectedValue)
                            {
                                // Check for quantity on hand and back-order,track inventory settings
                                if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                                {
                                    statusMessage = this.GetLocalResourceObject("OutOfStock").ToString().Replace("<name>", AddOn.Name);
                                    retValue = true;
                                }

                                // Add to Selected Addon list for this product
                                addOnValues.Append(AddOnValue.AddOnValueID.ToString());
                                break;
                            }
                        }
                    }
                }

                // BindCheckBox
                if (AddOn.DisplayType == "CheckBox")
                {
                    System.Web.UI.WebControls.CheckBoxList chkBoxListControl = new CheckBoxList();
                    chkBoxListControl = (System.Web.UI.WebControls.CheckBoxList)ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        // Loop through the Add-on values for each Add-on
                        foreach (ListItem listCheckedItem in chkBoxListControl.Items)
                        {
                            if (listCheckedItem.Selected)
                            {
                                // Optional Addons are not selected,then leave those addons 
                                // If optinal Addons are selected, it should add with the Selected item 
                                // Check for Selected Addon value 
                                if (AddOnValue.AddOnValueID.ToString() == listCheckedItem.Value)
                                {
                                    // Check for quantity on hand and back-order,track inventory settings
                                    if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                                    {
                                        statusMessage = this.GetLocalResourceObject("OutOfStock").ToString().Replace("<name>", AddOn.Name);
                                        retValue = true;
                                    }

                                    // Add to Selected Addon list for this product
                                    addOnValues.Append(AddOnValue.AddOnValueID.ToString());
                                    addOnValues.Append(",");
                                    break;
                                }
                            }
                        }
                    }
                }

                if (AddOn.DisplayType == "DropDownList")
                {
                    // Default DropDownList Control
                    System.Web.UI.WebControls.DropDownList ddlistControl = new DropDownList();
                    ddlistControl = (System.Web.UI.WebControls.DropDownList)ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        int selvalue = int.Parse(ddlistControl.SelectedValue);

                        if (selvalue > 0 && AddOnValue.AddOnValueID.ToString() == ddlistControl.SelectedValue)
                        {
                            // Check for quantity on hand and back-order,track inventory settings
                            if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                            {
                                statusMessage = this.GetLocalResourceObject("OutOfStock").ToString().Replace("<name>", AddOn.Name);
                                retValue = true;
                            }

                            // Add to Selected Addon list for this product
                            addOnValues.Append(AddOnValue.AddOnValueID.ToString());
                            break;
                        }
                    }
                }
            }

            ZNodeAddOnList _AddOnList = new ZNodeAddOnList();

            if (addOnValues.Length > 0)
            {
                // Get a sku based on attributes selected
                _AddOnList = ZNodeAddOnList.CreateByProductAndAddOns(this.product.ProductID, addOnValues.ToString());
            }

            status = retValue;
            message = statusMessage;

            return _AddOnList;
        }

        /// <summary>
        /// Validate selected attributes and return a user message
        /// </summary>
        /// <param name="message">The value of Message</param>
        /// <param name="selectedAddOnList">Selected Addon List</param>
        /// <returns>Returns a bool value to Validate Addons </returns>
        public bool ValidateAddOns(out string message, out ZNodeAddOnList selectedAddOnList)
        {
            System.Text.StringBuilder addOnValues = new System.Text.StringBuilder();
            bool status = false;
            selectedAddOnList = new ZNodeAddOnList();

            foreach (ZNodeAddOn AddOn in this.product.ZNodeAddOnCollection)
            {
                status = false;

                // Bind RadioButton Control
                if (AddOn.DisplayType == "TextBox")
                {
                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        status = false;

                        TextBox textBoxControl = new TextBox();
                        textBoxControl = (TextBox)ControlsPlaceHolder.FindControl("txtBoxAddOn" + AddOnValue.AddOnValueID.ToString());

                        if (textBoxControl != null &&
                            !string.IsNullOrEmpty(textBoxControl.Text.Trim()))
                        {
                            if (addOnValues.Length > 0)
                            {
                                addOnValues.Append(",");
                            }

                            // Check for quantity on hand and back-order,track inventory settings
                            if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                            {
                                message = this.GetLocalResourceObject("OutOfStock").ToString().Replace("<name>", AddOnValue.Name);
                                status = false;

                                return false;
                            }

                            status = true;

                            // Add to Selected Addon list for this product
                            addOnValues.Append(AddOnValue.AddOnValueID.ToString());
                        }

                        Image img = (Image)ControlsPlaceHolder.FindControl("Image_AddOnValue" + AddOnValue.AddOnValueID.ToString());

                        if (img != null)
                        {
                            img.Visible = false;
                        }

                        if (!status && !AddOn.OptionalInd)
                        {
                            if (img != null)
                            {
                                img.Visible = true;
                            }

                            message = "Enter " + AddOnValue.Name;

                            status = false;

                            return status;
                        }
                    }
                }

                // Bind RadioButton Control
                if (AddOn.DisplayType == "RadioButton")
                {
                    System.Web.UI.WebControls.RadioButtonList rbtnlistControl = new RadioButtonList();
                    rbtnlistControl = (System.Web.UI.WebControls.RadioButtonList)ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        // Optional Addons are not selected,then leave those addons 
                        // If optinal Addons are selected, it should add with the Selected item 
                        // Check for Selected Addon value 
                        if (AddOnValue.AddOnValueID.ToString() == rbtnlistControl.SelectedValue)
                        {
                            // Check for quantity on hand and back-order,track inventory settings
                            if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                            {
                                message = this.GetLocalResourceObject("OutOfStock").ToString().Replace("<name>", AddOn.Name);

                                return false;
                            }

                            status = true;

                            // Add to Selected Addon list for this product
                            addOnValues.Append(AddOnValue.AddOnValueID.ToString());

                            break;
                        }
                    }
                }

                // BindCheckBox
                if (AddOn.DisplayType == "CheckBox")
                {
                    System.Web.UI.WebControls.CheckBoxList chkBoxListControl = new CheckBoxList();
                    chkBoxListControl = (System.Web.UI.WebControls.CheckBoxList)ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    foreach (ListItem listCheckedItem in chkBoxListControl.Items)
                    {
                        if (listCheckedItem.Selected)
                        {
                            // Loop through the Add-on values for each Add-on
                            foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                            {
                                // Optional Addons are not selected,then leave those addons 
                                // If optinal Addons are selected, it should add with the Selected item 
                                // Check for Selected Addon value 
                                if (AddOnValue.AddOnValueID.ToString() == listCheckedItem.Value)
                                {
                                    // Check for quantity on hand and back-order,track inventory settings
                                    if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                                    {
                                        message = this.GetLocalResourceObject("OutOfStock").ToString().Replace("<name>", AddOn.Name);

                                        return false;
                                    }

                                    status = true;

                                    // Add to Selected Addon list for this product
                                    addOnValues.Append(AddOnValue.AddOnValueID.ToString());
                                    addOnValues.Append(",");
                                    break;
                                }
                            }
                        }
                    }
                }

                if (AddOn.DisplayType == "DropDownList")
                {
                    // Default DropDownList Control
                    System.Web.UI.WebControls.DropDownList ddlistControl = new DropDownList();
                    ddlistControl = (System.Web.UI.WebControls.DropDownList)ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        // Optional Addons are not selected,then leave those addons 
                        // If optinal Addons are selected, it should add with the Selected item 
                        // Check for Selected Addon value 
                        if (AddOnValue.AddOnValueID.ToString() == ddlistControl.SelectedValue)
                        {
                            // Check for quantity on hand and back-order,track inventory settings
                            if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                            {
                                message = this.GetLocalResourceObject("OutOfStock").ToString().Replace("<name>", AddOn.Name);

                                return false;
                            }

                            status = true;

                            // Add to Selected Addon list for this product
                            addOnValues.Append(AddOnValue.AddOnValueID.ToString());

                            break;
                        }
                    }
                }

                if (string.Compare(AddOn.DisplayType, "TextBox") != 0)
                {
                    Image img = (Image)ControlsPlaceHolder.FindControl("Image_AddOn" + AddOn.ToString());

                    if (img != null)
                    {
                        img.Visible = false;
                    }

                    if (!status && !AddOn.OptionalInd)
                    {
                        if (img != null)
                        {
                            img.Visible = true;
                        }

                        message = "Select " + AddOn.Name;

                        if (AddOn.DisplayType.Equals("TextBox", StringComparison.OrdinalIgnoreCase))
                        {
                            message = "Enter " + AddOn.Name;
                        }

                        return false;
                    }
                }
            }

            if (addOnValues.Length > 0)
            {
                // Get a add-on values based on Add-ons selected
                selectedAddOnList = ZNodeAddOnList.CreateByProductAndAddOns(this.product.ProductID, addOnValues.ToString());
            }

            foreach (ZNodeAddOn addOn in selectedAddOnList.ZNodeAddOnCollection)
            {
                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity addOnvalue in addOn.AddOnValueCollection)
                {
                    TextBox textBoxControl = new TextBox();
                    textBoxControl = (TextBox)ControlsPlaceHolder.FindControl("txtBoxAddOn" + addOnvalue.AddOnValueID.ToString());

                    if (textBoxControl != null)
                    {
                        addOnvalue.CustomText = textBoxControl.Text.Trim();
                    }
                }
            }

            selectedAddOnList.SelectedAddOnValues = addOnValues.ToString();

            message = string.Empty;
            return true;
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["itemid"]);
            }
            else if (Session["itemid"] != null)
            {
                this.ProductId = int.Parse(Session["itemid"].ToString());
            }
            else
            {
                return;
            }

            this.product = ZNodeProduct.Create(this.ProductId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

            this.Bind();

            if (!IsPostBack)
            {
                bool ret = false;
                string msg = string.Empty;

                this.product.SelectedAddOnItems = this.GetSelectedAddOns(out ret, out msg);
            }
        }
        #endregion

        #region Bind Data

        /// <summary>
        /// Validate selected addons and returns the Inventory related messages
        /// </summary>
        /// <param name="AddOn">Addon Instance</param>
        /// <param name="AddOnValue">AddonValue Instance</param>
        /// <returns>Returns the string</returns>
        protected string BindStatusMsg(ZNodeAddOn AddOn, ZNodeAddOnValue AddOnValue)
        {
            int qty = 1;

            // Don't track Inventory - Items can always be added to the cart,TrackInventory is disabled
            if (!AddOn.AllowBackOrder && !AddOn.TrackInventoryInd)
            {
                if (AddOnValue.QuantityOnHand <= 0)
                {
                    return string.Empty;
                }
                else
                {
                    return AddOn.InStockMsg;
                }
            }
            else if (AddOnValue.QuantityOnHand <= qty && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
            {
                // If quantity available is less and track inventory is enabled
                return AddOn.OutOfStockMsg;
            }
            else if (AddOnValue.QuantityOnHand < qty && AddOn.AllowBackOrder == true && AddOn.TrackInventoryInd)
            {
                return AddOn.BackOrderMsg;
            }
            else if (AddOn.TrackInventoryInd && AddOnValue.QuantityOnHand >= qty)
            {
                return AddOn.InStockMsg;
            }

            return string.Empty;
        }

        /// <summary>
        /// Event Handler for the dynamic control DropDownList
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Controls_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedIndexChanged != null)
            {
                this.SelectedIndexChanged(sender, e);
            }
        }

        #endregion

        #region Bind Dynamic Controls

        /// <summary>
        /// Bind the Addon in DropDown Control
        /// </summary>
        /// <param name="AddOn">Addon Instance</param>
        protected void BindTextBoxControl(ZNodeAddOn AddOn)
        {
            // Don't display list box if there is no add-on values for AddOns
            if (AddOn.AddOnID > 0)
            {
                foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                {
                    ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='Option'>"));

                    TextBox txtBox = new TextBox();
                    txtBox.ID = "txtBoxAddOn" + AddOnValue.AddOnValueID.ToString();
                    txtBox.CssClass = "AddOnTextBox";
                    txtBox.Text = AddOnValue.Description;

                    // Add controls to the place holder
                    Literal lit1 = new Literal();
                    lit1.Text += "<div class=\"Option\"><span>";

                    ControlsPlaceHolder.Controls.Add(lit1);

                    RegularExpressionValidator validator = new RegularExpressionValidator();
                    validator.CssClass = "AddOnValidator";
                    validator.ControlToValidate = txtBox.ID;
                    validator.ErrorMessage = "Invalid";
                    validator.Display = System.Web.UI.WebControls.ValidatorDisplay.Dynamic;
                    validator.ValidationExpression = @"^[a-zA-Z0-9_. ]+$";

                    ControlsPlaceHolder.Controls.Add(validator);

                    if (!AddOn.OptionalInd)
                    {
                        Image img = new Image();
                        img.ID = "Image_AddOnValue" + AddOnValue.AddOnValueID;
                        img.ImageAlign = ImageAlign.AbsMiddle;
                        img.ImageUrl = ResolveUrl(ZNodeCatalogManager.GetImagePathByLocale("Required.gif"));
                        img.Visible = false;

                        ControlsPlaceHolder.Controls.Add(img);
                    }

                    Literal ltrl = new Literal();
                    ltrl.Text = "</span><span class='OptionLabel'>" + AddOnValue.Name + " :</span>";

                    ControlsPlaceHolder.Controls.Add(ltrl);

                    Literal lt = new Literal();
                    lt.Text = "<span class='OptionValue'>";

                    ControlsPlaceHolder.Controls.Add(lt);
                    ControlsPlaceHolder.Controls.Add(txtBox);

                    Literal literal = new Literal();
                    literal.Text = "</span></div>";

                    ControlsPlaceHolder.Controls.Add(literal);

                    ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
                }
            }
        }

        /// <summary>
        /// Bind the Addon in DropDown Control
        /// </summary>
        /// <param name="AddOn">Addon Instance</param>
        protected void BindDropDownList(ZNodeAddOn AddOn)
        {
            // Don't display list box if there is no add-on values for AddOns
            if (AddOn.ZNodeAddOnValueCollection.Count > 0)
            {
                ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='Option'>"));

                System.Web.UI.WebControls.DropDownList ddlistControl = new DropDownList();
                ddlistControl.ID = "lstAddOn" + AddOn.AddOnID.ToString();
                ddlistControl.AutoPostBack = true;
                ddlistControl.CssClass = "AddOnDropDown";

                ddlistControl.SelectedIndexChanged += new EventHandler(this.Controls_SelectedIndexChanged);

                if (AddOn.OptionalInd)
                {
                    ListItem OptionalItem = new ListItem("Select " + AddOn.Title + " - Optional", "0");
                    ddlistControl.Items.Insert(0, OptionalItem);
                    ddlistControl.SelectedValue = "0";
                }
                else
                {
                    // Added the Label as title
                    ListItem li = new ListItem("Select " + AddOn.Title, "0");
                    ddlistControl.Items.Add(li);
                }

                foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                {
                    string AddOnValueName = AddOnValue.Name;

                    // Added Inventory Message with the Addon Value Name in the dropdownlist
                    AddOnValueName += " " + this.BindStatusMsg(AddOn, AddOnValue);

                    ListItem li1 = new ListItem(AddOnValueName, AddOnValue.AddOnValueID.ToString());
                    ddlistControl.Items.Add(li1);

                    // Set Add-on is Default.
                    if (AddOnValue.IsDefault && !AddOn.OptionalInd)
                    {
                        ddlistControl.SelectedValue = AddOnValue.AddOnValueID.ToString();
                    }
                }

                // Add controls to the place holder
                Literal lit1 = new Literal();
                lit1.Text += "<div class=\"Option\"><span>";

                ControlsPlaceHolder.Controls.Add(lit1);

                if (!AddOn.OptionalInd)
                {
                    Image img = new Image();
                    img.ID = "Image_AddOn" + AddOn.AddOnID;
                    img.ImageAlign = ImageAlign.AbsMiddle;
                    img.ImageUrl = ResolveUrl(ZNodeCatalogManager.GetImagePathByLocale("Required.gif"));
                    img.Visible = false;

                    ControlsPlaceHolder.Controls.Add(img);
                }

                Literal ltrl = new Literal();
                ltrl.Text = "</span><span class='OptionLabel'>" + AddOn.Title + " :</span>";

                ControlsPlaceHolder.Controls.Add(ltrl);

                Literal lt = new Literal();
                lt.Text = "<span class='OptionValue'>";

                ControlsPlaceHolder.Controls.Add(lt);

                // Dropdown list control
                ControlsPlaceHolder.Controls.Add(ddlistControl);

                Literal literal = new Literal();
                literal.Text = "</span></div>";

                ControlsPlaceHolder.Controls.Add(literal);

                ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
            }
        }

        /// <summary>
        /// Bind the Addon in RadioButton Control
        /// </summary>
        /// <param name="AddOn">Addon Instance</param>
        protected void BindRadioButtonList(ZNodeAddOn AddOn)
        {
            // Don't display list box if there is no add-on values for AddOns
            if (AddOn.ZNodeAddOnValueCollection.Count > 0)
            {
                ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='OptionLabel'> <table>"));

                System.Web.UI.WebControls.RadioButtonList rbtnlistControl = new RadioButtonList();

                rbtnlistControl.ID = "lstAddOn" + AddOn.AddOnID.ToString();
                rbtnlistControl.AutoPostBack = true;

                rbtnlistControl.SelectedIndexChanged += new EventHandler(this.Controls_SelectedIndexChanged);

                // Set Vertical Or Horizontal view
                if (this.ShowRadioButtonsVerically)
                {
                    rbtnlistControl.RepeatDirection = RepeatDirection.Vertical;
                }
                else
                {
                    rbtnlistControl.RepeatDirection = RepeatDirection.Horizontal;
                }

                rbtnlistControl.TextAlign = TextAlign.Left;
                rbtnlistControl.CellPadding = 0;
                rbtnlistControl.CellSpacing = 0;

                if (AddOn.OptionalInd)
                {
                    // Add controls to the place holder
                    Literal litOptional = new Literal();
                    litOptional.Text = "<tr><td><div class='AddonTitle'>Select " + AddOn.Title + " - Optional</div></td></tr>";
                    ControlsPlaceHolder.Controls.Add(litOptional);
                }
                else
                {
                    // Add controls to the place holder
                    Literal litrl = new Literal();
                    litrl.Text = "<tr><td><div class='AddonTitle'>Select " + AddOn.Title + "</div></td></tr>";
                    ControlsPlaceHolder.Controls.Add(litrl);
                }

                foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                {
                    string AddOnValueName = AddOnValue.Name;

                    // Added Inventory Message with the Addon Value Name in the dropdownlist
                    AddOnValueName += " " + this.BindStatusMsg(AddOn, AddOnValue);

                    ListItem li1 = new ListItem(AddOnValueName, AddOnValue.AddOnValueID.ToString());
                    rbtnlistControl.Items.Add(li1);

                    // Set Add-on is Default.
                    if (AddOnValue.IsDefault && !AddOn.OptionalInd)
                    {
                        rbtnlistControl.SelectedValue = AddOnValue.AddOnValueID.ToString();
                    }
                }

                // Add controls to the place holder
                Literal lit1 = new Literal();
                lit1.Text = "<tr><td class='DynamicValueStyle'><div>";

                ControlsPlaceHolder.Controls.Add(lit1);

                // Dropdown list control
                ControlsPlaceHolder.Controls.Add(rbtnlistControl);

                Literal lit3 = new Literal();
                lit3.Text = "</div></td></tr>";

                ControlsPlaceHolder.Controls.Add(lit3);

                ControlsPlaceHolder.Controls.Add(new LiteralControl("</table></div>"));
            }
        }

        /// <summary>
        /// Bind the Addon in CheckBox Control
        /// </summary>
        /// <param name="AddOn">Addon Instance</param>
        protected void BindCheckBoxList(ZNodeAddOn AddOn)
        {
            // Don't display list box if there is no add-on values for AddOns
            if (AddOn.ZNodeAddOnValueCollection.Count > 0)
            {
                ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='OptionLabel'> <table>"));

                System.Web.UI.WebControls.CheckBoxList chkboxlistControl = new CheckBoxList();

                chkboxlistControl.ID = "lstAddOn" + AddOn.AddOnID.ToString();
                chkboxlistControl.AutoPostBack = true;

                chkboxlistControl.SelectedIndexChanged += new EventHandler(this.Controls_SelectedIndexChanged);

                // Set Vertical Or Horizontal view
                if (this.ShowCheckBoxesVertically)
                {
                    chkboxlistControl.RepeatDirection = RepeatDirection.Vertical;
                }
                else
                {
                    chkboxlistControl.RepeatDirection = RepeatDirection.Horizontal;
                }

                chkboxlistControl.TextAlign = TextAlign.Left;
                chkboxlistControl.CellPadding = 0;
                chkboxlistControl.CellSpacing = 0;

                if (AddOn.OptionalInd)
                {
                    // Add controls to the place holder
                    Literal litOptional = new Literal();
                    litOptional.Text = "<tr><td><div class='AddonTitle'>Select " + AddOn.Title + " - Optional</div></td></tr>";
                    ControlsPlaceHolder.Controls.Add(litOptional);
                }
                else
                {
                    // Add controls to the place holder
                    Literal litrl = new Literal();
                    litrl.Text = "<tr><td><div class='AddonTitle'>Select " + AddOn.Title + "</div></td></tr>";
                    ControlsPlaceHolder.Controls.Add(litrl);
                }

                foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                {
                    string AddOnValueName = AddOnValue.Name;

                    // Added Inventory Message with the Addon Value Name in the dropdownlist
                    AddOnValueName += " " + this.BindStatusMsg(AddOn, AddOnValue);

                    ListItem li1 = new ListItem(AddOnValueName, AddOnValue.AddOnValueID.ToString());
                    chkboxlistControl.Items.Add(li1);

                    // Set Add-on is Default.
                    if (AddOnValue.IsDefault && !AddOn.OptionalInd)
                    {
                        chkboxlistControl.SelectedValue = AddOnValue.AddOnValueID.ToString();
                    }
                }

                // Add controls to the place holder
                Literal lit1 = new Literal();
                lit1.Text = "<tr><td class='DynamicValueStyle'><div>";

                ControlsPlaceHolder.Controls.Add(lit1);

                // Dropdown list control
                ControlsPlaceHolder.Controls.Add(chkboxlistControl);

                Literal lit3 = new Literal();
                lit3.Text = "</div></td></tr>";

                ControlsPlaceHolder.Controls.Add(lit3);

                ControlsPlaceHolder.Controls.Add(new LiteralControl("</table></div>"));
            }
        }

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        private void Bind()
        {
            if (this.product.ZNodeAddOnCollection.Count > 0)
            {
                pnlAddOns.Visible = true;

                string addOnValues = string.Empty;

                foreach (ZNodeAddOn AddOn in this.product.ZNodeAddOnCollection)
                {
                    string DisplayType = AddOn.DisplayType;

                    if (DisplayType == "RadioButton")
                    {
                        this.BindRadioButtonList(AddOn);
                    }

                    if (DisplayType == "CheckBox")
                    {
                        this.BindCheckBoxList(AddOn);
                    }

                    if (DisplayType == "TextBox")
                    {
                        this.BindTextBoxControl(AddOn);
                    }

                    if (DisplayType == "DropDownList")
                    {
                        // By default we use Dropdownist for AddOns
                        this.BindDropDownList(AddOn);
                    }
                }
            }
            else
            {
                pnlAddOns.Visible = false;
                return;
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;

namespace SiteAdmin.Controls.Default.Search
{

    /// <summary>
    /// Represents the SiteAdmin - SiteAdmin.Controls.Default.Search.ProductSelect class
    /// </summary> 
    /// 


    public partial class ProductSelect : System.Web.UI.UserControl
    {
        Func<ListItem> getDefaultAllChoice = () => new ListItem("ALL", "0");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindDropDownData();
            }
            else
            {
                ClearSaveResultLabel();
            }
        }

        private void Bind()
        {
            ucProductList.Bind(Server.HtmlEncode(txtProductName.Text.Trim()), txtProductNumber.Text.Trim(), txtSKU.Text.Trim(), ddBrand.SelectedValue, ddProductType.SelectedValue,
                                ddProductCategory.SelectedValue, ddCatalog.SelectedValue);
            lblTitle.Visible = true;
            if (!ucProductList.IsCountExist)
            {
                btnSave.Visible = false;
                btnCancel.Visible = false;
                lblMsg.Visible = true;
            }
            else
            {
                lblMsg.Visible = false;
            }
        }

        private void BindDropDownData()
        {
            //Func<ListItem> getDefaultAllChoice = () => new ListItem("ALL", "0");

            // Add the manufacturers to the drop-down list
            var manufacturerAdmin = new ManufacturerAdmin();
            BindDropDown(ddBrand, manufacturerAdmin.GetAll(), "Name", "ManufacturerID", getDefaultAllChoice());

            // Add Product categories to the drop-down list
            var categoryadmin = new CategoryAdmin();
            BindDropDown(ddProductCategory, categoryadmin.GetAllCategories(), "Name", "Categoryid", getDefaultAllChoice());

            var productTypeAdmin = new ProductTypeAdmin();
            BindDropDown(ddProductType, productTypeAdmin.GetAllProductTypes(), "Name", "ProductTypeID", getDefaultAllChoice());

            var catalogAdmin = new CatalogAdmin();
            BindDropDown(ddCatalog, catalogAdmin.GetAllCatalogs(), "Name", "CatalogID", getDefaultAllChoice());
        }

        private void BindDropDown(ListControl listControl, object dataSource, string dataTextField, string dataValueField, params ListItem[] specialListItems)
        {
            listControl.DataSource = dataSource;
            listControl.DataTextField = dataTextField;
            listControl.DataValueField = dataValueField;
            listControl.DataBind();

            foreach (var specialListItem in specialListItems.Reverse())
            {
                listControl.Items.Insert(0, specialListItem);
            }

            if(listControl.Items.Count > 0)
                listControl.SelectedIndex = 0;
        }

        protected void BtnCancel_Click(object sender, ImageClickEventArgs e)
        {
            ClearSaveResultLabel(); 
            ucProductList.CancelChanges();
            ucProductList.ClearDataSource();
            this.btnSave.Visible = false;
            this.btnCancel.Visible = false;
            lblTitle.Visible = false;
            lblMsg.Visible = false;
        }

        protected void BtnClear_Click(object sender, ImageClickEventArgs e)
        {
            ClearSaveResultLabel();

            txtProductName.Text = string.Empty;
            txtProductNumber.Text = string.Empty;
            txtSKU.Text = string.Empty;
            ddCatalog.SelectedIndex = 0;
            ddBrand.SelectedIndex = 0;
            ddProductType.SelectedIndex = 0;
            ddProductCategory.SelectedIndex = 0;
            ucProductList.ClearDataSource();
            lblTitle.Visible = false;
            this.btnSave.Visible = false;
            this.btnCancel.Visible = false;
            lblMsg.Visible = false;
        }

        protected void BtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            ClearSaveResultLabel();
            this.btnSave.Visible = true;
            this.btnCancel.Visible = true;
            lblMsg.Visible = false;
            this.Bind();
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                var saveResult = ucProductList.SaveChages();
                lblSaveResult.Visible = true;
                if (saveResult)
                {
                    lblSaveResult.Text = "Changes Saved";
                    lblSaveResult.CssClass = "Success";
                }
                else
                {
                    lblSaveResult.Text = "Changes Not Saved or No Changes Made";
                    lblSaveResult.CssClass = "Error";
                }
            }
            catch (Exception ex)
            {
                lblSaveResult.Text = string.Format("Error Saving Changes - {0}", ex.Message);
                lblSaveResult.CssClass = "Error";
            }
        }

        protected void ClearSaveResultLabel()
        {
            lblSaveResult.Visible = false;
        }

        protected void ddCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
           if (Convert.ToInt32(ddCatalog.SelectedValue) > 0)
            {
                ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();
                BindDropDown(ddProductCategory, categoryHelper.GetCategoryByCatalogID(Convert.ToInt32(ddCatalog.SelectedValue)), "Name", "Categoryid", getDefaultAllChoice());
            }
            else
            {
                var categoryadmin = new CategoryAdmin();
                BindDropDown(ddProductCategory, categoryadmin.GetAllCategories(), "Name", "Categoryid", getDefaultAllChoice());
            }
        }

        protected void ddProductCategory_DataBound(object sender, EventArgs e)
        {
            for (int index = 0; index < ddProductCategory.Items.Count; index++)
            {
                ddProductCategory.Items[index].Text = Server.HtmlDecode(ddProductCategory.Items[index].Text);
            }
        }
    }
}
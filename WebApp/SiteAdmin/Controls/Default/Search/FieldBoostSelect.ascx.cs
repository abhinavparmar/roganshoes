﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;

namespace SiteAdmin.Controls.Default.Search
{
    /// <summary>
    /// Represents the SiteAdmin -  SiteAdmin.Controls.Default.Search.FieldBoostSelect class
    /// </summary> 
    /// 
    public partial class FieldBoostSelect : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
            else
            {
                ClearSaveResultLabel();
            }
        }

        protected void BtnCancel_Click(object sender, ImageClickEventArgs e)
        {
            ClearSaveResultLabel();

            ucFieldBoostList.CancelChanges();
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                var saveResult = ucFieldBoostList.SaveChages();
                if (saveResult)
                {
                    lblSaveResult.Text = "Changes Saved";
                    lblSaveResult.CssClass = "Success";
                }
                else
                {
                    lblSaveResult.Text = "Changes Not Saved or No Changes Made";
                    lblSaveResult.CssClass = "Error";
                }
            }
            catch (Exception ex)
            {
                lblSaveResult.Text = string.Format("Error Saving Changes - {0}", ex.Message);
                lblSaveResult.CssClass = "Error";
            }
            
        }

        protected void ClearSaveResultLabel()
        {
            lblSaveResult.Text = string.Empty;
        }
    }
}
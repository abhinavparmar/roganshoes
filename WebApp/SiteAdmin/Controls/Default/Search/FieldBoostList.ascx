﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FieldBoostList.ascx.cs" Inherits="SiteAdmin.Controls.Default.Search.FieldBoostList" %>

<asp:GridView ID="gvFieldBoostList" CellPadding="0" CssClass="Grid" runat="server" AutoGenerateColumns="False"
    AllowPaging="True" Width="100%" GridLines="Both" DataKeyNames="LuceneDocumentMappingID" ViewStateMode="Enabled" OnPageIndexChanging="gvFieldBoostList_PageIndexChanging">
    <Columns>
        <asp:BoundField HeaderText="Field Name" DataField="DocumentName" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass ="TextSpace"/>
        <asp:TemplateField HeaderText="Boost Value">
            <ItemStyle HorizontalAlign="Left"></ItemStyle>
            <ItemTemplate>
                <asp:TextBox ID="txtBoostValue" Text='<%# DataBinder.Eval(Container.DataItem, "BoostValue").ToString() %>' 
                    runat="server">
                </asp:TextBox>
                <asp:RangeValidator ID="rvBoostValue" runat="server" Type="Double" MinimumValue="0.00" MaximumValue="1000.00" 
                    ControlToValidate="txtBoostValue" Display="Dynamic" 
                    ErrorMessage="Please enter a valid boost value – Valid values are: 0.00 to 1000.00">
                </asp:RangeValidator>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EditRowStyle CssClass="EditRowStyle" />
    <FooterStyle CssClass="FooterStyle" />
    <RowStyle CssClass="RowStyle" />
    <SelectedRowStyle CssClass="SelectedRowStyle" />
    <PagerStyle CssClass="PagerStyle" />
    <HeaderStyle CssClass="HeaderStyle" />
    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
</asp:GridView>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZNewArrivalProducts.ascx.cs" Inherits="WebApp.Controls.CheerandPom.zNewArrival.ZNewArrivalProducts" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>
<%@ OutputCache Duration="21600" VaryByParam="None" %>
<div id="pnlNewArrivalsList">
    <div id="NewArrivals">
        <div class="HeadingBox">
            <h2 class="Title">
                <span>
                    <Znode:CustomMessage ID="ucNewArrivalTitle" runat="server" MessageKey="HomeNewArrivalTitle" EnableViewState="False" />
                    <div class="Subtitle">
                        <%-- <asp:HyperLink ID="lnkNewArrivals" runat="server" EnableViewState="false" Visible="false"
                            meta:resourcekey="lnkNewArrivalsResource"></asp:HyperLink>--%>
                    </div>
                </span>
            </h2>
        </div>
        <div id="divNewArrival">
            <ul id="ULNewArrivals">
                <asp:Repeater ID="RepeaterNewArrivals" runat="server" EnableViewState="true">
                    <ItemTemplate>
                        <li>
                            <div class="item">
                                <div class="SpecialItem">
                                    <div class="Image">
                                        <div class="ItemType" id="divItemType" runat="server" visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>'>
                                            <span class="rs-new"><span class="rs-new-text">New</span></span>
                                            <%--<asp:Image EnableViewState="false" runat="server" ImageUrl='<%#ResolveUrl(ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png")) %>'
                                                meta:resourcekey="NewItemImageResource1" Visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>' AlternateText="New" alt="New" />--%>
                                        </div>
                                        <a href='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'>
                                            <img border="0" alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>' title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>' src="images/grey1px.gif" class="lazy" data-original='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "SmallImageFilePath").ToString()) %>' />&nbsp;
                                        </a>
                                    </div>
                                    <div class="StarRating">
                                        <Znode:ProductAverageRating ID="uxProductAverageRating" ProductName='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>'  TotalReviews='<%# DataBinder.Eval(Container.DataItem, "TotalReviews") %>'
                                            ViewProductLink='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink") %>'
                                            ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ReviewRating") %>' runat="server" />
                                    </div>
                                    <div class="DetailLink">
                                        <a href='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' class='DetailLink' title='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>'><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></a>
                                        <span style="width: 1px"></span>
                                        <span class="Price">
                                            <asp:Literal runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>' EnableViewState="false"
                                                Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && SpecialProfile.ShowPrice %>'></asp:Literal>
                                        </span>
                                    </div>
                                    <div class="CallForPrice">
                                        <span class="Price"><%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
</div>



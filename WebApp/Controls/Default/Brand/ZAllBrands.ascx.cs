﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.Brand
{
    public partial class ZAllBrands : System.Web.UI.UserControl
    {
        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindBrandLinkTabs();
                BindAllBrandsAlphabetical();
            }

        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtAllBrands");
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Bind All Brands Alphabetically
        /// </summary>
        private void BindAllBrandsAlphabetical()
        {
            StringBuilder sbAllBrands = new StringBuilder();
            DataTable dtBrands = new DataTable();
            dtBrands = this.GetAllBrandAlphabetical();
            string[] brandAlphabets = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            if (dtBrands != null && dtBrands.Rows.Count > 0)
            {
                for (int count = 0; count < brandAlphabets.Length; count++)
                {
                    string brandsString = string.Empty;
                    brandsString = GetBrandsByAlphabet(dtBrands.Select("InitialChar ='" + brandAlphabets[count] + "'"), brandAlphabets[count]);

                    if (!string.IsNullOrEmpty(brandsString))
                    {
                        if (brandAlphabets[count].Equals("#")) { brandAlphabets[count] = "Other"; }
                        sbAllBrands.AppendLine(@"<Div id='dvBrandsAlpha-" + brandAlphabets[count] + "'class='BrandList'>");
                        sbAllBrands.AppendLine(brandsString);
                        sbAllBrands.AppendLine(@"</Div>");
                    }
                }
            }
            ltlAllBrands.Text = sbAllBrands.ToString();
        }

        /// <summary>
        /// get all brands by alphabets
        /// </summary>
        /// <param name="brandRows">datarow</param>
        /// <param name="brandAlphabet">string</param>
        /// <returns>string</returns>
        private string GetBrandsByAlphabet(DataRow[] brandRows, string brandAlphabet)
        {

            int columnCurrentItemCount = 0;
            StringBuilder sbBrandNames = new StringBuilder();

            int totalChildCount = Convert.ToInt32(brandRows.Count());
            int eachColumnItemCount = totalChildCount / 12;

            if (totalChildCount % 12 > 0)
            {
                eachColumnItemCount += 1;
            }

            sbBrandNames.Append("<h1 class='CategoryTitle'>" + brandAlphabet + "</h1>");

            for (int currentItemCount = 0; currentItemCount < totalChildCount; currentItemCount++)
            {

                if (columnCurrentItemCount == 0)
                {
                    sbBrandNames.AppendLine(@"<ul>");
                }

                sbBrandNames.Append("<li>");
                string seoURL =GetBrandURL(brandRows[currentItemCount]["ManufacturerID"],brandRows[currentItemCount]["SEOURL"]) ;
                string brandName = string.Empty;
                int iManuId = 0;
                //Image Setting
                ZNodeImage zImage = new ZNodeImage();
                string brandImg=string.Empty;
                //string imgTag = "<img src={0} alt={1} title={2} />";
                if (brandRows[currentItemCount]["Image"] != null)
                {
                  brandImg = zImage.GetImageHttpPathThumbnail(brandRows[currentItemCount]["Image"].ToString());
                }
               
                int.TryParse(brandRows[currentItemCount]["ManufacturerID"].ToString(), out iManuId);
                brandName = brandRows[currentItemCount]["Name"].ToString();
                sbBrandNames.AppendLine(@"<a title= '"+ brandRows[currentItemCount]["Name"].ToString()+"' href='" + seoURL.Replace("~/", "")+ "'>");
              
                //Showing Image if Exist
                if (!string.IsNullOrEmpty(brandImg)) {
                    //sbBrandNames.Append(string.Format(imgTag, brandImg.Replace("~/", ""), brandRows[currentItemCount]["Name"].ToString(), brandRows[currentItemCount]["Name"].ToString()));
                    sbBrandNames.Append("<div>"+brandName+"</div>");
                }
                //else { sbBrandNames.Append(brandName);}
                sbBrandNames.Append(@"</a>");
                sbBrandNames.AppendLine(@"</li>");

                columnCurrentItemCount++;
                if (columnCurrentItemCount == eachColumnItemCount)
                {
                    columnCurrentItemCount = 0;
                    sbBrandNames.AppendLine(@"</ul>");
                }
                else if (currentItemCount == totalChildCount - 1)
                {
                    sbBrandNames.AppendLine(@"</ul>");
                }
            }
            return sbBrandNames.ToString();
        }

        /// <summary>
        /// Bind All Brand Link Tabs in top of the page
        /// </summary>
        private void BindBrandLinkTabs()
        {
            //string[] brandAlphabets = new string[] { "#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "All" };
            //StringBuilder sbAllBrandsTabs = new StringBuilder();
            //sbAllBrandsTabs.Append("<ul>");
            //for (int count = 0; count < brandAlphabets.Length; count++)
            //{
            //    string ancharTag = "<li'><a onclick='LoadBrand('" + brandAlphabets[count] + "')'>";
            //    sbAllBrandsTabs.AppendLine(@"<li onclick=zeonAllBrands.ShowBrand('" + brandAlphabets[count] + "')>");
            //    sbAllBrandsTabs.AppendLine(@"<a class='LinkBrand'>");
            //    sbAllBrandsTabs.AppendLine(brandAlphabets[count]);
            //    sbAllBrandsTabs.AppendLine(@"</a></li>");

            //}
            //sbAllBrandsTabs.Append("</ul>");
            //ltlBrandLinkTab.Text = sbAllBrandsTabs.ToString();
            ltlBrandLinkTab.Text = Resources.CommonCaption.AllBrandsTopLinks;
        }

        /// <summary>
        /// Get all brands 
        /// </summary>
        private DataTable GetAllBrandAlphabetical()
        {
            DataTable dtManufacturer = (DataTable)System.Web.HttpRuntime.Cache["AllBrands" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + HttpContext.Current.Session.SessionID];
                       
            if (dtManufacturer == null)
            {
                ManufacturerHelper manuHelper = new ManufacturerHelper();
                dtManufacturer = manuHelper.GetActiveBrandsByPortal(ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID, 0);
                //Insert into cache
                ZNodeCacheDependencyManager.Insert("AllBrands" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId
                                             + HttpContext.Current.Session.SessionID,
                                             dtManufacturer,
                                             DateTime.Now.AddHours(1),
                                             System.Web.Caching.Cache.NoSlidingExpiration,
                                             null);
            }

            return dtManufacturer;

        }

        /// <summary>
        /// get brand URL
        /// </summary>
        /// <param name="brandID"></param>
        /// <returns></returns>
        private string GetBrandURL(object brandID, object seoURL)
        {
            string currentId = brandID != null ? brandID.ToString() : string.Empty;
            string currentUrl = seoURL != null ? seoURL.ToString() : string.Empty;
            string defaultURL = "~/brand.aspx?mid=";
            if (!string.IsNullOrEmpty(currentUrl))
            {
                return currentUrl.ToLower(); 
            }
            else
            {
                return ResolveUrl(defaultURL + currentId).ToLower();
            }
        }

        /// <summary>
        /// Registers the Script for this page
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var zeonAllBrands = new AllBrands({");
            script.Append("});zeonAllBrands.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "AllBrands", script.ToString(), true);
        }

        #endregion
    }
}



using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using System.Linq;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Utilities;

/// <summary>
/// Displays home page specials
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the Brand user control class.
    /// </summary>
    public partial class Controls_Default_Brand_Brand : System.Web.UI.UserControl
    {
        #region Private Variables
        private static int totalRecords = 0;
        private static int recCount = 0;
        private static int ncurrentpage = 0;
        private ZNodeProfile _Profile = new ZNodeProfile();
        private string _title = string.Empty;
        private int _manufacturerId;
        private ZNodeProductList _productList;
        //Zeon Custom Code: Start
        private ZNodeProductList _latestReviewedProductList;
        private string _view = string.Empty;
        private string _listActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/list-active.gif";
        private string _gridDeActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/grid.gif";
        private string _gridActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/grid-active.gif";
        private string _listDeActiveURL = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/list.gif";
        //Zeon Custom Code: End
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Product list object
        /// </summary>
        public ZNodeProductList ProductList
        {
            get
            {
                return this._productList;
            }

            set
            {
                this._productList = value;
            }
        }

        /// <summary>
        /// Gets or sets the title for this control
        /// </summary>
        public string Title
        {
            get
            {
                return this._title;
            }

            set
            {
                this._title = value;
            }
        }

        /// <summary>
        /// Gets or sets the brand profile.
        /// </summary>
        public ZNodeProfile BrandProfile
        {
            get
            {
                return this._Profile;
            }

            set
            {
                this._Profile = value;
            }
        }

        /// <summary>
        /// Gets the product seperator image.
        /// </summary>
        public string ProductListSeparatorImage
        {
            get { return "~/themes/" + ZNodeCatalogManager.Theme + "/Images/line_seperator.gif"; }
        }

        /// <summary>
        /// Gets or sets the CurrentPage object
        /// </summary>
        private int CurrentPage
        {
            get
            {
                int currentPage = 0;

                if (ViewState["CurrentPage"] != null)
                {
                    currentPage = (int)ViewState["CurrentPage"];
                }

                return currentPage;
            }

            set
            {
                ViewState["CurrentPage"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the FirstIndex object
        /// </summary>
        private int FirstIndex
        {
            get
            {
                int firstIndex = 0;

                if (ViewState["FirstIndex"] != null)
                {
                    firstIndex = Convert.ToInt32(ViewState["FirstIndex"]);
                }

                return firstIndex;
            }

            set
            {
                ViewState["FirstIndex"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the LastIndex object
        /// </summary>
        private int LastIndex
        {
            get
            {
                int lastIndex = 0;

                if (ViewState["LastIndex"] != null)
                {
                    lastIndex = Convert.ToInt32(ViewState["LastIndex"]);
                }

                return lastIndex;
            }

            set
            {
                ViewState["LastIndex"] = value;
            }
        }

        /// <summary>
        /// Sets the Product List
        /// </summary>
        public ZNodeProductList LatestReviewedProductList
        {
            get { return this._latestReviewedProductList; }
            set { this._latestReviewedProductList = value; }
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind display based on a product list
        /// </summary>
        public void BindProducts()
        {
            if (this._productList.ZNodeProductCollection.Count == 0)
            {
                // Hide datalist and paging & sorting controls
                pnlProductList.Visible = false;

                lblMsg.Text = "No products were found for this brand.";
                return;
            }

            if (this._productList.ZNodeProductCollection.Count > 0)
            {
                // Set Viewstate object with product collection object
                ViewState["ProductList"] = this._productList;
                this.BindDataListPagedDataSource(true);
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Page Pre Render Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Update the top and bottom paging controls.
            if (this.IsPostBack)
            {
                string EventTarget = this.Page.Request.Form["__EventTarget"].Substring(this.Page.Request.Form["__EventTarget"].LastIndexOf("$") + 1);

                if (EventTarget == "ddlTopPaging")
                {
                    this.SelectDropdownValue(ddlBottomPaging, ddlTopPaging.SelectedValue);
                }
                else if (EventTarget == "ddlBottomPaging")
                {
                    this.SelectDropdownValue(ddlTopPaging, ddlBottomPaging.SelectedValue);
                }
            }
        }

        /// <summary>
        /// Event fired when lstFilter Selected Index is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindDataListPagedDataSource(true);
        }

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="source">source object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlTopPaging_SelectedIndexChanged(object source, EventArgs e)
        {
            this.SetSelectedPageSize(Convert.ToInt32(ddlTopPaging.SelectedValue));
        }

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBottomPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetSelectedPageSize(Convert.ToInt32(ddlBottomPaging.SelectedValue));
        }

        /// <summary>
        ///  Event triggered when a DataBind Method is called for DataListProducts
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DataListProducts_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            // Find the seperator image and then remove for the last column
            if (e.Item.ItemType == ListItemType.Separator)
            {
                int lastColumnIndex = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
                if ((e.Item.ItemIndex + 1) % lastColumnIndex == 0 && e.Item.ItemIndex != 0)
                {
                    foreach (Control ctrl in e.Item.Controls)
                    {
                        if (ctrl.GetType().ToString() == "System.Web.UI.HtmlControls.HtmlImage")
                        {
                            e.Item.Controls.Remove(ctrl);
                        }
                    }
                }
            }
            RegisterClientScriptClasses();//Zeon Custom Code
        }
        #endregion

        #region PageLoad
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get manufacturer id from querystring  
            if (Request.Params["mid"] != null)
            {
                this._manufacturerId = int.Parse(Request.Params["mid"]);
            }

            // This code must be placed here for SEO Url Redirect to work and fix postback problem with URL rewriting
            if (this.Page.Form != null)
            {
                this.Page.Form.Action = Request.RawUrl;
            }

            // if (!Page.IsPostBack || HttpContext.Current.Cache["BrandNavigation" + ZNodeConfigManager.SiteConfig.PortalID] == null) //Znode old code
            if (!Page.IsPostBack) //Zeon Custom code
            {
                //Znode Old Code
                // this._productList = ZNodeProductList.GetProductsByBrand(ZNodeConfigManager.SiteConfig.PortalID, this._manufacturerId, ZNodeCatalogManager.LocaleId);
                //Zeon Custom Function call to bring AlternateImageCount
                this._productList = ZNodeProductList.GetProductsListByBrand(ZNodeConfigManager.SiteConfig.PortalID, this._manufacturerId, ZNodeCatalogManager.LocaleId);
                this.BindProducts();
                BindTopReviewedProductList();//Zeon Custom Code

                // Set Repeat columns to the product Data list
                DataListProducts.RepeatColumns = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
            }

            ZNodeManufacturer manuf = null;

            if (HttpContext.Current.Items["Brand"] != null)
            {
                manuf = (ZNodeManufacturer)HttpContext.Current.Items["Brand"];
            }

            if (manuf != null)
            {
                lblBrandName.Text = Server.HtmlDecode(manuf.Name);
                BrandDescription.Text = Server.HtmlDecode(manuf.Description);

            }

            if (!this.IsPostBack)
            {
                this.DoPaging();
            }
        }

        #endregion

        #region DataList Paging Methods & Events

        /// <summary>
        /// Previous Link Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PrevRecord(object sender, EventArgs e)
        {
            ncurrentpage = this.CurrentPage;
            this.CurrentPage = ncurrentpage -= 1;
            this.BindDataListPagedDataSource(false);
        }

        /// <summary>
        /// Next Link Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void NextRecord(object sender, EventArgs e)
        {
            ncurrentpage = this.CurrentPage;
            this.CurrentPage = ncurrentpage += 1;
            this.BindDataListPagedDataSource(false);
        }

        /// <summary>
        /// Represents the CheckForCallFor Pricing method
        /// </summary>
        /// <param name="fieldValue">field value</param>
        /// <returns>Returns the Call For Pricing message</returns>
        protected string CheckForCallForPricing(object fieldValue)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());
            // string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, ZNodeConfigManager.SiteConfig.PortalID, 43); //zeon custom code

            if (Status)
            {
                return message;
            }
            else if (!this._Profile.ShowPrice)
            {
                return message;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Represents the GetColorOption method
        /// </summary>
        /// <param name="AlternateProductImageCount">Alternate Product Image Count</param>
        /// <returns>Returns the Color Option</returns>
        protected string GetColorCaption(int AlternateProductImageCount)
        {
            string output = string.Empty;
            if (AlternateProductImageCount > 0)
            {
                output = "MORE COLORS AVAILABLE ";
            }

            return output;
        }

        /// <summary>
        /// Bind Datalist using Paged DataSource object
        /// </summary>
        /// <param name="ApplySort">Apply Sort</param>
        private void BindDataListPagedDataSource(bool ApplySort)
        {
            // retrieve collection object from Viewstate
            if (ViewState["ProductList"] != null)
            {
                this._productList = (ZNodeProductList)ViewState["ProductList"];
            }

            // Apply Sorting Only Page gets Reload.
            if (ApplySort)
            {
                // Filter products by Price range
                if (lstFilter.SelectedValue.Equals("1"))
                {
                    this._productList.ZNodeProductCollection.Sort("FinalPrice");
                }
                else if (lstFilter.SelectedValue.Equals("2"))
                {
                    this._productList.ZNodeProductCollection.Sort("FinalPrice", SortDirection.Descending);
                }
                else if (lstFilter.SelectedValue.Equals("0"))
                {
                    this._productList.ZNodeProductCollection.Sort("DisplayOrder");
                }

                lstBottomFilter.SelectedValue = lstFilter.SelectedValue;//Zeon Custom Code
                ViewState["ProductList"] = this._productList;
            }

            //Zeon Custom Code: Start
            if (this._productList.ZNodeProductCollection.Count > 0)
            {
                ltrTopTotalCounts.Text = Convert.ToString(this._productList.ZNodeProductCollection.Count);
                ltrBottomTotalCount.Text = Convert.ToString(this._productList.ZNodeProductCollection.Count);

            }
            //Zeon Custom Code: End

            // Creating an object for the 'PagedDataSource' for holding the data.
            PagedDataSource objPage = new PagedDataSource();

            // Assigning the datasource to the 'objPage' object.
            objPage.DataSource = this._productList.ZNodeProductCollection;

            // Enable paging
            if (this.GetPageSize() <= 0)
            {
                objPage.AllowPaging = false;
            }
            else
            {
                objPage.AllowPaging = true;
                objPage.PageSize = this.GetPageSize();
            }

            // "CurrentPage" is public static variable to hold the current page index value declared in the global section.        
            objPage.CurrentPageIndex = this.CurrentPage;
            ViewState["TotalPages"] = objPage.PageCount;
            ViewState["CurrentPage"] = this.CurrentPage;

            // Checking for enabling/disabling next/prev buttons.
            // Next/prev buton will be disabled when is the last/first page of the pageobject.
            BotNextLink.Enabled = !objPage.IsLastPage;
            TopNextLink.Enabled = !objPage.IsLastPage;
            TopPrevLink.Enabled = !objPage.IsFirstPage;
            BotPrevLink.Enabled = !objPage.IsFirstPage;

            //Zeon Custom Code:Starts
            locBottomPageNum.Text = locTopPageNum.Text = string.Format(this.GetLocalResourceObject("txtPage").ToString(), objPage.CurrentPageIndex + 1);
            locTopPageTotal.Text = locBottomPageTotal.Text = string.Format(this.GetLocalResourceObject("txtOF").ToString(), objPage.PageCount);
            //Zeon Custom Code:Ends
            // Set current page index
            ncurrentpage = objPage.CurrentPageIndex + 1;
            recCount = objPage.PageCount;
            totalRecords = objPage.DataSourceCount;

            // Assigning Datasource to the DataList.
            DataListProducts.DataSource = objPage;
            DataListProducts.DataBind();
            //Zeon Custom Code: Start
            if (imgGrid1.CssClass == "DeActiveLink")
            {
                SetGridView();
            }
            else
            {
                SetListView();
            }


            if (!this.IsPostBack)
            {
                this.DoPaging();
            }
        }

        /// <summary>
        /// Represents the GetPageSize method
        /// </summary>
        /// <returns>Return the Page size</returns>
        private int GetPageSize()
        {
            if (ViewState["PageSize"] == null)
            {
                // Default page size
                return 48;
            }
            else
            {
                if (ViewState["PageSize"].ToString().ToLower() == "-1")
                {
                    // Disply all items in one page
                    return -1;
                }
                else
                {
                    // Display selected page size
                    return Convert.ToInt32(ViewState["PageSize"].ToString());
                }
            }
        }

        /// <summary>
        /// Binding Paging List
        /// </summary>
        private void DoPaging()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("PageIndex");
            dt.Columns.Add("PageText");
            this.FirstIndex = this.CurrentPage - 5;
            if (this.CurrentPage > 5)
            {
                this.LastIndex = this.CurrentPage + 5;
            }
            else
            {
                this.LastIndex = 10;
            }

            if (this.LastIndex > Convert.ToInt32(ViewState["TotalPages"]))
            {
                this.LastIndex = Convert.ToInt32(ViewState["TotalPages"]);
                this.FirstIndex = this.LastIndex - 10;
            }

            if (this.FirstIndex < 0)
            {
                this.FirstIndex = 0;
            }

            ddlTopPaging.Items.Clear();
            ddlBottomPaging.Items.Clear();

            string pagingText = this.GetLocalResourceObject("PagingText").ToString();

            // Each items seperated with & symbol
            string[] pagingItems = pagingText.Split(new char[] { '&' });
            foreach (string pagingItem in pagingItems)
            {
                if (pagingItem.Contains("|") && pagingItem.Trim().Length > 0)
                {
                    // Key value pair seperated with | symbol
                    string[] itemText = pagingItem.Split(new char[] { '|' });
                    DataRow dr = null;
                    dr = dt.NewRow();
                    dr[0] = Convert.ToInt32(itemText[1].Trim());
                    dr[1] = itemText[0].Trim();
                    dt.Rows.Add(dr);
                }
            }

            this.ddlTopPaging.DataTextField = "PageText";
            this.ddlTopPaging.DataValueField = "PageIndex";
            this.ddlTopPaging.DataSource = dt;
            this.ddlTopPaging.DataBind();

            this.ddlBottomPaging.DataTextField = "PageText";
            this.ddlBottomPaging.DataValueField = "PageIndex";
            this.ddlBottomPaging.DataSource = dt;
            this.ddlBottomPaging.DataBind();
            //Zeon Custom Code:Starts
            if (ViewState["PageSize"] == null)
            {
                string defaultPageSize = "48";
                ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(defaultPageSize));
                ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
            }
            //Zeon Custom Code:Ends
        }
        #endregion

        #region Helper Functions

        /// <summary>
        /// Represents the SetSelectedPageSize method
        /// </summary>
        /// <param name="selectedPageSize">Selected page Size</param>
        private void SetSelectedPageSize(int selectedPageSize)
        {
            ViewState["PageSize"] = selectedPageSize;

            // Point to the first page if user selects the page size.
            ViewState["CurrentPage"] = 0;
            this.BindDataListPagedDataSource(false);
        }

        /// <summary>
        /// Select the selected value in the both top and bottom page controls.
        /// </summary>
        /// <param name="ddlPaging">Paging DropDownList</param>
        /// <param name="p">Paging value</param>
        private void SelectDropdownValue(DropDownList ddlPaging, string p)
        {
            ListItem li = ddlPaging.Items.FindByValue(p);
            if (li != null)
            {
                ddlPaging.SelectedValue = p;
            }
        }

        #endregion

        # region Zeon Custom  Methods

        protected void imgbtnList_OnClick(object sender, EventArgs e)
        {
            SetListView();
        }

        /// <summary>
        /// Set List View of product
        /// </summary>
        private void SetListView()
        {
            DataListProducts.RepeatColumns = 1;
            DataListProducts.RepeatDirection = RepeatDirection.Vertical;

            imgbtnGrid.Enabled = true;
            imgbtnGrid.CssClass = "ActiveLink GridView";
            imgGrid1.Enabled = true;

            imgbtnList.Enabled = false;
            imgbtnList.CssClass = "DeActiveLink ListView";
            imgListActive1.Enabled = false;

            imgListActive1.ImageUrl = _listActiveURL;

            imgGrid1.ImageUrl = _gridDeActiveURL;

            imgListActive1.CssClass = "DeActiveLink";
            imgGrid1.CssClass = "ActiveLink";

        }

        /// <summary>
        /// Handeles imgbtnGrid_OnClick click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgbtnGrid_OnClick(object sender, EventArgs e)
        {
            SetGridView();
        }

        /// <summary>
        /// Set Grid view of product list
        /// </summary>
        private void SetGridView()
        {
            DataListProducts.RepeatColumns = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
            DataListProducts.RepeatDirection = RepeatDirection.Horizontal;

            imgbtnGrid.CssClass = "DeActiveLink GridView";
            imgbtnGrid.Enabled = false;
            imgGrid1.Enabled = false;

            imgbtnList.CssClass = "ActiveLink ListView";
            imgbtnList.Enabled = true;
            imgListActive1.Enabled = true;

            imgListActive1.ImageUrl = _listDeActiveURL;

            imgGrid1.ImageUrl = _gridActiveURL;

            imgListActive1.CssClass = "ActiveLink";

            imgGrid1.CssClass = "DeActiveLink";
        }

        /// <summary>
        /// Handeles LstFilterBottom selected index change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LstFilterBottom_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Zeon Custom Code:start
            lstFilter.SelectedValue = lstBottomFilter.SelectedValue;
            BindDataListPagedDataSource(true);
            //Zeon Custom Code: end
            //this.CustomBindDataListPagedDataSource(true);//Znode old code

        }

        /// <summary>
        /// Bind Datalist using Paged DataSource object
        /// </summary>
        /// <param name="ApplySort">Apply Sort</param>
        private void CustomBindDataListPagedDataSource(bool ApplySort)
        {
            // retrieve collection object from Viewstate
            if (ViewState["ProductList"] != null)
            {
                this._productList = (ZNodeProductList)ViewState["ProductList"];
            }

            // Apply Sorting Only Page gets Reload.
            if (ApplySort)
            {
                // Filter products by Price range
                if (lstBottomFilter.SelectedValue.Equals("1"))
                {
                    this._productList.ZNodeProductCollection.Sort("FinalPrice");
                }
                else if (lstBottomFilter.SelectedValue.Equals("2"))
                {
                    this._productList.ZNodeProductCollection.Sort("FinalPrice", SortDirection.Descending);
                }
                else if (lstBottomFilter.SelectedValue.Equals("0"))
                {
                    this._productList.ZNodeProductCollection.Sort("DisplayOrder");
                }
                lstFilter.SelectedValue = lstBottomFilter.SelectedValue;
                ViewState["ProductList"] = this._productList;
            }
        }

        /// <summary>
        /// set shortdescription visibility
        /// </summary>
        /// <returns>bool</returns>
        protected bool SetShortDescriptionVisibility()
        {
            bool isVisible = false;
            if (Request.Params["v"] != null && !string.IsNullOrEmpty(Request.Params["v"].ToString()))
            {
                if (Request.Params["v"].ToString().Equals("l")) { isVisible = true; }
            }
            return isVisible;
        }

        /// <summary>
        /// Register Client Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder quckwatchscript = new StringBuilder();
            quckwatchscript.Append("var quickViewHelper = new QuickViewHelper({");
            quckwatchscript.Append("\"Controls\":{");
            quckwatchscript.AppendFormat("\"{0}\":\"{1}\"", ifzQuickView.ID, ifzQuickView.ClientID);
            quckwatchscript.Append("},\"Messages\":{");
            quckwatchscript.Append("}");
            quckwatchscript.Append("});quickViewHelper.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "QuickwatchScript", quckwatchscript.ToString(), true);
        }

        /// <summary>
        /// Bind Top Reviwed Product List of Brand
        /// </summary>
        private void BindTopReviewedProductList()
        {
            pnlLatestTopReviewedPrd.Visible = false;
            ZNodeManufacturer manuf = null;
            int maxCount = int.Parse(Resources.CommonCaption.MaxBrandLatestReviewdListCount);
            if (HttpContext.Current.Items["Brand"] != null)
            {
                manuf = (ZNodeManufacturer)HttpContext.Current.Items["Brand"];
            }
            if (manuf != null)
            {
                locTopReviewedTitle.Text = string.Format(GetLocalResourceObject("TopReviewdProductListTitle").ToString(), manuf.Name);
                this.LatestReviewedProductList = ZNodeProductList.GetBrandsTopReviewedProductList(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, manuf.ManufacturerID, maxCount);
                if (this.LatestReviewedProductList != null && this.LatestReviewedProductList.ZNodeProductCollection.Count > 0)
                {
                    pnlLatestTopReviewedPrd.Visible = true;
                    rptTopReviewedProdList.DataSource = this.LatestReviewedProductList.ZNodeProductCollection;
                    rptTopReviewedProdList.DataBind();
                }
            }
        }

        /// <summary>
        /// return SEO url
        /// </summary>
        /// <param name="ProductId">object</param>
        /// <param name="seo">object</param>
        /// <returns>string</returns>
        protected string GetProductURL(object ProductId, object seo)
        {
            string productURL = string.Empty;
            if (ProductId != null && seo != null)
            {
                productURL = ZNode.Libraries.ECommerce.SEO.ZNodeSEOUrl.MakeURL(ProductId.ToString(), SEOUrlType.Product, seo.ToString());
            }

            return productURL;
        }

        /// <summary>
        /// Get Image Path
        /// </summary>
        /// <param name="imgName"></param>
        /// <returns></returns>
        protected string GetImagePath(object imgName)
        {
            string imageName = string.Empty;
            ZNodeImage _znodeImage = new ZNodeImage();
            if (imgName != null)
            {
                imageName = _znodeImage.GetImageHttpPathSmall(imgName.ToString());
            }
            return imageName;
        }

        /// <summary>
        /// return Formatted Price
        /// </summary>
        /// <param name="salesPrice">object</param>
        /// <param name="retailPrice">object</param>
        /// <returns>string</returns>
        protected string GetFormattedPrice(object salesPrice, object retailPrice)
        {
            string formattedPrice = string.Empty;
            if (retailPrice != null)
            {
                if (salesPrice != null && !string.IsNullOrEmpty(salesPrice.ToString()))
                {
                    formattedPrice = ZNodePricingFormatter.ConfigureProductPricing(decimal.Parse(retailPrice.ToString()), decimal.Parse(salesPrice.ToString()));
                  
                }
                else
                {
                    formattedPrice = ZNodePricingFormatter.ConfigureProductPricing(decimal.Parse(retailPrice.ToString()));                 

                }
            }
            return formattedPrice;

        }

        # endregion

    }
}
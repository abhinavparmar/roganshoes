<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Brand_Brand" CodeBehind="Brand.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/SwatchImages.ascx" TagName="Swatches" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Category/NavigationCategories.ascx" TagName="NavigationCategories" TagPrefix="ZNode" %>
<%--<%@ Register Src="~/Controls/Default/specials/navigationspecials.ascx" TagName="NavigationSpecials" TagPrefix="ZNode" %>--%>
<div id="LeftColumn">

    <ZNode:NavigationCategories ID="CATEGORIES" runat="server" IsShowAllCategories="true" EnableNavigationViewState="false"></ZNode:NavigationCategories>
    <%-- <ZNode:NavigationSpecials ID="SPECIALS" runat="server"></ZNode:NavigationSpecials>--%>
</div>

<div id="SeperateColumn"></div>
<div id="MiddleColumn">
    <div>
        <asp:Panel ID="pnlProductList" runat="server" meta:resourcekey="pnlProductListResource1" ClientIDMode="Static">
            <div class="Brand">
                <div class="PageTitle">
                    <h1>
                        <asp:Label ID="lblBrandName" runat="server" meta:resourcekey="lblBrandNameResource1" ClientIDMode="Static"></asp:Label></h1>
                </div>
                <div class="Description">
                    <asp:Label ID="BrandDescription" runat="server" ClientIDMode="Static"></asp:Label>
                </div>
                <div class="ProductList">
                    <div id="ProductListNavigation">
                        <div class="TopPagingSection">
                            <div class="Sorting">
                                <span class="PageResult"><strong>
                                    <asp:Literal ID="ltrTopTotalCounts" meta:resourcekey="ltrTotalCountresource" runat="server"></asp:Literal></strong>
                                    &nbsp;&nbsp;<asp:Literal ID="ltrTopTotalResults" meta:resourcekey="ltrTotalResultsresource" runat="server"></asp:Literal>
                                </span>&nbsp;&nbsp; 
                        <div class="PageSort">
                            <span class="Label">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:CommonCaption, Sort%>" ClientIDMode="Static" /></span>
                            <asp:DropDownList ID="lstFilter" runat="server" OnSelectedIndexChanged="LstFilter_SelectedIndexChanged" AutoPostBack="True" meta:resourcekey="lstFilterResource1" ClientIDMode="Static">
                                <asp:ListItem Value="0" meta:resourcekey="ListItemResource1">Popular Items</asp:ListItem>
                                <%--<asp:ListItem Value="1" meta:resourcekey="ListItemResource2">Price Low to High</asp:ListItem>
                                <asp:ListItem Value="2" meta:resourcekey="ListItemResource3">Price High to Low</asp:ListItem>--%>
                            </asp:DropDownList>
                        </div>
                                <div class="PageShow">
                                    <span class="label">
                                        <asp:Literal ID="ltrTopShow" runat="server" meta:resourceKey="ltrShowresource"></asp:Literal></span>
                                    <asp:DropDownList ID="ddlTopPaging" runat="server" AutoPostBack="True" CssClass="Pagingdropdown" meta:resourcekey="ddlTopPagingResource1" OnSelectedIndexChanged="DdlTopPaging_SelectedIndexChanged" ClientIDMode="Static"></asp:DropDownList>

                                </div>
                                <div class="PageNavArrow">
                                    <span class="label">
                                        <asp:Literal ID="ltrTopPage" runat="server" meta:resourceKey="ltrShowPage"></asp:Literal></span>
                                    <asp:LinkButton ID="TopPrevLink" CssClass="Button" runat="server" OnClick="PrevRecord" meta:resourcekey="hlkPrviLink" ClientIDMode="Static"></asp:LinkButton>

                                    <span>
                                        <asp:Localize ID="locTopPageNum" runat="server" meta:resourceKey="txtPage" ClientIDMode="Static">
                                        </asp:Localize>&nbsp;&nbsp;
                           
                                    </span>
                                </div>
                                <asp:LinkButton ID="TopNextLink" runat="server" OnClick="NextRecord" CssClass="Button" meta:resourcekey="hlkNextiLink" ClientIDMode="Static"></asp:LinkButton>
                                <span>
                                    <asp:Localize ID="locTopPageTotal" runat="server" meta:resourceKey="txtOF" ClientIDMode="Static">
                                    </asp:Localize></span>
                            </div>
                            <div class="TopPaging">
                                <div class="ListGrideView">
                                    <asp:LinkButton ID="imgbtnGrid" CssClass="ActiveLink GridView" runat="server" Visible="False" Enabled="False" ToolTip="Grid" OnClick="imgbtnGrid_OnClick" ClientIDMode="Static"></asp:LinkButton>
                                    <asp:ImageButton ID="imgGrid1" runat="server" AlternateText="Grid" CssClass="DeActiveLink" ImageUrl="~/Themes/Default/Images/grid-active.gif" ToolTip="Grid" OnClick="imgbtnGrid_OnClick" Enabled="false" ClientIDMode="Static" Visible="false" />
                                    &nbsp;&nbsp;<asp:LinkButton ID="imgbtnList" CssClass="DeActiveLink ListView" runat="server" Visible="False" ToolTip="List" OnClick="imgbtnList_OnClick" Enabled="true"></asp:LinkButton>
                                    <asp:ImageButton ID="imgListActive1" AlternateText="List" ToolTip="List" ImageUrl="~/Themes/Default/Images/list.gif" runat="server" CssClass="DeActiveLink" OnClick="imgbtnList_OnClick" Enabled="true" ClientIDMode="Static" Visible="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="BrandProductlist">
                        <asp:DataList ID="DataListProducts" runat="server" RepeatDirection="Horizontal" OnItemDataBound="DataListProducts_ItemDataBound"
                            meta:resourcekey="DataListProductsResource1"  RepeatLayout="Flow">
                            <ItemStyle CssClass="ItemStyle" VerticalAlign="Top" />
                            <ItemTemplate>
                                <div class="BrandListItem">
                                    <div class="ItemType">
                                        <%--<asp:Image ID="NewItemImage" EnableViewState="false" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png") %>'
                                            meta:resourcekey="NewItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "NewProductInd") %>' ClientIDMode="Static" AlternateText="New" alt="New" />--%>
                                        <span class="rs-new" id="spnRSNew" runat="server" visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>'><span class="rs-new-text">New</span></span>
                                        &nbsp;<asp:Image EnableViewState="false" ID="FeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                            meta:resourcekey="FeaturedItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' ClientIDMode="Static" AlternateText="Featured" ToolTip="Featured" />
                                    </div>
                                    <div class="Image">
                                        <a id="imgProduct" class="boxed" href='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'>
                                            <img id='<%# "Img"  + DataBinder.Eval(Container.DataItem, "ProductID")%>' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                                border="0" src='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "SmallImageFilePath").ToString()) %>' style="vertical-align: bottom;" title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'>
                                            </img>
                                        </a>
                                        <div class="AddViewProduct">
                                            <div class="HoverBox">
                                                <div class="CompareBox">
                                                    <%-- <asp:CheckBox ID="chkCompareProduct" data-pid='<%# DataBinder.Eval(Container.DataItem, "ProductID").ToString() %>'
                                            data-catid='<%# this._categoryId %>' runat="server" EnableViewState="false" onchange="zeonCatProdList.CheckBoxChange(this);" />--%>
                                                    <%-- <div class="SubCompare">
                                            <asp:LinkButton ID="lbtnCompare" runat="server" meta:resourceKey="lbtnCompareResource1" EnableViewState="false"
                                                CssClass="CompareButton" ClientIDMode="Static"></asp:LinkButton>
                                        </div>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="QuickViewLink">
                                            <div class="SubCompare">
                                                <a href="javascript:void(0)" onclick="return quickViewHelper.SetSrcInframe(<%# DataBinder.Eval(Container.DataItem, "ProductID").ToString() %>)">Quick View</a>
                                            </div>
                                        </div>
                                        <%--<div>
                                    <div>
                                        <a href="javascript:void(0)" onclick="quickViewHelper.SetSrcInframe(<%# DataBinder.Eval(Container.DataItem, "ProductID").ToString() %>)">Quick View</a>
                                    </div>
                                </div>--%>
                                    </div>
                                    <div class="ProductSwatches">
                                        <div class="ColorCaption">
                                            <a href='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' id="lnkColorCaption" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ProductSKUCount")) > 1 %>' class="ColorPicker">MORE COLORS AVAILABLE</a>
                                        </div>
                                        <%--<ZNode:Swatches ID="uxProductSwatches" runat="server" ControlType="Skus" Product='<%# Container.DataItem %>'
                                    ProductId='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ProductSKUCount")) > 0? DataBinder.Eval(Container.DataItem, "ProductID"):0 %>' IncludeProductImage="true" ClientIDMode="Static"
                                    ZNodeSKUImageCollection='<%#((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).ZNodeSKUImageCollection %>' />--%>
                                    </div>
                                    <div class="StarRating">
                                        <ZNode:ProductAverageRating ID="uxProductAverageRating" runat="server" TotalReviews='<%# DataBinder.Eval(Container.DataItem, "TotalReviews") %>'
                                            ViewProductLink='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink")%>'
                                            ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ReviewRating")%>' />
                                    </div>
                                    <div class="DetailLink">
                                        <asp:HyperLink ID="hlName" runat="server" NavigateUrl='<%#ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' ClientIDMode="Static">
                                <%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></asp:HyperLink>
                                    </div>
                                    <%--Zeon Custom HTML: Start--%>
                                    <div class="ShortDescription">
                                        <asp:Label ID="ShortDescription" ClientIDMode="Static" runat="server" Text='<%# WebApp.ZCommonHelper.GetFormattedProductName(((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).ShortDescription.ToString(), System.Configuration.ConfigurationManager.AppSettings["ProductGridNameMaxLimit"].ToString())%>' meta:resourcekey="ShortDescriptionResource1" EnableViewState="false" Visible="<%#SetShortDescriptionVisibility()%>" />
                                    </div>
                                    <br />
                                    <%--Zeon Custom HTML: End--%>
                                    <div class="Price">
                                        <asp:Label ID="Label2" EnableViewState="false" runat="server" meta:resourcekey="Label2Resource1" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>'
                                            Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && BrandProfile.ShowPrice %>' ></asp:Label>
                                    </div>
                                    <div class="CallForPrice">
                                        <asp:Label ID="uxCallForPricing" EnableViewState="false" runat="server" CssClass="Price" meta:resourcekey="uxCallForPricingResource1"
                                            Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing")) %>'></asp:Label>
                                    </div>
                                    <div class="BuyButton">
                                        <div>
                                            <%--<asp:HyperLink ID="btnbuy" EnableViewState="false" runat="server"
                                        CssClass="Button View" meta:resourcekey="btnbuyResource1" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink") %>'
                                        Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && BrandProfile.ShowAddToCart %>' ClientIDMode="Static">View � </asp:HyperLink>--%>
                                        </div>

                                        <div style="clear: both;">
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                    <div class="BottomPaging">
                        <div class="Paging">
                            <span class="Sorting">
                                <strong>
                                    <asp:Literal ID="ltrBottomTotalCount" meta:resourcekey="ltrTotalCountresource" ClientIDMode="Static" runat="server"></asp:Literal>
                                </strong>

                                &nbsp;&nbsp;<asp:Literal ID="ltrBottomTotalResult" ClientIDMode="Static" meta:resourcekey="ltrTotalResultsresource" runat="server"></asp:Literal></span>
                            <div class="PageSort">
                                <span class="Label">
                                    <asp:Literal ID="ltrBottomFilter" runat="server" Text="<%$ Resources:CommonCaption, Sort%>" ClientIDMode="Static" /></span>
                                <asp:DropDownList ID="lstBottomFilter" runat="server" OnSelectedIndexChanged="LstFilterBottom_SelectedIndexChanged" AutoPostBack="True" meta:resourcekey="lstFilterResource1" ClientIDMode="Static">
                                    <asp:ListItem Value="0" meta:resourcekey="ListItemResource1">Popular Items</asp:ListItem>
                                    <%--<asp:ListItem Value="1" meta:resourcekey="ListItemResource2">Price Low to High</asp:ListItem>
                                    <asp:ListItem Value="2" meta:resourcekey="ListItemResource3">Price High to Low</asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>
                            <div class="PageShow">
                                <span class="label">
                                    <asp:Literal ID="ltrBottomShow" meta:resourceKey="ltrShowresource" runat="server"></asp:Literal></span>
                                <asp:DropDownList runat="server" ID="ddlBottomPaging" CssClass="Pagingdropdown" AutoPostBack="True"
                                    OnSelectedIndexChanged="DdlBottomPaging_SelectedIndexChanged" meta:resourcekey="ddlBottomPagingResource1" ClientIDMode="Static">
                                </asp:DropDownList>
                            </div>
                            <div class="PageNavArrow">
                                <span class="label">PAGE</span>
                                <asp:LinkButton ID="BotPrevLink" runat="server" OnClick="PrevRecord" CssClass="Button" meta:resourcekey="hlkPrviLink" ClientIDMode="Static">
                                </asp:LinkButton>
                                <span>
                                    <asp:Localize ID="locBottomPageNum" runat="server" ClientIDMode="Static"></asp:Localize>
                                </span>&nbsp;&nbsp;
                            </div>
                            <asp:LinkButton ID="BotNextLink" OnClick="NextRecord" runat="server" CssClass="Button" meta:resourcekey="hlkNextiLink" ClientIDMode="Static">
                            </asp:LinkButton>
                            <span>
                                <asp:Localize ID="locBottomPageTotal" runat="server" ClientIDMode="Static"></asp:Localize></span>
                        </div>
                    </div>
                </div>

            </div>
        </asp:Panel>

        <div class="MessageText">
            <asp:Label ID="lblMsg" runat="server" meta:resourcekey="lblMsgResource1" ClientIDMode="Static"></asp:Label>
        </div>
        <div id="divQuickWatch" style="display: none">
            <iframe id="ifzQuickView" title="quickview" runat="server" style="border-style: none; border: 0px; background-color: transparent; width: 100%; min-height: 100%; overflow: hidden;"
               ></iframe>
        </div>
    </div>
</div>
<div class="BrandReview">
    <asp:Panel ID="pnlLatestTopReviewedPrd" runat="server">
        <h2 class="media-heading"><span>
            <asp:Localize ID="locTopReviewedTitle" runat="server"></asp:Localize></span></h2>
        <ul>
            <asp:Repeater ID="rptTopReviewedProdList" runat="server">
                <ItemTemplate>
                    <li>
                        <div class="ProductImage">
                            <div class="Image">
                                <div class="ItemType">
                                    <%--<asp:Image ID="NewReviewdItemImage" EnableViewState="false" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png") %>'
                                        meta:resourcekey="NewItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "NewProductInd") %>' ClientIDMode="Static" />--%>
                                    <span class="rs-new" id="spnRSNew" runat="server" visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>'><span class="rs-new-text">New</span></span>
                                    &nbsp;<asp:Image EnableViewState="false" ID="RevFeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                        meta:resourcekey="FeaturedItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' ClientIDMode="Static" AlternateText="Featured Item" ToolTip="Featured Item" />

                                </div>
                                <a id="imgReviewedProduct" class="boxed" href='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'>
                                    <img id='<%# "Img"  + DataBinder.Eval(Container.DataItem, "ProductID")%>' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                        border="0" src='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "SmallImageFilePath").ToString()) %>' style="vertical-align: bottom;" title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'>
                                    </img>
                                </a>
                            </div>
                        </div>
                        <div class="ProductSpecification">
                            <div class="DetailLink">
                                <asp:HyperLink ID="hlRevName" runat="server" NavigateUrl='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' ClientIDMode="Static">
                                <%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></asp:HyperLink>
                            </div>
                            <div id="Div1" class="Price" runat="server" visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && BrandProfile.ShowPrice %>'>
                                <asp:Label ID="lblRevPrice" EnableViewState="false" runat="server" meta:resourcekey="Label2Resource1" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>'
                                    Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && BrandProfile.ShowPrice %>'></asp:Label>
                            </div>
                            <div class="CallForPrice">
                                <asp:Label ID="uxRevCallForPricing" EnableViewState="false" runat="server" CssClass="Price" meta:resourcekey="uxCallForPricingResource1"
                                    Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing")) %>'></asp:Label>
                            </div>

                            <div class="StarRating">
                                <ZNode:ProductAverageRating ID="uxProductAverageRating" runat="server" TotalReviews='1'
                                    ViewProductLink='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString())%>'
                                    ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ZNodeBrandTopReview.Rating")%>' />
                            </div>
                        </div>
                        <div class="ProductReview">
                            <div class="Reviewver">
                                <div class="ReviewSubject">
                                    <asp:Localize ID="Localize1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ZNodeBrandTopReview.Subject")%>'></asp:Localize></div>
                                <div class="ReviewUser">
                                    <asp:Localize ID="locCreateUser" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ZNodeBrandTopReview.CreateUser")%>'></asp:Localize></div>
                            </div>
                            <div class="ReviewverDetails">
                                <div class="ReviewDetail">
                                    <asp:Localize ID="locReviewProduct" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name")+ " Review :" %>'></asp:Localize></div>
                                <div class="ReviewComment">
                                    <asp:Localize ID="locReview" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ZNodeBrandTopReview.Comments")%>'></asp:Localize></div>
                            </div>
                        </div>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </asp:Panel>
</div>


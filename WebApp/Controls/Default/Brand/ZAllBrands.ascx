﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZAllBrands.ascx.cs" Inherits="WebApp.Controls.Default.Brand.ZAllBrands" %>
<div class="ContentContainer">
    <h1 class="CategoryTitle">
        <asp:Literal ID="ltrAllBrandTitle"  EnableViewState="false" Text="Shop By Brand" runat="server" ClientIDMode="Static"></asp:Literal></h1>
    <div class="AllBrands">
        <asp:Literal ID="ltlBrandLinkTab" runat="server" EnableViewState="false"></asp:Literal>
    </div>
    <br />

    <div class="Clear"></div>
    <div id="AllBrandsContainer">
        <div id="dvAllBrandsList">
            <asp:Literal ID="ltlAllBrands" runat="server" EnableViewState="false"></asp:Literal>
        </div>
    </div>
    <div class="horizontalline">&nbsp;</div>
</div>

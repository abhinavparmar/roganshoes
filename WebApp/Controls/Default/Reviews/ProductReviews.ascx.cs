using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Product Reviews user control class.
    /// </summary>
    public partial class Controls_Default_Reviews_ProductReviews : System.Web.UI.UserControl
    {
        #region Private Variables
        private const string _ImageFilePath = "<img src='{0}' alt='{1}' />";
        private static int CurrentPage;
        private ZNodeProduct _Product;
        private int _PageSize = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayItems;
        private bool _ShowProsField = false;
        private bool _ShowConsField = false;
        private string _StarRatingImagePath = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/ShadeStar.png";
        private string _ShadeStarImageImagePath = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/Star.png";

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the catalog item object. The control will bind to the data from this object
        /// </summary>
        public ZNodeProduct Product
        {
            get
            {
                return this._Product;
            }

            set
            {
                this._Product = value;
            }
        }

        /// <summary>
        /// Gets or sets the paging size
        /// (i.e) The number of rows from the datasource to display per page
        /// </summary>
        public int PageSize
        {
            get
            {
                return this._PageSize;
            }

            set
            {
                this._PageSize = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the bool value display or hides the Pros text field to the end user
        /// </summary>
        public bool ShowProsField
        {
            get
            {
                return this._ShowProsField;
            }

            set
            {
                this._ShowProsField = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether bool value display or hides the Cons text field to the end user
        /// </summary>
        public bool ShowConsField
        {
            get
            {
                return this._ShowConsField;
            }

            set
            {
                this._ShowConsField = value;
            }
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            int totalReviews = this.Product.ZNodeReviewCollection.Count;

            if (totalReviews > 0)
            {
                PagedDataSource datalistPaging = new PagedDataSource();
                datalistPaging.DataSource = this.Product.ZNodeReviewCollection;
                datalistPaging.PageSize = this.PageSize;
                datalistPaging.AllowPaging = true;
                datalistPaging.CurrentPageIndex = 0;
                CurrentPage = 0;

                lblPagingInfo.Text = " | " + Resources.CommonCaption.Page + " " + (CurrentPage + 1) + " " + Resources.CommonCaption.of + " " + datalistPaging.PageCount.ToString() + " | ";

                // Checking for enabling/disabling next/prev buttons.
                // Next/prev button will be disabled when is the last/first page of the pageobject.        
                TopNextLink.Enabled = !datalistPaging.IsLastPage;
                TopPrevLink.Enabled = !datalistPaging.IsFirstPage;

                ViewState["ReviewList"] = this.Product.ZNodeReviewCollection;
                ReviewList.DataSource = datalistPaging;
                ReviewList.DataBind();
                //PRFT Custom Code:Starts
                ucProductReviewMessage.Visible = true;
                //PRFT Custom Code:Ends
            }
            else
            {
                topNavigation.Visible = false;
                lblErrorMsg.Text = this.GetLocalResourceObject("NoReviews").ToString();
            }

            CustomersCnt.Text = " " + totalReviews.ToString() + " ";
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._Product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            if (!Page.IsPostBack)
            {
                this.Bind();
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Returns star image for this rating value
        /// </summary>
        /// <param name="rate">The Rating Value</param>
        /// <returns>Returns the Star Image Path</returns>
        protected string GetRatingImagePath(string rate)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 1; i <= 5; i++)
            {
                if (int.Parse(rate) >= i)
                {
                    sb.Append(string.Format(_ImageFilePath, ResolveUrl(this._StarRatingImagePath), "Star " + Product.Name));
                }
                else
                {
                    sb.Append(string.Format(_ImageFilePath, ResolveUrl(this._ShadeStarImageImagePath), "Shaded Star " + Product.Name));
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Returns the Formatted Date
        /// </summary>
        /// <param name="createdDate">Created Date</param>
        /// <returns>Returns the formatted date</returns>
        protected string GetFormattedDate(object createdDate)
        {
            DateTime dt = DateTime.Parse(createdDate.ToString());
            return dt.ToString("m") + ", " + dt.Year.ToString();
        }

        /// <summary>
        /// Represents the Next Record Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void NextRecord(object sender, EventArgs e)
        {
            CurrentPage += 1;

            this.BindDataListPagedDataSource();
        }

        /// <summary>
        /// Represents the Previous Record method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PrevRecord(object sender, EventArgs e)
        {
            CurrentPage -= 1;

            this.BindDataListPagedDataSource();
        }

        /// <summary>
        /// Event is fired when RateList selected Index is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RateList_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentPage = 0;
            this.BindDataListPagedDataSource();
            RateList.Focus();
        }

        #endregion

        #region DataList Paging Methods & Events

        /// <summary>
        /// Bind Datalist using Paged DataSource object
        /// </summary>
        private void BindDataListPagedDataSource()
        {
            try
            {
                // Declare dataset
                ZNodeGenericCollection<ZNodeReview> reviewList = new ZNodeGenericCollection<ZNodeReview>();

                // Retrieve dataset from Viewstate
                if (ViewState["ReviewList"] != null)
                {
                    reviewList = ViewState["ReviewList"] as ZNodeGenericCollection<ZNodeReview>;
                }

                // Check whether dataview object has more than 0 items
                if (reviewList.Count > 0)
                {
                    // Filter reviews
                    if (RateList.SelectedValue.Equals("desc"))
                    {
                        reviewList.Sort("ReviewID", SortDirection.Descending);
                    }
                    else if (RateList.SelectedValue.Equals("asc"))
                    {
                        reviewList.Sort("ReviewID");
                    }
                    else if (RateList.SelectedValue.Equals("5"))
                    {
                        reviewList.Sort("Rating", SortDirection.Descending);
                    }
                    else if (RateList.SelectedValue.Equals("1"))
                    {
                        reviewList.Sort("Rating", SortDirection.Ascending);
                    }
                }

                PagedDataSource datalistPaging = new PagedDataSource();
                datalistPaging.DataSource = reviewList;
                datalistPaging.PageSize = this.PageSize;
                datalistPaging.AllowPaging = true;
                datalistPaging.CurrentPageIndex = CurrentPage;

                lblPagingInfo.Text = " | Page " + " " + (CurrentPage + 1) + " of " + datalistPaging.PageCount.ToString() + " | ";

                // Checking for enabling/disabling next/prev buttons.
                // Next/prev buton will be disabled when is the last/first page of the pageobject.        
                TopNextLink.Enabled = !datalistPaging.IsLastPage;
                TopPrevLink.Enabled = !datalistPaging.IsFirstPage;

                ReviewList.DataSource = datalistPaging;
                ReviewList.DataBind();
                //PRFT Custom Code:Starts
                ucProductReviewMessage.Visible = true;
                //PRFT Custom Code:Ends
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }
        }
        #endregion
    }
}
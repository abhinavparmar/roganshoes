using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Zeon.Libraries.Elmah;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Customer Review user control class.
    /// </summary>
    public partial class Controls_Default_Reviews_CustomerReview : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private ZNodeProduct Product;
        #endregion

        #region Public Properties
        /// <summary>
        /// Sets a value indicating whether to display or hides the Pros text field to the end user
        /// </summary>
        public bool ShowProsField
        {
            set { rowPros.Visible = value; }
        }

        /// <summary>
        /// Sets a value indicating whether to display or hides the Cons text field to the end user
        /// </summary>
        public bool ShowConsField
        {
            set { rowCons.Visible = value; }
        }
        #endregion

        #region Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Bind product Info
            this.Bind();
            Title.Focus();
        }

        /// <summary>
        /// Event is raised when Submit button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ReviewService reviewService = new ReviewService();
            Review entity = new Review();
            ZNodeUserAccount _account = ZNodeUserAccount.CurrentAccount();

            // Set properties
            if (_account != null)
            {
                entity.AccountID = _account.AccountID;
            }
            else
            {
                entity.AccountID = null;
            }

            entity.ProductID = this.Product.ProductID;
            entity.Rating = int.Parse(RatingList.SelectedValue);
            entity.Subject = Server.HtmlEncode(Title.Text.Trim());
            entity.Comments = Server.HtmlEncode(Comments.Text.Trim());
            entity.CreateUser = Server.HtmlEncode(Name.Text.Trim());
            entity.CreateDate = System.DateTime.Now;
            entity.Status = ZNodeConfigManager.SiteConfig.DefaultReviewStatus;
            entity.Pros = Server.HtmlEncode(Pros.Text.Trim());
            entity.Cons = Server.HtmlEncode(Cons.Text.Trim());
            entity.UserLocation = Server.HtmlEncode(Location.Text.Trim());

            // Update to database
            bool check = reviewService.Insert(entity);

            if (check)
            {
                SaveCustomerReviewExtn(entity.ReviewID);//Zeon Custom Code
                Response.Redirect(this.Product.ViewProductLink);
            }
            else
            {
                ErrorMessage.Text = this.GetLocalResourceObject("ReviewSubmitFailed").ToString();
            }
        }

        /// <summary>
        /// Event is raised when Cancel button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.Product.ViewProductLink);
        }
        #endregion

        #region Bind Method
        /// <summary>
        /// Bind data list
        /// </summary>
        private void Bind()
        {
            if (HttpContext.Current.Items["Product"] == null)
            {
                Response.Redirect("~/default.aspx");

                return;
            }

            // retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.Product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            if (this.Product != null)
            {
                ltrlProductName.Text = "\"" + this.Product.Name + "\"";
                ZNodeImage zImage = new ZNodeImage();
                imgProduct.ImageUrl = this.Product.SmallImageFilePath;
                imgProduct.AlternateText = this.Product.Name;
                imgProduct.ToolTip = this.Product.Name;
                ltrProdDesc.Text = this.Product.ShortDescription;
            }
        }
        #endregion

        #region Zeon Custom Code

        /// <summary>
        /// save Customer email in ZnodeReviewExtn
        /// </summary>
        /// <param name="reviewID"></param>
        private void SaveCustomerReviewExtn(int reviewID)
        {
            ReviewExtnService extnService = new ReviewExtnService();
            try
            {
                ReviewExtn reviewExtn = new ReviewExtn();
                reviewExtn.ReviewID = reviewID;
                reviewExtn.Email = txtEmail.Text;
                extnService.Insert(reviewExtn);
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }
        }
        #endregion
    }
}
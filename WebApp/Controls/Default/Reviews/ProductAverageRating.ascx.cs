using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the ProductAverageRating user control class.
    /// </summary>
    public partial class Controls_Default_Reviews_ProductAverageRating : System.Web.UI.UserControl
    {
        #region Private Variables

        private const string _ImageFilePath = "<img src='{0}' valign='absmiddle' alt='{1}' border='0' width='20' height='19'/>";
        private bool _IsQuickWatch = false;
        private string _StarRatingImagePath = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/ShadeStar.png";
        private string _ShadeStarImageImagePath = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/Star.png";
        private string _ViewProductLink = string.Empty;
        private int _ReviewRating = 0;
        private int _TotalReviews = 0;
        int countReview = 0;
        private bool _ShowWriteReviewLink = false;
        private string _ProductName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Sets or retrieves the product review collection list
        /// </summary>
        public int ReviewRating
        {
            set
            {
                this._ReviewRating = value;
                this.DisplayRating();
            }
        }

        #region Zeon Custom Properties

        /// <summary>
        /// Gets or sets a value indicating whether QuickWatch is enabled or not
        /// </summary>
        public bool IsQuickWatch
        {
            get
            {
                return this._IsQuickWatch;
            }

            set
            {
                this._IsQuickWatch = value;
            }
        }

        #endregion



        /// <summary>
        /// Gets or sets a value indicating whether to show Write Review Link or not
        /// </summary>
        public bool ShowWriteReviewLink
        {
            get
            {
                return this._ShowWriteReviewLink;
            }

            set
            {
                this._ShowWriteReviewLink = value;
            }
        }

        /// <summary>
        /// Sets the product total reviews
        /// </summary>
        public int TotalReviews
        {
            set
            {
                this._TotalReviews = value;
                this.DisplayRating();
            }
            get
            {
                return this._TotalReviews;
            }
        }

        /// <summary>
        /// Sets the product total reviews
        /// </summary>
        public string ProductName
        {
            get
            {
                return this._ProductName;
            }

            set
            {
                //if (string.IsNullOrEmpty(this._ProductName))
                //{
                //    _product = (ZNodeProduct)HttpContext.Current.Items["Product"];
                //}
                this._ProductName = value;
            }
        }

        /// <summary>
        /// Gets or sets the product view link with mode selected
        /// </summary>
        public string ViewProductLink
        {
            get
            {
                return this._ViewProductLink;
            }

            set
            {
                this._ViewProductLink = value;
                this.DisplayRating();
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Show star image and total no.of reviews. 
        /// </summary>
        protected void DisplayRating()
        {
            if (this.ViewProductLink.Contains("?zpid=") && !this.ViewProductLink.Contains("mode=review"))
            {
                this.ViewProductLink += "&mode=review";
            }
            else if (!this.ViewProductLink.Contains("mode=review"))
            {
                this.ViewProductLink += "?mode=review";
            }

            if (this._IsQuickWatch)
            {
                // Product Link
                Productpath1.HRef = "javascript:self.parent.location ='" + ResolveUrl(this.ViewProductLink) + "'";
            }
            else
            {
                // Product Link
                Productpath1.HRef = this.ViewProductLink;

                // Product Link
                //Productpath2.HRef = this.ViewProductLink;
            }

            //ImageRatingStar.Controls.Clear();
            Productpath1.Attributes.Add("Class", "star-" + this._ReviewRating);
            Productpath1.Title = "Star Rating for " + this.ProductName;

            //for (int i = 1; i <= 5; i++)
            //{
            //    if (this._ReviewRating >= i)
            //    {
            //        ImageRatingStar.Controls.Add(ParseControl(string.Format(_ImageFilePath, ResolveUrl(this._StarRatingImagePath), this.GetLocalResourceObject("FullRatingStarAltText").ToString() + " " + ProductName)));
            //        countReview++;
            //    }
            //    else
            //    {
            //        ImageRatingStar.Controls.Add(ParseControl(string.Format(_ImageFilePath, ResolveUrl(this._ShadeStarImageImagePath), this.GetLocalResourceObject("EmptyRatingStarAltText").ToString() + " " + ProductName)));
            //    }
            //}
            countReview = 0;
            //Zeon Custom Code:Bind Meta Tags Ends
            // TotalRating count 
            //lbltotalRating.Text = this._TotalReviews.ToString() + " " + this.GetLocalResourceObject("Review").ToString();
            //ReviewLink.Visible = this._ShowWriteReviewLink;
        }
        #endregion

    }
}
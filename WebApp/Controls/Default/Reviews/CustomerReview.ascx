<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="WebApp.Controls_Default_Reviews_CustomerReview" CodeBehind="CustomerReview.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<div class="ReviewPage">
    <div class="CustomerReview">
        <div class="PageTitle">
            <h1> <asp:Localize ID="Localize9" runat="server" meta:resourceKey="txtHead"></asp:Localize>
            <asp:Literal ID="ltrlProductName" runat="server" Text="" ClientIDMode="Static"></asp:Literal></h1>
        </div>
        <div class="Container">
            <div class="ReviewProductImage">
                <asp:Image ID="imgProduct" runat="server" />
                <asp:Literal ID="ltrProdDesc" runat="server"></asp:Literal>

            </div>
            <div class="ReviewDetails">
                <asp:Panel ID="pnlCustomerReview" runat="server" DefaultButton="btnSubmit">
                    <div class="Form">
                        <div class="Row">
                            <div class="FieldStyle ReviewLeftContent">
                                <label>
                                    <asp:Localize ID="Localize3" meta:resourceKey="txtReviewHeadLine" runat="server" /></label>
                            </div>
                            <div class="FieldStyle ReviewRightContent">
                                <asp:TextBox ID="Title" runat="server" MaxLength="50" Columns="65" 
                                    meta:resourcekey="TitleResource1" ClientIDMode="Static" title="Title" placeholder="Review Headline *"></asp:TextBox>
                                <div>
                                    <asp:RequiredFieldValidator ID="NameRequired" runat="server" ControlToValidate="Title" SetFocusOnError="true"
                                        CssClass="Error" Display="Dynamic"
                                        meta:resourcekey="NameRequiredResource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="Clear">
                            <ZNode:spacer ID="Spacer1" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
                        </div>
                        <div class="Row">
                            <div class="FieldStyle ReviewLeftContent">
                                <label>
                                    <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtSelectRating"></asp:Localize></label>
                            </div>
                            <div class="FieldStyle ReviewRightContent">
                                <asp:DropDownList ID="RatingList" runat="server" meta:resourcekey="RatingListResource1" ClientIDMode="Static" title="RatingList">
                                    <asp:ListItem Value="5" Selected="True" meta:resourcekey="ListItemResource1"></asp:ListItem>
                                    <asp:ListItem Value="4" meta:resourcekey="ListItemResource2"></asp:ListItem>
                                    <asp:ListItem Value="3" meta:resourcekey="ListItemResource3"></asp:ListItem>
                                    <asp:ListItem Value="2" meta:resourcekey="ListItemResource4"></asp:ListItem>
                                    <asp:ListItem Value="1" meta:resourcekey="ListItemResource5"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="Clear">
                            <ZNode:spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
                        </div>
                        <div class="Row" id="rowPros" runat="server">
                            <div class="FieldStyle ReviewLeftContent">
                                <label>
                                    <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtPros"></asp:Localize></label>
                            </div>
                            <div class="FieldStyle ReviewRightContent">
                                <asp:TextBox ID="Pros" ValidationGroup="GroupPros" runat="server"
                                    meta:resourcekey="ProsResource1" ClientIDMode="Static" title="Pros"></asp:TextBox>
                            </div>
                        </div>
                        <div class="Clear">
                            <ZNode:spacer ID="Spacer3" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
                        </div>
                        <div class="Row" id="rowCons" runat="server">
                            <div class="FieldStyle ReviewLeftContent">
                                <label>
                                    <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtCons"></asp:Localize></label>
                            </div>
                            <div class="FieldStyle ReviewRightContent">
                                <asp:TextBox ID="Cons" ValidationGroup="GroupCons" runat="server"
                                    meta:resourcekey="ConsResource1" ClientIDMode="Static" title="Cons" placeholder="Your Name or Nickname *"/>
                            </div>
                        </div>
                        <div class="Clear">
                            <ZNode:spacer ID="Spacer4" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
                        </div>
                        <div class="Row">
                            <div class="FieldStyle ReviewLeftContent">
                                <label>
                                    <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtYourReview"></asp:Localize></label>
                            </div>
                            <div class="FieldStyle ReviewRightContent">
                                <asp:TextBox ID="Comments" runat="server" TextMode="MultiLine"  Rows="5"
                                    meta:resourcekey="CommentsResource1" ClientIDMode="Static" title="Comments" placeholder="Your Review *"></asp:TextBox>
                                <div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Comments"
                                        CssClass="Error" Display="Dynamic" SetFocusOnError="True" meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="Clear">
                            <ZNode:spacer ID="Spacer5" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
                        </div>
                        <div class="Row">
                            <div class="FieldStyle ReviewLeftContent">
                                <label>
                                    <asp:Localize ID="Localize6" runat="server" meta:resourceKey="txtNickName"></asp:Localize></label>
                            </div>
                            <div class="FieldStyle ReviewRightContent">
                                <asp:TextBox ID="Name" runat="server" meta:resourcekey="NameResource1" ClientIDMode="Static"  title="Name" placeholder="Your Name or Nickname *"></asp:TextBox>
                                <div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="Name"
                                        CssClass="Error" Display="Dynamic"
                                        meta:resourcekey="RequiredFieldValidator4Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="Row">
                            <div class="FieldStyle ReviewLeftContent">
                                <label>
                                    <asp:Localize ID="locEmail" runat="server" meta:resourceKey="txtEmail"></asp:Localize></label>
                            </div>
                            <div class="FieldStyle ReviewRightContent">
                                <asp:TextBox ID="txtEmail" runat="server" ClientIDMode="Static" title="Email" placeholder="Your Email *"></asp:TextBox>
                                <div>
                                    <asp:RequiredFieldValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                        CssClass="Error" Display="Dynamic"
                                        meta:resourcekey="revEmail" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rgerevEmail" runat="server" ControlToValidate="txtEmail" SetFocusOnError="true"
                                        ValidationGroup="uxCreateUserWizard" CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired %>" ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired %>"
                                        ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression %>'
                                        Display="Dynamic">
                                    </asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>
                        <div class="Clear">
                            <ZNode:spacer ID="Spacer6" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
                        </div>
                        <div class="Row">
                            <div class="FieldStyle ReviewLeftContent">
                                <label>
                                    <asp:Localize ID="Localize7" runat="server" meta:resourceKey="txtYourLocation"></asp:Localize></label>
                            </div>
                            <div class="FieldStyle ReviewRightContent">
                                <asp:TextBox ID="Location" runat="server" MaxLength="100" 
                                    meta:resourcekey="LocationResource1" ClientIDMode="Static" title="Location" placeholder="Your Location *"></asp:TextBox>
                                <div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Location"
                                        CssClass="Error" Display="Dynamic"
                                        meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="Clear">
                            <ZNode:spacer ID="Spacer7" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
                        </div>
                        <div class="Row">
                            <div colspan="2" class="FailureText">
                                <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"
                                    meta:resourcekey="ErrorMessageResource1" ClientIDMode="Static"></asp:Literal>
                            </div>
                        </div>
                        <div class="Row">
                            <%--<div class="ReviewLeftContent">
                            &nbsp;
                        </div>--%>
                            <div class="ReviewRightContent">
                                <div class="Instruction">
                                    <asp:Localize ID="Localize8" runat="server" meta:resourceKey="txtDesc"></asp:Localize>
                                </div>
                            </div>
                        </div>
                        <div class="Clear">
                            <ZNode:spacer ID="Spacer8" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
                        </div>
                        <div class="Row">
                            <div class="FieldStyle ReviewLeftContent">
                            </div>
                            <div class="Buttons">
                                <asp:LinkButton ID="btnSubmit" runat="server" OnClick="BtnSubmit_Click" CssClass="Button"
                                    Text="<%$ Resources:CommonCaption, Submit %>" ClientIDMode="Static" />
                                <asp:LinkButton ID="btnCancel" runat="server" OnClick="BtnCancel_Click" CssClass="ClearButton"
                                    CausesValidation="False" Text="<%$ Resources:CommonCaption, Cancel %>" ClientIDMode="Static" TabIndex="8" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</div>

﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using Category = ZNode.Libraries.DataAccess.Entities.Category;
using System.Configuration;

namespace WebApp.Controls.Default.SearchEngine
{
    public partial class TopSearch : System.Web.UI.UserControl
    {

        #region Private Members

        private ZNodeCategory _category;
        private string zcid = string.Empty;
        private string searchTextValue = "Search by SKU# or Keyword";

        #endregion

        #region Page Events

        /// <summary>
        /// Page Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //btnSearch.ImageUrl = ResolveUrl("~/Themes/" + ZNodeCatalogManager.Theme + "/Images/search.png");

            //BindData();//znode old code

            BindSearchCategory(); //zeon custom code

            //SetCategory(Request.QueryString["category"]);

            //Populate the category and dropdown while category page load / product page load.
            PopulateCategory();
            /*if (ddlCategory.SelectedIndex == 0)
            {
                hdneditMode.Value = "true";
            }
            else
            {
                hdneditMode.Value = "false";
            }*/
            // }
            //PRFT Custom Codde ADA Tool Changes:Starts
            //if (Request.Path.IndexOf("default.aspx", StringComparison.OrdinalIgnoreCase)>=0)
            //{
                //SearchText.Focus();
            //}
            //PRFT Custom Codde ADA Tool Changes:Ends

        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        #endregion

        #region Events
        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
            /*
            var searchText = string.Empty;
            //var category = Request.Form[ddlCategory.UniqueID].ToLower();
            //Zeon Custom Code: Start
            var category = Request.Form[ddlCategory.UniqueID];
            if (!SearchText.Text.ToLower().Equals(searchTextValue.ToLower()))
            {
                searchText = Request.Form[SearchText.UniqueID];
            }
            Response.Redirect("searchengine.aspx?" + GetQueryString(category, searchText));
            */
            //Zeon Custom Code: End            
        }

        protected override void Render(HtmlTextWriter writer)
        {
            // GetAllCategories();
            base.Render(writer);
        }
        #endregion

        #region Private Methods
        /*
        private void SetCategory(string categoryString)
        {
            if (!string.IsNullOrEmpty(categoryString))
            {
                var category = ddlCategory.Items.FindByText(categoryString);
                if (category == null)
                {
                    //ddlCategory.Items.Insert(0, new ListItem(categoryString, categoryString));
                    //Zeon Custom Code: Start
                    if (ddlCategory.Items.Count > 0)
                    {
                        ddlCategory.SelectedIndex = 0;
                    }
                    else
                    {
                        BindSearchCategory();
                        ddlCategory.SelectedIndex = 0;
                    }
                    //Zeon Custom Code: End
                }
                //var index = ddlCategory.Items.IndexOf(new ListItem(categoryString, categoryString));
                //ddlCategory.SelectedIndex = index;

                // SearchText.Text = Request.QueryString["text"];
            }

        }
        */

        private string GetQueryString(string category, string text)
        {
            NameValueCollection queryString = HttpUtility.ParseQueryString(string.Empty);

            queryString["category"] = category;
            //queryString["text"] = text;//Znode Old Code
            queryString["text"] = HttpContext.Current.Server.UrlEncode(text.Trim());//Zeon Custom Code

            return queryString.ToString();

        }

        private void GetAllCategories()
        {
            /*
            CategoryHelper categoryHelper = new CategoryHelper();
            var categoryList = categoryHelper.GetCategoryByCatalogID(ZNodeCatalogManager.CatalogConfig.CatalogID);

            if (ddlCategory.Items.Count > 0)
            {
                for (int index = 0; index < categoryList.Tables[0].Rows.Count; index++)
                {
                    var categoryField = Server.HtmlDecode(categoryList.Tables[0].Rows[index]["Name"].ToString());

                    if (ddlCategory.Items.FindByValue(categoryField) == null)
                    {
                        Page.ClientScript.RegisterForEventValidation(ddlCategory.UniqueID, categoryField);
                    }
                }
            }
            */
        }

        private void BindData()
        {
            /*
            string cacheKey = string.Format("{0}_{1}_{2}_{3}", "SearchDropdownData", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, ZNodeProfile.CurrentUserProfileId);
            DataSet categoryList = HttpContext.Current.Cache[cacheKey] as DataSet;
            if (categoryList == null)
            {
                CategoryHelper categoryHelper = new CategoryHelper();
                categoryList = categoryHelper.GetRootCategoryItems(ZNodeCatalogManager.CatalogConfig.PortalID, ZNodeCatalogManager.CatalogConfig.CatalogID);

                for (int index = 0; index < categoryList.Tables[0].Rows.Count; index++)
                {
                    categoryList.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(categoryList.Tables[0].Rows[index]["Name"].ToString());
                }

                ZNodeCacheDependencyManager.Insert(cacheKey, categoryList, "ZNodeCategoryNode", "ZNodeCategory", "ZNodePortalCatalog");
            }
            ddlCategory.DataSource = categoryList;
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataValueField = "Name";
            ddlCategory.DataBind();
            ListItem li = new ListItem(Resources.CommonCaption.AllDepartmentsText, "");
            ddlCategory.Items.Insert(0, li);
            ddlCategory.SelectedIndex = 0;
            */
        }

        private void PopulateCategory()
        {
            //Populate the category dropdown while category page load.
            if (Request.QueryString["zcid"] != null)
            {
                this._category = (ZNodeCategory)HttpContext.Current.Items["category"];

                if (_category != null && !string.IsNullOrEmpty(_category.Name.ToString()))
                {
                    //SetCategory(this._category.Name);
                    //ddlCategory.SelectedIndex = ddlCategory.Items.IndexOf(ddlCategory.Items.FindByText(HttpUtility.HtmlDecode(new ZNodeNavigation().GetParentCategoryTitle(this._category.Title))));
                }
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["category"]))
            {
                //ddlCategory.SelectedIndex = ddlCategory.Items.IndexOf(ddlCategory.Items.FindByValue(HttpUtility.HtmlDecode(new ZNodeNavigation().GetParentCategoryName(HttpUtility.HtmlEncode(Request.QueryString["Category"])))));
            }

            //Populate the category dropdown while product page load.
            if (Request.QueryString["zpid"] != null)
            {
                if (Session["BreadCrumzcid"] != null)
                {
                    this.zcid = Session["BreadCrumzcid"].ToString();
                }
                else
                {
                    ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                    TList<ProductCategory> pcategories = service.GetByProductID(Convert.ToInt32(Request.QueryString["zpid"]));
                    if (pcategories.Count > 0)
                    {
                        this.zcid = pcategories[0].CategoryID.ToString();
                    }
                }

                if (!string.IsNullOrEmpty(this.zcid))
                {
                    ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
                    ZNode.Libraries.DataAccess.Entities.Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));
                    if (category != null)
                    {
                        //ddlCategory.SelectedIndex = ddlCategory.Items.IndexOf(ddlCategory.Items.FindByText(HttpUtility.HtmlDecode(new ZNodeNavigation().GetParentCategoryName(category.Name))));

                    }
                }
            }

        }
        #endregion

        #region Zeon Custom Mehtods

        /// <summary>
        /// Bind Search Category
        /// </summary>
        private void BindSearchCategory()
        {
            if (ConfigurationManager.AppSettings["SearchCategoryName"] != null)
            {
                string[] searchSpilt = ConfigurationManager.AppSettings["SearchCategoryName"].Split(',');
                if (searchSpilt != null && searchSpilt.Length > 0)
                {
                    foreach (string store in searchSpilt)
                    {
                        string[] portalSpilt = store.Split(':');
                        if (portalSpilt != null && portalSpilt.Length > 0)
                        {
                            if (ZNodeConfigManager.SiteConfig.PortalID.ToString() == portalSpilt[0])
                            {
                                ListItem liItem = new ListItem(portalSpilt[1], portalSpilt[1]);
                                //ddlCategory.Items.Insert(0, liItem);
                                //ddlCategory.SelectedIndex = 0;
                            }
                        }
                    }
                }
            }
        }
         
        /// <summary>
        /// Get Search Category
        /// </summary>
        /// <returns>string</returns>
        protected string GetSearchCategory()
        {
            string searchCategoryName = string.Empty;
            try
            {
                if (ConfigurationManager.AppSettings["SearchCategoryName"] != null)
                {
                    string[] searchSpilt = ConfigurationManager.AppSettings["SearchCategoryName"].Split(',');
                    if (searchSpilt != null && searchSpilt.Length > 0)
                    {
                        foreach (string store in searchSpilt)
                        {
                            string[] portalSpilt = store.Split(':');
                            if (portalSpilt != null && portalSpilt.Length > 0)
                            {
                                if (ZNodeConfigManager.SiteConfig.PortalID.ToString() == portalSpilt[0])
                                {
                                    searchCategoryName = portalSpilt[1];
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in GetSearchCategory!!" + ex.ToString());
            }
            return searchCategoryName;
        }
        
        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var zeonTopSearch = new TopSearch({");
            script.Append("\"Controls\":{");
            script.AppendFormat("\"{0}\":\"{1}\"", SearchText.ID, SearchText.ClientID);
            script.Append("},\"Messages\":{");
            script.AppendFormat("\"{0}\":\"{1}\"", "CategoryName", GetSearchCategory());
            script.Append("}");
            script.Append("});zeonTopSearch.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "zeonTopSearchScript", script.ToString(), true);
        }

        #endregion
    }
}
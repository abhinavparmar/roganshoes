﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApp.Controls.Default.SearchEngine {
    
    
    public partial class SearchList {
        
        /// <summary>
        /// ucPromoBannerRibbon control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WebApp.Controls_Default_CustomMessage_CustomMessage ucPromoBannerRibbon;
        
        /// <summary>
        /// Categories control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WebApp.Controls.Default.SearchEngine.LeftNavigation Categories;
        
        /// <summary>
        /// uxCategories control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WebApp.Controls_Default_Category_NavigationCategories uxCategories;
        
        /// <summary>
        /// ucSearchFacets control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WebApp.Controls.Default.SearchEngine.ZFacets ucSearchFacets;
        
        /// <summary>
        /// SearchProductList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::WebApp.Controls.Default.SearchEngine.ProductList SearchProductList;
    }
}

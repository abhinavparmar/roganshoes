﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftNavigation.ascx.cs"
    Inherits="WebApp.Controls.Default.SearchEngine.LeftNavigation" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>

<div class="CategoryTreeView" >
    <div class="Title" id="divCategoryTreeTitle" runat="server" onclick="zeonTopSearch.ToggalCategory();">
        <uc1:CustomMessage ID="CustomMessage2" MessageKey="LeftNavigationShopByCategoryTitle" runat="server" ClientIDMode="Static" />
    </div>
    <div class="SearchKeys">
        <asp:TreeView runat="server" ID="CategoryList" Font-Bold="true" ShowExpandCollapse="false" ClientIDMode="Static">
        </asp:TreeView>
    </div>
</div>


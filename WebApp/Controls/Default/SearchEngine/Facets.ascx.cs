﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Ecommerce.Entities;
using System.Data;
using System.Reflection;
using System.ComponentModel;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;
using System.Text;

namespace WebApp.Controls.Default.SearchEngine
{

    public partial class Facets : System.Web.UI.UserControl
    {
        IEnumerable<ZNodeFacet> facets;
        public delegate void UpdatePageDelegate(string facetquery);
        public event UpdatePageDelegate UpdatePageHandler;
        //const string querystringDefaultParams = "zcid,category,text,page,size,sort";
        const string querystringDefaultParams = "zcid,category,text,page,size,sort,v"; //Zeon Custom Code

        public IEnumerable<ZNodeFacet> Facetslist
        {
            get
            {
                return this.facets;
            }

            set
            {
                this.facets = value;
            }
        }

        public void BindFacets()
        {
            FacetRepeater.DataSource = facets;
            FacetRepeater.DataBind();
            FacetRepeater.Visible = facets.Any();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindSelectedItems();
            }
        }

        private string GetRemoveUrlForFacet(string facetName)
        {
            string query = string.Empty;
            foreach (string key in Request.QueryString.Keys)
            {
                if (key != facetName && !key.Contains("zcid"))
                {
                    Request.QueryString[key].Split(',').ToList().ForEach(x =>
                    {
                        query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
                    });
                }
            }

            if (query.Length > 0)
                query = query.Substring(1);


            string currentURL = Request.Path;
            if (currentURL.Contains("searchengine.aspx"))
            {
                return Request.Url.GetLeftPart(UriPartial.Path) + "?" + query;
            }
            else
            {
                string url = this.GetCurrentPageURL();
                return url + "?" + query;
            }
            //return Request.Url.GetLeftPart(UriPartial.Path) + "?" + query;


        }

        public string GetClearAllUrlForFacet()
        {
            string query = string.Empty;
            string[] facetName = querystringDefaultParams.Split(',');
            foreach (string key in Request.QueryString.Keys)
            {
                if (!key.Contains("zcid") && facetName.Contains(key))
                {
                    Request.QueryString[key].Split(',').ToList().ForEach(x =>
                    {
                        query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
                    });
                }
            }
            if (query.Length > 0)
                query = query.Substring(1);


            string currentURL = Request.Path;
            if (currentURL.Contains("searchengine.aspx"))
            {
                return Request.Url.GetLeftPart(UriPartial.Path) + "?" + query;
            }
            else
            {
                string url = this.GetCurrentPageURL();
                return url + "?" + query;
            }
            //return Request.Url.GetLeftPart(UriPartial.Path) + "?" + query;

        }

        protected void FacetRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ZNodeFacet facet = (ZNodeFacet)e.Item.DataItem;
                switch ((ZNodeFacetControlType)(facet.ControlTypeID))
                {
                    case ZNodeFacetControlType.LABEL:
                        Repeater links = e.Item.FindControl("FacetLinks") as Repeater;
                        links.DataSource = facet.AttributeValues;
                        links.DataBind();
                        break;
                }
            }
        }

        protected void FacetLinks_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "UpdateFacet")
            {
                string query = string.Empty;
                bool isKeyAdded = false;
                string[] qparams = querystringDefaultParams.Split(',');
                var currentQuery = e.CommandArgument.ToString().Split(':');
                var currentkey = currentQuery[0].Split('|')[0];

                foreach (string key in Request.QueryString.Keys)
                {
                    if (!qparams.Contains(key))
                        Request.QueryString[key].Split(',').ToList().ForEach(x =>
                        {
                            query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
                        });

                    if (key.Equals(currentkey, StringComparison.InvariantCultureIgnoreCase))
                    {
                        query += string.Format("&{0}={1}", currentkey, HttpUtility.UrlEncode(currentQuery[1]));
                        isKeyAdded = true;
                    }
                }

                if (isKeyAdded == false)
                {
                    query += string.Format("&{0}={1}", currentkey, HttpUtility.UrlEncode(currentQuery[1]));
                }


                if (UpdatePageHandler != null)
                    UpdatePageHandler(query);
            }
        }

        private void BindSelectedItems()
        {
            Dictionary<string, KeyValuePair<string, string>> selectedItems = new Dictionary<string, KeyValuePair<string, string>>();
            if (FacetRepeater.Visible)
            {

                foreach (RepeaterItem item in FacetRepeater.Items)
                {
                    var repeater = item.FindControl("FacetLinks") as Repeater;
                    foreach (RepeaterItem rptitem in repeater.Items)
                    {
                        var button = rptitem.FindControl("facetLink") as LinkButton;
                        var currentItem = button.CommandArgument.ToString().Split(':');
                        var key = currentItem[0].Split('|')[0];
                        if (Request.QueryString[key] != null)
                        {
                            if (button.Font.Bold = Request.QueryString[key].Split(',').Contains(currentItem[1]))
                            {
                                button.Visible = false;
                            }

                            if (!selectedItems.Keys.Contains(key))
                                selectedItems.Add(key, new KeyValuePair<string, string>(Request.QueryString[key], GetRemoveUrlForFacet(key)));
                        }
                    }
                    item.Visible = repeater.Items.Count > 0 && repeater.Items.Cast<RepeaterItem>().Any(x => (x.FindControl("facetLink") as LinkButton).Visible);
                }

                FacetRepeater.Visible = FacetRepeater.Items.Cast<RepeaterItem>().Any(x => x.Visible);
            }
            else
            {
                // show selected facets when no facets found from search engine result.      
                string[] qparams = querystringDefaultParams.Split(',');
                foreach (string key in Request.QueryString.Keys)
                {
                    if (!qparams.Contains(key))
                    {
                        selectedItems.Add(key, new KeyValuePair<string, string>(Request.QueryString[key], GetRemoveUrlForFacet(key)));
                    }
                }
            }

            FacetSelectedRepeater.DataSource = selectedItems;
            FacetSelectedRepeater.DataBind();
            FacetSelectedRepeater.Visible = selectedItems.Any();
        }

        #region Zeon Custom Methods

        /// <summary>
        /// SEO URL settings
        /// </summary>
        /// <returns></returns>
        private string IsSEOUrl()
        {
            string zcid = string.Empty;

            if (Request.QueryString["zcid"] != null)
            {
                Session["BreadCrumzcid"] = Request.QueryString["zcid"];
                zcid = Request.QueryString["zcid"].ToString();
            }
            else if (Session["BreadCrumzcid"] != null)
            {
                zcid = Session["BreadCrumzcid"].ToString();
            }
            else if (Request.QueryString["zpid"] != null)
            {
                ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                TList<ProductCategory> pcategories = service.GetByProductID(Convert.ToInt32(Request.QueryString["zpid"]));
                if (pcategories.Count > 0)
                {
                    zcid = pcategories[0].CategoryID.ToString();
                }
            }
            else
            {
                zcid = "0";
            }

            ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
            ZNode.Libraries.DataAccess.Entities.Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));
            if (category != null)
            {
                string seourl = string.Empty;
                if (category.SEOURL != null)
                {
                    seourl = category.SEOURL;
                }

                return ZNodeSEOUrl.MakeURL(category.CategoryID.ToString(), SEOUrlType.Category, seourl);
            }

            return ZNodeSEOUrl.MakeURL("0", SEOUrlType.Category, string.Empty);
        }
        
        /// <summary>
        /// Gets the Current Page URL
        /// </summary>
        /// <returns>Returns the Current Page URL</returns>
        private string GetCurrentPageURL()
        {
            return this.IsSEOUrl();
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var objFacets = new Facets({");
            script.Append("});objFacets.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "ScriptFacets", script.ToString(), true);
        }

        #endregion
    }
}
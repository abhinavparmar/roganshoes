﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="SearchList.ascx.cs"
    Inherits="WebApp.Controls.Default.SearchEngine.SearchList" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/Facets.ascx" TagName="FacetSearch" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/LeftNavigation.ascx" TagName="LeftNavigation" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/ProductList.ascx" TagName="ProductList" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/ZFacets.ascx" TagName="Facets" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Category/NavigationCategories.ascx" TagName="NavigationCategories" TagPrefix="ZNode" %>
<ZNode:CustomMessage ID="ucPromoBannerRibbon" runat="server" EnableViewState="false" MessageKey="FestivalPromoBannerRibbon" />
<div class="row">
    <div class="col-lg-12 SearchEngine">

        <div id="LeftColumn" class="SearchListing">
            <div id="overlaypopup"></div>
        
            <div class="MobilePopwarpper">
                <div class="PopUpTitle" style="display: none;">Narrow Result</div>
                <div id="popclose" style="display: none">
                    <button type="button" class="ui-dialog-titlebar-close" onclick="objFacets.ClosePopup()"></button>
                </div>
                <div class="MobilePoppup" id="FacetNavigation">
                    <div>
                        <ZNode:LeftNavigation ID="Categories" IsCategoryPage="false" runat="server"></ZNode:LeftNavigation>
                         <ZNode:NavigationCategories ID="uxCategories" runat="server" IsShowAllCategories="true" EnableNavigationViewState="false"></ZNode:NavigationCategories>
                    </div>
                </div>
               
               
                <%--<ZNode:FacetSearch ID="FacetSearch" runat="server" Title="Featured Products"></ZNode:FacetSearch>--%>
                <Zeon:Facets ID="ucSearchFacets" runat="server" Title="Featured Products" />
            </div>
            <div id="NewsLetter">
            </div>
        </div>
        <div id="MiddleColumn">
            <div>
                <ZNode:ProductList ID="SearchProductList" runat="server"></ZNode:ProductList>
            </div>
        </div>
    </div>
</div>


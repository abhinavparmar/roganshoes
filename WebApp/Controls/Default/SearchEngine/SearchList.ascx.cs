﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Ecommerce.Entities;
using ZNode.Libraries.Framework.Business;
using System.Configuration;

namespace WebApp.Controls.Default.SearchEngine
{
    public partial class SearchList : System.Web.UI.UserControl
    {
        #region private members
        private string searchText = string.Empty;
        private string category = string.Empty;
        private string refineBy = string.Empty;
        private string _view = string.Empty;
        private int sort = 0;
        const string querystringparams = "category,text,page,s,sort";
        //const string querystringparams = "category,text,page,s,sort,v"; // Zeon Custom Code
        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            category = Request.QueryString["category"];
            //Zeon Custom Code: Starts
            string customQueryStringParam = ConfigurationManager.AppSettings["SEOQuerystringKeys"] != null ? querystringparams + ConfigurationManager.AppSettings["SEOQuerystringKeys"].ToString() : querystringparams;
            _view = Request.QueryString["v"];
            //Zeon Custom Code: End
            if ((Request.QueryString["text"] != null))
            {
                searchText = HttpContext.Current.Server.UrlDecode(Request.QueryString["text"]).Trim();
            }

            sort = (!string.IsNullOrEmpty(Request.QueryString["sort"])) ? Convert.ToInt32(Request.QueryString["sort"]) : 0;


            List<KeyValuePair<string, IEnumerable<string>>> RefinedFacets = new List<KeyValuePair<string, IEnumerable<string>>>();

            foreach (var item in Request.QueryString.Keys)
            {
                if (item != null)
                {
                    string itemString = item.ToString();
                    if (!customQueryStringParam.ToLower().Split(',').Contains(itemString.ToLower()))
                    {
                        RefinedFacets.Add(new KeyValuePair<string, IEnumerable<string>>(itemString, Request.QueryString.GetValues(itemString)));
                    }
                }
            }

            //Znode Old Code:Starts
            //FacetSearch.UpdatePageHandler += UpdatePage;
            //BindData(category, searchText, RefinedFacets, sort);
            //Znode Old Code:Ends
            //Zeon Custom Code:Starts
            ucSearchFacets.UpdatePageHandler += UpdatePage;
            CustomBindData(category, searchText, RefinedFacets, sort);
            //Zeon Custom Code:Starts

            if (searchText != null && searchText.Length > 0 && !Page.IsPostBack)
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.KeywordSearch, searchText.ToLower());

        }

        private void BindData(string category, string searchText, List<KeyValuePair<string, IEnumerable<string>>> refineBy, int sort)
        {
            ZNodeSearchEngine search = new ZNodeSearchEngine();
            ZNodeSearchEngineResult result = search.Search(category, searchText, refineBy, sort);
            if (result != null)
            {
                if (result.Facets != null)
                {
                    ucSearchFacets.Facetslist = result.Facets;
                    ucSearchFacets.BindFacets();
                }
                if (result.CategoryNavigation != null)
                {
                    Categories.Visible = true;
                    Categories.CategoryNavigation = result.CategoryNavigation;
                    Categories.DataBind();
                }
                else
                {
                    Categories.Visible = false;
                }
                if (result.ProductIDs != null)
                {
                    SearchProductList.ProductID = result.ProductIDs;
                    SearchProductList.DataBind();
                }
            }
        }

        public void UpdatePage(string facetquery)
        {
            string baseUrl = this.GetNavigationUrl();

            // Do not add the & or ? in the following query string.
            //string navigateUrl = string.Format("{0}category={1}&text={2}&page={3}&size={4}&sort={5}&v={6}", baseUrl, HttpUtility.UrlEncode(category), HttpUtility.UrlEncode(searchText), SearchProductList.CurrentPage, SearchProductList.PageSize, SearchProductList.SortOption, _view);//List view grid view customization
            //string navigateUrl = string.Format("{0}category={1}&text={2}&page={3}&s={4}&sort={5}", baseUrl, HttpUtility.UrlEncode(category), HttpUtility.UrlEncode(searchText), SearchProductList.CurrentPage, SearchProductList.PageSize, SearchProductList.SortOption);//Old with updated paging
            int sort = (!string.IsNullOrEmpty(Request.QueryString["sort"])) ? Convert.ToInt32(Request.QueryString["sort"]) : 1;
            string navigateUrl = string.Format("{0}category={1}&text={2}&page={3}&s={4}&sort={5}", baseUrl, HttpUtility.UrlEncode(category), HttpContext.Current.Server.UrlEncode(searchText), 1, SearchProductList.PageSize, sort);//set 1 as default page
            Response.Redirect(navigateUrl + facetquery);
        }

        /// <summary>
        /// Gets the navigation url for the previous/next navigation link
        /// </summary>
        /// <returns>Returns the navigation base url.</returns>
        private string GetNavigationUrl()
        {
            string baseUrl = string.Empty;
            if (this.Request.QueryString.Count > 0)
            {
                StringBuilder url = new StringBuilder();
                url.Append(Request.Url.GetLeftPart(UriPartial.Authority));
                url.Append(Request.Url.AbsolutePath);
                url.Append("?");
                baseUrl = url.ToString();
            }
            else
            {
                baseUrl = Request.Url.ToString() + "?";
            }

            return baseUrl;
        }

        #region Zeon Custom Methods

        /// <summary>
        /// Bind Search data and store it in cache
        /// </summary>
        /// <param name="category">string</param>
        /// <param name="searchText">searchText</param>
        /// <param name="refineBy">refineBy</param>
        /// <param name="sort">sort</param>
        private void CustomBindData(string category, string searchText, List<KeyValuePair<string, IEnumerable<string>>> refineBy, int sort)
        {
            string modifiedSearchText = string.Empty;
            if (!string.IsNullOrEmpty(searchText))
            {
                if (searchText.IndexOf(' ') > 0)
                {
                    modifiedSearchText = "_" + searchText.Replace(" ", "_");
                }
                else
                {
                    modifiedSearchText = "_" + searchText;
                }
            }
            List<KeyValuePair<string, IEnumerable<string>>> emptyRefinedList = new List<KeyValuePair<string, IEnumerable<string>>>();
            var cacheKey = string.Format("{0}_{1}_{2}_{3}_{4}", "SearchListPage", category + modifiedSearchText, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, ZNodeProfile.CurrentUserProfileId);
            var result = HttpContext.Current.Cache[cacheKey] as ZNodeSearchEngineResult;
            if (result == null)
            {
                ZNodeSearchEngine search = new ZNodeSearchEngine();
                result = search.Search(category, searchText, emptyRefinedList, sort);
                ZNodeCacheDependencyManager.Insert(cacheKey, result, "ZNodeCategoryNode", "ZNodeCategory", "ZNodePortalCatalog", "ZNodeProductCategory", "ZNodeLuceneIndexServerStatus");
            }
            if (result != null)
            {
                if (result.Facets != null && result.Facets.Count > 0)
                {
                    ucSearchFacets.Facetslist = result.Facets;
                    //Search Implementation Code:Starts
                    ucSearchFacets.CurrentSelFacetProductList = refineBy != null && refineBy.Count > 0 ? GetCurrentFacetProductList(refineBy, result.Facets, result.ProductIDs) : result.ProductIDs;
                    ucSearchFacets.SelectedProductList = GetSelectedFacetsSearchProductList(refineBy, result.Facets);//Assign Selected Product List
                    //Search Implementation Code:Ends
                    ucSearchFacets.BindFacets();

                    Categories.Visible = false;

                }
                else if (result.CategoryNavigation != null)
                {
                    Categories.CategoryNavigation = result.CategoryNavigation;
                    Categories.DataBind();
                }
                if (result.ProductIDs != null)
                {
                    //SearchProductList.ProductID = refineBy.Count <= 0 ? result.ProductIDs : GetSelectedFacetsProductList(refineBy, result.Facets);
                    SearchProductList.ProductID = refineBy.Count <= 0 ? result.ProductIDs : GetSelectedFacetsSearchProductList(refineBy, result.Facets);//Display Selected Facets Product List;
                    SearchProductList.DataBind();
                }
            }
        }

        /// <summary>
        /// Get Refined facets Product List 
        /// </summary>
        /// <param name="selectedFacets">List<KeyValuePair<string, IEnumerable<string>>></param>
        /// <param name="facetList"> List<ZNodeFacet></param>
        /// <returns> List<int></returns>
        private List<int> GetSelectedFacetsProductList(List<KeyValuePair<string, IEnumerable<string>>> selectedFacets, List<ZNodeFacet> facetList)
        {
            List<int> selectedProducts = new List<int>();

            if (facetList != null && selectedFacets != null)
            {
                foreach (KeyValuePair<string, IEnumerable<string>> facet in selectedFacets)
                {
                    ZNodeFacet currentFacet = facetList.Find(x => x.AttributeName.Split('|')[0].ToLower().Equals(facet.Key.ToString().ToLower()));
                    if (currentFacet != null)
                    {
                        using (var refinedFacet = facet.Value.GetEnumerator())
                        {
                            while (refinedFacet.MoveNext())
                            {
                                if (refinedFacet.Current.ToString().IndexOf('^') > 0)
                                {
                                    string[] selFac = refinedFacet.Current.ToString().Split('^');

                                    foreach (string selAttr in selFac)
                                    {
                                        ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(selAttr));
                                        if (selectedAttribute != null && selAttr.Equals(selectedAttribute.AttributeValue))
                                        {
                                            selectedProducts.AddRange(selectedAttribute.ProductIDList);
                                        }
                                    }
                                }
                                else
                                {
                                    ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(refinedFacet.Current.ToString()));
                                    if (selectedAttribute != null && refinedFacet.Current.ToString().Equals(selectedAttribute.AttributeValue))
                                    {
                                        selectedProducts.AddRange(selectedAttribute.ProductIDList);
                                    }
                                }
                            }
                        }
                    }
                }

            }
            return selectedProducts.Distinct().ToList();
        }

        #region Search List Implementation (referd:http://www.onlineshoes.com/)

        /// <summary>
        /// Get Refined facets Product List 
        /// </summary>
        /// <param name="selectedFacets">List<KeyValuePair<string, IEnumerable<string>>></param>
        /// <param name="facetList"> List<ZNodeFacet></param>
        /// <returns> List<int></returns>
        private List<int> GetSelectedFacetsSearchProductList(List<KeyValuePair<string, IEnumerable<string>>> selectedFacets, List<ZNodeFacet> facetList)
        {
            List<int> selectedProductsList = new List<int>();
            if (facetList != null && selectedFacets != null)
            {
                List<int> currentSelectedFacetProducts = new List<int>();
                foreach (KeyValuePair<string, IEnumerable<string>> facet in selectedFacets)
                {
                    ZNodeFacet currentFacet = facetList.Find(x => x.AttributeName.Split('|')[0].ToLower().Equals(facet.Key.ToString().ToLower()));
                    if (currentFacet != null)
                    {
                        currentSelectedFacetProducts.Clear();
                        using (var refinedFacet = facet.Value.GetEnumerator())
                        {
                            while (refinedFacet.MoveNext())
                            {
                                if (refinedFacet.Current.ToString().IndexOf('^') > 0)
                                {
                                    string[] selFac = refinedFacet.Current.ToString().Split('^');

                                    foreach (string selAttr in selFac)
                                    {
                                        ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(selAttr));
                                        if (selectedAttribute != null && selAttr.Equals(selectedAttribute.AttributeValue))
                                        {
                                            currentSelectedFacetProducts.AddRange(selectedAttribute.ProductIDList);
                                        }
                                    }
                                }
                                else
                                {
                                    ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(refinedFacet.Current.ToString()));
                                    if (selectedAttribute != null && refinedFacet.Current.ToString().Equals(selectedAttribute.AttributeValue))
                                    {
                                        currentSelectedFacetProducts.AddRange(selectedAttribute.ProductIDList);
                                    }
                                }
                                //Add values in CurrentFacet list 
                                if (selectedProductsList.Count > 0)
                                {
                                    selectedProductsList = selectedProductsList.Intersect(currentSelectedFacetProducts).ToList();
                                }
                                else
                                {
                                    selectedProductsList.AddRange(currentSelectedFacetProducts);
                                }
                            }
                        }
                    }
                }

            }
            return selectedProductsList.Distinct().ToList();
        }

        /// <summary>
        /// Get Refined facets Product List to Manage Facet Count
        /// </summary>
        /// <param name="selectedFacets">List<KeyValuePair<string, IEnumerable<string>>></param>
        /// <param name="facetList"> List<ZNodeFacet></param>
        /// <returns> List<int></returns>
        private List<int> GetCurrentFacetProductList(List<KeyValuePair<string, IEnumerable<string>>> selectedFacets, List<ZNodeFacet> facetList, List<int> allProductList)
        {
            List<int> selectedProductsList = new List<int>();
            string currentFacetName = string.Empty, facetName = string.Empty, currentFacetValue = string.Empty;
            facetName = Session["CurrentUpdatedFacet"] != null ? GetCurrentUpdatedFacetFromSession() : GetCurrentFacetName(selectedFacets);
            currentFacetName = facetName.IndexOf(':') > 0 ? facetName.Split(':')[0] : facetName;
            currentFacetValue = facetName.IndexOf(':') > 0 ? facetName.Split(':')[1] : string.Empty;
            if (facetList != null && selectedFacets != null)
            {
                List<int> currentSelectedFacetProducts = new List<int>();
                foreach (KeyValuePair<string, IEnumerable<string>> facet in selectedFacets)
                {
                    if (!currentFacetName.ToLower().Equals(facet.Key.ToString().ToLower()) || !string.IsNullOrEmpty(currentFacetValue))
                    {
                        ZNodeFacet currentFacet = facetList.Find(x => x.AttributeName.Split('|')[0].ToLower().Equals(facet.Key.ToString().ToLower()));
                        if (currentFacet != null)
                        {
                            currentSelectedFacetProducts.Clear();
                            using (var refinedFacet = facet.Value.GetEnumerator())
                            {
                                while (refinedFacet.MoveNext())
                                {
                                    if (refinedFacet.Current.ToString().IndexOf('^') > 0)
                                    {
                                        string[] selFac = refinedFacet.Current.ToString().Split('^');

                                        foreach (string selAttr in selFac)
                                        {
                                            ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(selAttr));
                                            if (selectedAttribute != null && selAttr.Equals(selectedAttribute.AttributeValue))
                                            {
                                                if (currentFacetName.ToLower().Equals(facet.Key.ToString().ToLower()) && currentFacetValue.Split(',').Contains(selectedAttribute.AttributeValue) || (selectedAttribute.AttributeValue.Equals(currentFacetValue)))
                                                {
                                                    continue;
                                                }
                                                else
                                                {
                                                    currentSelectedFacetProducts.AddRange(selectedAttribute.ProductIDList);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ZNodeFacetValue selectedAttribute = currentFacet.AttributeValues.Find(y => y.AttributeValue.Equals(refinedFacet.Current.ToString()));
                                        if (selectedAttribute != null && refinedFacet.Current.ToString().Equals(selectedAttribute.AttributeValue))
                                        {
                                            if (currentFacetName.ToLower().Equals(facet.Key.ToString().ToLower()) && currentFacetValue.Split(',').Contains(selectedAttribute.AttributeValue) || (selectedAttribute.AttributeValue.Equals(currentFacetValue)))
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                currentSelectedFacetProducts.AddRange(selectedAttribute.ProductIDList);
                                            }
                                        }
                                    }
                                    //Add CurrentFacet list 
                                    if (selectedProductsList.Count > 0)
                                    {
                                        if (currentSelectedFacetProducts.Count > 0)
                                        {
                                            selectedProductsList = selectedProductsList.Intersect(currentSelectedFacetProducts).ToList();
                                        }
                                    }
                                    else
                                    {
                                        selectedProductsList.AddRange(currentSelectedFacetProducts);
                                    }
                                }
                            }
                        }

                    }
                }
            }
            if (selectedProductsList.Count <= 0) { selectedProductsList = allProductList; }
            return selectedProductsList.Distinct().ToList();
        }

        /// <summary>
        /// Get Current Selected Facet Name
        /// </summary>
        /// <returns>string</returns>
        private string GetCurrentFacetName(List<KeyValuePair<string, IEnumerable<string>>> selectedFacets)
        {
            string currentFacetSelected = string.Empty;
            if (selectedFacets != null && selectedFacets.Count > 0)
            {
                for (int facetIndex = selectedFacets.Count - 1; facetIndex >= 0; facetIndex--)
                {
                    if (selectedFacets.Count > 0)
                    {
                        currentFacetSelected = selectedFacets[facetIndex].Key + ":";
                        using (var refinedFacet = selectedFacets[facetIndex].Value.GetEnumerator())
                        {
                            while (refinedFacet.MoveNext())
                            {
                                if (refinedFacet.Current.ToString().IndexOf('^') > 0)
                                {
                                    string[] selFac = refinedFacet.Current.ToString().Split('^');
                                    foreach (string attrVal in selFac)
                                    {
                                        currentFacetSelected = currentFacetSelected + attrVal + ",";
                                    }
                                    currentFacetSelected.TrimEnd(',');
                                }
                                else
                                {
                                    currentFacetSelected = currentFacetSelected + refinedFacet.Current;
                                    break;
                                }
                            }
                        }
                        if (currentFacetSelected.IndexOf(':') > 0) { break; }
                    }
                }
            }
            return currentFacetSelected;
        }

        /// <summary>
        /// Get Current Selected Facet Name from Session
        /// </summary>
        /// <returns>string</returns>
        private string GetCurrentUpdatedFacetFromSession()
        {
            string currentFacetSelected = string.Empty;
            if (Session["CurrentUpdatedFacet"] != null)
            {
                currentFacetSelected = Session["CurrentUpdatedFacet"].ToString();
            }
            return currentFacetSelected;
        }


        #endregion
        #endregion

    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductList.ascx.cs" Inherits="WebApp.Controls.Default.SearchEngine.ProductList" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/SwatchImages.ascx" TagName="Swatches"
    TagPrefix="ZNode" %>
<div class="ResultTitle">
    <asp:Localize ID="txtSearchResultTitle" EnableViewState="false" meta:resourceKey="txtSearchResultTitle" runat="server" ClientIDMode="Static" />
</div>
<div class="SearchError">
    <asp:Label ID="ErrorMsg" EnableViewState="false" runat="server" CssClass="Error" meta:resourcekey="ErrorMsgResource1" ClientIDMode="Static"></asp:Label>
</div>
<div class="SearchFound Error" id="divSearchResult" runat="server" enableviewstate="false">
    <div class="uxMsg ErrorSection">
        <em>
            <asp:Label ID="SearchMessage" EnableViewState="false" runat="server" ClientIDMode="Static"></asp:Label></em>
    </div>
</div>
<asp:Panel ID="pnlProductList" runat="server" Visible="True" meta:resourcekey="pnlProductListResource1" ClientIDMode="Static">
    <div class="CategoryDetail">
        <div class="ProductListNavigation">
            <div class="TopPagingSection">
                <div class="Sorting">
                    <span class="PageResult"><strong>
                        <asp:Literal ID="ltrTopTotalCounts" EnableViewState="false" meta:resourcekey="ltrTotalCountresource" runat="server"></asp:Literal></strong>
                        &nbsp;&nbsp;<asp:Literal ID="ltrTopTotalResults" EnableViewState="false" meta:resourcekey="ltrTotalResultsresource" runat="server"></asp:Literal>
                    </span>&nbsp;&nbsp;
                    <div class="filter" id="NarrowResult"><a href="javascript:void(0)" onclick="objFacets.ShowFacets()">Narrow Results</a></div>
                    <div class="PageSort">
                        <span class="Label">
                            <asp:Localize ID="Localize5" runat="server" EnableViewState="false" Text="<%$ Resources:CommonCaption, Sort%>" ClientIDMode="Static"></asp:Localize>
                        </span>
                        <asp:DropDownList ID="lstFilter" runat="server" OnSelectedIndexChanged="LstFilter_SelectedIndexChanged" AutoPostBack="false" ClientIDMode="Static" onchange="zeonCatProdList.ApplySortByPageNarrowResult(this);">
                            <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Relevance</asp:ListItem>
                            <asp:ListItem Value="4" meta:resourcekey="ListItemResource4">Product A-Z</asp:ListItem>
                            <asp:ListItem Value="5" meta:resourcekey="ListItemResource5">Product Z-A</asp:ListItem>
                            <asp:ListItem Value="6" meta:resourcekey="ListItemResource6">Highest Rated</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="PageShow">
                        <span class="label">
                            <asp:Literal ID="ltrTopShow" EnableViewState="false" runat="server" meta:resourceKey="ltrShowresource"></asp:Literal>
                        </span>
                        <asp:DropDownList ID="ddlTopPaging" runat="server" AutoPostBack="false" CssClass="Pagingdropdown" onchange="zeonCatProdList.ApplyShowNarrowResult(this);"
                            meta:resourcekey="ddlTopPagingResource1" OnSelectedIndexChanged="DdlTopPaging_SelectedIndexChanged" ClientIDMode="Static">
                        </asp:DropDownList>
                    </div>
                    <div class="PageNavArrow">
                        <span class="label">
                            <asp:Literal runat="server" EnableViewState="false" ID="ltrPage" meta:resourceKey="ltrPageresource"></asp:Literal></span>
                        <asp:HyperLink ID="hlTopPrevLink" CssClass="Button" EnableViewState="false" meta:resourcekey="hlkPrviLink" runat="server" ClientIDMode="Static"></asp:HyperLink>
                        <span>
                            <asp:Localize ID="locTopPageNum" runat="server" ClientIDMode="Static">
                            </asp:Localize>&nbsp;&nbsp;
                        </span>
                    </div>
                    <asp:HyperLink ID="hlTopNextLink" EnableViewState="false" CssClass="Button" meta:resourcekey="hlkNextiLink" runat="server" ClientIDMode="Static"></asp:HyperLink>
                    <span>
                        <asp:Localize ID="locTopPageTotal" EnableViewState="false" runat="server" ClientIDMode="Static">
                        </asp:Localize></span>

                    <div class="TopPaging">
                        <div style="float: left;">
                            <asp:LinkButton ID="imgbtnGrid" CssClass="ActiveLink GridView" runat="server" Visible="False" Enabled="False" ToolTip="Grid" OnClick="imgbtnGrid_OnClick" ClientIDMode="Static"></asp:LinkButton>
                            <asp:ImageButton ID="imgGrid1" runat="server" AlternateText="Grid" CssClass="ActiveLink" ImageUrl="~/Themes/Default/Images/grid-active.gif" ToolTip="Grid" OnClick="imgbtnGrid_OnClick" ClientIDMode="Static" Visible="false" />
                            &nbsp;&nbsp;<asp:LinkButton ID="imgbtnList" CssClass="DeActiveLink ListView" runat="server" Visible="False" ToolTip="List" OnClick="imgbtnList_OnClick" ClientIDMode="Static"></asp:LinkButton>
                            <asp:ImageButton ID="imgListActive1" AlternateText="List" ToolTip="List" ImageUrl="~/Themes/Default/Images/list.gif" runat="server" CssClass="DeActiveLink" OnClick="imgbtnList_OnClick" ClientIDMode="Static" Visible="false" />
                        </div>
                    </div>
                </div>
            </div>
            <%--<div class="HorizontalLine"></div>--%>
            <div>
                <div id="ProductListItem">
                    <div>
                        <div class="CategoryProductlist">
                            <asp:DataList ID="DataListProducts" RepeatLayout="Flow" runat="server" RepeatDirection="Horizontal"
                                RepeatColumns="4" meta:resourcekey="DataListProductsResource1"
                                >
                                <ItemStyle CssClass="ItemStyle" VerticalAlign="Top" />
                                <ItemTemplate>
                                    <div class="ProductListItem">
                                        <%--<asp:HiddenField ID="hdnPID" EnableViewState="false" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>' ClientIDMode="Static"></asp:HiddenField>--%>
                                        <div class="Image">
                                            <div class="ItemType">
                                                <%--<asp:Image ID="NewItemImage" EnableViewState="false" runat="server" ImageUrl='<%#ResolveUrl(ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png")) %>'
                                                    meta:resourcekey="NewItemImageResource1" Visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>' ClientIDMode="Static" AlternateText="New" alt="New" />--%>
                                                <span class="rs-new" id="spnRSNew" runat="server" visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>'><span class="rs-new-text">New</span></span>
                                                &nbsp;<asp:Image EnableViewState="false" ID="FeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                                    meta:resourcekey="FeaturedItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' ClientIDMode="Static" AlternateText="Featured" ToolTip="Featured" />
                                            </div>
                                            <a id="imgProduct" class="boxed" href='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'>
                                                <img id='<%# "Img" + DataBinder.Eval(Container.DataItem, "ProductID")%>' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                                    border="0" src="images/grey1px.gif" title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                                    style="vertical-align: bottom" class="lazy"
                                                    data-original='<%# ResolveUrl(((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).SmallImageFilePath) %>'></a>
                                            <div class="AddViewProduct">
                                                <div class="HoverBox">
                                                </div>
                                            </div>
                                            <div class="QuickViewLink">
                                                <div class="SubCompare">
                                                    <%--<a href="javascript:void(0)" onclick="return quickViewHelper.SetSrcInframe(<%# DataBinder.Eval(Container.DataItem, "ProductID").ToString() %>)">Quick View</a>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ProductSwatches">
                                            <div class="ColorCaption">
                                                <a href='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' id="A1" class="ColorPicker" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ProductSKUCount")) > 1 %>'>MORE COLORS AVAILABLE</a>
                                            </div>
                                        </div>
                                        <div class="StarRating">
                                            <ZNode:ProductAverageRating ID="ProductAverageRating1" ProductName='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' 
                                                TotalReviews='<%# DataBinder.Eval(Container.DataItem, "TotalReviews") %>'
                                                ViewProductLink='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink")%>'
                                                ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ReviewRating")%>' runat="server" />
                                        </div>
                                        <div class="DetailLink">
                                            <asp:HyperLink ID="HyperLink2" ToolTip='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink") %>' >  </asp:HyperLink>
                                        </div>
                                        <div class="Price">
                                            <asp:Label ID="Label2" EnableViewState="false" runat="server" meta:resourcekey="Label2Resource1"
                                                Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>'
                                                Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && ProductListProfile.ShowPrice %>'></asp:Label>
                                            <%--<asp:Label ID="lblERPPrice" runat="server" EnableViewState="false" Text="Loading.."
                                                Visible='<%# !(bool)((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).CallForPricing && ProductListProfile.ShowPrice %>'></asp:Label>--%>
                                        </div>
                                        <div class="CallForPrice">
                                            <asp:Label ID="uxCallForPricing" runat="server" CssClass="Price" meta:resourcekey="uxCallForPricingResource1"
                                                Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>' ></asp:Label>
                                        </div>

                                        <div class="ItemNumber" style="display: none">
                                            <asp:Label ID="lblItemHash" runat="server" Text='<%# ((ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)Container.DataItem).ProductID %>'
                                                EnableViewState="false" Style="font-weight: normal" />
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div class="space"></div>
                    </div>
                    <div class="BottomPaging">
                        <div class="Paging">
                            <span class="Sorting"><strong>
                                <asp:Literal ID="ltrBottomTotalCount" meta:resourcekey="ltrTotalCountresource" EnableViewState="false" ClientIDMode="Static" runat="server"></asp:Literal></strong>
                                &nbsp;&nbsp;<asp:Literal ID="ltrBottomTotalResult" EnableViewState="false" ClientIDMode="Static" meta:resourcekey="ltrTotalResultsresource" runat="server"></asp:Literal>
                            </span>
                            <div class="PageSort">
                                <span class="Label">
                                    <asp:Localize ID="Localize6" EnableViewState="false" runat="server" Text="<%$ Resources:CommonCaption, Sort%>" ClientIDMode="Static"></asp:Localize>
                                </span>
                                <asp:DropDownList ID="LstFilterBottom" runat="server" OnSelectedIndexChanged="LstFilterBottom_SelectedIndexChanged"
                                    AutoPostBack="false" ClientIDMode="Static" onchange="zeonCatProdList.ApplySortByPageNarrowResult(this);">
                                    <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Relevance</asp:ListItem>
                                    <asp:ListItem Value="4" meta:resourcekey="ListItemResource4">Product A-Z</asp:ListItem>
                                    <asp:ListItem Value="5" meta:resourcekey="ListItemResource5">Product Z-A</asp:ListItem>
                                    <asp:ListItem Value="6" meta:resourcekey="ListItemResource6">Highest Rated</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="PageShow">
                                <span class="label">
                                    <asp:Literal ID="ltrBottomShow" EnableViewState="false" meta:resourceKey="ltrShowresource" runat="server"></asp:Literal></span>
                                <asp:DropDownList runat="server" ID="ddlBottomPaging" CssClass="Pagingdropdown" AutoPostBack="false" onchange="zeonCatProdList.ApplyShowNarrowResult(this);"
                                    OnSelectedIndexChanged="DdlBottomPaging_SelectedIndexChanged" meta:resourcekey="ddlBottomPagingResource1" ClientIDMode="Static">
                                </asp:DropDownList>

                            </div>
                            <div class="PageNavArrow">
                                <span class="label">
                                    <asp:Literal runat="server" ID="ltrbottomPage" EnableViewState="false" meta:resourceKey="ltrPageresource"></asp:Literal>&nbsp;&nbsp;</span>
                                <asp:HyperLink ID="hlBotPrevLink" CssClass="Button" meta:resourcekey="hlkPrviLink" runat="server" EnableViewState="false" ClientIDMode="Static"></asp:HyperLink>
                                <span>
                                    <asp:Localize ID="locBottomPageNum" EnableViewState="false" runat="server" ClientIDMode="Static"></asp:Localize>&nbsp;&nbsp;
                                  
                                </span>
                            </div>
                            <asp:HyperLink ID="hlBotNextLink" EnableViewState="false" meta:resourcekey="hlkNextiLink" CssClass="Button" runat="server" ClientIDMode="Static"></asp:HyperLink>
                            <span>
                                <asp:Localize ID="locBottomPageTotal" EnableViewState="false" runat="server" ClientIDMode="Static"></asp:Localize></span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
<div id="divQuickWatch" style="display: none;">
    <iframe id="ifzQuickView" title="quickview" runat="server" style="border-style: none; border: 0px; background-color: transparent; width: 100%; min-height: 100%; overflow: hidden;"
        ></iframe>
</div>

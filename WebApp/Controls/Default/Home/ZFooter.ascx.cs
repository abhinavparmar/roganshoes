﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SquishIt.Framework;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.Home
{
    public partial class ZFooter : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.CustomerServiceEmail))
            {
                footerEmailContact.HRef = "mailto:" + ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
                ltrEmailConatct.Text = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
                //lnkTelPhone.HRef = "tel:+" + ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {

            string sPagePath = Request.Url.AbsolutePath;
            FileInfo oFileInfo = new FileInfo(sPagePath);
            string sPageName = oFileInfo.Name;

            string includejsfiles = string.Empty;

            string commonBundle = Bundle.JavaScript()
                .Add("~/js/jquery-1.9.0.min.js")
                //.Add("~/js/jquery-ui-1.10.0.custom.min.js")
                .Add("~/js/jquery-ui-1.10.4.js")
                .Add("~/js/jquery-migrate-1.0.0.min.js")
                .Add("~/js/jquery.selectbox-0.2.min.js")
                .Add("~/js/jquery.znode.js")
                .Add("~/bootstrap/js/bootstrap.min.js")
                .Add("~/js/TopSearch.js")
                .Add("~/js/jquery.flexisel.js")
                .Add("~/js/zeonslideproducts.js")
                .Add("~/js/MiniCartFlyOut.js")
                .Add("~/js/menu.js")

                .Render("~/squished/js/CommonSquished_#.js?v=" + ConfigurationManager.AppSettings["jsversion"].ToString());

            string pageSpecificBundle = string.Empty;

            if (sPageName.ToLower().Contains("default"))
            {
                pageSpecificBundle = Bundle.JavaScript()
                    .Add("~/js/homemaster.js")
                    .Add("~/js/LazyLoad.js")
                    .Render("~/squished/js/HomePageSpecificSquished_#.js?v=" + ConfigurationManager.AppSettings["jsversion"].ToString());
            }

            if (sPageName.ToLower().Contains("category") || sPageName.ToLower().Contains("searchengine"))
            {
                pageSpecificBundle = Bundle.JavaScript()
                    .Add("~/js/treemenu.js")
                    .Add("~/js/facets.js")
                    .Add("~/js/categoryproductlist.js")
                    .Add("~/js/zquickviewHelper.js")
                    .Add("~/js/LazyLoad.js")
                    .Render("~/squished/js/CategoryPageSpecificSquished_#.js?v=" + ConfigurationManager.AppSettings["jsversion"].ToString());
            }

            if (sPageName.ToLower().Contains("product"))
            {
                //pageSpecificBundle = Bundle.JavaScript().Add("~/js/multizoom.js").Render("~/squished/js/ProductPageSpecificSquished_#.js?v=" + ConfigurationManager.AppSettings["jsversion"].ToString());
                pageSpecificBundle = pageSpecificBundle + Bundle.JavaScript()
                    .Add("~/js/product.js")
                    .Add("~/js/ZAttributeHelper.js")
                    .Add("~/js/JQueryZoom/jquery.elevatezoom.min.js")
                    .Add("~/js/LazyLoad.js")
                    .Render("~/squished/js/ProductPageSpecificSquished_#.js?v=" + ConfigurationManager.AppSettings["jsversion"].ToString());
            }

            if (sPageName.ToLower().Contains("checkout"))
            {
                pageSpecificBundle = Bundle.JavaScript()
                     .Add("~/js/Payment.js") 
                    .Add("~/js/checkout.js") 
                    .Render("~/squished/js/CheckoutPageSpecificSquished_#.js?v=" + ConfigurationManager.AppSettings["jsversion"].ToString());
            }

            if (sPageName.ToLower().Contains("allbrands") || sPageName.ToLower().Contains("shopbybrand"))
            {
                pageSpecificBundle = Bundle.JavaScript()
                    .Add("~/js/allbrands.js")
                    .Render("~/squished/js/AllBrandsPageSpecificSquished_#.js?v=" + ConfigurationManager.AppSettings["jsversion"].ToString());
            }

            if (sPageName.ToLower().Contains("productcomparison"))
            {
                pageSpecificBundle = Bundle.JavaScript()
                    .Add("~/js/productcomparison.js")
                    .Render("~/squished/js/ProductComparisonPageSpecificSquished_#.js?v=" + ConfigurationManager.AppSettings["jsversion"].ToString());
            }

            if (sPageName.ToLower().Contains("account"))
            {
                pageSpecificBundle = Bundle.JavaScript()
                    .Add("~/js/treemenu.js")
                    .Add("~/js/account.js")
                    .Render("~/squished/js/AccountPageSpecificSquished_#.js?v=" + ConfigurationManager.AppSettings["jsversion"].ToString());
            }

            if (sPageName.ToLower().Contains("affiliatesignup"))
            {
                pageSpecificBundle = Bundle.JavaScript()
                    .Add("~/js/affiliatesignup.js")
                    .Render("~/squished/js/AffiliateSignupPageSpecificSquished_#.js?v=" + ConfigurationManager.AppSettings["jsversion"].ToString());
            }
            if (sPageName.ToLower().Split('.')[0].Equals("brand"))
            {
                pageSpecificBundle = Bundle.JavaScript()
                .Add("~/js/zquickviewHelper.js")
                 .Render("~/squished/js/BrandPageSpecificSquished_#.js?v=" + ConfigurationManager.AppSettings["jsversion"].ToString());
            }

            includejsfiles = commonBundle + pageSpecificBundle;

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "AllScripts", includejsfiles, false);


        }
    }
}
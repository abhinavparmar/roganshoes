using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.DataAccess.Entities;
using System.Text;

namespace WebApp
{
    public partial class Controls_Default_Home_Home : System.Web.UI.UserControl
    {
        #region Page Load
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Set HTML home page content
                lblHtml.Text = ZNodeContentManager.GetPageHTMLByName("home", ZNodeConfigManager.SiteConfig.PortalID);               
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // This is script for New arrival control. This is placed here to adding output cache to new arrival control. Output cache was caching script and was rendering it on improper location.
            StringBuilder script = new StringBuilder();

            script.Append("var newArr = new ZeonSlideProducts({'sliderID':'divNewArrival','MaxItemCount':'5','MobPortraitItemCount':'3','MobLandItemCount':'3','TabletItemCount':'4','MinimumItemsShown':'2'});");
            script.Append("newArr.Init();");

            this.Page.ClientScript.RegisterStartupScript(GetType(), "Slider", script.ToString(), true);
        }
        #endregion
       
    }
}
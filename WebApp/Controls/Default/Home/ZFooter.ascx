﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZFooter.ascx.cs" Inherits="WebApp.Controls.Default.Home.ZFooter" %>
<%@ Register Src="~/Controls/Default/EmailFriend/SignupNewsletter.ascx" TagName="NewsletterSignUp" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Common/CustomerServicePhone.ascx" TagName="CustomerServicePhone" TagPrefix="ZNode" %>

<div id="FooterContent">
    <div class="Newsletter hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="NewsLetterSignUp">
                        <ZNode:NewsletterSignUp ID="uxNewsletterSignUp" runat="server"></ZNode:NewsletterSignUp>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer id="Footer">
    <div class="FooterTopSection">
        <div class="container">

            <div class="row FooterSection">
                <div class="FooterMenu sec1">
                    <uc1:CustomMessage ID="ucFooterColumn1Message" runat="server" MessageKey="FooterColumn1" EnableViewState="false"></uc1:CustomMessage>
                </div>
                <div class="FooterMenu sec2">
                    <uc1:CustomMessage ID="ucFooterColumn2Message" runat="server" MessageKey="FooterColumn2" EnableViewState="false"></uc1:CustomMessage>
                </div>
                <div class="FooterMenu sec3">
                    <uc1:CustomMessage ID="ucFooterColumn3Message" runat="server" MessageKey="FooterColumn3" EnableViewState="false"></uc1:CustomMessage>
                </div>
                <div class="FooterMenu sec4">
                    <uc1:CustomMessage ID="ucFooterColumn4Message" runat="server" MessageKey="FooterColumn4" EnableViewState="false"></uc1:CustomMessage>

                </div>
                <div class="FooterMenu sec5">
                    <uc1:CustomMessage ID="ucFooterColumn5Message" runat="server" MessageKey="FooterColumn5" EnableViewState="false"></uc1:CustomMessage>

                </div>
                <div class="Copyright">
                    <uc1:CustomMessage ID="CustomMessage2" runat="server" MessageKey="FooterCopyrightText" EnableViewState="false"></uc1:CustomMessage>
                </div>
            </div>
        </div>
    </div>
    <div class="FooterBottomSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12 BottomSection">
                    <div class="SocialMedia"> 
                        <uc1:CustomMessage ID="ucShareUsCustomMessage" runat="server" MessageKey="FooterShareUsText" EnableViewState="false"></uc1:CustomMessage>
                    </div>
                    <div class="Email">
                        <a ID="footerEmailContact" runat="server" enableviewstate="false" title="Email"><asp:Literal ID="ltrEmailConatct" EnableViewState="false" runat="server"></asp:Literal></a> 
                    </div>
                    <div class="Phone">
                        <ZNode:CustomerServicePhone ID="uxCustomerServicePhone" EnableViewState="false" runat="server"/>
                        </div>
                </div>
            </div>
            <div class="row">
                <div class="CopyRight"> <uc1:CustomMessage ID="uxCopyRight" runat="server" MessageKey="FooterCopyrightText" EnableViewState="false"></uc1:CustomMessage></div>
            </div>
           <%-- <div class="row">
                <div class="BottomMenu">
                    <ul>
                        <li><a href="productcomparison.aspx"><span class="fa fa-refresh"></span><em><asp:Literal ID="ltrCmparison"  meta:resourcekey="ltrCmparisonResource1" runat="server" EnableViewState="false"></asp:Literal></em></a></li>
                        <li><a href="account.aspx"><span class="fa fa-heart"></span><em><asp:Literal ID="ltrWishList"  meta:resourcekey="ltrWishListResource1" runat="server" EnableViewState="false"></asp:Literal> </em></a></li>
                        <li><a href="storelocator.aspx"><span class="fa fa-map-marker"></span><em><asp:Literal ID="ltrLocation"  meta:resourcekey="ltrLocationResource1" runat="server" EnableViewState="false"></asp:Literal></em></a></li>
                        <li><a href="account.aspx"><span class="fa fa-lock"></span><em><asp:Literal ID="ltrAccount"   meta:resourcekey="ltrAccountResource1" runat="server" EnableViewState="false"></asp:Literal></em></a></li>
                        <li><a href="tel:+1-800-976-4267" id="lnkTelPhone" runat="server"><span class="fa fa-comments"></span><em><asp:Literal ID="ltrCallUs"  meta:resourcekey="ltrCallUsResource1" runat="server" EnableViewState="false"></asp:Literal></em></a></li>
                    </ul>
                </div>
            </div>--%>
        </div>
    </div>
</footer>

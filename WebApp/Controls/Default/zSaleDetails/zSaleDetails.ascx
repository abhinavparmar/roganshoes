﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="zSaleDetails.ascx.cs" Inherits="WebApp.Controls.Default.zSaleDetails.zSaleDetails" EnableViewState="false" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>
<div class="ContentContainer">

    <div class="Static Content">
        <ZNode:CustomMessage ID="uxCustomMessage" runat="server" MessageKey="SaleDetailHeader" EnableViewState="false"/>
    </div>
    <div class="Clear"></div>
    <div class="AllBrandsContainer">
        <div id="dvAllBrandsList">
            <asp:Literal ID="ltlAllBrands" runat="server" EnableViewState="false"></asp:Literal>
        </div>
    </div>
    <br />
    <div class="SaleDetailFooter">
        <ZNode:CustomMessage ID="CustomMessage1" runat="server" MessageKey="SaleDetaiFooter" EnableViewState="false" />
    </div>
</div>

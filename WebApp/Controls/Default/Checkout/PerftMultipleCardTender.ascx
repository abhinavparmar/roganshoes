﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PerftMultipleCardTender.ascx.cs" Inherits="WebApp.Controls.Default.Checkout.PerficientMoreCreditCard" %>
<%@ Register TagPrefix="ZNode" Assembly="ZNode.Libraries.ECommerce.Catalog" Namespace="ZNode.Libraries.ECommerce.Catalog" %>

<div id="<%=GetDynamicID()%>" class="MoreCards" style="display: none;">
    <ul>
        <li>
            <label class="CardName">
                <asp:Localize ID="locCardNumber" runat="server" meta:resourceKey="locCardNumber"></asp:Localize>
            </label>
        </li>
        <li>
            <div class="CreditCardNumber">
                <asp:TextBox ID="txtCreditCardNumber" runat="server" Columns="30" MaxLength="20"
                    autocomplete="off"></asp:TextBox>&nbsp;&nbsp;
                       
              <div class="Req1">
                  <asp:RequiredFieldValidator ID="reqCreditCardNumber1" runat="server" ControlToValidate="txtCreditCardNumber"
                      Display="Dynamic" meta:resourcekey="reqCreditCardNumberRes" CssClass="CCError"></asp:RequiredFieldValidator>
              </div>
                <div class="RegularExpressionValidator">
                    <asp:RegularExpressionValidator ID="regCreditCardNumber1" runat="server" ControlToValidate="txtCreditCardNumber"
                        Display="Dynamic" meta:resourcekey="regCreditCardNumber1Res" Enabled="false" CssClass="CCError"></asp:RegularExpressionValidator>
                     <asp:CustomValidator ID="cardNumValidator" ControlToValidate="txtCreditCardNumber" Display="Dynamic"
                        runat="server" ClientValidationFunction="perfiPayment.ValidateAllCardNumber"
                        ValidateEmptyText="false" EnableViewState="false"></asp:CustomValidator>
                </div>
                <div class="MyValidator">
                    <ZNode:ZNodeCreditCardValidator runat="server" AcceptedCardTypes="All, Unknown"
                        ControlToValidate="txtCreditCardNumber" Display="Dynamic" meta:resourcekey="MyValidatorResource1Res"
                        ValidateCardType="True" CssClass="CCError">
                    </ZNode:ZNodeCreditCardValidator>
                </div>

            </div>
            <div class="CreditCardImg"> 
                <img id="imgVisa" src="~/Controls/Images/card_visa.gif" runat="server" alt="visa" title="Visa"
                    align="absmiddle" clientidmode="Static"/>
                <img id="imgMastercard" runat="server" align="absmiddle" alt="Mastercard" title="Mastercard"
                    src="~/Controls/Images/card_mastercard.gif" clientidmode="Static" />
                <img id="imgAmex" runat="server" align="absmiddle" src="~/Controls/Images/card_amex.gif" clientidmode="Static" alt="Amex" title="Amex" />
                <img id="imgDiscover" runat="server" align="absmiddle" src="~/Controls/Images/card_discover.gif" clientidmode="Static" alt="Discover" title="Discover" />
            </div>
        </li>

        <li>
            <label>
                <asp:Localize ID="locExpirationDate" runat="server" meta:resourceKey="locExpirationDate"></asp:Localize>
            </label>
        </li>

        <li runat="server">
            <div class="Month" id="divCCMonth<%=ControlInd %>">
                <asp:DropDownList ID="lstCardMonthExpiry" runat="server">
                    <asp:ListItem Value="0" meta:resourcekey="ListItemResource1">-- Month --</asp:ListItem>
                    <asp:ListItem Value="01" meta:resourcekey="ListItemResource2">Jan</asp:ListItem>
                    <asp:ListItem Value="02" meta:resourcekey="ListItemResource3">Feb</asp:ListItem>
                    <asp:ListItem Value="03" meta:resourcekey="ListItemResource4">Mar</asp:ListItem>
                    <asp:ListItem Value="04" meta:resourcekey="ListItemResource5">Apr</asp:ListItem>
                    <asp:ListItem Value="05" meta:resourcekey="ListItemResource6">May</asp:ListItem>
                    <asp:ListItem Value="06" meta:resourcekey="ListItemResource7">Jun</asp:ListItem>
                    <asp:ListItem Value="07" meta:resourcekey="ListItemResource8">Jul</asp:ListItem>
                    <asp:ListItem Value="08" meta:resourcekey="ListItemResource9">Aug</asp:ListItem>
                    <asp:ListItem Value="09" meta:resourcekey="ListItemResource10">Sep</asp:ListItem>
                    <asp:ListItem Value="10" meta:resourcekey="ListItemResource11">Oct</asp:ListItem>
                    <asp:ListItem Value="11" meta:resourcekey="ListItemResource12">Nov</asp:ListItem>
                    <asp:ListItem Value="12" meta:resourcekey="ListItemResource13">Dec</asp:ListItem>
                </asp:DropDownList>
                <div>
                    <asp:RequiredFieldValidator ID="reqCardMonth" ControlToValidate="lstCardMonthExpiry"
                        runat="server" Display="Dynamic" InitialValue="0" ErrorMessage="Select Month" CssClass="CCError"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="Year" id="divCCYear<%=ControlInd %>">
                <asp:DropDownList ID="lstCardYearExpiry" runat="server">
                </asp:DropDownList>

                <asp:CustomValidator ID="csvlstMonthCard1" ControlToValidate="lstCardMonthExpiry" Display="Dynamic" runat="server"
                    ClientValidationFunction="perfiPayment.ValidateCardForExpiry"
                    ValidateEmptyText="true"  EnableViewState="false"></asp:CustomValidator>
                <asp:CustomValidator ID="csvlstYearCard1" ControlToValidate="lstCardYearExpiry" Display="Dynamic"
                    runat="server" ClientValidationFunction="perfiPayment.ValidateCardForExpiry"
                    ValidateEmptyText="true"  EnableViewState="false"></asp:CustomValidator>
                <div>
                    <asp:RequiredFieldValidator ID="reqlstYearCard1" ControlToValidate="lstCardYearExpiry"
                        runat="server" Display="Dynamic" InitialValue="0" meta:resourceKey="reqlstYearCard1Res" CssClass="CCError"></asp:RequiredFieldValidator>
                </div>
            </div>
            <span id="lblCardExpiry<%=ControlInd %>" class="CCError" style="display: none;">
                <asp:Label ID="ltrExpiryMessage" runat="server" meta:resourceKey="lblCardExpiryMessage"
                    CssClass="CCError" ClientIDMode="Static"></asp:Label>
            </span>
        </li>

        <li runat="server">
            <label>
                <asp:Localize ID="locSecurityCodeCard1" runat="server" meta:resourceKey="locSecurityCodeCard1"></asp:Localize></label>
        </li>
        <li runat="server">
            <div class="SecurityCode">
                <asp:TextBox ID="txtCardCVV" runat="server" Columns="30" MaxLength="4" autocomplete="off"></asp:TextBox>&nbsp;&nbsp;
               <asp:HyperLink ID="hlnkCVVHelp" EnableViewState="false" runat="server"></asp:HyperLink>
            </div>
            <div>
                <asp:RequiredFieldValidator ID="reqtxtCVVCard" ControlToValidate="txtCardCVV"
                    runat="server" Display="Dynamic" meta:resourcekey="reqtxtCVVCardRes" CssClass="CCError"  EnableViewState="false"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regtxtCVVCard" runat="server" ControlToValidate="txtCardCVV"
                    Display="Dynamic" meta:resourcekey="regtxtCVVCardRes" CssClass="CCError"  EnableViewState="false"></asp:RegularExpressionValidator>
            </div>
        </li>

        <li>
            <div id="divCardTotal<%=ControlInd %>" class="SecurityCode PriceBox">
                <ul>
                    <li>
                        <label>
                            <asp:Localize ID="locAmount" runat="server" meta:resourceKey="locAmount"></asp:Localize>
                        </label>
                    </li>
                </ul>
                <asp:TextBox ID="txtCardTotal" runat="server" Text="0.00" onblur="perfiPayment.ManageCreditCardAmount(<%=ControlInd %>,event)" CssClass="TenderTotal"></asp:TextBox>
            </div>
            <div>
                 <asp:RequiredFieldValidator ID="revTotalMoreThanZero" runat="server" ControlToValidate="txtCardTotal" meta:resourcekey="reqtxtTotalCard1Res" Display="Dynamic" EnableViewState="false" InitialValue="0" ></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="revTotal" runat="server" ControlToValidate="txtCardTotal" meta:resourcekey="reqtxtTotalCard1Res" Display="Dynamic"  EnableViewState="false"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="reqtxtTotalCard1" runat="server" ControlToValidate="txtCardTotal" SetFocusOnError="true" Display="Dynamic" meta:resourceKey="reqtxtTotalCard1Res" InitialValue="0.00" CssClass="CCError"  EnableViewState="false"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revtxtTotalCard1" runat="server" ControlToValidate="txtCardTotal" SetFocusOnError="true" Display="Dynamic" meta:resourceKey="revtxtTotalCard1Res" CssClass="CCError"  EnableViewState="false"></asp:RegularExpressionValidator>
            </div>
        </li>
    </ul>
    <div class="RemoveCard">
        <a id="removeCardLnk<%=ControlInd %>" onclick="perfiPayment.RemoveCreditCardPanel(<%=ControlInd %>);">Remove</a>
    </div>
    <asp:HiddenField ID="hdnIsVisible" Value="0" runat="server" />
</div>


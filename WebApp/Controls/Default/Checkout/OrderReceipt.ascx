<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Checkout_OrderReceipt"
    CodeBehind="OrderReceipt.ascx.cs" %>
<%@ Register Src="StepTracker.ascx" TagName="StepTracker" TagPrefix="Znode" %>
<div class="Checkout Confirmation">
    <div class="Steps">
        <Znode:StepTracker id="uxStepTracker" runat="server" />
    </div>
    <div class="ContinueShoppingBtn">
        <a href="/"><asp:Localize ID="locContinueShopping" runat="server" meta:resourceKey="locContinueShopping" ></asp:Localize></a>
        <a id="hylnkCreateAccount" href="~/registeruser.aspx?ReturnUrl=account.aspx" runat="server" visible="false"><asp:Localize ID="locCreateAccount" runat="server" meta:resourceKey="locCreateAccount" ></asp:Localize></a>
    </div>
    <div>
        <img src="<%=BrowserFingerprintImageUrl %>" height="1" width="1" />
        <input type="hidden" id="OrderID" value="<%=OrderID %>" />
        <input type="hidden" id="OrderTotal" value="<%=OrderTotal %>" />
        <input type="hidden" id="ShippingTotal" value="<%=ShippingTotal %>" />
        <input type="hidden" id="TaxTotal" value="<%=TaxTotal %>" />
        <%=ReceiptTemplate%>
    </div>
</div>


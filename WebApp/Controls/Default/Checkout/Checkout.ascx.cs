using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Suppliers;
using ZNode.Libraries.ECommerce.Taxes;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using EnumPaymentType = ZNode.Libraries.ECommerce.Entities.PaymentType;
using Zeon.Libraries.Avatax;
using ZNode.Libraries.DataAccess.Custom;
using Zeon.Libraries.Utilities;
using Zeon.Libraries.Elmah;

namespace WebApp
{
    /// <summary>
    /// Represents the Checkout user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_Checkout : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeCheckout _checkout;
        private PaymentSetting _paymentSetting;
        private ZNodeShoppingCart _shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private ZNodeUserAccount _userAccount;
        private DateTime currentdate = System.DateTime.Now.Date;
        #endregion

        #region Page Load

        /// <summary>
        ///  Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {

            // get objects from session
            this._checkout = new ZNodeCheckout();

            //Znode Custom Code: start
            if (this._checkout.ShoppingCart == null)
            {
                Response.Redirect("~/shoppingcart.aspx");
                return;
            }
            //Znode Custom Code: end
            if (this._checkout.UserAccount != null)
            {
                // Add the checkout page listrack tracking script.
                ZNodeAnalytics analytics = new ZNodeAnalytics();
                analytics.AnalyticsData.IsCheckoutPage = true;
                analytics.AnalyticsData.UserAccount = this._checkout.UserAccount;
                analytics.Bind();
            }
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            errorMsg.Visible = false;
            uxStepTracker.Step = 2;

            //Zeon Customization :: Start
            //if (!this.IsPostBack)
            //{
            //    SubmitButton.Attributes.Add("onclick", string.Format("return submitOrder('{0}','{1}');", SubmitButton.ClientID, SubmitButton.UniqueID));
            //}
            //Zeon Customization :: Start

            if (Session["IsAddressValidated"] == null)
            {
                // User skipped the address validation by type url in address bar. Redirect to the address page.
                Response.Redirect("~/editAddress.aspx?ErrorMsg=AddressError");
            }

            // registers the event for the payment (child) control
            this.uxPayment.EditAddressClicked += new EventHandler(this.UxPayment_EditAddressClicked);
            this.uxPayment.PayPalClicked += new EventHandler(this.UxPayment_PayPalClicked);
            this.uxPayment.GoogleCheckoutClicked += new EventHandler(this.UxPayment_GoogleCheckoutClicked);
            this.uxPayment.TwoCheckoutClicked += new EventHandler(this.UxPayment_2COClicked);

            try
            {
                PaymentSettingService _pmtServ = new PaymentSettingService();
                ZNode.Libraries.DataAccess.Entities.TList<ZNode.Libraries.DataAccess.Entities.PaymentSetting> _pmtSetting = _pmtServ.GetAll();
                int profileID = 0;

                // Check user Session
                if (this._checkout.UserAccount != null)
                {
                    profileID = ZNodeProfile.CurrentUserProfileId;
                }
                else
                {
                    // Get Default Registered profileId
                    profileID = ZNodeProfile.DefaultRegisteredProfileId;
                }

                if (profileID > 0)
                {
                    _pmtSetting.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt) { return (pmt.ProfileID == null || pmt.ProfileID == profileID || pmt.ProfileID == ZNodeProfile.DefaultRegisteredProfileId) && pmt.ActiveInd == true; }); //Perfi Custom code
                    //_pmtSetting.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt) { return (pmt.ProfileID == null || pmt.ProfileID == ZNodeProfile.DefaultRegisteredProfileId) && pmt.ActiveInd == true; });//Znode old

                    // check whether this profile has payment options
                    if (_pmtSetting.Count == 0)
                    {
                        Response.Redirect("~/shoppingcart.aspx?ReturnURL=checkout.aspx&ErrorMsg=paymenterror");
                        return;
                    }
                }
                else
                {
                    Response.Redirect("~/shoppingcart.aspx?ReturnURL=checkout.aspx&ErrorMsg=1");
                    return;
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("No payment options have been setup for this profile in the database. Please use the admin to setup valid payment options first. Additional Information: " + exception.Message);
            }

            this.GetControlData();//Zeon Custom Code

            // check if shopping cart is empty
            //Zeon Custom Code:Added Additional Check to check cart count
            if (this._checkout.ShoppingCart == null || this._checkout.ShoppingCart.Count == 0)
            {
                Response.Redirect("~/shoppingcart.aspx");
                return;
            }
            else
            {
                if (this._checkout.ShoppingCart.Count == 0)
                {
                    throw new ApplicationException("Checkout failed since there are no items in the shopping cart.");
                }

                if (!Page.IsPostBack)
                {
                    // Display promotion description and error message
                    this.Calculate();
                }
            }

            //Zeon Custom Code:: Start ::
            //this.BindCartItems();
            if (!IsPostBack)
            {
                this.BindCartItems();
                bool isSubChecked = this._checkout.UserAccount.ShippingAddress.CountryCode.Equals("US", StringComparison.CurrentCultureIgnoreCase) || this._checkout.UserAccount.EmailOptIn;
                uxPayment.SetSubscriptionCheckBox(isSubChecked);
                string email = this._checkout != null && this._checkout.UserAccount != null ? this._checkout.UserAccount.EmailID : string.Empty;
                CreateLog("||Checkout Process Started-Order Review Page Load||Email-" + email);
            }
            //Zeon Custom Code:: End :: 

            // this.GetControlData();//Znode old code

            if (this.Page.Title != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "PageTitle");
            }

            //Genrate Trigger Email Pixel Tag
            //this.GenerateTriggerMailHelperCartPixelTags();        
            //Nivi Cutom Code:: Start :: 30/08/2018
            //For displaying Sales Tax Notice on Checkout Page for states where Taxes are not collected.
            decimal taxvalue = _shoppingCart.OrderLevelTaxes;
            if (taxvalue == 0)
            {
                divnotice.Visible = true;
            }

            //Nivi Cutom Code:: End ::    
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptCart.Focus();
            }
            RegisterClientScriptClasses();
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Represents the GetImageFilePath function
        /// </summary>    
        /// <returns>Returns the Image File Path</returns>
        protected string GetImageFilePath()
        {
            return ResolveUrl(ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("submit_arrow.gif"));
        }

        /// <summary>
        /// Bind data to payment control
        /// </summary>
        protected void BindPaymentControlData()
        {
            ZNodePayment Payment = new ZNodePayment();
            Payment.BillingAddress = this._checkout.UserAccount.BillingAddress;
            Payment.ShippingAddress = this._checkout.UserAccount.ShippingAddress;
            uxPayment.Payment = Payment;
            this._checkout.ShoppingCart.Payment = (ZNodePayment)Payment;
        }

        /// <summary>
        /// Refreshes all the data in the checkout object using the data collected from the forms
        /// </summary>
        protected void GetControlData()
        {
            this._checkout.ShoppingCart.Payment = uxPayment.Payment;
            this._checkout.ShoppingCart.Payment.BillingAddress = this._checkout.UserAccount.BillingAddress;
            this._checkout.ShoppingCart.Payment.ShippingAddress = this._checkout.UserAccount.ShippingAddress;
            this._checkout.AdditionalInstructions = uxPayment.AdditionalInstructions;
            this._checkout.UserAccount.BillingAddress = this._checkout.UserAccount.BillingAddress;
            this._checkout.UserAccount.ShippingAddress = this._checkout.UserAccount.ShippingAddress;
            //Zeon Custsom Code:Start
            if (BuyerUser != null)
            {
                this._checkout.UserAccount.BillingAddress = BuyerUser.BillingAddress;
                this._checkout.UserAccount.ShippingAddress = BuyerUser.ShippingAddress;
                //Added To solve new user by admin order issue
                this._checkout.UserAccount.BillingAddressExtn = BuyerUser.BillingAddressExtn;
                this._checkout.UserAccount.ShippingAddressExtn = BuyerUser.ShippingAddressExtn;
                this._checkout.ShoppingCart.Payment.BillingAddress = BuyerUser.BillingAddress;
                this._checkout.ShoppingCart.Payment.ShippingAddress = BuyerUser.ShippingAddress;
            }
            //Zeon Custom Code:end
            this._checkout.ShoppingCart.Payment.SaveCardData = uxPayment.SaveCardData;
            this._checkout.PaymentSettingID = uxPayment.PaymentSettingID;
            this._checkout.PurchaseOrderNumber = uxPayment.PurchaseOrderNumber;
            this._checkout.AdditionalInstructions = uxPayment.AdditionalInstructions;
        }

        /// <summary>
        /// Google checkout event raised by payment control. Do the submit order with google checkout payment method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Arguments</param>
        protected void UxPayment_GoogleCheckoutClicked(object sender, EventArgs e)
        {
            this.OnSubmitOrder(sender, e);
        }

        /// <summary>
        /// PayPal clicked event raised by payment control Do the submit order with PayPal payment method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxPayment_PayPalClicked(object sender, EventArgs e)
        {
            this.OnSubmitOrder(sender, e);
        }

        /// <summary>
        /// 2CO clicked event raised by payment control Do the submit order with PayPal payment method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxPayment_2COClicked(object sender, EventArgs e)
        {
            this.On2COCheckout(sender, e);
        }

        /// <summary>
        /// Repeater Control Item Data bound event.
        /// </summary>
        /// <param name="sender">repeater object</param>
        /// <param name="e">event args</param>
        protected void RptCart_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ZNodeShoppingCartItem item = (ZNodeShoppingCartItem)e.Item.DataItem;
                Controls_Default_Checkout_Order ShopingCart = (Controls_Default_Checkout_Order)e.Item.FindControl("uxOrder");
                ShopingCart.ShippingSelectedIndexChanged += new EventHandler(this.UxOrder_SelectedIndexChanged);
                ShopingCart.PortalID = item.Product.PortalID;

                #region[ZNode Old Code :: Code Commented :: Call BindShipping on Order.ascx page load]
                //ShopingCart.BindGrid();
                //ShopingCart.BindShipping();
                #endregion
            }
        }

        #endregion

        #region Submit Order & Payment

        /// <summary>
        /// Event triggered when the 2CO checkout button is clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void On2COCheckout(object sender, EventArgs e)
        {
            string thankyouPage = "paymentinshandler.aspx?mode=2co";

            string insURL = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier(thankyouPage);
            string returnURL = insURL;

            // Get payment setting object for 2co payment type
            PaymentSetting paymentSetting = this.GetByPaymentTypeId(5);

            ZNode.Libraries.ECommerce.Payment.Gateway2CO twoCO = new ZNode.Libraries.ECommerce.Payment.Gateway2CO();
            twoCO.SubmitPayment(paymentSetting, returnURL, insURL);
        }

        /// <summary>
        /// Event triggered when the submit order button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void OnSubmitOrder(object sender, EventArgs e)
        {
            if (CheckIsOrderSubmittedSuccessfully())
            {
                Response.Redirect("~/orderreceipt.aspx");
            }
            else
            {
                #region Perficient Multiple Credit Card Section
                if ((this.uxPayment.PaymentType == EnumPaymentType.CREDIT_CARD && this.uxPayment.Payment.IsMultipleCardPayment && !this.CheckCreditCardTotal()))
                {
                    lblError.Text = this.GetLocalResourceObject("PaymentErrorMsg").ToString();
                    return;
                }
                #endregion

                int registerprofileID = ZNodeProfile.DefaultRegisteredProfileId;//Perfi custom codes

                bool isCustomerTaxExempt = false;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();

                // Find is default store, so we include all profiles.
                CategoryService categoryService = new CategoryService();
                int total;
                TList<Category> categoryList = categoryService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID, 0, 1, out total);
                bool IsDefaultPortal = categoryList.Count == 0;

                // Get vendor specific cart.
                System.Collections.Generic.List<int> portalIds = this.GetShoppingCartVendors();
                List<ZNodeOrderFulfillment> orders = new List<ZNodeOrderFulfillment>();
                foreach (int portalId in portalIds)
                {
                    int? profileID = null;
                    if (portalId > 0)
                    {
                        categoryList = categoryService.GetByPortalID(portalId, 0, 1, out total);
                        IsDefaultPortal = categoryList.Count == 0;

                        PortalProfileService ps = new PortalProfileService();
                        TList<PortalProfile> portalProfiles = ps.GetByPortalID(portalId);

                        PortalProfile portalProfile = portalProfiles.Find(PortalProfileColumn.ProfileID, this._checkout.UserAccount.ProfileID);

                        if (portalProfile != null)
                        {
                            // Assign profile Id only for franchise store.
                            profileID = this._checkout.UserAccount.ProfileID;

                            //Zeon Custom Code:Start                          
                            if (ZNodeProfile.CurrentUserProfileId != this._checkout.UserAccount.ProfileID)
                            {
                                profileID = ZNodeProfile.CurrentUserProfileId;
                            }
                            //Zeon Custom Code:End
                        }
                        else
                        {
                            PortalService portalService = new PortalService();
                            Portal otherFranchisePortal = portalService.GetByPortalID(portalId);
                            profileID = otherFranchisePortal.DefaultRegisteredProfileID;
                        }

                        this._checkout.PortalID = portalId;
                    }
                    else
                    {
                        // Getting Global Store PortalID
                        PortalService portalService = new PortalService();
                        TList<Portal> stores = portalService.GetByActiveInd(true);
                        stores.Sort("PortalID");
                        this._checkout.PortalID = stores[0].PortalID;
                        Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), stores[0].PortalID)] = Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), portalId)];
                    }

                    this._checkout.ShoppingCart = (ZNodeShoppingCart)Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), this._checkout.PortalID)];

                    // Get data from user controls
                    this.GetControlData();
                    //Zeon Custom Code:Starts

                    this._checkout.ShoppingCartNotesEnabled = CheckCurrentPortalHasNotesEnabled();
                    //Zeon Custom Code:Ends

                    // No payment method selected.
                    if (this._checkout.PaymentSettingID == 0)
                    {
                        lblError.Text = this.GetLocalResourceObject("PaymentNotSelected").ToString();
                        errorMsg.Visible = true;
                        return;
                    }

                    int paymentTypeID = (int)uxPayment.PaymentType;

                    PaymentSettingService paymentSettingService = new PaymentSettingService();
                    TList<PaymentSetting> paymentSettings = paymentSettingService.GetAll();

                    var _profilePayments = paymentSettings.FindAll(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                    {
                        return (pmt.ProfileID == profileID || pmt.ProfileID == registerprofileID) &&
                            pmt.ActiveInd == true;
                    });

                    var allProfilePayments = paymentSettings.FindAll(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                    {
                        return (pmt.ProfileID == null && IsDefaultPortal) &&
                            pmt.ActiveInd == true;
                    });

                    if (allProfilePayments.Any())
                    {
                        _profilePayments.AddRange(allProfilePayments.Where(x => !_profilePayments.Any(y => y.PaymentTypeID == x.PaymentTypeID)).ToList());
                    }

                    paymentSettings = _profilePayments.FindAll(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                    {
                        return (pmt.ProfileID == profileID || pmt.ProfileID == registerprofileID || (pmt.ProfileID == null && IsDefaultPortal)) &&
                                (pmt.PaymentTypeID == paymentTypeID) &&
                                pmt.ActiveInd == true;
                    });

                    if (paymentSettings != null && paymentSettings.Count > 0)
                    {
                        this._checkout.ShoppingCart.Payment.PaymentSetting = paymentSettings[0];
                        this._checkout.PaymentSettingID = paymentSettings[0].PaymentSettingID;
                    }
                    else
                    {
                        log.LogActivityTimerEnd(5003, null);

                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest : Users purchasing items from multiple vendors, but the site is not configured for Credit Card payment");

                        lblError.Text = this.GetLocalResourceObject("MultivendorNotSupported").ToString();
                        errorMsg.Visible = true;
                        return;
                    }

                    ZNodeOrderFulfillment order = new ZNodeOrderFulfillment();

                    try
                    {
                        log.LogActivityTimerStart();

                        if (this._checkout.ShoppingCart.PreSubmitOrderProcess())
                        {
                            if (uxPayment.PaymentType == EnumPaymentType.PAYPAL)
                            {
                                //string returnURL = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier("PaymentINSHandler.aspx");
                                string returnURL = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier("PayPalOrderReview.aspx");
                                string cancelURL = returnURL.Replace("paymentinshandler.aspx", "shoppingcart.aspx");
                                returnURL += "?mode=Paypal";

                                this._shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                                if (this._shoppingCart != null)
                                {
                                    // Get payment setting object for paypal payment type
                                    this._paymentSetting = this.GetByPaymentTypeId(2);

                                    ZNode.Libraries.Paypal.PaypalGateway paypal =
                                        new ZNode.Libraries.Paypal.PaypalGateway(this._paymentSetting, returnURL, cancelURL,
                                            this._checkout.ShoppingCart.IsShipngChrgsExtended, this._checkout.ShoppingCart.ExtendedShippingCost);

                                    paypal.OrderTotal = this._shoppingCart.Total;
                                    paypal.PaymentActionTypeCode = "Sale";
                                    //paypal.ExtendedShippingCost = this._checkout.ShoppingCart.ExtendedShippingCost;
                                    //paypal.IsShipngChrgsExtended = this._checkout.ShoppingCart.IsShipngChrgsExtended;

                                    ZNode.Libraries.Paypal.PaypalResponse response = paypal.DoPaypalExpressCheckout(true);

                                    if (response.ResponseCode != "0")
                                    {
                                        // Display error page
                                        lblError.Text = response.ResponseText;
                                        errorMsg.Visible = true;
                                        return;
                                    }
                                    else
                                    {
                                        try
                                        {
                                            // Redirect to paypal server
                                            Response.Redirect(response.HostUrl, true);
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                    }
                                }
                            }
                            else if (uxPayment.PaymentType == EnumPaymentType.GOOGLE_CHECKOUT)
                            {
                                bool isSuccess = this.DoGoogleCheckout();
                                if (!isSuccess)
                                {
                                    return;
                                }
                            }
                            //Zeon Custom Code:Starts
                            isCustomerTaxExempt = IsTaxExemptedCustomer();
                            this._checkout.TaxExemptedCustomer = isCustomerTaxExempt;
                            //Zeon Custom Code:Ends
                            order = (ZNodeOrderFulfillment)this._checkout.SubmitOrder();
                            orders.Add(order);
                        }
                        else
                        {
                            // display error page
                            lblError.Text = this._checkout.ShoppingCart.ErrorMessage + "<br>" + this._checkout.ShoppingCart.PromoDescription;
                            errorMsg.Visible = true;
                            return;
                        }
                    }
                    catch (ZNode.Libraries.ECommerce.Entities.ZNodePaymentException ex)
                    {
                        log.LogActivityTimerEnd(5003, Request.UserHostAddress.ToString(), null, null, null, ex.Message + " " + ex.StackTrace);

                        // display payment error message
                        lblError.Text = ex.Message;
                        errorMsg.Visible = true;
                        return;
                    }
                    catch (Exception exc)
                    {
                        //Zeon Custom Code:start
                        string exceptionorderId = string.Empty;
                        if (order != null && order.OrderID > 0)
                        {
                            exceptionorderId = order.OrderID.ToString();
                        }
                        //log.LogActivityTimerEnd(5003, null);//Znode old code
                        log.LogActivityTimerEnd(5003, Request.UserHostAddress.ToString(), null, null, null, "OrderID=" + exceptionorderId + " Detail:" + exc.StackTrace);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest OrderID=" + exceptionorderId + "PaymentType=" + uxPayment.PaymentType + "Detail:" + exc.ToString() + " " + exc.StackTrace.ToString());

                        string strlog = "\nGeneral Checkout Exception . \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nOrderID : {4}, \nPaymentType : {5}\n\n";
                        strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, exceptionorderId, uxPayment.PaymentType);
                        Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

                        Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(exc.ToString());
                        //Zeon Custom Code:End

                        // display error page
                        lblError.Text = Resources.CommonCaption.UnableToProcessRequest;
                        errorMsg.Visible = true;
                        return;
                    }

                    // Post order submission
                    if (this._checkout.IsSuccess && orders.Count > 0)
                    {
                        log.LogActivityTimerEnd(5002, orders[0].OrderID.ToString());

                        // Worldpay
                        if (!string.IsNullOrEmpty(this._checkout.ECRedirectURL))
                        {
                            if (!string.IsNullOrEmpty(this._checkout.ShoppingCart.Payment.WorldPayPostData))
                            {
                                this.DoUrlPost(this._checkout.ECRedirectURL);
                            }
                            else
                            {
                                Response.Redirect(this._checkout.ECRedirectURL);
                            }
                        }

                        // Make an entry in tracking event for placing an Order 
                        ZNodeTracking tracker = new ZNodeTracking();
                        if (tracker.AffiliateId.Length > 0)
                        {
                            tracker.AccountID = order.AccountID;
                            tracker.OrderID = order.OrderID;
                            tracker.LogTrackingEvent("Placed an Order");
                        }

                        this.PostSubmitOrder(order);

                        //Zeon Custom Code: Start
                        //Set the session variable for the Order ID and commit the tax.
                        Session["SetOrderIdForAvatax"] = order.OrderID;
                        if (!isCustomerTaxExempt)
                        {
                            AvataxSalesInvoiceCall(order, this._shoppingCart);
                        }
                        Session["SetOrderIdForAvatax"] = null;
                        if (this._checkout.UserAccount != null && !string.IsNullOrEmpty(this._checkout.UserAccount.EmailID))
                        {
                            SaveOptInAccount();
                            if (uxPayment.IsMailSubscribe)
                            {
                                SubscribeEmailUser();
                            }
                            else
                            {
                                string contactID = !string.IsNullOrEmpty(this._checkout.UserAccount.Custom2) ? this._checkout.UserAccount.Custom2 : string.Empty;
                                if (!string.IsNullOrEmpty(contactID))
                                {
                                    UnSubscribeEmailUser(contactID);
                                }
                            }
                        }
                        //Zeon Custom Code: End
                    }
                    else
                    {
                        log.LogActivityTimerEnd(5001, null, null, null, null, this._checkout.PaymentResponseText);
                        lblError.Text = this._checkout.PaymentResponseText;
                        errorMsg.Visible = true;
                        //Perficient Mutltiple Tender Custom Code:Starts 
                        if (uxPayment.PaymentType == EnumPaymentType.CREDIT_CARD)
                        {
                            uxPayment.CardPayStatus = !string.IsNullOrEmpty(this._checkout.AdditionalCreditCardError) ? this._checkout.PaymentResponseText + string.Format(this.GetLocalResourceObject("MultipleCardProcessError").ToString(), this._checkout.AdditionalCreditCardError) : this._checkout.PaymentResponseText;
                        }
                        lblError.Text += !string.IsNullOrEmpty(this._checkout.AdditionalCreditCardError) ? string.Format(this.GetLocalResourceObject("MultipleCardProcessError").ToString(), this._checkout.AdditionalCreditCardError) : string.Empty;
                        //Perficient Mutltiple Tender Custom Code:Ends 
                        return;
                    }
                }

                // Create session for Order and Shopping cart
                ZNodeShoppingCart shopping = new ZNodeShoppingCart();
                Session.Add("OrderDetail", orders);

                ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                // If shopping cart is null, create in session
                if (shoppingCart == null)
                {
                    shoppingCart = new ZNodeShoppingCart();
                    shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                }

                // Clear the payment type session.
                Session["PaymentType"] = null;
                //Zeon Custom Code: Start
                //Commented to solve Address validation Error after Placing Order
                //Session["IsAddressValidated"] = null;
                //Zeon Custom Code: Ends
                Response.Redirect("~/orderreceipt.aspx");
            }
        }

        /// <summary>
        /// Do google express checkout.
        /// </summary>
        /// <returns>Returns whether Google Checkout can be processed or not </returns>
        protected bool DoGoogleCheckout()
        {
            string returnURL = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier("notification.aspx");
            string cancelURL = returnURL.Replace("notification.aspx", "shoppingcart.aspx");
            returnURL += "?mode=Google";

            // Get payment setting object for google payment type
            this._shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
            this._userAccount = (ZNodeUserAccount)ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount);
            if (this._shoppingCart != null)
            {
                this._paymentSetting = this.GetByPaymentTypeId(3);

                ZNode.Libraries.Google.GoogleCheckout google = new ZNode.Libraries.Google.GoogleCheckout(returnURL, cancelURL);
                google.PaymentSetting = this._paymentSetting;

                ZNode.Libraries.Google.GoogleCheckoutResponse response = google.SendRequestToGoogle();

                if (response.ResponseCode != "0")
                {
                    // Display error page
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest : " + response.ResponseText);
                    lblError.Text = Resources.CommonCaption.UnableToProcessRequest;
                    errorMsg.Visible = true;
                    return false;
                }
                else
                {
                    // Get the GUID key
                    string serialNo = response.ResponseText;

                    this._shoppingCart.Payment.PaymentSetting = this._paymentSetting;
                    HttpRuntime.Cache.Add(serialNo + "cart", this._shoppingCart, null, DateTime.Now.AddHours(1), System.TimeSpan.FromMinutes(0), System.Web.Caching.CacheItemPriority.Normal, null);

                    // Add user account into the cache.
                    if (this._userAccount == null)
                    {
                        this._userAccount = new ZNodeUserAccount();
                        this._userAccount.AddUserAccount();
                    }

                    HttpRuntime.Cache.Add(serialNo + "user", this._userAccount, null, DateTime.Now.AddHours(1), System.TimeSpan.FromMinutes(0), System.Web.Caching.CacheItemPriority.Normal, null);

                    // Add portal id to the cache.
                    HttpRuntime.Cache.Add(serialNo + "portalid", ZNodeConfigManager.SiteConfig.PortalID, null, DateTime.Now.AddHours(1), System.TimeSpan.FromMinutes(0), System.Web.Caching.CacheItemPriority.Normal, null);

                    this._shoppingCart.PreSubmitOrderProcess();

                    Response.Redirect(response.ECRedirectURL);
                }
            }

            return true;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Shipping option list selected index changed Event raised by the order control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Calculate();

            if (this._checkout.ShoppingCart.Total == 0)
            {
                // Hide payment section on the final checkout page
                uxPayment.ShowPaymentSection = false;
            }
            else
            {
                // Display payment section
                uxPayment.ShowPaymentSection = true;
            }
        }

        /// <summary>
        /// Edit address Event raised by the payment control - move to first step
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxPayment_EditAddressClicked(object sender, EventArgs e)
        {
            Response.Redirect("editaddress.aspx");
        }

        private void BindCartItems()
        {
            System.Collections.Generic.List<int> list = new System.Collections.Generic.List<int>();
            List<ZNodeShoppingCartItem> cartItems = new List<ZNodeShoppingCartItem>();
            if (this._shoppingCart != null)
            {
                foreach (ZNodeShoppingCartItem cartItem in this._shoppingCart.ShoppingCartItems)
                {
                    int portalId = 0;
                    if (cartItem.Product.PortalID > 0)
                    {
                        portalId = cartItem.Product.PortalID;
                    }

                    if (!list.Contains(portalId))
                    {
                        list.Add(portalId);
                        cartItems.Add(cartItem);
                    }
                }
            }

            rptCart.DataSource = cartItems;
            rptCart.DataBind();

            if (list.Count > 1 || list.Find(delegate(int portalId) { return portalId != ZNodeConfigManager.SiteConfig.PortalID; }) > 0)
            {
                uxPayment.IsMultipleVendorShoppingCart = true;
                uxPayment.BindPaymentTypeData();

                Localize2.Text = this.GetLocalResourceObject("txtMVReviewText").ToString();
            }
        }

        private System.Collections.Generic.List<int> GetShoppingCartVendors()
        {
            System.Collections.Generic.List<int> list = new System.Collections.Generic.List<int>();
            if (this._shoppingCart != null)
            {
                foreach (ZNodeShoppingCartItem cartItem in this._shoppingCart.ShoppingCartItems)
                {
                    int portalId = 0;
                    if (cartItem.Product.PortalID > 0)
                    {
                        portalId = cartItem.Product.PortalID;
                    }

                    if (!list.Contains(portalId))
                    {
                        list.Add(portalId);
                    }
                }
            }

            return list;
        }

        #endregion

        #region Post Submit Order - Set Digital Asset

        /// <summary>
        /// Post Order Submission CreateCustomerProfile
        /// </summary>
        /// <param name="order">Order information</param>
        private void PostSubmitOrder(ZNodeOrderFulfillment order)
        {
            this._checkout.ShoppingCart.Payment.TransactionID = order.CardTransactionID;
            this._checkout.ShoppingCart.Payment.AuthCode = order.CardAuthCode;
            this._checkout.ShoppingCart.Payment.SubscriptionID = order.Custom2;
            this._checkout.ShoppingCart.PostSubmitOrderProcess();

            ZNodeSupplierOption supplierOption = new ZNodeSupplierOption();
            supplierOption.SubmitOrder(order, this._checkout.ShoppingCart);

            // Clear the savedcart info
            ZNodeSavedCart savedCart = new ZNodeSavedCart();
            savedCart.RemoveSavedCart();

            int Counter = 0;
            DigitalAssetService digitalAssetService = new DigitalAssetService();

            // Loop through the Order Line Items
            foreach (OrderLineItem orderLineItem in order.OrderLineItems)
            {
                ZNodeShoppingCartItem shoppingCartItem = this._checkout.ShoppingCart.ShoppingCartItems[Counter++];

                // Set quantity ordered
                int qty = shoppingCartItem.Quantity;

                // Set product id
                int productId = shoppingCartItem.Product.ProductID;
                ZNodeGenericCollection<ZNodeDigitalAsset> AssignedDigitalAssets = new ZNodeGenericCollection<ZNodeDigitalAsset>();

                // get Digital assets for productid and quantity
                AssignedDigitalAssets = ZNodeDigitalAssetList.CreateByProductIdAndQuantity(productId, qty).DigitalAssetCollection;

                // Loop through the digital asset retrieved for this product
                foreach (ZNodeDigitalAsset digitalAsset in AssignedDigitalAssets)
                {
                    DigitalAsset entity = digitalAssetService.GetByDigitalAssetID(digitalAsset.DigitalAssetID);

                    // Set OrderLineitemId property
                    entity.OrderLineItemID = orderLineItem.OrderLineItemID;

                    // Update digital asset to the database
                    digitalAssetService.Update(entity);
                }

                // Set retrieved digital asset collection to shopping product object
                // if product has digital assets, it will display it on the receipt page along with the product name
                shoppingCartItem.Product.ZNodeDigitalAssetCollection = AssignedDigitalAssets;

                // Update Display Order
                ZNode.Libraries.ECommerce.Catalog.ZNodeDisplayOrder displayOrder = new ZNode.Libraries.ECommerce.Catalog.ZNodeDisplayOrder();
                displayOrder.SetDisplayOrder(ZNode.Libraries.ECommerce.Catalog.ZNodeProduct.Create(shoppingCartItem.Product.ProductID));
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Get the customer address by address Id.
        /// </summary>
        /// <param name="addressId">Address Id to get the customer address.</param>
        /// <returns>Returns the Address object.</returns>
        private Address GetDefaultAddress(int addressId)
        {
            AddressService addressService = new AddressService();
            Address address = addressService.GetByAddressID(addressId);

            return address;
        }

        /// <summary>
        /// Calculate tax, shipping cost
        /// </summary>
        private void Calculate()
        {
            //Zeon Customization :: Calculates final pricing, and taxes in the cart. :: Start
            //this._checkout.ShoppingCart.Calculate();

            this._checkout.ShoppingCart.CalculateExtn();
            //Zeon Customization :: Calculates final pricing, and taxes in the cart. :: End

            // If next button was clicked on the address step then set payment billing address
            if (!string.IsNullOrEmpty(this._checkout.ShoppingCart.ErrorMessage))
            {
                lblError.Text = this._checkout.ShoppingCart.ErrorMessage;
                errorMsg.Visible = true;
            }
        }

        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type Id</param>
        /// <returns>Returns PaymentSetting</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            int? profileID = 0;
            int registerProfileID = ZNodeProfile.DefaultRegisteredProfileId;
            if (this._userAccount != null)
            {
                // Get loggedIn user profile
                profileID = this._userAccount.ProfileID;
            }
            else
            {
                profileID = ZNodeProfile.DefaultRegisteredProfileId;
            }

            TList<PaymentSetting> list = _pmtServ.GetByPaymentTypeID(paymentTypeID);

            PaymentSetting listResult = list.Find((e) => { return ((e.ProfileID == profileID || e.ProfileID == registerProfileID) && e.ActiveInd == true); });
            PaymentSetting listAllProfileResult = null;
            //list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (listResult != null)
            {
                return listResult;
            }

            if (listResult == null)
            {
                //Get All Profiles payment setting               
                listAllProfileResult = list.Find((e) => { return (e.ProfileID == null && e.ActiveInd == true); });
            }

            if (listAllProfileResult != null)
            {
                return listAllProfileResult;
            }
            return null;

            //list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            //if (list.Count == 0)
            //{
            //    // Get All Profiles payment setting
            //    list.Filter = "ActiveInd = true";
            //}

            //list.Sort("ProfileID DESC");

            //return list[0];
        }

        private void DoUrlPost(string url)
        {
            Uri requestUrl = new Uri(HttpUtility.HtmlEncode(url));

            NameValueCollection queryCollection = new NameValueCollection();

            foreach (string vp in Regex.Split(url.Substring(url.LastIndexOf("?") + 1), "&"))
            {
                int indx = vp.IndexOf('=');
                queryCollection.Add(vp.Substring(0, indx), vp.Substring(indx + 1));
            }

            // Create temporary HTML form to post the parameters to 2CO site.            
            StringBuilder resp = new StringBuilder();
            resp.Append("<html>");
            resp.Append("<head><title>Processing Payment...</title></head>");
            resp.Append("<body onLoad=\"document.forms['gateway_form'].submit();\">");
            resp.Append(string.Format("<form method=\"POST\" name=\"gateway_form\" action=\"{0}\">", requestUrl.AbsoluteUri.Replace(requestUrl.Query.Substring(requestUrl.Query.LastIndexOf("?")), string.Empty)));
            resp.Append("<p><h2 style=\"text-align:center;\">Please wait, your order is being processed and you will be redirected to the payment website.</h2></p>");

            // Create hidden input form fields
            string value;
            foreach (string key in queryCollection.Keys)
            {
                value = queryCollection.GetValues(key)[0];
                resp.Append(string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"/>", key, value));
            }

            resp.Append("<p style=\"text-align:center;\"><br/><br/>If you are not automatically redirected to payment website within 5 seconds...<br/><br/>");
            resp.Append("<input type=\"submit\" value=\"Click Here\"></p>");
            resp.Append("</form>");
            resp.Append("</body></html>");
            Response.Clear();
            Response.Write(resp.ToString());
            Response.End();
        }

        #endregion

        #region Zeon Custom Code

        /// <summary>
        /// Add Buyer User
        /// </summary>
        public ZNodeUserAccount BuyerUser
        {
            get
            {
                if (Session["BuyerUser"] != null)
                {
                    return (ZNodeUserAccount)Session["BuyerUser"];
                }
                return null;
            }
        }

        /// <summary>
        /// Final call to avatax for Sales invoice 
        /// </summary>
        private void AvataxSalesInvoiceCall(ZNodeOrderFulfillment ord, ZNodeShoppingCart cart)
        {
            int accountId = ZNodeUserAccount.CurrentAccount() != null ? ZNodeUserAccount.CurrentAccount().AccountID : 0;
            Avatax avaTax = new Avatax();
            avaTax.CalculateTax(cart, accountId, ZNodeConfigManager.SiteConfig.PortalID);
        }

        /// <summary>
        /// Check if the Customer is Tax Expempted or Not.
        /// </summary>
        /// <returns>Returns True if Customer is Tax Expempt else False.</returns>
        private bool IsTaxExemptedCustomer()
        {
            bool customertaxExempt = false;
            this._userAccount = ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount) as ZNodeUserAccount;
            int accountid = 0;
            if (this._userAccount != null && this._userAccount.AccountID > 0)
            {
                accountid = this._userAccount.AccountID;
            }
            AccountProfileService service = new AccountProfileService();
            if (BuyerUser != null && BuyerUser.AccountID > 0)
            {
                accountid = BuyerUser.AccountID;
            }
            TList<AccountProfile> lstProfile = service.GetByAccountID(accountid);
            foreach (AccountProfile item in lstProfile)
            {
                ProfileService profileInfoService = new ProfileService();
                Profile profileInfo = profileInfoService.GetByProfileID(item.ProfileID == null ? 0 : Convert.ToInt32(item.ProfileID));

                if (profileInfo.TaxExempt)
                {
                    customertaxExempt = true;
                    break;
                }
            }
            return customertaxExempt;
        }

        /// <summary>
        /// Check is Order Placed 
        /// Before Submitting the Order Check is Order Submited,to prevent placed duplicate order in ie 11
        /// </summary>
        /// <returns></returns>
        private bool CheckIsOrderSubmittedSuccessfully()
        {
            bool isPlacedSuccessfully = false;
            try
            {
                ZNodeOrderFulfillment _Order = null;
                List<ZNodeOrderFulfillment> _Orders = new List<ZNodeOrderFulfillment>();
                if (Session["OrderDetail"] is List<ZNodeOrderFulfillment>)
                {
                    _Orders = Session["OrderDetail"] as List<ZNodeOrderFulfillment>;
                }
                else if (Session["OrderDetail"] is ZNodeOrderFulfillment)
                {
                    _Order = Session["OrderDetail"] as ZNodeOrderFulfillment;
                }

                if (_Order == null && _Orders.Count > 0)
                {
                    _Order = _Orders[0];
                }

                if (_Order != null && _Order.OrderID > 0)
                {
                    isPlacedSuccessfully = true;
                }
            }
            catch (Exception ex)
            {
                isPlacedSuccessfully = false;
            }
            return isPlacedSuccessfully;
        }

        /// <summary>
        /// check if current portal has enabled Notes in shopping cart
        /// </summary>
        /// <returns></returns>
        private bool CheckCurrentPortalHasNotesEnabled()
        {
            bool isNotesEnabled = false;
            if (ConfigurationManager.AppSettings["ShoppingCartNotesPortal"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["ShoppingCartNotesPortal"]))
            {
                string portalList = ConfigurationManager.AppSettings["ShoppingCartNotesPortal"].ToString().Trim();
                string[] allPortals = null;
                if (portalList.IndexOf(',') > 0)
                {
                    allPortals = portalList.Split(',');
                    foreach (string strPortal in allPortals)
                    {
                        if (strPortal.Equals(ZNodeConfigManager.SiteConfig.PortalID.ToString()))
                        {
                            isNotesEnabled = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (portalList.Equals(ZNodeConfigManager.SiteConfig.PortalID.ToString()))
                    {
                        isNotesEnabled = true;
                    }

                }
            }
            return isNotesEnabled;
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var zeonCheckout = new Checkout({");
            script.Append("});zeonCheckout.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "Checkout", script.ToString(), true);
        }

        #region Email Subscription Code

        /// <summary>
        /// Subscribe User For Email Service
        /// </summary>
        private void SubscribeEmailUser()
        {
            string responseContactID = string.Empty;
            try
            {
                if (ConfigurationManager.AppSettings["GetResponseAPIKey"] != null && ConfigurationManager.AppSettings["GetResponseAPIURL"] != null && this._checkout.UserAccount != null)
                {
                    string newsLetterCampaign = GetCompaignNameByPortal();
                    if (!string.IsNullOrEmpty(newsLetterCampaign))
                    {
                        GetResponseEmailSubscriber emailSubscriber = new GetResponseEmailSubscriber();
                        string campaignID = emailSubscriber.AddContactEmailSubcriber(ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString(), ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString(), newsLetterCampaign, this._checkout.UserAccount.EmailID, this._checkout.UserAccount.EmailID);
                    }
                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }
        }

        /// <summary>
        /// Get Newsletter Compaign Name by Portal
        /// </summary>
        /// <returns></returns>
        private string GetCompaignNameByPortal()
        {
            string compaignName = string.Empty;
            PortalExtnService portalExtnService = new PortalExtnService();
            TList<PortalExtn> currentPortal = portalExtnService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID);
            if (currentPortal != null && currentPortal.Count > 0)
            {
                compaignName = currentPortal[0].NewsLetterCampaignName;
            }

            return compaignName;
        }

        /// <summary>
        /// Save Email Opt In if Check box is check
        /// </summary>
        private void SaveOptInAccount()
        {
            AccountService accountService = new AccountService();
            Account account = accountService.GetByAccountID(this._checkout.UserAccount.AccountID);
            try
            {
                if (account != null)
                {
                    account.EmailOptIn = uxPayment.IsMailSubscribe;
                    accountService.Update(account);
                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }
        }

        /// <summary>
        /// Subscribe User For Email Service
        /// </summary>
        /// <param name="contactId">string</param>
        private void UnSubscribeEmailUser(string contactId)
        {
            string responseContactID = string.Empty;
            try
            {
                if (ConfigurationManager.AppSettings["GetResponseAPIKey"] != null && ConfigurationManager.AppSettings["GetResponseAPIURL"] != null)
                {
                    string newsLetterCampaign = GetCompaignNameByPortal();
                    if (!string.IsNullOrEmpty(newsLetterCampaign))
                    {
                        GetResponseEmailSubscriber emailSubscriber = new GetResponseEmailSubscriber();
                        emailSubscriber.UnsubscribeContactEmail(ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString(), ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString(), contactId);
                    }
                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }
        }
        #endregion

        #region Multiple Tender/CC Code

        /// <summary>
        /// Verify order Total and cc total
        /// </summary>
        /// <returns>bool</returns>
        private bool CheckCreditCardTotal()
        {
            bool isCardTotalValid = true;
            if (this.uxPayment.Payment.CreditCardList != null && this.uxPayment.Payment.CreditCardList.Count > 0 && !this.uxPayment.CompareCCTotalWithOrderTotal(this._checkout.ShoppingCart.Total))
            {
                //Show Error if Total Do not match
                lblError.Text = this.GetLocalResourceObject("CardAmountAndOrderTotalMisMatchMsg").ToString();
                errorMsg.Visible = true;
                isCardTotalValid = false;
            }
            return isCardTotalValid;
        }
        #endregion

        private void CreateLog(string eventName)
        {
            string strlog = "\n" + eventName + ". \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
            strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, "", "");
            Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);
        }

        #endregion

    }
}
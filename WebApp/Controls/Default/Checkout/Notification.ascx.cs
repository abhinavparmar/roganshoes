using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Suppliers;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Paypal;

namespace WebApp
{
    /// <summary>
    /// Represents the Notification user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_Notification : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private string _paymentGateway = string.Empty;
        private string _token = string.Empty;
        private string _payerID = string.Empty;
        private ZNodeCheckout _checkout;
        private ZNodeUserAccount _userAccount = null;
        private ZNodeShoppingCart _shoppingCart = null;
        private PaymentSetting _paymentSetting;
        #endregion

        #region Protected Properties
        /// <summary>
        /// Gets the unique token (i.e transaction id)
        /// </summary>
        protected string Token
        {
            get
            {
                if (this._token.Length == 0)
                {
                    this._token = this.Request.QueryString.Get("token");
                }

                return this._token;
            }
        }

        /// <summary>
        /// Gets the gateway type like google,paypal
        /// </summary>
        protected string Mode
        {
            get
            {
                return this.Request["mode"];
            }
        }

        /// <summary>
        /// Gets the unique Payer id
        /// </summary>
        protected string PayerId
        {
            get
            {
                if (this._payerID.Length == 0)
                {
                    this._payerID = this.Request.QueryString.Get("payerid");
                }

                return this._payerID;
            }
        }

        #endregion

        #region Page Load Event

        /// <summary>
        /// Page Load Event    
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this._paymentGateway = this.Mode;

                if (this._paymentGateway.Length > 0)
                {
                    if (this._paymentGateway.Equals("Paypal"))
                    {
                        if (HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()] != null)
                        {
                            this._userAccount = ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount) as ZNodeUserAccount;
                            this._shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
                        }

                        if (this._shoppingCart == null)
                        {
                            Response.Redirect("~/");
                        }

                        // Paypal Express Checkout - Save Orders
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
                        ZNodeOrderFulfillment order = new ZNodeOrderFulfillment();
                        this._checkout = new ZNodeCheckout();
                        try
                        {
                            log.LogActivityTimerStart();

                            // Get PaymentSettingID for PayPal
                            this._paymentSetting = this.GetByPaymentTypeId(2);
                            this._checkout.PaymentSettingID = this._paymentSetting.PaymentSettingID;
                            order = (ZNodeOrderFulfillment)this._checkout.SubmitOrder();
                        }
                        catch (ZNode.Libraries.ECommerce.Entities.ZNodePaymentException ex)
                        {
                            log.LogActivityTimerEnd(5003, Request.UserHostAddress.ToString(), null, null, null, ex.Message);

                            // Display payment error message
                            lblMsg.Text = ex.Message;
                            return;
                        }
                        catch (Exception exc)
                        {
                            log.LogActivityTimerEnd(5003, null);

                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest : " + exc.Message);

                            // Display error page
                            lblMsg.Text = Resources.CommonCaption.UnableToProcessRequest;
                            return;
                        }

                        // Post order submission
                        if (this._checkout.IsSuccess)
                        {
                            log.LogActivityTimerEnd(5002, order.OrderID.ToString());
                            
                            // Worldpay
                            if (!string.IsNullOrEmpty(this._checkout.ECRedirectURL))
                            {
                                Response.Redirect(this._checkout.ECRedirectURL);
                            }
                            
                            // Make an entry in tracking event for placing an Order 
                            ZNodeTracking tracker = new ZNodeTracking();
                            if (tracker.AffiliateId.Length > 0)
                            {
                                tracker.AccountID = order.AccountID;
                                tracker.OrderID = order.OrderID;
                                tracker.LogTrackingEvent("Placed an Order");
                            }
                            
                            this.PostSubmitOrder(order);

                            // Create session for Order and Shopping cart
                            ZNodeShoppingCart shopping = new ZNodeShoppingCart();
                            Session.Add("OrderDetail", order);

                            // Cookie based persistent cart.
                            ZNodeSavedCart savedCart = new ZNodeSavedCart();
                            savedCart.RemoveSavedCart();

                            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
                            
                            // If shopping cart is null, create in session
                            if (shoppingCart == null)
                            {
                                shoppingCart = new ZNodeShoppingCart();
                                shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                            }

                            Response.Redirect("~/orderreceipt.aspx");
                        }
                        else
                        {
                            log.LogActivityTimerEnd(5001, null, null, null, null, this._checkout.PaymentResponseText);
                            lblMsg.Text = this._checkout.PaymentResponseText;
                            return;
                        }
                    }
                    else if (this._paymentGateway.Equals("2CO"))
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (string key in Request.Form.Keys)
                        {
                            sb.AppendFormat("\n{0}={1}", key, Request.Form[key]);
                        }

                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(sb.ToString());
                        pnlConfirmation.Visible = true;
                        lblMsg.Text = this.GetLocalResourceObject("TrackByPaypal").ToString();
                    }
                    else
                    {
                        pnlConfirmation.Visible = true;
                        lblMsg.Text = this.GetLocalResourceObject("TrackByGoogle").ToString();

                        // Cookie based persistent cart.
                        ZNodeSavedCart savedCart = new ZNodeSavedCart();
                        savedCart.RemoveSavedCart(); 
                    }
                }
                else
                {
                    Response.Redirect("~/");
                }

                // Empty cart & logoff user
                Session.Abandon();
                FormsAuthentication.SignOut();
            }
        }
        #endregion

        #region Post Submit Order - Set Digital Asset

        /// <summary>
        /// Post Order Submission Method
        /// </summary>
        /// <param name="order">Instance of Order</param>
        private void PostSubmitOrder(ZNodeOrderFulfillment order)
        {
            this._checkout.ShoppingCart.PostSubmitOrderProcess();

            ZNodeSupplierOption supplierOption = new ZNodeSupplierOption();
            supplierOption.SubmitOrder(order, this._checkout.ShoppingCart);

            // Set Digital Asset
            int Counter = 0;
            DigitalAssetService digitalAssetService = new DigitalAssetService();

            // Loop through the Order Line Items
            foreach (OrderLineItem orderLineItem in order.OrderLineItems)
            {
                ZNodeShoppingCartItem shoppingCartItem = this._checkout.ShoppingCart.ShoppingCartItems[Counter++];
                
                // Set quantity ordered
                int qty = shoppingCartItem.Quantity;
                
                // Set product id
                int productId = shoppingCartItem.Product.ProductID;
                ZNodeGenericCollection<ZNodeDigitalAsset> AssignedDigitalAssets = new ZNodeGenericCollection<ZNodeDigitalAsset>();
                
                // Get Digital assets for productid and quantity
                AssignedDigitalAssets = ZNodeDigitalAssetList.CreateByProductIdAndQuantity(productId, qty).DigitalAssetCollection;

                // Loop through the digital asset retrieved for this product
                foreach (ZNodeDigitalAsset digitalAsset in AssignedDigitalAssets)
                {
                    DigitalAsset entity = digitalAssetService.GetByDigitalAssetID(digitalAsset.DigitalAssetID);
                    entity.OrderLineItemID = orderLineItem.OrderLineItemID; // Set OrderLineitemId property
                    digitalAssetService.Update(entity); // Update digital asset to the database
                }

                // Set retrieved digital asset collection to shopping product object
                // If product has digital assets, it will display it on the receipt page along with the product name
                shoppingCartItem.Product.ZNodeDigitalAssetCollection = AssignedDigitalAssets;
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type ID</param>
        /// <returns>Returns the Payment Setting</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            int? profileID = 0;

            if (HttpContext.Current.Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
            {
                this._userAccount = ZNodeUserAccount.CurrentAccount();
            }

            if (this._userAccount != null)
            {
                // Get loggedIn user profile
                profileID = this._userAccount.ProfileID; 
            }

            TList<PaymentSetting> list = _pmtServ.GetByPaymentTypeID(paymentTypeID);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (list.Count == 0)
            {
                // Get All Profiles payment setting
                list.Filter = "ActiveInd = true AND ProfileID = null";
            }

            return list[0];
        }
        #endregion
    }
}
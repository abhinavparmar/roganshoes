﻿using System;
using System.Configuration;
using System.Text;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;

namespace WebApp.Controls.Default.Checkout
{
    public partial class PerficientMoreCreditCard : System.Web.UI.UserControl
    {
        #region Private Member
        private int _MoreCreditCardCount = 2;
        private bool _IsMultiCardAllow = false;
        private string _ContainerID = "divMoreCreditCard";
        private int _ControlInd = 1;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or set multiple card allowed value
        /// </summary>
        public int ControlInd
        {
            get { return _ControlInd; }
            set { _ControlInd = value; }
        }

        /// <summary>
        /// Gets or set multiple card allowed value
        /// </summary>
        public string ContainerID
        {
            get { return _ContainerID + ControlInd; }
            set { _ContainerID = ControlInd + value; }
        }

        /// <summary>
        /// return if Tender is Visble or not
        /// </summary>
        public bool IsTenderVisible
        {
            get
            {
                bool isVisible = false;
                if (!string.IsNullOrWhiteSpace(hdnIsVisible.Value))
                {
                    if (hdnIsVisible.Value == "1")
                    {
                        isVisible = true;
                    }
                }
                return isVisible;
            }
        }

        /// <summary>
        /// set validation group name
        /// </summary>
        public string ValidationGroupName
        {
            get
            {
                return "MultiCardValidataion" + ControlInd;
            }
        }

        #endregion

        #region Page Event

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadIntialControlSetting();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //reqCreditCardNumber1.ValidationGroup = ValidationGroup;
            //ZNodeCreditCardValidator1.ValidationGroup = ValidationGroup;
        }

        #endregion

        #region Protected Methods

        protected string GetValidationGroupName()
        {
            return "MultiCardValidataion" + this.ControlInd;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var zeonPayment = new Payments({");
            script.Append("\"Controls\":{");
            //script.AppendFormat("\"{0}\":\"{1}\"", txtTotalCard1.ID, txtTotalCard1.ClientID);
            script.Append("},\"Messages\":{");
            script.AppendFormat("\"MoreCreditCardCount\":\"{0}\"", _MoreCreditCardCount);
            script.Append("}");
            script.Append("});zeonPayment.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "Payments", script.ToString(), true);
        }

        /// <summary>
        /// Check if User opt for multiple Credit card Payment 
        /// </summary>
        /// <returns></returns>
        private bool GetCurrentCardPayemntMode()
        {
            bool isMultiCardModeSel = false;

            isMultiCardModeSel = true;

            return isMultiCardModeSel;
        }

        /// <summary>
        /// Bind Years in Additional Card Sections
        /// </summary>
        private void BindYearsList()
        {
            ListItem defaultItem = new ListItem("-- Year --", "0");
            lstCardYearExpiry.Items.Add(defaultItem);
            defaultItem.Selected = true;

            int currentYear = System.DateTime.Now.Year;
            int counter = 25;

            do
            {
                string itemtext = currentYear.ToString();

                lstCardYearExpiry.Items.Add(new ListItem(itemtext));
                currentYear = currentYear + 1;

                counter = counter - 1;
            }
            while (counter > 0);

        }

        /// <summary>
        /// create dynamic IDs
        /// </summary>
        /// <returns></returns>
        protected string GetDynamicID()
        {
            return this.ContainerID;
        }

        /// <summary>
        /// Set Cvv File path dynamically
        /// </summary>
        /// <returns>Returns the Cvv Path</returns>
        private string GetCvvPath()
        {
            string JavaScript = "javascript:popupWin=window.open('Controls/default/Cvv/cvv.htm','EIN','scrollbars,resizable,width=515,height=300,left=50,top=50');popupWin.focus();";

            hlnkCVVHelp.NavigateUrl = JavaScript;

            return JavaScript;
        }

        /// <summary>
        /// Load Initial Control Setting on Page Load
        /// </summary>
        private void LoadIntialControlSetting()
        {
            // Set Image Path
            imgAmex.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_amex.gif";
            imgDiscover.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_discover.gif";
            imgMastercard.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_mastercard.gif";
            imgVisa.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_visa.gif";

            hlnkCVVHelp.Attributes.Add("OnClientClick", this.GetCvvPath() + "return false");
            //Add Blur Event
            string handle = string.Format("perfiPayment.ManageCreditCardAmount({0},event);", ControlInd);
            txtCardTotal.Attributes.Add("onblur", handle);

            //Bind Year List
            if (!IsPostBack)
            {
                BindYearsList();
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get Credit card Details
        /// </summary>
        /// <returns></returns>
        public ZNode.Libraries.ECommerce.Entities.CreditCard GetCreditCardDetails()
        {
            ZNode.Libraries.ECommerce.Entities.CreditCard currCreditCard = new ZNode.Libraries.ECommerce.Entities.CreditCard();
            if (this.HasAllValidData())
            {
                currCreditCard.CardNumber = txtCreditCardNumber.Text.Trim();
                currCreditCard.CreditCardExp = lstCardMonthExpiry.SelectedValue + "/" + lstCardYearExpiry.SelectedValue;
                currCreditCard.CardSecurityCode = txtCardCVV.Text.Trim();
                currCreditCard.Amount = currCreditCard.SubTotal = decimal.Parse(txtCardTotal.Text);

            }
            return currCreditCard;
        }

        /// <summary>
        /// Get Currunt Card Total
        /// </summary>
        /// <returns></returns>
        public decimal GetCardTotal()
        {
            decimal ordeTotal = 0;
            if (!string.IsNullOrWhiteSpace(txtCreditCardNumber.Text) && !string.IsNullOrWhiteSpace(txtCardTotal.Text))
            {
                decimal.TryParse(txtCardTotal.Text, out ordeTotal);
            }
            return ordeTotal;
        }

        /// <summary>
        /// Check if all data in control is valid
        /// </summary>
        /// <returns></returns>
        public bool HasAllValidData()
        {
            bool isValidData = true;
            if (this.IsTenderVisible)
            {
                isValidData = string.IsNullOrWhiteSpace(txtCreditCardNumber.Text) || (string.IsNullOrWhiteSpace(txtCardTotal.Text) || Decimal.Parse(txtCardTotal.Text) <= 0) || (string.IsNullOrWhiteSpace(txtCardCVV.Text)) || lstCardYearExpiry.SelectedIndex == 0 || lstCardMonthExpiry.SelectedIndex == 0 ? false : true;
            }
            return isValidData;
        }

        #endregion
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="zPayPalOrderSummary.ascx.cs" Inherits="WebApp.zPayPalOrderSummary" %>
<div class="ShoppingCart">
    <h3 class="HeaderStyle">
        <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtOrderInformation"></asp:Localize>
    </h3>
    <div>
        <asp:GridView ID="uxCart" runat="server" AutoGenerateColumns="False" EmptyDataText="Shopping Cart Empty"
            GridLines="None" CellPadding="4" CssClass="Grid"
            meta:resourcekey="uxCartResource1">
            <Columns>
                <asp:TemplateField HeaderText="ITEM" HeaderStyle-HorizontalAlign="Left"
                    meta:resourcekey="TemplateFieldResource2">
                    <ItemTemplate>
                        <div class="ProductImage">
                            <a enableviewstate="false" id="A1" href='<%# DataBinder.Eval(Container.DataItem, "Product.ViewProductLink").ToString() %>'
                                runat="server">
                                <img id="Img1" enableviewstate="false" alt='<%# DataBinder.Eval(Container.DataItem, "Product.ImageAltTag") %>'
                                    title='<%# DataBinder.Eval(Container.DataItem, "Product.ImageAltTag") %>'
                                    border='0' src='<%# DataBinder.Eval(Container.DataItem, "Product.ThumbnailImageFilePath") %>'
                                    runat="server" />
                            </a>
                        </div>
                        <div class="ProductName">
                            <a href='<%# DataBinder.Eval(Container.DataItem, "Product.ViewProductLink").ToString()%>'><%# DataBinder.Eval(Container.DataItem, "Product.Name").ToString()%></a>
                        </div>
                        <div class="Description">
                            <div class="ProductDesc">
                                <p>
                                    <strong>Item#</strong>
                                    <%# DataBinder.Eval(Container.DataItem, "Product.ProductNum").ToString()%>
                                </p>
                                <%# DataBinder.Eval(Container.DataItem, "Product.ShoppingCartDescription").ToString()%>
                            </div>

                        </div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SubTotal" HeaderStyle-HorizontalAlign="Left"
                    meta:resourcekey="TemplateFieldResource3">
                    <ItemTemplate>
                        <div class="Price">
                            <%# DataBinder.Eval(Container.DataItem, "UnitPrice", "{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                        </div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="QTY" HeaderStyle-HorizontalAlign="Left"
                    meta:resourcekey="TemplateFieldResource1">
                    <ItemTemplate>
                        <div class="Quantity">
                            <%# DataBinder.Eval(Container.DataItem, "Quantity").ToString()%>
                        </div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TOTAL" HeaderStyle-HorizontalAlign="Right"
                    ItemStyle-HorizontalAlign="Right" meta:resourcekey="TemplateFieldResource4">
                    <ItemTemplate>
                        <div class="Price">
                            <%# DataBinder.Eval(Container.DataItem, "ExtendedPrice", "{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                        </div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="Footer" />
            <RowStyle CssClass="Row" />
            <HeaderStyle CssClass="Header" />
            <AlternatingRowStyle CssClass="AlternatingRow" />
        </asp:GridView>
    </div>
</div>
<div class="ShoppingCart Form">
    <div class="TotalBox">
        <div class="LeftContent">
        </div>
        <div class="RightContent" align="right">
            <div class="CartPricingSection">
                <div class="Row Clear Right">
                    <div class="ShoppingTotalContent clearfix">
                        <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtsubtotal"></asp:Localize>
                    </div>
                    <div class="ShoppingTotalContent">
                        <asp:Label ID="SubTotal" runat="server" EnableViewState="False"
                            meta:resourcekey="SubTotalResource1"></asp:Label>
                    </div>
                </div>
                <div class="Row Clear SubTotal Right" runat="server" id="rowDiscount" visible="false">
                    <div class="ShoppingTotalContent">
                        <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtDiscount"></asp:Localize>
                    </div>
                    <div class="ShoppingTotalContent">
                        <asp:Label ID="Discount" runat="server" EnableViewState="False"
                            meta:resourcekey="DiscountResource1"></asp:Label>
                    </div>
                </div>
                <div class="Row Clear SubTotal Right" runat="server" id="rowTax" visible="false">
                    <div class="ShoppingTotalContent">
                        <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtTax"></asp:Localize>
                        <asp:Label ID="TaxPct" runat="server" EnableViewState="False"
                            meta:resourcekey="TaxPctResource1"></asp:Label>
                    </div>
                    <div class="ShoppingTotalContent">
                        <asp:Label ID="Tax" runat="server" EnableViewState="False"
                            meta:resourcekey="TaxResource1"></asp:Label>
                    </div>
                </div>
                <div class="Row Clear SubTotal Right" runat="server" id="rowShipping" enableviewstate="false" visible="false">
                    <div class="ShoppingTotalContent">
                        <asp:Localize ID="Localize6" runat="server" meta:resourceKey="txtShipping"></asp:Localize>
                        <span class="shipname">
                            <asp:Literal ID="ltshippingName" runat="server"></asp:Literal>
                        </span>:
                    </div>
                    <div class="ShoppingTotalContent">

                        <asp:Label ID="Shipping" runat="server" EnableViewState="False"
                            meta:resourcekey="ShippingResource2"></asp:Label>
                    </div>
                </div>
                <div class="Row Clear SubTotal Right" runat="server" id="divGiftCardAmount" visible="false">
                    <div class="ShoppingTotalContent">
                        <asp:Localize ID="Localize7" runat="server" meta:resourceKey="lblGiftCard"></asp:Localize>
                    </div>
                    <div class="ShoppingTotalContent">
                        <asp:Label ID="lblGiftCardAmount" runat="server" EnableViewState="False"></asp:Label>
                    </div>
                </div>
                <div class="Row Clear Right TopBorder">
                    <div class="ShoppingTotalAmountContent">
                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:CommonCaption, Total%>" />
                    </div>
                    <div class="ShoppingTotalAmountContent">
                        <asp:Label ID="Total" runat="server" EnableViewState="False"
                            meta:resourcekey="TotalResource2"></asp:Label>
                    </div>
                </div>
            </div>
        </div>

        <div class="ShipBy" id="ShipSection" runat="server" visible="false">
            <h3>Shipping Method</h3>
            <span>
                <p>Selected Shipping Method</p>
                <table border="0" id="rdolstShipping">
                    <tbody>
                        <tr>
                            <td>
                                <asp:Label ID="lblShippingName" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </span>
        </div>
        <div class="Clear"></div>
    </div>
</div>

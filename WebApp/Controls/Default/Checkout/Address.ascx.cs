using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using Zeon.Libraries.Utilities;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using System.Data.SqlClient;


/// <summary>
/// Address update during checkout
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the Address user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_Address : System.Web.UI.UserControl
    {
        #region Private Variables
        private Address _billingAddress = new Address();
        private Address _shippingAddress = new Address();
        private int _accountID = 0;
        private int _billingAddressID = 0;
        private int _shippingAddressID = 0;
        private string _email = string.Empty;
        private ZNodeCheckout checkout;
        private ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();
        private string billingStateCode = string.Empty, shippingStateCode = string.Empty;

        //Zeon Custom Private Member:Starts
        private AddressExtn _billingAddressExtn = new AddressExtn();
        private AddressExtn _shippingAddressExtn = new AddressExtn();
        //Zeon Custom Private Member:Ends
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets accountobject with addresses
        /// </summary>
        public Address BillingAddress
        {
            get
            {
                // Get fields
                if (ddlBillingAddressName.SelectedItem != null)
                {
                    this._billingAddress.AddressID = Convert.ToInt32(ddlBillingAddressName.SelectedItem.Value);
                }

                this._billingAddress.FirstName = Server.HtmlEncode(txtBillingFirstName.Text);
                this._billingAddress.LastName = Server.HtmlEncode(txtBillingLastName.Text);
                this._billingAddress.CompanyName = Server.HtmlEncode(txtBillingCompanyName.Text);
                this._billingAddress.Street = Server.HtmlEncode(txtBillingStreet1.Text);
                this._billingAddress.Street1 = Server.HtmlEncode(txtBillingStreet2.Text);
                this._billingAddress.City = Server.HtmlEncode(txtBillingCity.Text);
                //this._billingAddress.StateCode = Server.HtmlEncode(txtBillingState.Text.ToUpper());
                this._billingAddress.PostalCode = Server.HtmlEncode(txtBillingPostalCode.Text);
                this._billingAddress.CountryCode = lstBillingCountryCode.SelectedValue;
                //Zeon Custom Code:Start
                if (lstBillingState.Visible && ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(lstBillingCountryCode.SelectedValue))
                {
                    this._billingAddress.StateCode = lstBillingState.SelectedValue.IndexOf("AE") >= 0 ? "AE" : lstBillingState.SelectedValue;
                }
                else
                {
                    this._billingAddress.StateCode = Server.HtmlEncode(txtBillingState.Text.ToUpper());
                }
                //Zeon Custom Code:Start
                this._billingAddress.PhoneNumber = Server.HtmlEncode(txtBillingPhoneNumber.Text);

                return this._billingAddress;
            }

            set
            {
                this._billingAddress = value;

                // Set field values
                txtBillingFirstName.Text = Server.HtmlDecode(this._billingAddress.FirstName);
                txtBillingLastName.Text = Server.HtmlDecode(this._billingAddress.LastName);
                txtBillingCompanyName.Text = Server.HtmlDecode(this._billingAddress.CompanyName);
                txtBillingStreet1.Text = Server.HtmlDecode(this._billingAddress.Street);
                txtBillingStreet2.Text = Server.HtmlDecode(this._billingAddress.Street1);
                txtBillingCity.Text = Server.HtmlDecode(this._billingAddress.City);
                //txtBillingState.Text = Server.HtmlDecode(this._billingAddress.StateCode);
                //Zeon Custom Code:Start
                if (lstBillingState.Visible && !string.IsNullOrEmpty(this._billingAddress.CountryCode) && ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(this._billingAddress.CountryCode))
                {
                    if (lstBillingState.Items.FindByValue(this._billingAddress.StateCode) != null)
                    {
                        lstBillingState.SelectedValue = this._billingAddress.StateCode;
                    }
                }
                else
                {
                    txtBillingState.Text = Server.HtmlDecode(this._billingAddress.StateCode);
                }
                //Zeon Custom Code:Start
                txtBillingPostalCode.Text = Server.HtmlDecode(this._billingAddress.PostalCode);

                if (!string.IsNullOrEmpty(this._billingAddress.CountryCode))
                {
                    lstBillingCountryCode.SelectedValue = this._billingAddress.CountryCode;
                }

                txtBillingPhoneNumber.Text = Server.HtmlDecode(this._billingAddress.PhoneNumber);
            }
        }

        /// <summary>
        /// Gets or sets accountobject with addresses
        /// </summary>
        public Address ShippingAddress
        {
            get
            {
                if (chkSameAsBilling.Checked)
                {
                    if (ddlShippingAddressName.SelectedItem != null)
                    {
                        this._shippingAddress.AddressID = Convert.ToInt32(ddlBillingAddressName.SelectedItem.Value);
                    }

                    this._shippingAddress.FirstName = Server.HtmlEncode(txtBillingFirstName.Text);
                    this._shippingAddress.LastName = Server.HtmlEncode(txtBillingLastName.Text);
                    this._shippingAddress.CompanyName = Server.HtmlEncode(txtBillingCompanyName.Text);
                    this._shippingAddress.Street = Server.HtmlEncode(txtBillingStreet1.Text);
                    this._shippingAddress.Street1 = Server.HtmlEncode(txtBillingStreet2.Text);
                    this._shippingAddress.City = Server.HtmlEncode(txtBillingCity.Text);
                    //this._shippingAddress.StateCode = Server.HtmlEncode(txtBillingState.Text.ToUpper());
                    //Zeon Custom Code:Start
                    if (lstBillingState.Visible && ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(lstBillingCountryCode.SelectedValue))
                    {
                        this._shippingAddress.StateCode = lstBillingState.SelectedValue.IndexOf("AE") >= 0 ? "AE" : lstBillingState.SelectedValue;
                    }
                    else
                    {
                        this._shippingAddress.StateCode = Server.HtmlEncode(txtBillingState.Text.ToUpper());
                    }
                    //Zeon Custom Code:Ends
                    this._shippingAddress.PostalCode = Server.HtmlEncode(txtBillingPostalCode.Text);
                    this._shippingAddress.CountryCode = lstBillingCountryCode.SelectedValue;
                    this._shippingAddress.PhoneNumber = Server.HtmlEncode(txtBillingPhoneNumber.Text);
                }
                else
                {
                    if (ddlShippingAddressName.SelectedItem != null)
                    {
                        this._shippingAddress.AddressID = Convert.ToInt32(ddlShippingAddressName.SelectedItem.Value);
                    }

                    this._shippingAddress.FirstName = Server.HtmlEncode(txtShippingFirstName.Text);
                    this._shippingAddress.LastName = Server.HtmlEncode(txtShippingLastName.Text);
                    this._shippingAddress.CompanyName = Server.HtmlEncode(txtShippingCompanyName.Text);
                    this._shippingAddress.Street = Server.HtmlEncode(txtShippingStreet1.Text);
                    this._shippingAddress.Street1 = Server.HtmlEncode(txtShippingStreet2.Text);
                    this._shippingAddress.City = Server.HtmlEncode(txtShippingCity.Text);
                    this._shippingAddress.PostalCode = Server.HtmlEncode(txtShippingPostalCode.Text);
                    this._shippingAddress.CountryCode = lstShippingCountryCode.SelectedValue;
                    // this._shippingAddress.StateCode = Server.HtmlEncode(txtShippingState.Text.ToUpper());
                    //Zeon Custom Code:Start
                    if (lstShippingState.Visible && ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(lstShippingCountryCode.SelectedValue))
                    {
                        this._shippingAddress.StateCode = lstShippingState.SelectedValue.IndexOf("AE") >= 0 ? "AE" : lstShippingState.SelectedValue;
                    }
                    else
                    {
                        this._shippingAddress.StateCode = Server.HtmlEncode(txtShippingState.Text.ToUpper());
                    }
                    //Zeon Custom Code:Ends
                    this._shippingAddress.PhoneNumber = Server.HtmlEncode(txtShippingPhoneNumber.Text);
                }

                return this._shippingAddress;
            }

            set
            {
                this._shippingAddress = value;

                // Set field values
                txtShippingFirstName.Text = Server.HtmlDecode(this._shippingAddress.FirstName);
                txtShippingLastName.Text = Server.HtmlDecode(this._shippingAddress.LastName);
                txtShippingCompanyName.Text = Server.HtmlDecode(this._shippingAddress.CompanyName);
                txtShippingStreet1.Text = Server.HtmlDecode(this._shippingAddress.Street);
                txtShippingStreet2.Text = Server.HtmlDecode(this._shippingAddress.Street1);
                txtShippingCity.Text = Server.HtmlDecode(this._shippingAddress.City);
                // txtShippingState.Text = Server.HtmlDecode(this._shippingAddress.StateCode);
                txtShippingPostalCode.Text = Server.HtmlDecode(this._shippingAddress.PostalCode);
                //Zeon Custom Code:Start
                if (lstShippingState.Visible && !string.IsNullOrEmpty(this._shippingAddress.CountryCode) && ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(this._shippingAddress.CountryCode))
                {
                    if (lstShippingState.Items.FindByValue(this._shippingAddress.StateCode) != null)
                    {
                        lstShippingState.SelectedValue = this._shippingAddress.StateCode;
                    }
                }
                else
                {
                    txtShippingState.Text = Server.HtmlDecode(this._shippingAddress.StateCode);
                }
                //Zeon Custom Code:Ends
                if (!string.IsNullOrEmpty(this._shippingAddress.CountryCode))
                {
                    lstShippingCountryCode.SelectedValue = this._shippingAddress.CountryCode;
                }

                txtShippingPhoneNumber.Text = Server.HtmlDecode(this._shippingAddress.PhoneNumber);
            }
        }

        /// <summary>
        /// Gets or sets the Account Id.
        /// </summary>
        public int AccountID
        {
            get { return this._accountID; }

            set { this._accountID = value; }
        }

        /// <summary>
        /// Gets or sets the billing address
        /// </summary>
        public int BillingAddressID
        {
            get
            {
                // Get fields
                if (ddlBillingAddressName.SelectedItem != null)
                {
                    this._billingAddressID = Convert.ToInt32(ddlBillingAddressName.SelectedItem.Value);
                }

                return this._billingAddressID;
            }

            set
            {
                this._billingAddressID = value;
            }
        }

        /// <summary>
        /// Gets or sets the shipping address
        /// </summary>
        public int ShippingAddressID
        {
            get
            {
                // If billing and shipping address is different then get shipping address Id from dropdown list. 
                if (ddlShippingAddressName.SelectedItem != null && !chkSameAsBilling.Checked)
                {
                    this._shippingAddressID = Convert.ToInt32(ddlShippingAddressName.SelectedItem.Value);
                }
                else if (ddlBillingAddressName.SelectedItem != null)
                {
                    // If shipping address is same as billing address selected.
                    this._shippingAddressID = Convert.ToInt32(ddlBillingAddressName.SelectedItem.Value);
                }

                return this._shippingAddressID;
            }

            set
            {
                this._shippingAddressID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Account email address.
        /// </summary>
        public string Email
        {
            get
            {
                this._email = txtEmailAddress.Text;
                return this._email;
            }

            set
            {
                this._email = value;
                txtEmailAddress.Text = value;
            }
        }

        #region Zeon Public Properties

        /// <summary>
        /// get or set _BillingAddressExtn value
        /// </summary>
        public AddressExtn BillingAddressExtn
        {
            get
            {
                this._billingAddressExtn.AddressID = this.BillingAddress.AddressID;
                this._billingAddressExtn.IsPOBox = chkBillingPOBox.Checked;
                this._billingAddressExtn.AddressExtnID = !string.IsNullOrEmpty(hdnBillingExtnID.Value) ? int.Parse(hdnBillingExtnID.Value) : 0;
                try
                {
                    this._billingAddressExtn.Custom1 = (billingStateCode.IndexOf("AE") >= 0 || this.BillingAddress.StateCode.IndexOf("AE") >= 0) && lstBillingState.Visible ? lstBillingState.SelectedItem.Text : null;
                }
                catch (Exception ex)
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in BillingAddressExtn get!!" + ex.ToString());
                }
                return _billingAddressExtn;
            }
            set
            {
                this._billingAddressExtn = value;
                chkBillingPOBox.Checked = this._billingAddressExtn.IsPOBox != null ? bool.Parse(this._billingAddressExtn.IsPOBox.ToString()) : false;
                try
                {
                    if (lstBillingState.Visible && lstBillingState != null && lstBillingState.Items.Count > 0)
                    {
                        lstBillingState.SelectedValue = (billingStateCode.IndexOf("AE") >= 0 || this.BillingAddress.StateCode.IndexOf("AE") >= 0) && !string.IsNullOrEmpty(this._billingAddressExtn.Custom1)
                          && lstBillingState.Items.FindByText(this._billingAddressExtn.Custom1) != null
                            ? lstBillingState.Items.FindByText(this._billingAddressExtn.Custom1).Value : this.BillingAddress.StateCode;
                    }
                }
                catch (Exception ex)
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in BillingAddressExtn set!!" + ex.ToString());
                }
                hdnBillingExtnID.Value = this._billingAddressExtn.AddressExtnID.ToString();
            }
        }

        /// <summary>
        ///  get or set _ShipingAddressExtn value
        /// </summary>
        public AddressExtn ShippingAddressExtn
        {
            get
            {
                this._shippingAddressExtn.AddressID = this.ShippingAddress.AddressID;
                this._shippingAddressExtn.IsPOBox = chkShippingPOBox.Checked;
                this._shippingAddressExtn.AddressExtnID = !string.IsNullOrEmpty(hdnShippingExtnID.Value) ? int.Parse(hdnShippingExtnID.Value) : 0;
                try
                {
                    this._shippingAddressExtn.Custom1 = (shippingStateCode.IndexOf("AE") >= 0 || this.ShippingAddress.StateCode.IndexOf("AE") >= 0) && lstShippingState.Visible ? lstShippingState.SelectedItem.Text : null;
                }
                catch (Exception ex)
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ShippingAddressExtn set!!" + ex.ToString());
                }
                return _shippingAddressExtn;
            }
            set
            {
                this._shippingAddressExtn = value;
                chkShippingPOBox.Checked = this._shippingAddressExtn.IsPOBox != null ? bool.Parse(this._shippingAddressExtn.IsPOBox.ToString()) : false;
                try
                {
                    if (lstShippingState.Visible && lstShippingState != null && lstShippingState.Items.Count > 0)
                    {
                        lstShippingState.SelectedValue = (shippingStateCode.IndexOf("AE") >= 0 || this.ShippingAddress.StateCode.IndexOf("AE") >= 0)
                            && !string.IsNullOrEmpty(this._shippingAddressExtn.Custom1) && lstShippingState.Items.FindByText(this._shippingAddressExtn.Custom1) != null
                            ? lstShippingState.Items.FindByText(this._shippingAddressExtn.Custom1).Value : this.ShippingAddress.StateCode;
                    }
                }
                catch (Exception ex)
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in ShippingAddressExtn set!!" + ex.ToString());
                }

                hdnShippingExtnID.Value = this._shippingAddressExtn.AddressExtnID.ToString();
            }

        }
        #endregion
        #endregion

        #region Page Load
        /// <summary>
        ///  Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (this.userAccount != null)
            {
                // Add the shopping cart page listrack tracking script.
                ZNodeAnalytics analytics = new ZNodeAnalytics();
                analytics.AnalyticsData.UserAccount = this.userAccount;
                analytics.AnalyticsData.IsAddressPage = true;
                analytics.Bind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Zeon Custom Code: Start
            this.ddlBillingAddressName.Focus();
            // Get checkout object from session
            this.checkout = new ZNodeCheckout();
            if (BuyerUser != null)
            {
                this.AccountID = this.BuyerUser.AccountID;
                userAccount = BuyerUser;
                this.checkout.UserAccount = userAccount;
            }
            else if (this.userAccount != null)
            {
                this.AccountID = this.userAccount.AccountID;
            }
            //Zeon Custom Code: End

            //Znode old code: start
            //if (this.userAccount != null)
            //{
            //    this.AccountID = this.userAccount.AccountID;
            //}
            //Znode old code: end

            if (Request.QueryString["ErrorMsg"] != null && Session["IsAddressValidated"] == null)
            {
                lblError.Text = this.GetLocalResourceObject("AddressValidationFailed").ToString();
            }

            uxStepTracker.Step = 1;

            if (!Page.IsPostBack)
            {
                this.BindCountry();
                //Zeon Custom Code:Starts
                BindStatesByCountryCode(lstBillingCountryCode.SelectedValue, lstBillingCountryCode.ID);
                BindStatesByCountryCode(lstShippingCountryCode.SelectedValue, lstShippingCountryCode.ID);
                //Zeon Custom Code:Ends
                if (this.userAccount == null)
                {
                    // For anonymous user hide the address name
                    txtBillingFirstName.Focus();//zeon custom code:
                    divBillingAddressName.Visible = false;
                    divShippingAddressName.Visible = false;
                }
                else
                {
                    txtEmailAddress.Text = this.userAccount.EmailID;

                    // Load billing address names
                    this.BindAddressNames(true);

                    // Load shipping address names.
                    this.BindAddressNames(false);
                }

                chkSameAsBilling.Checked = false;
                if (this.userAccount != null && userAccount.BillingAddress != null && userAccount.ShippingAddress != null)
                {
                    if (userAccount.BillingAddress.AddressID == userAccount.ShippingAddress.AddressID)
                    {
                        // Both billing and shipping address are same.
                        chkSameAsBilling.Checked = true;

                        //Mantis Issue : 0041651: Shipping Address Changes:V3 :When
                        //selecting shipping address same as billign adress, the shipping address fileds vanishes, 
                        //bad Usability. Please auto fill shipping address and still display the addresses. 
                        //pnlShipping.Visible = false;
                        pnlShipping.CssClass = "Ship-DeActive";
                        this.LoadAddress(this.userAccount.BillingAddress.AddressID, false);

                    }
                    else
                    {
                        this.LoadAddress(this.userAccount.BillingAddress.AddressID, true);
                        this.LoadAddress(this.userAccount.ShippingAddress.AddressID, false);

                        chkSameAsBilling.Checked = false;
                        pnlShipping.Visible = true;
                        pnlShipping.CssClass = "Ship-Active";
                    }
                }

                // If checkout without registration or account not yet have email then display the email address column.
                if (this.userAccount == null || !this.userAccount.UserID.HasValue || string.IsNullOrEmpty(this.userAccount.EmailID))
                {
                    // Add the listrack email capture script only if user account null.
                    if (this.userAccount == null)
                    {
                        ZNodeAnalytics analytics = new ZNodeAnalytics();
                        analytics.AnalyticsData.EmailCaptureContorlId = txtEmailAddress.ClientID;
                        analytics.Bind();
                        string emailCaptureScript = analytics.AnalyticsData.EmailCaptureScript;
                        if (!Page.ClientScript.IsStartupScriptRegistered("EmailCapture"))
                        {
                            Page.ClientScript.RegisterStartupScript(GetType(), "EmailCapture", emailCaptureScript.ToString());
                        }
                    }

                    divEmailAddress.Visible = true;
                }
                else
                {
                    divEmailAddress.Visible = false;
                }

                this.BindAddress();
                txtBillingFirstName.Focus();//PRFT Custom Code
                CreateLog("||Checkout Process Started-Address Page Load||");
            }
            //Zeon Custom Code:Start
            //Genrate Trigger Email Pixel Tag
            //this.GenerateTriggerMailHelperCartPixelTags();
            //Zeon Custom Code:Ends
        }

        /// <summary>
        /// Page_PreRender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Zeon Custom Code:Start
            this.EnablePostalCodeValidation(lstBillingCountryCode);
            this.EnablePostalCodeValidation(lstShippingCountryCode);
            this.SetStateTextBoxVisibility(lstBillingCountryCode, txtBillingState, lstBillingState);
            this.SetStateTextBoxVisibility(lstShippingCountryCode, txtShippingState, lstShippingState);
            //Zeon Custom Code:Ends
        }

        #endregion

        #region Protected Methods and Events
        protected void ChkSameAsBilling_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSameAsBilling.Checked)
            {
                //pnlShipping.Visible = false;
                pnlShipping.CssClass = "Ship-DeActive";

                this.SetSameAsBillingAddress();
            }
            else
            {
                pnlShipping.CssClass = "Ship-Active";
                AddressService addressService = new AddressService();
                Address address = null;
                TList<Address> addressList = new TList<Address>();
                if (this.userAccount != null)
                {
                    addressList = addressService.GetByAccountID(this.userAccount.AccountID);
                    if (addressList.Count == 1)
                    {
                        // Create new shipping address from existing billing address.
                        address = (Address)addressList[0].Clone();
                        address.AddressID = 0;
                        address.Name = "Default Shipping Address";
                        address.IsDefaultBilling = false;
                        address.IsDefaultShipping = true;
                        addressService.Insert(address);

                        // Clear previous default shipping address.
                        foreach (Address currentItem in addressList)
                        {
                            currentItem.IsDefaultShipping = false;
                        }

                        addressService.Update(addressList);

                        this.BindAddressNames(false);
                    }
                    else
                    {
                        if (this.userAccount.ShippingAddress != null && this.userAccount.ShippingAddress.AddressID > 0)
                        {
                            // Load user selected shipping address from session shipping address.
                            address = addressService.GetByAddressID(this.userAccount.ShippingAddress.AddressID);
                        }
                        else
                        {
                            // Load user's default shipping address from account.
                            AccountAdmin accountAdmin = new AccountAdmin();
                            address = accountAdmin.GetDefaultShippingAddress(this.userAccount.AccountID);
                        }
                    }

                    if (address != null)
                    {
                        this.LoadAddress(address.AddressID, false);
                    }
                }

                pnlShipping.Visible = true;
            }
            //Zeon Custom Code:Start
            ShowHideStateControl();
            if (ddlBillingAddressName.Visible == true) { ddlBillingAddressName.Focus(); } else { txtBillingFirstName.Focus(); };
            if (chkSameAsBilling.Checked)
            {
                lbProceedToNext.Focus();
            }
            //Zeon Custom Code:end
        }

        protected void DdlAddressName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            int addressId = Convert.ToInt32(ddl.SelectedValue);
            if (addressId == 0)
            {
                Response.Redirect("address.aspx?returnurl=editAddress.aspx");
            }
            else
            {
                //Zeon Custom Code:Start
                //loading billing and shipping both on chkSameAsBilling is checked
                if (chkSameAsBilling.Checked)
                {
                    this.LoadAddress(addressId, true);
                    this.LoadAddress(addressId, false);
                }
                else
                {
                    if (ddl.ID.ToLower().Contains("billing"))
                    {
                        this.LoadAddress(addressId, true);

                    }
                    else
                    {
                        this.LoadAddress(addressId, false);
                    }
                }
            }
        }

        protected void LbProceedToNext_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            else
            {
                this.ProceedToCheckout();
            }
        }

        /// <summary>
        /// Mantis Isse :0041651: Shipping Address Changes:V3 
        /// Purpose : Shipping Address Same When Click box � should auto fill NOT hide 
        /// </summary>
        protected void SetSameAsBillingAddress()
        {
            this.CopyBillingAddressToShippingAddress();
            pnlShipping.Visible = true;
        }

        private void CopyBillingAddressToShippingAddress()
        {
            try
            {
                txtShippingFirstName.Text = Server.HtmlDecode(this.BillingAddress.FirstName);
                txtShippingLastName.Text = Server.HtmlDecode(this.BillingAddress.LastName);
                txtShippingCompanyName.Text = Server.HtmlDecode(this.BillingAddress.CompanyName);
                txtShippingStreet1.Text = Server.HtmlDecode(this.BillingAddress.Street);
                txtShippingStreet2.Text = Server.HtmlDecode(this.BillingAddress.Street1);
                txtShippingCity.Text = Server.HtmlDecode(this.BillingAddress.City);
                // txtShippingState.Text = Server.HtmlDecode(this._shippingAddress.StateCode);
                txtShippingPostalCode.Text = Server.HtmlDecode(this.BillingAddress.PostalCode);
                chkShippingPOBox.Checked = this.BillingAddressExtn.IsPOBox != null ? bool.Parse(this.BillingAddressExtn.IsPOBox.ToString()) : false;
                //Zeon Custom Code:Start
                if (!string.IsNullOrEmpty(this.BillingAddress.CountryCode) && ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(this.BillingAddress.CountryCode))
                {
                    BindStatesByCountryCode(this.BillingAddress.CountryCode, lstShippingState.ID);
                    if (this.BillingAddress.StateCode.IndexOf("AE") >= 0 && !string.IsNullOrEmpty(this.BillingAddressExtn.Custom1) && lstShippingState.Items.FindByText(this.BillingAddressExtn.Custom1).Value != null)
                    {
                        lstShippingState.SelectedValue = lstShippingState.Items.FindByText(this.BillingAddressExtn.Custom1).Value;
                    }
                    else
                    {
                        if (lstShippingState.Items.FindByValue(this.BillingAddress.StateCode) != null)
                        {
                            lstShippingState.SelectedValue = this.BillingAddress.StateCode;
                        }
                        else
                        {
                            if (lstShippingState.Items.FindByValue(this.BillingAddress.StateCode) != null)
                            {
                                lstShippingState.SelectedValue = this.BillingAddress.StateCode;
                            }
                        }
                    }

                }
                else
                {
                    txtShippingState.Text = Server.HtmlDecode(this.BillingAddress.StateCode);
                }
                //Zeon Custom Code:Ends

                if (!string.IsNullOrEmpty(this.BillingAddress.CountryCode))
                {
                    if (lstShippingCountryCode.Items.FindByValue(this.BillingAddress.CountryCode) != null)
                    {
                        lstShippingCountryCode.SelectedValue = this.BillingAddress.CountryCode;
                    }
                }

                txtShippingPhoneNumber.Text = Server.HtmlDecode(this.BillingAddress.PhoneNumber);

                if (divBillingAddressName.Visible)
                {
                    if (ddlShippingAddressName.Items.FindByValue(ddlBillingAddressName.SelectedValue) != null)
                    {
                        ddlShippingAddressName.SelectedValue = ddlBillingAddressName.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in CopyBillingAddressToShippingAddress!!" + ex.ToString());
            }

        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Load the selected address
        /// </summary>
        /// <param name="addressId">Address Id</param>
        /// <param name="isBillingAddress">IsBilling address (true/false)</param>
        private void LoadAddress(int addressId, bool isBillingAddress)
        {
            AddressService addressService = new AddressService();
            Address address = addressService.GetByAddressID(addressId);

            txtEmailAddress.Text = this.Email;
            if (address != null)
            {
                if (isBillingAddress)
                {
                    this._billingAddress = address;
                    ddlBillingAddressName.SelectedIndex = ddlBillingAddressName.Items.IndexOf(ddlBillingAddressName.Items.FindByValue(addressId.ToString()));
                    txtBillingFirstName.Text = Server.HtmlDecode(address.FirstName);
                    txtBillingLastName.Text = Server.HtmlDecode(address.LastName);
                    txtBillingCompanyName.Text = Server.HtmlDecode(address.CompanyName);
                    txtBillingStreet1.Text = Server.HtmlDecode(address.Street);
                    txtBillingStreet2.Text = Server.HtmlDecode(address.Street1);
                    txtBillingCity.Text = Server.HtmlDecode(address.City);
                    // txtBillingState.Text = address.StateCode;
                    txtBillingPostalCode.Text = address.PostalCode;

                    if (address.CountryCode != null && address.CountryCode.Length > 0 && lstBillingCountryCode.Items.FindByValue(address.CountryCode) != null)
                    {
                        lstBillingCountryCode.SelectedValue = address.CountryCode;
                    }
                    //Zeon Custom Code:Start
                    else
                    {
                        SetDefaultCountryandStateValue(lstBillingCountryCode, "billing", address.CountryCode);
                    }
                    BindStatesByCountryCode(lstBillingCountryCode.SelectedValue, lstBillingCountryCode.ID);
                    if (!string.IsNullOrEmpty(address.StateCode))
                    {
                        if (lstBillingState.Visible && lstBillingState.Items.FindByValue(address.StateCode.ToString()) != null)
                        {
                            lstBillingState.SelectedValue = address.StateCode;
                        }
                        else if (!lstBillingState.Visible)
                        {
                            txtBillingState.Text = address.StateCode;
                        }
                    }
                    else
                    {
                        SetDefaultCountryandStateValue(lstBillingState, "billing", address.CountryCode);
                    }
                    //Zeon Custom Code:Ends

                    txtBillingPhoneNumber.Text = address.PhoneNumber;
                }
                else
                {
                    this._shippingAddress = address;

                    ddlShippingAddressName.SelectedIndex = ddlShippingAddressName.Items.IndexOf(ddlShippingAddressName.Items.FindByValue(addressId.ToString()));
                    txtShippingFirstName.Text = Server.HtmlDecode(address.FirstName);
                    txtShippingLastName.Text = Server.HtmlDecode(address.LastName);
                    txtShippingCompanyName.Text = Server.HtmlDecode(address.CompanyName);
                    txtShippingStreet1.Text = Server.HtmlDecode(address.Street);
                    txtShippingStreet2.Text = Server.HtmlDecode(address.Street1);
                    txtShippingCity.Text = Server.HtmlDecode(address.City);
                    //txtShippingState.Text = address.StateCode;
                    txtShippingPostalCode.Text = address.PostalCode;
                    if (address.CountryCode != null && address.CountryCode.Length > 0 && lstShippingCountryCode.Items.FindByValue(address.CountryCode) != null)
                    {
                        lstShippingCountryCode.SelectedValue = address.CountryCode;
                    }
                    //Zeon Custom Code:Start
                    else
                    {
                        SetDefaultCountryandStateValue(lstShippingCountryCode, "shipping", address.CountryCode);
                    }
                    BindStatesByCountryCode(lstShippingCountryCode.SelectedValue, lstShippingCountryCode.ID);
                    if (!string.IsNullOrEmpty(address.CountryCode))
                    {
                        if (lstShippingState.Visible && lstShippingState.Items.FindByValue(address.StateCode.ToString()) != null)
                        {
                            lstShippingState.SelectedValue = address.StateCode;
                        }
                        else if (!lstShippingState.Visible)
                        {
                            txtShippingState.Text = address.StateCode;
                        }
                    }
                    else
                    {
                        SetDefaultCountryandStateValue(lstShippingState, "shipping", address.StateCode);
                    }
                    //Zeon Custom Code:Ends
                    txtShippingPhoneNumber.Text = address.PhoneNumber;
                }
                if (isBillingAddress) { billingStateCode = address.StateCode; } else { shippingStateCode = address.StateCode; }
            }
            //Zeon Custom Code:Start
            ShowHideStateControl();
            LoadAddressExtnData(addressId, isBillingAddress);
            //Zeon Custom Code:end
        }

        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            // PortalCountry
            ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper = new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
            DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            DataView BillingView = new DataView(countryDs.Tables[0]);
            BillingView.RowFilter = "BillingActive = 1";

            DataView ShippingView = new DataView(countryDs.Tables[0]);
            ShippingView.RowFilter = "ShippingActive = 1";

            lstBillingCountryCode.DataSource = BillingView;
            lstBillingCountryCode.DataTextField = "Name";
            lstBillingCountryCode.DataValueField = "Code";
            lstBillingCountryCode.DataBind();
            ListItem bblistItem = lstBillingCountryCode.Items.FindByValue("US");
            if (bblistItem != null)
            {
                lstBillingCountryCode.SelectedIndex = lstBillingCountryCode.Items.IndexOf(bblistItem);
            }

            lstShippingCountryCode.DataSource = ShippingView;
            lstShippingCountryCode.DataTextField = "Name";
            lstShippingCountryCode.DataValueField = "Code";
            lstShippingCountryCode.DataBind();
            ListItem sslistItem = lstShippingCountryCode.Items.FindByValue("US");
            if (sslistItem != null)
            {
                lstShippingCountryCode.SelectedIndex = lstShippingCountryCode.Items.IndexOf(sslistItem);
            }

            // This will pre-select the billing and shipping address country code
            ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();
            if (_userAccount != null)
            {
                ListItem blistItem = lstBillingCountryCode.Items.FindByValue(_userAccount.BillingAddress.CountryCode);
                if (blistItem != null)
                {
                    lstBillingCountryCode.SelectedIndex = lstBillingCountryCode.Items.IndexOf(blistItem);
                }

                ListItem slistItem = lstShippingCountryCode.Items.FindByValue(_userAccount.ShippingAddress.CountryCode);
                if (slistItem != null)
                {
                    lstShippingCountryCode.SelectedIndex = lstShippingCountryCode.Items.IndexOf(slistItem);
                }
            }

        }

        /// <summary>
        /// Bind address names.
        /// </summary>
        /// <param name="isBillingAddress">isBillingAddress value either true or false</param>
        private void BindAddressNames(bool isBillingAddress)
        {
            AddressService addressService = new AddressService();
            TList<Address> addressList = addressService.GetByAccountID(this.AccountID);
            ListItem newAddressItem = new ListItem("Add New Address", "0");
            int defaultAddressId = 0;

            if (isBillingAddress)
            {
                // If account has no billing address then hide the billing address name
                divBillingAddressName.Visible = addressList.Count == 0 ? false : true;
                foreach (Address currentItem in addressList)
                {
                    if (currentItem.IsDefaultBilling)
                    {
                        currentItem.Name = Server.HtmlDecode(currentItem.Name) + " (default)";
                        defaultAddressId = currentItem.AddressID;
                    }
                    else
                    {
                        //Remove Special Symbol from Name
                        currentItem.Name = Server.HtmlDecode(currentItem.Name);
                    }
                }

                ddlBillingAddressName.DataTextField = "Name";
                ddlBillingAddressName.DataValueField = "AddressID";
                ddlBillingAddressName.DataSource = addressList;
                ddlBillingAddressName.DataBind();
                ddlBillingAddressName.Items.Insert(addressList.Count, newAddressItem);

                this.LoadAddress(defaultAddressId, true);

                ddlBillingAddressName.SelectedIndex = ddlBillingAddressName.Items.IndexOf(ddlBillingAddressName.Items.FindByValue(defaultAddressId.ToString()));
                this.BillingAddressID = defaultAddressId;
            }
            else
            {
                // If account has no shipping address then hide the shipping address name
                divShippingAddressName.Visible = addressList.Count == 0 ? false : true;
                foreach (Address currentItem in addressList)
                {
                    if (currentItem.IsDefaultShipping)
                    {
                        currentItem.Name = Server.HtmlDecode(currentItem.Name) + " (default)";
                        defaultAddressId = currentItem.AddressID;
                    }
                    else
                    {
                        //Remove Special Symbol from Name
                        currentItem.Name = Server.HtmlDecode(currentItem.Name);
                    }
                }

                ddlShippingAddressName.DataTextField = "Name";
                ddlShippingAddressName.DataValueField = "AddressID";
                ddlShippingAddressName.DataSource = addressList;
                ddlShippingAddressName.DataBind();
                ddlShippingAddressName.Items.Insert(addressList.Count, newAddressItem);

                this.LoadAddress(defaultAddressId, false);

                ddlShippingAddressName.SelectedIndex = ddlShippingAddressName.Items.IndexOf(ddlShippingAddressName.Items.FindByValue(defaultAddressId.ToString()));
                this.ShippingAddressID = defaultAddressId;
            }
        }

        #endregion

        /// <summary>
        /// Bind the customer billing and shipping address.
        /// </summary>
        private void BindAddress()
        {
            if (this.checkout.UserAccount != null)
            {
                // Set the account Id.
                this.AccountID = this.checkout.UserAccount.AccountID;
                this.Email = this.checkout.UserAccount.EmailID;

                // if no address was previously entered
                if (this.BillingAddress.FirstName.Length == 0)
                {
                    this.BillingAddress = new Address();
                    this.ShippingAddress = new Address();

                    this.BillingAddress = this.checkout.UserAccount.BillingAddress;
                    this.ShippingAddress = this.checkout.UserAccount.ShippingAddress;
                }
            }
        }

        public void validateCountry(object source, ServerValidateEventArgs args)
        {
            if (chkSameAsBilling.Checked)
            {
                ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper = new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
                DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

                DataTable BillingView = countryDs.Tables[0];
                DataRow[] drBillingView = BillingView.Select("CountryCode='" + this.lstBillingCountryCode.Text + "'");
                foreach (DataRow drView in drBillingView)
                {
                    if (drView["ShippingActive"].ToString() != "True")
                    {
                        args.IsValid = false;
                        valCountry.ErrorMessage = "Country has No Shipping Facility";
                    }
                    else
                    {
                        args.IsValid = true;
                    }
                }

            }
        }

        /// <summary>


        /// <summary>
        /// Hnadles country dropdown selected index event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lstCountry_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlCountryList = (DropDownList)sender;
            BindStatesByCountryCode(ddlCountryList.SelectedValue, ddlCountryList.ID);
            ShowHideStateControl();
            ddlCountryList.Focus();
            //Changes done on date  : 16/Sep/2014 : Purpose :Apply regx validation for US Postal Code
            if (ddlCountryList.SelectedValue.Equals("US", StringComparison.OrdinalIgnoreCase))
            {
                if (ddlCountryList.ID.Equals("lstShippingCountryCode", StringComparison.OrdinalIgnoreCase))
                {
                    rgxShippingPostalCode.Visible = true;
                }
                if (ddlCountryList.ID.Equals("lstBillingCountryCode", StringComparison.OrdinalIgnoreCase))
                {
                    rgxBillingPostalCodeValidation.Visible = true;
                }

            }

            if (!ddlCountryList.SelectedValue.Equals("US", StringComparison.OrdinalIgnoreCase))
            {
                if (ddlCountryList.ID.Equals("lstShippingCountryCode", StringComparison.OrdinalIgnoreCase))
                {
                    rgxShippingPostalCode.Visible = false;
                }
                if (ddlCountryList.ID.Equals("lstBillingCountryCode", StringComparison.OrdinalIgnoreCase))
                {
                    rgxBillingPostalCodeValidation.Visible = false;
                }

            }

            //if (ddlCountryList.ID.Equals("lstShippingCountryCode", StringComparison.OrdinalIgnoreCase))
            //{
            //    lstShippingCountryCode.Focus();
            //}
            //if (ddlCountryList.ID.Equals("lstBillingCountryCode", StringComparison.OrdinalIgnoreCase))
            //{
            //    lstBillingCountryCode.Focus();
            //}

        }

        /// <summary>
        /// Handeles lstBillingCountryCode_TextChanged changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lstBillingCountryCode_TextChanged(object sender, EventArgs e)
        {

            if (chkSameAsBilling.Checked)
            {
                ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper = new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
                DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

                DataTable BillingView = countryDs.Tables[0];
                DataRow[] drBillingView = BillingView.Select("CountryCode='" + this.lstBillingCountryCode.Text + "'");

                ShowHideStateControl();//Zeon Custom code
                foreach (DataRow drView in drBillingView)
                {
                    if (drView["ShippingActive"].ToString() != "True")
                    {
                        valCountry.ErrorMessage = "Country has No Shipping Facility";
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
        /// <summary>
        /// NIVI CODE START:22/11/2018
        /// Modified on 13/12/2018
        /// </summary>
        private void SaveAddressDetails()
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                connection.Open();
                SqlCommand command = new SqlCommand("Nivi_InsertAddressDetails", connection);             
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 0;
                command.Parameters.AddWithValue("@BillingAddress", Convert.ToString(this.BillingAddress));
                command.Parameters.AddWithValue("@ShippingAddress", Convert.ToString(this.ShippingAddress));
                command.Parameters.AddWithValue("@Amount", Convert.ToString(this.checkout.ShoppingCart.Total));
                command.Parameters.AddWithValue("@PortalId", ZNodeConfigManager.SiteConfig.PortalID);
                command.ExecuteNonQuery();
                connection.Close();
                command.Dispose();
            }
        }
        //NIVI CODE END:22/11/2018
        /// <summary>
        /// Validate and save the customer address and move to checkout page.
        /// </summary>
        private void ProceedToCheckout()
        {
            /// NIVI CODE START:22/11/2018
            try
            {                
                SaveAddressDetails();                
            }
            catch (Exception ex1)
            {
                string strlog1 = "SaveAddressDetails Exception: " + ex1 + " Stack Trace: " + ex1.StackTrace + "Continuing further execution";
                Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog1);
            }
            //NIVI CODE END:22/11/2018
            try
            {
                    // Validate billing address statecode
                    if (this.BillingAddress.CountryCode == "US" && this.BillingAddress.StateCode.IndexOf("AE") < 0 && this.BillingAddress.StateCode.Trim().Length != 2)
                    {
                        return;
                    }
                    // Validate shipping address statecode
                    if (this.ShippingAddress.CountryCode == "US" && this.ShippingAddress.StateCode.IndexOf("AE") < 0 && this.ShippingAddress.StateCode.Trim().Length != 2)
                    {
                        return;
                    }
                    CreateLog("||Checkout Process Started-ProceedToCheckout||Email-" + this.Email);
                    // set address objects
                    if (this.checkout.UserAccount != null)
                    {
                        AddressService addressService = new AddressService();
                        Address billingAddress = new Address();
                        Address shippingAddress = new Address();

                        int billingAddressId = this.BillingAddressID;
                        int shippingAddressId = this.ShippingAddressID;

                        TList<Address> addressList = addressService.GetByAccountID(this.checkout.UserAccount.AccountID);

                        // Update or Add billing address
                        billingAddress = this.BillingAddress;
                        // Clear all previous default billing address flag.
                        //Zeon Custom Code to set selected address as default :2015-02-18:Starts
                        foreach (Address currentItem in addressList)
                        {
                            currentItem.IsDefaultBilling = false;
                            currentItem.IsDefaultShipping = false;
                        }
                        //Zeon Custom Code to set selected address as default :2015-02-18:Ends
                        addressService.Update(addressList);

                        if (billingAddressId > 0)
                        {

                            billingAddress = addressService.GetByAddressID(billingAddressId);
                            billingAddress.FirstName = this.BillingAddress.FirstName;
                            billingAddress.LastName = this.BillingAddress.LastName;
                            billingAddress.CompanyName = this.BillingAddress.CompanyName;
                            billingAddress.Street = this.BillingAddress.Street;
                            billingAddress.Street1 = this.BillingAddress.Street1;
                            billingAddress.City = this.BillingAddress.City;
                            billingAddress.StateCode = this.BillingAddress.StateCode.IndexOf("AE") >= 0 ? "AE" : this.BillingAddress.StateCode;
                            billingAddress.PostalCode = this.BillingAddress.PostalCode;
                            billingAddress.CountryCode = this.BillingAddress.CountryCode;
                            billingAddress.PhoneNumber = this.BillingAddress.PhoneNumber;

                            billingAddress.IsDefaultShipping = false;
                            billingAddress.IsDefaultBilling = true;//Zeon Custom Code to set selected address as default :2015-02-18

                            addressService.Update(billingAddress);
                        }
                        else
                        {
                            billingAddress.Name = "Default Billing";
                            billingAddress.AccountID = this.checkout.UserAccount.AccountID;
                            billingAddress.IsDefaultBilling = true;
                            addressService.Insert(billingAddress);
                        }

                        this.BillingAddress = billingAddress;
                        billingAddressId = billingAddress.AddressID;

                        // Update or Add shipping address
                        shippingAddress = this.ShippingAddress;


                        if (shippingAddressId > 0)
                        {

                            shippingAddress = addressService.GetByAddressID(shippingAddressId);
                            shippingAddress.FirstName = this.ShippingAddress.FirstName;
                            shippingAddress.LastName = this.ShippingAddress.LastName;
                            shippingAddress.CompanyName = this.ShippingAddress.CompanyName;
                            shippingAddress.Street = this.ShippingAddress.Street;
                            shippingAddress.Street1 = this.ShippingAddress.Street1;
                            shippingAddress.City = this.ShippingAddress.City;
                            shippingAddress.StateCode = this.ShippingAddress.StateCode.IndexOf("AE") >= 0 ? "AE" : this.ShippingAddress.StateCode;
                            shippingAddress.PostalCode = this.ShippingAddress.PostalCode;
                            shippingAddress.CountryCode = this.ShippingAddress.CountryCode;
                            shippingAddress.PhoneNumber = this.ShippingAddress.PhoneNumber;

                            shippingAddress.IsDefaultShipping = true;
                            shippingAddress.IsDefaultBilling = false;
                            if (shippingAddressId == billingAddressId && chkSameAsBilling.Checked) //Zeon Custom Code to set selected address as default :2015-02-20
                            {
                                shippingAddress.IsDefaultBilling = true;
                                addressService.Update(shippingAddress);
                            }
                            else if (shippingAddressId == billingAddressId && chkSameAsBilling.Checked == false)
                            {
                                addressService.Insert(shippingAddress);
                                shippingAddress.Name = shippingAddress.Name + shippingAddress.AddressID;
                                addressService.Update(shippingAddress);
                                ddlShippingAddressName.SelectedItem.Value = shippingAddress.AddressID.ToString();
                            }
                            else
                            {
                                addressService.Update(shippingAddress);
                            }
                        }
                        else
                        {
                            shippingAddress.Name = "Default Shipping";
                            shippingAddress.AccountID = this.checkout.UserAccount.AccountID;
                            shippingAddress.IsDefaultShipping = true;
                            addressService.Insert(shippingAddress);
                        }

                        this.ShippingAddress = shippingAddress;
                        shippingAddressId = shippingAddress.AddressID;

                        // Set address in session
                        if (billingAddressId > 0)
                        {
                            this.checkout.UserAccount.SetBillingAddress(billingAddressId);
                        }

                        if (shippingAddressId > 0)
                        {
                            this.checkout.UserAccount.SetShippingAddress(shippingAddressId);
                        }

                        this.checkout.UserAccount.CompanyName = billingAddress.CompanyName;
                        this.checkout.UserAccount.BillingAddress = billingAddress;
                        this.checkout.UserAccount.ShippingAddress = shippingAddress;

                        AccountAdmin accountAdmin = new AccountAdmin();
                        Account account = accountAdmin.GetByAccountID(this.checkout.UserAccount.AccountID);
                        if (account != null)
                        {
                            account.Email = this.Email;
                            this.checkout.UserAccount.EmailID = account.Email;
                            accountAdmin.Update(account);
                        }

                        // Update online account email address
                        if (this.checkout.UserAccount.UserID.HasValue)
                        {
                            MembershipUser user = Membership.GetUser();

                            if (user != null)
                            {
                                user.Email = this.checkout.UserAccount.EmailID;
                                Membership.UpdateUser(user);
                            }
                        }
                    }
                    else
                    {
                        // If no address was previously entered
                        if (this.BillingAddress.FirstName.Length > 0)
                        {
                            this.checkout.UserAccount = new ZNodeUserAccount();
                            this.checkout.UserAccount.BillingAddress = this.BillingAddress;
                            this.checkout.UserAccount.ShippingAddress = this.ShippingAddress;
                            this.checkout.UserAccount.EmailID = this.Email;
                            this.checkout.UserAccount.ProfileID = ZNodeProfile.CurrentUserProfile.ProfileID;

                            // Affiliate Settings
                            ZNodeTracking tracking = new ZNodeTracking();
                            string referralAffiliate = tracking.AffiliateId;
                            int referralAccountId = 0;

                            if (!string.IsNullOrEmpty(referralAffiliate))
                            {
                                if (int.TryParse(referralAffiliate, out referralAccountId))
                                {
                                    ZNode.Libraries.DataAccess.Service.AccountService accountServ = new ZNode.Libraries.DataAccess.Service.AccountService();
                                    ZNode.Libraries.DataAccess.Entities.Account account = accountServ.GetByAccountID(referralAccountId);

                                    // Affiliate account exists
                                    if (account != null)
                                    {
                                        if (account.ReferralStatus == "A")
                                        {
                                            this.checkout.UserAccount.ReferralAccountId = account.AccountID;
                                        }
                                    }
                                }
                            }

                            // Add new contact
                            this.checkout.UserAccount.AddUserAccount();
                            this.BillingAddress = this.BillingAddress;
                            AddressService addressService = new AddressService();
                            if (chkSameAsBilling.Checked)
                            {
                                // If both address are same then create single address entity                    
                                this.BillingAddress.Name = "Default Address";
                                this.BillingAddress.IsDefaultBilling = true;
                                this.BillingAddress.IsDefaultShipping = true;
                                this.BillingAddress.AccountID = this.checkout.UserAccount.AccountID;
                                addressService.Insert(this.BillingAddress);

                                // Both address are same
                                this.BillingAddress = this.checkout.UserAccount.BillingAddress;
                                this.ShippingAddress = this.checkout.UserAccount.BillingAddress;
                            }
                            else
                            {
                                // Insert billing address
                                this.BillingAddress.Name = "Default Billing Address";
                                this.BillingAddress.IsDefaultBilling = true;
                                this.BillingAddress.AccountID = this.checkout.UserAccount.AccountID;
                                bool isSuccess = addressService.Insert(this.BillingAddress);
                                this.checkout.UserAccount.SetBillingAddress(this.BillingAddress.AddressID);

                                // Insert shipping address
                                this.ShippingAddress = this.ShippingAddress;
                                this.ShippingAddress.Name = "Default Shipping Address";
                                this.ShippingAddress.IsDefaultShipping = true;
                                this.ShippingAddress.AccountID = this.checkout.UserAccount.AccountID;
                                isSuccess = addressService.Insert(this.ShippingAddress);
                                this.checkout.UserAccount.SetShippingAddress(this.ShippingAddress.AddressID);

                                this.BillingAddress = this.checkout.UserAccount.BillingAddress;
                                this.ShippingAddress = this.checkout.UserAccount.ShippingAddress;
                            }

                            this.checkout.UserAccount.SetBillingAddress(this.BillingAddress.AddressID);
                            this.checkout.UserAccount.SetShippingAddress(this.ShippingAddress.AddressID);

                            AccountProfile accountProfile = new AccountProfile();
                            accountProfile.AccountID = this.checkout.UserAccount.AccountID;
                            accountProfile.ProfileID = ZNodeProfile.CurrentUserProfile.ProfileID;
                            AccountProfileService accountProfileServ = new AccountProfileService();
                            accountProfileServ.Insert(accountProfile);

                            // This block enforces user logn/registration.
                            // Do not allow user to go to any other checkout step unless he is logged-in
                            this.checkout.UserAccount.AddToSession(ZNodeSessionKeyType.UserAccount);
                            //PRFT Custom Code To Solve ProfileID null Issue:Starts
                            if (this.checkout.UserAccount.ProfileID <= 0)
                            {
                                this.checkout.UserAccount.ProfileID = ZNodeConfigManager.SiteConfig.DefaultRegisteredProfileID.GetValueOrDefault();
                                //PRFT Custom Code To Solve ProfileID null Issue:Ends
                                if (this.checkout.UserAccount.ProfileID <= 0)
                                {
                                    HttpContext.Current.Session.Remove(ZNode.Libraries.Framework.Business.ZNodeSessionKeyType.UserAccount.ToString());
                                    HttpContext.Current.Session["ProfileCache"] = null; // Reset profile cache    
                                    HttpContext.Current.Session.Abandon();
                                    FormsAuthentication.SignOut();
                                    Response.Redirect("login.aspx");
                                }
                            }
                            // Get Profile entity for logged-in user profileId
                            ZNode.Libraries.DataAccess.Service.ProfileService profileService = new ZNode.Libraries.DataAccess.Service.ProfileService();
                            ZNode.Libraries.DataAccess.Entities.Profile profile = profileService.GetByProfileID(this.checkout.UserAccount.ProfileID);

                            // Hold this profile object in the session state
                            HttpContext.Current.Session["ProfileCache"] = profile;
                        }
                    }

                    //Zeon Custom Code :Start
                    //PO Box Implementation
                    SaveAddressExtnDetails();
                    this.checkout.UserAccount.BillingAddressExtn = this.BillingAddressExtn;
                    this.checkout.UserAccount.ShippingAddressExtn = this.ShippingAddressExtn;
                    //Zeon Custom Code :Ends

                    // Validate the customer shipping address if address validation enabled.
                    if (ZNodeConfigManager.SiteConfig.EnableAddressValidation != null ||
                        ZNodeConfigManager.SiteConfig.EnableAddressValidation == true)
                    {
                        AccountAdmin _accountAdmin = new AccountAdmin();
                        bool isAddressValid = _accountAdmin.IsAddressValid(this.ShippingAddress);

                        // Do not allow the customer to go to next page if valid shipping address required is enabled. 
                        if (!isAddressValid && ZNodeConfigManager.SiteConfig.RequireValidatedAddress != null &&
                            ZNodeConfigManager.SiteConfig.RequireValidatedAddress == true)
                        {
                            lblError.Text = this.GetLocalResourceObject("AddressValidationFailed").ToString();

                            // Clear the session value
                            Session["IsAddressValidated"] = null;
                            return;
                        }
                    }
                    //Zeon Custom Code:start
                    if (BuyerUser != null)
                    {
                        Session["BuyerUser"] = this.checkout.UserAccount;
                    }
                    //Zeon Custom Code:End
                    Session["IsAddressValidated"] = "true";
                    try
                    {
                        Response.Redirect("checkout.aspx", false);
                    }
                    catch (Exception ex)
                    {
                    }

                }
                catch (Exception ex)
                {
                    Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error While ProceedToCheckout() in Address.ascx !!" + ex.StackTrace.ToString());

                    string strlog = "\nEdit Address page . \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
                    strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, "", "");
                    Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);
                }
            
        }

        #region ZEON:Trigger Email Implementation for Abandon Cart

        /// <summary>
        /// Genrate Trigger Email Pixel Tag
        /// </summary>
        private void GenerateTriggerMailHelperCartPixelTags()
        {
            TriggerMailHelper triggerMailCartHelper = new TriggerMailHelper();

            ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();

            string tiggerMailPixelTag = triggerMailCartHelper.AddCheckoutShippingBillingPagePixcelTag(userAccount, this.checkout.ShoppingCart);

            if (!string.IsNullOrEmpty(tiggerMailPixelTag))
            {
                TriggerMailCartImagesHolder.Text = tiggerMailPixelTag;
            }
        }

        #endregion

        #region Zeon Custom Code

        /// <summary>
        /// Show and Hide State Control
        /// </summary>
        private void ShowHideStateControl()
        {
            if (ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(lstBillingCountryCode.SelectedValue))
            {
                lstBillingState.Visible = true;
                //lstBillingState.Focus();
                txtBillingState.Visible = false;
                revBillingStateCode.Enabled = false;
                revBillingState.Enabled = true;
            }
            else
            {
                lstBillingState.Visible = false;
                txtBillingState.Visible = true;
                revBillingStateCode.Enabled = true;
                revBillingState.Enabled = false;
                //txtBillingState.Focus();
            }
            if (ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(lstShippingCountryCode.SelectedValue))
            {
                lstShippingState.Visible = true;
                txtShippingState.Visible = false;
                revShippingStateCode.Enabled = false;
                revShippingState.Enabled = true;
                //lstShippingState.Focus();
            }
            else
            {
                lstShippingState.Visible = false;
                txtShippingState.Visible = true;
                revShippingStateCode.Enabled = true;
                revShippingState.Enabled = false;
                //txtShippingState.Focus();
            }
        }

        /// <summary>
        /// Bind State as per countryCode
        /// </summary>
        private void BindStatesByCountryCode(string countryCode, string currentListControlId)
        {
            bool isStateListEnable = false;
            //TList<State> stateList = null;
            if (ConfigurationManager.AppSettings["AddressCustomCountry"] != null && ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(countryCode))
            {
                //Zeon Custom Code:Starts
                //ZNode Old Code
                //StateService stateService = new StateService();
                //string filterQuery = "CountryCode='" + countryCode + "'";
                //stateList = stateService.Find(filterQuery);
                ZNode.Libraries.DataAccess.Custom.ZStateHelper zStateHelper = new ZNode.Libraries.DataAccess.Custom.ZStateHelper();
                DataTable dtStatesList = zStateHelper.GetStateListByCountryCode(countryCode);
                //Zeon Custom Code:Ends
                if (dtStatesList != null && dtStatesList.Rows.Count > 0)
                {
                    BindStateListControl(currentListControlId, dtStatesList);
                    isStateListEnable = true;
                }
            }
            SetStateBindingControls(isStateListEnable, currentListControlId);

        }

        /// <summary>
        /// set visibility of state controls as per country data.
        /// </summary>
        /// <param name="isStateList"></param>
        /// <param name="currentListControl"></param>
        private void SetStateBindingControls(bool isStateList, string currentListControlID)
        {
            if (currentListControlID.ToLower().IndexOf("shipping") > 0)
            {
                lstShippingState.Visible = revShippingState.Enabled = revShippingState.Visible = isStateList;
                txtShippingState.Visible = revShippingStateCode.Visible = revShippingStateCode.Enabled = !isStateList;
            }
            else if (currentListControlID.ToLower().IndexOf("billing") > 0)
            {
                lstBillingState.Visible = revBillingState.Enabled = revShippingState.Enabled = revShippingState.Visible = isStateList;
                txtBillingState.Visible = revBillingStateCode.Enabled = revShippingStateCode.Enabled = revShippingStateCode.Visible = !isStateList;
            }
        }

        /// <summary>
        /// Bind State Control List
        /// </summary>
        /// <param name="countryListControlID"></param>
        /// <param name="stateList"></param>
        private void BindStateListControl(string countryListControlID, DataTable stateList)
        {
            ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();
            if (countryListControlID.ToLower().IndexOf("shipping") > 0)
            {
                if (stateList.Rows.Count > 0)
                {
                    this.ClearStateDropDown(false);

                    lstShippingState.AppendDataBoundItems = true;
                    lstShippingState.DataSource = stateList;
                    lstShippingState.DataTextField = "Name";
                    lstShippingState.DataValueField = "Code";
                    lstShippingState.DataBind();
                    lstShippingState.Items.Insert(0, new ListItem(Resources.CommonCaption.DefaultSelectedState.ToString(), Resources.CommonCaption.DefaultSelectedState.ToString()));
                    if (_userAccount != null && _userAccount.ShippingAddress != null)
                    {
                        lstShippingState.SelectedIndex = 0;
                        ListItem slistItem = lstShippingState.Items.FindByValue(_userAccount.ShippingAddress.StateCode);
                        if (slistItem != null && lstShippingState.Items.IndexOf(slistItem) >= 0)
                        {
                            lstShippingState.SelectedIndex = lstShippingState.Items.IndexOf(slistItem);
                        }
                    }
                    else
                    {
                        lstShippingState.SelectedIndex = 0;
                    }
                }
            }
            else if (countryListControlID.ToLower().IndexOf("billing") > 0)
            {
                if (stateList.Rows.Count > 0)
                {
                    this.ClearStateDropDown(true);

                    lstBillingState.AppendDataBoundItems = true;
                    lstBillingState.DataSource = stateList;
                    lstBillingState.DataTextField = "Name";
                    lstBillingState.DataValueField = "Code";
                    lstBillingState.DataBind();
                    lstBillingState.Items.Insert(0, new ListItem(Resources.CommonCaption.DefaultSelectedState.ToString(), Resources.CommonCaption.DefaultSelectedState.ToString()));
                    if (_userAccount != null && _userAccount.BillingAddress != null)
                    {
                        lstBillingState.SelectedIndex = 0;
                        ListItem blistItem = lstBillingState.Items.FindByValue(_userAccount.BillingAddress.StateCode);
                        if (blistItem != null && lstBillingState.Items.IndexOf(blistItem) >= 0)
                        {
                            lstBillingState.SelectedIndex = lstBillingState.Items.IndexOf(blistItem);
                        }
                    }
                    else
                    {
                        lstBillingState.SelectedIndex = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Set default value in dropdown
        /// </summary>
        /// <param name="ddlList"></param>
        private void SetDefaultCountryandStateValue(DropDownList ddlList, string addressType, string code)
        {
            bool userItemExist = false;
            // This will pre-select the billing and shipping address country code
            ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();
            if (_userAccount != null && !string.IsNullOrEmpty(code))
            {
                string findValue = ddlList.ID.ToLower().IndexOf("country") > 0 ? _userAccount.BillingAddress.CountryCode : _userAccount.BillingAddress.StateCode;
                if (ddlList.ID.ToLower().IndexOf("country") > 0)
                {
                    if (addressType.ToLower().Equals("billing")) { findValue = userAccount.BillingAddress.CountryCode; } else { findValue = userAccount.ShippingAddress.CountryCode; }
                }
                else
                {
                    if (addressType.ToLower().Equals("billing")) { findValue = userAccount.BillingAddress.StateCode; } else { findValue = userAccount.ShippingAddress.StateCode; }
                }
                ListItem blistItem = ddlList.Items.FindByValue(findValue);
                if (blistItem != null)
                {
                    ddlList.SelectedIndex = ddlList.Items.IndexOf(blistItem);
                    userItemExist = true;
                }
            }
            if (!userItemExist)
            {
                ddlList.SelectedValue = ddlList.ID.ToLower().IndexOf("country") > 0 ? "US" : ddlList.SelectedValue = Resources.CommonCaption.DefaultSelectedState.ToString();
            }
        }

        /// <summary>
        /// load PO Box Detail of Address
        /// </summary>
        /// <param name="addressID">int</param>
        /// <param name="isBillingAddress">bool</param>
        private void LoadAddressExtnData(int addressID, bool isBillingAddress)
        {
            AddressExtnService addrssExtnSer = new AddressExtnService();
            TList<AddressExtn> addressList = addrssExtnSer.GetByAddressID(addressID);
            if (addressList != null && addressList.Count > 0)
            {
                if (isBillingAddress)
                {
                    this.BillingAddressExtn = addressList[0];
                }
                else
                {
                    this.ShippingAddressExtn = addressList[0];
                }
            }
        }

        /// <summary>
        /// Save and Update Address Extn Details
        /// </summary>
        private void SaveAddressExtnDetails()
        {
            AddressExtn billingAddExtn = new AddressExtn();
            AddressExtn shippingAddExtn = new AddressExtn();
            if (chkSameAsBilling.Checked && !this.BillingAddressExtn.IsPOBox.ToString().Equals(chkBillingPOBox.Checked))
            {
                this.BillingAddressExtn = this.ShippingAddressExtn = SaveAddressExtnData(this.BillingAddressExtn);
            }
            else
            {
                if (this.BillingAddressExtn.IsPOBox != null && !this.BillingAddressExtn.IsPOBox.ToString().Equals(chkBillingPOBox.Checked))
                {
                    this.BillingAddressExtn = SaveAddressExtnData(this.BillingAddressExtn);
                }
                if (this.ShippingAddressExtn.IsPOBox != null && !this.ShippingAddressExtn.IsPOBox.ToString().Equals(chkShippingPOBox.Checked))
                {
                    if (this.ShippingAddressExtn.AddressExtnID == this.BillingAddressExtn.AddressExtnID)
                    {
                        hdnShippingExtnID.Value = string.Empty;
                    }
                    this.ShippingAddressExtn = SaveAddressExtnData(this.ShippingAddressExtn);

                }
            }
        }

        /// <summary>
        ///  save Address Extn
        /// </summary>
        /// <param name="addressExtn">AddressExtn</param>
        /// <returns>AddressExtn</returns>
        private AddressExtn SaveAddressExtnData(AddressExtn addressExtn)
        {
            AddressExtnService addrssExtnSer = new AddressExtnService();
            try
            {
                if (addressExtn != null && addressExtn.AddressID > 0)
                {
                    if (addressExtn.AddressExtnID > 0)
                    {
                        //Update Details
                        addrssExtnSer.Update(addressExtn);
                    }
                    else
                    {
                        addrssExtnSer.Save(addressExtn);
                    }
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in SaveAddressExtnData!!" + this.Request.Url + addressExtn.AddressID + ex.ToString());
            }
            return addressExtn;
        }

        /// <summary>
        /// Add Buyer User
        /// </summary>
        public ZNodeUserAccount BuyerUser
        {
            get
            {
                if (Session["BuyerUser"] != null)
                {
                    return (ZNodeUserAccount)Session["BuyerUser"];
                }
                return null;
            }
        }

        /// <summary>
        /// Fix Issue : THE 'SELECTEDINDEX' AND 'SELECTEDVALUE' ATTRIBUTES ARE MUTUALLY EXCLUSIVE
        /// </summary>
        /// <param name="isBilling"></param>
        private void ClearStateDropDown(bool isBilling)
        {
            if (isBilling)
            {
                this.lstBillingState.ClearSelection();
                this.lstBillingState.DataSource = null;
                this.lstBillingState.SelectedIndex = -1;
                this.lstBillingState.DataBind();
                this.lstBillingState.Dispose();
                this.lstBillingState.Items.Clear();
            }
            else
            {
                this.lstShippingState.ClearSelection();
                this.lstShippingState.DataSource = null;
                this.lstShippingState.SelectedIndex = -1;
                this.lstShippingState.DataBind();
                this.lstShippingState.Dispose();
                this.lstShippingState.Items.Clear();
            }
        }

        /// <summary>
        /// Set Postal Code Validation
        /// </summary>
        /// <param name="ddlCountryList"></param>
        private void EnablePostalCodeValidation(DropDownList ddlCountryList)
        {
            //Changes done on date  : 16/Sep/2014 : Purpose :Apply regx validation for US Postal Code
            if (ddlCountryList.SelectedValue.Equals("US", StringComparison.OrdinalIgnoreCase))
            {
                if (ddlCountryList.ID.Equals("lstShippingCountryCode", StringComparison.OrdinalIgnoreCase))
                {
                    rgxShippingPostalCode.Visible = true;
                }
                if (ddlCountryList.ID.Equals("lstBillingCountryCode", StringComparison.OrdinalIgnoreCase))
                {
                    rgxBillingPostalCodeValidation.Visible = true;
                }

            }

            if (!ddlCountryList.SelectedValue.Equals("US", StringComparison.OrdinalIgnoreCase))
            {
                if (ddlCountryList.ID.Equals("lstShippingCountryCode", StringComparison.OrdinalIgnoreCase))
                {
                    rgxShippingPostalCode.Visible = false;
                }
                if (ddlCountryList.ID.Equals("lstBillingCountryCode", StringComparison.OrdinalIgnoreCase))
                {
                    rgxBillingPostalCodeValidation.Visible = false;
                }

            }

        }

        /// <summary>
        /// Set State Drop Down Visibilty
        /// </summary>
        /// <param name="lstCountry"></param>
        /// <param name="txtState"></param>
        /// <param name="lstState"></param>
        private void SetStateTextBoxVisibility(DropDownList lstCountry, TextBox txtState, DropDownList lstState)
        {
            txtState.Visible = true;
            lstState.Visible = false;
            string selectedValue = lstCountry.SelectedValue;
            string[] countryCodeArray = ConfigurationManager.AppSettings["AddressCustomCountry"] != null ? ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().Split(',') : null;
            if (countryCodeArray != null && countryCodeArray.Length > 0)
            {
                foreach (string str in countryCodeArray)
                {
                    if (selectedValue.Equals(str))
                    {
                        lstState.Visible = true;
                        txtState.Visible = false;
                        break;
                    }
                }

            }
        }

        private void CreateLog(string eventName)
        {
            string strlog = "\n" + eventName + ". \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
            strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, "", "");
            Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);
        }
        #endregion
    }
}
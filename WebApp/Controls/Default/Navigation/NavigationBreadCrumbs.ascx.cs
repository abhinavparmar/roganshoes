using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Tagging;
using ZNode.Libraries.Framework.Business;
using Category = ZNode.Libraries.DataAccess.Entities.Category;

/// <summary>
/// Displays the bread crumb menu for the multifront pages
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the NavigationBreadCrumbs user control class.
    /// </summary>
    public partial class Controls_Default_Navigation_NavigationBreadCrumbs : System.Web.UI.UserControl
    {
        #region Private Variables
        private int _categoryId = 0;
        private int _productId = 0;
        private string _breadcrumbcid = string.Empty;
        private string tagValues = string.Empty;
        private ZNodeNavigation navigation = new ZNodeNavigation();
        private ZNodeTag Tagging = new ZNodeTag();
        private DataSet tagNameDataset = new DataSet();
        private bool _isProductPage = false;//Zeon Custom Code
        #endregion

        #region Public properties

        public string BreadCrumbCid
        {
            get { return this._breadcrumbcid; }
            set { this._breadcrumbcid = value; }
        }

        #region Zeon Custom Properties
        /// <summary>
        /// get or set value of _isProductPage
        /// </summary>
        public bool IsProductPage
        {
            get { return _isProductPage; }
            set { _isProductPage = value; }
        }
        #endregion

        #endregion

        #region Helper Methods

        public string IsSEOUrl()
        {
            string zcid = string.Empty;

            if (Request.QueryString["zcid"] != null)
            {
                Session["BreadCrumzcid"] = Request.QueryString["zcid"];
                zcid = Request.QueryString["zcid"].ToString();
            }
            else if (Session["BreadCrumzcid"] != null && Request.QueryString["zpid"] == null)
            {
                zcid = Session["BreadCrumzcid"].ToString();
            }
            else if (Request.QueryString["zpid"] != null)
            {
                ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                TList<ProductCategory> pcategories = service.GetByProductID(Convert.ToInt32(Request.QueryString["zpid"]));
                if (pcategories.Count > 0)
                {
                    zcid = pcategories[0].CategoryID.ToString();
                }
            }
            else
            {
                zcid = "0";
            }

            ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
            Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));

            if (category != null)
            {
                string seourl = string.Empty;
                if (category.SEOURL != null)
                {
                    seourl = category.SEOURL;
                }

                return ZNodeSEOUrl.MakeURL(category.CategoryID.ToString(), SEOUrlType.Category, seourl);
            }

            return ZNodeSEOUrl.MakeURL("0", SEOUrlType.Category, string.Empty);
        }

        public string GetPageURL()
        {
            return this.IsSEOUrl();
        }

        public void GetTagsBreadCrumbs()
        {
            // Default path        
            StringBuilder breadCrumbPath = new StringBuilder();
            StringBuilder queryStringPath = new StringBuilder();

            MessageConfigAdmin messageadmin = new MessageConfigAdmin();
            MessageConfig messageconfig = new MessageConfig();

            // ResolveUrl - Converts this URL into one that is usable on the requesting client.
            messageconfig = messageadmin.GetByKeyPortalIDLocaleID(ZNodeMessageKey.BreadCrumbsHomeLinkText.ToString(), ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
            if (messageconfig != null)
            {
                breadCrumbPath.Append(this.navigation.CreateLink(ResolveUrl("~/"), messageconfig.Value));
            }

            if (Request.Params["zcid"] != null)
            {
                ZNodeCategory category = (ZNodeCategory)HttpContext.Current.Items["Category"];

                // Get bread crumb string based on category
                breadCrumbPath.Append(category.CategoryPath);
            }
            else
            {
                if (Session["BreadCrumzcid"] != null)
                {
                    ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                    ProductCategory pcategory = service.GetByProductIDCategoryID(Convert.ToInt32(Request.QueryString["zpid"]), Convert.ToInt32(Session["BreadCrumzcid"]));

                    if (pcategory != null)
                    {
                        this._categoryId = pcategory.CategoryID;
                    }
                }

                // Get bread crumb string based on category/ product
                breadCrumbPath.Append(this.navigation.GetBreadCrumbPath(this._categoryId, this._productId, " &gt; ", ZNodeConfigManager.SiteConfig.PortalID));

                if (!string.IsNullOrEmpty(this.navigation.BreadCrumbCid))
                {
                    this._categoryId = Convert.ToInt32(this.navigation.BreadCrumbCid);

                    Session["BreadCrumzcid"] = this.navigation.BreadCrumbCid;
                }
            }
            int i = 0;
            foreach (String key in Request.QueryString.AllKeys)
            {
                if (key == null)
                    continue;
                else if (key.ToLower() == "zcid" || key.ToLower() == "page" || key.ToLower() == "size" || key.ToLower() == "sort")
                    continue;
                else
                    i++;

            }
            if (i == 0)
                Session["TaggedValues"] = null;

            if (Session["TaggedValues"] != null)
            {
                this.tagValues = Session["TaggedValues"].ToString();

                if (!string.IsNullOrEmpty(this.tagValues))
                {
                    // Bind Tag BreadCrumb Label
                    this.BindLabelControl(this.tagValues);
                }
            }

            // Set label to breadcrumb
            lblPath.Text = breadCrumbPath.ToString();
            //Zeon Custom Code:Format Bread Crumb As per SEO Meta Tag requirement
            if (IsProductPage)
            {
                lblPath.Text = FormatBreadCrumbForProductPage(breadCrumbPath.ToString());
            }
            //Zeon Custom Code:Format Bread Crumb As per SEO Meta Tag requirement


            // Set the last categoryId
            this._breadcrumbcid = this.navigation.BreadCrumbCid;
        }

        /// <summary>
        /// Bind the Label control
        /// </summary>
        /// <param name="_selectedTagValues">Selected Tag Values</param>
        public void BindLabelControl(string _selectedTagValues)
        {
            // Clear the controls
            ControlsPlaceHolder.Controls.Clear();

            // String Builder
            StringBuilder labelTagValues = new StringBuilder();

            this.tagNameDataset = this.Tagging.GetBreadCrumbsByTagIds(_selectedTagValues);
            this.tagNameDataset.Tables[0].PrimaryKey = new DataColumn[] { this.tagNameDataset.Tables[0].Columns["TagId"] };

            foreach (string tags in _selectedTagValues.Split(','))
            {
                if (!string.IsNullOrEmpty(tags))
                {
                    DataRow tagRow = this.tagNameDataset.Tables[0].Rows.Find(tags);

                    if (tagRow != null)
                    {
                        // Start to add the dynamic controls here
                        ControlsPlaceHolder.Controls.Add(new LiteralControl("<span>"));

                        Literal literal1 = new Literal();
                        literal1.Text = " &raquo; ";
                        literal1.EnableViewState = false;

                        HyperLink linkBtnTagName = new HyperLink();
                        linkBtnTagName.ID = "breadcrumblnkBtnTag" + tagRow["TagId"].ToString();
                        linkBtnTagName.Text = tagRow["TagName"].ToString();
                        linkBtnTagName.CssClass = "linkBtnTagText";
                        //linkBtnTagName.ToolTip = tagRow["TagName"].ToString();
                        linkBtnTagName.NavigateUrl = this.ConstructQueryStringFromSessionValues(tagRow["TagId"].ToString());
                        linkBtnTagName.EnableViewState = false;

                        ControlsPlaceHolder.Controls.Add(literal1);
                        ControlsPlaceHolder.Controls.Add(linkBtnTagName);
                        ControlsPlaceHolder.Controls.Add(new LiteralControl("</span>"));
                    }
                }
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            // Get category id from querystring  
            if (Request.Params["zcid"] != null)
            {
                int.TryParse(Request.Params["zcid"], out this._categoryId);
                Session["BreadCrumzcid"] = this._categoryId;
            }
            else
            {
                if (Session["BreadCrumzcid"] != null && Request.Params["zpid"] != null)
                {
                    // Get categoryid from session
                    // this._categoryId = int.Parse(Session["BreadCrumzcid"].ToString());
                }
            }

            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                int.TryParse(Request.Params["zpid"], out this._productId);
            }

            this.GetTagsBreadCrumbs();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Construct QueryString key name.
        /// </summary>
        /// <param name="squeryname">query name string</param>
        /// <returns>Returns the formatted Query Name</returns>
        private string QKeyName(string squeryname)
        {
            return squeryname.Replace(" ", "_") + "=";
        }

        /// <summary>
        /// Construct the query string from session values
        /// </summary>
        /// <param name="tagId">String represents the Tag Id</param>
        /// <returns>Returns the Query String from session values</returns>
        private string ConstructQueryStringFromSessionValues(string tagId)
        {
            string qstring = string.Empty;

            ZNode.Libraries.DataAccess.Service.TagService tagService = new ZNode.Libraries.DataAccess.Service.TagService();
            ZNode.Libraries.DataAccess.Service.TagGroupService tagGroupService = new ZNode.Libraries.DataAccess.Service.TagGroupService();

            string[] keyValues = Session["TaggedValues"].ToString().Split(',');

            foreach (string sessionkey in keyValues)
            {
                if (!string.IsNullOrEmpty(sessionkey))
                {
                    ZNode.Libraries.DataAccess.Entities.Tag tag = tagService.GetByTagID(Convert.ToInt32(sessionkey));

                    if (tag != null)
                    {
                        qstring = qstring + (qstring.Trim().Length == 0 ? string.Empty : "&") + this.QKeyName(tagGroupService.GetByTagGroupID(tag.TagGroupID.Value).TagGroupLabel) + Server.UrlEncode(tag.TagName);
                    }

                    if (sessionkey == tagId)
                    {
                        break;
                    }
                }
            }

            string qurl = string.Empty;

            if (qstring.Trim().Length > 0)
            {
                string url = this.GetPageURL();
                qurl = url + ((url.IndexOf('?') > 0) ? "&" : "?") + qstring.Trim();
            }

            return qurl;
        }
        #endregion

        #region Zeon Custom Code
        /// <summary>
        /// Added Custom SEO meta Tag On Product Page
        /// </summary>
        /// <param name="navigationLink">string</param>
        /// <returns>string</returns>
        private string FormatBreadCrumbForProductPage(string navigationLink)
        {
            string[] delmtr = { " &gt; " };
            StringBuilder formattedLink = new StringBuilder();
            int splittedStrInd = 0;
            if (!string.IsNullOrEmpty(navigationLink))
            {
                string[] splittedLinks = navigationLink.Split(delmtr, StringSplitOptions.None);
                foreach (string navLink in splittedLinks)
                {
                    splittedStrInd++;
                    string strReplace = System.Text.RegularExpressions.Regex.Replace(navLink, "(<[a|A][^>]*>|</[a|A]>)", "");
                    string strAltSpanTag = string.Format(Resources.CommonCaption.ProductBreadCrumbAncharText, strReplace);
                    string splitText = Resources.CommonCaption.ProductBreadCrumbLinkSplit;
                    string updatedLink = navLink.Replace(strReplace, strAltSpanTag);
                    string divFormat = splittedStrInd < splittedLinks.Length ? string.Format(Resources.CommonCaption.ProductBreadCrumbLinkMetaDiv, updatedLink.Replace("<a", Resources.CommonCaption.ProductAncharWithItemProp) + splitText) : string.Format(Resources.CommonCaption.ProductBreadCrumbLinkMetaDiv, updatedLink.Replace("<a", Resources.CommonCaption.ProductAncharWithItemProp));
                    formattedLink.Append(divFormat);
                }
            }
            return formattedLink.ToString().Replace("'", "\"");
        }
        #endregion
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZPageNotFound.ascx.cs" Inherits="WebApp.Controls.Default.Common.ZPageNotFound" %>
<div class="ContentContainer">
    <div class="ErrorPage">
        <div>
            <div class="PageNotFound">
                <div class="PageTitle">
                    <asp:Label ID="lblTitle" runat="server" meta:resourceKey="lblTitleresourcekey"></asp:Label>
                </div>
            </div>
        </div>
        <div id="MiddleColumns">
            <p>
                <asp:Localize ID="locContact" runat="server" meta:resourceKey="locContactresourcekey" ></asp:Localize></p>
            <ul>
                <li class="ContactNo">
                    <asp:Localize ID="locCustomerServicePhone" runat="server"></asp:Localize></li>
                <li>OR</li>
                <li class="Email">
                    <a runat="server" id="emailLink" title="Email Customer Service">Send us an Email</a>
                </li>
            </ul>
        </div>
    </div>
</div>

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the HomeQuickSearch user control class.
    /// </summary>
    public partial class Controls_Default_Common_HomeQuickSearch : System.Web.UI.UserControl
    {
         #region Private Variables
        private string searchLink = string.Empty;      
        #endregion       
        
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            string theme = ZNodeCatalogManager.Theme;
            
            // If category theme wise theme selected then apply the current category theme.
            if (Session["CurrentCategoryTheme"] != null)
            {
                theme = Session["CurrentCategoryTheme"].ToString();
            }

            // Set button image
            btnSearch.ImageUrl = string.Format("~/themes/{0}/Images/search.gif", theme);

            ZNodeUrl url = new ZNodeUrl();
            this.searchLink = "~/search.aspx";
        }

        #endregion

        #region Events
        protected void BtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            ZNodeUrl url = new ZNodeUrl();

            System.Text.StringBuilder link = new System.Text.StringBuilder();
            link.Append("~/search.aspx?keyword=");
            link.Append(Server.UrlEncode(txtKeyword.Text.Trim()));

            Response.Redirect(link.ToString());
        }
        #endregion
    }
}
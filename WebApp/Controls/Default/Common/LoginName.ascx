﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Common_LoginName" Codebehind="LoginName.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Common/LoginStatus.ascx" TagName="logout" TagPrefix="uc1" %>
<span class="LoginText">
    <asp:LoginName ID="lnLoginName" runat="server" meta:resourceKey="lnLoginName" />
</span><span>
    <uc1:logout ID="logout" runat="server" />
</span>
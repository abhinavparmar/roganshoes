using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

/// <summary>
/// This control creates a Clear spacer
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the Spacer user control class.
    /// </summary>
    public partial class Controls_Default_Common_Spacer : System.Web.UI.UserControl
    {
        private int _SpacerWidth;
        private int _SpacerHeight;

        /// <summary>
        /// Gets or sets the spacer control width.
        /// </summary>
        public int SpacerWidth
        {
            get { return this._SpacerWidth; }
            set { this._SpacerWidth = value; }
        }

        /// <summary>
        /// Gets or sets the spacer control height.
        /// </summary>
        public int SpacerHeight
        {
            get { return this._SpacerHeight; }
            set { this._SpacerHeight = value; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            imgClear.ImageUrl = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/clear.gif";

            imgClear.Width = this.SpacerWidth;
            imgClear.Height = this.SpacerHeight;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
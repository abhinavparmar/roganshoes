<%@ Control Language="C#" EnableViewState="false" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Common_CartItemCount" CodeBehind="CartItemCount.ascx.cs" %>

<div class="MiniShoppingCart">
    <span id="CartItemCount">
        <a id="CartItemCountSC" href="shoppingcart.aspx" class="Button">
           <%-- <asp:Label ID="lblShoppingCartTitle" runat="server" meta:resourceKey="lblShoppingCartTitleResource1" CssClass="Title"></asp:Label>--%>
            <asp:Label ID="lblMobileShoppingCartTitle" runat="server" meta:resourceKey="lblMobileShoppingCartTitleResource1" CssClass="BagTitle"></asp:Label>
            <div class="Title">
                <span class="Text">
                    <span class="CartItemCountText">
                        <asp:Label ID="lblCartItemCount" runat="server"></asp:Label>&nbsp;Item<span class="ItemsText">(s)</span>
                    </span>
                </span>
            </div>
            <span class="MobileCartNumber">
                <asp:Label ID="lblMobileCartItemCount" runat="server"></asp:Label>

            </span>
            <span class="Text">
                <span class="CartItemTotalText">
                    <asp:Label ID="lblCartSubTotal" runat="server"></asp:Label>
                </span>
            </span>
        </a>
        <asp:LinkButton ID="lbCheckout" runat="server" AlternateText="Check Out" OnClick="Checkout_Click"
            CssClass="CartCheckoutButton" meta:resourcekey="Checkout1Resource1" ClientIDMode="Static" CausesValidation="false">Checkout</asp:LinkButton></span>
</div>
<div class="MiniShoppingCartFlyOut">
    <div class="containermycart">
    </div>
</div>

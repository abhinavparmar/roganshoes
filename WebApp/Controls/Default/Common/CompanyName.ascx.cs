using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Framework.Business;

/// <summary>
/// Displays the company name
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the Company Name user control class.
    /// </summary>
    public partial class Controls_Default_Common_CompanyName : System.Web.UI.UserControl
    {
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            uxCompanyName.Text = ZNodeConfigManager.SiteConfig.CompanyName;
        }
        #endregion
    }
}
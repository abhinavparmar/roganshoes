<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Common_QuickSearch" Codebehind="QuickSearch.ascx.cs" %>

<asp:Panel ID="pnlQuickSearch" runat="server" DefaultButton="BtnSearch">
	<div id="QuickSearch">		
		<ajaxToolKit:TextBoxWatermarkExtender ID="watermark" TargetControlID="txtKeyword" WatermarkCssClass="WaterMark" WatermarkText="search Maxwell's..." runat="server"></ajaxToolKit:TextBoxWatermarkExtender>
		<asp:TextBox ID="txtKeyword" Width="125" runat="server" CssClass="TextBox"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton ID="btnSearch" AlternateText="Search" ToolTip="Search" runat="server" OnClick="BtnSearch_Click" CssClass="Button" />
	</div>
</asp:Panel>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZScheduleATruck.ascx.cs" Inherits="WebApp.Controls.Default.zLandingPages.ZScheduleATruck" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="Zeon" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="ajax" %>
<div class="ContentContainer">
    <div class="PageTitle">
        <h1>
            <asp:Localize ID="lclScheduleATruck" runat="server" meta:resourcekey="lclScheduleATruck"></asp:Localize></h1>
    </div>
    <div class="Form">
        <asp:Panel ID="pnlScheduleATruck" runat="server" DefaultButton="imgSubmit" ClientIDMode="Static" CssClass="CommonLableNone">
            <p class="FormContent">
                <uc1:CustomMessage ID="uxCustomMessage" runat="server" MessageKey="ScheduleATruckIntroText"></uc1:CustomMessage>
            </p>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblCompanyName" runat="server" AssociatedControlID="txtCompanyName" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclCompanyName" runat="server" Text="<%$ Resources:CommonCaption, CompanyName%>" EnableViewState="false">
                        </asp:Localize>
                    </asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="txtCompanyName" runat="server" MaxLength="100" ToolTip="<%$ Resources:CommonCaption, CompanyName%>"  EnableViewState="false" ClientIDMode="Static" alt="Comapny Name" placeholder="Company Name *"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvCompanyName" runat="server" ControlToValidate="txtCompanyName" SetFocusOnError="true" ValidationGroup="ScheduleAtruckGrpValidation"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, CompanyNameRequired%>"
                            ToolTip="<%$ Resources:CommonCaption, CompanyNameRequired%>"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblDatesRequested" runat="server" AssociatedControlID="txtDatesRequested" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclDatesRequested" runat="server" meta:resourcekey="lclDatesRequested" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <div class="ForScheduleTruckWrapper">

                    <asp:TextBox ID="txtDatesRequested" CssClass="datesForScheduleTruck" runat="server" meta:resourcekey="txtDatesRequested" ClientIDMode="Static" alt="Requested Date" placeholder="Dates Requested *"></asp:TextBox>
                    <div>
                        <ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDatesRequested" Format="MM/dd/yyyy" runat="server"></ajax:CalendarExtender>
                    </div>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvDatesRequested" runat="server" ControlToValidate="txtDatesRequested" SetFocusOnError="true" ValidationGroup="ScheduleAtruckGrpValidation"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, DatesRequestedRequired%>"
                            ToolTip="<%$ Resources:CommonCaption, DatesRequestedRequired%>"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cmpDatesRequested" Text="<%$ Resources:CommonCaption, GreaterDatesRequestedRequired%>" ToolTip="<%$ Resources:CommonCaption, GreaterDatesRequestedRequired%>" ControlToValidate="txtDatesRequested" ValidationGroup="ScheduleAtruckGrpValidation" Type="Date" Display="Dynamic" Operator="GreaterThanEqual" runat="server"></asp:CompareValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblTimesRequested" runat="server" AssociatedControlID="txtTimesRequested" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclTimesRequested" runat="server" meta:resourcekey="lclTimesRequested" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <div class="ForScheduleTruckWrapper">
                    <asp:TextBox ID="txtTimesRequested" runat="server" MaxLength="100" EnableViewState="false"  meta:resourcekey="txtTimesRequested" ClientIDMode="Static" alt="Time Requested" placeholder="Time Requested *"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvTimesRequested" runat="server" ControlToValidate="txtTimesRequested" SetFocusOnError="true" ValidationGroup="ScheduleAtruckGrpValidation"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, TimesRequestedRequired%>"
                            ToolTip="<%$ Resources:CommonCaption, TimesRequestedRequired%>"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblAddress" runat="server" AssociatedControlID="txtAddress" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:CommonCaption, Address%>" EnableViewState="false">
                        </asp:Localize>
                        :</asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="txtAddress" runat="server" MaxLength="100" ToolTip="<%$ Resources:CommonCaption, Address%>" EnableViewState="false" ClientIDMode="Static" alt="Address" placeholder="Address"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, AddressRequired%>" ValidationGroup="ScheduleAtruckGrpValidation"
                            ToolTip="<%$ Resources:CommonCaption, AddressRequired%>"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblPhoneNumber" runat="server" AssociatedControlID="txtPhoneNumber" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclPhoneNumber" runat="server" Text="<%$ Resources:CommonCaption, PhoneNumber%>" EnableViewState="false">
                        </asp:Localize>
                    </asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="txtPhoneNumber" runat="server" MaxLength="100" ToolTip="<%$ Resources:CommonCaption, PhoneNumber%>" EnableViewState="false" ClientIDMode="Static" alt="Phone Number" placeholder="Phone Number *"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvPhoneNumber" runat="server" ControlToValidate="txtPhoneNumber" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, PhoneNoRequired%>" ValidationGroup="ScheduleAtruckGrpValidation"
                            ToolTip="<%$ Resources:CommonCaption, PhoneNoRequired%>"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblFirst" runat="server" AssociatedControlID="txtFirstNameScheduleTruck" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:CommonCaption, FirstName%>" EnableViewState="false">
                        </asp:Localize>
                    </asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="txtFirstNameScheduleTruck" runat="server" ClientIDMode="Static" alt="First Name" placeholder="First Name *"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvtxtFirstNameScheduleTruck" runat="server" ControlToValidate="txtFirstNameScheduleTruck" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired%>" ValidationGroup="ScheduleAtruckGrpValidation"
                            ToolTip="<%$ Resources:CommonCaption, PhoneNoRequired%>"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:CommonCaption, LastName%>" EnableViewState="false"></asp:Localize>
                    </asp:Label>
                </div>
                <div class="ForScheduleTruckWrapper">
                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="100" EnableViewState="false"  ToolTip="<%$ Resources:CommonCaption, LastName%>" ClientIDMode="Static" alt="Last Name" placeholder="Last Name *"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired%>" ValidationGroup="ScheduleAtruckGrpValidation"
                            ToolTip="<%$ Resources:CommonCaption, LastNameRequired %>">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblNumberOfEmployees" runat="server" AssociatedControlID="txtNumOfEmployees" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclNumberOfEmployees" runat="server" meta:resourcekey="lclNumberOfEmploye" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <div class="ForScheduleTruckWrapper">
                    <asp:TextBox ID="txtNumOfEmployees" runat="server" MaxLength="100" EnableViewState="false" meta:resourcekey="txtNumberOfEmployees" ClientIDMode="Static" alt="No Of Employees" placeholder="Number Of Employees *"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvNumOfEmployees" runat="server" ControlToValidate="txtNumOfEmployees" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, NumberOfEmployeesRequired%>" ValidationGroup="ScheduleAtruckGrpValidation"
                            ToolTip="<%$ Resources:CommonCaption, NumberOfEmployeesRequired %>">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblRationMaleFemale" runat="server" AssociatedControlID="txtRationMaleFemale" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclRationMaleFemale" runat="server" meta:resourcekey="lclRationMaleFemale" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <div class="ForScheduleTruckWrapper">
                    <asp:TextBox ID="txtRationMaleFemale" runat="server" MaxLength="100" EnableViewState="false" meta:resourcekey="txtRationMaleFemale" ClientIDMode="Static" alt="Ratio" placeholder="Ratio Male / Female *"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvRationMaleFemale" runat="server" ControlToValidate="txtNumOfEmployees" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, RationMaleFemaleRequired%>" ValidationGroup="ScheduleAtruckGrpValidation"
                            ToolTip="<%$ Resources:CommonCaption, RationMaleFemaleRequired %>">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblSteelToeReq" runat="server" AssociatedControlID="txtSteelToeReq" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclSteelToeReq" runat="server" meta:resourcekey="lclSteelToeReq" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <div class="ForScheduleTruckWrapper">
                    <asp:TextBox ID="txtSteelToeReq" runat="server" MaxLength="100" EnableViewState="false" meta:resourcekey="txtSteelToeReqRec" ClientIDMode="Static" alt="Toe Request" placeholder="Steel Toe Required *"></asp:TextBox>
                    <div class="ForScheduleTruckWrapper">
                        <asp:RequiredFieldValidator ID="rfvSteelToeReq" runat="server" ControlToValidate="txtNumOfEmployees" SetFocusOnError="true" ValidationGroup="ScheduleAtruckGrpValidation"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, SteelToeReqRequired%>"
                            ToolTip="<%$ Resources:CommonCaption, SteelToeReqRequired %>">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblContributionAmount" runat="server" AssociatedControlID="txtPayrollDeduction" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclContributionAmount" runat="server" meta:resourcekey="lclContributionAmount" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <div class="ForScheduleTruckWrapper">
                    <asp:TextBox ID="txtContributionAmount" runat="server" MaxLength="100" EnableViewState="false" meta:resourcekey="txtContributionAmount" ClientIDMode="Static" alt="Contribution Amount" placeholder="Contribution $$ Amount *"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvContributionAmount" runat="server" ControlToValidate="txtContributionAmount" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption,  ContributionAmountRequired%>" ValidationGroup="ScheduleAtruckGrpValidation"
                            ToolTip="<%$ Resources:CommonCaption, ContributionAmountRequired %>">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblPayrollDeduction" runat="server" AssociatedControlID="txtPayrollDeduction" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclPayrollDeduction" runat="server" meta:resourcekey="lclPayrollDeduction" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <div class="ForScheduleTruckWrapper">
                    <asp:TextBox ID="txtPayrollDeduction" runat="server" MaxLength="100" EnableViewState="false"  meta:resourcekey="txtPayrollDeduction" ClientIDMode="Static" alt="Payroll Deduction" placeholder="Payroll Deduction *"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvPayrollDeduction" runat="server" ControlToValidate="txtPayrollDeduction" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption,  PayrollDeductionRequired%>" ValidationGroup="ScheduleAtruckGrpValidation"
                            ToolTip="<%$ Resources:CommonCaption, PayrollDeductionRequired %>">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclEmail" runat="server" Text="<%$ Resources:CommonCaption, EmailAddress%>" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <div class="ForScheduleTruckWrapper">
                    <asp:TextBox ID="txtEmail" runat="server"  ToolTip="<%$ Resources:CommonCaption, EmailAddress%>" EnableViewState="false" ClientIDMode="Static" alt="Email" placeholder="Email Address *"> </asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" SetFocusOnError="true" ValidationGroup="ScheduleAtruckGrpValidation"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                            ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationGroup="ScheduleAtruckGrpValidation"
                            ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblPostersNeeded" runat="server" AssociatedControlID="txtPostersNeeded" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclPostersNeeded" runat="server" meta:resourcekey="lclPostersNeeded" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <div class="ForScheduleTruckWrapper">
                    <asp:TextBox ID="txtPostersNeeded" runat="server" MaxLength="100" EnableViewState="false"  meta:resourcekey="txtPostersNeeded" ClientIDMode="Static" alt="Poster Needed" placeholder="Posters Needed *"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvPostersNeeded" runat="server" ControlToValidate="txtPostersNeeded" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, PostersNeededRequired%>" ValidationGroup="ScheduleAtruckGrpValidation"
                            ToolTip="<%$ Resources:CommonCaption, PostersNeededRequired %>">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblNotes" runat="server" CssClass="ReqNo" AssociatedControlID="txtNotes" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="lclNotes" runat="server" meta:resourcekey="lclNotes" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <div class="ForScheduleTruckWrapper">
                    <asp:TextBox ID="txtNotes" runat="server" Rows="5" MaxLength="100" EnableViewState="false" meta:resourcekey="txtNotes" TextMode="MultiLine" ClientIDMode="Static" alt="Notes" placeholder="Notes"></asp:TextBox>
                </div>
            </div>
            <div class="Row Clear BottomSpacer">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblPicture" runat="server" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="locPictures" runat="server" meta:resourcekey="locPicture" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <asp:UpdatePanel ID="pnlCaptcha" runat="server" UpdateMode="Conditional" class="CaptchaControl" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="CaptchaImage">
                            <Zeon:CaptchaControl ID="CaptchaID" runat="server" ToolTip="CaptchaImage" CaptchaBackgroundNoise="None" CaptchaLength="6" CaptchaHeight="55" CaptchaWidth="200" CaptchaLineNoise="None" CaptchaMinTimeout="5" BackColor="#af64a8" CaptchaMaxTimeout="240" FontColor="#FFFFFF" ClientIDMode="Static" />
                        </div>
                        <div class="CaptchaRefreshButton">
                            <asp:LinkButton ID="lnkbtnRefresh" runat="server" meta:resourcekey="lnkbtnRefreshRes" Text="Refresh" CssClass="Button" ToolTip="Refresh" OnClick="lnkbtnRefresh_Click" ClientIDMode="Static"></asp:LinkButton>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent">
                                <asp:Label ID="lblCharacters" runat="server" AssociatedControlID="txtCharacters" EnableViewState="false" ClientIDMode="Static">
                                    <asp:Localize ID="lclCharacters" runat="server" meta:resourcekey="lclCharacters" EnableViewState="false"></asp:Localize>
                                    :</asp:Label>
                            </div>
                            <div class="ForScheduleTruckWrapper CaptchaText">
                                <asp:TextBox ID="txtCharacters" runat="server" MaxLength="100" EnableViewState="false" meta:resourcekey="txtCharacter" alt="Character" placeholder="Enter Character *"></asp:TextBox>&nbsp;&nbsp;
                            <a id="whatsthisID" title="what's this?" href="javascript:void(0);" onclick="PopUpCaptchaWhatsThisWindow()" class="whatsthis">what's this?</a>
                                <div>
                                    <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error" ClientIDMode="Static"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent No-Text">
                    &nbsp;
                </div>
                <div class="ContactUsButton">
                    <asp:LinkButton ID="imgSubmit" runat="server" Text="<%$ Resources:CommonCaption, Submit%>" EnableViewState="false" ValidationGroup="ScheduleAtruckGrpValidation"
                        CssClass="Button" ToolTip="<%$ Resources:CommonCaption, Submit%>" OnClick="imgSubmit_Click" ClientIDMode="Static" />
                </div>
            </div>

            <div class="Clear">
                <ZNode:Spacer ID="Spacer8" EnableViewState="false" SpacerHeight="15" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:Spacer>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlConfirm" runat="server" Visible="False" meta:resourcekey="pnlConfirmResource1" ClientIDMode="Static">
            <div class="SuccessMsg">
                <span class="uxMsg SuccessSection"><em>
                    <uc1:CustomMessage ID="CustomMessage2" runat="server" MessageKey="ScheduleATruckConfirmationText" ClientIDMode="Static"></uc1:CustomMessage>
                </em></span>
            </div>
            <ZNode:Spacer ID="Spacer1" EnableViewState="false" SpacerHeight="20" SpacerWidth="10" runat="server" ClientIDMode="Static" />
        </asp:Panel>
    </div>
</div>


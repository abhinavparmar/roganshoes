﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZShoeFinder.ascx.cs" Inherits="WebApp.Controls.Default.zLandingPages.ZShoeFinder" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<div class="ContentContainer">
    <div class="PageTitle">
        <h1><asp:Localize ID="txtShoeFinder" runat="server" meta:resourcekey="lcltxtShoeFinder"></asp:Localize></h1>
    </div>
    <div class="ShoeFinder">
        <asp:Panel ID="pnlShoeFinder" runat="server" DefaultButton="imgSubmit" CssClass="CommonLableNone">
            <p class="FormContent">
                <uc1:CustomMessage ID="CustomMessage1" runat="server" MessageKey="ShoeFinderIntroText"></uc1:CustomMessage>
            </p>
            <div class="LeftContent">
                <div class="FormTitle">
                    <asp:Localize ID="lclContactInformation" runat="server" meta:resourceKey="lclContactInformation" EnableViewState="true"></asp:Localize>
                </div>
                <div class="Clear">
                    <ZNode:Spacer ID="Spacer3" EnableViewState="false" SpacerHeight="15" SpacerWidth="10" runat="server"></ZNode:Spacer>
                </div>
                <div class="Form">
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstNameShoeFinder" EnableViewState="false">
                                <asp:Localize ID="lclFirstName" runat="server" Text="<%$ Resources:CommonCaption, FirstName%>" EnableViewState="false"></asp:Localize></asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtFirstNameShoeFinder" runat="server" ToolTip="<%$ Resources:CommonCaption, FirstName%>" alt="First Name" placeholder="First Name *"></asp:TextBox>
                            <div>
                                <asp:RequiredFieldValidator ID="rfvNameRequired" runat="server" ControlToValidate="txtFirstNameShoeFinder" SetFocusOnError="true" ValidationGroup="ShoeFinderGrpValidation"
                                    CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired%>"
                                    ToolTip="<%$ Resources:CommonCaption, FirstNameRequired%>"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" EnableViewState="false">
                                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:CommonCaption, LastName%>" EnableViewState="false"></asp:Localize></asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtLastName" runat="server" EnableViewState="false" ToolTip="<%$ Resources:CommonCaption, LastName%>" alt="Last Name" placeholder="Last Name *"></asp:TextBox>
                            <div>
                                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" SetFocusOnError="true"
                                    CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired%>" ValidationGroup="ShoeFinderGrpValidation"
                                    ToolTip="<%$ Resources:CommonCaption, LastNameRequired %>">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail" EnableViewState="false">
                                <asp:Localize ID="lclEmail" runat="server" Text="<%$ Resources:CommonCaption, EmailAddress%>" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtEmail" runat="server" ToolTip="<%$ Resources:CommonCaption, EmailAddress%>" EnableViewState="false" alt="Email" placeholder="E-mail Address *"> </asp:TextBox>
                            <div>
                                <asp:RequiredFieldValidator ID="rfvEmailRequired" runat="server" ControlToValidate="txtEmail" SetFocusOnError="true" ValidationGroup="ShoeFinderGrpValidation"
                                    CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                                    ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEmailRequired" runat="server" ControlToValidate="txtEmail" SetFocusOnError="true"
                                    CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationGroup="ShoeFinderGrpValidation"
                                    ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="FormTitle">
                    <asp:Localize ID="lclOtherOptionalInformation" runat="server" meta:resourceKey="lclOtherOptionalInformation" EnableViewState="false"></asp:Localize>
                </div>
                <div class="Clear">
                    <ZNode:Spacer ID="Spacer2" EnableViewState="false" SpacerHeight="15" SpacerWidth="10" runat="server"></ZNode:Spacer>
                </div>
                <div class="Form">
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblType" CssClass="ReqNo" runat="server" AssociatedControlID="txtType" EnableViewState="false">
                                <asp:Localize ID="lclType" runat="server" meta:resourceKey="lclType" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtType" runat="server" MaxLength="100" EnableViewState="false" meta:resourceKey="txtType" alt="Type" placeholder="Type"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblStockNumber" CssClass="ReqNo" runat="server" AssociatedControlID="txtStockNumber" EnableViewState="false">
                                <asp:Localize ID="lcltxtStockNumber" runat="server" meta:resourceKey="lcltxtStockNumber" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtStockNumber" runat="server" MaxLength="100" EnableViewState="false" a meta:resourceKey="txtStockNumberRes" alt="Stock Number" placeholder="Stock Number"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblLikeOurSite" runat="server" AssociatedControlID="drpLikeOurSite" EnableViewState="false" CssClass="ReqNo">
                                <asp:Localize ID="lclLikeOurSite" runat="server" meta:resourceKey="lclLikeOurSiteRes" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:DropDownList ID="drpLikeOurSite" runat="server" MaxLength="100" EnableViewState="true"  meta:resourceKey="drpLikeOurSiteRes" CssClass="DdlList"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblComments" CssClass="ReqNo" runat="server" AssociatedControlID="txtComments" EnableViewState="false">
                                <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtComments" EnableViewState="false"></asp:Localize>:</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtComments" runat="server" Rows="5" TextMode="MultiLine" EnableViewState="false"
                                meta:resourcekey="Comments" alt="Comments" placeholder="Comments"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="LeftContent RightSection">
                <div class="FormTitle">
                    <asp:Localize ID="lclShoeInformation" runat="server" meta:resourceKey="lclShoeInformation" EnableViewState="false"></asp:Localize>
                </div>
                <div class="Clear">
                    <ZNode:Spacer ID="Spacer1" EnableViewState="false" SpacerHeight="15" SpacerWidth="10" runat="server"></ZNode:Spacer>
                </div>
                <div class="Form">
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblBrand" runat="server" AssociatedControlID="ddlBrand" EnableViewState="false" CssClass="ReqNo">
                                <asp:Localize ID="lclBrand" runat="server" meta:resourceKey="lclBrand" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:DropDownList ID="ddlBrand" runat="server" meta:resourceKey="ddlBrand" MaxLength="100" EnableViewState="true"  CssClass="DdlList"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblShoeName" runat="server" AssociatedControlID="txtShoeName" EnableViewState="false" CssClass="ReqNo">
                                <asp:Localize ID="lclShoeName" runat="server" meta:resourceKey="lclShoeName" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtShoeName" runat="server" MaxLength="100" meta:resourceKey="txtShoeName" EnableViewState="false" placeholder="Shoe Name"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblColor" runat="server" AssociatedControlID="txtColor" EnableViewState="false" CssClass="ReqNo">
                                <asp:Localize ID="lclColor" runat="server" meta:resourceKey="lclColor" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtColor" runat="server" MaxLength="100" meta:resourceKey="txtColor" EnableViewState="false" alt="Color" placeholder="Color"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblSize" runat="server" AssociatedControlID="txtSize" EnableViewState="false" CssClass="ReqNo">
                                <asp:Localize ID="lclSize" runat="server" meta:resourceKey="lclSize" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtSize" runat="server" MaxLength="100" meta:resourceKey="txtSize" EnableViewState="false" alt="Size" placeholder="Size"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblGender" runat="server" AssociatedControlID="ddlGender" EnableViewState="false" CssClass="ReqNo">
                                <asp:Localize ID="lclGender" runat="server" meta:resourceKey="lclGender" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:DropDownList ID="ddlGender" CssClass="DdlList" runat="server" MaxLength="100" meta:resourceKey="txtGender" EnableViewState="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblWidth" runat="server" AssociatedControlID="ddlWidth" EnableViewState="false" CssClass="ReqNo">
                                <asp:Localize ID="lclWidth" runat="server" meta:resourceKey="lclWidth" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:DropDownList ID="ddlWidth" CssClass="DdlList" runat="server" MaxLength="100" meta:resourceKey="ddlWidth" EnableViewState="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblStoreLocation" runat="server" AssociatedControlID="ddlStoreLocation" EnableViewState="false" CssClass="ReqNo">
                                <asp:Localize ID="lclStoreLocation" runat="server" meta:resourceKey="lclStoreLocation" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:DropDownList ID="ddlStoreLocation" CssClass="DdlList" runat="server" MaxLength="100" meta:resourceKey="ddlStoreLocationRes" EnableViewState="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="Row Clear BottomSpacer">
                        <div class="FieldStyle">
                            <asp:Label ID="lblPicture" runat="server" EnableViewState="false">
                                <asp:Localize ID="locPictures" runat="server" meta:resourcekey="locPicture" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <asp:UpdatePanel ID="pnlCaptcha" runat="server" UpdateMode="Conditional" class="CaptchaControl">
                            <ContentTemplate>
                                <div class="CaptchaImage">
                                    <Zeon:CaptchaControl ID="CaptchaID" ToolTip="CaptchaImage" runat="server" CaptchaBackgroundNoise="None" CaptchaLength="6" CaptchaHeight="55" CaptchaWidth="200" CaptchaLineNoise="None" CaptchaMinTimeout="5" BackColor="#af64a8" CaptchaMaxTimeout="240" FontColor="#FFFFFF" />
                                </div>
                                <div class="CaptchaRefreshBtn">
                                    <asp:LinkButton ID="lnkbtnRefresh" runat="server" Text="Refresh" CssClass="Button" ToolTip="Refresh" OnClick="lnkbtnRefresh_Click"></asp:LinkButton>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle">
                                        <asp:Label ID="lblCharacters" runat="server" CssClass="Req" AssociatedControlID="txtCharacters" EnableViewState="false">
                                            <asp:Localize ID="locCharecter" runat="server" meta:resourcekey="lblCharacter" EnableViewState="false"></asp:Localize>
                                            :</asp:Label>
                                    </div>
                                    <div class="FieldStyleRight">
                                        <div class="CaptchaBox">
                                            <asp:TextBox ID="txtCharacters" runat="server" MaxLength="100" EnableViewState="false" meta:resourcekey="txtCharacter" placeholder="Enter Character *"></asp:TextBox>&nbsp;&nbsp;
                                            <a id="whatsthisID" title="what's this?" href="javascript:void(0);" onclick="PopUpCaptchaWhatsThisWindow()" class="whatsthis">what's this?</a>
                                            <div>
                                                <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error"></asp:Label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle No-Text">
                            &nbsp;
                        </div>
                        <div class="ContactUsButton">
                            <asp:LinkButton ID="imgSubmit" runat="server" Text="<%$ Resources:CommonCaption, Submit%>" EnableViewState="false" ValidationGroup="ShoeFinderGrpValidation"
                                CssClass="Button" ToolTip="<%$ Resources:CommonCaption, Submit%>" OnClick="imgSubmit_Click" />
                        </div>
                    </div>
                    <div class="Clear">
                        <ZNode:Spacer ID="Spacer8" EnableViewState="false" SpacerHeight="15" SpacerWidth="10" runat="server"></ZNode:Spacer>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlConfirm" runat="server" Visible="False" meta:resourcekey="pnlConfirmResource1">
            <div class="SuccessMsg">
                <span class="uxMsg SuccessSection"><em>
                    <uc1:CustomMessage ID="uxCustomMessage" runat="server" MessageKey="ShoeFinderConfirmationText"></uc1:CustomMessage>
                </em></span>
            </div>
            <ZNode:Spacer ID="Spacer4" EnableViewState="false" SpacerHeight="20" SpacerWidth="10" runat="server" ClientIDMode="Static" />
        </asp:Panel>
    </div>
</div>


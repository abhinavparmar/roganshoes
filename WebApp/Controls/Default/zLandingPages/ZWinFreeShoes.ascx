﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZWinFreeShoes.ascx.cs" Inherits="WebApp.Controls.Default.zLandingPages.ZWinFreeShoes" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<div class="ContentContainer">
    <div class="PageTitle">
        <h1><asp:Localize ID="locWinFreeShoesTitle" runat="server" meta:resourcekey="locWinFreeShoesTitle"></asp:Localize></h1>
    </div>
    <div class="WinShoes">
        <div class="Form">
            <asp:Panel runat="server" ID="pnlWinFreeShoesTitle" DefaultButton="imgSubmit" ClientIDMode="Static">
                <p class="FormContent">
                    <uc1:CustomMessage ID="CustomMessage3" runat="server" MessageKey="WinFreeShoeIntroText" ClientIDMode="Static"></uc1:CustomMessage>
                </p>
                <div class="LeftContent">
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstNameWinFreeShoes" EnableViewState="false" ClientIDMode="Static">
                                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:CommonCaption, FirstName%>" EnableViewState="false"></asp:Localize>
                            </asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtFirstNameWinFreeShoes" runat="server" ToolTip="<%$ Resources:CommonCaption, FirstName%>"  ClientIDMode="Static" alt="FirstName"></asp:TextBox>
                            <div>
                                <asp:RequiredFieldValidator ID="rfvNameRequired" runat="server" ControlToValidate="txtFirstNameWinFreeShoes" SetFocusOnError="true" ValidationGroup="WinFreeShowGrpValidation"
                                    CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired%>"
                                    ToolTip="<%$ Resources:CommonCaption, FirstNameRequired%>"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" EnableViewState="false" ClientIDMode="Static">
                                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:CommonCaption, LastName%>" EnableViewState="false"></asp:Localize>
                            </asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtLastName" runat="server" MaxLength="100" EnableViewState="false" ToolTip="<%$ Resources:CommonCaption, LastName%>" ClientIDMode="Static" alt="LastName"></asp:TextBox>
                            <div>
                                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" SetFocusOnError="true" ValidationGroup="WinFreeShowGrpValidation"
                                    CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired%>"
                                    ToolTip="<%$ Resources:CommonCaption, LastNameRequired %>">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail" EnableViewState="false" ClientIDMode="Static">
                                <asp:Localize ID="locEmailLabel" runat="server" meta:resourcekey="locEmailLabel" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <asp:TextBox ID="txtEmail" runat="server"  meta:resourcekey="txtEmailID" EnableViewState="false" ClientIDMode="Static" alt="Email"> </asp:TextBox>
                            <div>
                                <asp:RequiredFieldValidator ID="rfvEmailRequired" runat="server" ControlToValidate="txtEmail" SetFocusOnError="true" ValidationGroup="WinFreeShowGrpValidation"
                                    CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                                    ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" SetFocusOnError="true" ValidationGroup="WinFreeShowGrpValidation"
                                    CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
                                    ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblPicture" runat="server" EnableViewState="false" ClientIDMode="Static">
                                <asp:Localize ID="locPictures" runat="server" meta:resourcekey="locPicture" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <asp:UpdatePanel ID="pnlCaptcha" runat="server" UpdateMode="Conditional" class="CaptchaControl" ClientIDMode="Static">
                            <ContentTemplate>
                                <div class="CaptchaImage">
                                    <Zeon:CaptchaControl ID="CaptchaID" ToolTip="CaptchaImage" runat="server" CaptchaBackgroundNoise="None" CaptchaLength="6" CaptchaHeight="55" CaptchaWidth="200" CaptchaLineNoise="None" CaptchaMinTimeout="5" BackColor="#af64a8" CaptchaMaxTimeout="240" FontColor="#FFFFFF" ClientIDMode="Static" />
                                </div>
                                <div class="CaptchaRefreshButtoninFreeShoes">
                                    <asp:LinkButton ID="lnkbtnRefresh" runat="server" Text="Refresh" CssClass="Button" ToolTip="Refresh" OnClick="lnkbtnRefresh_Click" ClientIDMode="Static"></asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            <asp:Label ID="lblCharacters" CssClass="Req" runat="server" AssociatedControlID="txtCharacters" EnableViewState="false">
                                <asp:Localize ID="Localize3" runat="server" meta:resourcekey="lblCharacter" EnableViewState="false"></asp:Localize>
                                :</asp:Label>
                        </div>
                        <div class="FieldStyleRight">
                            <div class="CaptchaBox">
                                <asp:TextBox ID="txtCharacters" runat="server" MaxLength="100" EnableViewState="true"  meta:resourcekey="txtCharacter" ClientIDMode="Static"></asp:TextBox>&nbsp;&nbsp;
                    <a id="whatsthisID" title="what's this?" href="javascript:void(0);" onclick="PopUpCaptchaWhatsThisWindow()" class="whatsthis">what's this?</a>
                                <div>
                                    <div>
                                        <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle">
                            &nbsp;
                        </div>
                        <div class="ContactUsButton">
                            <asp:LinkButton ID="imgSubmit" runat="server" Text="<%$ Resources:CommonCaption, Submit%>" ToolTip="<%$ Resources:CommonCaption, Submit%>" EnableViewState="false" ValidationGroup="WinFreeShowGrpValidation"
                                CssClass="Button" OnClick="imgSubmit_Click" ClientIDMode="Static" />
                        </div>
                    </div>
                    <div class="Clear">
                        <ZNode:Spacer ID="Spacer8" EnableViewState="false" SpacerHeight="15" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:Spacer>
                    </div>
                </div>
                <div class="LeftContent RightSection">
                    <div class="WinShoesInfo">
                        <uc1:CustomMessage ID="CustomMessage1" runat="server" MessageKey="WinFreeShoesInformationText" ClientIDMode="Static"></uc1:CustomMessage>
                    </div>
                    <div class="WinShoesWinner">
                        <uc1:CustomMessage ID="CustomMessage4" runat="server" MessageKey="WinFreeShoesWinnersNames" ClientIDMode="Static"></uc1:CustomMessage>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlConfirm" runat="server" Visible="False" meta:resourcekey="pnlConfirmResource1" ClientIDMode="Static">
                <div class="SuccessMsg">
                    <span class="uxMsg SuccessSection"><em>
                        <uc1:CustomMessage ID="CustomMessage2" runat="server" MessageKey="WinFreeShoesConfirmationText" ClientIDMode="Static"></uc1:CustomMessage>
                    </em></span>
                </div>
                <ZNode:Spacer ID="Spacer1" EnableViewState="false" SpacerHeight="20" SpacerWidth="10"  runat="server" ClientIDMode="Static" />
            </asp:Panel>
        </div>
    </div>
</div>


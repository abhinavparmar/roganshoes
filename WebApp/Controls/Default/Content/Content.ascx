<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Content_Content" Codebehind="Content.ascx.cs" %>
<div class="ContentContainer">
    <div class="ContentPage Faq">
	    <div class="PageTitle"><h1><asp:Literal ID="lblTitle" runat="server" EnableViewState="false" ClientIDMode="Static"></asp:Literal></h1></div>
	    <div class="SubTitle"><asp:Label ID="lblHtml" runat="server" EnableViewState="false" ClientIDMode="Static"></asp:Label></div>
    </div>
</div>

﻿using System;
using System.Configuration;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.zNewArrival
{
    public partial class ZNewArrivalProducts : System.Web.UI.UserControl
    {
        #region Private Variables

        private ZNodeProfile _SpecialProfile = new ZNodeProfile();
        private ZNodeProductList _ProductList;
        private string _Title = string.Empty;
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/ViewAdmin.gif";
        private int _DisplayItem = 10;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the profile.
        /// </summary>
        public ZNodeProfile SpecialProfile
        {
            get { return this._SpecialProfile; }
            set { this._SpecialProfile = value; }
        }

        /// <summary>
        /// Gets or sets the Product list
        /// </summary>
        public ZNodeProductList ProductList
        {
            get
            {
                return this._ProductList;
            }

            set
            {
                this._ProductList = value;
            }
        }

        /// <summary>
        /// Gets or sets the title for this control
        /// </summary>
        public string Title
        {
            get
            {
                return this._Title;
            }
            set
            {
                this._Title = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of items to be displayed
        /// </summary>
        public int DisplayItem
        {
            get
            {
                return this._DisplayItem;
            }

            set
            {
                this._DisplayItem = value;
            }
        }

        /// <summary>
        /// to return current portal Id
        /// </summary>
        protected string PortalID
        {
            get { return ZNodeConfigManager.SiteConfig.PortalID.ToString(); }
        }

        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BindNavigationURL();
            this._ProductList = ZNodeProductList.GetNewArrivals(this.DisplayItem, ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID);
            this.BindProducts();
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void Page_PreRender(object sender, EventArgs e)
        //{
        //    RegisterClientScriptClasses();
        //}

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind display based on a product list
        /// </summary>
        public void BindProducts()
        {
            if (_ProductList.ZNodeProductCollection.Count == 0)
            {
                this.Visible = false;
            }
            else
            {
                RepeaterNewArrivals.DataSource = _ProductList.ZNodeProductCollection;
                RepeaterNewArrivals.DataBind();

                // Disable view state for data list item
                foreach (RepeaterItem item in RepeaterNewArrivals.Items)
                {
                    item.EnableViewState = false;
                }
            }
        }
        #endregion

        #region Helper Functions

        /// <summary>
        /// Binds the Navigation URL
        /// </summary>
        private void BindNavigationURL()
        {
            CategoryHelper helper = new CategoryHelper();
            string categoryList = ConfigurationManager.AppSettings["NewArrivalCategories"] != null ? ConfigurationManager.AppSettings["NewArrivalCategories"].ToString() : string.Empty;
            lnkNewArrivals.NavigateUrl = helper.GetCategorySEOUrl(ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID, categoryList);
        }

        /// <summary>
        /// Represents the Check for Call For Pricing method
        /// </summary>
        /// <param name="fieldValue">The field value</param>
        /// <returns>Returns the Call For Pricing message</returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            bool Status = bool.Parse(fieldValue.ToString());

            if (Status)
            {
                if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
                {
                    return callMessage.ToString();
                }
            }
            else if (this.SpecialProfile.ShowPrice)
            {
                return string.Empty;
            }

            return new MessageConfigAdmin().GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        /// This script is moved to parent user control (home). 
        /// This is done due to adding this user control to output cache. Output cache was caching script and was rendering it on improper location.
        /// 
        //private void RegisterClientScriptClasses()
        //{
        //    StringBuilder script = new StringBuilder();

        //    script.Append("var zeonNewArrivalProducts = new ZeonSlideProducts({");
        //    script.Append("'sliderID':'divNewArrival',");
        //    script.Append("'MaxItemCount':'5',");
        //    script.Append("'MobPortraitItemCount':'3',");
        //    script.Append("'MobLandItemCount':'3',");
        //    script.Append("'TabletItemCount':'4',");
        //    script.Append("'MinimumItemsShown':'2',");
        //    script.Append("});zeonNewArrivalProducts.Init();");
        //    this.Page.ClientScript.RegisterStartupScript(GetType(), "Slider", script.ToString(), true);
        //}

        #endregion
    }
}
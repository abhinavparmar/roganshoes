using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

/// <summary>
/// Displays the sub-categories on the category page
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the SubCategories List user control class.
    /// </summary>
    public partial class Controls_Default_Category_SubCategoryList : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeCategory _category;
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (this._category.SubCategoryGridVisibleInd)
            {
                if (this._category.ZNodeCategoryCollection.Count == 0)
                {
                    pnlSubCategoryList.Visible = false;
                }
                else
                {
                    pnlSubCategoryList.Visible = true;
                }

                // Bind to datalist
                DataListSubCategories.DataSource = this._category.ZNodeCategoryCollection;
                DataListSubCategories.RepeatColumns = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
                DataListSubCategories.DataBind();

                foreach (DataListItem item in DataListSubCategories.Items)
                {
                    item.EnableViewState = false;
                }
            }
            else
            {
                pnlSubCategoryList.Visible = false;
            }
        }

        #endregion

        #region helper method
        /// <summary>
        /// To get the Parent Categories  when the CategoryId is not passed.
        /// </summary>
        /// <param name="portalid">Portal Id value</param>
        public void ParentCategory(int portalid)
        {
            // Retrieve the data
            CategoryHelper categoryHelper = new CategoryHelper();
            DataSet ds = categoryHelper.GetRootCategoryItems(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.CatalogID);

            // Bind to datalist
            DataListParentCategory.DataSource = ds;
            DataListParentCategory.DataBind();
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve category data from httpContext (set previously in the page_preinit event)
            this._category = (ZNodeCategory)HttpContext.Current.Items["Category"];

            // Bind data to page
            this.Bind();
            //Zeon Custom Code: start Change Page Heading as per Parent Category
            ltrSubCategoryTitle.Text = string.Format("Browse {0}", this._category.Name);
            lblCategoryDescription.Text = this._category.Description;
            lblBottomCategoryDescription.Text = this._category.Description;
            //Zeon Custom code: end
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Return true if this category has image
        /// </summary>
        /// <param name="imageFileName">Name of the Image File</param>
        /// <returns>Returns boolean value to show category image or not</returns>
        protected bool ShowCategoryImage(string imageFileName)
        {
            // Check whether Image file name length is greater than 0 or not
            if (imageFileName.Length > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get http image path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the image file path with name.</returns>
        protected string GetImagePath(string imageFileName)
        {
            //Znode Old Code
            //ZNodeImage znodeImage = new ZNodeImage();
            // return znodeImage.GetImageHttpPathSmall(imageFileName);
            //Zeon Custom Code Starts
            string imagePath = string.Empty;
            if (ConfigurationManager.AppSettings["CategoryImagePath"] != null)
            {
                imagePath = ConfigurationManager.AppSettings["CategoryImagePath"] + imageFileName;
                if (!System.IO.File.Exists(Server.MapPath(imagePath)))
                {
                    imagePath = ConfigurationManager.AppSettings["CategoryImagePath"] + "noimage.gif";
                }
            }
            else
            {
                imagePath = "/Data/Default/Images/Category/noimage.gif"; ;
            }
            return imagePath;
            //Zeon Custom Code Ends
        }
        #endregion
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZLatestReviewedProductsList.ascx.cs" Inherits="WebApp.Controls_Default_Category_ZLatestReviewedProductsList" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<asp:Panel ID="pnlLatestTopReviewedPrd" runat="server" EnableViewState="false" class="BrandReview">
    <h2 class="media-heading"><span>
        <asp:Localize ID="locTopReviewedTitle" runat="server" EnableViewState="false"></asp:Localize></span></h2>
    <ul>
        <asp:Repeater ID="rptTopReviewedProdList" runat="server" EnableViewState="false">
            <ItemTemplate>
                <li>
                    <div class="ProductImage">
                        <div class="Image">
                            <div class="ItemType">
                                <%--<asp:Image ID="NewReviewdItemImage" EnableViewState="false" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png") %>'
                                    meta:resourcekey="NewItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "NewProductInd") %>' ClientIDMode="Static"  AlternateText="New" alt="New"  />--%>
                                <span class="rs-new" id="spnRSNew" runat="server" visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>'><span class="rs-new-text">New</span></span>
                                &nbsp;<asp:Image EnableViewState="false" ID="RevFeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                    meta:resourcekey="FeaturedItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' AlternateText="Featured" ToolTip="Featured" />

                            </div>
                            <a id="imgReviewedProduct" class="boxed" href='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'>
                                <img id='<%# "Img"  + DataBinder.Eval(Container.DataItem, "ProductID")%>' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                    src="images/grey1px.gif" class="lazy"
                                    data-original='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "SmallImageFilePath").ToString()) %>'
                                    style="vertical-align: bottom;" title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'>
                                </img>
                            </a>
                        </div>
                    </div>
                    <div class="ProductSpecification">
                        <div class="DetailLink">
                            <asp:HyperLink ID="hlRevName" runat="server" NavigateUrl='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' EnableViewState="false">
                                <%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></asp:HyperLink>
                        </div>
                        <div id="Div1" class="Price" runat="server" visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && BrandProfile.ShowPrice %>'>
                            <asp:Label ID="lblRevPrice" EnableViewState="false" runat="server" meta:resourcekey="Label2Resource1" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>'
                                Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && BrandProfile.ShowPrice %>' ></asp:Label>
                        </div>
                        <div class="CallForPrice">
                            <asp:Label ID="uxRevCallForPricing" EnableViewState="false" runat="server" CssClass="Price" meta:resourcekey="uxCallForPricingResource1"
                                Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>' ></asp:Label>
                        </div>

                        <div class="StarRating">
                            <ZNode:ProductAverageRating ID="uxProductAverageRating" runat="server" TotalReviews='1'
                                ProductName='<%# DataBinder.Eval(Container.DataItem, "Name") %>'
                                ViewProductLink='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString())%>'
                                ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ZNodeBrandTopReview.Rating")%>' EnableViewState="false" 
                                 />
                        </div>
                    </div>
                    <div class="ProductReview">
                        <div class="Reviewver">
                            <div class="ReviewSubject">
                                <asp:Localize ID="Localize1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ZNodeBrandTopReview.Subject")%>' EnableViewState="false"></asp:Localize>
                            </div>
                            <div class="ReviewUser">
                                <asp:Localize ID="locCreateUser" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ZNodeBrandTopReview.CreateUser")%>' EnableViewState="false"></asp:Localize>
                            </div>
                        </div>
                        <div class="ReviewverDetails">
                            <div class="ReviewDetail">
                                <asp:Localize ID="locReviewProduct" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name")+ " Review :" %>' EnableViewState="false"></asp:Localize>
                            </div>
                            <div class="ReviewComment">
                                <asp:Localize ID="locReview" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ZNodeBrandTopReview.Comments")%>' EnableViewState="false"></asp:Localize>
                            </div>
                        </div>
                    </div>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</asp:Panel>


<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Category_SubCategoryList" CodeBehind="SubCategoryList.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>

<asp:Panel ID="pnlSubCategoryList" runat="server" Visible="true"  EnableViewState="false">
    <div class='FeaturedCategory'>
        <div class='SubCategoryList'>
            <div class="Title">
                <h1>
                    <asp:Literal ID="ltrSubCategoryTitle"  EnableViewState="false" runat="server"></asp:Literal></h1>
            </div>
            <asp:Label ID="lblCategoryDescription" runat="server" CssClass="TopCategoryDescription"  EnableViewState="false"></asp:Label>
            <asp:DataList ID="DataListSubCategories" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" EnableViewState="false">
                <ItemStyle CssClass="SubCategoryListItem" />
                <ItemTemplate>
                    <div class='Image'>
                        <a id="A1" enableviewstate="false" href='<%# DataBinder.Eval(Container.DataItem, "ViewCategoryLink").ToString()%>' runat="server">
                            <img id="Img2" enableviewstate="false" alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag").ToString()%>' title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag").ToString()%>' runat="server" style="border: none" src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()) %>' visible='<% # ShowCategoryImage(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>' />
                        </a>
                    </div>

                    <div class="CategoryLink">
                        <a id="A2" enableviewstate="false" href='<%# DataBinder.Eval(Container.DataItem, "ViewCategoryLink").ToString()%>' runat="server">
                            <asp:Label ID="lblName" EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>'></asp:Label>
                        </a>
                    </div>
                    <div class="ShortDescription" visible="false">
                        <asp:Label ID="lblShortDecription" EnableViewState="false" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShortDescription")%>'></asp:Label>
                    </div>
                </ItemTemplate>
            </asp:DataList>
            <asp:DataList ID="DataListParentCategory" runat="server" RepeatDirection="Horizontal"  RepeatLayout="Flow" EnableViewState="false">
                <ItemStyle CssClass="SubCategoryListItem" />
                <ItemTemplate>
                    <div class='Image'>
                        <a enableviewstate="false" href='<%# "~/category.aspx?zcid=" + DataBinder.Eval(Container.DataItem, "CategoryId") %>' runat="server">
                            <img id="Img2" enableviewstate="false" alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag").ToString()%>' title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag").ToString()%>' runat="server" style="border: none" src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()) %>' />
                        </a>
                    </div>
                    <div class="CategoryLink">
                        <a id="A3" enableviewstate="false" href='<%# "~/category.aspx?zcid=" + DataBinder.Eval(Container.DataItem, "CategoryId") %>' runat="server" >
                            <asp:Label ID="lblName" EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>' ></asp:Label>
                        </a>
                    </div>
                    <div class="ShortDescription" visible="false">
                        <asp:Label ID="lblShortDescription" EnableViewState="false" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShortDescription")%>' ></asp:Label>
                    </div>

                </ItemTemplate>
            </asp:DataList>
           
        </div>
          <asp:Label ID="lblBottomCategoryDescription" runat="server"  CssClass="MobBottomCategoryDescription" EnableViewState="false"></asp:Label>
    </div>
</asp:Panel>

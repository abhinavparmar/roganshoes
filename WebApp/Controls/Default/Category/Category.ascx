﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Category_Category"
    CodeBehind="Category.ascx.cs" %>
<%@ Register Src="ProductList.ascx" TagName="CategoryProductList" TagPrefix="ZNode" %>
<%@ Register Src="SubCategoryList.ascx" TagName="SubCategoryList" TagPrefix="ZNode" %>
<%@ Register Src="NavigationCategories.ascx" TagName="NavigationCategories" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductViewed.ascx" TagName="RecentlyViewedProducts"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/navigation/NavigationBreadCrumbs.ascx" TagName="StoreBreadCrumbs"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/ZFacets.ascx" TagName="ZFacets" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/Default/Product/ZProductListCompare.ascx" TagName="ProductListCompare" TagPrefix="Zeon" %>
<%@ Register Src="~/Controls/Default/Category/ZLatestReviewedProductsList.ascx" TagName="LatestReviewList" TagPrefix="Zeon" %>
<ZNode:CustomMessage ID="ucPromoBannerRibbon" runat="server" EnableViewState="false" MessageKey="FestivalPromoBannerRibbon" />
<div id="BreadCrumb">
    <ZNode:StoreBreadCrumbs ID="uxBreadCrumbs" runat="server" ClientIDMode="Static"></ZNode:StoreBreadCrumbs>
</div>
<div class="row">
    <div class="col-lg-12 ProductListing">
        <div id="LeftColumn" class="hidefortab">
            <div id="overlaypopup"></div>
            <div class="MobilePopwarpper">
                <div class="PopUpTitle" style="display: none;">Narrow Result</div>
                <div id="popclose" style="display: none">
                    <button type="button" class="ui-dialog-titlebar-close" onclick="objFacets.ClosePopup()"></button>
                </div>
                <div class="MobilePoppup" id="FacetNavigation">
                    <ZNode:NavigationCategories ID="uxCategories" runat="server" IsShowAllCategories="true" EnableNavigationViewState="false"></ZNode:NavigationCategories>
                    <Zeon:ZFacets ID="ucFacets" runat="server" />
                </div>
            </div>
            <div id="MiniProductCompare">
                <Zeon:ProductListCompare runat="server" ID="ucLeftProductCompare"></Zeon:ProductListCompare>
            </div>
        </div>
        <div id="MiddleColumn">
            <div class="CategoryDetail">
                <asp:Panel ID="ProductDetails" runat="server" ClientIDMode="Static">
                    <h1 class="CategoryTitle">
                        <asp:Literal ID="CategoryTitle" Text="{0}" EnableViewState="false" runat="server" ClientIDMode="Static"></asp:Literal></h1>
                    <asp:Panel ID="pnlCategoryDescription" runat="server" Visible="false" EnableViewState="false">
                        <asp:Label ID="CategoryDescription" runat="server" ClientIDMode="Static" EnableViewState="false" CssClass="CatDec"></asp:Label>
                    </asp:Panel>
                    <div class="Description">
                        <ZNode:CustomMessage ID="uxPromiseBanner" runat="server" MessageKey="ProductListSecurityInfo" EnableViewState="false" />
                    </div>
                    <div class="ProductListError">
                        <asp:Literal ID="ltrSearchResultMessage" EnableViewState="false" runat="server" Visible="false"></asp:Literal>
                    </div>
                </asp:Panel>
                <div class="CategorySpecificBanners">
                    <asp:Literal ID="ltrCategorySpecificBanner" EnableViewState="false" runat="server" ClientIDMode="Static"></asp:Literal>
                </div>
                <%--  <div style="clear: both; display: block; margin-left: 15px;">
                    <ZNode:Banner ID="Banner1" runat="server" BannerKey="CategoryPage" DisplayTime="5000" TransitionTime="1000" ClientIDMode="Static"/>
                </div> --%>
                <div style="clear: both; display: block;">
                    <ZNode:CategoryProductList ID="uxCategoryProductList" runat="server" CssClass="ProductList"
                        Visible="true" Title="Products"></ZNode:CategoryProductList>
                </div>
                <div style="clear: both; display: block;">
                    <ZNode:SubCategoryList ID="uxSubCategoryList" runat="server" CssClass="SubCategoryList"
                        Visible="true" Title="Categories" EnableViewState="false"></ZNode:SubCategoryList>
                </div>
                <%--<div class="AlternateDescription">
                    <asp:Label ID="AdditionalDescription" runat="server" ClientIDMode="Static"></asp:Label>
                </div>--%>
                <asp:Panel ID="pnlMobileCategoryDesc" runat="server" Visible="false">
                    <asp:Label ID="lblMobileCategoryDesc" runat="server" ClientIDMode="Static" EnableViewState="false" CssClass="MobCatDesc"></asp:Label>
                </asp:Panel>
            </div>
        </div>
        <Zeon:LatestReviewList runat="server" ID="ucLatestReviewList" EnableViewState="false"></Zeon:LatestReviewList>
        <div class="RecentItems hidden-xs">
            <ZNode:RecentlyViewedProducts ID="uxRecentlyViewedProducts" runat="server" Title="Recently Viewed Items"
                ShowName="true" ShowReviews="false" ShowImage="true" ShowDescription="false"
                ShowPrice="true" ShowAddToCart="false" ClientIDMode="Static" />
        </div>
    </div>
</div>




using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using System.Linq;
using ZNode.Libraries.DataAccess.Service;
using System.Diagnostics;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.DataAccess.Data;

namespace WebApp
{
    /// <summary>
    /// Represents the Product List user control class.
    /// </summary>
    public partial class Controls_Default_Category_ProductList : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeCategory _category;
        protected int _categoryId;
        private List<int> _ProductId;
        private ZNodeProfile _ProductListProfile = new ZNodeProfile();
        private ZNodeProductList _productList;
        private string _noRecordsText = string.Empty;
        //Zeon Custom Code: Start
        private string _view = string.Empty;
        private string _listActiveURL = "/Themes/" + ZNodeCatalogManager.Theme + "/Images/list-active.gif";
        private string _gridDeActiveURL = "/Themes/" + ZNodeCatalogManager.Theme + "/Images/grid.gif";
        private string _gridActiveURL = "/Themes/" + ZNodeCatalogManager.Theme + "/Images/grid-active.gif";
        private string _listDeActiveURL = "/Themes/" + ZNodeCatalogManager.Theme + "/Images/list.gif";
        private string _ComparePageURL = "/productcomparison.aspx";
        //Zeon Custom Code: End
        #endregion

        #region Protected Static Member Variables
        private int totalRecords = 0;
        private int recCount = 0;
        private int ncurrentpage = 0;
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/view_cart_bg.gif";
        string sessionKey = string.Empty;
        string isAsyncLoad = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableAsyncLoad"]) ? ConfigurationManager.AppSettings["EnableAsyncLoad"].ToString() : "0";
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the product seperator image
        /// </summary>
        public string ProductListSeparatorImage
        {
            get { return "~/themes/" + ZNodeCatalogManager.Theme + "/Images/line_seperator.gif"; }
        }

        /// <summary>
        /// Gets or sets the product list profile
        /// </summary>
        public ZNodeProfile ProductListProfile
        {
            get { return this._ProductListProfile; }
            set { this._ProductListProfile = value; }
        }

        /// <summary>
        /// Gets the page size object
        /// </summary>
        public int PageSize
        {
            get
            {
                int pageSize = 48;
                string eventTarget = "__EVENTTARGET";
                if (ddlTopPaging.SelectedIndex > 0 || ddlBottomPaging.SelectedIndex > 0)
                {
                    if (Request[eventTarget] != null && Request[eventTarget].Contains(ddlTopPaging.ID))
                    {
                        pageSize = Convert.ToInt32(ddlTopPaging.SelectedValue);
                    }
                    else
                    {
                        pageSize = Convert.ToInt32(ddlBottomPaging.SelectedValue);
                    }
                }
                else if (Request.QueryString["s"] != null)
                {
                    pageSize = Convert.ToInt32(Request.QueryString["s"]);
                    if (pageSize == 0)
                    {
                        pageSize = 48;
                    }
                }

                return pageSize;
            }
        }

        /// <summary>
        /// Gets or sets the TotalPages object
        /// </summary>
        public int TotalPages
        {
            get
            {
                int totalPages = 0;

                if (ViewState["TotalPages"] != null)
                {
                    totalPages = (int)ViewState["TotalPages"];
                }

                return totalPages;
            }

            set
            {
                ViewState["TotalPages"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the title for this control
        /// </summary>
        public string NoRecordsText
        {
            get
            {
                return this._noRecordsText;
            }

            set
            {
                this._noRecordsText = value;
            }
        }

        /// <summary>
        /// Gets or sets the product list passed in from the search page
        /// </summary>
        public ZNodeProductList ProductList
        {
            get
            {
                return this._productList;
            }

            set
            {
                this._productList = value;
            }
        }

        /// <summary>
        /// Gets or sets the list of ProductIDs
        /// </summary>
        public List<int> ProductID
        {
            get
            {
                return this._ProductId;
            }

            set
            {
                this._ProductId = value;
            }
        }
        /// <summary>
        /// Gets the CurrentPage object
        /// </summary>  
        public int CurrentPage
        {
            get
            {
                int currentPage = 1;

                if (Request.QueryString["page"] != null)
                {
                    currentPage = Convert.ToInt32(Request.QueryString["page"]);
                    if (currentPage <= 0)
                    {
                        currentPage = 1;
                    }
                }

                return currentPage;
            }
        }

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets or sets the FirstIndex object
        /// </summary>
        private int FirstIndex
        {
            get
            {
                int firstIndex = 0;
                if (ViewState["FirstIndex"] != null)
                {
                    firstIndex = Convert.ToInt32(ViewState["FirstIndex"]);
                }

                return firstIndex;
            }

            set
            {
                ViewState["FirstIndex"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the LastIndex object
        /// </summary>
        private int LastIndex
        {
            get
            {
                int lastIndex = 0;

                if (ViewState["LastIndex"] != null)
                {
                    lastIndex = Convert.ToInt32(ViewState["LastIndex"]);
                }

                return lastIndex;
            }

            set
            {
                ViewState["LastIndex"] = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Represents the CheckFor Call for pricing message
        /// </summary>
        /// <param name="fieldValue">passed the field value </param>
        /// <returns>Returns the Call For pricing message </returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());
            //string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID); //Zeon Custom code

            if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
            {
                message = callMessage.ToString();
            }

            if (Status)
            {
                return message;
            }
            else if (!this._ProductListProfile.ShowPrice)
            {
                return message;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// retrieve Product URL form View Link
        /// </summary>
        /// <param name="ViewProductLink">string</param>
        /// <returns>string</returns>
        public string GetProductUrl(string ViewProductLink)
        {
            string Url = ViewProductLink;
            if (this._categoryId > 0)
            {
                // Add to session            
                Session["BreadCrumbCategoryId"] = this._categoryId;
            }

            return Url;
        }

        /// <summary>
        /// Represents the GetColoroption method
        /// </summary>
        /// <param name="AlternateProductImageCount">Alternate Product Image Count</param>
        /// <returns>Returns the Color Option</returns>
        public string GetColorCaption(int AlternateProductImageCount)
        {
            string output = string.Empty;

            if (AlternateProductImageCount > 0)
            {
                output = this.GetLocalResourceObject("ProductColorMessage").ToString();
            }

            return output;
        }
        #endregion

        #region Bind Data

        // <summary>
        // Bind control display based on category object passed in
        // </summary>
        public void BindCategory()
        {
            if (Visible)
            {
                if (!Page.IsPostBack)
                {
                    this._productList = new ZNodeProductList();

                    // Set initial page value
                    ViewState["CurrentPage"] = 1;
                }
            }
        }

        /// <summary>
        /// Bind the Tagged Products.
        /// </summary>
        public void BindTagProducts()
        {
            ViewState["CurrentPage"] = 1;

            //Zeon Custom Code :: Changes done on date  :: 24/july/2014 :: Code commented as Method calling tiwce :: executed from page load - start
            //this.BindDataListPagedDataSource();
            //Zeon Custom Code :: Changes done on date  :: 24/july/2014 :: Code commented as Method calling tiwce :: executed from page load - end
        }

        #endregion

        #region Paging DataList Control Events

        /// <summary>
        /// Event triggered when a DataBind Method is called for DataListProducts
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DataListProducts_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            // Find the seperator image and then remove for the last column
            if (e.Item.ItemType == ListItemType.Separator)
            {
                int lastColumnIndex = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
                if ((e.Item.ItemIndex + 1) % lastColumnIndex == 0 && e.Item.ItemIndex != 0)
                {
                    foreach (Control ctrl in e.Item.Controls)
                    {
                        if (ctrl.GetType().ToString() == "System.Web.UI.HtmlControls.HtmlImage")
                        {
                            e.Item.Controls.Remove(ctrl);
                        }
                    }
                }
            }
            else if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
            {
                if (Session["CompareProductIDs"] != null)
                {
                    HiddenField hdnProductId = e.Item.FindControl("hdnPID") as HiddenField;
                    CheckBox chk = e.Item.FindControl("chkCompareProduct") as CheckBox;

                    int productID = Convert.ToInt32(hdnProductId.Value);
                    List<CompareProducts> comparableProducts = Session["CompareProductIDs"] as List<CompareProducts>;
                    foreach (CompareProducts prods in comparableProducts)
                    {
                        if (prods.ProductID.Equals(productID))
                        {
                            //chk.Checked = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="source">Source object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlTopPaging_SelectedIndexChanged(object source, EventArgs e)
        {
            // this.NavigationToUrl(1, Convert.ToInt32(ddlTopPaging.SelectedValue), lstFilter); //Zeon Custom Code
            this.NavigationToUrl(1, Convert.ToInt32(ddlTopPaging.SelectedValue));
        }

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBottomPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue), lstFilter);  //Zeon Custom Code
            this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue));
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["sort"] != null)
                {
                    lstFilter.SelectedValue = Request.QueryString["sort"].ToString();
                    lstFilterBottom.SelectedValue = Request.QueryString["sort"].ToString();//Zeon Custom Code
                }

                //Zeon Custom Code: Start
                SetAndGetProductComparisions();
                //Zeon Custom Code: End

            }
            if (Request.Params["zcid"] != null)
            {
                int.TryParse(Request.Params["zcid"], out this._categoryId);
            }

            // Set Repeat columns to the product Data list
            DataListProducts.RepeatColumns = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;

            if (this._categoryId > 0)
            {
                // Retrieve category data from httpContext (set previously in the page_preinit event)
                this._category = (ZNodeCategory)HttpContext.Current.Items["Category"];

                // Add to session            
                Session["BreadCrumbCategoryId"] = this._categoryId;
                //Zeon Custom Code:Starts
                int showSubCatPrdList = 0;
                int.TryParse(this._category.Custom2, out showSubCatPrdList);
                if (ZCommonHelper.IsCustomMulitipleAttributePortal())
                {
                    if ((IsSubCategory() == false || showSubCatPrdList == 1))
                    {
                        CallProductList();
                    }
                }
                else
                {
                    if ((IsSubCategory() == false || showSubCatPrdList == 1) || Convert.ToBoolean(this._category.ISShowSubCategory) == false)
                    {
                        CallProductList();
                    }
                }
            }

        }

        private void CallProductList()
        {
            //Zeon Custom Code: Start
            ErrorMsg.Visible = false;
            if (Request.Params["v"] != null)
            {
                _view = Request.Params["v"];
            }
            else
            {
                _view = "g";
                imgbtnGrid.Enabled = false;
                imgGrid1.Enabled = false;
            }
            //Zeon Custom Code: End
            this.BindCategory();

            ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(this.PageSize.ToString()));
            ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;

            string baseUrl = this.GetNavigationUrl();

            if (!Page.IsPostBack)
            {
                this.BindDataListPagedDataSource();
            }

            //// Do not add the & or ? in the following query string.
            //Zeon Custom Code: Start
            //hlTopPrevLink.NavigateUrl = hlBotPrevLink.NavigateUrl = string.Format("{0}page={1}&size={2}&sort={3}&v={4}", baseUrl, this.CurrentPage - 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue, _view);
            // hlTopNextLink.NavigateUrl = hlBotNextLink.NavigateUrl = string.Format("{0}page={1}&size={2}&sort={3}&v={4}", baseUrl, this.CurrentPage + 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue, _view);

            hlTopPrevLink.NavigateUrl = hlBotPrevLink.NavigateUrl = string.Format("{0}page={1}&s={2}&sort={3}", baseUrl, this.CurrentPage - 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue);
            hlTopNextLink.NavigateUrl = hlBotNextLink.NavigateUrl = string.Format("{0}page={1}&s={2}&sort={3}", baseUrl, this.CurrentPage + 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue);
            //Zeon Custom Code: End
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        /// <summary>
        /// Event triggered when LstFilter control is Selected Index Changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstFilter.SelectedValue = lstFilterBottom.SelectedValue = lstFilter.SelectedValue;//Zeon Custom Code
            //this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue), lstFilter);
            this.NavigationToUrl(this.CurrentPage, Convert.ToInt32(ddlBottomPaging.SelectedValue)); //Zeon Custom Code
        }


        #endregion

        #region Paging Methods & Events

        /// <summary>
        /// Navigate to the category page with query string the specified current page and page size.
        /// </summary>
        /// <param name="currentPage">Current page index.</param>
        /// <param name="pageSize">Category page size. If Show All selected then -1 will be used.</param>
        private void NavigationToUrl(int currentPage, int pageSize)
        {
            string baseUrl = this.GetNavigationUrl();

            // Do not add the & or ? in the following query string.
            //string navigateUrl = string.Format("{0}page={1}&size={2}&sort={3}", baseUrl, currentPage, pageSize, lstFilter.SelectedValue);
            //Zeon Custom Code :Start
            string view = "g";
            if (imgGrid1.CssClass == "ActiveLink")
            {
                view = "g";
            }
            else
            {
                view = "l";
            }

            // Do not add the & or ? in the following query string.
            // string navigateUrl = string.Format("{0}page={1}&size={2}&sort={3}&v={4}", baseUrl, currentPage, pageSize, lstFilter.SelectedValue, view);//Zeon Customization commented for list view and grid view
            string navigateUrl = string.Format("{0}page={1}&s={2}&sort={3}", baseUrl, currentPage, pageSize, lstFilter.SelectedValue);
            //Zeon Custom Code :End
            Response.Redirect(navigateUrl);

        }

        /// <summary>
        /// Gets the navigation url for the previous/next navigation link
        /// </summary>
        /// <returns>Returns the navigation base url.</returns>
        private string GetNavigationUrl()
        {
            string baseUrl = string.Empty;
            if (this._category.SEOURL.Trim() == string.Empty)
            {
                StringBuilder url = new StringBuilder();
                url.Append(Request.Url.GetLeftPart(UriPartial.Authority));
                url.Append(Request.Url.AbsolutePath);

                // Append the category Url with base url
                if (Request.QueryString["zcid"] != null)
                {
                    url.Append("?zcid=").Append(Request.QueryString["zcid"]);
                }

                url.Append("&");
                baseUrl = url.ToString();
            }
            else
            {
                //string rawUrl = string.Format("{0}.aspx", this._category.SEOURL);

                //Zeon Custom Code : Remove .aspx extension
                string rawUrl = string.Format("{0}", this._category.SEOURL);
                baseUrl = string.Format("{0}/{1}?", Request.Url.GetLeftPart(UriPartial.Authority), rawUrl);
            }

            foreach (String key in Request.QueryString.AllKeys)
            {
                //Zeon Custom Code: Start
                if (key == null)
                    continue;
                //if (key.ToLower() == "zcid" || key.ToLower() == "page" || key.ToLower() == "size" || key.ToLower() == "sort")
                if (key.ToLower() == "zcid" || key.ToLower() == "page" || key.ToLower() == "s" || key.ToLower() == "sort" || key.ToLower() == "v")
                    continue;
                //Zeon Custom Code: End

                if (Request.QueryString[key].Contains(","))
                {
                    string[] facets = Request.QueryString[key].Split(',');
                    foreach (string facet in facets)
                    {
                        baseUrl = baseUrl + key + "=" + HttpUtility.UrlEncode(facet) + "&";
                    }
                }
                else
                {
                    baseUrl = baseUrl + key + "=" + HttpUtility.UrlEncode(Request.QueryString[key]) + "&";
                }
            }

            return baseUrl;
        }

        /// <summary>
        /// Bind Datalist using Paged DataSource object
        /// </summary>
        private void BindDataListPagedDataSource()
        {
            //Zeon Custom Code : Start
            if (Request.QueryString["v"] != null)
            {
                if (Request.QueryString["v"].Equals("g", StringComparison.InvariantCultureIgnoreCase))
                {
                    SetGridView();
                }
                else
                {
                    SetListView();
                }
            }
            //Zeon Custom Code : End

            #region[Stop watch Diagnostics]
            // Create new stopwatch
            /*
            Stopwatch stopwatch = new Stopwatch();

            // Begin timing
            stopwatch.Start();
            //End time
            stopwatch.Stop();

            timeElapsedOnLoad.Text = "Bind Product List Time In MiliSecond :: " + stopwatch.ElapsedMilliseconds;
            */
            #endregion



            this._productList = new ZNodeProductList();
            if (ProductID != null && ProductID.Any())
            {
                //this._productList = ZNodePaging.GetProducts(ProductID, GetSortField(Request.QueryString["sort"]), GetSortDirection(Request.QueryString["sort"]), this.CurrentPage, this.PageSize, 1);//Old Code
                this._productList = ZNodePaging.GetProductsExtn(ProductID, GetSortField(Request.QueryString["sort"]), GetSortDirection(Request.QueryString["sort"]), this.CurrentPage, this.PageSize, 1, this._category.CategoryID);//Zeon Custom function call with all sorting enabled.
            }

            if (isAsyncLoad.Equals("1", StringComparison.OrdinalIgnoreCase))
            {
                sessionKey = "CategoryProductList" + Guid.NewGuid();
                HttpContext.Current.Session[sessionKey] = this._productList.ZNodeProductCollection;
            }

            DataListProducts.DataSource = this._productList.ZNodeProductCollection;
            DataListProducts.DataBind();

            //Zeon Custom Code: Start
            if (this._productList.TotalRecordCount > 0)
            {
                ltrTopTotalCounts.Text = Convert.ToString(this._productList.TotalRecordCount);
                ltrBottomTotalCount.Text = Convert.ToString(this._productList.TotalRecordCount);
                locBottomPageNum.Text = locTopPageNum.Text = string.Format(this.GetLocalResourceObject("txtPage").ToString(), this.CurrentPage);
                locTopPageTotal.Text = locBottomPageTotal.Text = string.Format(this.GetLocalResourceObject("txtOF").ToString(), this._productList.TotalPageCount);
            }
            //Zeon Custom Code: End

            // Disable the data list item view state.
            foreach (DataListItem item in DataListProducts.Items)
            {
                item.EnableViewState = false;
            }

            this.ncurrentpage = this.CurrentPage;
            this.recCount = this._productList.TotalPageCount;
            this.totalRecords = this._productList.TotalRecordCount;

            if (this._productList.ZNodeProductCollection.Count.Equals(0))
            {
                pnlProductList.Visible = false;
                ErrorMsg.Visible = true;
                ErrorMsg.Text = string.Empty;
            }
            else
            {
                pnlProductList.Visible = true;
                ErrorMsg.Visible = false;
            }

            hlTopNextLink.Enabled = hlBotNextLink.Enabled = !(this.ncurrentpage.Equals(this.recCount));
            hlTopPrevLink.Enabled = hlBotPrevLink.Enabled = !(this.ncurrentpage.Equals(1));

            if (!this.IsPostBack)
            {
                this.DoPaging();
            }
        }

        /// <summary>
        /// Binding Paging List
        /// </summary>
        private void DoPaging()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("PageIndex");
            dt.Columns.Add("PageText");
            this.FirstIndex = this.CurrentPage - 5;
            if (this.CurrentPage > 5)
            {
                this.LastIndex = this.CurrentPage + 5;
            }
            else
            {
                this.LastIndex = 10;
            }

            if (this.LastIndex > this.TotalPages)
            {
                this.LastIndex = this.TotalPages;
                this.FirstIndex = this.LastIndex - 10;
            }

            if (this.FirstIndex < 0)
            {
                this.FirstIndex = 0;
            }

            ddlTopPaging.Items.Clear();
            ddlBottomPaging.Items.Clear();

            string pagingText = this.GetLocalResourceObject("PagingText").ToString();

            // Each items seperated with & symbol
            string[] pagingItems = pagingText.Split(new char[] { '&' });
            foreach (string pagingItem in pagingItems)
            {
                if (pagingItem.Contains("|") && pagingItem.Trim().Length > 0)
                {
                    // Key value pair seperated with | symbol
                    string[] itemText = pagingItem.Split(new char[] { '|' });
                    DataRow dr = null;
                    dr = dt.NewRow();
                    dr[0] = Convert.ToInt32(itemText[1].Trim());
                    dr[1] = itemText[0].Trim();
                    dt.Rows.Add(dr);
                }
            }

            this.ddlTopPaging.DataTextField = "PageText";
            this.ddlTopPaging.DataValueField = "PageIndex";
            this.ddlTopPaging.DataSource = dt;
            this.ddlTopPaging.DataBind();

            this.ddlBottomPaging.DataTextField = "PageText";
            this.ddlBottomPaging.DataValueField = "PageIndex";
            this.ddlBottomPaging.DataSource = dt;
            this.ddlBottomPaging.DataBind();

            string defaultSize = "48";
            if (Request.QueryString["s"] != null)
            {
                int size = Convert.ToInt32(Request.QueryString["s"].ToString());
                if (size == 0)
                {
                    ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(defaultSize));
                    ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
                }
                else
                {
                    ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(size.ToString()));
                    ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
                }
            }
            else
            {
                ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(defaultSize));
                ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
            }
        }

        /// <summary>
        /// Get Sort Field
        /// </summary>
        private string GetSortField(string sort)
        {
            string sortOrder = string.Empty;

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort)
                {
                    case "2": return "FinalPrice";
                    case "3": return "FinalPrice";
                    case "4": return "Name";
                    case "5": return "Name";
                    case "6": return "Rating";
                }
            }
            return null;
        }

        /// <summary>
        /// Get Sort Direction
        /// </summary>
        private string GetSortDirection(string sort)
        {
            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort)
                {
                    case "2": return "ASC";
                    case "3": return "DESC";
                    case "4": return "ASC";
                    case "5": return "DESC";
                    case "6": return "DESC";
                }
            }
            return null;
        }

        #endregion

        # region Zeon Custom Methods

        /// <summary>
        /// Shows the list view of Product List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgbtnList_OnClick(object sender, EventArgs e)
        {
            SetListView();
            NavigationToUrl(this.CurrentPage, this.PageSize);
        }

        /// <summary>
        /// Set List View of product
        /// </summary>
        private void SetListView()
        {
            DataListProducts.RepeatColumns = 1;
            DataListProducts.RepeatDirection = RepeatDirection.Vertical;

            imgbtnGrid.Enabled = true;
            imgbtnGrid.CssClass = "DeActiveLink GridView";
            imgGrid1.Enabled = true;

            imgbtnList.Enabled = false;
            imgbtnList.CssClass = "ActiveLink ListView";
            imgListActive1.Enabled = false;

            imgListActive1.ImageUrl = _listActiveURL;

            imgGrid1.ImageUrl = _gridDeActiveURL;

            imgListActive1.CssClass = "ActiveLink";

            imgGrid1.CssClass = "DeActiveLink";

        }

        /// <summary>
        /// Shows the Grid View of Product list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgbtnGrid_OnClick(object sender, EventArgs e)
        {

            SetGridView();
            NavigationToUrl(this.CurrentPage, this.PageSize);


        }

        /// <summary>
        /// Set Grid view of product list
        /// </summary>
        private void SetGridView()
        {
            DataListProducts.RepeatColumns = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
            DataListProducts.RepeatDirection = RepeatDirection.Horizontal;

            imgbtnGrid.CssClass = "ActiveLink GridView";
            imgbtnGrid.Enabled = false;
            imgGrid1.Enabled = false;

            imgbtnList.CssClass = "DeActiveLink ListView";
            imgbtnList.Enabled = true;
            imgListActive1.Enabled = true;

            imgListActive1.ImageUrl = _listDeActiveURL;

            imgGrid1.ImageUrl = _gridActiveURL;

            imgListActive1.CssClass = "DeActiveLink";

            imgGrid1.CssClass = "ActiveLink";
        }

        /// <summary>
        /// Compare the products
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCompareProductList_Click(object sender, EventArgs e)
        {
            if (Session["CompareProductIDs"] != null)
            {
                List<CompareProducts> comparableProducts = Session["CompareProductIDs"] as List<CompareProducts>;
                if (comparableProducts != null && comparableProducts.Count < 2)
                {
                    ShowMininumItemsCompareError();
                }
                else
                {
                    ShowMininumItemsCompareError();
                }
            }
            else
            {
                ShowMininumItemsCompareError();
            }
        }

        /// <summary>
        /// Show Error of Minimum Comapre Items
        /// </summary>
        private void ShowMininumItemsCompareError()
        {
            ErrorMsg.Visible = true;
            ErrorMsg.Text = GetLocalResourceObject("ShowMinItemsCompareError").ToString();
        }

        /// <summary>
        /// Set And get Product Comparisions from session
        /// </summary>
        private void SetAndGetProductComparisions()
        {
            if (Session["CompareProductIDs"] != null)
            {
                List<CompareProducts> comparableProducts = Session["CompareProductIDs"] as List<CompareProducts>;
                hdnTotalComparableProducts.Value = comparableProducts != null && comparableProducts.Count > 0 ? Convert.ToString(comparableProducts.Count) : "0";
                hdnPreviousCategory.Value = comparableProducts != null && comparableProducts.Count > 0 ? Convert.ToString(comparableProducts[0].CategoryID) : string.Empty;
            }
            hdnCompareProductCount.Value = ConfigurationManager.AppSettings["CompareProductsCount"] != null ? Convert.ToString(ConfigurationManager.AppSettings["CompareProductsCount"]) : Convert.ToString(4);
        }

        /// <summary>
        /// Check wheather Sub category is Present or not
        /// </summary>
        /// <returns></returns>
        private bool IsSubCategory()
        {
            if (this._category != null)
            {
                if (this._category.SubCategoryGridVisibleInd)
                {
                    if (this._category.ZNodeCategoryCollection.Count == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var zeonCatProdList = new CategoryProductList({");
            script.Append("\"Controls\":{");
            script.AppendFormat("\"{0}\":\"{1}\"", hdnTotalComparableProducts.ID, hdnTotalComparableProducts.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", hdnCompareProductCount.ID, hdnCompareProductCount.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", hdnPreviousCategory.ID, hdnPreviousCategory.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", divCategoryChangedWarning.ID, divCategoryChangedWarning.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", divAddToCompareFailed.ID, divAddToCompareFailed.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", divMaxProductLimitReached.ID, divMaxProductLimitReached.ClientID);

            script.AppendFormat(",\"{0}\":\"{1}\"", divAlreadyProductExist.ID, divAlreadyProductExist.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", lblAddMoreItemsTemp.ID, lblAddMoreItemsTemp.ClientID);

            script.AppendFormat(",\"{0}\":\"{1}\"", lblMaxProductWarning.ID, lblMaxProductWarning.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", divAddMoreItems.ID, divAddMoreItems.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", lblAddMoreItems.ID, lblAddMoreItems.ClientID);
            script.Append("},\"Messages\":{");
            script.AppendFormat("\"WarningTitle\":\"{0}\"", GetLocalResourceObject("WarningTitle").ToString());
            script.AppendFormat(",\"ProdctAddedTitle\":\"{0}\"", GetLocalResourceObject("ProdctAddedTitle").ToString());
            script.AppendFormat(",\"AddToCompareFailedTitle\":\"{0}\"", GetLocalResourceObject("AddToCompareFailedTitle").ToString());
            script.AppendFormat(",\"ProductListSessionKey\":\"{0}\"", sessionKey);
            script.Append("}");
            script.Append("});zeonCatProdList.Init();");

            StringBuilder quckwatchscript = new StringBuilder();
            quckwatchscript.Append("var quickViewHelper = new QuickViewHelper({");
            quckwatchscript.Append("\"Controls\":{");
            quckwatchscript.AppendFormat("\"{0}\":\"{1}\"", ifzQuickView.ID, ifzQuickView.ClientID);
            quckwatchscript.Append("},\"Messages\":{");
            quckwatchscript.Append("}");
            quckwatchscript.Append("});quickViewHelper.Init();");

            this.Page.ClientScript.RegisterStartupScript(GetType(), "CategoryProductList", script.ToString(), true);

            this.Page.ClientScript.RegisterStartupScript(GetType(), "QuickwatchScript", quckwatchscript.ToString(), true);
        }

        /// <summary>
        /// Compare product button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCompareProduct_Click(object sender, EventArgs e)
        {
            if (Session["CompareProductIDs"] != null)
            {
                List<CompareProducts> comparableProducts = Session["CompareProductIDs"] as List<CompareProducts>;
                if (comparableProducts != null && comparableProducts.Count < 2)
                {
                    ShowMininumItemsCompareError();
                }
                else if (comparableProducts != null && comparableProducts.Count == 4)
                {
                    Response.Redirect(_ComparePageURL);
                }
                else
                {
                    ShowMininumItemsCompareError();
                }
            }
            else
            {
                ShowMininumItemsCompareError();
            }
        }

        /// <summary>
        /// set shortdescription visibility
        /// </summary>
        /// <returns></returns>
        protected bool SetShortDescriptionVisibility()
        {
            bool isVisible = false;
            if (Request.Params["v"] != null && !string.IsNullOrEmpty(Request.Params["v"].ToString()))
            {
                if (Request.Params["v"].ToString().Equals("l")) { isVisible = true; }
            }
            return isVisible;
        }

        /// <summary>
        /// Event triggered when LstFilter control is Selected Index Changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstFilterBottom_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstFilter.SelectedValue = lstFilterBottom.SelectedValue = lstFilterBottom.SelectedValue;
            this.NavigationToUrl(this.CurrentPage, Convert.ToInt32(ddlBottomPaging.SelectedValue));

        }

        # endregion

    }
}
<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Product_Product" CodeBehind="Product.ascx.cs" %>
<%@ Register Src="~/Controls/Default/navigation/NavigationBreadCrumbs.ascx" TagName="StoreBreadCrumbs" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductViewed.ascx" TagName="RecentlyViewedProducts" TagPrefix="ZNode" %>
<%@ Register Src="ProductHighlights.ascx" TagName="ProductHighlights" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register Src="ProductPrice.ascx" TagName="ProductPrice" TagPrefix="ZNode" %>
<%@ Register Src="ProductRelated.ascx" TagName="ProductRelated" TagPrefix="ZNode" %>
<%@ Register Src="ZProductAttributes.ascx" TagName="ProductAttributes" TagPrefix="ZNode" %>
<%@ Register TagName="AternateImages" TagPrefix="Zeon" Src="~/Controls/Default/Product/ZProductAlternateImages.ascx" %>
<%@ Register Src="ZGroupProductAttributes.ascx" TagName="GroupProductAttributes" TagPrefix="ZNode" %>
<%@ Register Src="ProductTabs.ascx" TagName="ProductTabs" TagPrefix="ZNode" %>
<%@ Register Src="ProductAddOns.ascx" TagName="ProductAddOns" TagPrefix="ZNode" %>
<%@ Register Src="ProductAttributeGrid.ascx" TagName="ProductAttributeGrid" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/Bundleproducts.ascx" TagName="BundleProduct" TagPrefix="ZNode" %>
<%@ Register Src="CatalogImage.ascx" TagName="CatalogItemImage" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ExternalLinks.ascx" TagName="ExternalLinks" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/Banner.ascx" TagName="Banner" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/CheerandPom/Product/ZProductSkuImages.ascx" TagName="ZProductSkuImages" TagPrefix="Zeon" %>


<%--<script type="text/javascript">
    var isCPActive = '<%# WebApp.ZCommonHelper.IsCustomMulitipleAttributePortal() %>';
    if (isCPActive != null && isCPActive == false) {
        var sidecar = sidecar || {};
        sidecar.product_info = {
            ProductNum: '<%= _productNum %>'
        }
    };
</script>--%>
<div id="MiddleColumn" class="ProductDetailsSection clearfix">
    <asp:UpdatePanel ID="UpdatePnlProductDetail" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="ProductDetail">
                <uc1:CustomMessage ID="ucPromoBannerRibbon" runat="server" EnableViewState="false" MessageKey="FestivalPromoBannerRibbon" />
                <div id="ProductDetailContainer" itemscope itemtype="http://schema.org/Product">
                    <asp:Literal ID="ltrProductMetaTag" runat="server" EnableViewState="false"></asp:Literal>
                    <div class="MainProductBlock clearfix">
                        <div class="LeftColumn">
                            <ZNode:StoreBreadCrumbs ID="BREAD_CRUMBS" runat="server" IsProductPage="true" EnableViewState="false"></ZNode:StoreBreadCrumbs>
                            <div class="ProductArea">
                                <div class="Image">
                                    <div id="BrandLabel" runat="server" visible="false" class="BrandImage" enableviewstate="false">
                                        <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtBRAND" EnableViewState="false"></asp:Localize>:&nbsp;<asp:HyperLink
                                            ID="BrandLink" runat="server" EnableViewState="false" meta:resourcekey="BrandLinkResource1"></asp:HyperLink>
                                    </div>
                                    <asp:UpdatePanel ID="upCatalogImage" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="CataLogImage">
                                                <ZNode:CatalogItemImage ID="CatalogItemImage" runat="server" />
                                                </a>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div id="PopupVideo" class="ProductVideo" style="display: none">
                                        <iframe id="ifrmYoutubeVideo" title="Youtube Video" class="YouTubeFrame" runat="server"></iframe>
                                    </div>
                                    <div class="ViewZoom">
                                        <div class="EnlargeImg">
                                            <asp:HiddenField ID="hdnfLargeImagename" runat="server" />
                                            <asp:HiddenField ID="hdnfSmallImagename" runat="server" />
                                            <%-- <div id="divLargeImage" runat="server" class="ViewLargeImage">
                                            <a title="Enlarge Image" id="imgoverlay" onclick="objProduct.OpenImageZoomOverlay();" class="Button">
                                                <asp:Localize ID="locZoomImage" EnableViewState="false" runat="server" meta:resourcekey="btnViewZoomImage"></asp:Localize>
                                            </a>
                                        </div>--%>
                                        </div>
                                    </div>
                                </div>                               
                                <div class="ThumbnailImage">
                                    <asp:UpdatePanel ID="upnlAlternateImage" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="AlternateImageSection">
                                                <%--<div id="gallery_01" class="ItemImg">--%>
                                                    <Zeon:AternateImages ID="uxAternateImages" runat="server" MaxDisplayColumns="4" IsProductPage="true" />
                                               <%-- </div>--%>
                                            </div>
                                            <span class="Prodict-video">
                                                <img id="imgYouTubeVideo" runat="server" src="/Themes/Default/Images/youtube-image.png" onclick="objProduct.ShowVideoArena();" visible="false" alt="Youtube Video" title="Youtube Video" /></span>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <asp:Literal ID="ltrProductTierPrice" runat="server" EnableViewState="false"></asp:Literal>
                            </div>
                        </div>
                        <div class="RightColumn">
                            <div>
                                <h1 class="ProductTitle" itemprop="name">
                                    <asp:Literal ID="ProductTitle" Text="{0}" EnableViewState="false" runat="server" meta:resourcekey="ProductTitleResource1"></asp:Literal>
                                </h1>
                            </div>
                            <div class="ProductPageNewItem" id="divProdPageNewItem" runat="server" visible="false">
                                <%--<asp:Image ID="NewItemImage" runat="server" Visible="False" meta:resourcekey="NewItemImageResource1" EnableViewState="false" AlternateText="New" alt="New" />--%>
                                <span class="rs-new"><span class="rs-new-text">New</span></span>
                            </div>
                            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                <div class="ProductNumbStock">
                                    <span class="ProductNumber" itemprop="sku">
                                        <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtITEM" EnableViewState="false"></asp:Localize>:&nbsp;#<asp:Label
                                            ID="ProductID" runat="server" EnableViewState="false" meta:resourcekey="ProductIDResource1"></asp:Label>
                                    </span>
                                    <span>
                                        <div>
                                            <link itemprop="availability" href="http://schema.org/InStock">
                                        </div>
                                        <asp:UpdatePanel runat="server" ID="upnlStock" UpdateMode="Conditional" RenderMode="Inline" Visible="true">
                                            <ContentTemplate>
                                                <div>
                                                    <asp:Label ID="lblstockmessage" runat="server" meta:resourcekey="lblstockmessageResource1" EnableViewState="false"></asp:Label>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </span>
                                </div>
                                <div class="StarRating">
                                    <ZNode:ProductAverageRating ID="uxReviewRating" runat="server" />
                                    | <a id="Hyperlink" runat="server" enableviewstate="false" meta:resourcekey="lnkWriteaReview">
                                        <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtWriteaReview" EnableViewState="false"></asp:Localize></a>
                                </div>
                                <div class="PriceContent">
                                    <ZNode:ProductPrice ID="uxProductPrice" runat="server" IsProductPage="true" />
                                    <asp:Image ID="FeaturedItemImage" EnableViewState="false" runat="server" Visible="False" ImageAlign="AbsMiddle"
                                        meta:resourcekey="FeaturedItemImageResource1" AlternateText="Featured Item" ToolTip="FeaturedItem" />
                                </div>
                            </div>
                            <div class="StarRight">
                                <ZNode:ProductHighlights ID="uxHighlights" MaxDisplayColumns="10" runat="server" Visible="False" />
                            </div>
                            <div class="ProductAttributes">
                                <asp:Panel ID="pnlDefaultButton" runat="server" DefaultButton="uxAddToCart">
                                    <asp:UpdatePanel ID="UpdPnlOrderingOptions" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="OrderingOptions">
                                                <ZNode:ProductAttributes ID="uxProductAttributes" runat="server" />
                                                <ZNode:ProductAddOns ID="uxProductAddOns" runat="server" ShowCheckBoxesVertically="true"
                                                    ShowRadioButtonsVerically="true"></ZNode:ProductAddOns>
                                                <asp:Panel ID="pnlQty" runat="server" Visible="False" meta:resourcekey="pnlQtyResource1" DefaultButton="uxAddToCart">
                                                    <table class="Quantity">
                                                        <tr>
                                                            <td class="QuantityOption">
                                                                <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtQuantity"></asp:Localize>
                                                                : 
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtQuantity" runat="server" MaxLength="5" Text="1" ValidationGroup="vgProductDetail" CssClass="qtytxt" alt="Quantity" title="Quantity"></asp:TextBox>
                                                                <ajaxToolKit:FilteredTextBoxExtender FilterType="Numbers" TargetControlID="txtQuantity" ID="fitTxtQty" runat="server">
                                                                </ajaxToolKit:FilteredTextBoxExtender>
                                                            </td>
                                                            <td>
                                                                <asp:UpdatePanel ID="pnlAddToCart" runat="server" UpdateMode="Always">
                                                                    <ContentTemplate>
                                                                        <div id="divAddToCart" runat="server">
                                                                            <div class="AddToCartButton">
                                                                                <asp:LinkButton CssClass="addtocartText" ID="uxAddToCart" runat="server" ValidationGroup="vgProductDetail"
                                                                                    meta:resourcekey="uxAddToCartResource1" OnClick="UxAddToCart_Click"></asp:LinkButton>
                                                                                <div class="CallForPrice">
                                                                                    <asp:Label ID="uxCallForPricing" EnableViewState="false" runat="server" Text="Call For Pricing" Visible="False"
                                                                                        meta:resourcekey="uxCallForPricingResource1"></asp:Label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <div class="QuantityInHand">
                                                    <asp:Literal ID="ltrQuantityInHand" runat="server" ClientIDMode="Static"></asp:Literal>
                                                </div>

                                                <div class="SalePrice">
                                                    <asp:Literal EnableViewState="False" ID="AdditionalPrice" runat="server" meta:resourcekey="AdditionalPriceResource1"></asp:Literal>
                                                </div>
                                                <div class="StatusMsg">
                                                    <div>
                                                        <asp:RequiredFieldValidator ID="rfvQty" CssClass="Error" runat="server" ControlToValidate="txtQuantity" ValidationGroup="vgProductDetail" Display="Dynamic" SetFocusOnError="true" ErrorMessage="<%$ Resources:CommonCaption,QuantityRequired %>" />
                                                        <asp:RangeValidator ID="rgvQuantity" CssClass="Error" runat="server" ControlToValidate="txtQuantity" MaximumValue="99999" MinimumValue="1"
                                                            Display="Dynamic" SetFocusOnError="true" Type="Integer" ErrorMessage="<%$ Resources:CommonCaption,ProductQuantityRange %>" ValidationGroup="vgProductDetail"></asp:RangeValidator>
                                                    </div>
                                                    <asp:Label ID="uxStatus" EnableViewState="False" runat="server" CssClass="Error"
                                                        meta:resourcekey="uxStatusResource1"></asp:Label>
                                                </div>
                                                <div class="Error" id="cookiediv">
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                                <uc1:CustomMessage ID="ucFestiveOrderShipping" runat="server" MessageKey="FestiveOrderShippingProductDetail" EnableViewState="false" />
                                <uc1:CustomMessage ID="uxFreeShipping" runat="server" MessageKey="AlwaysFreeShippingMsgKey" EnableViewState="false" />
                                <div class="StoreInventory" runat="server" id="divStoreInventory">
                                    <asp:UpdatePanel ID="upnlStoreInventory" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <%--<div class="ProductStoreLocator">
                                                <asp:HyperLink ID="hlnkStoreLocator" runat="server" EnableViewState="false" meta:resourcekey="hlnkStoreLocatorResource1"></asp:HyperLink>
                                            </div>--%>
                                            <%-- <h4>
                                                <asp:Label ID="lblStorewiseInventory" runat="server"></asp:Label>
                                            </h4>--%>
                                            <div id="InStoreAvailability">
                                                <span id="infoIcon" runat="server" class="fa-info-circle">
                                                    <asp:LinkButton ID="lnkInStoreAvailability" runat="server" meta:resourceKey="InStoreAvailabilityResource1" OnClientClick="objProduct.OpenStoreAvailability();"
                                                        OnClick="lnkInStoreAvailability_Click" CssClass="Button" EnableViewState="false"></asp:LinkButton></span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div id="inStoreAvailabilityListId" class="ListInStoreAvailability" runat="server" style="display: none;" title="Store Availability">
                                    <asp:UpdatePanel ID="upnlStoreInv" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <p class="AvailabilityTitle">
                                                <asp:Literal ID="ltrStoreMessage" runat="server" EnableViewState="false" meta:resourceKey="ltrStoreMessage"></asp:Literal>
                                            </p>
                                            <asp:Repeater ID="rptInStoreAvailability" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <ul class="ProductDataTable">
                                                        <li class="ALeft">
                                                            <ul>
                                                                <li>
                                                                    <asp:Literal ID="ltrStoreHeader" runat="server" EnableViewState="false" meta:resourceKey="ltrStoreHeaderResource1"></asp:Literal></li>
                                                                <li>
                                                                    <asp:Literal ID="ltrStoreContact" runat="server" EnableViewState="false" meta:resourceKey="ltrStoreContactResource1"></asp:Literal></li>
                                                                <li>
                                                                    <asp:Literal ID="ltrStoreAvailable" runat="server" EnableViewState="false" meta:resourceKey="ltrStoreAvailableResource1"></asp:Literal></li>
                                                            </ul>
                                                        </li>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li class="ALeft">
                                                        <ul class="<%# SetSelStoreHighlightedClass(DataBinder.Eval(Container.DataItem, "StoreID"))%>">
                                                            <li>
                                                                <asp:HiddenField ID="hdnStoreID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "StoreID")%>' />
                                                                <asp:Label ID="lblStoreName" EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name")%>'></asp:Label>
                                                            </li>
                                                            <li>
                                                                <%--  <span id="spanStorePhoneLink">--%>
                                                                <a href="tel:+<%# DataBinder.Eval(Container.DataItem, "Phone")%>">
                                                                    <asp:Label ID="lblStorePhone" EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Phone")%>'
                                                                        ClientIDMode="Static"></asp:Label></a>
                                                                <%--</span>
                                                                <span id="spanStorePhone">
                                                                    <%# DataBinder.Eval(Container.DataItem, "Phone")%>
                                                                </span>--%>
                                                                
                                                            </li>
                                                            <li>
                                                                <asp:Label ID="lblAvailability" EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Availability")%>'></asp:Label>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <div class="Error">
                                                <asp:Literal ID="lblStoreAvailErrorMsg" runat="server" meta:resourceKey="lblStoreAvailErrorMsgResource1"></asp:Literal>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>
                            </div>
                            <asp:Panel ID="pnlGroupProductAttribute" runat="server" Visible="false">
                                <div class="ProductAttributes GroupProductAttributes">
                                    <asp:UpdatePanel ID="upSkuImages" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <Zeon:ZProductSkuImages ID="uxProductSkuImages" runat="server" Visible="false" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <ZNode:GroupProductAttributes ID="uxGroupProductAttribute" runat="server" />
                                </div>
                            </asp:Panel>
                            <uc1:CustomMessage ID="uxProductDetailSecurityInfo" runat="server" MessageKey="ProductDetailSecurityInfo" />
                            <div id="BundleProduct">
                                <ZNode:BundleProduct ID="uxBundleProduct" runat="server" />
                            </div>
                            <div class="Socail-Icon">
                                <ZNode:ExternalLinks ID="ExternalLinks1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="Tabs">
                        <ZNode:ProductTabs ID="uxProductTabs" runat="server" />
                    </div>
                </div>
                <div id="RelatedTabs">
                    <div class="CrossSell">
                        <ZNode:ProductRelated ID="uxCrossSells" ProductCrossSellType="CrossSells" runat="server" />
                    </div>
                    <div class="RelatedProd">
                        <ZNode:ProductRelated ID="uxRelatedProducts" ProductCrossSellType="RelatedProducts" runat="server" />
                    </div>
                </div>
                <div class="RecentlyViewedProducts">
                    <div>
                        <ZNode:RecentlyViewedProducts ID="uxRecentlyViewedProducts" runat="server" Title="Recently Viewed Items"
                            ShowName="true" ShowReviews="false" ShowImage="true" ShowDescription="false"
                            ShowPrice="true" ShowAddToCart="false" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<input type="hidden" value="0" id="hdnAddtocartCount" name="hdnAddtoCartCount" />--%>
</div>
<div id="divEnlargedImage" runat="server" class="DilogModal" style="display: none;">
</div>
<%-- Zeon Custom HTML for Update progress bar --%>
<asp:UpdateProgress ID="updateProgressBar" DisplayAfter="0" runat="server"
    DynamicLayout="True">
    <ProgressTemplate>
        <div style="position: relative; top: 30%; text-align: center;" class="updateProgress">
            <img alt="loading" id="imgAddressloader" src="Themes/Default/Images/loading.gif" />&nbsp; Please wait...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<ajaxToolKit:ModalPopupExtender ID="mpeprogressbar" runat="server" TargetControlID="updateProgressBar"
    BackgroundCssClass="modalBackground" PopupControlID="updateProgressBar">
</ajaxToolKit:ModalPopupExtender>
<script>
    //Script for Modal progress bar
    var ModalProgressAddress = '<%= mpeprogressbar.ClientID %>';
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);
    function beginReq(sender, args) {
        // shows the Popup 
        $find(ModalProgressAddress).show();
    }
    function endReq(sender, args) {
        //  hide Popup 
        $find(ModalProgressAddress).hide();
    }

</script>
<script type="text/javascript" language="javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function EndRequestHandler(sender, args) {
        if (args.get_error() != undefined) {
            args.set_errorHandled(true);
        }
    }

    //function CheckAddToCartEvent()
    //{
    //    debugger;
    //    var count = $j('#hdnAddtocartCount').val();
    //    if (count == 0) {
    //        $j('#hdnAddtocartCount').val('1');
    //    }
    //    else {
    //       WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions('ctl00_ctl00_MainContent_uxProduct_uxAddToCart', false, true))
    //    }
    //}
    ///Nivi Code Start :: 31/08/2018
    //Stable Page After Reloading Page in Window
    var xPos, yPos, needScroll;
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_beginRequest(BeginRequestHandler);
    prm.add_pageLoaded(EndRequestHandler)

    function BeginRequestHandler(sender, args) {
        xPos = 0;
        yPos = window.pageYOffset || document.documentElement.scrollTop;
    }

    function EndRequestHandler(sender, args) {
        //  if (needScroll) {  
        window.setTimeout("window.scrollTo(" + xPos + "," + yPos + ")", 0);
        //  }  
    }

    ///Nivi Code End ::
</script>

<%-- Zeon Custom HTML for Update progress bar --%>

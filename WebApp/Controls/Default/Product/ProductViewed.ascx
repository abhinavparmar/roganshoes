<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Product_ProductViewed" CodeBehind="ProductViewed.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>

<asp:Panel ID="pnlRecentlyViewedProducts" runat="server" Visible="false">
    <div class="CustomTitle">
        <ZNode:CustomMessage ID="CustomMessage1" MessageKey="RecentlyViewedTitle" runat="server" EnableViewState="false"></ZNode:CustomMessage>
    </div>
    <div id="relRecentlyViewedProduct" runat="server" class="RecentlyViewedProduct" enableviewstate="false">
        <ul id="ProductViewed" class="FlexiSliderImageShow" runat="server" enableviewstate="false">
            <asp:Repeater ID="RecentlyViewedItems" runat="server" EnableViewState="false">
                <ItemTemplate>
                    <li>
                        <div class="item">
                            <div class="RecentlyViewedItem">
                                <div class="ItemBorder "></div>
                                <div class="ItemBorder"></div>
                                <div class="SpecialItem">
                                    <div class="Image">
                                        <div class="ItemType">
                                            <span class="rs-new" id="divRsNew" runat="server" visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>'><span class="rs-new-text">New</span></span>
                                            <%--<asp:Image ID="NewItemImage" EnableViewState="false" runat="server" ImageUrl='<%#ResolveUrl(ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png")) %>'
                                                meta:resourcekey="NewItemImageResource1" Visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>' ClientIDMode="Static" AlternateText="New" />--%>
                                            &nbsp;<asp:Image EnableViewState="false" ID="Image1" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                                meta:resourcekey="FeaturedItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' ClientIDMode="Static" AlternateText="Featured" ToolTip="Featured" />
                                        </div>
                                        <a id="A1RecentlyViewed" href='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>' runat="server" title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>'>
                                            <img alt='<%# "Recently Viewed "+ DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' src='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "SmallImageFilePath").ToString())%>' runat="server" visible='<%# ShowImage %>' title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' />
                                        </a>
                                    </div>
                                    <div class="StarRating">
                                        <ZNode:ProductAverageRating ID="uxProductAverageRating" ProductName='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' TotalReviews='<%# DataBinder.Eval(Container.DataItem, "TotalReviews") %>'
                                            ViewProductLink='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink") %>'
                                            ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ReviewRating") %>' runat="server" EnableViewState="false" />
                                    </div>
                                    <div class="DetailLink">
                                        <asp:HyperLink ID="hlName" runat="server" CssClass='DetailLink' Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' EnableViewState="false"
                                            NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString() %>' meta:resourcekey="hlNameResource1" ToolTip='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>'></asp:HyperLink>
                                        <span style="width: 1px;"></span>
                                        <span class="Price">
                                            <asp:Label ID="Price" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>' Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && ProductProfile.ShowPrice && ShowPrice %>'
                                                EnableViewState="false"></asp:Label>
                                            <asp:Image EnableViewState="false" ID="FeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                                Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' AlternateText="Featured Item" ToolTip="Featured Item" /></span>
                                        </span>
                                        <div class="CallForPrice">
                                            <asp:Label ID="uxRevCallForPricing" EnableViewState="false" runat="server" CssClass="Price"
                                                Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>' ></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</asp:Panel>

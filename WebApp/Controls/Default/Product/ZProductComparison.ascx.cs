﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using Zeon.Libraries.Elmah;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp.Controls.Default.Product
{
    public partial class ZProductComparison : System.Web.UI.UserControl
    {
        #region Private Members

        private ZNodeProfile productListProfile = new ZNodeProfile();
        private string productIDs = string.Empty;
        private ZNodeProductList productList = null;
        private string upcSpecificationText = System.Configuration.ConfigurationManager.AppSettings["UpcSpecificationText"].ToString();
        private string alternatePartNumberSpecificationText = System.Configuration.ConfigurationManager.AppSettings["AlternatePartNumberSpecificationText"].ToString();
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the product list profile
        /// </summary>
        public ZNodeProfile ProductListProfile
        {
            get { return this.productListProfile; }
            set { this.productListProfile = value; }
        }

        /// <summary>
        /// String Product IDS
        /// </summary>
        public string ProductIDs
        {
            get { return this.productIDs; }
            set { this.productIDs = value; }
        }

        /// <summary>
        /// Sets the Product List
        /// </summary>
        public ZNodeProductList ProductList
        {
            get { return this.productList; }
            set { this.productList = value; }
        }

        #endregion

        #region Page Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ltrerroMsg.Visible = false;
                iconDiv.Visible = false;
                if (NoProductsForComparison())
                {
                    // Response.Redirect("default.aspx");
                    ltrNoProductErrorMsg.Visible = true;
                    //spnNoProductErrorMsg.Visible = true;
                }
                else
                {
                    BindComparisonData();
                }
            }
            //PRFT Custom Code : Start
            this.Page.Title = "Product Comparison | Rogan’s Shoes";
            this.Page.MetaDescription = "Compare products online | Rogan’s Shoes";
            //PRFT Custom Code : End
            //successMsg.Visible = false;
            //ltrerroMsg.Visible = false;
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Represents the CheckFor Call for pricing message
        /// </summary>
        /// <param name="fieldValue">passed the field value </param>
        /// <returns>Returns the Call For pricing message </returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());
            string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);

            if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
            {
                message = callMessage.ToString();
            }

            if (Status)
            {
                return message;
            }
            else if (!this.productListProfile.ShowPrice)
            {
                return message;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Binds the product data for comparison
        /// </summary>
        private void BindComparisonData()
        {
            if (string.IsNullOrEmpty(this.productIDs))
            {
                List<CompareProducts> comparableProducts = Session["CompareProductIDs"] as List<CompareProducts>;
                if (comparableProducts.Count > 0 && comparableProducts != null)
                {
                    foreach (CompareProducts prods in comparableProducts)
                    {
                        this.productIDs = this.productIDs + Convert.ToString(prods.ProductID) + ",";
                    }
                    if (this.productIDs.LastIndexOf(',') > 0)
                    {
                        this.productIDs = this.productIDs.Substring(0, this.productIDs.Length - 1);
                    }
                }
            }

            if (ProductList == null)
            {
                ProductList = ZNodeProductList.GetCompareProducts(this.productIDs, upcSpecificationText, alternatePartNumberSpecificationText);
            }
            rptCompareProducts.DataSource = ProductList.ZNodeProductCollection;
            rptCompareProducts.DataBind();

            #region Old COde

            //DataSet dsProductComparision = new DataSet();
            //if (dsProductComparision != null && dsProductComparision.Tables != null && dsProductComparision.Tables[0].Rows.Count > 0)
            //{
            //    sb.Append("<ul class='ProductComparison'>");
            //    int counter = 0;
            //    foreach (DataRow dr in dsProductComparision.Tables[0].Rows)
            //    {
            //        string hdnProductID = "hdnProdID_" + Convert.ToString(counter);
            //        string imageID = "imgProduct_" + Convert.ToString(counter);
            //        string imageSrc = ZNodeConfigManager.EnvironmentConfig.SmallImagePath.Replace("~", string.Empty) + dr["ImagePath"].ToString();

            //        sb.Append("<li>");

            //        sb.Append("<div class='RemoveProduct'>");
            //        sb.Append("");
            //        sb.Append("</div>");

            //        sb.Append("<input type='hidden' id='" + hdnProductID + "' runat='server' value='" + dr["ProductID"].ToString() + "' />");

            //        sb.Append("<div class='ProductImage'>");
            //        sb.Append("<img id='" + imageID + "' border='0' src='" + imageSrc + "' alt='" + dr["ProductNum"].ToString() + "' runat='server'>");
            //        sb.Append("</div>");

            //        sb.Append("<div class='ProductName'>");
            //        sb.Append("<a href='" + GetNavigationURL(dr) + "'> " + dr["ProductName"].ToString() + "</a>");
            //        sb.Append("</div>");

            //        sb.Append("<div class='ProductDescription'>");
            //        sb.Append(dr["ShortDescription"] == null ? string.Empty : dr["ShortDescription"].ToString());
            //        sb.Append("</div>");

            //        sb.Append("<div class='SKU'>");
            //        sb.Append(dr["ProductNum"] == null ? string.Empty : dr["ProductNum"].ToString());
            //        sb.Append("</div>");

            //        sb.Append("<div class='Availability'>");
            //        sb.Append(dr["InStock"] == null ? string.Empty : dr["InStock"].ToString());
            //        sb.Append("</div>");

            //        sb.Append("<div class='Price'>");
            //        sb.Append(dr["Price"] == null ? string.Empty : dr["Price"].ToString());
            //        sb.Append("</div>");

            //        sb.Append("<div class='Specification'>");
            //        sb.Append(dr["AttributeName"].ToString());
            //        sb.Append("</div>");

            //        sb.Append("<div class='Navigation'>");
            //        sb.Append("<a href='" + GetNavigationURL(dr) + "'>View Details</a>");
            //        sb.Append("</div>");

            //        sb.Append("</li>");
            //        counter++;
            //    }

            //    sb.Append("</ul>");

            //    ltrProductComparison.Text = sb.ToString();
            //}
            #endregion
        }

        /// <summary>
        /// Check for the products.
        /// </summary>
        /// <returns>Returns true if there is not items in session</returns>
        private bool NoProductsForComparison()
        {
            bool noProductForComparison = false;
            if (Session["CompareProductIDs"] == null)
            {
                noProductForComparison = true;
            }
            else
            {
                List<CompareProducts> compProducts = Session["CompareProductIDs"] as List<CompareProducts>;

                if (compProducts == null && compProducts.Count < 1)
                {
                    noProductForComparison = true;
                }
                else if (compProducts != null && compProducts.Count < 2)
                {
                    HideProductInList();
                    noProductForComparison = false;
                }
                else if (compProducts != null && compProducts.Count >= 2)
                {
                    CompareScroll.Visible = true;
                    iconDiv.Visible = true;
                    noProductForComparison = false;
                }
            }
            return noProductForComparison;
        }

        /// <summary>
        ///Hide Single Product
        /// </summary>
        public void HideProductInList()
        {
            //CompareScroll.Visible = false;
            //iconDiv.Visible = false;
            ShowMininumItemsCompareError();
        }

        /// <summary>
        /// Show Error of Minimum Comapre Items
        /// </summary>
        private void ShowMininumItemsCompareError()
        {
            ltrerroMsg.Visible = true;
            //errorMsg.Visible = true;
            iconDiv.Visible = false;
            ltrerroMsg.Text = GetLocalResourceObject("ShowMinItemsCompareError").ToString();
        }

        /// <summary>
        /// Remove session and redirect to Home Page
        /// </summary>
        private void RemoveSessionAndPostBackToHomePage()
        {
            ltrNoProductErrorMsg.Visible = false;
            Session["CompareProductIDs"] = null;
            Response.Redirect("default.aspx");
        }

        /// <summary>
        /// Generate the HTML of Data from Product Collection
        /// </summary>
        /// <param name="path">string Domain Path</param>
        /// <returns>HTML format of Product Data</returns>
        private string GenerateHTMLFromData(string path)
        {
            StringBuilder sb = new StringBuilder();

            if (string.IsNullOrEmpty(this.productIDs))
            {
                List<CompareProducts> comparableProducts = Session["CompareProductIDs"] as List<CompareProducts>;
                if (comparableProducts != null && comparableProducts.Count > 0)
                {
                    foreach (CompareProducts prods in comparableProducts)
                    {
                        this.productIDs = this.productIDs + Convert.ToString(prods.ProductID) + ",";
                    }
                    if (this.productIDs.LastIndexOf(',') > 0)
                    {
                        this.productIDs = this.productIDs.Substring(0, this.productIDs.Length - 1);
                    }
                }
                else
                {
                    return string.Empty;
                }
            }

            if (ProductList == null)
            {
                ProductList = ZNodeProductList.GetCompareProducts(this.productIDs, upcSpecificationText, alternatePartNumberSpecificationText);
            }

            sb.Append("<table style='width:100%;' class='CompareProductEmail'>");
            sb.Append("<tr>");

            for (int iLen = 0; iLen < ProductList.ZNodeProductCollection.Count; iLen++)
            {
                // ZNodeProduct prod = ZNodeProduct.Create(ProductList.ZNodeProductCollection[iLen].ProductID);Comment Extra Call

                sb.Append("<td style='width:25%;'>");
                sb.Append("<table class='CompareEmailItem'>");

                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append("<img src='" + path + ResolveUrl(ProductList.ZNodeProductCollection[iLen].SmallImageFilePath) + "' alt='" + ProductList.ZNodeProductCollection[iLen].Name + "' title='" + ProductList.ZNodeProductCollection[iLen].Name + "' />");
                sb.Append("</td>");
                sb.Append("</tr>");

                if (!ZCommonHelper.IsCustomMulitipleAttributePortal() && ProductList.ZNodeProductCollection[iLen].NewProductInd)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>");
                    sb.Append("<img src='" + path + ResolveUrl(ZNodeCatalogManager.GetImagePathByLocale("new.png")) + "' alt='" + ProductList.ZNodeProductCollection[iLen].Name + "' title='" + ProductList.ZNodeProductCollection[iLen].Name + "' />");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }

                if (ProductList.ZNodeProductCollection[iLen].FeaturedInd)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>");
                    sb.Append("<img src='" + path + ResolveUrl(ZNodeCatalogManager.GetImagePathByLocale("sale.png")) + "' alt='" + ProductList.ZNodeProductCollection[iLen].Name + "' title='" + ProductList.ZNodeProductCollection[iLen].Name + "' />");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }

                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append("<a href='" + path + Response.ApplyAppPathModifier(ProductList.ZNodeProductCollection[iLen].ViewProductLink) + "' >" + ProductList.ZNodeProductCollection[iLen].Name + "</a>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append("<div> <span>Item:&nbsp;</span><span>" + "#" + ProductList.ZNodeProductCollection[iLen].ProductNum + "<span></div>");
                sb.Append("</td>");
                sb.Append("</tr>");

                if (!string.IsNullOrWhiteSpace(ProductList.ZNodeProductCollection[iLen].Custom3) && !ProductList.ZNodeProductCollection[iLen].Custom1.Equals("0"))
                {
                    sb.Append("<tr>");
                    sb.Append("<td>");
                    sb.Append("<div class='BrandName'> <span>Brand:&nbsp;</span><span>" + ProductList.ZNodeProductCollection[iLen].Custom3 + "<span></div>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }

                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append("<div><span>" + ProductList.ZNodeProductCollection[iLen].ShortDescription + "<span></div>");
                sb.Append("</td>");
                sb.Append("</tr>");

                if (!string.IsNullOrWhiteSpace(ProductList.ZNodeProductCollection[iLen].Custom1) && !ProductList.ZNodeProductCollection[iLen].Custom1.Equals("0"))
                {
                    sb.Append("<tr>");
                    sb.Append("<td>");
                    sb.Append("<div><span>" + ProductList.ZNodeProductCollection[iLen].Custom1 + "<span></div>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }

                if (!ProductList.ZNodeProductCollection[iLen].CallForPricing && ProductListProfile.ShowPrice)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>");
                    sb.Append("<div><span>" + ProductList.ZNodeProductCollection[iLen].FormattedPrice + "<span></div>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }

                sb.Append("<tr>");
                sb.Append("<td class='ProductComparisonMail'>");
                sb.Append("<div><span>" + ProductList.ZNodeProductCollection[iLen].Custom2 + "<span></div>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append("<div><span>" + this.CheckForCallForPricing(ProductList.ZNodeProductCollection[iLen].CallForPricing, ProductList.ZNodeProductCollection[iLen].CallMessage) + "<span></div>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append("<div><span>" + GetTierPriceCollectionTable(ProductList.ZNodeProductCollection[iLen]) + "<span></div>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append("<a class='Navigation' href='" + path + Response.ApplyAppPathModifier(ProductList.ZNodeProductCollection[iLen].ViewProductLink) + "' >View Details</a>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("</table>");
                sb.Append("</td>");

                if (!iLen.Equals(0) && (iLen % 3).Equals(0))
                {
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                }
            }

            sb.Append("</tr>");
            sb.Append("</table>");

            return sb.ToString();
        }

        /// <summary>
        /// Generate Email Template
        /// </summary>
        /// <returns></returns>
        private string GenerateTemplate()
        {
            string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
            string htmlTemplatePath = string.Empty;
            string emailBody = GenerateHTMLFromData(domainPath);
            if (!string.IsNullOrEmpty(emailBody))
            {
                string CurrentCulture = ZNodeCatalogManager.CultureInfo;
                string defaultHtmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZProductComparisonTemplate.htm");

                if (CurrentCulture != string.Empty)
                {
                    htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZProductComparisonTemplate_" + CurrentCulture + ".htm");
                }
                else
                {
                    htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ZProductComparisonTemplate.htm");
                }

                FileInfo fileinfo = new FileInfo(htmlTemplatePath);

                if (!fileinfo.Exists)
                {
                    htmlTemplatePath = defaultHtmlTemplatePath;
                }

                //string emailBody = GenerateHTMLFromData(domainPath);

                StreamReader streamReader = new StreamReader(htmlTemplatePath);
                string messageText = streamReader.ReadToEnd();

                string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
                int portalId = ZNodeConfigManager.SiteConfig.PortalID;

                ZEmailHelper zEmailHelper = new ZEmailHelper();
                ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
                string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

                string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
                string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

                //To add header to the template
                Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
                messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

                //To add footer to the template
                Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
                messageText = rxFooterContent.Replace(messageText, messageTextFooter);

                Regex rxFromEmail = new Regex("#FromFriend#", RegexOptions.IgnoreCase);
                messageText = rxFromEmail.Replace(messageText, hdnEmailTo.Value.Trim());

                Regex rxBody = new Regex("#ComparisonDetails#", RegexOptions.IgnoreCase);
                messageText = rxBody.Replace(messageText, emailBody);

                return messageText;
            }

            return string.Empty;
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var zeonCompareProduct = new ProductComaparsion({");
            script.Append("\"Controls\":{");
            script.AppendFormat("\"{0}\":\"{1}\"", lblErrors.ID, lblErrors.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", txtEmailFrom.ID, txtEmailFrom.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", txtEmailTo.ID, txtEmailTo.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", lblMessages.ID, lblMessages.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", btnEmailProducts.ID, btnEmailProducts.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", hdnEmailFrom.ID, hdnEmailFrom.ClientID);
            script.AppendFormat(",\"{0}\":\"{1}\"", hdnEmailTo.ID, hdnEmailTo.ClientID);
            script.Append("},\"Messages\":{");
            script.AppendFormat("\"EmailValidationErrorMessage\":\"{0}\"", GetLocalResourceObject("EmailValidationErrorMessage").ToString());
            script.AppendFormat(",\"RequiredFieldsErrorMessage\":\"{0}\"", GetLocalResourceObject("RequiredFieldsErrorMessage").ToString());
            script.Append("}");
            script.Append("});zeonCompareProduct.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "ProductComaparsion", script.ToString(), true);
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Get the image url
        /// </summary>
        /// <param name="imageName"></param>
        /// <returns></returns>
        protected string GetImageURL(object imageName)
        {
            string imageUrl = string.Empty;

            if (imageName != null)
            {
                ZNodeImage znodeImage = new ZNodeImage();
                imageUrl = znodeImage.GetImageHttpPathSmall(Convert.ToString(imageName));
                // imageUrl = ZNodeConfigManager.EnvironmentConfig.SmallImagePath.Replace("~", string.Empty) + Convert.ToString(imageName);
            }

            return imageUrl;
        }

        /// <summary>
        /// Get the Product url
        /// </summary>
        /// <param name="drr"></param>
        /// <returns></returns>
        protected string GetProductNavigationURL(object seoUrl, object productID)
        {
            string navigateUrl = string.Empty;

            if (seoUrl == null)
            {
                navigateUrl = "product.aspx?zpid=" + Convert.ToString(productID);
            }
            else
            {
                navigateUrl = Convert.ToString(seoUrl);
            }

            return navigateUrl.ToLower();
        }

        #endregion

        #region General Events

        /// <summary>
        /// Remove all items from compare products
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtnRemoveAll_Click(object sender, EventArgs e)
        {
            RemoveSessionAndPostBackToHomePage();
        }

        /// <summary>
        /// Command Event of Repeater
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void rptCompareProducts_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("RemoveProduct"))
            {
                List<CompareProducts> comparableProducts = Session["CompareProductIDs"] as List<CompareProducts>;
                foreach (CompareProducts comp in comparableProducts)
                {
                    if (comp.ProductID.Equals(Convert.ToInt32(e.CommandArgument)))
                    {
                        comparableProducts.Remove(comp);
                        if (comparableProducts.Count < 2)
                        {
                            HideProductInList();
                        }
                        break;
                    }
                }

                if (comparableProducts.Count.Equals(0))
                {
                    RemoveSessionAndPostBackToHomePage();
                }

                Session["CompareProductIDs"] = comparableProducts;
                BindComparisonData();
            }
        }

        /// <summary>
        /// Send Email Functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEmailProducts_Click(object sender, EventArgs e)
        {
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string emailBody = GenerateTemplate();
            string emailSubject = zeonMessageConfig.GetMessageKey("CompareEmailSubject");
            try
            {
                if (!string.IsNullOrEmpty(emailBody))
                {
                    ZNodeEmail.SendEmail(hdnEmailTo.Value, hdnEmailFrom.Value, string.Empty, emailSubject, emailBody, true);
                    lblMessages.Text = Resources.CommonCaption.CompareEmailSent;
                }
                else
                {
                    ltrNoProductErrorMsg.Visible = true;
                    ltrNoProductErrorMsg.Text = this.GetLocalResourceObject("CompareEmailSentError").ToString();
                    iconDiv.Visible = false;
                    BindComparisonData();
                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex);
                ltrNoProductErrorMsg.Visible = true;
                ltrNoProductErrorMsg.Text = this.GetLocalResourceObject("CompareEmailSentError").ToString();
            }
        }

        /// <summary>
        /// Get tier Price table for product
        /// </summary>
        /// <param name="product">ZNodeProduct</param>
        /// <returns>string</returns>
        protected string GetTierPriceCollectionTable(ZNodeProductBase product)
        {
            bool isProductHasCustomPromotion = false;
            int tierIndex = 0;
            StringBuilder htmlBuilder = new StringBuilder();
            StringBuilder priceRow = new StringBuilder();
            StringBuilder qtyRow = new StringBuilder();
            StringBuilder rowBuilder = new StringBuilder();
            if (product != null && product.ZNodeTieredPriceCollection != null && product.ZNodeTieredPriceCollection.Count > 0)
            {
                decimal price = 0;

                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductTierEntity productTier in product.ZNodeTieredPriceCollection)
                {
                    //Create Qty Row
                    qtyRow.Clear();
                    priceRow.Clear();
                    rowBuilder.Append("<tr>");
                    qtyRow.Append("<td>");
                    if (tierIndex == 0)
                    {
                        qtyRow.Append(productTier.TierStart + " - " + productTier.TierEnd);
                    }
                    else
                    {
                        qtyRow.Append((Convert.ToInt32(productTier.TierStart.ToString())) + " - " + productTier.TierEnd.ToString());
                    }
                    qtyRow.Append("</td>");

                    //Create Price Row
                    string priceText = string.Empty;
                    if (tierIndex == 0 || isProductHasCustomPromotion)
                    {
                        ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption pricing = new ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption();
                        price = pricing.PromotionalPrice(product, productTier.Price);
                        isProductHasCustomPromotion = product.IsCustomPromotionApplied;
                    }
                    string htmText = product.IsCustomPromotionApplied ? "<td><span class='RegularPrice'>" : "<td>";
                    priceRow.Append(htmText);
                    if (!product.IsCustomPromotionApplied)
                    {
                        if (price == productTier.Price || price == 0)
                        {
                            priceText = string.Format(this.GetLocalResourceObject("TierRegulerPriceFormat").ToString(), productTier.Price.ToString("C"));
                        }
                        else
                        {
                            priceText = string.Format(this.GetLocalResourceObject("TierRegulerPriceFormat").ToString(), productTier.Price.ToString("C"));
                        }
                    }
                    else
                    {
                        priceText = productTier.Price.ToString("C") + string.Format(this.GetLocalResourceObject("TierSalesPriceFormat").ToString(), price.ToString("C")); ;
                    }
                    priceRow.Append(priceText);
                    priceRow.Append("</td>");
                    rowBuilder.Append(qtyRow.ToString());
                    rowBuilder.Append(priceRow.ToString());
                    rowBuilder.Append("</tr>");
                    tierIndex++;

                }
                htmlBuilder.Append(string.Format(this.GetLocalResourceObject("TierPriceTableStructureFormat").ToString(), rowBuilder.ToString()));
                //Create Proper HTML and Assign it to below variable.

            }
            return htmlBuilder.ToString();
        }

        #endregion

    }

}
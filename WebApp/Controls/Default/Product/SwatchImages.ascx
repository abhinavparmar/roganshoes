<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Product_SwatchImages" CodeBehind="SwatchImages.ascx.cs" %>

<div class="ProductViews">
    <asp:DataList ID="DataListAlternateImages" runat="server" RepeatDirection="horizontal" OnItemDataBound="DataListAlternateImages_ItemDataBound" ClientIDMode="Static">
        <ItemTemplate>
            <div class="Swatches">
                <img id="imgSwatch" runat="server" alt='<%# GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag"),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                    class="SwatchImage" src='<%#ResolveUrl(GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString(), false)) %>'
                    title='<%# GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag"),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                    visible='<%#  ShowImage(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage")) %>' ClientIDMode="Static" />
            </div>
        </ItemTemplate>
        <ItemStyle CssClass="ItemStyle" />
    </asp:DataList>

    <div class="Swatches" id="divSwatchImages" runat="server" enableviewstate="false" ClientIDMode="Static">
        <asp:DataList ID="DataListSwatches" runat="server" RepeatDirection="horizontal" OnItemDataBound="DataListSwatches_ItemDataBound" ClientIDMode="Static">
            <ItemTemplate>
                <img id="imgSwatch" runat="server" alt='<%# GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag").ToString(),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                    class="SwatchImage" ClientIDMode="Static"
                    src='<%# ResolveUrl(GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString(), true)) %>'
                    title='<%# GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag").ToString(),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                    visible='<%#  ShowImage(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage")) %>' />
            </ItemTemplate>
            <ItemStyle CssClass="ItemStyle" />
        </asp:DataList>
    </div>

    <div class="Swatches" style="cursor: pointer; padding-left: 5px!important;" id="divSKUImages" runat="server" enableviewstate="false" ClientIDMode="Static">
        <asp:DataList ID="DataListSKU" runat="server" RepeatDirection="horizontal" OnItemDataBound="DataListSKU_ItemDataBound" RepeatColumns="4" ClientIDMode="Static">
            <ItemTemplate>
                <%--<a href='<%# ResolveUrl(GetViewLinkURL()) %>'>--%>
                <img id="imgSwatch" runat="server" alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                    class="SwatchImage" ClientIDMode="Static"
                    src='<%# ResolveUrl(GetImagePath(DataBinder.Eval(Container.DataItem, "SKUPicturePath").ToString(), false)) %>'
                    title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                    visible='<%#  ShowImage(DataBinder.Eval(Container.DataItem, "ActiveInd")) %>' />
                <%-- </a>--%> 
            </ItemTemplate>
            <ItemStyle CssClass="ItemStyle" />
        </asp:DataList>
    </div>

</div>

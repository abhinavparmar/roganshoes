﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Product_ExternalLinks" CodeBehind="ExternalLinks.ascx.cs" %> 
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>
<div id="DetailPageLink">
    <div id="InternalLinks">
        
        <div id="SocialLinks">
            <!-- uses the AddThis.com plugin -->
            <!-- AddThis Button BEGIN -->
            <script type="text/javascript">
                var addthis_config = {
                    ui_language: "<%= this.AddThisLanguageTwoLetterCode %>"
                }
            </script>
            <div class="ShareIt">
                <asp:Localize ID="Localize5" runat="server" EnableViewState="false" meta:resourceKey="txtShareit"></asp:Localize>
            </div>
            <%--<span class="addthis_toolbox addthis_default_style addthis_32x32_style">
                <a class="addthis_button_facebook" title="Facebook"></a>
                <a class="addthis_button_twitter" title="Twitter"></a>
                <a target="_blank" rel="nofollow" href="https://www.youtube.com/profile?user=RogansShoes" title="YouTube" class="YouTube">&nbsp;</a>
                <a target="_blank" rel="nofollow" href="http://blog.rogansshoes.com/" title="RoganBlog" class="RoganBlog">&nbsp;</a>
                <a class="addthis_button_google_plusone_share" title="Google+"></a>
                <a class="addthis_button_pinterest_share right" title="Pinterest"></a>
                <%--   <a class="addthis_button_google_plusone_share" title="Google+"></a>
            <a class="addthis_button_compact"></a>
            <a class="addthis_counter addthis_bubble_style"></a>--%>
            <%--</span>--%>
            <Znode:CustomMessage ID="ucExternalLinks" runat="server" EnableViewState="False" MessageKey="ProductExternalLinks" />
            <%--<script type="text/javascript" src="<%= this.AddThisScriptURL %>"></script>--%>
            <!-- AddThis Button END -->
        </div>
     
    </div>
    <div class="Block-Section">
            <div class="Send-Mail">
                <asp:HyperLink ID="EmailFriendLink" runat="server" meta:resourcekey="EmailFriendLinkResource" ClientIDMode="Static" EnableViewState="false"></asp:HyperLink>
                <asp:ImageButton ID="EmailImage" EnableViewState="false" runat="server" CssClass="FriendImage" OnClick="EmailImage_Click"
                    meta:resourcekey="EmailImageResource1" ClientIDMode="Static" ToolTip="Email Friend" AlternateText="EmailFriend"></asp:ImageButton>
            </div>

            <div class="Print">
                <a id="A1PrintImage" onclick="window.print();return false;" style="cursor: pointer" meta:resourcekey="lblPrint" runat="server" clientidmode="Static">
                    <asp:Localize ID="Localize2" runat="server" EnableViewState="false"></asp:Localize>
                    <asp:ImageButton ID="PrintImage" EnableViewState="false" runat="server" CssClass="PrintImage"
                        meta:resourcekey="PrintImageResource1" ClientIDMode="Static" ToolTip="Print" AlternateText="Print"></asp:ImageButton></a>
            </div>
        </div>
    <div class="block-area">       
        <div class="WishListLink">
            <asp:LinkButton OnClick="WishListLink_Click" ID="lbAddToWishList" runat="server" EnableViewState="false" meta:resourcekey="LinkButtonResource1" ClientIDMode="Static"></asp:LinkButton>
            <asp:ImageButton ID="WishListImage" runat="server" EnableViewState="false"
                CssClass="GiftImage" OnClick="WishListImage_Click"
                meta:resourcekey="WishListImageResource1" ClientIDMode="Static" ToolTip="WishList" AlternateText="WishList"></asp:ImageButton>
        </div>       
    </div>
</div>

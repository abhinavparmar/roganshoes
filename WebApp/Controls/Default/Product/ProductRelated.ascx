<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Product_ProductRelated" CodeBehind="ProductRelated.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>


<asp:Panel ID="pnlRelated" runat="server" Visible="true" EnableViewState="false">
    <%--<h5><uc1:CustomMessage ID ="RelatedProductTitle" MessageKey="ProductRelatedProductsTitle" runat="server"></uc1:CustomMessage></h5>--%>
    <div>
        <div id="CrossSell_Products">
            <div class="CustomTitle">
                <span class="Head">
                    <asp:Label ID="lblQuickProductTitle" EnableViewState="false" runat="server"></asp:Label>
                </span>
            </div>
            <div id="relProdProduct" runat="server" enableviewstate="false">
                <ul id="ProductRelatedProd" class="FlexiSliderImageShow" runat="server" enableviewstate="false">
                    <asp:Repeater ID="DataListRelated" runat="server" EnableViewState="false">
                        <ItemTemplate>
                            <li>
                                <div class="item">
                                    <div class="ItemBorder"></div>
                                    <div class="SpecialItem">
                                        <div class="Image">
                                            <div class="ItemType">
                                                <span class="rs-new" id="spnRsNew" runat="server" visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>'><span class="rs-new-text">New</span></span>
                                                <!--<asp:Image ID="NewItemImage" EnableViewState="false" runat="server" ImageUrl='<%#ResolveUrl(ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.png")) %>'
                                                    meta:resourcekey="NewItemImageResource1" Visible='<%#DataBinder.Eval(Container.DataItem, "NewProductInd")%>' ClientIDMode="Static" AlternateText="New" alt="New" />-->
                                                &nbsp;<asp:Image EnableViewState="false" ID="FeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.png") %>'
                                                    meta:resourcekey="FeaturedItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' ClientIDMode="Static" AlternateText="Featured" ToolTip="Featured" />
                                            </div>
                                            <a id="ProductLink" href='<%# GetViewProductPageUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' runat="server" title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>'>
                                                <img alt='<%# "You May Also Like " + DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' src="images/grey1px.gif" class="lazy"
                                                    data-original='<%#ResolveUrl(GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()))%>'
                                                     title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' />
                                            </a>
                                        </div>
                                        <div class="StarRating">
                                            <ZNode:ProductAverageRating ID="uxProductAverageRating" ProductName='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' TotalReviews='<%# DataBinder.Eval(Container.DataItem, "TotalReviews") %>'
                                                ViewProductLink='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink") %>' 
                                                ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ReviewRating") %>' runat="server" EnableViewState="false" />
                                        </div>
                                        <div class="DetailLink">
                                            <asp:HyperLink ID="hlName" runat="server" CssClass='DetailLink' Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' EnableViewState="false"
                                                NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString() %>' meta:resourcekey="hlNameResource1" ToolTip='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>'></asp:HyperLink>
                                            <span style="width: 1px;"></span>
                                            <span class="Price">
                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>' EnableViewState="false"
                                                    Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && SpecialProfile.ShowPrice %>' meta:resourcekey="Label1Resource1"></asp:Label>
                                            </span>
                                            <div class="CallForPrice">
                                                <asp:Label ID="uxRevCallForPricing" EnableViewState="false" runat="server" CssClass="Price"
                                                    Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>'></asp:Label>
                                            </div>
                                        </div>
                                        <div class="ItemBorder"></div>
                                    </div>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
        </div>
    </div>
</asp:Panel>




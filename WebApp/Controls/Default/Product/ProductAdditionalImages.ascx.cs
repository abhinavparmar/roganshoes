using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace WebApp
{
    /// <summary>
    /// Represents the Product Additional Images user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductAdditionalImages : System.Web.UI.UserControl
    {
        #region Private Variables

        private int _productId;
        private int _MaxDisplayColumns = ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.MaxCatalogDisplayItems;
        private bool _HasImages = false;
        private bool _IncludeProductImage = true;
        private ZNodeImage znodeImage = new ZNodeImage();
        private ZNodeProduct Product = new ZNodeProduct();
        private string _imageSizePath = "thumbnail";
        private string _ControlType = string.Empty;

        #endregion

        #region Public Variables

        /// <summary>
        /// Gets a value indicating whether there are related images in this control.
        /// </summary>
        public bool HasImages
        {
            get { return this._HasImages; }
        }

        /// <summary>
        /// Gets or sets the Maximum number of columns used to display the images. Defaults to Max Catalog Display Columns in General Settings.
        /// </summary>
        public int MaxDisplayColumns
        {
            get 
            { 
                return this._MaxDisplayColumns; 
            }

            set 
            { 
                this._MaxDisplayColumns = value; 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to include the main product picture with the images displayed.
        /// </summary>
        public bool IncludeProductImage
        {
            get 
            { 
                return this._IncludeProductImage; 
            }

            set 
            { 
                this._IncludeProductImage = value; 
            }
        }

        /// <summary>
        /// Gets or sets the directory the images should be pulled from (thumbnail, small, medium or large). You do not need to specify the whole path.
        /// </summary>
        public string ImageSizePath
        {
            get 
            { 
                return this._imageSizePath; 
            }

            set 
            { 
                this._imageSizePath = value; 
            }
        }

        /// <summary>
        /// Gets or sets the ControlType for the AdditionalImages
        /// </summary>
        public string ControlType
        {
            get { return this._ControlType; }
            set { this._ControlType = value; }
        }

        #endregion

        #region Public Methods
        
        public string ImageURL(string path)
        {
            return "retrieveimages.aspx?path=" + Server.MapPath(path);
        }

        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Get catalog product image Name
        /// </summary>
        /// <param name="Title">The Value of Title</param>
        /// <returns>Returns the ImageName</returns>
        protected string GetImageName(string Title)
        {
            if (Title.Trim().Length > 0)
            {
                return Title;
            }
            else
            {
                return "&nbsp";
            }
        }

        /// <summary>
        /// Get product image alternative text
        /// </summary>
        /// <param name="AltTagText">Alternate Image Tag Text</param>
        /// <param name="ProductName">Name of the Product</param>
        /// <returns>Returns the Product Image Alternate Text</returns>
        protected string GetImageAltTagText(string AltTagText, string ProductName)
        {
            if (string.IsNullOrEmpty(AltTagText))
            {
                return ProductName;
            }
            else
            {
                return AltTagText;
            }
        }

        /// <summary>
        /// Returns the javascript popup calling method
        /// </summary>    
        /// <param name="Title">The Value of Title</param>
        /// <returns>Returns the  Display Popup string</returns>
        protected string DisplayPopup(string Title)
        {
            if (Title.Trim().Length > 0)
            {
                return "ddrivetip('" + Title.Replace("'", "\\'") + "')";
            }

            return string.Empty;
        }

        /// <summary>
        /// Get the Alternate Thumbnail Image
        /// </summary>
        /// <param name="Imagefile">ImgageFile Name</param>
        /// <param name="AlternateImageFile">Alternate Image File Name</param>
        /// <returns>Returns the Product Swatch Images</returns>
        protected string GetProductSwatchImage(string Imagefile, object AlternateImageFile)
        {
            if (AlternateImageFile != null)
            {
                if (AlternateImageFile.ToString().Length > 0)
                {
                    return AlternateImageFile.ToString();
                }
            }

            return Imagefile.ToString();
        }

        /// <summary>
        /// Get ImagePath path
        /// </summary>
        /// <param name="Imagefile">Image file name.</param>
        /// <returns>Returns image file name with path.</returns>
        protected string GetThumbnailImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(Imagefile);
        }

        /// <summary>
        /// Get ImagePath path
        /// </summary>
        /// <param name="Imagefile">Image file name.</param>
        /// <returns>Returns image file name with path.</returns>
        protected string GetSmallThumbnailImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathSmallThumbnail(Imagefile);
        }

        #endregion

        #region Page Load
        
        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.Product = (ZNodeProduct)HttpContext.Current.Items["Product"];
            this._productId = this.Product.ProductID;
            this.BindImage();
        }
        #endregion

        #region Private Methods
        private void BindImage()
        {
            // Set up our grid.
            DataListAlternateImages.RepeatColumns = this._MaxDisplayColumns;
            DataListSwatches.RepeatColumns = this._MaxDisplayColumns;

            ZNode.Libraries.DataAccess.Service.ProductImageService service = new ZNode.Libraries.DataAccess.Service.ProductImageService();

            if (this.ControlType == "AlternateImage")
            {
                DataSet ds = service.GetByProductIDActiveIndProductImageTypeID(this.Product.ProductID, true, 1).ToDataSet(false);

                DataView dv = new DataView(ds.Tables[0]);

                // Sort by displayorder
                dv.Sort = "DisplayOrder";
                DataListAlternateImages.Visible = true;
                if (dv.Count > 0)
                {
                    DataListAlternateImages.Visible = true;
                    DataListAlternateImages.DataSource = dv;
                    DataListAlternateImages.DataBind();
                }
                else
                {
                    DataListAlternateImages.Visible = false;
                }
            }
            else if (this.ControlType == "Swatches")
            {
                ZNode.Libraries.DataAccess.Entities.TList<ZNode.Libraries.DataAccess.Entities.ProductImage> swatchesList = service.GetByProductIDActiveIndProductImageTypeID(this.Product.ProductID, true, 2);

                // Sort by display order
                swatchesList.Sort("DisplayOrder");

                DataListSwatches.DataSource = swatchesList;
                DataListSwatches.DataBind();
            }
        }
        #endregion
    }
}
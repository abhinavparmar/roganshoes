<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Product_ProductZoom" Codebehind="ProductZoom.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register Src="ProductHighlights.ascx" TagName="ProductHighlights" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<%@ Register Src="ProductAdditionalImages.ascx" TagName="ProductAdditionalImages" TagPrefix="uc1" %>
<%@ Register Src="ProductPrice.ascx" TagName="ProductPrice" TagPrefix="ZNode" %>
<%@ Register Src="ProductRelated.ascx" TagName="ProductRelated" TagPrefix="ZNode" %>
<%@ Register Src="ProductAttributes.ascx" TagName="ProductAttributes" TagPrefix="ZNode" %>
<%@ Register Src="ProductTabs.ascx" TagName="ProductTabs" TagPrefix="ZNode" %>
<%@ Register Src="ProductAddOns.ascx" TagName="ProductAddOns" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>

<asp:Image ID="CatalogItemImage" runat="server" />
<div><asp:ImageButton ID="uxView" runat="server" ImageAlign="AbsMiddle" />
<asp:ImageButton ID="uxAddToCart" runat="server" OnClick="UxAddToCart_Click" ImageAlign="AbsMiddle" /></div>

﻿using System;
using System.Data;
using System.Text;
using System.Web;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the CatalogImage user control class.
    /// </summary>
    public partial class Controls_Default_Product_CatalogImage : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _Product;
        private int _ProductId = 0;
        public int SkuID { get; set; }
        public string ProductImagePath
        {
            get;
            set;
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this._ProductId = int.Parse(Request.Params["zpid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._Product = (ZNodeProduct)HttpContext.Current.Items["Product"];


        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.Bind();

        }
        #endregion

        #region Protected Methods
        /// <summary>
        ///  Represents the Bind Method
        /// </summary>
        protected void Bind()
        {
            if (this._Product != null)
            {
                string element = "<div class=\"CatalogImage_element\">{0}{1}</div>";

                //uxPlaceHolder.Controls.Add(ParseControl(string.Format(element, "<div itemscope itemtype='http://schema.org/Product' class=\"targetarea\"><img border='0'  EnableViewState='false' ID='CatalogItemImage'  alt=\"" + this._Product.ImageAltTag + "\" src=\"" + GetSkuImagePath() + "\" Title=\"" + this._Product.ImageAltTag + "\" data-zoom-image=" + GetSkuImagePath().Replace("/385/","/Original/") + " />", "<link rel='image_src' href=\"" + ResolveUrl(this._Product.MediumImageFilePath) + "\" /></div>")));
                uxPlaceHolder.Controls.Add(ParseControl(string.Format(element, "<div  class=\"targetarea\"><img EnableViewState='false' ID='CatalogItemImage'  alt=\"" + this._Product.ImageAltTag + "\" src=\"" + GetSkuImagePath() + "\" Title=\"" + this._Product.ImageAltTag + "\" data-zoom-image=" + GetSkuImagePath().Replace("/385/", "/Original/") + " />", "<link rel='image_src' href=\"" + ResolveUrl(this._Product.MediumImageFilePath) + "\" /></div>")));

                ZNode.Libraries.DataAccess.Service.ProductImageService service = new ZNode.Libraries.DataAccess.Service.ProductImageService();

                //Znode Old Code : Start
                // AlternateImages
                //TList<ProductImage> productImages = service.GetByProductIDActiveIndProductImageTypeID(this._Product.ProductID, true, 1);

                //productImages.Filter = "ReviewstateId = 20";

                //productImages.Sort("DisplayOrder");

                //ZNodeImage znodeImage = new ZNodeImage();

                //foreach (ProductImage productImage in productImages)
                //{
                //    uxPlaceHolder.Controls.Add(ParseControl(string.Format(element, "<img border='0' runat='server'  EnableViewState='false' alt='" + this.GetImageAltTag(productImage.ImageAltTag, this._Product.Name) + "' src=\"" + znodeImage.GetImageHttpPathMedium(productImage.ImageFile) + "\" Title=\"" + this.GetImageAltTag(productImage.ImageAltTag, this._Product.Name) + "\" />", "<link rel='image_src' href=\"" + znodeImage.GetImageHttpPathMedium(productImage.ImageFile) + "\" />")));

                //}
                //Znode Old Code : End
            }
        }

        /// <summary>
        /// Get product image alternative text
        /// </summary>
        /// <param name="AltTagText">Alternate Image Tag Text</param>
        /// <param name="ProductName">Name of the product</param>
        /// <returns>Returns the Alternate Image Tag</returns>
        protected string GetImageAltTag(string AltTagText, string ProductName)
        {
            if (string.IsNullOrEmpty(AltTagText))
            {
                return ProductName;
            }
            else
            {
                return AltTagText;
            }
        }

        /// <summary>
        /// Get SKU Image Text
        /// </summary>
        /// <param name="AttributeId"></param>
        /// <param name="skuID"></param>
        /// <returns></returns>
        private string GetSkuImagePath()
        {

            ZNodeImage znodeImage = new ZNodeImage();
            string skuImagePath = string.Empty;
            if (this._Product.ProductAllAttributeCollection != null && this._Product.ProductAllAttributeCollection.Rows.Count > 0 && SkuID > 0)
            {
                foreach (DataRow row in this._Product.ProductAllAttributeCollection.Rows)
                {
                    if (row["SkuID"].ToString() == SkuID.ToString() && !string.IsNullOrEmpty(row["SkuPicturePath"].ToString()))
                    {
                        //Create Large Image
                        skuImagePath = ResolveUrl(znodeImage.GetImageHttpPathMedium(row["SkuPicturePath"].ToString()));
                        ProductImagePath = ResolveUrl(znodeImage.GetImageHttpPathLarge(row["SkuPicturePath"].ToString()));
                        break;
                    }
                }
            }
            if (string.IsNullOrEmpty(skuImagePath))
            {
                ProductImagePath = skuImagePath = ResolveUrl(this._Product.MediumImageFilePath);
                ProductImagePath = ResolveUrl(this._Product.LargeImageFilePath);
            }
            return skuImagePath;
        }
        #endregion
    }
}
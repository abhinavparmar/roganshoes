using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.Controls.Default.Product;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
namespace WebApp
{
    /// <summary>
    /// Represents the ProductAttributes user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductAttributes : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _product;
        private ZNodeSKU _SelectedSKU = new ZNodeSKU();
        private string _DefaultAttributeType = string.Empty;
        private bool _IsValid = false;
        private int _BundleProductID = 0;
        //Zeon Private Member
        DataTable dtProductAttribute = null;
        int _SkuID = 0;
        #endregion

        // Public Event Handler
        public event System.EventHandler SelectedIndexChanged;

        #region Public Properties
        /// <summary>
        /// Gets or sets the selected Sku for this attribute combination
        /// </summary>
        public ZNodeSKU SelectedSKU
        {
            get
            {
                return this._SelectedSKU;
            }

            set
            {
                this._SelectedSKU = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the selected items are valid
        /// </summary>
        public bool IsValid
        {
            get
            {
                return this._IsValid;
            }

            set
            {
                this._IsValid = value;
            }
        }

        public string DefaultAttributeType
        {
            get
            {
                return this._DefaultAttributeType;
            }

            set
            {
                this._DefaultAttributeType = value;
            }
        }

        /// <summary>
        /// Gets or sets the bundle product
        /// </summary>
        public int BundleProductID
        {
            get
            {
                return this._BundleProductID;
            }

            set
            {
                this._BundleProductID = value;
            }
        }

        #region Zeon Properties

        /// <summary>
        /// get or set values of _SkuID
        /// </summary>
        public int SKUID
        {
            get
            {
                GetSKUID();
                return this._SkuID;
            }

            set
            {
                this._SkuID = value;
            }
        }

        #endregion

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (this._product.ZNodeAttributeTypeCollection.Count > 0)
            {
                int Counter = 1;
                pnlOptions.Visible = true;
                ControlPlaceHolder.Controls.Add(new LiteralControl("<div>"));

                foreach (ZNodeAttributeType AttributeType in this._product.ZNodeAttributeTypeCollection)
                {
                    //Zeon Custom Code:Starts
                    if (AttributeType.Name.ToString().ToLower().Contains("color"))
                    {
                        System.Web.UI.WebControls.RadioButtonList lstRadioControl = new RadioButtonList();
                        lstRadioControl.ID = "lstAttribute" + AttributeType.AttributeTypeId.ToString();
                        lstRadioControl.AutoPostBack = true;
                        lstRadioControl.CssClass = "AttributeRadio";
                        lstRadioControl.RepeatDirection = RepeatDirection.Horizontal;
                        lstRadioControl.RepeatLayout = RepeatLayout.Flow;
                        lstRadioControl.RepeatColumns = 4;

                        this.DefaultAttributeType = lstRadioControl.ID;
                        lstRadioControl.SelectedIndexChanged += new EventHandler(this.LstRadioControl_SelectedIndexChanged);

                        int defaultSelectedColor = AttributeType.ZNodeAttributeCollection[0].AttributeId;
                        foreach (ZNodeAttribute Attribute in AttributeType.ZNodeAttributeCollection)
                        {
                            ListItem li1 = new ListItem();
                            string skuID = string.Empty;
                            string listItemText = GetSKUImageText(Attribute.AttributeId, out skuID);
                            if (!string.IsNullOrEmpty(listItemText))
                            {
                                li1.Attributes.Add("color", Attribute.Name);
                                li1.Attributes.Add("SKU", skuID);
                                li1.Text = listItemText;
                                li1.Value = Attribute.AttributeId.ToString();
                            }
                            else
                            {
                                li1.Text = Attribute.Name;
                                li1.Value = Attribute.AttributeId.ToString();
                            }
                            li1.Attributes.Add("class", "ItemColor");
                            lstRadioControl.Items.Add(li1);

                        }
                        lstRadioControl.SelectedValue = defaultSelectedColor.ToString(); ;
                        if (!AttributeType.IsPrivate)
                        {
                            //Creating Div Structure
                            Literal ltlDiv = new Literal();
                            ltlDiv.Text = "<div style='float:left;'>" + AttributeType.Name + ": </div><div>";
                            ControlPlaceHolder.Controls.Add(ltlDiv);

                            System.Web.UI.WebControls.Label lblSelColors = new Label();
                            lblSelColors.ID = "lblSelectdColor";
                            lblSelColors.Text = AttributeType.ZNodeAttributeCollection[0].Name;
                            ControlPlaceHolder.Controls.Add(lblSelColors);
                            Literal ltlDivColor = new Literal();
                            ltlDivColor.Text = "</div>";
                            ControlPlaceHolder.Controls.Add(ltlDivColor);

                            Literal ltlDivRadioStart = new Literal();
                            ltlDivRadioStart.Text = "<div ID='divRadioList'>";
                            ControlPlaceHolder.Controls.Add(ltlDivRadioStart);

                            ControlPlaceHolder.Controls.Add(lstRadioControl);

                            ControlPlaceHolder.Controls.Add(new LiteralControl("</div>"));
                            ControlPlaceHolder.Controls.Add(new LiteralControl("</div>"));
                            //Creating Div Structure
                        }
                    }
                    else
                    //Zeon Custom Code:Ends
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                        lstControl.ID = "lstAttribute" + AttributeType.AttributeTypeId.ToString();
                        lstControl.AutoPostBack = true;
                        lstControl.CssClass = "AttributeDropDown";

                        if (Counter++.Equals(1))
                        {
                            this.DefaultAttributeType = lstControl.ID;
                        }

                        ListItem li = new ListItem("Select " + AttributeType.Name, "0");
                        li.Selected = true;

                        lstControl.SelectedIndexChanged += new EventHandler(this.LstControl_SelectedIndexChanged);
                        lstControl.Items.Add(li);

                        foreach (ZNodeAttribute Attribute in AttributeType.ZNodeAttributeCollection)
                        {
                            ListItem li1 = new ListItem(Attribute.Name, Attribute.AttributeId.ToString());

                            lstControl.Items.Add(li1);
                        }

                        #region Commented Code
                        //if (!AttributeType.IsPrivate)
                        //{
                        //    Literal lit1 = new Literal();
                        //    lit1.Text = "<div class=\"Option\"><span>";

                        //    ControlPlaceHolder.Controls.Add(lit1);

                        //    Literal ltrl = new Literal();
                        //    ltrl.Text = "</span><span class='OptionLabel'>" + AttributeType.Name + " :</span>";

                        //    ControlPlaceHolder.Controls.Add(ltrl);

                        //    Literal lt = new Literal();
                        //    lt.Text = "<span class='OptionValue'>";

                        //    ControlPlaceHolder.Controls.Add(lt);
                        //    ControlPlaceHolder.Controls.Add(lstControl);

                        //    Literal literal = new Literal();
                        //    literal.Text = "</span>";
                        //    ControlPlaceHolder.Controls.Add(literal);

                        //    Image img = new Image();
                        //    img.ID = "Image_Attribute" + AttributeType.AttributeTypeId;
                        //    img.ImageAlign = ImageAlign.AbsMiddle;
                        //    img.ImageUrl = ResolveUrl(ZNodeCatalogManager.GetImagePathByLocale("Required.gif"));
                        //    img.Visible = false;

                        //    ControlPlaceHolder.Controls.Add(img);
                        //    ControlPlaceHolder.Controls.Add(new LiteralControl("</div>"));
                        #endregion

                        if (!AttributeType.IsPrivate)
                        {
                            Literal lit1 = new Literal();
                            lit1.Text = "<table class=\"Option\">";

                            ControlPlaceHolder.Controls.Add(lit1);

                            Literal ltrl = new Literal();
                            ltrl.Text = "<tr><td class='OptionLabel'>" + AttributeType.Name + " :</td>";

                            ControlPlaceHolder.Controls.Add(ltrl);

                            Literal lt = new Literal();
                            lt.Text = "<td class='OptionValue'>";

                            ControlPlaceHolder.Controls.Add(lt);
                            ControlPlaceHolder.Controls.Add(lstControl);

                            Literal literal = new Literal();
                            literal.Text = "</td>";
                            ControlPlaceHolder.Controls.Add(literal);
                            ControlPlaceHolder.Controls.Add(new LiteralControl("<td>"));
                            Image img = new Image();
                            img.ID = "Image_Attribute" + AttributeType.AttributeTypeId;
                            img.ImageAlign = ImageAlign.AbsMiddle;
                            img.ImageUrl = ResolveUrl(ZNodeCatalogManager.GetImagePathByLocale("Required.gif"));
                            img.Visible = false;

                            ControlPlaceHolder.Controls.Add(img);
                            ControlPlaceHolder.Controls.Add(new LiteralControl("</td>"));
                            ControlPlaceHolder.Controls.Add(new LiteralControl("</tr></table>"));
                        }
                    }
                }
            }
            else
            {
                pnlOptions.Visible = false;
                return;
            }
        }

        #endregion

        #region Helper Functions
        /// <summary>
        /// Validate selected attributes and return a user message
        /// </summary>
        /// <param name="Message">The value of Message</param>
        /// <param name="Attributes">The Value of Attributes</param>
        /// <param name="SelectedAttributesDescription">Selected Attributes Description</param>
        /// <returns>Returns the user message to validate selected attributes </returns>
        public bool ValidateAttributes(out string Message, out string Attributes, out string SelectedAttributesDescription)
        {
            System.Text.StringBuilder _attributes = new System.Text.StringBuilder();
            System.Text.StringBuilder _description = new System.Text.StringBuilder();

            // Loop through types to locate the controls
            foreach (ZNodeAttributeType AttributeType in this._product.ZNodeAttributeTypeCollection)
            {
                if (_attributes.Length > 0)
                {
                    _attributes.Append(",");
                }

                if (!AttributeType.IsPrivate)
                {
                    //Zeon Custom Code:Starts
                    int selValue = 0;
                    string selText = string.Empty;
                    if (AttributeType.Name.ToString().ToLower().Contains("color"))
                    {
                        System.Web.UI.WebControls.RadioButtonList lstControl = (System.Web.UI.WebControls.RadioButtonList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());
                        selValue = int.Parse(lstControl.SelectedValue);
                        selText = lstControl.SelectedItem.Attributes["color"].ToString();
                    }
                    else
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());
                        selValue = int.Parse(lstControl.SelectedValue);
                        selText = lstControl.SelectedItem.Text;
                    }

                    // System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString()); //Znode old Code
                    //int selValue = int.Parse(lstControl.SelectedValue);
                    //Zeon Custom Code:Ends
                    Image img = (Image)ControlPlaceHolder.FindControl("Image_Attribute" + AttributeType.AttributeTypeId.ToString());

                    if (img != null)
                    {
                        img.Visible = false;
                    }

                    if (selValue > 0)
                    {
                        AttributeType.SelectedAttributeId = selValue;

                        _attributes.Append(selValue.ToString());

                        _description.Append(AttributeType.Name);
                        _description.Append(" - ");
                        //_description.Append(lstControl.SelectedItem.Text);//Znode old Code
                        _description.Append(selText);
                        //_description.Append("<br />");
                    }
                    else
                    {
                        if (img != null)
                        {
                            img.Visible = true;
                        }

                        Message = " <div>Select " + AttributeType.Name + "</div>";
                        Attributes = _attributes.ToString();
                        SelectedAttributesDescription = string.Empty;

                        return false;
                    }
                }
            }

            Message = string.Empty;
            Attributes = _attributes.ToString();
            SelectedAttributesDescription = _description.ToString();
            return true;
        }
        #endregion

        #region Protected Methods and  Events

        /// <summary>
        /// Event Handler for the dynamic control DropDownList
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Message = String.Empty;
            string ProductAttributes = String.Empty;
            string Description = String.Empty;
            ZNodeSKU _SKU = null;

            // Validate attributes first - This is to verify that all required attributes have a valid selection
            // If a valid selection is not found then an error message is displayed
            this._IsValid = this.ValidateAttributes(out Message, out ProductAttributes, out Description);

            if (this._IsValid)
            {
                // Get a sku based on attributes selected
                _SKU = ZNodeSKU.CreateByProductAndAttributes(this._product.ProductID, ProductAttributes);
                _SKU.AttributesDescription = Description;

                this._SelectedSKU = _SKU;
            }
            if (this.BundleProductID > 0)
            {
                ZNodeProduct product = (ZNodeProduct)HttpContext.Current.Items["Product"];

                for (int idx = 0; idx < product.ZNodeBundleProductCollection.Count; idx++)
                {
                    if (product.ZNodeBundleProductCollection[idx].ProductID == this.BundleProductID)
                    {
                        // Set sku object
                        product.ZNodeBundleProductCollection[idx].SelectedSKUvalue = this._SelectedSKU;
                    }
                }
            }

            if (this.SelectedIndexChanged != null)
            {
                this.SelectedIndexChanged(sender, e);
            }
            //load alternate image control
            LoadAlternateImageControl();
        }

        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            if (this.BundleProductID > 0)
            {
                if (this._product == null)
                {
                    this._product = ZNodeProduct.Create(this.BundleProductID);
                }
            }

            this.Bind();
            if (!IsPostBack)
            {
                LoadAlternateImageControl();
            }
        }
        #endregion

        #region Zeon Custom Code

        #region Events

        protected void LstRadioControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Set Current Color Text
            System.Web.UI.WebControls.RadioButtonList radiolist = (System.Web.UI.WebControls.RadioButtonList)sender;
            System.Web.UI.WebControls.Label lblSelColor = (System.Web.UI.WebControls.Label)ControlPlaceHolder.FindControl("lblSelectdColor");
            string selectedColor = radiolist.SelectedItem.Attributes["color"].ToString();
            if (lblSelColor != null)
            {
                lblSelColor.Text = radiolist.SelectedItem.Attributes["color"].ToString();
            }
            string Message = String.Empty;
            string ProductAttributes = String.Empty;
            string Description = String.Empty;
            ZNodeSKU _SKU = null;

            // Validate attributes first - This is to verify that all required attributes have a valid selection
            // If a valid selection is not found then an error message is displayed
            this._IsValid = this.ValidateAttributes(out Message, out ProductAttributes, out Description);

            if (this._IsValid)
            {
                // Get a sku based on attributes selected
                _SKU = ZNodeSKU.CreateByProductAndAttributes(this._product.ProductID, ProductAttributes);
                _SKU.AttributesDescription = Description;

                this._SelectedSKU = _SKU;
                //Zeon Custom Code : Start
                this._product.SelectedSKU = _SKU;
                HttpContext.Current.Items["Product"] = this._product;
                //Zeon Custom Code : End
            }

            if (this.BundleProductID > 0)
            {
                ZNodeProduct product = (ZNodeProduct)HttpContext.Current.Items["Product"];

                for (int idx = 0; idx < product.ZNodeBundleProductCollection.Count; idx++)
                {
                    if (product.ZNodeBundleProductCollection[idx].ProductID == this.BundleProductID)
                    {
                        // Set sku object
                        product.ZNodeBundleProductCollection[idx].SelectedSKUvalue = this._SelectedSKU;
                    }
                }
            }
            //load alternate image control
            LoadAlternateImageControl();
        }       

        #endregion

        #region Private Methods

        /// <summary>
        /// Get SKU Image Text
        /// </summary>
        /// <param name="AttributeId"></param>
        /// <param name="skuID"></param>
        /// <returns></returns>
        private string GetSKUImageText(int AttributeId, out string skuID)
        {
            skuID = string.Empty;
            ZNodeImage znodeImage = new ZNodeImage();
            string skuImagePath = string.Empty;
            string imageHTML = "<img class='SwatchImage' src='{0}' onclick=\"document.getElementById('CatalogItemImage').src='{1}';\"/>";
            string productImage = string.Empty;
            if (this._product.ProductAllAttributeCollection != null && this._product.ProductAllAttributeCollection.Rows.Count > 0)
            {
                foreach (DataRow row in this._product.ProductAllAttributeCollection.Rows)
                {
                    if (row["AttributeID"].ToString() == AttributeId.ToString() && !string.IsNullOrEmpty(row["SkuPicturePath"].ToString()))
                    {
                        skuImagePath = ResolveUrl(znodeImage.GetImageHttpPathSmallThumbnail(row["SkuPicturePath"].ToString()));
                        skuImagePath = skuImagePath.Replace("-swatch.", ".");
                        productImage = ResolveUrl(znodeImage.GetImageHttpPathMedium(row["SkuPicturePath"].ToString()));
                        //Create Large Image
                        znodeImage.GetImageHttpPathLarge(row["SkuPicturePath"].ToString());
                        skuImagePath = string.Format(imageHTML, skuImagePath, productImage);
                        skuID = row["SkuId"].ToString();
                        break;

                    }
                }
            }

            return skuImagePath;
        }

        /// <summary>
        /// GET SKUID
        /// </summary>
        private void GetSKUID()
        {
            int selSku = 0;
            foreach (ZNodeAttributeType AttributeType in this._product.ZNodeAttributeTypeCollection)
            {
                if (AttributeType.Name.ToString().ToLower().Contains("color"))
                {
                    System.Web.UI.WebControls.RadioButtonList lstControl = (System.Web.UI.WebControls.RadioButtonList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());
                    selSku = int.Parse(lstControl.SelectedItem.Attributes["SKU"].ToString());
                    this.SKUID = selSku;
                }
            }
        }

        /// <summary>
        /// load alternate image control
        /// </summary>
        private void LoadAlternateImageControl()
        {
            ZProductAlternateImages alternateImage = (ZProductAlternateImages)this.Parent.FindControl("uxAternateImages");
            UpdatePanel updatepnl = (UpdatePanel)this.Parent.FindControl("upnlAlternateImage");
            if (alternateImage != null)
            {
                alternateImage.SKUID = this.SKUID;
                updatepnl.Update();
            }
        }

        #endregion

        #endregion

    }
}
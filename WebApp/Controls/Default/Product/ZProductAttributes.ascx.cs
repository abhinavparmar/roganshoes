﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.Controls.Default.Product;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    public partial class ZProductAttributes : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct product;
        private ZNodeSKU selectedSKU = new ZNodeSKU();
        private string defaultAttributeType = string.Empty;
        private bool isValid = false;
        private int bundleProductID = 0;
        private bool isQuickWatch = false;
        int skuID = 0;
        private string sAttributeTypeComboInitial = "lstAttribute";

        #endregion

        // Public Event Handler
        public event System.EventHandler SelectedIndexChanged;
        public event System.EventHandler ColorSelectedIndexChanged;

        #region Public Properties
        /// <summary>
        /// Gets or sets the selected Sku for this attribute combination
        /// </summary>
        public ZNodeSKU SelectedSKU
        {
            get
            {
                return this.selectedSKU;
            }

            set
            {
                this.selectedSKU = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the selected items are valid
        /// </summary>
        public bool IsValid
        {
            get
            {
                return this.isValid;
            }

            set
            {
                this.isValid = value;
            }
        }

        public string DefaultAttributeType
        {
            get
            {
                return this.defaultAttributeType;
            }

            set
            {
                this.defaultAttributeType = value;
            }
        }

        /// <summary>
        /// Gets or sets the bundle product
        /// </summary>
        public int BundleProductID
        {
            get
            {
                return this.bundleProductID;
            }

            set
            {
                this.bundleProductID = value;
            }
        }



        /// <summary>
        /// get or set values of _SkuID
        /// </summary>
        public int SKUID
        {
            get
            {
                GetSKUID();
                return this.skuID;
            }

            set
            {
                this.skuID = value;
            }
        }

        /// <summary>
        /// get or set default SKUID
        /// </summary>
        public int DefaultSKU { get; set; }

        public bool IsLastAttributeSelectedIndexChanged { get; set; }

        /// <summary>
        /// get or set color is last attribute
        /// </summary>
        public bool IsColorIsLastAttribute { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether QuickWatch is enabled or not
        /// </summary>
        public bool IsQuickWatch
        {
            get
            {
                return this.isQuickWatch;
            }

            set
            {
                this.isQuickWatch = value;
            }
        }

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (this.product.ZNodeAttributeTypeCollection.Count > 0)
            {
                int counter = 1;
                int sizeCounter = 1;
                pnlOptions.Visible = true;
                ControlPlaceHolder.Controls.Add(new LiteralControl("<div>"));

                foreach (ZNodeAttributeType AttributeType in this.product.ZNodeAttributeTypeCollection)
                {
                    //Zeon Custom Code:Starts
                    if (AttributeType.Name.ToString().ToLower().Contains("color"))
                    {
                        this.BindColorAttribute(AttributeType);
                    }
                    else
                    {
                        //Zeon Custom Code:Ends
                        this.DropDownListControlData(AttributeType, counter, sizeCounter);
                    }
                    sizeCounter++;
                }
                ControlPlaceHolder.Controls.Add(new LiteralControl("</div>"));
            }
            else
            {
                pnlOptions.Visible = false;
                return;
            }
        }

        #endregion

        #region Helper Functions
        /// <summary>
        /// Validate selected attributes and return a user message
        /// </summary>
        /// <param name="Message">The value of Message</param>
        /// <param name="Attributes">The Value of Attributes</param>
        /// <param name="SelectedAttributesDescription">Selected Attributes Description</param>
        /// <returns>Returns the user message to validate selected attributes </returns>
        public bool ValidateAttributes(out string Message, out string Attributes, out string SelectedAttributesDescription)
        {
            System.Text.StringBuilder _attributes = new System.Text.StringBuilder();
            System.Text.StringBuilder _description = new System.Text.StringBuilder();

            // Loop through types to locate the controls
            foreach (ZNodeAttributeType AttributeType in this.product.ZNodeAttributeTypeCollection)
            {
                if (_attributes.Length > 0)
                {
                    _attributes.Append(",");
                }

                if (!AttributeType.IsPrivate)
                {
                    //Zeon Custom Code:Starts
                    int selValue = 0;
                    string selText = string.Empty;
                    if (AttributeType.Name.ToString().ToLower().Contains("color"))
                    {
                        System.Web.UI.WebControls.RadioButtonList lstControl = (System.Web.UI.WebControls.RadioButtonList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());
                        if (lstControl != null)
                        {
                            selValue = int.Parse(lstControl.SelectedValue);
                            selText = lstControl.SelectedItem.Attributes["color"].ToString();
                        }
                    }
                    else
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());
                        if (lstControl != null)
                        {
                            selValue = int.Parse(lstControl.SelectedValue);
                            selText = lstControl.SelectedItem.Text;
                        }
                    }

                    // System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString()); //Znode old Code
                    //int selValue = int.Parse(lstControl.SelectedValue);
                    //Zeon Custom Code:Ends
                    //Image img = (Image)ControlPlaceHolder.FindControl("Label_Attribute" + AttributeType.AttributeTypeId.ToString());

                    //if (img != null)
                    //{
                    //    img.Visible = false;
                    //}


                    Literal lblError = (Literal)ControlPlaceHolder.FindControl("Literal_Attribute" + AttributeType.AttributeTypeId.ToString());
                    if (lblError != null)
                    {
                        string text = lblError.Text;
                        //lblError.Text = text.Replace("OptionLabel ImageRequired", "OptionLabel");
                    }
                    //PRFT Custom Code : Start
                    Literal lblErrorMsg = (Literal)ControlPlaceHolder.FindControl("Literal_Error" + AttributeType.AttributeTypeId.ToString());
                    if (lblErrorMsg != null)
                    {
                        lblErrorMsg.Visible = false;
                        lblErrorMsg.Text = "<div class='Error'> Please Select " + AttributeType.Name + "</div>";
                    }

                    if (!AttributeType.Name.ToString().ToLower().Contains("color"))
                    {
                        DropDownList ltrOption = (DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());
                        if (ltrOption != null)
                        {
                            if (AttributeType.Name.ToLower().Equals("size")) { ltrOption.CssClass = "AttributeDropDown SelectBoxSize"; } else if (AttributeType.Name.ToLower().Equals("width")) { ltrOption.CssClass = "AttributeDropDown SelectBoxWidth"; };
                        }
                    }
                    //PRFT Custom Code : End
                    if (selValue > 0)
                    {

                        string desc = "<span><strong>#attrname#:</strong> #attrValue#&nbsp;</span>";

                        AttributeType.SelectedAttributeId = selValue;
                        desc = desc.Replace("#attrname#", AttributeType.Name).Replace("#attrValue#", selText); ;

                        _attributes.Append(selValue.ToString());

                        //_description.Append(AttributeType.Name);
                        //_description.Append(" - ");
                        ////_description.Append(lstControl.SelectedItem.Text);//Znode old Code
                        //_description.Append(selText);
                        //_description.Append("<br />");

                        _description.Append(desc);
                    }
                    else
                    {
                        //PRFT Custom Code : Start
                        if (lblErrorMsg != null)
                        {
                            lblErrorMsg.Visible = true;
                        }
                        DropDownList ltrOption = (DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());
                        if (ltrOption != null)
                        {
                            if (AttributeType.Name.ToLower().Equals("size")) { ltrOption.CssClass = "AttributeDropDown SelectBoxSize err-msg"; } else if (AttributeType.Name.ToLower().Equals("width")) { ltrOption.CssClass = "AttributeDropDown SelectBoxWidth err-msg"; };
                        }
                        //PRFT Custom Code : End
                        if (lblError != null)
                        {
                            string text = lblError.Text;
                            lblError.Text = text.Replace("OptionLabel", "OptionLabel ImageRequired");
                        }

                        //Message = "<div>Please Select " + AttributeType.Name + "</div>";
                        Message = String.Empty;
                        Attributes = _attributes.ToString();
                        SelectedAttributesDescription = string.Empty;

                        return false;
                    }
                }
            }

            Message = string.Empty;
            Attributes = _attributes.ToString();
            SelectedAttributesDescription = _description.ToString();
            return true;
        }

        public void ResetAttributeErrorMessage()
        {
            foreach (ZNodeAttributeType AttributeType in this.product.ZNodeAttributeTypeCollection)
            {
                Literal lblErrorMsg = (Literal)ControlPlaceHolder.FindControl("Literal_Error" + AttributeType.AttributeTypeId.ToString());
                if (lblErrorMsg != null)
                {
                    lblErrorMsg.Text = string.Empty;
                }

                if (!AttributeType.Name.ToString().ToLower().Contains("color"))
                {
                    DropDownList ltrOption = (DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());
                    if (ltrOption != null)
                    {
                        if (AttributeType.Name.ToLower().Equals("size")) { ltrOption.CssClass = "AttributeDropDown SelectBoxSize"; } else if (AttributeType.Name.ToLower().Equals("width")) { ltrOption.CssClass = "AttributeDropDown SelectBoxWidth"; };
                    }
                }
            }
        }
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            if (this.BundleProductID > 0)
            {
                if (this.product == null)
                {
                    this.product = ZNodeProduct.Create(this.BundleProductID);
                }
            }

            this.Bind();
            if (!IsPostBack)
            {
                this.SelectCurrentSKUAttributes();
                this.LoadAlternateImageControl();
                this.LoadDefaultCatalogImage();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.SetSelectedCssForColorAttribute();

            #region[Code commented : as do not implemented slide on color attribute]
            //RegisterClientScriptClasses();
            #endregion
        }

        #endregion

        #region Zeon Custom Methods

        #region  Protected Methods and  Events

        /// <summary>
        /// Color Attribute Selected Index Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LstRadioControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.RadioButtonList radiolist = (System.Web.UI.WebControls.RadioButtonList)sender;
            System.Web.UI.WebControls.Label lblSelColor = (System.Web.UI.WebControls.Label)ControlPlaceHolder.FindControl("lblSelectdColor");
            string selectedColor = radiolist.SelectedItem.Attributes["color"].ToString();
            if (lblSelColor != null)
            {
                lblSelColor.Text = radiolist.SelectedItem.Attributes["color"].ToString();
            }
            string Message = String.Empty;
            string ProductAttributes = String.Empty;
            string Description = String.Empty;

            if (sender is RadioButtonList)
            {
                bool isLastAttribute = IsLastAttributeSelectedIndexChanged = CheckIsLastAttribute((RadioButtonList)sender);
                if (!isLastAttribute)
                {
                    this.LoadNextAttributeTypeCombo(radiolist);
                }
            }

            //LoadNextAttributeTypeCombo(radiolist);

            if (this.ColorSelectedIndexChanged != null)
            {
                this.ColorSelectedIndexChanged(sender, e);
            }

            #region[Code Commented]
            /*
             ZNodeSKU _SKU = null;
            // Validate attributes first - This is to verify that all required attributes have a valid selection
            // If a valid selection is not found then an error message is displayed
            this._IsValid = this.ValidateAttributes(out Message, out ProductAttributes, out Description);

            if (this._IsValid)
            {
                // Get a sku based on attributes selected
                _SKU = ZNodeSKU.CreateByProductAndAttributes(this._product.ProductID, ProductAttributes);
                _SKU.AttributesDescription = Description;

                this._SelectedSKU = _SKU;
                //Zeon Custom Code : Start
                this._product.SelectedSKU = _SKU;
                HttpContext.Current.Items["Product"] = this._product;
                //Zeon Custom Code : End
            }

            if (this.BundleProductID > 0)
            {
                ZNodeProduct product = (ZNodeProduct)HttpContext.Current.Items["Product"];

                for (int idx = 0; idx < product.ZNodeBundleProductCollection.Count; idx++)
                {
                    if (product.ZNodeBundleProductCollection[idx].ProductID == this.BundleProductID)
                    {
                        // Set sku object
                        product.ZNodeBundleProductCollection[idx].SelectedSKUvalue = this._SelectedSKU;
                    }
                }
            }
            */
            #endregion

            this.LoadAlternateImageControl();
            this.LoadDefaultCatalogImage();
        }


        /// <summary>
        /// Event Handler for the dynamic control DropDownList
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Message = String.Empty;
            string ProductAttributes = String.Empty;
            string Description = String.Empty;


            if (sender is DropDownList)
            {
                bool isLastAttribute = IsLastAttributeSelectedIndexChanged = CheckIsLastAttribute((DropDownList)sender);
                if (!isLastAttribute)
                {
                    this.LoadNextAttributeTypeCombo((DropDownList)sender);
                }
            }

            #region[Commented Code:: Zeon Customization :: As calling same method multiple times]
            /*
             ZNodeSKU _SKU = null;
            // Validate attributes first - This is to verify that all required attributes have a valid selection
            // If a valid selection is not found then an error message is displayed
            this._IsValid = this.ValidateAttributes(out Message, out ProductAttributes, out Description);

            if (this._IsValid)
            {
                // Get a sku based on attributes selected
                _SKU = ZNodeSKU.CreateByProductAndAttributes(this._product.ProductID, ProductAttributes);
                _SKU.AttributesDescription = Description;

                this._SelectedSKU = _SKU;
            }
            if (this.BundleProductID > 0)
            {
                ZNodeProduct product = (ZNodeProduct)HttpContext.Current.Items["Product"];

                for (int idx = 0; idx < product.ZNodeBundleProductCollection.Count; idx++)
                {
                    if (product.ZNodeBundleProductCollection[idx].ProductID == this.BundleProductID)
                    {
                        // Set sku object
                        product.ZNodeBundleProductCollection[idx].SelectedSKUvalue = this._SelectedSKU;
                    }
                }
            }
            */
            #endregion

            if (this.SelectedIndexChanged != null)
            {
                this.SelectedIndexChanged(sender, e);
            }

            //load alternate image control
            LoadAlternateImageControl();
            LoadDefaultCatalogImage();
            if (sender is DropDownList)
            {
                DropDownList ddl = (DropDownList)sender;
                ddl.Focus();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get SKU Image Text
        /// </summary>
        /// <param name="AttributeId"></param>
        /// <param name="skuID"></param>
        /// <returns></returns>
        private string GetSKUImageText(int AttributeId, string attributeName, out string skuID)
        {
            skuID = string.Empty;
            ZNodeImage znodeImage = new ZNodeImage();
            string skuImagePath = string.Empty;
            string imageHTML = string.Empty;
            if (!IsQuickWatch)
            {
                imageHTML = "<img class='SwatchImage' src='{0}' onclick=\"{1}\" alt={2} title={3} />";
            }
            else
            {
                imageHTML = "<img class='SwatchImage' src='{0}' alt={2} title={3}  onclick=\"document.getElementById('CatalogItemImage').src='{1}';\"/>";
            }

            string productImage = string.Empty;
            if (this.product.ProductAllAttributeCollection != null && this.product.ProductAllAttributeCollection.Rows.Count > 0)
            {
                foreach (DataRow row in this.product.ProductAllAttributeCollection.Rows)
                {
                    if (row["AttributeID"].ToString() == AttributeId.ToString() && !string.IsNullOrEmpty(row["SkuPicturePath"].ToString()))
                    {
                        skuImagePath = ResolveUrl(znodeImage.GetImageHttpPathThumbnail(row["SkuPicturePath"].ToString()));
                        skuImagePath = skuImagePath.Replace("-swatch.", ".");
                        productImage = ResolveUrl(znodeImage.GetImageHttpPathMedium(row["SkuPicturePath"].ToString()));
                        //Create Large Image
                        znodeImage.GetImageHttpPathLarge(row["SkuPicturePath"].ToString());
                        if (!IsQuickWatch)
                        {
                            skuImagePath = string.Format(imageHTML, skuImagePath, "objProduct.UpdateAlternateImage('" + productImage + "')", attributeName, attributeName);
                        }
                        else
                        {
                            skuImagePath = string.Format(imageHTML, skuImagePath, attributeName, productImage, attributeName);
                        }
                        skuID = row["SkuId"].ToString();
                        break;
                    }
                }
            }
            return skuImagePath;
        }

        /// <summary>
        /// GET SKUID
        /// </summary>
        private void GetSKUID()
        {
            int selSku = 0;
            foreach (ZNodeAttributeType AttributeType in this.product.ZNodeAttributeTypeCollection)
            {
                if (AttributeType.Name.ToString().ToLower().Contains("color"))
                {
                    System.Web.UI.WebControls.RadioButtonList lstControl = (System.Web.UI.WebControls.RadioButtonList)ControlPlaceHolder.FindControl("lstAttribute" + AttributeType.AttributeTypeId.ToString());
                    if (lstControl != null && lstControl.SelectedItem.Attributes["SKU"] != null)
                    {
                        selSku = int.Parse(lstControl.SelectedItem.Attributes["SKU"].ToString());
                        //Set Default SKU
                        this.DefaultSKU = int.Parse(lstControl.Items[0].Attributes["SKU"].ToString());
                        this.SKUID = selSku;
                    }
                }
            }
        }

        /// <summary>
        /// load alternate image control
        /// </summary>
        private void LoadAlternateImageControl()
        {
            ZProductAlternateImages alternateImage = (ZProductAlternateImages)this.Parent.FindControl("uxAternateImages");
            UpdatePanel updatepnl = (UpdatePanel)this.Parent.FindControl("upnlAlternateImage");
            if (alternateImage != null)
            {
                //set default SKU
                int skuId = this.SKUID;
                alternateImage.DefaultSKU = DefaultSKU;
                alternateImage.SKUID = skuId;
                if (updatepnl != null)
                {
                    updatepnl.Update();
                }
            }
        }

        /// <summary>
        /// Load Default Catalog Image
        /// </summary>
        private void LoadDefaultCatalogImage()
        {
            Controls_Default_Product_CatalogImage znodecatlogImage = (Controls_Default_Product_CatalogImage)this.Parent.FindControl("CatalogItemImage");
            UpdatePanel upCatalogImage = (UpdatePanel)this.Parent.FindControl("upCatalogImage");
            if (znodecatlogImage != null)
            {
                znodecatlogImage.SkuID = this.SKUID;
                if (upCatalogImage != null)
                {
                    upCatalogImage.Update();
                }
            }
        }

        /// <summary>
        /// Bind Color Attribute Data
        /// </summary>
        /// <param name="AttributeType"></param>
        private void BindColorAttribute(ZNodeAttributeType AttributeType)
        {
            if (AttributeType != null && AttributeType.ZNodeAttributeCollection != null && AttributeType.ZNodeAttributeCollection.Count > 0)
            {
                System.Web.UI.WebControls.RadioButtonList lstRadioControl = new RadioButtonList();
                lstRadioControl.ID = "lstAttribute" + AttributeType.AttributeTypeId.ToString();
                lstRadioControl.AutoPostBack = true;
                lstRadioControl.CssClass = "AttributeRadio";
                lstRadioControl.RepeatDirection = RepeatDirection.Vertical;
                lstRadioControl.RepeatLayout = RepeatLayout.UnorderedList;
                //lstRadioControl.RepeatColumns = 4;

                lstRadioControl.Attributes.Add("data-attrtypeid", AttributeType.AttributeTypeId.ToString());

                this.DefaultAttributeType = lstRadioControl.ID;
                lstRadioControl.SelectedIndexChanged += new EventHandler(this.LstRadioControl_SelectedIndexChanged);

                int defaultSelectedColor = AttributeType.ZNodeAttributeCollection[0].AttributeId;
                foreach (ZNodeAttribute Attribute in AttributeType.ZNodeAttributeCollection)
                {
                    ListItem li1 = new ListItem();
                    string skuID = string.Empty;
                    string listItemText = GetSKUImageText(Attribute.AttributeId, Attribute.Name, out skuID);
                    if (!string.IsNullOrEmpty(listItemText))
                    {
                        li1.Attributes.Add("color", Attribute.Name);
                        li1.Attributes.Add("SKU", skuID);

                        li1.Text = Attribute.Name + listItemText;
                        li1.Value = Attribute.AttributeId.ToString();
                    }
                    else
                    {
                        li1.Text = Attribute.Name;
                        li1.Value = Attribute.AttributeId.ToString();
                    }
                    li1.Attributes.Add("title", Attribute.Name);
                    li1.Attributes.Add("class", "item ItemColor");
                    lstRadioControl.Items.Add(li1);
                }
                lstRadioControl.SelectedValue = defaultSelectedColor.ToString(); ;
                if (!AttributeType.IsPrivate)
                {
                    //Creating Div Structure
                    Literal ltlDiv = new Literal();
                    //ltlDiv.Text = "<div style='float:left;'>" + AttributeType.Name + " - </div><div>";
                    ltlDiv.Text = "<fieldset><legend><div style='float:left;'>" + AttributeType.Name + " - </div><div>";
                    ControlPlaceHolder.Controls.Add(ltlDiv);

                    System.Web.UI.WebControls.Label lblSelColors = new Label();
                    lblSelColors.ID = "lblSelectdColor";
                    lblSelColors.CssClass = "SelectedColor";
                    lblSelColors.Text = AttributeType.ZNodeAttributeCollection[0].Name;
                    ControlPlaceHolder.Controls.Add(lblSelColors);
                    Literal ltlDivColor = new Literal();
                    ltlDivColor.Text = "</div></legend>";
                    ControlPlaceHolder.Controls.Add(ltlDivColor);

                    Literal ltlDivRadioStart = new Literal();
                    ltlDivRadioStart.Text = "<div ID='divRadioList'>";
                    ControlPlaceHolder.Controls.Add(ltlDivRadioStart);

                    ControlPlaceHolder.Controls.Add(lstRadioControl);

                    ControlPlaceHolder.Controls.Add(new LiteralControl("</div></fieldset>"));

                    //Creating Div Structure
                }
                IsColorIsLastAttribute = product.ZNodeAttributeTypeCollection != null && product.ZNodeAttributeTypeCollection.Count <= 1 ? true : false;
            }
        }

        /// <summary>
        /// Bind Drop Down control on contentplace holder
        /// </summary>
        /// <param name="AttributeType"></param>
        /// <param name="counter"></param>
        private void DropDownListControlData(ZNodeAttributeType AttributeType, int counter, int sizeCounter)
        {
            System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
            this.BindAttributeDropDownListData(AttributeType, lstControl, counter);

            if (!AttributeType.IsPrivate)
            {
                Literal lit1 = new Literal();
                lit1.Text = "<table class=\"Option\">";

                ControlPlaceHolder.Controls.Add(lit1);

                Literal ltrl = new Literal();

                //Adding Size Chart Link
                string sizeChartLink = string.Empty;
                if (sizeCounter == this.product.ZNodeAttributeTypeCollection.Count)
                {
                    sizeChartLink = !string.IsNullOrEmpty(this.product.DownloadLink) ? GetSizeChartLinkHTML() : string.Empty;
                }

                //ControlPlaceHolder.Controls.Add(new LiteralControl("<td>"));
                //Image img = new Image();
                //img.ID = "Image_Attribute" + AttributeType.AttributeTypeId;
                //img.ImageAlign = ImageAlign.AbsMiddle;
                //img.ImageUrl = ResolveUrl(ZNodeCatalogManager.GetImagePathByLocale("Required.gif"));
                //img.Visible = false;
                //ControlPlaceHolder.Controls.Add(img);
                //ControlPlaceHolder.Controls.Add(new LiteralControl("</td>"));

                Literal ltrlErrortable = new Literal();
                ltrlErrortable.Text = "<tr><td colspan='2' class='err-divbox'>";
                Literal ltrlError = new Literal();
                ltrlError.ID = "Literal_Error" + AttributeType.AttributeTypeId;
                //ltrlError.Text = "<tr><td colspan='2'><div class='Error'> Please Select " + AttributeType.Name + "</div></td></tr>";
                ltrlError.Text = "";
                Literal ltre = new Literal();
                ltre.Text = "</td></tr>";
                ControlPlaceHolder.Controls.Add(ltrlErrortable);
                ControlPlaceHolder.Controls.Add(ltrlError);
                ControlPlaceHolder.Controls.Add(ltre);


                ltrl.ID = "Literal_Attribute" + AttributeType.AttributeTypeId;
                ltrl.Text = "<tr><td class='OptionLabel ImageRequired'>" + AttributeType.Name + " :</td>";

                ControlPlaceHolder.Controls.Add(ltrl);
                Literal lt = new Literal();
                lt.Text = "<td class='OptionValue'>";

                ControlPlaceHolder.Controls.Add(lt);
                ControlPlaceHolder.Controls.Add(lstControl);

                if (sizeCounter < 3)
                {
                    lstControl.Focus(); //PRFT Custom Code : ADA task for auto focus on dropdown list
                }
                Literal literal = new Literal();
                literal.Text = "&nbsp;&nbsp;" + sizeChartLink + "</td>";
                ControlPlaceHolder.Controls.Add(literal);

                ControlPlaceHolder.Controls.Add(new LiteralControl("</tr></table>"));
            }
        }

        /// <summary>
        /// Bind Drop Down Attribute (Size & width)
        /// </summary>
        /// <param name="AttributeType"></param>
        /// <param name="Counter"></param>
        private void BindAttributeDropDownListData(ZNodeAttributeType AttributeType, System.Web.UI.WebControls.DropDownList lstControl, int Counter)
        {
            lstControl.ID = "lstAttribute" + AttributeType.AttributeTypeId.ToString();
            lstControl.AutoPostBack = true;
            if (AttributeType.Name.ToLower().Equals("size")) { lstControl.CssClass = "AttributeDropDown SelectBoxSize"; } else if (AttributeType.Name.ToLower().Equals("width")) { lstControl.CssClass = "AttributeDropDown SelectBoxWidth"; };
            if (Counter++.Equals(1))
            {
                this.DefaultAttributeType = lstControl.ID;
            }
            lstControl.Attributes.Add("alt", AttributeType.Name.ToString());
            lstControl.Attributes.Add("title", AttributeType.Name.ToString());
            ListItem li = new ListItem("Select", "0");
            li.Selected = true;
            lstControl.SelectedIndexChanged += new EventHandler(this.LstControl_SelectedIndexChanged);
            lstControl.Items.Add(li);
        }

        /// <summary>
        /// Logding next Attribute 
        /// </summary>
        /// <param name="currentAttributeTypeCombo"></param>
        private void LoadNextAttributeTypeCombo(Control currentAttributeTypeControl)
        {
            int NextAttributeTypeId = 0;
            string NextAttributeTypeName = string.Empty;

            NextAttributeTypeId = GetNextAttributeTypeId(currentAttributeTypeControl, out NextAttributeTypeName);

            DropDownList NextAttributeCombo = (DropDownList)ControlPlaceHolder.FindControl(sAttributeTypeComboInitial + NextAttributeTypeId.ToString());
            if (NextAttributeCombo != null)
            {
                this.BindNextAttributeTypeCombo(NextAttributeCombo, NextAttributeTypeName);
            }
        }

        /// <summary>
        /// Get Next Attribute
        /// </summary>
        /// <param name="currentAttributeTypeCombo"></param>
        /// <param name="NextAttributeTypeName"></param>
        /// <returns></returns>
        private int GetNextAttributeTypeId(Control currentAttributeTypeControl, out string NextAttributeTypeName)
        {
            int NextAttributeTypeId = 0;
            int currentAttributeTypeDisplayOrder = 0;
            int currentAttributeTypeId = 0;

            NextAttributeTypeName = string.Empty;
            //previouSelectedsAttributeID = 0;
            if (currentAttributeTypeControl != null)
            {
                if (currentAttributeTypeControl is DropDownList)
                {
                    int.TryParse(currentAttributeTypeControl.ID.Replace(sAttributeTypeComboInitial, ""), out currentAttributeTypeId);
                    System.Web.UI.WebControls.DropDownList ddl = currentAttributeTypeControl as System.Web.UI.WebControls.DropDownList;
                    //int.TryParse(ddl.SelectedValue, out previouSelectedsAttributeID);

                }
                else if (currentAttributeTypeControl is System.Web.UI.WebControls.RadioButtonList)
                {
                    System.Web.UI.WebControls.RadioButtonList rBtn = currentAttributeTypeControl as System.Web.UI.WebControls.RadioButtonList;
                    if (rBtn.HasAttributes)
                    {
                        int.TryParse(rBtn.Attributes["data-attrtypeid"], out currentAttributeTypeId);
                        //int.TryParse(rBtn.SelectedValue, out previouSelectedsAttributeID);
                    }

                }
                //currentAttributeTypeDisplayOrder =currentAttributeCombo.SelectedItem[];
                System.Collections.Generic.List<ZNodeAttributeType> AttributeTypeCollection = product.ZNodeAttributeTypeCollection.OfType<ZNodeAttributeType>().AsQueryable().Where(zn => zn.AttributeTypeId == (currentAttributeTypeId)).ToList();

                if (AttributeTypeCollection != null && AttributeTypeCollection.Count > 0)
                {
                    currentAttributeTypeDisplayOrder = AttributeTypeCollection[0].DisplayOrder;

                    System.Collections.Generic.List<ZNodeAttributeType> NextAttributeTypeCollection = product.ZNodeAttributeTypeCollection.OfType<ZNodeAttributeType>().AsQueryable().Where(zn => zn.AttributeTypeId != currentAttributeTypeId && zn.DisplayOrder >= (currentAttributeTypeDisplayOrder)).ToList();

                    if (NextAttributeTypeCollection != null && NextAttributeTypeCollection.Count > 0)
                    {
                        NextAttributeTypeId = NextAttributeTypeCollection[0].AttributeTypeId;
                        NextAttributeTypeName = NextAttributeTypeCollection[0].Name;
                    }
                }
            }
            return NextAttributeTypeId;
        }

        /// <summary>
        ///  Binding next dropdown box of attribute
        /// </summary>
        /// <param name="currentAttributeTypeCombo"></param>
        /// <param name="productId"></param>
        /// <param name="attributeId"></param>
        /// <param name="attributeTypeId"></param>
        /// <param name="AttributeTypeName"></param>
        /// <param name="attributeTypeIds"></param>
        /// <param name="attributeIds"></param>
        private void BindNextAttributeTypeCombo(DropDownList NextAttributeTypeCombo, string AttributeTypeName)
        {
            int productId = product.ProductID;
            string previousSelectedAttributeIDs = string.Empty;
            if (NextAttributeTypeCombo != null)
            {

                int currentAttributeTypeId = 0;

                int.TryParse(NextAttributeTypeCombo.ID.Replace(sAttributeTypeComboInitial, ""), out currentAttributeTypeId);
                if (currentAttributeTypeId > 0)
                {
                    ClearNextAttributeTypeComboValues(currentAttributeTypeId, out previousSelectedAttributeIDs);
                }

                zAttributeHelper attributeHelper = new zAttributeHelper();

                DataSet dsAttributes = attributeHelper.GetNextLevelAttriutes(product.ProductID, previousSelectedAttributeIDs, currentAttributeTypeId);

                if (dsAttributes != null && dsAttributes.Tables.Count > 0 && dsAttributes.Tables[0].Rows.Count > 0)
                {
                    NextAttributeTypeCombo.Items.Clear();

                    /*if (dsAttributes.Tables[0].Rows.Count > 1)
                    {
                        NextAttributeTypeCombo.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = true });
                    }
                    else
                    {
                        IsLastAttributeSelectedIndexChanged = true;
                    }*/

                    NextAttributeTypeCombo.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = true });
                    foreach (DataRow dr in dsAttributes.Tables[0].Rows)
                    {
                        ListItem lstItem = new ListItem();
                        lstItem.Text = Server.HtmlDecode(dr["Name"].ToString());
                        lstItem.Value = dr["AttributeId"].ToString();
                        lstItem.Attributes.Add("alt", Server.HtmlDecode(dr["Name"].ToString()));
                        lstItem.Attributes.Add("title", Server.HtmlDecode(dr["Name"].ToString()));
                        NextAttributeTypeCombo.Items.Add(lstItem);
                        //NextAttributeTypeCombo.Items.Add(new ListItem { Text = Server.HtmlDecode(dr["Name"].ToString()), Value = dr["AttributeId"].ToString()});//OLD Code
                    }
                }
            }
        }

        /// <summary>
        /// Check is Selected Attribute is Last Attribute if Selected Attribute is Last Attribute the no need to call BindAttributeTypeCombo()
        /// for next attribute
        /// </summary>
        /// <param name="currentAttributeTypeControl"></param>
        /// <returns></returns>
        private bool CheckIsLastAttribute(Control currentAttributeTypeControl)
        {
            int currentAttributeTypeDisplayOrder = 0;
            int currentAttributeTypeId = 0;
            bool isLastAttribute = false;

            if (currentAttributeTypeControl != null)
            {
                if (currentAttributeTypeControl is DropDownList)
                {
                    int.TryParse(currentAttributeTypeControl.ID.Replace(sAttributeTypeComboInitial, ""), out currentAttributeTypeId);
                }

                if (currentAttributeTypeControl is RadioButtonList)
                {
                    int.TryParse(currentAttributeTypeControl.ID.Replace(sAttributeTypeComboInitial, ""), out currentAttributeTypeId);
                }

                System.Collections.Generic.List<ZNodeAttributeType> AttributeTypeCollection = product.ZNodeAttributeTypeCollection.OfType<ZNodeAttributeType>().AsQueryable().Where(zn => zn.AttributeTypeId == (currentAttributeTypeId)).ToList();

                if (AttributeTypeCollection != null && AttributeTypeCollection.Count > 0)
                {
                    currentAttributeTypeDisplayOrder = AttributeTypeCollection[0].DisplayOrder;

                    System.Collections.Generic.List<ZNodeAttributeType> NextAttributeTypeCollection = product.ZNodeAttributeTypeCollection.OfType<ZNodeAttributeType>().AsQueryable().Where(zn => zn.AttributeTypeId != currentAttributeTypeId && zn.DisplayOrder >= (currentAttributeTypeDisplayOrder)).ToList();

                    if (NextAttributeTypeCollection == null || (NextAttributeTypeCollection != null && NextAttributeTypeCollection.Count == 0))
                    {
                        isLastAttribute = true;
                    }
                }
            }
            return isLastAttribute;
        }

        /// <summary>
        /// Fusction to load default sku attributes 
        /// </summary>
        private void SelectCurrentSKUAttributes()
        {
            var colorAttributeList = this.product.ZNodeAttributeTypeCollection.OfType<ZNodeAttributeType>().AsQueryable().Where(o => o.Name.Equals("color", StringComparison.OrdinalIgnoreCase)).ToList();
            if (colorAttributeList != null && colorAttributeList.Count > 0)
            {
                System.Web.UI.WebControls.RadioButtonList lstControl = (System.Web.UI.WebControls.RadioButtonList)ControlPlaceHolder.FindControl("lstAttribute" + colorAttributeList[0].AttributeTypeId.ToString());

                LoadNextAttributeTypeCombo(lstControl);
            }
        }

        /// <summary>
        /// Clear Attribute dropdown box
        /// </summary>
        /// <param name="currentAttributeTypeCombo"></param>
        private void ClearNextAttributeTypeComboValues(int currentAttributeTypeId, out string previousSelectedAttributeID)
        {
            System.Collections.Generic.List<ZNodeAttributeType> AttributeTypeCollection = product.ZNodeAttributeTypeCollection.OfType<ZNodeAttributeType>().AsQueryable().Where(zn => zn.AttributeTypeId == (currentAttributeTypeId)).ToList();
            previousSelectedAttributeID = string.Empty;
            if (AttributeTypeCollection != null && AttributeTypeCollection.Count > 0)
            {
                foreach (ZNodeAttributeType AttributeType in product.ZNodeAttributeTypeCollection)
                {
                    if (AttributeType.DisplayOrder >= AttributeTypeCollection[0].DisplayOrder)
                    {
                        DropDownList NextAttributeCombo = ControlPlaceHolder.FindControl(sAttributeTypeComboInitial + AttributeType.AttributeTypeId.ToString()) as DropDownList;
                        if (NextAttributeCombo != null)
                        {
                            NextAttributeCombo.Items.Clear();
                            NextAttributeCombo.Items.Add(new ListItem { Text = "Select", Value = "0", Selected = true });
                        }

                    }
                    else if (AttributeType.DisplayOrder < AttributeTypeCollection[0].DisplayOrder)
                    {
                        if (AttributeType.Name.Equals("color", StringComparison.OrdinalIgnoreCase))
                        {
                            previousSelectedAttributeID += GetSelectedColorAttributeID() + ",";
                        }
                        else
                        {
                            var attributeCombo = (DropDownList)ControlPlaceHolder.FindControl(sAttributeTypeComboInitial + AttributeType.AttributeTypeId.ToString());
                            if (attributeCombo != null)
                            {
                                previousSelectedAttributeID += attributeCombo.SelectedValue + ",";
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set Selected CSS
        /// </summary>
        private void SetSelectedCssForColorAttribute()
        {
            var colorAttributeList = this.product.ZNodeAttributeTypeCollection.OfType<ZNodeAttributeType>().AsQueryable().Where(o => o.Name.Equals("color", StringComparison.OrdinalIgnoreCase)).ToList();
            if (colorAttributeList != null && colorAttributeList.Count > 0)
            {
                System.Web.UI.WebControls.RadioButtonList lstControl = (System.Web.UI.WebControls.RadioButtonList)ControlPlaceHolder.FindControl("lstAttribute" + colorAttributeList[0].AttributeTypeId.ToString());
                if (lstControl != null)
                {
                    foreach (ListItem li in lstControl.Items)
                    {
                        if (li.Selected)
                        {
                            li.Attributes.Add("class", "item ItemColor Selected");
                            break;
                        }
                    }
                }
            }
        }

        //private string GetPreviousSelectedAttributeIDs(int nextAttributeTypeID)
        //{
        //    string previousSelectedAttributeID = string.Empty;

        //    foreach (ZNodeAttributeType attributeType in _product.ZNodeAttributeTypeCollection)
        //    {
        //        if (nextAttributeTypeID == attributeType.AttributeTypeId)
        //        {
        //            break;
        //        }
        //        else
        //        {
        //            if (attributeType.Name.Equals("color", StringComparison.OrdinalIgnoreCase))
        //            {
        //                previousSelectedAttributeID += GetSelectedColorAttributeID() + ",";
        //            }
        //            else
        //            {
        //                var attributeCombo = (DropDownList)ControlPlaceHolder.FindControl(_sAttributeTypeComboInitial + attributeType.AttributeTypeId.ToString());
        //                if (attributeCombo != null)
        //                {
        //                    previousSelectedAttributeID += attributeCombo.SelectedValue + ",";
        //                }
        //            }
        //        }
        //    }

        //    return previousSelectedAttributeID;
        //}

        /// <summary>
        /// Get Selected Color by AttributeID
        /// </summary>
        /// <returns></returns>
        private int GetSelectedColorAttributeID()
        {
            int selectedColorAttributeID = 0;
            var colorAttributeList = this.product.ZNodeAttributeTypeCollection.OfType<ZNodeAttributeType>().AsQueryable().Where(o => o.Name.Equals("color", StringComparison.OrdinalIgnoreCase)).ToList();
            if (colorAttributeList != null && colorAttributeList.Count > 0)
            {
                System.Web.UI.WebControls.RadioButtonList lstControl = (System.Web.UI.WebControls.RadioButtonList)ControlPlaceHolder.FindControl("lstAttribute" + colorAttributeList[0].AttributeTypeId.ToString());
                foreach (ListItem li in lstControl.Items)
                {
                    if (li.Selected)
                    {
                        int.TryParse(li.Value, out selectedColorAttributeID);
                    }
                }
            }

            return selectedColorAttributeID;
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();

            script.Append("var zeonProductColorSlider = new ZeonSlideProducts({");
            //script.Append("'sliderID':'divRadioList',");
            //script.Append("'MaxItemCount':'6',");
            //script.Append("'MobPortraitItemCount':'4',");
            //script.Append("'MobLandItemCount':'5',");
            //script.Append("'TabletItemCount':'5',");
            //script.Append("'MinimumItemsShown':'2',");
            script.Append("});");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "zeonProductColorSliderScript", script.ToString(), true);
        }

        /// <summary>
        /// get navigation link for downloads
        /// </summary>
        /// <returns></returns>
        private string GetNavigationLink()
        {
            string outputLink = string.Empty;
            if (this.product.DownloadLink.IndexOf("http") < 0 && this.product.DownloadLink.IndexOf("https") < 0)
            {
                string pdfPath = string.Empty;
                ////File has only name then serach on ProductPDF Folder
                //pdfPath = !this.product.DownloadLink.StartsWith("/") ? Resources.CommonCaption.SizeChartLocation + this.product.DownloadLink : this.product.DownloadLink;
                //if (File.Exists(Server.MapPath(pdfPath)))
                //{
                //    outputLink = pdfPath;
                //}
                string domainPath = string.Format("{0}://{1}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Authority);
                pdfPath = !this.product.DownloadLink.StartsWith("/") ? "/" + this.product.DownloadLink : this.product.DownloadLink;
                pdfPath = domainPath + pdfPath;
                outputLink = IsValidURL(pdfPath, false);

            }
            else
            {
                outputLink = IsValidURL(this.product.DownloadLink, true);
            }
            return outputLink;
        }

        private string IsValidURL(string pdfPath, bool isFullUrl)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest
                                       .Create(pdfPath);
            webRequest.AllowAutoRedirect = false;
            try
            {
                using (var response = webRequest.GetResponse() as HttpWebResponse)
                {
                    if (response != null)
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            if (!isFullUrl)
                                return !this.product.DownloadLink.StartsWith("/") ? "/" + this.product.DownloadLink : this.product.DownloadLink;
                            else
                                return pdfPath;
                        }
                        response.Close();
                    }
                }
                return string.Empty;
            }
            catch (WebException e)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// set Size chart link on page
        /// </summary>
        /// <returns>string</returns>
        private string GetSizeChartLinkHTML()
        {
            string sizeChartHTML = string.Empty;
            string downloadLink = GetNavigationLink();
            if (!string.IsNullOrEmpty(this.product.DownloadLink) && !string.IsNullOrEmpty(downloadLink))
            {
                StringBuilder strSizeLink = new StringBuilder();
                strSizeLink.Append(" <a href='" + downloadLink + "' class='szchartClass' id='lnkSizeChart' onclick='" + GetOnClickFunction(downloadLink) + "'>");
                strSizeLink.Append(Resources.CommonCaption.PDPSizeChart);
                strSizeLink.Append("</a>");
                sizeChartHTML = strSizeLink.ToString();
            }
            return sizeChartHTML;
        }

        /// <summary>
        /// create Size chart function call based on QuicView
        /// </summary>
        /// <returns>string</returns>
        private string GetOnClickFunction(string link)
        {
            string functioncall = "return objProduct.ShowSizeChartLink()";
            if (IsQuickWatch)
            {
                functioncall = "return window.parent.quickViewHelper.ShowSizeChartLink(this)";
            }
            return functioncall;
        }

        #endregion

        #endregion
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZGroupProductAttributes.ascx.cs" Inherits="WebApp.Controls.Default.Product.ZGroupProductAttributes" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="Znode" %>
<div class="ProductAttributes GroupProductAttributes">
    <div class="ButtonArea">
        <asp:Literal ID="ltrTopCallForPrice" runat="server" Visible="false"></asp:Literal>
        <a href="javascript:void(0)" onclick="attributehelper.AddToCart()" class="btn btn-danger" runat="server" id="lnkTopGrpAddToCart">
            <asp:Label ID="lblAddToCartTop" meta:resourcekey="lblAddToBagResource" runat="server"></asp:Label></a>
        &nbsp;
        <a href="javascript:void(0)" onclick="attributehelper.CreateClone()" class="btn btn-success">
            <asp:Label ID="lblAddToRowTop" meta:resourcekey="lblAddNewRowResource" runat="server"></asp:Label>
        </a>
        <%-- &nbsp;
    <a href="javascript:void(0)" onclick="attributehelper.ResetAll()" class="btn btn-info">Reset All</a>--%>
    </div>
    <div id="GroupAttributeConatiner" class="table-responsive">
        <asp:Panel ID="pnlOptions" runat="server" Visible="true">
            <asp:Literal ID="ltlProductAttribute" runat="server"></asp:Literal>
        </asp:Panel>
    </div>
    <div class="ButtonArea">
        <asp:Literal ID="ltrBotmCallForPrice" runat="server" Visible="false"></asp:Literal>
        <a href="javascript:void(0)" onclick="attributehelper.AddToCart()" class="btn btn-danger" runat="server" id="lnkBtmGrpAddToCart">
            <asp:Label ID="lblAddToBagBottom" meta:resourcekey="lblAddToBagResource" runat="server"></asp:Label></a>
        &nbsp;
        <a href="javascript:void(0)" onclick="attributehelper.CreateClone()" class="btn btn-success">
            <asp:Label ID="lblAddtoRowBottom" meta:resourcekey="lblAddNewRowResource" runat="server"></asp:Label></a>

        <%--  &nbsp;
    <a href="javascript:void(0)" onclick="attributehelper.ResetAll()" class="btn btn-info">Reset All</a>--%>
    </div>
</div>
<script type="text/javascript">
    var productID = '<%= ProductID  %>';
</script>

<div class="ErrorMessage" style="display: none;" id="divGroupProductAttrErrorBox">
    <asp:Panel ID="pnlErrorMessage" runat="server" ClientIDMode="Static">
        <asp:Literal ID="ltrErrorMessage" runat="server" meta:resourceKey="ltrErrorMessage"></asp:Literal>
    </asp:Panel>
</div>
<div id="divNameDescripion"  style="display:none">
   <Znode:CustomMessage ID="ucNameHelp" runat="server"  MessageKey="ProductNameHelpMessage" EnableViewState="false"/>
</div>

<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Product_HighlightInfo" Codebehind="HighlightInfo.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<div class="HighlightInfo">
    <h1><asp:Label ID="lblTitle" runat="server" meta:resourcekey="lblTitleResource1" ClientIDMode="Static"></asp:Label> </h1>
    <div class="Description">
        <asp:Label ID="HighlightDescription" runat="server" CssClass="Description" 
            meta:resourcekey="HighlightDescriptionResource1" ClientIDMode="Static"></asp:Label>
    </div>    
    <ZNode:Spacer ID="Spacer2" spacerheight="20" spacerwidth="1" runat="server" ClientIDMode="Static" />
    <div class="DetailLink"><asp:HyperLink id="back" runat="Server" 
            meta:resourcekey="backResource1" ClientIDMode="Static"></asp:HyperLink></div>
    <ZNode:Spacer ID="Spacer1" spacerheight="20" spacerwidth="1" runat="server" ClientIDMode="Static"/>
</div> 
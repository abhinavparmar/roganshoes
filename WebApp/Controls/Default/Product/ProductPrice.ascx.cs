using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Product Price user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductTitle : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _product;
        private int _Quantity = 1;
        private ZNodeProfile _Profile = new ZNodeProfile();
        //Zeon Custom Code:Starts
        private bool _isProductPage = false;
        private bool _IsQuickWatch = false;
        //Zeon Custom Code:Ends
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Znodeproduct object
        /// </summary>
        public ZNodeProduct Product
        {
            get
            {
                return this._product;
            }

            set
            {
                this._product = value;
            }
        }

        /// <summary>
        /// Gets or sets the quantity value
        /// </summary>
        public int Quantity
        {
            get
            {
                return this._Quantity;
            }

            set
            {
                this._Quantity = value;
            }
        }

        /// <summary>
        /// Gets or sets the product price.
        /// </summary>
        public decimal ProductPrice
        {
            get
            {
                if (ViewState["ProductPrice"] != null)
                {
                    return Convert.ToDecimal(ViewState["ProductPrice"]);
                }

                return 0;
            }

            set
            {
                ViewState["ProductPrice"] = value;
            }
        }

        #region Zeon Custom Properties

        /// <summary>
        /// get or set value of _isProductPage
        /// </summary>
        public bool IsProductPage
        {
            get { return _isProductPage; }
            set { _isProductPage = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether QuickWatch is enabled or not
        /// </summary>
        public bool IsQuickWatch
        {
            get
            {
                return this._IsQuickWatch;
            }

            set
            {
                this._IsQuickWatch = value;
            }
        }
        #endregion

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (Visible)
            {
                if (this._product.ZNodeAttributeTypeCollection.Count == 0)
                {
                    ZNodeSKU SKU = ZNodeSKU.CreateByProductDefault(this._product.ProductID);
                    this._product.SelectedSKU = SKU;
                }

                if (this._product.CallForPricing)
                {
                    StringBuilder stringBuilderPrice = new StringBuilder();
                    MessageConfigAdmin mconfig = new MessageConfigAdmin();
                    stringBuilderPrice.Append("<span class=CallForPriceMsg>");

                    if (this._product.CallMessage != null && !string.IsNullOrEmpty(this._product.CallMessage.ToString()))
                    {
                        stringBuilderPrice.Append(this._product.CallMessage.ToString());
                    }
                    else
                    {
                        stringBuilderPrice.Append(mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43));
                    }


                    stringBuilderPrice.Append("</span>");

                    return;
                }

                ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                item.Product = new ZNodeProductBase(this._product);
                item.Quantity = this._Quantity;

                decimal price = item.ExtendedPrice;
                
                decimal unitPrice = this._product.RetailPrice + this._product.SelectedAddOnItems.TotalAddOnCost;
                //prft custom code:268 Start
                if (this._product.SalePrice > 0)
                {
                    unitPrice = Convert.ToDecimal(this._product.SalePrice + this._product.SelectedAddOnItems.TotalAddOnCost);
                }
                //prft custom code:END
                //Zeon Custom code:start
                if (this._product != null && this._product.TieredPrice > 0)
                {
                    unitPrice = this._product.TieredPrice + this._product.SelectedAddOnItems.TotalAddOnCost;
                }
                //Zeon Custom code:End

                for (int idx = 0; idx < this._product.ZNodeBundleProductCollection.Count; idx++)
                {
                    unitPrice += this._product.ZNodeBundleProductCollection[idx].AddOnPrice;
                }

                decimal retailprice = unitPrice * this._Quantity;

                this.ProductPrice = retailprice;

                if (this._product.SalePrice.HasValue || (this._product.WholesalePrice.HasValue && this._Profile.UseWholesalePricing))
                {
                    //Zeon Custom Code>>Start >>Show price strike out only if retail price is not equals sale price

                    Price.Text = ZNodePricingFormatter.ConfigureProductPricing(retailprice, price);
                    if (retailprice != price)
                    {
                        Price.Text = ZNodePricingFormatter.ConfigureProductPricing(retailprice, price);
                        //PRFT Custom Code:Stars
                        //Price.Text = Price.Text + FormatPriceDiscount(retailprice, price);
                        //PRFT Custom Code:Ends
                    }
                    else
                    {
                        if (this._product.IsMapPrice)
                        {
                            Price.Text = ZNodePricingFormatter.ConfigureProductPricing(price);
                        }
                        else
                        {
                            Price.Text = ZNodePricingFormatter.ConfigureProductPricingRed(price);
                        }

                    }
                    //Zeon Custom Code>>End >>Show price strike out only if retail price is not equals sale price
                    //Price.Text = ZNodePricingFormatter.ConfigureProductPricing(price);
                }
                else if (this._product.IsCustomPromotionApplied) //Zeon Customization :: Start
                {
                    //Show Promotional Product Price with Orginal Price 
                    Price.Text = ZNodePricingFormatter.ConfigureProductPricing(retailprice, price);
                    //PRFT Custom Code:Stars
                    //Price.Text = Price.Text + FormatPriceDiscount(retailprice, price);
                    //PRFT Custom Code:Ends
                }
                //Zeon Customization :: End
                else
                {
                   // Price.Text = ZNodePricingFormatter.ConfigureProductPricing(price);
                    if (this._product.IsMapPrice)
                    {
                        Price.Text = ZNodePricingFormatter.ConfigureProductPricing(price);
                    }
                    else
                    {
                        Price.Text = ZNodePricingFormatter.ConfigureProductPricingRed(price);
                    }
                }
                //Formatting Pricing
                //Zeon Custom Code to add additional tags :Starts
                if (IsProductPage)
                {
                    Price.Text = AddCustomTagsInPrice(Price.Text);
                }
                bool isCustomPortal = ZCommonHelper.IsCustomMulitipleAttributePortal();
                if (isCustomPortal)
                {
                    TeamPrice.Text = GetTeamPriceHTML();
                    TeamPrice.Visible = true;
                }
                else
                {
                    TeamPrice.Visible = false;
                }
                //Zeon Custom Code to add additional tags :Ends
                item.Product = null;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            if (!Page.IsPostBack)
            {
                this.Bind();

            }
            //Zeon Custom Code
            if (IsQuickWatch) { RegisterClientScriptClasses(); }

        }
        #endregion

        #region Zeon Custom Methods

        /// <summary>
        /// Format Formatted Price with an extra meta tag and additional info
        /// </summary>
        /// <param name="formattedPrice">string</param>
        /// <returns>string</returns>
        private string AddCustomTagsInPrice(string formattedPrice)
        {
            string formattedText = string.Empty;
            string priceMetaTag = string.Format(this.GetLocalResourceObject("CureencyMetaTag").ToString(), System.Globalization.RegionInfo.CurrentRegion.ISOCurrencySymbol);
            if (formattedPrice.Trim().IndexOf("<span class=SalePrice>$") > -1)
            {
                formattedPrice = formattedPrice.Replace("<span class=SalePrice>$", "<span class=SaleDollar>$</span><span class=SalePrice>");
            }
            if (formattedPrice.Trim().IndexOf("<span class=Price>$") > -1)
            {
                formattedPrice = formattedPrice.Replace("<span class=Price>$", "<span class=PrcDollar>$</span><span class=Price>");
            }
            if (formattedPrice.Trim().IndexOf("<span class=RedPrice>$") > -1)
            {
                formattedPrice = formattedPrice.Replace("<span class=RedPrice>$", "<span class=PrcDollarRed>$</span><span class=RedPrice>");
            }
            formattedText = formattedPrice.IndexOf("SalePrice") > 0 ? formattedPrice.Replace("SalePrice", string.Format(this.GetLocalResourceObject("PriceAttribute").ToString(), "SalePrice")) : formattedPrice.Replace("Price", string.Format(this.GetLocalResourceObject("PriceAttribute").ToString(), "Price"));
            formattedText = formattedText + priceMetaTag;

            return formattedText;
        }

        /// <summary>
        /// Get Team Price HTML
        /// </summary>
        /// <returns></returns>
        public string GetTeamPriceHTML()
        {
            string html = string.Empty;
            string priceHTML = string.Empty;
            if (this._product != null && this._product.ZNodeTieredPriceCollection != null && this._product.ZNodeTieredPriceCollection.Count > 0)
            {
                string priceText = string.Empty;
                decimal price = 0;
                ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption pricing = new ZNode.Libraries.ECommerce.Promotions.ZNodePromotionPricingOption();
                price = pricing.PromotionalPrice(this.Product, this._product.ZNodeTieredPriceCollection[0].Price);
                if (this.Product.IsCustomPromotionApplied)
                {
                    priceHTML = ZNodePricingFormatter.ConfigureProductPricing(this._product.ZNodeTieredPriceCollection[0].Price, price);
                    html = IsQuickWatch ? string.Format(this.GetLocalResourceObject("QuickViewSaleTeamPriceHTML").ToString(), priceHTML) : string.Format(this.GetLocalResourceObject("ProductSaleTeamPriceHTML").ToString(), priceHTML);
                }
                else
                {
                    //priceHTML = ZNodePricingFormatter.ConfigureProductPricing(this._product.ZNodeTieredPriceCollection[0].Price);
                    if (this._product.IsMapPrice)
                    {
                        priceHTML = ZNodePricingFormatter.ConfigureProductPricing(this._product.ZNodeTieredPriceCollection[0].Price);
                    }
                    else
                    {
                        priceHTML = ZNodePricingFormatter.ConfigureProductPricingRed(this._product.ZNodeTieredPriceCollection[0].Price);
                    }
                    html = IsQuickWatch ? string.Format(this.GetLocalResourceObject("QuickViewTeamPriceHTML").ToString(), priceHTML) : string.Format(this.GetLocalResourceObject("ProductTeamPriceHTML").ToString(), priceHTML);
                }

            }
            return html;
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            script.Append("var productPrice = new ProductPrice({");
            script.Append("\"Controls\":{");
            script.Append("}});productPrice.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "productPrice", script.ToString(), true);
        }

        /// <summary>
        /// Calculate discount from origional price to discount Price
        /// </summary>
        /// <param name="descPrice"></param>
        /// <param name="oriPrice"></param>
        /// <returns></returns>
        private decimal GetFormattedDiscountValue(decimal oriPrice, decimal descPrice)
        {
            decimal discountVal = 0;
            if (oriPrice > 0 && descPrice > 0)
            {
                discountVal = (oriPrice - descPrice) * 100 / oriPrice;
                discountVal = decimal.Round(discountVal, 0);
            }
            return discountVal;
        }

        /// <summary>
        /// Format Discount Text
        /// </summary>
        /// <param name="retailprice"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        private string FormatPriceDiscount(decimal retailprice, decimal price)
        {
            string discPriceText = string.Empty;
            string discText = string.Empty;
            decimal discount = GetFormattedDiscountValue(retailprice, price);
            if (discount > 0)
            {
                if (ConfigurationManager.AppSettings["CheerAndPomPortalID"] != null && !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString()))
                {
                    if (ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString().Equals(ZNodeConfigManager.SiteConfig.PortalID.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        discText = HttpContext.GetGlobalResourceObject("CommonCaption", "CheerProductDiscountText") == null ? "<span class='ProductDiscount'>Save {0}%</span>" : HttpContext.GetGlobalResourceObject("CommonCaption", "CheerProductDiscountText").ToString();
                    }
                    else
                    {
                        discText = HttpContext.GetGlobalResourceObject("CommonCaption", "RogansProductDiscountText") == null ? "<span class='ProductDiscount'>Save {0}%</span>" : HttpContext.GetGlobalResourceObject("CommonCaption", "RogansProductDiscountText").ToString();

                    }
                    discPriceText = string.Format(discText, discount);
                }
            }
            return discPriceText;
        }

        #endregion
    }
}
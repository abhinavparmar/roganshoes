using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using System.Linq;
using System.Text;
using ZNode.Libraries.Admin;


/// <summary>
/// Displays the related products
/// </summary>
namespace WebApp
{
    /// <summary>
    /// Represents the ProductRelated user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductRelated : System.Web.UI.UserControl
    {
        #region Private Variables
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/images/buynow.gif";
        private ZNodeProfile _SpecialProfile = new ZNodeProfile();
        private ZNodeProductBase _product;
        private ZNodeProfile _ProductProfile = new ZNodeProfile();
        private bool _HasItems = false;
        private bool _ShowDescription = false;
        private bool _ShowName = false;
        private bool _ShowImage = false;
        private bool _ShowPrice = false;
        private bool _IsQuickWatch = false;
        private ZNodeImage znodeImage = new ZNodeImage();
        protected ProductHelper.ProductCrossSellTypeEnum _ProductCrossSellType = ProductHelper.ProductCrossSellTypeEnum.RelatedProducts;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the profile.
        /// </summary>
        public ZNodeProfile SpecialProfile
        {
            get { return this._SpecialProfile; }
            set { this._SpecialProfile = value; }
        }

        public ZNodeProfile ProductProfile
        {
            get { return this._ProductProfile; }
            set { this._ProductProfile = value; }
        }
        /// <summary>
        /// Gets or sets the catalog item object. The control will bind to the data from this object
        /// </summary>
        public ZNodeProductBase Product
        {
            get
            {
                return this._product;
            }

            set
            {
                this._product = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether Product has related items or not
        /// </summary>
        public bool HasItems
        {
            get
            {
                return this._HasItems;
            }

            set
            {
                this._HasItems = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Name or not
        /// </summary>
        public bool ShowName
        {
            get
            {
                return this._ShowName;
            }

            set
            {
                this._ShowName = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Price
        /// </summary>
        public bool ShowPrice
        {
            get
            {
                return this._ShowPrice;
            }

            set
            {
                this._ShowPrice = value;
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether to Show Image
        /// </summary>
        public bool ShowImage
        {
            get
            {
                return this._ShowImage;
            }

            set
            {
                this._ShowImage = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Description
        /// </summary>
        public bool ShowDescription
        {
            get
            {
                return this._ShowDescription;
            }

            set
            {
                this._ShowDescription = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether QuickWatch is enabled or not
        /// </summary>
        public bool IsQuickwatch
        {
            get
            {
                return this._IsQuickWatch;
            }

            set
            {
                this._IsQuickWatch = value;
            }
        }

        //Zeon Custom Code: Start
        public ProductHelper.ProductCrossSellTypeEnum ProductCrossSellType
        {
            get
            {
                return _ProductCrossSellType;
            }

            set
            {
                _ProductCrossSellType = value;
            }
        }
        //Zeon Custom Code: End
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindCrossSellProductTitle();

            Bind();
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        #endregion

        #region Zeon Custom Code

        /// <summary>
        /// Bind the title of the CrossSell Products    
        /// </summary>
        private void BindCrossSellProductTitle()
        {
            //Zeon custom code to change control title
            Product = (ZNodeProduct)HttpContext.Current.Items["Product"];
            string sQuickProductTitle = string.Empty;

            if (_ProductCrossSellType == ProductHelper.ProductCrossSellTypeEnum.CrossSells)
            {
                //sQuickProductTitle = "Cross Sells";
                sQuickProductTitle = "You May Also Like";
            }
            else if (_ProductCrossSellType == ProductHelper.ProductCrossSellTypeEnum.RelatedProducts)
            {
                sQuickProductTitle = "Related Products";
            }
            else if (_ProductCrossSellType == ProductHelper.ProductCrossSellTypeEnum.UpSells)
            {
                sQuickProductTitle = "Up Sells/ Recommended Products";
            }

            lblQuickProductTitle.Text = sQuickProductTitle;

        }
        #endregion

        #region Bind Data

        /// <summary>
        /// Get ImagePath
        /// </summary>
        /// <param name="imageFile">Image File Name</param>
        /// <returns>Returns the Image Path</returns>
        public string GetImagePath(string imageFile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathSmall(imageFile);
        } 

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (this.Product.ZNodeCrossSellItemCollection.Count > 0)
            {
                //  pnlRelated.Visible = true;
                if (!Page.IsPostBack)
                {
                    this._HasItems = true;
                    //Zeon Custom Code: Start
                    //DataListRelated.DataSource = this.Product.ZNodeCrossSellItemCollection;
                    DataListRelated.DataSource = this.Product.ZNodeCrossSellItemCollection.OfType<ZNodeCrossSellItem>().AsQueryable().Where(zn => zn.CrossSellTypeID == ((int)_ProductCrossSellType)).ToList();
                    DataListRelated.DataBind();
                    //Zeon Custom Code: End

                    // Disable view state for data list item`
                    //foreach (DataListItem item in DataListRelated.Items)
                    //{
                    //    item.EnableViewState = false;
                    //}

                    if (DataListRelated.Items.Count == 0)
                    {
                        pnlRelated.Visible = false;
                    }
                }
            }
            else
            {
                pnlRelated.Visible = false;
            }
        }

        #endregion

        #region Helper Functions

        /// <summary>
        /// Represents the GetProductUrl method
        /// </summary>
        /// <param name="productId">Product Id value</param>
        /// <returns>Returns the Product Url</returns>
        protected string GetProductUrl(string productId)
        {
            ZNodeUrl url = new ZNodeUrl();
            string PagePath = "~/product.aspx?zpid=" + productId;
            return PagePath;
        }

        /// <summary>
        ///  Represents the GetViewProductImageUrl method
        /// </summary>
        /// <param name="link">Represents the Link Value</param>
        /// <returns>Returns the Path of Product Page Url</returns>
        protected string GetViewProductPageUrl(string link)
        {
            if (this._IsQuickWatch)
            {
                return "javascript:self.parent.location ='" + ResolveUrl(link) + "';";
            }

            return link;
        }

        /// <summary>
        /// Returns properties value for PlugIn Control
        /// </summary>
        /// <param name="reviewCollection">Review Collection List</param>
        /// <param name="viewProductLink">View Product Link</param>
        /// <returns>Returns the ReviewCollection hashtable list</returns>
        protected Hashtable GetParameterList(object reviewCollection, object viewProductLink)
        {
            // Create Instance for hashtable
            Hashtable collection = new Hashtable();

            // Add property name and value to it
            collection.Add("ViewProductLink", viewProductLink);
            collection.Add("ReviewCollection", reviewCollection);

            return collection;
        }


        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();
            string objName = "zeonSlideProducts" + ProductCrossSellType;

            script.Append("var " + objName + " = new ZeonSlideProducts({");
            script.Append("'sliderID':'" + relProdProduct.ClientID + "',");
            script.Append("'MaxItemCount':'5',");
            script.Append("'MobPortraitItemCount':'3',");
            script.Append("'MobLandItemCount':'3',");
            script.Append("'TabletItemCount':'3',");
            script.Append("'MinimumItemsShown':'2',");
            script.Append("});" + objName + ".Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "Slider_" + ProductCrossSellType, script.ToString(), true);
        }

        #endregion

        #region General Events

        /// <summary>
        /// Event is raised when Buy button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Buy_Click(object sender, ImageClickEventArgs e)
        {
            string link = "~/product.aspx?zpid=";

            // Getting ProductID from the image button
            ImageButton but_buy = sender as ImageButton;
            int zpid = int.Parse(but_buy.CommandArgument);

            Response.Redirect(link + zpid + "&action=addtocart");
        }

        #endregion

        #region Zeon Custom Methods
        /// <summary>
        ///  Check for Call for Pricing value
        /// </summary>
        /// <param name="fieldValue">The Field Value</param>
        /// <returns>Returns the Hahstable Collection</returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());

            if (Status && callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
            {
                return callMessage.ToString();
            }

            if (Status)
            {
                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else if (!this._ProductProfile.ShowPrice)
            {
                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion
    }
}
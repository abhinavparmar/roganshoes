<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="WebApp.Controls_Default_Product_ProductAdditionalImages" Codebehind="ProductAdditionalImages.ascx.cs" %>
<div style="text-align: left;">
        <asp:DataList ID="DataListAlternateImages" runat="server" 
            RepeatDirection="Horizontal" 
            meta:resourcekey="DataListAlternateImagesResource1">
        <ItemTemplate>
            <asp:Panel ID="pnlAlternateImage" runat="server" 
                meta:resourcekey="pnlAlternateImageResource1">
                <div class="ProductAdditionalImages">
                    <div>
                        <div style="cursor: pointer;">
                            <img onclick='javascript:document.getElementById(&#039;&#039;).src = (&#039;<%# GetThumbnailImagePath(Eval("ImageFile").ToString()) %>&#039;);
                                javascript:document.getElementById(&#039;Hidden1&#039;).value =(&#039;<%# Eval("ImageFile") %>&#039;);
                                javascript:document.getElementById(&#039;Hidden2&#039;).value =(&#039;<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>&#039;);'
                                src='<%# GetThumbnailImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()) %>'
                                alt='<%# GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag").ToString(),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                                
                                title='<%# GetImageName(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>' />
                        </div>
                        <div class="Name">
                            <asp:Label ID="Name" runat="server" Visible="False" 
                                Text='<%# GetImageName(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>' 
                                meta:resourcekey="NameResource1"></asp:Label>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ItemTemplate>
        <ItemStyle CssClass="ItemStyle" />
    </asp:DataList>
    <asp:DataList ID="DataListSwatches" runat="server" RepeatDirection="Horizontal" 
            meta:resourcekey="DataListSwatchesResource1">
    <ItemTemplate>
        <asp:Panel ID="pnlSwatches" runat="server" 
            meta:resourcekey="pnlSwatchesResource1">
            <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtCOLORS"></asp:Localize>:
                <a href="javascript:openwindow()">
                    <img border="0" class="SwatchImage" onclick='javascript:document.getElementById(&#039;Hidden1&#039;).value =(&#039;<%# GetProductSwatchImage( Eval("ImageFile").ToString(), Eval("AlternateThumbnailImageFile")) %>&#039;);
                               javascript:document.getElementById(&#039;Hidden2&#039;).value =(&#039;<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>&#039;);'
                        src='<%# GetSmallThumbnailImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString().Replace(".","-swatch.")) %>'
                        alt='<%# GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag").ToString(),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                        title='<%# GetImageName(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                        
                visible='<%# DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage") %>' />
                </a>
            </asp:Panel>
        </ItemTemplate>
        <ItemStyle CssClass="ItemStyle" />
    </asp:DataList></div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZProductListCompare.ascx.cs" Inherits="WebApp.Controls.Default.Product.ZProductListCompare" %>
<div class="CompareSelectedProduct">
    <h3>
        <asp:Literal ID="ltrCompareTitle" runat="server" Text="Compare Products" EnableViewState="false"></asp:Literal></h3>
    <asp:Repeater ID="rptlistPageCompare" runat="server" ClientIDMode="Static" EnableViewState="false">
        <ItemTemplate>
            <ul id="compare_<%# DataBinder.Eval(Container.DataItem, "ProductID") %>">
                <li>
                    <div class="ThumbImg">
                        <a href='<%#ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'>
                            <img id="imgProduct" border="0" src='<%# ResolveUrl(GetImageURL(DataBinder.Eval(Container.DataItem, "ImageFile"))) %>'
                                alt='<%# DataBinder.Eval(Container.DataItem, "ProductNum") %>' title='<%# DataBinder.Eval(Container.DataItem, "ProductNum") %>'>
                        </a>
                    </div>
                    <div class="ProductName"><a href='<%#ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'><%#DataBinder.Eval(Container.DataItem, "Name") %></a></div>
                    <div>
                        <a onclick='zeonCatProdList.RemoveProductFromComparsion(<%# DataBinder.Eval(Container.DataItem, "ProductID") %>)' href="javascript:void(0)" class="fa fa-times-circle"></a>
                    </div>
                    <asp:HiddenField ID="hdnProdID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>' ClientIDMode="Static" />
                </li>
            </ul>
        </ItemTemplate>
    </asp:Repeater>
    <div class="Button">
        <a href="productcomparison.aspx" class="CompareBtn">
            <span>Compare<span class="fa fa-refresh"></span></span>
        </a>
        <a href="javascript:void(0)" onclick="zeonCatProdList.ClearProductFromComparsion()">
            Clear all
        </a>
    </div>
    
</div>

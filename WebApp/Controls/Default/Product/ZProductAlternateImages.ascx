﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZProductAlternateImages.ascx.cs" Inherits="WebApp.Controls.Default.Product.ZProductAlternateImages" %>
<div id="relAlternateImageList" runat="server" class="AlternateProductImages">
    <asp:Repeater ID="RepeaterAlternateImage" runat="server" OnItemDataBound="RepeaterAlternateImage_ItemDataBound"
        EnableViewState="false">
        <HeaderTemplate >
            <div id="gallery_01" class="ItemImg">
        </HeaderTemplate>
        <ItemTemplate>
            <div class="item">
                <a href="#" class="active" data-image="<%#ResolveUrl(GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()).Replace("/55/","/385/")) %>"
                    data-zoom-image="<%#ResolveUrl(GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()).Replace("/55/","/Original/")) %>">
                    <img id="imgSwatch" runat="server" alt='<%# "Swatch Image " + GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag"),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                        class="SwatchImage" src='<%#ResolveUrl(GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())) %>'
                        title='<%# GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag"),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                        visible="false" clientidmode="Static" />
                    <img src="<%#ResolveUrl(GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())) %>"
                        alt='<%# "Alternate Image "+ GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag"),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                        title='<%# GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag"),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>' />
                </a>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </div>
        </FooterTemplate>
    </asp:Repeater>
</div>

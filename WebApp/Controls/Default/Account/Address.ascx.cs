using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Address user control class.
    /// </summary>
    public partial class Controls_Default_Account_Address : System.Web.UI.UserControl
    {
        private ZNodeUserAccount _userAccount;
        private Address _shippingAddress = new Address();
        private int itemId = 0;

        /// <summary>
        /// This event raised on clicking the button btnUpdate
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnUpdate_Click(object sender, EventArgs e)
        {
            // Validate billing state code
            if ((lstBillingCountryCode.SelectedValue == "US") && (!string.IsNullOrEmpty(txtBillingState.Text.Trim())) && txtBillingState.Visible)
            {
                if (txtBillingState.Text.Trim().Length > 2 || txtBillingState.Text.Trim().Length < 2)
                {
                    lblError.Text = Resources.CommonCaption.ValidateStateCode;
                    return;
                }
            }

            this._userAccount = (ZNodeUserAccount)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.UserAccount);

            Address address = new Address();
            AddressService addressService = new AddressService();
            if (this.itemId > 0)
            {
                address = addressService.GetByAddressID(this.itemId);
            }

            if (!this.HasDefaultAddress(this._userAccount.AccountID, true))
            {
                lblError.Text = this.GetLocalResourceObject("DefaultBillingAddressRequired").ToString();
                return;
            }

            if (!this.HasDefaultAddress(this._userAccount.AccountID, false))
            {
                lblError.Text = this.GetLocalResourceObject("DefaultShippingAddressRequired").ToString();
                return;
            }

            address.AddressID = this.itemId;
            address.AccountID = this._userAccount.AccountID;
            address.Name = Server.HtmlEncode(txtAddressName.Text);
            address.FirstName = Server.HtmlEncode(txtBillingFirstName.Text);
            address.LastName = Server.HtmlEncode(txtBillingLastName.Text);
            address.CompanyName = Server.HtmlEncode(txtBillingCompanyName.Text);
            address.Street = Server.HtmlEncode(txtBillingStreet1.Text);
            address.Street1 = Server.HtmlEncode(txtBillingStreet2.Text);
            address.City = Server.HtmlEncode(txtBillingCity.Text);
            //address.StateCode = txtBillingState.Text.ToUpper();
            address.PostalCode = txtBillingPostalCode.Text;
            address.CountryCode = lstBillingCountryCode.SelectedValue;
            if (lstBillingCountryCode.Visible && ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(lstBillingCountryCode.SelectedValue))
            {
                address.StateCode = lstBillingStateCode.SelectedValue.IndexOf("AE") >= 0 ? "AE" : lstBillingStateCode.SelectedValue;
            }
            else
            {
                address.StateCode = txtBillingState.Text.ToUpper();
            }
            address.PhoneNumber = txtBillingPhoneNumber.Text;
            address.IsDefaultBilling = chkUseAsDefaultBillingAddress.Checked;
            address.IsDefaultShipping = chkUseAsDefaultShippingAddress.Checked;
            TList<Address> addressList = addressService.GetByAccountID(this._userAccount.AccountID);
            if (chkUseAsDefaultBillingAddress.Checked)
            {
                // Clear all previous default billing address flag.
                foreach (Address currentItem in addressList)
                {
                    currentItem.IsDefaultBilling = false;
                }

                addressService.Update(addressList);
            }

            if (chkUseAsDefaultShippingAddress.Checked)
            {
                // Clear all previous default billing address flag.
                foreach (Address currentItem in addressList)
                {
                    currentItem.IsDefaultShipping = false;
                }

                addressService.Update(addressList);
            }

            bool isSuccess = false;
            if (this.itemId > 0)
            {
                isSuccess = addressService.Update(address);
            }
            else
            {
                isSuccess = addressService.Insert(address);
            }

            if (isSuccess)
            {
                //Zeon Custom Code:Starts
                SaveAddressExtnData(address.AddressID);

                if (address != null && address.IsDefaultBilling)
                {
                    this._userAccount.SetBillingAddress(address.AddressID);
                }
                if (address != null && address.IsDefaultShipping)
                {
                    this._userAccount.SetShippingAddress(address.AddressID);
                }

                //Zeon Custom Code:Ends
                this.RedirectPage();
            }
        }

        /// <summary>
        /// This event raised on clicking the button btnCancel
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            this.RedirectPage();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Zeon Custom Code: Start
            this.txtAddressName.Focus();
            //Zeon Custom Code: End
            this._userAccount = ZNodeUserAccount.CurrentAccount();

            if (this._userAccount == null)
            {
                Response.Redirect("~/account.aspx");
            }

            if (Request.QueryString["ItemID"] != null)
            {
                this.itemId = Convert.ToInt32(Request.QueryString["ItemID"]);
            }

            if (this.itemId > 0)
            {
                lblAddressTitle.Text = this.GetLocalResourceObject("EditAddress.Text").ToString();
            }
            else
            {
                lblAddressTitle.Text = this.GetLocalResourceObject("AddAddress.Text").ToString();
            }

            if (!Page.IsPostBack)
            {
                this.BindCountry();
                this.BindAddress();

            }

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtUpdateAddress.Text");
            }

            this.SetStateTextBoxVisibility(lstBillingCountryCode, txtBillingState, lstBillingStateCode);
        }

        /// <summary>
        /// Page_PreRender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Zeon Custom Code:Start
            this.EnablePostalCodeValidation(lstBillingCountryCode);
            //Zeon Custom Code:Ends
        }

        /// <summary>
        /// Handeles selected indexchange event of lstBillingCountryCode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lstBillingCountryCode_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            BindStateByCountryCode(lstBillingCountryCode.SelectedValue);

            EnablePostalCodeValidation(lstBillingCountryCode);
            lstBillingCountryCode.Focus();
        }

        /// <summary>
        /// Bind the address.
        /// </summary>
        private void BindAddress()
        {
            AddressService addressService = new AddressService();
            Address address = addressService.GetByAddressID(this.itemId);
            if (address != null)
            {
                // Set field values
                txtAddressName.Text = Server.HtmlDecode(address.Name);
                txtBillingFirstName.Text = Server.HtmlDecode(address.FirstName);
                txtBillingLastName.Text = Server.HtmlDecode(address.LastName);
                txtBillingCompanyName.Text = Server.HtmlDecode(address.CompanyName);
                txtBillingStreet1.Text = Server.HtmlDecode(address.Street);
                txtBillingStreet2.Text = Server.HtmlDecode(address.Street1);
                txtBillingCity.Text = address.City;
                // txtBillingState.Text = address.StateCode;
                txtBillingPostalCode.Text = address.PostalCode;

                ListItem blistItem = lstBillingCountryCode.Items.FindByValue(address.CountryCode);
                if (blistItem != null)
                {
                    lstBillingCountryCode.SelectedIndex = lstBillingCountryCode.Items.IndexOf(blistItem);
                }

                //Zeon Custom Code:Starts
                if (ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(address.CountryCode))
                {
                    this.BindStateByCountryCode(lstBillingCountryCode.SelectedValue);
                    lstBillingStateCode.SelectedValue = address.StateCode;

                }
                else
                {
                    txtBillingState.Text = address.StateCode;
                }
                BindAddressExtensionDetails(address.AddressID, address.StateCode);
                //Zeon Custom Code:Ends

                txtBillingPhoneNumber.Text = address.PhoneNumber;
                chkUseAsDefaultBillingAddress.Checked = address.IsDefaultBilling;
                chkUseAsDefaultShippingAddress.Checked = address.IsDefaultShipping;
            }
            //Zeon Custom Code:Start
            else
            {
                this.BindStateByCountryCode(lstBillingCountryCode.SelectedValue);
            }

            //Zeon Custom Code:Ends
        }

        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            // PortalCountry
            ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper = new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
            DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            DataView billingView = new DataView(countryDs.Tables[0]);
            billingView.RowFilter = "BillingActive = 1";

            DataView shippingView = new DataView(countryDs.Tables[0]);
            shippingView.RowFilter = "ShippingActive = 1";

            lstBillingCountryCode.DataSource = billingView;
            lstBillingCountryCode.DataTextField = "Name";
            lstBillingCountryCode.DataValueField = "Code";
            lstBillingCountryCode.DataBind();
            lstBillingCountryCode.SelectedValue = "US";

        }

        /// <summary>
        /// Check whether the account has default billing or shipping address.
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <param name="isBillingAddress">True to check billing address, false to check shipping address.</param>
        /// <returns>Returns true if account has default address or false.</returns>
        private bool HasDefaultAddress(int accountId, bool isBillingAddress)
        {
            bool hasDefault = true;

            AccountAdmin accountAdmin = new AccountAdmin();
            Address address = null;
            if (isBillingAddress)
            {
                address = accountAdmin.GetDefaultBillingAddress(accountId);
                hasDefault = this.ValidateDefaultAddress(address, chkUseAsDefaultBillingAddress);
            }
            else
            {
                address = accountAdmin.GetDefaultShippingAddress(accountId);
                hasDefault = this.ValidateDefaultAddress(address, chkUseAsDefaultShippingAddress);
            }

            return hasDefault;
        }

        /// <summary>
        /// Validate account had default address.
        /// </summary>
        /// <param name="address">Address object</param>
        /// <param name="cb">Checkbox checked or unchecked</param>
        /// <returns>Returns true if account has default address.</returns>
        private bool ValidateDefaultAddress(Address address, CheckBox cb)
        {
            if (address == null && !cb.Checked)
            {
                return false;
            }
            else
            {
                if (!cb.Checked && address != null && address.AddressID == this.itemId)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Redirect to the Page as per return url
        /// </summary>
        private void RedirectPage()
        {
            // If request comes from checkout page then redirect to the same 
            if (Request.QueryString["returnurl"] != null)
            {
                Response.Redirect("~/editAddress.aspx");
            }
            else
            {
                Response.Redirect("~/account.aspx");
            }
        }

        #region Zeon Custom Code

        /// <summary>
        /// Bind State as per countryCode
        /// </summary>
        private void BindStateByCountryCode(string countryCode)
        {
            bool islistVisible = false;
            if (ConfigurationManager.AppSettings["AddressCustomCountry"] != null && ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().ToUpper().Contains(countryCode))
            {
                //Zeon Custom Code:Starts
                //Znode Old Code
                //StateService stateService = new StateService();
                //string filterQuery = "CountryCode='" + countryCode + "'";
                //TList<State> stateList = stateService.Find(filterQuery);
                ZNode.Libraries.DataAccess.Custom.ZStateHelper zStateHelper = new ZNode.Libraries.DataAccess.Custom.ZStateHelper();
                DataTable dtStatesList = zStateHelper.GetStateListByCountryCode(countryCode);
                //Zeon Custom Code:Ends
                if (dtStatesList != null && dtStatesList.Rows.Count > 0)
                {
                    lstBillingStateCode.DataSource = dtStatesList;
                    lstBillingStateCode.DataTextField = "Name";
                    lstBillingStateCode.DataValueField = "Code";
                    lstBillingStateCode.DataBind();
                    lstBillingStateCode.Items.Insert(0, new ListItem(Resources.CommonCaption.DefaultSelectedState.ToString(), Resources.CommonCaption.DefaultSelectedState.ToString()));
                    lstBillingStateCode.SelectedIndex = 0;
                    islistVisible = true;

                }
            }
            SetStateControlVisibility(islistVisible);
        }

        /// <summary>
        /// set State controls visibility
        /// </summary>
        /// <param name="isStatelist"></param>
        private void SetStateControlVisibility(bool isStatelist)
        {
            lstBillingStateCode.Visible = isStatelist;
            txtBillingState.Visible = !isStatelist;
            Requiredfieldvalidator6.Enabled = !isStatelist;
        }

        /// <summary>
        /// Set State Drop Down Visibilty
        /// </summary>
        /// <param name="lstCountry"></param>
        /// <param name="txtState"></param>
        /// <param name="lstState"></param>
        private void SetStateTextBoxVisibility(DropDownList lstCountry, TextBox txtState, DropDownList lstState)
        {
            txtState.Visible = true;
            lstState.Visible = false;
            string selectedValue = lstCountry.SelectedValue;
            string[] countryCodeArray = ConfigurationManager.AppSettings["AddressCustomCountry"] != null ? ConfigurationManager.AppSettings["AddressCustomCountry"].ToString().Split(',') : null;
            if (countryCodeArray != null && countryCodeArray.Length > 0)
            {
                foreach (string str in countryCodeArray)
                {
                    if (selectedValue.Equals(str))
                    {
                        lstState.Visible = true;
                        txtState.Visible = false;
                        break;
                    }
                }

            }
        }

        /// <summary>
        /// Bind Address Extn Details
        /// </summary>
        /// <param name="addressID"></param>
        private void BindAddressExtensionDetails(int addressID, string stateCode)
        {
            AddressExtnService addrssExtnSer = new AddressExtnService();
            TList<AddressExtn> addressList = addrssExtnSer.GetByAddressID(addressID);
            if (addressList != null && addressList.Count > 0)
            {
                chkBillingPOBox.Checked = addressList[0].IsPOBox != null ? bool.Parse(addressList[0].IsPOBox.ToString()) : false;
                if (lstBillingStateCode.Visible && stateCode.IndexOf("AE") >= 0 && !string.IsNullOrEmpty(addressList[0].Custom1))
                {
                    lstBillingStateCode.SelectedValue = lstBillingStateCode.Items.FindByText(addressList[0].Custom1).Value;
                }
                hdnBillingExtnID.Value = addressList[0].AddressExtnID.ToString();
            }
            else
            {
                chkBillingPOBox.Checked = false;
            }
        }

        /// <summary>
        /// save Address Extn Data
        /// </summary>
        /// <param name="addressID"></param>
        private void SaveAddressExtnData(int addressID)
        {
            int addressExtnId = 0;
            AddressExtn addressExtn = new AddressExtn();
            AddressExtnService addrssExtnSer = new AddressExtnService();
            if (!string.IsNullOrEmpty(hdnBillingExtnID.Value)) { int.TryParse(hdnBillingExtnID.Value, out addressExtnId); }
            addressExtn.IsPOBox = chkBillingPOBox.Checked;
            addressExtn.Custom1 = lstBillingStateCode.SelectedValue.IndexOf("AE") >= 0 && lstBillingStateCode.Visible ? lstBillingStateCode.SelectedItem.Text : null;
            addressExtn.AddressID = addressID;
            try
            {
                if (addressExtnId > 0)
                {
                    addressExtn.AddressExtnID = addressExtnId;
                    addrssExtnSer.Update(addressExtn);
                }
                else
                {
                    addrssExtnSer.Save(addressExtn);
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.Log("!!Error in SaveAddressExtnData in!!" + this.Request.Url + addressID + ex.ToString());
            }
        }

        /// <summary>
        /// Set Postal Code Validation
        /// </summary>
        /// <param name="ddlCountryList"></param>
        private void EnablePostalCodeValidation(DropDownList ddlCountryList)
        {
            //Changes done on date  : 30/Jan/2015 : Purpose :Apply regx validation for US Postal Code
            if (ddlCountryList.SelectedValue.Equals("US", StringComparison.OrdinalIgnoreCase))
            {
                rgxBillingPostalCodeValidation.Visible = true;
            }
            else if (!ddlCountryList.SelectedValue.Equals("US", StringComparison.OrdinalIgnoreCase))
            {
                rgxBillingPostalCodeValidation.Visible = false;
            }

        }
        #endregion

    }
}
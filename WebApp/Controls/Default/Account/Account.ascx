<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Account_Account" CodeBehind="Account.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="SavedCardInfo" Src="~/Controls/Default/Account/SavedCardInfo.ascx" %>

<div class="Form">
    <div class="PageTitle">
        <asp:Localize ID="txtAccountsTitle" meta:resourceKey="txtAccountTitle" runat="server" ClientIDMode="Static" />
    </div>
    <div>
        <asp:Localize ID="Localize1" meta:resourceKey="txtAccountInfo" runat="server" ClientIDMode="Static" />
    </div>
    <div>
        <uc1:Spacer ID="Spacer5" SpacerHeight="10" EnableViewState="false" SpacerWidth="10"
            runat="server" ClientIDMode="Static"></uc1:Spacer>
    </div>
    <ul id="AccountSectionTree">
        <li>
            <span class="HeaderStyle AccountItem">
                <asp:Localize ID="Localize2" meta:resourceKey="txtLogin" runat="server" ClientIDMode="Static" />
            </span>
            <ul>
                <li>
                    <div class="AccountSection">
                        <div class="LeftContent">
                            <p>
                                <asp:Localize ID="Localize3" meta:resourceKey="txtLoginName" runat="server" ClientIDMode="Static" /><br />
                                <asp:LoginName ID="LoginName1" runat="server" FormatString="<b> {0}</b>" />
                            </p>
                        </div>
                        <div class="RightContent">
                            <asp:LinkButton ID="ibChangePassword" runat="server" OnClick="BtnChangePassword_Click"
                                CssClass="Button Large" meta:resourceKey="txtChangePassword" ClientIDMode="Static"></asp:LinkButton>
                        </div>
                        <div class="Clear">
                            <uc1:Spacer ID="Spacer4" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
                                runat="server" ClientIDMode="Static"></uc1:Spacer>
                        </div>
                    </div>
                </li>
            </ul>

        </li>
        <li>
            <span class="HeaderStyle AccountItem">
                <asp:Localize ID="Localize4" meta:resourceKey="txtContact" runat="server" ClientIDMode="Static" />
            </span>
            <ul>
                <li>
                    <div class="AccountSection">
                        <div class="LeftContent">
                            <p>
                                <span class="title">
                                    <asp:Localize ID="Localize5" meta:resourceKey="txtEmail" runat="server" ClientIDMode="Static" /></span><asp:Label ID="lblEmail" runat="server" ClientIDMode="Static"></asp:Label>
                            </p>
                            <p>
                                <span class="title">
                                    <asp:Localize ID="Localize14" meta:resourceKey="txtEmailOptIn" runat="server" ClientIDMode="Static" /></span><asp:Image ID="EmailOptin" AlternateText="Email" ToolTip="Email" runat="server" ClientIDMode="Static" />
                            </p>
                        </div>
                        <div class="RightContent">
                            <asp:LinkButton ID="ibEditAddress" runat="server" OnClick="BtnEditAddress_Click"
                                CssClass="Button Large" meta:resourceKey="txtEditContact" ClientIDMode="Static"></asp:LinkButton>
                        </div>
                        <div class="Clear">
                            <uc1:Spacer ID="Spacer12" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
                                runat="server" ClientIDMode="Static"></uc1:Spacer>
                        </div>
                    </div>
                </li>
            </ul>
        </li>
        <li>
            <span class="HeaderStyle AccountItem">
                <asp:Localize ID="Localize6" meta:resourceKey="txtAddresses" runat="server" ClientIDMode="Static" />
            </span>
            <ul>
                <li>
                    <div class="AccountSection NoBorder">
                        <div class="LeftContent">
                        </div>
                        <div class="RightContent">
                            <asp:LinkButton ID="lbAddNewAddress" runat="server" CssClass="Button Large" meta:resourceKey="AddNewAddress"
                                OnClick="LbAddNewAddress_Click" ClientIDMode="Static"></asp:LinkButton>
                        </div>
                        <div class="UserAddress">
                            <asp:DataList ID="dlAddress" runat="server" RepeatLayout="Flow" ClientIDMode="Static" EnableViewState="false" CssClass="Grid" RepeatDirection="Horizontal" OnDeleteCommand="dlAddress_OnDeleteCommand" OnItemCommand="dlAddress_OnItemCommand">
                                <ItemStyle CssClass="ItemStyle" VerticalAlign="Top" />
                                <HeaderTemplate>
                                    <header class="col1">
                                        <asp:Literal ID="ltrNameHead" Text="<%$ Resources:CommonCaption, Name %>" runat="server"></asp:Literal>
                                    </header>
                                    <header class="col2">
                                        <asp:Literal ID="ltrShipHead" Text="<%$ Resources:CommonCaption, DefaultShipping%>" runat="server"></asp:Literal>
                                    </header>
                                    <header class="col3">
                                        <asp:Literal ID="ltrDefaultBilling" Text="<%$ Resources:CommonCaption, DefaultBilling %>" runat="server"></asp:Literal>
                                    </header>
                                    <header class="col4">
                                        <asp:Literal ID="Literal1" Text="<%$ Resources:CommonCaption, Address %>" runat="server"></asp:Literal>
                                    </header>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span class="col1"><%#DataBinder.Eval(Container.DataItem,"Name")%></span>
                                    <span class="col2">
                                        <img alt="Default Shipping" title="Default Shipping" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultShipping").ToString()))%>'
                                            runat="server" clientidmode="Static" /></span>
                                    <span class="col3">
                                        <img alt="Default Billing" title="Default Billing" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultBilling").ToString()))%>'
                                            runat="server" clientidmode="Static" /></span>
                                    <span class="col4">
                                        <asp:Label ID="lblAddress" runat="server" Text='<%# GetAddress(DataBinder.Eval(Container.DataItem, "AddressID").ToString())%>' ClientIDMode="Static"></asp:Label>
                                        <span class="EditButton">
                                            <asp:LinkButton ID="lbDelete" runat="server" CommandName="Delete" CssClass="Button"
                                                Text="<%$ Resources:CommonCaption, Delete %>" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AddressID")%>' ClientIDMode="Static"></asp:LinkButton>
                                            <a href='<%#"address.aspx?ItemID="+(DataBinder.Eval(Container.DataItem, "AddressID").ToString())%>' class="Button" id="hlnkEdit">
                                                <asp:Localize ID="locEditlnk" Text="<%$ Resources:CommonCaption, Edit %>" runat="server"></asp:Localize></a>
                                        </span></span>
                                </ItemTemplate>
                                <AlternatingItemStyle CssClass="AlternatingRowStyle" />
                                <FooterStyle CssClass="FooterStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <ItemStyle CssClass="item" />
                            </asp:DataList>
                            <div runat="server" id="DivAddressErr" class="Error" visible="false">
                                <div class="uxMsg ErrorSection"><em>
                                    <asp:Label ID="lblError" CssClass="Error" runat="server" ClientIDMode="Static" /></em></div>
                            </div>


                            <div class="AccountPager" runat="server" id="divAddressPager" visible="false">
                                <asp:HiddenField ID="hdnAddressCurrrentPage" runat="server" />
                                <asp:PlaceHolder ID="dlAddressPaging" runat="server"></asp:PlaceHolder>
                            </div>
                        </div>
                    </div>
                    <%--Affiliate section--%>
                    <asp:Panel ID="pnlAffiliate" runat="server" Visible="false" ClientIDMode="Static">
                        <div>
                            <div class="HeaderStyle">
                                <asp:Localize ID="Localize7" meta:resourceKey="txtAffiliateInfo" runat="server" />
                            </div>
                            <div class="LeftContent">
                                <b>
                                    <asp:Localize ID="Localize9" meta:resourceKey="txtRefCommision" runat="server" /></b>
                                :
                            </div>
                            <div class="LeftContent">
                                <uc1:Spacer ID="Spacer15" EnableViewState="false" SpacerHeight="10" SpacerWidth="30"
                                    runat="server" ClientIDMode="Static"></uc1:Spacer>
                            </div>
                            <div>
                                <%=ReferralCommision%>
                            </div>
                            <div class="Clear">
                                <uc1:Spacer ID="Spacer3" EnableViewState="false" SpacerHeight="5" SpacerWidth="10"
                                    runat="server" ClientIDMode="Static"></uc1:Spacer>
                            </div>
                            <div class="LeftContent">
                                <b>
                                    <asp:Localize ID="Localize10" meta:resourceKey="txtRefTaxID" runat="server" /></b>
                                :
                            </div>
                            <div>
                                <%=TaxId%>
                            </div>
                            <div class="Clear">
                                <uc1:Spacer ID="Spacer13" EnableViewState="false" SpacerHeight="5" SpacerWidth="10"
                                    runat="server" ClientIDMode="Static"></uc1:Spacer>
                            </div>
                            <div class="LeftContent">
                                <b>
                                    <asp:Localize ID="Localize11" meta:resourceKey="txtRefCommisionType" runat="server" /></b>
                                :
                            </div>
                            <div class="LeftContent">
                                <uc1:Spacer ID="Spacer17" EnableViewState="false" SpacerHeight="10" SpacerWidth="30"
                                    runat="server" ClientIDMode="Static"></uc1:Spacer>
                            </div>
                            <div>
                                <%=ReferralCommisionType%>
                            </div>
                            <div class="Clear">
                                <uc1:Spacer ID="Spacer14" EnableViewState="false" SpacerHeight="5" SpacerWidth="10"
                                    runat="server"></uc1:Spacer>
                            </div>
                            <div class="LeftContent">
                                <strong>
                                    <asp:Localize ID="Localize12" meta:resourceKey="txtRefTrack" runat="server" /></strong>
                                :
                            </div>
                            <div class="LeftContent">
                                <uc1:Spacer ID="Spacer18" EnableViewState="false" SpacerHeight="10" SpacerWidth="30"
                                    runat="server" ClientIDMode="Static"></uc1:Spacer>
                            </div>
                            <asp:HyperLink ID="hlAffiliateLink" Text="NA" runat="server" Target="_blank"></asp:HyperLink>
                            <div class="Clear">
                                <uc1:Spacer ID="Spacer11" EnableViewState="false" SpacerHeight="5" SpacerWidth="10"
                                    runat="server" ClientIDMode="Static"></uc1:Spacer>
                            </div>
                        </div>
                    </asp:Panel>
                </li>
            </ul>
        </li>
        <li>
            <span class="HeaderStyle AccountItem">
                <asp:Localize ID="Localize8" meta:resourceKey="txtOrderHistory" runat="server" ClientIDMode="Static" />
            </span>
            <ul>
                <li>
                    <div class="OrderHistory">
                        <div>
                            <asp:Literal ID="ltrEmptyOrderMessage" runat="server"></asp:Literal>
                            <asp:DataList ID="dlOrder" runat="server" RepeatLayout="Flow" ClientIDMode="Static" RepeatDirection="Horizontal">
                                <ItemStyle CssClass="ItemStyle" VerticalAlign="Top" />
                                <HeaderTemplate>
                                    <header class="col1">
                                        <asp:Literal ID="ltrOrderNum" meta:resourceKey="ltrOrderNum" runat="server"></asp:Literal>
                                    </header>
                                    <header class="col2">
                                        <asp:Literal ID="ltrOrderDate" meta:resourceKey="ltrOrderDate" runat="server"></asp:Literal>
                                    </header>
                                    <header class="col3">
                                        <asp:Literal ID="ltrStatus" meta:resourceKey="ltrStatus" runat="server"></asp:Literal>
                                    </header>
                                    <header class="col4">
                                        <asp:Literal ID="ltrCustName" meta:resourceKey="ltrCustName" runat="server"></asp:Literal>
                                    </header>
                                    <header class="col5">
                                        <asp:Literal ID="ltrTotalCost" meta:resourceKey="ltrTotalCost" runat="server"></asp:Literal>
                                    </header>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="OrderTitle">
                                        <div class="OrderCol1">Order# </div>
                                        <div class="OrderCol2">Total Cost</div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="col1">
                                        <%# DataBinder.Eval(Container.DataItem, "OrderID")%>
                                    </div>
                                    <div class="col2">
                                        <%# DataBinder.Eval(Container.DataItem, "OrderDate")%>
                                    </div>
                                    <div class="col3 Status">
                                        <%# FormatOrderStatus(DataBinder.Eval(Container.DataItem, "OrderStatus").ToString())%>
                                    </div>
                                    <div class="col4">
                                        <%# DataBinder.Eval(Container.DataItem, "BillingFirstName").ToString() + " " + DataBinder.Eval(Container.DataItem, "BillingLastName").ToString()%>
                                    </div>
                                    <div class="col5">
                                        <%# DataBinder.Eval(Container.DataItem, "Total", "{0:c}")%>
                                    </div>
                                    <div class="col6">
                                        <asp:HyperLink ID="hlnkViewOrder" runat="server" NavigateUrl='<%# "~/order.aspx?itemid="+ DataBinder.Eval(Container.DataItem, "OrderID")%>' CssClass="Button" meta:resourceKey="ViewItem"></asp:HyperLink>
                                    </div>
                                </ItemTemplate>
                                <AlternatingItemStyle CssClass="AlternatingRowStyle" />
                                <FooterStyle CssClass="FooterStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                            </asp:DataList>
                        </div>
                        <div class="AccountPager" id="divOrderPager" runat="server">
                            <asp:HiddenField ID="hdnOrderCurrentPage" runat="server" />
                            <asp:PlaceHolder ID="dlOrderPager" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>
                </li>
            </ul>

        </li>
        <li>
            <span class="AccountItem HeaderStyle">
                <asp:Localize ID="Localize13" meta:resourceKey="txtWishList" runat="server" ClientIDMode="Static" />
            </span>
            <ul>
                <li>
                    <div>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </div>
                    <div>
                        <uc1:Spacer ID="Spacer7" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
                            runat="server" ClientIDMode="Static"></uc1:Spacer>
                    </div>
                    <div class="WishListSection">
                        <asp:Label ID="lblEmptyMessage" runat="server"></asp:Label>
                        <asp:DataList ID="dlWishList" runat="server" EnableViewState="false" ClientIDMode="Static" RepeatLayout="Flow" RepeatDirection="Horizontal">
                            <ItemStyle CssClass="ItemStyle" VerticalAlign="Top" />
                            <HeaderTemplate>
                                <header class="col1">
                                    <asp:Literal ID="ltrRemoveHead" meta:resourceKey="ltrRemoveHead" runat="server"></asp:Literal>
                                </header>
                                <header class="col2">
                                    <asp:Literal ID="ltrWLItem" meta:resourceKey="ltrWLItem" runat="server"></asp:Literal>
                                </header>
                                <header class="col3">&nbsp;</header>
                                <header class="col4">
                                    <asp:Literal ID="ltrWLPrice" meta:resourceKey="ltrWLPrice" runat="server"></asp:Literal>
                                </header>
                                <header class="col5">&nbsp;</header>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="col1">
                                    <div>
                                        <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductID")%>'
                                            ID="lnkRemove" runat="server" meta:resourceKey="RemoveItem" CommandName="remove"
                                            OnClick="LnkRemove_Click" ClientIDMode="Static"></asp:LinkButton>
                                    </div>
                                    <%--<br />
                                    <div>
                                        <a title="Send Email" href="javascript:void(0);" class="sendEmail" onclick="zeonAccount.accountPrmPageLoadedEventHandler('<%# DataBinder.Eval(Container.DataItem, "ProductID")%>','<%# DataBinder.Eval(Container.DataItem, "Name")%>');">Send Email</a>
                                    </div>--%>
                                </div>
                                <div class="col2">
                                    <a id="A1" href='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>'
                                        runat="server">
                                        <img id="Img1" alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' title='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' border='0'
                                            src='<%# DataBinder.Eval(Container.DataItem, "ThumbnailImageFilePath")%>' runat="server" clientidmode="Static" /></a>
                                </div>
                                <div class="col3">
                                    <div>
                                        <a id="A2" href='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>'
                                            runat="server">
                                            <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                                    </div>
                                    <div class="Description">
                                        Item#
                                        <%# DataBinder.Eval(Container.DataItem, "ProductNum").ToString()%><br>
                                        <%# DataBinder.Eval(Container.DataItem, "ShortDescription").ToString()%>
                                    </div>
                                </div>
                                <div class="col4">
                                    <div class="CallForPrice">
                                        <%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>
                                    </div>
                                    <asp:Label ID="Price" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>'
                                        Visible='<%#!(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && AccountProfile.ShowPrice%>'></asp:Label>
                                </div>
                                <div class="col5">
                                    <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductID")%>'
                                        ID="btnbuy" runat="server" OnClick="Buy_Click" Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && AccountProfile.ShowAddToCart %>'
                                        CssClass="Button Large" meta:resourceKey="AddToCart" ClientIDMode="Static"></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                            <AlternatingItemStyle CssClass="AlternatingRowStyle" />
                            <FooterStyle CssClass="FooterStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                        </asp:DataList>
                        <div class="AccountPager" runat="server" id="divWishlistPager">
                            <asp:HiddenField ID="hdnWishListCurrentPage" runat="server" />
                            <asp:PlaceHolder ID="dlWishListPager" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
    <div>
        <uc1:Spacer ID="Spacer19" EnableViewState="false" SpacerHeight="20" SpacerWidth="10"
            runat="server" ClientIDMode="Static"></uc1:Spacer>
    </div>
    <div id="emailDialog" title="Send WishList Email" style="display: none;">
        <asp:Panel ID="pnlSendEmail" runat="server" DefaultButton="btnSendWishListEmail" EnableViewState="false" ClientIDMode="Static">
            <div>
                <asp:Label ID="lblErrors" runat="server" EnableViewState="false" CssClass="Error" ClientIDMode="Static"></asp:Label>
            </div>
            <div>
                <asp:Localize ID="Localize19" meta:resourceKey="ProductName" EnableViewState="false" runat="server" ClientIDMode="Static"></asp:Localize>&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblProductName" runat="server" Font-Bold="true" EnableViewState="false" ClientIDMode="Static"></asp:Label>
            </div>
            <div>
                <asp:Localize ID="Localize16" runat="server" meta:resourceKey="Name" EnableViewState="false" ClientIDMode="Static"> </asp:Localize>

                <asp:TextBox ID="txtFirstName" runat="server" meta:resourceKey="txtName"  EnableViewState="false" ValidationGroup="SendWishListEmial" ClientIDMode="Static"> </asp:TextBox>
            </div>

            <div>
                <asp:Localize ID="Localize17" runat="server" Text="<%$ Resources:CommonCaption, EmailID%>" EnableViewState="false" ClientIDMode="Static"></asp:Localize>
                <asp:TextBox ID="txtEmailId" Width="115px" runat="server" EnableViewState="false" meta:resourceKey="EmailAddress" ValidationGroup="SendWishListEmial" ToolTip="<%$ Resources:CommonCaption, EmailID%>" ClientIDMode="Static"></asp:TextBox>
            </div>

            <div>
                <asp:LinkButton ID="btnSendWishListEmail" runat="server"  meta:resourceKey="SendWishListEmail" OnClick="btnSendWishListEmail_Click" EnableViewState="false" OnClientClick="return zeonAccount.SetHiddenFiledValue();" CssClass="Button" ClientIDMode="Static" />
            </div>
        </asp:Panel>
    </div>
    <asp:HiddenField ID="hdnProductID" runat="server" EnableViewState="false" />
    <asp:HiddenField ID="hdnName" runat="server" EnableViewState="false" />
    <asp:HiddenField ID="hdnEmailAddress" runat="server" EnableViewState="false" />
</div>



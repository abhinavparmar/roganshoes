<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Account_Register" CodeBehind="Register.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>

<asp:Panel DefaultButton="ibRegister" ID="pnlRegister" runat="server" ClientIDMode="Static">
    <div class="Register">
        <div class="Form">
            <div class="PageTitle">
                <asp:Localize ID="Localize8" meta:resourceKey="txtAccountTitle" runat="server" EnableViewState="false" ClientIDMode="Static" />
            </div>
            <div class="Row Clear">
                <div align="left" colspan="2" class="FailureText">
                    <div class="Error " runat="server" enableviewstate="false" id="errorMsg">
                        <span class="uxMsg ErrorSection"><em>
                            <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False" ClientIDMode="Static"></asp:Literal>
                        </em></span>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblFirstName" CssClass="Req" runat="server" AssociatedControlID="txtFirstName" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize2" meta:resourceKey="lblFirstName" runat="server" EnableViewState="false" ClientIDMode="Static" /></asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="txtFirstName" runat="server" autocomplete="off" placeholder="First Name *" CssClass="TextField" EnableViewState="false"  ClientIDMode="Static" alt="First Name"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="revtxtFirstName" runat="server" SetFocusOnError="true"
                            ControlToValidate="txtFirstName"
                            ValidationGroup="uxCreateUserWizard" CssClass="Error"
                            meta:resourceKey="revtxtFirstName"
                            Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblLastName" runat="server" CssClass="Req" AssociatedControlID="UserName" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize4" meta:resourceKey="lblLastName" runat="server" EnableViewState="false" ClientIDMode="Static" /></asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="txtLastName" runat="server" autocomplete="off" meta:resourceKey="UserName" placeholder="Last Name *" CssClass="TextField" EnableViewState="false"  ClientIDMode="Static" alt="Last Name"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="revtxtLastName" runat="server" SetFocusOnError="true"
                            ControlToValidate="txtLastName"
                            meta:resourceKey="revtxtLastName"
                            ValidationGroup="uxCreateUserWizard" CssClass="Error"
                            Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <%-- Zeon Custom Implementation:Replacing UserId with Email--%>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="UserNameLabel" CssClass="Req" runat="server" AssociatedControlID="UserName" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize1" meta:resourceKey="txtUserName" runat="server" EnableViewState="false" ClientIDMode="Static" /></asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="UserName" runat="server" autocomplete="off" meta:resourceKey="UserName" CssClass="TextField" placeholder="User Name *" EnableViewState="false"  ClientIDMode="Static" alt="User Name"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" SetFocusOnError="true"
                            ControlToValidate="UserName"
                            ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired %>"
                            ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired %>"
                            ValidationGroup="uxCreateUserWizard" CssClass="Error"
                            Display="Dynamic">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UserName" SetFocusOnError="true"
                            ValidationGroup="uxCreateUserWizard" CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired %>" ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired %>"
                            ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression %>'
                            Display="Dynamic">
                        </asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="PasswordLabel" CssClass="Req" runat="server" AssociatedControlID="Password" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize3" meta:resourceKey="txtPassword" runat="server" EnableViewState="false" ClientIDMode="Static" /></asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="Password" runat="server" TextMode="Password" autocomplete="off" CssClass="TextField" placeholder="Password *" EnableViewState="false" meta:resourceKey="Password" ClientIDMode="Static" alt="Password"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="PasswordRequired" SetFocusOnError="true" runat="server" ErrorMessage="<%$ Resources:CommonCaption, PasswordRequired%>" ToolTip="<%$ Resources:CommonCaption, PasswordRequired%>" ControlToValidate="Password" ValidationGroup="uxCreateUserWizard"
                            CssClass="Error" Display="Dynamic">
                      
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ValidationGroup="uxCreateUserWizard" SetFocusOnError="true" ID="PasswordRegularExpression"
                            ControlToValidate="Password" runat="server"
                            ValidationExpression="<%$ Resources:CommonCaption, PasswordStrengthExpression%>"
                            Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, ValidPasswordRequired %>"
                            ToolTip="<%$ Resources:CommonCaption, ValidPasswordRequired %>" CssClass="Error"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="ConfirmPasswordLabel" CssClass="Req" runat="server" AssociatedControlID="ConfirmPassword" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize5" meta:resourceKey="txtConfirmPassword" EnableViewState="false" runat="server" ClientIDMode="Static" /></asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"  EnableViewState="false" meta:resourceKey="ConfirmPassword" autocomplete="off" placeholder="Confirm Password *" CssClass="TextField" ClientIDMode="Static" alt="ConfirmPassword"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" SetFocusOnError="true"
                            ErrorMessage="<%$ Resources:CommonCaption, ConfirmPasswordRequired %>"
                            ToolTip="<%$ Resources:CommonCaption, ConfirmPasswordRequired %>"
                            runat="server" ControlToValidate="ConfirmPassword"
                            ValidationGroup="uxCreateUserWizard" CssClass="Error" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div>
                        <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" SetFocusOnError="true"
                            ControlToValidate="ConfirmPassword"
                            ErrorMessage="<%$ Resources:CommonCaption, PasswordMismatch %>"
                            ToolTip="<%$ Resources:CommonCaption, PasswordMismatch %>"
                            ValidationGroup="uxCreateUserWizard" CssClass="Error" Display="Dynamic"></asp:CompareValidator>
                    </div>
                </div>
            </div>
            <%--<div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblSecretQuestion" runat="server" AssociatedControlID="Email" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize12" meta:resourceKey="txtSecurityQ" runat="server" EnableViewState="false" ClientIDMode="Static"/></asp:Label>
                </div>
                <div class="LeftContent">
                    <div class="TextField">
                        <asp:DropDownList ID="ddlSecretQuestions" runat="server" EnableViewState="false" TabIndex="4" meta:resourceKey="SecurityQ" ClientIDMode="Static">
                            <asp:ListItem Enabled="true" Selected="True" Text="<%$ Resources:CommonCaption, txtFavPet %>" />
                            <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCityBorn %>" />
                            <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtSchool %>" />
                            <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMovie %>" />
                            <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMotherName %>" />
                            <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCar %>" />
                            <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtColor %>" />
                        </asp:DropDownList>
                    </div>
                </div>
            </div>--%>
            <%-- <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize13" meta:resourceKey="txtSecurityA" runat="server" EnableViewState="false" ClientIDMode="Static"/></asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="Answer" runat="server" CssClass="TextField" TabIndex="5" meta:resourceKey="SecurityA" EnableViewState="false" ClientIDMode="Static"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="AnswerRequired" SetFocusOnError="false"
                            ErrorMessage="<%$ Resources:CommonCaption, SecurityAnswerRequired %>"
                            ToolTip="<%$ Resources:CommonCaption, SecurityAnswerRequired %>"
                            runat="server" ControlToValidate="Answer"
                            ValidationGroup="uxCreateUserWizard" CssClass="Error" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>--%>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email" EnableViewState="false" CssClass="Req">
                        <asp:Localize ID="Localize7" Text="<%$ Resources:CommonCaption, EmailID%>" EnableViewState="false" runat="server" ClientIDMode="Static" /></asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="Email" placeholder="Subscription Email *" runat="server" CssClass="TextField" ToolTip="<%$ Resources:CommonCaption, EmailID%>" EnableViewState="false" ClientIDMode="Static" alt="Email"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email" SetFocusOnError="true"
                            ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired %>" ValidationGroup="uxCreateUserWizard" CssClass="Error"
                            Display="Dynamic"></asp:RequiredFieldValidator>

                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2"  runat="server" ControlToValidate="Email" SetFocusOnError="true"
                            ValidationGroup="uxCreateUserWizard" CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired %>" ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired %>"
                            ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression %>'
                            Display="Dynamic">
                        </asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent TextNone">
                    &nbsp;
                </div>
                <div class="LeftContent NotMandatory checkbox-container">
                    <asp:CheckBox ID="chkOptIn" runat="server" meta:resourceKey="txtSendMail" Checked="true"  EnableViewState="false" ClientIDMode="Static" />
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent TextNone">
                    <ZNode:spacer ID="Spacer1" SpacerHeight="60" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:spacer>
                </div>
                <div class="Register">
                    <asp:LinkButton ID="ibRegister" runat="server"  EnableViewState="false"
                        OnClick="LbRegister_Click" ValidationGroup="uxCreateUserWizard"
                        meta:resourcekey="RegisterButton" CssClass="Button" ClientIDMode="Static">Register</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>

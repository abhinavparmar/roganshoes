using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using Account = ZNode.Libraries.DataAccess.Entities.Account;
using ZNode.Libraries.ECommerce.ShoppingCart;
using WebApp.CustomClasses;
using Zeon.Libraries.Utilities;

namespace WebApp
{
    /// <summary>
    /// Represents the Login user control class.
    /// </summary>
    public partial class Controls_Default_Account_Login : System.Web.UI.UserControl
    {
        #region Public Properties
        /// <summary>
        /// Sets a value indicating whether "Remember Me Option" checkbox visible property
        /// </summary>
        public bool ShowRememberMeOption
        {
            set
            {
                uxLogin.DisplayRememberMe = value;
                (uxLogin.FindControl("RememberMe") as CheckBox).Visible = value;
            }
        }
        public TextBox UserName
        {
            get { return (TextBox)uxLogin.FindControl("UserName"); }
        }

        public TextBox Password
        {
            get { return (TextBox)uxLogin.FindControl("Password"); }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Event fired when user authenticates
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        public void UxLogin_Authenticate(object sender, AuthenticateEventArgs e)
        {
            bool Authenticated = false;
            Authenticated = this.CustomAuthenticationMethod(uxLogin.UserName.Trim(), uxLogin.Password.Trim());
            e.Authenticated = Authenticated;
        }

        #endregion

        #region Protected Methods and Events
        protected void ForgotPassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/forgotpassword.aspx");
        }

        protected void UxLogin_LoggedIn(object sender, EventArgs e)
        {
            ZNodeCacheDependencyManager.Remove("SpecialsNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + HttpContext.Current.Session.SessionID);

            string returnURL = Request.QueryString["returnurl"];

            if (returnURL.Contains("zpid"))
            {
                Response.Redirect(returnURL);
                return;
            }

            if (System.Web.HttpContext.Current.Session["IsCartChanged"] != null)
            {
                Response.Redirect("~/shoppingcart.aspx", true);
            }
            if (System.Web.HttpContext.Current.Session["RedirectToCart"] != null)
            {
                Response.Redirect("~/shoppingcart.aspx", true);
            }

        }

        protected void uxCreate_Click(object sender, EventArgs e)
        {
            if (Request.RawUrl.ToLower().Contains("editaddress"))
            {
                Response.Redirect("~/registeruser.aspx?ReturnUrl=editAddress.aspx");
            }
            else
            {
                Response.Redirect("~/registeruser.aspx?ReturnUrl=account.aspx");
            }
        }

        protected void uxCheckout_Click(object sender, EventArgs e)
        {
            CreateLog("||Checkout Process Started-Proceed As Guest Checkout||");
            Response.Redirect("~/editAddress.aspx");
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Zeon Custom Code: Start
            TextBox txtUserName = (TextBox)this.uxLogin.FindControl("UserName");
            uxLogin.FindControl("errorMsg").Visible = false;
            txtUserName.Focus();
            //Zeon Custom Code: End
            // To load the image based on locale
            //ImgSeparator.ImageUrl = ZNodeCatalogManager.GetImagePathByLocale("quickwatch_separator.jpg");

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                lblaccess.Visible = true;
            }

            // Set page title
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(ZNodeConfigManager.SiteConfig.StoreName);
            sb.Append(" : ");
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();
            sb.Append(resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "Title"));
            Page.Title = sb.ToString();

            // Check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }

            if (Request.RawUrl.ToLower().Contains("editaddress"))
            {
                uxExpressCheckout.Visible = true;
            }
            //Zeon Custom Code
            if (!Page.IsPostBack)
            {
                UserName.Focus();
                GetUserDetailsFromCookie();
                CreateLog("||Checkout Process Started-Login Page Load||");
            }

            //Genrate Trigger Email Pixel Tag
            //this.GenerateTriggerMailHelperCartPixelTags();
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// This method overrides the default login implementation
        /// </summary>
        /// <param name="UserName">Name of the User</param>
        /// <param name="Password">User Password</param>
        /// <returns>Returns boolean value</returns>
        private bool CustomAuthenticationMethod(string UserName, string Password)
        {
            // Insert code that implements a site-specific custom 
            // Authentication method here.
            //Zeon Custom Code:Starts
            if (!Roles.IsUserInRole(UserName, "ADMIN"))
            {
                UserName = UserName + "_" + ZNodeConfigManager.SiteConfig.PortalID.ToString();
            }
            //Zeon Custom Code:Ends
            ZNodeUserAccount userAcct = new ZNodeUserAccount();
            bool loginSuccess = userAcct.Login(ZNodeConfigManager.SiteConfig.PortalID, UserName, Password);


            if (loginSuccess)
            {
                string retValue = string.Empty;
                bool isLoginEnabled = false;
                int profileId = userAcct.GetAccountPortalProfileID(userAcct.AccountID, ZNodeConfigManager.SiteConfig.PortalID);

                if (profileId == 0)
                {
                    return false;
                }

                bool status = ZNodeUserAccount.CheckLastPasswordChangeDate((Guid)userAcct.UserID, out retValue);

                AccountService acctService = new AccountService();
                Account account = acctService.GetByAccountID(userAcct.AccountID);

                // Get account and set to session
                Session.Add("AccountObject", account);

                if (!status)
                {
                    // Set Error Code to session Object
                    Session.Add("ErrorCode", retValue);

                    Response.Redirect("~/resetpassword.aspx");
                }


                if (profileId != 0)
                {
                    ProfileService profileService = new ProfileService();
                    Profile ProfileEntity = profileService.GetByProfileID(profileId);

                    if (ProfileEntity != null)
                    {
                        // Hold this profile object in the session state
                        HttpContext.Current.Session["ProfileCache"] = ProfileEntity;

                        // Set CurrentUserProfile ProfileID
                        userAcct.ProfileID = ProfileEntity.ProfileID;

                        isLoginEnabled = true;
                    }
                }

                // Get account and set to session
                Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), userAcct);

                // retrieve roles associated with this user
                bool isAdminRole = Roles.IsUserInRole(UserName, "ADMIN");

                if (!isLoginEnabled && !isAdminRole)
                {
                    loginSuccess = false;
                    uxLogin.FindControl("errorMsg").Visible = true;
                }

                // If Login success, merge the persistent cart items.
                if (loginSuccess)
                {
                    ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart savedCart = new ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart();
                    savedCart.Merge();
                    uxLogin.FindControl("errorMsg").Visible = false;

                }
                SaveRememberMeCookie();//Zeon Custom Code
            }

            if (!loginSuccess) { uxLogin.FindControl("errorMsg").Visible = true; }

            return loginSuccess;
        }

        private void CreateLog(string eventName)
        {
            string strlog = "\n" + eventName + ". \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
            strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, "", "");
            Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);
        }

        #endregion

        #region Zeon Custom Code :Remember Me Changes

        /// <summary>
        /// On Remember Me checked save customer detail
        /// </summary>
        private void SaveRememberMeCookie()
        {
            ZLoginCookieHelper.ClearLoginRememberMeCookie();
            CheckBox chkRememberMe = ((CheckBox)uxLogin.FindControl("chkRememberMe"));
            if (chkRememberMe != null && chkRememberMe.Checked)
            {
                string strPassword = string.Empty;
                string strUserName = string.Empty;
                if (Password.Text == "*******")
                {
                    strUserName = ZLoginCookieHelper.GetLoginNameFromCookie().Trim();
                }
                if (string.IsNullOrEmpty(strPassword))
                {
                    strPassword = uxLogin.Password;
                }
                if (string.IsNullOrEmpty(strUserName))
                {
                    strUserName = uxLogin.UserName.Trim();
                }
                ZLoginCookieHelper.SaveLoginRememberMeCookie(strUserName);
            }
        }

        /// <summary>
        /// Setting the textbox value by default if its saved in cookie
        /// </summary>
        private void GetUserDetailsFromCookie()
        {
            //Check if the cookies with name Remember  exist on user's machine 
            if (!string.IsNullOrEmpty(ZLoginCookieHelper.GetLoginRememberMeCookie()))
            {
                if (!string.IsNullOrEmpty(ZLoginCookieHelper.GetLoginNameFromCookie()))
                {
                    TextBox username = (TextBox)uxLogin.FindControl("UserName");
                    CheckBox chkRememberme = (CheckBox)uxLogin.FindControl("chkRememberMe");
                    if (UserName != null && Password != null && chkRememberme != null)
                    {
                        username.Text = ZLoginCookieHelper.GetLoginNameFromCookie();
                        //Password.Attributes["value"] = "*******";
                        chkRememberme.Checked = true;
                    }
                }
            }
        }

        #endregion

    }
}
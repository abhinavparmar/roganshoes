<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Account_ForgotPassword" CodeBehind="ForgotPassword.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>


<div class="Form ResetPassword">
    <div class="PageTitle">
        <h1>
            <asp:Localize ID="Localize21" meta:resourceKey="txtPasswordTitle" runat="server" ClientIDMode="Static" />
        </h1>
    </div>
    <asp:Wizard ID="PasswordRecoveryWizard" DisplaySideBar="False" runat="server"
        OnActiveStepChanged="PasswordRecoveryWizard_ActiveStepChanged" CellPadding="-1" CellSpacing="-1"
         ActiveStepIndex="0">
        <WizardSteps>
            <asp:TemplatedWizardStep ID="step1" runat="server" StepType="Start" AllowReturn="true" ClientIDMode="AutoID">
                <ContentTemplate>
                    <p class="FormTitle">
                        <asp:Localize ID="Localize3" meta:resourceKey="txtEnterDetail" runat="server" EnableViewState="false" ClientIDMode="Static" />
                    </p>
                    <asp:Panel ID="UserNamePanel" DefaultButton="ibSubmit" runat="server" ClientIDMode="Static" CssClass="CommonLableNone">
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent">
                                <%--<asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" EnableViewState="false" ClientIDMode="Static">
                                    <asp:Localize ID="Localize1" meta:resourceKey="txtUserName" runat="server" ClientIDMode="Static" /></asp:Label>--%>
                                <label class="Req">
                                    <asp:Literal ID="ltrSmallMessage" Text="<%$ Resources:CommonCaption, EmailAsUserName %>" runat="server" ClientIDMode="Static"></asp:Literal>
                                    :</label>
                            </div>
                            <div class="LeftContent">
                                <asp:TextBox ID="UserName" autocomplete="off" meta:resourceKey="UserName" runat="server" placeholder="Email Address as User Name *" ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator
                                    ID="UserNameRequired" SetFocusOnError="true"
                                    runat="server"
                                    ControlToValidate="UserName"
                                    ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired %>"
                                    ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired %>"
                                    ValidationGroup="PasswordRecovery1"
                                    CssClass="Error"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UserName" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:CommonCaption, EmailValidErrorMessage %>" ValidationGroup="PasswordRecovery1" Display="Dynamic" ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression%>'
                                    CssClass="Error"></asp:RegularExpressionValidator>
                            </div>
                        </div>

                        <%--<div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Label ID="EmailLabel" runat="server" EnableViewState="false" AssociatedControlID="Email" ClientIDMode="Static">
                                        <asp:Localize ID="Localize2" meta:resourceKey="txtEmail" EnableViewState="false" runat="server" ClientIDMode="Static" /></asp:Label>

                                </div>
                                <div>
                                    <asp:TextBox ID="Email" meta:resourceKey="Email" EnableViewState="false" TabIndex="2" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Email" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:CommonCaption, SubEmailAddressRequired %>" ToolTip="<%$ Resources:CommonCaption, SubEmailAddressRequired %>" CssClass="Error" ValidationGroup="PasswordRecovery1" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="Email" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:CommonCaption, EmailValidErrorMessage %>" ValidationGroup="PasswordRecovery1" Display="Dynamic" ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression%>'
                                        CssClass="Error"></asp:RegularExpressionValidator>
                                </div>
                            </div>--%>
                        <div class="Row Clear">
                            <div class="FailureText LeftContent Error">
                                <div class="Error" id="errorMsg" runat="server" enableviewstate="false" visible="false">
                                    <span class="uxMsg ErrorSection">
                                        <em>
                                            <asp:Literal ID="FailureText" runat="server" EnableViewState="false" ClientIDMode="Static"></asp:Literal>
                                        </em></span>
                                </div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle TextNone">&nbsp;</div>
                            <div class="Input-box">
                                <asp:LinkButton ID="ibSubmit" runat="server" EnableViewState="false" meta:resourceKey="Submit" CommandName="MoveNext" ValidationGroup="PasswordRecovery1" CssClass="Button Submit">
                                    <asp:Localize ID="Localize6" meta:resourceKey="txtSubmit" runat="server" ClientIDMode="Static" />
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div class="Clear">
                            <ZNode:spacer ID="Spacer18" SpacerHeight="10" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:spacer>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <CustomNavigationTemplate></CustomNavigationTemplate>
            </asp:TemplatedWizardStep>

            <asp:TemplatedWizardStep ID="step2" runat="server" StepType="Finish" AllowReturn="False">
                <ContentTemplate>
                    <div>
                        <ZNode:spacer ID="Spacer3" SpacerWidth="5" SpacerHeight="10" runat="server" EnableViewState="false" ClientIDMode="Static" />
                    </div>
                    <p class="FormTitle">
                        <asp:Localize ID="Localize11" meta:resourceKey="txtPlsAnswer" runat="server" EnableViewState="false" ClientIDMode="Static" />
                    </p>
                    <asp:Panel ID="QuestionPanel" DefaultButton="FinishButton" runat="server" ClientIDMode="Static">
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent">
                                <asp:Localize ID="Localize4" meta:resourceKey="txtSecretQuestion" runat="server" ClientIDMode="Static" />
                            </div>
                            <div class="FieldStyle LeftContent">
                                <asp:Literal ID="Question" EnableViewState="false" runat="server" ClientIDMode="Static"></asp:Literal>
                            </div>
                        </div>
                        <div class="Row Clear SecretAnswer">
                            <div class="FieldStyle LeftContent">
                                <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer" ClientIDMode="Static">
                                    <asp:Localize ID="Localize5" meta:resourceKey="txtSecretAnswer" runat="server" ClientIDMode="Static" /></asp:Label>
                            </div>
                            <div class="ValueField Input-box">
                                <asp:TextBox ID="Answer" runat="server" meta:resourceKey="SecretAnswer"  ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer" ErrorMessage="<%$ Resources:CommonCaption, AnswerRequired %>" ToolTip="<%$ Resources:CommonCaption, AnswerRequired %>" ValidationGroup="PasswordRecovery1" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                                <span class="Error">
                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False" ClientIDMode="Static"></asp:Literal></span>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FailureText LeftContent Error">
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle TextNone">&nbsp;</div>
                            <div class="LeftContent FinishButton">
                                <asp:LinkButton ID="FinishButton" runat="server" meta:resourceKey="Submit" CommandName="MoveComplete" ValidationGroup="PasswordRecovery1" CssClass="Button">
                                    <asp:Localize ID="Localize6" meta:resourceKey="txtSubmit" runat="server" ClientIDMode="Static" />
                                </asp:LinkButton>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <CustomNavigationTemplate></CustomNavigationTemplate>
            </asp:TemplatedWizardStep>

            <asp:WizardStep ID="step3" runat="server" StepType="Complete" >
                <ZNode:spacer SpacerWidth="15" SpacerHeight="15" ID="Spacer1" runat="server" />
                <div class="SuccessMsg">
                    <span class="uxMsg SuccessSection"><em>
                        <asp:Literal Text="<%$ Resources:CommonCaption, PasswordSent %>" ID="Message" runat="server" EnableViewState="False" ClientIDMode="Static"></asp:Literal>
                    </em></span>
                </div>
                <ZNode:spacer SpacerWidth="15" SpacerHeight="10" ID="Spacer2" runat="server" />
                <div class="BackLink">
                    <a id="BackLink" runat="server" href="~/account.aspx">
                        <asp:Localize ID="Localize5" meta:resourceKey="txtBack" runat="server" ClientIDMode="Static" /></a>
                </div>
                <div>
                    <ZNode:spacer ID="Spacer" SpacerWidth="5" SpacerHeight="25" runat="server" ClientIDMode="Static" />
                </div>
            </asp:WizardStep>
        </WizardSteps>
        <StepNextButtonStyle />
        <StartNextButtonStyle />
    </asp:Wizard>
    <%--End--%>
</div>

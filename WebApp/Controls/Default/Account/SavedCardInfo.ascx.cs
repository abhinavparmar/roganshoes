﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Saved Card Info user control class.
    /// </summary>
    public partial class Controls_Default_Account_SavedCardInfo : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeUserAccount _userAccount;
        private int accountId = 0;
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // get the user account from session
            this._userAccount = ZNodeUserAccount.CurrentAccount();
            this.accountId = this._userAccount.AccountID;
            this.BindSavedPaymentMethod();
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Saved Payment Method Grid
        /// </summary>
        protected void BindSavedPaymentMethod()
        {
            PaymentTokenService savedcardService = new PaymentTokenService();
            TList<PaymentToken> savedCardList = savedcardService.GetByAccountID(this.accountId);
            ZNodeEncryption encrypt = new ZNodeEncryption();
            foreach (PaymentToken spaymentmethod in savedCardList)
            {
                spaymentmethod.CreditCardDescription = spaymentmethod.CreditCardDescription;
            }

            uxGrid.DataSource = savedCardList;
            uxGrid.DataBind();
        }
        #endregion

        #region Grid Events
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                string Id = e.CommandArgument.ToString();
                ZNodePayment payment = new ZNodePayment();
                payment.DeleteGatewayToken(Convert.ToInt32(Id));
                this.BindSavedPaymentMethod();
            }
        }

        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindSavedPaymentMethod();
        }
        #endregion
    }
}
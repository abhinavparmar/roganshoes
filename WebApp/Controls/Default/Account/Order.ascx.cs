using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Order user control class.
    /// </summary>
    public partial class Controls_Default_Account_Order : System.Web.UI.UserControl
    {
        #region Private Variables
        private int _itemID;
        private ZNodeUserAccount _userAccount;
        private string _AccountPageLink = string.Empty;
        private string _OrderNumber = string.Empty;
        private string _OrderDate = string.Empty;
        private string _OrderTotal = string.Empty;
        private string _PaymentMethod = string.Empty;
        private string _PurchaseOrderNumber = string.Empty;
        private string _TransactionID = string.Empty;
        private string _TrackingNumber = string.Empty;
        private string _BillingAddress = string.Empty;
        private string _ShippingAddress = string.Empty;
        private bool _DigitalAssetExist = false;
        private bool _DownloadLinkExist = false;
        private string _DownLoadLinkText = string.Empty;
        private string _GiftCard = string.Empty;
        private int orderPageSize = 8;//Zeon Custom Code

        #endregion

        #region Public Properties

        /// <summary>
        /// Gers or sets the account page link
        /// </summary>
        public string AccountPageLink
        {
            get { return _AccountPageLink; }
            set { _AccountPageLink = value; }
        }

        /// <summary>
        /// Gers or sets the order number
        /// </summary>
        public string OrderNumber
        {
            get { return _OrderNumber; }
            set { _OrderNumber = value; }
        }

        /// <summary>
        /// Gers or sets the order date.
        /// </summary>
        public string OrderDate
        {
            get { return Convert.ToDateTime(_OrderDate).ToString("MM/dd/yyyy hh:mm tt"); }
            set { _OrderDate = value; }
        }

        /// <summary>
        /// Gers or sets the order total.
        /// </summary>
        public string OrderTotal
        {
            get { return _OrderTotal; }
            set { _OrderTotal = value; }
        }

        /// <summary>
        /// Gers or sets the payment method.
        /// </summary>
        public string PaymentMethod
        {
            get { return _PaymentMethod; }
            set { _PaymentMethod = value; }
        }

        /// <summary>
        /// Gers or sets the purchase order number
        /// </summary>
        public string PurchaseOrderNumber
        {
            get { return _PurchaseOrderNumber; }
            set { _PurchaseOrderNumber = value; }
        }

        /// <summary>
        /// Gers or sets the tracnsaction Id.
        /// </summary>
        public string TransactionID
        {
            get { return _TransactionID; }
            set { _TransactionID = value; }
        }

        /// <summary>
        /// Gers or sets the tracking number.
        /// </summary>
        public string TrackingNumber
        {
            get { return _TrackingNumber; }
            set { _TrackingNumber = value; }
        }

        /// <summary>
        /// Gers or sets the billing address.
        /// </summary>
        public string BillingAddress
        {
            get { return _BillingAddress; }
            set { _BillingAddress = value; }
        }

        /// <summary>
        /// Gers or sets the shipping address.
        /// </summary>
        public string ShippingAddress
        {
            get { return _ShippingAddress; }
            set { _ShippingAddress = value; }
        }

        /// <summary>
        /// Gers or sets a value indicating whether the digital assets exists for the order.
        /// </summary>
        public bool DigitalAssetExist
        {
            get { return _DigitalAssetExist; }
            set { _DigitalAssetExist = value; }
        }

        /// <summary>
        /// Gers or sets a value indicating whether the download link exist for the order.
        /// </summary>
        public bool DownloadLinkExist
        {
            get { return _DownloadLinkExist; }
            set { _DownloadLinkExist = value; }
        }

        /// <summary>
        /// Gers or sets the download link text
        /// </summary>
        public string DownLoadLinkText
        {
            get { return _DownLoadLinkText; }
            set { _DownLoadLinkText = value; }
        }

        /// <summary>
        /// Gers or sets the gift card number
        /// </summary>
        public string GiftCard
        {
            get { return _GiftCard; }
            set { _GiftCard = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns payment type name for this payment type id
        /// </summary>
        /// <param name="PaymentTypeId">Payment Type Id</param>
        /// <returns>Returns the Payment Type Name</returns>
        public string GetPaymentTypeName(int PaymentTypeId)
        {
            string Name = string.Empty;
            ZNode.Libraries.Admin.StoreSettingsAdmin settingsAdmin = new ZNode.Libraries.Admin.StoreSettingsAdmin();
            PaymentType entity = settingsAdmin.GetPaymentTypeById(PaymentTypeId);
            if (entity != null)
            {
                Name = entity.Name;
            }

            return Name;
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Returns all digital assets for this Order line Item
        /// </summary>
        /// <param name="OrderLineItemId">Order Line Item Id</param>
        /// <returns>Returns the Digital Asset value</returns>
        protected string GetDigitalAssets(int OrderLineItemId)
        {
            ZNode.Libraries.DataAccess.Service.DigitalAssetService _digitalAssetService = new ZNode.Libraries.DataAccess.Service.DigitalAssetService();
            ZNode.Libraries.DataAccess.Entities.TList<ZNode.Libraries.DataAccess.Entities.DigitalAsset> digitalAssetList = _digitalAssetService.GetByOrderLineItemID(OrderLineItemId);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            foreach (ZNode.Libraries.DataAccess.Entities.DigitalAsset digitalAsset in digitalAssetList)
            {
                sb.Append(digitalAsset.DigitalAsset + "<br />");
                this.DigitalAssetExist = true;
            }

            return sb.ToString();
        }

        protected string GetDownloadLinkText(object DownLoadLinkText)
        {
            if (DownLoadLinkText == null)
            {
                return string.Empty;
            }

            return DownLoadLinkText.ToString();
        }

        protected bool ShowReorder(object ParentOrderLineItemId)
        {
            if (ParentOrderLineItemId == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Handeles Datalist item command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dlistOrder_OnItemCommand(object sender, DataListCommandEventArgs e)
        {
            if (string.Compare(e.CommandName, "Reorder", true) == 0)
            {
                HiddenField hiddenid = (HiddenField)e.Item.FindControl("hiddenOrderlineitemid");
                HiddenField hiddensku = (HiddenField)e.Item.FindControl("hiddenSKU");
                HiddenField hiddenprodnum = (HiddenField)e.Item.FindControl("hiddenProductNum");
                if (hiddenid != null && hiddensku != null && hiddenprodnum != null)
                {
                    // Get OrderlineItemId from Selected row
                    int _orderlineitemid;
                    if (!int.TryParse(hiddenid.Value, out _orderlineitemid))
                    {
                        _orderlineitemid = 0;
                    }
                    // Get product SKU from Selected row
                    string SKU = hiddensku.Value;

                    // Get ProductNum from Selected row
                    string _productNum = hiddenprodnum.Value;

                    // Get product Quantity from Selected row
                    int _quantity;
                    Literal ltrQuantity = (Literal)e.Item.FindControl("ltrQuantity");
                    if (!int.TryParse(ltrQuantity.Text, out _quantity))
                    {
                        _quantity = 1;
                    }
                    Literal ltrProductName = (Literal)e.Item.FindControl("ltrProductName");
                    if (SKU.Length == 0 && _productNum.Length == 0)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "AlertBox", "alert('We are sorry but " + ltrProductName.Text + " is no longer available.');", true);
                        return;
                    }

                    Reorder reorder = new Reorder();
                    if (reorder.ReorderItem(_orderlineitemid, _productNum, SKU, _quantity))
                    {
                        string link = "~/shoppingcart.aspx";
                        Response.Redirect(link);
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "AlertBox", "alert('We are sorry but " + ltrProductName.Text + " is no longer available.');", true);
                        return;
                    }
                }
            }
        }

        #endregion

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.QueryString["itemid"] != null)
            {
                this._itemID = int.Parse(Request.QueryString["itemid"]);
            }
            else
            {
                this._itemID = 0;
                Response.Redirect("account.aspx");
            }

            this.OrderNumber = this._itemID.ToString();

            // If the user has not logged in
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/login.aspx?ReturnURL=order.aspx?itemId=" + Request.QueryString["itemid"]);
            }

            this._userAccount = ZNodeUserAccount.CurrentAccount();

            // Bind grid data
            //this.Bind();//Znode Old Code
            BindOrderLineItemWithPaging();//Zeon Custom Code

            // Account page link
            ZNodeUrl url = new ZNodeUrl();
            this.AccountPageLink = url.GetURLFromPageId("account");

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "Title");
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Order data
        /// </summary>
        private TList<OrderLineItem> Bind()
        {
            // First retrieve order info by orderid to check if it valid for the current account
            ZNode.Libraries.DataAccess.Service.OrderService ordServ = new OrderService();
            ZNode.Libraries.DataAccess.Entities.Order ord = ordServ.GetByOrderID(this._itemID);
            TList<OrderLineItem> ordLineItems = new TList<OrderLineItem>();

            // Account id on order matches current user's account id
            if (ord.AccountID == this._userAccount.AccountID)
            {
                GiftCardHistoryService giftCardHistoryService = new GiftCardHistoryService();
                TList<GiftCardHistory> cardHistoryList = giftCardHistoryService.GetByOrderID(this._itemID);
                if (cardHistoryList.Count == 0)
                {
                    this.GiftCard = "0.00";
                }
                else
                {
                    this.GiftCard = "-" + cardHistoryList[0].TransactionAmount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                }

                this.OrderDate = ord.OrderDate.ToString();
                this.OrderTotal = ((decimal)ord.Total).ToString("c");
                if (ord.PaymentTypeId.HasValue)
                {
                    string paymentTypeName = this.GetPaymentTypeName(ord.PaymentTypeId.Value);
                    this.PaymentMethod = paymentTypeName;
                }

                this.TransactionID = ord.CardTransactionID;
                //this.TrackingNumber = ord.TrackingNumber;//Old Code
                this.TrackingNumber = !string.IsNullOrEmpty(ord.Custom1) ? GetOrderTrackingNumber(ord.Custom1) : string.Empty;
                this.PurchaseOrderNumber = ord.PurchaseOrderNumber;

                // Print billing address info
                Address billingAddress = new Address();
                billingAddress.FirstName = ord.BillingFirstName;
                billingAddress.LastName = ord.BillingLastName;
                billingAddress.Street = ord.BillingStreet;
                billingAddress.Street1 = ord.BillingStreet1;
                billingAddress.City = ord.BillingCity;
                billingAddress.StateCode = ord.BillingStateCode;
                billingAddress.PostalCode = ord.BillingPostalCode;
                billingAddress.CountryCode = ord.BillingCountry;
                billingAddress.CompanyName = ord.BillingCompanyName;
                billingAddress.PhoneNumber = ord.BillingPhoneNumber;
                this.BillingAddress = ZCommonHelper.GetAddressString(billingAddress);//Zeon Custom Code

                // Print shipping address info
                Address shippingAddress = new Address();
                shippingAddress.FirstName = ord.ShipFirstName;
                shippingAddress.LastName = ord.ShipLastName;
                shippingAddress.Street = ord.ShipStreet;
                shippingAddress.Street1 = ord.ShipStreet1;
                shippingAddress.City = ord.ShipCity;
                shippingAddress.StateCode = ord.ShipStateCode;
                shippingAddress.PostalCode = ord.ShipPostalCode;
                shippingAddress.CountryCode = ord.ShipCountry;
                shippingAddress.CompanyName = ord.ShipCompanyName;
                shippingAddress.PhoneNumber = ord.ShipPhoneNumber;
                this.ShippingAddress = ZCommonHelper.GetAddressString(shippingAddress);//Zeon Custom Code

                ZNode.Libraries.DataAccess.Service.OrderLineItemService oliServ = new OrderLineItemService();
                ordLineItems = oliServ.GetByOrderID(this._itemID);
            }
            return ordLineItems;
        }
        #endregion

        #region Zeon Custom Code

        /// <summary>
        /// Get OrderLineItemString
        /// </summary>
        /// <param name="OrderLineItemID">string</param>
        /// <returns>string</returns>
        protected string GetLineItemNotes(string orderLineItemID, object parentOrderLineItem)
        {
            string notesString = string.Empty;
            if (parentOrderLineItem == null)
            {
                ZNode.Libraries.DataAccess.Service.OrderLineItemExtnService orderLineExtnSer = new ZNode.Libraries.DataAccess.Service.OrderLineItemExtnService();
                TList<OrderLineItemExtn> lineExtnList = orderLineExtnSer.GetByOrderLineItemID(int.Parse(orderLineItemID));
                if (lineExtnList != null && lineExtnList.Count > 0 && !string.IsNullOrEmpty(lineExtnList[0].Notes))
                {
                    notesString = this.GetLocalResourceObject("NotesHeading").ToString() + lineExtnList[0].Notes;
                }
            }
            return notesString;
        }

        #region Paging Section

        /// <summary>
        /// Bind Pager Control for list on the page
        /// </summary>
        /// <param name="containerControl">PlaceHolder</param>
        /// <param name="listPageCount">int</param>
        /// <param name="listName">string</param>
        private void CreatePagingControl(int listPageCount, int currentPage)
        {
            StringBuilder pagerList = new StringBuilder();
            ucOrderItemPager.Controls.Clear();
            for (int pagecount = 1; pagecount <= listPageCount; pagecount++)
            {
                LinkButton lnkPager = new LinkButton();
                lnkPager.Text = pagecount.ToString();
                if (currentPage == pagecount)
                {
                    lnkPager.Enabled = false;
                    lnkPager.CssClass = "GridPaging";
                }
                else
                {
                    lnkPager.CssClass = "GridPaging Active";
                }
                lnkPager.CommandArgument = pagecount.ToString();
                lnkPager.Click += new EventHandler(this.lnkPager_Click);
                pagerList.Append(lnkPager);
                ucOrderItemPager.Controls.Add(lnkPager);
            }
        }

        /// <summary>
        /// Handles Order Pager click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkPager_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            hdnOrderItemPage.Value = btn.CommandArgument;
            BindOrderLineItemWithPaging();
            dlistOrder.Focus();
        }

        /// <summary>
        /// Bind Orderline item with paging
        /// </summary>
        /// <param name="orderLineItems"></param>
        private void BindOrderLineItemWithPaging()
        {
            TList<OrderLineItem> _orderLineItemList = new TList<OrderLineItem>();
            _orderLineItemList = ViewState["OrderItemList"] == null ? this.Bind() : (TList<OrderLineItem>)ViewState["OrderItemList"];
            PagedDataSource objPage = new PagedDataSource();
            objPage.DataSource = _orderLineItemList;
            objPage.AllowPaging = true;
            objPage.PageSize = orderPageSize;
            if (objPage.PageCount > 1)
            {
                objPage.CurrentPageIndex = !string.IsNullOrEmpty(hdnOrderItemPage.Value) && int.Parse(hdnOrderItemPage.Value) <= objPage.PageCount ? int.Parse(hdnOrderItemPage.Value) - 1 : 0;
                CreatePagingControl(objPage.PageCount, objPage.CurrentPageIndex + 1);
                DivPager.Visible = true;
            }
            else
            {
                DivPager.Visible = false;
            }

            dlistOrder.DataSource = objPage;
            dlistOrder.DataBind();
        }

        /// <summary>
        /// Get Order Tracking Number
        /// </summary>
        /// <param name="custom1">string</param>
        /// <returns>string</returns>
        private string GetOrderTrackingNumber(string custom1)
        {
            string trackingNumber = string.Empty;
            zShippingInformation shippingInformation = new zShippingInformation();
            var shippingInfo = shippingInformation.GetShippingInformation(custom1);
            if (shippingInfo != null && shippingInfo.ShippingInfo != null && shippingInfo.ShippingInfo.Count > 0)
            {
                StringBuilder strShippingInformation = new StringBuilder();
                foreach (ShippingInfo ShippingInfo in shippingInfo.ShippingInfo)
                {
                    string trackingURL = string.Empty;
                    string anchor = "<a target=\"_blank\" href=\"#href#\">#TrackingNumber# #Name#</a>";
                    if (ShippingInfo.Name.ToLower().Contains("ups"))
                    {
                        trackingURL = HttpContext.Current.Server.HtmlDecode(ConfigurationManager.AppSettings["UPS"].ToString());

                    }
                    else if (ShippingInfo.Name.ToLower().Contains("speedee"))
                    {
                        trackingURL = HttpContext.Current.Server.HtmlDecode(ConfigurationManager.AppSettings["SPEEDY"].ToString());

                    }
                    else if (ShippingInfo.Name.ToLower().Contains("usps"))
                    {
                        trackingURL = HttpContext.Current.Server.HtmlDecode(ConfigurationManager.AppSettings["USPS"].ToString());

                    }
                    else if (ShippingInfo.Name.ToLower().Contains("fedex"))
                    {
                        trackingURL = HttpContext.Current.Server.HtmlDecode(ConfigurationManager.AppSettings["FEDEX"].ToString());
                    }
                    anchor = anchor.Replace("#href#", trackingURL);
                    anchor = anchor.Replace("#TrackingNumber#", ShippingInfo.TrackingNumber).Replace("#Name#", " (" + ShippingInfo.Name + ")");
                    strShippingInformation.Append(anchor);
                    strShippingInformation.Append("<br/>");
                }

                trackingNumber = strShippingInformation.ToString();
            }
            return trackingNumber;
        }

        #endregion

        #endregion
    }
}
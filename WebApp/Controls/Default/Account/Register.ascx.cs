using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.ShoppingCart;
using System.IO;
using System.Text.RegularExpressions;
using ZNode.Libraries.ECommerce.Utilities;
using Zeon.Libraries.Utilities;
using Zeon.Libraries.Elmah;
using ZNode.Libraries.DataAccess.Custom;


namespace WebApp
{
    /// <summary>
    /// Represents the Register user control class.
    /// </summary>
    public partial class Controls_Default_Account_Register : System.Web.UI.UserControl
    {
        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Zeon Custom Code: Start
            // Zeon Custom Implementation:Replacing UserId with Email:Task Sprint 2 all over page
            txtFirstName.Focus();
            errorMsg.Visible = false;
            //Zeon Custom Code: End
            // Check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");
                    Response.Redirect(outURL);
                }
            }

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtAccountTitle.Text");
            }

            if (!Page.IsPostBack)
            {
                // Add the listrack email capture script.
                ZNodeAnalytics analytics = new ZNodeAnalytics();
                analytics.AnalyticsData.EmailCaptureContorlId = Email.ClientID;
                analytics.Bind();
                string emailCaptureScript = analytics.AnalyticsData.EmailCaptureScript;
                if (!Page.ClientScript.IsStartupScriptRegistered("EmailCapture"))
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "EmailCapture", emailCaptureScript.ToString());
                }
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Event fired when Register link button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LbRegister_Click(object sender, EventArgs e)
        {
            this.RegisterUser();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// This function registers a new user 
        /// </summary>
        private void RegisterUser()
        {
            string newUsername = UserName.Text.Trim() + "_" + ZNodeConfigManager.SiteConfig.PortalID.ToString();//Zeon Custom Code

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
            log.LogActivityTimerStart();

            int profileId = ZNodeProfile.DefaultRegisteredProfileId;
            try
            {
                if (profileId == 0)
                {
                    ErrorMessage.Text = Resources.CommonCaption.CreateFailed;
                    errorMsg.Visible = true;
                    return;
                }

                // Check if loginName already exists in DB
                ZNodeUserAccount userAccount = new ZNodeUserAccount();
                //Zeon Custom Code:Replace UserName with Email in all Places
                if (!userAccount.IsLoginNameAvailable(ZNodeConfigManager.SiteConfig.PortalID, newUsername))
                {
                    log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, newUsername, null, null, "User name already exists", null);
                    ErrorMessage.Text = Resources.CommonCaption.AlreadyExist;
                    errorMsg.Visible = true;
                    return;
                }

                // Create user membership
                //Zeon Custom Code:Commented as part of removing security Question and answer:Starts
                // MembershipCreateStatus memStatus;
                // MembershipUser user = Membership.CreateUser(newUsername, Password.Text.Trim(), Email.Text, ddlSecretQuestions.SelectedItem.Text, Answer.Text, true, out memStatus);
                MembershipUser user = Membership.CreateUser(newUsername, Password.Text.Trim(), Email.Text);
                //Zeon Custom Code:Commented as part of removing security Question and answer:Ends

                //Zeon Custom Code:Commented as part of removing security Question and answer:Starts
                //if (memStatus != MembershipCreateStatus.Success)
                //Zeon Custom Code:Commented as part of removing security Question and answer:Ends
                if (!user.IsApproved)
                {
                    log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, newUsername);
                    ErrorMessage.Text = Resources.CommonCaption.CreateFailed;
                    errorMsg.Visible = true;
                    return;
                }

                log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, newUsername);

                // Create the user account
                ZNodeUserAccount newUserAccount = new ZNodeUserAccount();
                newUserAccount.UserID = (Guid)user.ProviderUserKey;
                newUserAccount.EmailOptIn = chkOptIn.Checked;
                newUserAccount.EmailID = Email.Text;

                // Copy info to billing
                Address billingAddress = new Address();
                newUserAccount.BillingAddress = billingAddress;

                // Copy info to shipping
                Address shippingAddress = new Address();
                newUserAccount.ShippingAddress = shippingAddress;

                // Affiliate Settings
                ZNodeTracking tracker = new ZNodeTracking();
                string referralAffiliate = tracker.AffiliateId;
                int referralAccountId = 0;

                if (!string.IsNullOrEmpty(referralAffiliate))
                {
                    if (int.TryParse(referralAffiliate, out referralAccountId))
                    {
                        ZNode.Libraries.DataAccess.Service.AccountService accountServ = new ZNode.Libraries.DataAccess.Service.AccountService();
                        ZNode.Libraries.DataAccess.Entities.Account account = accountServ.GetByAccountID(referralAccountId);

                        // Affiliate account exists
                        if (account != null)
                        {
                            if (account.ReferralStatus == "A")
                            {
                                newUserAccount.ReferralAccountId = account.AccountID;
                            }
                        }
                    }
                }

                // Register account
                newUserAccount.AddUserAccount();

                AddressService addressService = new AddressService();
                billingAddress.Name = "Default Billing";
                billingAddress.FirstName = txtFirstName.Text;
                billingAddress.MiddleName = string.Empty;
                billingAddress.LastName = txtLastName.Text;
                billingAddress.CompanyName = string.Empty;
                billingAddress.Street = string.Empty;
                billingAddress.Street1 = string.Empty;
                billingAddress.City = string.Empty;
                billingAddress.StateCode = string.Empty;
                billingAddress.PostalCode = string.Empty;
                billingAddress.CountryCode = string.Empty;
                billingAddress.PhoneNumber = string.Empty;
                billingAddress.IsDefaultBilling = true;
                billingAddress.IsDefaultShipping = false;
                billingAddress.AccountID = newUserAccount.AccountID;
                addressService.Insert(billingAddress);
                newUserAccount.SetBillingAddress(billingAddress.AddressID);

                shippingAddress.Name = "Default Shipping";
                shippingAddress.FirstName = string.Empty;
                shippingAddress.MiddleName = string.Empty;
                shippingAddress.LastName = string.Empty;
                shippingAddress.CompanyName = string.Empty;
                shippingAddress.Street = string.Empty;
                shippingAddress.Street1 = string.Empty;
                shippingAddress.City = string.Empty;
                shippingAddress.StateCode = string.Empty;
                shippingAddress.PostalCode = string.Empty;
                shippingAddress.CountryCode = string.Empty;
                shippingAddress.PhoneNumber = string.Empty;
                shippingAddress.AccountID = newUserAccount.AccountID;
                shippingAddress.IsDefaultBilling = false;
                shippingAddress.IsDefaultShipping = true;
                addressService.Insert(shippingAddress);
                newUserAccount.SetShippingAddress(shippingAddress.AddressID);

                // Add Account Profile
                AccountProfile accountProfile = new AccountProfile();
                accountProfile.AccountID = newUserAccount.AccountID;
                accountProfile.ProfileID = profileId;

                AccountProfileService accountProfileServ = new AccountProfileService();
                accountProfileServ.Insert(accountProfile);

                // Login the user
                ZNodeUserAccount userAcct = new ZNodeUserAccount();
                bool loginSuccess = userAcct.Login(ZNodeConfigManager.SiteConfig.PortalID, newUsername, Password.Text.Trim());

                if (loginSuccess)
                {
                    // Set current user Profile.
                    userAcct.ProfileID = accountProfile.ProfileID.Value;

                    // Get account and set to session
                    Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), userAcct);

                    // Log password for further debugging
                    ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, Password.Text.Trim());

                    // Get Profile entity for logged-in user profileId
                    ZNode.Libraries.DataAccess.Service.ProfileService _ProfileService = new ZNode.Libraries.DataAccess.Service.ProfileService();
                    ZNode.Libraries.DataAccess.Entities.Profile _Profile = _ProfileService.GetByProfileID(userAcct.ProfileID);

                    // Hold this profile object in the session state
                    HttpContext.Current.Session["ProfileCache"] = _Profile;

                    // Make an entry in tracking event for Account Creation            
                    if (tracker.AffiliateId.Length > 0)
                    {
                        tracker.AccountID = accountProfile.AccountID;
                        tracker.LogTrackingEvent("Created an Account");
                    }

                    //Zeon Custom Code: Start
                    SendAccountConfirmationEmail(UserName.Text.Trim());
                    if (chkOptIn.Checked) { SubscribeEmailUser(); }
                    //Zeon Custom Code: End

                    ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart savedCart = new ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart();
                    savedCart.Merge();

                    if (Request.QueryString["ReturnUrl"] != null)
                    {
                        FormsAuthentication.RedirectFromLoginPage(newUsername, false);
                    }
                    else
                    {
                        FormsAuthentication.SetAuthCookie(newUsername, false);
                        Response.Redirect("~/");
                    }

                    ZNodeCacheDependencyManager.Remove("SpecialsNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + HttpContext.Current.Session.SessionID);

                    if (System.Web.HttpContext.Current.Session["IsCartChanged"] != null)
                    {
                        Response.Redirect("~/shoppingcart.aspx", true);
                    }
                }
                else
                {
                    ErrorMessage.Text = this.GetLocalResourceObject("InvalidAccountInfo").ToString();
                    errorMsg.Visible = true;
                }
            }
            catch (Exception e)
            {
                bool isSuccess = Membership.DeleteUser(newUsername, true);
                log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, newUsername + e.StackTrace + "Membership Data deleted : " + isSuccess);
                ErrorMessage.Text = Resources.CommonCaption.CreateFailed;
                errorMsg.Visible = true;

                return;
            }
        }



        #endregion

        #region Zeon Custom  Methods

        /// <summary>
        /// To Account confirmation Email
        /// </summary>
        /// <param name="Email">Email Id of the register user</param>
        ///
        private void SendAccountConfirmationEmail(string email)
        {
            try
            {
                string accountConfirmationEmailTemplate = GetAccountConfirmationEmailTemplate();
                ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();

                if (!string.IsNullOrEmpty(accountConfirmationEmailTemplate))
                {
                    ZNodeEmail.SendEmail(email, ZNodeConfigManager.SiteConfig.CustomerServiceEmail, string.Empty, zeonMessageConfig.GetMessageKey("EmailConfirmationSubject"), accountConfirmationEmailTemplate, true);
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Register user email template sending problem(Mehtod name: SendAccountConfirmationEmail()):" + ex.StackTrace);
            }
        }

        /// <summary>
        /// To Get Account Confriumation email template with body content
        /// </summary>
        /// <returns></returns>
        private string GetAccountConfirmationEmailTemplate()
        {
            string userName = string.Empty;
            string password = string.Empty;
            string defaultTemplatePath = string.Empty;
            string portalLogo = string.Empty;
            bool isChecked = false;

            userName = UserName.Text.Trim().Replace("_" + ZNodeConfigManager.SiteConfig.PortalID.ToString(), "");
            password = Password.Text.Trim();
            isChecked = chkOptIn.Checked;
            portalLogo = ZCommonHelper.GetLogoHTML();

            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ZAccountConfirmationEmail.html"));

            StreamReader streamReader = new StreamReader(defaultTemplatePath);
            string messageText = streamReader.ReadToEnd();

            string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
            string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
            int portalId = ZNodeConfigManager.SiteConfig.PortalID;

            ZEmailHelper zEmailHelper = new ZEmailHelper();
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

            string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
            string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

            //To add header to the template
            Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
            messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

            //To add footer to the template
            Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
            messageText = rxFooterContent.Replace(messageText, messageTextFooter);

            Regex rxUserName = new Regex("#UserName#", RegexOptions.IgnoreCase);
            messageText = rxUserName.Replace(messageText, userName);

            //Regex rxPassword = new Regex("#Password#", RegexOptions.IgnoreCase);
            //messageText = rxPassword.Replace(messageText, password);

            Regex rxisChecked = new Regex("#not#", RegexOptions.IgnoreCase);

            Regex rxUserFirstName = new Regex("#UserFirstName#", RegexOptions.IgnoreCase);
            messageText = rxUserFirstName.Replace(messageText, txtFirstName.Text.Trim());

            string userMessage = isChecked ? this.GetLocalResourceObject("AccountWithSubcribtionMsg").ToString() : this.GetLocalResourceObject("AccountWithoutSubcribtionMsg").ToString();
            Regex UserMessage = new Regex("#MessageForUser#", RegexOptions.IgnoreCase);
            messageText = UserMessage.Replace(messageText, userMessage);

            messageText = isChecked ? rxisChecked.Replace(messageText, this.GetLocalResourceObject("EmailSubscriptionMessage").ToString()) : rxisChecked.Replace(messageText, "");

            return messageText;
        }

        /// <summary>
        /// Subscribe User For Email Service
        /// </summary>
        private void SubscribeEmailUser()
        {
            string responseContactID = string.Empty;
            try
            {
                if (ConfigurationManager.AppSettings["GetResponseAPIKey"] != null && ConfigurationManager.AppSettings["GetResponseAPIURL"] != null)
                {
                    string newsLetterCampaign = GetCompaignNameByPortal();
                    if (!string.IsNullOrEmpty(newsLetterCampaign))
                    {
                        GetResponseEmailSubscriber emailSubscriber = new GetResponseEmailSubscriber();
                        string campaignID = emailSubscriber.AddContactEmailSubcriber(ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString(), ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString(), newsLetterCampaign, Email.Text.Trim(), Email.Text.Trim());

                        //string contactid = emailSubscriber.GetContactEmailSubcriber(ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString(), ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString(), campaignID, Email.Text.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                ElmahErrorManager.Log(ex.StackTrace);
            }
        }

        /// <summary>
        /// Get Newsletter Compaign Name by Portal
        /// </summary>
        /// <returns></returns>
        private string GetCompaignNameByPortal()
        {
            string compaignName = string.Empty;
            PortalExtnService portalExtnService = new PortalExtnService();
            TList<PortalExtn> currentPortal = portalExtnService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID);
            if (currentPortal != null && currentPortal.Count > 0)
            {
                compaignName = currentPortal[0].NewsLetterCampaignName;
            }

            return compaignName;
        }

        #endregion
    }
}
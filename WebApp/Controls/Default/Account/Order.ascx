<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false"
    Inherits="WebApp.Controls_Default_Account_Order" CodeBehind="Order.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<div>
    <div class="PageTitle">
        <asp:Localize ID="Localize8" meta:resourceKey="txtOrdernumber" runat="server" ClientIDMode="Static" />
        <%=OrderNumber%>
    </div>
</div>
<div class="Form ViewOrderPage">
    <div class="Row">
        <div class="LeftContent Label">

            <asp:Localize ID="Localize1" meta:resourceKey="txtOrderDate" runat="server" ClientIDMode="Static" />
        </div>
        <div class="LeftContent">
            <%=OrderDate%>
        </div>
    </div>
    <div class="Row">
        <div class="LeftContent Label">

            <asp:Localize ID="Localize2" meta:resourceKey="txtOrderTotal" runat="server" ClientIDMode="Static" />
        </div>
        <div class="LeftContent">
            <%=OrderTotal%>
        </div>
    </div>
    <div class="Row">
        <div class="LeftContent Label">

            <asp:Localize ID="Localize11" meta:resourceKey="txtGiftCard" runat="server" ClientIDMode="Static" />
        </div>
        <div class="LeftContent">
            <%=GiftCard%>
        </div>
    </div>
    <div class="Row">
        <div class="LeftContent Label">

            <asp:Localize ID="Localize3" meta:resourceKey="txtPaymentMethod" runat="server" />
        </div>
        <div class="LeftContent">
            <%=PaymentMethod%>
        </div>
    </div>
    <div class="Row">
        <div class="LeftContent Label">

            <asp:Localize ID="Localize4" meta:resourceKey="txtPurchaseNo" runat="server" />
        </div>
        <div class="LeftContent">
            <%=PurchaseOrderNumber%>
        </div>
    </div>
    <div class="Row">
        <div class="LeftContent Label">

            <asp:Localize ID="Localize5" meta:resourceKey="txtTransactionID" runat="server" />
        </div>
        <div class="LeftContent">
            <%=TransactionID%>
        </div>
    </div>
    <div class="Row">
        <div class="LeftContent Label">

            <asp:Localize ID="Localize6" meta:resourceKey="txtTrackingNo" runat="server" />
        </div>
        <div class="LeftContent">
            <%=TrackingNumber%>
        </div>
    </div>
    <div class="Row Clear">
        &nbsp;
    </div>
    <div class="Row">
        <div class="LeftContent Label">

            <asp:Localize ID="Localize7" meta:resourceKey="txtBillTo" runat="server" />
        </div>
        <div class="LeftContent">
            <%=BillingAddress%>
        </div>
    </div>
    <%--<div class="Row Clear">
        &nbsp;
    </div>--%>
    <div class="Row Clear">
        <div class="LeftContent Label">

            <asp:Localize ID="Localize9" meta:resourceKey="txtShipTo" runat="server" />
        </div>
        <div class="LeftContent">
            <%=ShippingAddress%>
        </div>
    </div>
    <div>
        <div class="Clear">
            <ZNode:Spacer ID="Spacer1" SpacerHeight="30" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:Spacer>
        </div>
    </div>
</div>
<div class="OrderHistoryView">
    <asp:DataList ID="dlistOrder" runat="server" ClientIDMode="Static" OnItemCommand="dlistOrder_OnItemCommand" RepeatLayout="Flow" RepeatDirection="Horizontal">
        <HeaderStyle HorizontalAlign="Left" CssClass="HeaderStyle" />
        <HeaderTemplate>
            <header class="col1">
                <asp:Literal ID="ltrImage" runat="server"></asp:Literal>
            </header>
            <header class="col1">
                <asp:Literal ID="ltrQuantityHeader" runat="server" meta:resourceKey="ltrQuantityHeader"></asp:Literal>
            </header>
            <header class="col2">
                <asp:Literal ID="ltrPrdName" runat="server" meta:resourceKey="ltrPrdName"></asp:Literal>
            </header>
            <header class="col3">
                <asp:Literal ID="ltrProductDesc" runat="server" meta:resourceKey="ltrProductDesc"></asp:Literal>
            </header>
            <header class="col4">
                <asp:Literal ID="ltrPriceHeader" runat="server" meta:resourceKey="ltrPriceHeader"></asp:Literal>
            </header>
            <header class="col5">
                <asp:Literal ID="ltrLicKeyHeader" runat="server" meta:resourceKey="ltrLicKeyHeader" Visible="<%#this.DigitalAssetExist %>"></asp:Literal>
            </header>
            <%--<header runat="server" visible='<%#true?!string.IsNullOrEmpty(GetDownloadLinkText(DataBinder.Eval(Container.DataItem, "DownloadLink"))):false%>'>
                <asp:Literal ID="ltrDownloadHeader" runat="server" meta:resourceKey="ltrDownloadHeader"></asp:Literal>
            </header>--%>
        </HeaderTemplate>
        <ItemStyle CssClass="ItemStyle" VerticalAlign="Top" />
        <ItemTemplate>
            <div class="OrderTitle">
                <div class="OrderCol1">Detail# </div>
                <div class="OrderCol2">Total Cost</div>
            </div>
            <div class="col1">
                <img src='<%# DataBinder.Eval(Container.DataItem, "Custom2") %>' alt='<%# DataBinder.Eval(Container.DataItem, "Name") %>' title='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />
            </div>
            <div class="col1">
                <span class="QtyLabel">Qty :</span>
                <asp:Literal ID="ltrQuantity" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>'></asp:Literal>
            </div>
            <div class="col2">
                <asp:Literal ID="ltrProductName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'></asp:Literal>
            </div>
            <div class="col3">
                <%# DataBinder.Eval(Container.DataItem, "Description") %>
                <asp:Literal ID="ltrNote" runat="server" Text='<%# GetLineItemNotes(DataBinder.Eval(Container.DataItem,"OrderLineItemID").ToString(),DataBinder.Eval(Container.DataItem, "ParentOrderLineItemId")) %>' ClientIDMode="Static"></asp:Literal>
                <%-- <div id="divDownloads" runat="server" visible='<%# true?!string.IsNullOrEmpty(GetDownloadLinkText(DataBinder.Eval(Container.DataItem, "DownloadLink"))):false%>'>
                    <asp:HyperLink ID="HDownloadLink" runat="server" NavigateUrl='<%# GetDownloadLink(DataBinder.Eval(Container.DataItem, "DownloadLink"))%>' ClientIDMode="Static">
                        <asp:Localize ID="locDownload" meta:resourceKey="txtDownload" runat="server" ClientIDMode="Static" />
                    </asp:HyperLink>
                </div>--%>
            </div>
            <div class="col4">
                <%# DataBinder.Eval(Container.DataItem, "Price", "{0:c}") %>
                <asp:Literal ID="ltrDigAssets" runat="server" Text='<%# GetDigitalAssets(int.Parse(DataBinder.Eval(Container.DataItem, "OrderLineItemId").ToString()))%>'></asp:Literal>
            </div>


            <div class="col5">
                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Reorder" CssClass="Button"
                    meta:resourceKey="Reorder" Visible='<%# ShowReorder(DataBinder.Eval(Container.DataItem, "ParentOrderLineItemId")) %>' ClientIDMode="Static"></asp:LinkButton>
                <asp:HiddenField ID="hiddenOrderlineitemid" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "OrderLineItemId") %>' ClientIDMode="Static" />
                <asp:HiddenField ID="hiddenSKU" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SKU") %>' ClientIDMode="Static" />
                <asp:HiddenField ID="hiddenProductNum" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ProductNum") %>' ClientIDMode="Static" />
            </div>
        </ItemTemplate>

        <FooterStyle CssClass="FooterStyle" />
        <AlternatingItemStyle CssClass="AlternatingRowStyle" />
    </asp:DataList>
    <div class="AccountPager" id="DivPager" runat="server">
        <asp:PlaceHolder ID="ucOrderItemPager" runat="server"></asp:PlaceHolder>
        <asp:HiddenField ID="hdnOrderItemPage" runat="server" />
    </div>
</div>
<div class="BackButton">
    <a id="A1" href="~/account.aspx" enableviewstate="false" runat="server">
        <asp:Localize ID="Localize10" meta:resourceKey="txtBack" runat="server" /></a>
</div>
<div>
    <ZNode:Spacer ID="Spacer5" EnableViewState="false" SpacerHeight="30" SpacerWidth="10"
        runat="server"></ZNode:Spacer>
</div>

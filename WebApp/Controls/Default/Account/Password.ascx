<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Account_Password" Codebehind="Password.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<div class="Form ChangePassword1">
    <div class="PageTitle">
        <asp:Localize ID="Localize2" meta:resourceKey="txtTitle" runat="server" EnableViewState="false"/></div>
    <div>
        <uc1:spacer ID="Spacer1" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server" ClientIDMode="Static"></uc1:spacer>
    </div>
    <asp:ChangePassword ID="ChangePassword1" runat="server" CancelDestinationPageUrl="~/account.aspx"
        ContinueDestinationPageUrl="~/account.aspx" DisplayUserName="False" ChangePasswordTitleText=""
        OnChangePasswordError="ChangePassword1_ChangePasswordError" OnChangingPassword="ChangePassword1_ChangingPassword" ClientIDMode="Static" CssClass="CommonLableNone">
        <SuccessTemplate>
            <asp:Panel ID="pnlConfirm" runat="server" ClientIDMode="Static">
                <!--Succesfull Message -->
                <asp:Label ID="lblMessage" CssClass="" meta:resourceKey="txtPasswordChanged" runat="server" EnableViewState="false"></asp:Label>
                <uc1:spacer ID="Spacer3" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
                <uc1:spacer ID="Spacer2" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
                <asp:LinkButton ID="ContinuePushButton" runat="server" CssClass="Button" CausesValidation="False"
                    CommandName="Continue" meta:resourceKey="txtContinue" />
            </asp:Panel>
        </SuccessTemplate>
        <ChangePasswordTemplate>
            <div class="FieldStyle LeftContent">
                <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword" EnableViewState="false" ClientIDMode="Static">
                    <asp:Localize ID="Localize2" meta:resourceKey="txtCurrent" runat="server"  EnableViewState="false"/></asp:Label></div>
            <div class="LeftContent">
                <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" EnableViewState="false" placeholder="Current Password *" meta:resourceKey="CurrentPassword" ClientIDMode="Static"></asp:TextBox>
                <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:CommonCaption, PasswordRequired %>" ToolTip="<%$ Resources:CommonCaption, PasswordRequired %>"
                    ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic"
                     meta:resourceKey="txtReqPassword"></asp:RequiredFieldValidator>
            </div>
            <div class="Clear">
                <uc1:spacer ID="Spacer4" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server" ClientIDMode="Static"></uc1:spacer>
            </div>
            <div class="FieldStyle LeftContent">
                <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword" EnableViewState="false" ClientIDMode="Static">
                    <asp:Localize ID="Localize9" meta:resourceKey="txtNewPassword" runat="server" EnableViewState="false" ClientIDMode="Static" /></asp:Label></div>
            <div class="LeftContent">
                <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" EnableViewState="false" placeholder="New Password *" meta:resourceKey="NewPassword" ClientIDMode="Static"></asp:TextBox>
                <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" SetFocusOnError="true"
                    ControlToValidate="NewPassword"
                    ErrorMessage="<%$ Resources:CommonCaption, NewPasswordRequired %>"
                    ToolTip="<%$ Resources:CommonCaption, NewPasswordRequired %>"
                    ValidationGroup="ChangePassword1" 
                    CssClass="Error" 
                    Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="NewPassword" 
                    Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, ValidPasswordRequired %>"
                    SetFocusOnError="True" ToolTip="<%$ Resources:CommonCaption, ValidPasswordRequired %>"
                    ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})" ValidationGroup="ChangePassword1"
                    CssClass="Error" meta:resourceKey="txtReq" ></asp:RegularExpressionValidator>
            </div>
            <div class="Clear">
                <uc1:spacer ID="Spacer8" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server" ClientIDMode="Static"></uc1:spacer>
            </div>
            <div class="FieldStyle LeftContent">
                <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword" EnableViewState="false">
                    <asp:Localize ID="Localize4" meta:resourceKey="txtConfirm" runat="server"  EnableViewState="false" ClientIDMode="Static"/></asp:Label>
            </div>
            <div class="LeftContent">
                <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" meta:resourceKey="ConfirmPassword" Placeholder="Confirm New Password *" EnableViewState="false" ClientIDMode="Static"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:CommonCaption, ConfirmPasswordNewRequired %>" ToolTip="<%$ Resources:CommonCaption, ConfirmPasswordNewRequired %>"
                    ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic"
                     meta:resourceKey="txtConfirmPassword" runat="server" ></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword" SetFocusOnError="true"
                    ControlToValidate="ConfirmNewPassword" Display="Dynamic" 
                    ErrorMessage="<%$ Resources:CommonCaption, ConfirmPasswordNotMatch %>"
                    ValidationGroup="ChangePassword1" CssClass="Error"></asp:CompareValidator>
            </div>
            <div class="Clear">
                <uc1:spacer ID="Spacer5" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server" ClientIDMode="Static"></uc1:spacer>
            </div>
            <%--<div class="FieldStyle LeftContent">
                <asp:Label ID="lblSecretQuestion" runat="server" AssociatedControlID="ddlSecretQuestions" EnableViewState="false" ClientIDMode="Static">
                    <asp:Localize ID="Localize6" meta:resourceKey="txtSecurityQ" runat="server"  EnableViewState="false" /></asp:Label></div>
            <div class="LeftContent">
                <asp:DropDownList ID="ddlSecretQuestions" runat="server" meta:resourceKey="SecurityQ" TabIndex="4" EnableViewState="false" ClientIDMode="Static">
                    <asp:ListItem Enabled="true" Selected="True" Text="<%$ Resources:CommonCaption, txtFavPet %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCityBorn %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtSchool %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMovie %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMotherName %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCar %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtColor %>" />
                </asp:DropDownList>
            </div>--%>
           <%-- <div class="Clear">
                <uc1:spacer ID="Spacer6" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server" ClientIDMode="Static"></uc1:spacer>
            </div>
            <div class="FieldStyle LeftContent">
                <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer" EnableViewState="false" ClientIDMode="Static">
                    <asp:Localize ID="Localize7" meta:resourceKey="txtSecurityA" runat="server" EnableViewState="false" /></asp:Label></div>
            <div>
                <asp:TextBox ID="Answer" Width="140px" runat="server" TabIndex="5" EnableViewState="false" meta:resourceKey="SecurityA" ClientIDMode="Static" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Answer" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:CommonCaption, SecurityAnswerRequired %>" ToolTip="<%$ Resources:CommonCaption, SecurityAnswerRequired %>"
                    ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic"
                    meta:resourceKey="txtSecurityAReq"></asp:RequiredFieldValidator>
            </div>
            <div class="Clear">
                <uc1:spacer ID="Spacer7" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server" ClientIDMode="Static"></uc1:spacer>
            </div>--%>
            <div class="FailureText">
                <asp:Literal ID="PasswordFailureText" runat="server" EnableViewState="False" ClientIDMode="Static"></asp:Literal></div>
            <div class="Clear">
                <uc1:spacer ID="Spacer9" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server" ClientIDMode="Static"></uc1:spacer>
            </div>
            <div class="FieldStyle LeftContent No-Text">&nbsp;</div>
            <div class="Buttons">
                <asp:LinkButton ID="ibChangePassword" runat="server" meta:resourceKey="txtChangePassword"
                    CommandName="ChangePassword" ValidationGroup="ChangePassword1" CssClass="Button Large" ClientIDMode="Static"/>
                <asp:LinkButton ID="ibCancelButton" runat="server" meta:resourceKey="txtCancel" CausesValidation="False"
                    CssClass="ClearButton" CommandName="Cancel" ClientIDMode="Static"/>
            </div>
            <div class="Clear">
                <uc1:spacer ID="Spacer10" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
            </div>
        </ChangePasswordTemplate>
    </asp:ChangePassword>
</div>
<div>
    <uc1:spacer ID="Spacer4" EnableViewState="false" SpacerHeight="60" SpacerWidth="10" runat="server" ClientIDMode="Static"></uc1:spacer>
</div>

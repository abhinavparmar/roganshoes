﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.CustomClasses;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Admin;


namespace WebApp.Controls.Default.ShoppingCart
{
    public partial class zQuickOrder : System.Web.UI.UserControl
    {
        #region[public property]

        public bool IsConfigurableProduct { get; set; }
        public bool IsOutOfStock { get; set; }
        public bool IsInvalidSku { get; set; }

        public string ProductSEOUrl { get; set; }
        public string OutOfStockMessage { get; set; }

        #endregion

        #region[Page Events]

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion

        #region[Events]

        protected void btnAddtoCart_Click(object sender, EventArgs e)
        {
            try
            {
                bool isProductAddedSuccessfully = this.ProceedAddToCart();
                this.ShowMessage(isProductAddedSuccessfully);
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage("Error in zQuickOrder::zQuickOrder::" + ex.ToString());
            }
        }

        #endregion

        #region[Methods]
        /// <summary>
        /// Process Quick Add To Cart
        /// </summary>
        /// <returns></returns>
        private bool ProceedAddToCart()
        {
            bool isProductAddedSuccessfully = false;
            try
            {
                string productSku = txtSku.Text.Trim();
                if (!string.IsNullOrEmpty(productSku))
                {
                    int productID = GetProductIDBySkuNum(productSku);
                    if (productID > 0)
                    {
                        int quantity = 0;
                        int.TryParse(txtQuantity.Text.Trim(), out quantity);
                        isProductAddedSuccessfully = this.AddToCart(productID, quantity, productSku);
                    }
                    else
                    {
                        IsInvalidSku = true;
                    }
                }
            }
            catch (Exception ex)
            {
                isProductAddedSuccessfully = false;
                ZNodeLoggingBase.LogMessage("Error in zQuickOrder.ascx.cs::Method::ProceedAddToCart::" + ex.ToString());
            }
            return isProductAddedSuccessfully;
        }

        /// <summary>
        /// Method for Adding Item to the Cart 
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="qty"></param>
        /// <param name="skuNum"></param>
        /// <returns></returns>

        private bool AddToCart(int productId, int qty, string skuNum)
        {
            bool isProductAddedSuccessfully = false;
            try
            {
                ZNodeProduct product = new ZNodeProduct();
                //product = ZNodeProduct.Create(productId);
                product = ZNodeProduct.Create(productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

                // Set Selected SKU/Attribute
                product.SelectedSKU = ZNodeSKU.CreateBySKU(skuNum);

                if (!CheckIsConfigurable(product))
                {
                    IsConfigurableProduct = true;
                    ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                    item.Product = new ZNodeProductBase(product);
                    item.Quantity = qty;
                    item.Product.QuantityOnHand = product.QuantityOnHand;



                    // Add product to cart
                    ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                    // If shopping cart is null, create in session
                    if (shoppingCart == null)
                    {
                        shoppingCart = new ZNodeShoppingCart();
                        shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                    }

                    // Add item to cart
                    if (shoppingCart.AddToCart(item))
                    {
                        // Update SavedCart items
                        ZNodeSavedCart.AddToSavedCart(item);
                        isProductAddedSuccessfully = true;
                    }
                    else
                    {
                        IsOutOfStock = true;
                        OutOfStockMessage = product.OutOfStockMsg;
                    }
                }
                else
                {
                    IsConfigurableProduct = true;
                    ProductSEOUrl = product.SEOURL;

                }
            }
            catch (Exception ex)
            {
                isProductAddedSuccessfully = false;
                ZNodeLoggingBase.LogMessage("Error in zQuickOrder.ascx.cs::Method::AddToCart::" + ex.ToString());
            }
            return isProductAddedSuccessfully;
        }

        /// <summary>
        /// Method Return productid based on SKU Number
        /// </summary>
        /// <param name="skuNum"></param>
        /// <returns></returns>
        private int GetProductIDBySkuNum(string skuNum)
        {
            int productID = 0;
            SKUService skuService = new SKUService();
            TList<SKU> skuList = skuService.GetBySKU(skuNum);
            if (skuList != null && skuList.Count > 0)
            {
                productID = skuList[0].ProductID;
            }

            return productID;
        }

        /// <summary>
        /// Check is Product is of Configurable Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        private bool CheckIsConfigurable(ZNodeProduct product)
        {
            bool isConfigurableProduct = false;
            if (product != null && product.ZNodeAddOnCollection != null && product.ZNodeAddOnCollection.Count > 0)
            {
                isConfigurableProduct = true;
            }

            return isConfigurableProduct;
        }

        /// <summary>
        /// Show Message
        /// </summary>
        /// <param name="isSuccess"></param>
        private void ShowMessage(bool isSuccess)
        {
            string message = string.Empty;
            string className = isSuccess ? "CommonSuccessBox" : "CommonErrorBox";
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            if (!isSuccess)
            {
                if (IsConfigurableProduct && !string.IsNullOrEmpty(ProductSEOUrl))
                {
                    message = zeonMessageConfig.GetMessageKey("ConfigurableProductError");
                    if (!string.IsNullOrEmpty(message))
                    {
                        message = message.Replace("#SEOURL#", ProductSEOUrl + ".aspx");
                    }
                }
                else if (IsInvalidSku)
                {
                    message = zeonMessageConfig.GetMessageKey("QuickOrderErrorMessage");
                }
                else if (IsOutOfStock)
                {
                    message = OutOfStockMessage;
                }
                else
                {
                    message = "Unable to quick add to cart.Please try again";
                }

            }
            else if (isSuccess)
            {
                message = zeonMessageConfig.GetMessageKey("QuickOrderSuccessMessage");
            }

            ltErrorMessage.Text = message;
            divErrorMessage.Attributes.Add("class", className);
        }

        #endregion
    }
}
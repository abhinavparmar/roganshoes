﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="zQuickOrder.ascx.cs" Inherits="WebApp.Controls.Default.ShoppingCart.zQuickOrder" %>
<div class="QuickOrderForm">
    <asp:Panel ID="pnlSKU" runat="server" DefaultButton="btnAddToCart" CssClass="chkout-btm">
        <div class="QuickOrder-info">
            <span id="ExCheckout_lblProduct">SKU: </span>
            <asp:TextBox ID="txtSku" runat="server" ValidationGroup="QuickOrder"></asp:TextBox>
            <span id="spanQuantity">Quantity : </span>
            <asp:TextBox ID="txtQuantity" runat="server" MaxLength="4" Width="25px" Text="1"
                onblur="TrimStart(this)" meta:resourcekey="txtValResource1"></asp:TextBox>
            <ajaxToolKit:FilteredTextBoxExtender ID="fteVal" runat="server" BehaviorID="fteVal"
                ValidChars="1234567890" FilterType="Numbers" TargetControlID="txtQuantity" Enabled="True" />
            <asp:LinkButton ID="btnAddToCart" runat="server" AlternateText="Add To Cart"
                CssClass="Button" Text="Add To cart" OnClick="btnAddtoCart_Click"></asp:LinkButton>
        </div>
    </asp:Panel>
</div>
<div id="divErrorMessage" runat="server" enableviewstate="false">
    <asp:Literal ID="ltErrorMessage" runat="server"></asp:Literal>
</div>

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using System.Linq;
using System.Text;

namespace WebApp
{
    /// <summary>
    /// Represents the Product Related user control class.
    /// </summary>
    public partial class Controls_Default_ShoppingCart_ProductRelated : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProfile _RelatedProductProfile = new ZNodeProfile();

        /// <summary>
        /// Gets or sets the related product profile
        /// </summary>
        public ZNodeProfile RelatedProductProfile
        {
            get { return _RelatedProductProfile; }
            set { _RelatedProductProfile = value; }
        }
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/ViewAdmin.gif";
        private ZNodeImage znodeImage = new ZNodeImage();
        private bool _EnableViewstate = false;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value indicating whether to EnableViewState or not
        /// </summary>
        public bool EnableViewstate
        {
            get
            {
                return this._EnableViewstate;
            }

            set
            {
                this._EnableViewstate = value;
            }
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind related Items
        /// </summary>
        public void Bind()
        {
            // Get Current shopping cart object from session
            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (shoppingCart != null)
            {
                pnlRelatedProduct.Visible = false;

                if (shoppingCart.ShoppingCartItems.Count == 0)
                {
                    return;
                }

                // Loop through each line item in the cart
                for (int index = shoppingCart.Count - 1; index >= 0; index--)
                {
                    ZNodeProductBase product = shoppingCart.ShoppingCartItems[index].Product;

                    if (product.ZNodeCrossSellItemCollection.Count != 0)
                    {
                        // RelatedProductList.RepeatDirection = RepeatDirection.Horizontal;                       
                        if (product.ZNodeCrossSellItemCollection.OfType<ZNodeCrossSellItem>().AsQueryable().Where(zn => zn.CrossSellTypeID == ((int)ProductHelper.ProductCrossSellTypeEnum.UpSells)).ToList().Count > 0)
                        {
                            //Zeon Custom Code: Start
                            // Bind list control
                            // RelatedProductList.DataSource = product.ZNodeCrossSellItemCollection;
                            RelatedProductList.DataSource = product.ZNodeCrossSellItemCollection.OfType<ZNodeCrossSellItem>().AsQueryable().Where(zn => zn.CrossSellTypeID == ((int)ProductHelper.ProductCrossSellTypeEnum.UpSells)).ToList();
                            //Zeon Custom Code: End
                            RelatedProductList.DataBind();
                            pnlRelatedProduct.Visible = true;
                            break;
                        }
                    }
                }
            }
        }
        #endregion

        #region Helper Functions
        /// <summary>
        /// Check the Call For Pricing Message
        /// </summary>
        /// <param name="fieldValue">Field Value</param>
        /// <returns>Returns the Call For Pricing Message</returns>
        public string CheckForCallForPricing(object fieldValue)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();

            bool Status = bool.Parse(fieldValue.ToString());

            if (Status)
            {
                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else if (!this.RelatedProductProfile.ShowPrice)
            {
                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        ///  Show Pricing Method
        /// </summary>
        /// <param name="CallForPricing">Call For Pricing value</param>
        /// <returns>Returns the bool value to Show the Pricing or not</returns>
        public bool ShowPricing(bool CallForPricing)
        {
            if (CallForPricing)
            {
                return false;
            }
            else if (!this.RelatedProductProfile.ShowPrice)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get Image Path
        /// </summary>
        /// <param name="imageFile">Image File</param>
        /// <returns>Returns the Image Path</returns>
        protected string GetImagePath(string imageFile)
        {
            return this.znodeImage.GetImageHttpPathSmall(imageFile);
        }

        /// <summary>
        /// Registers the Script
        /// </summary>
        private void RegisterClientScriptClasses()
        {
            StringBuilder script = new StringBuilder();

            script.Append("var zeonRelProductShoopingCart = new ZeonSlideProducts({");
            script.Append("'sliderID':'" + relProdShoppingCart.ClientID + "',");
            script.Append("'MaxItemCount':'5',");
            script.Append("'MobPortraitItemCount':'3',");
            script.Append("'MobLandItemCount':'3',");
            script.Append("'TabletItemCount':'3',");
            script.Append("'MinimumItemsShown':'2',");
            script.Append("});zeonRelProductShoopingCart.Init();");
            this.Page.ClientScript.RegisterStartupScript(GetType(), "SliderZeonRelProductShoopingCart", script.ToString(), true);
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this._EnableViewstate == false || !Page.IsPostBack)
            {
                this.Bind();
            }
        }

        /// <summary>
        /// Page Prerender event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            RegisterClientScriptClasses();
        }

        #endregion
    }
}
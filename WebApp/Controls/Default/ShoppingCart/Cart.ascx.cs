using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Cart user control class.
    /// </summary>
    public partial class Controls_Default_ShoppingCart_Cart : System.Web.UI.UserControl
    {
        #region Member Variables
        private ZNodeShoppingCart shoppingCart;
        private ZNodeUserAccount userAccount;
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (!Page.IsPostBack)
            {
                PaymentSettingService _pmtServ = new PaymentSettingService();
                ZNode.Libraries.DataAccess.Entities.TList<ZNode.Libraries.DataAccess.Entities.PaymentSetting> _pmtSetting = _pmtServ.GetAll();

                int profileID = 0;

                // Check user Session
                if (this.userAccount != null)
                {
                    profileID = this.userAccount.ProfileID;
                }
                else
                {
                    // Get Default Registered profileId
                    profileID = ZNodeProfile.DefaultRegisteredProfileId;
                }

                List<int> portalIds = this.GetShoppingCartVendors();

            }

            // Registers the event for the shopping cart (child) control.
            this.uxCart.CartItem_RemoveLinkClicked += new EventHandler(this.CartItem_RemoveLinkClicked);
            this.uxCart.ProceedToCheckoutEvent += uxCart_ProceedToCheckoutEvent;

            if (this.Page.Title != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtShoppingCart.Text");
            }

            // Hide the continue shopping button if request comes from mobile service.
            if (Session["cs"] != null)
            {
                //ContinueShopping1.Visible = false;
            }
        }

        /// <summary>
        /// On Pre Render Method
        /// </summary>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            if (this.userAccount == null)
            {
                this.userAccount = ZNodeUserAccount.CurrentAccount();
            }

            //Zeon Customization :: Changes done on date  : 28/May/2012 :: Issue Fix :: Shopping object cart is null while adding item from quick order.
            if (this.shoppingCart == null)
            {
                this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
            }

            if (this.shoppingCart != null)
            {
                ShowHideApplyTeamPriceButton();

                this.Bind();
            }
            else
            {
                pnlShoppingCart.Visible = false;
                uxProductRelatedItems.Visible = false;
                uxMsg.Text = this.GetLocalResourceObject("CartEmpty").ToString();
                divErrorMessage.Visible = true;
                lnkProceeedToCheckout.Visible = false;
                lbtnApplyTeamPrice.Visible = false;
                lbtnApplyRetailPrice.Visible = false;

            }

            if (!this.IsPostBack && System.Web.HttpContext.Current.Session["IsCartChanged"] != null)
            {
                // We have added the contents of cart from existing user's saved cart, so alert the user.
                uxMsg.Visible = true;
                divErrorMessage.Visible = true;
                uxMsg.Text = this.GetLocalResourceObject("SavedCartText").ToString();
                System.Web.HttpContext.Current.Session.Remove("IsCartChanged");
            }
            if (!this.IsPostBack && System.Web.HttpContext.Current.Session["RedirectToCart"] != null)
            {
                // We have added the contents of cart from existing user's saved cart, so alert the user.
                uxMsg.Visible = true;
                divErrorMessage.Visible = true;
                uxMsg.Text = "Please check to confirm that all items and quantities are correct.";
                System.Web.HttpContext.Current.Session.Remove("RedirectToCart");
            }

        }



        /// <summary>
        /// Checkout button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Checkout_Click(object sender, EventArgs e)
        {
            string strlog = "\nProceed to Checkout. \nIP : {0} \nBrowser: {1}, \nVersion : {2}, \nType : {3}, \nProductNum : {4}, \nQty : {5}\n\n";
            strlog = string.Format(strlog, Request.UserHostAddress, Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, "","");
            Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(strlog);

            ZNodeShoppingCart ShoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (ShoppingCart == null)
            {
                uxMsg.Text = this.GetLocalResourceObject("AddItemsToCart").ToString();
                divErrorMessage.Visible = true;
                return;
            }
            else
            {
                //Zeon Custom Code:Starts
                if (uxCart.GetNotesVisibility()) { uxCart.UpdateCartItems(); }
                //Zeon Custom Code:Ends

                ZNodeUrl url = new ZNodeUrl();
                string link = "~/editaddress.aspx";

                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    link = "~/login.aspx?returnurl=editaddress.aspx";
                }

                if (uxCart.CouponCode.Length > 0)
                {
                    ShoppingCart.AddCouponCode(uxCart.CouponCode.Trim());
                }

                // Redirect to checkout page
                Response.Redirect(link);
            }
        }

        /// <summary>
        /// Event is raised when Continue Shopping button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ContinueShopping1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }

        /// <summary>
        /// Fired when child Control Remove link triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CartItem_RemoveLinkClicked(object sender, EventArgs e)
        {
            string cartCount = "0";

            // Re-bind related items
            uxProductRelatedItems.Bind();

            ZNodeShoppingCart ShoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

            if (ShoppingCart != null)
            {
                cartCount = ShoppingCart.ShoppingCartItems.Count.ToString();
            }
            else
            {
                cartCount = "0";
            }
            //Zeon Custom Code:start
            UserControl homeControl = (UserControl)this.Page.Master.Master.FindControl("uxZHeader");
            if (homeControl != null)
            {
                //UserControl cartItemCount = (UserControl)this.Page.Master.Master.FindControl("CART_ITEM_COUNT");      
                UserControl cartItemCount = (UserControl)homeControl.FindControl("CART_ITEM_COUNT");
                if (cartItemCount != null)
                {
                    Label lblCartCount = (Label)cartItemCount.FindControl("lblCartItemCount");
                    lblCartCount.Text = cartCount.ToString();
                }
            }
            //Zeon Custom Code:start
        }

        protected void uxCart_ProceedToCheckoutEvent(object sender, EventArgs e)
        {
            Checkout_Click(sender, e);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Bind All Controls
        /// </summary>
        protected void Bind()
        {
            // Show/hide cart
            if (this.shoppingCart.Count > 0)
            {
                pnlShoppingCart.Visible = true;
                uxMsg.Text = string.Empty;
                divErrorMessage.Visible = false;
                lnkProceeedToCheckout.Visible = true;
                divContinueShopping.Visible = false;
            }
            else
            {
                pnlShoppingCart.Visible = false;
                divErrorMessage.Visible = true;
                uxMsg.Text = this.GetLocalResourceObject("CartEmpty").ToString();
                lnkProceeedToCheckout.Visible = false;
                divContinueShopping.Visible = true;
            }

            // Bind grid
            uxCart.Bind();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Gets the Shopping Cart Vendors list
        /// </summary>
        /// <returns>Returns the Shopping Cart vendor list</returns>
        private System.Collections.Generic.List<int> GetShoppingCartVendors()
        {
            System.Collections.Generic.List<int> list = new System.Collections.Generic.List<int>();

            if (this.shoppingCart != null)
            {
                foreach (ZNodeShoppingCartItem cartItem in this.shoppingCart.ShoppingCartItems)
                {
                    int portalId = 0;
                    if (cartItem.Product.PortalID > 0)
                    {
                        portalId = cartItem.Product.PortalID;
                    }

                    if (!list.Contains(portalId))
                    {
                        list.Add(portalId);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type ID</param>
        /// <returns>Returns the payment setting instance</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            int? profileID = 0;
            if (this.userAccount != null)
            {
                // Get loggedIn user profile
                profileID = this.userAccount.ProfileID;
            }

            TList<PaymentSetting> list = _pmtServ.GetByPaymentTypeID(paymentTypeID);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (list.Count == 0)
            {
                // Get All Profiles payment setting
                list.Filter = "ActiveInd = true";
            }

            return list[0];
        }
        #endregion

        #region Zeon Custom Methods

        protected void ApplyTeamPrice_Click(object sender, EventArgs e)
        {
            ZNodeShoppingCart ShoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (ShoppingCart == null)
            {
                uxMsg.Text = this.GetLocalResourceObject("AddItemsToCart").ToString();
                divErrorMessage.Visible = true;
                return;
            }
            else
            {
                if (ShoppingCart != null && ShoppingCart.ShoppingCartItems.Count > 0)
                {
                    foreach (ZNodeShoppingCartItem item in ShoppingCart.ShoppingCartItems)
                    {
                        if (item.IsTierPrice)
                        {
                            item.UnitPriceOverride = item.TeamPrice;
                        }
                    }
                }
            }
        }

        protected void ApplyRetailPrice_Click(object sender, EventArgs e)
        {
            ZNodeShoppingCart ShoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (ShoppingCart == null)
            {
                uxMsg.Text = this.GetLocalResourceObject("AddItemsToCart").ToString();
                divErrorMessage.Visible = true;
                return;
            }
            else
            {
                if (ShoppingCart != null && ShoppingCart.ShoppingCartItems.Count > 0)
                {
                    foreach (ZNodeShoppingCartItem item in ShoppingCart.ShoppingCartItems)
                    {
                        item.UnitPriceOverride = item.ShowRetailPrice;
                    }
                }
            }
        }

        /// <summary>
        /// Show Hide Apply Team or Retail Price Buttons
        /// </summary>
        private void ShowHideApplyTeamPriceButton()
        {
            lbtnApplyTeamPrice.Visible = false;
            lbtnApplyRetailPrice.Visible = false;
            if (ConfigurationManager.AppSettings["CheerAndPomPortalID"] != null && !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString()) && ConfigurationManager.AppSettings["CheerAndPomPortalID"].ToString() == ZNodeConfigManager.SiteConfig.PortalID.ToString() && (Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP")))
            {                
                if (this.shoppingCart != null && this.shoppingCart.ShoppingCartItems.Count > 0)
                {
                    
                    if (this.shoppingCart.IsTeamPriceProductCart)
                    {
                        lbtnApplyTeamPrice.Visible = true;
                        lbtnApplyRetailPrice.Visible = true;
                    }
                    else
                    {
                        lbtnApplyTeamPrice.Visible = false;
                        lbtnApplyRetailPrice.Visible = true;
                    }
                }
            }
        }

        #endregion
    }
}
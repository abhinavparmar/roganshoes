<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="WebApp.Controls_Default_Contact_CustomerFeedback" CodeBehind="CustomerFeedback.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<div class="Form">
    <asp:Panel ID="pnlContact" runat="server" class="Feedback" meta:resourcekey="pnlContactResource1" DefaultButton="Button1">
        <div class="PageTitle">
            <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtHeading" EnableViewState="false"></asp:Localize>
        </div>
        <p>
            <uc1:CustomMessage ID="CustomMessage1" runat="server" MessageKey="CustomerFeedbackIntroText"></uc1:CustomMessage>
        </p>
        <div class="CustomerFeedbackForm">
            <div class="Row">
                <div class="FieldStyle">
                    <label>
                        <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtComments" EnableViewState="false"></asp:Localize>
                        :</label>
                </div>
                <div class="FieldStyleRight">
                    <asp:TextBox ID="Comments" runat="server" TextMode="MultiLine" Rows="5" Columns="3" EnableViewState="false" 
                        meta:resourcekey="Comments" alt="Comments" placeholder="Comments"></asp:TextBox>
                </div>
            </div>
            <div class="Clear">
                <ZNode:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
            </div>
            <div class="Row">
                <div class="FieldStyle">
                    <label>
                        <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtFirstName"></asp:Localize>
                        :</label>
                </div>
                <div class="FieldStyleRight">
                    <asp:TextBox ID="txtFirstName" runat="server" ToolTip="<%$ Resources:CommonCaption, FirstName%>"  alt="FirstName" placeholder="First Name"></asp:TextBox>
                </div>
            </div>
            <div class="Row">
                <div class="FieldStyle">
                    <label>
                        <asp:Localize ID="Localize7" runat="server" meta:resourceKey="txtLastName"></asp:Localize>:</label>
                </div>
                <div class="FieldStyleRight">
                    <asp:TextBox ID="txtLastName" runat="server" ToolTip="<%$ Resources:CommonCaption, LastName%>"  alt="Last Name" placeholder="Last Name"></asp:TextBox>
                </div>
            </div>
            <div class="Clear">
                <ZNode:Spacer ID="Spacer4" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
            </div>
            <div class="Row">
                <div class="FieldStyle">
                    <label>
                        <asp:Localize ID="lblCity" runat="server" Text="<%$ Resources:CommonCaption, City%>"></asp:Localize></label>
                </div>
                <div class="FieldStyleRight">
                    <asp:TextBox ID="txtcity" runat="server" ToolTip="<%$ Resources:CommonCaption, City%>"  EnableViewState="false" ClientIDMode="Static" alt="City" placeholder="City"> </asp:TextBox>
                </div>
            </div>
            <div class="Clear">
                <ZNode:Spacer ID="Spacer5" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
            </div>
            <div class="Row">
                <div class="FieldStyle">
                    <label>
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:CommonCaption, State%>" EnableViewState="false"></asp:Localize></label>
                </div>
                <div class="FieldStyleRight">
                    <asp:TextBox ID="txtstate" runat="server" ToolTip="<%$ Resources:CommonCaption, State%>" TabIndex="5" EnableViewState="false" ClientIDMode="Static" alt="State" placeholder="State"></asp:TextBox>
                </div>
            </div>
            <div class="Clear">
                <ZNode:Spacer ID="Spacer6" SpacerHeight="1" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:Spacer>
            </div>
            <div class="Row">
                <div class="FieldStyle">
                    <label class="Req">
                        <asp:Localize ID="Localize4" runat="server" Text="Email" EnableViewState="false"></asp:Localize></label>
                </div>
                <div class="FieldStyleRight">
                    <asp:TextBox ID="txtemail" runat="server" ToolTip="Email"  EnableViewState="false" ClientIDMode="Static" alt="Email" placeholder="Email *"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="txtemail" SetFocusOnError="true"
                        ValidationGroup="1" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
                        Display="Dynamic" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"
                        CssClass="Error"></asp:RegularExpressionValidator>

                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>" ControlToValidate="txtemail" SetFocusOnError="true"
                        ValidationGroup="1" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="Clear">
                <ZNode:Spacer ID="Spacer7" SpacerHeight="1" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:Spacer>
            </div>
            <div class="Row">
                <div class="FieldStyle">
                    &nbsp;
                </div>
                <div class="FieldStyleRight">
                    <asp:CheckBox Checked="True" ID="chkPublish" runat="server" meta:resourcekey="chkPublish" EnableViewState="false" />
                    <asp:Localize ID="Localize6" runat="server" meta:resourceKey="txtPublish" EnableViewState="false" ClientIDMode="Static"></asp:Localize>
                </div>
            </div>
            <div class="Clear">
                <ZNode:Spacer ID="Spacer8" SpacerHeight="1" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:Spacer>
            </div>
            <div class="Row">
                <div class="FieldStyle">
                    &nbsp;
                </div>
                <div class="FieldStyleRight Buttons">

                    <asp:LinkButton ID="Button1" runat="server" ValidationGroup="1" OnClick="BtnSubmit_Click" ClientIDMode="Static"
                        CssClass="Button" Text="<%$ Resources:CommonCaption, Submit%>" ToolTip="<%$ Resources:CommonCaption, Submit%>" />
                </div>
            </div>
        </div>

    </asp:Panel>
    <asp:Panel ID="pnlConfirm" runat="server" Visible="False" meta:resourcekey="pnlConfirmResource1" ClientIDMode="Static">
        <div class="SuccessMsg">
            <span class="uxMsg SuccessSection"><em>
                <uc1:CustomMessage ID="CustomMessage2" runat="server" MessageKey="CustomerFeedbackConfirmationIntroText" ClientIDMode="Static"></uc1:CustomMessage>
            </em></span>
        </div>
        <ZNode:Spacer ID="Spacer1" EnableViewState="false" SpacerHeight="20" SpacerWidth="10" runat="server" ClientIDMode="Static" />
        <asp:LinkButton ID="lnkContinueShopping" runat="server" AlternateText="Continue Shopping"
            OnClick="ContinueShopping1_Click" ValidationGroup="groupCart" CssClass="Button ContinueShopping"
            meta:resourcekey="ContinueShopping1Resource1" CausesValidation="false" ClientIDMode="Static">
            <asp:Localize ID="Localize8" runat="server" meta:resourcekey="lnkContinueShopping"></asp:Localize>
        </asp:LinkButton>
    </asp:Panel>
</div>

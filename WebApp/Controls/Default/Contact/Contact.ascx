<%@ Control Language="C#" AutoEventWireup="true" Inherits="WebApp.Controls_Default_Contact_Contact" CodeBehind="Contact.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="Zeon" %>
<asp:Panel ID="pnlContact" class="ContactUs" runat="server" meta:resourcekey="pnlContactResource1" DefaultButton="imgSubmit" ClientIDMode="Static">
    <div class="PageTitle">
        <h1><asp:Localize ID="LocalizeTitle" runat="server" meta:resourceKey="txtContactus"></asp:Localize></h1>
    </div>
    <p>
        <uc1:CustomMessage ID="CustomMessage1" runat="server" MessageKey="ContactUsIntroText" ClientIDMode="Static"></uc1:CustomMessage>
    </p>
    <div class="Form">
        <asp:Panel ID="pnlContactus" runat="server" DefaultButton="imgSubmit" ClientIDMode="Static" CssClass="CommonLableNone">
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblName" CssClass="Req" runat="server" AssociatedControlID="FirstName" meta:resourcekey="lblNameResource1" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:CommonCaption, FirstName%>" EnableViewState="false"></asp:Localize></asp:Label>
                </div>
                <div class="Input-box">
                    <asp:TextBox ID="FirstName" runat="server" MaxLength="100" EnableViewState="false"  ToolTip="<%$ Resources:CommonCaption, FirstName%>" ClientIDMode="Static" alt="FirstName" placeholder="First Name *"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="NameRequired" runat="server" ControlToValidate="FirstName" SetFocusOnError="true"
                        CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, FirstNameRequired%>"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="Label1" runat="server" CssClass="Req" AssociatedControlID="LastName" meta:resourcekey="Label1Resource1" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:CommonCaption, LastName%>" EnableViewState="false"></asp:Localize></asp:Label>
                </div>
                <div class="Input-box">
                    <asp:TextBox ID="LastName" runat="server" MaxLength="100" EnableViewState="false" ToolTip="<%$ Resources:CommonCaption, LastName%>" ClientIDMode="Static" alt="LastName" placeholder="Last Name *"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="LastName" SetFocusOnError="true"
                        CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, LastNameRequired %>">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="lblCompanyName" runat="server" AssociatedControlID="CompanyName" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:CommonCaption, CompanyName%>" EnableViewState="false">
                        </asp:Localize></asp:Label>
                </div>
                <div class="Input-box">
                    <asp:TextBox ID="CompanyName" runat="server" MaxLength="100" ToolTip="<%$ Resources:CommonCaption, CompanyName%>"  EnableViewState="false" ClientIDMode="Static" alt="Company Name" placeholder="Company Name"></asp:TextBox>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email" CssClass="Req" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:CommonCaption, EmailID%>" EnableViewState="false"></asp:Localize></asp:Label>
                </div>
                <div class="Input-box">
                    <asp:TextBox ID="Email" runat="server"  ToolTip="<%$ Resources:CommonCaption, EmailID%>" EnableViewState="false" ClientIDMode="Static" alt="Email" placeholder="Subscription Email *"> </asp:TextBox>
                    <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email" SetFocusOnError="true"
                        CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="Email" SetFocusOnError="true"
                        CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="PhoneNumberLabel" runat="server" AssociatedControlID="PhoneNumber" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:CommonCaption, PhoneNumber%>" EnableViewState="false"></asp:Localize></asp:Label>
                </div>
                <div class="Input-box">
                    <asp:TextBox ID="PhoneNumber" runat="server" MaxLength="20" meta:resourcekey="PhoneNumberResource1"  EnableViewState="false" ClientIDMode="Static" ToolTip="<%$ Resources:CommonCaption, PhoneNumber%>" alt="Phone Number" placeholder="Phone Number"></asp:TextBox>
                    <%--  <asp:RegularExpressionValidator id="RegularExpressionValidator7" ControlToValidate="PhoneNumber" CssClass="Error"
            ValidationExpression ="\d{3}-\d{3}-\d{4}" ErrorMessage="Enter valid phone number" Display="Dynamic"  runat="server"/>--%>
                </div>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="CommentsLabel" runat="server" AssociatedControlID="Comments" EnableViewState="false" ClientIDMode="Static">
                        <asp:Localize ID="Localize7" runat="server" meta:resourcekey="CommentsLabel" EnableViewState="false"></asp:Localize></asp:Label>
                </div>
                <div class="Input-box">
                    <asp:TextBox ID="Comments" runat="server" Height="136px" TextMode="MultiLine" EnableViewState="false" meta:resourcekey="Comments"  Width="300px" ClientIDMode="Static" alt="Comments" placeholder="Comments"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Comments" SetFocusOnError="true"
                            CssClass="Error" Display="Dynamic" ValidationGroup="uxCreateUserWizard" meta:resourcekey="RequiredFieldValidator2">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear BottomSpacer">
                <div class="FieldStyle">
                    <asp:Label ID="lblPicture" runat="server" EnableViewState="false">
                        <asp:Localize ID="locPictures" runat="server" meta:resourcekey="locPicture" EnableViewState="false"></asp:Localize>
                        :</asp:Label>
                </div>
                <asp:UpdatePanel ID="pnlCaptcha" runat="server" UpdateMode="Conditional" class="CaptchaControl">
                    <ContentTemplate>
                        <div class="CaptchaImage">
                            <Zeon:CaptchaControl ID="CaptchaID" runat="server" CaptchaBackgroundNoise="None" CaptchaLength="6" ToolTip="CaptchaImage" CaptchaHeight="55" CaptchaWidth="200" CaptchaLineNoise="None" CaptchaMinTimeout="5" BackColor="#af64a8" CaptchaMaxTimeout="240" FontColor="#FFFFFF"   />
                        </div>
                        <div class="CaptchaRefreshBtn">
                            <asp:LinkButton ID="lnkbtnRefresh" runat="server" Text="Refresh" CssClass="Button" ToolTip="Refresh" OnClick="lnkbtnRefresh_Click" CausesValidation="false"></asp:LinkButton>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle">
                                <asp:Label ID="lblCharacters" runat="server" CssClass="Req" AssociatedControlID="txtCharacters" EnableViewState="false">
                                    <asp:Localize ID="locCharecter" runat="server" meta:resourcekey="lblCharacter" EnableViewState="false"></asp:Localize>
                                    :</asp:Label>
                            </div>
                            <div class="FieldStyleRight">
                                <div class="CaptchaBox">
                                    <asp:TextBox ID="txtCharacters" runat="server" MaxLength="100" EnableViewState="false"  meta:resourcekey="txtCharacter" alt="Character" placeholder="Enter Character Here *"></asp:TextBox>
                                    <a id="whatsthisID" title="what's this?" href="javascript:void(0);" onclick="PopUpCaptchaWhatsThisWindow()" class="whatsthis">what's this?</a>
                                    <div>
                                        <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error"></asp:Label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="Row Clear">
                <div class="FieldStyle LeftContent No-Text">&nbsp;</div>
                <div class="ContactUsButton">
                    <asp:LinkButton ID="imgSubmit" runat="server" OnClick="ImgSubmit_Click" Text="Submit" EnableViewState="false"
                        CssClass="Button" meta:resourcekey="imgSubmitResource1" ClientIDMode="Static" />
                </div>
            </div>
            <div class="Row Clear">
                <div colspan="2" class="FailureText LeftContent">
                    <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False" ClientIDMode="Static"></asp:Literal>
                </div>
                <div class="LeftContent">
                    &nbsp;
                </div>
            </div>
        </asp:Panel>

    </div>
</asp:Panel>
<asp:Panel ID="pnlConfirm" runat="server" Visible="False" CssClass="SuccessMsg" meta:resourcekey="pnlConfirmResource1" ClientIDMode="Static">
    <div class="SuccessMsg">
        <span class="uxMsg SuccessSection"><em>
            <asp:Label ID="lblMessage" runat="server"  EnableViewState="false" ClientIDMode="Static"></asp:Label>
     </em>   </span>
    </div>
    <div class="Clear">
        <ZNode:spacer ID="Spacer9" EnableViewState="false" SpacerHeight="20" SpacerWidth="10" runat="server" ClientIDMode="Static"></ZNode:spacer>
    </div>
</asp:Panel>

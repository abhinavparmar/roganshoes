using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Customer Feedback user control class.
    /// </summary>
    public partial class Controls_Default_Contact_CustomerFeedback : System.Web.UI.UserControl
    {
        #region Private Variable
        private string _CaseTitle = "User Feedback";
        #endregion

        #region Public Property
        /// <summary>
        /// Gets or sets the CaseTitle
        /// </summary>
        public string CaseTitle
        {
            get
            {
                return this._CaseTitle;
            }

            set
            {
                this._CaseTitle = value;
            }
        }
        #endregion

        #region Page Event

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Zeon Custome Code: Start
            this.Comments.Focus();
            //Zeon Custom Code: End
            this._CaseTitle = GetLocalResourceObject("UserFeedback").ToString();

            if (this.Page.Title != null)
            {
                this.Page.Title = this._CaseTitle;
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Event is raised when Submit button is pressed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            this.SendEmail();
            this.CreateCaseRecord();
            pnlContact.Visible = false;
            pnlConfirm.Visible = true;
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Send Email Receipt
        /// </summary>
        private void SendEmail()
        {
            //Zeon Custom Code: Start
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string customerFeedbackTemplate = GetCustomerFeedbackEmailTemplate();
            try
            {
                if (!string.IsNullOrEmpty(customerFeedbackTemplate))
                {
                    ZNodeEmail.SendEmail(ZNodeConfigManager.SiteConfig.CustomerServiceEmail, txtemail.Text, string.Empty,zeonMessageConfig.GetMessageKey("CustomerFeedbackSubject") + " : " + this.CaseTitle, customerFeedbackTemplate, true);
                }
             }
                //Zeon Custom Code: End
            catch(Exception ex)
            {
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Customer Feedabck user email template sending problem(Mehtod name: SendEmail())=>CustomerFeedBack Page:" + ex.StackTrace);
            }
        }

        /// <summary>
        /// Creates New Case Record
        /// </summary>
        private void CreateCaseRecord()
        {
            ZNode.Libraries.Admin.CaseAdmin _CaseAdmin = new ZNode.Libraries.Admin.CaseAdmin();
            ZNode.Libraries.DataAccess.Entities.CaseRequest CaseRecord = new ZNode.Libraries.DataAccess.Entities.CaseRequest();

            // Set Values 
            CaseRecord.Title = this.CaseTitle;
            CaseRecord.Description = Server.HtmlEncode("Comments: " + Comments.Text + "; <br>City: " + txtcity.Text + "; <br>State: " + txtstate.Text + "; <br>It is OK to share this feedback with other customers - " + chkPublish.Checked);
            CaseRecord.FirstName = Server.HtmlEncode(txtFirstName.Text);
            CaseRecord.LastName = Server.HtmlEncode(txtLastName.Text);
            CaseRecord.EmailID = Server.HtmlEncode(txtemail.Text);

            // Set Some Default Values
            CaseRecord.CasePriorityID = 3;
            CaseRecord.CaseStatusID = 1;
            CaseRecord.CreateDte = System.DateTime.Now;
            CaseRecord.PortalID = ZNodeConfigManager.SiteConfig.PortalID;
            CaseRecord.OwnerAccountID = null;
            CaseRecord.CaseOrigin = "Feedback Form";
            CaseRecord.CreateUser = System.Web.HttpContext.Current.User.Identity.Name;
            
            CaseRecord.CompanyName = string.Empty;
            CaseRecord.PhoneNumber = string.Empty;

            if (Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
            {
                ZNodeUserAccount _usrAccount = Session[ZNodeSessionKeyType.UserAccount.ToString()] as ZNodeUserAccount;
                CaseRecord.AccountID = _usrAccount.AccountID;
            }
            else
            {
                CaseRecord.AccountID = null;
            }

            // Add New Case for this Email
            _CaseAdmin.Add(CaseRecord);
        }
        #endregion
        
        #region Zeon Custom Code:Start
        /// <summary>
        /// Get Customer Feedback email template with body content
        /// </summary>
        /// <param name="content">body of email</param>
        /// <returns></returns>
        private string GetCustomerFeedbackEmailTemplate()
        {
            string defaultTemplatePath = string.Empty;
            string comments = string.Empty;
            string firstName = string.Empty;
            string lastName = string.Empty;
            string city = string.Empty;
            string state = string.Empty;
            string emailId = string.Empty;
            string checkboxText = string.Empty;
            bool flag = false;

            comments = Comments.Text;
            firstName = txtFirstName.Text;
            lastName = txtLastName.Text;
            city = txtcity.Text;
            state = txtstate.Text;
            emailId = txtemail.Text;
            checkboxText = GetLocalResourceObject("txtPublish.Text").ToString() + " - ";
            flag = chkPublish.Checked;

            //currentCulture = ZNodeCatalogManager.CultureInfo;
            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ZCustomerFeedback.html"));

            StreamReader streamReader = new StreamReader(defaultTemplatePath);
            string messageText = streamReader.ReadToEnd();

            string spacerImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Images/space.png";
            string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
            int portalId = ZNodeConfigManager.SiteConfig.PortalID;

            ZEmailHelper zEmailHelper = new ZEmailHelper();
            ZeonMessageConfig zeonMessageConfig = new ZeonMessageConfig();
            string copyRightText = zeonMessageConfig.GetMessageKey("FooterCopyrightText");

            string messageTextHeader = zEmailHelper.EmailHeaderContent(domainPath, portalId, spacerImage);
            string messageTextFooter = zEmailHelper.EmailFooterContent(domainPath, portalId, copyRightText, spacerImage);

            //To add header to the template
            Regex rxHeaderContent = new Regex("#HeaderContent#", RegexOptions.IgnoreCase);
            messageText = rxHeaderContent.Replace(messageText, messageTextHeader);

            //To add footer to the template
            Regex rxFooterContent = new Regex("#FooterContent#", RegexOptions.IgnoreCase);
            messageText = rxFooterContent.Replace(messageText, messageTextFooter);

            Regex rxComments = new Regex("#Comments#", RegexOptions.IgnoreCase);
            messageText = rxComments.Replace(messageText, comments);

            Regex rxFirstName = new Regex("#First Name#", RegexOptions.IgnoreCase);
            messageText = rxFirstName.Replace(messageText, firstName);

            Regex rxLastName = new Regex("#Last Name#", RegexOptions.IgnoreCase);
            messageText = rxLastName.Replace(messageText, lastName);

            Regex rxCity = new Regex("#City#", RegexOptions.IgnoreCase);
            messageText = rxCity.Replace(messageText, city);

            Regex rxState = new Regex("#State#", RegexOptions.IgnoreCase);
            messageText = rxState.Replace(messageText, state);

            Regex rxEmailId = new Regex("#EmailId#", RegexOptions.IgnoreCase);
            messageText = rxEmailId.Replace(messageText, emailId);

            Regex rxCheckBoxText = new Regex("#CheckBoxText#", RegexOptions.IgnoreCase);
            messageText = rxCheckBoxText.Replace(messageText, checkboxText);

            Regex rxFlag = new Regex("#Flag#", RegexOptions.IgnoreCase);
            messageText = rxFlag.Replace(messageText, flag.ToString());

            return messageText;
        }

        /// <summary>
        /// Event is raised when Continue Shopping button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ContinueShopping1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SquishIt.Framework;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    public partial class Unsubscribe : CommonPageBase
    {

        #region Public Methods
        /// <summary>
        /// Set UI culture value in cookie.
        /// </summary>
        /// <param name="localeID">localeID selected</param>
        public void SetUICulture(int localeID)
        {
            ZNode.Libraries.DataAccess.Service.LocaleService localeService = new ZNode.Libraries.DataAccess.Service.LocaleService();
            ZNode.Libraries.DataAccess.Entities.Locale local = localeService.GetByLocaleID(localeID);
            string cultureValue = local.LocaleCode;

            // Remove the existing Cookie
            HttpContext.Current.Response.Cookies.Remove("CultureLanguage");

            // Set Culture in Cookie
            HttpCookie cookie = new HttpCookie("CultureLanguage");
            cookie.Value = cultureValue;
            cookie.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
            HttpContext.Current.Response.Cookies.Add(cookie);

            if (HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session["NewCulture"] = cultureValue;

                // Set Culture in Session
                HttpContext.Current.Session["CurrentUICulture"] = cultureValue;

                // Set NULL to last viewed category when locale change.
                HttpContext.Current.Session["BreadCrumzcid"] = null;

                // Reset the Session and Cache Value
                System.Web.HttpContext.Current.Session.Remove("CatalogConfig");

                // Set Culture in Thread
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cultureValue);
            }

            // Set locale id in Cookie
            HttpCookie cookieLocaleId = new HttpCookie("LocaleID");
            cookieLocaleId.Value = localeID.ToString();
            cookieLocaleId.Expires = DateTime.Now.AddDays(Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["CookieExpiresValue"]));
            HttpContext.Current.Response.Cookies.Add(cookieLocaleId);

            if (HttpContext.Current.Request.QueryString["redir"] != null)
            {
                HttpContext.Current.Response.Redirect(HttpContext.Current.Request.QueryString["redir"].ToString());
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/default.aspx", true);
            }
        }
        #endregion

        #region Page Events
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            ZNodeSEO seo = new ZNodeSEO();

            seo.SEOTitle = "Unsubscribe User";
            seo.SEODescription = "Unsubscribe User";
            seo.SEOKeywords = "Unsubscribe User";
            this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/PUnsubscribe.master";
        }

        #endregion
    }
} 
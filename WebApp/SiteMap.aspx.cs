﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    /// <summary>
    /// Represents the SiteMap Page class.
    /// </summary>
    public partial class SiteMap : CommonPageBase
    {
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            // Clear the current category session theme if some other category selected.
            Session["CurrentCategoryTheme"] = null;

            this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/sitemap.master";
        }
    }
}

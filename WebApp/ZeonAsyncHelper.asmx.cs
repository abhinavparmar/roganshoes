﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using System.Linq;
using System.Data.Linq;
using System.Web.Security;

namespace WebApp
{
    /// <summary>
    /// Summary description for ZeonAsyncHelper
    /// </summary>
    //[WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    //[System.Web.Script.Services.ScriptService]
    public class ZeonAsyncHelper : System.Web.Services.WebService
    {
        #region Compare Products

        #region Public WebMethods

        [WebMethod(EnableSession = true)]
        public ProductCompareHelper AddCompareProductIDToSession(int productID, int categoryID)
        {
            List<CompareProducts> compProds = new List<CompareProducts>();
            ProductCompareHelper productCompareHelper = new ProductCompareHelper();

            if (Session["CompareProductIDs"] != null)
            {
                compProds = Session["CompareProductIDs"] as List<CompareProducts>;

                bool isCatChanged = false;
                bool isProductExists = false;
                productCompareHelper.ProductCompareHTML = string.Empty;
                productCompareHelper.ProductCount = 0;
                productCompareHelper.IsAlreadyExist = false;
                foreach (CompareProducts comp in compProds)
                {
                    if (!comp.CategoryID.Equals(categoryID))
                    {
                        isCatChanged = true;
                        break;
                    }

                    if (comp.ProductID.Equals(productID))
                    {
                        isProductExists = true;
                        productCompareHelper.IsAlreadyExist = true;
                    }
                }

                if (isCatChanged)
                {
                    Session["CompareProductIDs"] = null;
                    compProds = new List<CompareProducts>();
                    AddProductToList(categoryID, productID, compProds);
                }
                else
                {
                    if (!isProductExists)
                    {
                        if (compProds == null)
                        {
                            compProds = Session["CompareProductIDs"] as List<CompareProducts>;
                        }

                        AddProductToList(categoryID, productID, compProds);
                    }
                }
            }
            else
            {
                AddProductToList(categoryID, productID, compProds);
            }

            productCompareHelper.ProductCompareHTML = GetCompareProductStructure();
            productCompareHelper.ProductCount = compProds.Count;

            return productCompareHelper;
        }

        [WebMethod(EnableSession = true)]
        public void RemoveCompareProductIDFromSession(int productID)
        {
            ProductCompareHelper productCompareHelper = new ProductCompareHelper();
            if (Session["CompareProductIDs"] != null)
            {
                List<CompareProducts> compProds = Session["CompareProductIDs"] as List<CompareProducts>;

                foreach (CompareProducts comp in compProds)
                {
                    if (comp.ProductID.Equals(productID))
                    {
                        compProds.Remove(comp);
                        break;
                    }
                }
                Session["CompareProductIDs"] = compProds;
            }
        }


        [WebMethod(EnableSession = true)]
        public ProductCompareHelper RemoveCompareProductFromSession(int productID)
        {
            ProductCompareHelper productCompareHelper = new ProductCompareHelper();
            //int count = 0;
            if (Session["CompareProductIDs"] != null)
            {
                List<CompareProducts> compProds = Session["CompareProductIDs"] as List<CompareProducts>;

                foreach (CompareProducts comp in compProds)
                {
                    if (comp.ProductID.Equals(productID))
                    {
                        compProds.Remove(comp);
                        break;
                    }
                }
                productCompareHelper.ProductCompareHTML = GetCompareProductStructure();
                productCompareHelper.ProductCount = compProds.Count;
                Session["CompareProductIDs"] = compProds;
                // count = compProds.Count;
            }
            return productCompareHelper;
        }

        [WebMethod(EnableSession = true)]
        public void ClearProductFromComparsion()
        {
            Session.Remove("CompareProductIDs");
        }

        [WebMethod(EnableSession = true)]
        public int GetCountOfTotalProducts()
        {
            int count = 0;
            if (Session["CompareProductIDs"] != null)
            {
                List<CompareProducts> compProds = Session["CompareProductIDs"] as List<CompareProducts>;
                count = compProds.Count;
            }
            return count;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Add the Product IDs to Compare session
        /// </summary>
        /// <param name="categoryID"></param>
        /// <param name="productID"></param>
        /// <param name="compProds"></param>
        private void AddProductToList(int categoryID, int productID, List<CompareProducts> compProds)
        {
            CompareProducts prod = new CompareProducts();
            prod.CategoryID = categoryID;
            prod.ProductID = productID;

            compProds.Add(prod);
            Session["CompareProductIDs"] = compProds;
        }

        #endregion

        #endregion

        #region Add To Cart

        [WebMethod(EnableSession = true)]
        public string GetMiniCartItems()
        {
            try
            {
                StringBuilder sbHtmlCode = new StringBuilder();
                ZNodeShoppingCart shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

                if (shoppingCart != null && shoppingCart.Count > 0)
                {
                    if (shoppingCart.Count > 3)
                    {
                        sbHtmlCode.Append(@"<div class='MiniCart CartScroll'>");
                    }
                    else
                    {
                        sbHtmlCode.Append(@"<div class='MiniCart'>");
                    }

                    sbHtmlCode.Append(@"<div class='ItemCountTop'>");
                    sbHtmlCode.Append(@"<div class='ItemTotal'><a href='shoppingcart.aspx'>" + GetShoppingCartTotalItemCount(shoppingCart.ShoppingCartItems) + "<span> Item(s)</span></a></div>");
                    sbHtmlCode.Append(@"</div>");
                    sbHtmlCode.Append(@"<div class='ItemCountWrapper'>");

                    foreach (ZNodeShoppingCartItem ShoppingCartItem in shoppingCart.ShoppingCartItems)
                    {
                        ZNodeImage imageHelper = new ZNodeImage();
                        string seoUrl = string.Empty;
                        seoUrl = System.Web.VirtualPathUtility.ToAbsolute(ShoppingCartItem.Product.ViewProductLink.Replace("~", ""));
                        string imagePath = System.Web.VirtualPathUtility.ToAbsolute(imageHelper.GetImageHttpPathThumbnail(ShoppingCartItem.Product.ImageFile.Replace("~", "")));

                        sbHtmlCode.Append(@"<div class='MiniCartProductImage'><a href='" + seoUrl + "'><img id='" + ShoppingCartItem.Product.SKU + "' alt='" + ShoppingCartItem.Product.ImageAltTag + "' title='" + ShoppingCartItem.Product.ImageAltTag + "' border='0' src='" + imagePath + "'");
                        sbHtmlCode.Append("/></a></div>");
                        sbHtmlCode.Append(@"<div class='MiniCartProductInfo'>");
                        sbHtmlCode.Append(@"<div class='ProdName'><a href='" + seoUrl + "' title='" + ShoppingCartItem.Product.ShortDescription + "'>" + ZCommonHelper.GetFormattedProductName(ShoppingCartItem.Product.ShortDescription, System.Configuration.ConfigurationManager.AppSettings["ProductGridNameMaxLimit"].ToString()) + "</a></div>");
                        sbHtmlCode.Append(@"<div class='SKU'><strong>Item # </strong>" + ShoppingCartItem.Product.ProductNum + "</div>");
                        sbHtmlCode.Append(@"<div class='ItemId'><strong>Qty </strong>" + ShoppingCartItem.Quantity + "</div>");
                        sbHtmlCode.Append(@"<div class='CartPrice'><strong>Price</strong>&nbsp;" + ShoppingCartItem.ExtendedPrice.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix() + "</div>");
                        sbHtmlCode.Append(@"</div>");
                    }
                    sbHtmlCode.Append(@"</div>");
                    sbHtmlCode.Append(@"<div class='ItemCountBottom'>");
                    sbHtmlCode.Append(@"<div class='MiniCartTotalSec'><div class='QtyTotal'><a href='shoppingcart.aspx'>" + GetShoppingCartTotalItemCount(shoppingCart.ShoppingCartItems) + "<span> Item(s)</span></a></div><div class='ItemTotal'><a href='shoppingcart.aspx'>" + shoppingCart.Total.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix() + "</a></div></div>");
                    sbHtmlCode.Append(@"<div class='MiniCartLinkSec'>");
                    sbHtmlCode.Append(@"<div><a href='shoppingcart.aspx' alternatetext='View Cart' class='ViewCartButton' title='View Cart'>View Cart</a><a href='" + GetCheckoutURL() + "' alternatetext='Checkout' class='CartCheckoutButton' title='Checkout'>Checkout</a></div></div>");
                    sbHtmlCode.Append(@"</div>$$$" + shoppingCart.Count);

                }
                else
                {
                    sbHtmlCode.Append(@"<div class='MiniCart'>");
                    sbHtmlCode.Append(@"<div class='MiniCartImage'><span id='spanNoItem' style='font-weight:bold;text-align:center'>" + HttpContext.GetGlobalResourceObject("CommonCaption", "NoItemInCart").ToString() + "</span>");
                    sbHtmlCode.Append(@"</div>");
                    sbHtmlCode.Append(@"</div>$$$0");
                }
                return sbHtmlCode.ToString();
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(ex.ToString());
                return string.Empty;
            }
        }

        #endregion

        #region[Process Multiple Attribute AddTo Cart]

        [WebMethod(EnableSession = true)]
        public string ProcessAddToCart(string attributelist)
        {
            int SkuAddedSuccessfully = 0;
            string response = string.Empty;
            string isAllItemAdded = "1";

            // Add product to cart
            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (!string.IsNullOrEmpty(attributelist))
            {
                List<SelectedAttributeCollection> selectedAttributeCollection = attributelist.ToJsonObject<List<SelectedAttributeCollection>>(100);

                if (selectedAttributeCollection != null && selectedAttributeCollection.Count > 0)
                {
                    //Create Product from product ID
                    ZNode.Libraries.ECommerce.Catalog.ZNodeProduct Product = ZNode.Libraries.ECommerce.Catalog.ZNodeProduct.Create(selectedAttributeCollection[0].ProductID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                    int count = 1;
                    foreach (SelectedAttributeCollection selectedAttributes in selectedAttributeCollection)
                    {
                        //Bind SKU Price on last attribute selected index changes
                        ZNode.Libraries.ECommerce.Catalog.ZNodeSKU SKU = ZNode.Libraries.ECommerce.Catalog.ZNodeSKU.CreateByProductAndAttributes(Product.ProductID, selectedAttributes.SelectedAttributes);
                        // Set Attributes Description to Description property
                        SKU.AttributesDescription = FormatSKUDescription(selectedAttributes.SelectedAttributeDescription);
                        if (SKU != null && SKU.SKUID > 0)
                        {
                            Product.SelectedSKU = SKU;

                            // Create shopping cart item
                            ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                            item.Product = new ZNodeProductBase(Product);
                            item.Quantity = selectedAttributes.Quantity;
                            item.Notes = selectedAttributes.Comment;
                            item.AddProductAsLineItem = true;

                            // If shopping cart is null, create in session
                            if (shoppingCart == null)
                            {
                                shoppingCart = new ZNodeShoppingCart();
                                shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                            }

                            // Add item to cart
                            string successfullyAdded = string.Empty;
                            if (shoppingCart.AddToCart(item))
                            {
                                SkuAddedSuccessfully = SkuAddedSuccessfully + 1;
                                ZNodeSavedCart.AddToSavedCart(item);
                                successfullyAdded = "true";
                            }
                            else
                            {
                                successfullyAdded = "false";
                                isAllItemAdded = "0";
                            }

                            if (count == selectedAttributeCollection.Count)
                            {

                                response = response + successfullyAdded;
                            }
                            else
                            {
                                response = response + successfullyAdded + ",";
                            }
                        }
                        count++;
                    }
                }

            }
            response = response + "|" + isAllItemAdded.ToString();//Added Success Message
            string jsonResponse = response.ToString();

            return jsonResponse;
        }

        [WebMethod(EnableSession = true)]
        public string ProcessInstockItemsAddToCart(string attributelist)
        {
            int SkuAddedSuccessfully = 0;
            string response = string.Empty;
            List<ZNodeShoppingCartItem> shoppingCartItemsToAdd = new List<ZNodeShoppingCartItem>();
            string successfullyAdded = string.Empty;

            // Add product to cart
            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (!string.IsNullOrEmpty(attributelist))
            {
                List<SelectedAttributeCollection> selectedAttributeCollection = attributelist.ToJsonObject<List<SelectedAttributeCollection>>(100);

                if (selectedAttributeCollection != null && selectedAttributeCollection.Count > 0)
                {
                    bool allowOutOfStockToUser = Roles.IsUserInRole("ADMIN") || Roles.IsUserInRole("CUSTOMER SERVICE REP") ? true : false;
                    //Create Product from product ID
                    ZNode.Libraries.ECommerce.Catalog.ZNodeProduct Product = ZNode.Libraries.ECommerce.Catalog.ZNodeProduct.Create(selectedAttributeCollection[0].ProductID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                    int count = 1;
                    foreach (SelectedAttributeCollection selectedAttributes in selectedAttributeCollection)
                    {
                        //Bind SKU Price on last attribute selected index changes
                        ZNode.Libraries.ECommerce.Catalog.ZNodeSKU SKU = ZNode.Libraries.ECommerce.Catalog.ZNodeSKU.CreateByProductAndAttributes(Product.ProductID, selectedAttributes.SelectedAttributes);
                        // Set Attributes Description to Description property
                        SKU.AttributesDescription = FormatSKUDescription(selectedAttributes.SelectedAttributeDescription);
                        if (SKU != null && SKU.SKUID > 0)
                        {
                            //Check if this product is in stock
                            if (!allowOutOfStockToUser && SKU.QuantityOnHand < selectedAttributes.Quantity)
                            {
                                successfullyAdded = "false";
                            }
                            else
                            {
                                // Create shopping cart item
                                ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                                Product.SelectedSKU = SKU;
                                item.Product = new ZNodeProductBase(Product);
                                item.Quantity = selectedAttributes.Quantity;
                                item.Notes = selectedAttributes.Comment;
                                item.AddProductAsLineItem = true;
                                shoppingCartItemsToAdd.Add(item);
                                successfullyAdded = "true";
                            }
                            response = response + selectedAttributes.RowNumber + "|" + successfullyAdded + ",";
                        }
                        count++;
                    }
                    if (shoppingCartItemsToAdd.Count == selectedAttributeCollection.Count)
                    {
                        response = string.Empty;
                        // If shopping cart is null, create in session
                        if (shoppingCart == null)
                        {
                            shoppingCart = new ZNodeShoppingCart();
                            shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                        }
                        foreach (ZNodeShoppingCartItem shoppingCartItem in shoppingCartItemsToAdd)
                        {
                            // Add item to cart
                            if (shoppingCart.AddToCart(shoppingCartItem))
                            {
                                SkuAddedSuccessfully = SkuAddedSuccessfully + 1;
                                ZNodeSavedCart.AddToSavedCart(shoppingCartItem);
                                successfullyAdded = "true";
                            }
                            else
                            {
                                successfullyAdded = "false";
                            }
                            response = response + successfullyAdded + ",";
                        }
                    }
                }
            }
            string jsonResponse = response.TrimEnd(',').ToString();

            return jsonResponse;
        }

        [WebMethod(EnableSession = true)]
        public string ValidateAttribute(string attributelist)
        {
            string response = string.Empty;
            if (!string.IsNullOrEmpty(attributelist))
            {
                List<SelectedAttributeCollection> selectedAttributeCollection = attributelist.ToJsonObject<List<SelectedAttributeCollection>>(100);

                if (selectedAttributeCollection != null && selectedAttributeCollection.Count > 0)
                {
                    //Create Product from product ID
                    ZNode.Libraries.ECommerce.Catalog.ZNodeProduct Product = ZNode.Libraries.ECommerce.Catalog.ZNodeProduct.Create(selectedAttributeCollection[0].ProductID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

                    foreach (SelectedAttributeCollection selectedAttributes in selectedAttributeCollection)
                    {
                        //Bind SKU Price on last attribute selected index changes
                        ZNode.Libraries.ECommerce.Catalog.ZNodeSKU SKU = ZNode.Libraries.ECommerce.Catalog.ZNodeSKU.CreateByProductAndAttributes(selectedAttributes.ProductID, selectedAttributes.SelectedAttributes);
                        if (SKU != null && SKU.SKUID > 0)
                        {
                            response = SKU.SKUID.ToString();
                        }
                        else
                        {
                            response = "0";
                        }
                    }
                }
            }

            return response;
        }

        [WebMethod(EnableSession = true)]
        public string GetNextLevelAttribute(string attributelist)
        {
            try
            {
                string jsonstring = string.Empty;
                List<NextLevelProductAttribute> nextLevelAttributeData = new List<NextLevelProductAttribute>();
                if (!string.IsNullOrEmpty(attributelist))
                {
                    List<SelectedAttributeCollection> selectedAttributeCollection = attributelist.ToJsonObject<List<SelectedAttributeCollection>>(100);
                    if (selectedAttributeCollection != null && selectedAttributeCollection.Count > 0)
                    {
                        zAttributeHelper attributeHelper = new zAttributeHelper();
                        DataSet dsAttributes = attributeHelper.GetNextLevelAttriutes(selectedAttributeCollection[0].ProductID, selectedAttributeCollection[0].SelectedAttributes, selectedAttributeCollection[0].CurrentAttributeTypeId);
                        try
                        {
                            if (dsAttributes != null)
                            {
                                if (dsAttributes.Tables.Count > 0)
                                {
                                    if (dsAttributes.Tables[0].Rows.Count > 0)
                                    {
                                        int count = 1;
                                        foreach (DataRow dr in dsAttributes.Tables[0].Rows)
                                        {
                                            #region[Commented code]

                                            //nextLevelAttributeData.Add(new NextLevelProductAttribute()
                                            //{
                                            //    AttributeID = Convert.ToInt32(dr["AttributeId"]),
                                            //    Name = dr["Name"].ToString()
                                            //});

                                            #endregion

                                            if (count == dsAttributes.Tables[0].Rows.Count)
                                            {
                                                jsonstring = jsonstring + Convert.ToInt32(dr["AttributeId"]) + ":" + dr["Name"].ToString();

                                                //jsonstring = jsonstring + "{'id':" + Convert.ToInt32(dr["AttributeId"]) + ",'name':\"" + dr["Name"].ToString() + "\"}";
                                            }
                                            else
                                            {
                                                jsonstring = jsonstring + Convert.ToInt32(dr["AttributeId"]) + ":" + dr["Name"].ToString() + ",";
                                            }
                                            count++;
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
                return jsonstring;
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(ex.ToString());
                return string.Empty;
            }
        }


        [WebMethod(EnableSession = true)]
        public string GetShoppingCartItemCountandSubTotal()
        {
            string jsonstring = string.Empty;
            try
            {

                ZNodeShoppingCart shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
                if (shoppingCart != null && shoppingCart.ShoppingCartItems != null)
                {
                    jsonstring = jsonstring + "ItemCount:" + shoppingCart.ShoppingCartItems.Count;
                    jsonstring = jsonstring + "," + "SubTotal:" + shoppingCart.Total;
                }
            }
            catch (Exception ex)
            {
                Zeon.Libraries.Elmah.ElmahErrorManager.CreateDebugLog(ex.ToString());
                return jsonstring;
            }
            return jsonstring;
        }

        #endregion

        #region[Get Product Price List]
        [WebMethod(EnableSession = true)]
        public PriceListResponse GetProductPriceList(string productIDs, string sessionKey)
        {
            PriceListResponse priceListResponse = new PriceListResponse();
            ZNodeGenericCollection<ZNodeProductBase> ZNodeProductCollection = HttpContext.Current.Session[sessionKey] as ZNodeGenericCollection<ZNodeProductBase>;
            if (ZNodeProductCollection != null && ZNodeProductCollection.Count > 0)
            {
                priceListResponse.PriceList = new List<PriceList>();
                foreach (ZNodeProductBase item in ZNodeProductCollection)
                {
                    PriceList priceList = new PriceList()
                    {
                        FormattedPrice = item.FormattedPrice,
                        ProductID = item.ProductID,
                        ProductNumber = item.ProductNum
                    };
                    priceListResponse.PriceList.Add(priceList);
                }
            }
            return priceListResponse;
        }

        [WebMethod(EnableSession = true)]
        public PriceListResponse GetProductPriceListByPageSize(string productIDs, int pageSize, int pageNumber, string sessionKey)
        {
            PriceListResponse priceListResponse = new PriceListResponse();
            ZNodeGenericCollection<ZNodeProductBase> ZNodeProductCollection = HttpContext.Current.Session[sessionKey] as ZNodeGenericCollection<ZNodeProductBase>;

            if (ZNodeProductCollection != null && ZNodeProductCollection.Count > 0)
            {
                priceListResponse = this.GetPrice(ZNodeProductCollection, pageSize, pageNumber, sessionKey);
            }

            return priceListResponse;
        }

        private PriceListResponse GetPrice(ZNodeGenericCollection<ZNodeProductBase> ZNodeProductCollection, int pageSize, int pageNumber, string sessionKey)
        {
            var filteredProductList = ZNodeProductCollection.OfType<ZNodeProductBase>().AsQueryable().Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            PriceListResponse priceListResponse = new PriceListResponse();
            if (filteredProductList != null && filteredProductList.Count > 0)
            {
                priceListResponse.PriceList = new List<PriceList>();
                foreach (ZNodeProductBase item in filteredProductList)
                {
                    PriceList priceList = new PriceList()
                    {
                        FormattedPrice = item.FormattedPrice,
                        ProductID = item.ProductID,
                        ProductNumber = item.ProductNum
                    };
                    priceListResponse.PriceList.Add(priceList);
                }

                int totalRecordCount = ZNodeProductCollection.Count;
                int totalPageCount = totalRecordCount % pageSize == 0 ? totalRecordCount / pageSize : ((totalRecordCount / pageSize) + 1);
                priceListResponse.NextPageNumber = pageNumber + 1;
                priceListResponse.TotalPages = totalPageCount;
                if (totalPageCount == pageNumber)
                {
                    priceListResponse.HasNextPage = false;
                    //Session.Remove(sessionKey);
                }
                else
                {
                    priceListResponse.HasNextPage = true;
                }


            }
            return priceListResponse;

        }

        #endregion

        #region[Helper Mtohods]
        /// <summary>
        /// Get the compare product HTML structure
        /// </summary>
        /// <returns></returns>
        public string GetCompareProductStructure()
        {
            string commaSeperatedProductID = string.Empty;
            DataTable productCompareInformation = new DataTable();
            StringBuilder compareProductHTML = new StringBuilder();
            ProductHelper productHelper = new ProductHelper();
            int Count = 1;
            if (Session["CompareProductIDs"] != null)
            {
                List<CompareProducts> compProds = Session["CompareProductIDs"] as List<CompareProducts>;
                if (compProds != null && compProds.Count > 0)
                {
                    foreach (CompareProducts compItems in compProds)
                    {
                        if (Count != compProds.Count)
                        {
                            commaSeperatedProductID = commaSeperatedProductID + compItems.ProductID.ToString() + ",";
                            Count++;
                        }
                        else
                        {
                            commaSeperatedProductID = commaSeperatedProductID + compItems.ProductID.ToString();
                        }
                    }
                    Count = 1;
                }
            }
            productCompareInformation = productHelper.GetCompareProductListByProductID(commaSeperatedProductID);

            if (productCompareInformation.Rows.Count > 0 && productCompareInformation != null)
            {
                ZNodeImage znodeImage = new ZNodeImage();
                compareProductHTML.Append("<div class='CompareSelectedProduct'><h3>Compare Products</h3>");
                foreach (DataRow item in productCompareInformation.Rows)
                {
                    if (Count != productCompareInformation.Rows.Count)
                    {
                        compareProductHTML.Append("<ul id='compare_" + item["ProductID"] + "'>");
                        compareProductHTML.Append("<li>");
                        compareProductHTML.Append("<div class='ThumbImg'><a href='" + item["SeoUrl"] + "'><img id='imgProduct' border='0' src=" + znodeImage.GetImageBySize(ZNodeConfigManager.SiteConfig.MaxCatalogItemSmallThumbnailWidth, item["ImagePath"].ToString()).Replace("~/", "") + " alt='" + item["ProductNum"] + "' ></a></div>");
                        compareProductHTML.Append("<div class='ProductName'><a href='" + item["SeoUrl"] + "'>" + item["ProductName"] + "</a></div>");
                        compareProductHTML.Append("<div><a onclick='zeonCatProdList.RemoveProductFromComparsion(" + item["ProductID"] + ")'  href='javascript:void(0)' class='fa fa-times-circle'></a></div>");
                        compareProductHTML.Append("</li></ul>");
                    }
                    else
                    {
                        compareProductHTML.Append("<ul id='compare_" + item["ProductID"] + "'>");
                        compareProductHTML.Append("<li>");
                        compareProductHTML.Append("<div class='ThumbImg'><a href='" + item["SeoUrl"] + "'><img id='imgProduct' border='0' src=" + znodeImage.GetImageBySize(ZNodeConfigManager.SiteConfig.MaxCatalogItemSmallThumbnailWidth, item["ImagePath"].ToString()).Replace("~/", "") + " alt='" + item["ProductNum"] + "'></a></div>");
                        compareProductHTML.Append("<div class='ProductName'><a href='" + item["SeoUrl"] + "'>" + item["ProductName"] + "</a></div>");
                        compareProductHTML.Append("<div><a onclick='zeonCatProdList.RemoveProductFromComparsion(" + item["ProductID"] + ")'  href='javascript:void(0)' class='fa fa-times-circle'></a></div>");
                        compareProductHTML.Append("</li></ul>");
                    }
                }
                compareProductHTML.Append("<div class='Button'><a href='productcomparison.aspx' class='CompareBtn'><span>Compare<span class='fa fa-refresh'></span></span></a><a href='javascript:void(0)' onclick='zeonCatProdList.ClearProductFromComparsion()'>Clear all</a></div></div>");
            }
            return compareProductHTML.ToString();
        }

        /// <summary>
        /// get checkout page url
        /// </summary>
        /// <returns>string</returns>
        private string GetCheckoutURL()
        {
            ZNodeShoppingCart shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            string link = "editaddress.aspx";
            if (shoppingCart == null || (shoppingCart != null && shoppingCart.ShoppingCartItems.Count == 0))
            {
                link = "shoppingcart.aspx";
            }
            else if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                link = "login.aspx?returnurl=editaddress.aspx";
            }

            return link;
        }

        /// <summary>
        /// get Shopping cart item total quantity count
        /// </summary>
        /// <returns></returns>
        private string GetShoppingCartTotalItemCount(ZNodeGenericCollection<ZNodeShoppingCartItem> shoppingCartItems)
        {
            int totalQuantity = 0;
            foreach (ZNodeShoppingCartItem item in shoppingCartItems)
            {
                totalQuantity += item.Quantity;
            }
            return totalQuantity.ToString();
        }

        /// <summary>
        /// Format SKU Description  to maintain UI on Shopping cart and other pages
        /// </summary>
        /// <param name="skuDescription">string</param>
        /// <returns>string</returns>
        private string FormatSKUDescription(string skuDescription)
        {
            StringBuilder formattedDesc = new StringBuilder();
            string descFormat = "<span><strong>{0}:</strong> {1}&nbsp;</span>";
            if (!string.IsNullOrWhiteSpace(skuDescription))
            {
                string[] splitter = { "<br/>" };
                string[] descArray = skuDescription.Split(splitter, StringSplitOptions.None);
                if (descArray.Length > 0)
                {
                    foreach (string attr in descArray)
                    {
                        if (attr.IndexOf('-') > 0)
                        {
                            string[] attrArray = attr.Split('-');
                            if (attrArray.Length > 1)
                            {
                                formattedDesc.Append(string.Format(descFormat, attrArray[0], attrArray[1]));
                            }
                        }
                    }
                }
            }
            return formattedDesc.ToString();
        }
        #endregion

    }

    #region[Helper Classes]

    public class CompareProducts
    {
        public int ProductID
        {
            get;
            set;
        }

        public int CategoryID
        {
            get;
            set;
        }
    }

    public class SelectedAttributeCollection
    {
        public string SelectedAttributes { get; set; }
        public string SelectedAttributeDescription { get; set; }
        public int ProductID { get; set; }
        public string Comment { get; set; }
        public int Quantity { get; set; }
        public int CurrentAttributeTypeId { get; set; }
        public int RowNumber { get; set; }
    }

    public class NextLevelProductAttribute
    {
        public int AttributeID { get; set; }
        public string Name { get; set; }
    }

    public class ProductCompareHelper
    {
        public int ProductCount { get; set; }
        public string ProductCompareHTML { get; set; }
        public bool IsAlreadyExist { get; set; }
    }

    public class PriceListResponse
    {
        public List<PriceList> PriceList { get; set; }
        public int NextPageNumber { get; set; }
        public int TotalPages { get; set; }
        public bool HasNextPage { get; set; }
    }

    public class PriceList
    {
        public int ProductID { get; set; }
        public string ProductNumber { get; set; }
        public string FormattedPrice { get; set; }


    }
    #endregion

    #region[String Extension]
    /// <summary>
    /// Summary Extension for Converting Objext to JSON 
    /// </summary>
    public static class Extension
    {
        public static T ToJsonObject<T>(this string obj, int recursionDepth)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.RecursionLimit = recursionDepth;
            return serializer.Deserialize<T>(obj);
        }

        public static string ToJsonString(this object obj, int recusionDepth)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.RecursionLimit = recusionDepth;
            return serializer.Serialize(obj);
        }
    }

    #endregion

}

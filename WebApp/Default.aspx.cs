﻿using SquishIt.Framework;
using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Default Page class.
    /// </summary>
    public partial class Default : CommonPageBase
    {
        #region Member Variables
        private bool isApplyStyle = true;
        private ContentPage contentPage;
        #endregion

        #region Public Methods
        /// <summary>
        /// Set UI culture value in cookie.
        /// </summary>
        /// <param name="localeID">localeID selected</param>
        public void SetUICulture(int localeID)
        {
            ZNode.Libraries.DataAccess.Service.LocaleService localeService = new ZNode.Libraries.DataAccess.Service.LocaleService();
            ZNode.Libraries.DataAccess.Entities.Locale local = localeService.GetByLocaleID(localeID);
            string cultureValue = local.LocaleCode;

            // Remove the existing Cookie
            HttpContext.Current.Response.Cookies.Remove("CultureLanguage");

            // Set Culture in Cookie
            HttpCookie cookie = new HttpCookie("CultureLanguage");
            cookie.Value = cultureValue;
            cookie.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
            HttpContext.Current.Response.Cookies.Add(cookie);

            if (HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session["NewCulture"] = cultureValue;

                // Set Culture in Session
                HttpContext.Current.Session["CurrentUICulture"] = cultureValue;

                // Set NULL to last viewed category when locale change.
                HttpContext.Current.Session["BreadCrumzcid"] = null;

                // Reset the Session and Cache Value
                System.Web.HttpContext.Current.Session.Remove("CatalogConfig");

                // Set Culture in Thread
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cultureValue);
            }

            // Set locale id in Cookie
            HttpCookie cookieLocaleId = new HttpCookie("LocaleID");
            cookieLocaleId.Value = localeID.ToString();
            cookieLocaleId.Expires = DateTime.Now.AddDays(Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["CookieExpiresValue"]));
            HttpContext.Current.Response.Cookies.Add(cookieLocaleId);

            if (HttpContext.Current.Request.QueryString["redir"] != null)
            {
                HttpContext.Current.Response.Redirect(HttpContext.Current.Request.QueryString["redir"].ToString());
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/default.aspx", true);
            }
        }
        #endregion

        protected override void Page_PreInit(object sender, EventArgs e)
        {
            if (ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig == null)
            {
                throw new ApplicationException("Could not retrieve site settings from the database. Please check your database connection by browsing to Diagnostics.aspx");
            }

            // Set new locale.
            if (Request.QueryString["nlid"] != null)
            {
                this.SetUICulture(Convert.ToInt32(Request.QueryString["nlid"]));
            }

            // Clear the current category session theme if some other category selected.
            Session["CurrentCategoryTheme"] = null;

            // Default home.master page file path
            string masterPageFilePath = "~/themes/" + ZNodeCatalogManager.Theme + "/MasterPages/home.master";

            // Get Home page data
            this.contentPage = ZNodeContentManager.GetPageByName("home", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

            if (this.contentPage != null)
            {
                // Set Template
                if (this.contentPage.MasterPage != null && this.contentPage.MasterPage.Length > 0 && this.contentPage.Theme != null && this.contentPage.Theme.Length > 0)
                {
                    string masterFilePath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + this.contentPage.Theme + "/MasterPages/" + this.contentPage.MasterPage + ".master";

                    if (System.IO.File.Exists(Server.MapPath(masterFilePath)))
                    {
                        // If template master page exists, then override default master page
                        masterPageFilePath = masterFilePath;
                    }
                }

                // Set CSS
                //if (this.contentPage.CSS != null && this.contentPage.CSS.Length > 0 && this.contentPage.Theme != null && this.contentPage.Theme.Length > 0)
                //{
                //    this.isApplyStyle = false;
                //}

                // Set master page
                this.MasterPageFile = masterPageFilePath;

                // Add to context for control access
                HttpContext.Current.Items.Add("PageTitle", this.contentPage.Title);

                // SEO stuff
                ZNodeSEO seo = new ZNodeSEO();
                seo.SEODescription = this.contentPage.SEOMetaDescription;
                seo.SEOKeywords = this.contentPage.SEOMetaKeywords;
                seo.SEOTitle = this.contentPage.SEOTitle;
            }

            // Get the user account from session
            ZNodeUserAccount _userAccount = (ZNodeUserAccount)ZNodeUserAccount.CurrentAccount();
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected override void Page_Load(object sender, EventArgs e)
        {
            //Zeon Custom Code: Start
            BindThemeCSSForSquishItOrWithout();

            //Bind Canonical Tag
            BindDefultPageCananonicalTag();
            AddFaviconIconOnPage();
            // Add a reference for skype meta tag to the head section
            HtmlMeta metaTag = new HtmlMeta();
            metaTag.Name = "SKYPE_TOOLBAR";
            metaTag.Content = "SKYPE_TOOLBAR_PARSER_COMPATIBLE";
            if (this.Page.Header != null) this.Page.Header.Controls.Add(metaTag);
            HtmlMeta metaTelTag = new HtmlMeta();
            metaTelTag.Name = "format-detection";
            metaTelTag.Content = "telephone=no";
            if (this.Page.Header != null) this.Page.Header.Controls.Add(metaTelTag);
            // Add a reference for skype meta tag to the head section
            BindRobotMetaTags();
            BindOptimizedTagOnHomePage();
            //Zeon Custom Code:End
        }

        #region Zeon Custom Methods

        /// <summary>
        /// Bind Theme CSS Fo rSquishIt Or Without using web.config flag
        /// </summary>
        private void BindThemeCSSForSquishItOrWithout()
        {
            bool isapplySquishIT = ConfigurationManager.AppSettings["ApplySquishIT"] != null ? bool.Parse(ConfigurationManager.AppSettings["ApplySquishIT"].ToString()) : false;

            if (isapplySquishIT)
            {
                this.ApplySquishITCss();
            }
            else
            {
                ApplyCssWithoutUsingSquishIT();
            }
        }

        #region[Zeon Custom Code for Render Css]

        /// <summary>
        /// Apply Css Without Using SquishIT
        /// </summary>
        private void ApplyCssWithoutUsingSquishIT()
        {
            if (this.isApplyStyle)
            {
                //Include BootstrapCSS
                IncludeCSS(this, "bootstrap/css/bootstrap.min.css");
                //Include Default CSS
                IncludeCSS(this, ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS).Replace("~/", string.Empty));

                // Add a reference for StyleSheet to the head section
                if (this.Page.Header != null)
                {
                    //Include Fonts
                    string fontURL = GetFontURL();

                    IncludeCSS(this, fontURL);
                    //Include Jquery CSS
                    IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css");
                    IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css");
                    IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css");
                    IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css"); //PRFT Custom Code
                    //Include MetaTag
                    this.Page.Header.Controls.Add(new LiteralControl("<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />"));

                }
            }
            else
            {
                // HTML
                //Include BootstrapCSS
                IncludeCSS(this, "bootstrap/css/bootstrap.min.css");
                //Include Default CSS
                IncludeCSS(this, ZNodeCatalogManager.GetCssPathByLocale(this.contentPage.Theme, this.contentPage.CSS).Replace("~/", string.Empty));

                // Add a reference for StyleSheet to the head section
                if (this.Page.Header != null)
                {
                    //Include Fonts
                    //string fontURL = GetFontURL();

                    //IncludeCSS(this, fontURL);
                    //Include Jquery CSS
                    IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css");
                    IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css");
                    IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css");
                    IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css"); //PRFT Custom Code
                    //Include MetaTag
                    this.Page.Header.Controls.Add(new LiteralControl("<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />"));
                }
            }
        }

        /// <summary>
        /// Apply SquishIT Css
        /// </summary>
        private void ApplySquishITCss()
        {  
            string userAgent = Request.UserAgent; //entire UA string
            string browser = Request.Browser.Type; //Browser name and Major Version #
            int browserVersion = Request.Browser.MajorVersion;
            string include = string.Empty;

            if (this.isApplyStyle)
            {
                // HTML 
                if (userAgent != null && userAgent.Contains("Trident") && userAgent.Contains("Trident/5.0"))
                {
                    ApplyIE9CssPatch(include);
                }
                else
                {
                    include = (Bundle.Css()
                        .Add("~/bootstrap/css/bootstrap.min.css")
                        .Add(ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS))
                        .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css")
                        .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css")
                        .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css")
                        .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css") //PRFT Custom Code
                        .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString()));

                    // Add a reference for StyleSheet to the head section
                    if (this.Page.Header != null)
                    {
                        string fontURL = GetFontURL();
                        include = include + "<link href='" + fontURL + "' rel='stylesheet' type='text/css'>";
                        //include = include + "<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css' />";                   
                        include = include + "<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />";
                        this.Page.Header.InnerHtml = include;
                    }
                }
            }
            else
            {
                // HTML 
                if (userAgent.Contains("Trident"))
                {
                    ApplyIE9CssPatch(include);
                }
                else
                {
                    include = (Bundle.Css()
                            .Add("~/bootstrap/css/bootstrap.min.css")
                            .Add(ZNodeCatalogManager.GetCssPathByLocale(this.contentPage.Theme, this.contentPage.CSS))
                            .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery.selectbox.css")
                            .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui-1.10.0.custom.min.css")
                              .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/JsCss/jquery-ui.css")
                              .Add("~/Themes/" + ZNodeCatalogManager.Theme + "/Css/" + ZNodeCatalogManager.Theme + "_Client.css") //PRFT Custom Code
                            .Render("~/Squished/CSS/squished_" + ZNodeCatalogManager.Theme + "_#.css?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString()));

                    // Add a reference for StyleSheet to the head section
                    if (this.Page.Header != null)
                    {
                        string fontURL = GetFontURL();

                        include = include + "<link href='" + fontURL + "' rel='stylesheet' type='text/css'>";
                        // include = include + "<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css' />";
                        include = include + "<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><meta name='viewport' content='width=device-width, initial-scale=1'><meta name='viewport' content='minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no' />";
                        this.Page.Header.InnerHtml = include;
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// Bind Default Canonical tag on home page
        /// </summary>
        private void BindDefultPageCananonicalTag()
        {
            HtmlGenericControl includeConanical = new HtmlGenericControl("link");
            includeConanical.Attributes.Add("rel", "canonical");
            /*Nivi Code Start:8/10/2018 
                 Just made http to https 
                 */
            string baseUrl = "https://"+Request.Url.Authority;
            if (Request.Url.Authority.StartsWith("www."))
            {
                baseUrl = "https://" + Request.Url.Authority + "/";
            }
            else
            {
                baseUrl = "https://www." + Request.Url.Authority + "/";
            }
            /*Nivi Code End:8/10/2018            
                */
            includeConanical.Attributes.Add("href", baseUrl);
            // Add a reference for canonical to the head section 
            if (this.Page.Header != null) this.Page.Header.Controls.Add(includeConanical);
        }

        /// <summary>
        /// Bind Optimized Tag on Home Page
        /// </summary>
        public void BindOptimizedTagOnHomePage()
        {
            bool isOptimizedTagEnabled = false;
            if (ConfigurationManager.AppSettings["OptimizedTagEnabled"] != null && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["OptimizedTagEnabled"]))
            {
                isOptimizedTagEnabled = ConfigurationManager.AppSettings["OptimizedTagEnabled"].ToString().Equals("1") ? true : false;
                if (isOptimizedTagEnabled)
                {
                    HtmlGenericControl includeScript = new HtmlGenericControl("script");
                    includeScript.Attributes.Add("src", Resources.CommonCaption.OptimizedTagValue);
                    if (this.Page.Header != null) this.Page.Header.Controls.Add(includeScript);
                }
            }
          
        }
        #endregion
    }
}

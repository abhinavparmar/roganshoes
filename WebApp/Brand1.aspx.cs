﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the Brand Page class.
    /// </summary>
    public partial class Brand : CommonPageBase
    {
        private int brandId = 0;

        protected override void Page_PreInit(object sender, EventArgs e)
        {
            // Clear the current category session theme if some other category selected.
            Session["CurrentCategoryTheme"] = null;

            ZNodeManufacturer manufacturer = null;

            this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/brand.master";

            // Get Brand id from querystring  
            if (Request.Params["mid"] != null)
            {
                this.brandId = int.Parse(Request.Params["mid"]);

                manufacturer = ZNodeManufacturer.Create(this.brandId, ZNodeConfigManager.SiteConfig.PortalID);
            }

            if (manufacturer == null)
            {
                Response.Redirect("~/default.aspx");
            }

            // Add to http context so it can be shared with user controls
            HttpContext.Current.Items.Add("Brand", manufacturer);

            // Set Default SEO values
            //Znode Old Code:Starts
            // ZNodeSEO seo = new ZNodeSEO();
            //seo.SEOTitle = manufacturer.Name;
            //seo.SEODescription = manufacturer.Name; ;
            //seo.SEOKeywords = manufacturer.Name;
            //Znode Old Code:Ends
            GetPageSEODetails(brandId, manufacturer.Name);//Zeon Custom Call to bind SEO Details

        }

        #region Zeon Custom Code

        /// <summary>
        ///  get seo detail from Extn Table
        /// </summary>
        /// <param name="manufacturerID"></param>
        private void GetPageSEODetails(int manufacturerID, string manufacturerName)
        {
            ZNodeSEO seo = new ZNodeSEO();
            string SEOUrl=string.Empty;
            if (this.brandId > 0)
            {
                ManufacturerExtn manufactExtn = null;
                if (HttpContext.Current.Items["BrandExtn"] != null) { manufactExtn = HttpContext.Current.Items["BrandExtn"] as  ManufacturerExtn; }
                ZNode.Libraries.DataAccess.Service.ManufacturerExtnService mfrSer = new ZNode.Libraries.DataAccess.Service.ManufacturerExtnService();
                ZNode.Libraries.DataAccess.Entities.TList<ZNode.Libraries.DataAccess.Entities.ManufacturerExtn> manufacturerList = mfrSer.GetByManufacturerID(brandId);
                if (manufacturerList != null && manufacturerList.Count > 0)
                {
                    seo.SEOTitle = !string.IsNullOrEmpty(manufacturerList[0].SEODescription) ? manufacturerList[0].SEODescription : manufacturerName;
                    seo.SEODescription = !string.IsNullOrEmpty(manufacturerList[0].SEODescription) ? manufacturerList[0].SEODescription : manufacturerName;
                    seo.SEOKeywords = !string.IsNullOrEmpty(manufacturerList[0].SEODescription) ? manufacturerList[0].SEOKeywords : manufacturerName;
                    manufactExtn=manufacturerList[0];
                    HttpContext.Current.Items.Add("BrandExtn", manufactExtn);
                }
            }
        }
        #endregion
    }
}

﻿var isQuickWatchEventFired = false;
var QuickViewHelper = function (args) {
    this.Controls = args.Controls;
    this.Messages = args.Messages;
};

QuickViewHelper.prototype = {
    Init: function () {
        this.SetQuickViewLink();
        this.ManageStoreLocatorPhoneLinkByDevice();
    },
    ShowQuickViewPopup: function (popupWidth, iframe) {
        $j('body.Body').addClass('overflow');
        $j("#divQuickWatch").dialog({
            title: "Quick View",
            closeOnEscape: true,
            closeText: 'hide',
            draggable: false,
            //show: 'blind',
            //hide: 'blind',
            modal: true,
            resizable: false,
            zIndex: 900,
            width: popupWidth,
            height: 530,
            dialogClass: 'Quickwatch',
            scrolllock: false,
            close: function (event, ui) {
                $j(iframe).attr('src', '');
                $j('body.Body').removeClass('overflow');
                isQuickWatchEventFired = false;
            }
        });
    },
    SetSrcInframe: function (productID) {
        //isQuickWatchEventFired = true;
        //$j(iframe).attr('src', '');
        //var iframe = $j("#" + this.Controls.ifzQuickView);
        //var iframeSrc = "zQuickView.aspx?zpid=" + productID;
        //$j(iframe).attr('src', iframeSrc);
        //var poupWidth = 757;
        //if ($j(window).width() >= 768 && $j(window).width() <= 1024) {
        //    poupWidth = 727;
        //}
        //this.ShowQuickViewPopup(poupWidth, iframe);
        ////Changes for Ipad and tablet;
        //$j(window).resize(function () {
        //    if (isQuickWatchEventFired == true) {
        //        quickViewHelper.ShowQuickViewPopup(poupWidth, iframe);
        //    }
        //});
        return false;
    },
    UpdateMiniCart: function (cartItemCount, cartSubTotal) {
        //debugger;
        $j.find('[id*="_lblCartItemCount"]')[0].innerHTML = cartItemCount;
        $j.find('[id*="_lblCartSubTotal"]')[0].innerHTML = cartSubTotal;

        var miniCartFlyOut = new MiniCartFlyOut();
        miniCartFlyOut.ShowMiniCart();
        setTimeout(function () { miniCartFlyOut.HideMiniCart(); }, 2000);
    },
    OpenImageZoomOverlay: function (largeImageSize, smallImageSize, altimages) {
        //debugger;
        var images;
        //var altimages = $j("#"+"CatalogItemImage").attr('src');
        if (largeImageSize != null && smallImageSize != null) {
            images = altimages.replace('/' + smallImageSize + '/', '/' + largeImageSize + '/');
        }
        else {
            images = altimages.replace('/250/', '/450/');
        }

        $j("#" + "_divEnlargedImage").html("");
        var generateImgTag = "<img border='0' alt='MaximizedImage' src=" + images + " />";
        $j("#" + "_divEnlargedImage").append(generateImgTag);
        $j("#" + "_divEnlargedImage").dialog({
            position: { my: "center", at: "center", of: window },
            resizable: false,
            modal: true,
            width: 'auto'
        });
        $j("#ui-dialog-title-dialog").hide();
        $j(".ui-dialog-titlebar").removeClass('ui-widget-header');
    },
    CloseVideoArena: function () {
        $j(".CataLogImage").show();
        $j(".ProductVideo").hide();
    },
    ShowVideoArena: function () {
        $j(".CataLogImage").hide();
        $j(".ProductVideo").show();
    },
    ShowCatalogImagePanel: function () {
        $j(".CataLogImage").show();
        $j(".ProductVideo").hide();
    },
    SetQuickViewLink: function () {
        if ($j(window).width() <= 767) {
            //Hide Quick View Link
            $j(".QuickViewLink").attr('style', 'display:none;');
        }
        else {
            $j(".QuickViewLink").attr('style', 'display:block;');
        }
    },
    ShowSizeChartLink: function (obj) {
        window.open(obj.href, "sizeChartWindow", "width=650,height=700,scrollbars=yes");
        return false;
    },
    CloseQuickWatch: function () {
        $j("#divQuickWatch").dialog("close");
        var hostUrl = location.protocol + "//" + location.host
        var completeURL = hostUrl + "/" + "shoppingcart.aspx";
        window.top.location.href = completeURL

    },
    Detect: function () {
        document.cookie = 'znode' + escape('nothing')
        document.cookie = 'znodecookie';
        if (document.cookie.indexOf("znodecookie") < 0) {
            alert("you must enable cookies in your browser settings in order to add items to your cart.");
            return false;
        }
        else {
            return true;
        }
    },
    ManageStoreLocatorPhoneLinkByDevice: function () {
        if ($j(window).width() >= 1024) {
            var contactControl = $j("[id$='_lblStorewiseInventory'] a");
            contactControl.removeAttr("href");
        }
    }
};



﻿var ZeonScheduleATruck = function (args) { };
ZeonScheduleATruck.prototype = {
    Init: function () {
        this.OnReady();
        $j(".ui-datepicker").on("blur", $j.proxy(this.DatePicker, this));
    },
    OnReady: function () {
        var txScheduleATrucktDateID = $j(".datesForScheduleTruck");
        $j(txScheduleATrucktDateID).datepicker({
            changeMonth: true,
            minDate: '0',
            changeYear: true,
            showOn: "button",
            buttonImage: " Themes/Default/Images/SmallCalendar.gif",
            buttonImageOnly: true
        });
    },
    DatePicker: function () {
        var txScheduleATrucktDateID = $j(".datesForScheduleTruck");
        $j(txScheduleATrucktDateID).datepicker("hide");
    }
};
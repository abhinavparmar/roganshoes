﻿var AllBrands = function (args) {
    this.Arguments = args;
};
AllBrands.prototype = {
    Init: function () {
        this.ManageAlphbetListByDevice();
    },
    ShowBrand: function (char) {
        if (char == 'All') {
            $j('[id^="dvBrandsAlpha-"]').show();
        }
        else if (char == '#') {
            char = 'Other';
            $j('[id^="dvBrandsAlpha-"]').hide();
            var ID = ("#" + 'dvBrandsAlpha-' + char);
            $j(ID).show();
        }
        else {
            $j('[id^="dvBrandsAlpha-"]').hide();
            var ID = ("#" + 'dvBrandsAlpha-' + char);
            $j(ID).show();
        }
        return false;
    },
    ShowBrandByAlphabet: function (char, currentlink) {
        if (char == 'All') {
            $j('[id^="lstAlpha-"]').show();
            $j(".ShopByBrandColumn").show();
        }
        else {
            $j(".ShopByBrandColumn").hide();
            jQuery('[id^="lstAlpha-"]').hide();
            var ID = 'lstAlpha-' + char;
            $j('[id^="' + ID + '"]').show();
            $j('[id^="' + ID + '"]').parent().show();
        }
        $j("#AlphaList >li").removeClass("Selected")
        $j(currentlink).addClass("Selected");
        return false;
    },
    ManageAlphbetListByDevice: function () {
        if ($j(window).width() < 768) {
            var count = $j("#AlphaList").children().length;
            var totalItemsCount = $j("#AlphaList >li").length;
            var maxitemcount = $j(window).width() < 768 ? 15 : 8;
            $j('#AlphaList').flexisel({
                visibleItems: 6,
                autoPlay: false,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: {
                    MoBportrait: {
                        changePoint: 361,
                        visibleItems: maxitemcount
                    },
                    landscape: {
                        changePoint: 641,
                        visibleItems: maxitemcount
                    }
                }
            });
            if ((totalItemsCount < maxitemcount || totalItemsCount == maxitemcount))
            {
                $j('#AlphaList').parent().next('div.nbs-flexisel-nav-left').addClass('disable');
                $j('#AlphaList').parent().next('div.nbs-flexisel-nav-left').next('div.nbs-flexisel-nav-right').addClass('disable');
            }
        }
    },
}




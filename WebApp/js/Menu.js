﻿var ua = navigator.userAgent.toLowerCase();
var isiPad = /ipad/i.test(navigator.userAgent.toLowerCase());
var isiPhone = /iphone/i.test(navigator.userAgent.toLowerCase());
var isiPod = /ipod/i.test(navigator.userAgent.toLowerCase());
var isiDevice = /ipad|iphone|ipod/i.test(navigator.userAgent.toLowerCase());
var isBlackBerry = /blackberry/i.test(navigator.userAgent.toLowerCase());
var isWindowsPhone = /windows phone/i.test(navigator.userAgent.toLowerCase());
var isAndroid = ua.indexOf("android") > -1;



var Menu = function () { };
Menu.prototype = {
    Init: function () {
        $j(".StaticMenuStyle").menu({ position: { at: "left bottom" } });
        this.ApplySubMenuClass();
        this.ApplyMenuClassToSecondElement();
        if (!this.IsMobileBrowser()) {
            this.DelayOnHover();
        }
    },
    ApplySubMenuClass: function () {
        var loopCount = $j("#myNavmenu").find("li.dropdown").length;
        for (var iLen = 0; iLen < loopCount; iLen++) {
            if ($j($j("#myNavmenu").find("li.dropdown")[iLen]).find("ul.dropdown-menu") != null) {
                $j($j("#myNavmenu").find("li.dropdown")[iLen]).find("ul.dropdown-menu").parent().addClass('SubMenu');
            }
        }
    },
    ApplyMenuClassToSecondElement: function () {
        var loopCount = $j("ul.col-sm-3").length;
        for (var iLen = 0; iLen < loopCount; iLen++) {
            if ($j($j("ul.col-sm-3")[iLen]).find("ul.CatLevelMenu") != null && $j($j("ul.col-sm-3")[iLen]).find("ul.CatLevelMenu").length > 0) {
                $j($j("ul.col-sm-3")[iLen]).find("ul.CatLevelMenu").parent().addClass("CatSubMenu");
                var windowWidth = $j(window).width();
                if ($j(window).width() < 768) {
                    $j($j("ul.col-sm-3")[iLen]).find("ul.CatLevelMenu").parent().addClass("CatSubMenu").addClass("active");
                    $j(".CatSubMenu").find("ul.CatLevelMenu").slideDown();
                    $j(".CatLevelMenu").attr('style', 'display: block;');
                } else {
                    $j($j("ul.col-sm-3")[iLen]).find("ul.CatLevelMenu").parent().addClass("CatSubMenu").removeClass("active");
                }
            }
            else {
                if ($j($j("ul.col-sm-3")[iLen]).parent().find("ul.CatLevelMenu").length == 0) {
                    $j($j("ul.col-sm-3")[iLen]).addClass('SecondLevelMenu');
                }
            }
        }
    },
    DelayOnHover: function () {
        var timeout;
        $j('.navbar-default .navbar-nav li.dropdown').hover(
        function () {
            $j('.navbar-default .navbar-nav li.dropdown ul.dropdown-menu').hide();
            $j('.navbar-default .navbar-nav li.dropdown').removeClass('activetab');
            $j(this).addClass('activetab');
            timeout = setTimeout(function () {
                $j('.navbar-default .navbar-nav li.dropdown.activetab').children('ul.dropdown-menu').show();
            }, 500);
        }, function () {
            clearTimeout(timeout);
            $j('.navbar-default .navbar-nav li.dropdown ul.dropdown-menu').hide();
        });
    },
    IsMobileBrowser: function () {
        var isMobile = false;
        if (isiPad) {
            isMobile = true;
            //alert('ipad');
        }
        else if (isiPhone) {
            isMobile = true;
        }
        else if (isiDevice) {
            isMobile = true;
        }
        else if (isBlackBerry) {
            isMobile = true;
        }
        else if (isAndroid) {
            isMobile = false;
        }
        return isMobile;
    }
};

$j(document).ready(function () {
    $j(".CatSubMenu").find(".fa.fa-minus").click(function () {
        if ($j(this).next("ul").css('display') == 'block') {
            $j(this).closest('li').addClass("minus");
        }
        else {
            $j(this).closest('li').removeClass("minus");
        }
        $j(this).next("ul").slideToggle();

        //var indexChar = $j(".CatLevelMenu").attr('style').indexOf('block');
        //$j(".CatSubMenu").find("ul.CatLevelMenu").slideToggle();
        //if (indexChar > 0) {
        //    $j(".CatLevelMenu").parent().addClass("minus");
        //} else {
        //    $j(".CatLevelMenu").parent().removeClass("minus");
        //}

        return false;
    });

    $j(".LastCategory").click(function (evt) {
        evt.stopPropagation();
        window.location = $j(this).attr('href');
    });

    $j(".CustomerServiceLinks .SearchButton").click(function () {
        if (isMobileView()) {
            if (($j("#Container").hasClass('BreadCrumbTopSpacer'))) {
                $j("#Container").removeClass('BreadCrumbTopSpacer');
            } else {
                $j("#Container").addClass('BreadCrumbTopSpacer');
            }
        }

    })

    function isMobileView() {
        
         if ($j(window).width() < 767) {
             return true;
         }
         return false;
    }
    $j(function () {

        $j("#SearchText").click(function () {
            if (isMobileView()) {
                $j("#Container").addClass('topHeaderSpacer');
            }
        })

        $j(document).on('focusin', ' #SearchText', function () {
            if (isMobileView()) {
                $j("#Container").addClass('topHeaderSpacer');
            }
        })
        $j(document).on('focusout', '#SearchText', function () {
            if (isMobileView()) {
                $j("#Container").removeClass('topHeaderSpacer');
            }
        })


        $j(" input,textarea").click(function () {
            if (isMobileView()) {
                $j("#Container").addClass('unfixed');
            }
            })

        $j(document).on('focusin', ' input, textarea', function () {
            if (isMobileView()) {
                $j("#Container").addClass('unfixed');
            }
            })
        $j(document).on('focusout', ' input, textarea', function () {
            if (isMobileView()) {
                $j("#Container").removeClass('unfixed');
            }
        });

    });
});

$j(window).resize(function () {
    var loopCount = $j("ul.col-sm-3").length;
    for (var iLen = 0; iLen < loopCount; iLen++) {
        if ($j($j("ul.col-sm-3")[iLen]).find("ul.CatLevelMenu") != null) {
            $j($j("ul.col-sm-3")[iLen]).find("ul.CatLevelMenu").parent().addClass("CatSubMenu");
            var windowWidth = $j(window).width();
            if (windowWidth < 768) {
                $j($j("ul.col-sm-3")[iLen]).find("ul.CatLevelMenu").parent().addClass("CatSubMenu").addClass("active");
                $j(".CatSubMenu").find("ul.CatLevelMenu").slideDown();
                $j(".CatLevelMenu").attr('style', 'display: block;');
            } else {
                $j($j("ul.col-sm-3")[iLen]).find("ul.CatLevelMenu").parent().addClass("CatSubMenu").removeClass("active");
            }
        }
    }


    if ($j(window).width() < 360) {
        if ($j('#myNavmenu').hasClass('in')) {
            var stwidth = jQuery('#myNavmenu').width();
            $j('body,.BottomMenu,.overlay').css('left', stwidth);
            $j('#HeaderWrapper').css('left', parseInt(stwidth) - 15);
        }
    }
});

$j('#UserName').keyup(function () {
    $j('#Email').val(this.value);
});

$j(document).ready(function () {
    var winWidth = $j(window).width();
    var contentHeight = $j('.text-content .text-block').innerHeight();
    if ($j('.text-block').text() == "") {
        $j('#divSiteWideBanner').addClass('hideDivNocontent');
    } else {
        if (winWidth < 767) {
            if (contentHeight > 5) {
                $j('.moreless-box').show();
                $j(".more").click(function (e) {
                    $j('.text-content').css("height", "auto");
                    $j('.text-content').removeClass("less-content");
                    $j('.more').hide();
                    $j('.less').css("display", "inline");
                });
                $j(".less").click(function (e) {
                    $j('.text-content').removeAttr("style");
                    $j('.text-content').addClass("less-content");
                    $j('.less').hide();
                    $j('.more').css("display", "inline");
                });
            }
            else {
                $j('.moreless-box').hide();
            }
        }
    }
});
$j(document).ready(function () {
    if ($j(window).width() >= 767) {
        $j('#liStoreLocation').hide();
        $j('#liMyAccount').hide();
        $j('#liLogOut').hide();
    }
});


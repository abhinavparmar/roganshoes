﻿var Checkout = function (args) {
    this.Arguments = args;
};
Checkout.prototype = {
    Init: function () {
    },
    showWait: function () {
        for (var i = 0; i < Page_Validators.length; i++) {
            if (Page_Validators[i].id.indexOf("CustomValidator1") < 0) {
                var visible = $j('#' + Page_Validators[i].controltovalidate).is(':visible');
                ValidatorEnable(Page_Validators[i], visible)
            }
        }
        if (Page_ClientValidate('')) { this.ProgessBar(); }
    },
    ProgessBar: function () {
        var divControl = 'progressBarDiv';
        if (document.documentElement.scrollHeight != null && document.documentElement.scrollHeight > 0) {
            document.getElementById(divControl).style.height = document.documentElement.scrollHeight + 'px';
        }
        else {
            document.getElementById(divControl).style.height = document.documentElement.clientHeight + 'px';
        }
        document.getElementById(divControl).style.width = document.documentElement.clientWidth + 'px';
        document.getElementById(divControl).style.zIndex = 1000000;
        document.getElementById(divControl).style.backgroundColor = '#736F6E';
        document.getElementById(divControl).style.position = 'absolute';
        document.getElementById(divControl).style.left = '0px';
        document.getElementById(divControl).style.top = '0px';
        document.getElementById(divControl).style.display = '';
    },
    validateCardForExpiry: function (sender, args) {
        var selYear = $j("[id$='lstYear']").val();
        var selMonth = $j("[id$='lstMonth']").val();
        var lblMessage = $j("[id$='lblCardExpiryMessage']");
        if (selYear != "0" && selMonth != "0") {
            var currentTime = new Date();
            if (selYear == currentTime.getFullYear() && currentTime.getMonth() + 1 > selMonth) {
                args.IsValid = false;
                lblMessage.show();
            }
            else {
                args.IsValid = true;
                lblMessage.hide();
            }
        }
        else {
            lblMessage.hide();
        }
    }
};

//Znode Default Functions
//function submitOrder(ctrlId, btdId) {
//    if (Page_ClientValidate('')) {
//        document.getElementById(ctrlId).disabled = true;
//        if (!IsIE9()) __doPostBack(btdId, '');
//        return true;
//    }
//    return true;
//}

//function IsIE9() {
//    var rv = -1; // Return value assumes failure.
//    if (navigator.appName == 'Microsoft Internet Explorer') {
//        var ua = navigator.userAgent;
//        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
//        if (re.exec(ua) != null)
//            rv = parseFloat(RegExp.$1);
//    }
//    return parseInt(rv) == 9;
//}
//Znode Default Functions
function CheckBoxRequired_ClientValidate(sender, e) {
    e.IsValid = $j(".AcceptedAgreement input:checkbox").is(':checked');
}
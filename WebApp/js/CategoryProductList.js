﻿var pageSize = 8;
var pageNumber = 1;

var CategoryProductList = function (args) {
    this.Controls = args.Controls;
    this.Messages = args.Messages;
    var counter, maxProducts, previousCategory;
    var addToCompareFailedDilog, maxProductLimitReachedDialog;
    var addToCompareFailedTitleMessage, warningTitleMessage, prodctAddedTitle, dialogwidth;
    this.WebServicePageUrl = "/zeonasynchelper.asmx/GetProductPriceListByPageSize";
    this.WebServiceSuccessResponseDelegate = Function.createDelegate(this, this.BindGetPriceSuccessResponse);
    this.WebServiceErrorResponseDelegate = Function.createDelegate(this, this.WebServiceErrorResponse);

    //PRFT Custom Code : For setting focus on check box and dropdown list
    if (window.location.search.length == 0) {
        $j("#lstFilter").focus();
        this.setCookie("plpfocus", "");
    }
    else {
        var selectedfocus = getCookie("plpfocus");
        
        if (selectedfocus == "SortBy") {
            $j("#lstFilter").focus();
        }
        else if (selectedfocus == "Checkbox") {
            if ($j("[id*='chkFacets']:checked").length > 0) {
                $j("[id*='chkFacets']:checked").each(function (x) {
                    $j(this).focus();
                })
            }
            else {
                $j("#lstFilter").focus();
                this.setCookie("plpfocus", "");
            }
        }
        else {
            $j("#lstFilter").focus();
        }

    }
};

CategoryProductList.prototype = {
    Init: function () {
        counter = $j("#" + this.Controls.hdnTotalComparableProducts).val();
        maxProducts = $j("#" + this.Controls.hdnCompareProductCount).val();
        previousCategory = $j("#" + this.Controls.hdnPreviousCategory).val();

        addToCompareFailedDilog = "#" + this.Controls.divAddToCompareFailed;
        maxProductLimitReachedDialog = "#" + this.Controls.divMaxProductLimitReached;

        addToCompareFailedTitleMessage = this.Messages.AddToCompareFailedTitle;
        warningTitleMessage = this.Messages.WarningTitle;
        prodctAddedTitle = this.Messages.ProdctAddedTitle;

        this.ManagePageByDevice();
        this.ApplyLazyLoad();
        this.UpdateCategoryDescriptionHtmlStructure();


        //this.GetPriceDetails()
    },
    ApplyLazyLoad: function () {
        $j(function () {
            $j("img.lazy").show().lazyload({ event: "scrollstop" });
        });
    },
    ApplySortByPageNarrowResult: function (obj) {
        var hostUrl = location.protocol + "//" + location.host + location.pathname
        var querystring = "?";
        var pageKey = "page";
        var pageSizeKey = "s";
        var sortKey = "sort";
        var pageURL = window.location.href;
        var count = 1;
        if (pageURL != '') {
            var querystringarray = pageURL.split('?');
            if (querystringarray != '' && querystringarray.length > 1) {
                var array = querystringarray[1].split("&");
                for (var i = 0; i < array.length; i++) {
                    var keyValuePair = array[i].split('=');
                    if (i == array.length - 1) {
                        if (keyValuePair[0].toLowerCase() == sortKey) {
                            querystring = querystring + keyValuePair[0] + "=" + obj.value;
                        }
                        else {
                            if (keyValuePair[0].toLowerCase() == pageKey) {
                                querystring = querystring + keyValuePair[0] + "=1";
                            }
                            else {
                                querystring = querystring + keyValuePair[0] + "=" + keyValuePair[1];
                            }
                        }
                    }
                    else {
                        if (keyValuePair[0].toLowerCase() == sortKey) {
                            querystring = querystring + keyValuePair[0] + "=" + obj.value + "&";
                        }
                        else {
                            if (keyValuePair[0].toLowerCase() == pageKey) {
                                querystring = querystring + keyValuePair[0] + "=1" + "&";
                            }
                            else {
                                querystring = querystring + keyValuePair[0] + "=" + keyValuePair[1] + "&";
                            }
                        }
                    }
                }
                if (array.length == 2) {
                    querystring = querystring + "&" + pageKey + "=1" + "&" + pageSizeKey + "=48" + "&" + sortKey + "=" + obj.value;
                }
            }
            else {
                querystring = "?" + pageKey + "=1" + "&" + pageSizeKey + "=48" + "&" + sortKey + "=" + obj.value;
            }

            var url = hostUrl + querystring;
            //window.location.href = url;
            location.href = url
            this.setCookie("plpfocus", "SortBy");
            //alert(url);
        }
    },
    ApplyShowNarrowResult: function (obj) {
        var hostUrl = location.protocol + "//" + location.host + location.pathname
        var querystring = "?";
        var pageKey = "page";
        var pageSizeKey = "s";
        var sortKey = "sort";
        var count = 1;
        var pageURL = window.location.href;
        if (pageURL != '') {
            var querystringarray = pageURL.split('?');
            if (querystringarray != '' && querystringarray.length > 1) {
                var array = querystringarray[1].split("&");
                for (var i = 0; i < array.length; i++) {
                    var keyValuePair = array[i].split('=');
                    var keyValuePair = array[i].split('=');
                    if (i == array.length - 1) {
                        if (keyValuePair[0].toLowerCase() == pageSizeKey) {
                            querystring = querystring + keyValuePair[0] + "=" + obj.value;
                        }
                        else {
                            if (keyValuePair[0].toLowerCase() == pageKey) {
                                querystring = querystring + keyValuePair[0] + "=1";
                            }
                            else {
                                querystring = querystring + keyValuePair[0] + "=" + keyValuePair[1];
                            }
                        }
                    }
                    else {
                        if (keyValuePair[0].toLowerCase() == pageSizeKey) {
                            querystring = querystring + keyValuePair[0] + "=" + obj.value + "&";
                        }
                        else {
                            if (keyValuePair[0].toLowerCase() == pageKey) {
                                querystring = querystring + keyValuePair[0] + "=1" + "&";
                            }
                            else {
                                querystring = querystring + keyValuePair[0] + "=" + keyValuePair[1] + "&";
                            }
                        }
                    }
                }

                if (array.length == 2) {
                    querystring = querystring + "&" + pageKey + "=1" + "&" + pageSizeKey + "=" + obj.value + "&" + sortKey + "=0";
                }
            }
            else {
                querystring = "?" + pageKey + "=1" + "&" + pageSizeKey + "=" + obj.value + "&" + sortKey + "=0";
            }

            var url = hostUrl + querystring;
            //window.location.href = url;
            location.href = url
            //alert(url);
        }
    },
    ManagePageByDevice: function () {
        if ($j(window).width() <= 360) {
            dialogwidth = 280;
        }
        else {
            dialogwidth = 319;
        }
    },
    RemoveProductFromComparsion: function (productid) {
        $j.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/zeonasynchelper.asmx/removecompareproductfromsession",
            data: '{"productID":"' + productid + '"}',
            datatype: "json",
            success: function (data) {
                $j("#MiniProductCompare").html(data.d.ProductCompareHTML);
                var count = parseInt(data.d.ProductCount, 10);
                if (count == 0) {
                    $j('.CompareSelectedProduct').attr('style', 'display:none');
                }
                else {
                    $j("#compare_" + productid).remove();
                }
                counter = data.d.ProductCount;
            },
            error: function (result) {
                $j(addToCompareFailedDilog).dialog({
                    title: addToCompareFailedTitleMessage,
                    resizable: false,
                    draggable: false,
                    dialogClass: 'fixed-box',
                    height: 75,
                    modal: true
                });
                $j(addToCompareFailedDilog).parent().addClass("AddToCompareFailedPopUp")
            }
        });
    },
    ClearProductFromComparsion: function () {
        $j.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/zeonasynchelper.asmx/clearproductfromcomparsion",
            datatype: "json",
            success: function (data) {
                $j('.CompareSelectedProduct').remove();
                $j('span[data-pid] > input:checkbox').prop("checked", false);
                counter = '0';
            },
            error: function (result) {
                $j(addToCompareFailedDilog).dialog({
                    title: addToCompareFailedTitleMessage,
                    resizable: false,
                    draggable: false,
                    dialogClass: 'fixed-box',
                    height: 75,
                    modal: true
                });
                $j(addToCompareFailedDilog).parent().addClass("AddToCompareFailedPopUp")
            }
        });
    },
    AddProductToCompareList: function (control) {
        var element = control;
        counter++;
        var divId = "#" + this.Controls.divAddMoreItems;
        var divIdAlreadyExist = "#" + this.Controls.divAlreadyProductExist;
        var hdnTotalProd = "#" + this.Controls.hdnTotalComparableProducts;
        var hdnPrevproduct = "#" + this.Controls.hdnPreviousCategory;
        if (counter > maxProducts) {
            var categoryID = $j(control).data("catid");
            if (previousCategory != '' && previousCategory != categoryID) {
                $j("#" + this.Controls.divCategoryChangedWarning).dialog({
                    title: warningTitleMessage,
                    height: 150,
                    resizable: false,
                    draggable: false,
                    width: dialogwidth,
                    dialogClass: 'fixed-box',
                    modal: true,
                    buttons: {
                        "Ok": function () {
                            $j(this).dialog("close");
                            var productID = $j(element).data("pid");
                            $j.ajax({
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: "/zeonasynchelper.asmx/addcompareproductidtosession",
                                data: '{"productID":"' + productID + '", "categoryID":"' + categoryID + '"}',
                                datatype: "json",
                                success: function (data) {
                                    counter = data.d.ProductCount;
                                    $j("#MiniProductCompare").html(data.d.ProductCompareHTML);
                                    previousCategory = categoryID;
                                    $j(divId).dialog({
                                        title: prodctAddedTitle,
                                        resizable: false,
                                        height: 170,
                                        width: dialogwidth,
                                        draggable: false,
                                        dialogClass: 'fixed-box',
                                        modal: true,
                                        buttons: {
                                            "View Comparison": function () {
                                                window.location = "productcomparison.aspx";
                                            },
                                            "Add More": function () {
                                                $j(hdnTotalProd).val(counter++);
                                                $j(this).dialog("close");
                                                return false;
                                            }
                                        }
                                    });
                                    $j(divId).parent().addClass("ProductAddedPopUp");
                                },
                                error: function (result) {
                                    return false;
                                }
                            });
                        },
                        Cancel: function () {
                            $j(this).dialog("close");
                            return false;
                        }
                    }
                });
                $j("#" + this.Controls.divCategoryChangedWarning).parent().addClass("ProductAddedPopUp");
                return false;
            } else {
                $j("#" + this.Controls.lblMaxProductWarning).text($j("#" + this.Controls.lblMaxProductWarning).text().replace('{0}', maxProducts));
                var productID = $j(element).data("pid");
                if ($j(".CompareSelectedProduct ul#compare_" + productID).length > 0) {
                    $j(divIdAlreadyExist).dialog({
                        title: prodctAddedTitle,
                        resizable: false,
                        height: 170,
                        dialogClass: 'fixed-box',
                        width: dialogwidth,
                        draggable: false,
                        modal: true,
                        buttons: {
                            "View Comparison": function () {
                                window.location = "productcomparison.aspx";
                            },
                            "Ok": function () {
                                $j(this).dialog("close");
                                return false;
                            }
                        }
                    });
                    $j(divIdAlreadyExist).parent().addClass("ProductAddedPopUp");
                }
                else {
                    $j(maxProductLimitReachedDialog).dialog({
                        title: warningTitleMessage,
                        height: 190,
                        resizable: false,
                        width: dialogwidth,//345
                        draggable: false,
                        dialogClass: 'fixed-box',
                        modal: true,
                        buttons: {
                            "View Comparison": function () {
                                window.location = "productcomparison.aspx";
                            },
                        }
                    });
                    $j(maxProductLimitReachedDialog).parent().addClass("CategoryChangedWarning");
                }
                return false;
            }
        } else {
            var categoryID = $j(control).data("catid");
            if (previousCategory != '' && previousCategory != categoryID) {
                $j("#" + this.Controls.divCategoryChangedWarning).dialog({
                    title: warningTitleMessage,
                    width: 350,
                    resizable: false,
                    modal: true,
                    dialogClass: 'fixed-box',
                    draggable: false,
                    buttons: {
                        "Ok": function () {
                            $j(this).dialog("close");
                            var productID = $j(element).data("pid");
                            $j.ajax({
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: "/zeonasynchelper.asmx/addcompareproductidtosession",
                                data: '{"productID":"' + productID + '", "categoryID":"' + categoryID + '"}',
                                datatype: "json",
                                success: function (data) {
                                    counter = data.d.ProductCount;
                                    $j("#MiniProductCompare").html(data.d.ProductCompareHTML);
                                    previousCategory = categoryID;
                                    $j(divId).dialog({
                                        title: prodctAddedTitle,
                                        resizable: false,
                                        height: 170,
                                        width: dialogwidth,
                                        draggable: false,
                                        dialogClass: 'fixed-box',
                                        modal: true,
                                        buttons: {
                                            "View Comparison": function () {
                                                window.location = "productcomparison.aspx";
                                            },
                                            "Add More": function () {
                                                $j(hdnTotalProd).val(counter++);
                                                $j(this).dialog("close");
                                                return false;
                                            }
                                        }
                                    });
                                    $j(divId).parent().addClass("ProductAddedPopUp");

                                },
                                error: function (result) {
                                    return false;
                                }
                            });
                        },
                        Cancel: function () {
                            $j(this).dialog("close");
                            return false;
                        }
                    }
                });
                $j("#" + this.Controls.divCategoryChangedWarning).parent().addClass("CategoryChangedWarning");
                return false;
            } else {
                var productID = $j(control).data("pid");
                var categoryID = $j(control).data("catid");
                $j.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/zeonasynchelper.asmx/addcompareproductidtosession",
                    data: '{"productID":"' + productID + '", "categoryID":"' + categoryID + '"}',
                    datatype: "json",
                    success: function (data) {
                        counter = data.d.ProductCount;
                        $j("#MiniProductCompare").html(data.d.ProductCompareHTML);
                        if (data.d.IsAlreadyExist == true) {
                            $j(divIdAlreadyExist).dialog({
                                title: prodctAddedTitle,
                                resizable: false,
                                height: 170,
                                width: dialogwidth,
                                dialogClass: 'fixed-box',
                                modal: true,
                                draggable: false,
                                buttons: {
                                    "View Comparison": function () {
                                        window.location = "productcomparison.aspx";
                                    },
                                    "Ok": function () {
                                        $j(this).dialog("close");
                                        return false;
                                    }
                                }
                            });
                            $j(divIdAlreadyExist).parent().addClass("ProductAddedPopUp");
                        }
                        else {
                            $j(divId).dialog({
                                title: prodctAddedTitle,
                                resizable: false,
                                height: 170,
                                width: dialogwidth,
                                resizable: false,
                                draggable: false,
                                dialogClass: 'fixed-box',
                                modal: true,
                                buttons: {
                                    "View Comparison": function () {
                                        window.location = "productcomparison.aspx";
                                    },
                                    "Add More": function () {
                                        $j(this).dialog("close");
                                        return false;
                                    }
                                }
                            });
                            $j(divId).parent().addClass("ProductAddedPopUp");
                        }
                    },
                    error: function (result) {
                        return false;
                    }
                });
                return false;
            }
        }
    },
    GetPriceDetails: function () {
        this.LoadPriceList();
    },
    LoadPriceList: function () {
        //var requestdata = this.GetData();
        var requestdata = 0;
        //this.SendRequest(requestdata);
        this.GetPriceByPageSize(requestdata);
    },
    SendRequest: function (requestData) {
        var prodIds = requestData;
        pageNumber = 1;
        $j.ajax({
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            cache: false,
            url: this.WebServicePageUrl,
            data: '{"productIDs":"' + prodIds + '"}',
            success: this.WebServiceSuccessResponseDelegate,
            error: this.WebServiceErrorResponseDelegate
        });
    },
    GetPriceByPageSize: function (prodIds) {
        var sessionKey = this.Messages.ProductListSessionKey
        $j.ajax({
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            cache: false,
            url: this.WebServicePageUrl,
            data: '{"productIDs":"' + prodIds + '","pageSize":"' + pageSize + '","pageNumber":"' + pageNumber + '","sessionKey":"' + sessionKey + '"}',
            success: this.WebServiceSuccessResponseDelegate,
            error: this.WebServiceErrorResponseDelegate
        });
    },
    GetData: function () {
        var items = $j(".ProductListItem >.ItemNumber");
        var sbItems = new Sys.StringBuilder();
        for (var i = 0; i < items.length; i++) {
            var partNumber = $j($j(items[i]).find('[id$="lblItemHash"]')[0]).text();
            sbItems.append(partNumber + ",");
        }
        return sbItems.toString();
    },
    BindGetPriceSuccessResponse: function (serviceResponse, textStatus) {
        var priceList = serviceResponse.d;
        this.SetPriceOnDataList(priceList);
        if (priceList.HasNextPage == true) {
            pageNumber = priceList.NextPageNumber;
            this.GetPriceByPageSize(0);
        }
    },
    WebServiceErrorResponse: function (XMLHttpRequest, textStatus, errorThrown) {
        //this.HideAddToCartBtn();
    },
    SetPriceOnDataList: function (responseData) {
        if (responseData != null) {
            var items = jQuery(".ProductListItem");
            for (var i = 0; i < items.length; i++) {
                var partNumber = jQuery(jQuery(items[i]).find('.ItemNumber [id$="lblItemHash"]')[0]).text();
                var price = jQuery(jQuery(items[i]).find('.Price [id$="lblERPPrice"]')[0]);
                //price.html("");
                if (responseData.PriceList != null && responseData.PriceList.length > 0) {
                    for (var cnt = 0; cnt < responseData.PriceList.length; cnt++) {
                        if (responseData.PriceList[cnt] != null && typeof (responseData.PriceList[cnt]) != "undefined") {
                            if (partNumber == responseData.PriceList[cnt].ProductID) {
                                price.html("");
                                price.html(responseData.PriceList[cnt].FormattedPrice);
                            }
                        }
                    }
                }
            }
        }
        else {
            //this.HideAddToCartBtn();
        }
    },
    UpdateCategoryDescriptionHtmlStructure: function () {
        var categoryDesc = $j(".CatDec").html();
        if (categoryDesc != 'undefined' && categoryDesc != '') {
            var finalHTML = "<div class='CategoryInformation'>" + categoryDesc + "</div>";
            $j(".CatDec").html(finalHTML);
        }

        var mobileCatDesc = $j(".MobCatDesc").html();
        if (mobileCatDesc != 'undefined' && mobileCatDesc != '') {
            var finalHTML = "<div class='CategoryInformation'>" + categoryDesc + "</div>";
            $j(".MobCatDesc").html(finalHTML);
        }
    },
    setCookie: function (cname, cvalue) {
        var d = new Date();
        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    getCookie: function (cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }


};


//function setCookie(cname, cvalue) {
//    var d = new Date();
//    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
//    var expires = "expires=" + d.toGMTString();
//    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
//}

//function getCookie(cname) {
//    var name = cname + "=";
//    var decodedCookie = decodeURIComponent(document.cookie);
//    var ca = decodedCookie.split(';');
//    for (var i = 0; i < ca.length; i++) {
//        var c = ca[i];
//        while (c.charAt(0) == ' ') {
//            c = c.substring(1);
//        }
//        if (c.indexOf(name) == 0) {
//            return c.substring(name.length, c.length);
//        }
//    }
//    return "";
//}

//function checkCookie() {
//    var user = getCookie("username");
//    if (user != "") {
//        alert("Welcome again " + user);
//    } else {
//        user = prompt("Please enter your name:", "");
//        if (user != "" && user != null) {
//            setCookie("username", user, 30);
//        }
//    }
//}

//CategoryDescription read more
$j(document).ready(function () {
    var desc = $j('#CategoryDescription .CategoryInformation, #lblMobileCategoryDesc .CategoryInformation');
    if (desc != undefined && desc.html() != undefined && desc.html().length > 0) {
        var descLength = desc.html().length;
        var ismoreData = $j('#CategoryDescription .CategoryInformation p,#lblMobileCategoryDesc .CategoryInformation p').hasClass('more');
        if (descLength > 660 && ismoreData) {
            desc.hide();
            var encDesc = desc.html().substring(0, 660);
            var appendDesc = '<p class="more newDesc"></p><span class="lessMoreText">Show more &gt; </span>';
            $j('#CategoryDescription, #lblMobileCategoryDesc').append(appendDesc);
            $j('.more.newDesc').append(encDesc + '...');
            //$j('#CategoryDescription h2, #lblMobileCategoryDesc h2').hide();
        }
    }

    $j('.lessMoreText').click(function () {
        if ($j(this).hasClass('active')) {
            $j(this).removeClass('active');
            $j('#CategoryDescription .more.newDesc, #lblMobileCategoryDesc .more.newDesc').show();
            //$j('#CategoryDescription h2, #lblMobileCategoryDesc h2').hide();
            $j('#CategoryDescription .CategoryInformation').hide();
            $j('#lblMobileCategoryDesc .CategoryInformation').hide();
            $j('.lessMoreText').text('Show more >');
        } else {
            $j(this).addClass('active');
            $j('#CategoryDescription .more.newDesc, #lblMobileCategoryDesc .more.newDesc').hide();
            //$j('#CategoryDescription h2, #lblMobileCategoryDesc h2').show();
            $j('#CategoryDescription .CategoryInformation').show();
            $j('#lblMobileCategoryDesc .CategoryInformation').show();
            $j('.lessMoreText').text('Show Less ');
        }
    });

});
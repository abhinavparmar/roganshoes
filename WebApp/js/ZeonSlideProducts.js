﻿var ZeonSlideProducts = function (args) {
    this.Arguments = args;
};

ZeonSlideProducts.prototype =
{
    Init: function () {
        this.FlexSlider();
    },
    FlexSlider: function () {
        var slidername = this.Arguments.sliderID;
        if (slidername != undefined) {
            var className = "";
            var totalItemsCount = 0;
            var actualCount = 0;

            if (slidername == 'divRadioList') {
                //debugger;
                var maxItem = this.Arguments.MaxItemCount;
                //className = "AttributeRadio";
                className = $j("#" + slidername).find('ul').attr('id');
                totalItemsCount = $j('#' + slidername).find('ul.AttributeRadio').find('li').length;
                actualCount = this.Arguments.MaxItemCount;

                if (totalItemsCount < this.Arguments.MaxItemCount) {
                    maxItem = totalItemsCount;

                    if (totalItemsCount == this.Arguments.MinimumItemsShown) {
                        this.Arguments.MobPortraitItemCount = this.Arguments.MinimumItemsShown;
                        this.Arguments.MobLandItemCount = this.Arguments.MinimumItemsShown;
                        this.Arguments.TabletItemCount = this.Arguments.MinimumItemsShown;
                    }
                }
                $j('#' + className).flexisel({
                    visibleItems: maxItem,
                    autoPlay: false,
                    enableResponsiveBreakpoints: true,
                    responsiveBreakpoints: {
                        portrait: {
                            changePoint: 481,
                            visibleItems: this.Arguments.MobPortraitItemCount
                        },
                        MoBportrait: {
                            changePoint: 361,
                            visibleItems: this.Arguments.MobPortraitItemCount
                        },
                        landscape: {
                            changePoint: 641,
                            visibleItems: this.Arguments.MobLandItemCount
                        },
                        tablet: {
                            changePoint: 801,
                            visibleItems: this.Arguments.TabletItemCount
                        }
                    }
                });

            } else {
                className = $j("#" + slidername).find('ul').attr('id');
                if (className != undefined) {
                    totalItemsCount = $j("#" + slidername).find('ul > li').length;
                    actualCount = this.Arguments.MaxItemCount;

                    if (totalItemsCount < this.Arguments.MaxItemCount) {
                        this.Arguments.MaxItemCount = totalItemsCount;

                        if (totalItemsCount == this.Arguments.MinimumItemsShown) {
                            this.Arguments.MobPortraitItemCount = this.Arguments.MinimumItemsShown;
                            this.Arguments.MobLandItemCount = this.Arguments.MinimumItemsShown;
                            this.Arguments.TabletItemCount = this.Arguments.MinimumItemsShown;
                        }
                    }

                    $j('#' + className).flexisel({
                        visibleItems: this.Arguments.MaxItemCount,
                        autoPlay: false,
                        enableResponsiveBreakpoints: true,
                        responsiveBreakpoints: {
                            portrait: {
                                changePoint: 481,
                                visibleItems: this.Arguments.MobPortraitItemCount
                            },
                            MoBportrait: {
                                changePoint: 361,
                                visibleItems: this.Arguments.MobPortraitItemCount
                            },
                            landscape: {
                                changePoint: 641,
                                visibleItems: this.Arguments.MobLandItemCount
                            },
                            tablet: {
                                changePoint: 801,
                                visibleItems: this.Arguments.TabletItemCount
                            }
                        }
                    });
                }
            }
            if ((totalItemsCount < actualCount || totalItemsCount == actualCount) && className != undefined) {
                var controlID = "";
                if (slidername == 'divRadioList') {
                    controlID = '#' + className;
                } else {
                    controlID = '#' + className;
                }
                $j(controlID).parent().next('div.nbs-flexisel-nav-left').addClass('disable');
                $j(controlID).parent().next('div.nbs-flexisel-nav-left').next('div.nbs-flexisel-nav-right').addClass('disable');
            }
        }
    }
};
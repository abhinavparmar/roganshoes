﻿var shoppingcartItemDetails = "";
var shoppingCartItemCount = 0;
var MiniCartFlyOut = function (args) {
};

MiniCartFlyOut.prototype =
{
    ShowMiniCart: function () {
        $j.ajax({
            url: "zeonasynchelper.asmx/GetMiniCartItems",
            cache: false,
            dataType: "html",
            success: function (data) {
                data = $j(data).text();
                var itemdetails = shoppingcartItemDetails = data.split('$$$');
                if (itemdetails.length > 0) {
                    var items = itemdetails[0];
                    var count = shoppingCartItemCount = itemdetails[1];
                    var html = $j.parseHTML(items);
                    $j($j('.containermycart')[0]).html("");
                    $j($j('.containermycart')[0]).append(html[1]);
                    $j('.MiniShoppingCartFlyOut').show(0);
                    $j('.MiniShoppingCartFlyOut').delay(5000).hide(0);
                }
                return true;
            },
            error: function () {
                return false;
            }
        });
    },
    HideMiniCart: function () {
        $j('.MiniShoppingCartFlyOut').hide();
    },
    ShowMiniCartOnMouseOver: function () {
        if (shoppingcartItemDetails != 'undefined' && shoppingcartItemDetails.length > 0 && shoppingCartItemCount > 0) {
            var items = shoppingcartItemDetails[0];
            var count = shoppingcartItemDetails[1];
            var html = $j.parseHTML(items);
            $j($j('.containermycart')[0]).html("");
            $j($j('.containermycart')[0]).append(html[1]);
            $j('.MiniShoppingCartFlyOut').show();
            return true;
        }
        else {
            $j.ajax({
                url: "zeonasynchelper.asmx/GetMiniCartItems",
                cache: false,
                dataType: "html",
                success: function (data) {
                    data = $j(data).text();
                    var itemdetails = shoppingcartItemDetails = data.split('$$$');
                    if (itemdetails.length > 0) {
                        var items = itemdetails[0];
                        var count = shoppingCartItemCount = itemdetails[1];
                        var html = $j.parseHTML(items);
                        $j($j('.containermycart')[0]).html("");
                        $j($j('.containermycart')[0]).append(html[1]);
                        $j('.MiniShoppingCartFlyOut').show();
                    }
                    return true;
                },
                error: function () {
                    return false;
                }
            });
        }
    }
};
$j(document).ready(function () {

    $j("#ShoppingCart")
        .on("mouseenter", function () {
            var miniCartFlyOut = new MiniCartFlyOut();
            miniCartFlyOut.ShowMiniCartOnMouseOver();
        })
        .on("mouseleave", function () {
            $j('.MiniShoppingCartFlyOut').hide();
        });

});
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    public partial class PageNotFound : CommonPageBase
    {
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            ZNodeSEO seo = new ZNodeSEO();

            seo.SEOTitle = "Page Not Found : 404";
            seo.SEODescription = "Page Not Found : 404";
            seo.SEOKeywords = "Page Not Found : 404";

            this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/ZPageNotFound.Master";

            Response.Status = "404 Not Found";
            Response.StatusCode = 404;
        }
    }
}
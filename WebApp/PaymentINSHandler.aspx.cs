﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Suppliers;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using Gateway2COResponse = ZNode.Libraries.ECommerce.Payment.Gateway2COResponse;

namespace WebApp
{
    /// <summary>
    /// Represents the PaymentINSHandler Page class.
    /// </summary>
    public partial class PaymentINSHandler : ZNodePageBase
    {
        private ZNodeCheckout _checkout = null;
        private ZNodeUserAccount _userAccount = null;
        private ZNodeShoppingCart _shoppingCart = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["mode"].ToUpper().IndexOf("2CO") == 0)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(Request["mode"].ToUpper());
                this.Create2COOrder();
            }

            if (Request["mode"].ToUpper().IndexOf("PAYPAL") == 0)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(Request["mode"].ToUpper());
                this.CreatePayPalExpressCheckoutOrder();
            }
        }

        private void Create2COOrder()
        {
            string redirectURL = string.Empty;
            try
            {
                Gateway2COResponse twoCOResponse = new Gateway2COResponse();
                ZNode.Libraries.DataAccess.Service.PaymentSettingService paymentSettingService = new ZNode.Libraries.DataAccess.Service.PaymentSettingService();
                
                // Get payment setting for 2CO payment type.
                PaymentSetting paymentSetting = paymentSettingService.GetByPaymentTypeID(5)[0];
                ZNodeEncryption encrypt = new ZNodeEncryption();
                ZNodeShoppingCart cart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

                // Validate the IPN
                Gateway2CO gateway2co = new Gateway2CO();
                string hashInput = string.Concat(encrypt.DecryptData(paymentSetting.GatewayPassword), twoCOResponse.VendorId, twoCOResponse.SaleId, twoCOResponse.InvoiceListAmount);
                if (!twoCOResponse.MD5Hash.Equals(gateway2co.CalculateMD5hash(hashInput)) && !paymentSetting.TestMode)
                {
                    cart.AddErrorMessage = "Invalid IPN match from 2CO response, try again";
                    redirectURL = "~/shoppingCart.aspx";
                    throw new Exception("Invalid IPN match from 2CO response");
                }

                cart.Payment = new ZNode.Libraries.ECommerce.Payment.ZNodePayment();
                cart.Payment.PaymentName = "2CO";

                foreach (ZNodeShoppingCartItem cartItem in cart.ShoppingCartItems)
                {
                    cartItem.IsTaxCalculated = false;
                    cartItem.Product.SelectedAddonValues.IsTaxCalculated = false;
                }

                ZNode.Libraries.DataAccess.Service.ShippingService service = new ZNode.Libraries.DataAccess.Service.ShippingService();
                TList<Shipping> shipList = service.GetAll();
                Shipping selectedShip = null;
                selectedShip = shipList.Find("ShippingCode", twoCOResponse.ShipMethod);

                if (selectedShip != null)
                {
                    ZNode.Libraries.ECommerce.Shipping.ZNodeShipping ship = new ZNode.Libraries.ECommerce.Shipping.ZNodeShipping();

                    ship.ShippingID = selectedShip.ShippingID;
                    ship.ShippingName = selectedShip.ShippingCode;
                    ship.ResponseCode = "0";
                    ship.ResponseMessage = string.Empty;

                    cart.Shipping = ship;
                }

                _userAccount = ZNodeUserAccount.CurrentAccount();
                if (_userAccount == null)
                {
                    _userAccount = new ZNodeUserAccount();
                }

                string billAddressId = twoCOResponse.GetCustomValue("billing_addressId");
                string shipAddressId = twoCOResponse.GetCustomValue("shipping_addressId");
                AddressService addressService = new AddressService();
                Address billingAddress = new Address();
                if (!string.IsNullOrEmpty(billAddressId))
                {
                    billingAddress = addressService.GetByAddressID(Convert.ToInt32(billAddressId));
                    billingAddress.IsDefaultBilling = true;
                }
                
                billingAddress.FirstName = twoCOResponse.CustomerFirstName;
                billingAddress.LastName = twoCOResponse.CustomerLastName;
                billingAddress.Street = twoCOResponse.BillStreetAddress;
                billingAddress.Street1 = twoCOResponse.BillStreetAddress2;
                billingAddress.City = twoCOResponse.BillCity;
                billingAddress.StateCode = twoCOResponse.BillState;
                billingAddress.CountryCode = twoCOResponse.BillCountry;
                billingAddress.PostalCode = twoCOResponse.BillPostalCode;
                billingAddress.PhoneNumber = twoCOResponse.CustomerPhone;
                _userAccount.EmailID = twoCOResponse.CustomerEmail;
                _userAccount.BillingAddress = billingAddress;

                this._userAccount.BillingAddress = billingAddress;
                this._userAccount.BillingAddress.AccountID = _userAccount.AccountID;
                addressService.Save(this._userAccount.BillingAddress);

                // Check if Bill and Ship address are same.
                if (billAddressId == shipAddressId)
                {
                    _userAccount.ShippingAddress = billingAddress;
                }
                else
                {
                    Address shippingAddress = new Address();
                    if (!string.IsNullOrEmpty(shipAddressId))
                    {
                        shippingAddress = addressService.GetByAddressID(Convert.ToInt32(shipAddressId));
                        shippingAddress.IsDefaultShipping = true;
                    }

                    string lastname = string.Empty;
                    string firstname = twoCOResponse.ShipName;

                    if (firstname.Split(' ').Length > 1)
                    {
                        lastname = firstname.Split(' ')[1];
                        firstname = firstname.Split(' ')[0];
                    }

                    shippingAddress.FirstName = firstname;
                    shippingAddress.LastName = lastname;
                    shippingAddress.Street = twoCOResponse.ShipStreetAddress;
                    shippingAddress.Street1 = twoCOResponse.ShipStreetAddress2;
                    shippingAddress.City = twoCOResponse.ShipCity;
                    shippingAddress.StateCode = twoCOResponse.ShipState;
                    shippingAddress.CountryCode = twoCOResponse.ShipCountry;
                    shippingAddress.PostalCode = twoCOResponse.ShipPostalCode;
                    _userAccount.ShippingAddress = shippingAddress;

                    this._userAccount.ShippingAddress = shippingAddress;
                    this._userAccount.ShippingAddress.AccountID = _userAccount.AccountID;
                    addressService.Save(this._userAccount.ShippingAddress);
                }
                if (_userAccount.AccountID <= 0)
                {
                    _userAccount.AddUserAccount();
                    _userAccount.AddToSession(ZNodeSessionKeyType.UserAccount);
                }
                else
                {
                    _userAccount.UpdateUserAccount();
                }                        
               
                this._checkout = new ZNodeCheckout();
                this._checkout.PaymentSettingID = paymentSetting.PaymentSettingID;

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
                ZNodeOrderFulfillment order = new ZNodeOrderFulfillment();
                try
                {
                    log.LogActivityTimerStart();

                    order = (ZNodeOrderFulfillment)this._checkout.SubmitOrder();
                }
                catch (ZNode.Libraries.ECommerce.Entities.ZNodePaymentException ex)
                {
                    log.LogActivityTimerEnd(5003, Request.UserHostAddress.ToString(), null, null, null, ex.Message);

                    // Display payment error message
                    return;
                }
                catch (Exception exc)
                {
                    log.LogActivityTimerEnd(5003, null);

                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest : " + exc.Message);

                    // Display error page
                    return;
                }

                // Post order submission
                if (this._checkout.IsSuccess)
                {
                    log.LogActivityTimerEnd(5002, order.OrderID.ToString());
                    
                    // Worldpay
                    if (!string.IsNullOrEmpty(this._checkout.ECRedirectURL))
                    {
                        Response.Redirect(this._checkout.ECRedirectURL);
                    }
                   
                    // Make an entry in tracking event for placing an Order 
                    ZNodeTracking tracker = new ZNodeTracking();
                    if (tracker.AffiliateId.Length > 0)
                    {
                        tracker.AccountID = order.AccountID;
                        tracker.OrderID = order.OrderID;
                        tracker.LogTrackingEvent("Placed an Order");
                    }

                    this.PostSubmitOrder(order);
                    List<ZNodeOrderFulfillment> orders = new List<ZNodeOrderFulfillment>();
                    orders.Add(order);

                    // Create session for Order and Shopping cart
                    Session.Add("OrderDetail", orders);

                    ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                    // Cookie based persistent cart.
                    ZNodeSavedCart savedCart = new ZNodeSavedCart();
                    savedCart.RemoveSavedCart();                    

                    shoppingCart.AddShoppingCartToSession(order.PortalId);

                    redirectURL = "~/orderreceipt.aspx";
                }
                else
                {
                    log.LogActivityTimerEnd(5001, null, null, null, null, this._checkout.PaymentResponseText);
                    return;
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            Response.Redirect(redirectURL);
        }

        private void CreatePayPalExpressCheckoutOrder()
        {
            string redirectURL = string.Empty;
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
            ZNodeOrderFulfillment order = new ZNodeOrderFulfillment();
            if (HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()] != null)
            {
                this._userAccount = ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount) as ZNodeUserAccount;
                this._shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            }

            if (this._shoppingCart == null)
            {
                Response.Redirect("~/");
            }

            try
            {
                log.LogActivityTimerStart();

                // Get payment setting for Paypal Express Checkout payment type.
                PaymentSetting _paymentSetting = this.GetByPaymentTypeId(2);
                this._checkout = new ZNodeCheckout();
                this._checkout.PaymentSettingID = _paymentSetting.PaymentSettingID;

                order = (ZNodeOrderFulfillment)this._checkout.SubmitOrder();
            }
            catch (ZNode.Libraries.ECommerce.Entities.ZNodePaymentException ex)
            {
                log.LogActivityTimerEnd(5003, Request.UserHostAddress.ToString(), null, null, null, ex.Message);

                // Display payment error message
                return;
            }
            catch (Exception exc)
            {
                log.LogActivityTimerEnd(5003, null);

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest : " + exc.Message);
                               
                // Display error page
                return;
            }

            // Post order submission
            if (this._checkout.IsSuccess)
            {
                log.LogActivityTimerEnd(5002, order.OrderID.ToString());
                
                // Worldpay
                if (!string.IsNullOrEmpty(this._checkout.ECRedirectURL))
                {
                    Response.Redirect(this._checkout.ECRedirectURL);
                }

                // Make an entry in tracking event for placing an Order 
                ZNodeTracking tracker = new ZNodeTracking();
                if (tracker.AffiliateId.Length > 0)
                {
                    tracker.AccountID = order.AccountID;
                    tracker.OrderID = order.OrderID;
                    tracker.LogTrackingEvent("Placed an Order");
                }

                this.PostSubmitOrder(order);

                List<ZNodeOrderFulfillment> orders = new List<ZNodeOrderFulfillment>();
                orders.Add(order);
                Session.Add("OrderDetail", orders);

                // Create session for Order and Shopping cart               
                ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                // Cookie based persistent cart.
                ZNodeSavedCart savedCart = new ZNodeSavedCart();
                savedCart.RemoveSavedCart();               

                shoppingCart.AddShoppingCartToSession(order.PortalId);

                Response.Redirect("~/orderreceipt.aspx");
            }
            else
            {
                log.LogActivityTimerEnd(5001, null, null, null, null, this._checkout.PaymentResponseText);
                return;
            }

            Response.Redirect(redirectURL);
        }
        #region Post Submit Order - Set Digital Asset
        /// <summary>
        /// Post Order Submission Method
        /// </summary>
        /// <param name="order">Order information</param>
        private void PostSubmitOrder(ZNodeOrderFulfillment order)
        {
            ZNodeSupplierOption supplierOption = new ZNodeSupplierOption();
            supplierOption.SubmitOrder(order, this._checkout.ShoppingCart);

            // Set Digital Asset
            int counter = 0;
            DigitalAssetService digitalAssetService = new DigitalAssetService();

            // Loop through the Order Line Items
            foreach (OrderLineItem orderLineItem in order.OrderLineItems)
            {
                ZNodeShoppingCartItem shoppingCartItem = this._checkout.ShoppingCart.ShoppingCartItems[counter++];
                
                // Set quantity ordered
                int qty = shoppingCartItem.Quantity;
                
                // Set product id
                int productId = shoppingCartItem.Product.ProductID;
                ZNodeGenericCollection<ZNodeDigitalAsset> assignedDigitalAssets = new ZNodeGenericCollection<ZNodeDigitalAsset>();
                
                // Get Digital assets for productid and quantity
                assignedDigitalAssets = ZNodeDigitalAssetList.CreateByProductIdAndQuantity(productId, qty).DigitalAssetCollection;

                // Loop through the digital asset retrieved for this product
                foreach (ZNodeDigitalAsset digitalAsset in assignedDigitalAssets)
                {
                    DigitalAsset entity = digitalAssetService.GetByDigitalAssetID(digitalAsset.DigitalAssetID);
                    entity.OrderLineItemID = orderLineItem.OrderLineItemID; // Set OrderLineitemId property
                    digitalAssetService.Update(entity); // Update digital asset to the database
                }

                // Set retrieved digital asset collection to shopping product object
                // If product has digital assets, it will display it on the receipt page along with the product name
                shoppingCartItem.Product.ZNodeDigitalAssetCollection = assignedDigitalAssets;
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type Id</param>
        /// <returns>Returns the PaymentSetting</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            int? profileID = 0;

            if (HttpContext.Current.Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
            {
                this._userAccount = ZNodeUserAccount.CurrentAccount();
            }

            if (this._userAccount != null)
            {
                profileID = this._userAccount.ProfileID; // Get loggedIn user profile
            }

            TList<PaymentSetting> list = _pmtServ.GetByPaymentTypeID(paymentTypeID);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (list.Count == 0)
            {
                // Get All Profiles payment setting
                list.Filter = "ActiveInd = true AND ProfileID = null";
            }

            return list[0];
        }

        #endregion
    }
}

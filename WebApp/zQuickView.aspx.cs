﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;

namespace WebApp
{
    public partial class zQuickView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            IncludeCSS(this, "Themes/" + ZNodeCatalogManager.Theme + "/Css/zQuickView.css" + "?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString());
            IncludeCSS(this, GetFontURL());
            BindRobotMetaTags();//PI Task 94 Changes
        }

        /// <summary>
        /// Include the CSS on the page 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="cssfile"></param>
        private void IncludeCSS(Page page, string cssfile)
        {
            HtmlGenericControl child = new HtmlGenericControl("link");
            child.Attributes.Add("rel", "stylesheet");
            child.Attributes.Add("href", cssfile);
            child.Attributes.Add("type", "text/css");
            page.Header.Controls.Add(child);
        }

        /// <summary>
        /// Get Font URL
        /// </summary>
        /// <returns></returns>
        private string GetFontURL()
        {
            string fontURL = string.Empty;
            if (Request.IsSecureConnection)
            {
                fontURL = "https://fonts.googleapis.com/css?family=Oswald:400,300,700|Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,900,900italic,700italic" + "?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString(); ;
            }
            else
            {
                fontURL = "http://fonts.googleapis.com/css?family=Oswald:400,300,700|Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,900,900italic,700italic" + "?v=" + ConfigurationManager.AppSettings["CSSVersion"].ToString(); ;
            }
            return fontURL;
        }

        /// <summary>
        /// Bind Robot  Meta Tag
        /// </summary>
        public void BindRobotMetaTags()
        {
            string tagContent = string.Empty;
            tagContent = "NOINDEX, FOLLOW";
            HtmlMeta roboMetaTag = new HtmlMeta();
            roboMetaTag.Name = "ROBOTS";
            roboMetaTag.Content = tagContent;
            if (this.Page.Header != null) this.Page.Header.Controls.Add(roboMetaTag);
        }
    }
}
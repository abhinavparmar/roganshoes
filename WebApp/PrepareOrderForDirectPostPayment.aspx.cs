﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using log4net;
using ZNodeShoppingCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;
using EnumPaymentType = ZNode.Libraries.ECommerce.Entities.PaymentType;

namespace WebApp
{
    public partial class PrepareOrderForDirectPostPayment : System.Web.UI.Page
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (PrepareOrderForDirectPostPayment));
        private readonly CategoryService _categoryService;
        private readonly PaymentSettingService _paymentSettingService;

        private ZNodeCheckout Checkout { get; set; }
        private ZNodeShoppingCart ShoppingCart { get; set; }

        public PrepareOrderForDirectPostPayment()
        {
            _categoryService = new CategoryService();
            _paymentSettingService = new PaymentSettingService();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Checkout = new ZNodeCheckout();
            ShoppingCart = Checkout.ShoppingCart;

            if (Checkout.UserAccount != null)
            {
                // Add the checkout page listrack tracking script.
                var analytics = new ZNodeAnalytics();
                analytics.AnalyticsData.IsCheckoutPage = true;
                analytics.AnalyticsData.UserAccount = Checkout.UserAccount;
                analytics.Bind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var sb = new StringBuilder();

            foreach (var key in Session.Keys)
            {
                sb.AppendLine(string.Format("\tkey:{0}, value:{1}", key, Session[key.ToString()]));
            }

            _log.DebugFormat("The following values are in the session: {0}{1}", System.Environment.NewLine, sb);

            int total;

            var categoryList = _categoryService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID, 0, 1, out total);
            var isDefaultPortal = categoryList.Count == 0;

            var portalIds = GetShoppingCartVendors(ShoppingCart);
            _log.DebugFormat("The following portal ids are present: {0}", string.Join(",", portalIds));

            var orders = new List<ZNodeOrderFulfillment>();
            
            foreach (var portalId in portalIds)
            {
                int? profileId = null;
                if (portalId > 0)
                {
                    categoryList = _categoryService.GetByPortalID(portalId, 0, 1, out total);
                    isDefaultPortal = categoryList.Count == 0;

                    var ps = new PortalProfileService();
                    TList<PortalProfile> portalProfiles = ps.GetByPortalID(portalId);

                    var portalProfile = portalProfiles.Find(PortalProfileColumn.ProfileID, Checkout.UserAccount.ProfileID);

                    if (portalProfile != null)
                    {
                        // Assign profile Id only for franchise store.
                        profileId = Checkout.UserAccount.ProfileID;
                    }
                    else
                    {
                        PortalService portalService = new PortalService();
                        Portal otherFranchisePortal = portalService.GetByPortalID(portalId);
                        profileId = otherFranchisePortal.DefaultRegisteredProfileID;
                    }

                    Checkout.PortalID = portalId;
                }
                else
                {
                    // Getting Global Store PortalID
                    PortalService portalService = new PortalService();
                    TList<Portal> stores = portalService.GetByActiveInd(true);
                    stores.Sort("PortalID");
                    Checkout.PortalID = stores[0].PortalID;
                    Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), stores[0].PortalID)] = Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), portalId)];
                }

                Checkout.ShoppingCart = (ZNodeShoppingCart)Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), Checkout.PortalID)];

                GetControlData();

                _log.DebugFormat("PaymentSettingId: {0}", Checkout.PaymentSettingID);
                _log.DebugFormat("Additional Instructions: {0}", Checkout.AdditionalInstructions);
                _log.DebugFormat("BillingAddress: {0}", Checkout.UserAccount.BillingAddress.Street);
                _log.DebugFormat("ShippingAddress: {0}", Checkout.UserAccount.ShippingAddress.Street);

                var paymentSettings = GetPaymentSettings((int)EnumPaymentType.CREDIT_CARD, profileId, isDefaultPortal);

                if(paymentSettings != null && paymentSettings.Count > 0)
                {
                    ShoppingCart.Payment.PaymentSetting = paymentSettings[0];
                    Checkout.PaymentSettingID = paymentSettings[0].PaymentSettingID;
                }
                else
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(
                        "General Checkout Exception - UnableToProcessRequest : Users purchasing items from multiple vendors, but the site is not configured for Credit Card payment");

                    _log.Error("General Checkout Exception - UnableToProcessRequest : Users purchasing items from multiple vendors, but the site is not configured for Credit Card payment");

                    //TODO: Fix this - lblError.Text = this.GetLocalResourceObject("MultivendorNotSupported").ToString();
                    return;
                }


                var order = new ZNodeOrderFulfillment();

                try
                {
                    if(ShoppingCart.PreSubmitOrderProcess())
                    {
                        order = (ZNodeOrderFulfillment) Checkout.SubmitOrder(false);
                        orders.Add(order);
                    }
                    else
                    {
                        _log.Error(ShoppingCart.ErrorMessage);
                        return;
                    }
                }
                catch(ZNodePaymentException paymentException)
                {
                    _log.Error(paymentException);
                    return;
                }
                catch(Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest : " + ex.Message);
                    _log.Error(e);
                    return;
                }
            }

            var paymentSetting = _paymentSettingService.GetByPaymentSettingID(Checkout.PaymentSettingID);

            var directPostPaymentForm = CreateDirectPostPaymentForm(orders[0], paymentSetting);
            Response.Write(directPostPaymentForm);
        }

        private TList<PaymentSetting> GetPaymentSettings(int paymentTypeId, int? profileId, bool isDefaultPortal)
        {
            var paymentSettings = _paymentSettingService.GetAll();

            paymentSettings.ApplyFilter(
                pmt => (pmt.ProfileID == profileId || (pmt.ProfileID == null && isDefaultPortal)) &&
                       pmt.PaymentTypeID == paymentTypeId &&
                       pmt.ActiveInd == true);

            return paymentSettings;
        }

        private void GetControlData()
        {
            Checkout.ShoppingCart.Payment.BillingAddress = Checkout.UserAccount.BillingAddress;
            Checkout.ShoppingCart.Payment.ShippingAddress = Checkout.UserAccount.ShippingAddress;
            Checkout.AdditionalInstructions = Request.Form["additionalInstructions"];
            Checkout.PaymentSettingID = int.Parse(Request.Form["paymentTypeId"]);
        }

        private string CreateDirectPostPaymentForm(ZNodeOrderFulfillment order, PaymentSetting paymentSettings)
        {
            _log.DebugFormat("Is test mode? {0}", paymentSettings.TestMode);

            return string.Format(@"<form action=""{0}"" method=""POST"" name=""direct-post"" id=""direct-post"">{1}</form>", 
                paymentSettings.TestMode ? AuthorizeNet.Gateway.TEST_URL : AuthorizeNet.Gateway.LIVE_URL, 
                ConfigureAuthorizeNetDPM(order, paymentSettings));
        }

        private string ConfigureAuthorizeNetDPM(ZNodeOrderFulfillment order, PaymentSetting paymentSettings)
        {
            var crypto = new ZNodeEncryption();

            var transactionKey = crypto.DecryptData(paymentSettings.TransactionKey);
            var timestamp = AuthorizeNet.Crypto.GenerateTimestamp();
            var sequence = AuthorizeNet.Crypto.GenerateSequence();
            var login = crypto.DecryptData(paymentSettings.GatewayUsername);
            var orderAmount = order.Total;

            var dictionary = new Dictionary<string, object>();
            dictionary["x_fp_hash"] = AuthorizeNet.Crypto.GenerateFingerprint(transactionKey, login, orderAmount,
                                                                              sequence, timestamp.ToString());
            dictionary["x_fp_sequence"] = sequence;
            dictionary["x_fp_timestamp"] = timestamp.ToString();
            dictionary["x_login"] = login;
            dictionary["x_amount"] = orderAmount;
            dictionary["x_card_num"] = string.Empty;
            dictionary["x_exp_date"] = string.Empty;
            dictionary["x_card_code"] = string.Empty;
            dictionary["x_type"] = string.Format("AUTH_{0}", paymentSettings.PreAuthorize ? "ONLY" : "CAPTURE");
            dictionary["x_method"] = "CC";
            dictionary["x_version"] = "3.1";
            dictionary["x_relay_url"] = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier("SIM.aspx");
            dictionary["x_relay_response"] = "TRUE";
            dictionary["x_test_request"] = paymentSettings.TestMode.ToString().ToUpper();
            dictionary["x_delim_data"] = "FALSE";
            dictionary["X_invoice_num"] = order.OrderID;

            return String.Join(string.Empty, dictionary.Keys.Select(
                key => string.Format(@"<input type=""hidden"" name=""{0}"" value=""{1}"" id=""{0}"" />", key,
                                     dictionary[key])));
        }

        private IEnumerable<int> GetShoppingCartVendors(ZNodeShoppingCart shoppingCart)
        {
            if(shoppingCart != null)
            {
                var shoppingCartItems = shoppingCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>();

                return shoppingCartItems
                    .Select(shoppingCartItem => shoppingCartItem.Product.PortalID > 0 ? shoppingCartItem.Product.PortalID : 0)
                    .Distinct();
            }
            return new List<int>();
        }
    }
}
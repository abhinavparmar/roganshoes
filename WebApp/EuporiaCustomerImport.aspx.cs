﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    public partial class EuporiaCustomerImport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handeles Button Click Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnImportCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                string response = string.Empty;
                ZeonRegisterUsers registerUser = new ZeonRegisterUsers();
                response=registerUser.RegisterUsers();
                if (!string.IsNullOrEmpty(response))
                {
                    lblImportStatus.Text = "Customer Import Completed!!";
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Unable To Exicute Customer Import Service problem(EuporiaCustomerImport Page Mehtod name: btnImportCustomer_Click())=>ZShoeFinder Page:" + ex.StackTrace);
                lblImportStatus.Text = "Import Failed." + ex.StackTrace;
            }
        }
    }
}
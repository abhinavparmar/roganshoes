﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    public partial class AsyncHelper : System.Web.UI.Page
    {
       

        #region Properties

        /// <summary>
        /// Gets or sets Shopping cart quantity
        /// </summary>
        public int ShoppingCartQuantity
        {
            get
            {
                if (ViewState["Quantity"] != null)
                {
                    return (int)ViewState["Quantity"];
                }

                return 1;
            }

            set
            {
                ViewState["Quantity"] = value;
            }
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            string jsonResponse = "{}";
            if (Request["cmd"] != null && Request["cmd"].ToString().Equals("addtocart", StringComparison.OrdinalIgnoreCase))
            {
                int quantity = 0;
                int.TryParse(Request["quantity"].ToString(), out quantity);

                if (AddToCart(quantity))
                {
                    StringBuilder response = new StringBuilder();

                    response.Append("{\"Result\":true,\"Message\":\"Product added to cart successfully.\"");
                    response.AppendFormat(",\"ItemCount\":\"({0})\"", "1");
                    response.AppendFormat(",\"SubTotal\":\"${0}\"", "10");
                    response.Append("}");

                    jsonResponse = response.ToString();
                }
            }

            Response.Clear();
            Response.Write(jsonResponse);
            Response.End();
        }

        public bool AddToCart(int quantity)
        {
            return true;
            


        }
    }
}
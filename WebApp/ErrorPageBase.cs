using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Framework.Business;

namespace WebApp
{
    /// <summary>
    /// Represents the ErrorPageBase Page class.
    /// </summary>
    public partial class ErrorPageBase : CommonPageBase
    {
        protected  override void  Page_PreInit(object sender, EventArgs e)
        {
            #region Zeon Custom Code
            ZNodeSEO seo = new ZNodeSEO();

            seo.SEOTitle = "Error Page : 500";
            seo.SEODescription = "Error Page : 500";
            seo.SEOKeywords = "Error Page : 500";

            this.Page.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/main.master";
            // Clear the current category session theme if some other category selected.
            Session["CurrentCategoryTheme"] = null;

            Response.Status = "500 Error";
            Response.StatusCode = 500;
            #endregion
        }
    }
}
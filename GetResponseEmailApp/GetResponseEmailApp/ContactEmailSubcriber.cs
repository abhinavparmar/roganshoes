﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Data;
using System.Collections.Generic;

namespace GetResponseEmailApp
{
    class ContactEmailSubcriber
    {
        # region Private Variables

        private string api_key = ConfigurationManager.AppSettings["api_key"].ToString();
        private string api_url = ConfigurationManager.AppSettings["api_url"].ToString();
        private Hashtable ht = new Hashtable();
        string appFilePath = AppDomain.CurrentDomain.BaseDirectory + "EmailSubscriberApplicationLog.txt";

        #endregion

        #region Methods
        /// <summary>
        /// To get campaignId
        /// </summary>
        /// <param name="campaignName"></param>
        /// <returns></returns>
        public string GetCampaignId(string campaignName, string userId)
        {
            if (ht != null && ht.Count > 0)
            {
                if (ht[campaignName] != null)
                {
                    return ht[campaignName].ToString();
                }
            }
            string campaign_id = string.Empty;
            try
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                // get CAMPAIGN_ID of 'sample_marketing' campaign
                // new request object
                Hashtable _request = new Hashtable();
                _request["jsonrpc"] = "2.0";
                _request["id"] = 1;
                // set method name
                _request["method"] = "get_campaigns";
                // set conditions
                Hashtable operator_obj = new Hashtable();
                operator_obj["EQUALS"] = campaignName;
                Hashtable name_obj = new Hashtable();
                name_obj["name"] = operator_obj;
                // set params request object
                object[] params_array = { api_key, name_obj };
                _request["params"] = params_array;

                // send headers and content in one request
                // (disable 100 Continue behavior)
                System.Net.ServicePointManager.Expect100Continue = false;

                // initialize client
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
                request.Method = "POST";

                byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

                String response_string = null;

                try
                {
                    // call method 'get_messages' and get result
                    Stream request_stream = request.GetRequestStream();
                    request_stream.Write(request_bytes, 0, request_bytes.Length);
                    request_stream.Close();

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream response_stream = response.GetResponseStream();

                    StreamReader reader = new StreamReader(response_stream);
                    response_string = reader.ReadToEnd();
                    reader.Close();

                    response_stream.Close();
                    response.Close();
                }
                catch (Exception e)
                {
                    //Log Exceptions
                    LogWriter.Write(appFilePath, "Exception Occur For GetCampaignId() Method UserID=" + userId + ":" + e.ToString());
                    Environment.Exit(0);
                }

                // decode response to Json object
                Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;

                // get result
                Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;

                // get campaign id
                foreach (object key in result.Keys)
                {
                    campaign_id = key.ToString();
                }
                ht.Add(campaignName, campaign_id);
            }
            catch (Exception ex)
            {
                DBHelper db = new DBHelper();
                db.LogError(ex.ToString(), "GetCampaignId() Method" , userId);
                //Log Exceptions
                LogWriter.Write(appFilePath, "Exception Occur For GetCampaignId() Method UserID=" + userId + ":" + ex.ToString());
                
            }
            return campaign_id;
        }

        /// <summary>
        /// Get contact detail from getResponse
        /// </summary>
        /// <param name="api_key"></param>
        /// <param name="api_url"></param>
        /// <param name="campaign_id"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public string GetContactID(string email, string campaignName,string userId)
        {
            string campaign_id = GetCampaignId(campaignName, userId);
           

            string contactID = "";
            try
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                // get CAMPAIGN_ID of 'sample_marketing' campaign
                // new request object
                Hashtable _request = new Hashtable();
                _request["jsonrpc"] = "2.0";
                _request["id"] = 3;

                // set method name
                _request["method"] = "get_contacts";

                Hashtable contact_params = new Hashtable();
                contact_params["campaigns"] = campaign_id;

                Hashtable contact_params1 = new Hashtable();
                contact_params1["EQUALS"] = email;

                Hashtable name_obj1 = new Hashtable();
                name_obj1["email"] = contact_params1;

                // set params request object
                object[] add_contact_params_array = { api_key, name_obj1, contact_params };

                _request["params"] = add_contact_params_array;

                // send headers and content in one request
                // (disable 100 Continue behavior)
                System.Net.ServicePointManager.Expect100Continue = false;

                // initialize client
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
                request.Method = "POST";

                byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

                string response_string = null;

                try
                {
                    // call method 'get_contacts' and get result
                    Stream request_stream = request.GetRequestStream();
                    request_stream.Write(request_bytes, 0, request_bytes.Length);
                    request_stream.Close();

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream response_stream = response.GetResponseStream();

                    StreamReader reader = new StreamReader(response_stream);
                    response_string = reader.ReadToEnd();
                    reader.Close();

                    response_stream.Close();
                    response.Close();
                }
                catch (Exception e)
                {
                    //Log Exceptions
                    LogWriter.Write(appFilePath, "Exception Occur For GetCampaignId() Method UserID=" + userId + ":" + e.ToString());
                    Environment.Exit(0);
                }

                // decode response to Json object
                Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;

                // get result
                Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;
              

                // get campaign id
                foreach (object key in result.Keys)
                {
                    contactID = key.ToString();
                }
            }
            catch (Exception ex)
            {
                DBHelper db = new DBHelper();
                db.LogError(ex.ToString(), "GetContactID() Method",userId);
                //Log Exceptions
                LogWriter.Write(appFilePath, "Exception Occur For GetCampaignId() Method UserID=" + userId + ":" + ex.ToString());
            }
            return contactID;
        }

        /// <summary>
        /// To get Required Data For Email Subcriber in datatable
        /// </summary>
        /// <returns></returns>
        public DataTable GetRequiredDataForEmailSubcriber()
        {
            DBHelper dBHelper = new DBHelper();
            DataTable dataTable = dBHelper.GetEmailSubcriberUserDetail();
            return dataTable;
        }

        /// <summary>
        /// To update data in account table
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="contactId"></param>
        public void UpdateDataOfZnodeAccount(string userId, string contactId)
        {
            DBHelper db = new DBHelper();
            db.UpdateZnodeAccount(userId , contactId);
        }

        /// <summary>
        /// To get conatactID and Update ZnodeAccount.
        /// </summary>
        public void GetConatactIdToUpdateZNodeAccount()
        {
            DataTable dataTable = GetRequiredDataForEmailSubcriber();
            string contatcId = string.Empty;
            if (dataTable.Rows.Count > 0 && dataTable != null)
            {
                foreach (DataRow item in dataTable.Rows)
                {
                    if (!string.IsNullOrEmpty(item["CAMPAINNAME"].ToString()) && !string.IsNullOrEmpty(item["EMAILID"].ToString()) && !string.IsNullOrEmpty(item["USERID"].ToString()))
                    {
                        contatcId = GetContactID(Convert.ToString(item["EMAILID"]), Convert.ToString(item["CAMPAINNAME"]), Convert.ToString(item["USERID"]));
                        UpdateDataOfZnodeAccount(Convert.ToString(item["USERID"]), contatcId);
                    }
                }
            }
            else
            {
                Console.WriteLine("DataSet is null");
                //Log Exceptions
                LogWriter.Write(appFilePath, "DataSet is null");
            }
        }
        #endregion
    }
}

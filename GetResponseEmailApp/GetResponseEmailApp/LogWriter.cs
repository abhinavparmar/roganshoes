﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetResponseEmailApp
{
    public class LogWriter
    {
        public static void Write(string fileName, string message)
        {
            TextWriter textWriter = new StreamWriter(fileName, true);
            textWriter.WriteLine(message);
            textWriter.WriteLine("");
            textWriter.Close();
        }
    }
}

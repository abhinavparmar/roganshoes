﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetResponseEmailApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string appPath = AppDomain.CurrentDomain.BaseDirectory + "EmailSubscriberApplicationLog.txt";
            LogWriter.Write(appPath, "********Log Begin at " + DateTime.Now.ToString() + " ********");
            Console.WriteLine("Application Started.");
            ContactEmailSubcriber contactEmailSubcriber = new ContactEmailSubcriber();
            contactEmailSubcriber.GetConatactIdToUpdateZNodeAccount();
            LogWriter.Write(appPath, "********Log Ends at " + DateTime.Now.ToString() + " ********");
            Console.WriteLine("Application Ended.\n Press Enter to exit.");           
        }
    }
}

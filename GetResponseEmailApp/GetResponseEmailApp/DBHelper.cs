﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetResponseEmailApp
{
    public class DBHelper
    {

        /// <summary>
        /// Get all product attribute details for a product Id.
        /// </summary>
        /// <param name="productId">Get product attribute details for the product.</param>
        /// <returns>Returns the product attribute  details dataset.</returns>
        public DataTable GetEmailSubcriberUserDetail()
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Zeon_GetEmailSubcriberUserDetail", connection);

                // Create and fill the dataset
                DataTable datatab = new DataTable();
                connection.Open();
                adapter.Fill(datatab);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return datatab;
            }
        }

        /// <summary>
        /// Update Znode Account Table
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="contactId"></param>
        public void UpdateZnodeAccount(string userId, string contactId)
        {
            if (contactId != null)
            {
                using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
                {
                    SqlCommand command = new SqlCommand("Zeon_UpdateZnodeAccount", connection);
                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;

                    // Add parameters to command object                
                    command.Parameters.AddWithValue("@UserID", userId);
                    command.Parameters.AddWithValue("@ContactID", contactId);

                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Log the error in the table
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="methodName"></param>
        public void LogError(string exception, string methodName, string userId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("Zeon_LogError", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 0;

                // Add parameters to command object                
                command.Parameters.AddWithValue("@Exception", exception);
                command.Parameters.AddWithValue("@MethodName", methodName);
                command.Parameters.AddWithValue("@UserId", userId);

                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }
    }
}

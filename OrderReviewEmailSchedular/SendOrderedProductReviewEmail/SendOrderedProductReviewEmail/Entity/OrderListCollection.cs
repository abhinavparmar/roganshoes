﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SendOrderedProductReviewEmail.Entity
{
    [Serializable()]
    [XmlRoot]
    public class OrderList
    {
      [XmlElement("Orders")]
      public  List<Orders> Orders { get; set; }
    }

    [Serializable()]
    public class Orders
    {
        [XmlElement]
        public int OrderID { get; set; }
        [XmlElement]
        public string Firstname { get; set; }
        [XmlElement]
        public string Lastname { get; set; }
        [XmlElement]
        public string Email { get; set; }
        [XmlElement]
        public int AccountID { get; set; }
       

        [XmlElement]
        public int PortalID { get; set; }
      
        [XmlElement("Item")]
        public List<Item>  Item { get; set; }
    }

    [Serializable()]
    public class Item
    {
        [XmlElement]
        public string ProductNum { get; set; }
        [XmlElement]
        public string SKU { get; set; }
        [XmlElement]
        public string ProductFullImage { get; set; }
        [XmlElement]
        public Product Product { get; set; }
    }

    [Serializable()]
    public class Product
    {
        [XmlElement]
        public int ProductID { get; set; }

        [XmlElement]
        public string Name { get; set; }

        [XmlElement]
        public string SEOURL { get; set; }
    }


    public class StoreDetails
    {
          public int  PortalID { get; set; }
          public string StoreName { get; set; }
          public string CustomerServiceEmail { get; set; }
          public string CustomerServicePhoneNumber { get; set; }
          public string LogoPath { get; set; }
          public string FooterText { get; set; }
    }
}

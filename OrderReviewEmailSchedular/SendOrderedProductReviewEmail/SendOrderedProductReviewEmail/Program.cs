﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SearchFeedApp.Helper;
using SendOrderedProductReviewEmail.Helper;

namespace SendOrderedProductReviewEmail
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                string appPath = AppDomain.CurrentDomain.BaseDirectory;
                StringBuilder logMessage = new StringBuilder();
                logMessage.Append(DateTime.Now.ToString() + ":Sending Ordered Product Review Mail...Started" + Environment.NewLine + " " + System.DateTime.Now);
                OrderHelper ordHelper = new OrderHelper();
                ordHelper.SendOrderedProductReviewEmails();
                logMessage.Append(DateTime.Now.ToString() + ":Sending Ordered Product Review Mail...Done" + Environment.NewLine + " " + System.DateTime.Now);
                LogWritter.Write(appPath + "ApplicationLog.txt", logMessage);
            }
            catch (Exception ex)
            {
                string appPath = AppDomain.CurrentDomain.BaseDirectory;
                StringBuilder logMessage = new StringBuilder();
                logMessage.Append(ex.Message);
                logMessage.Append("Order Review Email Sending Utility Failed" + DateTime.Now.ToString());
                LogWritter.Write(appPath + "ApplicationLog.txt", logMessage);
                //Send Failure mail
                SendFailureMail(ex);
            }
        }

        /// <summary>
        /// Send Failure mail
        /// </summary>
        /// <param name="ex"></param>
        private static void SendFailureMail(Exception ex)
        {
            EmailHelper mailHelper = new EmailHelper();
            EmailParameters emailOptions = new EmailParameters();
            emailOptions.Host = ConfigurationSettings.AppSettings["Host"]; ;
            emailOptions.Port = Convert.ToUInt16(ConfigurationSettings.AppSettings["Port"]);
            emailOptions.To = ConfigurationSettings.AppSettings["ErrorEmailList"];
            emailOptions.From = ConfigurationSettings.AppSettings["EmailUsername"]; ;
            emailOptions.Username = ConfigurationSettings.AppSettings["EmailUsername"];
            emailOptions.Password = ConfigurationSettings.AppSettings["EmailPassword"];
            emailOptions.Subject = "Rogan Shoes: Order Review Email Report" + DateTime.Now.ToString();
            emailOptions.Body = "Order Review Email Sending Utility Failed" + DateTime.Now.ToString() + "<BR/" + ex.ToString();
            mailHelper.SendEmail(emailOptions);
        }
    }
}

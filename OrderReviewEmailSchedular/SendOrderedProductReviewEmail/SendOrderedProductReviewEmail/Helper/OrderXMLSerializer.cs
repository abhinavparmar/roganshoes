﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using SendOrderedProductReviewEmail.Entity;

namespace SendOrderedProductReviewEmail.Helper
{
    public class OrderXMLSerializer
    {
        public T FromXml<T>(String xml)
        {
            T xmlClass = default(T);

            try
            {
                using (TextReader reader = new StringReader(xml))
                {
                    try
                    {
                        xmlClass =
                            (T)new XmlSerializer(typeof(T)).Deserialize(reader);
                    }
                    catch (InvalidOperationException)
                    {
                        Console.Write("String passed is not XML, simply return XMLSerializer");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write("String passed is not XML, simply return XMLSerializer",ex.StackTrace);
            }

            return xmlClass;
        }

        public void DeserializeObject(string filename)
        {
            Console.WriteLine("Reading with XmlReader");

            // Create an instance of the XmlSerializer specifying type and namespace.
            XmlSerializer serializer = new
            XmlSerializer(typeof(OrderList));

            // A FileStream is needed to read the XML document.
            FileStream fs = new FileStream(filename, FileMode.Open);
            XmlReader reader = XmlReader.Create(fs);

            // Declare an object variable of the type to be deserialized.
            OrderList i;

            // Use the Deserialize method to restore the object's state.
            i = (OrderList)serializer.Deserialize(reader);
            fs.Close();

            // Write out the properties of the object.
            //Console.Write(
            //i.Orders.Count + "\t");
        
        }

        /// <summary>
        /// Save XML File
        /// </summary>
        /// <param name="xmlString"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool SaveXMLFile(string xmlString,string fileName)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlString);
           // logMessage.Append(DateTime.Now.ToString() + ":Saving XML Response" + Environment.NewLine);
            xmlDocument.Save(fileName);
            return true;
        }

        /// <summary>
        /// Deseralize xml to class
        /// </summary>
        /// <param name="serializaeString"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        public object Deserialize(string serializaeString, object className)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serializaeString);
            XmlNodeReader reader = new XmlNodeReader(doc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(className.GetType());
            object obj = ser.Deserialize(reader);
            return obj;
        }
    }
}

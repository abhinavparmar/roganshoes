﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SearchFeedApp.Helper
{
    public class LogWritter
    {
        public static void Write(string fileName, StringBuilder message)
        {
            fileName = fileName.Split('.')[0] + "-" + System.DateTime.Today.Year + "-" + System.DateTime.Today.Month + "-" + System.DateTime.Today.Day+"."+fileName.Split('.')[1];
            TextWriter textWriter = new StreamWriter(fileName,true);
            textWriter.WriteLine ("********Log Begin at " + DateTime.Now.ToString() + " ********");
            textWriter.WriteLine(message);
            textWriter.WriteLine("");
            textWriter.Close();
        }
    }
}

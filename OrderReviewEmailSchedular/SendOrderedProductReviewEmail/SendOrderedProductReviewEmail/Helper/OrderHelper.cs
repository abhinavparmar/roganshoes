﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using SearchFeedApp.Helper;
using SendOrderedProductReviewEmail.Entity;
namespace SendOrderedProductReviewEmail.Helper
{
    public class OrderHelper
    {
        #region Private Global Member
        DataTable dtPortalDetails = null;
        #endregion

        #region Public Methods

        /// <summary>
        /// Send Order Review Emails
        /// </summary>
        public void SendOrderedProductReviewEmails()
        {
            int currentOrderID = 0;
            string currentEmail = string.Empty;
            try
            {
                int orderOldDays = 0;
                string portalIDs = ConfigurationSettings.AppSettings["PortalIDs"] != null ? ConfigurationSettings.AppSettings["PortalIDs"].ToString() : string.Empty;
                int.TryParse(ConfigurationSettings.AppSettings["OrderOldInDays"].ToString(), out orderOldDays);

                if (!string.IsNullOrWhiteSpace(portalIDs))
                {
                    //get all Data Needed From Portal
                    dtPortalDetails = GetPortalDetailsByIDs(portalIDs);
                    string xmlOrderItemList = GetOrdersPlaced(portalIDs, orderOldDays);
                    if (!string.IsNullOrWhiteSpace(xmlOrderItemList))
                    {

                        OrderXMLSerializer ordXML = new OrderXMLSerializer();
                        OrderList orderList = new OrderList();
                        orderList = ordXML.Deserialize(xmlOrderItemList, orderList) as OrderList;
                        if (orderList != null && orderList.Orders != null && orderList.Orders.Count > 0)
                        {
                            for (int curItem = 0; curItem < orderList.Orders.Count; curItem++)
                            {
                                Orders currentOrder = orderList.Orders[curItem];
                                if (currentOrder != null && currentOrder.OrderID > 0)
                                {

                                    currentOrderID = currentOrder.OrderID;
                                    currentEmail = currentOrder.Email;
                                    SendMailForOrderReview(currentOrder);

                                }
                                else
                                {
                                    string appPath = AppDomain.CurrentDomain.BaseDirectory;
                                    StringBuilder logMessage = new StringBuilder();
                                    logMessage.Append("Current Order NULL =" + currentOrderID + "&" + currentEmail);
                                    LogWritter.Write(appPath + "ApplicationLog.txt", logMessage);
                                }

                            }
                        }


                    }

                }
            }
            catch (Exception ex)
            {
                string appPath = AppDomain.CurrentDomain.BaseDirectory;
                StringBuilder logMessage = new StringBuilder();
                logMessage.Append(ex.StackTrace);
                logMessage.Append("Error For Order ID=" + currentOrderID + "&" + currentEmail);
                LogWritter.Write(appPath + "ApplicationLog.txt", logMessage);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get Home Page Banner By Date and Portal id
        /// </summary>
        /// <param name="portalId"></param>
        /// <returns>datatable</returns>
        private string GetOrdersPlaced(string portalIds, int orderOldDays)
        {
            //orderOldDays = orderOldDays > 0 ? orderOldDays : 14;
             
            using (SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"]))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();
                xmlOut.AppendLine("<?xml version='1.0' encoding='UTF-8'?> <OrderList>");
                try
                {
                    SqlCommand command = new SqlCommand("Get_OrderListByPortalID", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalId", portalIds);
                    command.Parameters.AddWithValue("@DaysCount", orderOldDays);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }
                xmlOut.AppendLine("</OrderList>");
                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get all Manufacturer with its initial alphabets
        /// </summary>
        /// <returns></returns>
        private DataTable GetPortalDetailsByIDs(string portalIDs)
        {

            using (SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"]))
            {
                SqlDataAdapter command = new SqlDataAdapter("Get_StoreDetailsByIDs", connection);
                // Mark the command as store procedure
                command.SelectCommand.CommandType = CommandType.StoredProcedure;
                command.SelectCommand.CommandTimeout = 0;

                SqlParameter parameter = new SqlParameter("@portalId", SqlDbType.NVarChar);
                parameter.Value = portalIDs;
                command.SelectCommand.Parameters.Add(parameter);

                // Create and Fill the datatale
                DataTable myDatatable = new DataTable();
                command.Fill(myDatatable);

                command.Dispose();

                // close connection
                connection.Close();

                // Return the datadet.
                return myDatatable;
            }

        }

        /// <summary>
        /// Create mail Template from Order
        /// </summary>
        /// <param name="order">Orders</param>
        /// <returns>string</returns>
        private string CreateMailTemplateFromOrder(Orders order, StoreDetails storeDetails)
        {
            string mailTemplate = string.Empty;
            try
            {

                if (order != null)
                {
                    string targetEmail = order.Email;
                    string emailBodyContent = string.Empty;
                    string appPath = AppDomain.CurrentDomain.BaseDirectory;
                    string htmlFile = string.Format(ConfigurationSettings.AppSettings["MailTemplateFile"].ToString(), order.PortalID);
                    FileInfo fileinfo = new FileInfo(htmlFile);
                    if (fileinfo.Exists)
                    {
                        System.IO.StreamReader myFile = new System.IO.StreamReader(htmlFile);
                        string formatString = myFile.ReadToEnd();
                        myFile.Close();
                        StringBuilder strItemList = new StringBuilder();

                        string listFormat = EmailTextResource.ReviewLinkText;
                        string currentDomain = GetDomainString(order.Item[0].ProductFullImage);
                        string unSubscribePath = currentDomain + EmailTextResource.UnsubscribeLink + GetEncryptedAcctID(order.AccountID);
                        string storeLogo = ConfigurationSettings.AppSettings["StoreLogo_" + storeDetails.PortalID] != null && !string.IsNullOrEmpty(ConfigurationSettings.AppSettings["StoreLogo_" + storeDetails.PortalID].ToString()) ? currentDomain + ConfigurationSettings.AppSettings["StoreLogo_" + storeDetails.PortalID].ToString() : storeDetails.LogoPath.Replace("~", string.Empty).Replace("original", "170");
                        // List<int> distinctProductList = new List<int>();
                        for (int itemInd = 0; itemInd < order.Item.Count; itemInd++)
                        {
                            Item currItem = order.Item[itemInd];
                            //if (!distinctProductList.Contains(currItem.Product.ProductID))
                            {
                                string ratingPath = currentDomain + EmailTextResource.CustomerReviewLink + currItem.Product.ProductID;
                                string productPath = currentDomain + "/" + currItem.Product.SEOURL;
                                string prdImagePath = ConfigurationSettings.AppSettings["ReplaceProductImageSize"] != null && !string.IsNullOrEmpty(ConfigurationSettings.AppSettings["ReplaceProductImageSize"].ToString()) ? currItem.ProductFullImage.Replace("/55/", ConfigurationSettings.AppSettings["ReplaceProductImageSize"].ToString()) : currItem.ProductFullImage;
                                string ratingImagePath = currentDomain + ConfigurationSettings.AppSettings["RatingImagePath"].ToString();
                                strItemList.Append(string.Format(listFormat, ratingPath, currItem.Product.Name, prdImagePath, ratingPath, ratingImagePath, currItem.Product.Name, currItem.SKU, ratingPath, currItem.Product.Name, ratingPath));
                                //distinctProductList.Add(currItem.Product.ProductID);
                            }

                        }
                        formatString = formatString.Replace("#SiteURL#", currentDomain);
                        formatString = formatString.Replace("#SiteLogo#", storeLogo);
                        formatString = formatString.Replace("#CustomerCarePhoneNo#", storeDetails.CustomerServiceEmail);
                        formatString = formatString.Replace("#CustomerCareEmailId#", storeDetails.CustomerServicePhoneNumber);
                        formatString = formatString.Replace("#FirstName#", order.Firstname);
                        formatString = formatString.Replace("#LastName#", order.Lastname);
                        formatString = formatString.Replace("#CopyRightText#", storeDetails.FooterText);
                        formatString = formatString.Replace("#OrderLineItemText#", strItemList.ToString());
                        formatString = formatString.Replace("#UnSub#", unSubscribePath.ToString());
                        formatString = formatString.Replace("#ChangeContact#", unSubscribePath.ToString());
                        formatString = formatString.Replace("#SiteLink#",currentDomain);
                        if (!string.IsNullOrWhiteSpace(strItemList.ToString()))
                        {
                            mailTemplate = formatString;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                string appPath = AppDomain.CurrentDomain.BaseDirectory;
                StringBuilder logMessage = new StringBuilder();
                logMessage.Append(ex.Message);
                LogWritter.Write(appPath + "ApplicationLog.txt", logMessage);
            }
            return mailTemplate;
        }

        /// <summary>
        /// Send Each Order Email Temlplate 
        /// </summary>
        /// <param name="currentOrder"></param>
        private void SendMailForOrderReview(Orders currentOrder)
        {
            int currentOrderID = 0;
            string currentEmail = string.Empty;
            try
            {
                if (currentOrder != null && currentOrder.OrderID > 0)
                {
                    currentOrderID = currentOrder.OrderID;
                    currentEmail = currentOrder.Email;
                    StoreDetails storeDetails = GetCurrentStoreDetails(currentOrder.PortalID);
                    if (storeDetails != null && storeDetails.PortalID > 0)
                    {
                        string mailstring = CreateMailTemplateFromOrder(currentOrder, storeDetails);
                        if (!string.IsNullOrWhiteSpace(mailstring))
                        {
                            EmailHelper mailHelper = new EmailHelper();
                            EmailParameters emailOptions = new EmailParameters();
                            emailOptions.Host = ConfigurationSettings.AppSettings["Host"]; ;
                            emailOptions.Port = Convert.ToUInt16(ConfigurationSettings.AppSettings["Port"]);
                            emailOptions.To = currentOrder.Email;
                            emailOptions.From = storeDetails.CustomerServiceEmail;
                            emailOptions.Username = ConfigurationSettings.AppSettings["EmailUsername"];
                            emailOptions.Password = ConfigurationSettings.AppSettings["EmailPassword"];
                            emailOptions.Subject = ConfigurationSettings.AppSettings["MailSubject"].ToString();
                            emailOptions.Body = mailstring;
                            mailHelper.SendEmail(emailOptions);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string appPath = AppDomain.CurrentDomain.BaseDirectory;
                StringBuilder logMessage = new StringBuilder();
                logMessage.Append(ex.StackTrace);
                logMessage.Append("Error For Order ID=" + currentOrderID + "&" + currentEmail);
                LogWritter.Write(appPath + "ApplicationLog.txt", logMessage);
            }
        }

        /// <summary>
        /// Get Domain Name
        /// </summary>
        /// <param name="imagePath"></param>
        private string GetDomainString(string imagePath)
        {
            string domainName = string.Empty;
            string[] delim = new string[] { "/data" };
            string[] strBreak = imagePath.Split(delim as string[], StringSplitOptions.RemoveEmptyEntries);
            domainName = strBreak[0];
            return domainName;
        }

        /// <summary>
        /// Filter data by Portal from DATATABLE
        /// </summary>
        /// <param name="portalID">INT</param>
        private StoreDetails GetCurrentStoreDetails(int portalID)
        {
            StoreDetails storeDetails = new StoreDetails();
            DataRow[] currRow = dtPortalDetails.Select("PortalID = " + portalID);
            if (currRow != null && currRow.Length > 0)
            {
                storeDetails.PortalID = int.Parse(currRow[0]["PortalID"].ToString());
                storeDetails.StoreName = currRow[0]["StoreName"].ToString();
                storeDetails.CustomerServiceEmail = currRow[0]["CustomerServiceEmail"].ToString();
                storeDetails.CustomerServicePhoneNumber = currRow[0]["CustomerServicePhoneNumber"].ToString();
                storeDetails.LogoPath = currRow[0]["LogoPath"].ToString();
                storeDetails.FooterText = currRow[0]["FooterText"].ToString();
            }
            return storeDetails;
        }

        /// <summary>
        /// Encrypt AccountID
        /// </summary>
        /// <param name="acctID"></param>
        /// <returns></returns>
        private string GetEncryptedAcctID(int acctID)
        {
            string encrAcctID = string.Empty;
            if (ConfigurationSettings.AppSettings["RatingImagePath"] != null && !string.IsNullOrEmpty(ConfigurationSettings.AppSettings["RatingImagePath"].ToString()))
            {
                string encryptKey = ConfigurationSettings.AppSettings["EncryptionPassKey"].ToString();
                encrAcctID = ZEncryption.EncryptString(acctID.ToString(), encryptKey);

            }
            return encrAcctID;
        }
        #endregion
    }
}

/*
Run this script on:

        Multifront V6.8   -  This database will be modified

to synchronize it with:

        Multifront V7.0

You are recommended to back up your database before running this script

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[ZNodeRMARequestItem]'
GO
ALTER TABLE [dbo].[ZNodeRMARequestItem] DROP CONSTRAINT[FK__ZNodeRMAR__Reaso__7874C3FF]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[db_changes]'
GO
ALTER TABLE [dbo].[db_changes] DROP CONSTRAINT [PK__db_chang__3213E83F7FEAFD3E]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[db_schema]'
GO
ALTER TABLE [dbo].[db_schema] DROP CONSTRAINT [PK__db_schem__3213E83F7C1A6C5A]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeRMARequest]'
GO
ALTER TABLE [dbo].[ZNodeRMARequest] DROP CONSTRAINT [UQ__ZNodeRMA__9ADA6BE02334397B]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[ZNodeSKUProfileEffective]'
GO
ALTER TABLE [dbo].[ZNodeSKUProfileEffective] DROP CONSTRAINT [PK__ZNodeSKU__87015C6359D0414E]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeLuceneIndexMonitor]'
GO
CREATE TABLE [dbo].[ZNodeLuceneIndexMonitor]
(
[LuceneIndexMonitorID] [bigint] NOT NULL IDENTITY(1, 1),
[SourceID] [int] NOT NULL,
[SourceType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceTransactionType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransactionDateTime] [datetime] NOT NULL,
[IsDuplicate] [bit] NULL,
[AffectedType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IndexerStatusChangedBy] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeLuceneIndexMonitor] on [dbo].[ZNodeLuceneIndexMonitor]'
GO
ALTER TABLE [dbo].[ZNodeLuceneIndexMonitor] ADD CONSTRAINT [PK_ZNodeLuceneIndexMonitor] PRIMARY KEY CLUSTERED  ([LuceneIndexMonitorID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodePortal]'
GO
ALTER TABLE [dbo].[ZNodePortal] ADD
[ExternalID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeCatalog]'
GO
ALTER TABLE [dbo].[ZNodeCatalog] ADD
[ExternalID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeLuceneGlobalProductBoost]'
GO
CREATE TABLE [dbo].[ZNodeLuceneGlobalProductBoost]
(
[LuceneGlobalProductBoostID] [int] NOT NULL IDENTITY(1, 1),
[ProductID] [int] NOT NULL,
[Boost] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeLuceneGlobalProductBoost] on [dbo].[ZNodeLuceneGlobalProductBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductBoost] ADD CONSTRAINT [PK_ZNodeLuceneGlobalProductBoost] PRIMARY KEY CLUSTERED  ([LuceneGlobalProductBoostID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodeAddOnValue]'
GO
ALTER TABLE [dbo].[ZNodeAddOnValue] ADD
[ExternalID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportsFrequentFiltered]'
GO



ALTER PROCEDURE [dbo].[ZNode_ReportsFrequentFiltered] 
    (  
      @FromDate DATETIME,  
      @ToDate DATETIME,  
      @PortalId VARCHAR(10) = ''  
    )  
AS   
    BEGIN                                        
        SET NOCOUNT ON ;    
     
        SELECT DISTINCT  
                ( O.AccountID ),  
                (Select top 1 BillingFirstName from ZNodeOrder Where AccountID=O.AccountId) BillingFirstName,  
                (Select top 1 BillingLastName from ZNodeOrder Where AccountID=O.AccountId) BillingLastName,  
                (Select top 1 BillingCompanyName from ZNodeOrder Where AccountID=O.AccountId) BillingCompanyName, 
               
                COUNT(O.AccountId) AS 'OrderCount',  
                SUM(O.Total) AS 'Total',  
                SUM(ol.Quantity) AS 'Quantity'  
        FROM    ZNodeOrder O                  
                INNER JOIN ZNodeOrderLineItem ol ON ol.OrderID = O.OrderID and ParentOrderLineItemID is null  
        WHERE   O.OrderDate >= @FromDate  
                AND O.OrderDate <= @ToDate  
                AND (O.PortalID = @PortalId OR @PortalId = '0' OR @PortalId = '' )  
        GROUP BY 
                O.AccountId
               
        ORDER BY OrderCount desc  
                                  
    END  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNodePromotion]'
GO
ALTER TABLE [dbo].[ZNodePromotion] ADD
[ExternalID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_ReportsAffiliateFiltered]'
GO


ALTER PROCEDURE [dbo].[ZNode_ReportsAffiliateFiltered]
    (
      @FromDate DATETIME,
      @ToDate DATETIME,
      @PortalId VARCHAR(100) = ''
    )
AS
    BEGIN
        SET NOCOUNT ON ;
            SELECT ( Adrs.FirstName + ' ' + Adrs.LastName ) As 'Name',
                A.TaxID AS 'TaxID',
                P.StoreName AS 'StoreName',
                COUNT(O.AccountId) AS 'NumberOfOrders',
                SUM(O.Total) AS 'OrderValue',
                R.[Name] AS 'CommissionType',
                RC.ReferralCommission AS 'Commission',
                CASE WHEN RC.ReferralCommissionTypeID = 1
                    THEN SUM(O.Total) * RC.ReferralCommission / 100
                    WHEN RC.ReferralCommissionTypeID = 2
                    THEN RC.ReferralCommission
                END AS 'CommissionOwed',
                RC.ReferralCommissionTypeID AS ReferralCommissionTypeID
            FROM
                  ZNodeAccount A
                  INNER JOIN ZNodeOrder B ON B.ReferralAccountID = A.AccountID
				  INNER JOIN ZNodeReferralCommission RC ON RC.ReferralAccountID=A.AccountID  
                  INNER JOIN ZNodeReferralCommissionType R ON R.ReferralCommissionTypeID = RC.ReferralCommissionTypeID
                  INNER JOIN ZNodeOrder O ON O.AccountID = B.AccountID
                  INNER JOIN ZNodePortal P ON P.PortalID = O.PortalId
                  LEFT OUTER JOIN ZNodeAddress Adrs ON A.AccountID=Adrs.AccountID AND Adrs.IsDefaultBilling=1 

            WHERE  A.ReferralStatus = 'A'
              AND o.OrderID=RC.OrderID 
              AND o.OrderDate >= @FromDate
              AND o.OrderDate <= DateAdd(mi,1,@ToDate) 
              AND (P.PortalID = @PortalId OR @PortalId = '0')
                  GROUP BY p.StoreName,
                Adrs.FirstName,
                Adrs.LastName,
                A.TaxID,
                r.Name,
                RC.ReferralCommission,
                RC.ReferralCommissionTypeID
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeLuceneGlobalProductCategoryBoost]'
GO
CREATE TABLE [dbo].[ZNodeLuceneGlobalProductCategoryBoost]
(
[LuceneGlobalProductCategoryBoostID] [int] NOT NULL IDENTITY(1, 1),
[ProductCategoryID] [int] NOT NULL,
[Boost] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeProductCategoryBoost] on [dbo].[ZNodeLuceneGlobalProductCategoryBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductCategoryBoost] ADD CONSTRAINT [PK_ZNodeProductCategoryBoost] PRIMARY KEY CLUSTERED  ([LuceneGlobalProductCategoryBoostID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeAddOnValue table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_Get_List

AS


				
				SELECT
					[AddOnValueID],
					[AddOnID],
					[Name],
					[Description],
					[SKU],
					[DefaultInd],
					[DisplayOrder],
					[ImageFile],
					[ImageAltTag],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[Weight],
					[Length],
					[Height],
					[Width],
					[ShippingRuleTypeID],
					[FreeShippingInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[TaxClassID],
					[ExternalProductID],
					[ExternalProductAPIID],
					[ExternalID]
				FROM
					[dbo].[ZNodeAddOnValue]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeAddOnValue table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_Insert
(

	@AddOnValueID int    OUTPUT,

	@AddOnID int   ,

	@Name nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@SKU nvarchar (100)  ,

	@DefaultInd bit   ,

	@DisplayOrder int   ,

	@ImageFile nvarchar (MAX)  ,

	@ImageAltTag nvarchar (MAX)  ,

	@RetailPrice money   ,

	@SalePrice money   ,

	@WholesalePrice money   ,

	@RecurringBillingInd bit   ,

	@RecurringBillingInstallmentInd bit   ,

	@RecurringBillingPeriod nvarchar (50)  ,

	@RecurringBillingFrequency nvarchar (MAX)  ,

	@RecurringBillingTotalCycles int   ,

	@RecurringBillingInitialAmount money   ,

	@Weight decimal (18, 2)  ,

	@Length decimal (18, 2)  ,

	@Height decimal (18, 2)  ,

	@Width decimal (18, 2)  ,

	@ShippingRuleTypeID int   ,

	@FreeShippingInd bit   ,

	@WebServiceDownloadDte datetime   ,

	@UpdateDte datetime   ,

	@SupplierID int   ,

	@TaxClassID int   ,

	@ExternalProductID int   ,

	@ExternalProductAPIID varchar (50)  ,

	@ExternalID varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ZNodeAddOnValue]
					(
					[AddOnID]
					,[Name]
					,[Description]
					,[SKU]
					,[DefaultInd]
					,[DisplayOrder]
					,[ImageFile]
					,[ImageAltTag]
					,[RetailPrice]
					,[SalePrice]
					,[WholesalePrice]
					,[RecurringBillingInd]
					,[RecurringBillingInstallmentInd]
					,[RecurringBillingPeriod]
					,[RecurringBillingFrequency]
					,[RecurringBillingTotalCycles]
					,[RecurringBillingInitialAmount]
					,[Weight]
					,[Length]
					,[Height]
					,[Width]
					,[ShippingRuleTypeID]
					,[FreeShippingInd]
					,[WebServiceDownloadDte]
					,[UpdateDte]
					,[SupplierID]
					,[TaxClassID]
					,[ExternalProductID]
					,[ExternalProductAPIID]
					,[ExternalID]
					)
				VALUES
					(
					@AddOnID
					,@Name
					,@Description
					,@SKU
					,@DefaultInd
					,@DisplayOrder
					,@ImageFile
					,@ImageAltTag
					,@RetailPrice
					,@SalePrice
					,@WholesalePrice
					,@RecurringBillingInd
					,@RecurringBillingInstallmentInd
					,@RecurringBillingPeriod
					,@RecurringBillingFrequency
					,@RecurringBillingTotalCycles
					,@RecurringBillingInitialAmount
					,@Weight
					,@Length
					,@Height
					,@Width
					,@ShippingRuleTypeID
					,@FreeShippingInd
					,@WebServiceDownloadDte
					,@UpdateDte
					,@SupplierID
					,@TaxClassID
					,@ExternalProductID
					,@ExternalProductAPIID
					,@ExternalID
					)
				
				-- Get the identity value
				SET @AddOnValueID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeAddOnValue table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_Update
(

	@AddOnValueID int   ,

	@AddOnID int   ,

	@Name nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@SKU nvarchar (100)  ,

	@DefaultInd bit   ,

	@DisplayOrder int   ,

	@ImageFile nvarchar (MAX)  ,

	@ImageAltTag nvarchar (MAX)  ,

	@RetailPrice money   ,

	@SalePrice money   ,

	@WholesalePrice money   ,

	@RecurringBillingInd bit   ,

	@RecurringBillingInstallmentInd bit   ,

	@RecurringBillingPeriod nvarchar (50)  ,

	@RecurringBillingFrequency nvarchar (MAX)  ,

	@RecurringBillingTotalCycles int   ,

	@RecurringBillingInitialAmount money   ,

	@Weight decimal (18, 2)  ,

	@Length decimal (18, 2)  ,

	@Height decimal (18, 2)  ,

	@Width decimal (18, 2)  ,

	@ShippingRuleTypeID int   ,

	@FreeShippingInd bit   ,

	@WebServiceDownloadDte datetime   ,

	@UpdateDte datetime   ,

	@SupplierID int   ,

	@TaxClassID int   ,

	@ExternalProductID int   ,

	@ExternalProductAPIID varchar (50)  ,

	@ExternalID varchar (50)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeAddOnValue]
				SET
					[AddOnID] = @AddOnID
					,[Name] = @Name
					,[Description] = @Description
					,[SKU] = @SKU
					,[DefaultInd] = @DefaultInd
					,[DisplayOrder] = @DisplayOrder
					,[ImageFile] = @ImageFile
					,[ImageAltTag] = @ImageAltTag
					,[RetailPrice] = @RetailPrice
					,[SalePrice] = @SalePrice
					,[WholesalePrice] = @WholesalePrice
					,[RecurringBillingInd] = @RecurringBillingInd
					,[RecurringBillingInstallmentInd] = @RecurringBillingInstallmentInd
					,[RecurringBillingPeriod] = @RecurringBillingPeriod
					,[RecurringBillingFrequency] = @RecurringBillingFrequency
					,[RecurringBillingTotalCycles] = @RecurringBillingTotalCycles
					,[RecurringBillingInitialAmount] = @RecurringBillingInitialAmount
					,[Weight] = @Weight
					,[Length] = @Length
					,[Height] = @Height
					,[Width] = @Width
					,[ShippingRuleTypeID] = @ShippingRuleTypeID
					,[FreeShippingInd] = @FreeShippingInd
					,[WebServiceDownloadDte] = @WebServiceDownloadDte
					,[UpdateDte] = @UpdateDte
					,[SupplierID] = @SupplierID
					,[TaxClassID] = @TaxClassID
					,[ExternalProductID] = @ExternalProductID
					,[ExternalProductAPIID] = @ExternalProductAPIID
					,[ExternalID] = @ExternalID
				WHERE
[AddOnValueID] = @AddOnValueID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_GetByShippingRuleTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAddOnValue table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_GetByShippingRuleTypeID
(

	@ShippingRuleTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AddOnValueID],
					[AddOnID],
					[Name],
					[Description],
					[SKU],
					[DefaultInd],
					[DisplayOrder],
					[ImageFile],
					[ImageAltTag],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[Weight],
					[Length],
					[Height],
					[Width],
					[ShippingRuleTypeID],
					[FreeShippingInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[TaxClassID],
					[ExternalProductID],
					[ExternalProductAPIID],
					[ExternalID]
				FROM
					[dbo].[ZNodeAddOnValue]
				WHERE
					[ShippingRuleTypeID] = @ShippingRuleTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_GetBySKU]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAddOnValue table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_GetBySKU
(

	@SKU nvarchar (100)  
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AddOnValueID],
					[AddOnID],
					[Name],
					[Description],
					[SKU],
					[DefaultInd],
					[DisplayOrder],
					[ImageFile],
					[ImageAltTag],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[Weight],
					[Length],
					[Height],
					[Width],
					[ShippingRuleTypeID],
					[FreeShippingInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[TaxClassID],
					[ExternalProductID],
					[ExternalProductAPIID],
					[ExternalID]
				FROM
					[dbo].[ZNodeAddOnValue]
				WHERE
					[SKU] = @SKU
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_GetBySupplierID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAddOnValue table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_GetBySupplierID
(

	@SupplierID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AddOnValueID],
					[AddOnID],
					[Name],
					[Description],
					[SKU],
					[DefaultInd],
					[DisplayOrder],
					[ImageFile],
					[ImageAltTag],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[Weight],
					[Length],
					[Height],
					[Width],
					[ShippingRuleTypeID],
					[FreeShippingInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[TaxClassID],
					[ExternalProductID],
					[ExternalProductAPIID],
					[ExternalID]
				FROM
					[dbo].[ZNodeAddOnValue]
				WHERE
					[SupplierID] = @SupplierID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_GetByTaxClassID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAddOnValue table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_GetByTaxClassID
(

	@TaxClassID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AddOnValueID],
					[AddOnID],
					[Name],
					[Description],
					[SKU],
					[DefaultInd],
					[DisplayOrder],
					[ImageFile],
					[ImageAltTag],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[Weight],
					[Length],
					[Height],
					[Width],
					[ShippingRuleTypeID],
					[FreeShippingInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[TaxClassID],
					[ExternalProductID],
					[ExternalProductAPIID],
					[ExternalID]
				FROM
					[dbo].[ZNodeAddOnValue]
				WHERE
					[TaxClassID] = @TaxClassID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_GetByAddOnID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAddOnValue table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_GetByAddOnID
(

	@AddOnID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[AddOnValueID],
					[AddOnID],
					[Name],
					[Description],
					[SKU],
					[DefaultInd],
					[DisplayOrder],
					[ImageFile],
					[ImageAltTag],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[Weight],
					[Length],
					[Height],
					[Width],
					[ShippingRuleTypeID],
					[FreeShippingInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[TaxClassID],
					[ExternalProductID],
					[ExternalProductAPIID],
					[ExternalID]
				FROM
					[dbo].[ZNodeAddOnValue]
				WHERE
					[AddOnID] = @AddOnID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_GetByExternalProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAddOnValue table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_GetByExternalProductID
(

	@ExternalProductID int   
)
AS


				SELECT
					[AddOnValueID],
					[AddOnID],
					[Name],
					[Description],
					[SKU],
					[DefaultInd],
					[DisplayOrder],
					[ImageFile],
					[ImageAltTag],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[Weight],
					[Length],
					[Height],
					[Width],
					[ShippingRuleTypeID],
					[FreeShippingInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[TaxClassID],
					[ExternalProductID],
					[ExternalProductAPIID],
					[ExternalID]
				FROM
					[dbo].[ZNodeAddOnValue]
				WHERE
					[ExternalProductID] = @ExternalProductID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_GetByAddOnValueID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeAddOnValue table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_GetByAddOnValueID
(

	@AddOnValueID int   
)
AS


				SELECT
					[AddOnValueID],
					[AddOnID],
					[Name],
					[Description],
					[SKU],
					[DefaultInd],
					[DisplayOrder],
					[ImageFile],
					[ImageAltTag],
					[RetailPrice],
					[SalePrice],
					[WholesalePrice],
					[RecurringBillingInd],
					[RecurringBillingInstallmentInd],
					[RecurringBillingPeriod],
					[RecurringBillingFrequency],
					[RecurringBillingTotalCycles],
					[RecurringBillingInitialAmount],
					[Weight],
					[Length],
					[Height],
					[Width],
					[ShippingRuleTypeID],
					[FreeShippingInd],
					[WebServiceDownloadDte],
					[UpdateDte],
					[SupplierID],
					[TaxClassID],
					[ExternalProductID],
					[ExternalProductAPIID],
					[ExternalID]
				FROM
					[dbo].[ZNodeAddOnValue]
				WHERE
					[AddOnValueID] = @AddOnValueID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeAddOnValue table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_Find
(

	@SearchUsingOR bit   = null ,

	@AddOnValueID int   = null ,

	@AddOnID int   = null ,

	@Name nvarchar (MAX)  = null ,

	@Description nvarchar (MAX)  = null ,

	@SKU nvarchar (100)  = null ,

	@DefaultInd bit   = null ,

	@DisplayOrder int   = null ,

	@ImageFile nvarchar (MAX)  = null ,

	@ImageAltTag nvarchar (MAX)  = null ,

	@RetailPrice money   = null ,

	@SalePrice money   = null ,

	@WholesalePrice money   = null ,

	@RecurringBillingInd bit   = null ,

	@RecurringBillingInstallmentInd bit   = null ,

	@RecurringBillingPeriod nvarchar (50)  = null ,

	@RecurringBillingFrequency nvarchar (MAX)  = null ,

	@RecurringBillingTotalCycles int   = null ,

	@RecurringBillingInitialAmount money   = null ,

	@Weight decimal (18, 2)  = null ,

	@Length decimal (18, 2)  = null ,

	@Height decimal (18, 2)  = null ,

	@Width decimal (18, 2)  = null ,

	@ShippingRuleTypeID int   = null ,

	@FreeShippingInd bit   = null ,

	@WebServiceDownloadDte datetime   = null ,

	@UpdateDte datetime   = null ,

	@SupplierID int   = null ,

	@TaxClassID int   = null ,

	@ExternalProductID int   = null ,

	@ExternalProductAPIID varchar (50)  = null ,

	@ExternalID varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AddOnValueID]
	, [AddOnID]
	, [Name]
	, [Description]
	, [SKU]
	, [DefaultInd]
	, [DisplayOrder]
	, [ImageFile]
	, [ImageAltTag]
	, [RetailPrice]
	, [SalePrice]
	, [WholesalePrice]
	, [RecurringBillingInd]
	, [RecurringBillingInstallmentInd]
	, [RecurringBillingPeriod]
	, [RecurringBillingFrequency]
	, [RecurringBillingTotalCycles]
	, [RecurringBillingInitialAmount]
	, [Weight]
	, [Length]
	, [Height]
	, [Width]
	, [ShippingRuleTypeID]
	, [FreeShippingInd]
	, [WebServiceDownloadDte]
	, [UpdateDte]
	, [SupplierID]
	, [TaxClassID]
	, [ExternalProductID]
	, [ExternalProductAPIID]
	, [ExternalID]
    FROM
	[dbo].[ZNodeAddOnValue]
    WHERE 
	 ([AddOnValueID] = @AddOnValueID OR @AddOnValueID IS NULL)
	AND ([AddOnID] = @AddOnID OR @AddOnID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([SKU] = @SKU OR @SKU IS NULL)
	AND ([DefaultInd] = @DefaultInd OR @DefaultInd IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([ImageFile] = @ImageFile OR @ImageFile IS NULL)
	AND ([ImageAltTag] = @ImageAltTag OR @ImageAltTag IS NULL)
	AND ([RetailPrice] = @RetailPrice OR @RetailPrice IS NULL)
	AND ([SalePrice] = @SalePrice OR @SalePrice IS NULL)
	AND ([WholesalePrice] = @WholesalePrice OR @WholesalePrice IS NULL)
	AND ([RecurringBillingInd] = @RecurringBillingInd OR @RecurringBillingInd IS NULL)
	AND ([RecurringBillingInstallmentInd] = @RecurringBillingInstallmentInd OR @RecurringBillingInstallmentInd IS NULL)
	AND ([RecurringBillingPeriod] = @RecurringBillingPeriod OR @RecurringBillingPeriod IS NULL)
	AND ([RecurringBillingFrequency] = @RecurringBillingFrequency OR @RecurringBillingFrequency IS NULL)
	AND ([RecurringBillingTotalCycles] = @RecurringBillingTotalCycles OR @RecurringBillingTotalCycles IS NULL)
	AND ([RecurringBillingInitialAmount] = @RecurringBillingInitialAmount OR @RecurringBillingInitialAmount IS NULL)
	AND ([Weight] = @Weight OR @Weight IS NULL)
	AND ([Length] = @Length OR @Length IS NULL)
	AND ([Height] = @Height OR @Height IS NULL)
	AND ([Width] = @Width OR @Width IS NULL)
	AND ([ShippingRuleTypeID] = @ShippingRuleTypeID OR @ShippingRuleTypeID IS NULL)
	AND ([FreeShippingInd] = @FreeShippingInd OR @FreeShippingInd IS NULL)
	AND ([WebServiceDownloadDte] = @WebServiceDownloadDte OR @WebServiceDownloadDte IS NULL)
	AND ([UpdateDte] = @UpdateDte OR @UpdateDte IS NULL)
	AND ([SupplierID] = @SupplierID OR @SupplierID IS NULL)
	AND ([TaxClassID] = @TaxClassID OR @TaxClassID IS NULL)
	AND ([ExternalProductID] = @ExternalProductID OR @ExternalProductID IS NULL)
	AND ([ExternalProductAPIID] = @ExternalProductAPIID OR @ExternalProductAPIID IS NULL)
	AND ([ExternalID] = @ExternalID OR @ExternalID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AddOnValueID]
	, [AddOnID]
	, [Name]
	, [Description]
	, [SKU]
	, [DefaultInd]
	, [DisplayOrder]
	, [ImageFile]
	, [ImageAltTag]
	, [RetailPrice]
	, [SalePrice]
	, [WholesalePrice]
	, [RecurringBillingInd]
	, [RecurringBillingInstallmentInd]
	, [RecurringBillingPeriod]
	, [RecurringBillingFrequency]
	, [RecurringBillingTotalCycles]
	, [RecurringBillingInitialAmount]
	, [Weight]
	, [Length]
	, [Height]
	, [Width]
	, [ShippingRuleTypeID]
	, [FreeShippingInd]
	, [WebServiceDownloadDte]
	, [UpdateDte]
	, [SupplierID]
	, [TaxClassID]
	, [ExternalProductID]
	, [ExternalProductAPIID]
	, [ExternalID]
    FROM
	[dbo].[ZNodeAddOnValue]
    WHERE 
	 ([AddOnValueID] = @AddOnValueID AND @AddOnValueID is not null)
	OR ([AddOnID] = @AddOnID AND @AddOnID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([SKU] = @SKU AND @SKU is not null)
	OR ([DefaultInd] = @DefaultInd AND @DefaultInd is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([ImageFile] = @ImageFile AND @ImageFile is not null)
	OR ([ImageAltTag] = @ImageAltTag AND @ImageAltTag is not null)
	OR ([RetailPrice] = @RetailPrice AND @RetailPrice is not null)
	OR ([SalePrice] = @SalePrice AND @SalePrice is not null)
	OR ([WholesalePrice] = @WholesalePrice AND @WholesalePrice is not null)
	OR ([RecurringBillingInd] = @RecurringBillingInd AND @RecurringBillingInd is not null)
	OR ([RecurringBillingInstallmentInd] = @RecurringBillingInstallmentInd AND @RecurringBillingInstallmentInd is not null)
	OR ([RecurringBillingPeriod] = @RecurringBillingPeriod AND @RecurringBillingPeriod is not null)
	OR ([RecurringBillingFrequency] = @RecurringBillingFrequency AND @RecurringBillingFrequency is not null)
	OR ([RecurringBillingTotalCycles] = @RecurringBillingTotalCycles AND @RecurringBillingTotalCycles is not null)
	OR ([RecurringBillingInitialAmount] = @RecurringBillingInitialAmount AND @RecurringBillingInitialAmount is not null)
	OR ([Weight] = @Weight AND @Weight is not null)
	OR ([Length] = @Length AND @Length is not null)
	OR ([Height] = @Height AND @Height is not null)
	OR ([Width] = @Width AND @Width is not null)
	OR ([ShippingRuleTypeID] = @ShippingRuleTypeID AND @ShippingRuleTypeID is not null)
	OR ([FreeShippingInd] = @FreeShippingInd AND @FreeShippingInd is not null)
	OR ([WebServiceDownloadDte] = @WebServiceDownloadDte AND @WebServiceDownloadDte is not null)
	OR ([UpdateDte] = @UpdateDte AND @UpdateDte is not null)
	OR ([SupplierID] = @SupplierID AND @SupplierID is not null)
	OR ([TaxClassID] = @TaxClassID AND @TaxClassID is not null)
	OR ([ExternalProductID] = @ExternalProductID AND @ExternalProductID is not null)
	OR ([ExternalProductAPIID] = @ExternalProductAPIID AND @ExternalProductAPIID is not null)
	OR ([ExternalID] = @ExternalID AND @ExternalID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeLuceneDocumentMapping]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodeLuceneDocumentMapping]
(
[LuceneDocumentMappingID] [int] NOT NULL IDENTITY(1, 1),
[PropertyName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DocumentName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsIndexed] [bit] NOT NULL CONSTRAINT [DF_ZNodeLuceneDocumentMapping_IsIndexed] DEFAULT ((1)),
[IsFaceted] [bit] NOT NULL CONSTRAINT [DF_ZNodeLuceneDocumentMapping_IsFaceted] DEFAULT ((0)),
[IsStored] [bit] NOT NULL CONSTRAINT [DF_ZNodeLuceneDocumentMapping_IsStored] DEFAULT ((0)),
[IsBoosted] [bit] NOT NULL CONSTRAINT [DF_ZNodeLuceneDocumentMapping_IsBoosted] DEFAULT ((0)),
[FieldBoostable] [bit] NOT NULL CONSTRAINT [DF_ZNodeLuceneDocumentMapping_FieldBoostable] DEFAULT ((0)),
[Boost] [float] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeDocumentMapping] on [dbo].[ZNodeLuceneDocumentMapping]'
GO
ALTER TABLE [dbo].[ZNodeLuceneDocumentMapping] ADD CONSTRAINT [PK_ZNodeDocumentMapping] PRIMARY KEY CLUSTERED  ([LuceneDocumentMappingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetDashboardItemsByPortal]'
GO

ALTER PROCEDURE [dbo].[ZNode_GetDashboardItemsByPortal]  
(@PortalID INT) 
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @TotalProducts AS INT;
    DECLARE @TotalCategories AS INT;
    DECLARE @TotalInventory AS INT;
    DECLARE @TotalOutOfStock AS INT;
    DECLARE @YTDRevenue AS SMALLMONEY;
    DECLARE @MTDRevenue AS SMALLMONEY;
    DECLARE @TodayRevenue AS SMALLMONEY;
    DECLARE @ShippedToday AS INT;
    DECLARE @ReturnedToday AS INT;
    DECLARE @TotalOrders AS INT;
    DECLARE @TotalOrdersMTD AS INT;
    DECLARE @TotalNewOrders AS INT;
    DECLARE @TotalSubmittedOrders AS INT;
    DECLARE @TotalShippedOrders AS INT;
    DECLARE @TotalPaymentPendingOrders AS INT;
    DECLARE @TotalAccounts AS INT;
    DECLARE @TotalAccountsMTD AS INT;
    DECLARE @TotalPages AS INT;
    DECLARE @TotalShippingOptions AS INT;
    DECLARE @PaymentGateway AS VARCHAR (MAX);
    DECLARE @TotalPendingCases AS INT;
    DECLARE @TotalReviewsToApprove AS INT;
    DECLARE @TotalAffiliatesToApprove AS INT;
    DECLARE @TotalDeclinedTransactions AS INT;
    DECLARE @TotalLoginFailedToday AS INT;
    DECLARE @TotalLowInventoryItems AS INT;    
    DECLARE @MonthFirstDate AS DATETIME;
    SET @PaymentGateway = 'None';
    
    SELECT @TotalProducts = Count(1)
    FROM   ZNodeProduct
    WHERE  ZnodeProduct.ProductId IN (SELECT ProductId
                                      FROM   ZnodeProductCategory
                                      WHERE  CategoryID IN (SELECT CategoryId
                                                            FROM   ZNodeCategoryNode));
    SELECT @TotalCategories = Count(1)
    FROM   ZNodeCategory
    WHERE  CategoryId IN (SELECT CategoryId
                          FROM   ZNodeCategoryNode, ZNodePortalCatalog, ZNodeCatalog
                          WHERE  ZNodeCatalog.CatalogId = ZNodeCategoryNode.CatalogId
                                 AND ZNodePortalCatalog.CatalogId = ZNodeCatalog.CatalogId);
    SELECT @TotalInventory = isnull(sum(ZNodeSKUInventory.QuantityOnHand), 0)
    FROM   ZNodeSKU
           INNER JOIN
           ZNodeSKUInventory
           ON ZNodeSKUInventory.SKU = ZNodeSKU.SKU
           INNER JOIN
           ZNodeProduct
           ON ZNodeSKU.ProductID = ZNodeProduct.ProductID
    WHERE  ZnodeProduct.ProductId IN (SELECT ProductId
                                      FROM   ZnodeProductCategory
                                      WHERE  CategoryID IN (SELECT CategoryId
                                                            FROM   ZNodeCategoryNode));
    SELECT @TotalOutOfStock = COUNT(1) FROM 
	(SELECT ZNodeSKUInventory.SKU
    FROM   ZNodeSKUInventory
           INNER JOIN
           ZNodeSKU
           ON ZNodeSKUInventory.SKU = ZNodeSKU.SKU
           INNER JOIN
           ZNodeProduct
           ON ZNodeSKU.ProductID = ZNodeProduct.ProductID
    WHERE  ZnodeProduct.ProductId IN (SELECT ProductId
                                      FROM   ZnodeProductCategory
                                      WHERE  CategoryID IN (SELECT CategoryId
                                                            FROM   ZNodeCategoryNode))
           AND ZNodeSKUInventory.QuantityOnHand = 0
			GROUP BY ZNodeSKUInventory.SKU) TABLE1
     
   IF @PortalID = 1 
		BEGIN
			 SELECT @YTDRevenue = isnull(sum(total), 0)
			 FROM   ZNodeOrder
			 WHERE  year(OrderDate) = year(GetDate());
		END
    ELSE
    BEGIN
        SELECT @YTDRevenue = isnull(sum(total), 0)
        FROM ZNodeOrder WHERE PortalID = @PortalID 
        AND  year(OrderDate) = year(GetDate());
    END
          
           
   
    
    -- Get current month first date.
    SET @MonthFirstDate=CAST(MONTH(GETDATE()) AS VARCHAR(10)) + '/01/' + CAST(YEAR(GETDATE()) AS VARCHAR(10));
    
    
    SELECT @MTDRevenue = ISNULL(SUM(total), 0)
    FROM   ZNodeOrder
    WHERE  CAST (CONVERT (VARCHAR (10),OrderDate, 101) AS DATETIME) >= CAST (CONVERT (VARCHAR (10),@MonthFirstDate, 101) AS DATETIME)
           AND CAST (CONVERT (VARCHAR (10),OrderDate, 101) AS DATETIME) <= CAST(CONVERT(VARCHAR(10), GETDATE(),101) AS DateTime);
            
           
    SELECT @TodayRevenue = isnull(sum(total), 0)
    FROM   ZNodeOrder
    WHERE  DATEADD(dd, 0, DATEDIFF(dd, 0, orderdate)) = CONVERT (DATETIME, FLOOR(CONVERT (FLOAT (24), GETDATE())));
    SELECT @ShippedToday = COUNT(1)
    FROM   ZNodeOrder
    WHERE  DATEADD(dd, 0, DATEDIFF(dd, 0, orderdate)) = CONVERT (DATETIME, FLOOR(CONVERT (FLOAT (24), GETDATE())))
           AND OrderStateID = 20;
           
           
    SELECT @ReturnedToday = COUNT(1)
    FROM   ZNodeOrder
    WHERE  DATEADD(dd, 0, DATEDIFF(dd, 0, orderdate)) = CONVERT (DATETIME, FLOOR(CONVERT (FLOAT (24), GETDATE())))
           AND OrderStateID = 30;
      
    IF @PortalID = 1 
		BEGIN
			SELECT @TotalOrders = COUNT(1)
			FROM   ZNodeOrder 
		END
    ELSE
    BEGIN
        SELECT @TotalOrders = COUNT(1)
		FROM   ZNodeOrder WHERE PortalID = @PortalID 
    END
    
    SELECT @TotalOrdersMTD = COUNT(1)
    FROM   ZNodeOrder
    WHERE  CAST (CONVERT (VARCHAR (10),OrderDate, 101) AS DATETIME) >= CAST (CONVERT (VARCHAR (10),@MonthFirstDate, 101) AS DATETIME)
           AND CAST (CONVERT (VARCHAR (10),OrderDate, 101) AS DATETIME) <= CAST(CONVERT(VARCHAR(10), GETDATE(),101) AS DateTime);
           
    SELECT @TotalNewOrders = COUNT(1)
    FROM   ZNodeOrder
    WHERE  OrderDate > GetDate() - 10;
    
    SELECT @TotalPaymentPendingOrders = COUNT(1)
    FROM   ZNodeOrder
    WHERE  OrderStateID = 1;
    
    SELECT @TotalSubmittedOrders = COUNT(1)
    FROM   ZNodeOrder
    WHERE  OrderStateID = 2;
    
    SELECT @TotalShippedOrders = COUNT(1)
    FROM   ZNodeOrder
    WHERE  OrderStateID = 3;
    
    
    
     IF @PortalID = 1 
		BEGIN
		   SELECT @TotalAccounts = count(DISTINCT (ZA.AccountId))
           FROM   ZNODEACCOUNT AS ZA, ZNODEACCOUNTPROFILE AS ZAP, ZNODEPROFILE AS ZP
           WHERE  ZA.ACCOUNTID = ZAP.ACCOUNTID
           AND ZAP.PROFILEID = ZP.PROFILEID 
		END
    ELSE
    BEGIN
           SELECT @TotalAccounts = count(DISTINCT (ZA.AccountId))
           FROM   ZNODEACCOUNT AS ZA, ZNODEACCOUNTPROFILE AS ZAP, ZNODEPROFILE AS ZP, ZNODEPORTALPROFILE AS ZPP
           WHERE  ZA.ACCOUNTID = ZAP.ACCOUNTID
           AND ZAP.PROFILEID = ZP.PROFILEID AND ZAP.PROFILEID = ZPP.PROFILEID 
           AND ZPP.PORTALID = @PORTALID;
    END
    
    
           
    SELECT @TotalAccountsMTD = count(DISTINCT (ZA.AccountId))
    FROM   ZNODEACCOUNT AS ZA, ZNODEACCOUNTPROFILE AS ZAP, ZNODEPROFILE AS ZP
    WHERE  ZA.ACCOUNTID = ZAP.ACCOUNTID
           AND ZAP.PROFILEID = ZP.PROFILEID
           AND CAST (CONVERT (VARCHAR (10),CreateDte, 101) AS DATETIME)>= CAST (CONVERT (VARCHAR (10),@MonthFirstDate, 101) AS DATETIME)
           AND CAST (CONVERT (VARCHAR (10),CreateDte, 101) AS DATETIME) <= CAST(CONVERT(VARCHAR(10), GETDATE(),101) AS DateTime);
           
    SELECT @TotalPages = COUNT(1)
    FROM   ZNodeContentPage;
    
    SELECT @TotalShippingOptions = COUNT(1)
    FROM   ZNodeShipping
           INNER JOIN
           ZNodeProfile
           ON ZNodeShipping.ProfileID = ZNodeProfile.ProfileID;
           
    IF EXISTS (SELECT *
               FROM   ZNodePaymentSetting
                      INNER JOIN
                      ZNodeGateway
                      ON ZNodeGateway.GatewayTypeID = ZNodePaymentSetting.GatewayTypeID
                      INNER JOIN
                      ZNodeProfile
                      ON ZNodePaymentSetting.ProfileID = ZNodeProfile.ProfileID
               WHERE  ZNodePaymentSetting.PaymentTypeID = 0)
        BEGIN
            SELECT @PaymentGateway = isnull(ZNodeGateway.GatewayName, 'None')
            FROM   ZNodePaymentSetting
                   INNER JOIN
                   ZNodeGateway
                   ON ZNodeGateway.GatewayTypeID = ZNodePaymentSetting.GatewayTypeID
                   INNER JOIN
                   ZNodeProfile
                   ON ZNodePaymentSetting.ProfileID = ZNodeProfile.ProfileID
            WHERE  ZNodePaymentSetting.PaymentTypeID = 0;
        END
    SET @TotalPendingCases = (SELECT COUNT(1)
                              FROM   ZNodeCaseRequest
                              WHERE  CaseStatusId = 1);
    SET @TotalReviewsToApprove = (SELECT COUNT(1)
                                  FROM   ZNodeReview
                                  WHERE  Status = 'N');
    SET @TotalAffiliatesToApprove = (SELECT COUNT(1)
                                     FROM   ZNodeAccount
                                     WHERE  ReferralStatus = 'N');
    SET @TotalDeclinedTransactions = (SELECT COUNT(1)
                                      FROM   ZNodeActivityLog
                                      WHERE  ActivityLogTypeId = 5001);
    SET @TotalLoginFailedToday = (SELECT COUNT(1)
                                  FROM   ZNodeActivityLog
                                  WHERE  (ActivityLogTypeId = 1001
                                          OR ActivityLogTypeId = 1107
                                          OR ActivityLogTypeId = 1109)
                                         AND (DateDiff(dd, CreateDte, GetDate()) = 0));
                                         
                                         
    IF @PortalID = 1 
		BEGIN
			SELECT @TotalLowInventoryItems = COUNT(1) FROM 
			(SELECT ZNodeSKUInventory.SKU
			FROM   ZNodeSKUInventory
				   INNER JOIN
				   ZNodeSKU
				   ON ZNodeSKUInventory.SKU = ZNodeSKU.SKU
				   INNER JOIN
				   ZNodeProduct
				   ON ZNodeSKU.ProductID = ZNodeProduct.ProductID
			WHERE ZnodeProduct.ProductId IN (SELECT ProductId
											  FROM   ZnodeProductCategory
											  WHERE  CategoryID IN (SELECT CategoryId
																	FROM   ZNodeCategoryNode))
				   AND (ZNodeSKUInventory.QuantityOnHand < ZNodeSKUInventory.ReOrderLevel                                                           
				   OR ZNodeSKUInventory.QuantityOnHand <= 0)
			GROUP BY ZNodeSKUInventory.SKU) TABLE1   
		END
	ELSE
	   BEGIN
			SELECT @TotalLowInventoryItems = COUNT(1) FROM 
			(SELECT ZNodeSKUInventory.SKU
			FROM   ZNodeSKUInventory
				   INNER JOIN
				   ZNodeSKU
				   ON ZNodeSKUInventory.SKU = ZNodeSKU.SKU
				   INNER JOIN
				   ZNodeProduct
				   ON ZNodeSKU.ProductID = ZNodeProduct.ProductID
			WHERE ZnodeProduct.PortalID = @PortalID AND  ZnodeProduct.ProductId IN (SELECT ProductId
											  FROM   ZnodeProductCategory
											  WHERE  CategoryID IN (SELECT CategoryId
																	FROM   ZNodeCategoryNode))
				   AND (ZNodeSKUInventory.QuantityOnHand < ZNodeSKUInventory.ReOrderLevel                                                           
				   OR ZNodeSKUInventory.QuantityOnHand <= 0)
			GROUP BY ZNodeSKUInventory.SKU) TABLE1   
       END
                                    
                                         
                                         
                                         
                                                                                  
    
    SELECT @TotalProducts AS 'TotalProducts',
           @TotalCategories AS 'TotalCategories',
           @TotalInventory AS 'TotalInventory',
           @TotalOutOfStock AS 'TotalOutOfStock',
           @YTDRevenue AS 'YTDRevenue',
           @MTDRevenue AS 'MTDRevenue',
           @TodayRevenue AS 'TodayRevenue',
           @ShippedToday AS 'ShippedToday',
           @ReturnedToday AS 'ReturnedToday',
           @TotalOrders AS 'TotalOrders',
           @TotalOrdersMTD AS 'TotalOrdersMTD',
           @TotalNewOrders AS 'TotalNewOrders',
           @TotalPaymentPendingOrders AS 'TotalPaymentPendingOrders',
           @TotalSubmittedOrders AS 'TotalSubmittedOrders',
           @TotalShippedOrders AS 'TotalShippedOrders',
           @TotalAccounts AS 'TotalAccounts',
           @TotalAccountsMTD AS 'TotalAccountsMTD',
           @TotalPages AS 'TotalPages',
           @TotalShippingOptions AS 'TotalShippingOptions',
           @PaymentGateway AS 'PaymentGateway',
           @TotalPendingCases AS 'TotalPendingServiceRequests',
           @TotalReviewsToApprove AS 'TotalReviewstoApprove',
           @TotalAffiliatesToApprove AS 'TotalAffiliatesToApprove',
           @TotalDeclinedTransactions AS 'TotalDeclinedTransactions',
           @TotalLoginFailedToday AS 'TotalLoginFailedToday',
           @TotalLowInventoryItems AS 'TotalLowInventoryItems';
           
    (SELECT  TOP 7 Data1,
                    COUNT(Data1)
     FROM     ZNodeActivityLog
     WHERE    (ActivityLogTypeId = 9500
               OR ActivityLogTypeId = 9501
               OR ActivityLogTypeId = 9502)
     GROUP BY Data1)
    ORDER BY  COUNT(Data1) DESC;
END  
 


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeLuceneServerConfigurationStatus]'
GO
CREATE TABLE [dbo].[ZNodeLuceneServerConfigurationStatus]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ServerName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IP_Address] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeLuceneServerConfigurationStatus] on [dbo].[ZNodeLuceneServerConfigurationStatus]'
GO
ALTER TABLE [dbo].[ZNodeLuceneServerConfigurationStatus] ADD CONSTRAINT [PK_ZNodeLuceneServerConfigurationStatus] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneServerConfigurationStatus_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeLuceneServerConfigurationStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneServerConfigurationStatus_Get_List

AS


				
				SELECT
					[ID],
					[ServerName],
					[IP_Address],
					[Status]
				FROM
					[dbo].[ZNodeLuceneServerConfigurationStatus]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneServerConfigurationStatus_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeLuceneServerConfigurationStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneServerConfigurationStatus_Insert
(

	@ID int    OUTPUT,

	@ServerName nvarchar (50)  ,

	@IPAddress nvarchar (50)  ,

	@Status bit   
)
AS


				
				INSERT INTO [dbo].[ZNodeLuceneServerConfigurationStatus]
					(
					[ServerName]
					,[IP_Address]
					,[Status]
					)
				VALUES
					(
					@ServerName
					,@IPAddress
					,@Status
					)
				
				-- Get the identity value
				SET @ID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneServerConfigurationStatus_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeLuceneServerConfigurationStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneServerConfigurationStatus_Update
(

	@ID int   ,

	@ServerName nvarchar (50)  ,

	@IPAddress nvarchar (50)  ,

	@Status bit   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeLuceneServerConfigurationStatus]
				SET
					[ServerName] = @ServerName
					,[IP_Address] = @IPAddress
					,[Status] = @Status
				WHERE
[ID] = @ID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneServerConfigurationStatus_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeLuceneServerConfigurationStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneServerConfigurationStatus_Delete
(

	@ID int   
)
AS


				DELETE FROM [dbo].[ZNodeLuceneServerConfigurationStatus] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneServerConfigurationStatus_GetByID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneServerConfigurationStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneServerConfigurationStatus_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[ServerName],
					[IP_Address],
					[Status]
				FROM
					[dbo].[ZNodeLuceneServerConfigurationStatus]
				WHERE
					[ID] = @ID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneServerConfigurationStatus_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeLuceneServerConfigurationStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneServerConfigurationStatus_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@ServerName nvarchar (50)  = null ,

	@IPAddress nvarchar (50)  = null ,

	@Status bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [ServerName]
	, [IP_Address]
	, [Status]
    FROM
	[dbo].[ZNodeLuceneServerConfigurationStatus]
    WHERE 
	 ([ID] = @ID OR @ID IS NULL)
	AND ([ServerName] = @ServerName OR @ServerName IS NULL)
	AND ([IP_Address] = @IPAddress OR @IPAddress IS NULL)
	AND ([Status] = @Status OR @Status IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [ServerName]
	, [IP_Address]
	, [Status]
    FROM
	[dbo].[ZNodeLuceneServerConfigurationStatus]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([ServerName] = @ServerName AND @ServerName is not null)
	OR ([IP_Address] = @IPAddress AND @IPAddress is not null)
	OR ([Status] = @Status AND @Status is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeLuceneIndexStatusNames]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodeLuceneIndexStatusNames]
(
[Status] [int] NOT NULL,
[StatusName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_LuceneIndexStatusNames] on [dbo].[ZNodeLuceneIndexStatusNames]'
GO
ALTER TABLE [dbo].[ZNodeLuceneIndexStatusNames] ADD CONSTRAINT [PK_LuceneIndexStatusNames] PRIMARY KEY CLUSTERED  ([Status])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexStatusNames_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeLuceneIndexStatusNames table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexStatusNames_Get_List

AS


				
				SELECT
					[Status],
					[StatusName]
				FROM
					[dbo].[ZNodeLuceneIndexStatusNames]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexStatusNames_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeLuceneIndexStatusNames table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexStatusNames_Insert
(

	@Status int   ,

	@StatusName nvarchar (50)  
)
AS


				
				INSERT INTO [dbo].[ZNodeLuceneIndexStatusNames]
					(
					[Status]
					,[StatusName]
					)
				VALUES
					(
					@Status
					,@StatusName
					)
				
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexStatusNames_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeLuceneIndexStatusNames table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexStatusNames_Update
(

	@Status int   ,

	@OriginalStatus int   ,

	@StatusName nvarchar (50)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeLuceneIndexStatusNames]
				SET
					[Status] = @Status
					,[StatusName] = @StatusName
				WHERE
[Status] = @OriginalStatus 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexStatusNames_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeLuceneIndexStatusNames table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexStatusNames_Delete
(

	@Status int   
)
AS


				DELETE FROM [dbo].[ZNodeLuceneIndexStatusNames] WITH (ROWLOCK) 
				WHERE
					[Status] = @Status
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexStatusNames_GetByStatus]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneIndexStatusNames table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexStatusNames_GetByStatus
(

	@Status int   
)
AS


				SELECT
					[Status],
					[StatusName]
				FROM
					[dbo].[ZNodeLuceneIndexStatusNames]
				WHERE
					[Status] = @Status
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexStatusNames_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeLuceneIndexStatusNames table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexStatusNames_Find
(

	@SearchUsingOR bit   = null ,

	@Status int   = null ,

	@StatusName nvarchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [Status]
	, [StatusName]
    FROM
	[dbo].[ZNodeLuceneIndexStatusNames]
    WHERE 
	 ([Status] = @Status OR @Status IS NULL)
	AND ([StatusName] = @StatusName OR @StatusName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [Status]
	, [StatusName]
    FROM
	[dbo].[ZNodeLuceneIndexStatusNames]
    WHERE 
	 ([Status] = @Status AND @Status is not null)
	OR ([StatusName] = @StatusName AND @StatusName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeLuceneIndexServerStatus]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodeLuceneIndexServerStatus]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ServerName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LuceneIndexMonitorID] [bigint] NOT NULL,
[Status] [int] NULL,
[StartTime] [datetime] NULL,
[EndTime] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeLuceneIndexServerStatus] on [dbo].[ZNodeLuceneIndexServerStatus]'
GO
ALTER TABLE [dbo].[ZNodeLuceneIndexServerStatus] ADD CONSTRAINT [PK_ZNodeLuceneIndexServerStatus] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexServerStatus_GetByServerNameLuceneIndexMonitorID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneIndexServerStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexServerStatus_GetByServerNameLuceneIndexMonitorID
(

	@ServerName nvarchar (50)  ,

	@LuceneIndexMonitorID bigint   
)
AS


				SELECT
					[ServerName],
					[LuceneIndexMonitorID],
					[Status],
					[StartTime],
					[EndTime]
				FROM
					[dbo].[ZNodeLuceneIndexServerStatus]
				WHERE
					[ServerName] = @ServerName
					AND [LuceneIndexMonitorID] = @LuceneIndexMonitorID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeLuceneGlobalProductBrandBoost]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodeLuceneGlobalProductBrandBoost]
(
[LuceneGlobalProductBrandBoostID] [int] NOT NULL IDENTITY(1, 1),
[ProductID] [int] NOT NULL,
[Boost] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeLuceneGlobalProductBrandBoost] on [dbo].[ZNodeLuceneGlobalProductBrandBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductBrandBoost] ADD CONSTRAINT [PK_ZNodeLuceneGlobalProductBrandBoost] PRIMARY KEY CLUSTERED  ([LuceneGlobalProductBrandBoostID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeLuceneGlobalProductBrandBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_Get_List

AS


				
				SELECT
					[LuceneGlobalProductBrandBoostID],
					[ProductID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductBrandBoost]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[TagProductView]'
GO
EXEC sp_refreshview N'[dbo].[TagProductView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeLuceneGlobalProductBrandBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_Insert
(

	@LuceneGlobalProductBrandBoostID int    OUTPUT,

	@ProductID int   ,

	@Boost float   
)
AS


				
				INSERT INTO [dbo].[ZNodeLuceneGlobalProductBrandBoost]
					(
					[ProductID]
					,[Boost]
					)
				VALUES
					(
					@ProductID
					,@Boost
					)
				
				-- Get the identity value
				SET @LuceneGlobalProductBrandBoostID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeLuceneGlobalProductBrandBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_Update
(

	@LuceneGlobalProductBrandBoostID int   ,

	@ProductID int   ,

	@Boost float   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeLuceneGlobalProductBrandBoost]
				SET
					[ProductID] = @ProductID
					,[Boost] = @Boost
				WHERE
[LuceneGlobalProductBrandBoostID] = @LuceneGlobalProductBrandBoostID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeLuceneGlobalProductBrandBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_Delete
(

	@LuceneGlobalProductBrandBoostID int   
)
AS


				DELETE FROM [dbo].[ZNodeLuceneGlobalProductBrandBoost] WITH (ROWLOCK) 
				WHERE
					[LuceneGlobalProductBrandBoostID] = @LuceneGlobalProductBrandBoostID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_GetByProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneGlobalProductBrandBoost table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_GetByProductID
(

	@ProductID int   
)
AS


				SELECT
					[LuceneGlobalProductBrandBoostID],
					[ProductID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductBrandBoost]
				WHERE
					[ProductID] = @ProductID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_GetByLuceneGlobalProductBrandBoostID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneGlobalProductBrandBoost table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_GetByLuceneGlobalProductBrandBoostID
(

	@LuceneGlobalProductBrandBoostID int   
)
AS


				SELECT
					[LuceneGlobalProductBrandBoostID],
					[ProductID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductBrandBoost]
				WHERE
					[LuceneGlobalProductBrandBoostID] = @LuceneGlobalProductBrandBoostID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeLuceneGlobalProductBrandBoost table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_Find
(

	@SearchUsingOR bit   = null ,

	@LuceneGlobalProductBrandBoostID int   = null ,

	@ProductID int   = null ,

	@Boost float   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LuceneGlobalProductBrandBoostID]
	, [ProductID]
	, [Boost]
    FROM
	[dbo].[ZNodeLuceneGlobalProductBrandBoost]
    WHERE 
	 ([LuceneGlobalProductBrandBoostID] = @LuceneGlobalProductBrandBoostID OR @LuceneGlobalProductBrandBoostID IS NULL)
	AND ([ProductID] = @ProductID OR @ProductID IS NULL)
	AND ([Boost] = @Boost OR @Boost IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LuceneGlobalProductBrandBoostID]
	, [ProductID]
	, [Boost]
    FROM
	[dbo].[ZNodeLuceneGlobalProductBrandBoost]
    WHERE 
	 ([LuceneGlobalProductBrandBoostID] = @LuceneGlobalProductBrandBoostID AND @LuceneGlobalProductBrandBoostID is not null)
	OR ([ProductID] = @ProductID AND @ProductID is not null)
	OR ([Boost] = @Boost AND @Boost is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNodeLuceneGlobalProductCatalogBoost]'
GO
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[ZNodeLuceneGlobalProductCatalogBoost]
(
[LuceneGlobalProductCatalogBoostID] [int] NOT NULL IDENTITY(1, 1),
[ProductID] [int] NOT NULL,
[CatalogID] [int] NOT NULL,
[Boost] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ZNodeLuceneGlobalProductCatalogBoost] on [dbo].[ZNodeLuceneGlobalProductCatalogBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductCatalogBoost] ADD CONSTRAINT [PK_ZNodeLuceneGlobalProductCatalogBoost] PRIMARY KEY CLUSTERED  ([LuceneGlobalProductCatalogBoostID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeLuceneGlobalProductCatalogBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_Get_List

AS


				
				SELECT
					[LuceneGlobalProductCatalogBoostID],
					[ProductID],
					[CatalogID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductCatalogBoost]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeLuceneGlobalProductCatalogBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_Insert
(

	@LuceneGlobalProductCatalogBoostID int    OUTPUT,

	@ProductID int   ,

	@CatalogID int   ,

	@Boost float   
)
AS


				
				INSERT INTO [dbo].[ZNodeLuceneGlobalProductCatalogBoost]
					(
					[ProductID]
					,[CatalogID]
					,[Boost]
					)
				VALUES
					(
					@ProductID
					,@CatalogID
					,@Boost
					)
				
				-- Get the identity value
				SET @LuceneGlobalProductCatalogBoostID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeLuceneGlobalProductCatalogBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_Update
(

	@LuceneGlobalProductCatalogBoostID int   ,

	@ProductID int   ,

	@CatalogID int   ,

	@Boost float   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeLuceneGlobalProductCatalogBoost]
				SET
					[ProductID] = @ProductID
					,[CatalogID] = @CatalogID
					,[Boost] = @Boost
				WHERE
[LuceneGlobalProductCatalogBoostID] = @LuceneGlobalProductCatalogBoostID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Znode_GetDistinctLuceneIndexMonitor]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Znode_GetDistinctLuceneIndexMonitor]
	@ServerName nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	--Drop Temp tables if it still exists.  Clean up.	
	IF OBJECT_ID('tempdb..#tmpMonitor') IS NOT NULL 
		DROP TABLE #tmpMonitor
	IF OBJECT_ID('tempdb..#tmpToProcess') IS NOT NULL 
		DROP TABLE #tmpToProcess
	
	--Retrieve current records for processing.
	SELECT mon.LuceneIndexMonitorID, mon.SourceID, mon.SourceType, UPPER(mon.SourceTransactionType) SourceTransactionType, mon.TransactionDateTime, mon.IsDuplicate, mon.AffectedType
		INTO #tmpMonitor
		FROM ZNodeLuceneIndexMonitor mon 
			LEFT JOIN ZNodeLuceneIndexServerStatus stat ON mon.LuceneIndexMonitorID = stat.LuceneIndexMonitorID
				AND (stat.ServerName = @ServerName OR stat.ServerName IS NULL)
		WHERE (stat.Status NOT IN (2,4) OR stat.Status IS NULL)				 				
	
	--For Lucene document updating.
	UPDATE #tmpMonitor
		SET SourceTransactionType = 'UPDATE'
		WHERE SourceID <> 0 AND AffectedType <> 'Product' 
			
	--Eliminate the duplicates and store into Temp table.  Improves performance.
	SELECT *
		INTO #tmpToProcess
		FROM #tmpMonitor
		WHERE LuceneIndexMonitorID IN 
			(
				SELECT MAX(LuceneIndexMonitorID)
					FROM #tmpMonitor
					GROUP BY SourceID, SourceType, SourceTransactionType
			)
		ORDER BY LuceneIndexMonitorID
	
	--If SourceID = 0 (means 'Create Index'), then remove all records except the 'Create Index' record.
	IF EXISTS(SELECT 1 
				FROM #tmpToProcess
				WHERE SourceID = 0)
	BEGIN	
		DELETE FROM #tmpToProcess
			WHERE SourceID <> 0;
	END		
	
	INSERT INTO ZNodeLuceneIndexServerStatus(ServerName, LuceneIndexMonitorID, Status, StartTime, EndTime)
	SELECT @ServerName, LuceneIndexMonitorID, 4, GETDATE(), GETDATE() --4 = ignored
		FROM #tmpMonitor
		WHERE LuceneIndexMonitorID NOT IN 
			(
				SELECT LuceneIndexMonitorID 
					FROM #tmpToProcess  where LuceneIndexMonitorID IN ( SELECT LuceneIndexMonitorID from ZNodeLuceneIndexServerStatus where ServerName = @ServerName  )
			)
			AND
			LuceneIndexMonitorID Not in
			(
				SELECT LuceneIndexMonitorID 
					FROM #tmpMonitor  where LuceneIndexMonitorID IN ( SELECT LuceneIndexMonitorID from ZNodeLuceneIndexServerStatus where ServerName = @ServerName  )
			);
	
	--Return the final results.
	SELECT *
		FROM #tmpToProcess		
		ORDER BY TransactionDateTime
		
	--Cleanup.	
	IF OBJECT_ID('tempdb..#tmpMonitor') IS NOT NULL 
		DROP TABLE #tmpMonitor
	IF OBJECT_ID('tempdb..#tmpToProcess') IS NOT NULL 
		DROP TABLE #tmpToProcess
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_Delete]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeLuceneGlobalProductCatalogBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_Delete
(

	@LuceneGlobalProductCatalogBoostID int   
)
AS


				DELETE FROM [dbo].[ZNodeLuceneGlobalProductCatalogBoost] WITH (ROWLOCK) 
				WHERE
					[LuceneGlobalProductCatalogBoostID] = @LuceneGlobalProductCatalogBoostID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCatalog_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeCatalog table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCatalog_Get_List

AS


				
				SELECT
					[CatalogID],
					[Name],
					[IsActive],
					[PortalID],
					[ExternalID]
				FROM
					[dbo].[ZNodeCatalog]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetPortalByExternalID]'
GO
SET ANSI_NULLS ON
GO


create PROCEDURE [dbo].[ZNode_GetPortalByExternalID]  
	 @ExternalID Varchar(50)
	 
AS
BEGIN
    SELECT portal.PortalID
	FROM [dbo].[ZNodePortal] portal
	WHERE portal.ExternalID = @ExternalID
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_GetByCatalogID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneGlobalProductCatalogBoost table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_GetByCatalogID
(

	@CatalogID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[LuceneGlobalProductCatalogBoostID],
					[ProductID],
					[CatalogID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductCatalogBoost]
				WHERE
					[CatalogID] = @CatalogID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetProductByProductID_XML]'
GO
SET ANSI_NULLS ON
GO


-- ==========================================================   
-- Create date: July 02, 2006
-- Update date: May 26,2010        
-- Description: Retrieve product and return a serialized xml
-- ==========================================================
ALTER PROCEDURE [dbo].[ZNode_GetProductByProductID_XML]
    @ProductId int = 0,
    @LocaleId int = 0,
    @PortalId int = 0,
    @ProfileId int = 0
AS 
    BEGIN                                
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;
		
		    CREATE TABLE #TmpPortal  
            (  
              PortalID int,  
              PRIMARY KEY CLUSTERED ( [PortalID] ASC )  
                WITH ( IGNORE_DUP_KEY = ON )   
            )  
  		
		-- Get SiteAdmin PortalID, if not franchise product        
        DECLARE @OwnPortalID INT
		INSERT  INTO #TmpPortal ( PortalID )
        SELECT ISNULL(A.PortalID,0) PortalID
			FROM ZNodePortal A
			WHERE A.PortalID NOT IN (SELECT ISNULL(PortalID, 0) FROM ZNodeCatalog)
			AND PortalID IN 
			(SELECT PortalID FROM ZNodePortalCatalog WHERE CatalogID IN
			(SELECT ZC.CatalogID FROM ZNodeCatalog ZC
			INNER JOIN ZNodeCategoryNode ZCN ON ZC.CatalogID = ZCN.CatalogID
			INNER JOIN ZNodeProductCategory ZPC ON ZPC.CategoryID = ZCN.CategoryID
			WHERE ZPC.ProductID = @ProductId))
			
		IF (EXISTS(SELECT PortalID FROM #TmpPortal WHERE PortalID = @PortalID))
		BEGIN
			SET @OwnPortalID = @PortalID
		END
		ELSE
		BEGIN
			SELECT TOP 1 @OwnPortalID = PortalID FROM #TmpPortal
		END

        CREATE TABLE #PRODUCTCATEGORYLIST
            (
              ProductId int,
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )
                WITH ( IGNORE_DUP_KEY = ON ) 
            )
		
		CREATE TABLE #CrossSellItem
            (
              ProductId int,
              PRIMARY KEY CLUSTERED ( [ProductId] ASC )
                WITH ( IGNORE_DUP_KEY = ON ) 
            )
		INSERT  INTO #CrossSellItem ( ProductId )
		SELECT  ZNodeProductCrossSell.RelatedProductID AS ProductID				
		FROM	ZNodeProductCrossSell ZNodeProductCrossSell
		INNER JOIN ZnodeProduct ZP ON ZNodeProductCrossSell.RelatedProductID = ZP.ProductID
		WHERE	ZNodeProductCrossSell.ProductID = @ProductId
				AND ZP.ReviewStateID = 20

		IF ((SELECT COUNT(1) FROM #CrossSellItem) > 0)
		BEGIN
			INSERT  INTO #PRODUCTCATEGORYLIST ( ProductId )
                SELECT  ZPC.ProductID
                FROM 
                        ZNodeProductCategory ZPC
                        INNER JOIN ZNodeCategoryNode ZCN ON ZPC.CategoryID = ZCN.CategoryID
                        INNER JOIN ZNodePortalCatalog PC ON ZCN.CatalogID = PC.CatalogID
                WHERE
						ZPC.ProductID IN  (SELECT ProductID FROM #CrossSellItem)
						AND	ZCN.ActiveInd = 1
                        AND ZPC.ActiveInd = 1
                        AND PC.LocaleID = @LocaleId
                        AND PC.PortalID = @PortalId
		END;
		
										
        SELECT  [ProductID],
                [Name],
                [ShortDescription],
                [Description],
                [FeaturesDesc],
                [ProductNum],
                [ProductTypeID],
                [RetailPrice],
                [SalePrice],
                [WholesalePrice],
                [ImageFile],
                [ImageAltTag],
                [Weight],
                [Length],
                [Width],
                [Height],
                [BeginActiveDate],
                [EndActiveDate],
                [DisplayOrder],
                [ActiveInd],
                [CallForPricing],
                [HomepageSpecial],
                [CategorySpecial],
                [InventoryDisplay],
                [Keywords],
                [ManufacturerID],
                [AdditionalInfoLink],
                [AdditionalInfoLinkLabel],
                [ShippingRuleTypeID],
                [ShippingRate],
                [SEOTitle],
                [SEOKeywords],
                [SEODescription],
                [Custom1],
                [Custom2],
                [Custom3],
                [ShipEachItemSeparately],
                [AllowBackOrder],
                [BackOrderMsg],
                [DropShipInd],
                [DropShipEmailID],
                [Specifications],
                [AdditionalInformation],
                [InStockMsg],
                [OutOfStockMsg],
                [TrackInventoryInd],
                [DownloadLink],
                [FreeShippingInd],
                [NewProductInd],
                [SEOURL],
                [MinQty],
                [MaxQty],
                [ShipSeparately],
                [FeaturedInd],
                [WebServiceDownloadDte],
                [UpdateDte],
                [SupplierID],
                [RecurringBillingInd],
                [RecurringBillingInstallmentInd],
                [RecurringBillingPeriod],
                [RecurringBillingFrequency],
                [RecurringBillingTotalCycles],
                [RecurringBillingInitialAmount],
                [TaxClassID],
                ISNULL([PortalID], @OwnPortalID) PortalID,
                [AffiliateUrl],
                (SELECT TOP 1 sku.SKU, QuantityOnHand,ExternalProductID
										FROM ZNodeSKU sku
											INNER JOIN ZNodeSKUInventory SkuInventory 
										ON sku.SKU = SkuInventory.SKU
										WHERE 
											EXISTS
											(SELECT 1 FROM ZNodeSKUAttribute
												WHERE SKUID = sku.SKUID having COUNT(1) = 0)
												AND	sku.ProductID = @ProductID
												
										FOR	XML PATH(''),TYPE
								),
                ManufacturerName = ( Select Name
                                     from   ZNodeManufacturer
                                     where  ManufacturerId = ZNodeProduct.ManufacturerId AND ActiveInd = 1
                                   ),
                ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                            TotalReviews = IsNull(COUNT(ProductId), 0)
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.Status = 'A'
                FOR
                  XML PATH(''),
                      TYPE
                ),
                ( SELECT    [ProductCategoryID],
                            ZNodeProductCategory.[ProductID],
                            [Theme],
                            [MasterPage],
                            [CSS],
                            [CategoryID],
                            [BeginDate],
                            [EndDate],
                            [DisplayOrder],
                            [ActiveInd]
                  FROM      ZNodeProductCategory ZNodeProductCategory
                  WHERE     ZNodeProductCategory.ProductId = ZNodeProduct.productid
                  ORDER BY  ZNodeProductCategory.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    ZNodeHighlight.[HighlightID],
                            [ImageFile],
                            [ImageAltTag],
                            [Name],
                            [Description],
                            [DisplayPopup],
                            [Hyperlink],
                            [HyperlinkNewWinInd],
                            [HighlightTypeID],
                            [ActiveInd],
                            ZNodeHighlight.[DisplayOrder],
                            [ShortDescription],
                            [LocaleId]
                  FROM      ZNodehighlight ZNodeHighlight
                            INNER JOIN ZNodeProductHighlight ZNodeProductHighlight ON ZNodeHighlight.[HighlightID] = ZNodeProductHighlight.[HighlightID]
                  WHERE     ZNodeProductHighlight.productid = ZNodeProduct.productid and [ActiveInd] = 1
                  ORDER BY  ZNodeProductHighlight.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    [ReviewID],
                            ZNodeReview.[ProductID],
                            [AccountID],
                            [Subject],
                            [Pros],
                            [Cons],
                            [Comments],
                            [CreateUser],
                            [UserLocation],
                            [Rating],
                            [Status],
                            [CreateDate],
                            [Custom1],
                            [Custom2],
                            [Custom3]
                  FROM      ZNodeReview ZNodeReview
                  WHERE     ZNodeReview.ProductId = ZNodeProduct.ProductId
                            AND ZNodeReview.[Status] = 'A'
                  ORDER BY  ZNodeReview.ReviewId desc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( select    ZNodeCrossSellItem.ProductId,
                            ZNodeCrossSellItem.Name,
                            ZNodeCrossSellItem.ShortDescription,
                            ZNodeCrossSellItem.ImageFile,
                            ZNodeCrossSellItem.SeoURL,
                            ZNodeCrossSellItem.ImageAltTag,
                            ZNodeCrossSellItem.RetailPrice,
                            ZNodeCrossSellItem.SalePrice,
                            ZNodeCrossSellItem.WholesalePrice,
                            ZNodeCrossSellItem.CallForPricing,
                            ( SELECT    ReviewRating = IsNull(AVG(Rating), 0),
                                        TotalReviews = IsNull(COUNT(ProductId), 0)
                              FROM      ZNodeReview ZNodeReview
                              WHERE     ZNodeReview.ProductId = ZNodeCrossSellItem.ProductId
                                        AND ZNodeReview.[Status] = 'A'
                            FOR
                              XML PATH(''),
                                  TYPE
                            )
                  FROM     ProductsView ZNodeCrossSellItem
                  WHERE 
							ZNodeCrossSellItem.ProductId IN (
                            SELECT  ProductId
                            from    #PRODUCTCATEGORYLIST )
                            AND ZNodeCrossSellItem.ActiveInd = 1
                   
                  ORDER BY  ZNodeCrossSellItem.DisplayOrder asc
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( Select    ZNodeAttributeType.[AttributeTypeId],
                            ZNodeAttributeType.[Name],
                            ZNodeAttributeType.[Description],
                            ZNodeAttributeType.[DisplayOrder],
                            [IsPrivate],
                            [LocaleId],
                            ( SELECT
      Distinct                          ( ZNodeAttribute.AttributeId ),
                                        ZNodeAttribute.Name,
                                        ZNodeAttribute.DisplayOrder
                              FROM      ZNodeskuattribute skua
                                        INNER JOIN ZNodeProductattribute ZNodeAttribute ON skua.attributeid = ZNodeAttribute.attributeid
                              WHERE     ZNodeAttribute.attributetypeid = ZNodeAttributeType.attributetypeid
                                        and ZNodeAttribute.IsActive = 1
                                        and SKUID in (
                                        SELECT  ZNodeSKU.SKUID
                                        FROM    ZNodeSKU
                                        LEFT JOIN ZNodeSKUProfileEffective ON ZNodeSKU.SKUID = ZNodeSKUProfileEffective.SkuId
                                        WHERE   ProductID = @ProductId
                                                AND ActiveInd = 1 
                                                AND ((EffectiveDate <= GETDATE() AND ProfileID = @ProfileID) 
														OR (EffectiveDate IS NULL AND ProfileID IS NULL) 
														OR @ProfileId=0))
                              Group By  ZNodeAttribute.AttributeId,
                                        ZNodeAttribute.Name,
                                        ZNodeAttribute.DisplayOrder
                              Order By  ZNodeAttribute.DisplayOrder
                            FOR
                              XML AUTO,
                                  TYPE,
                                  ELEMENTS
                            )
                  FROM      ZNodeAttributeType ZNodeAttributeType
                            INNER JOIN ZNodeProductTypeAttribute ProductTypeAttribute 
                            ON ZNodeAttributeType.attributetypeid = ProductTypeAttribute.attributetypeid
                            AND ProductTypeAttribute.ProductTypeId = ZNodeProduct.ProductTypeID
                  Order By  ZNodeAttributeType.DisplayOrder
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    ZNodeAddOn.[AddOnID],
                            ZNodeAddOn.[ProductID],
                            [Title],
                            [Name],
                            [Description],
                            [DisplayOrder],
                            [DisplayType],
                            [OptionalInd],
                            [AllowBackOrder],
                            [InStockMsg],
                            [OutOfStockMsg],
                            [BackOrderMsg],
                            [PromptMsg],
                            [TrackInventoryInd],
                            [LocaleId],
                            ( SELECT Distinct
                                        ( ZNodeAddonValue.AddOnValueID ),
                                        ZNodeAddonValue.Name,
                                        ZNodeAddonValue.[Description],
                                        ZNodeAddonValue.DisplayOrder,
                                        ZNodeAddonValue.RetailPrice as RetailPrice,
                                        ZNodeAddonValue.SalePrice as SalePrice,
                                        ZNodeAddonValue.WholeSalePrice as WholesalePrice,
                                        ZNodeAddonValue.DefaultInd as IsDefault,
                                        QuantityOnHand = ( SELECT   QuantityOnHand
                                                           FROM     ZNodeSKUInventory
                                                           WHERE    SKU = ZNodeAddOnValue.SKU
                                                         ),
                                        ZNodeAddonValue.weight as WeightAdditional,
                                        ZNodeAddonValue.ShippingRuleTypeID,
                                        ZNodeAddonValue.FreeShippingInd,
                                        ZNodeAddonValue.SupplierID,
                                        ZNodeAddonValue.TaxClassID,
                                        ZNodeAddonValue.ExternalProductID
                              FROM      ZNodeAddOnValue ZNodeAddOnValue
                              WHERE     ZNodeAddOnValue.AddOnId = ZNodeAddOn.AddOnId
                              Order By  ZNodeAddonValue.DisplayOrder
                            FOR
                              XML AUTO,
                                  TYPE,
                                  ELEMENTS
                            )
                  FROM      ZNodeAddOn ZNodeAddOn
                            INNER JOIN ZNodeProductAddOn ZNodeProductAddOn ON ZNodeAddOn.AddOnId = ZNodeProductAddOn.AddOnId
                  WHERE     ZNodeProductAddOn.ProductId = @ProductId
                  Order By  ZNodeAddon.DisplayOrder
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT    [ProductTierID],
                            [ProductID],
                            [ProfileID],
                            [TierStart],
                            [TierEnd],
                            [Price]
                  FROM      ZNodeProductTier ZNodeProductTier
                  WHERE     ZNodeProductTier.ProductId = ZNodeProduct.ProductId
                FOR
                  XML AUTO,
                      TYPE,
                      ELEMENTS
                ),
                ( SELECT TOP 1 *
					,(SELECT * 
										FROM ZNodeSKUProfile
										WHERE ExpirationDate >= DATEADD(d,-1,GETDATE()) AND
										ZNodeSKUProfile.SKUID = ZNodeSKU.SKUID
									   FOR XML PATH('ZNodeSKUProfile'), TYPE, ELEMENTS)    
                  FROM      ZNodeSKU
                  INNER JOIN ZnodeSKUInventory ON ZNodeSKU.SKU = ZNodeSKUInventory.SKU                     
                  WHERE     ZNodeSKU.ProductId = ZNodeProduct.ProductId
                  AND ProductID IN (SELECT ProductID					
								FROM ZNodeSKU
								LEFT JOIN ZNodeSKUProfileEffective ON ZNodeSKU.SKUID = ZNodeSKUProfileEffective.SkuId
								WHERE ((EffectiveDate <= GETDATE() AND ProfileID = @ProfileID) 
										OR (EffectiveDate IS NULL AND ProfileID IS NULL) 
										OR (ProfileID != @ProfileID) OR @ProfileId=0))  
                FOR  
                  XML PATH('ZNodeSKU'),  
                      TYPE,  
                      ELEMENTS  
                ),
                (SELECT  [ProductID],
                [Name],
                [ShortDescription],
                [Description],
                [FeaturesDesc],
                [ProductNum],
                [ProductTypeID],
                [RetailPrice],
                [SalePrice],
                [WholesalePrice],
                [ImageFile],
                [ImageAltTag],
                [Weight],
                [Length],
                [Width],
                [Height],
                [BeginActiveDate],
                [EndActiveDate],
                [DisplayOrder],
                [ActiveInd],
                [CallForPricing],
                [HomepageSpecial],
                [CategorySpecial],
                [InventoryDisplay],
                [Keywords],
                [ManufacturerID],
                [AdditionalInfoLink],
                [AdditionalInfoLinkLabel],
                [ShippingRuleTypeID],
                [ShippingRate],
                [SEOTitle],
                [SEOKeywords],
                [SEODescription],
                [Custom1],
                [Custom2],
                [Custom3],
                [ShipEachItemSeparately],
                [AllowBackOrder],
                [BackOrderMsg],
                [DropShipInd],
                [DropShipEmailID],
                [Specifications],
                [AdditionalInformation],
                [InStockMsg],
                [OutOfStockMsg],
                [TrackInventoryInd],
                [DownloadLink],
                [FreeShippingInd],
                [NewProductInd],
                [SEOURL],
                [MinQty],
                [MaxQty],
                [ShipSeparately],
                [FeaturedInd],
                [WebServiceDownloadDte],
                [UpdateDte],
                [SupplierID],
                [RecurringBillingInd],
                [RecurringBillingInstallmentInd],
                [RecurringBillingPeriod],
                [RecurringBillingFrequency],
                [RecurringBillingTotalCycles],
                [RecurringBillingInitialAmount],
                [TaxClassID],
                [PortalID],
                [AffiliateUrl],
                (SELECT TOP 1 sku.SKU, QuantityOnHand,ExternalProductID
										FROM ZNodeSKU sku
											INNER JOIN ZNodeSKUInventory SkuInventory 
										ON sku.SKU = SkuInventory.SKU
										WHERE 
											EXISTS
											(SELECT 1 FROM ZNodeSKUAttribute
												WHERE SKUID = sku.SKUID having COUNT(1) = 0)
												AND	sku.ProductID = ZNodeBundleProduct.ProductID
												
										FOR	XML PATH(''),TYPE
								),
                ManufacturerName = ( Select Name
                                     from   ZNodeManufacturer
                                     where  ManufacturerId = ZNodeBundleProduct.ManufacturerId AND ActiveInd = 1
                                   ),
                                   ( SELECT TOP 1 *
                                   ,(SELECT * 
										FROM ZNodeSKUProfile
										WHERE ExpirationDate >= DATEADD(d,-1,GETDATE()) AND
										ZNodeSKUProfile.SKUID = ZNodeSKU.SKUID
									   FOR XML PATH('ZNodeSKUProfile'), TYPE, ELEMENTS)  
								  FROM      ZNodeSKU
								  INNER JOIN ZnodeSKUInventory ON ZNodeSKU.SKU = ZNodeSKUInventory.SKU                     
								  WHERE     ZNodeSKU.ProductId = ZNodeBundleProduct.ProductID
								  AND ProductID IN (SELECT ProductID					
												FROM ZNodeSKU
												LEFT JOIN ZNodeSKUProfileEffective ON ZNodeSKU.SKUID = ZNodeSKUProfileEffective.SkuId
												WHERE ((EffectiveDate <= GETDATE() AND ProfileID = @ProfileID) 
														OR (EffectiveDate IS NULL AND ProfileID IS NULL) 
														OR @ProfileId=0)) 
                FOR  
                  XML PATH('ZNodeSKU'),  
                      TYPE,  
                      ELEMENTS  
                )       
                FROM ZNodeProduct ZNodeBundleProduct INNER JOIN ZNodeParentChildProduct B
                ON ZNodeBundleProduct.ProductID = B.ChildProductID WHERE   B.ParentProductID = @ProductId AND ZNodeBundleProduct.ActiveInd = 1 FOR XML AUTO,
                    TYPE,
                    ELEMENTS )
                  
        FROM    ZNodeProduct ZNodeProduct			    
        WHERE   ZNodeProduct.ProductId = @ProductId
				AND ProductID IN (SELECT ProductID					
								FROM ZNodeSKU
								LEFT JOIN ZNodeSKUProfileEffective ON ZNodeSKU.SKUID = ZNodeSKUProfileEffective.SkuId
								WHERE ((EffectiveDate <= GETDATE() AND ProfileID = @ProfileID) 
										OR (EffectiveDate IS NULL AND ProfileID IS NULL) 
										OR (ProfileID != @ProfileID) OR @ProfileId=0))
        FOR     XML AUTO,
                    TYPE,
                    ELEMENTS                  
    END  






GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetCatalogByExternalID]'
GO


create PROCEDURE [dbo].[ZNode_GetCatalogByExternalID]  
	 @ExternalID Varchar(50)
	 
AS
BEGIN
    SELECT [catalog].CatalogID
	FROM [dbo].[ZNodeCatalog] [catalog]
	WHERE [catalog].ExternalID = @ExternalID
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_GetByProductID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneGlobalProductCatalogBoost table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_GetByProductID
(

	@ProductID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[LuceneGlobalProductCatalogBoostID],
					[ProductID],
					[CatalogID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductCatalogBoost]
				WHERE
					[ProductID] = @ProductID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCatalog_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeCatalog table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCatalog_Insert
(

	@CatalogID int    OUTPUT,

	@Name nvarchar (MAX)  ,

	@IsActive bit   ,

	@PortalID int   ,

	@ExternalID varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ZNodeCatalog]
					(
					[Name]
					,[IsActive]
					,[PortalID]
					,[ExternalID]
					)
				VALUES
					(
					@Name
					,@IsActive
					,@PortalID
					,@ExternalID
					)
				
				-- Get the identity value
				SET @CatalogID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetPromotionByExternalID]'
GO
SET ANSI_NULLS ON
GO


create PROCEDURE [dbo].[ZNode_GetPromotionByExternalID]  
	 @ExternalID Varchar(50)
	 
AS
BEGIN
    SELECT promotion.PromotionID
	FROM [dbo].[ZNodePromotion] promotion
	WHERE promotion.ExternalID = @ExternalID
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_GetByCatalogIDProductID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneGlobalProductCatalogBoost table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_GetByCatalogIDProductID
(

	@CatalogID int   ,

	@ProductID int   
)
AS


				SELECT
					[LuceneGlobalProductCatalogBoostID],
					[ProductID],
					[CatalogID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductCatalogBoost]
				WHERE
					[CatalogID] = @CatalogID
					AND [ProductID] = @ProductID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCatalog_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeCatalog table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCatalog_Update
(

	@CatalogID int   ,

	@Name nvarchar (MAX)  ,

	@IsActive bit   ,

	@PortalID int   ,

	@ExternalID varchar (50)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeCatalog]
				SET
					[Name] = @Name
					,[IsActive] = @IsActive
					,[PortalID] = @PortalID
					,[ExternalID] = @ExternalID
				WHERE
[CatalogID] = @CatalogID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetAddOnByExternalID]'
GO
SET ANSI_NULLS ON
GO


create PROCEDURE [dbo].[ZNode_GetAddOnByExternalID]  
	 @ExternalID Varchar(50)
	 
AS
BEGIN
    SELECT addOn.AddOnID
	FROM [dbo].[ZNodeAddOn] addOn
	WHERE addOn.ExternalAPIID = @ExternalID
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_GetByLuceneGlobalProductCatalogBoostID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneGlobalProductCatalogBoost table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_GetByLuceneGlobalProductCatalogBoostID
(

	@LuceneGlobalProductCatalogBoostID int   
)
AS


				SELECT
					[LuceneGlobalProductCatalogBoostID],
					[ProductID],
					[CatalogID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductCatalogBoost]
				WHERE
					[LuceneGlobalProductCatalogBoostID] = @LuceneGlobalProductCatalogBoostID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_GetAddOnValueByExternalID]'
GO
SET ANSI_NULLS ON
GO


create PROCEDURE [dbo].[ZNode_GetAddOnValueByExternalID]  
	 @ExternalID Varchar(50)
	 
AS
BEGIN
    SELECT addOnValue.AddOnValueID
	FROM [dbo].[ZNodeAddOnValue] addOnValue
	WHERE addOnValue.ExternalID = @ExternalID
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_Find]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeLuceneGlobalProductCatalogBoost table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_Find
(

	@SearchUsingOR bit   = null ,

	@LuceneGlobalProductCatalogBoostID int   = null ,

	@ProductID int   = null ,

	@CatalogID int   = null ,

	@Boost float   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LuceneGlobalProductCatalogBoostID]
	, [ProductID]
	, [CatalogID]
	, [Boost]
    FROM
	[dbo].[ZNodeLuceneGlobalProductCatalogBoost]
    WHERE 
	 ([LuceneGlobalProductCatalogBoostID] = @LuceneGlobalProductCatalogBoostID OR @LuceneGlobalProductCatalogBoostID IS NULL)
	AND ([ProductID] = @ProductID OR @ProductID IS NULL)
	AND ([CatalogID] = @CatalogID OR @CatalogID IS NULL)
	AND ([Boost] = @Boost OR @Boost IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LuceneGlobalProductCatalogBoostID]
	, [ProductID]
	, [CatalogID]
	, [Boost]
    FROM
	[dbo].[ZNodeLuceneGlobalProductCatalogBoost]
    WHERE 
	 ([LuceneGlobalProductCatalogBoostID] = @LuceneGlobalProductCatalogBoostID AND @LuceneGlobalProductCatalogBoostID is not null)
	OR ([ProductID] = @ProductID AND @ProductID is not null)
	OR ([CatalogID] = @CatalogID AND @CatalogID is not null)
	OR ([Boost] = @Boost AND @Boost is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCatalog_GetByPortalID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeCatalog table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCatalog_GetByPortalID
(

	@PortalID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[CatalogID],
					[Name],
					[IsActive],
					[PortalID],
					[ExternalID]
				FROM
					[dbo].[ZNodeCatalog]
				WHERE
					[PortalID] = @PortalID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeLuceneGlobalProductCategoryBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_Get_List

AS


				
				SELECT
					[LuceneGlobalProductCategoryBoostID],
					[ProductCategoryID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductCategoryBoost]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCatalog_GetByCatalogID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeCatalog table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCatalog_GetByCatalogID
(

	@CatalogID int   
)
AS


				SELECT
					[CatalogID],
					[Name],
					[IsActive],
					[PortalID],
					[ExternalID]
				FROM
					[dbo].[ZNodeCatalog]
				WHERE
					[CatalogID] = @CatalogID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCatalog_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeCatalog table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCatalog_Find
(

	@SearchUsingOR bit   = null ,

	@CatalogID int   = null ,

	@Name nvarchar (MAX)  = null ,

	@IsActive bit   = null ,

	@PortalID int   = null ,

	@ExternalID varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CatalogID]
	, [Name]
	, [IsActive]
	, [PortalID]
	, [ExternalID]
    FROM
	[dbo].[ZNodeCatalog]
    WHERE 
	 ([CatalogID] = @CatalogID OR @CatalogID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([IsActive] = @IsActive OR @IsActive IS NULL)
	AND ([PortalID] = @PortalID OR @PortalID IS NULL)
	AND ([ExternalID] = @ExternalID OR @ExternalID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CatalogID]
	, [Name]
	, [IsActive]
	, [PortalID]
	, [ExternalID]
    FROM
	[dbo].[ZNodeCatalog]
    WHERE 
	 ([CatalogID] = @CatalogID AND @CatalogID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([IsActive] = @IsActive AND @IsActive is not null)
	OR ([PortalID] = @PortalID AND @PortalID is not null)
	OR ([ExternalID] = @ExternalID AND @ExternalID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Znode_UpsertZnodeLuceneIndexServerStatus]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Znode_UpsertZnodeLuceneIndexServerStatus]
	@ServerName nvarchar(50),
	@LuceneIndexMonitorID bigint,
	@Status int
AS
BEGIN

	IF EXISTS(SELECT 1 FROM ZNodeLuceneIndexServerStatus 
		WHERE ServerName = @ServerName AND LuceneIndexMonitorID = @LuceneIndexMonitorID)
	BEGIN
		UPDATE ZNodeLuceneIndexServerStatus 
            SET Status = @Status, 
				EndTime = CASE WHEN @Status = 2 THEN GETDATE() ELSE NULL END
            WHERE ServerName = @ServerName AND LuceneIndexMonitorID = @LuceneIndexMonitorId;	
	END
	ELSE
	BEGIN
		INSERT INTO ZNodeLuceneIndexServerStatus (ServerName, LuceneIndexMonitorID, Status, StartTime)
            VALUES (@ServerName, @LuceneIndexMonitorId, @Status, GETDATE())	
	END	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_Insert]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeLuceneGlobalProductCategoryBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_Insert
(

	@LuceneGlobalProductCategoryBoostID int    OUTPUT,

	@ProductCategoryID int   ,

	@Boost float   
)
AS


				
				INSERT INTO [dbo].[ZNodeLuceneGlobalProductCategoryBoost]
					(
					[ProductCategoryID]
					,[Boost]
					)
				VALUES
					(
					@ProductCategoryID
					,@Boost
					)
				
				-- Get the identity value
				SET @LuceneGlobalProductCategoryBoostID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodePromotion table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_Get_List

AS


				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl],
					[PromoCode],
					[ExternalID]
				FROM
					[dbo].[ZNodePromotion]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeLuceneGlobalProductCategoryBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_Update
(

	@LuceneGlobalProductCategoryBoostID int   ,

	@ProductCategoryID int   ,

	@Boost float   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeLuceneGlobalProductCategoryBoost]
				SET
					[ProductCategoryID] = @ProductCategoryID
					,[Boost] = @Boost
				WHERE
[LuceneGlobalProductCategoryBoostID] = @LuceneGlobalProductCategoryBoostID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeLuceneGlobalProductCategoryBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_Delete
(

	@LuceneGlobalProductCategoryBoostID int   
)
AS


				DELETE FROM [dbo].[ZNodeLuceneGlobalProductCategoryBoost] WITH (ROWLOCK) 
				WHERE
					[LuceneGlobalProductCategoryBoostID] = @LuceneGlobalProductCategoryBoostID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodePromotion table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_Insert
(

	@PromotionID int    OUTPUT,

	@Name nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@ProfileID int   ,

	@AccountID int   ,

	@ProductID int   ,

	@AddOnValueID int   ,

	@SKUID int   ,

	@DiscountTypeID int   ,

	@Discount decimal (18, 2)  ,

	@StartDate datetime   ,

	@EndDate datetime   ,

	@CouponInd bit   ,

	@CouponCode nvarchar (MAX)  ,

	@CouponQuantityAvailable int   ,

	@PromotionMessage nvarchar (MAX)  ,

	@OrderMinimum money   ,

	@QuantityMinimum int   ,

	@PromotionProductQty int   ,

	@PromotionProductID int   ,

	@DisplayOrder int   ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@EnableCouponUrl bit   ,

	@PromoCode varchar (50)  ,

	@ExternalID varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ZNodePromotion]
					(
					[Name]
					,[Description]
					,[ProfileID]
					,[AccountID]
					,[ProductID]
					,[AddOnValueID]
					,[SKUID]
					,[DiscountTypeID]
					,[Discount]
					,[StartDate]
					,[EndDate]
					,[CouponInd]
					,[CouponCode]
					,[CouponQuantityAvailable]
					,[PromotionMessage]
					,[OrderMinimum]
					,[QuantityMinimum]
					,[PromotionProductQty]
					,[PromotionProductID]
					,[DisplayOrder]
					,[Custom1]
					,[Custom2]
					,[Custom3]
					,[EnableCouponUrl]
					,[PromoCode]
					,[ExternalID]
					)
				VALUES
					(
					@Name
					,@Description
					,@ProfileID
					,@AccountID
					,@ProductID
					,@AddOnValueID
					,@SKUID
					,@DiscountTypeID
					,@Discount
					,@StartDate
					,@EndDate
					,@CouponInd
					,@CouponCode
					,@CouponQuantityAvailable
					,@PromotionMessage
					,@OrderMinimum
					,@QuantityMinimum
					,@PromotionProductQty
					,@PromotionProductID
					,@DisplayOrder
					,@Custom1
					,@Custom2
					,@Custom3
					,@EnableCouponUrl
					,@PromoCode
					,@ExternalID
					)
				
				-- Get the identity value
				SET @PromotionID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_GetByProductCategoryID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneGlobalProductCategoryBoost table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_GetByProductCategoryID
(

	@ProductCategoryID int   
)
AS


				SELECT
					[LuceneGlobalProductCategoryBoostID],
					[ProductCategoryID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductCategoryBoost]
				WHERE
					[ProductCategoryID] = @ProductCategoryID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodePromotion table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_Update
(

	@PromotionID int   ,

	@Name nvarchar (MAX)  ,

	@Description nvarchar (MAX)  ,

	@ProfileID int   ,

	@AccountID int   ,

	@ProductID int   ,

	@AddOnValueID int   ,

	@SKUID int   ,

	@DiscountTypeID int   ,

	@Discount decimal (18, 2)  ,

	@StartDate datetime   ,

	@EndDate datetime   ,

	@CouponInd bit   ,

	@CouponCode nvarchar (MAX)  ,

	@CouponQuantityAvailable int   ,

	@PromotionMessage nvarchar (MAX)  ,

	@OrderMinimum money   ,

	@QuantityMinimum int   ,

	@PromotionProductQty int   ,

	@PromotionProductID int   ,

	@DisplayOrder int   ,

	@Custom1 nvarchar (MAX)  ,

	@Custom2 nvarchar (MAX)  ,

	@Custom3 nvarchar (MAX)  ,

	@EnableCouponUrl bit   ,

	@PromoCode varchar (50)  ,

	@ExternalID varchar (50)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodePromotion]
				SET
					[Name] = @Name
					,[Description] = @Description
					,[ProfileID] = @ProfileID
					,[AccountID] = @AccountID
					,[ProductID] = @ProductID
					,[AddOnValueID] = @AddOnValueID
					,[SKUID] = @SKUID
					,[DiscountTypeID] = @DiscountTypeID
					,[Discount] = @Discount
					,[StartDate] = @StartDate
					,[EndDate] = @EndDate
					,[CouponInd] = @CouponInd
					,[CouponCode] = @CouponCode
					,[CouponQuantityAvailable] = @CouponQuantityAvailable
					,[PromotionMessage] = @PromotionMessage
					,[OrderMinimum] = @OrderMinimum
					,[QuantityMinimum] = @QuantityMinimum
					,[PromotionProductQty] = @PromotionProductQty
					,[PromotionProductID] = @PromotionProductID
					,[DisplayOrder] = @DisplayOrder
					,[Custom1] = @Custom1
					,[Custom2] = @Custom2
					,[Custom3] = @Custom3
					,[EnableCouponUrl] = @EnableCouponUrl
					,[PromoCode] = @PromoCode
					,[ExternalID] = @ExternalID
				WHERE
[PromotionID] = @PromotionID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Znode_CreateIndexMonitor]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Znode_CreateIndexMonitor]   
	
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	insert into dbo.ZNodeLuceneIndexMonitor(SourceID, SourceType, SourceTransactionType, TransactionDateTime, AffectedType,IndexerStatusChangedBy)
				values(0, 'CreateIndex','INSERT', GETDATE(), 'CreateIndex','SiteAdmin')
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_GetByLuceneGlobalProductCategoryBoostID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneGlobalProductCategoryBoost table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_GetByLuceneGlobalProductCategoryBoostID
(

	@LuceneGlobalProductCategoryBoostID int   
)
AS


				SELECT
					[LuceneGlobalProductCategoryBoostID],
					[ProductCategoryID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductCategoryBoost]
				WHERE
					[LuceneGlobalProductCategoryBoostID] = @LuceneGlobalProductCategoryBoostID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeLuceneGlobalProductCategoryBoost table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_Find
(

	@SearchUsingOR bit   = null ,

	@LuceneGlobalProductCategoryBoostID int   = null ,

	@ProductCategoryID int   = null ,

	@Boost float   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LuceneGlobalProductCategoryBoostID]
	, [ProductCategoryID]
	, [Boost]
    FROM
	[dbo].[ZNodeLuceneGlobalProductCategoryBoost]
    WHERE 
	 ([LuceneGlobalProductCategoryBoostID] = @LuceneGlobalProductCategoryBoostID OR @LuceneGlobalProductCategoryBoostID IS NULL)
	AND ([ProductCategoryID] = @ProductCategoryID OR @ProductCategoryID IS NULL)
	AND ([Boost] = @Boost OR @Boost IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LuceneGlobalProductCategoryBoostID]
	, [ProductCategoryID]
	, [Boost]
    FROM
	[dbo].[ZNodeLuceneGlobalProductCategoryBoost]
    WHERE 
	 ([LuceneGlobalProductCategoryBoostID] = @LuceneGlobalProductCategoryBoostID AND @LuceneGlobalProductCategoryBoostID is not null)
	OR ([ProductCategoryID] = @ProductCategoryID AND @ProductCategoryID is not null)
	OR ([Boost] = @Boost AND @Boost is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByAccountID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByAccountID
(

	@AccountID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl],
					[PromoCode],
					[ExternalID]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[AccountID] = @AccountID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByAddOnValueID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByAddOnValueID
(

	@AddOnValueID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl],
					[PromoCode],
					[ExternalID]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[AddOnValueID] = @AddOnValueID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByDiscountTypeID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByDiscountTypeID
(

	@DiscountTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl],
					[PromoCode],
					[ExternalID]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[DiscountTypeID] = @DiscountTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByProductID
(

	@ProductID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl],
					[PromoCode],
					[ExternalID]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[ProductID] = @ProductID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByProfileID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByProfileID
(

	@ProfileID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl],
					[PromoCode],
					[ExternalID]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[ProfileID] = @ProfileID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetBySKUID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetBySKUID
(

	@SKUID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl],
					[PromoCode],
					[ExternalID]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[SKUID] = @SKUID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetByPromotionID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePromotion table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetByPromotionID
(

	@PromotionID int   
)
AS


				SELECT
					[PromotionID],
					[Name],
					[Description],
					[ProfileID],
					[AccountID],
					[ProductID],
					[AddOnValueID],
					[SKUID],
					[DiscountTypeID],
					[Discount],
					[StartDate],
					[EndDate],
					[CouponInd],
					[CouponCode],
					[CouponQuantityAvailable],
					[PromotionMessage],
					[OrderMinimum],
					[QuantityMinimum],
					[PromotionProductQty],
					[PromotionProductID],
					[DisplayOrder],
					[Custom1],
					[Custom2],
					[Custom3],
					[EnableCouponUrl],
					[PromoCode],
					[ExternalID]
				FROM
					[dbo].[ZNodePromotion]
				WHERE
					[PromotionID] = @PromotionID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodePromotion table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_Find
(

	@SearchUsingOR bit   = null ,

	@PromotionID int   = null ,

	@Name nvarchar (MAX)  = null ,

	@Description nvarchar (MAX)  = null ,

	@ProfileID int   = null ,

	@AccountID int   = null ,

	@ProductID int   = null ,

	@AddOnValueID int   = null ,

	@SKUID int   = null ,

	@DiscountTypeID int   = null ,

	@Discount decimal (18, 2)  = null ,

	@StartDate datetime   = null ,

	@EndDate datetime   = null ,

	@CouponInd bit   = null ,

	@CouponCode nvarchar (MAX)  = null ,

	@CouponQuantityAvailable int   = null ,

	@PromotionMessage nvarchar (MAX)  = null ,

	@OrderMinimum money   = null ,

	@QuantityMinimum int   = null ,

	@PromotionProductQty int   = null ,

	@PromotionProductID int   = null ,

	@DisplayOrder int   = null ,

	@Custom1 nvarchar (MAX)  = null ,

	@Custom2 nvarchar (MAX)  = null ,

	@Custom3 nvarchar (MAX)  = null ,

	@EnableCouponUrl bit   = null ,

	@PromoCode varchar (50)  = null ,

	@ExternalID varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PromotionID]
	, [Name]
	, [Description]
	, [ProfileID]
	, [AccountID]
	, [ProductID]
	, [AddOnValueID]
	, [SKUID]
	, [DiscountTypeID]
	, [Discount]
	, [StartDate]
	, [EndDate]
	, [CouponInd]
	, [CouponCode]
	, [CouponQuantityAvailable]
	, [PromotionMessage]
	, [OrderMinimum]
	, [QuantityMinimum]
	, [PromotionProductQty]
	, [PromotionProductID]
	, [DisplayOrder]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [EnableCouponUrl]
	, [PromoCode]
	, [ExternalID]
    FROM
	[dbo].[ZNodePromotion]
    WHERE 
	 ([PromotionID] = @PromotionID OR @PromotionID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([ProfileID] = @ProfileID OR @ProfileID IS NULL)
	AND ([AccountID] = @AccountID OR @AccountID IS NULL)
	AND ([ProductID] = @ProductID OR @ProductID IS NULL)
	AND ([AddOnValueID] = @AddOnValueID OR @AddOnValueID IS NULL)
	AND ([SKUID] = @SKUID OR @SKUID IS NULL)
	AND ([DiscountTypeID] = @DiscountTypeID OR @DiscountTypeID IS NULL)
	AND ([Discount] = @Discount OR @Discount IS NULL)
	AND ([StartDate] = @StartDate OR @StartDate IS NULL)
	AND ([EndDate] = @EndDate OR @EndDate IS NULL)
	AND ([CouponInd] = @CouponInd OR @CouponInd IS NULL)
	AND ([CouponCode] = @CouponCode OR @CouponCode IS NULL)
	AND ([CouponQuantityAvailable] = @CouponQuantityAvailable OR @CouponQuantityAvailable IS NULL)
	AND ([PromotionMessage] = @PromotionMessage OR @PromotionMessage IS NULL)
	AND ([OrderMinimum] = @OrderMinimum OR @OrderMinimum IS NULL)
	AND ([QuantityMinimum] = @QuantityMinimum OR @QuantityMinimum IS NULL)
	AND ([PromotionProductQty] = @PromotionProductQty OR @PromotionProductQty IS NULL)
	AND ([PromotionProductID] = @PromotionProductID OR @PromotionProductID IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([Custom1] = @Custom1 OR @Custom1 IS NULL)
	AND ([Custom2] = @Custom2 OR @Custom2 IS NULL)
	AND ([Custom3] = @Custom3 OR @Custom3 IS NULL)
	AND ([EnableCouponUrl] = @EnableCouponUrl OR @EnableCouponUrl IS NULL)
	AND ([PromoCode] = @PromoCode OR @PromoCode IS NULL)
	AND ([ExternalID] = @ExternalID OR @ExternalID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PromotionID]
	, [Name]
	, [Description]
	, [ProfileID]
	, [AccountID]
	, [ProductID]
	, [AddOnValueID]
	, [SKUID]
	, [DiscountTypeID]
	, [Discount]
	, [StartDate]
	, [EndDate]
	, [CouponInd]
	, [CouponCode]
	, [CouponQuantityAvailable]
	, [PromotionMessage]
	, [OrderMinimum]
	, [QuantityMinimum]
	, [PromotionProductQty]
	, [PromotionProductID]
	, [DisplayOrder]
	, [Custom1]
	, [Custom2]
	, [Custom3]
	, [EnableCouponUrl]
	, [PromoCode]
	, [ExternalID]
    FROM
	[dbo].[ZNodePromotion]
    WHERE 
	 ([PromotionID] = @PromotionID AND @PromotionID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([ProfileID] = @ProfileID AND @ProfileID is not null)
	OR ([AccountID] = @AccountID AND @AccountID is not null)
	OR ([ProductID] = @ProductID AND @ProductID is not null)
	OR ([AddOnValueID] = @AddOnValueID AND @AddOnValueID is not null)
	OR ([SKUID] = @SKUID AND @SKUID is not null)
	OR ([DiscountTypeID] = @DiscountTypeID AND @DiscountTypeID is not null)
	OR ([Discount] = @Discount AND @Discount is not null)
	OR ([StartDate] = @StartDate AND @StartDate is not null)
	OR ([EndDate] = @EndDate AND @EndDate is not null)
	OR ([CouponInd] = @CouponInd AND @CouponInd is not null)
	OR ([CouponCode] = @CouponCode AND @CouponCode is not null)
	OR ([CouponQuantityAvailable] = @CouponQuantityAvailable AND @CouponQuantityAvailable is not null)
	OR ([PromotionMessage] = @PromotionMessage AND @PromotionMessage is not null)
	OR ([OrderMinimum] = @OrderMinimum AND @OrderMinimum is not null)
	OR ([QuantityMinimum] = @QuantityMinimum AND @QuantityMinimum is not null)
	OR ([PromotionProductQty] = @PromotionProductQty AND @PromotionProductQty is not null)
	OR ([PromotionProductID] = @PromotionProductID AND @PromotionProductID is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([Custom1] = @Custom1 AND @Custom1 is not null)
	OR ([Custom2] = @Custom2 AND @Custom2 is not null)
	OR ([Custom3] = @Custom3 AND @Custom3 is not null)
	OR ([EnableCouponUrl] = @EnableCouponUrl AND @EnableCouponUrl is not null)
	OR ([PromoCode] = @PromoCode AND @PromoCode is not null)
	OR ([ExternalID] = @ExternalID AND @ExternalID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneDocumentMapping_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeLuceneDocumentMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneDocumentMapping_Get_List

AS


				
				SELECT
					[LuceneDocumentMappingID],
					[PropertyName],
					[DocumentName],
					[IsIndexed],
					[IsFaceted],
					[IsStored],
					[IsBoosted],
					[FieldBoostable],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneDocumentMapping]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexMonitor_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeLuceneIndexMonitor table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexMonitor_Get_List

AS


				
				SELECT
					[LuceneIndexMonitorID],
					[SourceID],
					[SourceType],
					[SourceTransactionType],
					[TransactionDateTime],
					[IsDuplicate],
					[AffectedType],
					[IndexerStatusChangedBy]
				FROM
					[dbo].[ZNodeLuceneIndexMonitor]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneDocumentMapping_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeLuceneDocumentMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneDocumentMapping_Insert
(

	@LuceneDocumentMappingID int    OUTPUT,

	@PropertyName nvarchar (200)  ,

	@DocumentName nvarchar (200)  ,

	@IsIndexed bit   ,

	@IsFaceted bit   ,

	@IsStored bit   ,

	@IsBoosted bit   ,

	@FieldBoostable bit   ,

	@Boost float   
)
AS


				
				INSERT INTO [dbo].[ZNodeLuceneDocumentMapping]
					(
					[PropertyName]
					,[DocumentName]
					,[IsIndexed]
					,[IsFaceted]
					,[IsStored]
					,[IsBoosted]
					,[FieldBoostable]
					,[Boost]
					)
				VALUES
					(
					@PropertyName
					,@DocumentName
					,@IsIndexed
					,@IsFaceted
					,@IsStored
					,@IsBoosted
					,@FieldBoostable
					,@Boost
					)
				
				-- Get the identity value
				SET @LuceneDocumentMappingID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexMonitor_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeLuceneIndexMonitor table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexMonitor_Insert
(

	@LuceneIndexMonitorID bigint    OUTPUT,

	@SourceID int   ,

	@SourceType varchar (50)  ,

	@SourceTransactionType varchar (10)  ,

	@TransactionDateTime datetime   ,

	@IsDuplicate bit   ,

	@AffectedType varchar (50)  ,

	@IndexerStatusChangedBy nvarchar (50)  
)
AS


				
				INSERT INTO [dbo].[ZNodeLuceneIndexMonitor]
					(
					[SourceID]
					,[SourceType]
					,[SourceTransactionType]
					,[TransactionDateTime]
					,[IsDuplicate]
					,[AffectedType]
					,[IndexerStatusChangedBy]
					)
				VALUES
					(
					@SourceID
					,@SourceType
					,@SourceTransactionType
					,@TransactionDateTime
					,@IsDuplicate
					,@AffectedType
					,@IndexerStatusChangedBy
					)
				
				-- Get the identity value
				SET @LuceneIndexMonitorID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneDocumentMapping_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeLuceneDocumentMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneDocumentMapping_Update
(

	@LuceneDocumentMappingID int   ,

	@PropertyName nvarchar (200)  ,

	@DocumentName nvarchar (200)  ,

	@IsIndexed bit   ,

	@IsFaceted bit   ,

	@IsStored bit   ,

	@IsBoosted bit   ,

	@FieldBoostable bit   ,

	@Boost float   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeLuceneDocumentMapping]
				SET
					[PropertyName] = @PropertyName
					,[DocumentName] = @DocumentName
					,[IsIndexed] = @IsIndexed
					,[IsFaceted] = @IsFaceted
					,[IsStored] = @IsStored
					,[IsBoosted] = @IsBoosted
					,[FieldBoostable] = @FieldBoostable
					,[Boost] = @Boost
				WHERE
[LuceneDocumentMappingID] = @LuceneDocumentMappingID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexMonitor_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeLuceneIndexMonitor table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexMonitor_Update
(

	@LuceneIndexMonitorID bigint   ,

	@SourceID int   ,

	@SourceType varchar (50)  ,

	@SourceTransactionType varchar (10)  ,

	@TransactionDateTime datetime   ,

	@IsDuplicate bit   ,

	@AffectedType varchar (50)  ,

	@IndexerStatusChangedBy nvarchar (50)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeLuceneIndexMonitor]
				SET
					[SourceID] = @SourceID
					,[SourceType] = @SourceType
					,[SourceTransactionType] = @SourceTransactionType
					,[TransactionDateTime] = @TransactionDateTime
					,[IsDuplicate] = @IsDuplicate
					,[AffectedType] = @AffectedType
					,[IndexerStatusChangedBy] = @IndexerStatusChangedBy
				WHERE
[LuceneIndexMonitorID] = @LuceneIndexMonitorID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneDocumentMapping_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeLuceneDocumentMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneDocumentMapping_Delete
(

	@LuceneDocumentMappingID int   
)
AS


				DELETE FROM [dbo].[ZNodeLuceneDocumentMapping] WITH (ROWLOCK) 
				WHERE
					[LuceneDocumentMappingID] = @LuceneDocumentMappingID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBoost_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeLuceneGlobalProductBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBoost_Get_List

AS


				
				SELECT
					[LuceneGlobalProductBoostID],
					[ProductID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductBoost]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexMonitor_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeLuceneIndexMonitor table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexMonitor_Delete
(

	@LuceneIndexMonitorID bigint   
)
AS


				DELETE FROM [dbo].[ZNodeLuceneIndexMonitor] WITH (ROWLOCK) 
				WHERE
					[LuceneIndexMonitorID] = @LuceneIndexMonitorID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneDocumentMapping_GetByLuceneDocumentMappingID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneDocumentMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneDocumentMapping_GetByLuceneDocumentMappingID
(

	@LuceneDocumentMappingID int   
)
AS


				SELECT
					[LuceneDocumentMappingID],
					[PropertyName],
					[DocumentName],
					[IsIndexed],
					[IsFaceted],
					[IsStored],
					[IsBoosted],
					[FieldBoostable],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneDocumentMapping]
				WHERE
					[LuceneDocumentMappingID] = @LuceneDocumentMappingID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexMonitor_GetByLuceneIndexMonitorID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneIndexMonitor table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexMonitor_GetByLuceneIndexMonitorID
(

	@LuceneIndexMonitorID bigint   
)
AS


				SELECT
					[LuceneIndexMonitorID],
					[SourceID],
					[SourceType],
					[SourceTransactionType],
					[TransactionDateTime],
					[IsDuplicate],
					[AffectedType],
					[IndexerStatusChangedBy]
				FROM
					[dbo].[ZNodeLuceneIndexMonitor]
				WHERE
					[LuceneIndexMonitorID] = @LuceneIndexMonitorID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneDocumentMapping_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeLuceneDocumentMapping table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneDocumentMapping_Find
(

	@SearchUsingOR bit   = null ,

	@LuceneDocumentMappingID int   = null ,

	@PropertyName nvarchar (200)  = null ,

	@DocumentName nvarchar (200)  = null ,

	@IsIndexed bit   = null ,

	@IsFaceted bit   = null ,

	@IsStored bit   = null ,

	@IsBoosted bit   = null ,

	@FieldBoostable bit   = null ,

	@Boost float   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LuceneDocumentMappingID]
	, [PropertyName]
	, [DocumentName]
	, [IsIndexed]
	, [IsFaceted]
	, [IsStored]
	, [IsBoosted]
	, [FieldBoostable]
	, [Boost]
    FROM
	[dbo].[ZNodeLuceneDocumentMapping]
    WHERE 
	 ([LuceneDocumentMappingID] = @LuceneDocumentMappingID OR @LuceneDocumentMappingID IS NULL)
	AND ([PropertyName] = @PropertyName OR @PropertyName IS NULL)
	AND ([DocumentName] = @DocumentName OR @DocumentName IS NULL)
	AND ([IsIndexed] = @IsIndexed OR @IsIndexed IS NULL)
	AND ([IsFaceted] = @IsFaceted OR @IsFaceted IS NULL)
	AND ([IsStored] = @IsStored OR @IsStored IS NULL)
	AND ([IsBoosted] = @IsBoosted OR @IsBoosted IS NULL)
	AND ([FieldBoostable] = @FieldBoostable OR @FieldBoostable IS NULL)
	AND ([Boost] = @Boost OR @Boost IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LuceneDocumentMappingID]
	, [PropertyName]
	, [DocumentName]
	, [IsIndexed]
	, [IsFaceted]
	, [IsStored]
	, [IsBoosted]
	, [FieldBoostable]
	, [Boost]
    FROM
	[dbo].[ZNodeLuceneDocumentMapping]
    WHERE 
	 ([LuceneDocumentMappingID] = @LuceneDocumentMappingID AND @LuceneDocumentMappingID is not null)
	OR ([PropertyName] = @PropertyName AND @PropertyName is not null)
	OR ([DocumentName] = @DocumentName AND @DocumentName is not null)
	OR ([IsIndexed] = @IsIndexed AND @IsIndexed is not null)
	OR ([IsFaceted] = @IsFaceted AND @IsFaceted is not null)
	OR ([IsStored] = @IsStored AND @IsStored is not null)
	OR ([IsBoosted] = @IsBoosted AND @IsBoosted is not null)
	OR ([FieldBoostable] = @FieldBoostable AND @FieldBoostable is not null)
	OR ([Boost] = @Boost AND @Boost is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBoost_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeLuceneGlobalProductBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBoost_Insert
(

	@LuceneGlobalProductBoostID int    OUTPUT,

	@ProductID int   ,

	@Boost float   
)
AS


				
				INSERT INTO [dbo].[ZNodeLuceneGlobalProductBoost]
					(
					[ProductID]
					,[Boost]
					)
				VALUES
					(
					@ProductID
					,@Boost
					)
				
				-- Get the identity value
				SET @LuceneGlobalProductBoostID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexMonitor_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeLuceneIndexMonitor table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexMonitor_Find
(

	@SearchUsingOR bit   = null ,

	@LuceneIndexMonitorID bigint   = null ,

	@SourceID int   = null ,

	@SourceType varchar (50)  = null ,

	@SourceTransactionType varchar (10)  = null ,

	@TransactionDateTime datetime   = null ,

	@IsDuplicate bit   = null ,

	@AffectedType varchar (50)  = null ,

	@IndexerStatusChangedBy nvarchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LuceneIndexMonitorID]
	, [SourceID]
	, [SourceType]
	, [SourceTransactionType]
	, [TransactionDateTime]
	, [IsDuplicate]
	, [AffectedType]
	, [IndexerStatusChangedBy]
    FROM
	[dbo].[ZNodeLuceneIndexMonitor]
    WHERE 
	 ([LuceneIndexMonitorID] = @LuceneIndexMonitorID OR @LuceneIndexMonitorID IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([SourceType] = @SourceType OR @SourceType IS NULL)
	AND ([SourceTransactionType] = @SourceTransactionType OR @SourceTransactionType IS NULL)
	AND ([TransactionDateTime] = @TransactionDateTime OR @TransactionDateTime IS NULL)
	AND ([IsDuplicate] = @IsDuplicate OR @IsDuplicate IS NULL)
	AND ([AffectedType] = @AffectedType OR @AffectedType IS NULL)
	AND ([IndexerStatusChangedBy] = @IndexerStatusChangedBy OR @IndexerStatusChangedBy IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LuceneIndexMonitorID]
	, [SourceID]
	, [SourceType]
	, [SourceTransactionType]
	, [TransactionDateTime]
	, [IsDuplicate]
	, [AffectedType]
	, [IndexerStatusChangedBy]
    FROM
	[dbo].[ZNodeLuceneIndexMonitor]
    WHERE 
	 ([LuceneIndexMonitorID] = @LuceneIndexMonitorID AND @LuceneIndexMonitorID is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([SourceType] = @SourceType AND @SourceType is not null)
	OR ([SourceTransactionType] = @SourceTransactionType AND @SourceTransactionType is not null)
	OR ([TransactionDateTime] = @TransactionDateTime AND @TransactionDateTime is not null)
	OR ([IsDuplicate] = @IsDuplicate AND @IsDuplicate is not null)
	OR ([AffectedType] = @AffectedType AND @AffectedType is not null)
	OR ([IndexerStatusChangedBy] = @IndexerStatusChangedBy AND @IndexerStatusChangedBy is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBoost_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeLuceneGlobalProductBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBoost_Update
(

	@LuceneGlobalProductBoostID int   ,

	@ProductID int   ,

	@Boost float   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeLuceneGlobalProductBoost]
				SET
					[ProductID] = @ProductID
					,[Boost] = @Boost
				WHERE
[LuceneGlobalProductBoostID] = @LuceneGlobalProductBoostID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBoost_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeLuceneGlobalProductBoost table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBoost_Delete
(

	@LuceneGlobalProductBoostID int   
)
AS


				DELETE FROM [dbo].[ZNodeLuceneGlobalProductBoost] WITH (ROWLOCK) 
				WHERE
					[LuceneGlobalProductBoostID] = @LuceneGlobalProductBoostID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBoost_GetByProductID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneGlobalProductBoost table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBoost_GetByProductID
(

	@ProductID int   
)
AS


				SELECT
					[LuceneGlobalProductBoostID],
					[ProductID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductBoost]
				WHERE
					[ProductID] = @ProductID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBoost_GetByLuceneGlobalProductBoostID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneGlobalProductBoost table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBoost_GetByLuceneGlobalProductBoostID
(

	@LuceneGlobalProductBoostID int   
)
AS


				SELECT
					[LuceneGlobalProductBoostID],
					[ProductID],
					[Boost]
				FROM
					[dbo].[ZNodeLuceneGlobalProductBoost]
				WHERE
					[LuceneGlobalProductBoostID] = @LuceneGlobalProductBoostID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBoost_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeLuceneGlobalProductBoost table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBoost_Find
(

	@SearchUsingOR bit   = null ,

	@LuceneGlobalProductBoostID int   = null ,

	@ProductID int   = null ,

	@Boost float   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LuceneGlobalProductBoostID]
	, [ProductID]
	, [Boost]
    FROM
	[dbo].[ZNodeLuceneGlobalProductBoost]
    WHERE 
	 ([LuceneGlobalProductBoostID] = @LuceneGlobalProductBoostID OR @LuceneGlobalProductBoostID IS NULL)
	AND ([ProductID] = @ProductID OR @ProductID IS NULL)
	AND ([Boost] = @Boost OR @Boost IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LuceneGlobalProductBoostID]
	, [ProductID]
	, [Boost]
    FROM
	[dbo].[ZNodeLuceneGlobalProductBoost]
    WHERE 
	 ([LuceneGlobalProductBoostID] = @LuceneGlobalProductBoostID AND @LuceneGlobalProductBoostID is not null)
	OR ([ProductID] = @ProductID AND @ProductID is not null)
	OR ([Boost] = @Boost AND @Boost is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodePortal table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Get_List

AS


				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress],
					[EnablePIMS],
					[ExternalID]
				FROM
					[dbo].[ZNodePortal]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodePortal table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Insert
(

	@PortalID int    OUTPUT,

	@CompanyName nvarchar (MAX)  ,

	@StoreName nvarchar (MAX)  ,

	@LogoPath nvarchar (MAX)  ,

	@UseSSL bit   ,

	@AdminEmail nvarchar (MAX)  ,

	@SalesEmail nvarchar (MAX)  ,

	@CustomerServiceEmail nvarchar (MAX)  ,

	@SalesPhoneNumber nvarchar (MAX)  ,

	@CustomerServicePhoneNumber nvarchar (MAX)  ,

	@ImageNotAvailablePath nvarchar (MAX)  ,

	@MaxCatalogDisplayColumns tinyint   ,

	@MaxCatalogDisplayItems int   ,

	@MaxCatalogCategoryDisplayThumbnails int   ,

	@MaxCatalogItemSmallThumbnailWidth int   ,

	@MaxCatalogItemSmallWidth int   ,

	@MaxCatalogItemMediumWidth int   ,

	@MaxCatalogItemThumbnailWidth int   ,

	@MaxCatalogItemLargeWidth int   ,

	@MaxCatalogItemCrossSellWidth int   ,

	@ShowSwatchInCategory bit   ,

	@ShowAlternateImageInCategory bit   ,

	@ActiveInd bit   ,

	@SMTPServer nvarchar (MAX)  ,

	@SMTPUserName nvarchar (MAX)  ,

	@SMTPPassword nvarchar (MAX)  ,

	@SMTPPort int   ,

	@SiteWideBottomJavascript ntext   ,

	@SiteWideTopJavascript ntext   ,

	@OrderReceiptAffiliateJavascript ntext   ,

	@SiteWideAnalyticsJavascript ntext   ,

	@GoogleAnalyticsCode ntext   ,

	@UPSUserName nvarchar (MAX)  ,

	@UPSPassword nvarchar (MAX)  ,

	@UPSKey nvarchar (MAX)  ,

	@ShippingOriginZipCode nvarchar (50)  ,

	@MasterPage nvarchar (MAX)  ,

	@ShopByPriceMin int   ,

	@ShopByPriceMax int   ,

	@ShopByPriceIncrement int   ,

	@FedExAccountNumber nvarchar (MAX)  ,

	@FedExMeterNumber nvarchar (MAX)  ,

	@FedExProductionKey nvarchar (MAX)  ,

	@FedExSecurityCode nvarchar (MAX)  ,

	@FedExCSPKey nvarchar (MAX)  ,

	@FedExCSPPassword nvarchar (MAX)  ,

	@FedExClientProductId nvarchar (MAX)  ,

	@FedExClientProductVersion nvarchar (MAX)  ,

	@FedExDropoffType nvarchar (MAX)  ,

	@FedExPackagingType nvarchar (MAX)  ,

	@FedExUseDiscountRate bit   ,

	@FedExAddInsurance bit   ,

	@ShippingOriginAddress1 nvarchar (MAX)  ,

	@ShippingOriginAddress2 nvarchar (MAX)  ,

	@ShippingOriginCity nvarchar (MAX)  ,

	@ShippingOriginStateCode nvarchar (MAX)  ,

	@ShippingOriginCountryCode nvarchar (MAX)  ,

	@ShippingOriginPhone nvarchar (MAX)  ,

	@CurrencyTypeID int   ,

	@WeightUnit nvarchar (MAX)  ,

	@DimensionUnit nvarchar (MAX)  ,

	@EmailListLogin nvarchar (100)  ,

	@EmailListPassword nvarchar (100)  ,

	@EmailListDefaultList nvarchar (MAX)  ,

	@ShippingTaxable bit   ,

	@DefaultOrderStateID int   ,

	@DefaultReviewStatus nvarchar (1)  ,

	@DefaultAnonymousProfileID int   ,

	@DefaultRegisteredProfileID int   ,

	@InclusiveTax bit   ,

	@SeoDefaultProductTitle nvarchar (MAX)  ,

	@SeoDefaultProductDescription nvarchar (MAX)  ,

	@SeoDefaultProductKeyword nvarchar (MAX)  ,

	@SeoDefaultCategoryTitle nvarchar (MAX)  ,

	@SeoDefaultCategoryDescription nvarchar (MAX)  ,

	@SeoDefaultCategoryKeyword nvarchar (MAX)  ,

	@SeoDefaultContentTitle nvarchar (MAX)  ,

	@SeoDefaultContentDescription nvarchar (MAX)  ,

	@SeoDefaultContentKeyword nvarchar (MAX)  ,

	@TimeZoneOffset nvarchar (50)  ,

	@LocaleID int   ,

	@SplashCategoryID int   ,

	@SplashImageFile nvarchar (MAX)  ,

	@MobileTheme nvarchar (MAX)  ,

	@PersistentCartEnabled bit   ,

	@DefaultProductReviewStateID int   ,

	@UseDynamicDisplayOrder bit   ,

	@EnableAddressValidation bit   ,

	@RequireValidatedAddress bit   ,

	@EnablePIMS bit   ,

	@ExternalID varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ZNodePortal]
					(
					[CompanyName]
					,[StoreName]
					,[LogoPath]
					,[UseSSL]
					,[AdminEmail]
					,[SalesEmail]
					,[CustomerServiceEmail]
					,[SalesPhoneNumber]
					,[CustomerServicePhoneNumber]
					,[ImageNotAvailablePath]
					,[MaxCatalogDisplayColumns]
					,[MaxCatalogDisplayItems]
					,[MaxCatalogCategoryDisplayThumbnails]
					,[MaxCatalogItemSmallThumbnailWidth]
					,[MaxCatalogItemSmallWidth]
					,[MaxCatalogItemMediumWidth]
					,[MaxCatalogItemThumbnailWidth]
					,[MaxCatalogItemLargeWidth]
					,[MaxCatalogItemCrossSellWidth]
					,[ShowSwatchInCategory]
					,[ShowAlternateImageInCategory]
					,[ActiveInd]
					,[SMTPServer]
					,[SMTPUserName]
					,[SMTPPassword]
					,[SMTPPort]
					,[SiteWideBottomJavascript]
					,[SiteWideTopJavascript]
					,[OrderReceiptAffiliateJavascript]
					,[SiteWideAnalyticsJavascript]
					,[GoogleAnalyticsCode]
					,[UPSUserName]
					,[UPSPassword]
					,[UPSKey]
					,[ShippingOriginZipCode]
					,[MasterPage]
					,[ShopByPriceMin]
					,[ShopByPriceMax]
					,[ShopByPriceIncrement]
					,[FedExAccountNumber]
					,[FedExMeterNumber]
					,[FedExProductionKey]
					,[FedExSecurityCode]
					,[FedExCSPKey]
					,[FedExCSPPassword]
					,[FedExClientProductId]
					,[FedExClientProductVersion]
					,[FedExDropoffType]
					,[FedExPackagingType]
					,[FedExUseDiscountRate]
					,[FedExAddInsurance]
					,[ShippingOriginAddress1]
					,[ShippingOriginAddress2]
					,[ShippingOriginCity]
					,[ShippingOriginStateCode]
					,[ShippingOriginCountryCode]
					,[ShippingOriginPhone]
					,[CurrencyTypeID]
					,[WeightUnit]
					,[DimensionUnit]
					,[EmailListLogin]
					,[EmailListPassword]
					,[EmailListDefaultList]
					,[ShippingTaxable]
					,[DefaultOrderStateID]
					,[DefaultReviewStatus]
					,[DefaultAnonymousProfileID]
					,[DefaultRegisteredProfileID]
					,[InclusiveTax]
					,[SeoDefaultProductTitle]
					,[SeoDefaultProductDescription]
					,[SeoDefaultProductKeyword]
					,[SeoDefaultCategoryTitle]
					,[SeoDefaultCategoryDescription]
					,[SeoDefaultCategoryKeyword]
					,[SeoDefaultContentTitle]
					,[SeoDefaultContentDescription]
					,[SeoDefaultContentKeyword]
					,[TimeZoneOffset]
					,[LocaleID]
					,[SplashCategoryID]
					,[SplashImageFile]
					,[MobileTheme]
					,[PersistentCartEnabled]
					,[DefaultProductReviewStateID]
					,[UseDynamicDisplayOrder]
					,[EnableAddressValidation]
					,[RequireValidatedAddress]
					,[EnablePIMS]
					,[ExternalID]
					)
				VALUES
					(
					@CompanyName
					,@StoreName
					,@LogoPath
					,@UseSSL
					,@AdminEmail
					,@SalesEmail
					,@CustomerServiceEmail
					,@SalesPhoneNumber
					,@CustomerServicePhoneNumber
					,@ImageNotAvailablePath
					,@MaxCatalogDisplayColumns
					,@MaxCatalogDisplayItems
					,@MaxCatalogCategoryDisplayThumbnails
					,@MaxCatalogItemSmallThumbnailWidth
					,@MaxCatalogItemSmallWidth
					,@MaxCatalogItemMediumWidth
					,@MaxCatalogItemThumbnailWidth
					,@MaxCatalogItemLargeWidth
					,@MaxCatalogItemCrossSellWidth
					,@ShowSwatchInCategory
					,@ShowAlternateImageInCategory
					,@ActiveInd
					,@SMTPServer
					,@SMTPUserName
					,@SMTPPassword
					,@SMTPPort
					,@SiteWideBottomJavascript
					,@SiteWideTopJavascript
					,@OrderReceiptAffiliateJavascript
					,@SiteWideAnalyticsJavascript
					,@GoogleAnalyticsCode
					,@UPSUserName
					,@UPSPassword
					,@UPSKey
					,@ShippingOriginZipCode
					,@MasterPage
					,@ShopByPriceMin
					,@ShopByPriceMax
					,@ShopByPriceIncrement
					,@FedExAccountNumber
					,@FedExMeterNumber
					,@FedExProductionKey
					,@FedExSecurityCode
					,@FedExCSPKey
					,@FedExCSPPassword
					,@FedExClientProductId
					,@FedExClientProductVersion
					,@FedExDropoffType
					,@FedExPackagingType
					,@FedExUseDiscountRate
					,@FedExAddInsurance
					,@ShippingOriginAddress1
					,@ShippingOriginAddress2
					,@ShippingOriginCity
					,@ShippingOriginStateCode
					,@ShippingOriginCountryCode
					,@ShippingOriginPhone
					,@CurrencyTypeID
					,@WeightUnit
					,@DimensionUnit
					,@EmailListLogin
					,@EmailListPassword
					,@EmailListDefaultList
					,@ShippingTaxable
					,@DefaultOrderStateID
					,@DefaultReviewStatus
					,@DefaultAnonymousProfileID
					,@DefaultRegisteredProfileID
					,@InclusiveTax
					,@SeoDefaultProductTitle
					,@SeoDefaultProductDescription
					,@SeoDefaultProductKeyword
					,@SeoDefaultCategoryTitle
					,@SeoDefaultCategoryDescription
					,@SeoDefaultCategoryKeyword
					,@SeoDefaultContentTitle
					,@SeoDefaultContentDescription
					,@SeoDefaultContentKeyword
					,@TimeZoneOffset
					,@LocaleID
					,@SplashCategoryID
					,@SplashImageFile
					,@MobileTheme
					,@PersistentCartEnabled
					,@DefaultProductReviewStateID
					,@UseDynamicDisplayOrder
					,@EnableAddressValidation
					,@RequireValidatedAddress
					,@EnablePIMS
					,@ExternalID
					)
				
				-- Get the identity value
				SET @PortalID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_GetCategoryRootItems]'
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[ZNode_GetCategoryRootItems]                
 -- Add the parameters for the stored procedure here                
 @PortalId int          
 ,@CatalogID int = 0  
 ,@ProfileId int = 0
AS                
BEGIN                
 -- SET NOCOUNT ON added to prevent extra result sets from                
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
                
 SELECT                 
  ZCN.CategoryNodeId,ZCN.CategoryId,ZC.[Name],ZC.[Title],VisibleInd, ImageFile, ShortDescription,ZC.[SEOURL]
 FROM  
  ZNodeCategory ZC INNER JOIN ZNodeCategoryNode ZCN ON ZC.CategoryID = ZCN.CategoryID  
  INNER JOIN ZNodePortalCatalog ZPC ON ZCN.CatalogID = ZPC.CatalogID    
 WHERE  
  ZCN.ParentCategoryNodeId IS NULL                   
  AND ZCN.ActiveInd = 1  
  AND ZPC.PortalId= @PortalId   
  AND ZPC.CatalogID = @CatalogID
  AND (ZC.CategoryID NOT IN (SELECT CategoryID FROM ZNodeCategoryProfile	
									WHERE (ProfileID = @ProfileId
										AND EffectiveDate > GETDATE()))
			 OR @ProfileId = 0)  
 Order by ZC.Name  
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Update]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodePortal table
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Update
(

	@PortalID int   ,

	@CompanyName nvarchar (MAX)  ,

	@StoreName nvarchar (MAX)  ,

	@LogoPath nvarchar (MAX)  ,

	@UseSSL bit   ,

	@AdminEmail nvarchar (MAX)  ,

	@SalesEmail nvarchar (MAX)  ,

	@CustomerServiceEmail nvarchar (MAX)  ,

	@SalesPhoneNumber nvarchar (MAX)  ,

	@CustomerServicePhoneNumber nvarchar (MAX)  ,

	@ImageNotAvailablePath nvarchar (MAX)  ,

	@MaxCatalogDisplayColumns tinyint   ,

	@MaxCatalogDisplayItems int   ,

	@MaxCatalogCategoryDisplayThumbnails int   ,

	@MaxCatalogItemSmallThumbnailWidth int   ,

	@MaxCatalogItemSmallWidth int   ,

	@MaxCatalogItemMediumWidth int   ,

	@MaxCatalogItemThumbnailWidth int   ,

	@MaxCatalogItemLargeWidth int   ,

	@MaxCatalogItemCrossSellWidth int   ,

	@ShowSwatchInCategory bit   ,

	@ShowAlternateImageInCategory bit   ,

	@ActiveInd bit   ,

	@SMTPServer nvarchar (MAX)  ,

	@SMTPUserName nvarchar (MAX)  ,

	@SMTPPassword nvarchar (MAX)  ,

	@SMTPPort int   ,

	@SiteWideBottomJavascript ntext   ,

	@SiteWideTopJavascript ntext   ,

	@OrderReceiptAffiliateJavascript ntext   ,

	@SiteWideAnalyticsJavascript ntext   ,

	@GoogleAnalyticsCode ntext   ,

	@UPSUserName nvarchar (MAX)  ,

	@UPSPassword nvarchar (MAX)  ,

	@UPSKey nvarchar (MAX)  ,

	@ShippingOriginZipCode nvarchar (50)  ,

	@MasterPage nvarchar (MAX)  ,

	@ShopByPriceMin int   ,

	@ShopByPriceMax int   ,

	@ShopByPriceIncrement int   ,

	@FedExAccountNumber nvarchar (MAX)  ,

	@FedExMeterNumber nvarchar (MAX)  ,

	@FedExProductionKey nvarchar (MAX)  ,

	@FedExSecurityCode nvarchar (MAX)  ,

	@FedExCSPKey nvarchar (MAX)  ,

	@FedExCSPPassword nvarchar (MAX)  ,

	@FedExClientProductId nvarchar (MAX)  ,

	@FedExClientProductVersion nvarchar (MAX)  ,

	@FedExDropoffType nvarchar (MAX)  ,

	@FedExPackagingType nvarchar (MAX)  ,

	@FedExUseDiscountRate bit   ,

	@FedExAddInsurance bit   ,

	@ShippingOriginAddress1 nvarchar (MAX)  ,

	@ShippingOriginAddress2 nvarchar (MAX)  ,

	@ShippingOriginCity nvarchar (MAX)  ,

	@ShippingOriginStateCode nvarchar (MAX)  ,

	@ShippingOriginCountryCode nvarchar (MAX)  ,

	@ShippingOriginPhone nvarchar (MAX)  ,

	@CurrencyTypeID int   ,

	@WeightUnit nvarchar (MAX)  ,

	@DimensionUnit nvarchar (MAX)  ,

	@EmailListLogin nvarchar (100)  ,

	@EmailListPassword nvarchar (100)  ,

	@EmailListDefaultList nvarchar (MAX)  ,

	@ShippingTaxable bit   ,

	@DefaultOrderStateID int   ,

	@DefaultReviewStatus nvarchar (1)  ,

	@DefaultAnonymousProfileID int   ,

	@DefaultRegisteredProfileID int   ,

	@InclusiveTax bit   ,

	@SeoDefaultProductTitle nvarchar (MAX)  ,

	@SeoDefaultProductDescription nvarchar (MAX)  ,

	@SeoDefaultProductKeyword nvarchar (MAX)  ,

	@SeoDefaultCategoryTitle nvarchar (MAX)  ,

	@SeoDefaultCategoryDescription nvarchar (MAX)  ,

	@SeoDefaultCategoryKeyword nvarchar (MAX)  ,

	@SeoDefaultContentTitle nvarchar (MAX)  ,

	@SeoDefaultContentDescription nvarchar (MAX)  ,

	@SeoDefaultContentKeyword nvarchar (MAX)  ,

	@TimeZoneOffset nvarchar (50)  ,

	@LocaleID int   ,

	@SplashCategoryID int   ,

	@SplashImageFile nvarchar (MAX)  ,

	@MobileTheme nvarchar (MAX)  ,

	@PersistentCartEnabled bit   ,

	@DefaultProductReviewStateID int   ,

	@UseDynamicDisplayOrder bit   ,

	@EnableAddressValidation bit   ,

	@RequireValidatedAddress bit   ,

	@EnablePIMS bit   ,

	@ExternalID varchar (50)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodePortal]
				SET
					[CompanyName] = @CompanyName
					,[StoreName] = @StoreName
					,[LogoPath] = @LogoPath
					,[UseSSL] = @UseSSL
					,[AdminEmail] = @AdminEmail
					,[SalesEmail] = @SalesEmail
					,[CustomerServiceEmail] = @CustomerServiceEmail
					,[SalesPhoneNumber] = @SalesPhoneNumber
					,[CustomerServicePhoneNumber] = @CustomerServicePhoneNumber
					,[ImageNotAvailablePath] = @ImageNotAvailablePath
					,[MaxCatalogDisplayColumns] = @MaxCatalogDisplayColumns
					,[MaxCatalogDisplayItems] = @MaxCatalogDisplayItems
					,[MaxCatalogCategoryDisplayThumbnails] = @MaxCatalogCategoryDisplayThumbnails
					,[MaxCatalogItemSmallThumbnailWidth] = @MaxCatalogItemSmallThumbnailWidth
					,[MaxCatalogItemSmallWidth] = @MaxCatalogItemSmallWidth
					,[MaxCatalogItemMediumWidth] = @MaxCatalogItemMediumWidth
					,[MaxCatalogItemThumbnailWidth] = @MaxCatalogItemThumbnailWidth
					,[MaxCatalogItemLargeWidth] = @MaxCatalogItemLargeWidth
					,[MaxCatalogItemCrossSellWidth] = @MaxCatalogItemCrossSellWidth
					,[ShowSwatchInCategory] = @ShowSwatchInCategory
					,[ShowAlternateImageInCategory] = @ShowAlternateImageInCategory
					,[ActiveInd] = @ActiveInd
					,[SMTPServer] = @SMTPServer
					,[SMTPUserName] = @SMTPUserName
					,[SMTPPassword] = @SMTPPassword
					,[SMTPPort] = @SMTPPort
					,[SiteWideBottomJavascript] = @SiteWideBottomJavascript
					,[SiteWideTopJavascript] = @SiteWideTopJavascript
					,[OrderReceiptAffiliateJavascript] = @OrderReceiptAffiliateJavascript
					,[SiteWideAnalyticsJavascript] = @SiteWideAnalyticsJavascript
					,[GoogleAnalyticsCode] = @GoogleAnalyticsCode
					,[UPSUserName] = @UPSUserName
					,[UPSPassword] = @UPSPassword
					,[UPSKey] = @UPSKey
					,[ShippingOriginZipCode] = @ShippingOriginZipCode
					,[MasterPage] = @MasterPage
					,[ShopByPriceMin] = @ShopByPriceMin
					,[ShopByPriceMax] = @ShopByPriceMax
					,[ShopByPriceIncrement] = @ShopByPriceIncrement
					,[FedExAccountNumber] = @FedExAccountNumber
					,[FedExMeterNumber] = @FedExMeterNumber
					,[FedExProductionKey] = @FedExProductionKey
					,[FedExSecurityCode] = @FedExSecurityCode
					,[FedExCSPKey] = @FedExCSPKey
					,[FedExCSPPassword] = @FedExCSPPassword
					,[FedExClientProductId] = @FedExClientProductId
					,[FedExClientProductVersion] = @FedExClientProductVersion
					,[FedExDropoffType] = @FedExDropoffType
					,[FedExPackagingType] = @FedExPackagingType
					,[FedExUseDiscountRate] = @FedExUseDiscountRate
					,[FedExAddInsurance] = @FedExAddInsurance
					,[ShippingOriginAddress1] = @ShippingOriginAddress1
					,[ShippingOriginAddress2] = @ShippingOriginAddress2
					,[ShippingOriginCity] = @ShippingOriginCity
					,[ShippingOriginStateCode] = @ShippingOriginStateCode
					,[ShippingOriginCountryCode] = @ShippingOriginCountryCode
					,[ShippingOriginPhone] = @ShippingOriginPhone
					,[CurrencyTypeID] = @CurrencyTypeID
					,[WeightUnit] = @WeightUnit
					,[DimensionUnit] = @DimensionUnit
					,[EmailListLogin] = @EmailListLogin
					,[EmailListPassword] = @EmailListPassword
					,[EmailListDefaultList] = @EmailListDefaultList
					,[ShippingTaxable] = @ShippingTaxable
					,[DefaultOrderStateID] = @DefaultOrderStateID
					,[DefaultReviewStatus] = @DefaultReviewStatus
					,[DefaultAnonymousProfileID] = @DefaultAnonymousProfileID
					,[DefaultRegisteredProfileID] = @DefaultRegisteredProfileID
					,[InclusiveTax] = @InclusiveTax
					,[SeoDefaultProductTitle] = @SeoDefaultProductTitle
					,[SeoDefaultProductDescription] = @SeoDefaultProductDescription
					,[SeoDefaultProductKeyword] = @SeoDefaultProductKeyword
					,[SeoDefaultCategoryTitle] = @SeoDefaultCategoryTitle
					,[SeoDefaultCategoryDescription] = @SeoDefaultCategoryDescription
					,[SeoDefaultCategoryKeyword] = @SeoDefaultCategoryKeyword
					,[SeoDefaultContentTitle] = @SeoDefaultContentTitle
					,[SeoDefaultContentDescription] = @SeoDefaultContentDescription
					,[SeoDefaultContentKeyword] = @SeoDefaultContentKeyword
					,[TimeZoneOffset] = @TimeZoneOffset
					,[LocaleID] = @LocaleID
					,[SplashCategoryID] = @SplashCategoryID
					,[SplashImageFile] = @SplashImageFile
					,[MobileTheme] = @MobileTheme
					,[PersistentCartEnabled] = @PersistentCartEnabled
					,[DefaultProductReviewStateID] = @DefaultProductReviewStateID
					,[UseDynamicDisplayOrder] = @UseDynamicDisplayOrder
					,[EnableAddressValidation] = @EnableAddressValidation
					,[RequireValidatedAddress] = @RequireValidatedAddress
					,[EnablePIMS] = @EnablePIMS
					,[ExternalID] = @ExternalID
				WHERE
[PortalID] = @PortalID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchProduct]'
GO
SET ANSI_NULLS ON
GO

      
ALTER PROCEDURE [dbo].[ZNode_SearchProduct] 
 @Name VARCHAR (MAX)='',       
 @ProductNum VARCHAR (MAX)='',       
 @Sku VARCHAR (MAX)='',       
 @ManufacturerId VARCHAR (MAX)='',       
 @ProductTypeId VARCHAR (MAX)='',       
 @CategoryId VARCHAR (MAX)='',       
 @CatalogId INT=0,       
 @SupplierID INT=0,  
 @PortalID INT = 0      
AS      
BEGIN      
    SET NOCOUNT ON;      
    CREATE TABLE #ProductList      
    (      
            ProductId INT,      
            SKU       NVARCHAR (1000)      
    );      
          
    CREATE UNIQUE INDEX IX_ProductList      
        ON #ProductList(ProductId) WITH (IGNORE_DUP_KEY = ON);      
              
    INSERT INTO #ProductList      
    SELECT PRODUCTID,      
           SKU      
    FROM   ZNodeSKU      
   WHERE  ((SKU LIKE '%' + @Sku + '%')      
           OR Len(@Sku) = 0);      
          
     CREATE TABLE #ProductPortalList      
    (      
			PortalId int,
            ProductId INT      
              
    );      
      INSERT INTO #ProductPortalList    
      Select  distinct ZPT.PortalId,ZPC.ProductID from ZnodePortalCatalog ZPT,znodecategorynode ZCN, ZNODEPRODUCTCATEGORY ZPC
       Where ZPT.CatalogId=ZCN.CatalogId and
       ZCN.categoryId=ZPC.CAtegoryId 
      and (Portalid=@PortalId OR Len(@PortalID) = 0 or @PortalID= '0' OR @PortalID = 0 or @PortalId is null)    
                                
                                            
    SELECT distinct (p.ProductID),      
           ImageFile,      
           p.Name,      
           p.ProductNum,      
           RetailPrice,      
           SalePrice,      
           WholeSalePrice, 
           p.portalId,     
           p.DisplayOrder,      
           p.ActiveInd,
           p.Franchisable,  
           p.ReviewStateID,    
           (SELECT TOP 1 QuantityOnHand      
            FROM   ZNodeSKUInventory AS SkuInventory      
            WHERE  SkuInventory.SKU = sku.SKU) AS 'QuantityOnHand'      
    FROM   ZNodeProduct AS P 
           INNER JOIN      
           #ProductList AS sku      
           ON P.ProductID = sku.ProductID 
          LEFT JOIN #ProductPortalList portal on P.ProductID = portal.ProductId
    WHERE  (p.Name LIKE '%' + @Name + '%'      
            OR Len(@Name) = 0)      
           AND (p.ProductNum LIKE '%' + @ProductNum + '%'      
                OR Len(@ProductNum) = 0)      
           AND (p.ProductTypeID = @ProductTypeId      
                OR Len(@ProductTypeId) = 0      
                OR @ProductTypeId = 0      
                OR @ProductTypeId = '0')      
           AND (p.ManufacturerID = @ManufacturerId      
                OR Len(@ManufacturerId) = 0      
                OR @ManufacturerId = '0')      
            AND (p.AccountID = @SupplierID  
                OR Len(@SupplierID) = 0   
                OR @SupplierID= '0' or  @SupplierID is null)             
           AND (p.ProductID IN (SELECT ProductId      
                                FROM   ZNODEPRODUCTCATEGORY      
                                WHERE  CATEGORYID IN (SELECT DISTINCT CategoryID      
                                                      FROM   ZNodeCategoryNode      
                                                      WHERE  CatalogID = @CatalogId))      
                OR @CatalogId = 0)      
           AND (p.ProductID IN (SELECT ProductId      
                                FROM   ZNODEPRODUCTCATEGORY      
                                WHERE  CATEGORYID = @CategoryId)      
                OR LEN(@CategoryId) = 0      
                OR @CategoryId = '0')    
          
               
END        
   
   


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByCurrencyTypeID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByCurrencyTypeID
(

	@CurrencyTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress],
					[EnablePIMS],
					[ExternalID]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[CurrencyTypeID] = @CurrencyTypeID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByLocaleID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByLocaleID
(

	@LocaleID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress],
					[EnablePIMS],
					[ExternalID]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[LocaleID] = @LocaleID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByDefaultOrderStateID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByDefaultOrderStateID
(

	@DefaultOrderStateID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress],
					[EnablePIMS],
					[ExternalID]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[DefaultOrderStateID] = @DefaultOrderStateID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexServerStatus_Get_List]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeLuceneIndexServerStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexServerStatus_Get_List

AS


				
				SELECT
					[ID],
					[ServerName],
					[LuceneIndexMonitorID],
					[Status],
					[StartTime],
					[EndTime]
				FROM
					[dbo].[ZNodeLuceneIndexServerStatus]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByDefaultProductReviewStateID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through a foreign key
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByDefaultProductReviewStateID
(

	@DefaultProductReviewStateID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress],
					[EnablePIMS],
					[ExternalID]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[DefaultProductReviewStateID] = @DefaultProductReviewStateID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByActiveInd]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByActiveInd
(

	@ActiveInd bit   
)
AS


				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress],
					[EnablePIMS],
					[ExternalID]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[ActiveInd] = @ActiveInd
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexServerStatus_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeLuceneIndexServerStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexServerStatus_Insert
(

	@ID int    OUTPUT,

	@ServerName nvarchar (50)  ,

	@LuceneIndexMonitorID bigint   ,

	@Status int   ,

	@StartTime datetime   ,

	@EndTime datetime   
)
AS


				
				INSERT INTO [dbo].[ZNodeLuceneIndexServerStatus]
					(
					[ServerName]
					,[LuceneIndexMonitorID]
					,[Status]
					,[StartTime]
					,[EndTime]
					)
				VALUES
					(
					@ServerName
					,@LuceneIndexMonitorID
					,@Status
					,@StartTime
					,@EndTime
					)
				
				-- Get the identity value
				SET @ID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetByPortalID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodePortal table through an index
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetByPortalID
(

	@PortalID int   
)
AS


				SELECT
					[PortalID],
					[CompanyName],
					[StoreName],
					[LogoPath],
					[UseSSL],
					[AdminEmail],
					[SalesEmail],
					[CustomerServiceEmail],
					[SalesPhoneNumber],
					[CustomerServicePhoneNumber],
					[ImageNotAvailablePath],
					[MaxCatalogDisplayColumns],
					[MaxCatalogDisplayItems],
					[MaxCatalogCategoryDisplayThumbnails],
					[MaxCatalogItemSmallThumbnailWidth],
					[MaxCatalogItemSmallWidth],
					[MaxCatalogItemMediumWidth],
					[MaxCatalogItemThumbnailWidth],
					[MaxCatalogItemLargeWidth],
					[MaxCatalogItemCrossSellWidth],
					[ShowSwatchInCategory],
					[ShowAlternateImageInCategory],
					[ActiveInd],
					[SMTPServer],
					[SMTPUserName],
					[SMTPPassword],
					[SMTPPort],
					[SiteWideBottomJavascript],
					[SiteWideTopJavascript],
					[OrderReceiptAffiliateJavascript],
					[SiteWideAnalyticsJavascript],
					[GoogleAnalyticsCode],
					[UPSUserName],
					[UPSPassword],
					[UPSKey],
					[ShippingOriginZipCode],
					[MasterPage],
					[ShopByPriceMin],
					[ShopByPriceMax],
					[ShopByPriceIncrement],
					[FedExAccountNumber],
					[FedExMeterNumber],
					[FedExProductionKey],
					[FedExSecurityCode],
					[FedExCSPKey],
					[FedExCSPPassword],
					[FedExClientProductId],
					[FedExClientProductVersion],
					[FedExDropoffType],
					[FedExPackagingType],
					[FedExUseDiscountRate],
					[FedExAddInsurance],
					[ShippingOriginAddress1],
					[ShippingOriginAddress2],
					[ShippingOriginCity],
					[ShippingOriginStateCode],
					[ShippingOriginCountryCode],
					[ShippingOriginPhone],
					[CurrencyTypeID],
					[WeightUnit],
					[DimensionUnit],
					[EmailListLogin],
					[EmailListPassword],
					[EmailListDefaultList],
					[ShippingTaxable],
					[DefaultOrderStateID],
					[DefaultReviewStatus],
					[DefaultAnonymousProfileID],
					[DefaultRegisteredProfileID],
					[InclusiveTax],
					[SeoDefaultProductTitle],
					[SeoDefaultProductDescription],
					[SeoDefaultProductKeyword],
					[SeoDefaultCategoryTitle],
					[SeoDefaultCategoryDescription],
					[SeoDefaultCategoryKeyword],
					[SeoDefaultContentTitle],
					[SeoDefaultContentDescription],
					[SeoDefaultContentKeyword],
					[TimeZoneOffset],
					[LocaleID],
					[SplashCategoryID],
					[SplashImageFile],
					[MobileTheme],
					[PersistentCartEnabled],
					[DefaultProductReviewStateID],
					[UseDynamicDisplayOrder],
					[EnableAddressValidation],
					[RequireValidatedAddress],
					[EnablePIMS],
					[ExternalID]
				FROM
					[dbo].[ZNodePortal]
				WHERE
					[PortalID] = @PortalID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexServerStatus_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeLuceneIndexServerStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexServerStatus_Update
(

	@ID int   ,

	@ServerName nvarchar (50)  ,

	@LuceneIndexMonitorID bigint   ,

	@Status int   ,

	@StartTime datetime   ,

	@EndTime datetime   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeLuceneIndexServerStatus]
				SET
					[ServerName] = @ServerName
					,[LuceneIndexMonitorID] = @LuceneIndexMonitorID
					,[Status] = @Status
					,[StartTime] = @StartTime
					,[EndTime] = @EndTime
				WHERE
[ID] = @ID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodePortal table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_Find
(

	@SearchUsingOR bit   = null ,

	@PortalID int   = null ,

	@CompanyName nvarchar (MAX)  = null ,

	@StoreName nvarchar (MAX)  = null ,

	@LogoPath nvarchar (MAX)  = null ,

	@UseSSL bit   = null ,

	@AdminEmail nvarchar (MAX)  = null ,

	@SalesEmail nvarchar (MAX)  = null ,

	@CustomerServiceEmail nvarchar (MAX)  = null ,

	@SalesPhoneNumber nvarchar (MAX)  = null ,

	@CustomerServicePhoneNumber nvarchar (MAX)  = null ,

	@ImageNotAvailablePath nvarchar (MAX)  = null ,

	@MaxCatalogDisplayColumns tinyint   = null ,

	@MaxCatalogDisplayItems int   = null ,

	@MaxCatalogCategoryDisplayThumbnails int   = null ,

	@MaxCatalogItemSmallThumbnailWidth int   = null ,

	@MaxCatalogItemSmallWidth int   = null ,

	@MaxCatalogItemMediumWidth int   = null ,

	@MaxCatalogItemThumbnailWidth int   = null ,

	@MaxCatalogItemLargeWidth int   = null ,

	@MaxCatalogItemCrossSellWidth int   = null ,

	@ShowSwatchInCategory bit   = null ,

	@ShowAlternateImageInCategory bit   = null ,

	@ActiveInd bit   = null ,

	@SMTPServer nvarchar (MAX)  = null ,

	@SMTPUserName nvarchar (MAX)  = null ,

	@SMTPPassword nvarchar (MAX)  = null ,

	@SMTPPort int   = null ,

	@SiteWideBottomJavascript ntext   = null ,

	@SiteWideTopJavascript ntext   = null ,

	@OrderReceiptAffiliateJavascript ntext   = null ,

	@SiteWideAnalyticsJavascript ntext   = null ,

	@GoogleAnalyticsCode ntext   = null ,

	@UPSUserName nvarchar (MAX)  = null ,

	@UPSPassword nvarchar (MAX)  = null ,

	@UPSKey nvarchar (MAX)  = null ,

	@ShippingOriginZipCode nvarchar (50)  = null ,

	@MasterPage nvarchar (MAX)  = null ,

	@ShopByPriceMin int   = null ,

	@ShopByPriceMax int   = null ,

	@ShopByPriceIncrement int   = null ,

	@FedExAccountNumber nvarchar (MAX)  = null ,

	@FedExMeterNumber nvarchar (MAX)  = null ,

	@FedExProductionKey nvarchar (MAX)  = null ,

	@FedExSecurityCode nvarchar (MAX)  = null ,

	@FedExCSPKey nvarchar (MAX)  = null ,

	@FedExCSPPassword nvarchar (MAX)  = null ,

	@FedExClientProductId nvarchar (MAX)  = null ,

	@FedExClientProductVersion nvarchar (MAX)  = null ,

	@FedExDropoffType nvarchar (MAX)  = null ,

	@FedExPackagingType nvarchar (MAX)  = null ,

	@FedExUseDiscountRate bit   = null ,

	@FedExAddInsurance bit   = null ,

	@ShippingOriginAddress1 nvarchar (MAX)  = null ,

	@ShippingOriginAddress2 nvarchar (MAX)  = null ,

	@ShippingOriginCity nvarchar (MAX)  = null ,

	@ShippingOriginStateCode nvarchar (MAX)  = null ,

	@ShippingOriginCountryCode nvarchar (MAX)  = null ,

	@ShippingOriginPhone nvarchar (MAX)  = null ,

	@CurrencyTypeID int   = null ,

	@WeightUnit nvarchar (MAX)  = null ,

	@DimensionUnit nvarchar (MAX)  = null ,

	@EmailListLogin nvarchar (100)  = null ,

	@EmailListPassword nvarchar (100)  = null ,

	@EmailListDefaultList nvarchar (MAX)  = null ,

	@ShippingTaxable bit   = null ,

	@DefaultOrderStateID int   = null ,

	@DefaultReviewStatus nvarchar (1)  = null ,

	@DefaultAnonymousProfileID int   = null ,

	@DefaultRegisteredProfileID int   = null ,

	@InclusiveTax bit   = null ,

	@SeoDefaultProductTitle nvarchar (MAX)  = null ,

	@SeoDefaultProductDescription nvarchar (MAX)  = null ,

	@SeoDefaultProductKeyword nvarchar (MAX)  = null ,

	@SeoDefaultCategoryTitle nvarchar (MAX)  = null ,

	@SeoDefaultCategoryDescription nvarchar (MAX)  = null ,

	@SeoDefaultCategoryKeyword nvarchar (MAX)  = null ,

	@SeoDefaultContentTitle nvarchar (MAX)  = null ,

	@SeoDefaultContentDescription nvarchar (MAX)  = null ,

	@SeoDefaultContentKeyword nvarchar (MAX)  = null ,

	@TimeZoneOffset nvarchar (50)  = null ,

	@LocaleID int   = null ,

	@SplashCategoryID int   = null ,

	@SplashImageFile nvarchar (MAX)  = null ,

	@MobileTheme nvarchar (MAX)  = null ,

	@PersistentCartEnabled bit   = null ,

	@DefaultProductReviewStateID int   = null ,

	@UseDynamicDisplayOrder bit   = null ,

	@EnableAddressValidation bit   = null ,

	@RequireValidatedAddress bit   = null ,

	@EnablePIMS bit   = null ,

	@ExternalID varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalID]
	, [CompanyName]
	, [StoreName]
	, [LogoPath]
	, [UseSSL]
	, [AdminEmail]
	, [SalesEmail]
	, [CustomerServiceEmail]
	, [SalesPhoneNumber]
	, [CustomerServicePhoneNumber]
	, [ImageNotAvailablePath]
	, [MaxCatalogDisplayColumns]
	, [MaxCatalogDisplayItems]
	, [MaxCatalogCategoryDisplayThumbnails]
	, [MaxCatalogItemSmallThumbnailWidth]
	, [MaxCatalogItemSmallWidth]
	, [MaxCatalogItemMediumWidth]
	, [MaxCatalogItemThumbnailWidth]
	, [MaxCatalogItemLargeWidth]
	, [MaxCatalogItemCrossSellWidth]
	, [ShowSwatchInCategory]
	, [ShowAlternateImageInCategory]
	, [ActiveInd]
	, [SMTPServer]
	, [SMTPUserName]
	, [SMTPPassword]
	, [SMTPPort]
	, [SiteWideBottomJavascript]
	, [SiteWideTopJavascript]
	, [OrderReceiptAffiliateJavascript]
	, [SiteWideAnalyticsJavascript]
	, [GoogleAnalyticsCode]
	, [UPSUserName]
	, [UPSPassword]
	, [UPSKey]
	, [ShippingOriginZipCode]
	, [MasterPage]
	, [ShopByPriceMin]
	, [ShopByPriceMax]
	, [ShopByPriceIncrement]
	, [FedExAccountNumber]
	, [FedExMeterNumber]
	, [FedExProductionKey]
	, [FedExSecurityCode]
	, [FedExCSPKey]
	, [FedExCSPPassword]
	, [FedExClientProductId]
	, [FedExClientProductVersion]
	, [FedExDropoffType]
	, [FedExPackagingType]
	, [FedExUseDiscountRate]
	, [FedExAddInsurance]
	, [ShippingOriginAddress1]
	, [ShippingOriginAddress2]
	, [ShippingOriginCity]
	, [ShippingOriginStateCode]
	, [ShippingOriginCountryCode]
	, [ShippingOriginPhone]
	, [CurrencyTypeID]
	, [WeightUnit]
	, [DimensionUnit]
	, [EmailListLogin]
	, [EmailListPassword]
	, [EmailListDefaultList]
	, [ShippingTaxable]
	, [DefaultOrderStateID]
	, [DefaultReviewStatus]
	, [DefaultAnonymousProfileID]
	, [DefaultRegisteredProfileID]
	, [InclusiveTax]
	, [SeoDefaultProductTitle]
	, [SeoDefaultProductDescription]
	, [SeoDefaultProductKeyword]
	, [SeoDefaultCategoryTitle]
	, [SeoDefaultCategoryDescription]
	, [SeoDefaultCategoryKeyword]
	, [SeoDefaultContentTitle]
	, [SeoDefaultContentDescription]
	, [SeoDefaultContentKeyword]
	, [TimeZoneOffset]
	, [LocaleID]
	, [SplashCategoryID]
	, [SplashImageFile]
	, [MobileTheme]
	, [PersistentCartEnabled]
	, [DefaultProductReviewStateID]
	, [UseDynamicDisplayOrder]
	, [EnableAddressValidation]
	, [RequireValidatedAddress]
	, [EnablePIMS]
	, [ExternalID]
    FROM
	[dbo].[ZNodePortal]
    WHERE 
	 ([PortalID] = @PortalID OR @PortalID IS NULL)
	AND ([CompanyName] = @CompanyName OR @CompanyName IS NULL)
	AND ([StoreName] = @StoreName OR @StoreName IS NULL)
	AND ([LogoPath] = @LogoPath OR @LogoPath IS NULL)
	AND ([UseSSL] = @UseSSL OR @UseSSL IS NULL)
	AND ([AdminEmail] = @AdminEmail OR @AdminEmail IS NULL)
	AND ([SalesEmail] = @SalesEmail OR @SalesEmail IS NULL)
	AND ([CustomerServiceEmail] = @CustomerServiceEmail OR @CustomerServiceEmail IS NULL)
	AND ([SalesPhoneNumber] = @SalesPhoneNumber OR @SalesPhoneNumber IS NULL)
	AND ([CustomerServicePhoneNumber] = @CustomerServicePhoneNumber OR @CustomerServicePhoneNumber IS NULL)
	AND ([ImageNotAvailablePath] = @ImageNotAvailablePath OR @ImageNotAvailablePath IS NULL)
	AND ([MaxCatalogDisplayColumns] = @MaxCatalogDisplayColumns OR @MaxCatalogDisplayColumns IS NULL)
	AND ([MaxCatalogDisplayItems] = @MaxCatalogDisplayItems OR @MaxCatalogDisplayItems IS NULL)
	AND ([MaxCatalogCategoryDisplayThumbnails] = @MaxCatalogCategoryDisplayThumbnails OR @MaxCatalogCategoryDisplayThumbnails IS NULL)
	AND ([MaxCatalogItemSmallThumbnailWidth] = @MaxCatalogItemSmallThumbnailWidth OR @MaxCatalogItemSmallThumbnailWidth IS NULL)
	AND ([MaxCatalogItemSmallWidth] = @MaxCatalogItemSmallWidth OR @MaxCatalogItemSmallWidth IS NULL)
	AND ([MaxCatalogItemMediumWidth] = @MaxCatalogItemMediumWidth OR @MaxCatalogItemMediumWidth IS NULL)
	AND ([MaxCatalogItemThumbnailWidth] = @MaxCatalogItemThumbnailWidth OR @MaxCatalogItemThumbnailWidth IS NULL)
	AND ([MaxCatalogItemLargeWidth] = @MaxCatalogItemLargeWidth OR @MaxCatalogItemLargeWidth IS NULL)
	AND ([MaxCatalogItemCrossSellWidth] = @MaxCatalogItemCrossSellWidth OR @MaxCatalogItemCrossSellWidth IS NULL)
	AND ([ShowSwatchInCategory] = @ShowSwatchInCategory OR @ShowSwatchInCategory IS NULL)
	AND ([ShowAlternateImageInCategory] = @ShowAlternateImageInCategory OR @ShowAlternateImageInCategory IS NULL)
	AND ([ActiveInd] = @ActiveInd OR @ActiveInd IS NULL)
	AND ([SMTPServer] = @SMTPServer OR @SMTPServer IS NULL)
	AND ([SMTPUserName] = @SMTPUserName OR @SMTPUserName IS NULL)
	AND ([SMTPPassword] = @SMTPPassword OR @SMTPPassword IS NULL)
	AND ([SMTPPort] = @SMTPPort OR @SMTPPort IS NULL)
	AND ([UPSUserName] = @UPSUserName OR @UPSUserName IS NULL)
	AND ([UPSPassword] = @UPSPassword OR @UPSPassword IS NULL)
	AND ([UPSKey] = @UPSKey OR @UPSKey IS NULL)
	AND ([ShippingOriginZipCode] = @ShippingOriginZipCode OR @ShippingOriginZipCode IS NULL)
	AND ([MasterPage] = @MasterPage OR @MasterPage IS NULL)
	AND ([ShopByPriceMin] = @ShopByPriceMin OR @ShopByPriceMin IS NULL)
	AND ([ShopByPriceMax] = @ShopByPriceMax OR @ShopByPriceMax IS NULL)
	AND ([ShopByPriceIncrement] = @ShopByPriceIncrement OR @ShopByPriceIncrement IS NULL)
	AND ([FedExAccountNumber] = @FedExAccountNumber OR @FedExAccountNumber IS NULL)
	AND ([FedExMeterNumber] = @FedExMeterNumber OR @FedExMeterNumber IS NULL)
	AND ([FedExProductionKey] = @FedExProductionKey OR @FedExProductionKey IS NULL)
	AND ([FedExSecurityCode] = @FedExSecurityCode OR @FedExSecurityCode IS NULL)
	AND ([FedExCSPKey] = @FedExCSPKey OR @FedExCSPKey IS NULL)
	AND ([FedExCSPPassword] = @FedExCSPPassword OR @FedExCSPPassword IS NULL)
	AND ([FedExClientProductId] = @FedExClientProductId OR @FedExClientProductId IS NULL)
	AND ([FedExClientProductVersion] = @FedExClientProductVersion OR @FedExClientProductVersion IS NULL)
	AND ([FedExDropoffType] = @FedExDropoffType OR @FedExDropoffType IS NULL)
	AND ([FedExPackagingType] = @FedExPackagingType OR @FedExPackagingType IS NULL)
	AND ([FedExUseDiscountRate] = @FedExUseDiscountRate OR @FedExUseDiscountRate IS NULL)
	AND ([FedExAddInsurance] = @FedExAddInsurance OR @FedExAddInsurance IS NULL)
	AND ([ShippingOriginAddress1] = @ShippingOriginAddress1 OR @ShippingOriginAddress1 IS NULL)
	AND ([ShippingOriginAddress2] = @ShippingOriginAddress2 OR @ShippingOriginAddress2 IS NULL)
	AND ([ShippingOriginCity] = @ShippingOriginCity OR @ShippingOriginCity IS NULL)
	AND ([ShippingOriginStateCode] = @ShippingOriginStateCode OR @ShippingOriginStateCode IS NULL)
	AND ([ShippingOriginCountryCode] = @ShippingOriginCountryCode OR @ShippingOriginCountryCode IS NULL)
	AND ([ShippingOriginPhone] = @ShippingOriginPhone OR @ShippingOriginPhone IS NULL)
	AND ([CurrencyTypeID] = @CurrencyTypeID OR @CurrencyTypeID IS NULL)
	AND ([WeightUnit] = @WeightUnit OR @WeightUnit IS NULL)
	AND ([DimensionUnit] = @DimensionUnit OR @DimensionUnit IS NULL)
	AND ([EmailListLogin] = @EmailListLogin OR @EmailListLogin IS NULL)
	AND ([EmailListPassword] = @EmailListPassword OR @EmailListPassword IS NULL)
	AND ([EmailListDefaultList] = @EmailListDefaultList OR @EmailListDefaultList IS NULL)
	AND ([ShippingTaxable] = @ShippingTaxable OR @ShippingTaxable IS NULL)
	AND ([DefaultOrderStateID] = @DefaultOrderStateID OR @DefaultOrderStateID IS NULL)
	AND ([DefaultReviewStatus] = @DefaultReviewStatus OR @DefaultReviewStatus IS NULL)
	AND ([DefaultAnonymousProfileID] = @DefaultAnonymousProfileID OR @DefaultAnonymousProfileID IS NULL)
	AND ([DefaultRegisteredProfileID] = @DefaultRegisteredProfileID OR @DefaultRegisteredProfileID IS NULL)
	AND ([InclusiveTax] = @InclusiveTax OR @InclusiveTax IS NULL)
	AND ([SeoDefaultProductTitle] = @SeoDefaultProductTitle OR @SeoDefaultProductTitle IS NULL)
	AND ([SeoDefaultProductDescription] = @SeoDefaultProductDescription OR @SeoDefaultProductDescription IS NULL)
	AND ([SeoDefaultProductKeyword] = @SeoDefaultProductKeyword OR @SeoDefaultProductKeyword IS NULL)
	AND ([SeoDefaultCategoryTitle] = @SeoDefaultCategoryTitle OR @SeoDefaultCategoryTitle IS NULL)
	AND ([SeoDefaultCategoryDescription] = @SeoDefaultCategoryDescription OR @SeoDefaultCategoryDescription IS NULL)
	AND ([SeoDefaultCategoryKeyword] = @SeoDefaultCategoryKeyword OR @SeoDefaultCategoryKeyword IS NULL)
	AND ([SeoDefaultContentTitle] = @SeoDefaultContentTitle OR @SeoDefaultContentTitle IS NULL)
	AND ([SeoDefaultContentDescription] = @SeoDefaultContentDescription OR @SeoDefaultContentDescription IS NULL)
	AND ([SeoDefaultContentKeyword] = @SeoDefaultContentKeyword OR @SeoDefaultContentKeyword IS NULL)
	AND ([TimeZoneOffset] = @TimeZoneOffset OR @TimeZoneOffset IS NULL)
	AND ([LocaleID] = @LocaleID OR @LocaleID IS NULL)
	AND ([SplashCategoryID] = @SplashCategoryID OR @SplashCategoryID IS NULL)
	AND ([SplashImageFile] = @SplashImageFile OR @SplashImageFile IS NULL)
	AND ([MobileTheme] = @MobileTheme OR @MobileTheme IS NULL)
	AND ([PersistentCartEnabled] = @PersistentCartEnabled OR @PersistentCartEnabled IS NULL)
	AND ([DefaultProductReviewStateID] = @DefaultProductReviewStateID OR @DefaultProductReviewStateID IS NULL)
	AND ([UseDynamicDisplayOrder] = @UseDynamicDisplayOrder OR @UseDynamicDisplayOrder IS NULL)
	AND ([EnableAddressValidation] = @EnableAddressValidation OR @EnableAddressValidation IS NULL)
	AND ([RequireValidatedAddress] = @RequireValidatedAddress OR @RequireValidatedAddress IS NULL)
	AND ([EnablePIMS] = @EnablePIMS OR @EnablePIMS IS NULL)
	AND ([ExternalID] = @ExternalID OR @ExternalID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalID]
	, [CompanyName]
	, [StoreName]
	, [LogoPath]
	, [UseSSL]
	, [AdminEmail]
	, [SalesEmail]
	, [CustomerServiceEmail]
	, [SalesPhoneNumber]
	, [CustomerServicePhoneNumber]
	, [ImageNotAvailablePath]
	, [MaxCatalogDisplayColumns]
	, [MaxCatalogDisplayItems]
	, [MaxCatalogCategoryDisplayThumbnails]
	, [MaxCatalogItemSmallThumbnailWidth]
	, [MaxCatalogItemSmallWidth]
	, [MaxCatalogItemMediumWidth]
	, [MaxCatalogItemThumbnailWidth]
	, [MaxCatalogItemLargeWidth]
	, [MaxCatalogItemCrossSellWidth]
	, [ShowSwatchInCategory]
	, [ShowAlternateImageInCategory]
	, [ActiveInd]
	, [SMTPServer]
	, [SMTPUserName]
	, [SMTPPassword]
	, [SMTPPort]
	, [SiteWideBottomJavascript]
	, [SiteWideTopJavascript]
	, [OrderReceiptAffiliateJavascript]
	, [SiteWideAnalyticsJavascript]
	, [GoogleAnalyticsCode]
	, [UPSUserName]
	, [UPSPassword]
	, [UPSKey]
	, [ShippingOriginZipCode]
	, [MasterPage]
	, [ShopByPriceMin]
	, [ShopByPriceMax]
	, [ShopByPriceIncrement]
	, [FedExAccountNumber]
	, [FedExMeterNumber]
	, [FedExProductionKey]
	, [FedExSecurityCode]
	, [FedExCSPKey]
	, [FedExCSPPassword]
	, [FedExClientProductId]
	, [FedExClientProductVersion]
	, [FedExDropoffType]
	, [FedExPackagingType]
	, [FedExUseDiscountRate]
	, [FedExAddInsurance]
	, [ShippingOriginAddress1]
	, [ShippingOriginAddress2]
	, [ShippingOriginCity]
	, [ShippingOriginStateCode]
	, [ShippingOriginCountryCode]
	, [ShippingOriginPhone]
	, [CurrencyTypeID]
	, [WeightUnit]
	, [DimensionUnit]
	, [EmailListLogin]
	, [EmailListPassword]
	, [EmailListDefaultList]
	, [ShippingTaxable]
	, [DefaultOrderStateID]
	, [DefaultReviewStatus]
	, [DefaultAnonymousProfileID]
	, [DefaultRegisteredProfileID]
	, [InclusiveTax]
	, [SeoDefaultProductTitle]
	, [SeoDefaultProductDescription]
	, [SeoDefaultProductKeyword]
	, [SeoDefaultCategoryTitle]
	, [SeoDefaultCategoryDescription]
	, [SeoDefaultCategoryKeyword]
	, [SeoDefaultContentTitle]
	, [SeoDefaultContentDescription]
	, [SeoDefaultContentKeyword]
	, [TimeZoneOffset]
	, [LocaleID]
	, [SplashCategoryID]
	, [SplashImageFile]
	, [MobileTheme]
	, [PersistentCartEnabled]
	, [DefaultProductReviewStateID]
	, [UseDynamicDisplayOrder]
	, [EnableAddressValidation]
	, [RequireValidatedAddress]
	, [EnablePIMS]
	, [ExternalID]
    FROM
	[dbo].[ZNodePortal]
    WHERE 
	 ([PortalID] = @PortalID AND @PortalID is not null)
	OR ([CompanyName] = @CompanyName AND @CompanyName is not null)
	OR ([StoreName] = @StoreName AND @StoreName is not null)
	OR ([LogoPath] = @LogoPath AND @LogoPath is not null)
	OR ([UseSSL] = @UseSSL AND @UseSSL is not null)
	OR ([AdminEmail] = @AdminEmail AND @AdminEmail is not null)
	OR ([SalesEmail] = @SalesEmail AND @SalesEmail is not null)
	OR ([CustomerServiceEmail] = @CustomerServiceEmail AND @CustomerServiceEmail is not null)
	OR ([SalesPhoneNumber] = @SalesPhoneNumber AND @SalesPhoneNumber is not null)
	OR ([CustomerServicePhoneNumber] = @CustomerServicePhoneNumber AND @CustomerServicePhoneNumber is not null)
	OR ([ImageNotAvailablePath] = @ImageNotAvailablePath AND @ImageNotAvailablePath is not null)
	OR ([MaxCatalogDisplayColumns] = @MaxCatalogDisplayColumns AND @MaxCatalogDisplayColumns is not null)
	OR ([MaxCatalogDisplayItems] = @MaxCatalogDisplayItems AND @MaxCatalogDisplayItems is not null)
	OR ([MaxCatalogCategoryDisplayThumbnails] = @MaxCatalogCategoryDisplayThumbnails AND @MaxCatalogCategoryDisplayThumbnails is not null)
	OR ([MaxCatalogItemSmallThumbnailWidth] = @MaxCatalogItemSmallThumbnailWidth AND @MaxCatalogItemSmallThumbnailWidth is not null)
	OR ([MaxCatalogItemSmallWidth] = @MaxCatalogItemSmallWidth AND @MaxCatalogItemSmallWidth is not null)
	OR ([MaxCatalogItemMediumWidth] = @MaxCatalogItemMediumWidth AND @MaxCatalogItemMediumWidth is not null)
	OR ([MaxCatalogItemThumbnailWidth] = @MaxCatalogItemThumbnailWidth AND @MaxCatalogItemThumbnailWidth is not null)
	OR ([MaxCatalogItemLargeWidth] = @MaxCatalogItemLargeWidth AND @MaxCatalogItemLargeWidth is not null)
	OR ([MaxCatalogItemCrossSellWidth] = @MaxCatalogItemCrossSellWidth AND @MaxCatalogItemCrossSellWidth is not null)
	OR ([ShowSwatchInCategory] = @ShowSwatchInCategory AND @ShowSwatchInCategory is not null)
	OR ([ShowAlternateImageInCategory] = @ShowAlternateImageInCategory AND @ShowAlternateImageInCategory is not null)
	OR ([ActiveInd] = @ActiveInd AND @ActiveInd is not null)
	OR ([SMTPServer] = @SMTPServer AND @SMTPServer is not null)
	OR ([SMTPUserName] = @SMTPUserName AND @SMTPUserName is not null)
	OR ([SMTPPassword] = @SMTPPassword AND @SMTPPassword is not null)
	OR ([SMTPPort] = @SMTPPort AND @SMTPPort is not null)
	OR ([UPSUserName] = @UPSUserName AND @UPSUserName is not null)
	OR ([UPSPassword] = @UPSPassword AND @UPSPassword is not null)
	OR ([UPSKey] = @UPSKey AND @UPSKey is not null)
	OR ([ShippingOriginZipCode] = @ShippingOriginZipCode AND @ShippingOriginZipCode is not null)
	OR ([MasterPage] = @MasterPage AND @MasterPage is not null)
	OR ([ShopByPriceMin] = @ShopByPriceMin AND @ShopByPriceMin is not null)
	OR ([ShopByPriceMax] = @ShopByPriceMax AND @ShopByPriceMax is not null)
	OR ([ShopByPriceIncrement] = @ShopByPriceIncrement AND @ShopByPriceIncrement is not null)
	OR ([FedExAccountNumber] = @FedExAccountNumber AND @FedExAccountNumber is not null)
	OR ([FedExMeterNumber] = @FedExMeterNumber AND @FedExMeterNumber is not null)
	OR ([FedExProductionKey] = @FedExProductionKey AND @FedExProductionKey is not null)
	OR ([FedExSecurityCode] = @FedExSecurityCode AND @FedExSecurityCode is not null)
	OR ([FedExCSPKey] = @FedExCSPKey AND @FedExCSPKey is not null)
	OR ([FedExCSPPassword] = @FedExCSPPassword AND @FedExCSPPassword is not null)
	OR ([FedExClientProductId] = @FedExClientProductId AND @FedExClientProductId is not null)
	OR ([FedExClientProductVersion] = @FedExClientProductVersion AND @FedExClientProductVersion is not null)
	OR ([FedExDropoffType] = @FedExDropoffType AND @FedExDropoffType is not null)
	OR ([FedExPackagingType] = @FedExPackagingType AND @FedExPackagingType is not null)
	OR ([FedExUseDiscountRate] = @FedExUseDiscountRate AND @FedExUseDiscountRate is not null)
	OR ([FedExAddInsurance] = @FedExAddInsurance AND @FedExAddInsurance is not null)
	OR ([ShippingOriginAddress1] = @ShippingOriginAddress1 AND @ShippingOriginAddress1 is not null)
	OR ([ShippingOriginAddress2] = @ShippingOriginAddress2 AND @ShippingOriginAddress2 is not null)
	OR ([ShippingOriginCity] = @ShippingOriginCity AND @ShippingOriginCity is not null)
	OR ([ShippingOriginStateCode] = @ShippingOriginStateCode AND @ShippingOriginStateCode is not null)
	OR ([ShippingOriginCountryCode] = @ShippingOriginCountryCode AND @ShippingOriginCountryCode is not null)
	OR ([ShippingOriginPhone] = @ShippingOriginPhone AND @ShippingOriginPhone is not null)
	OR ([CurrencyTypeID] = @CurrencyTypeID AND @CurrencyTypeID is not null)
	OR ([WeightUnit] = @WeightUnit AND @WeightUnit is not null)
	OR ([DimensionUnit] = @DimensionUnit AND @DimensionUnit is not null)
	OR ([EmailListLogin] = @EmailListLogin AND @EmailListLogin is not null)
	OR ([EmailListPassword] = @EmailListPassword AND @EmailListPassword is not null)
	OR ([EmailListDefaultList] = @EmailListDefaultList AND @EmailListDefaultList is not null)
	OR ([ShippingTaxable] = @ShippingTaxable AND @ShippingTaxable is not null)
	OR ([DefaultOrderStateID] = @DefaultOrderStateID AND @DefaultOrderStateID is not null)
	OR ([DefaultReviewStatus] = @DefaultReviewStatus AND @DefaultReviewStatus is not null)
	OR ([DefaultAnonymousProfileID] = @DefaultAnonymousProfileID AND @DefaultAnonymousProfileID is not null)
	OR ([DefaultRegisteredProfileID] = @DefaultRegisteredProfileID AND @DefaultRegisteredProfileID is not null)
	OR ([InclusiveTax] = @InclusiveTax AND @InclusiveTax is not null)
	OR ([SeoDefaultProductTitle] = @SeoDefaultProductTitle AND @SeoDefaultProductTitle is not null)
	OR ([SeoDefaultProductDescription] = @SeoDefaultProductDescription AND @SeoDefaultProductDescription is not null)
	OR ([SeoDefaultProductKeyword] = @SeoDefaultProductKeyword AND @SeoDefaultProductKeyword is not null)
	OR ([SeoDefaultCategoryTitle] = @SeoDefaultCategoryTitle AND @SeoDefaultCategoryTitle is not null)
	OR ([SeoDefaultCategoryDescription] = @SeoDefaultCategoryDescription AND @SeoDefaultCategoryDescription is not null)
	OR ([SeoDefaultCategoryKeyword] = @SeoDefaultCategoryKeyword AND @SeoDefaultCategoryKeyword is not null)
	OR ([SeoDefaultContentTitle] = @SeoDefaultContentTitle AND @SeoDefaultContentTitle is not null)
	OR ([SeoDefaultContentDescription] = @SeoDefaultContentDescription AND @SeoDefaultContentDescription is not null)
	OR ([SeoDefaultContentKeyword] = @SeoDefaultContentKeyword AND @SeoDefaultContentKeyword is not null)
	OR ([TimeZoneOffset] = @TimeZoneOffset AND @TimeZoneOffset is not null)
	OR ([LocaleID] = @LocaleID AND @LocaleID is not null)
	OR ([SplashCategoryID] = @SplashCategoryID AND @SplashCategoryID is not null)
	OR ([SplashImageFile] = @SplashImageFile AND @SplashImageFile is not null)
	OR ([MobileTheme] = @MobileTheme AND @MobileTheme is not null)
	OR ([PersistentCartEnabled] = @PersistentCartEnabled AND @PersistentCartEnabled is not null)
	OR ([DefaultProductReviewStateID] = @DefaultProductReviewStateID AND @DefaultProductReviewStateID is not null)
	OR ([UseDynamicDisplayOrder] = @UseDynamicDisplayOrder AND @UseDynamicDisplayOrder is not null)
	OR ([EnableAddressValidation] = @EnableAddressValidation AND @EnableAddressValidation is not null)
	OR ([RequireValidatedAddress] = @RequireValidatedAddress AND @RequireValidatedAddress is not null)
	OR ([EnablePIMS] = @EnablePIMS AND @EnablePIMS is not null)
	OR ([ExternalID] = @ExternalID AND @ExternalID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_CopyCatalog]'
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[ZNode_CopyCatalog]   
 @CatalogID int,      
 @CatalogName nvarchar(max),      
 @LocaleID int = NULL       
AS      
BEGIN       
SET NOCOUNT ON;      
      
/* Insert a new Catalog Record */      
DECLARE @NewCatalogId int, @ErrCode int, @ErrMessage nvarchar(4000);      
    
INSERT INTO [ZNodeCatalog]        
           ([Name]      
           ,[IsActive])      
SELECT       
   @CatalogName,       
   IsActive      
FROM      
   ZNodeCatalog       
WHERE       
   CatalogId = @CatalogID;      
        
   SELECT @NewCatalogID =  scope_identity();       
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
  
  
/* Get the LocaleCode */  
DECLARE @LocaleCode VARCHAR(10) = NULL  
SET @LocaleCode = (SELECT LocaleCode FROM ZNodeLocale WHERE LocaleID = @LocaleID)  
  
         
/* Create copy of the PortalCatalog record, of existing catalog for new catalog */            
      
-- This block is commented because right now we are not using locale      
-- Comment Start      
/*       
INSERT INTO       
   [ZNodePortalCatalog]      
           ([PortalID]      
           ,[CatalogID]      
           ,[LocaleID]      
           ,[Theme]      
           ,[CSS])                 
SELECT       
   PortalID,       
   @NewCatalogId,       
   @LocaleID,       
   Theme,       
   CSS      
FROM      
   ZNodePortalCatalog ZP       
WHERE      
   ZP.CatalogID = @CatalogID;          
         
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
   */      
--Comment End      
-- PRINT @NewCatalogId      
/* Table variable to record the existing productid and link to the newly copied product */          
DECLARE  @ProductIDTable TABLE(      
   [KeyField] [int] IDENTITY(1,1),      
   [OldProductID] [int],      
   [NewProductID] [int]);      
         
INSERT INTO       
   @ProductIDTable ([OldProductID])      
SELECT      
   ProductID      
FROM      
   ZNodeProduct       
WHERE      
   ProductID IN      
   (SELECT      
    ProductID       
    FROM      
    ZNodeProductCategory       
    WHERE      
    CategoryID IN      
    (SELECT      
     CategoryID       
     FROM       
     ZNodeCategoryNode       
    WHERE       
     CatalogID = @CatalogID));      
      
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
      
/******************************************************************************      
 Iterate through the table variable created above, insert new copy product,       
 update table variable with new product id       
 *****************************************************************************/      
DECLARE  @i [int], @maxI [int], @newID [int]      
SET @i = 1;      
      
SELECT       
   @maxI = Max([KeyField])       
FROM       
   @ProductIDTable;      
      
WHILE  @i <= @maxI      
BEGIN      
   INSERT INTO [ZNodeProduct]      
           ([Name]      
           ,[ShortDescription]      
           ,[Description]      
           ,[FeaturesDesc]      
           ,[ProductNum]      
           ,[ProductTypeID]      
           ,[RetailPrice]      
           ,[SalePrice]      
           ,[WholesalePrice]      
           ,[ImageFile]      
           ,[ImageAltTag]      
           ,[Weight]      
           ,[Length]      
           ,[Width]      
           ,[Height]      
           ,[BeginActiveDate]      
           ,[EndActiveDate]      
           ,[DisplayOrder]      
           ,[ActiveInd]      
           ,[CallForPricing]      
           ,[HomepageSpecial]      
           ,[CategorySpecial]      
           ,[InventoryDisplay]      
           ,[Keywords]      
           ,[ManufacturerID]      
           ,[AdditionalInfoLink]      
           ,[AdditionalInfoLinkLabel]      
           ,[ShippingRuleTypeID]      
           ,[SEOTitle]      
           ,[SEOKeywords]      
           ,[SEODescription]      
           ,[Custom1]      
      ,[Custom2]      
           ,[Custom3]      
           ,[ShipEachItemSeparately]  
           ,[AllowBackOrder]      
           ,[BackOrderMsg]      
           ,[DropShipInd]      
           ,[DropShipEmailID]      
           ,[Specifications]      
           ,[AdditionalInformation]      
           ,[InStockMsg]      
           ,[OutOfStockMsg]      
           ,[TrackInventoryInd]      
           ,[DownloadLink]      
           ,[FreeShippingInd]      
           ,[NewProductInd]      
           ,[SEOURL]      
           ,[MaxQty]      
           ,[ShipSeparately]      
           ,[FeaturedInd]      
           ,[WebServiceDownloadDte]      
           ,[UpdateDte]      
           ,[SupplierID]      
           ,[RecurringBillingInd]      
           ,[RecurringBillingInstallmentInd]      
     ,[RecurringBillingPeriod]      
           ,[RecurringBillingFrequency]      
           ,[RecurringBillingTotalCycles]      
           ,[RecurringBillingInitialAmount]      
           ,[TaxClassID]  
           ,[ReviewStateID]  
        ,[AffiliateUrl]  
  ,[IsShippable]  
  ,[AccountID]  
  ,[PortalID]  
  ,[Franchisable]  
  ,[ExpirationPeriod]  
  ,[ExpirationFrequency])      
SELECT       
   [Name],       
   [ShortDescription],      
   [Description],      
   [FeaturesDesc],      
   [ProductNum],      
   [ProductTypeID],      
   [RetailPrice],      
   [SalePrice],      
   [WholesalePrice],      
   [ImageFile],      
   [ImageAltTag],      
   [Weight],      
   [Length],      
   [Width],      
   [Height],      
   [BeginActiveDate],      
   [EndActiveDate],      
   [DisplayOrder],      
   [ActiveInd],      
   [CallForPricing],      
   [HomepageSpecial],       
   [CategorySpecial],      
   [InventoryDisplay],      
   [Keywords],      
   [ManufacturerID],       
   [AdditionalInfoLink],       
   [AdditionalInfoLinkLabel],      
   [ShippingRuleTypeID],       
   [SEOTitle],      
   [SEOKeywords],      
   [SEODescription],      
   [Custom1],      
   [Custom2],       
   [Custom3],       
   [ShipEachItemSeparately],  
   [AllowBackOrder],      
   [BackOrderMsg],       
   [DropShipInd],      
   [DropShipEmailID],       
   [Specifications],      
   [AdditionalInformation],      
   [InStockMsg],      
   [OutOfStockMsg],      
   [TrackInventoryInd],      
   [DownloadLink],       
   [FreeShippingInd],       
   [NewProductInd],       
   null,       
   [MaxQty],      
   [ShipSeparately],       
   [FeaturedInd],       
   [WebServiceDownloadDte],       
   [UpdateDte],       
   [SupplierID],       
   [RecurringBillingInd],       
   [RecurringBillingInstallmentInd],       
   [RecurringBillingPeriod],      
   [RecurringBillingFrequency],      
   [RecurringBillingTotalCycles],  
   [RecurringBillingInitialAmount],       
   [TaxClassID],  
   [ReviewStateId]  
    ,[AffiliateUrl]  
 ,[IsShippable]  
 ,[AccountID]  
 ,[PortalID]  
 ,[Franchisable]  
 ,[ExpirationPeriod]  
 ,[ExpirationFrequency]  
FROM      
   ZNodeProduct P      
INNER JOIN      
   @ProductIDTable PT      
ON      
   P.ProductID = PT.OldProductID      
WHERE      
   PT.KeyField = @i;      
      
   SELECT @newID =  scope_identity()      
        
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
         
   UPDATE       
     @ProductIDTable      
   SET      
     NewProductID = @newID      
   WHERE      
     KeyField = @i;      
           
   SELECT @i = @i + 1;      
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
END      
            
         
/* Create table variable to record existing categoryid, linked to new categoryid*/         
DECLARE  @CategoryIDTable TABLE (      
   [KeyField] [int] IDENTITY(1,1),      
   [OldCategoryID] [int],      
   [NewCategoryID] [int]);      
         
INSERT INTO @CategoryIDTable      
           ([OldCategoryID])      
SELECT      
   [CategoryId]      
FROM      
   ZNodeCategory      
WHERE      
   CategoryID IN      
   (SELECT      
    CategoryID      
   FROM      
    ZNodeCategoryNode       
   WHERE      
    CatalogID = @CatalogID);        
        
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
          
      
/******************************************************************************      
 Iterate through the table variable created above, insert new copy category,       
 update table variable with new category id       
 *****************************************************************************/       
DECLARE  @intCounter int, @int int, @new int;      
SET @int = 1;      
      
SELECT       
   @intCounter = Max([KeyField])       
FROM       
   @CategoryIDTable;      
      
WHILE  @int <= @intCounter       
BEGIN      
  INSERT INTO [ZNodeCategory]      
           ([Name]      
           ,[Title]      
           ,[ShortDescription]      
           ,[Description]      
           ,[ImageFile]      
           ,[ImageAltTag]      
           ,[VisibleInd]      
           ,[SubCategoryGridVisibleInd]      
     ,[SEOTitle]      
           ,[SEOKeywords]      
           ,[SEODescription]      
           ,[AlternateDescription]      
           ,[DisplayOrder]      
           ,[Custom1]      
           ,[Custom2]      
           ,[Custom3]      
           ,[SEOURL])      
SELECT      
   [Name],      
   [Title],      
   [ShortDescription],      
   [Description],      
   [ImageFile],      
   [ImageAltTag],      
   [VisibleInd],      
   [SubCategoryGridVisibleInd],       
   [SEOTitle],      
   [SEOKeywords],      
   [SEODescription],      
   [AlternateDescription],      
   [DisplayOrder],      
   [Custom1],       
   [Custom2],       
   [Custom3],       
   null      
FROM      
   ZNodeCategory C      
INNER JOIN      
   @CategoryIDTable CT      
ON      
   CT.OldCategoryID = C.CategoryID      
WHERE      
   KeyField = @int;      
         
   SELECT @new =  scope_identity();      
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
          
   UPDATE      
     @CategoryIDTable      
   SET      
     NewCategoryID = @new      
   WHERE       
     KeyField = @int;      
       
   SELECT @int = @int + 1;      
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
END      
      
/*Insert newly created product and categories */         
INSERT INTO [ZNodeProductCategory]      
   ([ProductID]       
   ,[Theme]       
   ,[MasterPage]       
   ,[CSS]       
   ,[CategoryID]       
   ,[BeginDate]      
   ,[EndDate]      
   ,[DisplayOrder]      
   ,[ActiveInd])      
SELECT       
   PT.NewProductID,       
   ZP.Theme,       
   ZP.MasterPage,       
   ZP.CSS,       
   CT.NewCategoryID,       
   ZP.BeginDate,       
   ZP.EndDate,       
   ZP.DisplayOrder,       
   ZP.ActiveInd      
FROM      
   ZNodeProductCategory ZP   
INNER JOIN      
   @ProductIDTable PT       
ON      
   ZP.ProductID=  PT.OldProductID     
INNER JOIN      
   @CategoryIDTable CT       
ON       
   ZP.CategoryID =   CT.OldCategoryID    
    
      
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
          
      
/* Create table variable to record existing categoryid, linked to new categoryid*/         
DECLARE  @CategoryNodeIDTable TABLE (      
   [KeyField] [int] IDENTITY(1,1),      
   [OldID] [int],      
   [ParentID] [int],      
   [NewID] [int]);      
      
INSERT INTO @CategoryNodeIDTable      
           ([OldID],      
            [ParentID])      
SELECT      
   [CategoryNodeId],      
   [ParentCategoryNodeID]      
FROM      
   [ZNodeCategoryNode]      
WHERE          
   CatalogID = @CatalogID;       
         
            
DECLARE  @CNCount int, @CNIndex int, @CNNew int;      
SET @CNIndex = 1;      
      
SELECT       
   @CNCount = Max([KeyField])       
FROM       
   @CategoryNodeIDTable;      
      
WHILE  @CNIndex <= @CNCount      
BEGIN      
 INSERT INTO [ZNodeCategoryNode]      
    ([CatalogID]      
    ,[CategoryID]      
    ,[ParentCategoryNodeID]          
    ,[BeginDate]      
    ,[EndDate]      
    ,[Theme]      
    ,[MasterPage]      
    ,[CSS]      
    ,[DisplayOrder]      
    ,[ActiveInd])      
 SELECT       
    @NewCatalogID,       
    NewCategoryID,      
    ParentCategoryNodeID,      
    BeginDate,       
    EndDate,       
    Theme,       
    MasterPage,       
    CSS,       
    CN.DisplayOrder,       
    ActiveInd      
 FROM      
    ZnodeCategoryNode CN      
 INNER JOIN      
    @CategoryIDTable CT      
 ON      
    CT.OldCategoryID = CN.CategoryID      
 WHERE          
    CategoryNodeID = (SELECT OldId FROM @CategoryNodeIDTable WHERE KeyField = @CNIndex); --Added this line to avoid the duplicate      
      
 IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END       
          
 SELECT @CNNew =  scope_identity();      
        
 UPDATE @CategoryNodeIDTable       
 SET  [NewID] = @CNNew       
 WHERE KeyField = @CNIndex;      
       
       
 SELECT @CNIndex = @CNIndex + 1;      
             
END               
              
-- Update ParentCategoryNodeId with New CategoryNodeId      
UPDATE dbo.ZNodeCategoryNode      
SET  ParentCategoryNodeID = D.[NewID]      
FROM @CategoryNodeIDTable D       
WHERE ZNodeCategoryNode.ParentCategoryNodeID = D.OldID      
  AND ZNodeCategoryNode.[CatalogID] = @NewCatalogID;      
         
IF @@ERROR != 0       
BEGIN      
 SET @ErrMessage = ERROR_MESSAGE()      
 RETURN @ErrMessage      
END      
          
/* Table variable to record existing taggroupid's linked to new taggroupids */      
DECLARE  @TagGroupTable Table(      
   KeyField int IDENTITY(1,1),       
   OldTagGroupID int,       
   NewTagGroupID int);      
      
INSERT INTO      
   @TagGroupTable       
   (OldTagGroupID)      
SELECT       
   TG.TagGroupID      
FROM       
   ZNodeTagGroup TG      
WHERE      
   TG.TagGroupID IN      
   (SELECT       
     TagGroupID       
   FROM      
     ZNodeTagGroup TG      
   WHERE      
     TG.CatalogID = @CatalogID);      
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
          
/******************************************************************************      
 Iterate through the table variable created above, insert new copy tag groups,       
 update table variable with new taggroup id       
 *****************************************************************************/      
DECLARE  @intMaxID int, @NewIdentity int, @intCount int;      
SET @intCount = 1;      
      
SELECT       
   @intMaxID = MAX(KeyField)       
FROM      
   @TagGroupTable;      
      
WHILE  @intCount <= @intMaxID      
BEGIN      
      
 INSERT INTO ZNodeTagGroup       
    (TagGroupLabel,       
    CatalogID,      
    ControlTypeID)      
 SELECT       
    TagGroupLabel,       
    @NewCatalogID,      
    ControlTypeID      
 FROM       
    ZNodeTagGroup T      
 INNER JOIN      
    @TagGroupTable TT      
 ON      
    TT.OldTagGroupID = T.TagGroupID      
 WHERE      
    KeyField = @intCount;      
          
 SELECT  @newIdentity =  scope_identity();      
       
 IF @@ERROR != 0       
 BEGIN      
   SET @ErrMessage = ERROR_MESSAGE()      
   RETURN @ErrMessage      
 END      
         
 UPDATE      
    @TagGroupTable       
 SET       
    NewTagGroupID = @newIdentity      
 WHERE       
    KeyField = @intCount;      
          
 IF @@ERROR != 0       
  BEGIN      
   SET @ErrMessage = ERROR_MESSAGE()      
   RETURN @ErrMessage      
  END      
          
       
           
 SELECT @intCount = @intCount + 1;      
       
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
          
END      
      
      
-- Update Association      
INSERT INTO [ZNodeTagGroupCategory]      
     ([TagGroupID]      
     ,[CategoryID]      
     ,[CategoryDisplayOrder])      
 SELECT        
    T.NewTagGroupID,       
    C.NewCategoryID,       
    TG.CategoryDisplayOrder      
 FROM       
    ZNodeTagGroupCategory TG      
 INNER JOIN      
    @TagGroupTable T       
 ON      
    T.OldTagGroupID = TG.TagGroupID      
 INNER JOIN      
    @CategoryIDTable C      
 ON      
    C.OldCategoryID = TG.CategoryID      
      
      
/* Table variable to record existing tag ids linked to new tag id's */      
DECLARE  @TagTable Table(      
   [KeyField] int IDENTITY(1,1),       
   [NewTagID] int,       
   [OldTagID] int);      
      
      
INSERT INTO @TagTable       
   (OldTagID)      
SELECT        
   T.TagID      
FROM      
   ZnodeTag T      
WHERE      
   T.TagGroupID IN      
   (SELECT       
     TagGroupID      
   FROM      
     @TagGroupTable TG      
   INNER JOIN      
     ZNodeTag T      
   ON      
     TG.OldTagGroupID = T.TagGroupID);      
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
          
/******************************************************************************      
 Iterate through the table variable created above, insert new copy tags,       
 update table variable with new tag id       
 *****************************************************************************/      
DECLARE @intMID int, @nId int, @iCount int;      
SET @iCount = 1;      
      
SELECT @intMID = MAX(KeyField) From @TagTable;      
      
WHILE @iCount <= @intMID      
BEGIN      
  INSERT INTO ZNodeTag       
     (TagGroupId      
     ,TagName      
     ,TagDisplayOrder      
     ,IconPath)      
  SELECT       
     TG.NewTagGroupID,       
     T.TagName,       
     T.TagDisplayOrder,       
     IconPath      
  FROM       
     ZNodeTag T      
  INNER JOIN      
     @TagTable TT      
  ON      
   TT.OldTagID = T.TagId      
  INNER JOIN      
     @TagGroupTable TG      
  ON      
     T.TagGroupID = TG.OldTagGroupID      
  WHERE      
     TT.KeyField = @iCount;      
      
  SELECT  @nId =  scope_identity();      
        
  IF @@ERROR != 0       
  BEGIN      
   SET @ErrMessage = ERROR_MESSAGE()      
   RETURN @ErrMessage      
  END      
       
  UPDATE        
     @TagTable       
  SET         
     NewTagID = @nId      
  WHERE       
     KeyField = @iCount;      
      
  SELECT  @iCount = @iCount + 1;      
        
  IF @@ERROR != 0       
  BEGIN      
   SET @ErrMessage = ERROR_MESSAGE()      
   RETURN @ErrMessage      
  END      
          
END      
      
/* Insert new tag and product records in the znodetagproductsku table */         
INSERT INTO [ZNodeTagProductSKU]      
   ([TagID]      
   ,[ProductID])      
SELECT       
   T.NewTagID,       
   P.NewProductID      
FROM      
   @TagTable T      
INNER JOIN      
   ZNodeTagProductSKU TPS      
ON      
   TPS.TagID = T.OldTagID      
INNER JOIN      
   @ProductIDTable P      
ON      
   P.OldProductID = TPS.ProductID;      
      
         
   IF @@ERROR != 0       
   BEGIN      
    SET @ErrMessage = ERROR_MESSAGE()      
    RETURN @ErrMessage      
   END      
       
           
      
/* Variables for Iterations */      
DECLARE @recCount INT      
DECLARE @recIndex INT      
DECLARE @recID INT      
      
------------------------------      
-- Copy ZNodeHighlight records      
------------------------------      
/* Table variable to record existing ids linked to new id's */      
DECLARE  @HighlightTable Table(      
   [KeyField] int IDENTITY(1,1),       
   [NewID] int,       
   [OldID] int);      
      
INSERT INTO @HighlightTable       
   (OldID)      
SELECT        
   HighLightID      
FROM      
   ZNodeHighlight       
WHERE      
   HighLightID IN      
   (SELECT       
     PH.HighLightId      
   FROM      
     @ProductIDTable PT      
   INNER JOIN      
     ZNodeProductHighlight PH      
   ON      
     PT.OldProductID = PH.ProductID);            
      
-- Loop through each record and insert new highlight record and update generate id.      
SET @recCount = (SELECT MAX(KeyField) FROM @HighlightTable);      
SET @recIndex = 1      
      
WHILE (@recIndex <= @recCount)      
BEGIN      
       
 INSERT INTO ZNodeHighlight       
   ([ImageFile]      
      ,[ImageAltTag]      
      ,[Name]      
      ,[Description]      
      ,[DisplayPopup]      
      ,[Hyperlink]      
      ,[HyperlinkNewWinInd]      
      ,[HighlightTypeID]      
      ,[ActiveInd]      
      ,[DisplayOrder]      
      ,[ShortDescription]      
      ,[LocaleId])      
 SELECT       
       [ImageFile]      
      ,[ImageAltTag]      
      ,[Name]      
      ,[Description]      
      ,[DisplayPopup]      
      ,[Hyperlink]      
      ,[HyperlinkNewWinInd]      
      ,[HighlightTypeID]      
      ,[ActiveInd]      
      ,[DisplayOrder]      
      ,[ShortDescription]      
      ,COALESCE(@LocaleID, [LocaleId])           
 FROM ZNodeHighlight      
 WHERE HighlightID = (SELECT OldID FROM @HighlightTable WHERE KeyField=@recIndex);      
       
 UPDATE @HighlightTable      
 SET NewID =  scope_identity()      
 WHERE KeyField = @recIndex;       
       
 SET @recIndex = @recIndex + 1;      
       
END      
      
-- Insert new highlight association for new products.      
INSERT INTO ZNodeProductHighlight       
    ([ProductID]      
    ,[HighlightID]      
    ,[DisplayOrder])      
      SELECT      
     A.NewProductID      
    ,C.NewID      
    ,B.DisplayOrder       
   FROM @ProductIDTable A      
   INNER JOIN ZNodeProductHighlight B ON A.OldProductID = B.ProductID      
   INNER JOIN @HighlightTable C ON C.OldID = B.HighlightID;      
      
      
---------------------------      
-- Copy ZNodeAddOns records      
---------------------------      
/* Table variable to record existing ids linked to new id's */      
DECLARE  @AddOnTable Table(      
   [KeyField] int IDENTITY(1,1),       
   [NewID] int,       
   [OldID] int);      
      
INSERT INTO @AddOnTable       
   (OldID)      
SELECT        
   AddOnId      
FROM      
   ZNodeAddOn       
WHERE      
   AddOnId IN      
   (SELECT       
     B.AddOnId      
   FROM      
     @ProductIDTable A      
   INNER JOIN      
     ZNodeProductAddOn B      
   ON      
     A.OldProductID = B.ProductID);      
           
-- Loop through each record and insert new highlight record and update generate id.      
SET @recCount = (SELECT MAX(KeyField) FROM @AddOnTable);      
SET @recIndex = 1      
      
WHILE (@recIndex <= @recCount)      
BEGIN      
       
 -- Duplicate the ZNodeAddOn records.      
 INSERT INTO ZNodeAddOn       
   ([ProductID]      
      ,[Title]      
      ,[Name]      
      ,[Description]      
      ,[DisplayOrder]      
      ,[DisplayType]      
      ,[OptionalInd]      
      ,[AllowBackOrder]      
      ,[InStockMsg]      
      ,[OutOfStockMsg]      
      ,[BackOrderMsg]      
      ,[PromptMsg]      
      ,[TrackInventoryInd]      
      ,[LocaleId])      
 SELECT       
       [ProductID]      
      ,[Title]      
      ,[Name]      
      ,[Description]      
      ,[DisplayOrder]      
      ,[DisplayType]      
      ,[OptionalInd]      
      ,[AllowBackOrder]      
      ,[InStockMsg]      
      ,[OutOfStockMsg]      
      ,[BackOrderMsg]      
      ,[PromptMsg]      
      ,[TrackInventoryInd]      
      ,COALESCE(@LocaleID, [LocaleId])      
 FROM ZNodeAddOn      
 WHERE AddOnID = (SELECT OldID FROM @AddOnTable WHERE KeyField=@recIndex);      
       
 SET @recID =  scope_identity();      
          
 UPDATE @AddOnTable      
 SET NewID = @recID      
 WHERE KeyField = @recIndex;        
       
 -- Duplicate the ZNodeAddOnValue records based on new AddOn record.      
 INSERT INTO ZNodeAddOnValue      
    ([AddOnID]      
    ,[Name]      
    ,[Description]      
    ,[SKU]      
    ,[DefaultInd]      
    ,[DisplayOrder]      
    ,[ImageFile]      
    ,[ImageAltTag]      
    ,[RetailPrice]      
    ,[SalePrice]      
    ,[WholesalePrice]      
    ,[RecurringBillingInd]      
    ,[RecurringBillingInstallmentInd]      
    ,[RecurringBillingPeriod]      
    ,[RecurringBillingFrequency]      
    ,[RecurringBillingTotalCycles]      
    ,[RecurringBillingInitialAmount]      
    ,[Weight]      
    ,[Length]      
    ,[Height]      
    ,[Width]      
    ,[ShippingRuleTypeID]      
    ,[FreeShippingInd]      
    ,[WebServiceDownloadDte]      
    ,[UpdateDte]      
    ,[SupplierID]      
    ,[TaxClassID])      
 SELECT        
     @recID      
    ,[Name]      
    ,[Description]      
    ,[SKU]      
    ,[DefaultInd]      
    ,[DisplayOrder]      
    ,[ImageFile]      
    ,[ImageAltTag]      
    ,[RetailPrice]      
    ,[SalePrice]      
    ,[WholesalePrice]      
    ,[RecurringBillingInd]      
    ,[RecurringBillingInstallmentInd]      
    ,[RecurringBillingPeriod]      
    ,[RecurringBillingFrequency]      
    ,[RecurringBillingTotalCycles]      
    ,[RecurringBillingInitialAmount]      
    ,[Weight]      
    ,[Length]      
    ,[Height]      
    ,[Width]      
    ,[ShippingRuleTypeID]      
    ,[FreeShippingInd]      
    ,[WebServiceDownloadDte]      
    ,[UpdateDte]      
    ,[SupplierID]      
    ,[TaxClassID]      
 FROM ZNodeAddOnValue       
 WHERE AddOnID = (SELECT OldID FROM @AddOnTable WHERE KeyField=@recIndex);      
       
 SET @recIndex = @recIndex + 1;      
         
END      
      
-- Insert new AddOn association for new products.      
INSERT INTO ZNodeProductAddOn      
    ([ProductID]      
    ,AddOnID)      
      SELECT      
     A.NewProductID      
    ,C.NewID      
   FROM @ProductIDTable A      
   INNER JOIN ZNodeProductAddOn B ON A.OldProductID = B.ProductID      
   INNER JOIN @AddOnTable C ON C.OldID = B.AddOnID;         
              
                
-----------------------------------      
-- Copy ZNodeProductProfile records      
-----------------------------------      
-- Insert new Profile association for new products.      
INSERT INTO ZNodeProductProfile      
    ([ProductID]      
    ,ProfileID      
    ,IncludeInd)      
      SELECT      
    DISTINCT      
     A.NewProductID      
    ,B.ProfileID      
    ,B.IncludeInd      
   FROM @ProductIDTable A      
   INNER JOIN ZNodeProductProfile B ON A.OldProductID = B.ProductID      
           
           
--------------------------------      
-- Copy ZNodeProductTier records      
--------------------------------      
-- Insert new Tier association for new products.      
INSERT INTO ZNodeProductTier      
    ([ProductID]      
    ,[ProfileID]      
    ,[TierStart]      
    ,[TierEnd]      
    ,[Price])      
      SELECT      
      DISTINCT      
     A.NewProductID      
    ,B.ProfileID      
    ,B.TierStart      
    ,B.TierEnd      
    ,B.Price      
   FROM @ProductIDTable A      
   INNER JOIN ZNodeProductTier B ON A.OldProductID = B.ProductID      
         
         
-------------------------------------      
-- Copy ZNodeProductCrossSell records      
-------------------------------------      
-- Insert new CrossSell association for new products.      
INSERT INTO ZNodeProductCrossSell      
    ([ProductID]      
    ,[RelatedProductId]      
    ,[DisplayOrder])      
      SELECT      
      DISTINCT      
     A.NewProductID      
    ,C.NewProductID      
    ,B.DisplayOrder      
   FROM @ProductIDTable A      
   INNER JOIN ZNodeProductCrossSell B ON A.OldProductID = B.ProductID      
   INNER JOIN @ProductIDTable C ON B.RelatedProductId = C.OldProductID    
   WHERE A.NewProductID IS NOT NULL    
   AND C.NewProductID IS NOT NULL;    
         
         
---------------------------------      
-- Copy ZNodeProductImage records      
---------------------------------      
-- Insert new Image association for new products.      
INSERT INTO ZNodeProductImage      
    ([ProductID]      
    ,[Name]      
    ,[ImageFile]      
    ,[ImageAltTag]      
    ,[AlternateThumbnailImageFile]      
    ,[ActiveInd]      
    ,[ShowOnCategoryPage]      
    ,[ProductImageTypeID]      
    ,[DisplayOrder])      
      SELECT      
      DISTINCT      
     A.NewProductID          
    ,B.[Name]      
    ,B.[ImageFile]      
    ,B.[ImageAltTag]      
    ,B.[AlternateThumbnailImageFile]      
    ,B.[ActiveInd]      
    ,B.[ShowOnCategoryPage]      
    ,B.[ProductImageTypeID]      
    ,B.[DisplayOrder]      
   FROM @ProductIDTable A      
   INNER JOIN ZNodeProductImage B ON A.OldProductID = B.ProductID      
   WHERE A.NewProductID IS NOT NULL;       
                 
--------------------------------------------------------------------      
-- Copy ZNodeProductAttribute / ZNodeSKU / ZNodeSKUAttribute records      
--------------------------------------------------------------------      
      
-- Duplicate ZNodeAttributeType      
      
/* Table variable to record existing ids linked to new id's */      
DECLARE  @AttributeTypeTable Table(      
   [KeyField] int IDENTITY(1,1),       
   [NewID] int,       
   [OldID] int);      
      
      
INSERT INTO @AttributeTypeTable      
  (OldID)      
 SELECT      
  DISTINCT A.AttributeTypeId      
 FROM ZNodeAttributeType A      
 INNER JOIN ZNodeProductTypeAttribute B ON A.AttributeTypeId = B.AttributeTypeId      
 INNER JOIN ZNodeProductType C ON C.ProductTypeId = B.ProductTypeId      
 INNER JOIN ZNodeProduct D ON D.ProductTypeID = C.ProductTypeId      
 INNER JOIN @ProductIDTable E ON D.ProductID = E.OldProductID;      
      
-- Loop through each record and insert new record and update generate id.      
SET @recCount = (SELECT MAX(KeyField) FROM @AttributeTypeTable);      
SET @recIndex = 1      
      
WHILE (@recIndex <= @recCount)      
BEGIN      
      
 INSERT INTO ZNodeAttributeType       
   ([Name]      
   ,[Description]      
   ,[DisplayOrder]      
   ,[IsPrivate]      
   ,[LocaleId])      
 SELECT        
    [Name]      
      ,[Description]      
      ,[DisplayOrder]      
      ,[IsPrivate]      
      ,COALESCE(@LocaleID, [LocaleId])       
    FROM ZNodeAttributeType A      
    INNER JOIN @AttributeTypeTable B ON A.AttributeTypeId = B.OldID      
    WHERE B.KeyField = @recIndex;      
      
 SET @recID =  scope_identity();      
      
 UPDATE @AttributeTypeTable       
 SET NewID = @recID      
 WHERE KeyField = @recIndex;      
       
 SET @recIndex = @recIndex + 1;      
       
END      
      
-- Duplicate ZNodeProductAttribute      
      
/* Table variable to record existing ids linked to new id's */      
DECLARE  @ProductAttributeTable Table(      
   [KeyField] int IDENTITY(1,1),       
   [NewID] int,       
   [OldID] int);      
      
      
INSERT INTO @ProductAttributeTable      
  (OldID)      
 SELECT      
  DISTINCT A.AttributeId      
 FROM ZNodeProductAttribute A      
 INNER JOIN ZNodeSKUAttribute B ON A.AttributeId = B.AttributeId      
 INNER JOIN ZNodeSKU C ON C.SKUID = B.SKUID      
 INNER JOIN @ProductIDTable E ON C.ProductID = E.OldProductID;      
      
-- Loop through each record and insert new record and update generate id.      
SET @recCount = (SELECT MAX(KeyField) FROM @ProductAttributeTable);      
SET @recIndex = 1      
      
WHILE (@recIndex <= @recCount)      
BEGIN      
      
 INSERT INTO ZNodeProductAttribute       
   ([AttributeTypeId]      
      ,[Name]      
      ,[ExternalId]      
      ,[DisplayOrder]      
      ,[IsActive]      
      ,[OldAttributeId])      
 SELECT        
       C.NewID      
      ,[Name]      
      ,[ExternalId]      
      ,[DisplayOrder]      
      ,[IsActive]      
      ,[OldAttributeId]      
    FROM ZNodeProductAttribute A          
    INNER JOIN @ProductAttributeTable B ON A.AttributeId = B.OldID      
    INNER JOIN @AttributeTypeTable C ON A.AttributeTypeId = C.OldID      
    WHERE B.KeyField = @recIndex AND C.NewID IS NOT NULL;    
      
      
 SET @recID =  scope_identity();      
      
 UPDATE @ProductAttributeTable       
 SET NewID = @recID      
 WHERE KeyField = @recIndex;      
       
 SET @recIndex = @recIndex + 1;      
       
END      
      
          
-- Duplicate ZNodeSKU      
      
/* Table variable to record existing ids linked to new id's */      
DECLARE  @SkuTable Table(      
   [KeyField] int IDENTITY(1,1),       
   [NewID] int,       
   [OldID] int);      
      
      
INSERT INTO @SkuTable      
  (OldID)      
 SELECT      
  DISTINCT A.SKUID      
 FROM ZNodeSKU A       
 INNER JOIN @ProductIDTable E ON A.ProductID = E.OldProductID;      
      
-- Loop through each record and insert new record and update generate id.      
SET @recCount = (SELECT MAX(KeyField) FROM @SkuTable);      
SET @recIndex = 1      
      
WHILE (@recIndex <= @recCount)      
BEGIN      
      
 INSERT INTO ZNodeSKU       
   ([ProductID]      
      ,[SKU]      
      ,[SupplierID]      
      ,[Note]      
      ,[WeightAdditional]      
      ,[SKUPicturePath]      
      ,[ImageAltTag]      
      ,[DisplayOrder]      
      ,[RetailPriceOverride]      
      ,[SalePriceOverride]      
      ,[WholesalePriceOverride]      
      ,[RecurringBillingPeriod]      
      ,[RecurringBillingFrequency]      
      ,[RecurringBillingTotalCycles]      
      ,[RecurringBillingInitialAmount]      
      ,[ActiveInd]      
      ,[Custom1]      
      ,[Custom2]      
      ,[Custom3]      
      ,[WebServiceDownloadDte]      
      ,[UpdateDte])      
 SELECT        
    C.NewProductID      
      ,[SKU]      
      ,[SupplierID]      
      ,[Note]      
      ,[WeightAdditional]      
      ,[SKUPicturePath]      
      ,[ImageAltTag]      
      ,[DisplayOrder]      
      ,[RetailPriceOverride]      
      ,[SalePriceOverride]      
      ,[WholesalePriceOverride]      
      ,[RecurringBillingPeriod]      
      ,[RecurringBillingFrequency]      
      ,[RecurringBillingTotalCycles]      
      ,[RecurringBillingInitialAmount]      
      ,[ActiveInd]      
      ,[Custom1]      
      ,[Custom2]      
      ,[Custom3]      
      ,[WebServiceDownloadDte]      
      ,[UpdateDte]      
    FROM ZNodeSKU A          
    INNER JOIN @SkuTable B ON A.SKUID = B.OldID      
    INNER JOIN @ProductIDTable C ON A.ProductID = C.OldProductID      
    WHERE B.KeyField = @recIndex            
    AND C.NewProductID IS NOT NULL;    
      
 SET @recID =  scope_identity();      
      
 UPDATE @SkuTable       
 SET NewID = @recID      
 WHERE KeyField = @recIndex;      
       
 SET @recIndex = @recIndex + 1;      
       
END              
      
-- Duplicate ZNodeSKUAttribute      
      
INSERT INTO ZNodeSKUAttribute       
 ([SKUID]      
 ,[AttributeId])      
SELECT      
 DISTINCT      
  B.NewID      
 ,C.NewID      
FROM ZNodeSKUAttribute A      
INNER JOIN @SkuTable B ON A.SKUID = B.OldID      
INNER JOIN @ProductAttributeTable C ON A.AttributeId = C.OldID                 
WHERE C.NEWID IS NOT NULL AND B.NewID IS NOT NULL;      
      
-- Duplicate ZNodeProductType      
      
/* Table variable to record existing ids linked to new id's */      
DECLARE  @ProductTypeTable Table(      
   [KeyField] int IDENTITY(1,1),       
   [NewID] int,       
   [OldID] int);      
      
      
INSERT INTO @ProductTypeTable      
  (OldID)      
 SELECT      
  DISTINCT A.ProductTypeId      
 FROM ZNodeProductType A      
 INNER JOIN ZNodeProduct C ON C.ProductTypeID = A.ProductTypeID      
 INNER JOIN @ProductIDTable E ON C.ProductID = E.OldProductID;      
      
-- Loop through each record and insert new record and update generate id.      
SET @recCount = (SELECT MAX(KeyField) FROM @ProductTypeTable);      
SET @recIndex = 1      
      
WHILE (@recIndex <= @recCount)      
BEGIN      
  
IF (EXISTS(SELECT [Name] FROM ZNodeProductType   
   WHERE [Name] IN (  
     SELECT        
      [Name] + ISNULL(' - ' + (CASE WHEN ISNULL(A.IsGiftCard, 0) != 1 THEN @LocaleCode ELSE NULL END), '')  
     FROM ZNodeProductType A          
     INNER JOIN @ProductTypeTable B ON A.ProductTypeId = B.OldID  
     WHERE B.KeyField = @recIndex)))  
BEGIN  
  
 SET @recID = (SELECT TOP 1 ProductTypeID FROM ZNodeProductType   
     WHERE [Name] IN (SELECT [Name] + ISNULL(' - ' + (CASE WHEN ISNULL(A.IsGiftCard, 0) != 1 THEN @LocaleCode ELSE NULL END), '')  
          FROM ZNodeProductType A          
          INNER JOIN @ProductTypeTable B ON A.ProductTypeId = B.OldID   
          WHERE B.KeyField = @recIndex));      
END  
ELSE  
BEGIN             
 INSERT INTO ZNodeProductType      
      ([Name]       
      ,[Description]      
      ,[DisplayOrder])      
 SELECT        
       [Name] + ISNULL(' - ' + @LocaleCode, '')  
      ,[Description]      
      ,[DisplayOrder]      
    FROM ZNodeProductType A          
    INNER JOIN @ProductTypeTable B ON A.ProductTypeId = B.OldID      
    WHERE B.KeyField = @recIndex;      
      
 SET @recID =  scope_identity();      
END      
 UPDATE @ProductTypeTable       
 SET NewID = @recID      
 WHERE KeyField = @recIndex;      
       
 SET @recIndex = @recIndex + 1;      
       
END      
      
-- Duplicate ZNodeProductTypeAttribute      
INSERT INTO ZNodeProductTypeAttribute       
 (ProductTypeId      
 ,[AttributeTypeId])      
SELECT      
  B.NewID      
 ,C.NewID      
FROM ZNodeProductTypeAttribute A      
INNER JOIN @ProductTypeTable B ON A.ProductTypeId = B.OldID      
INNER JOIN @AttributeTypeTable C ON A.AttributeTypeId = C.OldID                
WHERE C.NEWID IS NOT NULL AND B.NewID IS NOT NULL;      
       
-- Update ZNodeProduct table with new ProductTypeId for new Products      
      
UPDATE ZNodeProduct      
SET ProductTypeID = B.NewID      
FROM @ProductTypeTable B      
INNER JOIN ZNodeProduct C ON C.ProductTypeID = B.OldID      
INNER JOIN @ProductIDTable D ON C.ProductID = D.NewProductID      
WHERE ProductID = C.ProductID AND B.NewID IS NOT NULL;      
      
END  -- end tag for BEGIN (root)      
                 
  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexServerStatus_Delete]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeLuceneIndexServerStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexServerStatus_Delete
(

	@ID int   
)
AS


				DELETE FROM [dbo].[ZNodeLuceneIndexServerStatus] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexServerStatus_GetByLuceneIndexMonitorID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneIndexServerStatus table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexServerStatus_GetByLuceneIndexMonitorID
(

	@LuceneIndexMonitorID bigint   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ID],
					[ServerName],
					[LuceneIndexMonitorID],
					[Status],
					[StartTime],
					[EndTime]
				FROM
					[dbo].[ZNodeLuceneIndexServerStatus]
				WHERE
					[LuceneIndexMonitorID] = @LuceneIndexMonitorID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Znode_UpsertZNodeLuceneServerConfigurationStatus]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Znode_UpsertZNodeLuceneServerConfigurationStatus]
	@ServerName nvarchar(50),
	@IP_Address nvarchar(50),
	@Status bit
AS
BEGIN
	IF EXISTS(SELECT 1 FROM ZNodeLuceneServerConfigurationStatus 
		WHERE ServerName = @ServerName)
	BEGIN
		UPDATE ZNodeLuceneServerConfigurationStatus 
            SET Status = @Status
            WHERE ServerName = @ServerName;            
	END
	ELSE
	BEGIN
		INSERT INTO ZNodeLuceneServerConfigurationStatus (ServerName, IP_Address, Status)
            VALUES (@ServerName, @IP_Address, @Status)	
	END
	
	--Error handling	
	IF @@ERROR > 0
		RETURN 0;  --SP failed
	ELSE
		RETURN 1;  --SP ran successfully
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexServerStatus_GetByID]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneIndexServerStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexServerStatus_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[ServerName],
					[LuceneIndexMonitorID],
					[Status],
					[StartTime],
					[EndTime]
				FROM
					[dbo].[ZNodeLuceneIndexServerStatus]
				WHERE
					[ID] = @ID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexServerStatus_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeLuceneIndexServerStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexServerStatus_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@ServerName nvarchar (50)  = null ,

	@LuceneIndexMonitorID bigint   = null ,

	@Status int   = null ,

	@StartTime datetime   = null ,

	@EndTime datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [ServerName]
	, [LuceneIndexMonitorID]
	, [Status]
	, [StartTime]
	, [EndTime]
    FROM
	[dbo].[ZNodeLuceneIndexServerStatus]
    WHERE 
	 ([ID] = @ID OR @ID IS NULL)
	AND ([ServerName] = @ServerName OR @ServerName IS NULL)
	AND ([LuceneIndexMonitorID] = @LuceneIndexMonitorID OR @LuceneIndexMonitorID IS NULL)
	AND ([Status] = @Status OR @Status IS NULL)
	AND ([StartTime] = @StartTime OR @StartTime IS NULL)
	AND ([EndTime] = @EndTime OR @EndTime IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [ServerName]
	, [LuceneIndexMonitorID]
	, [Status]
	, [StartTime]
	, [EndTime]
    FROM
	[dbo].[ZNodeLuceneIndexServerStatus]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([ServerName] = @ServerName AND @ServerName is not null)
	OR ([LuceneIndexMonitorID] = @LuceneIndexMonitorID AND @LuceneIndexMonitorID is not null)
	OR ([Status] = @Status AND @Status is not null)
	OR ([StartTime] = @StartTime AND @StartTime is not null)
	OR ([EndTime] = @EndTime AND @EndTime is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_SearchVendorProduct]'
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[ZNode_SearchVendorProduct]  
@ProductName VARCHAR (MAX)='',   
@ProductId VARCHAR (MAX)='',   
@Vendor VARCHAR (MAX)='',   
@VendorId VARCHAR (MAX)='',   
@ProductStatus VARCHAR (MAX)='',   
@COId VARCHAR (MAX)=''  
AS  
BEGIN  
 DECLARE @Query NVARCHAR(3000)  
 DECLARE @WhereClause NVARCHAR(1000)  
   
    SET @Query='SELECT P.ProductID,  
           ImageFile,  
           P.Name,  
           P.ProductNum,  
           RetailPrice,  
           SalePrice,  
           P.DisplayOrder,  
           P.ActiveInd,  
          Case When P.AccountId>0 then
             
           ( Select  top 1 ZPT.PortalId from ZnodePortalCatalog ZPT,znodecategorynode ZCN, ZNODEPRODUCTCATEGORY ZPC  
       Where ZPT.CatalogId=ZCN.CatalogId and  
       ZCN.categoryId=ZPC.CAtegoryId and ZPC.ProductID=P.ProductID)
       
       else P.PortalID END As PortalID,            
           
           (FirstName + '' ''+ LastName + '' '' + CompanyName) AS Vendor,  
           (SELECT TOP 1 QuantityOnHand  
            FROM   ZNodeSKUInventory AS SkuInventory  
            WHERE  SkuInventory.SKU IN (SELECT SKU  
                                        FROM   ZNodeSKU  
                                        WHERE  ProductID = P.ProductID)) AS ''QuantityOnHand'',  
           P.ReviewStateId,  
           R.ReviewStateName AS ProductStatus  
           FROM     
   ZNodeProduct AS P LEFT OUTER JOIN ZNodeProductReviewState R ON P.ReviewStateID=R.ReviewStateID   
   LEFT OUTER JOIN ZNodeAddress A ON P.AccountID=A.AccountID AND A.IsDefaultBilling=1   '  
      
    SET @WhereClause='';  
    IF(LEN(@ProductName)>0)  
  BEGIN  
       SET @ProductName = '%' + @ProductName + '%'  
   SET @WhereClause=@WhereClause+' AND P.Name LIKE @ProductName'  
  
  END  
 IF(LEN(@ProductId)>0)  
  BEGIN   
   SET @WhereClause=@WhereClause+' AND P.ProductNum = @ProductId';  
  END  
      
    IF(LEN(@ProductStatus)>0)  
  BEGIN   
   SET @WhereClause=@WhereClause+' AND P.ReviewStateId = @ProductStatus';  
  END  
 IF(LEN(@VendorId)>0)  
  BEGIN   
   SET @WhereClause=@WhereClause+' AND P.AccountId = @VendorId';  
  END    
    
 IF(LEN(@Vendor)>0)  
  BEGIN   
   SET @Vendor='%'+ @Vendor + '%'  
   SET @WhereClause=@WhereClause+' AND P.AccountId IN   
     (SELECT   
      AccountID   
     FROM   
      ZNodeAddress   
     WHERE FirstName LIKE @Vendor OR LastName LIKE @Vendor)';  
  END    
    IF(LEN(@COId)>0)  
  BEGIN   
   SET @WhereClause = @WhereClause+' AND P.ProductID IN (SELECT ProductID FROM ZNodeSKU WHERE SKU = @COId)'  
  END  
       
       
     IF(RTRIM(LTRIM(@WhereClause))<>'')  
  BEGIN  
  -- Replace the left most AND with WHERE clause.  
   SET @WhereClause=' WHERE '+ SUBSTRING(LTRIM(@WhereClause), 4, LEN(@WhereClause));  
  END  
       
     SET @Query =@Query + @WhereClause;       
       
     EXEC SP_EXECUTESQL @Query, N'@ProductName nvarchar(max),@ProductId varchar(max),@Vendor nvarchar(max),@VendorId varchar(max),@ProductStatus INT,@COId varchar(max)',  
    @ProductName = @ProductName,@ProductId = @ProductId,@Vendor = @Vendor,@VendorId = @VendorId,@ProductStatus = @ProductStatus,@COId = @COId;   
             
END  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePortal_GetPaged]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodePortal table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePortal_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[PortalID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [PortalID]'
				SET @SQL = @SQL + ', [CompanyName]'
				SET @SQL = @SQL + ', [StoreName]'
				SET @SQL = @SQL + ', [LogoPath]'
				SET @SQL = @SQL + ', [UseSSL]'
				SET @SQL = @SQL + ', [AdminEmail]'
				SET @SQL = @SQL + ', [SalesEmail]'
				SET @SQL = @SQL + ', [CustomerServiceEmail]'
				SET @SQL = @SQL + ', [SalesPhoneNumber]'
				SET @SQL = @SQL + ', [CustomerServicePhoneNumber]'
				SET @SQL = @SQL + ', [ImageNotAvailablePath]'
				SET @SQL = @SQL + ', [MaxCatalogDisplayColumns]'
				SET @SQL = @SQL + ', [MaxCatalogDisplayItems]'
				SET @SQL = @SQL + ', [MaxCatalogCategoryDisplayThumbnails]'
				SET @SQL = @SQL + ', [MaxCatalogItemSmallThumbnailWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemSmallWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemMediumWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemThumbnailWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemLargeWidth]'
				SET @SQL = @SQL + ', [MaxCatalogItemCrossSellWidth]'
				SET @SQL = @SQL + ', [ShowSwatchInCategory]'
				SET @SQL = @SQL + ', [ShowAlternateImageInCategory]'
				SET @SQL = @SQL + ', [ActiveInd]'
				SET @SQL = @SQL + ', [SMTPServer]'
				SET @SQL = @SQL + ', [SMTPUserName]'
				SET @SQL = @SQL + ', [SMTPPassword]'
				SET @SQL = @SQL + ', [SMTPPort]'
				SET @SQL = @SQL + ', [SiteWideBottomJavascript]'
				SET @SQL = @SQL + ', [SiteWideTopJavascript]'
				SET @SQL = @SQL + ', [OrderReceiptAffiliateJavascript]'
				SET @SQL = @SQL + ', [SiteWideAnalyticsJavascript]'
				SET @SQL = @SQL + ', [GoogleAnalyticsCode]'
				SET @SQL = @SQL + ', [UPSUserName]'
				SET @SQL = @SQL + ', [UPSPassword]'
				SET @SQL = @SQL + ', [UPSKey]'
				SET @SQL = @SQL + ', [ShippingOriginZipCode]'
				SET @SQL = @SQL + ', [MasterPage]'
				SET @SQL = @SQL + ', [ShopByPriceMin]'
				SET @SQL = @SQL + ', [ShopByPriceMax]'
				SET @SQL = @SQL + ', [ShopByPriceIncrement]'
				SET @SQL = @SQL + ', [FedExAccountNumber]'
				SET @SQL = @SQL + ', [FedExMeterNumber]'
				SET @SQL = @SQL + ', [FedExProductionKey]'
				SET @SQL = @SQL + ', [FedExSecurityCode]'
				SET @SQL = @SQL + ', [FedExCSPKey]'
				SET @SQL = @SQL + ', [FedExCSPPassword]'
				SET @SQL = @SQL + ', [FedExClientProductId]'
				SET @SQL = @SQL + ', [FedExClientProductVersion]'
				SET @SQL = @SQL + ', [FedExDropoffType]'
				SET @SQL = @SQL + ', [FedExPackagingType]'
				SET @SQL = @SQL + ', [FedExUseDiscountRate]'
				SET @SQL = @SQL + ', [FedExAddInsurance]'
				SET @SQL = @SQL + ', [ShippingOriginAddress1]'
				SET @SQL = @SQL + ', [ShippingOriginAddress2]'
				SET @SQL = @SQL + ', [ShippingOriginCity]'
				SET @SQL = @SQL + ', [ShippingOriginStateCode]'
				SET @SQL = @SQL + ', [ShippingOriginCountryCode]'
				SET @SQL = @SQL + ', [ShippingOriginPhone]'
				SET @SQL = @SQL + ', [CurrencyTypeID]'
				SET @SQL = @SQL + ', [WeightUnit]'
				SET @SQL = @SQL + ', [DimensionUnit]'
				SET @SQL = @SQL + ', [EmailListLogin]'
				SET @SQL = @SQL + ', [EmailListPassword]'
				SET @SQL = @SQL + ', [EmailListDefaultList]'
				SET @SQL = @SQL + ', [ShippingTaxable]'
				SET @SQL = @SQL + ', [DefaultOrderStateID]'
				SET @SQL = @SQL + ', [DefaultReviewStatus]'
				SET @SQL = @SQL + ', [DefaultAnonymousProfileID]'
				SET @SQL = @SQL + ', [DefaultRegisteredProfileID]'
				SET @SQL = @SQL + ', [InclusiveTax]'
				SET @SQL = @SQL + ', [SeoDefaultProductTitle]'
				SET @SQL = @SQL + ', [SeoDefaultProductDescription]'
				SET @SQL = @SQL + ', [SeoDefaultProductKeyword]'
				SET @SQL = @SQL + ', [SeoDefaultCategoryTitle]'
				SET @SQL = @SQL + ', [SeoDefaultCategoryDescription]'
				SET @SQL = @SQL + ', [SeoDefaultCategoryKeyword]'
				SET @SQL = @SQL + ', [SeoDefaultContentTitle]'
				SET @SQL = @SQL + ', [SeoDefaultContentDescription]'
				SET @SQL = @SQL + ', [SeoDefaultContentKeyword]'
				SET @SQL = @SQL + ', [TimeZoneOffset]'
				SET @SQL = @SQL + ', [LocaleID]'
				SET @SQL = @SQL + ', [SplashCategoryID]'
				SET @SQL = @SQL + ', [SplashImageFile]'
				SET @SQL = @SQL + ', [MobileTheme]'
				SET @SQL = @SQL + ', [PersistentCartEnabled]'
				SET @SQL = @SQL + ', [DefaultProductReviewStateID]'
				SET @SQL = @SQL + ', [UseDynamicDisplayOrder]'
				SET @SQL = @SQL + ', [EnableAddressValidation]'
				SET @SQL = @SQL + ', [RequireValidatedAddress]'
				SET @SQL = @SQL + ', [EnablePIMS]'
				SET @SQL = @SQL + ', [ExternalID]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePortal]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [PortalID],'
				SET @SQL = @SQL + ' [CompanyName],'
				SET @SQL = @SQL + ' [StoreName],'
				SET @SQL = @SQL + ' [LogoPath],'
				SET @SQL = @SQL + ' [UseSSL],'
				SET @SQL = @SQL + ' [AdminEmail],'
				SET @SQL = @SQL + ' [SalesEmail],'
				SET @SQL = @SQL + ' [CustomerServiceEmail],'
				SET @SQL = @SQL + ' [SalesPhoneNumber],'
				SET @SQL = @SQL + ' [CustomerServicePhoneNumber],'
				SET @SQL = @SQL + ' [ImageNotAvailablePath],'
				SET @SQL = @SQL + ' [MaxCatalogDisplayColumns],'
				SET @SQL = @SQL + ' [MaxCatalogDisplayItems],'
				SET @SQL = @SQL + ' [MaxCatalogCategoryDisplayThumbnails],'
				SET @SQL = @SQL + ' [MaxCatalogItemSmallThumbnailWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemSmallWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemMediumWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemThumbnailWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemLargeWidth],'
				SET @SQL = @SQL + ' [MaxCatalogItemCrossSellWidth],'
				SET @SQL = @SQL + ' [ShowSwatchInCategory],'
				SET @SQL = @SQL + ' [ShowAlternateImageInCategory],'
				SET @SQL = @SQL + ' [ActiveInd],'
				SET @SQL = @SQL + ' [SMTPServer],'
				SET @SQL = @SQL + ' [SMTPUserName],'
				SET @SQL = @SQL + ' [SMTPPassword],'
				SET @SQL = @SQL + ' [SMTPPort],'
				SET @SQL = @SQL + ' [SiteWideBottomJavascript],'
				SET @SQL = @SQL + ' [SiteWideTopJavascript],'
				SET @SQL = @SQL + ' [OrderReceiptAffiliateJavascript],'
				SET @SQL = @SQL + ' [SiteWideAnalyticsJavascript],'
				SET @SQL = @SQL + ' [GoogleAnalyticsCode],'
				SET @SQL = @SQL + ' [UPSUserName],'
				SET @SQL = @SQL + ' [UPSPassword],'
				SET @SQL = @SQL + ' [UPSKey],'
				SET @SQL = @SQL + ' [ShippingOriginZipCode],'
				SET @SQL = @SQL + ' [MasterPage],'
				SET @SQL = @SQL + ' [ShopByPriceMin],'
				SET @SQL = @SQL + ' [ShopByPriceMax],'
				SET @SQL = @SQL + ' [ShopByPriceIncrement],'
				SET @SQL = @SQL + ' [FedExAccountNumber],'
				SET @SQL = @SQL + ' [FedExMeterNumber],'
				SET @SQL = @SQL + ' [FedExProductionKey],'
				SET @SQL = @SQL + ' [FedExSecurityCode],'
				SET @SQL = @SQL + ' [FedExCSPKey],'
				SET @SQL = @SQL + ' [FedExCSPPassword],'
				SET @SQL = @SQL + ' [FedExClientProductId],'
				SET @SQL = @SQL + ' [FedExClientProductVersion],'
				SET @SQL = @SQL + ' [FedExDropoffType],'
				SET @SQL = @SQL + ' [FedExPackagingType],'
				SET @SQL = @SQL + ' [FedExUseDiscountRate],'
				SET @SQL = @SQL + ' [FedExAddInsurance],'
				SET @SQL = @SQL + ' [ShippingOriginAddress1],'
				SET @SQL = @SQL + ' [ShippingOriginAddress2],'
				SET @SQL = @SQL + ' [ShippingOriginCity],'
				SET @SQL = @SQL + ' [ShippingOriginStateCode],'
				SET @SQL = @SQL + ' [ShippingOriginCountryCode],'
				SET @SQL = @SQL + ' [ShippingOriginPhone],'
				SET @SQL = @SQL + ' [CurrencyTypeID],'
				SET @SQL = @SQL + ' [WeightUnit],'
				SET @SQL = @SQL + ' [DimensionUnit],'
				SET @SQL = @SQL + ' [EmailListLogin],'
				SET @SQL = @SQL + ' [EmailListPassword],'
				SET @SQL = @SQL + ' [EmailListDefaultList],'
				SET @SQL = @SQL + ' [ShippingTaxable],'
				SET @SQL = @SQL + ' [DefaultOrderStateID],'
				SET @SQL = @SQL + ' [DefaultReviewStatus],'
				SET @SQL = @SQL + ' [DefaultAnonymousProfileID],'
				SET @SQL = @SQL + ' [DefaultRegisteredProfileID],'
				SET @SQL = @SQL + ' [InclusiveTax],'
				SET @SQL = @SQL + ' [SeoDefaultProductTitle],'
				SET @SQL = @SQL + ' [SeoDefaultProductDescription],'
				SET @SQL = @SQL + ' [SeoDefaultProductKeyword],'
				SET @SQL = @SQL + ' [SeoDefaultCategoryTitle],'
				SET @SQL = @SQL + ' [SeoDefaultCategoryDescription],'
				SET @SQL = @SQL + ' [SeoDefaultCategoryKeyword],'
				SET @SQL = @SQL + ' [SeoDefaultContentTitle],'
				SET @SQL = @SQL + ' [SeoDefaultContentDescription],'
				SET @SQL = @SQL + ' [SeoDefaultContentKeyword],'
				SET @SQL = @SQL + ' [TimeZoneOffset],'
				SET @SQL = @SQL + ' [LocaleID],'
				SET @SQL = @SQL + ' [SplashCategoryID],'
				SET @SQL = @SQL + ' [SplashImageFile],'
				SET @SQL = @SQL + ' [MobileTheme],'
				SET @SQL = @SQL + ' [PersistentCartEnabled],'
				SET @SQL = @SQL + ' [DefaultProductReviewStateID],'
				SET @SQL = @SQL + ' [UseDynamicDisplayOrder],'
				SET @SQL = @SQL + ' [EnableAddressValidation],'
				SET @SQL = @SQL + ' [RequireValidatedAddress],'
				SET @SQL = @SQL + ' [EnablePIMS],'
				SET @SQL = @SQL + ' [ExternalID]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePortal]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeCatalog_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeCatalog table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeCatalog_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[CatalogID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [CatalogID]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ', [IsActive]'
				SET @SQL = @SQL + ', [PortalID]'
				SET @SQL = @SQL + ', [ExternalID]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeCatalog]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [CatalogID],'
				SET @SQL = @SQL + ' [Name],'
				SET @SQL = @SQL + ' [IsActive],'
				SET @SQL = @SQL + ' [PortalID],'
				SET @SQL = @SQL + ' [ExternalID]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeCatalog]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodeAddOnValue_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeAddOnValue table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodeAddOnValue_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[AddOnValueID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [AddOnValueID]'
				SET @SQL = @SQL + ', [AddOnID]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ', [Description]'
				SET @SQL = @SQL + ', [SKU]'
				SET @SQL = @SQL + ', [DefaultInd]'
				SET @SQL = @SQL + ', [DisplayOrder]'
				SET @SQL = @SQL + ', [ImageFile]'
				SET @SQL = @SQL + ', [ImageAltTag]'
				SET @SQL = @SQL + ', [RetailPrice]'
				SET @SQL = @SQL + ', [SalePrice]'
				SET @SQL = @SQL + ', [WholesalePrice]'
				SET @SQL = @SQL + ', [RecurringBillingInd]'
				SET @SQL = @SQL + ', [RecurringBillingInstallmentInd]'
				SET @SQL = @SQL + ', [RecurringBillingPeriod]'
				SET @SQL = @SQL + ', [RecurringBillingFrequency]'
				SET @SQL = @SQL + ', [RecurringBillingTotalCycles]'
				SET @SQL = @SQL + ', [RecurringBillingInitialAmount]'
				SET @SQL = @SQL + ', [Weight]'
				SET @SQL = @SQL + ', [Length]'
				SET @SQL = @SQL + ', [Height]'
				SET @SQL = @SQL + ', [Width]'
				SET @SQL = @SQL + ', [ShippingRuleTypeID]'
				SET @SQL = @SQL + ', [FreeShippingInd]'
				SET @SQL = @SQL + ', [WebServiceDownloadDte]'
				SET @SQL = @SQL + ', [UpdateDte]'
				SET @SQL = @SQL + ', [SupplierID]'
				SET @SQL = @SQL + ', [TaxClassID]'
				SET @SQL = @SQL + ', [ExternalProductID]'
				SET @SQL = @SQL + ', [ExternalProductAPIID]'
				SET @SQL = @SQL + ', [ExternalID]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeAddOnValue]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [AddOnValueID],'
				SET @SQL = @SQL + ' [AddOnID],'
				SET @SQL = @SQL + ' [Name],'
				SET @SQL = @SQL + ' [Description],'
				SET @SQL = @SQL + ' [SKU],'
				SET @SQL = @SQL + ' [DefaultInd],'
				SET @SQL = @SQL + ' [DisplayOrder],'
				SET @SQL = @SQL + ' [ImageFile],'
				SET @SQL = @SQL + ' [ImageAltTag],'
				SET @SQL = @SQL + ' [RetailPrice],'
				SET @SQL = @SQL + ' [SalePrice],'
				SET @SQL = @SQL + ' [WholesalePrice],'
				SET @SQL = @SQL + ' [RecurringBillingInd],'
				SET @SQL = @SQL + ' [RecurringBillingInstallmentInd],'
				SET @SQL = @SQL + ' [RecurringBillingPeriod],'
				SET @SQL = @SQL + ' [RecurringBillingFrequency],'
				SET @SQL = @SQL + ' [RecurringBillingTotalCycles],'
				SET @SQL = @SQL + ' [RecurringBillingInitialAmount],'
				SET @SQL = @SQL + ' [Weight],'
				SET @SQL = @SQL + ' [Length],'
				SET @SQL = @SQL + ' [Height],'
				SET @SQL = @SQL + ' [Width],'
				SET @SQL = @SQL + ' [ShippingRuleTypeID],'
				SET @SQL = @SQL + ' [FreeShippingInd],'
				SET @SQL = @SQL + ' [WebServiceDownloadDte],'
				SET @SQL = @SQL + ' [UpdateDte],'
				SET @SQL = @SQL + ' [SupplierID],'
				SET @SQL = @SQL + ' [TaxClassID],'
				SET @SQL = @SQL + ' [ExternalProductID],'
				SET @SQL = @SQL + ' [ExternalProductAPIID],'
				SET @SQL = @SQL + ' [ExternalID]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeAddOnValue]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ZNode_NT_ZNodePromotion_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodePromotion table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


ALTER PROCEDURE dbo.ZNode_NT_ZNodePromotion_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[PromotionID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [PromotionID]'
				SET @SQL = @SQL + ', [Name]'
				SET @SQL = @SQL + ', [Description]'
				SET @SQL = @SQL + ', [ProfileID]'
				SET @SQL = @SQL + ', [AccountID]'
				SET @SQL = @SQL + ', [ProductID]'
				SET @SQL = @SQL + ', [AddOnValueID]'
				SET @SQL = @SQL + ', [SKUID]'
				SET @SQL = @SQL + ', [DiscountTypeID]'
				SET @SQL = @SQL + ', [Discount]'
				SET @SQL = @SQL + ', [StartDate]'
				SET @SQL = @SQL + ', [EndDate]'
				SET @SQL = @SQL + ', [CouponInd]'
				SET @SQL = @SQL + ', [CouponCode]'
				SET @SQL = @SQL + ', [CouponQuantityAvailable]'
				SET @SQL = @SQL + ', [PromotionMessage]'
				SET @SQL = @SQL + ', [OrderMinimum]'
				SET @SQL = @SQL + ', [QuantityMinimum]'
				SET @SQL = @SQL + ', [PromotionProductQty]'
				SET @SQL = @SQL + ', [PromotionProductID]'
				SET @SQL = @SQL + ', [DisplayOrder]'
				SET @SQL = @SQL + ', [Custom1]'
				SET @SQL = @SQL + ', [Custom2]'
				SET @SQL = @SQL + ', [Custom3]'
				SET @SQL = @SQL + ', [EnableCouponUrl]'
				SET @SQL = @SQL + ', [PromoCode]'
				SET @SQL = @SQL + ', [ExternalID]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePromotion]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [PromotionID],'
				SET @SQL = @SQL + ' [Name],'
				SET @SQL = @SQL + ' [Description],'
				SET @SQL = @SQL + ' [ProfileID],'
				SET @SQL = @SQL + ' [AccountID],'
				SET @SQL = @SQL + ' [ProductID],'
				SET @SQL = @SQL + ' [AddOnValueID],'
				SET @SQL = @SQL + ' [SKUID],'
				SET @SQL = @SQL + ' [DiscountTypeID],'
				SET @SQL = @SQL + ' [Discount],'
				SET @SQL = @SQL + ' [StartDate],'
				SET @SQL = @SQL + ' [EndDate],'
				SET @SQL = @SQL + ' [CouponInd],'
				SET @SQL = @SQL + ' [CouponCode],'
				SET @SQL = @SQL + ' [CouponQuantityAvailable],'
				SET @SQL = @SQL + ' [PromotionMessage],'
				SET @SQL = @SQL + ' [OrderMinimum],'
				SET @SQL = @SQL + ' [QuantityMinimum],'
				SET @SQL = @SQL + ' [PromotionProductQty],'
				SET @SQL = @SQL + ' [PromotionProductID],'
				SET @SQL = @SQL + ' [DisplayOrder],'
				SET @SQL = @SQL + ' [Custom1],'
				SET @SQL = @SQL + ' [Custom2],'
				SET @SQL = @SQL + ' [Custom3],'
				SET @SQL = @SQL + ' [EnableCouponUrl],'
				SET @SQL = @SQL + ' [PromoCode],'
				SET @SQL = @SQL + ' [ExternalID]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodePromotion]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexerStatus_Find]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Finds records in the ZNodeLuceneIndexerStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexerStatus_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@IndexerIdentity nvarchar (50)  = null ,

	@LastRunDate datetime   = null ,

	@Status nchar (10)  = null ,

	@EndTime datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [IndexerIdentity]
	, [LastRunDate]
	, [Status]
	, [EndTime]
    FROM
	[dbo].[ZNodeLuceneIndexerStatus]
    WHERE 
	 ([ID] = @ID OR @ID IS NULL)
	AND ([IndexerIdentity] = @IndexerIdentity OR @IndexerIdentity IS NULL)
	AND ([LastRunDate] = @LastRunDate OR @LastRunDate IS NULL)
	AND ([Status] = @Status OR @Status IS NULL)
	AND ([EndTime] = @EndTime OR @EndTime IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [IndexerIdentity]
	, [LastRunDate]
	, [Status]
	, [EndTime]
    FROM
	[dbo].[ZNodeLuceneIndexerStatus]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([IndexerIdentity] = @IndexerIdentity AND @IndexerIdentity is not null)
	OR ([LastRunDate] = @LastRunDate AND @LastRunDate is not null)
	OR ([Status] = @Status AND @Status is not null)
	OR ([EndTime] = @EndTime AND @EndTime is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBoost_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeLuceneGlobalProductBoost table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBoost_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[LuceneGlobalProductBoostID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [LuceneGlobalProductBoostID]'
				SET @SQL = @SQL + ', [ProductID]'
				SET @SQL = @SQL + ', [Boost]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneGlobalProductBoost]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [LuceneGlobalProductBoostID],'
				SET @SQL = @SQL + ' [ProductID],'
				SET @SQL = @SQL + ' [Boost]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneGlobalProductBoost]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneDocumentMapping_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeLuceneDocumentMapping table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneDocumentMapping_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[LuceneDocumentMappingID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [LuceneDocumentMappingID]'
				SET @SQL = @SQL + ', [PropertyName]'
				SET @SQL = @SQL + ', [DocumentName]'
				SET @SQL = @SQL + ', [IsIndexed]'
				SET @SQL = @SQL + ', [IsFaceted]'
				SET @SQL = @SQL + ', [IsStored]'
				SET @SQL = @SQL + ', [IsBoosted]'
				SET @SQL = @SQL + ', [FieldBoostable]'
				SET @SQL = @SQL + ', [Boost]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneDocumentMapping]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [LuceneDocumentMappingID],'
				SET @SQL = @SQL + ' [PropertyName],'
				SET @SQL = @SQL + ' [DocumentName],'
				SET @SQL = @SQL + ' [IsIndexed],'
				SET @SQL = @SQL + ' [IsFaceted],'
				SET @SQL = @SQL + ' [IsStored],'
				SET @SQL = @SQL + ' [IsBoosted],'
				SET @SQL = @SQL + ' [FieldBoostable],'
				SET @SQL = @SQL + ' [Boost]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneDocumentMapping]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexMonitor_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeLuceneIndexMonitor table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexMonitor_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[LuceneIndexMonitorID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [LuceneIndexMonitorID]'
				SET @SQL = @SQL + ', [SourceID]'
				SET @SQL = @SQL + ', [SourceType]'
				SET @SQL = @SQL + ', [SourceTransactionType]'
				SET @SQL = @SQL + ', [TransactionDateTime]'
				SET @SQL = @SQL + ', [IsDuplicate]'
				SET @SQL = @SQL + ', [AffectedType]'
				SET @SQL = @SQL + ', [IndexerStatusChangedBy]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneIndexMonitor]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [LuceneIndexMonitorID],'
				SET @SQL = @SQL + ' [SourceID],'
				SET @SQL = @SQL + ' [SourceType],'
				SET @SQL = @SQL + ' [SourceTransactionType],'
				SET @SQL = @SQL + ' [TransactionDateTime],'
				SET @SQL = @SQL + ' [IsDuplicate],'
				SET @SQL = @SQL + ' [AffectedType],'
				SET @SQL = @SQL + ' [IndexerStatusChangedBy]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneIndexMonitor]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexerStatus_Insert]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Inserts a record into the ZNodeLuceneIndexerStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexerStatus_Insert
(

	@ID int    OUTPUT,

	@IndexerIdentity nvarchar (50)  ,

	@LastRunDate datetime   ,

	@Status nchar (10)  ,

	@EndTime datetime   
)
AS


				
				INSERT INTO [dbo].[ZNodeLuceneIndexerStatus]
					(
					[IndexerIdentity]
					,[LastRunDate]
					,[Status]
					,[EndTime]
					)
				VALUES
					(
					@IndexerIdentity
					,@LastRunDate
					,@Status
					,@EndTime
					)
				
				-- Get the identity value
				SET @ID = SCOPE_IDENTITY()
									
							
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexerStatus_Update]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Updates a record in the ZNodeLuceneIndexerStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexerStatus_Update
(

	@ID int   ,

	@IndexerIdentity nvarchar (50)  ,

	@LastRunDate datetime   ,

	@Status nchar (10)  ,

	@EndTime datetime   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ZNodeLuceneIndexerStatus]
				SET
					[IndexerIdentity] = @IndexerIdentity
					,[LastRunDate] = @LastRunDate
					,[Status] = @Status
					,[EndTime] = @EndTime
				WHERE
[ID] = @ID 
				
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexServerStatus_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeLuceneIndexServerStatus table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexServerStatus_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[ID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [ID]'
				SET @SQL = @SQL + ', [ServerName]'
				SET @SQL = @SQL + ', [LuceneIndexMonitorID]'
				SET @SQL = @SQL + ', [Status]'
				SET @SQL = @SQL + ', [StartTime]'
				SET @SQL = @SQL + ', [EndTime]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneIndexServerStatus]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [ID],'
				SET @SQL = @SQL + ' [ServerName],'
				SET @SQL = @SQL + ' [LuceneIndexMonitorID],'
				SET @SQL = @SQL + ' [Status],'
				SET @SQL = @SQL + ' [StartTime],'
				SET @SQL = @SQL + ' [EndTime]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneIndexServerStatus]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexerStatus_Delete]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Deletes a record in the ZNodeLuceneIndexerStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexerStatus_Delete
(

	@ID int   
)
AS


				DELETE FROM [dbo].[ZNodeLuceneIndexerStatus] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexerStatus_GetByID]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Select records from the ZNodeLuceneIndexerStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexerStatus_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[IndexerIdentity],
					[LastRunDate],
					[Status],
					[EndTime]
				FROM
					[dbo].[ZNodeLuceneIndexerStatus]
				WHERE
					[ID] = @ID
				SELECT @@ROWCOUNT
					
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexStatusNames_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeLuceneIndexStatusNames table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexStatusNames_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[Status]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [Status]'
				SET @SQL = @SQL + ', [StatusName]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneIndexStatusNames]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [Status],'
				SET @SQL = @SQL + ' [StatusName]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneIndexStatusNames]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeLuceneGlobalProductCategoryBoost table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCategoryBoost_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[LuceneGlobalProductCategoryBoostID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [LuceneGlobalProductCategoryBoostID]'
				SET @SQL = @SQL + ', [ProductCategoryID]'
				SET @SQL = @SQL + ', [Boost]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneGlobalProductCategoryBoost]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [LuceneGlobalProductCategoryBoostID],'
				SET @SQL = @SQL + ' [ProductCategoryID],'
				SET @SQL = @SQL + ' [Boost]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneGlobalProductCategoryBoost]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneServerConfigurationStatus_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeLuceneServerConfigurationStatus table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneServerConfigurationStatus_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[ID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [ID]'
				SET @SQL = @SQL + ', [ServerName]'
				SET @SQL = @SQL + ', [IP_Address]'
				SET @SQL = @SQL + ', [Status]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneServerConfigurationStatus]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [ID],'
				SET @SQL = @SQL + ' [ServerName],'
				SET @SQL = @SQL + ' [IP_Address],'
				SET @SQL = @SQL + ' [Status]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneServerConfigurationStatus]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_CheckLuceneTriggersDisabled]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.ZNode_CheckLuceneTriggersDisabled
 
AS


select objectproperty(object_id('AfterProductDelete'), 'ExecIsTriggerDisabled');





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_GetPaged]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeLuceneGlobalProductCatalogBoost table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductCatalogBoost_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[LuceneGlobalProductCatalogBoostID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [LuceneGlobalProductCatalogBoostID]'
				SET @SQL = @SQL + ', [ProductID]'
				SET @SQL = @SQL + ', [CatalogID]'
				SET @SQL = @SQL + ', [Boost]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneGlobalProductCatalogBoost]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [LuceneGlobalProductCatalogBoostID],'
				SET @SQL = @SQL + ' [ProductID],'
				SET @SQL = @SQL + ' [CatalogID],'
				SET @SQL = @SQL + ' [Boost]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneGlobalProductCatalogBoost]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeLuceneGlobalProductBrandBoost table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneGlobalProductBrandBoost_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[LuceneGlobalProductBrandBoostID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [LuceneGlobalProductBrandBoostID]'
				SET @SQL = @SQL + ', [ProductID]'
				SET @SQL = @SQL + ', [Boost]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneGlobalProductBrandBoost]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [LuceneGlobalProductBrandBoostID],'
				SET @SQL = @SQL + ' [ProductID],'
				SET @SQL = @SQL + ' [Boost]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneGlobalProductBrandBoost]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_TurnLuceneTriggersOnOff]'
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[ZNode_TurnLuceneTriggersOnOff]
	@TriggerFlag bit = 1
AS
BEGIN

IF @TriggerFlag = 1
BEGIN

	IF OBJECT_ID('AfterCatalogUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeCatalog ENABLE TRIGGER [AfterCatalogUpdate]
	END

	IF OBJECT_ID('AfterCategoryNodeDelete') IS NOT NULL
	BEGIN	
		ALTER TABLE dbo.ZNodeCategoryNode ENABLE TRIGGER [AfterCategoryNodeDelete]
	END

	IF OBJECT_ID('AfterCategoryNodeInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeCategoryNode ENABLE TRIGGER [AfterCategoryNodeInsert]
	END

	IF OBJECT_ID('AfterCategoryNodeUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeCategoryNode ENABLE TRIGGER [AfterCategoryNodeUpdate]
	END
	
	IF OBJECT_ID('AfterCategoryUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeCategory ENABLE TRIGGER [AfterCategoryUpdate]
	END
	
	IF OBJECT_ID('AfterLuceneDocumentMappingUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneDocumentMapping ENABLE TRIGGER [AfterLuceneDocumentMappingUpdate]
	END
	
	IF OBJECT_ID('AfterLuceneGlobalProductBoostUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductBoost ENABLE TRIGGER [AfterLuceneGlobalProductBoostUpdate]
	END
	
	IF OBJECT_ID('AfterLuceneGlobalProductBoostUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductBoost ENABLE TRIGGER [AfterLuceneGlobalProductBoostUpdate]
	END

	IF OBJECT_ID('AfterLuceneGlobalProductBoostDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductBoost ENABLE TRIGGER [AfterLuceneGlobalProductBoostDelete]
	END

	IF OBJECT_ID('AfterLuceneGlobalProductBoostInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductBoost ENABLE TRIGGER [AfterLuceneGlobalProductBoostInsert]
	END

	IF OBJECT_ID('AfterLuceneGlobalProductCategoryBoostUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductCategoryBoost ENABLE TRIGGER [AfterLuceneGlobalProductCategoryBoostUpdate]
	END

	IF OBJECT_ID('AfterLuceneGlobalProductCategoryBoostDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductCategoryBoost ENABLE TRIGGER [AfterLuceneGlobalProductCategoryBoostDelete]
	END

	IF OBJECT_ID('AfterLuceneGlobalProductCategoryBoostInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductCategoryBoost ENABLE TRIGGER [AfterLuceneGlobalProductCategoryBoostInsert]
	END

	IF OBJECT_ID('AfterManufacturerUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeManufacturer ENABLE TRIGGER [AfterManufacturerUpdate]
	END

	IF OBJECT_ID('AfterPortalCatalogDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodePortalCatalog ENABLE TRIGGER [AfterPortalCatalogDelete]
	END

	IF OBJECT_ID('AfterPortalCatalogInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodePortalCatalog ENABLE TRIGGER [AfterPortalCatalogInsert]
	END

	IF OBJECT_ID('AfterPortalCatalogUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodePortalCatalog ENABLE TRIGGER [AfterPortalCatalogUpdate]
	END

	IF OBJECT_ID('AfterPortalUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodePortal ENABLE TRIGGER [AfterPortalUpdate]
	END

	IF OBJECT_ID('AfterProductCategoryDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProductCategory ENABLE TRIGGER [AfterProductCategoryDelete]
	END

	IF OBJECT_ID('AfterProductCategoryInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProductCategory ENABLE TRIGGER [AfterProductCategoryInsert]
	END

	IF OBJECT_ID('AfterProductCategoryUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProductCategory ENABLE TRIGGER [AfterProductCategoryUpdate]
	END

	IF OBJECT_ID('AfterProductDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProduct ENABLE TRIGGER [AfterProductDelete]
	END

	IF OBJECT_ID('AfterProductInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProduct ENABLE TRIGGER [AfterProductInsert]
	END

	IF OBJECT_ID('AfterProductUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProduct ENABLE TRIGGER [AfterProductUpdate]
	END

	IF OBJECT_ID('AfterReviewDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeReview ENABLE TRIGGER [AfterReviewDelete]
	END

	IF OBJECT_ID('AfterReviewInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeReview ENABLE TRIGGER [AfterReviewInsert]
	END

	IF OBJECT_ID('AfterReviewUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeReview ENABLE TRIGGER [AfterReviewUpdate]
	END

	IF OBJECT_ID('AfterSKUDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeSKU ENABLE TRIGGER [AfterSKUDelete]
	END

	IF OBJECT_ID('AfterSKUInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeSKU ENABLE TRIGGER [AfterSKUInsert]
	END

	IF OBJECT_ID('AfterSKUUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeSKU ENABLE TRIGGER [AfterSKUUpdate]
	END

	IF OBJECT_ID('AfterSupplierUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeSupplier ENABLE TRIGGER [AfterSupplierUpdate]
	END
	
	IF OBJECT_ID('AfterTagProductSKUDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagProductSKU ENABLE TRIGGER [AfterTagProductSKUDelete]
	END
	
	IF OBJECT_ID('AfterTagProductSKUInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagProductSKU ENABLE TRIGGER [AfterTagProductSKUInsert]
	END
	
	IF OBJECT_ID('AfterTagProductSKUUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagProductSKU ENABLE TRIGGER [AfterTagProductSKUUpdate]
	END
	
	IF OBJECT_ID('AfterTagUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTag ENABLE TRIGGER [AfterTagUpdate]
	END
	
	IF OBJECT_ID('AfterTagGroupUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagGroup ENABLE TRIGGER [AfterTagGroupUpdate]
	END
	
	IF OBJECT_ID('AfterTagGroupCategoryDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagGroupCategory ENABLE TRIGGER [AfterTagGroupCategoryDelete]
	END
	
	IF OBJECT_ID('AfterTagGroupCategoryInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagGroupCategory ENABLE TRIGGER [AfterTagGroupCategoryInsert]
	END
	
	IF OBJECT_ID('AfterTagGroupCategoryUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagGroupCategory ENABLE TRIGGER [AfterTagGroupCategoryUpdate]
	END

END
ELSE
BEGIN

	IF OBJECT_ID('AfterCatalogUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeCatalog DISABLE TRIGGER [AfterCatalogUpdate]
	END

	IF OBJECT_ID('AfterCategoryNodeDelete') IS NOT NULL
	BEGIN	
		ALTER TABLE dbo.ZNodeCategoryNode DISABLE TRIGGER [AfterCategoryNodeDelete]
	END

	IF OBJECT_ID('AfterCategoryNodeInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeCategoryNode DISABLE TRIGGER [AfterCategoryNodeInsert]
	END

	IF OBJECT_ID('AfterCategoryNodeUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeCategoryNode DISABLE TRIGGER [AfterCategoryNodeUpdate]
	END
	
	IF OBJECT_ID('AfterCategoryUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeCategory DISABLE TRIGGER [AfterCategoryUpdate]
	END
	
	IF OBJECT_ID('AfterLuceneDocumentMappingUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneDocumentMapping DISABLE TRIGGER [AfterLuceneDocumentMappingUpdate]
	END

	IF OBJECT_ID('AfterLuceneGlobalProductBoostUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductBoost DISABLE TRIGGER [AfterLuceneGlobalProductBoostUpdate]
	END

	IF OBJECT_ID('AfterLuceneGlobalProductBoostDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductBoost DISABLE TRIGGER [AfterLuceneGlobalProductBoostDelete]
	END

	IF OBJECT_ID('AfterLuceneGlobalProductBoostInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductBoost DISABLE TRIGGER [AfterLuceneGlobalProductBoostInsert]
	END

	IF OBJECT_ID('AfterLuceneGlobalProductCategoryBoostUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductCategoryBoost DISABLE TRIGGER [AfterLuceneGlobalProductCategoryBoostUpdate]
	END

	IF OBJECT_ID('AfterLuceneGlobalProductCategoryBoostDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductCategoryBoost DISABLE TRIGGER [AfterLuceneGlobalProductCategoryBoostDelete]
	END

	IF OBJECT_ID('AfterLuceneGlobalProductCategoryBoostInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeLuceneGlobalProductCategoryBoost DISABLE TRIGGER [AfterLuceneGlobalProductCategoryBoostInsert]
	END

	IF OBJECT_ID('AfterManufacturerUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeManufacturer DISABLE TRIGGER [AfterManufacturerUpdate]
	END

	IF OBJECT_ID('AfterPortalCatalogDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodePortalCatalog DISABLE TRIGGER [AfterPortalCatalogDelete]
	END

	IF OBJECT_ID('AfterPortalCatalogInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodePortalCatalog DISABLE TRIGGER [AfterPortalCatalogInsert]
	END

	IF OBJECT_ID('AfterPortalCatalogUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodePortalCatalog DISABLE TRIGGER [AfterPortalCatalogUpdate]
	END

	IF OBJECT_ID('AfterPortalUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodePortal DISABLE TRIGGER [AfterPortalUpdate]
	END

	IF OBJECT_ID('AfterProductCategoryDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProductCategory DISABLE TRIGGER [AfterProductCategoryDelete]
	END

	IF OBJECT_ID('AfterProductCategoryInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProductCategory DISABLE TRIGGER [AfterProductCategoryInsert]
	END

	IF OBJECT_ID('AfterProductCategoryUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProductCategory DISABLE TRIGGER [AfterProductCategoryUpdate]
	END

	IF OBJECT_ID('AfterProductDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProduct DISABLE TRIGGER [AfterProductDelete]
	END

	IF OBJECT_ID('AfterProductInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProduct DISABLE TRIGGER [AfterProductInsert]
	END

	IF OBJECT_ID('AfterProductUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeProduct DISABLE TRIGGER [AfterProductUpdate]
	END

	IF OBJECT_ID('AfterReviewDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeReview DISABLE TRIGGER [AfterReviewDelete]
	END

	IF OBJECT_ID('AfterReviewInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeReview DISABLE TRIGGER [AfterReviewInsert]
	END

	IF OBJECT_ID('AfterReviewUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeReview DISABLE TRIGGER [AfterReviewUpdate]
	END

	IF OBJECT_ID('AfterSKUDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeSKU DISABLE TRIGGER [AfterSKUDelete]
	END

	IF OBJECT_ID('AfterSKUInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeSKU DISABLE TRIGGER [AfterSKUInsert]
	END

	IF OBJECT_ID('AfterSKUUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeSKU DISABLE TRIGGER [AfterSKUUpdate]
	END

	IF OBJECT_ID('AfterSupplierUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeSupplier DISABLE TRIGGER [AfterSupplierUpdate]
	END
	
	IF OBJECT_ID('AfterTagProductSKUDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagProductSKU DISABLE TRIGGER [AfterTagProductSKUDelete]
	END
	
	IF OBJECT_ID('AfterTagProductSKUInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagProductSKU DISABLE TRIGGER [AfterTagProductSKUInsert]
	END
	
	IF OBJECT_ID('AfterTagProductSKUUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagProductSKU DISABLE TRIGGER [AfterTagProductSKUUpdate]
	END
	
	IF OBJECT_ID('AfterTagUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTag DISABLE TRIGGER [AfterTagUpdate]
	END
	
	IF OBJECT_ID('AfterTagGroupUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagGroup DISABLE TRIGGER [AfterTagGroupUpdate]
	END
	
	IF OBJECT_ID('AfterTagGroupCategoryDelete') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagGroupCategory DISABLE TRIGGER [AfterTagGroupCategoryDelete]
	END
	
	IF OBJECT_ID('AfterTagGroupCategoryInsert') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagGroupCategory DISABLE TRIGGER [AfterTagGroupCategoryInsert]
	END
	
	IF OBJECT_ID('AfterTagGroupCategoryUpdate') IS NOT NULL
	BEGIN
		ALTER TABLE dbo.ZNodeTagGroupCategory DISABLE TRIGGER [AfterTagGroupCategoryUpdate]
	END
	
END
    
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexerStatus_Get_List]'
GO
SET ANSI_NULLS OFF
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets all records from the ZNodeLuceneIndexerStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexerStatus_Get_List

AS


				
				SELECT
					[ID],
					[IndexerIdentity],
					[LastRunDate],
					[Status],
					[EndTime]
				FROM
					[dbo].[ZNodeLuceneIndexerStatus]
					
				SELECT @@ROWCOUNT
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ZNode_NT_ZNodeLuceneIndexerStatus_GetPaged]'
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Znode Inc (http://www.znode.com)
-- Purpose: Gets records from the ZNodeLuceneIndexerStatus table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.ZNode_NT_ZNodeLuceneIndexerStatus_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
				BEGIN
					-- default order by to first column
					SET @OrderBy = '[ID]'
				END

				-- SQL Server 2005 Paging
				DECLARE @SQL AS nvarchar(MAX)
				SET @SQL = 'WITH PageIndex AS ('
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'
				SET @SQL = @SQL + ', [ID]'
				SET @SQL = @SQL + ', [IndexerIdentity]'
				SET @SQL = @SQL + ', [LastRunDate]'
				SET @SQL = @SQL + ', [Status]'
				SET @SQL = @SQL + ', [EndTime]'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneIndexerStatus]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				SET @SQL = @SQL + ' ) SELECT'
				SET @SQL = @SQL + ' [ID],'
				SET @SQL = @SQL + ' [IndexerIdentity],'
				SET @SQL = @SQL + ' [LastRunDate],'
				SET @SQL = @SQL + ' [Status],'
				SET @SQL = @SQL + ' [EndTime]'
				SET @SQL = @SQL + ' FROM PageIndex'
				SET @SQL = @SQL + ' WHERE RowIndex > ' + CONVERT(nvarchar, @PageLowerBound)
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' AND RowIndex <= ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				EXEC sp_executesql @SQL
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[ZNodeLuceneIndexerStatus]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
PRINT N'Creating primary key [PK__db_chang__3213E83F3DC8FF7D] on [dbo].[db_changes]'
GO
ALTER TABLE [dbo].[db_changes] ADD CONSTRAINT [PK__db_chang__3213E83F3DC8FF7D] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__db_schem__3213E83F440B1D61] on [dbo].[db_schema]'
GO
ALTER TABLE [dbo].[db_schema] ADD CONSTRAINT [PK__db_schem__3213E83F440B1D61] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__ZNodeSKU__87015C636D181FEC] on [dbo].[ZNodeSKUProfileEffective]'
GO
ALTER TABLE [dbo].[ZNodeSKUProfileEffective] ADD CONSTRAINT [PK__ZNodeSKU__87015C636D181FEC] PRIMARY KEY CLUSTERED  ([SkuProfileEffectiveID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[ZNodeLuceneGlobalProductBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductBoost] ADD CONSTRAINT [IX_ZNodeLuceneGlobalProductBoost] UNIQUE NONCLUSTERED  ([ProductID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[ZNodeLuceneGlobalProductBrandBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductBrandBoost] ADD CONSTRAINT [IX_ZNodeLuceneGlobalProductBrandBoost] UNIQUE NONCLUSTERED  ([ProductID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[ZNodeLuceneGlobalProductCatalogBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductCatalogBoost] ADD CONSTRAINT [IX_ZNodeLuceneGlobalProductCatalogBoost] UNIQUE NONCLUSTERED  ([CatalogID], [ProductID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[ZNodeLuceneGlobalProductCategoryBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductCategoryBoost] ADD CONSTRAINT [IX_ZNodeLuceneGlobalProductCategoryBoost] UNIQUE NONCLUSTERED  ([ProductCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[ZNodeRMARequest]'
GO
ALTER TABLE [dbo].[ZNodeRMARequest] ADD CONSTRAINT [UQ__ZNodeRMA__9ADA6BE06740165C] UNIQUE NONCLUSTERED  ([RequestNumber])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeLuceneGlobalProductBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductBoost] ADD CONSTRAINT [FK_ZNodeLuceneGlobalProductBoost_ZNodeProduct] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[ZNodeProduct] ([ProductID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeLuceneGlobalProductBrandBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductBrandBoost] ADD CONSTRAINT [FK_ZNodeLuceneGlobalProductBrandBoost_ZNodeProduct] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[ZNodeProduct] ([ProductID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeLuceneGlobalProductCatalogBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductCatalogBoost] ADD CONSTRAINT [FK_ZNodeLuceneGlobalProductCatalogBoost_ZNodeProduct] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[ZNodeProduct] ([ProductID]) ON DELETE CASCADE ON UPDATE CASCADE
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductCatalogBoost] ADD CONSTRAINT [FK_ZNodeLuceneGlobalProductCatalogBoost_ZNodeCatalog] FOREIGN KEY ([CatalogID]) REFERENCES [dbo].[ZNodeCatalog] ([CatalogID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeLuceneGlobalProductCategoryBoost]'
GO
ALTER TABLE [dbo].[ZNodeLuceneGlobalProductCategoryBoost] ADD CONSTRAINT [FK_ZNodeProductCategoryBoost_ZNodeProductCategory] FOREIGN KEY ([ProductCategoryID]) REFERENCES [dbo].[ZNodeProductCategory] ([ProductCategoryID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeLuceneIndexServerStatus]'
GO
ALTER TABLE [dbo].[ZNodeLuceneIndexServerStatus] ADD CONSTRAINT [FK_ZNodeLuceneIndexServerStatus_LuceneIndexMonitorID] FOREIGN KEY ([LuceneIndexMonitorID]) REFERENCES [dbo].[ZNodeLuceneIndexMonitor] ([LuceneIndexMonitorID]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ZNodeRMARequestItem]'
GO
ALTER TABLE [dbo].[ZNodeRMARequestItem] ADD CONSTRAINT [FK__ZNodeRMAR__Reaso__71BDA4CF] FOREIGN KEY ([ReasonForReturnID]) REFERENCES [dbo].[ZNodeReasonForReturn] ([ReasonForReturnID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterCategoryUpdate] on [dbo].[ZNodeCategory]'
GO
CREATE TRIGGER [dbo].[AfterCategoryUpdate] ON [dbo].[ZNodeCategory]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (NAME) )
	BEGIN
		SELECT @sourceId = category.CategoryID
		FROM inserted category;
		
		SET @source = 'Category';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'Category';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterCategoryNodeDelete] on [dbo].[ZNodeCategoryNode]'
GO

CREATE TRIGGER [dbo].[AfterCategoryNodeDelete] ON [dbo].[ZNodeCategoryNode]
AFTER DELETE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = categoryNode.CategoryID
	FROM deleted categoryNode;
		   
	SET @source = 'Category';
	SET @transactionType = 'DELETE';
	SET @affectedType = 'CategoryNode';
	
	IF @sourceId IS NOT NULL
	
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterCategoryNodeInsert] on [dbo].[ZNodeCategoryNode]'
GO
CREATE TRIGGER [dbo].[AfterCategoryNodeInsert] ON [dbo].[ZNodeCategoryNode]
AFTER INSERT
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = categoryNode.CategoryID
	FROM inserted categoryNode;
		   
	SET @source = 'Category';
	SET @transactionType = 'INSERT';
	SET @affectedType = 'CategoryNode';
	
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterCategoryNodeUpdate] on [dbo].[ZNodeCategoryNode]'
GO
CREATE TRIGGER [dbo].[AfterCategoryNodeUpdate] ON [dbo].[ZNodeCategoryNode]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (CATALOGID) OR UPDATE (CATEGORYID)  )
	BEGIN
		SELECT @sourceId = categoryNode.CategoryID
		FROM inserted categoryNode;
		
		SET @source = 'Category';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'CategoryNode';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterLuceneDocumentMappingUpdate] on [dbo].[ZNodeLuceneDocumentMapping]'
GO
CREATE TRIGGER [dbo].[AfterLuceneDocumentMappingUpdate] ON [dbo].[ZNodeLuceneDocumentMapping]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (BOOST))
	BEGIN
	SELECT @sourceId = 0
	FROM inserted luceneDocumentMapping;
		   
	SET @source = 'CreateIndex';
	SET @transactionType = 'INSERT';
	SET @affectedType = 'CreateIndex';
	
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
	END;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterLuceneGlobalProductBoostDelete] on [dbo].[ZNodeLuceneGlobalProductBoost]'
GO
CREATE TRIGGER [dbo].[AfterLuceneGlobalProductBoostDelete] ON [dbo].[ZNodeLuceneGlobalProductBoost]
AFTER DELETE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = luceneGlobalProductBoost.ProductID
	FROM deleted luceneGlobalProductBoost;
		   
	SET @source = 'Product';
	SET @transactionType = 'DELETE';
	SET @affectedType = 'LuceneGlobalProductBoost';
	
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterLuceneGlobalProductBoostInsert] on [dbo].[ZNodeLuceneGlobalProductBoost]'
GO
CREATE TRIGGER [dbo].[AfterLuceneGlobalProductBoostInsert] ON [dbo].[ZNodeLuceneGlobalProductBoost]
AFTER INSERT
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = luceneGlobalProductBoost.ProductID
	FROM inserted luceneGlobalProductBoost;
		   
	SET @source = 'Product';
	SET @transactionType = 'INSERT';
	SET @affectedType = 'LuceneGlobalProductBoost';
	
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterLuceneGlobalProductBoostUpdate] on [dbo].[ZNodeLuceneGlobalProductBoost]'
GO
CREATE TRIGGER [dbo].[AfterLuceneGlobalProductBoostUpdate] ON [dbo].[ZNodeLuceneGlobalProductBoost]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (PRODUCTID) OR UPDATE (Boost)  )
	BEGIN
		SELECT @sourceId = luceneGlobalProductBoost.ProductID
		FROM inserted luceneGlobalProductBoost;
		
		SET @source = 'Product';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'LuceneGlobalProductBoost';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterLuceneGlobalProductCategoryBoostDelete] on [dbo].[ZNodeLuceneGlobalProductCategoryBoost]'
GO
CREATE TRIGGER [dbo].[AfterLuceneGlobalProductCategoryBoostDelete] ON [dbo].[ZNodeLuceneGlobalProductCategoryBoost]
AFTER DELETE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = productCategory.ProductID
		FROM deleted luceneGlobalProductCategoryBoost JOIN dbo.ZNodeProductCategory productCategory
		ON luceneGlobalProductCategoryBoost.ProductCategoryID = productCategory.ProductCategoryID;
		   
	SET @source = 'Product';
	SET @transactionType = 'DELETE';
	SET @affectedType = 'LuceneGlobalProductCategoryBoost';
	
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(), @affectedType);

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterLuceneGlobalProductCategoryBoostInsert] on [dbo].[ZNodeLuceneGlobalProductCategoryBoost]'
GO
CREATE TRIGGER [dbo].[AfterLuceneGlobalProductCategoryBoostInsert] ON [dbo].[ZNodeLuceneGlobalProductCategoryBoost]
AFTER INSERT
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = productCategory.ProductID
		FROM inserted luceneGlobalProductCategoryBoost JOIN dbo.ZNodeProductCategory productCategory
		ON luceneGlobalProductCategoryBoost.ProductCategoryID = productCategory.ProductCategoryID;
		   
	SET @source = 'Product';
	SET @transactionType = 'INSERT';
	SET @affectedType = 'LuceneGlobalProductCategoryBoost';
	
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterLuceneGlobalProductCategoryBoostUpdate] on [dbo].[ZNodeLuceneGlobalProductCategoryBoost]'
GO
CREATE TRIGGER [dbo].[AfterLuceneGlobalProductCategoryBoostUpdate] ON [dbo].[ZNodeLuceneGlobalProductCategoryBoost]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (ProductCategoryID) OR UPDATE (Boost)  )
	BEGIN
		SELECT @sourceId = productCategory.ProductID
		FROM inserted luceneGlobalProductCategoryBoost JOIN dbo.ZNodeProductCategory productCategory
		ON luceneGlobalProductCategoryBoost.ProductCategoryID = productCategory.ProductCategoryID;
		
		SET @source = 'Product';
		SET	@transactionType = 'UPDATE';
	SET @affectedType = 'LuceneGlobalProductCategoryBoost';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(), @affectedType);
		
	END;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[ZNodeLuceneIndexMonitor_AspNet_SqlCacheNotification_Trigger] on [dbo].[ZNodeLuceneIndexMonitor]'
GO
CREATE TRIGGER dbo.[ZNodeLuceneIndexMonitor_AspNet_SqlCacheNotification_Trigger] ON [dbo].[ZNodeLuceneIndexMonitor]
                       FOR INSERT, UPDATE, DELETE AS BEGIN
                       SET NOCOUNT ON
                       EXEC dbo.AspNet_SqlCacheUpdateChangeIdStoredProcedure N'ZNodeLuceneIndexMonitor'
                       END
                       
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[ZNodeLuceneIndexServerStatus_AspNet_SqlCacheNotification_Trigger] on [dbo].[ZNodeLuceneIndexServerStatus]'
GO
CREATE TRIGGER dbo.[ZNodeLuceneIndexServerStatus_AspNet_SqlCacheNotification_Trigger] ON [dbo].[ZNodeLuceneIndexServerStatus]
                       FOR INSERT, UPDATE, DELETE AS BEGIN
                       SET NOCOUNT ON
                       EXEC dbo.AspNet_SqlCacheUpdateChangeIdStoredProcedure N'ZNodeLuceneIndexServerStatus'
                       END
                       
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterManufacturerUpdate] on [dbo].[ZNodeManufacturer]'
GO
CREATE TRIGGER [dbo].[AfterManufacturerUpdate] ON [dbo].[ZNodeManufacturer]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (NAME) )
	BEGIN
		SELECT @sourceId = manufacturer.ManufacturerID 
		FROM inserted manufacturer;
		
		SET @source = 'Manufacturer';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'Manufacturer';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterPortalCatalogDelete] on [dbo].[ZNodePortalCatalog]'
GO
CREATE TRIGGER [dbo].[AfterPortalCatalogDelete] ON [dbo].[ZNodePortalCatalog]
AFTER DELETE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = 0 
	FROM deleted portalCatalog;
		   
	SET @source = 'CreateIndex';
	SET @transactionType = 'DELETE';
	SET @affectedType = 'CreateIndex';
	IF(@sourceId IS NOT NULL)
	BEGIN
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterPortalCatalogInsert] on [dbo].[ZNodePortalCatalog]'
GO
CREATE TRIGGER [dbo].[AfterPortalCatalogInsert] ON [dbo].[ZNodePortalCatalog]
AFTER INSERT
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = 0
	FROM inserted portalCatalog;
		   
	SET @source = 'CreateIndex';
	SET @transactionType = 'INSERT';
	SET @affectedType = 'CreateIndex';
	IF(@sourceId IS NOT NULL)
	BEGIN
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterPortalCatalogUpdate] on [dbo].[ZNodePortalCatalog]'
GO
CREATE TRIGGER [dbo].[AfterPortalCatalogUpdate] ON [dbo].[ZNodePortalCatalog]
AFTER INSERT
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = 0
	FROM inserted portalCatalog;
		   
	SET @source = 'CreateIndex';
	SET @transactionType = 'INSERT';
	SET @affectedType = 'CreateIndex';
	
	IF(@sourceId IS NOT NULL)
	BEGIN
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterProductDelete] on [dbo].[ZNodeProduct]'
GO
CREATE TRIGGER [dbo].[AfterProductDelete] ON [dbo].[ZNodeProduct]  
AFTER DELETE  
AS  
DECLARE @sourceId int,  
@source varchar(50),  
@transactionType varchar(10),  
@affectedType varchar(50);  

SELECT @sourceId = product.ProductID  
FROM deleted product;  
   
SET @source = 'Product';  
SET @transactionType = 'DELETE';  
SET @affectedType = 'Product';  
IF(@sourceId IS NOT NULL)
BEGIN
INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)  
VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterProductInsert] on [dbo].[ZNodeProduct]'
GO
CREATE TRIGGER [dbo].[AfterProductInsert] ON [dbo].[ZNodeProduct]
AFTER INSERT
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = product.ProductID
	FROM inserted product;
		   
	SET @source = 'Product';
	SET @transactionType = 'INSERT';
	SET @affectedType = 'Product';
	
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterProductUpdate] on [dbo].[ZNodeProduct]'
GO
CREATE TRIGGER [dbo].[AfterProductUpdate] ON [dbo].[ZNodeProduct]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (NAME) OR UPDATE([DESCRIPTION]) OR UPDATE (SHORTDESCRIPTION) OR UPDATE(FEATURESDESC) OR UPDATE(SPECIFICATIONS) OR
		 UPDATE(PORTALID) OR UPDATE(SUPPLIERID) OR UPDATE(ACTIVEIND) )
	BEGIN
		SELECT @sourceId = product.ProductID 
		FROM inserted product;
		
		SET @source = 'Product';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'Product';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterProductCategoryDelete] on [dbo].[ZNodeProductCategory]'
GO
CREATE TRIGGER [dbo].[AfterProductCategoryDelete] ON [dbo].[ZNodeProductCategory]  
AFTER DELETE  
AS  
 DECLARE @sourceId int,  
   @source varchar(50),  
   @transactionType varchar(10),  
   @affectedType varchar(50);  
  
 SELECT @sourceId = productCategory.ProductID  
 FROM deleted productCategory;  
       
 SET @source = 'Product';  
 SET @transactionType = 'DELETE';  
 SET @affectedType = 'ProductCategory';  
IF(@sourceId IS NOT NULL)
BEGIN
 INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)  
  VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterProductCategoryInsert] on [dbo].[ZNodeProductCategory]'
GO

CREATE  TRIGGER [dbo].[AfterProductCategoryInsert] ON [dbo].[ZNodeProductCategory]  
AFTER INSERT  
AS  
 DECLARE @sourceId int,  
   @source varchar(50),  
   @transactionType varchar(10),  
   @affectedType varchar(50);  
  
 SELECT @sourceId = productCategory.ProductID  
 FROM inserted productCategory;  
       
 SET @source = 'Product';  
 SET @transactionType = 'INSERT';  
 SET @affectedType = 'ProductCategory';  
   
   IF(@SourceID is not null)
 INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)  
  VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterProductCategoryUpdate] on [dbo].[ZNodeProductCategory]'
GO
CREATE TRIGGER [dbo].[AfterProductCategoryUpdate] ON [dbo].[ZNodeProductCategory]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50),
			@activeIndValue varchar(50);
	
	
	IF ( UPDATE (PRODUCTID) OR UPDATE (CATEGORYID) OR UPDATE (ACTIVEIND))
	BEGIN
		SELECT @sourceId = productCategory.ProductID
		FROM inserted productCategory;
		
		SET @source = 'Product';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'ProductCategory';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
	
	
		
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterReviewDelete] on [dbo].[ZNodeReview]'
GO
CREATE TRIGGER [dbo].[AfterReviewDelete] ON [dbo].[ZNodeReview]
AFTER DELETE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = review.ProductID
	FROM deleted review;
		   
	SET @source = 'Product';
	SET @transactionType = 'DELETE';
	SET @affectedType = 'Review';
	
	IF @sourceId IS NOT NULL
	BEGIN
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
			VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterReviewInsert] on [dbo].[ZNodeReview]'
GO
CREATE TRIGGER [dbo].[AfterReviewInsert] ON [dbo].[ZNodeReview]
AFTER INSERT
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = review.ProductID
	FROM inserted review;
		   
	SET @source = 'Product';
	SET @transactionType = 'INSERT';
	SET @affectedType = 'Review';
	
	IF @sourceId IS NOT NULL
	BEGIN
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
			VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterReviewUpdate] on [dbo].[ZNodeReview]'
GO
CREATE TRIGGER [dbo].[AfterReviewUpdate] ON [dbo].[ZNodeReview]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (RATING) )
	BEGIN
		SELECT @sourceId = review.ProductID
		FROM inserted review;
		
		SET @source = 'Product';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'Review';
		
		IF @sourceId IS NOT NULL
		BEGIN
			INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
			VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		END
	END;
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterSKUDelete] on [dbo].[ZNodeSKU]'
GO
CREATE TRIGGER [dbo].[AfterSKUDelete] ON [dbo].[ZNodeSKU]  
AFTER DELETE  
AS  
 DECLARE @sourceId int,  
   @source varchar(50),  
   @transactionType varchar(10),  
   @affectedType varchar(50);  
  
 SELECT @sourceId = sku.ProductID  
 FROM deleted sku;  
       
 SET @source = 'Product';  
 SET @transactionType = 'DELETE';  
 SET @affectedType = 'SKU';  
   
   IF(@sourceId IS NOT NULL)
	BEGIN
 INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)  
  VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);  
  END
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterSKUInsert] on [dbo].[ZNodeSKU]'
GO
CREATE TRIGGER [dbo].[AfterSKUInsert] ON [dbo].[ZNodeSKU]
AFTER INSERT
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = sku.ProductID
	FROM inserted sku;
		   
	SET @source = 'Product';
	SET @transactionType = 'INSERT';
	SET @affectedType = 'SKU';
	
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterSKUUpdate] on [dbo].[ZNodeSKU]'
GO
CREATE TRIGGER [dbo].[AfterSKUUpdate] ON [dbo].[ZNodeSKU]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (PRODUCTID) OR UPDATE (SKU) )
	BEGIN
		SELECT @sourceId = SKU.ProductID 
		FROM inserted sku;
		
		SET @source = 'Product';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'SKU';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterSupplierUpdate] on [dbo].[ZNodeSupplier]'
GO
CREATE TRIGGER [dbo].[AfterSupplierUpdate] ON [dbo].[ZNodeSupplier]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (NAME) )
	BEGIN
		SELECT @sourceId = supplier.SupplierID 
		FROM inserted supplier;
		
		SET @source = 'Supplier';
		SET	@transactionType = 'UPDATE';		
		SET @affectedType = 'Supplier';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterTagUpdate] on [dbo].[ZNodeTag]'
GO
CREATE TRIGGER [dbo].[AfterTagUpdate] ON [dbo].[ZNodeTag]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (TAGGROUPID) OR UPDATE (TAGNAME) )
	BEGIN
		SELECT @sourceId = tag.TagID 
		FROM inserted tag;
		
		SET @source = 'Tag';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'Tag';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterTagGroupUpdate] on [dbo].[ZNodeTagGroup]'
GO
CREATE TRIGGER [dbo].[AfterTagGroupUpdate] ON [dbo].[ZNodeTagGroup]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (TagGroupLabel) )
	BEGIN
		SELECT @sourceId = tagGroup.TagGroupID 
		FROM inserted tagGroup;
		
		SET @source = 'TagGroup';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'TagGroup';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterTagGroupCategoryDelete] on [dbo].[ZNodeTagGroupCategory]'
GO
CREATE TRIGGER [dbo].[AfterTagGroupCategoryDelete] ON [dbo].[ZNodeTagGroupCategory]
AFTER DELETE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = tagGroupCategory.CategoryID 
	FROM deleted tagGroupCategory;
		   
	SET @source = 'CategoryFacet';
	SET @transactionType = 'DELETE';
	SET @affectedType = 'TagGroupCategory';
	
	if (@sourceId IS NOT NULL)
	BEGIN
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
	END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterTagGroupCategoryInsert] on [dbo].[ZNodeTagGroupCategory]'
GO

CREATE TRIGGER [dbo].[AfterTagGroupCategoryInsert] ON [dbo].[ZNodeTagGroupCategory]  
AFTER INSERT  
AS  
 DECLARE @sourceId int,  
   @source varchar(50),  
   @transactionType varchar(10),  
   @affectedType varchar(50);  
  
 SELECT @sourceId = tagGroupCategory.CategoryID   
 FROM inserted tagGroupCategory;  
       
 SET @source = 'CategoryFacet';  
 SET @transactionType = 'INSERT';  
 SET @affectedType = 'TagGroupCategory';  
   
 	IF @sourceId IS NOT NULL
 INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)  
  VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterTagGroupCategoryUpdate] on [dbo].[ZNodeTagGroupCategory]'
GO
CREATE TRIGGER [dbo].[AfterTagGroupCategoryUpdate] ON [dbo].[ZNodeTagGroupCategory]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (TagGroupID) OR UPDATE (CategoryID) )
	BEGIN
		SELECT @sourceId =  tagGroupCategory.CategoryID
		FROM inserted tagGroupCategory;
		
		SET @source = 'CategoryFacet';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'TagGroupCategory';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterTagProductSKUDelete] on [dbo].[ZNodeTagProductSKU]'
GO
CREATE TRIGGER [dbo].[AfterTagProductSKUDelete] ON [dbo].[ZNodeTagProductSKU]  
AFTER DELETE  
AS  
 DECLARE @sourceId int,  
   @source varchar(50),  
   @transactionType varchar(10),  
   @affectedType varchar(50);  
  
 SELECT @sourceId = tagProductSKU.ProductID  
 FROM deleted tagProductSKU;  
       
 SET @source = 'Product';  
 SET @transactionType = 'DELETE';  
 SET @affectedType = 'TagProductSKU';  
IF(@sourceId IS NOT NULL)
BEGIN
 INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)  
  VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);  
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterTagProductSKUInsert] on [dbo].[ZNodeTagProductSKU]'
GO
CREATE TRIGGER [dbo].[AfterTagProductSKUInsert] ON [dbo].[ZNodeTagProductSKU]
AFTER INSERT
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);

	SELECT @sourceId = tagProductSKU.ProductID
	FROM inserted tagProductSKU;
		   
	SET @source = 'Product';
	SET @transactionType = 'INSERT';
	SET @affectedType = 'TagProductSKU';
	
	IF @sourceId IS NOT NULL
	BEGIN
	INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
	END

----------------------------------------------------------------------------------------------------------------------

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[AfterTagProductSKUUpdate] on [dbo].[ZNodeTagProductSKU]'
GO
CREATE TRIGGER [dbo].[AfterTagProductSKUUpdate] ON [dbo].[ZNodeTagProductSKU]
AFTER UPDATE
AS
	DECLARE @sourceId int,
			@source varchar(50),
			@transactionType varchar(10),
			@affectedType varchar(50);
	
	IF ( UPDATE (PRODUCTID) OR UPDATE (SKUID) OR UPDATE (TAGID) )
	BEGIN
		SELECT @sourceId = tagProductSKU.ProductID 
		FROM inserted tagProductSKU;
		
		SET @source = 'Product';
		SET	@transactionType = 'UPDATE';
		SET @affectedType = 'TagProductSKU';
		
		INSERT INTO dbo.ZNodeLuceneIndexMonitor (SourceID,SourceType,SourceTransactionType,TransactionDateTime,AffectedType)
		VALUES(@sourceId, @source, @transactionType, GETDATE(),@affectedType);
		
	END;
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
